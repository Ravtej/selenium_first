package com.ihg.automation.common;

import java.util.ArrayList;
import java.util.List;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.Days;
import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

public class DateUtils
{
    public final static String DATE_FORMAT = "ddMMMYY";
    public final static String MMMM_d_comma_YYYY = "MMMM d, YYYY";
    public final static String dd_MMM_YYYY = "ddMMMYYYY";
    public final static String EXPIRATION_DATE_PATTERN = "01MMMYY";
    public final static String YYYY_MM = "yyyy-MM";

    private DateUtils()
    {
    }

    public static LocalDate now()
    {
        return new LocalDate(DateTimeZone.UTC);
    }

    public static LocalDate getStringToDate(String date, String dateFormat)
    {
        DateTimeFormatter format = DateTimeFormat.forPattern(dateFormat);
        return format.parseLocalDate(date);
    }

    public static LocalDate getStringToDate(String date)
    {
        return getStringToDate(date, DATE_FORMAT);
    }

    public static int currentYear()
    {
        return now().getYear();
    }

    public static int previousYear()
    {
        return now().minusYears(1).getYear();
    }

    public static String getFormattedDate(int daysForward)
    {
        return getFormattedDate(daysForward, DATE_FORMAT);
    }

    public static String getFormattedDate(int daysForward, String format)
    {
        return now().plusDays(daysForward).toString(format);
    }

    public static String getFormattedDate()
    {
        return getFormattedDate(0);
    }

    public static String getFormattedDate(String format)
    {
        return getFormattedDate(0, format);
    }

    public static String getDateBackward(int daysBackward, String format)
    {
        return now().minusDays(daysBackward).toString(format);
    }

    public static String getDateBackward(int daysBackward)
    {
        return getDateBackward(daysBackward, DATE_FORMAT);
    }

    public static int getDaysBetween(String startDate, String endDate, boolean notZero)
    {
        return getDaysBetween(startDate, endDate, DATE_FORMAT, notZero);
    }

    public static int getDaysBetween(String startDate, String endDate)
    {
        return getDaysBetween(startDate, endDate, false);
    }

    public static int getDaysBetween(String startDate, String endDate, String dateFormat, boolean notZero)
    {
        int days = Days.daysBetween(getStringToDate(startDate, dateFormat), getStringToDate(endDate, dateFormat))
                .getDays();
        return (days == 0 && notZero) ? 1 : days;
    }

    public static List<String> getYearsForward(int years)
    {
        List<String> result = new ArrayList<String>();
        for (int i = 0; i < years; i++)
        {
            result.add(String.valueOf(now().plusYears(i).getYear()));
        }

        return result;
    }

    public static int getYearFromNow(int years)
    {
        return now().plusYears(years).getYear();
    }

    public static int nextYear()
    {
        return getYearFromNow(1);
    }

    public static int getYearOfCenturyFromNow(int years)
    {
        return now().plusYears(years).getYearOfCentury();
    }

    public static String convertDateTimeFormat(String initDateTime, String initFormat, String resFormat)
    {
        DateTimeFormatter fmt = DateTimeFormat.forPattern(initFormat);
        DateTime resDate = DateTime.parse(initDateTime, fmt);

        return resDate.toString(DateTimeFormat.forPattern(resFormat));
    }

    public static String getMonthsForward(int months, String format)
    {
        return getMonthsForward(months).toString(format);
    }

    public static LocalDate getMonthsForward(int months)
    {
        return now().plusMonths(months);
    }
}
