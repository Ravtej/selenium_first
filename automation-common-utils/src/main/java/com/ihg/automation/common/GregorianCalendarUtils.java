package com.ihg.automation.common;

import java.util.Date;
import java.util.GregorianCalendar;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.joda.time.LocalDate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class GregorianCalendarUtils
{
    private static final Logger logger = LoggerFactory.getLogger(GregorianCalendarUtils.class);

    public static XMLGregorianCalendar convert(LocalDate localDate)
    {
        Date date = localDate.toDate();
        GregorianCalendar gregory = new GregorianCalendar();
        gregory.setTime(date);

        try
        {
            return DatatypeFactory.newInstance().newXMLGregorianCalendar(gregory);
        }
        catch (DatatypeConfigurationException e)
        {
            logger.error("Error while trying to convert Date to  XMLGregorianCalendar", e);
        }
        return null;
    }
}
