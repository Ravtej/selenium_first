package com.ihg.automation.common;

import org.springframework.jdbc.core.JdbcTemplate;

import com.google.common.base.Function;

public interface JdbcExpectedCondition<T> extends Function<JdbcTemplate, T>
{
}
