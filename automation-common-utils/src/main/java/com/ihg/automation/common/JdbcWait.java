package com.ihg.automation.common;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.NotFoundException;
import org.openqa.selenium.support.ui.Clock;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Sleeper;
import org.openqa.selenium.support.ui.SystemClock;
import org.springframework.jdbc.core.JdbcTemplate;

public class JdbcWait extends FluentWait<JdbcTemplate>
{
    public JdbcWait(JdbcTemplate jdbcTemplate, long timeOutInSeconds, long sleepInMillis)
    {
        this(jdbcTemplate, new SystemClock(), Sleeper.SYSTEM_SLEEPER, timeOutInSeconds, sleepInMillis);
    }

    protected JdbcWait(JdbcTemplate jdbcTemplate, Clock clock, Sleeper sleeper, long timeOutInSeconds, long sleepTimeOut)
    {
        super(jdbcTemplate, clock, sleeper);
        withTimeout(timeOutInSeconds, TimeUnit.SECONDS);
        pollingEvery(sleepTimeOut, TimeUnit.MILLISECONDS);
        ignoring(NotFoundException.class);
    }
}
