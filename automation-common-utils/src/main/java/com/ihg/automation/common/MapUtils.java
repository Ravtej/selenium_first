package com.ihg.automation.common;

import java.util.Map;
import java.util.stream.Collectors;

public class MapUtils
{
    // convert Map<String, Object> to Map<String, String> to avoid .toString()
    // converting in Tests
    public static Map<String, String> queryResult(Map<String, Object> oldMap)
    {
        return oldMap.entrySet().stream()
                .collect(Collectors.toMap(Map.Entry::getKey, e -> (String) e.getValue().toString()));
    }
}
