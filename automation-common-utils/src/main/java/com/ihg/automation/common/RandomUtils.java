package com.ihg.automation.common;

import java.util.Random;

import org.joda.time.DateTimeZone;
import org.joda.time.LocalDateTime;

public class RandomUtils
{
    private static final char[] NUMBERS = "0123456789".toCharArray();
    private static final String DATE_TIME_PATTERN = "yyyy.MM.dd.HH.mm.ss";

    private RandomUtils()
    {
    }

    private static StringBuilder getRandomNumber(final char[] numbers, final int length, final boolean withLeadingNil)
    {
        Random rnd = new Random();
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < length; i++)
        {
            sb.append(numbers[rnd.nextInt(numbers.length)]);
        }

        if (!withLeadingNil && sb.charAt(0) == '0')
        {
            sb = getRandomNumber(numbers, length, false);
        }

        return sb;
    }

    public static String getRandomNumber(final int length)
    {
        return getRandomNumber(length, true);
    }

    public static String getRandomNumber(final int length, final boolean withLeadingNil)
    {
        return getRandomNumber(NUMBERS, length, withLeadingNil).toString();
    }

    public static String getRandomLetters(final int length)
    {
        Random r = new Random();
        StringBuilder namePrefix = new StringBuilder();

        for (int index = 1; index <= length; index++)
        {
            char letter = (char) (r.nextInt(26) + 'a');
            namePrefix = namePrefix.append(letter);
        }
        return namePrefix.toString();
    }

    public static String getTimeStamp()
    {
        return new LocalDateTime(DateTimeZone.UTC).toString(DATE_TIME_PATTERN);
    }
}
