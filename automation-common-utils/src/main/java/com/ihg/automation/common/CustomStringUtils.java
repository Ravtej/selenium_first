package com.ihg.automation.common;

import java.text.MessageFormat;
import java.util.Arrays;
import java.util.Collection;

public class CustomStringUtils
{
    public static boolean isEmpty(String value)
    {
        return value == null || value.length() == 0;
    }

    public static String format(String str, String pattern)
    {
        if (!isEmpty(str))
        {
            return MessageFormat.format(pattern, str);
        }
        return null;
    }

    public static String concat(String separator, Collection<String> values)
    {
        StringBuilder result = new StringBuilder();
        for (String value : values)
        {
            if (!isEmpty(value))
            {
                if (result.length() > 0)
                {
                    result.append(separator);
                }
                result.append(value);
            }
        }
        return result.toString();
    }

    public static String concat(String separator, String... values)
    {
        return concat(separator, Arrays.asList(values));
    }

}
