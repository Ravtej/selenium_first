package com.ihg.crm.automation.service.refdata;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.xml.datatype.XMLGregorianCalendar;

import com.ihg.crm.refdata.ws.v1_0.types.service.ConvertCurrenciesRequestType;
import com.ihg.crm.refdata.ws.v1_0.types.service.CurrencyConversionRequestItemType;

public class ConvertCurrenciesRequestHelper
{
    public static ConvertCurrenciesRequestType getCurrencies(Integer amount, String currencyCode,
            XMLGregorianCalendar date)
    {
        CurrencyConversionRequestItemType currencyConversionRequestItemType = new CurrencyConversionRequestItemType();
        currencyConversionRequestItemType.setAmount(BigDecimal.valueOf(amount));
        currencyConversionRequestItemType.setCurrencyCode(currencyCode);
        currencyConversionRequestItemType.setAsOf(date);
        List<CurrencyConversionRequestItemType> conversionList = new ArrayList<>();
        conversionList.add(currencyConversionRequestItemType);

        ConvertCurrenciesRequestType convertCurrenciesRequestType = new ConvertCurrenciesRequestType();
        convertCurrenciesRequestType.setConversionRequestItem(conversionList);

        return convertCurrenciesRequestType;
    }

}
