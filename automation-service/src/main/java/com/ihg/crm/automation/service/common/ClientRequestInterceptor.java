package com.ihg.crm.automation.service.common;

import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.apache.cxf.interceptor.Fault;
import org.apache.cxf.message.Message;
import org.apache.cxf.phase.AbstractPhaseInterceptor;
import org.apache.cxf.phase.Phase;
import org.springframework.beans.factory.annotation.Value;

import com.ihg.atp.schema.common.atpcommonservicetypes.v1.ChannelType;
import com.ihg.atp.schema.common.atpcommonservicetypes.v1.EnvironmentType;
import com.ihg.atp.schema.common.atpcommonservicetypes.v1.HeaderVersionType;
import com.ihg.atp.schema.common.atpcommonservicetypes.v1.LanguageType;
import com.ihg.atp.schema.common.atpcommonservicetypes.v1.RequestHeaderType;
import com.ihg.atp.schema.common.atpcommonservicetypes.v1.RequestType;
import com.ihg.atp.schema.common.atpcommonservicetypes.v1.TransactionIDType;
import com.ihg.atp.schema.common.atpcommonservicetypes.v1.TransactionSequenceType;
import com.ihg.atp.schema.common.atpcommonservicetypes.v1.UserType;

public class ClientRequestInterceptor extends AbstractPhaseInterceptor<Message>
{
    public static final String MAJOR_HEADER_VERSION = "1";
    public static final String MINOR_HEADER_VERSION = "0";
    public static final String DEFAULT_ISO_COUNTRY = "US";
    public static final String DEFAULT_ISO_LANGUAGE = "en";
    private String channelName;
    private String userId;

    @Value("${env:int}")
    protected String env;

    /**
     * Default constructor, with interception on USER_LOGICAL
     * {@link org.apache.cxf.phase.Phase}
     */
    public ClientRequestInterceptor()
    {
        super(Phase.USER_LOGICAL);
    }

    public void setChannelName(String channelName)
    {
        this.channelName = channelName;
    }

    public String getUserId()
    {
        return userId;
    }

    public void setUserId(String userId)
    {
        this.userId = userId;
    }

    @Override
    public void handleMessage(Message message) throws Fault
    {
        @SuppressWarnings("unchecked")
        List<Object> messageList = message.getContent(List.class);
        for (Object object : messageList)
        {
            try
            {
                if (object instanceof RequestType)
                {
                    RequestType request = (RequestType) object;

                    RequestHeaderType header = request.getHeader();
                    if (header == null)
                    {
                        header = new RequestHeaderType();
                    }
                    EnvironmentType environment = new EnvironmentType();
                    environment.setName(env);
                    header.setEnvironment(environment);
                    LanguageType languageType = new LanguageType();
                    languageType.setIsoCountryCode(DEFAULT_ISO_COUNTRY);
                    languageType.setIsoLanguageCode(DEFAULT_ISO_LANGUAGE);
                    header.setLanguage(languageType);
                    HeaderVersionType headerVersion = new HeaderVersionType();
                    headerVersion.setMajor(MAJOR_HEADER_VERSION);
                    headerVersion.setMinor(MAJOR_HEADER_VERSION);
                    header.setHeaderVersion(headerVersion);
                    header.setSendTimeStamp(DateUtils.toXMLCalendar(new Date()));
                    TransactionIDType transactionIDType = new TransactionIDType();
                    header.setTransactionId(transactionIDType);
                    transactionIDType.setId(UUID.randomUUID().toString());
                    TransactionSequenceType transactionSequence = new TransactionSequenceType();
                    header.setTransactionSequence(transactionSequence);
                    transactionSequence.setNumber(0);
                    ChannelType chanelType = new ChannelType();
                    chanelType.setName(channelName);
                    header.setChannel(chanelType);

                    UserType user = new UserType();
                    user.setId(userId);
                    header.setUser(user);
                    request.setHeader(header);
                }
            }
            catch (Exception e)
            {
                throw new Fault(e);
            }
        }
    }

}
