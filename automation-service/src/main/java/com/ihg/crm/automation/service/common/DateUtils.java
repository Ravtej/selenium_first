package com.ihg.crm.automation.service.common;

import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

public class DateUtils
{
    private static final TimeZone UTC_TIME_ZONE = TimeZone.getTimeZone("UTC");
    private static DatatypeFactory DATATYPE_FACTORY;

    static
    {
        try
        {
            DATATYPE_FACTORY = DatatypeFactory.newInstance();
        }
        catch (DatatypeConfigurationException ex)
        {
            throw new RuntimeException(ex);
        }
    }

    public static XMLGregorianCalendar toXMLCalendar(final Date date)
    {
        if (date != null)
        {
            final GregorianCalendar calendar = new GregorianCalendar();
            calendar.setTime(date);
            calendar.setTimeZone(UTC_TIME_ZONE);
            return DATATYPE_FACTORY.newXMLGregorianCalendar(calendar);
        }

        return null;
    }
}
