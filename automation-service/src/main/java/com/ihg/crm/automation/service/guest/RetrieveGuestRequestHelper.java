package com.ihg.crm.automation.service.guest;

import java.util.ArrayList;

import javax.xml.bind.JAXBElement;

import com.ihg.atp.schema.crm.guest.guest.datatypes.v2.DataConstructionType;
import com.ihg.atp.schema.crm.guest.guest.servicetypes.v2.DataConstructionInstructionType;
import com.ihg.atp.schema.crm.guest.guest.servicetypes.v2.GuestIdEntryType;
import com.ihg.atp.schema.crm.guest.guest.servicetypes.v2.MembershipIdKeyType;
import com.ihg.atp.schema.crm.guest.guest.servicetypes.v2.ObjectFactory;
import com.ihg.atp.schema.crm.guest.guest.servicetypes.v2.RetrieveGuestRequestType;

public class RetrieveGuestRequestHelper
{
    public static RetrieveGuestRequestType byGuestId(String guestId)
    {
        RetrieveGuestRequestType request = new RetrieveGuestRequestType();

        ObjectFactory objectFactory = new ObjectFactory();

        GuestIdEntryType guestIdType = new GuestIdEntryType();
        guestIdType.setGuestId(Long.valueOf(guestId));
        request.setQuery(objectFactory.createByGuestId(guestIdType));

        request.getInstruction().add(getDeclaredConstructionInstruction());

        return request;
    }

    public static RetrieveGuestRequestType byMemberId(String memberId)
    {
        RetrieveGuestRequestType request = new RetrieveGuestRequestType();

        ObjectFactory objectFactory = new ObjectFactory();

        MembershipIdKeyType membershipIdKeyType = new MembershipIdKeyType();
        membershipIdKeyType.setMembershipId(memberId);
        ArrayList<String> programList = new ArrayList<>();
        programList.add("PC");
        membershipIdKeyType.setProgramCode(programList);
        request.setQuery(objectFactory.createByMembershipId(membershipIdKeyType));

        request.getInstruction().add(getDeclaredConstructionInstruction());

        return request;
    }

    private static JAXBElement<DataConstructionInstructionType> getDeclaredConstructionInstruction()
    {
        ObjectFactory objectFactory = new ObjectFactory();

        DataConstructionInstructionType dataConstructionInstructionType = new DataConstructionInstructionType();
        dataConstructionInstructionType.setDataConstruction(DataConstructionType.DECLARED);
        return objectFactory.createDataConstructionInstruction(dataConstructionInstructionType);
    }
}
