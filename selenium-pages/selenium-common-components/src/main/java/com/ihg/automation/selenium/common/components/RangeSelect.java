package com.ihg.automation.selenium.common.components;

import com.ihg.automation.selenium.gwt.components.Component;
import com.ihg.automation.selenium.gwt.components.input.InputXpath;
import com.ihg.automation.selenium.gwt.components.input.Select;

public class RangeSelect extends Select
{

    public RangeSelect(Component parent, InputXpath xpath, String description)
    {
        super(parent, xpath, description);
    }

    public void select(Range range)
    {
        super.select(range.getLabel());
    }

    @Override
    public String getDefaultValue()
    {
        return Range.ZERO_MONTH.getLabel();
    }

    public enum Range
    {
        ZERO_MONTH("+/- 0 Month"), ONE_MONTH("+/- 1 Month"), TWO_MONTH("+/- 2 Month"), THREE_MONTH("+/- 3 Month");

        private String label;

        Range(String label)
        {
            this.label = label;
        }

        public String getLabel()
        {
            return label;
        }
    }
}
