package com.ihg.automation.selenium.common.components;

import com.ihg.automation.selenium.gwt.components.EntityType;

public enum Country implements EntityType
{
    US("US", "UNITED STATES"), //
    CA("CA", "CANADA"), //
    ES("ES", "SPAIN"), //
    FR("FR", "FRANCE"), //
    BY("BY", "BELARUS"), //
    RU("RU", "RUSSIAN FEDERATION"), //
    CN("CN", "CHINA"), //
    TW("TW", "TAIWAN, PROVINCE OF CHINA"), //
    HK("HK", "HONG KONG"), //
    MO("MO", "MACAO"), //
    AU("AU", "AUSTRALIA"), //
    ID("ID", "INDONESIA"), //
    UA("UA", "UKRAINE"), //
    AR("AR", "ARGENTINA"), //
    BR("BR", "BRAZIL"), //
    ER("ER", "ERITREA"), //
    JP("JP", "JAPAN");

    private String code;
    private String value;

    private Country(String code, String value)
    {
        this.code = code;
        this.value = value;
    }

    @Override
    public String getCode()
    {
        return code;
    }

    @Override
    public String getValue()
    {
        return value;
    }

    @Override
    public String getCodeWithValue()
    {
        return code + " - " + value;
    }

    @Override
    public String toString()
    {
        return getValue();
    }
}
