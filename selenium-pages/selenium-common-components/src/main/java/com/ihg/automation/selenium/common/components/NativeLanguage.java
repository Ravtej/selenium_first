package com.ihg.automation.selenium.common.components;

public class NativeLanguage
{

    public static final String SIMPLIFIED_CHINESE = "Simplified Chinese";
    public static final String TRADITIONAL_CHINESE = "Traditional Chinese";

    private NativeLanguage()
    {
    }
}
