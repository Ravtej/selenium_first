package com.ihg.automation.selenium.common.components;

import com.ihg.automation.selenium.gwt.components.Component;
import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.input.InputXpath;
import com.ihg.automation.selenium.gwt.components.input.Select;

public class CountrySelect extends Select
{
    public CountrySelect(Container container)
    {
        super(container.getLabel("Country"), "Country");
    }

    public CountrySelect(Container container, String labelDescription)
    {
        super(container.getLabel(labelDescription), labelDescription);
    }

    public CountrySelect(Component parent, InputXpath xpath, String description)
    {
        super(parent, xpath, description);
    }

    public void select(Country country)
    {
        super.selectByCode(country);
    }
}
