package com.ihg.automation.selenium.common.components;

import com.ihg.automation.selenium.gwt.components.Component;
import com.ihg.automation.selenium.gwt.components.input.InputXpath;
import com.ihg.automation.selenium.gwt.components.input.Select;

public class IndicatorSelect extends Select
{
    public static final String YES = "Yes";
    public static final String NO = "No";

    public IndicatorSelect(Component parent)
    {
        super(parent, parent.getName());
    }

    public IndicatorSelect(Component parent, InputXpath xpath, String description)
    {
        super(parent, xpath, description);
    }

    public void select(boolean indicator)
    {
        if (indicator)
        {
            selectYes();
        }
        else
        {
            selectNo();
        }
    }

    public void selectYes()
    {
        super.select(YES);
    }

    public void selectNo()
    {
        super.select(NO);
    }
}
