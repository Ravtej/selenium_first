package com.ihg.automation.selenium.hotel.pages.order;

import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.CustomContainerBase;
import com.ihg.automation.selenium.gwt.components.input.DateInput;
import com.ihg.automation.selenium.gwt.components.input.Input;
import com.ihg.automation.selenium.gwt.components.input.Select;

public class OrdersSearch extends CustomContainerBase
{
    public OrdersSearch(Container container)
    {
        super(container);
    }

    public Input getItemId()
    {
        return container.getInputByLabel("Item ID");
    }

    public Input getItemName()
    {
        return container.getInputByLabel("Item Name");
    }

    public DateInput getDateFrom()
    {
        return new DateInput(container.getLabel("From"));
    }

    public DateInput getDateTo()
    {
        return new DateInput(container.getLabel("To"));
    }

    public Select getStatus()
    {
        return container.getSelectByLabel("Status");
    }
}
