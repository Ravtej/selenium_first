package com.ihg.automation.selenium.hotel.pages.hoteloperations.br.posted;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.hotel.pages.hoteloperations.br.posted.PostedEventsGridRow.PostedEventsGridCell.CURRENCY;
import static com.ihg.automation.selenium.hotel.pages.hoteloperations.br.posted.PostedEventsGridRow.PostedEventsGridCell.END_DATE;
import static com.ihg.automation.selenium.hotel.pages.hoteloperations.br.posted.PostedEventsGridRow.PostedEventsGridCell.ESTIMATED_COST;
import static com.ihg.automation.selenium.hotel.pages.hoteloperations.br.posted.PostedEventsGridRow.PostedEventsGridCell.EVENT_ID;
import static com.ihg.automation.selenium.hotel.pages.hoteloperations.br.posted.PostedEventsGridRow.PostedEventsGridCell.EVENT_NAME;
import static com.ihg.automation.selenium.hotel.pages.hoteloperations.br.posted.PostedEventsGridRow.PostedEventsGridCell.MBR_NUMBER;
import static com.ihg.automation.selenium.hotel.pages.hoteloperations.br.posted.PostedEventsGridRow.PostedEventsGridCell.MEMBER_NAME;
import static com.ihg.automation.selenium.hotel.pages.hoteloperations.br.posted.PostedEventsGridRow.PostedEventsGridCell.POINTS_AWARDED;
import static com.ihg.automation.selenium.hotel.pages.hoteloperations.br.posted.PostedEventsGridRow.PostedEventsGridCell.START_DATE;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;

import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.grid.Grid;
import com.ihg.automation.selenium.gwt.components.grid.GridHeader;

public class PostedEventsGrid extends Grid<PostedEventsGridRow, PostedEventsGridRow.PostedEventsGridCell>
{
    public PostedEventsGrid(Container parent)
    {
        super(parent);
    }

    public void verifyHeader()
    {
        GridHeader<PostedEventsGridRow.PostedEventsGridCell> header = getHeader();
        verifyThat(header.getCell(MEMBER_NAME), hasText(MEMBER_NAME.getValue()));
        verifyThat(header.getCell(MBR_NUMBER), hasText(MBR_NUMBER.getValue()));
        verifyThat(header.getCell(EVENT_ID), hasText(EVENT_ID.getValue()));
        verifyThat(header.getCell(EVENT_NAME), hasText(EVENT_NAME.getValue()));
        verifyThat(header.getCell(START_DATE), hasText(START_DATE.getValue()));
        verifyThat(header.getCell(END_DATE), hasText(END_DATE.getValue()));
        verifyThat(header.getCell(POINTS_AWARDED), hasText(POINTS_AWARDED.getValue()));
        verifyThat(header.getCell(ESTIMATED_COST), hasText(ESTIMATED_COST.getValue()));
        verifyThat(header.getCell(CURRENCY), hasText(CURRENCY.getValue()));
    }
}
