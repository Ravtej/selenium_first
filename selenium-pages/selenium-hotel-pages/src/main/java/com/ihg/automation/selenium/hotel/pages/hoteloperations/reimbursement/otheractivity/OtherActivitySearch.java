package com.ihg.automation.selenium.hotel.pages.hoteloperations.reimbursement.otheractivity;

import java.util.ArrayList;
import java.util.Arrays;

import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.CustomContainerBase;
import com.ihg.automation.selenium.gwt.components.checkbox.CheckBox;
import com.ihg.automation.selenium.gwt.components.input.DateInput;
import com.ihg.automation.selenium.gwt.components.input.Input;
import com.ihg.automation.selenium.gwt.components.input.Select;
import com.ihg.automation.selenium.gwt.components.utils.ByText;

public class OtherActivitySearch extends CustomContainerBase
{
    public static ArrayList<String> OTHER_STATUS_LIST = new ArrayList<>(
            Arrays.asList("INITIATED", "CANCELED", "ACCEPTED", "PAID", "All Statuses"));

    public OtherActivitySearch(Container container)
    {
        super(container);
    }

    public Select getHotelCode()
    {
        return container.getSelectByLabel("Hotel Code");
    }

    public Input getItemID()
    {
        return container.getInputByLabel("Item ID");
    }

    public Select getStatusOfRequest()
    {
        return container.getSelectByLabel("Status of Request");
    }

    public Input getCertificateNumber()
    {
        return container.getInputByLabel("Certificate Number");
    }

    public Input getConfirmationNumber()
    {
        return container.getInputByLabel("Confirmation Number");
    }

    public CheckBox getCheckIn()
    {
        return new CheckBox(container, ByText.exact("Check In"), CheckBox.CHECK_BOX_PATTERN2);
    }

    public CheckBox getCheckOut()
    {
        return new CheckBox(container, ByText.exact("Check Out"), CheckBox.CHECK_BOX_PATTERN2);
    }

    public DateInput getFromDate()
    {
        return new DateInput(container.getLabel("From"));
    }

    public DateInput getToDate()
    {
        return new DateInput(container.getLabel("To"));
    }
}
