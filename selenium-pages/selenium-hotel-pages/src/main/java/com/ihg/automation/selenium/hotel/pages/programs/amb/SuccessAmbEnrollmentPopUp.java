package com.ihg.automation.selenium.hotel.pages.programs.amb;

import com.ihg.automation.selenium.common.enroll.SuccessEnrolmentPopUp;
import com.ihg.automation.selenium.gwt.components.label.Label;

public class SuccessAmbEnrollmentPopUp extends SuccessEnrolmentPopUp
{
    public Label getPaymentMessage()
    {
        return container.getLabel("Do not forget to collect a payment:");
    }
}
