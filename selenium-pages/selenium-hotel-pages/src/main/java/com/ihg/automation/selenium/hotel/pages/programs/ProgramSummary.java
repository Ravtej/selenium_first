package com.ihg.automation.selenium.hotel.pages.programs;

import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.CustomContainerBase;
import com.ihg.automation.selenium.gwt.components.label.Label;

public class ProgramSummary extends CustomContainerBase
{
    public ProgramSummary(Container container)
    {
        super(container.getSeparator("Program Summary"));
    }

    public Label getMemberId()
    {
        return container.getLabel("Member ID");
    }

    public Label getStatus()
    {
        return container.getLabel("Status");
    }
}
