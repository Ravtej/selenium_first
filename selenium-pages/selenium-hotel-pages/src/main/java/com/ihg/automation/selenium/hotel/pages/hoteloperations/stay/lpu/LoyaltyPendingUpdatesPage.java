package com.ihg.automation.selenium.hotel.pages.hoteloperations.stay.lpu;

import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyNoError;

import com.ihg.automation.selenium.common.event.SearchAware;
import com.ihg.automation.selenium.common.event.SearchButtonBar;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.pages.TabCustomContainerBase;
import com.ihg.automation.selenium.common.stay.Stay;
import com.ihg.automation.selenium.gwt.components.Button;
import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.CustomContainerBase;
import com.ihg.automation.selenium.gwt.components.input.Select;
import com.ihg.automation.selenium.gwt.components.label.Label;
import com.ihg.automation.selenium.hotel.pages.LeftPanel;
import com.ihg.automation.selenium.hotel.pages.Tabs;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.HotelOperationsPage;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.stay.ResultsFilter;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.stay.lpu.missingstay.StayCreatePopUp;
import com.ihg.automation.selenium.listener.Verifier;

public class LoyaltyPendingUpdatesPage extends TabCustomContainerBase implements SearchAware
{
    public LoyaltyPendingUpdatesPage()
    {
        super(new Tabs().getStay().getLoyaltyPendingUpdates());
    }

    @Override
    public SearchButtonBar getButtonBar()
    {
        return new SearchButtonBar(container);
    }

    @Override
    public LoyaltyPendingUpdatesSearch getSearchFields()
    {
        return new LoyaltyPendingUpdatesSearch(container);
    }

    public Button getCreateMissingStay()
    {
        return container.getButton("Create Missing Stay");
    }

    public void clickCreateMissingStay(Verifier... verifiers)
    {
        getCreateMissingStay().click(verifiers);
    }

    public LoyaltyPendingUpdatesGrid getLoyaltyPendingUpdatesGrid()
    {
        return new LoyaltyPendingUpdatesGrid(container);
    }

    public class LoyaltyPendingUpdatesSearch extends CustomContainerBase
    {
        public LoyaltyPendingUpdatesSearch(Container container)
        {
            super(container);
        }

        public Select getHotelCode()
        {
            return container.getSelectByLabel("Hotel Code");
        }

        public Select getAvailableDates()
        {
            return container.getSelectByLabel("Available dates");
        }

        public Label getLastDateToAdjust()
        {
            return container.getLabel("Last Date to Adjust");
        }
    }

    public ResultsFilter getResultsFilter()
    {
        return new ResultsFilter(container);
    }

    public void createMissingStay(Member member, Stay stay)
    {
        clickCreateMissingStay(verifyNoError());
        StayCreatePopUp stayCreatePopUp = new StayCreatePopUp();
        stayCreatePopUp.populate(member, stay);
        stayCreatePopUp.createStay(verifyNoError());
    }

    public LoyaltyPendingUpdatesPage goToByHotelOperations()
    {
        new LeftPanel().clickHotelOperations(verifyNoError());
        new HotelOperationsPage().clickLoyaltyPendingUpdates(verifyNoError());
        return this;
    }
}
