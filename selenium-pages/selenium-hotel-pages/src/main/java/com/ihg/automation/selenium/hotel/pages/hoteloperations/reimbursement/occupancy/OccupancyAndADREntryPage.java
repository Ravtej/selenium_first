package com.ihg.automation.selenium.hotel.pages.hoteloperations.reimbursement.occupancy;

import java.util.ArrayList;
import java.util.Arrays;

import com.ihg.automation.selenium.common.event.SearchAware;
import com.ihg.automation.selenium.common.event.SearchButtonBar;
import com.ihg.automation.selenium.common.pages.TabCustomContainerBase;
import com.ihg.automation.selenium.hotel.pages.Tabs;

public class OccupancyAndADREntryPage extends TabCustomContainerBase implements SearchAware
{
    public static ArrayList<String> STATUS_LIST = new ArrayList<>(
            Arrays.asList("INITIATED", "PENDING", "ACCEPTED", "PAID", "All Statuses"));

    public static ArrayList<String> TYPE_LIST = new ArrayList<>(
            Arrays.asList("All Types", "Reward Night", "Free Night"));

    public OccupancyAndADREntryPage()
    {
        super(new Tabs().getReimbursement().getOccupancyAndADREntry());
    }

    @Override
    public SearchButtonBar getButtonBar()
    {
        return new SearchButtonBar(container);
    }

    @Override
    public OccupancyAndADREntrySearch getSearchFields()
    {
        return new OccupancyAndADREntrySearch(container);
    }

    public OccupancyAndADREntrySearchGrid getOccupancyAndADREntrySearchGrid()
    {
        return new OccupancyAndADREntrySearchGrid(container);
    }
}
