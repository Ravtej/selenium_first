package com.ihg.automation.selenium.hotel.pages.hoteloperations.awards.manage;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.assertThat;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;

import com.ihg.automation.selenium.common.event.SearchAware;
import com.ihg.automation.selenium.common.event.SearchButtonBar;
import com.ihg.automation.selenium.common.pages.TabCustomContainerBase;
import com.ihg.automation.selenium.gwt.components.Button;
import com.ihg.automation.selenium.hotel.pages.LeftPanel;
import com.ihg.automation.selenium.hotel.pages.Tabs;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.HotelOperationsPage;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.awards.CreatePointAwardPopUp;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.awards.PointAwardBase;
import com.ihg.automation.selenium.listener.Verifier;
import com.ihg.automation.selenium.matchers.component.IsSelected;

public class ManagePointAwardsPage extends TabCustomContainerBase implements SearchAware
{
    public ManagePointAwardsPage()
    {
        super(new Tabs().getAwardsTab().getManagePointAwards());
    }

    public void goToByOperation()
    {
        if (!this.isDisplayed() || !getTab().isSelected())
        {
            new LeftPanel().getHotelOperations().clickAndWait();
            new HotelOperationsPage().getPointAwards().clickAndWait();

            flush();
            assertThat(getTab(), IsSelected.isSelected(true));
            assertThat(this, displayed(true));
        }
    }

    @Override
    public SearchButtonBar getButtonBar()
    {
        return new SearchButtonBar(container);
    }

    @Override
    public ManagePointAwardsSearch getSearchFields()
    {
        return new ManagePointAwardsSearch(container);
    }

    public ResultsFilter getResultsFilter()
    {
        return new ResultsFilter(container);
    }

    public ManagePointAwardsGrid getManagePointAwardsGrid()
    {
        return new ManagePointAwardsGrid(container);
    }

    public Button getCreatePointAward()
    {
        return container.getButton("Create Point Award");
    }

    public Button getApproveSelected()
    {
        return container.getButton("Approve Selected");
    }

    public void createPointAward(PointAwardBase pointAward, Verifier... verifiers)
    {
        getCreatePointAward().clickAndWait();
        new CreatePointAwardPopUp().createPointAward(pointAward, verifiers);
    }
}
