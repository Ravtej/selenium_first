package com.ihg.automation.selenium.hotel.pages.hoteloperations;

import static com.ihg.automation.selenium.gwt.components.utils.FindStepUtils.byXpathWithWait;

import com.ihg.automation.selenium.gwt.components.Button;
import com.ihg.automation.selenium.gwt.components.CustomContainerBase;
import com.ihg.automation.selenium.gwt.components.utils.ByText;
import com.ihg.automation.selenium.listener.Verifier;

public class HotelOperationsPage extends CustomContainerBase
{
    public HotelOperationsPage()
    {
        super(null, "Search Panel", "Hotel Operations", byXpathWithWait(
                ".//div[@id='y-cental-panel-id' and descendant::div[normalize-space(text())='Hotel Operations']]"));
    }

    public Button getLoyaltyPendingUpdates()
    {
        return container.getButton("Loyalty Pending Updates");
    }

    public void clickLoyaltyPendingUpdates(Verifier... verifiers)
    {
        getLoyaltyPendingUpdates().clickAndWait(verifiers);
    }

    public Button getReviewGuestStays()
    {
        return container.getButton("Review Guest Stays");
    }

    public Button getIHGBusinessRewardsEventsCreateReviewAndApprove()
    {
        return new Button(container, ByText.exactSelf("IHG Business RewardsEvents - Create, Reviewand Approve"));
    }

    public Button getIHGBusinessRewardsEventsPosted()
    {
        return new Button(container, ByText.exactSelf("IHG Business RewardsEvents - Posted"));
    }

    public Button getProcessHotelReimbForRewardFreeNights()
    {
        return new Button(container, ByText.exactSelf("Process HotelReimbursement forReward/Free Nights Stays"));
    }

    public Button getSearchRewardFreeNightsStayByCertificate()
    {
        return new Button(container, ByText.exactSelf("Search for a Reward/Free Nights StayBy Certificate"));
    }

    public Button getHotelsRewardNightsSettings()
    {
        return new Button(container, ByText.exactSelf("View Your Hotel'sReward Nights Settings"));
    }

    public Button getHotelsFlatReimbursement()
    {
        return new Button(container, ByText.exactSelf("View Your Hotel'sFlat Reimbursement forFree Night Stays"));
    }

    public Button getOtherActivity()
    {
        return container.getButton("Other Activity");
    }

    public Button getReports()
    {
        return container.getButton("Reports");
    }

    public Button getPointAwards()
    {
        return container.getButton("Point Awards");
    }

}
