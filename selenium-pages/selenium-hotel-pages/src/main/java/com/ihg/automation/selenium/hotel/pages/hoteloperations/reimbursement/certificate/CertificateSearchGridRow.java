package com.ihg.automation.selenium.hotel.pages.hoteloperations.reimbursement.certificate;

import com.ihg.automation.selenium.gwt.components.grid.CellsName;
import com.ihg.automation.selenium.gwt.components.grid.GridRow;
import com.ihg.automation.selenium.gwt.components.grid.GridRowXpath;

public class CertificateSearchGridRow extends GridRow<CertificateSearchGridRow.CertificateSearchGridCell>
{
    public CertificateSearchGridRow(CertificateSearchGrid parent, GridRowXpath xpath)
    {
        super(parent, xpath);
    }

    public enum CertificateSearchGridCell implements CellsName
    {
        CONFIRMATION_NUMBER("confirmationNumber"), MEMBER_ID("memberId"), CERTIFICATE_TYPE("certificateType"), LAST_NAME(
                "lastName"), CHECK_IN("checkIn"), CHECK_OUT("checkOut"), CERTIFICATE_NUMBER("rewardNumber"), TOTAL_ACTIVE_NIGHTS(
                "totalActiveNights"), TOTAL_REIMBURSED("totalReimbursed");

        private String name;

        private CertificateSearchGridCell(String name)
        {
            this.name = name;
        }

        @Override
        public String getName()
        {
            return name;
        }
    }
}
