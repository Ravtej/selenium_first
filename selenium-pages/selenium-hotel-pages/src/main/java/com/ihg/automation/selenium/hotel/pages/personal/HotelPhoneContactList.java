package com.ihg.automation.selenium.hotel.pages.personal;

import com.ihg.automation.selenium.common.member.GuestPhone;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.personal.ContactListBase;
import com.ihg.automation.selenium.common.personal.PhoneContact;
import com.ihg.automation.selenium.gwt.components.Button;
import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.panel.EditablePanel;

public class HotelPhoneContactList extends ContactListBase<GuestPhone, PhoneContact>
{
    public HotelPhoneContactList(Container container)
    {
        super(container.getSeparator("Phones"));
    }

    @Override
    public Button getAddButton()
    {
        return container.getButton("Add phone");
    }

    @Override
    public PhoneContact getContact(int index)
    {
        return new PhoneContact(new EditablePanel(container, index, EditablePanel.HOTEL_XPATH));
    }

    @Override
    protected GuestPhone getRandomContact()
    {
        GuestPhone phone = MemberPopulateHelper.getRandomPhone();
        phone.setPreferred(false);
        return phone;
    }
}
