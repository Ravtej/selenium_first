package com.ihg.automation.selenium.hotel.pages.hoteloperations.reimbursement.certificate;

import java.util.ArrayList;
import java.util.Arrays;

import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.CustomContainerBase;
import com.ihg.automation.selenium.gwt.components.checkbox.CheckBox;
import com.ihg.automation.selenium.gwt.components.input.DateInput;
import com.ihg.automation.selenium.gwt.components.input.Input;
import com.ihg.automation.selenium.gwt.components.input.Select;

public class CertificateSearch extends CustomContainerBase
{
    public static ArrayList<String> TYPES = new ArrayList<>(Arrays.asList("All", "Reward Night", "Free Night"));

    public CertificateSearch(Container container)
    {
        super(container);
    }

    public Select getHotelCode()
    {
        return container.getSelectByLabel("Hotel Code");
    }

    public Input getCertificateNumber()
    {
        return container.getInputByLabel("Certificate Number");
    }

    public Select getType()
    {
        return container.getSelectByLabel("Type");
    }

    public DateInput getFromDate()
    {
        return new DateInput(container.getLabel("From"));
    }

    public DateInput getToDate()
    {
        return new DateInput(container.getLabel("To"));
    }

    public CheckBox getCheckIn()
    {
        return container.getCheckBox("Check In");
    }

    public CheckBox getCheckOut()
    {
        return container.getCheckBox("Check Out");
    }
}
