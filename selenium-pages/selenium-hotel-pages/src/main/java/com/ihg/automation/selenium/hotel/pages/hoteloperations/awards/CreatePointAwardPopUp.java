package com.ihg.automation.selenium.hotel.pages.hoteloperations.awards;

import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyNoError;

import com.ihg.automation.selenium.common.pages.SubmitPopUpBase;
import com.ihg.automation.selenium.gwt.components.Button;
import com.ihg.automation.selenium.gwt.components.dialog.ConfirmDialog;
import com.ihg.automation.selenium.gwt.components.input.Input;
import com.ihg.automation.selenium.gwt.components.input.Select;
import com.ihg.automation.selenium.gwt.components.utils.ByText;
import com.ihg.automation.selenium.gwt.components.utils.WaitUtils;
import com.ihg.automation.selenium.listener.Verifier;

public class CreatePointAwardPopUp extends SubmitPopUpBase
{
    public CreatePointAwardPopUp()
    {
        super("Create Point Award");
    }

    public Input getMembershipID()
    {
        return container.getInputByLabel("Membership ID");
    }

    public Select getHotel()
    {
        return container.getSelectByLabel("Hotel");
    }

    public Select getPointAwardType()
    {
        return container.getSelectByLabel("Point Award Type");
    }

    public Select getAwardDescription()
    {
        return container.getSelectByLabel("Description");
    }

    public Select getReason()
    {
        return container.getSelectByLabel("Reason");
    }

    public Input getComment()
    {
        return container.getInputByLabel("Comment");
    }

    public Button getClear()
    {
        return container.getButton("Clear");
    }

    public void clickClear()
    {
        getClear().clickAndWait();
    }

    /**
     * Add additional waitExecutingRequest() due to there are 2 masked
     * components executing after typing membershipID
     * 
     * List in PointAwardType dropdown appears after second masked component
     * executed
     * 
     */

    public void populate(PointAwardBase pointAward)
    {
        getMembershipID().typeAndWait(pointAward.getMemberNumber());
        new WaitUtils().waitExecutingRequest();
        getPointAwardType().select(pointAward.getTransaction().getType());
        getAwardDescription().select(ByText.startsWith(pointAward.getPoints()));
        getReason().select(pointAward.getReason());
        getComment().type(pointAward.getComments());
    }

    public void createPointAward(PointAwardBase pointAward, Verifier... verifiers)
    {
        populate(pointAward);
        clickSubmit(verifyNoError());

        new ConfirmDialog().clickYes(verifiers);
    }

}
