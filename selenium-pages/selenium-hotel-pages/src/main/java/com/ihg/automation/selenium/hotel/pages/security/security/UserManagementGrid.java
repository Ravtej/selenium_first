package com.ihg.automation.selenium.hotel.pages.security.security;

import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.grid.Grid;
import com.ihg.automation.selenium.gwt.components.grid.GridRowXpath;

public class UserManagementGrid extends Grid<UserManagementGridRow, UserManagementGridRow.UserManagementGridCell>
{
    public UserManagementGrid(Container parent)
    {
        super(parent);
    }

    public UserManagementGridRow getRowByUserName(UserManagement userManagement)
    {
        return getRow(new GridRowXpath.GridRowXpathBuilder()
                .cell(UserManagementGridRow.UserManagementGridCell.USERNAME, userManagement.getUsername()).build());
    }
}
