package com.ihg.automation.selenium.hotel.pages.hoteloperations.awards.posted;

import com.ihg.automation.common.DateUtils;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.awards.PointAwardBase;

public class PostedPointAward extends PointAwardBase implements Cloneable
{
    private String hotel;
    private String transactionDate = DateUtils.getFormattedDate();
    private String costOfPoints;

    public String getHotel()
    {
        return hotel;
    }

    public void setHotel(String hotel)
    {
        this.hotel = hotel;
    }

    public String getCostOfPoints()
    {
        return costOfPoints;
    }

    public void setCostOfPoints(String costOfPoints)
    {
        this.costOfPoints = costOfPoints;
    }

    public String getTransactionDate()
    {
        return transactionDate;
    }

    public void setTransactionDate(String transactionDate)
    {
        this.transactionDate = transactionDate;
    }

    @Override
    public PostedPointAward clone()
    {
        PostedPointAward newAward = new PostedPointAward();

        newAward.setCheckInDate(getCheckInDate());
        newAward.setCheckOutDate(getCheckOutDate());
        newAward.setMemberNumber(getMemberNumber());
        newAward.setMemberName(getMemberName());
        newAward.setPoints(getPoints());
        newAward.setReason(getReason());
        newAward.setComments(getComments());
        newAward.setSource(getSource());
        newAward.setTransaction(getTransaction());
        newAward.hotel = hotel;
        newAward.transactionDate = transactionDate;
        newAward.costOfPoints = costOfPoints;

        return newAward;
    }
}
