package com.ihg.automation.selenium.hotel.pages.programs.br;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasText;
import static com.ihg.automation.selenium.matchers.component.HasDefaultText.hasDefault;

import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.label.Label;
import com.ihg.automation.selenium.gwt.components.utils.ByText;
import com.ihg.automation.selenium.hotel.pages.programs.ProgramSummary;

public class BusinessRewardsSummary extends ProgramSummary
{

    public BusinessRewardsSummary(Container container)
    {
        super(container);
    }

    @Override
    public Label getStatus()
    {
        return new Label(container, ByText.exactSelf("Status"), 1);
    }

    public Label getStatusBR()
    {
        return new Label(container, ByText.exactSelf("Status"), 2);
    }

    public Label getTermsAndConditions()
    {
        return container.getLabel("Terms and Conditions");
    }

    public Label getDateOfResponse()
    {
        return container.getLabel("Date of Response");
    }

    public Label getSource()
    {
        return container.getLabel("Source");
    }

    public void verifyProgramDetailsAfterEnroll(String status, Member member, String statusBR)
    {
        verifyThat(getStatus(), hasText(status));
        verifyThat(getMemberId(), hasText(member.getBRProgramId()));
        verifyThat(getStatusBR(), hasText(statusBR));
        verifyThat(getTermsAndConditions(), hasDefault());
        verifyThat(getDateOfResponse(), hasDefault());
        verifyThat(getSource(), hasDefault());
    }
}
