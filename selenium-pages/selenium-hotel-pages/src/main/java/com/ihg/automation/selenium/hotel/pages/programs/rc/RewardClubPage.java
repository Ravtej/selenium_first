package com.ihg.automation.selenium.hotel.pages.programs.rc;

import com.ihg.automation.common.DateUtils;
import com.ihg.automation.selenium.common.pages.program.EmployeeRateEligibility;
import com.ihg.automation.selenium.common.pages.program.RewardClubTierLevelActivityGrid;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.gwt.components.panel.Panel;
import com.ihg.automation.selenium.gwt.components.utils.ByText;
import com.ihg.automation.selenium.hotel.pages.programs.ProgramPageBase;

public class RewardClubPage extends ProgramPageBase
{
    public RewardClubPage()
    {
        super(Program.RC);
    }

    @Override
    public RewardClubSummary getSummary()
    {
        return new RewardClubSummary(container);
    }

    public Panel getAnnualActivitiesPanel(int year)
    {
        return new Panel(container, ByText.exact("Tier Level Annual Activity From January 1, " + year));
    }

    public RewardClubTierLevelActivityGrid getAnnualActivities(int year)
    {
        return new RewardClubTierLevelActivityGrid(getAnnualActivitiesPanel(year));
    }

    public RewardClubTierLevelActivityGrid getCurrentYearAnnualActivities()
    {
        return getAnnualActivities(DateUtils.currentYear());
    }

    public RewardClubTierLevelActivityGrid getPreviousYearAnnualActivities()
    {
        return getAnnualActivities(DateUtils.previousYear());
    }

    public EmployeeRateEligibility getEmployeeRateEligibility()
    {
        return new EmployeeRateEligibility(container);
    }

    public EarningPreference getEarningDetails()
    {
        return new EarningPreference(container);
    }

    public Panel getAlliancesPanel()
    {
        return container.getPanel("Alliance Partners");
    }

    public AllianceGrid getAlliances()
    {
        return new AllianceGrid(getAlliancesPanel());
    }
}
