package com.ihg.automation.selenium.hotel.pages.hoteloperations.awards.posted;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;

import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.CustomContainerBase;
import com.ihg.automation.selenium.gwt.components.label.Label;

public class PostedMemberDetails extends CustomContainerBase
{
    public PostedMemberDetails(Container container)
    {
        super(container);
    }

    public Label getMemberNumber()
    {
        return container.getLabel("Member Number");
    }

    public Label getMemberName()
    {
        return container.getLabel("Member Name");
    }

    public void verify(PostedPointAward pointAward)
    {
        verifyThat(getMemberNumber(), hasText(pointAward.getMemberNumber()));
        verifyThat(getMemberName(), hasText(pointAward.getMemberName()));
    }
}
