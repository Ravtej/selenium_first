package com.ihg.automation.selenium.hotel.pages.hoteloperations.awards.manage;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static com.ihg.automation.selenium.matchers.text.ContainsIgnoreCase.containsIgnoreCase;

import com.ihg.automation.selenium.gwt.components.grid.CellsName;
import com.ihg.automation.selenium.gwt.components.grid.GridRow;
import com.ihg.automation.selenium.gwt.components.grid.GridRowXpath;

public class ManagePointAwardsGridRow extends GridRow<ManagePointAwardsGridRow.ManagePointAwardsGridCell>
{
    public ManagePointAwardsGridRow(ManagePointAwardsGrid parent, GridRowXpath xpath)
    {
        super(parent, xpath);
    }

    public void verify(ManagePointAward pointAward)
    {
        verifyThat(getCell(ManagePointAwardsGridCell.REQUESTED_DATE), hasText(pointAward.getRequestedDate()));
        verifyThat(getCell(ManagePointAwardsGridCell.MEMBER_NAME),
                hasText(containsIgnoreCase(pointAward.getMemberName())));
        verifyThat(getCell(ManagePointAwardsGridCell.MBR_NUMBER), hasText(pointAward.getMemberNumber()));
        verifyThat(getCell(ManagePointAwardsGridCell.AWARD_TYPE), hasText(pointAward.getTransaction().getType()));
        verifyThat(getCell(ManagePointAwardsGridCell.CHECK_IN), hasText(pointAward.getCheckInDate()));
        verifyThat(getCell(ManagePointAwardsGridCell.CHECK_OUT), hasText(pointAward.getCheckOutDate()));
        verifyThat(getCell(ManagePointAwardsGridCell.RC_LEVEL), hasText(pointAward.getMemberLevel().getCode()));
        verifyThat(getCell(ManagePointAwardsGridCell.POINTS), hasText(pointAward.getPoints()));
        verifyThat(getCell(ManagePointAwardsGridCell.STATUS), hasText(pointAward.getStatus()));
        verifyThat(getCell(ManagePointAwardsGridCell.DECLINED_REASON), hasText(pointAward.getDeclinedReason()));
    }

    public enum ManagePointAwardsGridCell implements CellsName
    {
        MEMBER_NAME("guestName"), MBR_NUMBER("membershipId"), CHECK_IN("checkInDate"), CHECK_OUT("checkOutDate"), REQUESTED_DATE(
                "requestedDate"), RC_LEVEL("tierLevelCode"), POINTS("preferredUnitsAmount"), AWARD_TYPE(
                "transactionDetails.category.description"), STATUS("status"), DECLINED_REASON("reason"), ACTION(
                "action");

        private String name;

        private ManagePointAwardsGridCell(String name)
        {
            this.name = name;
        }

        @Override
        public String getName()
        {
            return name;
        }
    }
}
