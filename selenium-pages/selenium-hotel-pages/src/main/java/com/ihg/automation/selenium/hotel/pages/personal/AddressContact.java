package com.ihg.automation.selenium.hotel.pages.personal;

import com.ihg.automation.selenium.common.contact.Address;
import com.ihg.automation.selenium.common.member.GuestAddress;
import com.ihg.automation.selenium.common.personal.EditableContactBase;
import com.ihg.automation.selenium.gwt.components.checkbox.CheckBox;
import com.ihg.automation.selenium.gwt.components.panel.EditablePanel;

public class AddressContact extends EditableContactBase<GuestAddress, Address>
{
    public AddressContact(EditablePanel parent)
    {
        super(parent, new Address(parent));
    }

    public CheckBox getSaveAsEntered()
    {
        return container.getCheckBox("Save as entered");
    }
}
