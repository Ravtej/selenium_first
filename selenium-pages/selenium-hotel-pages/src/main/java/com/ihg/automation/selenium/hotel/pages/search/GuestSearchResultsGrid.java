package com.ihg.automation.selenium.hotel.pages.search;

import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.gwt.components.grid.Grid;
import com.ihg.automation.selenium.gwt.components.grid.GridRowXpath;

public class GuestSearchResultsGrid extends Grid<GuestSearchResultsGridRow, GuestSearchResultsGridRow.GuestSearchCell>
{
    public GuestSearchResultsGrid()
    {
        super("Guest Search Results Grid");
    }

    public GuestSearchResultsGridRow getRow(Member member)
    {
        return getRow(new GridRowXpath.GridRowXpathBuilder()
                .cell(GuestSearchResultsGridRow.GuestSearchCell.MBR_NUMBER, member.getRCProgramId()).build());
    }
}
