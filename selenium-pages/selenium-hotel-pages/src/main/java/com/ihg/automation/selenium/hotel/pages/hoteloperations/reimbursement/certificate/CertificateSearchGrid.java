package com.ihg.automation.selenium.hotel.pages.hoteloperations.reimbursement.certificate;

import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.grid.Grid;

public class CertificateSearchGrid
        extends Grid<CertificateSearchGridRow, CertificateSearchGridRow.CertificateSearchGridCell>
{
    public CertificateSearchGrid(Container parent)
    {
        super(parent);
    }
}
