package com.ihg.automation.selenium.hotel.pages.hoteloperations.br.posted;

import com.ihg.automation.selenium.common.event.SearchAware;
import com.ihg.automation.selenium.common.event.SearchButtonBar;
import com.ihg.automation.selenium.common.pages.TabCustomContainerBase;
import com.ihg.automation.selenium.hotel.pages.Tabs;

public class PostedEventsPage extends TabCustomContainerBase implements SearchAware
{
    public PostedEventsPage()
    {
        super(new Tabs().getIHGBusinessRewards().getPostedEvents());
    }

    @Override
    public SearchButtonBar getButtonBar()
    {
        return new SearchButtonBar(container);
    }

    @Override
    public PostedEventsSearch getSearchFields()
    {
        return new PostedEventsSearch(container);
    }

    public PostedEventsGrid getManageEventsGrid()
    {
        return new PostedEventsGrid(container);
    }

    public ResultsFilter getResultsFilter()
    {
        return new ResultsFilter(container);
    }
}
