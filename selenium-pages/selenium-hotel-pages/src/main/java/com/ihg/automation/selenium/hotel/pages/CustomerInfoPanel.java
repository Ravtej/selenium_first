package com.ihg.automation.selenium.hotel.pages;

import static com.ihg.automation.selenium.gwt.components.Axis.CHILD;
import static com.ihg.automation.selenium.gwt.components.ComponentColumnPosition.SINGLE_TD_INNER_DIV;
import static com.ihg.automation.selenium.gwt.components.utils.FindStepUtils.byXpathWithWait;

import com.ihg.automation.selenium.gwt.components.Component;

public class CustomerInfoPanel extends com.ihg.automation.selenium.common.CustomerInfoPanel
{
    public CustomerInfoPanel()
    {
        super();
    }

    @Override
    public Component getPhone()
    {
        return new Component(container, "Label", "Phone", byXpathWithWait(CHILD.add(SINGLE_TD_INNER_DIV.getXpath(2))));
    }

    @Override
    public Component getEmail()
    {
        return new Component(container, "Label", "Email", byXpathWithWait(CHILD.add(SINGLE_TD_INNER_DIV.getXpath(4))));
    }

    @Override
    public Component getFullName()
    {
        return new Component(container, "Label", "Full Name",
                byXpathWithWait(CHILD.add(SINGLE_TD_INNER_DIV.getXpath(1))));
    }
}
