package com.ihg.automation.selenium.hotel.pages.programs.rc;

import com.ihg.automation.selenium.common.types.Alliance;
import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.grid.Grid;
import com.ihg.automation.selenium.gwt.components.grid.GridRowXpath;
import com.ihg.automation.selenium.gwt.components.utils.ByText;

public class AllianceGrid extends Grid<AllianceGridRow, AllianceGridRow.AllianceCell>
{
    public AllianceGrid(Container container)
    {
        super(container);
    }

    public AllianceGridRow getRow(Alliance alliance)
    {
        return getRow(new GridRowXpath.GridRowXpathBuilder()
                .cell(AllianceGridRow.AllianceCell.ALLIANCE, ByText.startsWithSelf(alliance.getCode())).build());
    }
}
