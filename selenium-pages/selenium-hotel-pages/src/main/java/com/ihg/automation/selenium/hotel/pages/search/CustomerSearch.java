package com.ihg.automation.selenium.hotel.pages.search;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.components.Country.US;
import static com.ihg.automation.selenium.common.types.program.Program.RC;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasText;
import static com.ihg.automation.selenium.matchers.text.EntityMatcherFactory.isValue;

import com.ihg.automation.selenium.common.components.CountrySelect;
import com.ihg.automation.selenium.gwt.components.CustomContainerBase;
import com.ihg.automation.selenium.gwt.components.input.Input;
import com.ihg.automation.selenium.gwt.components.input.InputXpath;
import com.ihg.automation.selenium.gwt.components.input.Select;
import com.ihg.automation.selenium.gwt.components.panel.Panel;

public class CustomerSearch extends CustomContainerBase
{

    private static final String MEMBER_NUMBER = "Member Number";
    private static final String NUMBER = "Number";
    private static final String LAST_NAME = "Last Name";
    private static final String FIRST_NAME = "First Name";
    private static final String STREET_ADDRESS = "Street Address";
    private static final String CITY_STATE_OR_PROVINCE = "City, State or Province";
    private static final String ZIP_OR_POSTAL_CODE = "ZIP or Postal Code";
    private static final String PHONE = "Phone";
    private static final String EMAIL = "Email";

    public CustomerSearch(GuestSearch guestSearch)
    {
        super(((Panel) guestSearch.container).getBody());
    }

    public Input getMemberNumber()
    {
        return container.getInput(MEMBER_NUMBER, 2);
    }

    public Input getNumber()
    {
        return container.getInput(NUMBER, 3);
    }

    public Input getLastName()
    {
        return container.getInput(LAST_NAME, 4);
    }

    public Input getFirstName()
    {
        return container.getInput(FIRST_NAME, 5);
    }

    public Input getStreetAddress()
    {
        return container.getInput(STREET_ADDRESS, 6);
    }

    public Input getCity()
    {
        return container.getInput(CITY_STATE_OR_PROVINCE, 7);
    }

    public Input getZip()
    {
        return container.getInput(ZIP_OR_POSTAL_CODE, 8);
    }

    public CountrySelect getCountry()
    {
        return new CountrySelect(container, new InputXpath.InputXpathBuilder().row(9).build(), "Country");
    }

    public Input getPhone()
    {
        Input phoneInput = container.getInput(PHONE, 10);
        phoneInput.setDefaultValue(PHONE);
        return phoneInput;
    }

    public Select getEmail()
    {
        return container.getSelect(EMAIL, 11);
    }

    public Select getProgram()
    {
        return container.getSelect("Program", 12);
    }

    public void verifyDefault()
    {
        verifyThat(getMemberNumber(), hasText(MEMBER_NUMBER));
        verifyThat(getNumber(), hasText(NUMBER));
        verifyThat(getLastName(), hasText(LAST_NAME));
        verifyThat(getFirstName(), hasText(FIRST_NAME));
        verifyThat(getStreetAddress(), hasText(STREET_ADDRESS));
        verifyThat(getCity(), hasText(CITY_STATE_OR_PROVINCE));
        verifyThat(getZip(), hasText(ZIP_OR_POSTAL_CODE));
        verifyThat(getCountry(), hasText(isValue(US)));
        verifyThat(getPhone(), hasText(PHONE));
        verifyThat(getEmail(), hasText(EMAIL));
        verifyThat(getProgram(), hasText(RC.getCode()));
    }
}
