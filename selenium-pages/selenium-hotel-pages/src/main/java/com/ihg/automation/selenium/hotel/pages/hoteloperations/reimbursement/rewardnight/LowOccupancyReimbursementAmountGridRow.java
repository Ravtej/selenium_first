package com.ihg.automation.selenium.hotel.pages.hoteloperations.reimbursement.rewardnight;

import com.ihg.automation.selenium.gwt.components.grid.CellsName;
import com.ihg.automation.selenium.gwt.components.grid.GridRow;
import com.ihg.automation.selenium.gwt.components.grid.GridRowXpath;

public class LowOccupancyReimbursementAmountGridRow
        extends GridRow<LowOccupancyReimbursementAmountGridRow.LowOccupancyReimbursementAmountGridCell>
{
    public LowOccupancyReimbursementAmountGridRow(LowOccupancyReimbursementAmountGrid parent, GridRowXpath xpath)
    {
        super(parent, xpath);
    }

    public enum LowOccupancyReimbursementAmountGridCell implements CellsName
    {
        CHECK_OUT_BEGINNING_DATE("stayStartDate"), LOW_OCC_REIMBURSEMENT_AMOUNT("amount"), LOVIR_ADDITIONAL_REIMBURSEMENT_AMOUNT(
                "bountyAmount"), CURRENCY("currency");

        private String name;

        private LowOccupancyReimbursementAmountGridCell(String name)
        {
            this.name = name;
        }

        @Override
        public String getName()
        {
            return name;
        }
    }
}
