package com.ihg.automation.selenium.hotel.pages.hoteloperations.br.manage;

import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyNoError;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.assertThat;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;

import com.ihg.automation.selenium.common.business.BusinessEvent;
import com.ihg.automation.selenium.common.business.BusinessRewardsEventDetailsPopUp;
import com.ihg.automation.selenium.common.event.SearchAware;
import com.ihg.automation.selenium.common.event.SearchButtonBar;
import com.ihg.automation.selenium.common.pages.TabCustomContainerBase;
import com.ihg.automation.selenium.gwt.components.Button;
import com.ihg.automation.selenium.hotel.pages.LeftPanel;
import com.ihg.automation.selenium.hotel.pages.Tabs;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.HotelOperationsPage;

public class ManageEventsPage extends TabCustomContainerBase implements SearchAware
{
    public ManageEventsPage()
    {
        super(new Tabs().getIHGBusinessRewards().getManageEvents());
    }

    public void goToByOperation()
    {
        if (!this.isDisplayed())
        {
            new LeftPanel().getHotelOperations().clickAndWait();
            new HotelOperationsPage().getIHGBusinessRewardsEventsCreateReviewAndApprove().clickAndWait();
            assertThat(this, displayed(true));
        }
    }

    @Override
    public SearchButtonBar getButtonBar()
    {
        return new SearchButtonBar(container);
    }

    @Override
    public ManageEventsSearch getSearchFields()
    {
        return new ManageEventsSearch(container);
    }

    public ResultsFilter getResultsFilter()
    {
        return new ResultsFilter(container);
    }

    public ManageEventsGrid getManageEventsGrid()
    {
        return new ManageEventsGrid(container);
    }

    public Button getCreateEvent()
    {
        return container.getButton("Create Event");
    }

    public BusinessRewardsEventDetailsPopUp openDetailsPopUp()
    {
        getCreateEvent().clickAndWait(verifyNoError());
        return new BusinessRewardsEventDetailsPopUp();
    }

    public Button getApproveSelected()
    {
        return container.getButton("Approve Selected");
    }

    public void searchByEventNameAndMemberId(BusinessEvent businessEvent)
    {
        ResultsFilter resultsFilter = getResultsFilter();
        resultsFilter.expand();
        resultsFilter.getEventName().typeAndWait(businessEvent.getEventName());
        resultsFilter.getMemberNumber().typeAndWait(businessEvent.getMember().getBRProgramId());
        getButtonBar().getSearch().clickAndWait();
    }
}
