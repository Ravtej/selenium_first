package com.ihg.automation.selenium.hotel.pages.search;

import static com.ihg.automation.selenium.gwt.components.utils.FindStepUtils.byXpathWithWait;
import static java.text.MessageFormat.format;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ihg.automation.selenium.common.pages.SearchBase;
import com.ihg.automation.selenium.gwt.components.Link;
import com.ihg.automation.selenium.gwt.components.input.Input;
import com.ihg.automation.selenium.gwt.components.label.TopLabel;
import com.ihg.automation.selenium.gwt.components.utils.ByText;
import com.ihg.automation.selenium.listener.Verifier;

public class GuestSearchByNumber extends SearchBase
{
    private static final Logger logger = LoggerFactory.getLogger(GuestSearchByNumber.class);

    public GuestSearchByNumber()
    {
        super("Search Panel", "Guest Search by Number",
                byXpathWithWait(format(SUB_PANEL_HEADER_TEXT_PATTERN, ByText.exact("Guest Search by Number"))));
    }

    public Input getMemberNumber()
    {
        return new TopLabel(container, ByText.exact("Member Number"), TopLabel.GO_TO_LABEL).getInput();
    }

    public Link getAdvancedSearch()
    {
        return new Link(container, "Advanced Search", Link.LINK_PATTERN_WITH_TEXT_NO_HYPER);
    }

    public void byMemberNumber(String number, Verifier... verifiers)
    {
        logger.info("Enter number and search");
        getMemberNumber().type(number);
        clickSearch(verifiers);
    }
}
