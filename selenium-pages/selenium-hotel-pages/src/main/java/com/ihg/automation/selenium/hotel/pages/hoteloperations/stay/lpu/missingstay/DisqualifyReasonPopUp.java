package com.ihg.automation.selenium.hotel.pages.hoteloperations.stay.lpu.missingstay;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasText;

import com.ihg.automation.selenium.common.pages.ClosablePopUpBase;

public class DisqualifyReasonPopUp extends ClosablePopUpBase
{
    public DisqualifyReasonPopUp()
    {
        super("Disqualification Reason");
    }

    public void verifyReasonAndClose(String reason)
    {
        verifyThat(this, hasText(reason));
        clickClose();
    }
}
