package com.ihg.automation.selenium.hotel.pages.programs.rc;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasText;
import static com.ihg.automation.selenium.matchers.text.EntityMatcherFactory.isValue;

import com.ihg.automation.selenium.common.member.MemberAlliance;
import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.CustomContainerBase;
import com.ihg.automation.selenium.gwt.components.label.Label;
import com.ihg.automation.selenium.gwt.components.panel.Panel;

public class EarningPreference extends CustomContainerBase
{
    public EarningPreference(Container container)
    {
        super(container.getSeparator("Earning Details"));
    }

    public Label getEarningPreference()
    {
        return container.getLabel("Earning Preference");
    }

    public Panel getAlliancesPanel()
    {
        return container.getPanel("Alliance Partners");
    }

    public AllianceGrid getAlliances()
    {
        return new AllianceGrid(getAlliancesPanel());
    }

    public void verify(MemberAlliance memberAlliance)
    {
        verifyThat(getEarningPreference(), hasText(isValue(memberAlliance.getAlliance())));
        getAlliances().getRow(1).verify(memberAlliance);
    }
}
