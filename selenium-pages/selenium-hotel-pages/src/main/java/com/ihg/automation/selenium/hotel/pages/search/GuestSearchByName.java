package com.ihg.automation.selenium.hotel.pages.search;

import static com.ihg.automation.selenium.gwt.components.utils.FindStepUtils.byXpathWithWait;
import static java.text.MessageFormat.format;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ihg.automation.selenium.common.pages.SearchBase;
import com.ihg.automation.selenium.gwt.components.input.Input;
import com.ihg.automation.selenium.gwt.components.input.Select;
import com.ihg.automation.selenium.gwt.components.label.TopLabel;
import com.ihg.automation.selenium.gwt.components.utils.ByText;
import com.ihg.automation.selenium.listener.Verifier;

public class GuestSearchByName extends SearchBase
{
    private static final Logger logger = LoggerFactory.getLogger(GuestSearchByName.class);

    public GuestSearchByName()
    {
        super("Search Panel", "Guest Search by Name",
                byXpathWithWait(format(SUB_PANEL_HEADER_TEXT_PATTERN, ByText.exact("Guest Search by Name"))));
    }

    public Input getLastName()
    {
        return new TopLabel(container, ByText.exact("Last Name"), TopLabel.GO_TO_LABEL).getInput();
    }

    public Input getFirstName()
    {
        return new TopLabel(container, ByText.exact("First Name"), TopLabel.GO_TO_LABEL).getInput();
    }

    public Input getPhone()
    {
        return new TopLabel(container, ByText.exact("Phone Number"), TopLabel.GO_TO_LABEL).getInput();
    }

    public Input getCity()
    {
        return new TopLabel(container, ByText.exact("City, State or Province"), TopLabel.GO_TO_LABEL).getInput();
    }

    public Input getZip()
    {
        return new TopLabel(container, ByText.exact("ZIP or Postal Code"), TopLabel.GO_TO_LABEL).getInput();
    }

    public Select getCountry()
    {
        return new TopLabel(container, ByText.exact("Country"), TopLabel.GO_TO_LABEL).getSelect();
    }

    public void byName(String lastName, String firstName, Verifier... verifiers)
    {
        logger.info("Enter names and search");
        getClear().clickAndWait();
        getLastName().type(lastName);
        getFirstName().type(firstName);
        clickSearch(verifiers);
    }
}
