package com.ihg.automation.selenium.hotel.pages.hoteloperations.stay.stays;

import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.grid.Grid;
import com.ihg.automation.selenium.gwt.components.grid.GridRowXpath;

public class StaysGrid extends Grid<StaysGridRow, StaysGridRow.StaysGridCell>
{
    public StaysGrid(Container parent)
    {
        super(parent);
    }

    public StaysGridRow getRow(String mbrNumber)
    {
        return getRow(
                new GridRowXpath.GridRowXpathBuilder().cell(StaysGridRow.StaysGridCell.MBR_NUMBER, mbrNumber).build());
    }
}
