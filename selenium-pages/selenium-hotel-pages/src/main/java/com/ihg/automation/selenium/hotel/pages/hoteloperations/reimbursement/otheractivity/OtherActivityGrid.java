package com.ihg.automation.selenium.hotel.pages.hoteloperations.reimbursement.otheractivity;

import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.grid.Grid;

public class OtherActivityGrid extends Grid<OtherActivityGridRow, OtherActivityGridRow.OtherActivityGridCell>
{
    public OtherActivityGrid(Container parent)
    {
        super(parent);
    }
}
