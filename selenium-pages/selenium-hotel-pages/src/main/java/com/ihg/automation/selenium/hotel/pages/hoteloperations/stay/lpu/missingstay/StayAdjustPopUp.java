package com.ihg.automation.selenium.hotel.pages.hoteloperations.stay.lpu.missingstay;

import com.ihg.automation.selenium.common.history.EventHistoryTab;
import com.ihg.automation.selenium.common.pages.SavePopUpBase;

public class StayAdjustPopUp extends SavePopUpBase
{
    public StayAdjustPopUp()
    {
        super();
    }

    public AdjustRevenueDetailsTab getAdjustRevenueDetailsTab()
    {
        return new AdjustRevenueDetailsTab(this);
    }

    public EventHistoryTab getHistoryTab()
    {
        return new EventHistoryTab(this, "Stay History");
    }
}
