package com.ihg.automation.selenium.hotel.pages.order;

import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.grid.Grid;
import com.ihg.automation.selenium.gwt.components.grid.GridRowXpath;

public class VouchersGrid extends Grid<VouchersGridRow, VouchersGridRow.VouchersGridCell>
{
    public VouchersGrid(Container parent)
    {
        super(parent, "Vouchers");
    }

    public VouchersGridRow getRow(String voucherId)
    {
        return getRow(new GridRowXpath.GridRowXpathBuilder()
                .cell(VouchersGridRow.VouchersGridCell.VOUCHER_ID, voucherId).build());
    }
}
