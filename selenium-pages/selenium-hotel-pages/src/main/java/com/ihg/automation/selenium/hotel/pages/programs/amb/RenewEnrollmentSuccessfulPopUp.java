package com.ihg.automation.selenium.hotel.pages.programs.amb;

import com.ihg.automation.selenium.common.pages.ClosablePopUpBase;
import com.ihg.automation.selenium.gwt.components.Button;
import com.ihg.automation.selenium.listener.Verifier;

public class RenewEnrollmentSuccessfulPopUp extends ClosablePopUpBase
{
    public RenewEnrollmentSuccessfulPopUp()
    {
        super("Renew Enrollment Successful");
    }

    public Button getOk()
    {
        return container.getButton("OK");
    }

    public void clickOk(Verifier... verifiers)
    {
        getOk().clickAndWait(verifiers);
    }
}
