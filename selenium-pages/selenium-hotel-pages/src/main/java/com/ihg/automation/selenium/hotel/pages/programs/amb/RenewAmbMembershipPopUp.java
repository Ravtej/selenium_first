package com.ihg.automation.selenium.hotel.pages.programs.amb;

import com.ihg.automation.selenium.common.pages.SubmitPopUpBase;
import com.ihg.automation.selenium.gwt.components.Component;
import com.ihg.automation.selenium.gwt.components.ComponentColumnPosition;
import com.ihg.automation.selenium.gwt.components.input.Input;

public class RenewAmbMembershipPopUp extends SubmitPopUpBase
{
    private static final ComponentColumnPosition POSITION = ComponentColumnPosition.FOLLOWING_TD;

    public RenewAmbMembershipPopUp()
    {
        super("Renew Membership");
    }

    public Input getEmployeeId()
    {
        return container.getInputByLabel("EMP Member ID");
    }

    public Component getEmployeeName()
    {
        return container.getLabel("EMP Member ID").getComponent("Employee Name", 1, POSITION);
    }
}
