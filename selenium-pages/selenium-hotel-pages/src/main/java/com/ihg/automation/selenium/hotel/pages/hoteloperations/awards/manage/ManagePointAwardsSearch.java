package com.ihg.automation.selenium.hotel.pages.hoteloperations.awards.manage;

import java.util.ArrayList;
import java.util.Arrays;

import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.checkbox.CheckBox;
import com.ihg.automation.selenium.gwt.components.input.Select;
import com.ihg.automation.selenium.gwt.components.utils.ByText;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.awards.AwardSearch;

public class ManagePointAwardsSearch extends AwardSearch
{
    public static ArrayList<String> STATUS_LIST = new ArrayList<>(
            Arrays.asList("Declined", "Disqualified", "Initiated", "Pending", "Processing", "Rejected"));

    public static ArrayList<String> TYPE_LIST = new ArrayList<>(
            Arrays.asList("Welcome Amenity", "Service Recovery", "Hotel Promotion", "A Greener Stay"));

    public static final int DEFAULT_FROM_DATE_SHIFT = -5;

    public ManagePointAwardsSearch(Container container)
    {
        super(container);
    }

    public Select getStatus()
    {
        return container.getSelectByLabel("Status");
    }

    public CheckBox getCheckIn()
    {
        return new CheckBox(container, ByText.exactSelf("Check In"), CheckBox.CHECK_BOX_PATTERN_LC);
    }

    public CheckBox getRequested()
    {
        return new CheckBox(container, ByText.exactSelf("Requested"), CheckBox.CHECK_BOX_PATTERN_LC);
    }
}
