package com.ihg.automation.selenium.hotel.pages;

import com.ihg.automation.selenium.common.SiteHelperBase;
import com.ihg.automation.selenium.common.UI;
import com.ihg.automation.selenium.common.User;
import com.ihg.automation.selenium.common.pages.login.HotelRolePopUp;

public class HotelHelper extends SiteHelperBase
{
    public HotelHelper(String url)
    {
        super(url, UI.LC);
    }

    @Override
    protected void selectRole(User user, String role, boolean isRoleMandatory)
    {
        HotelRolePopUp rolePopUp = new HotelRolePopUp();
        if (isRoleMandatory || rolePopUp.isDisplayed())
        {
            rolePopUp.selectAndSubmit(role, user.getLocation(), user.isHelpDeskRole());
        }
    }
}
