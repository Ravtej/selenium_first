package com.ihg.automation.selenium.hotel.pages.hoteloperations.awards.manage;

import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.label.Label;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.awards.posted.PostedMemberDetails;

public class MemberDetails extends PostedMemberDetails
{
    public MemberDetails(Container container)
    {
        super(container.getSeparator("Member Details"));
    }

    public Label getMemberLevel()
    {
        return container.getLabel("Member Level");
    }
}
