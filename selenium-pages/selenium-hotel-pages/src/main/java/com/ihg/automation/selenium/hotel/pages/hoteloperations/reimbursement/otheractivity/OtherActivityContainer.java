package com.ihg.automation.selenium.hotel.pages.hoteloperations.reimbursement.otheractivity;

import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.CustomContainerBase;
import com.ihg.automation.selenium.gwt.components.label.Label;

public class OtherActivityContainer extends CustomContainerBase
{
    public OtherActivityContainer(Container container)
    {
        super(container);
    }

    public Label getConfirmationNumber()
    {
        return container.getLabel("Confirmation Number");
    }

    public Label getItemID()
    {
        return container.getLabel("Item Id");
    }

    public Label getBillingEntity()
    {
        return container.getLabel("Billing Entity");
    }

    public Label getDescriptionGuestName()
    {
        return container.getLabel("Description/Guest Name");
    }

    public Label getCertificateNumber()
    {
        return container.getLabel("Certificate Number");
    }

    public Label getCheckInDate ()
    {
        return container.getLabel("Check In Date");
    }

    public Label getCheckOutDate ()
    {
        return container.getLabel("Check Out Date");
    }

    public Label getStatus()
    {
        return container.getLabel("Status");
    }

    public Label getFolioNumber()
    {
        return container.getLabel("Folio Number");
    }

    public Label getReimbursementAmount()
    {
        return container.getLabel("Reimbursement Amount");
    }

    public Label getReimbursementMethod()
    {
        return container.getLabel("Reimbursement Method");
    }

    public Label getReimbursementDate()
    {
        return container.getLabel("Reimbursement Date");
    }

    public Label getBundleNumber()
    {
        return container.getLabel("Bundle Number");
    }

}

