package com.ihg.automation.selenium.hotel.pages.order;

import static com.ihg.automation.common.CustomStringUtils.concat;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasText;

import com.ihg.automation.selenium.common.order.VoucherRule;
import com.ihg.automation.selenium.common.types.Currency;
import com.ihg.automation.selenium.gwt.components.grid.CellsName;
import com.ihg.automation.selenium.gwt.components.grid.GridRow;
import com.ihg.automation.selenium.gwt.components.grid.GridRowXpath;

public class VouchersGridRow extends GridRow<VouchersGridRow.VouchersGridCell>
{
    public VouchersGridRow(VouchersGrid parent, GridRowXpath xpath)
    {
        super(parent, xpath);
    }

    public enum VouchersGridCell implements CellsName
    {
        VOUCHER_ID("voucherId"), VOUCHER_NAME("orderDescription"), POINT_VOUCHER_DENOMINATION(
                "amountOfUnits"), COST_PER_VOUCHER("costPerVoucher");

        private String name;

        private VouchersGridCell(String name)
        {
            this.name = name;
        }

        @Override
        public String getName()
        {
            return name;
        }
    }

    public void clickById()
    {
        getCell(VouchersGridCell.VOUCHER_ID).asLink().clickAndWait();
    }

    public void verify(VoucherRule voucher)
    {
        verifyThat(getCell(VouchersGridCell.VOUCHER_ID), hasText(voucher.getPromotionId()));
        verifyThat(getCell(VouchersGridCell.VOUCHER_NAME), hasText(voucher.getVoucherName()));
        verifyThat(getCell(VouchersGridCell.POINT_VOUCHER_DENOMINATION), hasText(voucher.getPointVoucherDenomination()));
        verifyThat(getCell(VouchersGridCell.COST_PER_VOUCHER),
                hasText(concat(" ", voucher.getCostAmount(), Currency.USD.getCode())));
    }
}
