package com.ihg.automation.selenium.hotel.pages.hoteloperations.awards.posted;

import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.input.Select;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.awards.AwardSearch;

public class PostedPointAwardsSearch extends AwardSearch
{
    public PostedPointAwardsSearch(Container container)
    {
        super(container);
    }

    public Select getAmount()
    {
        return container.getSelectByLabel("Amount");
    }
}
