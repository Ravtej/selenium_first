package com.ihg.automation.selenium.hotel.pages.programs;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;

import com.ihg.automation.common.DateUtils;
import com.ihg.automation.selenium.common.EventSource;
import com.ihg.automation.selenium.common.EventSourceAware;
import com.ihg.automation.selenium.common.SiteHelperBase;
import com.ihg.automation.selenium.common.event.Source;
import com.ihg.automation.selenium.common.pages.MultiModeBase;
import com.ihg.automation.selenium.common.pages.TabCustomContainerBase;
import com.ihg.automation.selenium.common.payment.PaymentDetails;
import com.ihg.automation.selenium.common.payment.PaymentDetailsAware;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.input.DateInput;
import com.ihg.automation.selenium.gwt.components.label.Label;
import com.ihg.automation.selenium.gwt.components.panel.FieldSet;
import com.ihg.automation.selenium.hotel.pages.Tabs;

public abstract class ProgramPageBase extends TabCustomContainerBase
{
    private Program program;

    public ProgramPageBase(Program program)
    {
        super(new Tabs().getProgramInfo().getProgram(program));
        this.program = program;
    }

    abstract public ProgramSummary getSummary();

    public FieldSet getProgramOverview()
    {
        return container.getFieldSet(program.getValue() + " Overview");
    }

    public EnrollmentDetails getEnrollmentDetails()
    {
        return new EnrollmentDetails(container);
    }

    public class EnrollmentDetails extends MultiModeBase implements EventSourceAware, PaymentDetailsAware
    {

        public static final String OFFER_CODE = "HOTEL";

        EnrollmentDetails(Container container)
        {
            super(container.getSeparator("Enrollment Details"));
        }

        @Override
        public EventSource getSource()
        {
            return new EventSource(container);
        }

        @Override
        public PaymentDetails getPaymentDetails()
        {
            return new PaymentDetails(container);
        }

        public Label getPurchaseAmount()
        {
            return container.getLabel("Purchase Amount");
        }

        public Label getPaymentType()
        {
            return container.getLabel("Payment Type");
        }

        public DateInput getMemberSince()
        {
            DateInput birthDate = new DateInput(container.getLabel("Member Since"));
            return birthDate;
        }

        public Label getEnrollDate()
        {
            return container.getLabel("Enrollment Date");
        }

        public Label getRenewalDate()
        {
            return container.getLabel("Renewal Date");
        }

        public Label getOfferCode()
        {
            return container.getLabel("Offer Code");
        }

        public Label getReferringMember()
        {
            return container.getLabel("Referring Member");
        }

        public void verifyDetails(Source source)
        {
            verifyDetails(source, OFFER_CODE);
        }

        public void verifyDetails(Source source, String offerCode)
        {
            verifyThat(getEnrollDate(), hasText(DateUtils.getFormattedDate()));
            verifyThat(getOfferCode(), hasText(offerCode));
            getSource().verifyNoType(source);
        }

        public void verifyDetails(SiteHelperBase siteHelper)
        {
            verifyDetails(siteHelper.getSourceFactory().getSource());
        }

        public void verifyEmployeeDetails(SiteHelperBase siteHelper)
        {
            verifyDetails(siteHelper.getSourceFactory().getNoEmployeeIdSource());
        }
    }
}
