package com.ihg.automation.selenium.hotel.pages.hoteloperations.reimbursement.occupancy;

import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.CustomContainerBase;
import com.ihg.automation.selenium.gwt.components.input.DateInput;
import com.ihg.automation.selenium.gwt.components.input.Select;

public class OccupancyAndADREntrySearch extends CustomContainerBase
{
    public OccupancyAndADREntrySearch(Container container)
    {
        super(container);
    }

    public Select getHotelCode()
    {
        return container.getSelectByLabel("Hotel Code");
    }

    public Select getStatus()
    {
        return container.getSelectByLabel("Status");
    }

    public Select getType()
    {
        return container.getSelectByLabel("Type");
    }

    public DateInput getFromDate()
    {
        return new DateInput(container.getLabel("From"));
    }

    public DateInput getToDate()
    {
        return new DateInput(container.getLabel("To"));
    }
}
