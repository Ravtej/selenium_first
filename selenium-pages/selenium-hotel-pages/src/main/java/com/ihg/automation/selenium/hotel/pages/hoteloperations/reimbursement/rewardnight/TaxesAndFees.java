package com.ihg.automation.selenium.hotel.pages.hoteloperations.reimbursement.rewardnight;

import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.CustomContainerBase;

public class TaxesAndFees extends CustomContainerBase
{
    public TaxesAndFees(Container container)
    {
        super(container.getFieldSet("Taxes and Fees"));
    }

    public TaxesAndFeesGrid getTaxesAndFeesGrid()
    {
        return new TaxesAndFeesGrid(container);
    }
}
