package com.ihg.automation.selenium.hotel.pages.hoteloperations.reimbursement.rewardnight;

import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.grid.Grid;

public class TaxesAndFeesGrid extends Grid<TaxesAndFeesGridRow, TaxesAndFeesGridRow.TaxesAndFeesGridCell>
{
    public TaxesAndFeesGrid(Container parent)
    {
        super(parent);
    }
}
