package com.ihg.automation.selenium.hotel.pages.hoteloperations.awards.posted;

import com.ihg.automation.selenium.common.EventSource;
import com.ihg.automation.selenium.common.EventSourceAware;
import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.EventGridRowContainerWithDetails;

public class PostedPointAwardContainer extends EventGridRowContainerWithDetails implements EventSourceAware
{
    public PostedPointAwardContainer(Container parent)
    {
        super(parent);
    }

    public PostedPointAwardDetails getPointAwardDetails()
    {
        return new PostedPointAwardDetails(container);
    }

    @Override
    public EventSource getSource()
    {
        return new EventSource(container);
    }

    public void verify(PostedPointAward pointAward)
    {
        getPointAwardDetails().verify(pointAward);
        getSource().verify(pointAward.getSource());
    }
}
