package com.ihg.automation.selenium.hotel.pages.hoteloperations.br.manage;

import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.checkbox.CheckBox;
import com.ihg.automation.selenium.gwt.components.input.Select;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.br.posted.PostedEventsSearch;

public class ManageEventsSearch extends PostedEventsSearch
{
    public ManageEventsSearch(Container container)
    {
        super(container);
    }

    public Select getEventStatus()
    {
        return container.getSelectByLabel("Event Status");
    }

    public CheckBox getEventsWithLimitDays()
    {
        return container.getCheckBox("Events with less than 5 days to submit");
    }
}
