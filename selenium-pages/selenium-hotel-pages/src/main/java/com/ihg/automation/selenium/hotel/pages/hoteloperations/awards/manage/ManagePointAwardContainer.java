package com.ihg.automation.selenium.hotel.pages.hoteloperations.awards.manage;

import com.ihg.automation.selenium.common.EventSource;
import com.ihg.automation.selenium.common.EventSourceAware;
import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.EventGridRowContainerWithDetails;

public class ManagePointAwardContainer extends EventGridRowContainerWithDetails implements EventSourceAware
{
    private static final String DETAILS_BUTTON = "Event Details";

    public ManagePointAwardContainer(Container parent)
    {
        super(parent);
    }

    public ManagePointAwardDetails getManagePointAwardDetails()
    {
        return new ManagePointAwardDetails(container);
    }

    @Override
    public EventSource getSource()
    {
        return new EventSource(container);
    }
}
