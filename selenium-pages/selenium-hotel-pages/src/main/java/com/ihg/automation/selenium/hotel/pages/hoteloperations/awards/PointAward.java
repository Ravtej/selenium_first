package com.ihg.automation.selenium.hotel.pages.hoteloperations.awards;

public enum PointAward
{
    HOTEL_PROMOTION("HPR250", "Hotel Promotion", "Hotel Promotion Bonus Points"), //
    SERVICE_RECOVERY("SVC250", "Service Recovery", "Hotel Awarded Points");

    private String id;
    private String type;
    private String name;

    private PointAward(String id, String type, String name)
    {
        this.id = id;
        this.type = type;
        this.name = name;
    }

    public String getId()
    {
        return id;
    }

    public String getType()
    {
        return type;
    }

    public String getName()
    {
        return name;
    }

}
