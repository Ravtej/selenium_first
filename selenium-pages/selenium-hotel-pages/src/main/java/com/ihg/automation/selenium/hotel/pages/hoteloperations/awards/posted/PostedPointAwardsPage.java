package com.ihg.automation.selenium.hotel.pages.hoteloperations.awards.posted;

import com.ihg.automation.selenium.common.event.SearchAware;
import com.ihg.automation.selenium.common.event.SearchButtonBar;
import com.ihg.automation.selenium.common.pages.TabCustomContainerBase;
import com.ihg.automation.selenium.gwt.components.Button;
import com.ihg.automation.selenium.hotel.pages.Tabs;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.awards.CreatePointAwardPopUp;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.awards.PointAwardBase;
import com.ihg.automation.selenium.listener.Verifier;

public class PostedPointAwardsPage extends TabCustomContainerBase implements SearchAware
{
    public static final String SUCCESS_MESSAGE = "New Point Award was created for member ";

    public PostedPointAwardsPage()
    {
        super(new Tabs().getAwardsTab().getPostedPointAwards());
    }

    @Override
    public SearchButtonBar getButtonBar()
    {
        return new SearchButtonBar(container);
    }

    @Override
    public PostedPointAwardsSearch getSearchFields()
    {
        return new PostedPointAwardsSearch(container);
    }

    public PostedPointAwardsGrid getPostedPointAwardsGrid()
    {
        return new PostedPointAwardsGrid(container);
    }

    public Button getCreatePointAward()
    {
        return container.getButton("Create Point Award");
    }

    public void createPointAward(PointAwardBase pointAward, Verifier... verifiers)
    {
        getCreatePointAward().clickAndWait();
        new CreatePointAwardPopUp().createPointAward(pointAward, verifiers);
    }
}
