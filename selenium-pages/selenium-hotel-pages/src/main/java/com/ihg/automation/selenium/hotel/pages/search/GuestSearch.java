package com.ihg.automation.selenium.hotel.pages.search;

import com.ihg.automation.selenium.common.pages.SearchBase;
import com.ihg.automation.selenium.gwt.components.panel.Panel;
import com.ihg.automation.selenium.listener.Verifier;

public class GuestSearch extends SearchBase
{
    public GuestSearch()
    {
        super(new Panel("Guest Search"));
    }

    public CustomerSearch getCustomerSearch()
    {
        return new CustomerSearch(this);
    }

    public SearchByStay getSearchByStay()
    {
        return new SearchByStay(container);
    }

    public void byMemberNumber(String memberNumber, Verifier... verifiers)
    {
        clickClear();
        getCustomerSearch().getMemberNumber().type(memberNumber);
        clickSearch(verifiers);
    }

    public void byNumber(String number, Verifier... verifiers)
    {
        clickClear();
        getCustomerSearch().getNumber().type(number);
        clickSearch(verifiers);
    }

    public void byName(String lastName, String firstName, Verifier... verifiers)
    {
        clickClear();
        CustomerSearch customerSearch = getCustomerSearch();
        customerSearch.getLastName().type(lastName);
        customerSearch.getFirstName().type(firstName);
        clickSearch(verifiers);
    }
}
