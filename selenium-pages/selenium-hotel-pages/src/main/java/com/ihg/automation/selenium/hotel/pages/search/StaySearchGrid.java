package com.ihg.automation.selenium.hotel.pages.search;

import com.ihg.automation.selenium.gwt.components.grid.Grid;

public class StaySearchGrid extends Grid<StaySearchGridRow, StaySearchGridCell>
{
    public StaySearchGrid()
    {
        super("Stay Search Result");
    }

}
