package com.ihg.automation.selenium.hotel.pages.enroll;

import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.assertNoError;
import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyNoError;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ihg.automation.selenium.common.SiteHelperBase;
import com.ihg.automation.selenium.common.User;
import com.ihg.automation.selenium.common.enroll.EnrollProgramContainer;
import com.ihg.automation.selenium.common.enroll.EnrollmentPageBase;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.gwt.components.input.Input;
import com.ihg.automation.selenium.gwt.components.input.Select;
import com.ihg.automation.selenium.hotel.pages.LeftPanel;
import com.ihg.automation.selenium.hotel.pages.programs.amb.PrivacyConfirmationPopUp;
import com.ihg.automation.selenium.hotel.pages.programs.amb.PurchaseDetailsPopUp;
import com.ihg.automation.selenium.listener.Verifier;

public class EnrollmentPage extends EnrollmentPageBase
{
    private static final Logger logger = LoggerFactory.getLogger(EnrollmentPage.class);

    public EnrollmentPage()
    {
        super(new LeftPanel().getEnrollment());
    }

    public EarningPreference getEarningPreference()
    {
        return new EarningPreference(container);
    }

    public Select getHotelCode()
    {
        return container.getSelectByLabel("Hotel Code");
    }

    public Input getYourEmployeeID()
    {
        return container.getInputByLabel("Your Employee ID");
    }

    public void submitEnrollment(Member member, SiteHelperBase helper, boolean confirm, Boolean hasPrivacy)
    {
        goTo();
        populate(member, helper);
        if (confirm)
        {
            clickSubmitAndSkipWarnings(assertNoError());

            if (hasPrivacy)
            {
                PrivacyConfirmationPopUp privacyConfirmationPopUp = new PrivacyConfirmationPopUp();
                privacyConfirmationPopUp.getAccepted().check();
                privacyConfirmationPopUp.getConfirm().clickAndWait(verifyNoError());
            }

            if (null != member.getAmbassadorAmount())
            {
                PurchaseDetailsPopUp purchaseDetailsPopUp = new PurchaseDetailsPopUp();
                purchaseDetailsPopUp.selectAmount(member.getAmbassadorAmount());
                purchaseDetailsPopUp.clickSubmitNoError();
            }
        }
        else
        {
            clickCancel(assertNoError());
        }
    }

    public void populate(Member member, SiteHelperBase helper)
    {
        populate(member);
        getYourEmployeeID().type(helper.getUser().getEmployeeId());
    }

    public void submitEnrollment(Member member, SiteHelperBase helper)
    {
        submitEnrollment(member, helper, true, false);
    }

    public Member enroll(Member member, SiteHelperBase helper, boolean hasPrivacy, Verifier... verifiers)
    {
        submitEnrollment(member, helper, true, hasPrivacy);

        member = successEnrollmentPopUp(member, verifiers);

        return member;
    }

    public Member enroll(Member member, SiteHelperBase helper, Verifier... verifiers)
    {
        return enroll(member, helper, false, verifiers);
    }

    public Member enrollToAnotherProgram(Member member, User user, Program... programs)
    {
        member.addProgram(programs);

        goTo();

        for (Program program : programs)
        {
            new EnrollProgramContainer(container).selectProgram(program);
        }

        getYourEmployeeID().type(user.getEmployeeId());

        clickSubmitAndSkipWarnings(assertNoError());

        member = successEnrollmentPopUp(member);

        return member;
    }
}
