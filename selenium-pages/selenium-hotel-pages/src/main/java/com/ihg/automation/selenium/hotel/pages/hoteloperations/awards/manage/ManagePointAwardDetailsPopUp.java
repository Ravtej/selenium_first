package com.ihg.automation.selenium.hotel.pages.hoteloperations.awards.manage;

import com.ihg.automation.selenium.common.history.EventHistoryTab;
import com.ihg.automation.selenium.common.history.EventHistoryTabAware;
import com.ihg.automation.selenium.common.pages.PopUpBase;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.awards.posted.PostedPointAwardDetailsTab;

public class ManagePointAwardDetailsPopUp extends PopUpBase implements EventHistoryTabAware
{
    public ManagePointAwardDetailsPopUp()
    {
        super("Point Award Details");
    }

    public ManagePointAwardDetailsTab getManagePointAwardDetailsTab()
    {
        return new ManagePointAwardDetailsTab(this);
    }

    @Override
    public EventHistoryTab getHistoryTab()
    {
        return new EventHistoryTab(this, "History");
    }
}
