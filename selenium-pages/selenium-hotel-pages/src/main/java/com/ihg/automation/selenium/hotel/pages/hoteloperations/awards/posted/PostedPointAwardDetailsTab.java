package com.ihg.automation.selenium.hotel.pages.hoteloperations.awards.posted;

import com.ihg.automation.selenium.common.pages.PopUpBase;
import com.ihg.automation.selenium.common.pages.TabCustomContainerBase;

public class PostedPointAwardDetailsTab extends TabCustomContainerBase
{
    public PostedPointAwardDetailsTab(PopUpBase parent)
    {
        super(parent, "Posted Point Award Details");
    }

    public PostedMemberDetails getMemberDetails()
    {
        return new PostedMemberDetails(container);
    }

    public PostedPointAwardDetails getPostedPointAwardDetails()
    {
        return new PostedPointAwardDetails(container);
    }

    public void verify(PostedPointAward pointAward)
    {
        getMemberDetails().verify(pointAward);
        getPostedPointAwardDetails().verify(pointAward);
    }
}
