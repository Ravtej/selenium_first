package com.ihg.automation.selenium.hotel.pages.hoteloperations.stay.lpu.missingstay;

import com.ihg.automation.selenium.common.stay.Stay;
import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.CustomContainerBase;
import com.ihg.automation.selenium.gwt.components.input.DateInput;
import com.ihg.automation.selenium.gwt.components.input.Input;
import com.ihg.automation.selenium.gwt.components.input.Select;
import com.ihg.automation.selenium.gwt.components.label.Label;

public class StayDetailsEdit extends CustomContainerBase
{
    public StayDetailsEdit(Container container)
    {
        super(container.getSeparator("Stay Details"));
    }

    public DateInput getCheckIn()
    {
        return new DateInput(container.getLabel("Check In"));
    }

    public Select getCheckOut()
    {
        return container.getSelectByLabel("Check Out");
    }

    public Input getRoomNumber()
    {
        return container.getInputByLabel("Room Number");
    }

    public Input getRoomType()
    {
        return container.getInputByLabel("Room Type");
    }

    public Input getCorpAcctNumber()
    {
        return container.getInputByLabel("Corp Acct Number");
    }

    public Input getIATANumber()
    {
        return container.getInputByLabel("IATA Number");
    }

    public Input getFolioNumber()
    {
        return container.getInputByLabel("Folio Number");
    }

    public Label getHotelCurrency()
    {
        return container.getLabel("Hotel Currency");
    }

    public Input getConfirmationNumber()
    {
        return container.getInputByLabel("Confirmation Number");
    }

    public Input getRateCode()
    {
        return container.getInputByLabel("Rate Code");
    }

    public Label getHotel()
    {
        return container.getLabel("Hotel");
    }

    public void populate(Stay stay)
    {
        getCheckIn().typeAndWait(stay.getCheckIn());
        getRateCode().type(stay.getRateCode());
        getCorpAcctNumber().type(stay.getCorporateAccountNumber());

        getConfirmationNumber().type(stay.getConfirmationNumber());
        getCheckOut().select(stay.getCheckOut());
        getRoomType().type(stay.getRoomType());
        getIATANumber().type(stay.getIataCode());

        getFolioNumber().type(stay.getFolioNumber());
        getRoomNumber().type(stay.getRoomNumber());
    }
}
