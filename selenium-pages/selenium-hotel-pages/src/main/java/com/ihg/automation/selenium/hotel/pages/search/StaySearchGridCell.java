package com.ihg.automation.selenium.hotel.pages.search;

import com.ihg.automation.selenium.gwt.components.grid.CellsName;

public enum StaySearchGridCell implements CellsName
{
    GUEST_NAME("name"), //
    MEMBER_ID("membership.memberId"), //
    ADDRESS("address.addressLine"), //
    CITY("address.locality1"), //
    STATE("address.region1"), //
    ZIP("address.postalCode"), //
    COUNTRY("address.countryCode"), //
    PHONE("phone");

    private String name;

    private StaySearchGridCell(String name)
    {
        this.name = name;
    }

    @Override
    public String getName()
    {
        return name;
    }
}
