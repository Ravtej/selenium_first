package com.ihg.automation.selenium.hotel.pages.hoteloperations.reimbursement.rewardnight;

import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.grid.Grid;

public class RewardAndFreeNightsCountGrid
        extends Grid<RewardAndFreeNightsCountGridRow, RewardAndFreeNightsCountGridRow.RewardAndFreeNightsCountGridCell>
{
    public RewardAndFreeNightsCountGrid(Container parent)
    {
        super(parent);
    }

}
