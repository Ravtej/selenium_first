package com.ihg.automation.selenium.hotel.pages.hoteloperations.stay.lpu.missingstay;

import com.ihg.automation.selenium.common.pages.PopUpBase;
import com.ihg.automation.selenium.common.pages.TabCustomContainerBase;

public class AdjustRevenueDetailsTab extends TabCustomContainerBase
{
    public AdjustRevenueDetailsTab(PopUpBase parent)
    {
        super(parent, "Adjust Revenue Details");
    }

    public RevenueDetailsEdit getRevenueDetails()
    {
        return new RevenueDetailsEdit(container);
    }
}
