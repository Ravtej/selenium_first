package com.ihg.automation.selenium.hotel.pages.hoteloperations.br.manage;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;

import com.ihg.automation.selenium.common.business.BusinessEvent;
import com.ihg.automation.selenium.common.business.BusinessRewardsEventDetailsPopUp;
import com.ihg.automation.selenium.gwt.components.grid.CellsName;
import com.ihg.automation.selenium.gwt.components.grid.GridRow;
import com.ihg.automation.selenium.gwt.components.grid.GridRowXpath;

public class ManageEventsGridRow extends GridRow<ManageEventsGridRow.ManageEventsGridCell>
{
    public ManageEventsGridRow(ManageEventsGrid parent, GridRowXpath xpath)
    {
        super(parent, xpath);
    }

    public void verify(BusinessEvent businessEvent)
    {
        verifyThat(getCell(ManageEventsGridCell.MEMBER_NAME),
                hasText(businessEvent.getMember().getName().getShortName()));
        verifyThat(getCell(ManageEventsGridCell.MBR_NUMBER), hasText(businessEvent.getMember().getBRProgramId()));
        verifyThat(getCell(ManageEventsGridCell.EVENT_ID), hasText(businessEvent.getEventId()));
        verifyThat(getCell(ManageEventsGridCell.EVENT_NAME), hasText(businessEvent.getEventName()));
        verifyThat(getCell(ManageEventsGridCell.START_DATE), hasText(businessEvent.getMeetingDetails().getStartDate()));
        verifyThat(getCell(ManageEventsGridCell.END_DATE), hasText(businessEvent.getMeetingDetails().getEndDate()));
        verifyThat(getCell(ManageEventsGridCell.EVENT_STATUS), hasText(businessEvent.getStatus()));
        verifyThat(getCell(ManageEventsGridCell.DAYS_LEFT_TO_SUBMIT), hasText(businessEvent.getDaysLeftToSubmit()));
    }

    public BusinessRewardsEventDetailsPopUp openEventDetailsPopUp()
    {
        getCell(ManageEventsGridCell.EVENT_ID).asLink().click();
        return new BusinessRewardsEventDetailsPopUp();
    }

    public enum ManageEventsGridCell implements CellsName
    {
        MEMBER_NAME("memberName", "Member Name"), //
        MBR_NUMBER("membershipId", "Member Number"), //
        EVENT_ID("eventId", "Event ID"), //
        EVENT_NAME("eventName", "Event Name"), //
        START_DATE("startDate", "Start Date"), //
        END_DATE("endDate", "End Date"), //
        EVENT_STATUS("status", "Event Status"), //
        DAYS_LEFT_TO_SUBMIT("leftToSubmit", "Days Left to Submit");

        private String name;
        private String value;

        private ManageEventsGridCell(String name, String value)
        {
            this.name = name;
            this.value = value;
        }

        @Override
        public String getName()
        {
            return name;
        }

        public String getValue()
        {
            return value;
        }
    }
}
