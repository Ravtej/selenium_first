package com.ihg.automation.selenium.hotel.pages.hoteloperations.awards.manage;

import org.apache.commons.lang3.StringUtils;

import com.ihg.automation.common.DateUtils;
import com.ihg.automation.selenium.common.types.program.ProgramLevel;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.awards.PointAwardBase;

public class ManagePointAward extends PointAwardBase
{
    private ProgramLevel memberLevel;
    private String status;
    private String declinedReason = StringUtils.EMPTY;
    private String requestedDate = DateUtils.getFormattedDate();;
    private String confirmationNumber;
    private String rateCode;
    private String roomRate;
    private String hotelCurrency;
    private String corpAcctNumber;
    private String iATANumber;
    private String bookingDate;
    private String bookingSource;

    public ProgramLevel getMemberLevel()
    {
        return memberLevel;
    }

    public void setMemberLevel(ProgramLevel memberLevel)
    {
        this.memberLevel = memberLevel;
    }

    public String getStatus()
    {
        return status;
    }

    public void setStatus(String status)
    {
        this.status = status;
    }

    public String getDeclinedReason()
    {
        return declinedReason;
    }

    public void setDeclinedReason(String declinedReason)
    {
        this.declinedReason = declinedReason;
    }

    public String getRequestedDate()
    {
        return requestedDate;
    }

    public void setRequestedDate(String requestedDate)
    {
        this.requestedDate = requestedDate;
    }

    public String getConfirmationNumber()
    {
        return confirmationNumber;
    }

    public void setConfirmationNumber(String confirmationNumber)
    {
        this.confirmationNumber = confirmationNumber;
    }

    public String getRateCode()
    {
        return rateCode;
    }

    public void setRateCode(String rateCode)
    {
        this.rateCode = rateCode;
    }

    public String getRoomRate()
    {
        return roomRate;
    }

    public void setRoomRate(String roomRate)
    {
        this.roomRate = roomRate;
    }

    public String getHotelCurrency()
    {
        return hotelCurrency;
    }

    public void setHotelCurrency(String hotelCurrency)
    {
        this.hotelCurrency = hotelCurrency;
    }

    public String getCorpAcctNumber()
    {
        return corpAcctNumber;
    }

    public void setCorpAcctNumber(String corpAcctNumber)
    {
        this.corpAcctNumber = corpAcctNumber;
    }

    public String getiATANumber()
    {
        return iATANumber;
    }

    public void setiATANumber(String iATANumber)
    {
        this.iATANumber = iATANumber;
    }

    public String getBookingDate()
    {
        return bookingDate;
    }

    public void setBookingDate(String bookingDate)
    {
        this.bookingDate = bookingDate;
    }

    public String getBookingSource()
    {
        return bookingSource;
    }

    public void setBookingSource(String bookingSource)
    {
        this.bookingSource = bookingSource;
    }
}
