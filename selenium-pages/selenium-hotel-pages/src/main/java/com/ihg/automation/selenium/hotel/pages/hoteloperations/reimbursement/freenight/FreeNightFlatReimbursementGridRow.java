package com.ihg.automation.selenium.hotel.pages.hoteloperations.reimbursement.freenight;

import com.ihg.automation.selenium.gwt.components.grid.CellsName;
import com.ihg.automation.selenium.gwt.components.grid.GridRow;
import com.ihg.automation.selenium.gwt.components.grid.GridRowXpath;

public class FreeNightFlatReimbursementGridRow
        extends GridRow<FreeNightFlatReimbursementGridRow.FreeNightFlatReimbursementGridCell>
{
    public FreeNightFlatReimbursementGridRow(FreeNightFlatReimbursementGrid parent, GridRowXpath xpath)
    {
        super(parent, xpath);
    }

    public enum FreeNightFlatReimbursementGridCell implements CellsName
    {
        STAY_DATE("date"), NIGHTS_COUNT("nightsCount"), STATUS("status"), ACTUAL_REIMBURSEMENT(
                "actualReimbursement"), HOTEL_CURRENCY("hotelCurrency");

        private String name;

        private FreeNightFlatReimbursementGridCell(String name)
        {
            this.name = name;
        }

        @Override
        public String getName()
        {
            return name;
        }
    }

}
