package com.ihg.automation.selenium.hotel.pages.hoteloperations.stay.lpu.missingstay;

import static com.ihg.automation.selenium.gwt.components.ComponentColumnPosition.TABLE_TD;

import com.ihg.automation.selenium.common.components.IndicatorSelect;
import com.ihg.automation.selenium.common.pages.Populate;
import com.ihg.automation.selenium.formatter.Converter;
import com.ihg.automation.selenium.gwt.components.Component;
import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.input.Input;

public class RevenueExtended extends Revenue implements Populate<com.ihg.automation.selenium.common.stay.Revenue>
{
    public RevenueExtended(Container container, String labelText)
    {
        super(container, labelText);
    }

    @Override
    public Input getAmount()
    {
        return label.getInput("Local Amount", 1, TABLE_TD);
    }

    public IndicatorSelect getQualifying()
    {
        Component parent = label.getComponent("Qualifying?", 2, TABLE_TD);
        return new IndicatorSelect(parent);
    }

    public IndicatorSelect getFlagChangeReason()
    {
        Component parent = label.getComponent("Reason for Change ", 3, TABLE_TD);
        return new IndicatorSelect(parent);
    }

    @Override
    public void populate(com.ihg.automation.selenium.common.stay.Revenue revenue)
    {
        getAmount().typeAndWait(revenue.getAmount());
        getQualifying().select(Converter.booleanToString(revenue.isQualifying()));
        getFlagChangeReason().select(revenue.getReasonCode());
    }
}
