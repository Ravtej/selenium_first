package com.ihg.automation.selenium.hotel.pages.hoteloperations.br.posted;

import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.CustomContainerBase;
import com.ihg.automation.selenium.gwt.components.input.DateInput;
import com.ihg.automation.selenium.gwt.components.input.Select;

public class PostedEventsSearch extends CustomContainerBase
{
    public PostedEventsSearch(Container container)
    {
        super(container);
    }

    public Select getHotelCode()
    {
        return container.getSelectByLabel("Hotel Code");
    }

    public DateInput getFromDate()
    {
        return new DateInput(container.getLabel("From"));
    }

    public DateInput getToDate()
    {
        return new DateInput(container.getLabel("To"));
    }
}
