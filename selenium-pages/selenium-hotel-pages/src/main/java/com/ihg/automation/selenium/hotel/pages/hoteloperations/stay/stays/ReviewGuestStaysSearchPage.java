package com.ihg.automation.selenium.hotel.pages.hoteloperations.stay.stays;

import com.ihg.automation.selenium.common.event.SearchAware;
import com.ihg.automation.selenium.common.event.SearchButtonBar;
import com.ihg.automation.selenium.common.pages.TabCustomContainerBase;
import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.CustomContainerBase;
import com.ihg.automation.selenium.gwt.components.checkbox.CheckBox;
import com.ihg.automation.selenium.gwt.components.input.DateInput;
import com.ihg.automation.selenium.gwt.components.input.Select;
import com.ihg.automation.selenium.hotel.pages.Tabs;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.stay.ResultsFilter;

public class ReviewGuestStaysSearchPage extends TabCustomContainerBase implements SearchAware
{

    public ReviewGuestStaysSearchPage()
    {
        super(new Tabs().getStay().getReviewGuestStays());
    }

    @Override
    public SearchButtonBar getButtonBar()
    {
        return new SearchButtonBar(container);
    }

    @Override
    public LoyaltyStaySearch getSearchFields()
    {
        return new LoyaltyStaySearch(container);
    }

    public class LoyaltyStaySearch extends CustomContainerBase
    {
        public LoyaltyStaySearch(Container container)
        {
            super(container);
        }

        public Select getHotelSearch()
        {
            return container.getSelectByLabel("Search");
        }

        public CheckBox getServiceCenterCreatedStay()
        {
            return container.getCheckBox("Service Center Created Stay");
        }

        public CheckBox getSystemStay()
        {
            return container.getCheckBox("System Stay");
        }

        public CheckBox getEnrollingStay()
        {
            return container.getCheckBox("Enrolling Stay");
        }

        public CheckBox getCheckIn()
        {
            return container.getCheckBox("Check In");
        }

        public CheckBox getCheckOut()
        {
            return container.getCheckBox("Check Out");
        }

        public CheckBox getIHGRewardsClubStay()
        {
            return container.getCheckBox("IHG Rewards Club Stay");
        }

        public DateInput getFromDate()
        {
            return new DateInput(container.getLabel("From"));
        }

        public DateInput getToDate()
        {
            return new DateInput(container.getLabel("To"));
        }
    }

    public ResultsFilter getResultsFilter()
    {
        return new ResultsFilter(container);
    }

    public StaysGrid getStayGrid()
    {
        return new StaysGrid(container);
    }
}
