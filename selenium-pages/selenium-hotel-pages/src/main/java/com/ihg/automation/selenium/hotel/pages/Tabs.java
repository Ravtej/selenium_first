package com.ihg.automation.selenium.hotel.pages;

import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.gwt.components.CustomContainerBase;
import com.ihg.automation.selenium.gwt.components.Tab;

public class Tabs extends CustomContainerBase
{
    public CustomerInfoTab getCustomerInfo()
    {
        return new CustomerInfoTab();
    }

    public ProgramInfoTab getProgramInfo()
    {
        return new ProgramInfoTab();
    }

    public StayTab getStay()
    {
        return new StayTab();
    }

    public ReimbursementTab getReimbursement()
    {
        return new ReimbursementTab();
    }

    public IHGBusinessRewardsTab getIHGBusinessRewards()
    {
        return new IHGBusinessRewardsTab();
    }

    public AwardsTab getAwardsTab()
    {
        return new AwardsTab();
    }

    public EventsTab getEventsTab()
    {
        return new EventsTab();
    }

    public class CustomerInfoTab extends Tab
    {
        public CustomerInfoTab()
        {
            super(container, "Customer Information");
        }

        public Tab getPersonalInfo()
        {
            return new Tab(this, "Personal Information");
        }

        public Tab getCommunicationPref()
        {
            return new Tab(this, "Communication Preferences");
        }
    }

    public class ProgramInfoTab extends Tab
    {

        public ProgramInfoTab()
        {
            super(container, "Program Information");
        }

        public Tab getProgram(Program type)
        {
            return new Tab(this, type.getValue());
        }
    }

    public class StayTab extends Tab
    {
        public StayTab()
        {
            super(container, "Stay");
        }

        public Tab getLoyaltyPendingUpdates()
        {
            return new Tab(this, "LPU");
        }

        public Tab getReviewGuestStays()
        {
            return new Tab(this, "Stays");
        }
    }

    public class ReimbursementTab extends Tab
    {
        public ReimbursementTab()
        {
            super(container, "Reimbursement");
        }

        public Tab getOccupancyAndADREntry()
        {
            return new Tab(this, "Occupancy and ADR Entry");
        }

        public Tab getCertificateSearch()
        {
            return new Tab(this, "Certificate Search");
        }

        public Tab getRewardNightsSettings()
        {
            return new Tab(this, "Reward Nights Settings");
        }

        public Tab getFreeNightFlatReimbursement()
        {
            return new Tab(this, "Free Night Flat Reimbursement");
        }

        public Tab getOtherActivity()
        {
            return new Tab(this, "Other Activity");
        }
    }

    public class IHGBusinessRewardsTab extends Tab
    {
        public IHGBusinessRewardsTab()
        {
            super(container, "IHG Business Rewards");
        }

        public Tab getManageEvents()
        {
            return new Tab(this, "Manage Events");
        }

        public Tab getPostedEvents()
        {
            return new Tab(this, "Posted Events");
        }
    }

    public class AwardsTab extends Tab
    {
        public AwardsTab()
        {
            super(container, "Awards");
        }

        public Tab getManagePointAwards()
        {
            return new Tab(this, "Manage Point Awards");
        }

        public Tab getPostedPointAwards()
        {
            return new Tab(this, "Posted Point Awards");
        }
    }

    public class EventsTab extends Tab
    {
        public EventsTab()
        {
            super(container, "Events (view/create)");
        }

        public Tab getOrderHistory()
        {
            return new Tab(this, "Order History");
        }

        public Tab getNewOrders()
        {
            return new Tab(this, "New Orders");
        }
    }
}
