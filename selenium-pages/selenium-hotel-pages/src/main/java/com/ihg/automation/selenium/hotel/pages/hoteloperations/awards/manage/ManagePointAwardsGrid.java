package com.ihg.automation.selenium.hotel.pages.hoteloperations.awards.manage;

import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.grid.Grid;
import com.ihg.automation.selenium.gwt.components.grid.GridRowXpath;

public class ManagePointAwardsGrid
        extends Grid<ManagePointAwardsGridRow, ManagePointAwardsGridRow.ManagePointAwardsGridCell>
{
    public ManagePointAwardsGrid(Container parent)
    {
        super(parent);
    }

    public ManagePointAwardsGridRow getRow(String memberNumber)
    {
        return getRow(new GridRowXpath.GridRowXpathBuilder().cell(
                ManagePointAwardsGridRow.ManagePointAwardsGridCell.MBR_NUMBER, memberNumber).build());
    }
}
