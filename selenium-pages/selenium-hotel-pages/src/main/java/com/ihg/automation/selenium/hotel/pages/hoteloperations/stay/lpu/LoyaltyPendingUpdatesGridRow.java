package com.ihg.automation.selenium.hotel.pages.hoteloperations.stay.lpu;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasDefault;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasDefaultOrText;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasText;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasTextAsDouble;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasTextAsInt;
import static com.ihg.automation.selenium.matchers.text.EntityMatcherFactory.isValue;

import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.stay.Stay;
import com.ihg.automation.selenium.formatter.Converter;
import com.ihg.automation.selenium.gwt.components.grid.CellsName;
import com.ihg.automation.selenium.gwt.components.grid.GridRow;
import com.ihg.automation.selenium.gwt.components.grid.GridRowXpath;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.stay.lpu.missingstay.StayGridRowAdjustPanel;
import com.ihg.automation.selenium.matchers.component.HasText;

public class LoyaltyPendingUpdatesGridRow extends GridRow<LoyaltyPendingUpdatesGridRow.LoyaltyPendingUpdatesGridCell>
{
    public LoyaltyPendingUpdatesGridRow(LoyaltyPendingUpdatesGrid parent, GridRowXpath xpath)
    {
        super(parent, xpath);
    }

    public void verify(Member member, Stay stay)
    {
        verifyThat(getCell(LoyaltyPendingUpdatesGridCell.GUEST_NAME), hasText(member.getName().getShortName()));
        verifyThat(getCell(LoyaltyPendingUpdatesGridCell.MBR_NUMBER), hasText(member.getRCProgramId()));
        verifyThat(getCell(LoyaltyPendingUpdatesGridCell.CHECKIN), hasText(stay.getCheckIn()));
        verifyThat(getCell(LoyaltyPendingUpdatesGridCell.NIGHTS), hasTextAsInt(stay.getNights()));
        verifyThat(getCell(LoyaltyPendingUpdatesGridCell.ROOM_NUMBER), hasText(stay.getRoomNumber()));
        verifyThat(getCell(LoyaltyPendingUpdatesGridCell.RATE_CODE), hasText(stay.getRateCode()));
        verifyThat(getCell(LoyaltyPendingUpdatesGridCell.ROOM_RATE), hasTextAsDouble(stay.getAvgRoomRate()));
        verifyThat(getCell(LoyaltyPendingUpdatesGridCell.QUALIFYING_FLAG),
                hasText(Converter.booleanToString(stay.getIsQualifying())));
        if (stay.getAdjustReason() == null)
        {
            verifyThat(getCell(LoyaltyPendingUpdatesGridCell.REASON), hasDefault());
        }
        else
        {
            verifyThat(getCell(LoyaltyPendingUpdatesGridCell.REASON), hasText(isValue(stay.getAdjustReason())));
        }
        verifyThat(getCell(LoyaltyPendingUpdatesGridCell.TOTAL_QUALIFYING), hasDefaultOrText(Converter
                .doubleToString(Converter.ignoreZeroDouble(stay.getRevenues().getTotalQualifying().getAmount()))));
        verifyThat(getCell(LoyaltyPendingUpdatesGridCell.ADJUST), HasText.hasText("Adjust"));
    }

    public enum LoyaltyPendingUpdatesGridCell implements CellsName
    {
        GUEST_NAME("personName"), MBR_NUMBER("loyaltyMembershipId"), CHECKIN("checkIn"), NIGHTS("nights"), ROOM_NUMBER(
                "roomId"), RATE_CODE("rateCode"), ROOM_RATE("averageRoomRate"), QUALIFYING_FLAG(
                        "qualifyingFlag"), REASON(
                                "adjustReasonCode"), TOTAL_QUALIFYING("qualifyingRevenue"), ADJUST("action");

        private String name;

        private LoyaltyPendingUpdatesGridCell(String name)
        {
            this.name = name;
        }

        @Override
        public String getName()
        {
            return name;
        }
    }

    public StayGridRowAdjustPanel getStayGridRowAdjustPanel()
    {
        return new StayGridRowAdjustPanel(this);
    }
}
