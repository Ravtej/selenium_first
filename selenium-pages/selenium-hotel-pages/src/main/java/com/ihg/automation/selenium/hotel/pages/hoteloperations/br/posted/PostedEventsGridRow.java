package com.ihg.automation.selenium.hotel.pages.hoteloperations.br.posted;


import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.formatter.StringFormatter.asFormattedDouble;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasText;

import com.ihg.automation.selenium.common.business.BusinessEvent;
import com.ihg.automation.selenium.common.business.BusinessOffer;
import com.ihg.automation.selenium.common.business.MeetingDetails;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.gwt.components.grid.CellsName;
import com.ihg.automation.selenium.gwt.components.grid.GridRow;
import com.ihg.automation.selenium.gwt.components.grid.GridRowXpath;

public class PostedEventsGridRow extends GridRow<PostedEventsGridRow.PostedEventsGridCell>
{
    public PostedEventsGridRow(PostedEventsGrid parent, GridRowXpath xpath)
    {
        super(parent, xpath);
    }

    public enum PostedEventsGridCell implements CellsName
    {
        MEMBER_NAME("memberName", "Member Name"), //
        MBR_NUMBER("membershipId", "Member Number"), //
        EVENT_ID("eventId", "Event ID"), //
        EVENT_NAME("eventName", "Event Name"), //
        START_DATE("startDate", "Start Date"), //
        END_DATE("endDate", "End Date"), //
        POINTS_AWARDED("accruedUnits", "Points Awarded"), //
        ESTIMATED_COST("estimatedCostInHotelCurrency", "Estimated Cost"), //
        CURRENCY("hotelRevenueCurrencyCode", "Currency");

        private String name;
        private String value;

        private PostedEventsGridCell(String name, String value)
        {
            this.name = name;
            this.value = value;
        }

        @Override
        public String getName()
        {
            return name;
        }

        public String getValue()
        {
            return value;
        }
    }

    public void verify(BusinessEvent businessEvent)
    {
        Member member = businessEvent.getMember();
        verifyThat(getCell(PostedEventsGridRow.PostedEventsGridCell.MEMBER_NAME), hasText(
                member.getName().getShortName()));
        verifyThat(getCell(PostedEventsGridCell.MBR_NUMBER), hasText(member.getRCProgramId()));
        verifyThat(getCell(PostedEventsGridCell.EVENT_ID), hasText(businessEvent.getEventId()));
        verifyThat(getCell(PostedEventsGridCell.EVENT_NAME), hasText(businessEvent.getEventName()));

        MeetingDetails meetingDetails = businessEvent.getMeetingDetails();
        verifyThat(getCell(PostedEventsGridCell.START_DATE), hasText(meetingDetails.getStartDate()));
        verifyThat(getCell(PostedEventsGridCell.END_DATE), hasText(meetingDetails.getEndDate()));

        BusinessOffer offer = businessEvent.getOffer();
        verifyThat(getCell(PostedEventsGridCell.POINTS_AWARDED), hasText(asFormattedDouble(offer.getPointsToAward())));
        verifyThat(getCell(PostedEventsGridCell.ESTIMATED_COST), hasText(offer.getEstimatedCostToHotel()));
        verifyThat(getCell(PostedEventsGridCell.CURRENCY), hasText(businessEvent.getHotelCurrency().getCode()));
    }
}
