package com.ihg.automation.selenium.hotel.pages.order;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.order.DeliveryOption.EMAIL;
import static com.ihg.automation.selenium.common.order.DeliveryOption.PAPER;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasText;

import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.hotel.pages.CustomerInfoPanel;

public class ShippingInfoView extends com.ihg.automation.selenium.common.order.ShippingInfoView
{
    public ShippingInfoView(Container container)
    {
        super(container);
    }

    public void verifyAsOnCustomerInformation(String deliveryOption)
    {
        CustomerInfoPanel customerInfoPanel = new CustomerInfoPanel();

        if (EMAIL.equals(deliveryOption))
        {
            verifyThat(getEmail(), hasText(customerInfoPanel.getEmail().getText()));
        }
        else if (PAPER.equals(deliveryOption))
        {
            verifyThat(getAddress(), hasText(customerInfoPanel.getAddress().getText()));
        }
    }
}
