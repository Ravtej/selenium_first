package com.ihg.automation.selenium.hotel.pages.hoteloperations.awards.posted;

import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.grid.Grid;
import com.ihg.automation.selenium.gwt.components.grid.GridRowXpath;

public class PostedPointAwardsGrid
        extends Grid<PostedPointAwardsGridRow, PostedPointAwardsGridRow.PostedPointAwardsGridCell>
{
    public PostedPointAwardsGrid(Container parent)
    {
        super(parent);
    }

    public PostedPointAwardsGridRow getRow(String memberNumber)
    {
        return getRow(new GridRowXpath.GridRowXpathBuilder().cell(
                PostedPointAwardsGridRow.PostedPointAwardsGridCell.MBR_NUMBER, memberNumber).build());
    }

}
