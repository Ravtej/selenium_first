package com.ihg.automation.selenium.hotel.pages.hoteloperations.stay.lpu.missingstay;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;

import com.ihg.automation.selenium.common.pages.Populate;
import com.ihg.automation.selenium.common.stay.Stay;
import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.input.Input;
import com.ihg.automation.selenium.gwt.components.input.Select;

public class StayBookingInfoEdit extends StayBookingInfoBase implements Populate<Stay>
{
    public StayBookingInfoEdit(Container container)
    {
        super(container);
    }

    @Override
    public Input getBookingSource()
    {
        return container.getInputByLabel(BOOKING_SOURCE_LABEL);
    }

    public Select getStayPayment()
    {
        return container.getSelectByLabel("Stay Payment");
    }

    @Override
    public void populate(Stay stay)
    {
        getBookingSource().typeAndWait(stay.getBookingDetails().getBookingSource());
        getStayPayment().selectByCode(stay.getPayment());
    }

    public void verify(Stay stay)
    {
        super.verify(stay);
        verifyThat(getStayPayment(), hasText(stay.getBookingDetails().getBookingSource()));
    }
}
