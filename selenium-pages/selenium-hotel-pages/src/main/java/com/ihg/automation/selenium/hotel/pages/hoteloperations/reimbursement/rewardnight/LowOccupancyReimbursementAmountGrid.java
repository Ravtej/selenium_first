package com.ihg.automation.selenium.hotel.pages.hoteloperations.reimbursement.rewardnight;

import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.grid.Grid;

public class LowOccupancyReimbursementAmountGrid extends
        Grid<LowOccupancyReimbursementAmountGridRow, LowOccupancyReimbursementAmountGridRow.LowOccupancyReimbursementAmountGridCell>
{
    public LowOccupancyReimbursementAmountGrid(Container parent)
    {
        super(parent);
    }

}
