package com.ihg.automation.selenium.hotel.pages.order;

import com.ihg.automation.selenium.common.pages.TabCustomContainerBase;
import com.ihg.automation.selenium.hotel.pages.Tabs;

public class NewOrdersPage extends TabCustomContainerBase
{
    public NewOrdersPage()
    {
        super(new Tabs().getEventsTab().getNewOrders());
    }

    public VouchersGrid getVouchersGrid()
    {
        return new VouchersGrid(container);
    }
}
