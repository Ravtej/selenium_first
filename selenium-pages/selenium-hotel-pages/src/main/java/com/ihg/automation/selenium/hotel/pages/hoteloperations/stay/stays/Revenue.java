package com.ihg.automation.selenium.hotel.pages.hoteloperations.stay.stays;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.gwt.components.ComponentColumnPosition.FOLLOWING_TD;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasTextAsDouble;

import com.ihg.automation.selenium.common.pages.LabelContainerBase;
import com.ihg.automation.selenium.gwt.components.Component;
import com.ihg.automation.selenium.gwt.components.Container;

public class Revenue extends LabelContainerBase
{
    public Revenue(Container container, String labelText)
    {
        super(container.getLabel(labelText));
    }

    public Component getOriginalRevenue()
    {
        return label.getComponent("Original revenues", 1, FOLLOWING_TD);
    }

    public Component getCurrentRevenue()
    {
        return label.getComponent("Current revenues", 2, FOLLOWING_TD);
    }

    @Override
    public String toString()
    {
        return label.getDescription();
    }

    public void verifyOriginalRevenues(com.ihg.automation.selenium.common.stay.Revenue revenue)
    {
        verifyThat(getOriginalRevenue(), hasTextAsDouble(revenue.getAmount()));
    }

    public void verifyCurrentRevenue(com.ihg.automation.selenium.common.stay.Revenue revenue)
    {
        verifyThat(getCurrentRevenue(), hasTextAsDouble(revenue.getAmount()));
    }
}
