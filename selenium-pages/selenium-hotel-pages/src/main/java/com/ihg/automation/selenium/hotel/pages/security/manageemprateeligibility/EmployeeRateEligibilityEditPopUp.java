package com.ihg.automation.selenium.hotel.pages.security.manageemprateeligibility;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasText;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.isSelected;

import com.ihg.automation.selenium.gwt.components.Button;
import com.ihg.automation.selenium.gwt.components.checkbox.CheckBox;
import com.ihg.automation.selenium.gwt.components.label.Label;
import com.ihg.automation.selenium.gwt.components.utils.ByText;

public class EmployeeRateEligibilityEditPopUp extends EmployeeRateEligibilityPopUpBase
{
    public EmployeeRateEligibilityEditPopUp()
    {
        super();
    }

    public CheckBox getEligible()
    {
        return new CheckBox(container, ByText.exactSelf("Eligible"), CheckBox.CHECK_BOX_PATTERN2);
    }

    public Label getExpirationDate()
    {
        return container.getLabel("Expiration Date");
    }

    public Label getLocation()
    {
        return container.getLabel("Location");
    }

    public Button getExtend()
    {
        return container.getButton("Extend");
    }

    public void verify(EmployeeRateEligibilityRow row, boolean isEligible, String location)
    {
        verifyThat(getMemberNumber(), hasText(row.getMemberId()));
        verifyThat(getMemberName(), hasText(row.getFirstName() + " " + row.getLastName()));
        if (isEligible)
        {
            verifyThat(getEligible(), isSelected(true));
        }
        verifyThat(getExpirationDate(), hasText(row.getExpirationDate()));
        verifyThat(getLocation(), hasText(location));
    }
}
