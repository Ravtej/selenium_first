package com.ihg.automation.selenium.hotel.pages.security.manageemprateeligibility;

import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyNoError;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasText;

import com.ihg.automation.selenium.gwt.components.grid.CellsName;
import com.ihg.automation.selenium.gwt.components.grid.GridRow;
import com.ihg.automation.selenium.gwt.components.grid.GridRowXpath;

public class ManageEmpRateEligibilityGridRow
        extends GridRow<ManageEmpRateEligibilityGridRow.ManageEmpRateEligibilityGridCell>
{
    public ManageEmpRateEligibilityGridRow(ManageEmpRateEligibilityGrid parent, GridRowXpath xpath)
    {
        super(parent, xpath);
    }

    public enum ManageEmpRateEligibilityGridCell implements CellsName
    {
        DATE_UPDATED("dateUpdated"), FIRST_NAME("name.givenName"), LAST_NAME("name.surname"), MEMBER_NUMBER(
                "membershipId"), EXPIRATION_DATE("expiration");

        private String name;

        private ManageEmpRateEligibilityGridCell(String name)
        {
            this.name = name;
        }

        @Override
        public String getName()
        {
            return name;
        }
    }

    public void verify(EmployeeRateEligibilityRow eligibilityRow)
    {
        verifyThat(getCell(ManageEmpRateEligibilityGridCell.DATE_UPDATED), hasText(eligibilityRow.getDateUpdated()));
        verifyThat(getCell(ManageEmpRateEligibilityGridCell.FIRST_NAME), hasText(eligibilityRow.getFirstName()));
        verifyThat(getCell(ManageEmpRateEligibilityGridCell.LAST_NAME), hasText(eligibilityRow.getLastName()));
        verifyThat(getCell(ManageEmpRateEligibilityGridCell.MEMBER_NUMBER), hasText(eligibilityRow.getMemberId()));
        verifyThat(getCell(ManageEmpRateEligibilityGridCell.MEMBER_NUMBER).asLink(), displayed(true));
        verifyThat(getCell(ManageEmpRateEligibilityGridCell.EXPIRATION_DATE),
                hasText(eligibilityRow.getExpirationDate()));
    }

    public EmployeeRateEligibilityEditPopUp openEmployeeRateEligibilityEditPopUp()
    {
        getCell(ManageEmpRateEligibilityGridCell.MEMBER_NUMBER).asLink().clickAndWait(verifyNoError());
        return new EmployeeRateEligibilityEditPopUp();
    }
}
