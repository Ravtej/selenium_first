package com.ihg.automation.selenium.hotel.pages.hoteloperations.reimbursement.occupancy;

import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.grid.Grid;

public class OccupancyAndADREntrySearchGrid extends
        Grid<OccupancyAndADREntrySearchGridRow, OccupancyAndADREntrySearchGridRow.OccupancyAndADREntrySearchGridCell>
{
    public OccupancyAndADREntrySearchGrid(Container parent)
    {
        super(parent);
    }
}
