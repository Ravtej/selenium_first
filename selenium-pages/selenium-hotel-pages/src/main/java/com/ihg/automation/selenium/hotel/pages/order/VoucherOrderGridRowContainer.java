package com.ihg.automation.selenium.hotel.pages.order;

import com.ihg.automation.selenium.common.EventSourceAware;
import com.ihg.automation.selenium.common.event.EventGridRowContainerWithDetails;
import com.ihg.automation.selenium.common.event.EventSourceExtended;
import com.ihg.automation.selenium.common.order.VoucherOrder;
import com.ihg.automation.selenium.gwt.components.Container;

public class VoucherOrderGridRowContainer extends EventGridRowContainerWithDetails implements EventSourceAware
{
    public VoucherOrderGridRowContainer(Container parent)
    {
        super(parent);
    }

    public OrderDetailsView getOrderDetailsView()
    {
        return new OrderDetailsView(container);
    }

    public ShippingInfoView getShippingInfo()
    {
        return new ShippingInfoView(container);
    }

    @Override
    public EventSourceExtended getSource()
    {
        return new EventSourceExtended(container);
    }

    public void verify(VoucherOrder voucherOrder)
    {
        getOrderDetailsView().verify(voucherOrder);
        getShippingInfo().verifyAsOnCustomerInformation(voucherOrder.getDeliveryOption());
        getSource().verifyNoType(voucherOrder.getSource());
    }
}
