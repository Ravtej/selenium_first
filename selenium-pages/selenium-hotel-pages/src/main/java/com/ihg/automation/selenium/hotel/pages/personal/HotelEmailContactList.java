package com.ihg.automation.selenium.hotel.pages.personal;

import com.ihg.automation.selenium.common.member.GuestEmail;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.personal.ContactListBase;
import com.ihg.automation.selenium.gwt.components.Button;
import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.panel.EditablePanel;

public class HotelEmailContactList extends ContactListBase<GuestEmail, HotelEmailContact>
{
    public HotelEmailContactList(Container container)
    {
        super(container.getSeparator("Emails"));
    }

    @Override
    public Button getAddButton()
    {
        return container.getButton("Add email");
    }

    @Override
    public HotelEmailContact getContact(int index)
    {
        return new HotelEmailContact(new EditablePanel(container, index, EditablePanel.HOTEL_XPATH));
    }

    @Override
    protected GuestEmail getRandomContact()
    {
        return MemberPopulateHelper.getRandomEmail();
    }
}
