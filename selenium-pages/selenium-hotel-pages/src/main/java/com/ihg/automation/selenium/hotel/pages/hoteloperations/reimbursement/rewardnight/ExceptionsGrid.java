package com.ihg.automation.selenium.hotel.pages.hoteloperations.reimbursement.rewardnight;

import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.grid.Grid;

public class ExceptionsGrid extends Grid<ExceptionsGridRow, ExceptionsGridRow.ExceptionsGridCell>
{

    public ExceptionsGrid(Container parent)
    {
        super(parent);
    }
}
