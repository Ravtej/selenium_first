package com.ihg.automation.selenium.hotel.pages.hoteloperations.stay.lpu;

import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.grid.Grid;
import com.ihg.automation.selenium.gwt.components.grid.GridRowXpath;

public class LoyaltyPendingUpdatesGrid
        extends Grid<LoyaltyPendingUpdatesGridRow, LoyaltyPendingUpdatesGridRow.LoyaltyPendingUpdatesGridCell>
{
    public LoyaltyPendingUpdatesGrid(Container parent)
    {
        super(parent);
    }

    public LoyaltyPendingUpdatesGridRow getRow(String mbrNumber)
    {
        return getRow(new GridRowXpath.GridRowXpathBuilder()
                .cell(LoyaltyPendingUpdatesGridRow.LoyaltyPendingUpdatesGridCell.MBR_NUMBER, mbrNumber).build());
    }
}
