package com.ihg.automation.selenium.hotel.pages.hoteloperations.reimbursement.rewardnight;

import com.ihg.automation.selenium.gwt.components.grid.CellsName;
import com.ihg.automation.selenium.gwt.components.grid.GridRow;
import com.ihg.automation.selenium.gwt.components.grid.GridRowXpath;

public class ExceptionsGridRow extends GridRow<ExceptionsGridRow.ExceptionsGridCell>
{
    public ExceptionsGridRow(ExceptionsGrid parent, GridRowXpath xpath)
    {
        super(parent, xpath);
    }

    public enum ExceptionsGridCell implements CellsName
    {
        STAY_START_DATE("stayStartDate"), STAY_END_DATE("stayEndDate"), AMOUNT("amount"), CURRENCY("currency"), TYPE(
                "type");

        private String name;

        private ExceptionsGridCell(String name)
        {
            this.name = name;
        }

        @Override
        public String getName()
        {
            return name;
        }
    }
}
