package com.ihg.automation.selenium.hotel.pages.personal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ihg.automation.selenium.common.EmailNotMailableDialog;
import com.ihg.automation.selenium.common.contact.Email;
import com.ihg.automation.selenium.common.member.GuestEmail;
import com.ihg.automation.selenium.common.personal.EditableContactBase;
import com.ihg.automation.selenium.gwt.components.panel.EditablePanel;
import com.ihg.automation.selenium.listener.Verifier;

public class HotelEmailContact extends EditableContactBase<GuestEmail, Email>
{
    private static final Logger logger = LoggerFactory.getLogger(HotelEmailContact.class);

    public HotelEmailContact(EditablePanel parent)
    {
        super(parent, new Email(parent));
    }

    @Override
    public void populate(GuestEmail contact)
    {
        getBaseContact().populate(contact);
    }

    @Override
    public void update(GuestEmail contact, Verifier... verifiers)
    {
        super.update(contact, verifiers);

        EmailNotMailableDialog warningPopUp = new EmailNotMailableDialog();
        if (warningPopUp.isDisplayed())
        {
            logger.error(
                    "Most likely Email Validation Service is not available. Assuming the email address is correct and continuing enrollment");
            warningPopUp.clickSaveEmailAsIs();
        }
    }
}
