package com.ihg.automation.selenium.hotel.pages.order;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;

import com.ihg.automation.selenium.common.order.OrderDetailsViewBase;
import com.ihg.automation.selenium.common.order.OrderItem;
import com.ihg.automation.selenium.common.order.VoucherOrder;
import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.label.Label;

public class OrderDetailsView extends OrderDetailsViewBase
{
    public OrderDetailsView(Container container)
    {
        super(container);
    }

    @Override
    public Label getItemId()
    {
        return container.getLabel("Item ID");
    }

    @Override
    public void verify(VoucherOrder voucherOrder)
    {
        super.verify(voucherOrder);
        verifyThat(getItemId(), hasText(voucherOrder.getPromotionId()));
    }

    @Override
    public void verify(OrderItem orderItem, String unitType)
    {
        super.verify(orderItem, unitType);
        verifyThat(getItemId(), hasText(orderItem.getItemID()));
    }
}
