package com.ihg.automation.selenium.hotel.pages.order;

import com.ihg.automation.selenium.common.order.Order;
import com.ihg.automation.selenium.common.order.OrderDetailsTabBase;
import com.ihg.automation.selenium.common.order.VoucherOrder;
import com.ihg.automation.selenium.common.pages.PopUpBase;

public class OrderDetailsTab extends OrderDetailsTabBase
{
    public OrderDetailsTab(PopUpBase popUp)
    {
        super(popUp, "Detail");
    }

    public OrderDetailsEdit getOrderDetails()
    {
        return new OrderDetailsEdit(container);
    }

    public OrderDetailsEdit getOrderDetails(String itemId)
    {
        return new OrderDetailsEdit(container, itemId);
    }

    public ShippingInfoView getShippingInfoView()
    {
        return new ShippingInfoView(container);
    }

    public void verify(Order order)
    {
        super.verify(order);
        getShippingInfo().verify(order);
    }

    public void verify(VoucherOrder voucherOrder)
    {
        super.verify(voucherOrder);
        getOrderDetails().verify(voucherOrder);
        getShippingInfoView().verifyAsOnCustomerInformation(voucherOrder.getDeliveryOption());
    }
}
