package com.ihg.automation.selenium.hotel.pages.hoteloperations.awards.manage;

import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.CustomContainerBase;
import com.ihg.automation.selenium.gwt.components.label.Label;

public class PointAwardDetails extends CustomContainerBase
{
    public PointAwardDetails(Container container)
    {
        super(container.getSeparator("Point Award Details"));
    }

    public Label getCheckInDate()
    {
        return container.getLabel("Check In Date");
    }

    public Label getCheckOutDate()
    {
        return container.getLabel("Check Out Date");
    }

    public Label getStatus()
    {
        return container.getLabel("Status");
    }

    public Label getDeclinedReason()
    {
        return container.getLabel("Declined Reason");
    }

    public Label getRequestedDate()
    {
        return container.getLabel("Requested Date");
    }

    public ManagePointAwardDetails getManagePointAwardDetails()
    {
        return new ManagePointAwardDetails(container);
    }
}
