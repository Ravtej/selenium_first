package com.ihg.automation.selenium.hotel.pages.hoteloperations.stay.stays;

import com.ihg.automation.selenium.common.pages.PopUpBase;
import com.ihg.automation.selenium.common.pages.TabCustomContainerBase;

public class ViewStayDetailsTab extends TabCustomContainerBase
{
    public ViewStayDetailsTab(PopUpBase parent)
    {
        super(parent, "Revenue Details");
    }

    public StayRevenueDetailsView getStayRevenueDetailsView()
    {
        return new StayRevenueDetailsView(container);
    }
}
