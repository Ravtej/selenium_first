package com.ihg.automation.selenium.hotel.pages.hoteloperations.stay.lpu.missingstay;

import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyNoError;

import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.gwt.components.Button;
import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.input.Input;
import com.ihg.automation.selenium.gwt.components.label.Label;

public class GuestDetailsEdit extends GuestDetailsBase
{
    public GuestDetailsEdit(Container container)
    {
        super(container, "Guest Info");
    }

    @Override
    public Label getMemberName()
    {
        return container.getLabel("Member Name");
    }

    @Override
    public Input getMemberNumber()
    {
        return container.getInputByLabel(MEMBER_NUMBER);
    }

    public Button getEnter()
    {
        return container.getButton("Enter");
    }

    public Label getProgram()
    {
        return container.getLabel("Program");
    }

    public void addMemberId(Member member)
    {
        getMemberNumber().type(member.getRCProgramId());
        getEnter().clickAndWait(verifyNoError());
    }
}
