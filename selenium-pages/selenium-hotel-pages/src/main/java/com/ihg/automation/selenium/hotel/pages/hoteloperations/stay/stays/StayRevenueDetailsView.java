package com.ihg.automation.selenium.hotel.pages.hoteloperations.stay.stays;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasTextAsDouble;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasTextAsInt;

import com.ihg.automation.selenium.common.stay.Revenues;
import com.ihg.automation.selenium.common.stay.Stay;
import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.stay.RevenueDetailsBase;

public class StayRevenueDetailsView extends RevenueDetailsBase
{
    public StayRevenueDetailsView(Container container)
    {
        super(container);
    }

    @Override
    public Revenue getAvgRoomRate()
    {
        return new Revenue(container, AVERAGE_ROOM_RATE);
    }

    @Override
    public Revenue getNights()
    {
        return new Revenue(container, NIGHTS_LABEL);
    }

    @Override
    public Revenue getTotalRoom()
    {
        return new Revenue(container, TOTAL_ROOM_LABEL);
    }

    @Override
    public Revenue getFood()
    {
        return new Revenue(container, FOOD_LABEL);
    }

    @Override
    public Revenue getBeverage()
    {
        return new Revenue(container, BEVERAGE_LABEL);
    }

    @Override
    public Revenue getPhone()
    {
        return new Revenue(container, PHONE_LABEL);
    }

    @Override
    public Revenue getMeeting()
    {
        return new Revenue(container, MEETING_LABEL);
    }

    @Override
    public Revenue getMandatoryRevenue()
    {
        return new Revenue(container, MANDATORY_REVENUE_LABEL);
    }

    @Override
    public Revenue getOtherRevenue()
    {
        return new Revenue(container, OTHER_REVENUE_LABEL);
    }

    @Override
    public Revenue getNoRevenue()
    {
        return new Revenue(container, NO_REVENUE_LABEL);
    }

    @Override
    public Revenue getRoomTax()
    {
        return new Revenue(container, ROOM_TAX_LABEL);
    }

    @Override
    public Revenue getTotalRevenue()
    {
        return new Revenue(container, TOTAL_REVENUE_LABEL);
    }

    @Override
    public Revenue getTotalNonQualifyingRevenue()
    {
        return new Revenue(container, TOTAL_NON_QUALIFYING_REVENUE_LABEL);
    }

    @Override
    public Revenue getTotalQualifyingRevenue()
    {
        return new Revenue(container, TOTAL_QUALIFYING_REVENUE_LABEL);
    }

    public void verifyOriginalRevenues(Stay stay)
    {
        verifyThat(getAvgRoomRate().getOriginalRevenue(), hasTextAsDouble(stay.getAvgRoomRate()));
        verifyThat(getNights().getOriginalRevenue(), hasTextAsInt(stay.getNights()));

        Revenues revenues = stay.getRevenues();
        getTotalRoom().verifyOriginalRevenues(revenues.getRoom());
        getFood().verifyOriginalRevenues(revenues.getFood());
        getBeverage().verifyOriginalRevenues(revenues.getBeverage());
        getPhone().verifyOriginalRevenues(revenues.getPhone());
        getMeeting().verifyOriginalRevenues(revenues.getMeeting());
        getMandatoryRevenue().verifyOriginalRevenues(revenues.getMandatoryLoyalty());
        getOtherRevenue().verifyOriginalRevenues(revenues.getOtherLoyalty());
        getNoRevenue().verifyOriginalRevenues(revenues.getNoLoyalty());
        getRoomTax().verifyOriginalRevenues(revenues.getTax());
        getTotalRevenue().verifyOriginalRevenues(revenues.getOverallTotal());
        getTotalNonQualifyingRevenue().verifyOriginalRevenues(revenues.getTotalNonQualifying());
        getTotalQualifyingRevenue().verifyOriginalRevenues(revenues.getTotalQualifying());
    }

    public void verifyCurrentRevenues(Stay stay)
    {
        verifyThat(getAvgRoomRate().getCurrentRevenue(), hasTextAsDouble(stay.getAvgRoomRate()));
        verifyThat(getNights().getCurrentRevenue(), hasTextAsInt(stay.getNights()));

        Revenues revenues = stay.getRevenues();
        getTotalRoom().verifyCurrentRevenue(revenues.getRoom());
        getFood().verifyCurrentRevenue(revenues.getFood());
        getBeverage().verifyCurrentRevenue(revenues.getBeverage());
        getPhone().verifyCurrentRevenue(revenues.getPhone());
        getMeeting().verifyCurrentRevenue(revenues.getMeeting());
        getMandatoryRevenue().verifyCurrentRevenue(revenues.getMandatoryLoyalty());
        getOtherRevenue().verifyCurrentRevenue(revenues.getOtherLoyalty());
        getNoRevenue().verifyCurrentRevenue(revenues.getNoLoyalty());
        getRoomTax().verifyCurrentRevenue(revenues.getTax());
        getTotalRevenue().verifyCurrentRevenue(revenues.getOverallTotal());
        getTotalNonQualifyingRevenue().verifyCurrentRevenue(revenues.getTotalNonQualifying());
        getTotalQualifyingRevenue().verifyCurrentRevenue(revenues.getTotalQualifying());
    }

    public void verifyRevenuesForNewStay(Stay stay)
    {
        verifyOriginalRevenues(stay);
        verifyCurrentRevenues(stay);
    }
}
