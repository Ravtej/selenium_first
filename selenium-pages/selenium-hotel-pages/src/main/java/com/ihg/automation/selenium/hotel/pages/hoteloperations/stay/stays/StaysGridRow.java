package com.ihg.automation.selenium.hotel.pages.hoteloperations.stay.stays;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasText;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasTextAsInt;

import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.stay.Stay;
import com.ihg.automation.selenium.formatter.Converter;
import com.ihg.automation.selenium.gwt.components.grid.CellsName;
import com.ihg.automation.selenium.gwt.components.grid.GridRow;
import com.ihg.automation.selenium.gwt.components.grid.GridRowXpath;

public class StaysGridRow extends GridRow<StaysGridRow.StaysGridCell>
{
    public StaysGridRow(StaysGrid parent, GridRowXpath xpath)
    {
        super(parent, xpath);
    }

    public enum StaysGridCell implements CellsName
    {
        GUEST_NAME("personName"), MBR_NUMBER("loyaltyMembershipId"), CHECK_IN("checkIn"), CHECK_OUT("checkOut"), NIGHTS(
                "nights"), ROOM_NUMBER("roomId"), CONF_NUMBER("confirmationId"), FOLIO("folio"), QUALIFYING_FLAG(
                "qualifyingFlag"), ENROLLING_STAY("enrolling");

        private String name;

        private StaysGridCell(String name)
        {
            this.name = name;
        }

        @Override
        public String getName()
        {
            return name;
        }
    }

    public void verify(Member member, Stay stay)
    {
        verifyThat(getCell(StaysGridCell.GUEST_NAME), hasText(member.getName().getShortName()));
        verifyThat(getCell(StaysGridCell.MBR_NUMBER), hasText(member.getRCProgramId()));
        verifyThat(getCell(StaysGridCell.CHECK_IN), hasText(stay.getCheckIn()));
        verifyThat(getCell(StaysGridCell.CHECK_OUT), hasText(stay.getCheckOut()));
        verifyThat(getCell(StaysGridCell.NIGHTS), hasTextAsInt(stay.getNights()));
        verifyThat(getCell(StaysGridCell.ROOM_NUMBER), hasText(stay.getRoomNumber()));
        verifyThat(getCell(StaysGridCell.CONF_NUMBER), hasText(stay.getConfirmationNumber()));
        verifyThat(getCell(StaysGridCell.FOLIO), hasText(stay.getFolioNumber()));
        verifyThat(getCell(StaysGridCell.QUALIFYING_FLAG), hasText(Converter.booleanToString(stay.getIsQualifying())));
        verifyThat(getCell(StaysGridCell.ENROLLING_STAY), hasText(stay.getEnrollingStay()));
    }
}
