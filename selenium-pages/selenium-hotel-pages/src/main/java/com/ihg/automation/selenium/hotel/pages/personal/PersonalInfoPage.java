package com.ihg.automation.selenium.hotel.pages.personal;

import com.ihg.automation.selenium.common.pages.TabCustomContainerBase;
import com.ihg.automation.selenium.hotel.pages.Tabs;

public class PersonalInfoPage extends TabCustomContainerBase
{
    public PersonalInfoPage()
    {
        super(new Tabs().getCustomerInfo().getPersonalInfo());
    }

    public CustomerInfoFields getCustomerInfo()
    {
        return new CustomerInfoFields(container);
    }

    public HotelAddressContactList getAddressList()
    {
        return new HotelAddressContactList(container);
    }

    public HotelPhoneContactList getPhoneList()
    {
        return new HotelPhoneContactList(container);
    }

    public HotelSmsContactList getSmsList()
    {
        return new HotelSmsContactList(container);
    }

    public HotelEmailContactList getEmailList()
    {
        return new HotelEmailContactList(container);
    }
}
