package com.ihg.automation.selenium.hotel.pages.hoteloperations.stay.lpu.missingstay;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.gwt.components.Mode.EDIT;
import static com.ihg.automation.selenium.gwt.components.Mode.VIEW;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasTextAsDouble;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasTextAsInt;

import com.ihg.automation.selenium.common.pages.Populate;
import com.ihg.automation.selenium.common.stay.Revenues;
import com.ihg.automation.selenium.common.stay.Stay;
import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.stay.RevenueDetailsBase;

public class RevenueDetailsEdit extends RevenueDetailsBase implements Populate<Stay>
{
    public RevenueDetailsEdit(Container container)
    {
        super(container);
    }

    @Override
    public RevenueExtended getAvgRoomRate()
    {
        return new RevenueExtended(container, AVERAGE_ROOM_RATE);
    }

    @Override
    public Revenue getNights()
    {
        return new Revenue(container, NIGHTS_LABEL);
    }

    @Override
    public Revenue getTotalRoom()
    {
        return new Revenue(container, TOTAL_ROOM_LABEL);
    }

    @Override
    public RevenueExtended getFood()
    {
        return new RevenueExtended(container, FOOD_LABEL);
    }

    @Override
    public RevenueExtended getBeverage()
    {
        return new RevenueExtended(container, BEVERAGE_LABEL);
    }

    @Override
    public RevenueExtended getPhone()
    {
        return new RevenueExtended(container, PHONE_LABEL);
    }

    @Override
    public RevenueExtended getMeeting()
    {
        return new RevenueExtended(container, MEETING_LABEL);
    }

    @Override
    public RevenueExtended getMandatoryRevenue()
    {
        return new RevenueExtended(container, MANDATORY_REVENUE_LABEL);
    }

    @Override
    public RevenueExtended getOtherRevenue()
    {
        return new RevenueExtended(container, OTHER_REVENUE_LABEL);
    }

    @Override
    public Revenue getNoRevenue()
    {
        return new Revenue(container, NO_REVENUE_LABEL);
    }

    @Override
    public Revenue getRoomTax()
    {
        return new Revenue(container, ROOM_TAX_LABEL);
    }

    @Override
    public Revenue getTotalRevenue()
    {
        return new Revenue(container, TOTAL_REVENUE_LABEL);
    }

    @Override
    public Revenue getTotalNonQualifyingRevenue()
    {
        return new Revenue(container, TOTAL_NON_QUALIFYING_REVENUE_LABEL);
    }

    @Override
    public Revenue getTotalQualifyingRevenue()
    {
        return new Revenue(container, TOTAL_QUALIFYING_REVENUE_LABEL);
    }

    @Override
    public void populate(Stay stay)
    {
        populateAdjustRevenues(stay);
        Revenues revenues = stay.getRevenues();
        getNoRevenue().getAmount().type(revenues.getNoLoyalty().getAmount());
        getRoomTax().getAmount().type(revenues.getTax().getAmount());
    }

    public void populateAdjustRevenues(Stay stay)
    {
        getAvgRoomRate().getAmount().type(stay.getAvgRoomRate());
        Revenues revenues = stay.getRevenues();
        getFood().populate(revenues.getFood());
        getBeverage().populate(revenues.getBeverage());
        getPhone().populate(revenues.getPhone());
        getMeeting().populate(revenues.getMeeting());
        getMandatoryRevenue().populate(revenues.getMandatoryLoyalty());
        getOtherRevenue().populate(revenues.getOtherLoyalty());
    }

    public void verifyCreateAmount(Stay stay)
    {
        verifyThat(getAvgRoomRate().getCreateAmount(), hasTextAsDouble(stay.getAvgRoomRate()));
        verifyThat(getNights().getCreateAmount(), hasTextAsInt(stay.getNights()));
        Revenues revenues = stay.getRevenues();
        getTotalRoom().verifyCreateAmount(revenues.getRoom());
        getFood().verifyCreateAmount(revenues.getFood());
        getBeverage().verifyCreateAmount(revenues.getBeverage());
        getPhone().verifyCreateAmount(revenues.getPhone());
        getMeeting().verifyCreateAmount(revenues.getMeeting());
        getMandatoryRevenue().verifyCreateAmount(revenues.getMandatoryLoyalty());
        getOtherRevenue().verifyCreateAmount(revenues.getOtherLoyalty());
        getNoRevenue().verifyCreateAmount(revenues.getNoLoyalty());
        getRoomTax().verifyCreateAmount(revenues.getTax());
        getTotalRevenue().verifyCreateAmount(revenues.getOverallTotal());
        getTotalNonQualifyingRevenue().verifyCreateAmount(revenues.getTotalNonQualifying());
        getTotalQualifyingRevenue().verifyCreateAmount(revenues.getTotalQualifying());
    }

    public void verifyAdjustAmount(Stay stay)
    {
        verifyThat(getAvgRoomRate().getAmount(), hasTextAsDouble(stay.getAvgRoomRate()));
        verifyThat(getNights().getAmount().getView(), hasTextAsInt(stay.getNights()));

        Revenues revenues = stay.getRevenues();
        getTotalRoom().verifyAdjustAmount(revenues.getRoom(), VIEW);
        getFood().verifyAdjustAmount(revenues.getFood(), EDIT);
        getBeverage().verifyAdjustAmount(revenues.getBeverage(), EDIT);
        getPhone().verifyAdjustAmount(revenues.getPhone(), EDIT);
        getMeeting().verifyAdjustAmount(revenues.getMeeting(), EDIT);
        getMandatoryRevenue().verifyAdjustAmount(revenues.getMandatoryLoyalty(), EDIT);
        getOtherRevenue().verifyAdjustAmount(revenues.getOtherLoyalty(), EDIT);
        getNoRevenue().verifyAdjustAmount(revenues.getNoLoyalty(), VIEW);
        getRoomTax().verifyAdjustAmount(revenues.getTax(), VIEW);
        getTotalRevenue().verifyAdjustAmount(revenues.getOverallTotal(), VIEW);
        getTotalNonQualifyingRevenue().verifyAdjustAmount(revenues.getTotalNonQualifying(), VIEW);
        getTotalQualifyingRevenue().verifyAdjustAmount(revenues.getTotalQualifying(), VIEW);
    }

    public void verifyCreateAndAdjustedAmount(Stay createStay, Stay adjustStay)
    {
        verifyCreateAmount(createStay);
        verifyAdjustAmount(adjustStay);
    }
}
