package com.ihg.automation.selenium.hotel.pages.search;

import com.ihg.automation.selenium.common.components.RangeSelect;
import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.CustomContainerBase;
import com.ihg.automation.selenium.gwt.components.input.DateInput;
import com.ihg.automation.selenium.gwt.components.input.Input;
import com.ihg.automation.selenium.gwt.components.input.InputXpath;
import com.ihg.automation.selenium.gwt.components.input.Select;
import com.ihg.automation.selenium.gwt.components.panel.FieldSet;

public class SearchByStay extends CustomContainerBase
{
    public SearchByStay(Container container)
    {
        super(container.getFieldSet("Stay Details"));
    }

    public Select getHotel()
    {
        return container.getSelect("Hotel", 1);
    }

    public DateInput getStayDate()
    {
        return new DateInput(container, new InputXpath.InputXpathBuilder().row(2).column(1).build(), "Stay Date");
    }

    public RangeSelect getStayDateRange()
    {
        return new RangeSelect(container, new InputXpath.InputXpathBuilder().row(2).column(2).build(),
                "Stay Date Range");
    }

    public Input getFolioNumber()
    {
        return container.getInput("Folio Number", 3);
    }

    public Input getRoomNumber()
    {
        return container.getInput("Room Number", 4);
    }

    public Input getCreditCardNumber()
    {
        return container.getInput("CC Number", 5);
    }

    public Input getConfirmationNumber()
    {
        return container.getInput("Confirmation Number", 6);
    }

    public void expand()
    {
        ((FieldSet) container).expand();
    }
}
