package com.ihg.automation.selenium.hotel.pages.hoteloperations.awards.manage;

import com.ihg.automation.selenium.common.EventSource;
import com.ihg.automation.selenium.common.EventSourceAware;
import com.ihg.automation.selenium.common.pages.PopUpBase;
import com.ihg.automation.selenium.common.pages.TabCustomContainerBase;

public class ManagePointAwardDetailsTab extends TabCustomContainerBase implements EventSourceAware
{
    public ManagePointAwardDetailsTab(PopUpBase parent)
    {
        super(parent, "Posted Point Award Details");
    }

    public MemberDetails getMemberDetails()
    {
        return new MemberDetails(container);
    }

    public PointAwardDetails getPointAwardDetails()
    {
        return new PointAwardDetails(container);
    }

    @Override
    public EventSource getSource()
    {
        return new EventSource(container);
    }
}
