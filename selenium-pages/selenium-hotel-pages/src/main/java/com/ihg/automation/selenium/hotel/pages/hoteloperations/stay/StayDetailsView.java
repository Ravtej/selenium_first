package com.ihg.automation.selenium.hotel.pages.hoteloperations.stay;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasDefaultOrText;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasEmptyOrNullText;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasTextAsInt;
import static com.ihg.automation.selenium.matchers.component.HasDefaultText.hasDefault;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static com.ihg.automation.selenium.matchers.text.EntityMatcherFactory.isValue;
import static org.hamcrest.core.AllOf.allOf;
import static org.hamcrest.core.IsNot.not;

import com.ihg.automation.selenium.common.pages.Verify;
import com.ihg.automation.selenium.common.stay.Stay;
import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.CustomContainerBase;
import com.ihg.automation.selenium.gwt.components.label.Label;

public class StayDetailsView extends CustomContainerBase implements Verify<Stay>
{
    public StayDetailsView(Container container)
    {
        super(container.getSeparator("Stay Details"));
    }

    public Label getCheckIn()
    {
        return container.getLabel("Check In");
    }

    public Label getCheckOut()
    {
        return container.getLabel("Check Out");
    }

    public Label getRoomNumber()
    {
        return container.getLabel("Room Number");
    }

    public Label getRoomType()
    {
        return container.getLabel("Room Type");
    }

    public Label getCorpAcctNumber()
    {
        return container.getLabel("Corp Acct Number");
    }

    public Label getIATANumber()
    {
        return container.getLabel("IATA Number");
    }

    public Label getHotelCurrency()
    {
        return container.getLabel("Hotel Currency");
    }

    public Label getStayPayment()
    {
        return container.getLabel("Stay Payment");
    }

    public Label getConfirmationNumber()
    {
        return container.getLabel("Confirmation Number");
    }

    public Label getFolioNumber()
    {
        return container.getLabel("Folio Number");
    }

    public Label getOverlappingStay()
    {
        return container.getLabel("Overlapping Stay");
    }

    public Label getQualifiedNights()
    {
        return container.getLabel("Q-Nights");
    }

    public void verify(Stay stay)
    {
        verifyThat(getCheckIn(), hasText(stay.getCheckIn()));
        verifyThat(getCheckOut(), hasText(stay.getCheckOut()));
        verifyThat(getConfirmationNumber(), hasText(stay.getConfirmationNumber()));
        verifyThat(getFolioNumber(), hasText(stay.getFolioNumber()));
        verifyThat(getRoomNumber(), hasText(stay.getRoomNumber()));
        verifyThat(getRoomType(), hasText(stay.getRoomType()));
        verifyThat(getCorpAcctNumber(), hasText(stay.getCorporateAccountNumber()));
        verifyThat(getIATANumber(), hasText(stay.getIataCode()));
        verifyThat(getOverlappingStay(), hasDefaultOrText(stay.getOverlappingStay()));

        if (stay.getQualifyingNights() == null)
        {
            verifyThat(getQualifiedNights(), hasDefault());
        }
        else
        {
            verifyThat(getQualifiedNights(), hasTextAsInt(stay.getQualifyingNights()));
        }

        if (stay.getHotelCurrency() == null)
        {
            verifyThat(getHotelCurrency(), allOf(not(hasDefault()), not(hasEmptyOrNullText())));
        }
        else
        {
            verifyThat(getHotelCurrency(), hasText(isValue(stay.getHotelCurrency())));
        }
    }
}
