package com.ihg.automation.selenium.hotel.pages.security.security;

import com.ihg.automation.selenium.common.event.SearchAware;
import com.ihg.automation.selenium.common.event.SearchButtonBar;
import com.ihg.automation.selenium.gwt.components.CustomContainerBase;
import com.ihg.automation.selenium.gwt.components.panel.Panel;

public class UserManagementPage extends CustomContainerBase implements SearchAware
{
    public UserManagementPage()
    {
        super(new Panel("User Management"));
    }

    @Override
    public UserManagementSearch getSearchFields()
    {
        return new UserManagementSearch(container);
    }

    @Override
    public SearchButtonBar getButtonBar()
    {
        return new SearchButtonBar(container);
    }

    public UserManagementGrid getUserManagementGrid()
    {
        return new UserManagementGrid(container);
    }

    public void searchMemberByRole(UserManagement userManagement, String role)
    {
        SearchButtonBar buttonBar = getButtonBar();

        buttonBar.clickClearCriteria();
        getSearchFields().populate(userManagement, role);
        buttonBar.clickSearch();
    }

    public void searchMemberByAllRoles(UserManagement userManagement)
    {
        SearchButtonBar buttonBar = getButtonBar();

        buttonBar.clickClearCriteria();
        searchMemberByRole(userManagement, "All");
        buttonBar.clickSearch();
    }

    public void verifyAllMemberData(UserManagement userManagement)
    {
        getUserManagementGrid().getRowByUserName(userManagement).verify(userManagement);
    }
}
