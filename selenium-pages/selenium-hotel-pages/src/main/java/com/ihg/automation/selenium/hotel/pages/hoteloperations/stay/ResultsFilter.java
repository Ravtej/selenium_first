package com.ihg.automation.selenium.hotel.pages.hoteloperations.stay;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.gwt.components.ComponentColumnPosition.TABLE;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;

import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.stay.Stay;
import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.CustomContainerBase;
import com.ihg.automation.selenium.gwt.components.input.Input;
import com.ihg.automation.selenium.gwt.components.input.InputXpath;
import com.ihg.automation.selenium.gwt.components.panel.FieldSet;

public class ResultsFilter extends CustomContainerBase
{
    public ResultsFilter(Container container)
    {
        super(container.getFieldSet("Results Filter"));
    }

    public void expand()
    {
        ((FieldSet) container).expand();
    }

    public Input getGuestName()
    {
        return new Input(container, new InputXpath.InputXpathBuilder().row(1).column(1, TABLE).build(), "Guest Name");
    }

    public Input getConfirmationNumber()
    {
        return new Input(container, new InputXpath.InputXpathBuilder().row(1).column(2, TABLE).build(),
                "Confirmation Number");
    }

    public Input getRateCategory()
    {
        return new Input(container, new InputXpath.InputXpathBuilder().row(1).column(3, TABLE).build(),
                "Rate Category");
    }

    public Input getFolioNumber()
    {
        return new Input(container, new InputXpath.InputXpathBuilder().row(1).column(4, TABLE).build(), "Folio Number");
    }

    public Input getMemberNumber()
    {
        return new Input(container, new InputXpath.InputXpathBuilder().row(2).column(1, TABLE).build(),
                "Member Number");
    }

    public Input getIataNumber()
    {
        return new Input(container, new InputXpath.InputXpathBuilder().row(2).column(2, TABLE).build(), "Iata Number");
    }

    public Input getCorporateId()
    {
        return new Input(container, new InputXpath.InputXpathBuilder().row(2).column(3, TABLE).build(), "Corporate Id");
    }

    public Input getRoomNumber()
    {
        return new Input(container, new InputXpath.InputXpathBuilder().row(2).column(4, TABLE).build(), "Room Number");
    }

    public void populate(Member member, Stay stay)
    {
        getGuestName().type(member.getName().getFullName());
        getConfirmationNumber().type(stay.getConfirmationNumber());
        getRateCategory().type(stay.getRateCode());
        getFolioNumber().type(stay.getFolioNumber());
        getMemberNumber().type(member.getRCProgramId());
        getIataNumber().type(stay.getIataCode());
        getCorporateId().type(stay.getCorporateAccountNumber());
        getRoomNumber().type(stay.getRoomNumber());
    }

    public void verifyDefault()
    {
        expand();
        verifyThat(getGuestName(), hasText("Guest Name"));
        verifyThat(getMemberNumber(), hasText("Member Number"));
        verifyThat(getConfirmationNumber(), hasText("Confirmation#"));
        verifyThat(getIataNumber(), hasText("IATA#"));
        verifyThat(getFolioNumber(), hasText("Folio#"));
        verifyThat(getRoomNumber(), hasText("Room#"));
        verifyThat(getRateCategory(), hasText("Rate Category"));
        verifyThat(getCorporateId(), hasText("Corporate ID"));
    }
}
