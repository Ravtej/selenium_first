package com.ihg.automation.selenium.hotel.pages.hoteloperations.stay.stays;

import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.Link;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.stay.StayGridRowContainerBase;

public class StayGridRowContainer extends StayGridRowContainerBase
{
    public StayGridRowContainer(Container container)
    {
        super(container);
    }

    public Link getViewStayDetails()
    {
        return container.getLink("View Stay Details");
    }
}
