package com.ihg.automation.selenium.hotel.pages.hoteloperations.awards.posted;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;

import com.ihg.automation.selenium.gwt.components.grid.CellsName;
import com.ihg.automation.selenium.gwt.components.grid.GridRow;
import com.ihg.automation.selenium.gwt.components.grid.GridRowXpath;

public class PostedPointAwardsGridRow extends GridRow<PostedPointAwardsGridRow.PostedPointAwardsGridCell>
{
    public PostedPointAwardsGridRow(PostedPointAwardsGrid parent, GridRowXpath xpath)
    {
        super(parent, xpath);
    }

    public void verify(PostedPointAward pointAward)
    {
        verifyThat(getCell(PostedPointAwardsGridCell.TRANSACTION_DATE), hasText(pointAward.getTransactionDate()));
        verifyThat(getCell(PostedPointAwardsGridCell.GUEST_NAME), hasText(pointAward.getMemberName()));
        verifyThat(getCell(PostedPointAwardsGridCell.MBR_NUMBER), hasText(pointAward.getMemberNumber()));
        verifyThat(getCell(PostedPointAwardsGridCell.AWARD_TYPE), hasText(pointAward.getTransaction().getType()));
        verifyThat(getCell(PostedPointAwardsGridCell.AWARD_DESCRIPTION), hasText(pointAward.getDescription()));
        verifyThat(getCell(PostedPointAwardsGridCell.POINTS), hasText(pointAward.getPoints()));
        verifyThat(getCell(PostedPointAwardsGridCell.USD_AMOUNT), hasText(pointAward.getCostOfPoints()));
    }

    public enum PostedPointAwardsGridCell implements CellsName
    {
        TRANSACTION_DATE("eventDate"), GUEST_NAME("guestName"), MBR_NUMBER("memberNumber"), AWARD_TYPE(
                "transactionDetails.category.description"), AWARD_DESCRIPTION(
                "transactionDetails.transactionDescription"), POINTS("units.amount"), USD_AMOUNT("hotelBillingAmount");

        private String name;

        private PostedPointAwardsGridCell(String name)
        {
            this.name = name;
        }

        @Override
        public String getName()
        {
            return name;
        }
    }
}
