package com.ihg.automation.selenium.hotel.pages.order;

import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.grid.Grid;

public class OrdersGrid extends Grid<OrdersGridRow, OrdersGridRow.OrdersGridCell>
{
    public OrdersGrid(Container parent)
    {
        super(parent);
    }
}
