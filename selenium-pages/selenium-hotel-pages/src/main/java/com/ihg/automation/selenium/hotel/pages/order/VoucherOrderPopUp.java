package com.ihg.automation.selenium.hotel.pages.order;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.isSelected;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;

public class VoucherOrderPopUp extends com.ihg.automation.selenium.common.voucher.VoucherOrderPopUp
{
    public VoucherOrderPopUp()
    {
        super();
    }

    public void verifyShippingInfoPaperFulfillment()
    {
        com.ihg.automation.selenium.hotel.pages.CustomerInfoPanel customerInfo = new com.ihg.automation.selenium.hotel.pages.CustomerInfoPanel();

        verifyThat(getName(), hasText(customerInfo.getFullName().getText()));
        verifyThat(getAddress(), hasText(customerInfo.getAddress().getText()));
    }

    public void verifyShippingInfoEmailFulfillment()
    {
        com.ihg.automation.selenium.hotel.pages.CustomerInfoPanel customerInfo = new com.ihg.automation.selenium.hotel.pages.CustomerInfoPanel();

        verifyThat(getName(), hasText(customerInfo.getFullName().getText()));
        verifyThat(getEmail(), hasText(customerInfo.getEmail().getText()));
        verifyThat(getSaveEmail(), isSelected(false));
        // TODO check email type and format
    }
}
