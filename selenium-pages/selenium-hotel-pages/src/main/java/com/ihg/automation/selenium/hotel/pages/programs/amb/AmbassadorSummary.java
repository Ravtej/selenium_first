package com.ihg.automation.selenium.hotel.pages.programs.amb;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static com.ihg.automation.selenium.matchers.text.EntityMatcherFactory.isValue;

import com.ihg.automation.selenium.common.member.AmbassadorUtils;
import com.ihg.automation.selenium.common.types.program.AmbassadorLevel;
import com.ihg.automation.selenium.gwt.components.Button;
import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.label.Label;
import com.ihg.automation.selenium.hotel.pages.programs.ProgramSummary;

public class AmbassadorSummary extends ProgramSummary
{

    public AmbassadorSummary(Container container)
    {
        super(container);
    }

    public Button getRenew()
    {
        return container.getButton("Renew");
    }

    public Label getTierLevel()
    {
        return container.getLabel("Tier-Level");
    }

    public Label getExpirationDate()
    {
        return container.getLabel("Expiration Date");
    }

    public void verify(String expirationDate, String status, AmbassadorLevel ambassadorLevel, String member)
    {
        verifySummaryBase(status, ambassadorLevel, member);
        verifyThat(getExpirationDate(), hasText(expirationDate));
    }

    public void verifySummaryAfterRenew(int daysShift, String status, AmbassadorLevel ambassadorLevel, String member)
    {
        verifySummaryBase(status, ambassadorLevel, member);
        verifyThat(getExpirationDate(),
                hasText(AmbassadorUtils.getExpirationDateAfterRenew(daysShift)));
    }

    private void verifySummaryBase(String status, AmbassadorLevel ambassadorLevel, String member)
    {
        verifyThat(getStatus(), hasText(status));
        verifyThat(getTierLevel(), hasText(isValue(ambassadorLevel)));
        verifyThat(getMemberId(), hasText(member));
    }
}
