package com.ihg.automation.selenium.hotel.pages.communication;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.ViewableMatcher.hasTextInView;
import static com.ihg.automation.selenium.matchers.text.EntityMatcherFactory.isValue;

import com.ihg.automation.selenium.common.communication.ContactPermissionItems;
import com.ihg.automation.selenium.common.communication.MarketingPreferences;
import com.ihg.automation.selenium.common.communication.SmsMobileTextingMarketingPreferences;
import com.ihg.automation.selenium.common.pages.MultiModeBase;
import com.ihg.automation.selenium.common.types.WrittenLanguage;
import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.input.Select;
import com.ihg.automation.selenium.gwt.components.label.SecurityLabel;

public class CommunicationPreferences extends MultiModeBase
{
    public CommunicationPreferences(Container container)
    {
        super(container);
    }

    public Select getPreferredLanguage()
    {
        return new SecurityLabel(container, "Preferred Language").getSelect();
    }

    public MarketingPreferences getMarketingPreferences()
    {
        return new MarketingPreferences(container);
    }

    public SmsMobileTextingMarketingPreferences getSmsMobileTextingMarketingPreferences()
    {
        return new SmsMobileTextingMarketingPreferences(container);
    }

    public void verify(WrittenLanguage written)
    {
        verifyThat(getPreferredLanguage(), hasTextInView(isValue(written)));
    }

    public void verifyStatus(String mailStatus, String smsStatus)
    {
        CommunicationPreferencesPage commPrefPage = new CommunicationPreferencesPage();

        commPrefPage.goTo();

        CommunicationPreferences commPrefs = commPrefPage.getCommunicationPreferences();

        ContactPermissionItems marketingPermissionItems = commPrefs.getMarketingPreferences()
                .getContactPermissionItems();

        for (int index = 1; index <= marketingPermissionItems.getContactPermissionItemsCount(); index++)
        {
            verifyThat(marketingPermissionItems.getContactPermissionItem(index).getSubscribe(),
                    hasTextInView(mailStatus));
        }

        ContactPermissionItems smsPermissionItems = commPrefs.getSmsMobileTextingMarketingPreferences()
                .getContactPermissionItems();
        for (int index = 1; index <= smsPermissionItems.getContactPermissionItemsCount(); index++)
        {
            verifyThat(smsPermissionItems.getContactPermissionItem(index).getSubscribe(), hasTextInView(smsStatus));
        }
    }
}
