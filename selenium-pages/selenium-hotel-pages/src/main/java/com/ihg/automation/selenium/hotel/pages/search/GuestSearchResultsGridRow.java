package com.ihg.automation.selenium.hotel.pages.search;

import com.ihg.automation.selenium.gwt.components.grid.CellsName;
import com.ihg.automation.selenium.gwt.components.grid.GridRow;
import com.ihg.automation.selenium.gwt.components.grid.GridRowXpath;

public class GuestSearchResultsGridRow extends GridRow<GuestSearchResultsGridRow.GuestSearchCell>
{
    public GuestSearchResultsGridRow(GuestSearchResultsGrid parent, GridRowXpath xpath)
    {
        super(parent, xpath);
    }

    public enum GuestSearchCell implements CellsName
    {
        GUEST_NAME("name"), MBR_NUMBER("membership.memberId"), POINTS("membership.balance"), LEVEL(
                "membership.levelCode"), ADDRESS("address.addressLine"), CITY("address.locality1"), STATE(
                        "address.region1"), ZIP("address.postalCode"), COUNTRY("address.countryCode"), PHONE("phone");

        private String name;

        private GuestSearchCell(String name)
        {
            this.name = name;
        }

        @Override
        public String getName()
        {
            return name;
        }
    }
}
