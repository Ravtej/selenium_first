package com.ihg.automation.selenium.hotel.pages.hoteloperations.awards;

import static com.ihg.automation.common.CustomStringUtils.concat;
import static com.ihg.automation.selenium.common.Constant.NOT_AVAILABLE;
import static org.apache.commons.lang3.StringUtils.EMPTY;

import com.ihg.automation.selenium.common.SiteHelperBase;
import com.ihg.automation.selenium.common.UI;
import com.ihg.automation.selenium.common.event.Source;
import com.ihg.automation.selenium.common.member.Member;

public abstract class PointAwardBase
{
    private String memberNumber;
    private String memberName;
    private String checkInDate = EMPTY;
    private String checkOutDate = EMPTY;
    private PointAward transaction;
    private String points;
    private String reason;
    private String comments;

    private Source source;

    public String getMemberNumber()
    {
        return memberNumber;
    }

    public void setMemberNumber(String memberNumber)
    {
        this.memberNumber = memberNumber;
    }

    public String getMemberName()
    {
        return memberName;
    }

    public void setMemberName(Member member)
    {
        this.memberName = member.getName().getShortName();
    }

    protected void setMemberName(String memberName)
    {
        this.memberName = memberName;
    }

    public String getCheckInDate()
    {
        return checkInDate;
    }

    public void setCheckInDate(String checkInDate)
    {
        this.checkInDate = checkInDate;
    }

    public String getCheckOutDate()
    {
        return checkOutDate;
    }

    public void setCheckOutDate(String checkOutDate)
    {
        this.checkOutDate = checkOutDate;
    }

    public PointAward getTransaction()
    {
        return transaction;
    }

    public void setTransaction(PointAward transaction)
    {
        this.transaction = transaction;
    }

    public String getPoints()
    {
        return points;
    }

    public void setPoints(String points)
    {
        this.points = points;
    }

    public String getReason()
    {
        return reason;
    }

    public void setReason(String reason)
    {
        this.reason = reason;
    }

    public String getComments()
    {
        return comments;
    }

    public void setComments(String comments)
    {
        this.comments = comments;
    }

    public Source getSource()
    {
        return source;
    }

    protected void setSource(Source source)
    {
        this.source = source;
    }

    public void setSource(SiteHelperBase helper, String hotel)
    {
        this.source = new Source(UI.LC.getChannel(), hotel, "HOTEL", NOT_AVAILABLE, helper.getUser().getNameFull());
    }

    public String getDescription()
    {
        return concat(" ", getTransaction().getType(), getPoints(), "Points");
    }

}
