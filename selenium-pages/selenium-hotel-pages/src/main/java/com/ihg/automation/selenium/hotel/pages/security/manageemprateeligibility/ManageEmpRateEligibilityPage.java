package com.ihg.automation.selenium.hotel.pages.security.manageemprateeligibility;

import com.ihg.automation.selenium.common.event.SearchAware;
import com.ihg.automation.selenium.common.event.SearchButtonBar;
import com.ihg.automation.selenium.gwt.components.Button;
import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.CustomContainerBase;
import com.ihg.automation.selenium.gwt.components.input.DateInput;
import com.ihg.automation.selenium.gwt.components.input.Input;
import com.ihg.automation.selenium.gwt.components.input.Select;
import com.ihg.automation.selenium.gwt.components.panel.Panel;

public class ManageEmpRateEligibilityPage extends CustomContainerBase implements SearchAware
{
    public ManageEmpRateEligibilityPage()
    {
        super(new Panel("Employee Rate Eligibility"));
    }

    @Override
    public ManageEmpRateEligibilitySearch getSearchFields()
    {
        return new ManageEmpRateEligibilitySearch(container);
    }

    @Override
    public EmployeeSearchButtonBar getButtonBar()
    {
        return new EmployeeSearchButtonBar(this);
    }

    public ManageEmpRateEligibilityGrid getEmployeeRateEligibilityGrid()
    {
        return new ManageEmpRateEligibilityGrid(container);
    }

    public void searchMember(EmployeeRateEligibilityRow row)
    {
        getButtonBar().clickClearCriteria();
        getSearchFields().getMemberNumber().typeAndWait(row.getMemberId());
        getButtonBar().getSearch().clickAndWait();
    }

    public class ManageEmpRateEligibilitySearch extends CustomContainerBase
    {
        public ManageEmpRateEligibilitySearch(Container container)
        {
            super(container);
        }

        public Select getHotel()
        {
            return container.getSelectByLabel("Hotel");
        }

        public DateInput getDateUpdatedFrom()
        {
            return new DateInput(container.getLabel("Date Updated From"));
        }

        public DateInput getDateUpdatedTo()
        {
            return new DateInput(container.getLabel("To"));
        }

        public Input getMemberNumber()
        {
            return container.getInputByLabel("Member Number");
        }

    }

    public class EmployeeSearchButtonBar extends SearchButtonBar
    {
        public EmployeeSearchButtonBar(ManageEmpRateEligibilityPage page)
        {
            super(page.container);
        }

        public Button getAddNew()
        {
            return container.getButton("Add New");
        }
    }
}
