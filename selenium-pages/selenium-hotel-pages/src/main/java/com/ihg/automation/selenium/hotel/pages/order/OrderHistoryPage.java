package com.ihg.automation.selenium.hotel.pages.order;

import com.ihg.automation.selenium.common.event.SearchAware;
import com.ihg.automation.selenium.common.event.SearchButtonBar;
import com.ihg.automation.selenium.common.order.VoucherOrder;
import com.ihg.automation.selenium.common.pages.TabCustomContainerBase;
import com.ihg.automation.selenium.hotel.pages.Tabs;

public class OrderHistoryPage extends TabCustomContainerBase implements SearchAware
{
    public OrderHistoryPage()
    {
        super(new Tabs().getEventsTab().getOrderHistory());
    }

    @Override
    public OrdersSearch getSearchFields()
    {
        return new OrdersSearch(container);
    }

    @Override
    public SearchButtonBar getButtonBar()
    {
        return new SearchButtonBar(container);
    }

    public OrdersGrid getOrdersGrid()
    {
        return new OrdersGrid(container.getPanel("Orders"));
    }

    public void searchOrder(VoucherOrder voucherOrder)
    {
        getSearchFields().getItemId().type(voucherOrder.getPromotionId());
        getSearchFields().getItemName().type(voucherOrder.getVoucherName());
        getSearchFields().getStatus().select(voucherOrder.getStatus());
        getButtonBar().clickSearch();
    }
}
