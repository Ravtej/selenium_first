package com.ihg.automation.selenium.hotel.pages.hoteloperations.reimbursement.rewardnight;

import com.ihg.automation.selenium.common.pages.TabCustomContainerBase;
import com.ihg.automation.selenium.gwt.components.input.Select;
import com.ihg.automation.selenium.hotel.pages.Tabs;

public class RewardNightsSettingsPage extends TabCustomContainerBase
{
    public RewardNightsSettingsPage()
    {
        super(new Tabs().getReimbursement().getRewardNightsSettings());
    }

    public Select getHotelCode()
    {
        return container.getSelect("Hotel Code");
    }

    public LowOccupancyReimbursementAmount getLowOccupancyReimbursementAmount()
    {
        return new LowOccupancyReimbursementAmount(container);
    }

    public TaxesAndFees getTaxesAndFees()
    {
        return new TaxesAndFees(container);
    }

    public RewardAndFreeNightsCount getRewardAndFreeNightsCount()
    {
        return new RewardAndFreeNightsCount(container);
    }

}
