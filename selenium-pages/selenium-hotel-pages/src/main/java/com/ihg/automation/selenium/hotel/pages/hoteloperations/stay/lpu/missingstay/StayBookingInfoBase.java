package com.ihg.automation.selenium.hotel.pages.hoteloperations.stay.lpu.missingstay;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;

import com.ihg.automation.selenium.common.pages.Verify;
import com.ihg.automation.selenium.common.stay.BookingDetails;
import com.ihg.automation.selenium.common.stay.Stay;
import com.ihg.automation.selenium.gwt.components.Component;
import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.CustomContainerBase;
import com.ihg.automation.selenium.gwt.components.label.Label;

public abstract class StayBookingInfoBase extends CustomContainerBase implements Verify<Stay>
{
    protected final static String BOOKING_DATE_LABEL = "Booking Date";
    protected final static String BOOKING_SOURCE_LABEL = "Booking Source";
    protected final static String DATA_SOURCE_LABEL = "Data Source";

    public StayBookingInfoBase(Container container)
    {
        super(container);
    }

    public Label getBookingDate()
    {
        return container.getLabel(BOOKING_DATE_LABEL);
    }

    public Label getDataSource()
    {
        return container.getLabel(DATA_SOURCE_LABEL);
    }

    public abstract Component getBookingSource();

    @Override
    public void verify(Stay stay)
    {
        BookingDetails details = stay.getBookingDetails();

        verifyThat(getBookingDate(), hasText(details.getBookingDate()));
        verifyThat(getBookingSource(), hasText(details.getBookingSource()));
        verifyThat(getDataSource(), hasText("Booking"));
    }
}
