package com.ihg.automation.selenium.hotel.pages.security.security;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.hotel.pages.Role.ALL_ROLES;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.isSelected;

import java.util.ArrayList;
import java.util.List;

import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.CustomContainerBase;
import com.ihg.automation.selenium.gwt.components.checkbox.CheckBox;
import com.ihg.automation.selenium.hotel.pages.Role;

public class UserManagementRoleCheckBoxGroup extends CustomContainerBase
{
    public UserManagementRoleCheckBoxGroup(Container container)
    {
        super(container);
    }

    public CheckBox getHotelBackOffice()
    {
        return container.getCheckBox(Role.HOTEL_BACK_OFFICE);
    }

    public CheckBox getHotelFrontDeskStandard()
    {
        return container.getCheckBox(Role.HOTEL_FRONT_DESK_STANDARD);
    }

    public CheckBox getSalesManager()
    {
        return container.getCheckBox(Role.SALES_MANAGER);
    }

    public CheckBox getHotelManager()
    {
        return container.getCheckBox(Role.HOTEL_MANAGER);
    }

    public CheckBox getHotelOperationsManager()
    {
        return container.getCheckBox(Role.HOTEL_OPERATIONS_MANAGER);
    }

    public CheckBox getHotelSecurity()
    {
        return container.getCheckBox(Role.HOTEL_SECURITY);
    }

    public void verifyUserRoles(List<String> activeUserRoles)
    {
        activeUserRoles.forEach(role -> verifyThat(container.getCheckBox(role), isSelected(true)));

        List<String> notActiveRoles = new ArrayList<>(ALL_ROLES);
        notActiveRoles.removeAll(activeUserRoles);

        notActiveRoles.forEach(role -> verifyThat(container.getCheckBox(role), isSelected(false)));
    }

    public void checkAllRoles()
    {
        getHotelBackOffice().check();
        getHotelFrontDeskStandard().check();
        getSalesManager().check();
        getHotelManager().check();
        getHotelOperationsManager().check();
        getHotelSecurity().check();
    }
}
