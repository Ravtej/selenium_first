package com.ihg.automation.selenium.hotel.pages.programs.dr;

import static com.ihg.automation.common.DateUtils.EXPIRATION_DATE_PATTERN;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasTextWithWait;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;

import com.ihg.automation.common.DateUtils;
import com.ihg.automation.selenium.common.catalog.CatalogItem;
import com.ihg.automation.selenium.gwt.components.Button;
import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.Link;
import com.ihg.automation.selenium.gwt.components.label.Label;
import com.ihg.automation.selenium.hotel.pages.programs.ProgramSummary;

public class DiningRewardsSummary extends ProgramSummary
{

    public DiningRewardsSummary(Container container)
    {
        super(container);
    }

    public Button getRenew()
    {
        return container.getButton("Renew");
    }

    public Label getExpirationDate()
    {
        return container.getLabel("Expiration Date");
    }

    public Link getPackage()
    {
        return new Link(container.getLabel("Package"), Link.LINK_XPATH_NO_HYPER_LINK_IN_CLASS);
    }

    public void verify(int monthsShift, String status, String member, CatalogItem catalogItem)
    {
        verifyThat(getExpirationDate(), hasText(DateUtils.getMonthsForward(monthsShift, EXPIRATION_DATE_PATTERN)));
        verifyThat(getStatus(), hasText(status));
        verifyThat(getMemberId(), hasText(member));
        verifyThat(getPackage(), hasTextWithWait(catalogItem.getItemName()));
    }
}
