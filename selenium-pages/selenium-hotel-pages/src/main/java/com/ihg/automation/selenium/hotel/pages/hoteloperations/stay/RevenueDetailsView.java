package com.ihg.automation.selenium.hotel.pages.hoteloperations.stay;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasTextAsDouble;

import com.ihg.automation.selenium.common.stay.Revenues;
import com.ihg.automation.selenium.common.stay.Stay;
import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.label.Label;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.stay.RevenueDetailsBase;

public class RevenueDetailsView extends RevenueDetailsBase
{
    public RevenueDetailsView(Container container)
    {
        super(container);
    }

    @Override
    public Label getAvgRoomRate()
    {
        return container.getLabel(AVERAGE_ROOM_RATE);
    }

    @Override
    public Label getTotalRoom()
    {
        return container.getLabel(TOTAL_ROOM_LABEL);
    }

    @Override
    public Label getFood()
    {
        return container.getLabel(FOOD_LABEL);
    }

    @Override
    public Label getBeverage()
    {
        return container.getLabel(BEVERAGE_LABEL);
    }

    @Override
    public Label getPhone()
    {
        return container.getLabel(PHONE_LABEL);
    }

    @Override
    public Label getMeeting()
    {
        return container.getLabel(MEETING_LABEL);
    }

    @Override
    public Label getMandatoryRevenue()
    {
        return container.getLabel(MANDATORY_REVENUE_LABEL);
    }

    @Override
    public Label getOtherRevenue()
    {
        return container.getLabel(OTHER_REVENUE_LABEL);
    }

    @Override
    public Label getNoRevenue()
    {
        return container.getLabel(NO_REVENUE_LABEL);
    }

    @Override
    public Label getRoomTax()
    {
        return container.getLabel(ROOM_TAX_LABEL);
    }

    @Override
    public Label getTotalRevenue()
    {
        return container.getLabel(TOTAL_REVENUE_LABEL);
    }

    @Override
    public Label getTotalNonQualifyingRevenue()
    {
        return container.getLabel(TOTAL_NON_QUALIFYING_REVENUE_LABEL);
    }

    @Override
    public Label getTotalQualifyingRevenue()
    {
        return container.getLabel(TOTAL_QUALIFYING_REVENUE_LABEL);
    }

    @Override
    public Label getNights()
    {
        return container.getLabel(NIGHTS_LABEL);
    }

    public void verify(Stay stay)
    {
        verifyThat(getAvgRoomRate(), hasTextAsDouble(stay.getAvgRoomRate()));
        Revenues revenues = stay.getRevenues();
        verifyThat(getTotalRoom(), hasTextAsDouble(revenues.getRoom().getAmount()));
        verifyThat(getFood(), hasTextAsDouble(revenues.getFood().getAmount()));
        verifyThat(getBeverage(), hasTextAsDouble(revenues.getBeverage().getAmount()));
        verifyThat(getPhone(), hasTextAsDouble(revenues.getPhone().getAmount()));
        verifyThat(getMeeting(), hasTextAsDouble(revenues.getMeeting().getAmount()));
        verifyThat(getMandatoryRevenue(), hasTextAsDouble(revenues.getMandatoryLoyalty().getAmount()));
        verifyThat(getOtherRevenue(), hasTextAsDouble(revenues.getOtherLoyalty().getAmount()));
        verifyThat(getNoRevenue(), hasTextAsDouble(revenues.getNoLoyalty().getAmount()));
        verifyThat(getRoomTax(), hasTextAsDouble(revenues.getTax().getAmount()));
        verifyThat(getTotalRevenue(), hasTextAsDouble(revenues.getOverallTotal().getAmount()));
        verifyThat(getTotalNonQualifyingRevenue(), hasTextAsDouble(revenues.getTotalNonQualifying().getAmount()));
        verifyThat(getTotalQualifyingRevenue(), hasTextAsDouble(revenues.getTotalQualifying().getAmount()));
    }
}
