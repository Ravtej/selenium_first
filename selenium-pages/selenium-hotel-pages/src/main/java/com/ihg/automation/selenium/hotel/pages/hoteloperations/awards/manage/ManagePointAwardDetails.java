package com.ihg.automation.selenium.hotel.pages.hoteloperations.awards.manage;

import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.CustomContainerBase;
import com.ihg.automation.selenium.gwt.components.label.Label;

public class ManagePointAwardDetails extends CustomContainerBase
{
    public ManagePointAwardDetails(Container container)
    {
        super(container);
    }

    public Label getConfirmationNumber()
    {
        return container.getLabel("Confirmation Number");
    }

    public Label getRateCode()
    {
        return container.getLabel("Rate Code");
    }

    public Label getRoomRate()
    {
        return container.getLabel("Room Rate");
    }

    public Label getHotelCurrency()
    {
        return container.getLabel("Hotel Currency");
    }

    public Label getCorpAcctNumber()
    {
        return container.getLabel("Corp Acct Number");
    }

    public Label getIATANumber()
    {
        return container.getLabel("IATA Number");
    }

    public Label getBookingDate()
    {
        return container.getLabel("Booking Date");
    }

    public Label getBookingSource()
    {
        return container.getLabel("Booking Source");
    }

    public Label getTransactionID()
    {
        return container.getLabel("Transaction ID");
    }

    public Label getTransactionName()
    {
        return container.getLabel("Transaction Name");
    }

    public Label getTransactionDescription()
    {
        return container.getLabel("Transaction Description");
    }
}
