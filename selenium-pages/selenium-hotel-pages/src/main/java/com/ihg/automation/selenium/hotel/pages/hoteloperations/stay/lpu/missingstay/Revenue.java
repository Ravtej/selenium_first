package com.ihg.automation.selenium.hotel.pages.hoteloperations.stay.lpu.missingstay;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.gwt.components.ComponentColumnPosition.FOLLOWING_TD;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasTextAsDouble;
import static com.ihg.automation.selenium.matchers.component.ViewableMatcher.hasText;

import com.ihg.automation.selenium.common.pages.LabelContainerBase;
import com.ihg.automation.selenium.formatter.Converter;
import com.ihg.automation.selenium.gwt.components.Component;
import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.Mode;
import com.ihg.automation.selenium.gwt.components.input.Input;

public class Revenue extends LabelContainerBase
{
    public Revenue(Container container, String labelText)
    {
        super(container.getLabel(labelText));
    }

    public Component getCreateAmount()
    {
        return label.getComponent("Local Amount (create)", 1, FOLLOWING_TD);
    }

    public Input getAmount()
    {
        Component component = label.getComponent("Local Amount (create)", 2, FOLLOWING_TD);
        return new Input(component, "Local Amount (create)");
    }

    @Override
    public String toString()
    {
        return label.getDescription();
    }

    public void verifyCreateAmount(com.ihg.automation.selenium.common.stay.Revenue revenue)
    {
        verifyThat(getCreateAmount(), hasTextAsDouble(revenue.getAmount()));
    }

    public void verifyAdjustAmount(com.ihg.automation.selenium.common.stay.Revenue revenue, Mode mode)
    {
        if (revenue == null)
        {
            return;
        }
        verifyThat(getAmount(), hasText(Converter.doubleToString(revenue.getAmount()), mode));
    }
}
