package com.ihg.automation.selenium.hotel.pages.hoteloperations.stay;

import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.label.Label;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.stay.lpu.missingstay.StayBookingInfoBase;

public class StayBookingInfoView extends StayBookingInfoBase
{
    public StayBookingInfoView(Container container)
    {
        super(container);
    }

    @Override
    public Label getBookingSource()
    {
        return container.getLabel(BOOKING_SOURCE_LABEL);
    }
}
