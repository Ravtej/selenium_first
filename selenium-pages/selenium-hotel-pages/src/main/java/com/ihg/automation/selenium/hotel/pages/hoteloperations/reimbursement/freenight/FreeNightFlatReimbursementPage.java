package com.ihg.automation.selenium.hotel.pages.hoteloperations.reimbursement.freenight;

import com.ihg.automation.selenium.common.event.SearchAware;
import com.ihg.automation.selenium.common.event.SearchButtonBar;
import com.ihg.automation.selenium.common.pages.TabCustomContainerBase;
import com.ihg.automation.selenium.hotel.pages.Tabs;

public class FreeNightFlatReimbursementPage extends TabCustomContainerBase implements SearchAware
{
    public FreeNightFlatReimbursementPage()
    {
        super(new Tabs().getReimbursement().getFreeNightFlatReimbursement());
    }

    @Override
    public SearchButtonBar getButtonBar()
    {
        return new SearchButtonBar(container);
    }

    @Override
    public FreeNightFlatReimbursementSearch getSearchFields()
    {
        return new FreeNightFlatReimbursementSearch(container);
    }

    public FreeNightFlatReimbursementGrid getFreeNightFlatReimbursementGrid()
    {
        return new FreeNightFlatReimbursementGrid(container);
    }

}
