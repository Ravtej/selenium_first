package com.ihg.automation.selenium.hotel.pages.hoteloperations.awards.manage;

import static com.ihg.automation.selenium.gwt.components.ComponentColumnPosition.TABLE;

import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.CustomContainerBase;
import com.ihg.automation.selenium.gwt.components.input.Input;
import com.ihg.automation.selenium.gwt.components.input.InputXpath;
import com.ihg.automation.selenium.gwt.components.panel.FieldSet;

public class ResultsFilter extends CustomContainerBase
{
    public ResultsFilter(Container container)
    {
        super(container.getFieldSet("Results Filter"));
    }

    public void expand()
    {
        ((FieldSet) container).expand();
    }

    public Input getConfirmation()
    {
        return new Input(container, new InputXpath.InputXpathBuilder().row(1).column(1, TABLE).build(),
                "Confirmation#");
    }

    public Input getIata()
    {
        return new Input(container, new InputXpath.InputXpathBuilder().row(2).column(1, TABLE).build(), "IATA#");
    }

    public Input getRateCategory()
    {
        return new Input(container, new InputXpath.InputXpathBuilder().row(1).column(2, TABLE).build(),
                "Rate Category");
    }

    public Input getCorporateId()
    {
        return new Input(container, new InputXpath.InputXpathBuilder().row(2).column(2, TABLE).build(),
                "Corporate ID");
    }
}
