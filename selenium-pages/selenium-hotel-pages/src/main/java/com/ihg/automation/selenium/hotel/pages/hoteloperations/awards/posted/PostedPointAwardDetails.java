package com.ihg.automation.selenium.hotel.pages.hoteloperations.awards.posted;

import static com.ihg.automation.common.CustomStringUtils.concat;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;

import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.CustomContainerBase;
import com.ihg.automation.selenium.gwt.components.label.Label;
import com.ihg.automation.selenium.gwt.components.label.SecurityLabel;
import com.ihg.automation.selenium.gwt.components.utils.ByText;

public class PostedPointAwardDetails extends CustomContainerBase
{
    protected static final String LABEL_PATTERN = ".//td[*[div[child::*[{0}]]]]";

    public PostedPointAwardDetails(Container container)
    {
        super(container);
    }

    public Label getCheckInDate()
    {
        return container.getLabel("Check In Date");
    }

    public Label getCheckOutDate()
    {
        return container.getLabel("Check Out Date");
    }

    public Label getTransactionID()
    {
        return container.getLabel("Transaction ID");
    }

    public Label getTransactionName()
    {
        return container.getLabel("Transaction Name");
    }

    public Label getTransactionDescription()
    {
        return container.getLabel("Transaction Description");
    }

    public Label getHotel()
    {
        return container.getLabel("Hotel");
    }

    public Label getCostOfPoints()
    {
        return new SecurityLabel(container, "Cost of Points");
    }

    public Label getReason()
    {
        return new Label(container, ByText.exact("Reason"), LABEL_PATTERN);
    }

    public Label getComments()
    {
        return new Label(container, ByText.exact("Comments"), LABEL_PATTERN);
    }

    public void verify(PostedPointAward pointAward)
    {
        verifyThat(getCheckInDate(), hasText(pointAward.getCheckInDate()));
        verifyThat(getCheckOutDate(), hasText(pointAward.getCheckOutDate()));
        verifyThat(getTransactionID(), hasText(pointAward.getTransaction().getId()));
        verifyThat(getTransactionName(), hasText(pointAward.getTransaction().getName()));
        verifyThat(getTransactionDescription(), hasText(pointAward.getDescription()));
        verifyThat(getHotel(), hasText(pointAward.getHotel()));
        verifyThat(getCostOfPoints(), hasText(concat(" ", pointAward.getCostOfPoints(), "USD")));
        verifyThat(getReason(), hasText(pointAward.getReason()));
        verifyThat(getComments(), hasText(pointAward.getComments()));
    }
}
