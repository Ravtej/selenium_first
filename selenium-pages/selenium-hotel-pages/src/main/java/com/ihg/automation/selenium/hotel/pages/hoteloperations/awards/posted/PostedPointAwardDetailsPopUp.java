package com.ihg.automation.selenium.hotel.pages.hoteloperations.awards.posted;

import static com.ihg.automation.selenium.gwt.components.Button.CANCEL;

import com.ihg.automation.selenium.common.history.EventHistoryTab;
import com.ihg.automation.selenium.common.history.EventHistoryTabAware;
import com.ihg.automation.selenium.common.pages.PopUpBase;
import com.ihg.automation.selenium.gwt.components.Button;
import com.ihg.automation.selenium.gwt.components.PopUpWindow;

public class PostedPointAwardDetailsPopUp extends PopUpBase implements EventHistoryTabAware
{
    public PostedPointAwardDetailsPopUp()
    {
        super(PopUpWindow.withoutHeader("Posted Point Award Details"));
    }

    public Button getCancel()
    {
        return container.getButton(CANCEL);
    }

    public void clickCancel()
    {
        getCancel().clickAndWait();
    }

    public PostedPointAwardDetailsTab getPostedPointAwardDetailsTab()
    {
        return new PostedPointAwardDetailsTab(this);
    }

    @Override
    public EventHistoryTab getHistoryTab()
    {
        return new EventHistoryTab(this, "History");
    }
}
