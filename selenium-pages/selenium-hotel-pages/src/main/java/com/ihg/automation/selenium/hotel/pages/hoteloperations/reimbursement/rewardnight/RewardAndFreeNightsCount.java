package com.ihg.automation.selenium.hotel.pages.hoteloperations.reimbursement.rewardnight;

import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.CustomContainerBase;

public class RewardAndFreeNightsCount extends CustomContainerBase
{
    public RewardAndFreeNightsCount(Container container)
    {
        super(container.getFieldSet("Reward and Free Nights Count"));
    }

    public RewardAndFreeNightsCountGrid getRewardAndFreeNightsCountGrid()
    {
        return new RewardAndFreeNightsCountGrid(container);
    }
}
