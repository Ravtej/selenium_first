package com.ihg.automation.selenium.hotel.pages.hoteloperations.stay.stays;

import com.ihg.automation.selenium.common.history.EventHistoryTab;
import com.ihg.automation.selenium.common.pages.ClosablePopUpBase;

public class StayDetailsPopUp extends ClosablePopUpBase
{
    public StayDetailsPopUp()
    {
        super();
    }

    public ViewStayDetailsTab getViewStayDetailsTab()
    {
        return new ViewStayDetailsTab(this);
    }

    public EventHistoryTab getHistoryTab()
    {
        return new EventHistoryTab(this, "Stay History");
    }
}
