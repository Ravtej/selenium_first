package com.ihg.automation.selenium.hotel.pages.security.security;

import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.CustomContainerBase;
import com.ihg.automation.selenium.gwt.components.input.Input;
import com.ihg.automation.selenium.gwt.components.input.Select;

public class UserManagementSearch extends CustomContainerBase
{
    public UserManagementSearch(Container container)
    {
        super(container);
    }

    public Select getHotel()
    {
        return container.getSelectByLabel("Hotel");
    }

    public Input getFirstName()
    {
        return container.getInputByLabel("First Name");
    }

    public Input getLastName()
    {
        return container.getInputByLabel("Last Name");
    }

    public Input getJobTitle()
    {
        return container.getInputByLabel("Job Title");
    }

    public Select getRole()
    {
        return container.getSelectByLabel("Role");
    }

    public void populate(UserManagement userManagement, String role)
    {
        getHotel().select(userManagement.getHotel());
        getFirstName().type(userManagement.getFirstName());
        getLastName().type(userManagement.getLastName());
        getJobTitle().type(userManagement.getJobTitle());
        getRole().select(role);
    }
}
