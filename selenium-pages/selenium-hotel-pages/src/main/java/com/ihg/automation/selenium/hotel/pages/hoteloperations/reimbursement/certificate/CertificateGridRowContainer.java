package com.ihg.automation.selenium.hotel.pages.hoteloperations.reimbursement.certificate;

import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.CustomContainerBase;
import com.ihg.automation.selenium.gwt.components.label.Label;
import com.ihg.automation.selenium.gwt.components.label.SecurityLabel;

public class CertificateGridRowContainer extends CustomContainerBase
{
    public CertificateGridRowContainer(Container container)
    {
        super(container);
    }

    public Label getConfirmationNumber()
    {
        return new SecurityLabel(container, "Confirmation Number");
    }

    public Label getMemberID()
    {
        return container.getLabel("Member Id");
    }

    public Label getLastName()
    {
        return container.getLabel("Last Name");
    }

    public Label getCertificateType()
    {
        return container.getLabel("Certificate Type");
    }

    public Label getCertificateNumber()
    {
        return container.getLabel("Certificate Number");
    }

    public Label getSuffix()
    {
        return container.getLabel("Suffix");
    }

    public Label getStatus()
    {
        return container.getLabel("Status");
    }

    public Label getOfferName()
    {
        return container.getLabel("Offer Name");
    }

    public Label getCheckIn()
    {
        return container.getLabel("Check In");
    }

    public Label getCheckOut()
    {
        return container.getLabel("Check Out");
    }

    public Label getReimbursementType()
    {
        return container.getLabel("Reimbursement Type");
    }

    public Label getReimbursementMethod()
    {
        return container.getLabel("Reimbursement Method");
    }

    public Label getReimbursementDate()
    {
        return container.getLabel("Reimbursement Date");
    }

    public Label getTotalActiveNights()
    {
        return container.getLabel("Total Active Nights");
    }

    public Label getTotalReimbursed()
    {
        return container.getLabel("Total Reimbursed");
    }

}
