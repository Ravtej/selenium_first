package com.ihg.automation.selenium.hotel.pages.hoteloperations.reimbursement.rewardnight;

import com.ihg.automation.selenium.gwt.components.grid.CellsName;
import com.ihg.automation.selenium.gwt.components.grid.GridRow;
import com.ihg.automation.selenium.gwt.components.grid.GridRowXpath;

public class TaxesAndFeesGridRow extends GridRow<TaxesAndFeesGridRow.TaxesAndFeesGridCell>
{
    public TaxesAndFeesGridRow(TaxesAndFeesGrid parent, GridRowXpath xpath)
    {
        super(parent, xpath);
    }

    public enum TaxesAndFeesGridCell implements CellsName
    {
        CHECK_OUT_BEGINNING_DATE("effectiveDate"), PERCENT("taxPercentage"), FEE("flatFeeAmount "), CURRENCY(
                "currencyCode");

        private String name;

        private TaxesAndFeesGridCell(String name)
        {
            this.name = name;
        }

        @Override
        public String getName()
        {
            return name;
        }
    }
}
