package com.ihg.automation.selenium.hotel.pages;

import com.ihg.automation.selenium.common.LeftPanelBase;
import com.ihg.automation.selenium.gwt.components.Button;
import com.ihg.automation.selenium.gwt.components.utils.ByText;
import com.ihg.automation.selenium.listener.Verifier;

public class LeftPanel extends LeftPanelBase
{
    public Button getEnrollment()
    {
        return container.getButton("Enrollment");
    }

    public Button getGuestSearch()
    {
        return container.getButton("Guest Search");
    }

    public void clickGuestSearch(Verifier... verifiers)
    {
        getGuestSearch().clickAndWait(verifiers);
    }

    public Button getHotelOperations()
    {
        return container.getButton("Hotel Operations");
    }

    public void clickHotelOperations(Verifier... verifiers)
    {
        getHotelOperations().clickAndWait(verifiers);
    }

    public Button getManageEmpRateEligibility()
    {
        return container.getButton("Manage Emp Rate Eligibility");
    }

    public Button getOrderPointVoucher()
    {
        return container.getButton("Order Point Voucher");
    }

    public Button getHotelSecurity()
    {
        return container.getButton("Security");
    }

    @Override
    public Button getBackToResults()
    {
        return new Button(container, ByText.exact("Back to Results"));
    }
}
