package com.ihg.automation.selenium.hotel.pages;

import java.util.List;

import com.google.common.collect.ImmutableList;

public class Role
{
    private Role()
    {
    }

    public static final String ALL = "All";
    public static final String ANY_ROLE = "Any Role";
    public static final String NO_ROLE = "No Role";

    public static final String HOTEL_MANAGER = "HOTEL MANAGER";
    public static final String HOTEL_BACK_OFFICE = "HOTEL BACK OFFICE";
    public static final String HOTEL_FRONT_DESK_FEE_BASED = "HOTEL FRONT DESK - FEE BASED";
    public static final String HOTEL_FRONT_DESK_STANDARD = "HOTEL FRONT DESK - STANDARD";
    public static final String HOTEL_OPERATIONS_MANAGER = "HOTEL OPERATIONS MANAGER";
    public static final String HOTEL_SECURITY = "HOTEL SECURITY";
    public static final String SALES_MANAGER = "SALES MANAGER";

    public static final List<String> ALL_ROLES = ImmutableList.of(HOTEL_MANAGER, HOTEL_BACK_OFFICE,
            HOTEL_FRONT_DESK_STANDARD, HOTEL_OPERATIONS_MANAGER, HOTEL_SECURITY, SALES_MANAGER);
}
