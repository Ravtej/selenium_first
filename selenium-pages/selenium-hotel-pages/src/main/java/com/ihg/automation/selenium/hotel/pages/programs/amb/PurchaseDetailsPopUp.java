package com.ihg.automation.selenium.hotel.pages.programs.amb;

import com.ihg.automation.selenium.common.catalog.CatalogItem;
import com.ihg.automation.selenium.common.pages.SubmitPopUpBase;
import com.ihg.automation.selenium.gwt.components.input.Select;

public class PurchaseDetailsPopUp extends SubmitPopUpBase
{
    private static final String TOTAL_AMOUNT = "Total Amount";

    public PurchaseDetailsPopUp()
    {
        super("Purchase Details");
    }

    public Select getTotalAmount()
    {
        return container.getSelectByLabel(TOTAL_AMOUNT);
    }

    public void selectAmount(CatalogItem catalogItem)
    {
        getTotalAmount().select(catalogItem.getItemDescription());
    }
}
