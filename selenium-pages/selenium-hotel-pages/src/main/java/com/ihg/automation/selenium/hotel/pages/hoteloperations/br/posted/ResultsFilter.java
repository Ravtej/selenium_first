package com.ihg.automation.selenium.hotel.pages.hoteloperations.br.posted;

import static com.ihg.automation.selenium.gwt.components.ComponentColumnPosition.TABLE;

import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.CustomContainerBase;
import com.ihg.automation.selenium.gwt.components.input.Input;
import com.ihg.automation.selenium.gwt.components.input.InputXpath;
import com.ihg.automation.selenium.gwt.components.panel.FieldSet;

public class ResultsFilter extends CustomContainerBase
{
    public ResultsFilter(Container container)
    {
        super(container.getFieldSet("Results Filter"));
    }

    public void expand()
    {
        ((FieldSet) container).expand();
    }

    public Input getMemberName()
    {
        return new Input(container, new InputXpath.InputXpathBuilder().row(1).column(1, TABLE).build(), "Member Name");
    }

    public Input getMemberNumber()
    {
        return new Input(container, new InputXpath.InputXpathBuilder().row(1).column(2, TABLE).build(),
                "Member Number");
    }

    public Input getEventId()
    {
        return new Input(container, new InputXpath.InputXpathBuilder().row(1).column(3, TABLE).build(), "Event Id");
    }

    public Input getEventName()
    {
        return new Input(container, new InputXpath.InputXpathBuilder().row(1).column(4, TABLE).build(), "Event Name");
    }
}
