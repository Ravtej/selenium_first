package com.ihg.automation.selenium.hotel.pages.order;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.event.EventTransactionType.Order.ORDER_VOUCHER;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasText;

import com.ihg.automation.selenium.common.order.VoucherOrder;
import com.ihg.automation.selenium.gwt.components.grid.CellsName;
import com.ihg.automation.selenium.gwt.components.grid.GridRow;
import com.ihg.automation.selenium.gwt.components.grid.GridRowXpath;

public class OrdersGridRow extends GridRow<OrdersGridRow.OrdersGridCell>
{
    public OrdersGridRow(OrdersGrid parent, GridRowXpath xpath)
    {
        super(parent, xpath);
    }

    public enum OrdersGridCell implements CellsName
    {
        TRANS_DATE("eventDate"), TRANS_TYPE("transactionType"), ITEM_NAME("itemName"), STATUS(
                "status"), BILLING_AMT_USE("cash");

        private String name;

        private OrdersGridCell(String name)
        {
            this.name = name;
        }

        @Override
        public String getName()
        {
            return name;
        }
    }

    public void verify(VoucherOrder voucherOrder)
    {
        verifyThat(getCell(OrdersGridCell.TRANS_DATE), hasText(voucherOrder.getTransactionDate()));
        verifyThat(getCell(OrdersGridCell.TRANS_TYPE), hasText(ORDER_VOUCHER));
        verifyThat(getCell(OrdersGridCell.ITEM_NAME), hasText(voucherOrder.getVoucherName()));
        verifyThat(getCell(OrdersGridCell.STATUS), hasText(voucherOrder.getStatus()));
        verifyThat(getCell(OrdersGridCell.BILLING_AMT_USE), hasText(voucherOrder.getVoucherCostAmount()));
    }
}
