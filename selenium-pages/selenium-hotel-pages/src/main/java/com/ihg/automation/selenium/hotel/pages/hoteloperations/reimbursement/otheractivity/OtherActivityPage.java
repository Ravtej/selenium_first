package com.ihg.automation.selenium.hotel.pages.hoteloperations.reimbursement.otheractivity;

import com.ihg.automation.selenium.common.event.SearchAware;
import com.ihg.automation.selenium.common.event.SearchButtonBar;
import com.ihg.automation.selenium.common.pages.TabCustomContainerBase;
import com.ihg.automation.selenium.hotel.pages.Tabs;

public class OtherActivityPage extends TabCustomContainerBase implements SearchAware
{
    public OtherActivityPage()
    {
        super(new Tabs().getReimbursement().getOtherActivity());
    }

    @Override
    public SearchButtonBar getButtonBar()
    {
        return new SearchButtonBar(container);
    }

    @Override
    public OtherActivitySearch getSearchFields()
    {
        return new OtherActivitySearch(container);
    }

    public OtherActivityGrid getOtherActivityGrid()
    {
        return new OtherActivityGrid(container);
    }

}
