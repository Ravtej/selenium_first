package com.ihg.automation.selenium.hotel.pages.security.manageemprateeligibility;

import com.ihg.automation.selenium.common.pages.SavePopUpBase;
import com.ihg.automation.selenium.gwt.components.Component;
import com.ihg.automation.selenium.gwt.components.ComponentColumnPosition;
import com.ihg.automation.selenium.gwt.components.input.Input;

public class EmployeeRateEligibilityPopUpBase extends SavePopUpBase
{
    private static final ComponentColumnPosition POSITION = ComponentColumnPosition.FOLLOWING_TD;

    public EmployeeRateEligibilityPopUpBase()
    {
        super("Employee Rate Eligibility");
    }

    public Component getMemberName()
    {
        return container.getLabel("Member Number").getComponent("Member Number", 1, POSITION);
    }

    public Input getMemberNumber()
    {
        return container.getInputByLabel("Member Number");
    }
}
