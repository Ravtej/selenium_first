package com.ihg.automation.selenium.hotel.pages.personal;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.enroll.CustomerInfoCommonFieldsAware.GENDER_LABEL;
import static com.ihg.automation.selenium.gwt.components.utils.FindStepUtils.byXpathWithWait;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;

import com.ihg.automation.selenium.common.BirthDate;
import com.ihg.automation.selenium.common.components.Country;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.PersonalInfo;
import com.ihg.automation.selenium.common.name.PersonNameCustomerFields;
import com.ihg.automation.selenium.common.name.PersonNameFields;
import com.ihg.automation.selenium.common.pages.MultiModeBase;
import com.ihg.automation.selenium.common.pages.Populate;
import com.ihg.automation.selenium.common.pages.VerifyMultiMode;
import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.Mode;
import com.ihg.automation.selenium.gwt.components.input.Select;
import com.ihg.automation.selenium.gwt.components.label.Label;
import com.ihg.automation.selenium.listener.Verifier;
import com.ihg.automation.selenium.matchers.component.ViewableMatcher;
import com.ihg.automation.selenium.matchers.text.EntityMatcherFactory;

public class CustomerInfoFields extends MultiModeBase implements Populate<Member>, VerifyMultiMode<Member>
{
    private static final String TYPE = "Customer container";
    private static final String PATTERN = "./div/div[2]";

    public CustomerInfoFields(Container container)
    {
        super(container, TYPE, "Customer fields", byXpathWithWait(PATTERN));
    }

    @Override
    public void clickEdit(Verifier... verifiers)
    {
        getEdit().clickAndWait(verifiers);
    }

    public PersonNameFields getName()
    {
        return new PersonNameCustomerFields(container);
    }

    public Label getFullName()
    {
        return container.getLabel("Name");
    }

    public Select getGender()
    {
        return container.getSelectByLabel(GENDER_LABEL);
    }

    public BirthDate getBirthDate()
    {
        return new BirthDate(container);
    }

    public Label getResidenceCountry()
    {
        return container.getLabel("Country");
    }

    public Label getNativeLanguage()
    {
        return container.getLabel("Native Language");
    }

    @Override
    public void populate(Member member)
    {
        PersonNameFields name = getName();
        PersonalInfo personalInfo = member.getPersonalInfo();
        name.setConfiguration(personalInfo.getResidenceCountry());
        name.populate(personalInfo.getName());

        getGender().selectByValue(personalInfo.getGenderCode());
        getBirthDate().populate(personalInfo.getBirthDate());
    }

    @Override
    public void verify(Member member, Mode mode)
    {
        PersonalInfo personalInfo = member.getPersonalInfo();
        Country residenceCountry = personalInfo.getResidenceCountry();

        verifyThat(getResidenceCountry(), hasText(residenceCountry.getValue()));

        PersonNameFields name = getName();
        name.setConfiguration(residenceCountry);
        name.verify(personalInfo.getName(), mode);

        verifyThat(getGender(),
                ViewableMatcher.hasText(EntityMatcherFactory.isValue(personalInfo.getGenderCode()), mode));
        getBirthDate().verify(personalInfo.getBirthDate(), mode);
    }
}
