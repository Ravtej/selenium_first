package com.ihg.automation.selenium.hotel.pages.hoteloperations.stay.lpu.missingstay;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.gwt.components.utils.FindStepUtils.byXpath;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasEmptyOrNullText;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasTextAsDouble;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasTextAsInt;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static org.hamcrest.core.IsNot.not;

import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.stay.AdjustReasons;
import com.ihg.automation.selenium.common.stay.Stay;
import com.ihg.automation.selenium.formatter.Converter;
import com.ihg.automation.selenium.gwt.components.Component;
import com.ihg.automation.selenium.gwt.components.grid.RowEditor;
import com.ihg.automation.selenium.gwt.components.input.Input;
import com.ihg.automation.selenium.gwt.components.input.Select;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.stay.lpu.LoyaltyPendingUpdatesGridRow;

public class StayGridRowAdjustPanel extends RowEditor
{
    private static final String COMPONENT_XPATH_PATTERN = ".//div[@gxt-dindex='%s']";

    public StayGridRowAdjustPanel(LoyaltyPendingUpdatesGridRow row)
    {
        super(row);
    }

    public Component getGuestName()
    {
        return new Component(container, "Label", "Guest Name",
                byXpath(String.format(COMPONENT_XPATH_PATTERN, "personName")));
    }

    public Component getMemberNumber()
    {
        return new Component(container, "Label", "Mbr Number",
                byXpath(String.format(COMPONENT_XPATH_PATTERN, "loyaltyMembershipId")));
    }

    public Component getCheckInDate()
    {
        return new Component(container, "Label", "Check In",
                byXpath(String.format(COMPONENT_XPATH_PATTERN, "checkIn")));
    }

    public Component getNights()
    {
        return new Component(container, "Label", "Nights", byXpath(String.format(COMPONENT_XPATH_PATTERN, "nights")));
    }

    public Component getRoomNumber()
    {
        return new Component(container, "Label", "Room Number",
                byXpath(String.format(COMPONENT_XPATH_PATTERN, "roomId")));
    }

    public Component getRateCode()
    {
        return new Component(container, "Label", "Rate Code",
                byXpath(String.format(COMPONENT_XPATH_PATTERN, "rateCode")));
    }

    public Input getRoomRate()
    {
        return new Input(new Component(container, "Input", "Room Rate",
                byXpath(String.format(COMPONENT_XPATH_PATTERN, "averageRoomRate"))), "Room Rate");
    }

    public Select getQualifyingFlag()
    {
        return new Select(new Component(container, "Select", "Qualifying Flag",
                byXpath(String.format(COMPONENT_XPATH_PATTERN, "qualifyingFlag"))), "Qualifying Flag");
    }

    public Select getReason()
    {
        return new Select(new Component(container, "Select", "Reason",
                byXpath(String.format(COMPONENT_XPATH_PATTERN, "adjustReasonCode"))), "Reason");
    }

    public Component getTotalQualifying()
    {
        return new Component(container, "Label", "Total Qualifying",
                byXpath(String.format(COMPONENT_XPATH_PATTERN, "qualifyingRevenue")));
    }

    public void populate(Stay stay, AdjustReasons reasons)
    {
        getRoomRate().type(stay.getAvgRoomRate());
        getQualifyingFlag().select(Converter.booleanToString(stay.getIsQualifying()));
        getReason().selectByValue(reasons, false);
    }

    public void verify(Member member, Stay stay)
    {
        verifyThat(getGuestName(), not(hasEmptyOrNullText()));
        verifyThat(getMemberNumber(), hasText(member.getRCProgramId()));
        verifyThat(getCheckInDate(), hasText(stay.getCheckIn()));
        verifyThat(getNights(), hasTextAsInt(stay.getNights()));
        verifyThat(getRoomNumber(), hasText(stay.getRoomNumber()));
        verifyThat(getRateCode(), hasText(stay.getRateCode()));
        verifyThat(getRoomRate(), hasTextAsDouble(stay.getAvgRoomRate()));
        verifyThat(getQualifyingFlag(), hasText(Converter.booleanToString(stay.getIsQualifying())));
        verifyThat(getTotalQualifying(), hasTextAsDouble(stay.getRevenues().getTotalQualifying().getAmount()));
    }
}
