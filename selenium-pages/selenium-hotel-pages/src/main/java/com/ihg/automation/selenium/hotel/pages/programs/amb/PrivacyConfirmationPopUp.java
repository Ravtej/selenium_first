package com.ihg.automation.selenium.hotel.pages.programs.amb;

import com.ihg.automation.selenium.common.pages.PopUpBase;
import com.ihg.automation.selenium.gwt.components.Button;
import com.ihg.automation.selenium.gwt.components.Component;
import com.ihg.automation.selenium.gwt.components.TextArea;
import com.ihg.automation.selenium.gwt.components.checkbox.CheckBox;

public class PrivacyConfirmationPopUp extends PopUpBase
{

    public static final String PRIVACY_AGREEMENT = "Due to the Data Protection Act, I must inform you of the following:\n"
            + "If you enroll as an IHG Rewards Club Member, your personal data will be stored in the IHG database in the United States. This information may be used by our hotels and marketing partners worldwide.\n"
            + "If you agree to this, I can go and enroll you now.";

    public PrivacyConfirmationPopUp()
    {
        super("Privacy Scripts Confirmation");
    }

    public CheckBox getAccepted()
    {
        return container.getCheckBox("Guest verbally accepts terms and conditions");
    }

    public Button getCancel()
    {
        return container.getButton("Cancel");
    }

    public Button getConfirm()
    {
        return container.getButton("Confirm");
    }

    public Component getAgreementText()
    {
        return new TextArea(container, "Privacy Agreement Text");
    }
}
