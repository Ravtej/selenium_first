package com.ihg.automation.selenium.hotel.pages.hoteloperations.reimbursement.freenight;

import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.grid.Grid;

public class FreeNightFlatReimbursementGrid extends
        Grid<FreeNightFlatReimbursementGridRow, FreeNightFlatReimbursementGridRow.FreeNightFlatReimbursementGridCell>
{
    public FreeNightFlatReimbursementGrid(Container parent)
    {
        super(parent);
    }
}
