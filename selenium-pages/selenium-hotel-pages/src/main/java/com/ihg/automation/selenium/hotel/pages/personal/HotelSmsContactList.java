package com.ihg.automation.selenium.hotel.pages.personal;

import static com.ihg.automation.selenium.common.member.MemberPopulateHelper.getRandomPhone;

import com.ihg.automation.selenium.common.member.GuestPhone;
import com.ihg.automation.selenium.common.personal.ContactListBase;
import com.ihg.automation.selenium.common.personal.SmsContact;
import com.ihg.automation.selenium.gwt.components.Button;
import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.panel.EditablePanel;

public class HotelSmsContactList extends ContactListBase<GuestPhone, SmsContact>
{
    public HotelSmsContactList(Container container)
    {
        super(container.getSeparator("SMS"));
    }

    @Override
    public Button getAddButton()
    {
        return container.getButton("Add SMS");
    }

    @Override
    public SmsContact getContact(int index)
    {
        return new SmsContact(new EditablePanel(container, index, EditablePanel.HOTEL_XPATH));
    }

    @Override
    protected GuestPhone getRandomContact()
    {
        return getRandomPhone();
    }
}
