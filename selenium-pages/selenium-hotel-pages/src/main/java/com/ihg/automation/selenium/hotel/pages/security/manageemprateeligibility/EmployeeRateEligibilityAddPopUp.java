package com.ihg.automation.selenium.hotel.pages.security.manageemprateeligibility;

import com.ihg.automation.selenium.gwt.components.input.Select;

public class EmployeeRateEligibilityAddPopUp extends EmployeeRateEligibilityPopUpBase
{
    public EmployeeRateEligibilityAddPopUp()
    {
        super();
    }

    public Select getHotel()
    {
        return container.getSelectByLabel("Hotel");
    }
}
