package com.ihg.automation.selenium.hotel.pages.security.security;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.HasSameItemsAsCollection.hasSameItemsAsCollection;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static com.ihg.automation.selenium.matchers.text.AsCollection.asCollection;

import com.ihg.automation.selenium.gwt.components.grid.CellsName;
import com.ihg.automation.selenium.gwt.components.grid.GridRow;
import com.ihg.automation.selenium.gwt.components.grid.GridRowXpath;

public class UserManagementGridRow extends GridRow<UserManagementGridRow.UserManagementGridCell>
{
    public UserManagementGridRow(UserManagementGrid parent, GridRowXpath xpath)
    {
        super(parent, xpath);
    }

    public enum UserManagementGridCell implements CellsName
    {
        USERNAME("userId"), FIRST_NAME("firstName"), LAST_NAME("lastName"), JOB_TITLE("jobTitle"), ROLES(
                "securityRoles");

        private String name;

        private UserManagementGridCell(String name)
        {
            this.name = name;
        }

        @Override
        public String getName()
        {
            return name;
        }
    }

    public UserManagementDetailsPopUp getUserManagementPopUp()
    {
        getCell(UserManagementGridCell.USERNAME).asLink().clickAndWait();
        return new UserManagementDetailsPopUp();
    }

    public void verify(UserManagement userManagement)
    {
        verifyThat(getCell(UserManagementGridCell.USERNAME), hasText(userManagement.getUsername()));
        verifyThat(getCell(UserManagementGridCell.FIRST_NAME), hasText(userManagement.getFirstName()));
        verifyThat(getCell(UserManagementGridCell.LAST_NAME), hasText(userManagement.getLastName()));
        verifyThat(getCell(UserManagementGridCell.JOB_TITLE), hasText(userManagement.getJobTitle()));
        verifyThat(getCell(UserManagementGridCell.ROLES),
                hasText(asCollection(hasSameItemsAsCollection(userManagement.getRole()))));
    }
}
