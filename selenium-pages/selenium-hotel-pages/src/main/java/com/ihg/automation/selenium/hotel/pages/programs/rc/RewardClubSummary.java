package com.ihg.automation.selenium.hotel.pages.programs.rc;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static com.ihg.automation.selenium.matchers.component.ViewableMatcher.hasTextInView;

import com.ihg.automation.selenium.common.types.program.RewardClubLevel;
import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.input.Select;
import com.ihg.automation.selenium.gwt.components.label.Label;
import com.ihg.automation.selenium.gwt.components.label.SecurityLabel;
import com.ihg.automation.selenium.hotel.pages.programs.ProgramSummary;

public class RewardClubSummary extends ProgramSummary
{
    public RewardClubSummary(Container container)
    {
        super(container);
    }

    public Label getBalance()
    {
        return container.getLabel("Loyalty Unit Balance");
    }

    public Label getStatus()
    {
        return container.getLabel("Status");
    }

    public Label getTierLevel()
    {
        return container.getLabel("Tier-Level");
    }

    public Select getTierLevelExpiration()
    {
        return new SecurityLabel(container, "Tier-Level Expiration").getSelect();
    }

    public Label getMemberId()
    {
        return container.getLabel("Member ID");
    }

    public void verify(String balance, String status, RewardClubLevel rewardClubLevel, String tierLevelExpiration,
            String member)
    {
        verifyThat(getBalance(), hasText(balance));
        verifyThat(getStatus(), hasText(status));
        verifyThat(getTierLevel(), hasText(rewardClubLevel.getValue()));
        verifyThat(getTierLevelExpiration(), hasTextInView(tierLevelExpiration));
        verifyThat(getMemberId(), hasText(member));
    }
}
