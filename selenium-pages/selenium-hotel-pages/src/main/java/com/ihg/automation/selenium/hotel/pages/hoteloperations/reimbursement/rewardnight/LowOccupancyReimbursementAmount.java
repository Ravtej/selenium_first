package com.ihg.automation.selenium.hotel.pages.hoteloperations.reimbursement.rewardnight;

import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.CustomContainerBase;
import com.ihg.automation.selenium.gwt.components.utils.ByText;

public class LowOccupancyReimbursementAmount extends CustomContainerBase
{
    public LowOccupancyReimbursementAmount(Container container)
    {
        super(container.getFieldSet("Low Occupancy Reimbursement Amount"));
    }

    public LowOccupancyReimbursementAmountGrid getLowOccupancyReimbursementAmountGrid()
    {
        return new LowOccupancyReimbursementAmountGrid(container);
    }

    public ExceptionsGrid getExceptionsGrid()
    {
        return new ExceptionsGrid(container.getPanel(ByText.contains("Exceptions")));
    }
}
