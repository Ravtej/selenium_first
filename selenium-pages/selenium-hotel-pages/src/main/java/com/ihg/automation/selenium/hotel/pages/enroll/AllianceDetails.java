package com.ihg.automation.selenium.hotel.pages.enroll;

import com.ihg.automation.selenium.common.member.MemberAlliance;
import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.CustomContainerBase;
import com.ihg.automation.selenium.gwt.components.input.Input;
import com.ihg.automation.selenium.gwt.components.input.Select;

public class AllianceDetails extends CustomContainerBase
{
    public AllianceDetails(Container container)
    {
        super(container);
    }

    public Select getAlliance()
    {
        return container.getSelectByLabel("Carrier");
    }

    public Input getAllianceNumber()
    {
        return container.getInputByLabel("ID Number");
    }

    public void populate(MemberAlliance alliance)
    {
        getAlliance().selectByCode(alliance.getAlliance());
        getAllianceNumber().type(alliance.getNumber());
    }
}
