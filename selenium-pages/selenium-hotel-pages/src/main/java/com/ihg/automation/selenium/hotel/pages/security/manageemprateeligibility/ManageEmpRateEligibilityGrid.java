package com.ihg.automation.selenium.hotel.pages.security.manageemprateeligibility;

import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.grid.Grid;
import com.ihg.automation.selenium.gwt.components.grid.GridRowXpath;

public class ManageEmpRateEligibilityGrid
        extends Grid<ManageEmpRateEligibilityGridRow, ManageEmpRateEligibilityGridRow.ManageEmpRateEligibilityGridCell>
{
    public ManageEmpRateEligibilityGrid(Container parent)
    {
        super(parent);
    }

    public ManageEmpRateEligibilityGridRow getRow(String mbrNumber)
    {
        return getRow(new GridRowXpath.GridRowXpathBuilder()
                .cell(ManageEmpRateEligibilityGridRow.ManageEmpRateEligibilityGridCell.MEMBER_NUMBER, mbrNumber)
                .build());
    }
}
