package com.ihg.automation.selenium.hotel.pages.order;

import com.ihg.automation.selenium.common.history.EventHistoryTab;
import com.ihg.automation.selenium.common.history.EventHistoryTabAware;
import com.ihg.automation.selenium.common.pages.ClosablePopUpBase;
import com.ihg.automation.selenium.gwt.components.Button;

public class OrderDetailsPopUp extends ClosablePopUpBase implements EventHistoryTabAware
{
    public OrderDetailsPopUp()
    {
        super("Voucher Order Details");
    }

    public OrderDetailsTab getOrderDetailsTab()
    {
        return new OrderDetailsTab(this);
    }

    @Override
    public EventHistoryTab getHistoryTab()
    {
        return new EventHistoryTab(this, "History");
    }

    public Button getCancelOrder()
    {
        return container.getButton("Cancel Order");
    }

    public void clickCancelOrder()
    {
        getCancelOrder().clickAndWait();
    }
}
