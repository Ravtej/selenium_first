package com.ihg.automation.selenium.hotel.pages.hoteloperations.stay;

import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.label.Label;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.stay.lpu.missingstay.GuestDetailsBase;

public class GuestDetailsView extends GuestDetailsBase
{
    public GuestDetailsView(Container container)
    {
        super(container, "Guest Details");
    }

    @Override
    public Label getMemberName()
    {
        return container.getLabel("Guest Name");
    }

    @Override
    public Label getMemberNumber()
    {
        return container.getLabel(MEMBER_NUMBER);
    }
}
