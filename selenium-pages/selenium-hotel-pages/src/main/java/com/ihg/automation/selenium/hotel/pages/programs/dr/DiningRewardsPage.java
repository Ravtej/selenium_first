package com.ihg.automation.selenium.hotel.pages.programs.dr;

import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.hotel.pages.programs.ProgramPageBase;

public class DiningRewardsPage extends ProgramPageBase
{
    public DiningRewardsPage()
    {
        super(Program.DR);
    }

    @Override
    public DiningRewardsSummary getSummary()
    {
        return new DiningRewardsSummary(container);
    }
}
