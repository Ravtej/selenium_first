package com.ihg.automation.selenium.hotel.pages.security.security;

import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyNoError;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;

import com.ihg.automation.selenium.common.history.EventHistoryTab;
import com.ihg.automation.selenium.common.history.EventHistoryTabAware;
import com.ihg.automation.selenium.common.pages.PopUpBase;
import com.ihg.automation.selenium.gwt.components.Button;
import com.ihg.automation.selenium.gwt.components.label.Label;
import com.ihg.automation.selenium.listener.Verifier;

public class UserManagementDetailsPopUp extends PopUpBase implements EventHistoryTabAware
{
    public UserManagementDetailsPopUp()
    {
        super("User Details");
    }

    @Override
    public EventHistoryTab getHistoryTab()
    {
        return new EventHistoryTab(this, "History");
    }

    public UserManagementRoleCheckBoxGroup getManagementRoleCheckBoxGroup()
    {
        return new UserManagementRoleCheckBoxGroup(container);
    }

    public Label getUsername()
    {
        return container.getLabel("Username");
    }

    public Label getFirstName()
    {
        return container.getLabel("First Name");
    }

    public Label getLastName()
    {
        return container.getLabel("Last Name");
    }

    public Label getJobTitle()
    {
        return container.getLabel("Job Title");
    }

    public Label getRcNumberForEmpRate()
    {
        return container.getLabel("RC Number for Emp Rate");
    }

    public Button getCancel()
    {
        return container.getButton("Cancel");
    }

    public Button getClearAll()
    {
        return container.getButton("Clear All");
    }

    public Button getSaveChanges()
    {
        return container.getButton("Save Changes");
    }

    public void clickCancel()
    {
        getCancel().clickAndWait();
    }

    public void clickClearAll()
    {
        getClearAll().clickAndWait();
    }

    public void clickSaveChanges(Verifier... verifiers)
    {
        getSaveChanges().clickAndWait(verifiers);
    }

    public void verifyUserData(UserManagement userManagement)
    {
        verifyThat(getUsername(), hasText(userManagement.getUsername()));
        verifyThat(getFirstName(), hasText(userManagement.getFirstName()));
        verifyThat(getLastName(), hasText(userManagement.getLastName()));
        verifyThat(getJobTitle(), hasText(userManagement.getJobTitle()));
        verifyThat(getRcNumberForEmpRate(), hasText(userManagement.getRcNumberForEmpRate()));

        getManagementRoleCheckBoxGroup().verifyUserRoles(userManagement.getRole());
    }

    public void subscribeToAllRoles()
    {
        getManagementRoleCheckBoxGroup().checkAllRoles();
        clickSaveChanges(verifyNoError());
    }

    public void subscribeToOneRole(String role)
    {
        clickClearAll();
        container.getCheckBox(role).check();
        clickSaveChanges(verifyNoError());
    }
}
