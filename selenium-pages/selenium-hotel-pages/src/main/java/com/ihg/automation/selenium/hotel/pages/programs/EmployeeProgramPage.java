package com.ihg.automation.selenium.hotel.pages.programs;

import com.ihg.automation.selenium.common.types.program.Program;

public class EmployeeProgramPage extends ProgramPageBase
{
    public EmployeeProgramPage()
    {
        super(Program.EMP);
    }

    @Override
    public ProgramSummary getSummary()
    {
        return new ProgramSummary(container);
    }
}
