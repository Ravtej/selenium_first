package com.ihg.automation.selenium.hotel.pages.security.security;

import java.util.List;

public class UserManagement
{
    private String hotel;
    private String firstName;
    private String lastName;
    private String jobTitle;
    private List<String> role;
    private String username;
    private String rcNumberForEmpRate;

    public String getHotel()
    {
        return hotel;
    }

    public void setHotel(String hotel)
    {
        this.hotel = hotel;
    }

    public String getFirstName()
    {
        return firstName;
    }

    public void setFirstName(String firstName)
    {
        this.firstName = firstName;
    }

    public String getLastName()
    {
        return lastName;
    }

    public void setLastName(String lastName)
    {
        this.lastName = lastName;
    }

    public String getJobTitle()
    {
        return jobTitle;
    }

    public void setJobTitle(String jobTitle)
    {
        this.jobTitle = jobTitle;
    }

    public List<String> getRole()
    {
        return role;
    }

    public void setRole(List<String> role)
    {
        this.role = role;
    }

    public String getUsername()
    {
        return username;
    }

    public void setUsername(String username)
    {
        this.username = username;
    }

    public String getRcNumberForEmpRate()
    {
        return rcNumberForEmpRate;
    }

    public void setRcNumberForEmpRate(String rcNumberForEmpRate)
    {
        this.rcNumberForEmpRate = rcNumberForEmpRate;
    }
}
