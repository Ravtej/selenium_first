package com.ihg.automation.selenium.hotel.pages.hoteloperations.stay.lpu;

import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.Link;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.stay.StayGridRowContainerBase;

public class LpuGridRowContainer extends StayGridRowContainerBase
{
    public LpuGridRowContainer(Container container)
    {
        super(container);
    }

    public Link getAdjustRevenueDetails()
    {
        return container.getLink("Adjust Revenue Details");
    }
}
