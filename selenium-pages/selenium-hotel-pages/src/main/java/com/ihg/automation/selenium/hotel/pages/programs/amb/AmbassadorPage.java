package com.ihg.automation.selenium.hotel.pages.programs.amb;

import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.hotel.pages.programs.ProgramPageBase;

public class AmbassadorPage extends ProgramPageBase
{
    public AmbassadorPage()
    {
        super(Program.AMB);
    }

    @Override
    public AmbassadorSummary getSummary()
    {
        return new AmbassadorSummary(container);
    }
}
