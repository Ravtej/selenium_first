package com.ihg.automation.selenium.hotel.pages.hoteloperations.reimbursement.certificate;

import com.ihg.automation.selenium.common.event.SearchAware;
import com.ihg.automation.selenium.common.event.SearchButtonBar;
import com.ihg.automation.selenium.common.pages.TabCustomContainerBase;
import com.ihg.automation.selenium.hotel.pages.Tabs;

public class CertificateSearchPage extends TabCustomContainerBase implements SearchAware
{
    public CertificateSearchPage()
    {
        super(new Tabs().getReimbursement().getCertificateSearch());
    }

    @Override
    public SearchButtonBar getButtonBar()
    {
        return new SearchButtonBar(container);
    }

    @Override
    public CertificateSearch getSearchFields()
    {
        return new CertificateSearch(container);
    }

    public CertificateSearchGrid getCertificateSearchGrid()
    {
        return new CertificateSearchGrid(container);
    }

    public ResultsFilter getResultsFilter()
    {
        return new ResultsFilter(container);
    }
}
