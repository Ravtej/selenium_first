package com.ihg.automation.selenium.hotel.pages.hoteloperations.reimbursement.occupancy;

import com.ihg.automation.selenium.gwt.components.grid.CellsName;
import com.ihg.automation.selenium.gwt.components.grid.GridRow;
import com.ihg.automation.selenium.gwt.components.grid.GridRowXpath;

public class OccupancyAndADREntrySearchGridRow
        extends GridRow<OccupancyAndADREntrySearchGridRow.OccupancyAndADREntrySearchGridCell>
{
    public OccupancyAndADREntrySearchGridRow(OccupancyAndADREntrySearchGrid parent, GridRowXpath xpath)
    {
        super(parent, xpath);
    }

    public enum OccupancyAndADREntrySearchGridCell implements CellsName
    {
        STAY_DATE("date"), NIGHTS_COUNT("nightsCount"), OCC("occ"), ADR("adr"), OVERRIDE_OCC("actualOcc"), OVERRIDE_ADR(
                "actualAdr"), STATUS("status"), TOTAL_REIMBURSEMENT("totalReimbursement"), HOTEL_CURRENCY(
                "hotelCurrency"), DAYS_LEFT("leftToSubmit"), ADJUST_ACTION("adjustAction");

        private String name;

        private OccupancyAndADREntrySearchGridCell(String name)
        {
            this.name = name;
        }

        @Override
        public String getName()
        {
            return name;
        }
    }

}
