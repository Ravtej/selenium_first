package com.ihg.automation.selenium.hotel.pages.hoteloperations.stay.lpu.missingstay;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasText;

import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.gwt.components.Component;
import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.CustomContainerBase;

public abstract class GuestDetailsBase extends CustomContainerBase
{

    protected static final String MEMBER_NUMBER = "Member Number";

    public GuestDetailsBase(Container container, String separatorText)
    {
        super(container.getSeparator(separatorText));
    }

    public abstract Component getMemberName();

    public abstract Component getMemberNumber();

    public void verify(Member member)
    {
        verifyThat(getMemberName(), hasText(member.getName().getFullName()));
        verifyThat(getMemberNumber(), hasText(member.getRCProgramId()));
    }
}
