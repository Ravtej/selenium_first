package com.ihg.automation.selenium.hotel.pages.hoteloperations.stay.lpu.missingstay;

import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.pages.ClosablePopUpBase;
import com.ihg.automation.selenium.common.stay.Stay;
import com.ihg.automation.selenium.gwt.components.Button;
import com.ihg.automation.selenium.listener.Verifier;

public class StayCreatePopUp extends ClosablePopUpBase
{
    public static final String STAY_SUCCESS_CREATED = "Stay has been successfully created.";

    public StayCreatePopUp()
    {
    }

    public GuestDetailsEdit getGuestDetails()
    {
        return new GuestDetailsEdit(container);
    }

    public StayDetailsEdit getStayDetails()
    {
        return new StayDetailsEdit(container);
    }

    public RevenueDetailsEdit getRevenueDetails()
    {
        return new RevenueDetailsEdit(container);
    }

    public StayBookingInfoEdit getBookingDetails()
    {
        return new StayBookingInfoEdit(container);
    }

    public Button getCreateStay()
    {
        return container.getButton("Create Stay");
    }

    public void createStay(Verifier... verifiers)
    {
        getCreateStay().clickAndWait(verifiers);
    }

    public void populate(Member member, Stay stay)
    {
        getGuestDetails().addMemberId(member);
        getStayDetails().populate(stay);
        getRevenueDetails().populate(stay);
        getBookingDetails().populate(stay);
    }
}
