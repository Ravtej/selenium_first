package com.ihg.automation.selenium.hotel.pages.hoteloperations;

import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.CustomContainerBase;
import com.ihg.automation.selenium.gwt.components.Link;
import com.ihg.automation.selenium.listener.Verifier;

public class EventGridRowContainerWithDetails extends CustomContainerBase
{
    private static final String DETAILS_LINK = "Event Details";

    public EventGridRowContainerWithDetails(Container parent)
    {
        super(parent);
    }

    public Link getDetails()
    {
        return container.getLink(DETAILS_LINK);
    }

    public void clickDetails(Verifier... verifiers)
    {
        getDetails().clickAndWait(verifiers);
    }
}
