package com.ihg.automation.selenium.hotel.pages.hoteloperations.reimbursement.freenight;

import java.util.ArrayList;
import java.util.Arrays;

import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.CustomContainerBase;
import com.ihg.automation.selenium.gwt.components.input.DateInput;
import com.ihg.automation.selenium.gwt.components.input.Select;

public class FreeNightFlatReimbursementSearch extends CustomContainerBase
{
    public static ArrayList<String> STATUS = new ArrayList<>(Arrays.asList("ACCEPTED", "PAID", "All Statuses"));

    public FreeNightFlatReimbursementSearch(Container container)
    {
        super(container);
    }

    public Select getHotelCode()
    {
        return container.getSelectByLabel("Hotel Code");
    }

    public Select getStatus()
    {
        return container.getSelectByLabel("Status");
    }

    public DateInput getFromDate()
    {
        return new DateInput(container.getLabel("From"));
    }

    public DateInput getToDate()
    {
        return new DateInput(container.getLabel("To"));
    }
}
