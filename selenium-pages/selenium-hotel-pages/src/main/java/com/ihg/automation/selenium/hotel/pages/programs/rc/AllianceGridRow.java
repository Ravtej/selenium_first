package com.ihg.automation.selenium.hotel.pages.programs.rc;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasText;
import static org.hamcrest.CoreMatchers.containsString;

import com.ihg.automation.selenium.common.member.GuestName;
import com.ihg.automation.selenium.common.member.MemberAlliance;
import com.ihg.automation.selenium.gwt.components.grid.CellsName;
import com.ihg.automation.selenium.gwt.components.grid.Grid;
import com.ihg.automation.selenium.gwt.components.grid.GridRow;
import com.ihg.automation.selenium.gwt.components.grid.GridRowXpath;

public class AllianceGridRow extends GridRow<AllianceGridRow.AllianceCell>
{
    public AllianceGridRow(Grid parent, GridRowXpath xpath)
    {
        super(parent, xpath);
    }

    public void verify(MemberAlliance alliance)
    {
        verifyThat(getCell(AllianceCell.ALLIANCE), hasText(containsString(alliance.getAlliance().getValue())));
        verifyThat(getCell(AllianceCell.NUMBER), hasText(alliance.getNumber()));
        GuestName guestName = alliance.getName();
        if (guestName != null)
        {
            verifyThat(getCell(AllianceCell.NAME), hasText(guestName.getShortName()));
        }
    }

    enum AllianceCell implements CellsName
    {
        ALLIANCE("allianceName"), NUMBER("memberId"), NAME("name");

        private String name;

        AllianceCell(String name)
        {
            this.name = name;
        }

        @Override
        public String getName()
        {
            return name;
        }
    }

}
