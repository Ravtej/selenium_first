package com.ihg.automation.selenium.hotel.pages.hoteloperations.br.manage;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.hotel.pages.hoteloperations.br.manage.ManageEventsGridRow.ManageEventsGridCell.DAYS_LEFT_TO_SUBMIT;
import static com.ihg.automation.selenium.hotel.pages.hoteloperations.br.manage.ManageEventsGridRow.ManageEventsGridCell.END_DATE;
import static com.ihg.automation.selenium.hotel.pages.hoteloperations.br.manage.ManageEventsGridRow.ManageEventsGridCell.EVENT_ID;
import static com.ihg.automation.selenium.hotel.pages.hoteloperations.br.manage.ManageEventsGridRow.ManageEventsGridCell.EVENT_NAME;
import static com.ihg.automation.selenium.hotel.pages.hoteloperations.br.manage.ManageEventsGridRow.ManageEventsGridCell.EVENT_STATUS;
import static com.ihg.automation.selenium.hotel.pages.hoteloperations.br.manage.ManageEventsGridRow.ManageEventsGridCell.MBR_NUMBER;
import static com.ihg.automation.selenium.hotel.pages.hoteloperations.br.manage.ManageEventsGridRow.ManageEventsGridCell.MEMBER_NAME;
import static com.ihg.automation.selenium.hotel.pages.hoteloperations.br.manage.ManageEventsGridRow.ManageEventsGridCell.START_DATE;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;

import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.grid.Grid;
import com.ihg.automation.selenium.gwt.components.grid.GridHeader;

public class ManageEventsGrid extends Grid<ManageEventsGridRow, ManageEventsGridRow.ManageEventsGridCell>
{
    public ManageEventsGrid(Container parent)
    {
        super(parent);
    }

    public void verifyHeader()
    {
        GridHeader<ManageEventsGridRow.ManageEventsGridCell> header = getHeader();
        verifyThat(header.getCell(MEMBER_NAME), hasText(MEMBER_NAME.getValue()));
        verifyThat(header.getCell(MBR_NUMBER), hasText(MBR_NUMBER.getValue()));
        verifyThat(header.getCell(EVENT_ID), hasText(EVENT_ID.getValue()));
        verifyThat(header.getCell(EVENT_NAME), hasText(EVENT_NAME.getValue()));
        verifyThat(header.getCell(START_DATE), hasText(START_DATE.getValue()));
        verifyThat(header.getCell(END_DATE), hasText(END_DATE.getValue()));
        verifyThat(header.getCell(EVENT_STATUS), hasText(EVENT_STATUS.getValue()));
        verifyThat(header.getCell(DAYS_LEFT_TO_SUBMIT), hasText(DAYS_LEFT_TO_SUBMIT.getValue()));
    }
}
