package com.ihg.automation.selenium.hotel.pages.order;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;

import com.ihg.automation.selenium.common.order.OrderDetailsEditBase;
import com.ihg.automation.selenium.common.order.OrderItem;
import com.ihg.automation.selenium.common.order.VoucherOrder;
import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.label.Label;

public class OrderDetailsEdit extends OrderDetailsEditBase
{
    public OrderDetailsEdit(Container container)
    {
        super(container);
    }

    public OrderDetailsEdit(Container container, String itemId)
    {
        super(container, itemId);
    }

    @Override
    public Label getItemId()
    {
        return container.getLabel("Item ID");
    }

    public void verify(OrderItem orderItem)
    {
        super.verify(orderItem);
        verifyThat(getItemId(), hasText(orderItem.getItemID()));
    }

    @Override
    public void verify(VoucherOrder voucherOrder)
    {
        super.verify(voucherOrder);
        verifyThat(getItemId(), hasText(voucherOrder.getPromotionId()));
    }
}
