package com.ihg.automation.selenium.hotel.pages.hoteloperations.stay;

import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.stay.Stay;
import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.CustomContainerBase;

public abstract class StayGridRowContainerBase extends CustomContainerBase
{
    public StayGridRowContainerBase(Container container)
    {
        super(container);
    }

    public StayDetailsView getStayInfo()
    {
        return new StayDetailsView(container);
    }

    public GuestDetailsView getGuestDetails()
    {
        return new GuestDetailsView(container);
    }

    public StayBookingInfoView getStayBookingInfo()
    {
        return new StayBookingInfoView(container);
    }

    public RevenueDetailsView getRevenueDetails()
    {
        return new RevenueDetailsView(container);
    }

    public void verify(Stay stay, Member member)
    {
        getStayInfo().verify(stay);
        getGuestDetails().verify(member);
        getStayBookingInfo().verify(stay);
        getRevenueDetails().verify(stay);
    }
}
