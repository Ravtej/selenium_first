package com.ihg.automation.selenium.hotel.pages.hoteloperations.awards;

import org.joda.time.LocalDate;

import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.CustomContainerBase;
import com.ihg.automation.selenium.gwt.components.input.DateInput;
import com.ihg.automation.selenium.gwt.components.input.Input;
import com.ihg.automation.selenium.gwt.components.input.Select;
import com.ihg.automation.selenium.gwt.components.utils.ByText;

public class AwardSearch extends CustomContainerBase
{
    public AwardSearch(Container container)
    {
        super(container);
    }

    public Select getHotelCode()
    {
        return container.getSelectByLabel("Hotel Code");
    }

    public Input getMemberNumber()
    {
        return container.getInputByLabel("Member Number");
    }

    public Input getGuestName()
    {
        return container.getInputByLabel("Guest Name");
    }

    public Select getType()
    {
        return container.getSelectByLabel("Type");
    }

    public DateInput getFromDate()
    {
        return new DateInput(container.getLabel(ByText.contains("From")));
    }

    public DateInput getToDate()
    {
        return new DateInput(container.getLabel("To"));
    }

    public void fillDates(LocalDate fromDate, LocalDate toDate)
    {
        getFromDate().type(fromDate);
        getToDate().type(toDate);
    }
}
