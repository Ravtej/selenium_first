package com.ihg.automation.selenium.hotel.pages.hoteloperations.reimbursement.otheractivity;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static org.hamcrest.Matchers.containsString;

import com.ihg.automation.selenium.gwt.components.grid.CellsName;
import com.ihg.automation.selenium.gwt.components.grid.GridRow;
import com.ihg.automation.selenium.gwt.components.grid.GridRowXpath;

public class OtherActivityGridRow extends GridRow<OtherActivityGridRow.OtherActivityGridCell>
{
    public OtherActivityGridRow(OtherActivityGrid parent, GridRowXpath xpath)
    {
        super(parent, xpath);
    }

    public enum OtherActivityGridCell implements CellsName
    {
        CHECK_IN_DATE("checkInDate"), CERTIFICATE_NUMBER("certificateNumber"), DESCRIPTION_GUEST_NAME(
                "descriptionGuestName"), STATUS("status "), AMOUNT("amount"), CURRENCY("currency");

        private String name;

        private OtherActivityGridCell(String name)
        {
            this.name = name;
        }

        @Override
        public String getName()
        {
            return name;
        }
    }

    public void verify(String checkInDate, String certificateNumber, String guestName, String status, String amount,
            String currency)
    {
        verifyThat(getCell(OtherActivityGridCell.CHECK_IN_DATE), hasText(checkInDate));
        verifyThat(getCell(OtherActivityGridCell.CERTIFICATE_NUMBER), hasText(certificateNumber));
        verifyThat(getCell(OtherActivityGridCell.DESCRIPTION_GUEST_NAME), hasText(guestName));
        verifyThat(getCell(OtherActivityGridCell.STATUS), hasText(status));
        verifyThat(getCell(OtherActivityGridCell.AMOUNT), hasText(containsString(amount)));
        verifyThat(getCell(OtherActivityGridCell.CURRENCY), hasText(currency));
    }
}
