package com.ihg.automation.selenium.hotel.pages.hoteloperations.stay.stays;

import com.ihg.automation.selenium.common.pages.PopUpBase;
import com.ihg.automation.selenium.common.pages.TabCustomContainerBase;

public class StayDetailsTab extends TabCustomContainerBase
{
    public StayDetailsTab(PopUpBase parent)
    {
        super(parent, "Revenue Details");
    }
}
