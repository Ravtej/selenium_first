package com.ihg.automation.selenium.hotel.pages.hoteloperations.br.manage;

import com.ihg.automation.selenium.gwt.components.ComponentColumnPosition;
import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.CustomContainerBase;
import com.ihg.automation.selenium.gwt.components.input.Input;
import com.ihg.automation.selenium.gwt.components.input.InputXpath;
import com.ihg.automation.selenium.gwt.components.panel.FieldSet;

public class ResultsFilter extends CustomContainerBase
{
    public ResultsFilter(Container container)
    {
        super(container.getFieldSet("Results Filter"));
    }

    public void expand()
    {
        ((FieldSet) container).expand();
    }

    public Input getMemberName()
    {
        return new Input(container,
                new InputXpath.InputXpathBuilder().column(1, ComponentColumnPosition.TABLE).build(), "Member Name");
    }

    public Input getMemberNumber()
    {
        return new Input(container,
                new InputXpath.InputXpathBuilder().column(2, ComponentColumnPosition.TABLE).build(), "Member Number");
    }

    public Input getEventId()
    {
        return new Input(container,
                new InputXpath.InputXpathBuilder().column(3, ComponentColumnPosition.TABLE).build(), "Event Id");
    }

    public Input getEventName()
    {
        return new Input(container,
                new InputXpath.InputXpathBuilder().column(4, ComponentColumnPosition.TABLE).build(), "Event Name");
    }
}
