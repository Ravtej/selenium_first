package com.ihg.automation.selenium.hotel.pages.personal;

import com.ihg.automation.selenium.common.member.GuestAddress;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.personal.ContactListBase;
import com.ihg.automation.selenium.gwt.components.Button;
import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.panel.EditablePanel;

public class HotelAddressContactList extends ContactListBase<GuestAddress, AddressContact>
{
    public HotelAddressContactList(Container container)
    {
        super(container.getSeparator("Addresses"));
    }

    @Override
    public AddressContact getContact(int index)
    {
        return new AddressContact(new EditablePanel(container, index, EditablePanel.HOTEL_XPATH));
    }

    @Override
    public Button getAddButton()
    {
        return container.getButton("Add address");
    }

    @Override
    protected GuestAddress getRandomContact()
    {
        GuestAddress address = MemberPopulateHelper.getRandomAddress();
        address.setPreferred(false);
        address.setDoNotChangeLock(true);

        return address;
    }
}
