package com.ihg.automation.selenium.hotel.pages.enroll;

import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.CustomContainerBase;
import com.ihg.automation.selenium.gwt.components.input.Select;

public class EarningPreference extends CustomContainerBase
{
    public EarningPreference(Container container)
    {
        super(container);
    }

    public Select getEarningPreference()
    {
        return container.getSelectByLabel("Earning Preference");
    }

    public AllianceDetails getAllianceDetails()
    {
        return new AllianceDetails(container);
    }
}
