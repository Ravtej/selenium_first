package com.ihg.automation.selenium.hotel.pages.hoteloperations.reimbursement.occupancy;

import com.ihg.automation.selenium.gwt.components.Button;
import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.CustomContainerBase;
import com.ihg.automation.selenium.gwt.components.label.Label;

public class OccupancyGridRowContainer extends CustomContainerBase
{
    public OccupancyGridRowContainer(Container container)
    {
        super(container);
    }

    public Label getConfirmationNumber()
    {
        return container.getLabel("Confirmation Number");
    }

    public Label getMemberID()
    {
        return container.getLabel("Member ID");
    }

    public Label getLastName()
    {
        return container.getLabel("Last Name");
    }

    public Label getCertificateType()
    {
        return container.getLabel("Certificate Type");
    }

    public Label getCertificateNumber()
    {
        return container.getLabel("Certificate Number");
    }

    public Label getSuffix()
    {
        return container.getLabel("Suffix");
    }

    public Label getStatus()
    {
        return container.getLabel("Status");
    }

    public Label getOfferName()
    {
        return container.getLabel("Offer Name");
    }

    public Label getReimbursementAmount()
    {
        return container.getLabel("Reimbursement Amount");
    }

    public Label getReimbursementMethod()
    {
        return container.getLabel("Reimbursement Method");
    }

    public Label getReimbursementDate()
    {
        return container.getLabel("Reimbursement Date");
    }

    public Button getStayView()
    {
        return container.getButton("Stay View");
    }

}
