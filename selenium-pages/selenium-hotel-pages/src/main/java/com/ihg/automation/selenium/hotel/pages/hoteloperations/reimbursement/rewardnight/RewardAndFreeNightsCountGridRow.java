package com.ihg.automation.selenium.hotel.pages.hoteloperations.reimbursement.rewardnight;

import com.ihg.automation.selenium.gwt.components.grid.CellsName;
import com.ihg.automation.selenium.gwt.components.grid.GridRow;
import com.ihg.automation.selenium.gwt.components.grid.GridRowXpath;

public class RewardAndFreeNightsCountGridRow
        extends GridRow<RewardAndFreeNightsCountGridRow.RewardAndFreeNightsCountGridCell>
{
    public RewardAndFreeNightsCountGridRow(RewardAndFreeNightsCountGrid parent, GridRowXpath xpath)
    {
        super(parent, xpath);
    }

    public enum RewardAndFreeNightsCountGridCell implements CellsName
    {
        LOVIR_THRESHOLD("incentiveThreshold"), PROCESSED_AND_PAID_LOW_OCC("paidLowNights"), PROCESSED_AND_PAID_HIGH_OCC(
                "paidHighNights"), UNPROCESSED_BUT_PAID("pay1LowNights"), LOW_OCC_REIMBURSEMENT_AMOUNT(
                "lowOccupancyReimbursementAmount"), LOVIR_ADDITIONAL_REIMB_AMOUNT("bountyAmount"), TOTAL_REIMB_AMOUNT(
                "totalAmount"), CURRENCY("currencyCode");

        private String name;

        private RewardAndFreeNightsCountGridCell(String name)
        {
            this.name = name;
        }

        @Override
        public String getName()
        {
            return name;
        }
    }
}
