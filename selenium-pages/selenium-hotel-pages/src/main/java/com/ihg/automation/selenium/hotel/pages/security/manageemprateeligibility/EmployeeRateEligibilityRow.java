package com.ihg.automation.selenium.hotel.pages.security.manageemprateeligibility;

public class EmployeeRateEligibilityRow
{
    private String dateUpdated;
    private String firstName;
    private String lastName;
    private String memberId;
    private String expirationDate;

    public EmployeeRateEligibilityRow()
    {
    }

    public EmployeeRateEligibilityRow(String dateUpdated, String firstName, String lastName, String memberId,
            String expirationDate)
    {
        this.dateUpdated = dateUpdated;
        this.firstName = firstName;
        this.lastName = lastName;
        this.memberId = memberId;
        this.expirationDate = expirationDate;
    }

    public String getDateUpdated()
    {
        return dateUpdated;
    }

    public void setDateUpdated(String dateUpdated)
    {
        this.dateUpdated = dateUpdated;
    }

    public String getFirstName()
    {
        return firstName;
    }

    public void setFirstName(String firstName)
    {
        this.firstName = firstName;
    }

    public String getLastName()
    {
        return lastName;
    }

    public void setLastName(String lastName)
    {
        this.lastName = lastName;
    }

    public String getMemberId()
    {
        return memberId;
    }

    public void setMemberId(String memberId)
    {
        this.memberId = memberId;
    }

    public String getExpirationDate()
    {
        return expirationDate;
    }

    public void setExpirationDate(String expirationDate)
    {
        this.expirationDate = expirationDate;
    }

    public String getNameAndSurname()
    {
        return this.firstName + " " + this.lastName;
    }
}
