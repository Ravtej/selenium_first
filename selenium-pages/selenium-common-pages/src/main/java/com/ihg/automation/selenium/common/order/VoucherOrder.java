package com.ihg.automation.selenium.common.order;

import com.ihg.automation.common.DateUtils;
import com.ihg.automation.selenium.common.SiteHelperBase;
import com.ihg.automation.selenium.common.event.EventTransactionType;
import com.ihg.automation.selenium.common.event.Source;
import com.ihg.automation.selenium.common.types.Currency;

public class VoucherOrder
{
    public final static String TRANSACTION_TYPE = EventTransactionType.Order.ORDER_VOUCHER;

    private String promotionId;
    private String voucherName;
    private String currencyCode = Currency.USD.getCode();
    private String voucherCostAmount;
    private String numberOfVouchers = VoucherRule.DEFAULT_AMOUNT;
    private String numberOfVouchersDeposited = "0";
    private String voucherNumberMinValue;
    private String voucherNumberMaxValue;
    private String transactionDate = DateUtils.getFormattedDate();
    private String trackingNumber;
    private String signedFor;
    private String emailSendIndicator;
    private String status = VoucherOrderStatus.PROCESSING.getName();
    private String comments;
    private String userId;
    private String deliveryOption;

    private Source source;

    public VoucherOrder(String promotionId)
    {
        this.promotionId = promotionId;
    }

    public VoucherOrder(String promotionId, String voucherName)
    {
        this.promotionId = promotionId;
        this.voucherName = voucherName;
    }

    public VoucherOrder(VoucherRule voucherRule)
    {
        this.promotionId = voucherRule.getPromotionId();
        this.voucherName = voucherRule.getOrderDescription();
        this.numberOfVouchers = voucherRule.getQuantity();
        this.voucherCostAmount = voucherRule.getTotalCostAmount();
        this.currencyCode = voucherRule.getCurrencyCode();
    }

    public String getPromotionId()
    {
        return promotionId;
    }

    public void setPromotionId(String promotionId)
    {
        this.promotionId = promotionId;
    }

    public String getVoucherName()
    {
        return voucherName;
    }

    public void setVoucherName(String voucherName)
    {
        this.voucherName = voucherName;
    }

    public String getCurrencyCode()
    {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode)
    {
        this.currencyCode = currencyCode;
    }

    public String getVoucherCostAmount()
    {
        return voucherCostAmount;
    }

    public void setVoucherCostAmount(String voucherCostAmount)
    {
        this.voucherCostAmount = voucherCostAmount;
    }

    public String getNumberOfVouchers()
    {
        return numberOfVouchers;
    }

    public void setNumberOfVouchers(String numberOfVouchers)
    {
        this.numberOfVouchers = numberOfVouchers;
    }

    public String getNumberOfVouchersDeposited()
    {
        return numberOfVouchersDeposited;
    }

    public void setNumberOfVouchersDeposited(String numberOfVouchersDeposited)
    {
        this.numberOfVouchersDeposited = numberOfVouchersDeposited;
    }

    public String getVoucherNumberMinValue()
    {
        return voucherNumberMinValue;
    }

    public void setVoucherNumberMinValue(String voucherNumberMinValue)
    {
        this.voucherNumberMinValue = voucherNumberMinValue;
    }

    public String getVoucherNumberMaxValue()
    {
        return voucherNumberMaxValue;
    }

    public void setVoucherNumberMaxValue(String voucherNumberMaxValue)
    {
        this.voucherNumberMaxValue = voucherNumberMaxValue;
    }

    public String getTransactionDate()
    {
        return transactionDate;
    }

    public void setTransactionDate(String transactionDate)
    {
        this.transactionDate = transactionDate;
    }

    public String getTrackingNumber()
    {
        return trackingNumber;
    }

    public void setTrackingNumber(String trackingNumber)
    {
        this.trackingNumber = trackingNumber;
    }

    public String getSignedFor()
    {
        return signedFor;
    }

    public void setSignedFor(String signedFor)
    {
        this.signedFor = signedFor;
    }

    public String getEmailSendIndicator()
    {
        return emailSendIndicator;
    }

    public void setEmailSendIndicator(String emailSendIndicator)
    {
        this.emailSendIndicator = emailSendIndicator;
    }

    public String getStatus()
    {
        return status;
    }

    public void setStatus(String status)
    {
        this.status = status;
    }

    public void setStatus(VoucherOrderStatus status)
    {
        this.status = status.getName();
    }

    public String getComments()
    {
        return comments;
    }

    public void setComments(String comments)
    {
        this.comments = comments;
    }

    public String getUserId()
    {
        return userId;
    }

    public void setUserId(String userId)
    {
        this.userId = userId;
    }

    public String getDeliveryOption()
    {
        return deliveryOption;
    }

    public void setDeliveryOption(String deliveryOption)
    {
        this.deliveryOption = deliveryOption;
    }

    public Source getSource()
    {
        return source;
    }

    public void setSource(SiteHelperBase siteHelper)
    {
        this.source = siteHelper.getSourceFactory().getNoEmployeeIdSource();
    }

}
