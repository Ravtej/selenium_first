package com.ihg.automation.selenium.common.business;

import com.ihg.automation.selenium.gwt.components.grid.CellsName;
import com.ihg.automation.selenium.gwt.components.grid.GridRow;
import com.ihg.automation.selenium.gwt.components.grid.GridRowXpath;

public class AllPastStaysGridRow extends GridRow<AllPastStaysGridRow.AllPastStaysCell>
{
    public AllPastStaysGridRow(AllPastStaysGrid parent, GridRowXpath xpath)
    {
        super(parent, xpath);
    }

    public enum AllPastStaysCell implements CellsName
    {
        GUEST_NAME("guestName", "Guest Name"), //
        EVENT_ID("bookerEventId", "Event ID"), //
        MEMBER_NUMBER("memberId", "Member Number"), //
        CONF_NUMBER("confirmationNumber", "Conf Number"), //
        CHECK_IN("checkInDate", "Check In"), //
        NIGHTS("nights", "Nights"), //
        ROOM_NUMBER("roomNumber", "Room Number"), //
        RATE_CODE("rateCategoryCode", "Rate Code"), //
        ROOM_RATE("roomRate", "Room Rate"), //
        CHECK_BOX("check-box", "");

        private String name;
        private String value;

        private AllPastStaysCell(String name, String value)
        {
            this.name = name;
            this.value = value;
        }

        public String getValue()
        {
            return value;
        }

        @Override
        public String getName()
        {
            return name;
        }
    }
}
