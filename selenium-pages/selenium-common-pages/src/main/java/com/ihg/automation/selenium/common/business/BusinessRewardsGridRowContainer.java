package com.ihg.automation.selenium.common.business;

import com.ihg.automation.selenium.common.event.EventGridRowContainerWithDetails;
import com.ihg.automation.selenium.gwt.components.Container;

public class BusinessRewardsGridRowContainer extends EventGridRowContainerWithDetails
{
    public BusinessRewardsGridRowContainer(Container parent)
    {
        super(parent);
    }

    public BusinessRewardsDetails getBusinessRewardsDetails()
    {
        return new BusinessRewardsDetails(container);
    }
}
