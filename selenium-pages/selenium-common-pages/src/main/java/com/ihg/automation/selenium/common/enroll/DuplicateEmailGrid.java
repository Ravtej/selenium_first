package com.ihg.automation.selenium.common.enroll;

import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.grid.Grid;
import com.ihg.automation.selenium.gwt.components.grid.GridRowXpath;

public class DuplicateEmailGrid extends Grid<DuplicateEmailGridRow, DuplicateEmailGridRow.DuplicateEmailCell>
{

    public DuplicateEmailGrid(Container parent)
    {
        super(parent);
    }

    public DuplicateEmailGridRow getRowByMemberId(String memberId)
    {
        return getRow(new GridRowXpath.GridRowXpathBuilder()
                .cell(DuplicateEmailGridRow.DuplicateEmailCell.MBR_NUMBER, memberId).build());
    }
}
