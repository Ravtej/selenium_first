package com.ihg.automation.selenium.common.billing;

public interface EventBillingTabAware
{
    public EventBillingDetailsTab getBillingDetailsTab();
}
