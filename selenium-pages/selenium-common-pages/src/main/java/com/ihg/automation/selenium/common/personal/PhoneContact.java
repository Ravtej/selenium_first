package com.ihg.automation.selenium.common.personal;

import com.ihg.automation.selenium.common.contact.Phone;
import com.ihg.automation.selenium.common.member.GuestPhone;
import com.ihg.automation.selenium.gwt.components.Mode;
import com.ihg.automation.selenium.gwt.components.panel.EditablePanel;

public class PhoneContact extends EditableContactBase<GuestPhone, Phone>
{
    public PhoneContact(EditablePanel parent)
    {
        super(parent, new Phone(parent));
    }

    @Override
    public void populate(GuestPhone contact)
    {
        super.populate(contact);
    }

    public void verify(GuestPhone contact, Mode mode)
    {
        super.verify(contact, mode);
    }
}
