package com.ihg.automation.selenium.common.types.program;

import static com.ihg.automation.selenium.common.types.program.Program.AMB;
import static com.ihg.automation.selenium.common.types.program.Program.DR;
import static com.ihg.automation.selenium.common.types.program.Program.IHG;
import static com.ihg.automation.selenium.common.types.program.Program.KAR;
import static com.ihg.automation.selenium.common.types.program.Program.RC;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public enum TierLevelFlag
{
    AMERICA_AGENCY_CONTACTS("America's Agency Contacts", RC), //
    AMERICA_ALLIANCES_PARTNERS("America's Alliances/Partners", RC), //
    AMERICA_FRIENDS_OF_MANAGEMENT("America's Friends of Management", RC), //
    AMERICA_IAHI_MEMBERS("America's IAHI Members", RC), //
    AMERICA_PRESIDENT_BOARD_SVP("America's President/Board/SVP", RC), //
    AMERICA_RELATIONSHIP_MARKETING("America's Relationship Marketing", RC), //
    AMERICA_VIP_CUSTOMER("America's VIP Customer", RC), //
    AMEX_CENTURION("AMEX Centurion", RC, AMB), //
    AMEX_PLATINUM_GOLD_UPGRADE("AMEX Platinum- Gold Upgrade", RC), //
    APAC_OWNER_PROGRAM("APAC Owner's Program", RC), //
    ASIA_PACIFIC_ALLIANCES_PARTNERS("Asia Pacific Alliances/Partners", RC), //
    ASIA_PACIFIC_IAHI_MEMBERS("Asia Pacific IAHI Members", RC), //
    ASIA_PACIFIC_RELATIONSHIP_MARKETING("Asia Pacific Relationship Marketing", RC), //
    AUTO_RENEW("Auto-Renew", AMB), //
    BIG_CHEESE("BIG Cheese", KAR), //
    CISCO("Cisco", RC), //
    CITIC_AMBASSADOR("Citic Ambassador", AMB), //
    CHASE_AFFLUENT_REWARDS("Chase Affluent Rewards", AMB), //
    CORPORATE("Corporate", AMB), //
    CP_COLONIAL_PRO_AM_PLAYERS("CP Colonial Pro-Am Players", RC), //
    CP_PGA_PLAYERS("CP PGA Players", RC), //
    CUSTOMER_RETENTION("Customer Retention"), //
    DISTRIBUTION_AND_RELATIONSHIP_MARKETING_VIP("Distribution and Relationship Marketing VIP", RC), //
    DO_NOT_UPGRADE("Do Not Upgrade", AMB), //
    ELITE_VOUCHER_UPGRADE("Elite Voucher Upgrade", RC), //
    EMEA_ALLIANCES_PARTNERS("EMEA Alliances/Partners", RC), //
    EMEA_BRAND_CONTACTS("EMEA Brand Contacts", RC), //
    EMEA_IAHI_MEMBERS("EMEA IAHI Members", RC), //
    EMEA_RELATIONSHIP_MARKETING("EMEA Relationship Marketing", RC), //
    GENERAL_VIP("General VIP", RC, AMB), //
    GLOBAL_SALES("Global Sales", AMB), //
    HAND_RAISER_FOR_ENROLLMENT("Hand raiser for enrollment", IHG), //
    HEADS_UP("Heads Up", KAR), //
    HOLIDAY_CLUB_VACATIONS_PRIMARY("Holiday Club Vacations Primary", RC), //
    HOLIDAY_CLUB_VACATIONS_SECONDARY("Holiday Club Vacations Secondary", RC), //
    INACTIVE_FOR_6_MONTHS("Inactive for 6 months"), //
    JCB_AMBASSADOR("JCB Ambassador", AMB), //
    LIFE_TIME_MEMBER("Life time Member", AMB), //
    MAJOR_LEAGUE_BASEBALL("Major League Baseball", RC), //
    NEW_HOTEL_ACQUIRED("New Hotel/Acquired", AMB), //
    PARTNER("Partner", AMB), //
    POSSIBLE_FRAUD("Possible Fraud"), //
    PRIORITY_PRIVILEGE_GOLD("Priority Privilege Gold", RC), //
    PRIORITY_PRIVILEGE_PLATINUM("Priority Privilege Platinum", RC), //
    RELATIONSHIP_MARKETING_DEVELOPMENT_CONTACTS("Relationship Marketing Development Contacts", RC), //
    RETIREE("Retiree", AMB), //
    SOCIAL_INFLUENCER("Social Influencer", KAR), //
    STANDARD_STATEMENT_SEED("Standard Statement Seed", RC), //
    VERIFY_STAYS("Verify Stays", RC), //
    QASP("Quality Assurance Specialist", RC);

    private String value;
    private List<Program> programs;

    private TierLevelFlag(String value, Program... program)
    {
        this.value = value;
        this.programs = new ArrayList<Program>(program.length);
        Collections.addAll(this.programs, program);
    }

    public String getValue()
    {
        return value;
    }

    public List<Program> getPrograms()
    {
        return programs;
    }

    public static ArrayList<String> getRcValueList()
    {
        TierLevelFlag[] vals = values();
        ArrayList<String> list = new ArrayList<String>();
        for (TierLevelFlag val : vals)
        {
            if (val.programs.contains(RC))
            {
                list.add(val.getValue());
            }
        }

        list.addAll(getNoProgramValueList());
        return list;
    }

    public static ArrayList<String> getAmbValueList()
    {
        TierLevelFlag[] vals = values();
        ArrayList<String> list = new ArrayList<String>();
        for (TierLevelFlag val : vals)
        {
            if (val.programs.contains(AMB))
            {
                list.add(val.getValue());
            }
        }

        list.addAll(getNoProgramValueList());
        return list;
    }

    public static ArrayList<String> getDrValueList()
    {
        TierLevelFlag[] vals = values();
        ArrayList<String> list = new ArrayList<String>();
        for (TierLevelFlag val : vals)
        {
            if (val.programs.contains(DR))
            {
                list.add(val.getValue());
            }
        }

        list.addAll(getNoProgramValueList());
        return list;
    }

    public static ArrayList<String> getKarValueList()
    {
        TierLevelFlag[] vals = values();
        ArrayList<String> list = new ArrayList<String>();
        for (TierLevelFlag val : vals)
        {
            if (val.programs.contains(KAR))
            {
                list.add(val.getValue());
            }
        }

        list.addAll(getNoProgramValueList());
        return list;
    }

    public static ArrayList<String> getIhgValueList()
    {
        TierLevelFlag[] vals = values();
        ArrayList<String> list = new ArrayList<String>();
        for (TierLevelFlag val : vals)
        {
            if (val.programs.contains(IHG))
            {
                list.add(val.getValue());
            }
        }

        list.addAll(getNoProgramValueList());
        return list;
    }

    public String toString()
    {
        return getValue();
    }

    public static ArrayList<String> getNoProgramValueList()
    {
        TierLevelFlag[] vals = values();
        ArrayList<String> list = new ArrayList<String>();
        for (TierLevelFlag val : vals)
        {
            if (val.programs.isEmpty())
            {
                list.add(val.getValue());
            }
        }

        return list;
    }

}
