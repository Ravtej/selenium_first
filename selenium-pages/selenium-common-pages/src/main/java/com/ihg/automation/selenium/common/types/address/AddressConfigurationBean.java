package com.ihg.automation.selenium.common.types.address;

public class AddressConfigurationBean
{
    private Locality1Label locality1;
    private Locality2Label locality2;
    private RegionLabel region;
    private ZipLabel postalCode;

    AddressConfigurationBean()
    {
    }

    public void setLocality1(Locality1Label locality1)
    {
        this.locality1 = locality1;
    }

    public Locality1Label getLocality1()
    {
        return locality1;
    }

    public void setLocality2(Locality2Label locality2)
    {
        this.locality2 = locality2;
    }

    public Locality2Label getLocality2()
    {
        return locality2;
    }

    public void setRegion(RegionLabel region)
    {
        this.region = region;
    }

    public RegionLabel getRegion()
    {
        return region;
    }

    public void setPostalCode(ZipLabel postalCode)
    {
        this.postalCode = postalCode;
    }

    public ZipLabel getPostalCode()
    {
        return postalCode;
    }
}
