package com.ihg.automation.selenium.common.communication;

import static com.ihg.automation.selenium.gwt.components.utils.FindStepUtils.byXpath;
import static com.ihg.automation.selenium.gwt.components.utils.FindStepUtils.byXpathWithWait;
import static java.text.MessageFormat.format;

import com.ihg.automation.selenium.gwt.components.Component;
import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.CustomContainerBase;
import com.ihg.automation.selenium.gwt.components.input.Select;
import com.ihg.automation.selenium.gwt.components.utils.ByText;

public class ContactPermissionItem extends CustomContainerBase
{
    private static final String TYPE = "Communication Item";
    private static final String PATTERN = ".//tr[descendant::td[1]/span[{0}]]";

    public ContactPermissionItem(Container container, String item)
    {
        super(new Container(container, TYPE, item, byXpathWithWait(format(PATTERN, ByText.exact(item)))));
    }

    public ContactPermissionItem(Container container, int rowNumber)
    {
        super(new Container(container, TYPE, "Item", byXpathWithWait(format("./div/div/div/div[{0}]", rowNumber))));
    }

    public Component getCommunication()
    {
        return new Component(container, TYPE, "Communication", byXpath(".//td[1]/span"));
    }

    public Component getPreview()
    {
        return new Component(container, TYPE, "Preview", byXpath(".//td[2]/img[@class='gwt-Image']"));
    }

    public Component getFrequency()
    {
        return new Component(container, TYPE, "Frequency", byXpath(".//td[3]/span"));
    }

    public Select getSubscribe()
    {
        final String name = "Subscribe";
        Component parent = new Component(container, TYPE, name, byXpath(".//td[4]"));

        return new Select(parent, name);
    }

    public Component getDateUpdated()
    {
        return new Component(container, TYPE, "Date Updated", byXpath(".//td[5]/span"));
    }

    public Component getUpdatedBy()
    {
        return new Component(container, TYPE, "Updated By", byXpath(".//td[7]/span"));
    }

}
