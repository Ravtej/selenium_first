package com.ihg.automation.selenium.common.stay;

import static com.ihg.automation.common.DateUtils.DATE_FORMAT;
import static com.ihg.automation.common.DateUtils.previousYear;
import static com.ihg.automation.selenium.common.Constant.USD_TO_POINTS_CONVERSION_RATE;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.LocalDate;

import com.ihg.automation.common.DateUtils;
import com.ihg.automation.selenium.common.Constant;
import com.ihg.automation.selenium.common.event.Source;
import com.ihg.automation.selenium.common.pages.CreditCardType;
import com.ihg.automation.selenium.common.pages.PaymentType;
import com.ihg.automation.selenium.common.types.Currency;
import com.ihg.automation.selenium.gwt.components.EntityType;

public class Stay implements Cloneable
{
    public final static int QUALIFYING_NIGHTS_MAX_AMOUNT_FOR_STAY = 50;
    private String brandCode;
    private String tierLevelNights;
    private String ihgUnits = StringUtils.EMPTY;
    private String allianceUnits = StringUtils.EMPTY;
    private String eventDate = DateUtils.getFormattedDate();
    private String stayEventType = "CREATED";
    public final static int MIN_DAYS_SHIFT = -3;

    private String hotelCode;
    private String confirmationNumber;
    private String folioNumber;

    private String checkIn;
    private String checkOut;
    private Integer nights;
    private Double avgRoomRate;
    private Currency hotelCurrency = Currency.USD;
    private String earningPreference = Constant.RC_POINTS;

    private String rateCode;
    private String roomType;
    private String roomNumber;

    private String corporateAccountNumber;
    private String iataCode;

    private String enrollingStay;
    private String overlappingStay;
    private Integer qualifyingNights;

    private AdjustReasons adjustReasons;
    private BookingDetails bookingDetails = new BookingDetails();
    private PaymentType paymentType;
    private CreditCardType creditCardType;
    private Boolean isQualifying = true;

    private Revenues revenues = new Revenues();

    private Source activitySource;

    public String getHotelCode()
    {
        return hotelCode;
    }

    public void setHotelCode(String hotelCode)
    {
        this.hotelCode = hotelCode;
    }

    public String getConfirmationNumber()
    {
        return confirmationNumber;
    }

    public void setConfirmationNumber(String confirmationNumber)
    {
        this.confirmationNumber = confirmationNumber;
    }

    public String getFolioNumber()
    {
        return folioNumber;
    }

    public void setFolioNumber(String folioNumber)
    {
        this.folioNumber = folioNumber;
    }

    public String getCheckIn()
    {
        return checkIn;
    }

    public void setCheckIn(String checkIn)
    {
        this.checkIn = checkIn;
    }

    public String getCheckOut()
    {
        return checkOut;
    }

    public void setCheckOut(String checkOut)
    {
        this.checkOut = checkOut;
    }

    public Integer getNights()
    {
        return nights;
    }

    public String getNightsAsString()
    {
        return String.valueOf(nights);
    }

    public void setNights(Integer nights)
    {
        this.nights = nights;
    }

    public void setCheckInDateByCheckout()
    {
        this.checkIn = DateUtils.getStringToDate(checkOut).minusDays(nights).toString(DateUtils.DATE_FORMAT);
    }

    public void setCheckInOutByNights(int nights, int shift)
    {
        this.nights = nights;
        this.checkIn = DateUtils.getFormattedDate(MIN_DAYS_SHIFT - shift - nights);
        this.checkOut = DateUtils.getFormattedDate(MIN_DAYS_SHIFT - shift);
    }

    public void setCheckInOutByNightsInPreviousYear(int nights, int shift)
    {
        LocalDate CHECK_IN_DATE = new LocalDate(previousYear(), 3, 11).plusDays(shift);
        this.nights = nights;
        this.checkIn = CHECK_IN_DATE.toString(DATE_FORMAT);
        this.checkOut = CHECK_IN_DATE.plusDays(nights).toString(DATE_FORMAT);
    }

    public void setCheckInOutByNightsInPreviousYear(int nights)
    {
        setCheckInOutByNightsInPreviousYear(nights, 0);
    }

    public void setCheckInOutByNights(Integer nights)
    {
        setCheckInOutByNights(nights, 0);
    }

    public Double getAvgRoomRate()
    {
        return avgRoomRate;
    }

    public void setAvgRoomRate(Double avgRoomRate)
    {
        this.avgRoomRate = avgRoomRate;
    }

    public void setUsdAvgRoomRateByPoints(int avgRoomRate)
    {
        this.avgRoomRate = avgRoomRate / USD_TO_POINTS_CONVERSION_RATE;
    }

    public Currency getHotelCurrency()
    {
        return hotelCurrency;
    }

    public void setHotelCurrency(Currency hotelCurrency)
    {
        this.hotelCurrency = hotelCurrency;
    }

    public String getEarningPreference()
    {
        return earningPreference;
    }

    public void setEarningPreference(String earningPreference)
    {
        this.earningPreference = earningPreference;
    }

    public String getRateCode()
    {
        return rateCode;
    }

    public void setRateCode(String rateCode)
    {
        this.rateCode = rateCode;
    }

    public String getRoomType()
    {
        return roomType;
    }

    public void setRoomType(String roomType)
    {
        this.roomType = roomType;
    }

    public String getRoomNumber()
    {
        return roomNumber;
    }

    public void setRoomNumber(String roomNumber)
    {
        this.roomNumber = roomNumber;
    }

    public String getCorporateAccountNumber()
    {
        return corporateAccountNumber;
    }

    public void setCorporateAccountNumber(String corporateAccountNumber)
    {
        this.corporateAccountNumber = corporateAccountNumber;
    }

    public String getIataCode()
    {
        return iataCode;
    }

    public void setIataCode(String iataCode)
    {
        this.iataCode = iataCode;
    }

    public String getEnrollingStay()
    {
        return enrollingStay;
    }

    public void setEnrollingStay(String enrollingStay)
    {
        this.enrollingStay = enrollingStay;
    }

    public String getOverlappingStay()
    {
        return overlappingStay;
    }

    public void setOverlappingStay(String overlappingStay)
    {
        this.overlappingStay = overlappingStay;
    }

    public Integer getQualifyingNights()
    {
        return qualifyingNights;
    }

    public String getQualifyingNightsAsString()
    {
        return String.valueOf(qualifyingNights);
    }

    public void setQualifyingNights(Integer qualifyingNights)
    {
        this.qualifyingNights = qualifyingNights;
    }

    public BookingDetails getBookingDetails()
    {
        return bookingDetails;
    }

    public void setBookingDetails(BookingDetails bookingDetails)
    {
        this.bookingDetails = bookingDetails;
    }

    public void setPaymentType(PaymentType paymentType)
    {
        this.paymentType = paymentType;
    }

    public void setCreditCardType(CreditCardType creditCardType)
    {
        this.creditCardType = creditCardType;
    }

    public EntityType getPayment()
    {
        if (creditCardType != null)
        {
            return creditCardType;
        }

        if (paymentType != null)
        {
            return paymentType;
        }

        return null;
    }

    public AdjustReasons getAdjustReason()
    {
        return adjustReasons;
    }

    public void setAdjustReason(AdjustReasons adjustReasons)
    {
        this.adjustReasons = adjustReasons;
    }

    public Revenues getRevenues()
    {
        return revenues;
    }

    public void setRevenues(Revenues revenues)
    {
        this.revenues = revenues;
    }

    public Source getActivitySource()
    {
        return activitySource;
    }

    public void setActivitySource(Source activitySource)
    {
        this.activitySource = activitySource;
    }

    public void setIsQualifying(Boolean isQualifying)
    {
        this.isQualifying = isQualifying;
    }

    public Boolean getIsQualifying()
    {
        return isQualifying;
    }

    public String getBrandCode()
    {
        return brandCode;
    }

    public void setBrandCode(String brandCode)
    {
        this.brandCode = brandCode;
    }

    public String getTierLevelNights()
    {
        return tierLevelNights;
    }

    public void setTierLevelNights(String tierLevelNights)
    {
        this.tierLevelNights = tierLevelNights;
    }

    public String getIhgUnits()
    {
        return ihgUnits;
    }

    public void setIhgUnits(String ihgUnits)
    {
        this.ihgUnits = ihgUnits;
    }

    public String getAllianceUnits()
    {
        return allianceUnits;
    }

    public void setAllianceUnits(String allianceUnits)
    {
        this.allianceUnits = allianceUnits;
    }

    public String getEventDate()
    {
        return eventDate;
    }

    public void setEventDate(String eventDate)
    {
        this.eventDate = eventDate;
    }

    public String getStayEventType()
    {
        return stayEventType;
    }

    public void setStayEventType(String stayEventType)
    {
        this.stayEventType = stayEventType;
    }

    public Double getTotalRoomAmount()
    {
        return nights * avgRoomRate;
    }

    @Override
    public Stay clone()
    {
        Stay newStay = new Stay();

        newStay.brandCode = brandCode;
        newStay.tierLevelNights = tierLevelNights;

        newStay.ihgUnits = ihgUnits;
        newStay.allianceUnits = allianceUnits;
        newStay.eventDate = eventDate;
        newStay.stayEventType = stayEventType;
        newStay.hotelCode = hotelCode;
        newStay.confirmationNumber = confirmationNumber;
        newStay.folioNumber = folioNumber;
        newStay.checkIn = checkIn;
        newStay.checkOut = checkOut;
        newStay.nights = nights;
        newStay.avgRoomRate = avgRoomRate;
        newStay.hotelCurrency = hotelCurrency;
        newStay.earningPreference = earningPreference;
        newStay.rateCode = rateCode;
        newStay.roomType = roomType;
        newStay.roomNumber = roomNumber;
        newStay.corporateAccountNumber = corporateAccountNumber;
        newStay.iataCode = iataCode;
        newStay.enrollingStay = enrollingStay;
        newStay.overlappingStay = overlappingStay;
        newStay.qualifyingNights = qualifyingNights;
        newStay.bookingDetails = bookingDetails.clone();
        newStay.paymentType = paymentType;
        newStay.creditCardType = creditCardType;
        newStay.isQualifying = isQualifying;
        newStay.revenues = revenues.clone();

        if (activitySource != null)
        {
            newStay.activitySource = activitySource.clone();
        }

        return newStay;
    }

}
