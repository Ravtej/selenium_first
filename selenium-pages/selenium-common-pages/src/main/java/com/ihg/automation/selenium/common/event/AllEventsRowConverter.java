package com.ihg.automation.selenium.common.event;

import static com.ihg.automation.selenium.common.event.EventTransactionType.Order.ORDER_SYSTEM;
import static com.ihg.automation.selenium.common.event.EventTransactionType.Order.REDEEM;

import org.apache.commons.lang3.StringUtils;

import com.ihg.automation.selenium.common.Constant;
import com.ihg.automation.selenium.common.deposit.Deposit;
import com.ihg.automation.selenium.common.order.Order;
import com.ihg.automation.selenium.common.order.OrderItem;
import com.ihg.automation.selenium.common.stay.Stay;

public class AllEventsRowConverter
{
    private static final String STAY_DETAILS_PATTERN = "%s %s - %s %s Nights %s Qualifying Nights";
    private static final String DEPOSIT_DETAILS_PATTERN = "%s - %s";
    private static final String MULTIPLE_ITEMS = "Multiple items";

    public static AllEventsRow convert(Stay stay)
    {
        String detail = String.format(STAY_DETAILS_PATTERN, stay.getHotelCode(), stay.getCheckIn(), stay.getCheckOut(),
                stay.getNights(), stay.getQualifyingNights());

        return new AllEventsRow(stay.getEventDate(), "STAY - " + stay.getStayEventType(), detail, stay.getIhgUnits(),
                stay.getAllianceUnits());
    }

    public static AllEventsRow convert(Deposit deposit)
    {
        String detail = String.format(DEPOSIT_DETAILS_PATTERN, deposit.getTransactionId(),
                deposit.getTransactionName());

        return new AllEventsRow(deposit.getTransactionDate(), deposit.getTransactionType(), detail,
                deposit.getLoyaltyUnitsAmount(), deposit.getAllianceLoyaltyUnitsAmount());
    }

    /**
     * Only for single OrderItem
     */
    public static AllEventsRow convert(Order order)
    {
        return new AllEventsRow(order.getTransactionDate(), ORDER_SYSTEM, order.getOrderItems().get(0).getItemName(),
                "", "");
    }

    public static AllEventsRow getRedeemEvent(Order order)
    {
        StringBuilder detail = new StringBuilder();

        if (order.getOrderNumber() != null && !order.getOrderNumber().equals(Constant.NOT_AVAILABLE))
        {
            detail.append(order.getOrderNumber()).append(" - ");
        }

        if (order.getOrderItems().size() > 1)
        {
            detail.append(MULTIPLE_ITEMS);
        }
        else
        {
            OrderItem item = order.getOrderItems().get(0);
            detail.append(StringUtils.left(item.getItemName(), OrderItem.REDEEM_LENGTH_LIMIT).trim());

            if (item.getQuantity() != null && !item.getQuantity().equals(Constant.NOT_AVAILABLE))
            {
                detail.append(" (");
                detail.append(item.getQuantity());
                detail.append(")");
            }
        }

        return new AllEventsRow(order.getTransactionDate(), REDEEM, detail.toString(), order.getTotalLoyaltyUnits(),
                order.getTotalAllianceUnits());
    }
}
