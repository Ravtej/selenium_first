package com.ihg.automation.selenium.common.payment;

import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.grid.Grid;

public class BenefitsDetailGrid extends Grid<BenefitsDetailGridRow, BenefitsDetailGridRow.BenefitsDetailGridCell>
{
    public BenefitsDetailGrid(Container parent)
    {
        super(parent);
    }

}
