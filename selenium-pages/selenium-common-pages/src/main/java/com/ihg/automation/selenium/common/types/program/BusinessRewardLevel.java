package com.ihg.automation.selenium.common.types.program;

public enum BusinessRewardLevel implements ProgramLevel
{
    BR("BR", "IHG Business Rewards");

    private String code;
    private String value;

    private BusinessRewardLevel(String code, String description)
    {
        this.code = code;
        this.value = description;
    }

    @Override
    public String getCode()
    {
        return code;
    }

    @Override
    public String getValue()
    {
        return value;
    }

    @Override
    public String getCodeWithValue()
    {
        return code + " - " + value;
    }
}