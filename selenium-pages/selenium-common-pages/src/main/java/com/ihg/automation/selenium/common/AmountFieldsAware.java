package com.ihg.automation.selenium.common;

import com.ihg.automation.selenium.gwt.components.Component;

public interface AmountFieldsAware
{
    public Component getAmount();

    public Component getUnitType();
}
