package com.ihg.automation.selenium.common.event;

public class EventTransactionType
{
    private EventTransactionType()
    {
    }

    public static class Stay
    {
        public static final String STAY_PENDING = "STAY - PENDING";
        public static final String STAY_SYSTEM = "STAY - SYSTEM";
        public static final String STAY_ADJUSTED = "STAY - ADJUSTED";
        public static final String STAY_CREATED = "STAY - CREATED";
        public static final String STAY_ASSIGNED = "STAY - ASSIGNED";
        public static final String STAY_TRANSFERRED = "STAY - TRANSFERRED";
        public static final String STAY_REFERRAL = "STAY - REFERRAL";
    }

    public static class Order
    {
        public static final String ORDER_ADJUSTED = "ORDER - ADJUSTED";
        public static final String ORDER_CANCELED = "ORDER - CANCELED";
        public static final String ORDER_VOUCHER = "ORDER - VOUCHER";
        public static final String ORDER_SYSTEM = "ORDER - SYSTEM";
        public static final String REDEEM = "REDEEM";
        public static final String REDEEM_ADJUSTED = "REDEEM - ADJUSTED";
        public static final String REDEEM_CANCELED = "REDEEM - CANCELED";
    }

    public static class Offer
    {
        public static final String OFFER_REGISTRATION = "OFFER - REGISTRATION";
        public static final String OFFER_FORCE_TRACK = "OFFER - FORCE TRACK";
    }

    public static class Membership
    {
        public static final String ENROLL = "ENROLL";
        public static final String MEMBERSHIP_CLOSE = "MEMBERSHIP - CLOSE";
        public static final String MEMBERSHIP_OPEN = "MEMBERSHIP - OPEN";
        public static final String MEMBERSHIP_RENEWAL = "MEMBERSHIP - RENEWAL";
        public static final String MEMBERSHIP_EXTENSION = "MEMBERSHIP - EXTENSION";
        public static final String ENROLL_REFERRAL = "ENROLL - REFERRAL";
    }

    public static class TierLevel
    {
        public static final String UPGRADE_MANUAL = "UPGRADE - MANUAL";
        public static final String DOWNGRADE_MANUAL = "DOWNGRADE - MANUAL";
        public static final String UPGRADE_ACHIEVE = "UPGRADE - ACHIEVE";
        public static final String UPGRADE_MAINTAIN = "UPGRADE - MAINTAIN";
        public static final String DOWNGRADE_SYSTEM = "DOWNGRADE - SYSTEM";
        public static final String TIER_LEVEL_PURCHASE = "TIER LEVEL PURCHASE";
        public static final String BENEFIT_REDEEM = "BENEFIT - REDEEM";
        public static final String BENEFIT_UPGRADE = "BENEFIT - UPGRADE";
    }

    public static class Loyalty
    {
        public static final String UNIT_PURCHASE = "UNIT PURCHASE";
        public static final String TRANSFER_PURCHASE = "TRANSFER PURCHASE";
        public static final String TRANSFER = "TRANSFER";
        public static final String GIFT_PURCHASE = "GIFT PURCHASE";
        public static final String GIFT = "GIFT";
        public static final String BALANCE_TRANSFER = "BALANCE TRANSFER";
        public static final String Q_NIGHT_TRANSFER = "Q-NIGHT TRANSFER";
        public static final String ROLLOVER_NIGHTS = "ROLLOVER NIGHTS";
    }

    public static class Deposit
    {
        public static final String CO_PARTNER = "CO-PARTNER";
        public static final String GAME_PIECE = "GAME PIECE";
        public static final String GOODWILL_DEPOSIT = "GOODWILL DEPOSIT";
        public static final String INCENTIVE = "INCENTIVE";
        public static final String MEETING_BONUS = "MEETING BONUS";
        public static final String POINTS_AND_CASH_DEFAULT_PRICE = "POINTS AND CASH DEFAULT PRICE";
        public static final String POINTS_AND_CASH_VARIABLE_PRICE = "Q-NIGHT POINTS AND CASH VARIABLE PRICE";
        public static final String REBATE = "REBATE";
        public static final String SURVEY = "SURVEY";
        public static final String SWEEPSTAKES = "SWEEPSTAKES";
        public static final String COLLATERAL = "COLLATERAL";
        public static final String COMMUNICATION = "COMMUNICATION";
        public static final String GAME_INCENTIVE = "GAME INCENTIVE";
        public static final String LOAN = "LOAN";
        public static final String DEPOSIT = "DEPOSIT";
        public static final String OFFER_GOODWILL = "OFFER GOODWILL";
        public static final String HOTEL_POINT_AWARD = "HOTEL POINT AWARD";
    }

    public static class RewardNight
    {
        public static final String REWARD_NIGHT = "REWARD NIGHT";
        public static final String REWARD_NIGHT_CANCELED = "REWARD NIGHT - CANCELED";
    }

    public static class FreeNight
    {
        public static final String FREE_NIGHT = "FREE NIGHT";
        public static final String FREE_NIGHT_CANCELED = "FREE NIGHT - CANCELED";
    }

    public static class Voucher
    {
        public static final String VOUCHER_VOUCHER = "VOUCHER-VOUCHER";
    }

    public static class Goodwill
    {
        public static final String GOODWILL = "GOODWILL";
        /** Used only on <i>'Billing Details'</i> tab **/
        public static final String GOODWILL_CODE = "GDWIL";
    }

    public static class Profile
    {
        public static final String REGION_TRANSFER = "REGION TRANSFER";
    }

    public static class Meeting
    {
        public static final String MEETING = "MEETING";
    }

    public static class BusinessRewards
    {
        public static final String BUSINESS_REWARDS = "BUSINESS REWARDS";
    }

    public static class PointExpiration
    {
        public static final String EVENT_TYPE = "Point Expiration";

        public static final String EXPIRED_POINTS_REMOVAL = "EXPIRED POINTS REMOVAL";
        public static final String EXPIRED_POINTS_RESTORE = "EXPIRED POINTS RESTORE";
        public static final String EXTEND_POINT_EXPIRATION = "EXTEND POINT EXPIRATION";
    }
}
