package com.ihg.automation.selenium.common.business;

import com.ihg.automation.selenium.gwt.components.grid.CellsName;
import com.ihg.automation.selenium.gwt.components.grid.GridRow;
import com.ihg.automation.selenium.gwt.components.grid.GridRowXpath;

public class EventStaysGridRow extends GridRow<EventStaysGridRow.EventStaysCell>
{
    public EventStaysGridRow(EventStaysGrid parent, GridRowXpath xpath)
    {
        super(parent, xpath);
    }

    public enum EventStaysCell implements CellsName
    {
        GUEST_NAME("guestName", "Guest Name"), //
        MEMBER_NUMBER("memberId", "Member Number"), //
        CONF_NUMBER("confirmationNumber", "Conf Number"), //
        CHECK_IN("checkInDate", "Check In"), //
        NIGHTS("nights", "Nights"), //
        ROOM_NUMBER("roomNumber", "Room Number"), //
        RATE_CODE("rateCategoryCode", "Rate Code"), //
        ROOM_RATE("roomRate", "Room Rate"), //
        ELIGIBLE_FOR_POINTS("qualifiedIndicator", "Eligible for Points"), //
        REASON("qualificationChangeReasonCode", "Reason"), //
        TOTAL_ELIGIBLE_REVENUE("totalEligibleRoomRevenue", "Total Eligible Revenue");

        private String name;
        private String value;

        private EventStaysCell(String name, String value)
        {
            this.name = name;
            this.value = value;
        }

        public String getValue()
        {
            return value;
        }

        @Override
        public String getName()
        {
            return name;
        }
    }
}
