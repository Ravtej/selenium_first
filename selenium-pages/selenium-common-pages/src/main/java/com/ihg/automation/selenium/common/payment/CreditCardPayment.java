package com.ihg.automation.selenium.common.payment;

import com.ihg.automation.selenium.common.catalog.CatalogItem;
import com.ihg.automation.selenium.common.pages.CreditCardType;
import com.ihg.automation.selenium.common.pages.PaymentMethod;

public class CreditCardPayment extends Payment
{
    private String number;
    private CreditCardType type;

    public CreditCardPayment(String amount, String currency, String number, CreditCardType type)
    {
        super(PaymentMethod.CREDIT_CARD, amount, currency);
        this.number = number;
        this.type = type;
    }

    public CreditCardPayment(CatalogItem catalogItem, String number, CreditCardType type)
    {
        super(PaymentMethod.CREDIT_CARD, catalogItem);
        this.number = number;
        this.type = type;
    }

    public String getNumber()
    {
        return number;
    }

    public void setNumber(String number)
    {
        this.number = number;
    }

    public CreditCardType getType()
    {
        return type;
    }

    public void setType(CreditCardType type)
    {
        this.type = type;
    }
}
