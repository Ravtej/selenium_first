package com.ihg.automation.selenium.common.order;

import com.ihg.automation.selenium.gwt.components.Component;
import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.label.Label;

public abstract class OrderDetailsViewBase extends OrderDetailsBase
{
    public OrderDetailsViewBase(Container container)
    {
        super(container);
    }

    abstract public Component getItemId();

    @Override
    public Label getItemName()
    {
        return container.getLabel("Item Name");
    }

    @SuppressWarnings("unchecked")
    @Override
    public Label getEstimatedDeliveryDate()
    {
        return container.getLabel(ESTIMATED_DELIVERY_DATE_LABEL);
    }

    @SuppressWarnings("unchecked")
    @Override
    public Label getActualDeliveryDate()
    {
        return container.getLabel(ACTUAL_DELIVERY_DATE_LABEL);
    }

    @Override
    public Label getDeliveryStatus()
    {
        return container.getLabel(DELIVERY_STATUS_LABEL);
    }

    @SuppressWarnings("unchecked")
    @Override
    public Label getTrackingUniqueNumber()
    {
        return container.getLabel(TRACKING_NUMBER_LABEL);
    }

    @Override
    public Label getSignedForBy()
    {
        return container.getLabel(SIGNED_FOR_BY_LABEL);
    }

    @Override
    public Label getComments()
    {
        return container.getLabel(COMMENTS_LABEL);
    }
}
