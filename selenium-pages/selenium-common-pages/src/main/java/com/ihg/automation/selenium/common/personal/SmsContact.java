package com.ihg.automation.selenium.common.personal;

import com.ihg.automation.selenium.common.contact.Sms;
import com.ihg.automation.selenium.common.member.GuestPhone;
import com.ihg.automation.selenium.gwt.components.Mode;
import com.ihg.automation.selenium.gwt.components.panel.EditablePanel;

public class SmsContact extends EditableContactBase<GuestPhone, Sms>
{
    public SmsContact(EditablePanel parent)
    {
        super(parent, new Sms(parent));
    }

    @Override
    public void populate(GuestPhone contact)
    {
        getBaseContact().populate(contact);
        if (getInvalid().isDisplayed())
        {
            getInvalid().check(!contact.isValid());
        }
    }

    public void verify(GuestPhone contact, Mode mode)
    {
        getBaseContact().verify(contact, mode);
    }

    public void verifyMasked(GuestPhone contact, Mode mode)
    {
        getBaseContact().verifyMasked(contact, mode);
    }
}
