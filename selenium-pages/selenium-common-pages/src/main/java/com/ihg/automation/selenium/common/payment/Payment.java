package com.ihg.automation.selenium.common.payment;

import com.ihg.automation.selenium.common.catalog.CatalogItem;
import com.ihg.automation.selenium.common.pages.PaymentMethod;

public class Payment
{
    private PaymentMethod method;
    private String currency;
    private String amount;

    public Payment(PaymentMethod method, String amount, String currency)
    {
        this.method = method;
        this.currency = currency;
        this.amount = amount;
    }

    public Payment(PaymentMethod method, CatalogItem catalogItem)
    {
        this(method, catalogItem.getBuyerCost(), catalogItem.getBuyerCurrency());
    }

    public PaymentMethod getMethod()
    {
        return method;
    }

    public void setMethod(PaymentMethod method)
    {
        this.method = method;
    }

    public String getCurrency()
    {
        return currency;
    }

    public void setCurrency(String currency)
    {
        this.currency = currency;
    }

    public String getAmount()
    {
        return amount;
    }

    public void setAmount(String amount)
    {
        this.amount = amount;
    }
}
