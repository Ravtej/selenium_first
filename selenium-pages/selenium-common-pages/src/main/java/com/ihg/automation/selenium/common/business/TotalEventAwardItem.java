package com.ihg.automation.selenium.common.business;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.gwt.components.utils.FindStepUtils.byXpath;
import static com.ihg.automation.selenium.gwt.components.utils.FindStepUtils.byXpathWithWait;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasAnyText;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static java.text.MessageFormat.format;

import com.ihg.automation.selenium.common.Constant;
import com.ihg.automation.selenium.gwt.components.Component;
import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.CustomContainerBase;
import com.ihg.automation.selenium.gwt.components.utils.ByText;
import com.ihg.automation.selenium.gwt.components.utils.FindStep;

public class TotalEventAwardItem extends CustomContainerBase
{
    private static final String TYPE = "Total Event Award Item";
    private static final String PATTERN = ".//tr[descendant::*[{0}]]";
    private static final FindStep ESTIMATED_POINT_EARNING_PATTERN = byXpath(".//td[3]//span");
    private static final FindStep ESTIMATED_COST_HOTEL_PATTERN = byXpath(".//td[4]//span");

    public TotalEventAwardItem(Container container, String item)
    {
        super(new Container(container, TYPE, item, byXpathWithWait(format(PATTERN, ByText.exact(item)))));
    }

    public Component getEstimatedPointEarning()
    {
        return new Component(container, TYPE, "Estimated Point Earning", ESTIMATED_POINT_EARNING_PATTERN);
    }

    public Component getEstimatedCostToHotel()
    {
        return new Component(container, TYPE, "Estimated Cost to Hotel", ESTIMATED_COST_HOTEL_PATTERN);
    }

    public void verify(String estimatedPointEarning, String estimatedCostToHotel)
    {
        verifyThat(getEstimatedPointEarning(), hasText(estimatedPointEarning));
        verifyThat(getEstimatedCostToHotel(), hasText(estimatedCostToHotel));
    }

    public void verifyIsPrePopulated()
    {
        verifyThat(getEstimatedPointEarning(), hasAnyText());
        verifyThat(getEstimatedCostToHotel(), hasAnyText());
    }

    public void verifyDefault()
    {
        verify(Constant.NOT_AVAILABLE, Constant.NOT_AVAILABLE);
    }
}
