package com.ihg.automation.selenium.common;

public class ProgramCounter
{
    public static final String TOTAL_QUALIFIED = "TOTAL_QUALIFIED";
    public static final String TIER_LEVEL_NIGHT = "TIER_LEVEL_NIGHT";

    private ProgramCounter()
    {
    }
}
