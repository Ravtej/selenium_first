package com.ihg.automation.selenium.common.business;

import static com.ihg.automation.selenium.gwt.components.ComponentColumnPosition.FOLLOWING_TD;

import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.checkbox.CheckBox;
import com.ihg.automation.selenium.gwt.components.input.Input;
import com.ihg.automation.selenium.gwt.components.input.Select;
import com.ihg.automation.selenium.gwt.components.panel.FieldSet;
import com.ihg.automation.selenium.gwt.components.utils.WaitUtils;

public class OffersFieldsEdit extends OffersFieldsBase
{
    public OffersFieldsEdit(Container container)
    {
        super(container.getFieldSet("Offers"));
    }

    public CheckBox getIncludeOffers()
    {
        return container.getCheckBox("Include Offers");
    }

    @Override
    public Select getOffer()
    {
        return container.getLabel("Offer").getSelect("Offer", 2, FOLLOWING_TD);
    }

    @Override
    public Input getPointsToAward()
    {
        return container.getInputByLabel("Points to Award", "Offer", 2, FOLLOWING_TD);
    }

    public void expand()
    {
        ((FieldSet) container).expand();
    }

    public void populate(BusinessOffer businessOffer)
    {
        expand();
        getIncludeOffers().check();
        new WaitUtils().waitExecutingRequest();
        getOffer().select(businessOffer.getOfferName());
        getPointsToAward().type(businessOffer.getPointsToAward());
    }
}
