package com.ihg.automation.selenium.common.business;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.IsEnabled.enabled;

import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.CustomContainerBase;
import com.ihg.automation.selenium.gwt.components.checkbox.CheckBox;
import com.ihg.automation.selenium.gwt.components.input.DateInput;
import com.ihg.automation.selenium.gwt.components.input.Input;
import com.ihg.automation.selenium.gwt.components.panel.FieldSet;

public class MeetingFields extends CustomContainerBase
{
    public MeetingFields(Container container)
    {
        super(container.getFieldSet("Meeting"));
    }

    public CheckBox getIncludeMeeting()
    {
        return container.getCheckBox("Include Meeting");
    }

    public DateInput getStartDate()
    {
        return new DateInput(container.getLabel("Start Date"));
    }

    public DateInput getEndDate()
    {
        return new DateInput(container.getLabel("End Date"));
    }

    public Input getNumberOfMeetingRooms()
    {
        return container.getInputByLabel("Number of Meeting Rooms");
    }

    public Input getTotalDelegates()
    {
        return container.getInputByLabel("Total Delegates");
    }

    public MeetingRevenueDetails getRevenueDetails()
    {
        return new MeetingRevenueDetails(this);
    }

    public void expand()
    {
        ((FieldSet) container).expand();
    }

    public void populate(MeetingDetails meetingDetails)
    {
        expand();
        getIncludeMeeting().check();
        getStartDate().type(meetingDetails.getStartDate());
        getEndDate().type(meetingDetails.getEndDate());
        getNumberOfMeetingRooms().type(meetingDetails.getNumberOfMeetingRooms());
        getTotalDelegates().type(meetingDetails.getTotalDelegates());
    }

    public void verifyEnabled(Boolean isEnabled)
    {
        verifyThat(getStartDate(), enabled(isEnabled));
        verifyThat(getEndDate(), enabled(isEnabled));
        verifyThat(getNumberOfMeetingRooms(), enabled(isEnabled));
        verifyThat(getTotalDelegates(), enabled(isEnabled));
    }
}
