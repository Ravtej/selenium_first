package com.ihg.automation.selenium.common.stay;

import com.ihg.automation.selenium.gwt.components.EntityType;

public enum AdjustReasons implements EntityType
{
    ADDITIONAL("A", "Additional Revenue"), //
    ADJUST("J", "Adjust Revenue"), //
    COMPLIMENTARY("C", "Complimentary Stay"), //
    MEETING("M", "Meeting/Banquet"), //
    NOSHOW("O", "No-Show"), //
    NQ_RATE("N", "Non-Qualifying Rate"), //
    NQ_ROOM("R", "Non-Qualifying Room");

    private String code;
    private String value;

    AdjustReasons(String code, String value)
    {
        this.code = code;
        this.value = value;
    }

    @Override
    public String getCode()
    {
        return code;
    }

    @Override
    public String getValue()
    {
        return value;
    }

    @Override
    public String toString()
    {
        return getValue();
    }

    @Override
    public String getCodeWithValue()
    {
        return code + "-" + value;
    }
}
