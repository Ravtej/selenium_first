package com.ihg.automation.selenium.common.types.program;

public class ProgramStatusReason
{
    public static final String DECLINED = "Declined Ts&Cs";
    public static final String IHG_REQUEST = "IHG Request";
    public static final String FROZEN = "Frozen";
    public static final String MUTUALLY_EXCLUSIVE = "Mutually Exclusive";
    public static final String DUPLICATE = "Duplicate";
    public static final String CUSTOMER_REQUEST = "Customer Request";
    public static final String PENDING = "Pending";
    public static final String EXPIRED = "Expired";
    public static final String RENEW = "Renew";
    public static final String LAPSED = "Lapsed";

    private ProgramStatusReason()
    {
    }
}
