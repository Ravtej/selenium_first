package com.ihg.automation.selenium.common.pages;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;

import com.ihg.automation.selenium.gwt.components.Button;
import com.ihg.automation.selenium.gwt.components.PopUpWindow;
import com.ihg.automation.selenium.listener.Verifier;

public abstract class ClosablePopUpBase extends PopUpBase
{
    public ClosablePopUpBase()
    {
        super();
    }

    public ClosablePopUpBase(String header)
    {
        super(header);
    }

    public ClosablePopUpBase(PopUpWindow popUp)
    {
        super(popUp);
    }

    public Button getClose()
    {
        return container.getButton(Button.CLOSE);
    }

    public void clickClose(Verifier... verifiers)
    {
        getClose().clickAndWait(verifiers);
    }

    public void close()
    {
        if (isDisplayed())
        {
            clickClose();
            verifyThat(this, displayed(false));
        }
    }

}
