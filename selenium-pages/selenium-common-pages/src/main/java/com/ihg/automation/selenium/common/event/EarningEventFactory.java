package com.ihg.automation.selenium.common.event;

import static com.ihg.automation.common.DateUtils.dd_MMM_YYYY;
import static com.ihg.automation.selenium.common.Constant.BASE_AWARDS;
import static com.ihg.automation.selenium.common.Constant.BASE_UNITS;
import static com.ihg.automation.selenium.common.Constant.RC_POINTS;
import static org.apache.commons.lang3.StringUtils.EMPTY;

import org.joda.time.LocalDate;

import com.ihg.automation.selenium.common.Constant;
import com.ihg.automation.selenium.common.catalog.CatalogItem;
import com.ihg.automation.selenium.common.types.program.Program;

public class EarningEventFactory
{
    private static final String CREATED = "Created";
    public static final String REDEEM = "Redeem";
    public static final String AT_VENDOR = "at Vendor";

    public static final EarningEvent getAwardEvent(CatalogItem catalogItem)
    {
        EarningEvent event = new EarningEvent(BASE_AWARDS, "1", EMPTY, EMPTY, CREATED);
        event.setAward(catalogItem.getItemId());
        event.setAllianceTransactionCode(EMPTY);
        event.setOrderNumber(EMPTY);
        event.setAllianceSentDate(EMPTY);

        return event;
    }

    public static final EarningEvent getAwardEvent(CatalogItem catalogItem, LocalDate dateEarned)
    {
        EarningEvent event = getAwardEvent(catalogItem);
        event.setDateEarned(dateEarned.toString(dd_MMM_YYYY));

        return event;
    }

    public static final EarningEvent getUnitEvent(CatalogItem catalogItem)
    {
        return new EarningEvent(BASE_UNITS, catalogItem.getLoyaltyUnitCost(), "0", Constant.RC_POINTS, EMPTY);
    }

    public static final EarningEvent getRenewDREvent(LocalDate enrollDate)
    {
        EarningEvent drPointEvent = new EarningEvent(Program.DR.getCode(), "5,000", "0", Constant.RC_POINTS, "", "");
        drPointEvent.setDateEarned(enrollDate);

        return drPointEvent;
    }


    public static final EarningEvent getBaseUnitsEvent(String amount, String qualifyAmount)
    {
        return new EarningEvent(BASE_UNITS, amount, qualifyAmount, RC_POINTS);
    }

    public static final EarningEvent getRedeemEvent(CatalogItem catalogItem)
    {
        EarningEvent event = new EarningEvent(REDEEM, "-" + catalogItem.getLoyaltyUnitCost(), "0", RC_POINTS, CREATED);
        event.setAward(catalogItem.getItemId());
        event.setAllianceTransactionCode(EMPTY);
        event.setOrderNumber(EMPTY);
        event.setAllianceSentDate(EMPTY);

        return event;
    }

    public static final EarningEvent getProgramEvent(Program program, CatalogItem catalogItem)
    {
        return new EarningEvent(program.getCode(), "1", EMPTY, EMPTY, CREATED, catalogItem.getItemId());
    }

    /* Only for DR tests */
    public static final EarningEvent getProgramEventWithoutStatus(Program program, CatalogItem catalogItem,
            LocalDate date)
    {
        EarningEvent event = getProgramEvent(program, catalogItem);
        event.setDateEarned(date.toString(dd_MMM_YYYY));
        event.setAllianceSentDate(null);
        event.setStatus(null);

        return event;
    }
}
