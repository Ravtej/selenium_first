package com.ihg.automation.selenium.common.types.address;

import com.ihg.automation.selenium.common.types.LabelEnumAware;

public enum Locality1Label implements LabelEnumAware
{
    LOCALITY("Locality"), CITY("City"), POST_TOWN("Post Town"), CITY_TOWN("City/Post Town");

    private String label;

    private Locality1Label(String label)
    {
        this.label = label;
    }

    @Override
    public String getLabel()
    {
        return label;
    }
}
