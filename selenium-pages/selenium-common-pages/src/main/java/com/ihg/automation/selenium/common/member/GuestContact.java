package com.ihg.automation.selenium.common.member;

import com.ihg.automation.selenium.gwt.components.EntityType;

public abstract class GuestContact
{
    private boolean preferred = true;

    private boolean valid = true;

    protected EntityType type;

    public boolean isPreferred()
    {
        return preferred;
    }

    public void setPreferred(boolean preferred)
    {
        this.preferred = preferred;
    }

    public boolean isValid()
    {
        return valid;
    }

    public void setValid(boolean valid)
    {
        this.valid = valid;
    }

    public EntityType getType()
    {
        return type;
    }

    public void setType(EntityType type)
    {
        this.type = type;
    }
}
