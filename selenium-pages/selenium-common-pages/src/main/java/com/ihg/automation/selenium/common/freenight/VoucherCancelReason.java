package com.ihg.automation.selenium.common.freenight;

public class VoucherCancelReason
{
    public static final String EXCEPTION = "Exception";
    public static final String FRAUD = "Fraud";
    public static final String INACTIVE_ACCOUNT = "Inactive Account";

    private VoucherCancelReason()
    {
    }
}
