package com.ihg.automation.selenium.common.member;

import java.util.List;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ihg.automation.common.CustomStringUtils;
import com.ihg.automation.selenium.common.types.name.NameConfiguration;
import com.ihg.automation.selenium.common.types.name.NameConfigurationBean;

public class GuestName implements Cloneable
{
    private static final Logger logger = LoggerFactory.getLogger(GuestName.class);

    private String given;
    private String middle;
    private String surname;
    private String suffix;
    private String salutation;
    private String title;
    private String degree;
    private String nickname;
    private String nativeLanguage;
    private GuestName nativeName;

    public String getGiven()
    {
        return given;
    }

    public void setGiven(String given)
    {
        this.given = given;
    }

    public String getMiddle()
    {
        return middle;
    }

    public void setMiddle(String middle)
    {
        this.middle = middle;
    }

    public String getSurname()
    {
        return surname;
    }

    public void setSurname(String surname)
    {
        this.surname = surname;
    }

    public String getSuffix()
    {
        return suffix;
    }

    public void setSuffix(String suffix)
    {
        this.suffix = suffix;
    }

    public String getSalutation()
    {
        return salutation;
    }

    public void setSalutation(String salutation)
    {
        this.salutation = salutation;
    }

    public String getTitle()
    {
        return title;
    }

    public void setTitle(String title)
    {
        this.title = title;
    }

    public String getDegree()
    {
        return degree;
    }

    public void setDegree(String degree)
    {
        this.degree = degree;
    }

    public GuestName getNativeName()
    {
        return nativeName;
    }

    public void setNativeName(GuestName nativeName)
    {
        this.nativeName = nativeName;
    }

    public String getNickname()
    {
        return nickname;
    }

    public void setNickname(String nickname)
    {
        this.nickname = nickname;
    }

    public String getFullName(NameConfigurationBean nameConfiguration)
    {
        List<String> order = nameConfiguration.getOrder();
        String fullName = "";
        String name;

        for (String orderedName : order)
        {
            try
            {
                name = BeanUtils.getProperty(this, orderedName);
                fullName = CustomStringUtils.concat(" ", fullName, name);
            }
            catch (Exception e)
            {
                logger.error("Error while trying to return bean property value", e);
            }

        }
        return fullName;
    }

    public String getFullName()
    {
        return getFullName(NameConfiguration.getDefaultConfiguration());
    }

    public String getNameOnPanel()
    {
        return CustomStringUtils.concat(" ", salutation, given, surname);
    }

    public String getShortName()
    {
        return CustomStringUtils.concat(", ", surname, given);
    }

    public String getNativeLanguage()
    {
        return nativeLanguage;
    }

    public void setNativeLanguage(String nativeLanguage)
    {
        this.nativeLanguage = nativeLanguage;
    }

    @Override
    public String toString()
    {
        return getFullName();
    }

    public String getNameOnCard()
    {
        final int MAX_LENGTH = 26;

        String nameOnCard = composeNameOnCard(given, middle, surname, suffix, title);
        if (nameOnCard.length() > MAX_LENGTH)
            nameOnCard = composeNameOnCard(given, StringUtils.substring(middle, 0, 1), surname, suffix, title);
        if (nameOnCard.length() > MAX_LENGTH)
            nameOnCard = composeNameOnCard(given, null, surname, suffix, title);
        if (nameOnCard.length() > MAX_LENGTH)
            nameOnCard = composeNameOnCard(StringUtils.substring(given, 0, 1), null, surname, suffix, title);
        if (nameOnCard.length() > MAX_LENGTH)
            nameOnCard = composeNameOnCard(StringUtils.substring(given, 0, 1), null, surname, suffix, null);
        if (nameOnCard.length() > MAX_LENGTH)
            nameOnCard = composeNameOnCard(StringUtils.substring(given, 0, 1), null, surname, null, null);

        return nameOnCard.substring(0, MAX_LENGTH);
    }

    private String composeNameOnCard(String given, String middle, String surname, String suffix, String nameTitle)
    {
        String nameOnCard = given;
        nameOnCard = CustomStringUtils.concat(" ", nameOnCard, middle);
        nameOnCard = CustomStringUtils.concat(" ", nameOnCard, surname);
        nameOnCard = CustomStringUtils.concat(", ", nameOnCard, suffix);
        nameOnCard = CustomStringUtils.concat(", ", nameOnCard, nameTitle);

        return nameOnCard;
    }

    @Override
    public GuestName clone()
    {
        GuestName newName = new GuestName();

        newName.given = given;
        newName.middle = middle;
        newName.surname = surname;
        newName.suffix = suffix;
        newName.salutation = salutation;
        newName.title = title;
        newName.degree = degree;
        newName.nickname = nickname;

        return newName;
    }
}
