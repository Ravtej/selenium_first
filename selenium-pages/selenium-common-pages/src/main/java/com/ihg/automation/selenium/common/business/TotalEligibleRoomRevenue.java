package com.ihg.automation.selenium.common.business;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;

import com.ihg.automation.selenium.common.pages.LabelContainerBase;
import com.ihg.automation.selenium.common.types.Currency;
import com.ihg.automation.selenium.gwt.components.Component;
import com.ihg.automation.selenium.gwt.components.ComponentColumnPosition;
import com.ihg.automation.selenium.gwt.components.Container;

public class TotalEligibleRoomRevenue extends LabelContainerBase
{
    private static final ComponentColumnPosition POSITION = ComponentColumnPosition.SINGLE_TD;

    public TotalEligibleRoomRevenue(Container container)
    {
        super(container.getLabel("Total Eligible Room Revenue"));
    }

    public Component getAmount()
    {
        return label.getComponent("Amount", 1, POSITION);
    }

    public Component getCurrency()
    {
        return label.getComponent("Currency", 2, POSITION);
    }

    public void verify(String amount, Currency currency)
    {
        verifyThat(getAmount(), hasText(amount));
        verifyThat(getCurrency(), hasText(currency.getCode()));
    }
}
