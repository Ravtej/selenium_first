package com.ihg.automation.selenium.common.history;

import com.ihg.automation.selenium.common.pages.PopUpBase;
import com.ihg.automation.selenium.common.pages.TabCustomContainerBase;

public class EventHistoryTab extends TabCustomContainerBase
{
    public EventHistoryTab(PopUpBase parent, String tabText)
    {
        super(parent, tabText);
    }

    public EventHistoryGrid getGrid()
    {
        return new EventHistoryGrid(container);
    }
}
