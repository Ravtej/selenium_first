package com.ihg.automation.selenium.common.personal;

import com.ihg.automation.selenium.common.contact.Email;
import com.ihg.automation.selenium.common.member.GuestEmail;
import com.ihg.automation.selenium.gwt.components.panel.EditablePanel;

public class EmailContact extends EditableContactBase<GuestEmail, Email>
{
    public EmailContact(EditablePanel parent)
    {
        super(parent, new Email(parent));
    }
}
