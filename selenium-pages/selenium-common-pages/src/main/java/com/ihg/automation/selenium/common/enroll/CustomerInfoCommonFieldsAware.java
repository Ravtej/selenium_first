package com.ihg.automation.selenium.common.enroll;

import com.ihg.automation.selenium.common.name.PersonNameAware;
import com.ihg.automation.selenium.gwt.components.Component;
import com.ihg.automation.selenium.gwt.components.CustomContainerBase;

public interface CustomerInfoCommonFieldsAware extends PersonNameAware
{
    public static final String COUNTRY_LABEL = "Country";
    public static final String GENDER_LABEL = "Gender";
    public static final String DATE_OF_BIRTH_LABEL = "Date of Birth";

    public Component getGender();

    public CustomContainerBase getBirthDate();

    public Component getResidenceCountry();

}
