package com.ihg.automation.selenium.common.pages.program;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static com.ihg.automation.selenium.matchers.component.IsDisplayed.displayed;
import static com.ihg.automation.selenium.matchers.component.IsEnabled.enabled;
import static com.ihg.automation.selenium.matchers.component.IsSelected.isSelected;
import static com.ihg.automation.selenium.matchers.component.ViewableMatcher.hasTextInView;

import com.ihg.automation.selenium.common.pages.MultiModeBase;
import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.checkbox.CheckBox;
import com.ihg.automation.selenium.gwt.components.input.DateInput;
import com.ihg.automation.selenium.gwt.components.input.Select;
import com.ihg.automation.selenium.gwt.components.label.Label;
import com.ihg.automation.selenium.gwt.components.utils.ByText;

public class EmployeeRateEligibility extends MultiModeBase
{
    public static final String EMPLOYEE_RATE_ELIGIBILITY_SAVED = "Employee Rate Eligibility Saved";
    public static final String EMPLOYEE_RATE_ELIGIBILITY_REMOVED = "Employee Rate Eligibility Removed";

    public EmployeeRateEligibility(Container container)
    {
        super(container.getSeparator("Employee Rate Eligibility"));
    }

    public CheckBox getEligibleForEmployeeRate()
    {
        return new CheckBox(container, ByText.exact("Eligible for employee rate"), CheckBox.CHECK_BOX_PATTERN2);
    }

    public Label getEligibleForEmployeeRateLabel()
    {
        return container.getLabel("Eligible for employee rate");
    }

    public DateInput getExpires()
    {
        DateInput birthDate = new DateInput(container.getLabel("Expires"));
        return birthDate;
    }

    public Label getChannel()
    {
        return container.getLabel("Channel");
    }

    public Select getLocation()
    {
        return container.getSelectByLabel("Location");
    }

    public void verifyInViewMode(String rateLabel, String expires, String location, String channel)
    {
        verifyThat(getEligibleForEmployeeRateLabel(), hasText(rateLabel));
        verifyThat(getExpires(), hasTextInView(expires));
        verifyThat(getLocation(), hasTextInView(location));
        verifyThat(getChannel(), hasText(channel));
    }

    public void verifyInEditMode(String expires, String location, String channel)
    {
        verifyThat(getExpires(), hasText(expires));
        verifyThat(getLocation(), hasText(location));
        verifyThat(getChannel(), hasText(channel));
    }

    public void verifyFieldsDisplayed(Boolean isChecked)
    {
        verifyThat(getEligibleForEmployeeRate(), displayed(true));
        verifyThat(getEligibleForEmployeeRate(), isSelected(isChecked));
        verifyThat(getExpires(), displayed(true));
        verifyThat(getExpires(), enabled(isChecked));
        verifyThat(getLocation(), displayed(true));
        verifyThat(getChannel(), displayed(true));
    }
}
