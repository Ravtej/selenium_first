package com.ihg.automation.selenium.common.personal;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.assertThat;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.enabled;
import static org.hamcrest.core.Is.is;

import org.openqa.selenium.By;

import com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory;
import com.ihg.automation.selenium.common.member.GuestContact;
import com.ihg.automation.selenium.gwt.components.Button;
import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.CustomContainerBase;
import com.ihg.automation.selenium.gwt.components.Mode;
import com.ihg.automation.selenium.gwt.components.panel.EditablePanel;
import com.ihg.automation.selenium.listener.Verifier;

public abstract class ContactListBase<T extends GuestContact, F extends EditableComponent<T>>
        extends CustomContainerBase
{
    public ContactListBase(Container container)
    {
        super(container);
    }

    public abstract F getContact(int index);

    public F getExpandedContact(int index)
    {
        F contact = getContact(index);
        contact.expand();
        return contact;
    }

    /** Get the 1st contact */
    public F getContact()
    {
        return getContact(1);
    }

    /** Get the 1st contact and expand it */
    public F getExpandedContact()
    {
        return getExpandedContact(1);
    }

    public abstract Button getAddButton();

    public void clickAddButton(Verifier... verifiers)
    {
        getAddButton().clickAndWait(verifiers);
    }

    public int getContactsCount()
    {
        return findElements(By.xpath(EditablePanel.GENERAL_XPATH), "Contacts list").size();
    }

    public void add(T entity, Verifier... verifiers)
    {
        int count = getContactsCount() + 1;
        clickAddButton();
        F contact = getContact(count);
        contact.populate(entity);

        contact.clickSave(MessageBoxVerifierFactory.verifyNoError().add(verifiers));
        verifyThat(container, getContactsCount(), is(count), "Contact amount is successfully incremented (after add)");
    }

    protected abstract T getRandomContact();

    public void addRandomContact()
    {
        add(getRandomContact());
    }

    public void addMaxContactAmount(final int maxAmount)
    {
        for (int i = getContactsCount(); i < maxAmount; i++)
        {
            assertThat(getAddButton(), enabled(true));
            addRandomContact();
        }

        verifyThat(getAddButton(), enabled(false));
    }

    public void updateFirstContact(T newContact, String successMsg)
    {
        EditableComponent<T> updatedContact = getContact();
        updatedContact.update(newContact, MessageBoxVerifierFactory.verifySuccess(successMsg));

        updatedContact.expand();
        updatedContact.verify(newContact, Mode.VIEW);
    }

    public void addSecondContactAsPreferred(T existingPreferredContact, T newPreferredContact, String successMsg)
    {
        verifyThat(this, getContactsCount(), is(1), "Invalid contact amount (before add)");

        newPreferredContact.setPreferred(true);
        existingPreferredContact.setPreferred(false);

        add(newPreferredContact, MessageBoxVerifierFactory.verifySuccess(successMsg));

        // Preferred GuestContact is always 1st in the list
        getExpandedContact().verify(newPreferredContact, Mode.VIEW);

        // Initial address is moved to 2nd place
        getExpandedContact(2).verify(existingPreferredContact, Mode.VIEW);
    }

    /**
     * Method requires 2 contacts, one of which should be marked as preferred
     **/
    public void removePreferredContact(T survivalGuestContact, String successMsg)
    {
        survivalGuestContact.setPreferred(true);

        verifyThat(this, getContactsCount(), is(2), "Invalid contact amount (before remove)");
        getContact().remove(MessageBoxVerifierFactory.verifySuccess(successMsg));

        getExpandedContact().verify(survivalGuestContact, Mode.VIEW);

        verifyThat(this, getContactsCount(), is(1), "Invalid contact amount (after remove)");
    }
}
