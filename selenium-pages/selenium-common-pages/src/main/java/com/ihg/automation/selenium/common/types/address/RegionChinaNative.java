package com.ihg.automation.selenium.common.types.address;

import com.ihg.automation.selenium.gwt.components.EntityType;

public enum RegionChinaNative implements EntityType
{
    BEI_JING_SHI("11", "北京市"), //
    NEI_MENG("15", "內蒙古自治区");

    private String code;
    private String value;

    private RegionChinaNative(String code, String value)
    {
        this.code = code;
        this.value = value;
    }

    @Override
    public String getCode()
    {
        return code;
    }

    @Override
    public String getValue()
    {
        return value;
    }

    @Override
    public String getCodeWithValue()
    {
        return code + " - " + value;
    }

    @Override
    public String toString()
    {
        return getValue();
    }
}
