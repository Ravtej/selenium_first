package com.ihg.automation.selenium.common.types.name;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import com.ihg.automation.selenium.common.components.Country;

public class NameConfiguration
{
    private static final Map<Country, NameConfigurationBean> CONFIGS = initConfigs();

    private NameConfiguration()
    {
    }

    private static Map<Country, NameConfigurationBean> initConfigs()
    {
        Map<Country, NameConfigurationBean> configs = new HashMap<Country, NameConfigurationBean>();

        configs.put(Country.US, getDefaultConfiguration());
        configs.put(Country.CA, getDefaultConfiguration());
        configs.put(Country.ES, getWesternEuropeConfiguration());
        configs.put(Country.FR, getWesternEuropeConfiguration());
        configs.put(Country.BY, getDefaultConfiguration());
        configs.put(Country.RU, getDefaultConfiguration());
        configs.put(Country.CN, getEasternAsiaConfiguration());
        configs.put(Country.JP, getEasternAsiaConfiguration());
        configs.put(Country.TW, getEasternAsiaConfiguration());
        configs.put(Country.HK, getDefaultConfiguration());
        configs.put(Country.MO, getEasternAsiaConfiguration());
        configs.put(Country.AU, getDefaultConfiguration());
        configs.put(Country.ID, getEasternAsiaConfiguration());
        configs.put(Country.AR, getWesternEuropeConfiguration());
        configs.put(Country.UA, getDefaultConfiguration());
        configs.put(Country.ER, getDefaultConfiguration());

        return configs;
    }

    public static NameConfigurationBean getNameConfiguration(Country country)
    {
        NameConfigurationBean bean = CONFIGS.get(country);

        if (bean == null)
        {
            throw new RuntimeException(String.format("No name configuration is found for %s", country));
        }

        return bean;
    }

    public static NameConfigurationBean getDefaultConfiguration()
    {
        NameConfigurationBean bean = new NameConfigurationBean();

        bean.setGiven(GivenLabel.FIRST);
        bean.setMiddle(MiddleLabel.MIDDLE);
        bean.setSurname(SurnameLabel.LAST);
        bean.setSalutation(SalutationLabel.SALUTATION);
        bean.setTitle(TitleLabel.TITLE);
        bean.setDegree(DegreeLabel.DEGREE);

        bean.setOrder(Arrays.asList("salutation", "given", "middle", "surname", "suffix", "title", "degree"));

        return bean;
    }

    public static NameConfigurationBean getWesternEuropeConfiguration()
    {
        NameConfigurationBean conf = getDefaultConfiguration();
        conf.setSurname(SurnameLabel.FAMILY);

        return conf;
    }

    public static NameConfigurationBean getEasternAsiaConfiguration()
    {
        NameConfigurationBean conf = getDefaultConfiguration();
        conf.setGiven(GivenLabel.GIVEN);
        conf.setSurname(SurnameLabel.FAMILY);
        conf.setSalutation(SalutationLabel.PREFIX);
        conf.setTitle(TitleLabel.SURNAME_2ND);

        conf.setOrder(Arrays.asList("salutation", "surname", "given", "middle"));

        return conf;
    }

}
