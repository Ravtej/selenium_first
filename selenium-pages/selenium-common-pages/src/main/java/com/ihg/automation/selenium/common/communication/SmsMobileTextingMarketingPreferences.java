package com.ihg.automation.selenium.common.communication;

import com.ihg.automation.selenium.gwt.components.Button;
import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.CustomContainerBase;
import com.ihg.automation.selenium.gwt.components.Image;
import com.ihg.automation.selenium.gwt.components.input.Select;
import com.ihg.automation.selenium.gwt.components.label.Label;

public class SmsMobileTextingMarketingPreferences extends CustomContainerBase
{
    public SmsMobileTextingMarketingPreferences(Container container)
    {
        super(container.getSeparator("SMS Mobile Texting Marketing Preferences"));
    }

    public Select getSmsNumber()
    {
        return container.getSelectByLabel("SMS Number");
    }

    public Label getSmsNumberView()
    {
        return container.getLabel("SMS Number");
    }

    public Image getInvalidSmsImage()
    {
        return getSmsNumberView().getImage("Not Valid Image");
    }

    public Button getResend()
    {
        return container.getButton("Resend");
    }

    public Label getSMSNumberConfirmationStatus()
    {
        return container.getLabel("SMS Number Confirmation Status:");
    }

    public ContactPermissionItems getContactPermissionItems()
    {
        return new ContactPermissionItems(container);
    }

}
