package com.ihg.automation.selenium.common.enroll;

public class EmailConstant
{
    public static final String EMAIL_WITH_MORE_THAN_10_DUPLICATES = "more10mail11222016619@test.com";
    public static final String EMAIL_WITH_10_DUPLICATES = "equal10mail11222016619@test.com";
    public static final String EMAIL_WITH_LESS_THAN_10_DUPLICATES = "less10mail11222016619@test.com";

    private EmailConstant()
    {
    }
}
