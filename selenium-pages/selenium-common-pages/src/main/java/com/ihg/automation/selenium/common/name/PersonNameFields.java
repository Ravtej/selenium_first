package com.ihg.automation.selenium.common.name;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;

import com.ihg.automation.selenium.common.components.Country;
import com.ihg.automation.selenium.common.member.GuestName;
import com.ihg.automation.selenium.common.pages.Populate;
import com.ihg.automation.selenium.common.pages.VerifyMultiMode;
import com.ihg.automation.selenium.common.types.name.DegreeLabel;
import com.ihg.automation.selenium.common.types.name.GivenLabel;
import com.ihg.automation.selenium.common.types.name.MiddleLabel;
import com.ihg.automation.selenium.common.types.name.NameConfiguration;
import com.ihg.automation.selenium.common.types.name.NameConfigurationBean;
import com.ihg.automation.selenium.common.types.name.SalutationLabel;
import com.ihg.automation.selenium.common.types.name.SurnameLabel;
import com.ihg.automation.selenium.common.types.name.TitleLabel;
import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.CustomContainerBase;
import com.ihg.automation.selenium.gwt.components.Link;
import com.ihg.automation.selenium.gwt.components.Mode;
import com.ihg.automation.selenium.gwt.components.input.Input;
import com.ihg.automation.selenium.gwt.components.input.Select;
import com.ihg.automation.selenium.gwt.components.label.Label;
import com.ihg.automation.selenium.gwt.components.label.NativeNameLabel;
import com.ihg.automation.selenium.gwt.components.utils.ByText;
import com.ihg.automation.selenium.gwt.components.utils.FindStep;

public abstract class PersonNameFields extends CustomContainerBase
        implements Populate<GuestName>, VerifyMultiMode<GuestName>
{
    private NameConfigurationBean nameConfiguration;
    private FindStep step;

    public PersonNameFields(Container container, FindStep step)
    {
        super(container);
        this.step = step;
        setNameConfiguration(NameConfiguration.getDefaultConfiguration());
    }

    public PersonNameFields(Container container, Country country, FindStep step)
    {
        this(container, step);
        setConfiguration(country);
    }

    public NameConfigurationBean getNameConfiguration()
    {
        if (nameConfiguration == null)
        {
            throw new RuntimeException("Name config is not set!");
        }
        return nameConfiguration;
    }

    public void setNameConfiguration(NameConfigurationBean config)
    {
        this.nameConfiguration = config;
    }

    public void setConfiguration(Country country)
    {
        this.nameConfiguration = NameConfiguration.getNameConfiguration(country);
    }

    public Label getFullName()
    {
        return container.getLabel("Name");
    }

    public Label getFullNativeName()
    {
        return container.getLabel("Native Name");
    }

    public Select getSalutation(SalutationLabel salutation)
    {
        return container.getSelectByTopLabel(salutation.getLabel());
    }

    public Input getNativeSalutation(SalutationLabel salutation)
    {
        return new NativeNameLabel(container, ByText.exact(salutation.getLabel()), step).getInput();
    }

    public Select getSalutation()
    {
        return getSalutation(nameConfiguration.getSalutation());
    }

    public Input getNativeSalutation()
    {
        return getNativeSalutation(nameConfiguration.getSalutation());
    }

    public Input getGivenName(GivenLabel name)
    {
        return container.getInputByTopLabel(name.getLabel());
    }

    public Input getNativeGivenName(GivenLabel name)
    {
        return new NativeNameLabel(container, ByText.exact(name.getLabel()), step).getInput();
    }

    public Input getGivenName()
    {
        return getGivenName(nameConfiguration.getGiven());
    }

    public Input getNativeGivenName()
    {
        return getNativeGivenName(nameConfiguration.getGiven());
    }

    public Input getMiddleName(MiddleLabel name)
    {
        return container.getInputByTopLabel(name.getLabel());
    }

    public Input getNativeMiddleName(MiddleLabel name)
    {
        return new NativeNameLabel(container, ByText.exact(name.getLabel()), step).getInput();
    }

    public Input getMiddleName()
    {
        return getMiddleName(nameConfiguration.getMiddle());
    }

    public Input getNativeMiddleName()
    {
        return getNativeMiddleName(nameConfiguration.getMiddle());
    }

    public Input getSurname(SurnameLabel name)
    {
        return container.getInputByTopLabel(name.getLabel());
    }

    public Input getNativeSurname(SurnameLabel name)
    {
        return new NativeNameLabel(container, ByText.exact(name.getLabel()), step).getInput();
    }

    public Input getSurname()
    {
        return getSurname(nameConfiguration.getSurname());
    }

    public Input getNativeSurname()
    {
        return getNativeSurname(nameConfiguration.getSurname());
    }

    public Select getSuffix()
    {
        return container.getSelectByTopLabel("Suffix");
    }

    public Input getNativeSuffix()
    {
        return new NativeNameLabel(container, ByText.exact("Suffix"), step).getInput();
    }

    public Select getTitle(TitleLabel title)
    {
        return container.getSelectByTopLabel(title.getLabel());
    }

    public Input getNativeTitle(TitleLabel title)
    {
        return new NativeNameLabel(container, ByText.exact(title.getLabel()), step).getInput();
    }

    public Select getTitle()
    {
        return getTitle(nameConfiguration.getTitle());
    }

    public Input getNativeTitle()
    {
        return getNativeTitle(nameConfiguration.getTitle());
    }

    public Select getDegree(DegreeLabel degree)
    {
        return container.getSelectByTopLabel(degree.getLabel());
    }

    public Input getNativeDegree(DegreeLabel degree)
    {
        return new NativeNameLabel(container, ByText.exact(degree.getLabel()), step).getInput();
    }

    public Select getDegree()
    {
        return getDegree(nameConfiguration.getDegree());
    }

    public Input getNativeDegree()
    {
        return getNativeDegree(nameConfiguration.getDegree());
    }

    public Input getNickName()
    {
        return container.getInputByTopLabel("Nickname");
    }

    public Select getNativeLanguage()
    {
        return container.getSelectByLabel("Native Language");
    }

    public Link getTransliterate()
    {
        return new Link(container, "Transliterate", Link.LINK_PATTERN_WITH_TEXT_NO_HYPER);
    }

    @Override
    public void populate(GuestName name)
    {
        if (name == null)
        {
            return;
        }
        getSalutation().select(name.getSalutation());
        getGivenName().type(name.getGiven());
        getMiddleName().type(name.getMiddle());
        getSurname().type(name.getSurname());
        getSuffix().select(name.getSuffix());
        getTitle().select(name.getTitle());
        getDegree().select(name.getDegree());
    }

    public void populateNativeFields(GuestName name)
    {
        if (name == null)
        {
            return;
        }
        if (name.getNativeName() != null)
        {
            getNativeLanguage().select(name.getNativeName().getNativeLanguage());

            getNativeSalutation().type(name.getNativeName().getSalutation());
            getNativeGivenName().type(name.getNativeName().getGiven());
            getNativeMiddleName().type(name.getNativeName().getMiddle());
            getNativeSurname().type(name.getNativeName().getSurname());
            getNativeSuffix().type(name.getNativeName().getSuffix());
            getNativeTitle().type(name.getNativeName().getTitle());
            getNativeDegree().type(name.getNativeName().getDegree());
        }

        getTransliterate().clickAndWait();
    }

    @Override
    public void verify(GuestName name, Mode mode)
    {
        if (name == null)
        {
            return;
        }

        if (mode == Mode.EDIT)
        {
            verifyThat(getSalutation(), hasText(name.getSalutation()));
            verifyThat(getGivenName(), hasText(name.getGiven()));
            verifyThat(getMiddleName(), hasText(name.getMiddle()));
            verifyThat(getSurname(), hasText(name.getSurname()));
            verifyThat(getSuffix(), hasText(name.getSuffix()));
            verifyThat(getTitle(), hasText(name.getTitle()));
            verifyThat(getDegree(), hasText(name.getDegree()));
        }
        else if (mode == Mode.VIEW)
        {
            verifyThat(getFullName(), hasText(name.getFullName(nameConfiguration)));

            if (name.getNativeName() != null)
            {
                verifyThat(getFullNativeName(), hasText(name.getNativeName().getFullName(nameConfiguration)));
            }
        }

    }
}
