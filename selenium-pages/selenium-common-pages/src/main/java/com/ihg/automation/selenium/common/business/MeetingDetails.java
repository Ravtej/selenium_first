package com.ihg.automation.selenium.common.business;

public class MeetingDetails
{
    private String startDate;
    private String endDate;
    private int numberOfMeetingRooms;
    private int totalDelegates;
    private BusinessRevenueBean meetingRoom;
    private BusinessRevenueBean foodBeverage;
    private BusinessRevenueBean miscellaneous;
    private BusinessRevenueBean totalMeetingRevenue;

    public String getStartDate()
    {
        return startDate;
    }

    public void setStartDate(String startDate)
    {
        this.startDate = startDate;
    }

    public String getEndDate()
    {
        return endDate;
    }

    public void setEndDate(String endDate)
    {
        this.endDate = endDate;
    }

    public int getNumberOfMeetingRooms()
    {
        return numberOfMeetingRooms;
    }

    public void setNumberOfMeetingRooms(int numberOfMeetingRooms)
    {
        this.numberOfMeetingRooms = numberOfMeetingRooms;
    }

    public int getTotalDelegates()
    {
        return totalDelegates;
    }

    public void setTotalDelegates(int totalDelegates)
    {
        this.totalDelegates = totalDelegates;
    }

    public BusinessRevenueBean getMeetingRoom()
    {
        return meetingRoom;
    }

    public void setMeetingRoom(BusinessRevenueBean meetingRoom)
    {
        this.meetingRoom = meetingRoom;
    }

    public BusinessRevenueBean getFoodBeverage()
    {
        return foodBeverage;
    }

    public void setFoodBeverage(BusinessRevenueBean foodBeverage)
    {
        this.foodBeverage = foodBeverage;
    }

    public BusinessRevenueBean getMiscellaneous()
    {
        return miscellaneous;
    }

    public void setMiscellaneous(BusinessRevenueBean miscellaneous)
    {
        this.miscellaneous = miscellaneous;
    }

    public BusinessRevenueBean getTotalMeetingRevenue()
    {
        return totalMeetingRevenue;
    }

    public void setTotalMeetingRevenue(BusinessRevenueBean totalMeetingRevenue)
    {
        this.totalMeetingRevenue = totalMeetingRevenue;
    }
}
