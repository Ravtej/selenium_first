package com.ihg.automation.selenium.common.event;

import com.ihg.automation.common.DateUtils;

public class AllEventsRow
{
    private String transDate = "";
    private String transType = "";
    private String detail = "";
    private String ihgUnits;
    private String allianceUnits;

    private AllEventsRow()
    {
    }

    public AllEventsRow(String transDate, String transType, String detail, String ihgUnits, String allianceUnits)
    {
        this.transDate = transDate;
        this.transType = transType;
        this.detail = detail;
        this.ihgUnits = ihgUnits;
        this.allianceUnits = allianceUnits;
    }

    public AllEventsRow(String transDate, String transType, String detail)
    {
        this(transDate, transType, detail, null, null);
    }

    public AllEventsRow(String transType, String detail)
    {
        this(DateUtils.getFormattedDate(), transType, detail, null, null);
    }

    public void setTransDate(String transDate)
    {
        this.transDate = transDate;
    }

    public String getTransDate()
    {
        return transDate;
    }

    public void setTransType(String transType)
    {
        this.transType = transType;
    }

    public String getTransType()
    {
        return transType;
    }

    public void setIhgUnits(String ihgUnits)
    {
        this.ihgUnits = ihgUnits;
    }

    public String getIhgUnits()
    {
        return ihgUnits;
    }

    public void setDetail(String detail)
    {
        this.detail = detail;
    }

    public String getDetail()
    {
        return detail;
    }

    public void setAllianceUnits(String allianceUnits)
    {
        this.allianceUnits = allianceUnits;
    }

    public String getAllianceUnits()
    {
        return allianceUnits;
    }

}
