package com.ihg.automation.selenium.common.types.address;

import com.ihg.automation.selenium.common.types.LabelEnumAware;

public enum RegionLabel implements LabelEnumAware
{
    AREA("Area"), REGION("Region"), ISLAND("Island"), COUNTY("County"), GOVERNORATE("Governorate"), STATE_REGION(
            "State/Region"), DIVISION("Division"), TERRITORY("Territory"), CANTON("Canton"), PARISH("Parish"), COUNCIL(
            "Council"), PROVINCE("Province"), MUNICIPALITY("Municipality"), DISTRICT("District"), STATE("State"), COUNTY_AREA(
            "County/Area"), DEPARTMENT("Department"), STATE_TERRITORY("State/Territory");

    private String label;

    private RegionLabel(String label)
    {
        this.label = label;
    }

    @Override
    public String getLabel()
    {
        return label;
    }
}
