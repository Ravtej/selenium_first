package com.ihg.automation.selenium.common.member;

import static com.ihg.automation.selenium.common.Constant.LINE_SEPARATOR;

import com.ihg.automation.selenium.common.components.Country;
import com.ihg.automation.selenium.gwt.components.EntityType;

public class GuestAddress extends GuestContact implements Cloneable
{
    private boolean doNotChangeLock = false;

    // Common address fields
    private String formatted;
    private String line1;
    private String line2;
    private String line3;
    private String line4;
    private String line5;
    private String locality1;
    private String locality2;
    private Country country;
    private EntityType region1;
    private String region2;
    private String postalCode;
    private String companyName;
    private GuestAddress nativeAddress;

    public boolean isDoNotChangeLock()
    {
        return doNotChangeLock;
    }

    public void setDoNotChangeLock(boolean doNotChangeLock)
    {
        this.doNotChangeLock = doNotChangeLock;
    }

    public String getFormatted()
    {
        return formatted;
    }

    public void setFormatted(String formatted)
    {
        this.formatted = formatted;
    }

    public String getLine1()
    {
        return line1;
    }

    public void setLine1(String line1)
    {
        this.line1 = line1;
    }

    public String getLine2()
    {
        return line2;
    }

    public void setLine2(String line2)
    {
        this.line2 = line2;
    }

    public String getLine3()
    {
        return line3;
    }

    public void setLine3(String line3)
    {
        this.line3 = line3;
    }

    public String getLine4()
    {
        return line4;
    }

    public void setLine4(String line4)
    {
        this.line4 = line4;
    }

    public String getLine5()
    {
        return line5;
    }

    public void setLine5(String line5)
    {
        this.line5 = line5;
    }

    public String getLocality1()
    {
        return locality1;
    }

    public void setLocality1(String locality1)
    {
        this.locality1 = locality1;
    }

    public String getLocality2()
    {
        return locality2;
    }

    public void setLocality2(String locality2)
    {
        this.locality2 = locality2;
    }

    public Country getCountry()
    {
        return country;
    }

    public void setCountryCode(Country country)
    {
        this.country = country;
    }

    public EntityType getRegion1()
    {
        return region1;
    }

    public void setRegion1(EntityType region1)
    {
        this.region1 = region1;
    }

    public String getRegion2()
    {
        return region2;
    }

    public void setRegion2(String region2)
    {
        this.region2 = region2;
    }

    public String getPostalCode()
    {
        return postalCode;
    }

    public void setPostalCode(String postalCode)
    {
        this.postalCode = postalCode;
    }

    public String getCompanyName()
    {
        return companyName;
    }

    public void setCompanyName(String companyName)
    {
        this.companyName = companyName;
    }

    public GuestAddress getNativeAddress()
    {
        return nativeAddress;
    }

    public void setNativeAddress(GuestAddress nativeAddress)
    {
        this.nativeAddress = nativeAddress;
    }

    @Override
    public GuestAddress clone()
    {
        GuestAddress newAddress = new GuestAddress();

        newAddress.type = type;
        newAddress.type = type;

        newAddress.doNotChangeLock = doNotChangeLock;
        newAddress.formatted = formatted;
        newAddress.line1 = line1;
        newAddress.line2 = line2;
        newAddress.line3 = line3;
        newAddress.line4 = line4;
        newAddress.line5 = line5;
        newAddress.locality1 = locality1;
        newAddress.country = country;
        newAddress.region1 = region1;
        newAddress.region2 = region2;
        newAddress.postalCode = postalCode;
        newAddress.postalCode = postalCode;

        return newAddress;
    }

    public String getFormattedAddress()
    {
        StringBuilder builder = new StringBuilder();
        builder.append(getLine1()).append(LINE_SEPARATOR);
        builder.append(getLocality1()).append(", ").append(getRegion1()).append(LINE_SEPARATOR);
        builder.append(getPostalCode()).append(LINE_SEPARATOR);
        builder.append(getCountry().getValue());

        return builder.toString();
    }

    public String getCollapseAddress()
    {
        StringBuilder builder = new StringBuilder();
        builder.append(getLine1()).append(LINE_SEPARATOR);
        builder.append(getLocality1()).append(LINE_SEPARATOR).append(",").append(LINE_SEPARATOR).append(getRegion1());

        if (null != getNativeAddress())
        {
            builder.append(LINE_SEPARATOR).append(getNativeAddress().getLine1()).append(LINE_SEPARATOR);
            builder.append(getNativeAddress().getLocality1()).append(LINE_SEPARATOR).append(",").append(LINE_SEPARATOR)
                    .append(getNativeAddress().getRegion1());
        }

        return builder.toString();
    }
}
