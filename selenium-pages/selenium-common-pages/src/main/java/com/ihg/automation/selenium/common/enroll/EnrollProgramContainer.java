package com.ihg.automation.selenium.common.enroll;

import static com.ihg.automation.selenium.gwt.components.utils.FindStepUtils.byXpathWithWait;
import static java.text.MessageFormat.format;

import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.gwt.components.Component;
import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.CustomContainerBase;
import com.ihg.automation.selenium.gwt.components.checkbox.CheckBox;
import com.ihg.automation.selenium.gwt.components.utils.ByText;

public class EnrollProgramContainer extends CustomContainerBase
{
    public EnrollProgramContainer(Container container)
    {
        super(container.getSeparator("Programs"));
    }

    public CheckBox getProgramCheckbox(Program program)
    {
        return container.getCheckBox(program.getCode());
    }

    public Component getProgramChecked(Program program)
    {
        return new Component(container, "Program", program.getCode(),
                byXpathWithWait(
                        format("//td[child::span[{0}]]//preceding-sibling::td/div[contains(@class,'program_label')]",
                                ByText.exact(program.getCode()))));
    }

    public void selectProgram(Program program)
    {
        getProgramCheckbox(program).check();
    }
}
