package com.ihg.automation.selenium.common.business;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static com.ihg.automation.selenium.matchers.text.EntityMatcherFactory.isValue;

import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.CustomContainerBase;
import com.ihg.automation.selenium.gwt.components.input.DateInput;
import com.ihg.automation.selenium.gwt.components.input.Input;
import com.ihg.automation.selenium.gwt.components.input.SearchInput;
import com.ihg.automation.selenium.gwt.components.input.Select;
import com.ihg.automation.selenium.gwt.components.label.Label;

public class EventInformationFields extends CustomContainerBase
{
    public EventInformationFields(Container container)
    {
        super(container);
    }

    public Input getMemberNumber()
    {
        return container.getInputByLabel("Member Number");
    }

    public SearchInput getHotel()
    {
        return new SearchInput(container.getLabel("Hotel"));
    }

    public DateInput getEventContractDate()
    {
        return new DateInput(container.getLabel("Event Contract Date"));
    }

    public Select getEventType()
    {
        return container.getSelectByLabel("Event Type");
    }

    public Input getEventName()
    {
        return container.getInputByLabel("Event Name");
    }

    public Input getHotelContactName()
    {
        return container.getInputByLabel("Hotel Contact Name");
    }

    public Input getHotelContactEmail()
    {
        return container.getInputByLabel("Hotel Contact Email");
    }

    public Label getHotelCurrency()
    {
        return container.getLabel("Hotel Currency");
    }

    public Input getHotelContactPhone()
    {
        return container.getInputByLabel("Hotel Contact Phone");
    }

    public void populate(BusinessEvent businessEvent)
    {
        getHotel().typeAndWait(businessEvent.getHotel());
        getEventName().typeAndWait(businessEvent.getEventName());
        verifyThat(getHotelCurrency(), hasText(isValue(businessEvent.getHotelCurrency())));
    }

    public void populateHotelEvent(BusinessEvent businessEvent)
    {
        getMemberNumber().typeAndWait(businessEvent.getMember().getBRProgramId());
        getEventName().typeAndWait(businessEvent.getEventName());
        verifyThat(getHotelCurrency(), hasText(isValue(businessEvent.getHotelCurrency())));
    }
}
