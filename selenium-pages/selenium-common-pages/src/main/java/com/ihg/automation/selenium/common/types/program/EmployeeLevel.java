package com.ihg.automation.selenium.common.types.program;

public enum EmployeeLevel implements ProgramLevel
{
    EMPL("EMPL", "RC Employee Member");

    private String code;
    private String value;

    private EmployeeLevel(String code, String description)
    {
        this.code = code;
        this.value = description;
    }

    @Override
    public String getCode()
    {
        return code;
    }

    @Override
    public String getValue()
    {
        return value;
    }

    @Override
    public String getCodeWithValue()
    {
        return code + " - " + value;
    }
}
