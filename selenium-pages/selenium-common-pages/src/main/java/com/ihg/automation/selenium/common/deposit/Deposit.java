package com.ihg.automation.selenium.common.deposit;

import com.ihg.automation.common.DateUtils;
import com.ihg.automation.selenium.common.Constant;
import com.ihg.automation.selenium.common.types.Partner;

public class Deposit
{
    private String checkInDate;
    private String checkOutDate;
    private String hotel;
    private String loyaltyUnitsAmount;
    private String loyaltyUnitsType;
    private String orderTrackingNumber;
    private String partnerComment;
    private String customerServiceComment = Constant.NOT_AVAILABLE;
    private String partnerTransactionDate;
    private String transactionId;
    private String reason = Constant.NOT_AVAILABLE;
    private String allianceLoyaltyUnitsAmount;

    private String transactionDescription = Constant.NOT_AVAILABLE;
    private String transactionDate = DateUtils.getFormattedDate();
    private String transactionName = Constant.NOT_AVAILABLE;
    private String transactionType = Constant.NOT_AVAILABLE;
    private String catalogItem;
    private String tierLevel;

    /**
     * Uses only for CO-PARTNER event
     * <i>(RULE.DEP_EVN_CONFIG.CO_PARTNER_KEY)</i>
     **/
    private Partner coPartner;
    /**
     * Can be <i>hotelCode</i>, <i>partner id</i> or other entity described in
     * <i>RULE.DEP_EVN_CONFIG.BILL_ENT_TYP</i>
     **/
    private String billingTo;

    public void setCheckInDate(String checkInDate)
    {
        this.checkInDate = checkInDate;
    }

    public String getCheckInDate()
    {
        return checkInDate;
    }

    public void setCheckOutDate(String checkOutDate)
    {
        this.checkOutDate = checkOutDate;
    }

    public String getCheckOutDate()
    {
        return checkOutDate;
    }

    public void setLoyaltyUnitsAmount(String loyaltyUnitsAmount)
    {
        this.loyaltyUnitsAmount = loyaltyUnitsAmount;
    }

    public String getLoyaltyUnitsAmount()
    {
        return loyaltyUnitsAmount;
    }

    public void setHotel(String hotel)
    {
        this.hotel = hotel;
    }

    public String getHotel()
    {
        return hotel;
    }

    public void setLoyaltyUnitsType(String loyaltyUnitsType)
    {
        this.loyaltyUnitsType = loyaltyUnitsType;
    }

    public String getLoyaltyUnitsType()
    {
        return loyaltyUnitsType;
    }

    public void setPartnerComment(String partnerComment)
    {
        this.partnerComment = partnerComment;
    }

    public String getPartnerComment()
    {
        return partnerComment;
    }

    public String getCustomerServiceComment()
    {
        return customerServiceComment;
    }

    public void setCustomerServiceComment(String customerServiceComment)
    {
        this.customerServiceComment = customerServiceComment;
    }

    public void setOrderTrackingNumber(String orderTrackingNumber)
    {
        this.orderTrackingNumber = orderTrackingNumber;
    }

    public String getOrderTrackingNumber()
    {
        return orderTrackingNumber;
    }

    public void setPartnerTransactionDate(String partnerTransactionDate)
    {
        this.partnerTransactionDate = partnerTransactionDate;
    }

    public String getPartnerTransactionDate()
    {
        return partnerTransactionDate;
    }

    public void setTransactionId(String transactionId)
    {
        this.transactionId = transactionId;
    }

    public String getTransactionId()
    {
        return transactionId;
    }

    public String getAllianceLoyaltyUnitsAmount()
    {
        return allianceLoyaltyUnitsAmount;
    }

    public void setAllianceLoyaltyUnitsAmount(String allianceLoyaltyUnitsAmount)
    {
        this.allianceLoyaltyUnitsAmount = allianceLoyaltyUnitsAmount;
    }

    public void setTransactionDescription(String transactionDescription)
    {
        this.transactionDescription = transactionDescription;
    }

    public String getTransactionDescription()
    {
        return transactionDescription;
    }

    public void setTransactionName(String transactionName)
    {
        this.transactionName = transactionName;
    }

    public String getTransactionName()
    {
        return transactionName;
    }

    public void setTransactionType(String transactionType)
    {
        this.transactionType = transactionType;
    }

    public String getTransactionType()
    {
        return transactionType;
    }

    public void setReason(String reason)
    {
        this.reason = reason;
    }

    public String getReason()
    {
        return reason;
    }

    public void setCatalogItem(String catalogItem)
    {
        this.catalogItem = catalogItem;
    }

    public String getCatalogItem()
    {
        return catalogItem;
    }

    public void setTierLevel(String tierLevel)
    {
        this.tierLevel = tierLevel;
    }

    public String getTierLevel()
    {
        return tierLevel;
    }

    public String getTransactionDate()
    {
        return transactionDate;
    }

    public void setTransactionDate(String transactionDate)
    {
        this.transactionDate = transactionDate;
    }

    public Partner getCoPartner()
    {
        return coPartner;
    }

    public void setCoPartner(Partner coPartner)
    {
        this.coPartner = coPartner;
    }

    public String getBillingTo()
    {
        return billingTo;
    }

    public void setBillingTo(String billingTo)
    {
        this.billingTo = billingTo;
    }

    public void setBillingTo(Partner billingTo)
    {
        this.billingTo = billingTo.getCode();
    }
}
