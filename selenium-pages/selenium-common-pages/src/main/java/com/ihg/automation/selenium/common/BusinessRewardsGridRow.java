package com.ihg.automation.selenium.common;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.gwt.components.utils.FindStepUtils.byXpath;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasText;

import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.gwt.components.Component;

public class BusinessRewardsGridRow extends ProgramGridRow
{
    public static final String STATUS = "Status";

    protected BusinessRewardsGridRow(ProgramsGrid parent)
    {
        super(parent, Program.BR);
    }

    @Override
    public Component getLevelCode()
    {
        return new Component(levelCode, levelCode.getType(), levelCode.getName(), byXpath(".//tr[1]"));
    }

    public Component getStatus()
    {
        return new Component(levelCode, levelCode.getType(), "Status", byXpath(".//tr[2]"));
    }

    private void verify(Program program, String status, String pointsBalance)
    {
        verifyThat(getLevelCode(), hasText(program.getCode()));
        verifyThat(getStatus(), hasText(status));
    }
}
