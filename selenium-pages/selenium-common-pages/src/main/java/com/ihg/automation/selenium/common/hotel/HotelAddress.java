package com.ihg.automation.selenium.common.hotel;

import com.ihg.automation.selenium.common.components.Country;
import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.CustomContainerBase;
import com.ihg.automation.selenium.gwt.components.Separator;
import com.ihg.automation.selenium.gwt.components.input.Input;
import com.ihg.automation.selenium.gwt.components.input.Select;

public class HotelAddress extends CustomContainerBase
{
    public HotelAddress(Container container)
    {
        super(container.getSeparator("Address", Separator.FIELDSET_PATTERN_NEXT_TR));
    }

    public Input getAddress()
    {
        return container.getInputByLabel("Address");
    }

    public Select getState()
    {
        return container.getSelectByLabel("State/Province");
    }

    public Input getCity()
    {
        return container.getInputByLabel("City");
    }

    public Select getCountry()
    {
        return container.getSelectByLabel("Country");
    }

    public Input getPostalCode()
    {
        return container.getInputByLabel("Zip/Postal");
    }

    public void populateCountryAndState(Country country, String state) throws InterruptedException
    {
        getCountry().selectByCode(country);

        // since there were no maskers applied on UI side, we use Thread.sleep()
        // to make sure data from Refdata Service was received
        Thread.sleep(1000);
        getState().select(state);
    }
}
