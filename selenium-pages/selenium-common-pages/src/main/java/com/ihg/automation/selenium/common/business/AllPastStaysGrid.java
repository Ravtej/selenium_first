package com.ihg.automation.selenium.common.business;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.business.AllPastStaysGridRow.AllPastStaysCell.CHECK_IN;
import static com.ihg.automation.selenium.common.business.AllPastStaysGridRow.AllPastStaysCell.CONF_NUMBER;
import static com.ihg.automation.selenium.common.business.AllPastStaysGridRow.AllPastStaysCell.EVENT_ID;
import static com.ihg.automation.selenium.common.business.AllPastStaysGridRow.AllPastStaysCell.GUEST_NAME;
import static com.ihg.automation.selenium.common.business.AllPastStaysGridRow.AllPastStaysCell.MEMBER_NUMBER;
import static com.ihg.automation.selenium.common.business.AllPastStaysGridRow.AllPastStaysCell.NIGHTS;
import static com.ihg.automation.selenium.common.business.AllPastStaysGridRow.AllPastStaysCell.RATE_CODE;
import static com.ihg.automation.selenium.common.business.AllPastStaysGridRow.AllPastStaysCell.ROOM_NUMBER;
import static com.ihg.automation.selenium.common.business.AllPastStaysGridRow.AllPastStaysCell.ROOM_RATE;

import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.grid.Grid;
import com.ihg.automation.selenium.gwt.components.grid.GridHeader;
import com.ihg.automation.selenium.gwt.components.grid.GridRowXpath;
import com.ihg.automation.selenium.matchers.component.HasText;

public class AllPastStaysGrid extends Grid<AllPastStaysGridRow, AllPastStaysGridRow.AllPastStaysCell>
{
    public AllPastStaysGrid(Container parent)
    {
        super(parent);
    }

    public AllPastStaysGridRow getRow(String eventID)
    {
        return getRow(new GridRowXpath.GridRowXpathBuilder().cell(EVENT_ID, eventID).build());
    }

    public void verifyHeader()
    {
        GridHeader<AllPastStaysGridRow.AllPastStaysCell> header = getHeader();
        verifyThat(header.getCell(GUEST_NAME), HasText.hasText(GUEST_NAME.getValue()));
        verifyThat(header.getCell(EVENT_ID), HasText.hasText(EVENT_ID.getValue()));
        verifyThat(header.getCell(MEMBER_NUMBER), HasText.hasText(MEMBER_NUMBER.getValue()));
        verifyThat(header.getCell(CONF_NUMBER), HasText.hasText(CONF_NUMBER.getValue()));
        verifyThat(header.getCell(CHECK_IN), HasText.hasText(CHECK_IN.getValue()));
        verifyThat(header.getCell(NIGHTS), HasText.hasText(NIGHTS.getValue()));
        verifyThat(header.getCell(ROOM_NUMBER), HasText.hasText(ROOM_NUMBER.getValue()));
        verifyThat(header.getCell(RATE_CODE), HasText.hasText(RATE_CODE.getValue()));
        verifyThat(header.getCell(ROOM_RATE), HasText.hasText(ROOM_RATE.getValue()));
    }
}
