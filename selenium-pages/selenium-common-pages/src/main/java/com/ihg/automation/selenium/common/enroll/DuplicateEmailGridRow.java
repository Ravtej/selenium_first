package com.ihg.automation.selenium.common.enroll;

import com.ihg.automation.selenium.gwt.components.grid.CellsName;
import com.ihg.automation.selenium.gwt.components.grid.GridRow;
import com.ihg.automation.selenium.gwt.components.grid.GridRowXpath;

public class DuplicateEmailGridRow extends GridRow<DuplicateEmailGridRow.DuplicateEmailCell>
{

    public DuplicateEmailGridRow(DuplicateEmailGrid parent, GridRowXpath xpath)
    {
        super(parent, xpath);
    }

    public enum DuplicateEmailCell implements CellsName
    {
        EXPANDER("expander"), MBR_NUMBER("membership.memberId"), GUEST_NAME("name"), ADDRESS(
                "address.addressLine"), CITY("address.locality1"), STATE(
                        "address.region1"), COUNTRY("address.countryCode"), PHONE("phone"), EMAIL("email");

        private String name;

        private DuplicateEmailCell(String name)
        {
            this.name = name;
        }

        @Override
        public String getName()
        {
            return name;
        }
    }
}
