package com.ihg.automation.selenium.common.name;

import static com.ihg.automation.selenium.gwt.components.utils.FindStepUtils.byXpath;

import com.ihg.automation.selenium.common.components.Country;
import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.utils.ComponentFunctionUtils;
import com.ihg.automation.selenium.gwt.components.utils.FindStep;

public class PersonNameCustomerFields extends PersonNameFields
{
    private static final FindStep GO_TO_LABEL = byXpath(
            "./ancestor::tr//td[child::*[normalize-space(text())=''Native Name'']]/following-sibling::td//tr[2]//td[{0}]",
            ComponentFunctionUtils.getPosition());

    public PersonNameCustomerFields(Container container)
    {
        super(container, GO_TO_LABEL);
    }

    public PersonNameCustomerFields(Container container, Country country)
    {
        super(container, country, GO_TO_LABEL);
    }

}
