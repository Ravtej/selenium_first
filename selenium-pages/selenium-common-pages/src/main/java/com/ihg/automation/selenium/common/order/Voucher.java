package com.ihg.automation.selenium.common.order;

import org.apache.commons.lang3.StringUtils;

import com.ihg.automation.common.DateUtils;
import com.ihg.automation.selenium.common.Constant;
import com.ihg.automation.selenium.common.SiteHelperBase;

public class Voucher
{
    private String voucherNumber;
    private String itemName;
    private String memberFirstName;
    private String memberLastName;

    private String hotelMembershipId;

    private String status;
    private String eventDate = DateUtils.getFormattedDate();
    private String updatedDate = DateUtils.getFormattedDate();
    private String updatedBy;
    private String department = StringUtils.EMPTY;
    private String location = StringUtils.EMPTY;

    public Voucher(String voucherNumber, String itemName, VoucherStatusType status, SiteHelperBase helper)
    {
        this.voucherNumber = voucherNumber;
        this.itemName = itemName;
        this.status = status.name();
        this.updatedBy = helper.getUser().getUserId();
    }

    public String getVoucherNumber()
    {
        return voucherNumber;
    }

    public void setVoucherNumber(String voucherNumber)
    {
        this.voucherNumber = voucherNumber;
    }

    public String getItemName()
    {
        return itemName;
    }

    public void setItemName(String itemName)
    {
        this.itemName = itemName;
    }

    public String getMemberFirstName()
    {
        return memberFirstName;
    }

    public String getMemberLastName()
    {
        return memberLastName;
    }

    public void setMemberName(String memberFirstName, String memberLastName)
    {
        this.memberFirstName = memberFirstName;
        this.memberLastName = memberLastName;
    }

    public String getMemberName()
    {
        if (StringUtils.isEmpty(memberFirstName) && StringUtils.isEmpty(memberLastName))
        {
            return Constant.NOT_AVAILABLE;
        }

        return String.format("%s %s", memberFirstName, memberLastName);
    }

    public String getHotelMembershipId()
    {
        return hotelMembershipId;
    }

    public void setHotelMembershipId(String hotelMembershipId)
    {
        this.hotelMembershipId = hotelMembershipId;
    }

    public String getStatus()
    {
        return status;
    }

    public void setStatus(String status)
    {
        this.status = status;
    }

    public String getEventDate()
    {
        return eventDate;
    }

    public void setEventDate(String eventDate)
    {
        this.eventDate = eventDate;
    }

    public String getUpdatedDate()
    {
        return updatedDate;
    }

    public void setUpdatedDate(String updatedDate)
    {
        this.updatedDate = updatedDate;
    }

    public String getUpdatedBy()
    {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy)
    {
        this.updatedBy = updatedBy;
    }

    public String getDepartment()
    {
        return department;
    }

    public void setDepartment(String department)
    {
        this.department = department;
    }

    public String getLocation()
    {
        return location;
    }

    public void setLocation(String location)
    {
        this.location = location;
    }

}
