package com.ihg.automation.selenium.common.member;

import com.ihg.automation.selenium.common.types.Alliance;

public class MemberAlliance
{
    private Alliance alliance;
    private String number;
    private GuestName name;

    public MemberAlliance(Alliance alliance, String number, GuestName name)
    {
        this.alliance = alliance;
        this.number = number;
        this.name = name;
    }

    public MemberAlliance(Alliance alliance, String number)
    {
        this.alliance = alliance;
        this.number = number;
    }

    public Alliance getAlliance()
    {
        return alliance;
    }

    public void setAlliance(Alliance alliance)
    {
        this.alliance = alliance;
    }

    public String getNumber()
    {
        return number;
    }

    public void setNumber(String number)
    {
        this.number = number;
    }

    public GuestName getName()
    {
        return name;
    }

    public void setName(GuestName name)
    {
        this.name = name;
    }
}
