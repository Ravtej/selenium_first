package com.ihg.automation.selenium.common.member;

import static com.ihg.automation.selenium.common.catalog.CatalogItem.builder;
import static com.ihg.automation.selenium.common.types.Currency.USD;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.ihg.automation.common.DateUtils;
import com.ihg.automation.selenium.common.catalog.CatalogItem;

public class AmbassadorUtils
{
    public static final CatalogItem NEW_GOLD_AMBASSADOR_KIT = builder("FGA00", "NEW GOLD AMBASSADOR KIT")
            .awardCost("0", USD).buyerCost("0", USD).build();

    // Enroll awards
    public static final CatalogItem AMBASSADOR_$200 = builder("AB100", "AMBASSADOR $200").awardCost("200", USD)
            .buyerCost("200", USD).build();

    public static final CatalogItem AMBASSADOR_CROSS_CHARGE_$150 = builder("AB220", "AMBASSADOR CROSS CHARGE $150")
            .awardCost("150", USD).buyerCost("150", USD).build();

    public static final CatalogItem AMBASSADOR_COMPLIMENTARY_$0 = builder("AB200", "AMBASSADOR COMPLIMENTARY $0")
            .awardCost("0", USD).buyerCost("0", USD).build();

    public static final CatalogItem AMBASSADOR_COMP_PROMO_$0 = builder("AB210", "AMBASSADOR COMP PROMO $0")
            .awardCost("0", USD).buyerCost("0", USD).build();

    public static final CatalogItem AMBASSADOR_32000_POINTS = builder("AB300", "AMBASSADOR 32000 POINTS")
            .loyaltyUnitCost("32000").awardCost("200", USD).buyerCost("0", USD).build();

    public static final CatalogItem AMBASSADOR_CORP_DISC_$150 = builder("AB175", "AMBASSADOR CORP DISC $150")
            .awardCost("150", USD).buyerCost("150", USD).build();

    public static final List<String> ENROLL_AWARD_NAME_LIST = new ArrayList<>(
            Arrays.asList("JCB Ambassador Enrollment", "Citic Ambassador Enrollment",
                    AMBASSADOR_32000_POINTS.getItemDescription(), AMBASSADOR_CROSS_CHARGE_$150.getItemDescription(),
                    AMBASSADOR_COMP_PROMO_$0.getItemDescription(), AMBASSADOR_COMPLIMENTARY_$0.getItemDescription(),
                    AMBASSADOR_CORP_DISC_$150.getItemDescription(), AMBASSADOR_$200.getItemDescription()));
    // Enroll awards Loyalty Connect UI
    public static final CatalogItem PURCHASE_AMBASSADOR_HOTEL_$200 = builder("AA402", "PURCHASE AMBASSADOR HOTEL $200")
            .awardCost("200", USD).buyerCost("200", USD).build();

    public static final CatalogItem PURCHASE_AMBASSADOR_HOTEL_$200_GC_ONLY = builder("AA403",
            "PURCHASE AMBASSADOR HOTEL $200 (GC Only)").awardCost("173.17", USD).buyerCost("200", USD).build();

    // Renew awards
    public static final CatalogItem RENEW_GOLD_AMBASSADOR_KIT_150 = builder("FGR01", "RENEW GOLD AMBASSADOR KIT-150")
            .awardCost("0", USD).buyerCost("0", USD).build();

    public static final CatalogItem RENEW_GOLD_AMBASSADOR_KIT_200 = builder("FGR02", "RENEW GOLD AMBASSADOR KIT-200")
            .awardCost("0", USD).buyerCost("0", USD).build();

    public static final CatalogItem AR210_AMBASSADOR_RENEWAL_PROMO_$0 = builder("AR210", "AMBASSADOR RENEWAL PROMO $0")
            .awardCost("0", USD).buyerCost("0", USD).build();

    public static final CatalogItem AMBASSADOR_RENEWAL_$200 = builder("AR102", "AMBASSADOR RENEWAL $200")
            .awardCost("200", USD).buyerCost("200", USD).build();

    public static final CatalogItem AMBASSADOR_RENEWAL_PROMO_$0 = builder("FGR00", "AMBASSADOR RENEWAL PROMO $0")
            .awardCost("0", USD).buyerCost("0", USD).build();

    public static final CatalogItem AMBASSADOR_RENEWAL_24000_PTS = builder("AR600", "AMBASSADOR RENEWAL 24000 PTS")
            .itemDescription("AMBASSADOR RENEWAL 24000 POINTS").loyaltyUnitCost("24000").awardCost("150", USD)
            .buyerCost("0", USD).build();

    private static final int RENEW_SHIFT_MONTHS = 12;

    public static final String getExpirationDate()
    {
        return DateUtils.getMonthsForward(14, DateUtils.EXPIRATION_DATE_PATTERN);
    }

    public static final String getExpirationDateAfterRenew(int daysShift)
    {
        return DateUtils.now().plusDays(daysShift).plusMonths(RENEW_SHIFT_MONTHS).toString("ddMMMYY");
    }
}
