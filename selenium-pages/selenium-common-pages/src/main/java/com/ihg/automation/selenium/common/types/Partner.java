package com.ihg.automation.selenium.common.types;

import com.ihg.automation.selenium.gwt.components.EntityType;

public enum Partner implements EntityType
{
    DBG("DBG", "DBG"), //
    CPO("CPO", "CAPITAL ONE");

    private String code;
    private String value;

    Partner(String code, String value)
    {
        this.code = code;
        this.value = value;
    }

    @Override
    public String getCode()
    {
        return code;
    }

    @Override
    public String getValue()
    {
        return value;
    }

    @Override
    public String toString()
    {
        return getValue();
    }

    @Override
    public String getCodeWithValue()
    {
        return code + " - " + value;
    }
}
