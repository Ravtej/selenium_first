package com.ihg.automation.selenium.common;

public class Constant
{
    public static final String LINE_SEPARATOR = "\n";
    public static final String NOT_AVAILABLE = "n/a";
    public static final String RC_POINTS = "RC Points";
    public static final String BASE_UNITS = "Base Units";
    public static final String BASE_AWARDS = "Base Awards";
    public static final String PCR = "PCR";
    public static final String SC_LOCATION = "PCPHL";
    public final static double USD_TO_POINTS_CONVERSION_RATE = 10.00;

    private Constant()
    {
    }
}
