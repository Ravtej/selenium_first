package com.ihg.automation.selenium.common.order;

import static com.ihg.automation.common.CustomStringUtils.concat;

import java.util.ArrayList;

import com.ihg.automation.common.DateUtils;
import com.ihg.automation.selenium.common.Constant;
import com.ihg.automation.selenium.common.event.Source;
import com.ihg.automation.selenium.common.member.GuestAddress;
import com.ihg.automation.selenium.common.member.GuestEmail;
import com.ihg.automation.selenium.common.member.GuestName;
import com.ihg.automation.selenium.common.member.Member;

public class Order
{
    private String transactionDate = DateUtils.getFormattedDate();
    private String transactionType = Constant.NOT_AVAILABLE;
    private String orderNumber = Constant.NOT_AVAILABLE;
    private String totalLoyaltyUnits;
    private String orderedDate = DateUtils.getFormattedDate();
    private String channelOfOrder = Constant.NOT_AVAILABLE;
    private String orderedBy = Constant.NOT_AVAILABLE;
    private String totalAllianceUnits;

    private String name;
    private GuestAddress deliveryAddress;
    private Boolean deliveryAddressGood;
    private String deliveryEmail;
    private Boolean deliveryEmailGood;

    private ArrayList<OrderItem> orderItems = new ArrayList<OrderItem>();

    private Source source;

    public void setTransactionDate(String transactionDate)
    {
        this.transactionDate = transactionDate;
    }

    public String getTransactionDate()
    {
        return transactionDate;
    }

    public void setTransactionType(String transactionType)
    {
        this.transactionType = transactionType;
    }

    public String getTransactionType()
    {
        return transactionType;
    }

    public void setTotalLoyaltyUnits(String totalLoyaltyUnits)
    {
        this.totalLoyaltyUnits = totalLoyaltyUnits;
    }

    public String getTotalLoyaltyUnits()
    {
        return totalLoyaltyUnits;
    }

    public void setChannelOfOrder(String channelOfOrder)
    {
        this.channelOfOrder = channelOfOrder;
    }

    public String getChannelOfOrder()
    {
        return channelOfOrder;
    }

    public void setOrderNumber(String orderNumber)
    {
        this.orderNumber = orderNumber;
    }

    public String getOrderNumber()
    {
        return orderNumber;
    }

    public void setOrderedDate(String orderedDate)
    {
        this.orderedDate = orderedDate;
    }

    public String getOrderedDate()
    {
        return orderedDate;
    }

    public void setOrderedBy(String orderedBy)
    {
        this.orderedBy = orderedBy;
    }

    public String getOrderedBy()
    {
        return orderedBy;
    }

    public ArrayList<OrderItem> getOrderItems()
    {
        return orderItems;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public GuestAddress getDeliveryAddress()
    {
        return deliveryAddress;
    }

    public void setDeliveryAddress(GuestAddress deliveryAddress)
    {
        this.deliveryAddress = deliveryAddress;
    }

    public Boolean getDeliveryAddressGood()
    {
        return deliveryAddressGood;
    }

    public void setDeliveryAddressGood(Boolean deliveryAddressGood)
    {
        this.deliveryAddressGood = deliveryAddressGood;
    }

    public String getDeliveryEmail()
    {
        return deliveryEmail;
    }

    public void setDeliveryEmail(String deliveryEmail)
    {
        this.deliveryEmail = deliveryEmail;
    }

    public Boolean getDeliveryEmailGood()
    {
        return deliveryEmailGood;
    }

    public void setDeliveryEmailGood(Boolean deliveryEmailGood)
    {
        this.deliveryEmailGood = deliveryEmailGood;
    }

    /**
     * For catalog redeem order the member's name is displayed as salutation +
     * given + surname;
     *
     * For system and other redeem orders the member's name is displayed as
     * given + surname
     */
    public void setShippingInfo(Member member, boolean isCatalogRedeem)
    {
        GuestName guestName = member.getName();

        if (isCatalogRedeem)
        {
            this.name = guestName.getFullName();
        }
        else
        {
            name = concat(" ", guestName.getGiven(), guestName.getSurname());
        }

        final GuestAddress preferredAddress = member.getPreferredAddress();
        if (preferredAddress != null)
        {
            deliveryAddress = preferredAddress.clone();
            deliveryAddressGood = true;
        }

        final GuestEmail preferredEmail = member.getPreferredEmail();
        if (preferredEmail != null)
        {
            deliveryEmail = preferredEmail.getEmail();
            deliveryEmailGood = true;
        }
        else
        {
            deliveryEmail = Constant.NOT_AVAILABLE;
        }
    }

    public void setShippingInfo(Member member)
    {
        setShippingInfo(member, false);
    }

    public Source getSource()
    {
        return source;
    }

    public void setSource(Source source)
    {
        this.source = source;
    }

    public String getTotalAllianceUnits()
    {
        return totalAllianceUnits;
    }

    public void setTotalAllianceUnits(String totalAllianceUnits)
    {
        this.totalAllianceUnits = totalAllianceUnits;
    }
}
