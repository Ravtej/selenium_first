package com.ihg.automation.selenium.common.communication;

public class CommunicationPreferencesConstants
{
    public final static String OPT_OUT = "Opt-Out";
    public final static String OPT_IN = "Opt-In";
    public final static String OK_TO_SELL = "Ok to Sell All";
    public final static String DO_NOT_SELL = "Do Not Sell All";
    public final static String CANNOT_SAVE_PREFERENCES = "Communication preferences can't be saved, cause not all fields passed validation.";
    public final static String SUCCESS_MESSAGE_SC = "Communication preferences have been successfully saved";
    public final static String SUCCESS_MESSAGE_LC = "Communication preferences have been successfully saved.";
}
