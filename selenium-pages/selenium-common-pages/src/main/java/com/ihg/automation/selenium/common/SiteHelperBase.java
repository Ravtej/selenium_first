package com.ihg.automation.selenium.common;

import com.ihg.automation.selenium.common.event.SourceFactory;
import com.ihg.automation.selenium.common.pages.login.SingleSignOn;
import com.ihg.automation.selenium.runtime.Selenium;

public abstract class SiteHelperBase
{
    private String url;
    private UI ui;
    private User user;

    public SiteHelperBase(String url, UI ui)
    {
        this.url = url;
        this.ui = ui;
    }

    public User getUser()
    {
        return user;
    }

    public void openLoginPage()
    {
        Selenium.getInstance().getDriverWrapper().openUrl(url);
    }

    protected abstract void selectRole(User user, String role, boolean isRoleMandatory);

    public void login(User user, String role, boolean isRoleMandatory)
    {
        this.user = user;

        openLoginPage();
        new SingleSignOn().submitLoginDetails(user);

        selectRole(user, role, isRoleMandatory);
    }

    public void login(User user, String role)
    {
        login(user, role, true);
    }

    public void logout()
    {
        new SingleSignOn().signOff(url);
    }

    public SourceFactory getSourceFactory()
    {
        return new SourceFactory(user, ui);
    }

}
