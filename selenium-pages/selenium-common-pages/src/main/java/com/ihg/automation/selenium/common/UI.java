package com.ihg.automation.selenium.common;

public enum UI
{
    SC("SC", Channel.MSCUI), //
    LC("LC", Channel.MHUI);

    private String type;
    private String channel;

    UI(String type, String channel)
    {
        this.type = type;
        this.channel = channel;
    }

    public String getType()
    {
        return type;
    }

    public String getChannel()
    {
        return channel;
    }
}
