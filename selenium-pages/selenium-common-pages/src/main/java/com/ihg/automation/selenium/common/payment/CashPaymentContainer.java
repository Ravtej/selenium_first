package com.ihg.automation.selenium.common.payment;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.automation.selenium.matchers.component.ViewableMatcher.hasText;

import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.CustomContainerBase;
import com.ihg.automation.selenium.gwt.components.Mode;
import com.ihg.automation.selenium.gwt.components.input.DateInput;
import com.ihg.automation.selenium.gwt.components.input.Input;

public class CashPaymentContainer extends CustomContainerBase
{
    public CashPaymentContainer(Container container)
    {
        super(container);
    }

    public Input getName()
    {
        return container.getInputByLabel("Name of who received cash");
    }

    public Input getLocation()
    {
        return container.getInputByLabel("Location where cash was received");
    }

    public DateInput getDepositDate()
    {
        return new DateInput(container.getLabel("Deposit Date"));
    }

    public Input getDepositNumber()
    {
        return container.getInputByLabel("Deposit Number");
    }

    public void verifyPaymentFields()
    {
        verifyThat(getName(), displayed(true));
        verifyThat(getLocation(), displayed(true));
        verifyThat(getDepositDate(), displayed(true));
        verifyThat(getDepositNumber(), displayed(true));
    }

    public void verify(CashPayment cashPayment, Mode mode)
    {
        verifyThat(getName(), hasText(cashPayment.getName(), mode));
        verifyThat(getLocation(), hasText(cashPayment.getLocation(), mode));
        verifyThat(getDepositDate(), hasText(cashPayment.getDepositDate(), mode));
        verifyThat(getDepositNumber(), hasText(cashPayment.getDepositNumber(), mode));
    }

    public void populate(CashPayment cashPayment)
    {
        getName().type(cashPayment.getName());
        getLocation().type(cashPayment.getLocation());
        getDepositDate().type(cashPayment.getDepositDate());
        getDepositNumber().type(cashPayment.getDepositNumber());
    }
}
