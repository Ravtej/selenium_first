package com.ihg.automation.selenium.common;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;

import org.joda.time.LocalDate;

import com.ihg.automation.selenium.gwt.components.ComponentColumnPosition;
import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.CustomContainerBase;
import com.ihg.automation.selenium.gwt.components.Mode;
import com.ihg.automation.selenium.gwt.components.input.Select;
import com.ihg.automation.selenium.gwt.components.label.Label;

public class BirthDate extends CustomContainerBase
{
    private final ComponentColumnPosition POSITION = ComponentColumnPosition.TABLE;
    private String labelText = "Date of Birth";
    private static String MONTH = "MMMM";
    private static String DAY = "d";
    private static String MONTH_DAY = "MMMdd";
    public static String DAY_MONTH = "ddMMM";

    public BirthDate(Container container)
    {
        super(container);
    }

    public Select getMonth()
    {
        return container.getLabel(labelText).getSelect("Month", 1, POSITION);
    }

    public Select getDay()
    {
        return container.getLabel(labelText).getSelect("Day", 2, POSITION);
    }

    public Label getFullDate()
    {
        return container.getLabel(labelText);
    }

    public void populate(LocalDate guestBirthDate)
    {
        if (guestBirthDate == null)
        {
            return;
        }

        getMonth().select(guestBirthDate.toString(MONTH));
        getDay().select(guestBirthDate.toString(DAY));
    }

    public void verify(LocalDate date, Mode mode)
    {
        if (date == null)
        {
            return;
        }

        if (mode == Mode.EDIT)
        {
            verifyThat(getMonth(), hasText(date.toString(MONTH)));
            verifyThat(getDay(), hasText(date.toString(DAY)));
        }
        else if (mode == Mode.VIEW)
        {
            verifyThat(getFullDate(), hasText(date.toString(MONTH_DAY)));

        }

    }
}
