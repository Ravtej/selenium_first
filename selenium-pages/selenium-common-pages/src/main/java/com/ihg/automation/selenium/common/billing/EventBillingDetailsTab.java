package com.ihg.automation.selenium.common.billing;

import static com.ihg.automation.common.DateUtils.MMMM_d_comma_YYYY;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.LocalDate;

import com.ihg.automation.common.DateUtils;
import com.ihg.automation.selenium.common.User;
import com.ihg.automation.selenium.common.catalog.CatalogItem;
import com.ihg.automation.selenium.common.pages.PopUpBase;
import com.ihg.automation.selenium.common.pages.TabCustomContainerBase;

public class EventBillingDetailsTab extends TabCustomContainerBase
{
    public EventBillingDetailsTab(PopUpBase parent)
    {
        super(parent, "Billing Details");
    }

    public EventBillingDetail getEventBillingDetail(int position)
    {
        return new EventBillingDetail(container, position);
    }

    public void verifyTabIsEmpty(String eventType)
    {
        verifyThat(this, hasText(EventBillingDetail.getSeparatorText(eventType)));
    }

    public void verifyTabIsEmpty(LocalDate eventDate, String eventType)
    {
        verifyThat(this, hasText(EventBillingDetail.getSeparatorText(eventDate, eventType)));
    }

    public void verifyBaseUSDBilling(String eventType, String usdAmount, String billingTo)
    {
        goTo();
        if (StringUtils.isEmpty(usdAmount) || "0".equals(usdAmount))
        {
            verifyTabIsEmpty(eventType);
        }
        else
        {
            EventBillingDetail eventBillingDetail = getEventBillingDetail(1);
            eventBillingDetail.verifySeparator(eventType, DateUtils.getFormattedDate(DateUtils.MMMM_d_comma_YYYY));
            eventBillingDetail.getBaseLoyaltyUnits().verifyUSDAmount(usdAmount, billingTo);
        }
    }

    public void verifyBaseAwardUSDBilling(String eventType, String usdAmount, String billingTo, LocalDate billingDate)
    {
        goTo();
        if (StringUtils.isEmpty(usdAmount) || "0".equals(usdAmount))
        {
            verifyTabIsEmpty(eventType);
        }
        else
        {
            EventBillingDetail eventBillingDetail = getEventBillingDetail(1);
            eventBillingDetail.verifySeparator(eventType, billingDate.toString(MMMM_d_comma_YYYY));
            eventBillingDetail.getBaseAwardLoyaltyUnits().verifyUSDAmount(usdAmount, billingTo);
        }
    }

    public void verifyBaseAwardUSDBilling(String eventType, String usdAmount, String billingTo)
    {
        verifyBaseAwardUSDBilling(eventType, usdAmount, billingTo, DateUtils.now());
    }

    public void verifyBaseAwardUSDBilling(String eventType, CatalogItem award, String billingTo)
    {
        verifyBaseAwardUSDBilling(eventType, award.getAwardCost(), billingTo);
    }

    public void verifyBaseAwardUSDBilling(String eventType, CatalogItem award, String billingTo, LocalDate billingDate)
    {
        verifyBaseAwardUSDBilling(eventType, award.getAwardCost(), billingTo, billingDate);
    }

    public void verifyBaseAwardUSDBilling(String eventType, CatalogItem award, User billingTo)
    {
        verifyBaseAwardUSDBilling(eventType, award, billingTo.getLocation());
    }

    public void verifyBaseAwardUSDBilling(String eventType, CatalogItem award, User billingTo, LocalDate billingDate)
    {
        verifyBaseAwardUSDBilling(eventType, award, billingTo.getLocation(), billingDate);
    }
}
