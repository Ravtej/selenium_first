package com.ihg.automation.selenium.common.types.program;

public enum AmbassadorLevel implements ProgramLevel
{
    AMB("AMB", "AMBASSADOR"), RAM("RAM", "ROYAL AMBASSADOR");

    private String code;
    private String value;

    private AmbassadorLevel(String code, String description)
    {
        this.code = code;
        this.value = description;
    }

    @Override
    public String getCode()
    {
        return code;
    }

    @Override
    public String getValue()
    {
        return value;
    }

    @Override
    public String getCodeWithValue()
    {
        return code + " - " + value;
    }
}
