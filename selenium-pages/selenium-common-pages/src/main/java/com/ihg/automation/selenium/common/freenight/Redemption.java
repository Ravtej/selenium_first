package com.ihg.automation.selenium.common.freenight;

import com.ihg.automation.common.DateUtils;

public class Redemption
{
    private String confirmationNumber = "";
    private String hotel = "";
    private String confirmationDate = DateUtils.getFormattedDate();
    private String numberOfRooms = "";
    private String checkIn = "";
    private String checkOut = "";
    private String nights;

    public String getConfirmationNumber()
    {
        return confirmationNumber;
    }

    public void setConfirmationNumber(String confirmationNumber)
    {
        this.confirmationNumber = confirmationNumber;
    }

    public String getHotel()
    {
        return hotel;
    }

    public void setHotel(String hotel)
    {
        this.hotel = hotel;
    }

    public String getConfirmationDate()
    {
        return confirmationDate;
    }

    public void setConfirmationDate(String confirmationDate)
    {
        this.confirmationDate = confirmationDate;
    }

    public String getNumberOfRooms()
    {
        return numberOfRooms;
    }

    public void setNumberOfRooms(String numberOfRooms)
    {
        this.numberOfRooms = numberOfRooms;
    }

    public String getCheckIn()
    {
        return checkIn;
    }

    public void setCheckIn(String checkIn)
    {
        this.checkIn = checkIn;
    }

    public String getCheckOut()
    {
        return checkOut;
    }

    public void setCheckOut(String checkOut)
    {
        this.checkOut = checkOut;
    }

    public String getNights()
    {
        return nights;
    }

    public void setNights(String nights)
    {
        this.nights = nights;
    }

    public int getFreeNightVoucherAmount()
    {
        return Integer.parseInt(this.getNumberOfRooms()) * Integer.parseInt(this.getNights());
    }
}
