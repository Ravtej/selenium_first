package com.ihg.automation.selenium.common.types.address;

import com.ihg.automation.selenium.gwt.components.EntityType;

public enum RegionTaiwanNative implements EntityType
{
    TPE("TPE", "臺北市"), //
    TAI_BEI(null, "台北");

    private String code;
    private String value;

    private RegionTaiwanNative(String code, String value)
    {
        this.code = code;
        this.value = value;
    }

    @Override
    public String getCode()
    {
        return code;
    }

    @Override
    public String getValue()
    {
        return value;
    }

    @Override
    public String getCodeWithValue()
    {
        return code + " - " + value;
    }

    @Override
    public String toString()
    {
        return getValue();
    }
}
