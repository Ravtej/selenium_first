package com.ihg.automation.selenium.common.contact;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.ViewableMatcher.hasText;

import com.ihg.automation.selenium.common.member.GuestPhone;
import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.Mode;
import com.ihg.automation.selenium.gwt.components.input.Input;
import com.ihg.automation.selenium.gwt.components.panel.EditablePanel;

public class Sms extends ContactBase<GuestPhone>
{
    public Sms(Container container, int index)
    {
        super(new EditablePanel(container.getSeparator("SMS"), index));
    }

    public Sms(Container container)
    {
        super(container);
    }

    public Input getNumber()
    {
        return container.getInputByLabel("SMS Number");
    }

    @Override
    public void populate(GuestPhone phone)
    {
        getNumber().type(phone.getNumber());
    }

    @Override
    public void verify(GuestPhone phone, Mode mode)
    {
        verifyThat(getNumber(), hasText(phone.getNumber(), mode));
    }

    public void verifyMasked(GuestPhone phone, Mode mode)
    {
        verifyThat(getNumber(), hasText(phone.getNumber().replaceAll(".", "*"), mode));
    }
}
