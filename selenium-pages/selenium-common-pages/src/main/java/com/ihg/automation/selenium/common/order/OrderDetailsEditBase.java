package com.ihg.automation.selenium.common.order;

import com.ihg.automation.selenium.gwt.components.Component;
import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.input.DateInput;
import com.ihg.automation.selenium.gwt.components.input.Input;
import com.ihg.automation.selenium.gwt.components.label.Label;

public abstract class OrderDetailsEditBase extends OrderDetailsBase
{
    public OrderDetailsEditBase(Container container)
    {
        super(container);
    }

    public OrderDetailsEditBase(Container container, String itemId)
    {
        super(container, itemId);
    }

    abstract public Component getItemId();

    @Override
    public Label getItemName()
    {
        return container.getLabel("Name of Item");
    }

    @SuppressWarnings("unchecked")
    @Override
    public DateInput getEstimatedDeliveryDate()
    {
        return new DateInput(container.getLabel(ESTIMATED_DELIVERY_DATE_LABEL));
    }

    @SuppressWarnings("unchecked")
    @Override
    public DateInput getActualDeliveryDate()
    {
        return new DateInput(container.getLabel(ACTUAL_DELIVERY_DATE_LABEL));
    }

    @Override
    public Input getDeliveryStatus()
    {
        return container.getInputByLabel(DELIVERY_STATUS_LABEL);
    }

    @SuppressWarnings("unchecked")
    @Override
    public Input getTrackingUniqueNumber()
    {
        Input input = container.getInputByLabel(TRACKING_NUMBER_LABEL);
        input.setDefaultValue(TRACKING_NUMBER_LABEL);
        return input;
    }

    @Override
    public Input getSignedForBy()
    {
        return container.getInputByLabel(SIGNED_FOR_BY_LABEL);
    }

    @Override
    public Input getComments()
    {
        return container.getInputByLabel(COMMENTS_LABEL);
    }
}
