package com.ihg.automation.selenium.common.stay;

public class BookingDetails implements Cloneable
{
    private String bookingDate;
    private String bookingSource;
    private String dataSource = "SC";

    public String getBookingDate()
    {
        return bookingDate;
    }

    public void setBookingDate(String bookingDate)
    {
        this.bookingDate = bookingDate;
    }

    public String getBookingSource()
    {
        return bookingSource;
    }

    public void setBookingSource(String bookingSource)
    {
        this.bookingSource = bookingSource;
    }

    public String getDataSource()
    {
        return dataSource;
    }

    public void setDataSource(String dataSource)
    {
        this.dataSource = dataSource;
    }

    @Override
    public BookingDetails clone()
    {
        BookingDetails newBookingDetails = new BookingDetails();

        newBookingDetails.bookingDate = bookingDate;
        newBookingDetails.bookingSource = bookingSource;
        newBookingDetails.dataSource = dataSource;

        return newBookingDetails;
    }
}
