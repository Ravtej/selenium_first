package com.ihg.automation.selenium.common.types.address;

import com.ihg.automation.selenium.gwt.components.EntityType;

public enum Region implements EntityType
{
    AL("AL", "Alabama"), //
    AK("AK", "Alaska"), //
    CA("CA", "California"), //
    CO("CO", "Colorado"), //
    FL("FL", "Florida"), //
    HI("HI", "Hawaii"), //
    KY("KY", "Kentucky"), //
    NV("NV", "Nevada"), //
    NC("NC", "North Carolina"), //
    TX("TX", "Texas"), //
    UM("UM", "United States Minor Outlying Islands"), //
    UT("UT", "Utah"), //
    VI("VI", "Virgin Islands, U.S."), //
    VA("VA", "Virginia"), //
    WA("WA", "Washington"), //
    GE("GA", "Georgia"), //
    NY("NY", "New York");

    private String code;
    private String value;

    private Region(String code, String value)
    {
        this.code = code;
        this.value = value;
    }

    @Override
    public String getCode()
    {
        return code;
    }

    @Override
    public String getValue()
    {
        return value;
    }

    @Override
    public String getCodeWithValue()
    {
        return code + " - " + value;
    }

    @Override
    public String toString()
    {
        return getValue();
    }
}
