package com.ihg.automation.selenium.common.types.name;

public class Degree
{
    private Degree()
    {
    }

    public static final String DIPL_ING = "Dipl.Ing";
    public static final String DMD = "DMD";
    public static final String DDS = "DDS";
    public static final String ID = "ID";
    public static final String MD = "MD";
    public static final String INZ = "Inz";
    public static final String PHD = "PhD";
    public static final String JD = "JD";
    public static final String IR = "Ir";
    public static final String ING = "Ing";
    public static final String DL = "DL";
}
