package com.ihg.automation.selenium.common.types.program;

public class AlternateLoyaltyIDType
{
    public static final String HERTZ = "Hertz";
    public static final String KARMA = "Karma";

    private AlternateLoyaltyIDType()
    {
    }
}
