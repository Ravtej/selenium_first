package com.ihg.automation.selenium.common.payment;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.ViewableMatcher.hasText;

import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.CustomContainerBase;
import com.ihg.automation.selenium.gwt.components.Mode;
import com.ihg.automation.selenium.gwt.components.input.DateInput;
import com.ihg.automation.selenium.gwt.components.input.Input;

public class CheckPaymentContainer extends CustomContainerBase
{
    public CheckPaymentContainer(Container container)
    {
        super(container);
    }

    public Input getCheckNumber()
    {
        return container.getInputByLabel("Check Number");
    }

    public Input getNameOnAccount()
    {
        return container.getInputByLabel("Name on Checking Account");
    }

    public Input getIssuingBank()
    {
        return container.getInputByLabel("Issuing Bank");
    }

    public DateInput getDepositDate()
    {
        return new DateInput(container.getLabel("Deposit Date"));
    }

    public Input getDepositNumber()
    {
        return container.getInputByLabel("Deposit Number");
    }

    public void verify(CheckPayment checkPayment, Mode mode)
    {
        verifyThat(getCheckNumber(), hasText(checkPayment.getCheckNumber(), mode));
        verifyThat(getNameOnAccount(), hasText(checkPayment.getNameOnAccount(), mode));
        verifyThat(getIssuingBank(), hasText(checkPayment.getIssuingBank(), mode));
        verifyThat(getDepositDate(), hasText(checkPayment.getDepositDate(), mode));
        verifyThat(getDepositNumber(), hasText(checkPayment.getDepositNumber(), mode));
    }

    public void populate(CheckPayment checkPayment)
    {
        getCheckNumber().type(checkPayment.getCheckNumber());
        getNameOnAccount().type(checkPayment.getNameOnAccount());
        getIssuingBank().type(checkPayment.getIssuingBank());
        getDepositDate().type(checkPayment.getDepositDate());
        getDepositNumber().type(checkPayment.getDepositNumber());
    }
}
