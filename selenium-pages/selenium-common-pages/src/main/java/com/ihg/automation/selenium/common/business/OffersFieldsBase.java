package com.ihg.automation.selenium.common.business;

import com.ihg.automation.selenium.gwt.components.Component;
import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.CustomContainerBase;
import com.ihg.automation.selenium.gwt.components.label.TopLabel;
import com.ihg.automation.selenium.gwt.components.utils.ByText;

public abstract class OffersFieldsBase extends CustomContainerBase
{
    public OffersFieldsBase(Container container)
    {
        super(container);
    }

    public TopLabel getEstimatedCost()
    {
        return new TopLabel(container, ByText.startsWith("Estimated Cost to Hotel"), TopLabel.GO_TO_LABEL);
    }

    public abstract Component getOffer();

    public abstract Component getPointsToAward();
}
