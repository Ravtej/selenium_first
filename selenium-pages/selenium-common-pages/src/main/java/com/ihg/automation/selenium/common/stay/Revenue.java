package com.ihg.automation.selenium.common.stay;

public class Revenue implements Cloneable
{
    private String revenueCategoryCode;
    private Double amount;
    private Double usdAmount;
    private Boolean qualifying;
    private Boolean billHotel;
    private String reasonCode;
    /** Depends on configuration in RULE.LYTY_PGM_REV.ALLOW_OVRD_IND **/
    private boolean allowOverride = false;

    public Revenue()
    {
    }

    public Revenue(String categoryCode)
    {
        this.revenueCategoryCode = categoryCode;
    }

    public Double getAmount()
    {
        return amount;
    }

    public void setAmount(Double value)
    {
        this.amount = value;
    }

    public Double getUsdAmount()
    {
        return usdAmount;
    }

    public void setUsdAmount(Double usdAmount)
    {
        this.usdAmount = usdAmount;
    }

    public String getRevenueCategoryCode()
    {
        return revenueCategoryCode;
    }

    public void setRevenueCategoryCode(String value)
    {
        this.revenueCategoryCode = value;
    }

    public Boolean isQualifying()
    {
        return qualifying;
    }

    public void setQualifying(Boolean qualifying)
    {
        this.qualifying = qualifying;
    }

    public Boolean isBillHotel()
    {
        return billHotel;
    }

    public void setBillHotel(Boolean billHotel)
    {
        this.billHotel = billHotel;
    }

    public String getReasonCode()
    {
        return reasonCode;
    }

    public void setReasonCode(String reasonCode)
    {
        this.reasonCode = reasonCode;
    }

    public boolean isAllowOverride()
    {
        return allowOverride;
    }

    public void setAllowOverride(boolean allowOverride)
    {
        this.allowOverride = allowOverride;
    }

    @Override
    public Revenue clone()
    {
        Revenue newRevenue = new Revenue();

        newRevenue.revenueCategoryCode = revenueCategoryCode;
        newRevenue.amount = amount;
        newRevenue.usdAmount = usdAmount;
        newRevenue.qualifying = qualifying;
        newRevenue.billHotel = billHotel;
        newRevenue.reasonCode = reasonCode;
        newRevenue.allowOverride = allowOverride;

        return newRevenue;
    }
}
