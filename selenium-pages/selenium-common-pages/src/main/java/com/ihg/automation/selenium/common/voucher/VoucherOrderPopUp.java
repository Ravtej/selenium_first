package com.ihg.automation.selenium.common.voucher;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.isSelected;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static org.hamcrest.core.StringContains.containsString;

import com.ihg.automation.selenium.common.CustomerInfoPanel;
import com.ihg.automation.selenium.common.order.VoucherRule;
import com.ihg.automation.selenium.common.pages.PopUpBase;
import com.ihg.automation.selenium.common.pages.Verify;
import com.ihg.automation.selenium.gwt.components.Button;
import com.ihg.automation.selenium.gwt.components.checkbox.CheckBox;
import com.ihg.automation.selenium.gwt.components.input.Select;
import com.ihg.automation.selenium.gwt.components.label.Label;
import com.ihg.automation.selenium.gwt.components.label.TableLabel;
import com.ihg.automation.selenium.gwt.components.utils.ByText;
import com.ihg.automation.selenium.listener.Verifier;

public class VoucherOrderPopUp extends PopUpBase implements Verify<VoucherRule>
{
    public VoucherOrderPopUp()
    {
        super("Voucher Order");
    }

    public TableLabel getItemDescription()
    {
        return new TableLabel(container, ByText.exact("Item Description"));
    }

    public TableLabel getCostPerVoucher()
    {
        return new TableLabel(container, ByText.exact("Cost per Voucher"));
    }

    public Select getQuantity()
    {
        return new TableLabel(container, ByText.exact("Quantity")).getSelect();
    }

    public TableLabel getTotalCost()
    {
        return new TableLabel(container, ByText.exact("Total Cost"));
    }

    public Select getDeliveryOption()
    {
        return new Select(container, ".//td[normalize-space()='Delivery Option']//input", "Delivery Option");
    }

    public Label getName()
    {
        return container.getLabel("Name");
    }

    public Label getAddress()
    {
        return container.getLabel("Address");
    }

    public Select getEmail()
    {
        return container.getSelectByLabel("Email");
    }

    public Select getEmailType()
    {
        return container.getSelectByLabel("Type");
    }

    public Select getEmailFormat()
    {
        return container.getSelectByLabel("Format");
    }

    public CheckBox getSaveEmail()
    {
        return container.getLabel("Save Email").getCheckBox();
    }

    public Button getCancel()
    {
        return container.getButton(Button.CANCEL);
    }

    public void clickCancel()
    {
        getCancel().clickAndWait();
    }

    public Button getPlaceOrder()
    {
        return container.getButton("Place Order");
    }

    public void clickPlaceOrder(Verifier... verifiers)
    {
        getPlaceOrder().clickAndWait(verifiers);
    }

    public void order(String quantity, String deliveryOption)
    {
        getQuantity().select(quantity);
        getDeliveryOption().select(deliveryOption);
        clickPlaceOrder();
    }

    @Override
    public void verify(VoucherRule voucherRule)
    {
        verifyThat(getItemDescription(), hasText(voucherRule.getVoucherName()));
        verifyThat(getCostPerVoucher(), hasText(voucherRule.getCostAmountWithCurrency()));
        verifyThat(getQuantity(), hasText(containsString(voucherRule.getQuantity())));
        verifyThat(getTotalCost(), hasText(voucherRule.getTotalCostAmountWithCurrency()));
    }

    public void verifyShippingInfoPaperFulfillment()
    {
        CustomerInfoPanel customerInfo = new CustomerInfoPanel();

        verifyThat(getName(), hasText(customerInfo.getFullName().getText()));
        verifyThat(getAddress(), hasText(customerInfo.getAddress().getText()));
    }

    public void verifyShippingInfoEmailFulfillment()
    {
        CustomerInfoPanel customerInfo = new CustomerInfoPanel();

        verifyThat(getName(), hasText(customerInfo.getFullName().getText()));
        verifyThat(getEmail(), hasText(customerInfo.getEmail().getText()));
        verifyThat(getSaveEmail(), isSelected(false));
        // TODO check email type and format
    }
}
