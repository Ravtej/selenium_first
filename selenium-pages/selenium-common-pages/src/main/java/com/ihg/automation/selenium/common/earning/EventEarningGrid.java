package com.ihg.automation.selenium.common.earning;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.GridSize.size;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import org.hamcrest.collection.IsEmptyCollection;

import com.ihg.automation.selenium.common.earning.EventEarningGridRow.EventEarningCell;
import com.ihg.automation.selenium.common.event.EarningEvent;
import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.grid.Grid;
import com.ihg.automation.selenium.gwt.components.grid.GridRowXpath;

public class EventEarningGrid extends Grid<EventEarningGridRow, EventEarningCell>
{
    public EventEarningGrid(Container parent)
    {
        super(parent);
    }

    public void verify(final EarningEvent event)
    {
        verifyThat(this, size(1));
        this.getRow(1).verify(event);
    }

    public void verifyRowsFoundByType(final EarningEvent... events)
    {
        verifyThat(this, size(events.length), " (expected events amount)");

        List<EarningEvent> earningEvents = new ArrayList<>(Arrays.asList(events));
        Iterator<EventEarningGridRow> gridRowIterator = getRowList().iterator();

        while (gridRowIterator.hasNext())
        {
            EventEarningGridRow row = gridRowIterator.next();
            String type = row.getCell(EventEarningCell.TYPE).getText();
            Iterator<EarningEvent> eventIterator = earningEvents.iterator();

            while (eventIterator.hasNext())
            {
                EarningEvent event = eventIterator.next();

                if (event.getType().equals(type))
                {
                    row.verify(event);

                    eventIterator.remove();
                    gridRowIterator.remove();

                    break;
                }
            }
        }

        verifyThat(this, earningEvents, IsEmptyCollection.empty(), "All earning event(s) must be in grid.");
    }

    public void verifyRowsFoundByTypeAndAward(final EarningEvent... events)
    {
        verifyThat(this, size(events.length), " (expected events amount)");

        List<EarningEvent> earningEvents = new ArrayList<>(Arrays.asList(events));
        Iterator<EventEarningGridRow> gridRowIterator = getRowList().iterator();

        while (gridRowIterator.hasNext())
        {
            EventEarningGridRow row = gridRowIterator.next();
            String type = row.getCell(EventEarningCell.TYPE).getText();
            String award = row.getCell(EventEarningCell.AWARD).getText();
            Iterator<EarningEvent> eventIterator = earningEvents.iterator();

            while (eventIterator.hasNext())
            {
                EarningEvent event = eventIterator.next();

                if (event.getType().equals(type) && event.getAward().equals(award))
                {
                    row.verify(event);

                    eventIterator.remove();
                    gridRowIterator.remove();

                    break;
                }
            }
        }

        verifyThat(this, earningEvents, IsEmptyCollection.empty(), "All earning event(s) must be in grid.");
    }

    public EventEarningGridRow getRowByAward(String award)
    {
        return getRow(new GridRowXpath.GridRowXpathBuilder().cell(EventEarningCell.AWARD, award).build());
    }

    public EventEarningGridRow getRowByType(String text)
    {
        return getRow(new GridRowXpath.GridRowXpathBuilder().cell(EventEarningCell.TYPE, text).build());
    }
}
