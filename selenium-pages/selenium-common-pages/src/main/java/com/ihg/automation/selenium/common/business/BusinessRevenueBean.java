package com.ihg.automation.selenium.common.business;

public class BusinessRevenueBean
{
    private Double amount;
    private Double usdAmount;
    private String estimatedPointEarning;
    private String estimatedCostToHotel;

    public BusinessRevenueBean(Double amount)
    {
        this.amount = amount;
    }

    public BusinessRevenueBean(Double amount, Double usdAmount)
    {
        this.amount = amount;
        this.usdAmount = usdAmount;
    }

    public Double getAmount()
    {
        return amount;
    }

    public void setAmount(Double amount)
    {
        this.amount = amount;
    }

    public Double getUsdAmount()
    {
        return usdAmount;
    }

    public void setUsdAmount(Double usdAmount)
    {
        this.usdAmount = usdAmount;
    }

    public String getEstimatedPointEarning()
    {
        return estimatedPointEarning;
    }

    public void setEstimatedPointEarning(String estimatedPointEarning)
    {
        this.estimatedPointEarning = estimatedPointEarning;
    }

    public String getEstimatedCostToHotel()
    {
        return estimatedCostToHotel;
    }

    public void setEstimatedCostToHotel(String estimatedCostToHotel)
    {
        this.estimatedCostToHotel = estimatedCostToHotel;
    }
}
