package com.ihg.automation.selenium.common.business;

import com.ihg.automation.selenium.common.pages.PopUpBase;
import com.ihg.automation.selenium.common.pages.TabCustomContainerBase;

public class EventSummaryTab extends TabCustomContainerBase
{
    private final static String TOTAL_ELIGIBLE_REVENUE_LABEL = "Total Eligible Revenue";

    public EventSummaryTab(PopUpBase parent)
    {
        super(parent, "Event Summary");
    }

    public EventInformationFields getEventInformation()
    {
        return new EventInformationFields(container);
    }

    public EventDetailsFields getEventDetails()
    {
        return new EventDetailsFields(container);
    }

    public BusinessRevenueView getTotalEligibleRevenue()
    {
        return new BusinessRevenueView(container, TOTAL_ELIGIBLE_REVENUE_LABEL);
    }

    public DiscretionaryPointsFields getDiscretionaryPoints()
    {
        return new DiscretionaryPointsFields(container.getFieldSet("Discretionary Points"));
    }

    public OffersFieldsEdit getOffers()
    {
        return new OffersFieldsEdit(container);
    }

    public TotalEventAwardFields getTotalEventAward()
    {
        return new TotalEventAwardFields(container.getFieldSet("Total Event Award"));
    }

    public void populateHotelEventAndMeetingFields(BusinessEvent businessEvent)
    {
        getEventInformation().populateHotelEvent(businessEvent);

        populateMeetingFields(businessEvent.getMeetingDetails());
    }

    public void populateEventAndMeetingFields(BusinessEvent businessEvent)
    {
        getEventInformation().populate(businessEvent);

        populateMeetingFields(businessEvent.getMeetingDetails());
    }

    private void populateMeetingFields(MeetingDetails meetingDetails)
    {
        MeetingFields meetingFields = getEventDetails().getMeetingFields();
        meetingFields.populate(meetingDetails);

        meetingFields.getRevenueDetails().populateLocalAmount(meetingDetails);
    }
}
