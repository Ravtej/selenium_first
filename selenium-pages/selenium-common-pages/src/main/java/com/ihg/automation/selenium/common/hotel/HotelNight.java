package com.ihg.automation.selenium.common.hotel;

import static org.apache.commons.lang3.StringUtils.EMPTY;

public class HotelNight
{
    private String stayDate;
    private String nightsCount;
    private String ADRAmount = EMPTY;
    private String occupancyPercent = EMPTY;
    private String overrideADRAmount = EMPTY;
    private String overrideOccupancyPercent = EMPTY;
    private String currencyCode;
    private String leftToSubmit;
    private String status = "INITIATED";
    private String reimbursementAmount;

    public String getStayDate()
    {
        return stayDate;
    }

    public void setStayDate(String stayDate)
    {
        this.stayDate = stayDate;
    }

    public String getNightsCount()
    {
        return nightsCount;
    }

    public void setNightsCount(String nightsCount)
    {
        this.nightsCount = nightsCount;
    }

    public String getADRAmount()
    {
        return ADRAmount;
    }

    public void setADRAmount(String ADRAmount)
    {
        this.ADRAmount = ADRAmount;
    }

    public String getOccupancyPercent()
    {
        return occupancyPercent;
    }

    public void setOccupancyPercent(String occupancyPercent)
    {
        this.occupancyPercent = occupancyPercent;
    }

    public String getOverrideADRAmount()
    {
        return overrideADRAmount;
    }

    public void setOverrideADRAmount(String overrideADRAmount)
    {
        this.overrideADRAmount = overrideADRAmount;
    }

    public String getOverrideOccupancyPercent()
    {
        return overrideOccupancyPercent;
    }

    public void setOverrideOccupancyPercent(String overrideOccupancyPercent)
    {
        this.overrideOccupancyPercent = overrideOccupancyPercent;
    }

    public String getCurrencyCode()
    {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode)
    {
        this.currencyCode = currencyCode;
    }

    public String getLeftToSubmit()
    {
        return leftToSubmit;
    }

    public void setLeftToSubmit(String leftToSubmit)
    {
        this.leftToSubmit = leftToSubmit;
    }

    public String getStatus()
    {
        return status;
    }

    public void setStatus(String status)
    {
        this.status = status;
    }

    public String getReimbursementAmount()
    {
        return reimbursementAmount;
    }

    public void setReimbursementAmount(String reimbursementAmount)
    {
        this.reimbursementAmount = reimbursementAmount;
    }
}
