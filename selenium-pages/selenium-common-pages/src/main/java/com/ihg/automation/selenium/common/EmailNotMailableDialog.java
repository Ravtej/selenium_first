package com.ihg.automation.selenium.common;

import com.ihg.automation.selenium.gwt.components.Button;
import com.ihg.automation.selenium.gwt.components.dialog.DialogBase;
import com.ihg.automation.selenium.gwt.components.utils.ByText;

public class EmailNotMailableDialog extends DialogBase
{
    public EmailNotMailableDialog()
    {
        super(Type.DEFAULT, ByText.containsSelf("Email is not mailable"));
    }

    public Button getSaveAddressAsIs()
    {
        return container.getButton("Save email as is");
    }

    public Button getEditEmailAddress()
    {
        return container.getButton("Continue email editing");
    }

    public void clickSaveEmailAsIs()
    {
        getSaveAddressAsIs().clickAndWait();
    }

    public void clickEditEmailAddress()
    {
        getEditEmailAddress().clickAndWait();
    }
}
