package com.ihg.automation.selenium.common.order;

import org.apache.commons.lang3.StringUtils;

import com.ihg.automation.selenium.common.Constant;
import com.ihg.automation.selenium.common.catalog.CatalogItem;

public class OrderItem
{
    public static final int REDEEM_LENGTH_LIMIT = 30;
    private static final String NA = Constant.NOT_AVAILABLE;
    private String itemID;
    private String itemName;
    private String estimatedDeliveryDate = NA;
    private String actualDeliveryDate = NA;
    private String deliveryStatus = NA;
    private String trackingUniqueNumber = NA;
    private String signedForBy = NA;
    private String comments = NA;
    private String loyaltyUnits;
    private String deliveryMethod = NA;
    private String quantity = NA;
    private String size = NA;
    private String color = NA;
    private String allianceUnits;

    public OrderItem(String itemID)
    {
        this.itemID = itemID;
    }

    public OrderItem(String itemID, String itemName)
    {
        this.itemID = itemID;
        this.itemName = itemName;
    }

    public OrderItem(CatalogItem item)
    {
        this(item.getItemId(), item.getItemName());
        this.loyaltyUnits = item.getLoyaltyUnitCost();
    }

    public void setItemRedeemName(String itemName)
    {
        this.itemName = StringUtils.left(itemName, REDEEM_LENGTH_LIMIT).trim();
    }

    public String getItemName()
    {
        return itemName;
    }

    public void setItemName(String itemName)
    {
        this.itemName = itemName;
    }

    public String getSignedForBy()
    {
        return signedForBy;
    }

    public void setSignedForBy(String signedForBy)
    {
        this.signedForBy = signedForBy;
    }

    public String getDeliveryMethod()
    {
        return deliveryMethod;
    }

    public void setDeliveryMethod(String deliveryMethod)
    {
        this.deliveryMethod = deliveryMethod;
    }

    public String getEstimatedDeliveryDate()
    {
        return estimatedDeliveryDate;
    }

    public void setEstimatedDeliveryDate(String estimatedDeliveryDate)
    {
        this.estimatedDeliveryDate = estimatedDeliveryDate;
    }

    public String getDeliveryStatus()
    {
        return deliveryStatus;
    }

    public void setDeliveryStatus(String deliveryStatus)
    {
        this.deliveryStatus = deliveryStatus;
    }

    public String getActualDeliveryDate()
    {
        return actualDeliveryDate;
    }

    public void setActualDeliveryDate(String actualDeliveryDate)
    {
        this.actualDeliveryDate = actualDeliveryDate;
    }

    public String getTrackingUniqueNumber()
    {
        return trackingUniqueNumber;
    }

    public void setTrackingUniqueNumber(String trackingUniqueNumber)
    {
        this.trackingUniqueNumber = trackingUniqueNumber;
    }

    public String getComments()
    {
        return comments;
    }

    public void setComments(String comments)
    {
        this.comments = comments;
    }

    public String getItemID()
    {
        return itemID;
    }

    public void setItemID(String itemID)
    {
        this.itemID = itemID;
    }

    public String getLoyaltyUnits()
    {
        return loyaltyUnits;
    }

    public void setLoyaltyUnits(String loyaltyUnits)
    {
        this.loyaltyUnits = loyaltyUnits;
    }

    public String getQuantity()
    {
        return quantity;
    }

    public void setQuantity(String quantity)
    {
        this.quantity = quantity;
    }

    public String getSize()
    {
        return size;
    }

    public void setSize(String size)
    {
        this.size = size;
    }

    public String getColor()
    {
        return color;
    }

    public void setColor(String color)
    {
        this.color = color;
    }

    public String getAllianceUnits()
    {
        return allianceUnits;
    }

    public void setAllianceUnits(String allianceUnits)
    {
        this.allianceUnits = allianceUnits;
    }

}
