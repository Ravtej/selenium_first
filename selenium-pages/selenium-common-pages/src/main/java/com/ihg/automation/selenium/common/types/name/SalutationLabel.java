package com.ihg.automation.selenium.common.types.name;

import com.ihg.automation.selenium.common.types.LabelEnumAware;

public enum SalutationLabel implements LabelEnumAware
{
    PREFIX("Prefix"), SALUTATION("Salutation");

    private String label;

    private SalutationLabel(String label)
    {
        this.label = label;
    }

    @Override
    public String getLabel()
    {
        return label;
    }
}
