package com.ihg.automation.selenium.common.business;

import static com.ihg.automation.selenium.gwt.components.utils.FindStepUtils.byXpath;

import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.CustomContainerBase;
import com.ihg.automation.selenium.gwt.components.TextArea;
import com.ihg.automation.selenium.gwt.components.checkbox.CheckBox;
import com.ihg.automation.selenium.gwt.components.input.Input;
import com.ihg.automation.selenium.gwt.components.input.Select;
import com.ihg.automation.selenium.gwt.components.label.TopLabel;
import com.ihg.automation.selenium.gwt.components.panel.FieldSet;
import com.ihg.automation.selenium.gwt.components.utils.ByText;

public class DiscretionaryPointsFields extends CustomContainerBase
{
    public DiscretionaryPointsFields(Container container)
    {
        super(container);
    }

    public CheckBox getIncludeDiscretionaryPoints()
    {
        return container.getCheckBox("Include Discretionary Points");
    }

    public Input getAdditionalPointsToAward()
    {
        return container.getInputByLabel("Additional Points to Award");
    }

    public TextArea getComments()
    {
        return container.getLabel("Comments").getTextArea();
    }

    public Select getReason()
    {
        return container.getSelectByLabel("Reason");
    }

    public TopLabel getEstimatedCost()
    {
        return new TopLabel(container, ByText.startsWith("Estimated Cost to Hotel"),
                byXpath("./../following-sibling::tr/td[3]"));
    }

    public void expand()
    {
        ((FieldSet) container).expand();
    }

    public void populate(DiscretionaryPoints discretionaryPoints)
    {
        expand();
        getIncludeDiscretionaryPoints().check();
        getAdditionalPointsToAward().type(discretionaryPoints.getAdditionalPointsToAward());
        getReason().select(discretionaryPoints.getReason());
        getComments().type(discretionaryPoints.getComments());
    }
}
