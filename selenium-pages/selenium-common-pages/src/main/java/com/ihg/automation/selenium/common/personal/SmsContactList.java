package com.ihg.automation.selenium.common.personal;

import static com.ihg.automation.selenium.common.member.MemberPopulateHelper.getRandomPhone;

import com.ihg.automation.selenium.common.member.GuestPhone;
import com.ihg.automation.selenium.gwt.components.Button;
import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.panel.EditablePanel;

public class SmsContactList extends ContactListBase<GuestPhone, SmsContact>
{
    public SmsContactList(Container container)
    {
        super(container.getSeparator("SMS"));
    }

    @Override
    public SmsContact getContact(int index)
    {
        return new SmsContact(new EditablePanel(container, index));
    }

    @Override
    public Button getAddButton()
    {
        return container.getButton("Add SMS");
    }

    @Override
    protected GuestPhone getRandomContact()
    {
        return getRandomPhone();
    }
}
