package com.ihg.automation.selenium.common.pages;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.assertThat;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;

import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.CustomContainerBase;
import com.ihg.automation.selenium.gwt.components.MessageBox;
import com.ihg.automation.selenium.gwt.components.Selectable;
import com.ihg.automation.selenium.gwt.components.Tab;
import com.ihg.automation.selenium.matchers.component.IsSelected;

public abstract class TabCustomContainerBase extends CustomContainerBase implements NavigatePage, Selectable
{
    private Tab tab;

    public TabCustomContainerBase(Container container, String tabText)
    {
        tab = new Tab(container, tabText);
        this.container = tab.getContainer();
    }

    public TabCustomContainerBase(CustomContainerBase popUp, String tabText)
    {
        this(popUp.container, tabText);
    }

    public TabCustomContainerBase(Tab tab)
    {
        this.tab = tab;
        container = tab.getContainer();
    }

    @Override
    public void goTo()
    {
        MessageBox.closeAll();
        if (!tab.isSelected())
        {
            tab.goTo();

            flush();
            assertThat(tab, IsSelected.isSelected(true));
            assertThat(this, displayed(true));
        }
    }

    public Tab getTab()
    {
        return tab;
    }

    @Override
    public boolean isSelected()
    {
        return tab.isSelected();
    }
}
