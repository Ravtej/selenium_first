package com.ihg.automation.selenium.common.types.name;

import com.ihg.automation.selenium.common.types.LabelEnumAware;

public enum MiddleLabel implements LabelEnumAware
{
    MIDDLE("Middle Name"), SECOND("Second Name");

    private String label;

    private MiddleLabel(String label)
    {
        this.label = label;
    }

    @Override
    public String getLabel()
    {
        return label;
    }
}
