package com.ihg.automation.selenium.common;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.gwt.components.utils.FindStepUtils.byXpath;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasText;

import java.util.Arrays;

import com.ihg.automation.selenium.common.types.Alliance;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.common.types.program.RewardClubLevel;
import com.ihg.automation.selenium.gwt.components.Component;
import com.ihg.automation.selenium.gwt.components.utils.ComponentFunctionUtils;

public class RewardClubGridRow extends ProgramGridRow
{
    public static final String POINTS = "Points";

    protected RewardClubGridRow(ProgramsGrid parent)
    {
        super(parent, Program.RC);
    }

    @Override
    public Component getLevelCode()
    {
        return new Component(levelCode, levelCode.getType(), levelCode.getName(), byXpath(".//tr[1]"));
    }

    public Component getPointsBalance()
    {
        return new Component(levelCode, levelCode.getType(), "RC Points balance", byXpath(".//tr[2]"));
    }

    public Component getExpirationDate()
    {
        return new Component(this, levelCode.getType(), "Expiration Date", Arrays.asList(
                byXpath(".//tr[.='Expiration Date']"),
                byXpath("./../../../../following-sibling::td//tr[{0}]/td", ComponentFunctionUtils.getPosition())));
    }

    public Component getEarningPreference()
    {
        return new Component(levelCode, levelCode.getType(), "Earning preference", byXpath(".//tr[last()]"));
    }

    private void verify(RewardClubLevel level, String earningPreference, String pointsBalance)
    {
        verifyThat(getLevelCode(), hasText(level.getCode()));
        verifyThat(getEarningPreference(), hasText(earningPreference));
        verifyThat(getPointsBalance(), hasText(pointsBalance));
    }

    public void verify(RewardClubLevel level, Alliance alliance, String pointsBalance)
    {
        verify(level, alliance.getCode(), pointsBalance);
    }

    public void verify(RewardClubLevel level, String pointsBalance)
    {
        verify(level, POINTS, pointsBalance);
    }
}
