package com.ihg.automation.selenium.common.member;

import com.ihg.automation.selenium.common.types.Alliance;

public class AlliancePopulateHelper
{
    public static MemberAlliance getRandomBAA()
    {
        return new MemberAlliance(Alliance.BAA, getAllianceNumber(8));
    }

    public static MemberAlliance getRandomNZA()
    {
        return new MemberAlliance(Alliance.NZA, getAllianceNumber(10));
    }

    public static MemberAlliance getRandomDJA()
    {
        return new MemberAlliance(Alliance.DJA, getAllianceNumber(10));
    }

    public static MemberAlliance getRandomAZL()
    {
        return new MemberAlliance(Alliance.AZL, getAllianceNumber(8));
    }

    private static String getAllianceNumber(int length)
    {
        return String.valueOf(System.currentTimeMillis()).substring(13 - length, 13);
    }
}
