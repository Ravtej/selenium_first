package com.ihg.automation.selenium.common.member;

import com.ihg.automation.selenium.gwt.components.EntityType;

public enum DiningRewardOfferCode implements EntityType
{
    P0004("P0004", "DR - 5000 Points"), //
    P8871("P8871", "INTO THE NIGHTS");

    private String code;
    private String value;

    private DiningRewardOfferCode(String code, String value)
    {
        this.code = code;
        this.value = value;
    }

    @Override
    public String getCode()
    {
        return code;
    }

    @Override
    public String getValue()
    {
        return value;
    }

    @Override
    public String getCodeWithValue()
    {
        return code + " - " + value;
    }
}