package com.ihg.automation.selenium.common.hotel;

import static com.ihg.automation.selenium.common.Constant.NOT_AVAILABLE;

public class HotelNightDetails
{
    private String confirmationNumber;
    private String certificateNumber;

    private String memberID;
    private String lastName;
    private String suffix;
    private String status;
    private String certificateType;
    private String offerName = NOT_AVAILABLE;

    private String reimbursementAmount = NOT_AVAILABLE;
    private String reimbursementMethod = NOT_AVAILABLE;
    private String reimbursementDate = NOT_AVAILABLE;

    private String checkInDate;
    private String OCC = NOT_AVAILABLE;
    private String base;
    private String tax;
    private String fee;
    private String total;
    private String currency;

    public String getConfirmationNumber()
    {
        return confirmationNumber;
    }

    public void setConfirmationNumber(String confirmationNumber)
    {
        this.confirmationNumber = confirmationNumber;
    }

    public String getCertificateNumber()
    {
        return certificateNumber;
    }

    public void setCertificateNumber(String certificateNumber)
    {
        this.certificateNumber = certificateNumber;
    }

    public String getReimbursementAmount()
    {
        return reimbursementAmount;
    }

    public void setReimbursementAmount(String reimbursementAmount)
    {
        this.reimbursementAmount = reimbursementAmount;
    }

    public String getMemberID()
    {
        return memberID;
    }

    public void setMemberID(String memberID)
    {
        this.memberID = memberID;
    }

    public String getSuffix()
    {
        return suffix;
    }

    public void setSuffix(String suffix)
    {
        this.suffix = suffix;
    }

    public String getReimbursementMethod()
    {
        return reimbursementMethod;
    }

    public void setReimbursementMethod(String reimbursementMethod)
    {
        this.reimbursementMethod = reimbursementMethod;
    }

    public String getLastName()
    {
        return lastName;
    }

    public void setLastName(String lastName)
    {
        this.lastName = lastName;
    }

    public String getReimbursementDate()
    {
        return reimbursementDate;
    }

    public void setReimbursementDate(String reimbursementDate)
    {
        this.reimbursementDate = reimbursementDate;
    }

    public String getStatus()
    {
        return status;
    }

    public void setStatus(String status)
    {
        this.status = status;
    }

    public String getOfferName()
    {
        return offerName;
    }

    public void setOfferName(String offerName)
    {
        this.offerName = offerName;
    }

    public String getCertificateType()
    {
        return certificateType;
    }

    public void setCertificateType(String certificateType)
    {
        this.certificateType = certificateType;
    }

    public String getCheckInDate()
    {
        return checkInDate;
    }

    public void setCheckInDate(String checkInDate)
    {
        this.checkInDate = checkInDate;
    }

    public String getOCC()
    {
        return OCC;
    }

    public void setOCC(String oCC)
    {
        this.OCC = oCC;
    }

    public String getTax()
    {
        return tax;
    }

    public void setTax(String tax)
    {
        this.tax = tax;
    }

    public String getBase()
    {
        return base;
    }

    public void setBase(String base)
    {
        this.base = base;
    }

    public String getFee()
    {
        return fee;
    }

    public void setFee(String fee)
    {
        this.fee = fee;
    }

    public String getTotal()
    {
        return total;
    }

    public void setTotal(String total)
    {
        this.total = total;
    }

    public String getCurrency()
    {
        return currency;
    }

    public void setCurrency(String currency)
    {
        this.currency = currency;
    }

}
