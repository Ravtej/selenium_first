package com.ihg.automation.selenium.common.business;

import com.ihg.automation.selenium.common.event.SearchAware;
import com.ihg.automation.selenium.common.event.SearchButtonBar;
import com.ihg.automation.selenium.common.pages.PopUpBase;
import com.ihg.automation.selenium.common.pages.TabCustomContainerBase;
import com.ihg.automation.selenium.gwt.components.Button;
import com.ihg.automation.selenium.gwt.components.label.Label;
import com.ihg.automation.selenium.gwt.components.panel.Panel;
import com.ihg.automation.selenium.gwt.components.utils.ByText;
import com.ihg.automation.selenium.listener.Verifier;

public class RoomsTab extends TabCustomContainerBase implements SearchAware
{
    public RoomsTab(PopUpBase parent)
    {
        super(parent, "Rooms");
    }

    public Label getEventID()
    {
        return container.getLabel("Event ID");
    }

    public Label getEventStatus()
    {
        return container.getLabel("Event Status");
    }

    public Label getEventName()
    {
        return container.getLabel("Event Name");
    }

    public TotalEligibleRoomRevenue getTotalEligibleRoomRevenue()
    {
        return new TotalEligibleRoomRevenue(container);
    }

    public Label getStartDate()
    {
        return container.getLabel("Start Date");
    }

    public Label getEndDate()
    {
        return container.getLabel("End Date");
    }

    @Override
    public StaySearch getSearchFields()
    {
        return new StaySearch(container);
    }

    @Override
    public SearchButtonBar getButtonBar()
    {
        return new SearchButtonBar(container);
    }

    public Panel getEventStaysPanel()
    {
        return container.getPanel("Event Stays");
    }

    public EventStaysGrid getEventStaysGrid()
    {
        return new EventStaysGrid(getEventStaysPanel());
    }

    public Panel getAllPastStaysPanel()
    {
        return container.getPanel(ByText.contains("All Past Stays"));
    }

    public AllPastStaysGrid getAllPastStaysGrid()
    {
        return new AllPastStaysGrid(getAllPastStaysPanel());
    }

    public Button getAssign()
    {
        return container.getButton("Assign");
    }

    public void clickAssign(Verifier... verifiers)
    {
        getAssign().clickAndWait(verifiers);
    }
}
