package com.ihg.automation.selenium.common.member;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.YearMonth;

import com.ihg.automation.selenium.common.pages.CreditCardType;

public class CreditCard
{
    private static final int SHOW_NUMBER_DIGIT = 4;
    private static final String ASTERISKS = "***************";

    private CreditCardType type;
    private String number;
    private String maskedNumber;
    private YearMonth expirationDate;

    public CreditCard(CreditCardType type, String number, YearMonth expirationDate)
    {
        this.type = type;
        this.number = number;
        this.expirationDate = expirationDate;

        if (StringUtils.isNotEmpty(number))
        {
            int maskedLength = number.length() - SHOW_NUMBER_DIGIT;
            maskedNumber = ASTERISKS.substring(0, maskedLength) + number.substring(maskedLength);
        }
    }

    public CreditCardType getType()
    {
        return type;
    }

    public String getNumber()
    {
        return number;
    }

    public YearMonth getExpirationDate()
    {
        return expirationDate;
    }

    public String getMaskedNumber()
    {
        return maskedNumber;
    }
}
