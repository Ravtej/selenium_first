package com.ihg.automation.selenium.common.preferences;

public enum RatePreferencesType
{
    AAA("AAA/CAA Rate"), //
    ENTERTAINMENT_CARD("Entertainment Card"), //
    GOVERNMENT_CANADA("Government Canada"), //
    GOVERNMENT_RATE("Government Rate"), //
    REWARD_NIGHTS("Reward Nights"), //
    SENIOR_DISCOUNT("Senior Discount"), //
    STATE_GOVERNMENT("State Government-US");

    private String name;

    RatePreferencesType(String name)
    {
        this.name = name;
    }

    public String getName()
    {
        return name;
    }
}
