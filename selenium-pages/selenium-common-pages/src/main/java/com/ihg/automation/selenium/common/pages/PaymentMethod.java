package com.ihg.automation.selenium.common.pages;

public enum PaymentMethod
{
    CREDIT_CARD("Credit Card"), CHECK("Check"), CASH("Cash"), COMPLIMENTARY("Complimentary"), LOYALTY_UNITS(
            "Loyalty Units");

    private String label;

    PaymentMethod(String label)
    {
        this.label = label;
    }

    public String getLabel()
    {
        return label;
    }
}
