package com.ihg.automation.selenium.common.types;

import com.ihg.automation.selenium.gwt.components.EntityType;

public enum Currency implements EntityType
{
    USD("USD", "US Dollar"), //
    EUR("EUR", "Euro"), //
    JPY("JPY", "Yuan Renminbi"), //
    CNY("CNY", "Dutch"), //
    BYR("BYR", "Belarusian Ruble"), //
    AUD("AUD", "Australlian Dollar"), //
    RUB("RUB", "Russian Ruble"), //
    GBP("GBP", "Pound Sterling");

    private String code;
    private String value;

    private Currency(String code, String value)
    {
        this.code = code;
        this.value = value;
    }

    @Override
    public String getCode()
    {
        return code;
    }

    @Override
    public String getValue()
    {
        return value;
    }

    @Override
    public String toString()
    {
        return getValue();
    }

    @Override
    public String getCodeWithValue()
    {
        return code + "-" + value;
    }
}
