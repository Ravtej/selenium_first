package com.ihg.automation.selenium.common.hotel;

import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.CustomContainerBase;
import com.ihg.automation.selenium.gwt.components.Separator;
import com.ihg.automation.selenium.gwt.components.input.Select;

public class HotelBrandInfo extends CustomContainerBase
{
    public HotelBrandInfo(Container container)
    {
        super(container.getSeparator("Brand Info", Separator.FIELDSET_PATTERN_NEXT_TR));
    }

    public Select getChain()
    {
        return container.getSelectByLabel("Chain");
    }

    public Select getBrand()
    {
        return container.getSelectByLabel("Brand");
    }

    public Select getStatus()
    {
        return container.getSelectByLabel("Status");
    }
}
