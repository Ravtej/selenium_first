package com.ihg.automation.selenium.common.business;

import java.util.ArrayList;
import java.util.Arrays;

public class BusinessEventType
{
    public static final String ASSOCIATION_MEETING = "Association meeting";
    public static final String BUSINESS_LUNCH_DINNER = "Business lunch/dinner";
    public static final String CHARITY = "Charity";
    public static final String CONFERENCE_EXHIBITION = "Conference/exhibition";
    public static final String CORPORATE_MEETING = "Corporate meeting";
    public static final String GOVERNMENT_FUNCTION = "Government Function";
    public static final String INCENTIVE = "Incentive";
    public static final String KNR = "KNR";
    public static final String LNR = "LNR";
    public static final String NETWORKING_EVENT = "Networking event";
    public static final String SCHOOL_FUNCTION = "School Function";
    public static final String SEMINAR = "Seminar";
    public static final String SOCIAL_EVENTS = "Social events";
    public static final String SPORTS = "Sports";
    public static final String TRADEFARE = "Tradefare";
    public static final String WEDDING = "Wedding";

    public static ArrayList<String> BUSINESS_EVENT_TYPE_LIST = new ArrayList<>(Arrays.asList(ASSOCIATION_MEETING,
            BUSINESS_LUNCH_DINNER, CHARITY, CONFERENCE_EXHIBITION, CORPORATE_MEETING, GOVERNMENT_FUNCTION, INCENTIVE,
            KNR, LNR, NETWORKING_EVENT, SCHOOL_FUNCTION, SEMINAR, SOCIAL_EVENTS, SPORTS, TRADEFARE, WEDDING));

    private BusinessEventType()
    {
    }
}
