package com.ihg.automation.selenium.common.hotel;

import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.CustomContainerBase;

public class AdvancedHotelSearchGridRowContainer extends CustomContainerBase
{
    public AdvancedHotelSearchGridRowContainer(Container parent)
    {
        super(parent);
    }

    public HotelDetails getHotelDetails()
    {
        return new HotelDetails(container);
    }
}
