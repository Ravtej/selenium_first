package com.ihg.automation.selenium.common.pages.program;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasText;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasTextAsInt;

import org.hamcrest.Matcher;

import com.ihg.automation.selenium.gwt.components.Componentable;
import com.ihg.automation.selenium.gwt.components.grid.CellsName;
import com.ihg.automation.selenium.gwt.components.grid.Grid;
import com.ihg.automation.selenium.gwt.components.grid.GridRow;
import com.ihg.automation.selenium.gwt.components.grid.GridRowXpath;

public class RewardClubTierLevelActivityRow extends GridRow<RewardClubTierLevelActivityRow.TierLevelCell>
{
    public final static String ACHIEVED = "Achieved";

    public RewardClubTierLevelActivityRow(Grid parent, GridRowXpath xpath)
    {
        super(parent, xpath);
    }

    public void verifyCurrent(String current)
    {
        verifyThat(getCell(TierLevelCell.CURRENT), hasText(current));
    }

    public void verify(Matcher<Componentable> current, Matcher<Componentable> toAchieve)
    {
        verifyThat(getCell(TierLevelCell.CURRENT), current);
        verifyThat(getCell(TierLevelCell.ACHIEVE), toAchieve);
    }

    public void verify(String current, Integer achieve)
    {
        String toAchieve = String.valueOf(achieve - Integer.valueOf(current));
        verify(hasText(current), hasText(toAchieve));
    }

    public void verify(Integer current, Integer achieve)
    {
        verify(hasTextAsInt(current), hasText(getDiff(current, achieve)));
    }

    public void verify(Matcher<Componentable> current, Matcher<Componentable> toAchieve,
            Matcher<Componentable> toMaintain)
    {
        verify(current, toAchieve);
        verifyThat(getCell(TierLevelCell.MAINTAIN), toMaintain);
    }

    public void verify(Integer current, Integer achieve, Integer maintain)
    {
        verify(hasText(String.valueOf(current)), hasText(getDiff(current, achieve)),
                hasText(getDiff(current, maintain)));
    }

    public void verifyMaintainIsAchieved(Matcher<Componentable> current, Matcher<Componentable> toAchieve)
    {
        verify(current, toAchieve, hasText(ACHIEVED));
    }

    public void verifyMaintainIsAchieved(Integer current, Integer achieve)
    {
        verifyMaintainIsAchieved(hasTextAsInt(current), hasText(getDiff(current, achieve)));
    }

    private String getDiff(Integer current, Integer maxValue)
    {
        return (maxValue != null) ? String.valueOf(maxValue - current) : null;
    }

    public enum TierLevelCell implements CellsName
    {
        TYPE("type"), CURRENT("current"), ACHIEVE("achieve"), MAINTAIN("maintain");

        private String name;

        private TierLevelCell(String name)
        {
            this.name = name;
        }

        @Override
        public String getName()
        {
            return name;
        }
    }
}
