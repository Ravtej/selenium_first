package com.ihg.automation.selenium.common.types;

import com.ihg.automation.selenium.gwt.components.EntityType;

public enum PhoneType implements EntityType
{
    BUSINESS("B", "Business"), //
    RESIDENTIAL("R", "Residential"), //
    OTHER("O", "Other");

    private String code;
    private String value;

    private PhoneType(String code, String value)
    {
        this.code = code;
        this.value = value;
    }

    @Override
    public String getCode()
    {
        return code;
    }

    @Override
    public String getValue()
    {
        return value;
    }

    @Override
    public String toString()
    {
        return getValue();
    }

    @Override
    public String getCodeWithValue()
    {
        return code + " - " + value;
    }
}
