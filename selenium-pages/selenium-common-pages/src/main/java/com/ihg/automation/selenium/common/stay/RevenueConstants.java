package com.ihg.automation.selenium.common.stay;

public class RevenueConstants
{
    public static final String ROOM_CODE = "001";
    public static final String FOOD_CODE = "002";
    public static final String BEVERAGE_CODE = "003";
    public static final String PHONE_CODE = "004";
    public static final String MEETING_CODE = "005";
    public static final String MANDATORY_LOYALTY_CODE = "006";
    public static final String OTHER_LOYALTY_CODE = "007";
    public static final String NO_LOYALTY_CODE = "008";
    public static final String TAX_CODE = "009";
}
