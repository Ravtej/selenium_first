package com.ihg.automation.selenium.common;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;

import org.hamcrest.Matcher;

import com.ihg.automation.selenium.common.types.program.RewardClubLevel;
import com.ihg.automation.selenium.gwt.components.Button;
import com.ihg.automation.selenium.gwt.components.Componentable;
import com.ihg.automation.selenium.gwt.components.CustomContainerBase;
import com.ihg.automation.selenium.gwt.components.dialog.ConfirmDialog;

public abstract class LeftPanelBase extends CustomContainerBase
{
    public final static String YELLOW_COLOR = "#fefcd3";
    public final static String BLUE_COLOR = "#dfe8f6";
    public static final String GREY_COLOR = "#cfd8e6";

    public Button getEndEditSession()
    {
        return new Button(container, Button.END_EDIT_SESSION);
    }

    public CustomerInfoPanel getCustomerInfoPanel()
    {
        return new CustomerInfoPanel();
    }

    public ProgramInfoPanel getProgramInfoPanel()
    {
        return new ProgramInfoPanel();
    }

    public Button getNewSearch()
    {
        return new Button(container, Button.NEW_SEARCH);
    }

    public Button getBackToResults()
    {
        return new Button(container, Button.BACK_TO_RESULTS);
    }

    public void clickBackToResults()
    {
        getBackToResults().clickAndWait();
    }

    public void clickNewSearch()
    {
        getNewSearch().clickAndWait();
    }

    public void goToNewSearch()
    {
        if (this.getNewSearch().isDisplayed())
        {
            clickNewSearch();
            ConfirmDialog dialog = new ConfirmDialog();
            if (dialog.isDisplayed())
            {
                dialog.clickYes();
            }
        }
    }

    public void verifyBalance(Matcher<Componentable> matcher)
    {
        verifyThat(getProgramInfoPanel().getPrograms().getRewardClubProgram().getPointsBalance(), matcher);
    }

    public void verifyBalance(String amount)
    {
        verifyBalance(hasText(amount));
    }

    public void verifyRCLevel(RewardClubLevel level)
    {
        verifyThat(getProgramInfoPanel().getPrograms().getRewardClubProgram().getLevelCode(), hasText(level.getCode()));
    }
}
