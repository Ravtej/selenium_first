package com.ihg.automation.selenium.common.pages;

import java.util.ArrayList;

import com.ihg.automation.selenium.gwt.components.EntityType;

public enum CreditCardType implements EntityType
{
    AMERICAN_EXPRESS("AX", "AMERICAN EXPRESS"), //
    BUSINESS_ACCOUNT("BE", "BUSINESS ADVANTAGE"), //
    CARTE_BLANCHE("CB", "CARTE BLANCHE"), //
    DINERS_CLUB("DC", "DINERS CLUB"), //
    DISCOVER_CARD("DS", "DISCOVER"), //
    EURO_CARD("EC", "EURO CARD"), //
    JCB("JC", "JCB (JAPANESE)"), //
    MASTER_CARD("MC", "MASTERCARD"), //
    VISA("VS", "VISA"), //
    CHINA_UNION_PAY("CU", "CHINA UNION PAY CREDIT");

    private String code;
    private String value;

    private CreditCardType(String code, String value)
    {
        this.code = code;
        this.value = value;
    }

    @Override
    public String getCode()
    {
        return code;
    }

    @Override
    public String getValue()
    {
        return value;
    }

    @Override
    public String toString()
    {
        return getValue();
    }

    @Override
    public String getCodeWithValue()
    {
        return code + " - " + value;
    }

    public static ArrayList<String> getDataList()
    {
        CreditCardType[] vals = values();
        ArrayList<String> res = new ArrayList<String>();
        for (CreditCardType val : vals)
        {
            res.add(val.getCodeWithValue());
        }

        return res;
    }

}
