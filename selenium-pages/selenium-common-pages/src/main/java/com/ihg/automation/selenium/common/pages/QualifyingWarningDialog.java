package com.ihg.automation.selenium.common.pages;

import com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory;
import com.ihg.automation.selenium.gwt.components.Button;
import com.ihg.automation.selenium.gwt.components.dialog.DialogBase;
import com.ihg.automation.selenium.listener.Verifier;

public class QualifyingWarningDialog extends DialogBase
{
    public QualifyingWarningDialog()
    {
        super(Type.CONFIRM);
    }

    public Button getSubmit()
    {
        return container.getButton(Button.SUBMIT);
    }

    public void clickSubmit(Verifier... verifiers)
    {
        getSubmit().clickAndWait(verifiers);
    }

    public void clickSubmitNoError(Verifier... verifiers)
    {
        getSubmit().clickAndWait(MessageBoxVerifierFactory.assertNoError().add(verifiers));
    }

    public Button getCancel()
    {
        return container.getButton(Button.CANCEL);
    }
}
