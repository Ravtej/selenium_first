package com.ihg.automation.selenium.common.order;

import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.label.Label;

public class ShippingInfoEdit extends ShippingInfoBase
{

    public ShippingInfoEdit(Container container)
    {
        super(container);
    }

    @Override
    public Label getEmail()
    {
        return container.getLabel("Delivery Email");
    }

    // TODO take care about Invalid check boxes...
}
