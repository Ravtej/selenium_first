package com.ihg.automation.selenium.common.types.program;

public class TierLevelExpiration
{
    public static final String EXPIRE_CURRENT_YEAR = "Expire Current Year";
    public static final String EXPIRE_NEXT_YEAR = "Expire Next Year";
    public static final String LIFETIME = "Lifetime";

    private TierLevelExpiration()
    {
    }
}
