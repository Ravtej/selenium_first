package com.ihg.automation.selenium.common.event;

import com.ihg.automation.selenium.gwt.components.CustomContainerBase;

public interface SearchAware
{
    public CustomContainerBase getSearchFields();

    public SearchButtonBar getButtonBar();
}
