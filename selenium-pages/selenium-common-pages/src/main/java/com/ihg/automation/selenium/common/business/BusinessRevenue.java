package com.ihg.automation.selenium.common.business;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasTextAsDouble;

import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.input.Input;
import com.ihg.automation.selenium.gwt.components.input.InputXpath;

public class BusinessRevenue extends BusinessRevenueView
{
    public BusinessRevenue(Container container, String labelText)
    {
        super(container, labelText);
    }

    @Override
    public Input getLocalAmount()
    {
        return new Input(label, new InputXpath.InputXpathBuilder().column(1, getPosition()).build(), LOCAL_AMOUNT);
    }

    public void populateLocalAmount(BusinessRevenueBean amount)
    {
        getLocalAmount().typeAndWait(amount.getAmount());
    }
}
