package com.ihg.automation.selenium.common.types.address;

import com.ihg.automation.selenium.gwt.components.EntityType;

public enum RegionChinaRoman implements EntityType
{
    ANHUI("34", "Anhui"), //
    AOMEN("92", "Aomen (zh) ***"), //
    BEIJING("11", "Beijing"), //
    BEI_JING_SHI(null, "bei jing shi"), //
    CHONGQING("50", "Chongqing"), //
    FUJIAN("35", "Fujian"), //
    GANSU("62", "Gansu"), //
    GUANGDONG("44", "Guangdong"), //
    GUANGXI("45", "Guangxi"), //
    GUIZHOU("52", "Guizhou"), //
    HAINAN("46", "Hainan"), //
    HEBEI("13", "Hebei"), //
    HEILONGJIANG("23", "Heilongjiang"), //
    HENAN("41", "Henan"), //
    HUBEI("42", "Hubei"), //
    HUNAN("43", "Hunan"), //
    JIANGXI("36", "Jiangxi"), //
    JILIN("22", "Jilin"), //
    LIAONING("21", "Liaoning"), //
    NEI_MONGOL("15", "Nei Mongol (mn)"), //
    NINGXIA("64", "Ningxia"), //
    QINGHAI("63", "Qinghai"), //
    SHAANXI("61", "Shaanxi"), //
    SHANDONG("37", "Shandong"), //
    SHANGHAI("31", "Shanghai"), //
    SHANXI("14", "Shanxi"), //
    SICHUAN("51", "Sichuan"), //
    TAIWAN("71", "Taiwan *"), //
    TIANJIN("12", "Tianjin"), //
    XIANGGANG("91", "Xianggang (zh) **"), //
    XINJIANG("65", "Xinjiang"), //
    XIZANG("54", "Xizang"), //
    YUNNAN("53", "Yunnan"), //
    ZHEJIANG("33", "Zhejiang");

    private String code;
    private String value;

    private RegionChinaRoman(String code, String value)
    {
        this.code = code;
        this.value = value;
    }

    @Override
    public String getCode()
    {
        return code;
    }

    @Override
    public String getValue()
    {
        return value;
    }

    @Override
    public String getCodeWithValue()
    {
        return code + " - " + value;
    }

    @Override
    public String toString()
    {
        return getValue();
    }
}
