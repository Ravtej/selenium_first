package com.ihg.automation.selenium.common.order;

import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.label.Label;

public class ShippingInfoView extends ShippingInfoBase
{

    public ShippingInfoView(Container container)
    {
        super(container);
    }

    @Override
    public Label getEmail()
    {
        return container.getLabel("Delivery E-mail");
    }

}
