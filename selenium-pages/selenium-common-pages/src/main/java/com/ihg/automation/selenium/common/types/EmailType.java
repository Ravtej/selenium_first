package com.ihg.automation.selenium.common.types;

import com.ihg.automation.selenium.gwt.components.EntityType;

public enum EmailType implements EntityType
{
    BUSINESS("B", "Business"), PERSONAL("H", "Personal"), UNDEFINED("U", "Undefined");

    private String code;
    private String value;

    private EmailType(String code, String value)
    {
        this.code = code;
        this.value = value;
    }

    @Override
    public String getCode()
    {
        return code;
    }

    @Override
    public String getValue()
    {
        return value;
    }

    @Override
    public String toString()
    {
        return getValue();
    }

    @Override
    public String getCodeWithValue()
    {
        return code + " - " + value;
    }
}
