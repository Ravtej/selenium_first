package com.ihg.automation.selenium.common.personal;

import com.ihg.automation.selenium.common.pages.Populate;
import com.ihg.automation.selenium.common.pages.VerifyMultiMode;
import com.ihg.automation.selenium.gwt.components.Component;
import com.ihg.automation.selenium.listener.Verifier;

public interface EditableComponent<T> extends Populate<T>, VerifyMultiMode<T>
{
    Component getPreferredIcon();

    void clickSave(Verifier... verifiers);

    void clickCancel(Verifier... verifiers);

    void clickEdit(Verifier... verifiers);

    void expand();

    void update(T contact, Verifier... verifiers);

    void remove(Verifier... verifiers);
}
