package com.ihg.automation.selenium.common.event;

import static com.ihg.automation.common.DateUtils.DATE_FORMAT;
import static com.ihg.automation.selenium.common.Constant.RC_POINTS;
import static com.ihg.automation.selenium.common.event.EventTransactionType.Goodwill.GOODWILL;
import static com.ihg.automation.selenium.common.event.EventTransactionType.Loyalty.Q_NIGHT_TRANSFER;
import static com.ihg.automation.selenium.common.event.EventTransactionType.Membership.ENROLL;
import static com.ihg.automation.selenium.common.event.EventTransactionType.Offer.OFFER_REGISTRATION;
import static org.apache.commons.lang3.StringUtils.EMPTY;

import org.joda.time.LocalDate;

import com.ihg.automation.common.DateUtils;
import com.ihg.automation.selenium.common.offer.Offer;
import com.ihg.automation.selenium.common.types.program.Program;

public class AllEventsRowFactory
{
    private static final String NIGHT_TRANSFER_DETAILS_PATTERN = "%s Qualifying Nights";
    private static final String IHG_REWARDS_CLUB_POINTS = "IHG Rewards Club Points";

    public static AllEventsRow getNightTransfer(Object nights)
    {
        return new AllEventsRow(DateUtils.getFormattedDate(), Q_NIGHT_TRANSFER,
                String.format(NIGHT_TRANSFER_DETAILS_PATTERN, nights), EMPTY, EMPTY);
    }

    public static AllEventsRow getPointEvent(String event, Object points)
    {
        return new AllEventsRow(DateUtils.getFormattedDate(), event, IHG_REWARDS_CLUB_POINTS, String.valueOf(points),
                EMPTY);
    }

    public static AllEventsRow getEnrollEvent(LocalDate date, Program program)
    {
        return new AllEventsRow(date.toString(DATE_FORMAT), ENROLL, program.getCode(), EMPTY, EMPTY);
    }

    public static AllEventsRow getEnrollEvent(Program program)
    {
        return getEnrollEvent(DateUtils.now(), program);
    }

    public static AllEventsRow getOfferRegistrationEvent(LocalDate date, Offer offer)
    {
        return new AllEventsRow(date.toString(DATE_FORMAT), OFFER_REGISTRATION, offer.getName(), EMPTY, EMPTY);
    }

    public static AllEventsRow getOfferRegistrationEvent(Offer offer)
    {
        return getOfferRegistrationEvent(DateUtils.now(), offer);
    }

    public static AllEventsRow getGoodwillEvent(String balanceAmount)
    {
        return new AllEventsRow(DateUtils.getFormattedDate(), GOODWILL, RC_POINTS, balanceAmount, EMPTY);
    }
}
