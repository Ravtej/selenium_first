package com.ihg.automation.selenium.common.contact;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.ViewableMatcher.hasText;
import static com.ihg.automation.selenium.matchers.text.EntityMatcherFactory.isValue;

import com.ihg.automation.selenium.common.member.GuestPhone;
import com.ihg.automation.selenium.gwt.components.ComponentColumnPosition;
import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.Link;
import com.ihg.automation.selenium.gwt.components.Mode;
import com.ihg.automation.selenium.gwt.components.input.Input;
import com.ihg.automation.selenium.gwt.components.input.Select;
import com.ihg.automation.selenium.gwt.components.label.Label;
import com.ihg.automation.selenium.gwt.components.panel.EditablePanel;

public class Phone extends ContactBase<GuestPhone>
{
    private ComponentColumnPosition POSITION = ComponentColumnPosition.TABLE;
    private String labelText = "Phone";

    public Phone(Container container, int index)
    {
        super(new EditablePanel(container.getSeparator("Phone"), index));
    }

    public Phone(Container container, String leftLabelText, ComponentColumnPosition position)
    {
        super(container);
        POSITION = position;
        labelText = leftLabelText;
    }

    public Phone(Container container)
    {
        super(container);
    }

    public Label getFullPhone()
    {
        return container.getLabel(labelText);
    }

    public Link getCopyToSms()
    {
        return container.getLink("Copy to Sms");
    }

    public Select getPhoneSubType()
    {
        return container.getLabel(TYPE_LABEL).getSelect(TYPE_LABEL, 2, POSITION);
    }

    public Input getNumber()
    {
        return container.getInputByLabel(labelText);
    }

    @Override
    public void populate(GuestPhone phone)
    {
        super.populate(phone);

        getPhoneSubType().select(phone.getPhoneSubType().getValue());
        getNumber().type(phone.getNumber());
    }

    @Override
    public void verify(GuestPhone phone, Mode mode)
    {
        if (mode == Mode.VIEW)
        {
            verifyThat(getType(), hasText(phone.getType().getValue() + "\n" + phone.getPhoneSubType(), Mode.VIEW));
        }
        else
        {
            verifyThat(getType(), hasText(isValue(phone.getType()), Mode.EDIT));
            verifyThat(getPhoneSubType(), hasText(isValue(phone.getPhoneSubType()), Mode.EDIT));
        }
        verifyThat(getNumber(), hasText(phone.getNumber(), mode));
    }
}
