package com.ihg.automation.selenium.common.freenight;

public class CancelReason
{
    public static final String CANCEL = "Cancel";
    public static final String NO_SHOW = "No Show";

    private CancelReason()
    {
    }
}
