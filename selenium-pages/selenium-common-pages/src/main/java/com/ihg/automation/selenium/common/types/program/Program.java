package com.ihg.automation.selenium.common.types.program;

import java.util.ArrayList;
import java.util.Arrays;

import com.ihg.automation.selenium.gwt.components.EntityType;

public enum Program implements EntityType
{
    IHG("IHG", "IHG Guest Account"), //
    HTL("HTL", "Hotel"), //
    RC("PC", "RC", "IHG Rewards Club"), //
    AMB("AMB", "Ambassador"), //
    EMP("EMP", "Employee Program"), //
    DR("DR", "Dining Rewards"), //
    KAR("KAR", "Karma"), //
    BR("BR", "IHG Business Rewards");

    private String serviceCode;
    private String code;
    private String value;

    private Program(String serviceCode, String code, String description)
    {
        this.serviceCode = serviceCode;
        this.code = code;
        this.value = description;
    }

    private Program(String code, String description)
    {
        this(code, code, description);
    }

    public String getServiceCode()
    {
        return serviceCode;
    }

    @Override
    public String getCode()
    {
        return code;
    }

    @Override
    public String getValue()
    {
        return value;
    }

    @Override
    public String getCodeWithValue()
    {
        return code + " - " + value;
    }

    public static ArrayList<Program> getProgramList()
    {
        ArrayList<Program> programs = new ArrayList<Program>(Arrays.asList(values()));
        programs.remove(DR);
        programs.remove(KAR);
        return programs;
    }
}
