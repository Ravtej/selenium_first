package com.ihg.automation.selenium.common.event;

import com.ihg.automation.selenium.gwt.components.Button;
import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.CustomContainerBase;

public class SearchButtonBar extends CustomContainerBase
{
    public SearchButtonBar(Container container)
    {
        super(container);
    }

    public Button getSearch()
    {
        return container.getButton("Search");
    }

    public Button getClearCriteria()
    {
        return container.getButton("Clear Criteria");
    }

    public void clickSearch()
    {
        getSearch().clickAndWait();
    }

    public void clickClearCriteria()
    {
        getClearCriteria().clickAndWait();
    }
}
