package com.ihg.automation.selenium.common.pages;

import com.ihg.automation.selenium.gwt.components.Button;
import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.CustomContainerBase;
import com.ihg.automation.selenium.gwt.components.utils.FindStep;
import com.ihg.automation.selenium.listener.Verifier;

public abstract class SearchBase extends CustomContainerBase
{
    public static final String SUB_PANEL_HEADER_TEXT_PATTERN = ".//div[@class=''x-panel-body x-panel-body-noheader'' and descendant::div[@class=''gwt-Label'' and {0}]]";

    public SearchBase(Container container)
    {
        super(container);
    }

    public SearchBase(String type, String name, FindStep step)
    {
        super(null, type, name, step);
    }

    public Button getSearch()
    {
        return container.getButton(Button.SEARCH);
    }

    public Button getClear()
    {
        return container.getButton(Button.CLEAR);
    }

    protected Button getClearCriteria()
    {
        return container.getButton(Button.CLEAR_CRITERIA);
    }

    public void clickSearch(Verifier... verifiers)
    {
        getSearch().clickAndWait(verifiers);
    }

    public void clickClear(Verifier... verifiers)
    {
        getClear().clickAndWait(verifiers);
    }

    public void clickClearCriteria()
    {
        getClearCriteria().clickAndWait();
    }
}
