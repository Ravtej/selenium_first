package com.ihg.automation.selenium.common.pages.program;

import com.ihg.automation.selenium.common.pages.program.RewardClubTierLevelActivityRow.TierLevelCell;
import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.grid.CellsText;
import com.ihg.automation.selenium.gwt.components.grid.Grid;
import com.ihg.automation.selenium.gwt.components.grid.GridRowXpath;

public class RewardClubTierLevelActivityGrid extends Grid<RewardClubTierLevelActivityRow, TierLevelCell>
{
    public RewardClubTierLevelActivityGrid(Container container)
    {
        super(container);
    }

    public RewardClubTierLevelActivityRow getPointsRow()
    {
        return getRow(
                new GridRowXpath.GridRowXpathBuilder().cell(TierLevelCell.TYPE, TierLevelAccumulator.POINTS).build());
    }

    public RewardClubTierLevelActivityRow getNightsRow()
    {
        return getRow(
                new GridRowXpath.GridRowXpathBuilder().cell(TierLevelCell.TYPE, TierLevelAccumulator.NIGHTS).build());
    }

    public RewardClubTierLevelActivityRow getChainsRow()
    {
        return getRow(
                new GridRowXpath.GridRowXpathBuilder().cell(TierLevelCell.TYPE, TierLevelAccumulator.CHAINS).build());
    }

    public PointGridRowContainer getPointsRowContainer()
    {
        return getPointsRow().expand(PointGridRowContainer.class);
    }

    public NightGridRowContainer getNightsRowContainer()
    {
        return getNightsRow().expand(NightGridRowContainer.class);
    }

    private enum TierLevelAccumulator implements CellsText
    {
        POINTS("Qualifying points"), NIGHTS("Qualifying nights"), CHAINS("Qualifying chains");

        private String text;

        private TierLevelAccumulator(String text)
        {
            this.text = text;
        }

        @Override
        public String getText()
        {
            return text;
        }
    }
}
