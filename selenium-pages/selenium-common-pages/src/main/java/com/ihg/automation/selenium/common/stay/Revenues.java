package com.ihg.automation.selenium.common.stay;

import static com.ihg.automation.selenium.common.stay.RevenueConstants.BEVERAGE_CODE;
import static com.ihg.automation.selenium.common.stay.RevenueConstants.FOOD_CODE;
import static com.ihg.automation.selenium.common.stay.RevenueConstants.MANDATORY_LOYALTY_CODE;
import static com.ihg.automation.selenium.common.stay.RevenueConstants.MEETING_CODE;
import static com.ihg.automation.selenium.common.stay.RevenueConstants.NO_LOYALTY_CODE;
import static com.ihg.automation.selenium.common.stay.RevenueConstants.OTHER_LOYALTY_CODE;
import static com.ihg.automation.selenium.common.stay.RevenueConstants.PHONE_CODE;
import static com.ihg.automation.selenium.common.stay.RevenueConstants.ROOM_CODE;
import static com.ihg.automation.selenium.common.stay.RevenueConstants.TAX_CODE;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

public class Revenues implements Cloneable
{

    private List<String> categories = Arrays.asList(ROOM_CODE, FOOD_CODE, BEVERAGE_CODE, PHONE_CODE, MEETING_CODE,
            MANDATORY_LOYALTY_CODE, OTHER_LOYALTY_CODE, NO_LOYALTY_CODE, TAX_CODE);

    private List<String> noRoomCategories = Arrays.asList(FOOD_CODE, BEVERAGE_CODE, PHONE_CODE, MEETING_CODE,
            MANDATORY_LOYALTY_CODE, OTHER_LOYALTY_CODE, NO_LOYALTY_CODE, TAX_CODE);

    private HashMap<String, Revenue> revenuesMap = new HashMap<String, Revenue>(categories.size());

    // Auto-calculated(on UI) revenues
    // TODO only amount and local amount is used
    private Revenue overallTotal = new Revenue();
    private Revenue totalNonQualifying = new Revenue();
    private Revenue totalQualifying = new Revenue();

    public Revenues()
    {
        for (String category : categories)
        {
            put(category, new Revenue());
        }
    }

    public void put(String category, Revenue revenue)
    {
        if (!isValidCategory(category))
        {
            throw new IllegalArgumentException("Trying to put illegal category " + category);
        }

        revenue.setRevenueCategoryCode(category);
        revenuesMap.put(category, revenue);
    }

    public boolean isValidCategory(String category)
    {
        return categories.contains(category);
    }

    public Revenue get(String category)
    {
        return revenuesMap.get(category);
    }

    public Collection<Revenue> getRevenuesAsList()
    {
        return revenuesMap.values();
    }

    public Revenue getRoom()
    {
        return get(ROOM_CODE);
    }

    public void setRoom(Revenue room)
    {
        put(ROOM_CODE, room);
    }

    public Revenue getFood()
    {
        return get(FOOD_CODE);
    }

    public void setFood(Revenue food)
    {
        put(FOOD_CODE, food);
    }

    public Revenue getBeverage()
    {
        return get(BEVERAGE_CODE);
    }

    public void setBeverage(Revenue beverage)
    {
        put(BEVERAGE_CODE, beverage);
    }

    public Revenue getPhone()
    {
        return get(PHONE_CODE);
    }

    public void setPhone(Revenue phone)
    {
        put(PHONE_CODE, phone);
    }

    public Revenue getMeeting()
    {
        return get(MEETING_CODE);
    }

    public void setMeeting(Revenue meeting)
    {
        put(MEETING_CODE, meeting);
    }

    public Revenue getOtherLoyalty()
    {
        return get(OTHER_LOYALTY_CODE);
    }

    public void setOtherLoyalty(Revenue otherLoyalty)
    {
        put(OTHER_LOYALTY_CODE, otherLoyalty);
    }

    public Revenue getMandatoryLoyalty()
    {
        return get(MANDATORY_LOYALTY_CODE);
    }

    public void setMandatoryLoyalty(Revenue mandatoryLoyalty)
    {
        put(MANDATORY_LOYALTY_CODE, mandatoryLoyalty);
    }

    public Revenue getNoLoyalty()
    {
        return get(NO_LOYALTY_CODE);
    }

    public void setNoLoyalty(Revenue noLoyalty)
    {
        put(NO_LOYALTY_CODE, noLoyalty);
    }

    public Revenue getTax()
    {
        return get(TAX_CODE);
    }

    public void setTax(Revenue tax)
    {
        tax.setQualifying(false);
        put(TAX_CODE, tax);
    }

    public Revenue getOverallTotal()
    {
        return overallTotal;
    }

    public void setOverallTotal(Revenue overallTotal)
    {
        this.overallTotal = overallTotal;
    }

    public Revenue getTotalNonQualifying()
    {
        return totalNonQualifying;
    }

    public void setTotalNonQualifying(Revenue totalNonQualifying)
    {
        this.totalNonQualifying = totalNonQualifying;
    }

    public Revenue getTotalQualifying()
    {
        return totalQualifying;
    }

    public void setTotalQualifying(Revenue totalQualifying)
    {
        this.totalQualifying = totalQualifying;
    }

    @Override
    public Revenues clone()
    {
        Revenues newRevenues = new Revenues();

        for (String category : categories)
        {
            newRevenues.put(category, get(category).clone());
        }

        newRevenues.overallTotal = overallTotal.clone();
        newRevenues.totalNonQualifying = totalNonQualifying.clone();
        newRevenues.totalQualifying = totalQualifying.clone();

        return newRevenues;
    }
}
