package com.ihg.automation.selenium.common.business;

import com.ihg.automation.selenium.common.EventSourceAware;
import com.ihg.automation.selenium.common.event.EventSourceExtended;
import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.CustomContainerBase;

public class BusinessRewardsDetails extends CustomContainerBase implements EventSourceAware
{
    private final static String TOTAL_ELIGIBLE_REVENUE_LABEL = "Total Eligible Revenue";

    public BusinessRewardsDetails(Container container)
    {
        super(container);
    }

    public EventInformationFields getEventInformation()
    {
        return new EventInformationFields(container.getSeparator("Event Information"));
    }

    public BusinessRevenueView getTotalEligibleRevenue()
    {
        return new BusinessRevenueView(container, TOTAL_ELIGIBLE_REVENUE_LABEL);
    }

    public DiscretionaryPointsFields getDiscretionaryPoints()
    {
        return new DiscretionaryPointsFields(container.getSeparator("Discretionary Points"));
    }

    public OffersFieldsView getOffers()
    {
        return new OffersFieldsView(container);
    }

    public TotalEventAwardFields getTotalEventAward()
    {
        return new TotalEventAwardFields(container.getSeparator("Total Event Award"));
    }

    @Override
    public EventSourceExtended getSource()
    {
        return new EventSourceExtended(container);
    }

}
