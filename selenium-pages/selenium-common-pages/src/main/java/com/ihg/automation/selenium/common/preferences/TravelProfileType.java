package com.ihg.automation.selenium.common.preferences;

import com.ihg.automation.selenium.gwt.components.EntityType;

public enum TravelProfileType implements EntityType
{
    LEISURE("LS", "Leisure"), //
    BUSINESS("BS", "Business"), //
    BUSINESS_GROUP("BG", "Business Group"), //
    LEISURE_GROUP("LG", "Leisure Group"), //
    BUSINESS_REWARDS("Business Rewards", "Business Rewards");

    private String code;
    private String value;

    private TravelProfileType(String code, String value)
    {
        this.code = code;
        this.value = value;
    }

    @Override
    public String getCode()
    {
        return code;
    }

    @Override
    public String getValue()
    {
        return value;
    }

    @Override
    public String toString()
    {
        return getValue();
    }

    @Override
    public String getCodeWithValue()
    {
        return code + " - " + value;
    }

}
