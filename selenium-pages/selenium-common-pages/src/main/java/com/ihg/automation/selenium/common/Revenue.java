package com.ihg.automation.selenium.common;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasTextAsFormattedDouble;

import com.ihg.automation.selenium.common.pages.LabelContainerBase;
import com.ihg.automation.selenium.common.pages.Verify;
import com.ihg.automation.selenium.gwt.components.Component;
import com.ihg.automation.selenium.gwt.components.ComponentColumnPosition;
import com.ihg.automation.selenium.gwt.components.Container;

public class Revenue extends LabelContainerBase implements Verify<com.ihg.automation.selenium.common.stay.Revenue>
{
    protected static final String LOCAL_AMOUNT = "Local Amount";
    protected static final String USD_AMOUNT = "USD Amount";

    public Revenue(Container container, String labelText)
    {
        super(container.getLabel(labelText));
    }

    public Component getLocalAmount()
    {
        return label.getComponent(LOCAL_AMOUNT, 1, getPosition());
    }

    public Component getUSDAmount()
    {
        return label.getComponent(USD_AMOUNT, 2, getPosition());
    }

    protected ComponentColumnPosition getPosition()
    {
        return ComponentColumnPosition.FOLLOWING_TD;
    }

    @Override
    public String toString()
    {
        return label.getDescription();
    }

    @Override
    public void verify(com.ihg.automation.selenium.common.stay.Revenue revenue)
    {
        verifyThat(getLocalAmount(), hasTextAsFormattedDouble(revenue.getAmount()));
        verifyThat(getUSDAmount(), hasTextAsFormattedDouble(revenue.getUsdAmount()));
    }
}
