package com.ihg.automation.selenium.common.pages.login;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.assertThat;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasTextWithWait;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ihg.automation.selenium.gwt.components.Component;
import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.CustomContainerBase;
import com.ihg.automation.selenium.gwt.components.Link;
import com.ihg.automation.selenium.gwt.components.dialog.ConfirmDialog;
import com.ihg.automation.selenium.gwt.components.input.SearchInput;
import com.ihg.automation.selenium.gwt.components.input.Select;
import com.ihg.automation.selenium.gwt.components.utils.FindStepUtils;

public class TopUserSection extends CustomContainerBase
{
    private static final Logger logger = LoggerFactory.getLogger(TopUserSection.class);

    public TopUserSection()
    {
        super(new Container("Top User Section ", "User Section",
                FindStepUtils.byXpathWithWait(".//tr[child::td[contains(., 'Welcome,')]]")));
    }

    public Component getUser()
    {
        return new Component(container, "Label", "User", FindStepUtils.byXpath("./td[1]/span"));
    }

    public SearchInput getHotel()
    {
        return new SearchInput(container.getLabel("Hotel"));
    }

    public Select getRole()
    {
        Select roleSelect = container.getSelectByLabel("Welcome,");
        roleSelect.setName("Role");
        return roleSelect;
    }

    public Link getSignOff()
    {
        return new Link(container, "Sign Off");
    }

    public ConfirmDialog clickSignOff()
    {
        getSignOff().clickAndWait();

        return new ConfirmDialog();
    }

    public void signOff()
    {
        clickSignOff();
        confirmDialog(true);
        confirmDialog(true);
    }

    public void switchUserRole(String role, boolean isMemeberInContext)
    {
        if (getRole().select(role) && isMemeberInContext)
        {
            closeConfirmDialog();
        }
        assertThat(getRole(), hasTextWithWait(role));
    }

    public void switchUserRole(String role)
    {
        switchUserRole(role, true);
    }

    private void confirmDialog(boolean bConfirm)
    {
        ConfirmDialog warningPopUp = new ConfirmDialog();

        if (warningPopUp.isDisplayed())
        {
            if (bConfirm)
                warningPopUp.clickYes();
            else
                warningPopUp.clickNo();
        }
    }

    public boolean switchHotel(String hotel, boolean isMemberInContext)
    {
        if (!getHotel().getText().equals(hotel))
        {
            getHotel().typeAndWait(hotel);
            try
            {
                Thread.sleep(500);
            }
            catch (InterruptedException e)
            {
                logger.error("Sleep exception", e);
            }
            if (isMemberInContext)
            {
                closeConfirmDialog();
            }
            return true;
        }
        return false;
    }

    public boolean switchHotel(String hotel)
    {
        return switchHotel(hotel, true);
    }

    private void closeConfirmDialog()
    {
        ConfirmDialog warningPopUp = new ConfirmDialog();
        warningPopUp.close(true);
        verifyThat(warningPopUp, displayed(false));
    }
}
