package com.ihg.automation.selenium.common.pages;

public interface NavigatePage
{
    public void goTo();
}
