package com.ihg.automation.selenium.common.unit;

import com.ihg.automation.selenium.gwt.components.MultiModeComponent;
import com.ihg.automation.selenium.gwt.components.label.Label;

public interface MemberFieldsAware
{
    public MultiModeComponent getMembershipId();

    public Label getMemberName();
}
