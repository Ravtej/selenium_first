package com.ihg.automation.selenium.common.business;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.enabled;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasTextWithWait;
import static com.ihg.automation.selenium.matchers.component.IsSelected.isSelected;

import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.CustomContainerBase;
import com.ihg.automation.selenium.gwt.components.checkbox.CheckBox;
import com.ihg.automation.selenium.gwt.components.input.DateInput;
import com.ihg.automation.selenium.gwt.components.input.Input;
import com.ihg.automation.selenium.gwt.components.panel.FieldSet;
import com.ihg.automation.selenium.gwt.components.utils.ByText;

public class GuestRoomsFields extends CustomContainerBase
{
    private final static String ELIGIBLE_ROOM_REVENUE_LABEL = "Eligible Room Revenue";
    private final static String TOTAL_ROOM_LABEL = "Total Room";

    public GuestRoomsFields(Container container)
    {

        super(container.getFieldSet("Guest Rooms"));
    }

    public CheckBox getIncludeGuestRoomsRevenue()
    {
        return container.getCheckBox("Include additional Guest Rooms Revenue");
    }

    public DateInput getStayStartDate()
    {
        return new DateInput(container.getLabel("Stay Start Date"));
    }

    public DateInput getStayEndDate()
    {
        return new DateInput(container.getLabel("Stay End Date"));
    }

    public Input getTotalGuestRooms()
    {
        return container.getInputByLabel("Total Guest Rooms");
    }

    public Input getTotalRoomNights()
    {
        return container.getInputByLabel("Total Room Nights");
    }

    public Input getTotalGuests()
    {
        return container.getInputByLabel("Total Guests");
    }

    public Input getCorporateID()
    {
        return container.getInputByLabel("Corporate ID");
    }

    public Input getIATANumber()
    {
        return container.getInputByLabel("IATA Number");
    }

    public Input getKnr()
    {
        return container.getInputByLabel("KNR");
    }

    public CheckBox getKnrCheckBox()
    {
        return new CheckBox(container, ByText.exact("KNR"), CheckBox.CHECK_BOX_PATTERN2);
    }

    public BusinessRevenueView getEligibleRoomRevenue()
    {
        return new BusinessRevenueView(container, ELIGIBLE_ROOM_REVENUE_LABEL);
    }

    public BusinessRevenue getTotalRoom()
    {
        return new BusinessRevenue(container, TOTAL_ROOM_LABEL);
    }

    public void expand()
    {
        ((FieldSet) container).expand();
    }

    public void verifyEnabled(boolean isEnabled)
    {
        verifyThat(getTotalGuestRooms(), enabled(isEnabled));
        verifyThat(getStayEndDate(), enabled(isEnabled));
        verifyThat(getStayStartDate(), enabled(isEnabled));
        verifyThat(getTotalGuests(), enabled(isEnabled));
        verifyThat(getTotalRoomNights(), enabled(isEnabled));
    }

    public void populate(GuestRoom guestRoom)
    {
        getIncludeGuestRoomsRevenue().check();

        getStayStartDate().type(guestRoom.getStartDate());
        getStayEndDate().type(guestRoom.getEndDate());
        getTotalGuestRooms().type(guestRoom.getTotalGuestRooms());
        getTotalRoomNights().type(guestRoom.getTotalRoomNights());
        getTotalGuests().type(guestRoom.getTotalGuests());
        getCorporateID().type(guestRoom.getCorporateID());
        getIATANumber().type(guestRoom.getIataNumber());

        if (guestRoom.getKnr() != null)
        {
            getKnrCheckBox().check();
            getKnr().type(guestRoom.getKnr());
        }

    }

    public void verifyKnr(String knrUpdated, boolean selected)
    {
        verifyThat(getKnrCheckBox(), isSelected(selected));
        verifyThat(getKnr(), hasTextWithWait(knrUpdated));
    }
}
