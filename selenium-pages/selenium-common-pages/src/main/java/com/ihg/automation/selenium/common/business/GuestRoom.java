package com.ihg.automation.selenium.common.business;

public class GuestRoom
{
    private String startDate;
    private String endDate;
    private int totalGuestRooms;
    private int totalRoomNights;
    private int totalGuests;
    private String corporateID;
    private String iataNumber;
    private String knr;
    private BusinessRevenueBean totalRoom;
    private BusinessRevenueBean eligibleRoomRevenue;

    public String getStartDate()
    {
        return startDate;
    }

    public void setStartDate(String startDate)
    {
        this.startDate = startDate;
    }

    public String getEndDate()
    {
        return endDate;
    }

    public void setEndDate(String endDate)
    {
        this.endDate = endDate;
    }

    public int getTotalGuestRooms()
    {
        return totalGuestRooms;
    }

    public void setTotalGuestRooms(int totalGuestRooms)
    {
        this.totalGuestRooms = totalGuestRooms;
    }

    public int getTotalRoomNights()
    {
        return totalRoomNights;
    }

    public void setTotalRoomNights(int totalRoomNights)
    {
        this.totalRoomNights = totalRoomNights;
    }

    public int getTotalGuests()
    {
        return totalGuests;
    }

    public void setTotalGuests(int totalGuests)
    {
        this.totalGuests = totalGuests;
    }

    public String getCorporateID()
    {
        return corporateID;
    }

    public void setCorporateID(String corporateID)
    {
        this.corporateID = corporateID;
    }

    public String getIataNumber()
    {
        return iataNumber;
    }

    public void setIataNumber(String iataNumber)
    {
        this.iataNumber = iataNumber;
    }

    public String getKnr()
    {
        return knr;
    }

    public void setKnr(String knr)
    {
        this.knr = knr;
    }

    public BusinessRevenueBean getTotalRoom()
    {
        return totalRoom;
    }

    public void setTotalRoom(BusinessRevenueBean totalRoom)
    {
        this.totalRoom = totalRoom;
    }

    public BusinessRevenueBean getEligibleRoomRevenue()
    {
        return eligibleRoomRevenue;
    }

    public void setEligibleRoomRevenue(BusinessRevenueBean eligibleRoomRevenue)
    {
        this.eligibleRoomRevenue = eligibleRoomRevenue;
    }
}
