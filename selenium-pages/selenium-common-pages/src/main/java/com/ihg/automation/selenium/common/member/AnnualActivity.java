package com.ihg.automation.selenium.common.member;

import static com.ihg.automation.selenium.formatter.Converter.stringToInteger;

public class AnnualActivity
{
    private int year;

    private int tierLevelNights;
    private int rewardNights;
    private int qualifiedNights;
    private int nonQualifiedNights;
    private int qualifiedChains;
    private int totalQualifiedUnits;

    private int totalUnits;

    public int getYear()
    {
        return year;
    }

    public void setDate(int year)
    {
        this.year = year;
    }

    public int getTierLevelNights()
    {
        return tierLevelNights;
    }

    public void setTierLevelNightsFromString(String tierLevelNights)
    {
        setTierLevelNights(stringToInteger(tierLevelNights));
    }

    public void setTierLevelNights(int tierLevelNights)
    {
        this.tierLevelNights = tierLevelNights;
    }

    public int getRewardNights()
    {
        return rewardNights;
    }

    public void setRewardNightsFromString(String rewardNights)
    {
        setRewardNights(stringToInteger(rewardNights));
    }

    public void setRewardNights(int rewardNights)
    {
        this.rewardNights = rewardNights;
    }

    public int getQualifiedNights()
    {
        return qualifiedNights;
    }

    public void setQualifiedNightsFromString(String qualifiedNights)
    {
        setQualifiedNights(stringToInteger(qualifiedNights));
    }

    public void setQualifiedNights(int qualifiedNights)
    {
        this.qualifiedNights = qualifiedNights;
    }

    public int getNonQualifiedNights()
    {
        return nonQualifiedNights;
    }

    public void setNonQualifiedNightsFromString(String nonQualifiedNights)
    {
        setNonQualifiedNights(stringToInteger(nonQualifiedNights));
    }

    public void setNonQualifiedNights(int nonQualifiedNights)
    {
        this.nonQualifiedNights = nonQualifiedNights;
    }

    public int getQualifiedChains()
    {
        return qualifiedChains;
    }

    public void setQualifiedChainsFromString(String qualifiedChains)
    {
        setQualifiedChains(stringToInteger(qualifiedChains));
    }

    public void setQualifiedChains(int qualifiedChains)
    {
        this.qualifiedChains = qualifiedChains;
    }

    public int getTotalQualifiedUnits()
    {
        return totalQualifiedUnits;
    }

    public void setTotalQualifiedUnitsFromString(String totalQualifiedUnits)
    {
        setTotalQualifiedUnits(stringToInteger(totalQualifiedUnits));
    }

    public void setTotalQualifiedUnits(int totalQualifiedUnits)
    {
        this.totalQualifiedUnits = totalQualifiedUnits;
    }

    public int getTotalUnits()
    {
        return totalUnits;
    }

    public void setTotalUnitsFromString(String totalUnits)
    {
        setTotalUnits(stringToInteger(totalUnits));
    }

    public void setTotalUnits(int totalUnits)
    {
        this.totalUnits = totalUnits;
    }
}
