package com.ihg.automation.selenium.common.types.name;

import com.ihg.automation.selenium.common.types.LabelEnumAware;

public enum TitleLabel implements LabelEnumAware
{
    SURNAME_2ND("Surname 2"), TITLE("Title");

    private String label;

    private TitleLabel(String label)
    {
        this.label = label;
    }

    @Override
    public String getLabel()
    {
        return label;
    }
}
