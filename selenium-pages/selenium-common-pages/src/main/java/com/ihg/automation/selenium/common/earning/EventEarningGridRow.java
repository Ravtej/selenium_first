package com.ihg.automation.selenium.common.earning;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static com.ihg.automation.selenium.matchers.text.AsFormattedInteger.asFormattedInt;

import com.ihg.automation.selenium.common.event.EarningEvent;
import com.ihg.automation.selenium.common.pages.Verify;
import com.ihg.automation.selenium.gwt.components.grid.CellsName;
import com.ihg.automation.selenium.gwt.components.grid.GridRow;
import com.ihg.automation.selenium.gwt.components.grid.GridRowXpath;

public class EventEarningGridRow extends GridRow<EventEarningGridRow.EventEarningCell> implements Verify<EarningEvent>
{
    public EventEarningGridRow(EventEarningGrid parent, GridRowXpath xpath)
    {
        super(parent, xpath);
    }

    public enum EventEarningCell implements CellsName
    {
        TYPE("offerCode"), AMOUNT("amount"), QUALIFY_AMOUNT("qualifyAmountEarned"), UNIT_TYPE("uiUnitType"), ALLIANCE_TRANSACTION_CODE(
                "allianceTransactionCode"), DATE_EARNED("dateEarned"), STATUS("status"), AWARD("awardId"), ORDER_NUMBER(
                "orderNumber"), ALLIANCE_SENT_DATE("allianceSent");

        private String name;

        private EventEarningCell(String name)
        {
            this.name = name;
        }

        @Override
        public String getName()
        {
            return name;
        }
    }

    @Override
    public void verify(EarningEvent event)
    {
        verifyThat(getCell(EventEarningCell.TYPE), hasText(event.getType()));
        verifyThat(getCell(EventEarningCell.AMOUNT), hasText(asFormattedInt(event.getAmount())));
        verifyThat(getCell(EventEarningCell.QUALIFY_AMOUNT), hasText(asFormattedInt(event.getQualifyAmount())));
        verifyThat(getCell(EventEarningCell.UNIT_TYPE), hasText(event.getUnitType()));
        verifyThat(getCell(EventEarningCell.DATE_EARNED), hasText(event.getDateEarned()));
        verifyThat(getCell(EventEarningCell.ALLIANCE_TRANSACTION_CODE), hasText(event.getAllianceTransactionCode()));
        verifyThat(getCell(EventEarningCell.STATUS), hasText(event.getStatus()));
        verifyThat(getCell(EventEarningCell.AWARD), hasText(event.getAward()));
        verifyThat(getCell(EventEarningCell.ORDER_NUMBER), hasText(event.getOrderNumber()));
        verifyThat(getCell(EventEarningCell.ALLIANCE_SENT_DATE), hasText(event.getAllianceSentDate()));
    }
}
