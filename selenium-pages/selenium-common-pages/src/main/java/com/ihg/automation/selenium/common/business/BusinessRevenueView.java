package com.ihg.automation.selenium.common.business;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasAnyText;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasTextAsDouble;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static com.ihg.automation.selenium.matchers.component.IsDisplayed.displayed;

import com.ihg.automation.selenium.common.Constant;
import com.ihg.automation.selenium.common.Revenue;
import com.ihg.automation.selenium.gwt.components.Component;
import com.ihg.automation.selenium.gwt.components.ComponentColumnPosition;
import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.utils.ByText;

public class BusinessRevenueView extends Revenue
{
    public BusinessRevenueView(Container container, String labelText)
    {
        super(container, labelText);
    }

    public Component getEstimatedPointEarning()
    {
        return label.getComponent("Estimated Point Earning", 3, getPosition());
    }

    public Component getEstimatedCost()
    {
        return label.getComponent(ByText.startsWith("Estimated Cost to Hotel").getText(), 4, getPosition());
    }

    public void verify(String estimatedPointEarning, String estimatedCost)
    {
        verifyThat(getLocalAmount(), displayed(true));
        verifyThat(getUSDAmount(), displayed(true));
        verifyThat(getEstimatedPointEarning(), hasText(estimatedPointEarning));
        verifyThat(getEstimatedCost(), hasText(estimatedCost));
    }

    public void verifyIsPrePopulated()
    {
        verifyThat(getLocalAmount(), hasAnyText());
        verifyThat(getUSDAmount(), hasAnyText());
        verifyThat(getEstimatedPointEarning(), hasAnyText());
        verifyThat(getEstimatedCost(), hasAnyText());
    }

    public void verifyLocalAmount(BusinessRevenueBean amount)
    {
        verifyThat(getLocalAmount(), hasTextAsDouble(amount.getAmount()));
    }

    public void verifyUSDAmount(BusinessRevenueBean amount)
    {
        verifyThat(getUSDAmount(), hasTextAsDouble(amount.getUsdAmount()));
    }

    public void verifyDefault()
    {
        verify(Constant.NOT_AVAILABLE, Constant.NOT_AVAILABLE);
    }

    @Override
    protected ComponentColumnPosition getPosition()
    {
        return ComponentColumnPosition.TABLE;
    }
}
