package com.ihg.automation.selenium.common.event;

import com.ihg.automation.selenium.gwt.components.Component;

public interface CurrencyAmountFieldsAware
{
    public Component getAmount();

    public Component getCurrency();
}
