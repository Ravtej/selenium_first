package com.ihg.automation.selenium.common.contact;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.ViewableMatcher.hasText;
import static com.ihg.automation.selenium.matchers.text.EntityMatcherFactory.isValue;

import com.ihg.automation.selenium.common.member.GuestEmail;
import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.Mode;
import com.ihg.automation.selenium.gwt.components.input.Input;
import com.ihg.automation.selenium.gwt.components.input.Select;
import com.ihg.automation.selenium.gwt.components.panel.EditablePanel;

public class Email extends ContactBase<GuestEmail>
{
    public Email(Container container, int index)
    {
        super(new EditablePanel(container.getSeparator("E-mail"), index));
    }

    public Email(Container container)
    {
        super(container);
    }

    public Input getEmail()
    {
        return container.getInputByLabel("Email");
    }

    public Select getFormat()
    {
        return container.getSelectByLabel("Format");
    }

    @Override
    public void populate(GuestEmail email)
    {
        super.populate(email);

        getFormat().selectByValue(email.getFormat());
        getEmail().type(email.getEmail());
    }

    private void verifyEmail(GuestEmail email, Mode mode)
    {
        verifyThat(getType(), hasText(isValue(email.getType()), mode));
        verifyThat(getFormat(), hasText(isValue(email.getFormat()), mode));
    }

    public void verifyLcEmail(GuestEmail email, Mode mode)
    {
        verifyEmail(email, mode);
        verifyThat(getEmail(), hasText(email.getEmailDomain(), mode));
    }

    public void verifyScEmail(GuestEmail email, Mode mode)
    {
        verifyEmail(email, mode);
        verifyThat(getEmail(), hasText(email.getEmail(), mode));
    }
}
