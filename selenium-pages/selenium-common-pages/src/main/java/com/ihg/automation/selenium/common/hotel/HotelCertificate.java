package com.ihg.automation.selenium.common.hotel;

import com.ihg.automation.selenium.common.types.Currency;

public class HotelCertificate
{
    public static final String INVOICE = "Invoice";

    private String hotelCode;
    private String checkInDate;
    private String checkOutDate;

    private String certificateNumber;
    private String confirmationNumber;
    private String folioNumber;
    private String bundleNumber;

    private String descriptionGuestName;

    private String itemId;
    private String itemName;

    private String billingEntity;
    private String amount;
    private Currency currency;

    private String reimbursementAmount;
    private String reimbursementMethod;
    private String reimbursementDate;

    private String status;

    public String getHotelCode()
    {
        return hotelCode;
    }

    public void setHotelCode(String hotelCode)
    {
        this.hotelCode = hotelCode;
    }

    public String getCheckInDate()
    {
        return checkInDate;
    }

    public void setCheckInDate(String checkInDate)
    {
        this.checkInDate = checkInDate;
    }

    public String getCheckOutDate()
    {
        return checkOutDate;
    }

    public void setCheckOutDate(String checkOutDate)
    {
        this.checkOutDate = checkOutDate;
    }

    public String getCertificateNumber()
    {
        return certificateNumber;
    }

    public void setCertificateNumber(String certificateNumber)
    {
        this.certificateNumber = certificateNumber;
    }

    public String getConfirmationNumber()
    {
        return confirmationNumber;
    }

    public void setConfirmationNumber(String confirmationNumber)
    {
        this.confirmationNumber = confirmationNumber;
    }

    public String getFolioNumber()
    {
        return folioNumber;
    }

    public void setFolioNumber(String folioNumber)
    {
        this.folioNumber = folioNumber;
    }

    public String getBundleNumber()
    {
        return bundleNumber;
    }

    public void setBundleNumber(String bundleNumber)
    {
        this.bundleNumber = bundleNumber;
    }

    public String getDescriptionGuestName()
    {
        return descriptionGuestName;
    }

    public void setDescriptionGuestName(String descriptionGuestName)
    {
        this.descriptionGuestName = descriptionGuestName;
    }

    public String getItemId()
    {
        return itemId;
    }

    public void setItemId(String itemId)
    {
        this.itemId = itemId;
    }

    public String getItemName()
    {
        return itemName;
    }

    public void setItemName(String itemName)
    {
        this.itemName = itemName;
    }

    public String getBillingEntity()
    {
        return billingEntity;
    }

    public void setBillingEntity(String billingEntity)
    {
        this.billingEntity = billingEntity;
    }

    public String getAmount()
    {
        return amount;
    }

    public void setAmount(String amount)
    {
        this.amount = amount;
    }

    public Currency getCurrency()
    {
        return currency;
    }

    public void setCurrency(Currency currency)
    {
        this.currency = currency;
    }

    public String getReimbursementAmount()
    {
        return reimbursementAmount;
    }

    public void setReimbursementAmount(String reimbursementAmount)
    {
        this.reimbursementAmount = reimbursementAmount;
    }

    public String getReimbursementMethod()
    {
        return reimbursementMethod;
    }

    public void setReimbursementMethod(String reimbursementMethod)
    {
        this.reimbursementMethod = reimbursementMethod;
    }

    public String getReimbursementDate()
    {
        return reimbursementDate;
    }

    public void setReimbursementDate(String reimbursementDate)
    {
        this.reimbursementDate = reimbursementDate;
    }

    public String getStatus()
    {
        return status;
    }

    public void setStatus(String status)
    {
        this.status = status;
    }

    public HotelCertificate clone()
    {
        HotelCertificate clone = new HotelCertificate();

        clone.hotelCode = hotelCode;
        clone.checkInDate = checkInDate;
        clone.checkOutDate = checkOutDate;

        clone.certificateNumber = certificateNumber;
        clone.confirmationNumber = confirmationNumber;
        clone.folioNumber = folioNumber;
        clone.bundleNumber = bundleNumber;

        clone.descriptionGuestName = descriptionGuestName;

        clone.itemId = itemId;
        clone.itemName = itemName;

        clone.billingEntity = billingEntity;
        clone.amount = amount;
        clone.currency = currency;

        clone.reimbursementAmount = reimbursementAmount;
        clone.reimbursementMethod = reimbursementMethod;
        clone.reimbursementDate = reimbursementDate;

        clone.status = status;

        return clone;
    }

}
