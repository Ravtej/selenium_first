package com.ihg.automation.selenium.common.types.program;

public enum MeetingLevel implements ProgramLevel
{
    MP1("MP1", "MEETING REWARDS");

    private String code;
    private String value;

    private MeetingLevel(String code, String description)
    {
        this.code = code;
        this.value = description;
    }

    @Override
    public String getCode()
    {
        return code;
    }

    @Override
    public String getValue()
    {
        return value;
    }

    @Override
    public String getCodeWithValue()
    {
        return code + " - " + value;
    }
}
