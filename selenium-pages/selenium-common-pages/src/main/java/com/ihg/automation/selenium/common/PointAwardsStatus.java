package com.ihg.automation.selenium.common;

public class PointAwardsStatus
{
    public static final String PROCESSING = "Processing";
    public static final String PENDING = "Pending";
    public static final String REJECTED = "Rejected";
    public static final String DECLINED = "Declined";
    public static final String DISQUALIFIED = "Disqualified";
    public static final String INITIATED = "Initiated";

    private PointAwardsStatus()
    {
    }
}
