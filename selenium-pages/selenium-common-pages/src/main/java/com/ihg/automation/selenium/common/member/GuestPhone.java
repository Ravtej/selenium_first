package com.ihg.automation.selenium.common.member;

import com.ihg.automation.selenium.common.types.PhoneSubType;

public class GuestPhone extends GuestContact
{
    private String number;
    private PhoneSubType phoneType;

    public String getNumber()
    {
        return number;
    }

    public void setNumber(String number)
    {
        this.number = number;
    }

    public String getFullPhone()
    {
        return getNumber();
    }

    public PhoneSubType getPhoneSubType()
    {
        return phoneType;
    }

    public void setPhoneSubType(PhoneSubType phoneType)
    {
        this.phoneType = phoneType;
    }

    public String getAuditPhoneType()
    {
        return getType().getValue() + " " + getPhoneSubType().getValue();
    }
}
