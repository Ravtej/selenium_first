package com.ihg.automation.selenium.common.member;

import static com.ihg.automation.common.CustomStringUtils.concat;
import static com.ihg.automation.common.CustomStringUtils.format;

public class MeetingPhone
{
    private String countryCode;
    private String cityCode;
    private String number;
    private String extension;

    public String getCountryCode()
    {
        return countryCode;
    }

    public void setCountryCode(String countryCode)
    {
        this.countryCode = countryCode;
    }

    public String getCityCode()
    {
        return cityCode;
    }

    public void setCityCode(String cityCode)
    {
        this.cityCode = cityCode;
    }

    public String getNumber()
    {
        return number;
    }

    public void setNumber(String number)
    {
        this.number = number;
    }

    public String getExtension()
    {
        return extension;
    }

    public void setExtension(String extension)
    {
        this.extension = extension;
    }

    public String getFullPhone()
    {
        return concat(" ", format(countryCode, "+{0}"), format(cityCode, "({0})"), getNumber(),
                format(extension, "ext.{0}"));
    }

}
