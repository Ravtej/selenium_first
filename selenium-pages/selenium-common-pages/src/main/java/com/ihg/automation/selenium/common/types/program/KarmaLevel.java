package com.ihg.automation.selenium.common.types.program;

public enum KarmaLevel implements ProgramLevel
{
    KAR("KAR", "Karma"), KIC("KIC", "Inner Circle");

    private String code;
    private String value;

    private KarmaLevel(String code, String description)
    {
        this.code = code;
        this.value = description;
    }

    @Override
    public String getCode()
    {
        return code;
    }

    @Override
    public String getValue()
    {
        return value;
    }

    @Override
    public String getCodeWithValue()
    {
        return code + " - " + value;
    }
}
