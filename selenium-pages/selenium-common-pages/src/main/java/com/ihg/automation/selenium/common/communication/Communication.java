package com.ihg.automation.selenium.common.communication;

import java.util.ArrayList;

public enum Communication
{
    E_STATEMENT("eStatement"), //
    MEMBER_SURVEYS("Member Surveys"), //
    PARTNER_OFFERS_AND_PROMOTIONS("Partner Offers and Promotions"), //
    IHG_REWARDS_CLUB_SPECIAL_OFFERS_AND_PROMOTIONS("IHG Rewards Club Special Offers and Promotions"), //
    IHG_REWARDS_CLUB_CREDIT_CARD_OFFERS_AND_PROMOTIONS("IHG Rewards Club Credit Card Offers and Promotions"), //
    HEARTBEAT("Heartbeat");

    private String value;

    private Communication(String value)
    {
        this.value = value;
    }

    public String getValue()
    {
        return value;
    }

    public static ArrayList<String> getValueList()
    {
        Communication[] vals = values();
        ArrayList<String> list = new ArrayList<String>();
        for (Communication val : vals)
        {
            list.add(val.getValue());
        }
        return list;
    }
}
