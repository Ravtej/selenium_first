package com.ihg.automation.selenium.common.hotel;

import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.CustomContainerBase;
import com.ihg.automation.selenium.gwt.components.input.Input;

public class AdvancedHotelSearch extends CustomContainerBase
{
    public AdvancedHotelSearch(Container container)
    {
        super(container);
    }

    public Input getHotel()
    {
        return container.getInputByLabel("Hotel");
    }

    public Input getHotelName()
    {
        return container.getInputByLabel("Hotel Name");
    }

    public Input getLocationNumber()
    {
        return container.getInputByLabel("Location Number");
    }

    public Input getFacilityId()
    {
        return container.getInputByLabel("Facility ID");
    }

    public HotelAddress getHotelAddress()
    {
        return new HotelAddress(container);
    }

    public HotelBrandInfo getHotelBrandInfo()
    {
        return new HotelBrandInfo(container);
    }
}
