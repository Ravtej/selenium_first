package com.ihg.automation.selenium.common.freenight;

import com.ihg.automation.common.DateUtils;

public class VoucherRedemption
{
    private String earningDate = DateUtils.getFormattedDate();
    private String voucherNumber;
    private String startBookDates;
    private String endBookDates;
    private String status;
    private String redeemDate = "";
    private String confirmation = "";
    private String hotel = "";

    public String getEarningDate()
    {
        return earningDate;
    }

    public void setEarningDate(String earningDate)
    {
        this.earningDate = earningDate;
    }

    public String getVoucherNumber()
    {
        return voucherNumber;
    }

    public void setVoucherNumber(String voucherNumber)
    {
        this.voucherNumber = voucherNumber;
    }

    public String getStatus()
    {
        return status;
    }

    public void setStatus(String status)
    {
        this.status = status;
    }

    public String getRedeemDate()
    {
        return redeemDate;
    }

    public void setRedeemDate(String redeemDate)
    {
        this.redeemDate = redeemDate;
    }

    public String getConfirmation()
    {
        return confirmation;
    }

    public void setConfirmation(String confirmation)
    {
        this.confirmation = confirmation;
    }

    public String getHotel()
    {
        return hotel;
    }

    public void setHotel(String hotel)
    {
        this.hotel = hotel;
    }

    public String getStartBookDates()
    {
        return startBookDates;
    }

    public void setStartBookDates(String startBookDates)
    {
        this.startBookDates = startBookDates;
    }

    public String getEndBookDates()
    {
        return endBookDates;
    }

    public void setEndBookDates(String endBookDates)
    {
        this.endBookDates = endBookDates;
    }
}
