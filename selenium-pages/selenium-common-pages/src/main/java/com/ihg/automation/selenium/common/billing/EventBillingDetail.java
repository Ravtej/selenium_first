package com.ihg.automation.selenium.common.billing;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.gwt.components.utils.FindStepUtils.byXpath;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;

import org.joda.time.LocalDate;

import com.ihg.automation.common.DateUtils;
import com.ihg.automation.selenium.common.Constant;
import com.ihg.automation.selenium.gwt.components.Component;
import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.CustomContainerBase;

public class EventBillingDetail extends CustomContainerBase
{

    public static final String SEPARATOR = "Separator";

    public EventBillingDetail(Container parent, int position)
    {
        super(parent, "Event Billing", "", byXpath(String.format("./div[%d]", position)));
    }

    public Component getBillingDate()
    {
        return new Component(container, SEPARATOR, "Billing Date", byXpath("./div[1]/div/div[1]"));
    }

    public Component getEventType()
    {
        return new Component(container, SEPARATOR, "Billing Date", byXpath("./div[1]/div/div[2]"));
    }

    public EventBillingAmount getBaseLoyaltyUnits()
    {
        return new EventBillingAmount(container.getLabel("Cost of Base Loyalty Units"));
    }

    public EventBillingAmount getBaseAwardLoyaltyUnits()
    {
        return new EventBillingAmount(container.getLabel("Cost of Base Award Loyalty Units"));
    }

    public void verifyBillingIsEmpty(String eventType)
    {
        verifyThat(this, hasText(getSeparatorText(eventType)));
    }

    public void verifySeparator(String eventType, String billingDate)
    {
        verifyThat(getBillingDate(), hasText(billingDate));
        verifyThat(getEventType(), hasText(eventType));
    }

    final static String getSeparatorText(LocalDate eventDate, String eventType)
    {
        return eventDate.toString(DateUtils.MMMM_d_comma_YYYY) + Constant.LINE_SEPARATOR + eventType;
    }

    final static String getSeparatorText(String eventType)
    {
        return getSeparatorText(DateUtils.now(), eventType);
    }

}
