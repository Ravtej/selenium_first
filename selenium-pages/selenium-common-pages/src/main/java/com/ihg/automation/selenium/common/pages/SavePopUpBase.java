package com.ihg.automation.selenium.common.pages;

import com.ihg.automation.selenium.gwt.components.Button;
import com.ihg.automation.selenium.gwt.components.PopUpWindow;
import com.ihg.automation.selenium.listener.Verifier;

public abstract class SavePopUpBase extends ClosablePopUpBase
{
    public SavePopUpBase()
    {
    }

    public SavePopUpBase(String header)
    {
        super(header);
    }

    public SavePopUpBase(PopUpWindow popUp)
    {
        super(popUp);
    }

    public Button getSave()
    {
        return container.getButton("Save");
    }

    public void clickSave(Verifier... verifiers)
    {
        getSave().clickAndWait(verifiers);
    }
}
