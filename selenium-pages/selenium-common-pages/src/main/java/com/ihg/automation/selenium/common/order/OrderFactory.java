package com.ihg.automation.selenium.common.order;

import static com.ihg.automation.selenium.common.event.EventTransactionType.Order.ORDER_SYSTEM;
import static com.ihg.automation.selenium.common.event.EventTransactionType.Order.REDEEM;
import static org.apache.commons.lang3.StringUtils.EMPTY;

import com.ihg.automation.selenium.common.SiteHelperBase;
import com.ihg.automation.selenium.common.catalog.CatalogItem;
import com.ihg.automation.selenium.common.member.Member;

public class OrderFactory
{
    private static final String PROCESSING = "PROCESSING";

    public static final Order getSystemOrder(CatalogItem item, SiteHelperBase siteHelper)
    {
        Order order = new Order();

        OrderItem orderItem = new OrderItem(item.getItemId());

        // For SYSTEM - ORDER 'item name' is 'award description'
        orderItem.setItemName(item.getItemDescription());
        orderItem.setDeliveryStatus(PROCESSING);
        order.getOrderItems().add(orderItem);
        if (siteHelper != null)
        {
            order.setSource(siteHelper.getSourceFactory().getOrderSource());
        }
        order.setTransactionType(ORDER_SYSTEM);

        return order;
    }

    public static final Order getSystemOrder(CatalogItem item, Member member, SiteHelperBase siteHelper)
    {
        Order order = getSystemOrder(item, siteHelper);
        order.setShippingInfo(member);

        return order;
    }

    public static final Order getRedeemOrder(CatalogItem item, SiteHelperBase siteHelper)
    {
        Order order = new Order();

        OrderItem orderItem = new OrderItem(item);
        orderItem.setItemRedeemName(item.getItemName());
        orderItem.setDeliveryStatus(PROCESSING);
        orderItem.setQuantity("1");
        order.getOrderItems().add(orderItem);
        order.setTotalLoyaltyUnits(item.getLoyaltyUnitCost());
        order.setTotalAllianceUnits(EMPTY);
        if (siteHelper != null)
        {
            order.setSource(siteHelper.getSourceFactory().getOrderSource());
        }
        order.setTransactionType(REDEEM);

        return order;
    }

    public static final Order getRedeemOrder(CatalogItem item, Member member, SiteHelperBase siteHelper)
    {
        Order order = getRedeemOrder(item, siteHelper);
        order.setShippingInfo(member);

        return order;
    }

}
