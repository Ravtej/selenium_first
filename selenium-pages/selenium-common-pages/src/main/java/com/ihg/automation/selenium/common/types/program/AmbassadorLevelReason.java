package com.ihg.automation.selenium.common.types.program;

import java.util.ArrayList;

public enum AmbassadorLevelReason implements ProgramLevelReason
{
    ROYAL_AMBASSADOR_CERTIFICATE("R", "Royal Ambassador Certificate"), //
    VIP("V", "VIP"), //
    CUSTOMER_SERVICE_ISSUE("I", "Customer Service Issue"), //
    SYSTEM_ERROR("E", "System Error"), //
    CO_BRANDED_CARD_HOLDER("H", "Co-Branded Card Holder");

    private String code;
    private String value;

    private AmbassadorLevelReason(String code, String description)
    {
        this.code = code;
        this.value = description;
    }

    @Override
    public String getCode()
    {
        return code;
    }

    @Override
    public String getValue()
    {
        return value;
    }

    @Override
    public String getCodeWithValue()
    {
        return code + " - " + value;
    }

    public String toString()
    {
        return getValue();
    }

    public static ArrayList<String> getValuesList()
    {
        AmbassadorLevelReason[] vals = values();
        ArrayList<String> res = new ArrayList<String>();
        for (AmbassadorLevelReason val : vals)
        {
            res.add(val.getValue());
        }

        return res;
    }

}
