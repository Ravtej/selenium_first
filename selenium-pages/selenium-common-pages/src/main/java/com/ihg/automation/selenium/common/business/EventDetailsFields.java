package com.ihg.automation.selenium.common.business;

import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.CustomContainerBase;

public class EventDetailsFields extends CustomContainerBase
{
    public EventDetailsFields(Container container)
    {
        super(container);
    }

    public MeetingFields getMeetingFields()
    {
        return new MeetingFields(container);
    }

    public GuestRoomsFields getGuestRoomsFields()
    {
        return new GuestRoomsFields(container);
    }
}
