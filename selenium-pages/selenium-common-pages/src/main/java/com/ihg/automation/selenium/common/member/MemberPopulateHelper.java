package com.ihg.automation.selenium.common.member;

import static com.ihg.automation.common.RandomUtils.getRandomNumber;

import com.ihg.automation.common.RandomUtils;
import com.ihg.automation.selenium.common.components.Country;
import com.ihg.automation.selenium.common.types.EmailType;
import com.ihg.automation.selenium.common.types.PhoneSubType;
import com.ihg.automation.selenium.common.types.PhoneType;
import com.ihg.automation.selenium.common.types.address.AddressType;
import com.ihg.automation.selenium.common.types.address.Region;

public class MemberPopulateHelper
{
    public static GuestName getRandomName()
    {
        GuestName name = new GuestName();

        String namePrefix = RandomUtils.getRandomLetters(25);

        name.setGiven(namePrefix + "Given");
        name.setSurname(namePrefix + "Surname");

        return name;
    }

    public static PersonalInfo getSimplePersonalInfo()
    {
        PersonalInfo personalInfo = new PersonalInfo();
        personalInfo.setName(getRandomName());
        return personalInfo;
    }

    public static PersonalInfo getSimplePersonalInfo(Country country)
    {
        PersonalInfo personalInfo = new PersonalInfo();

        personalInfo.setName(getRandomName());
        personalInfo.setResidenceCountry(country);
        return personalInfo;
    }

    public static GuestAddress getUnitedStatesAddress()
    {
        GuestAddress address = new GuestAddress();

        address.setCountryCode(Country.US);
        address.setType(AddressType.RESIDENCE);
        address.setLine1("1 1ST AVE");
        address.setLocality1("NEW YORK");
        address.setRegion1(Region.NY);
        address.setPostalCode("10003-9401");

        return address;
    }

    public static GuestAddress getUsBusinessAddress()
    {
        GuestAddress address = new GuestAddress();

        address.setCountryCode(Country.US);
        address.setType(AddressType.BUSINESS);
        address.setLine1("2 E 2ND ST");
        address.setLocality1("NEW YORK");
        address.setRegion1(Region.NY);
        address.setPostalCode("10003-8962");

        return address;
    }

    public static GuestAddress getUnitedStatesSecondAddress()
    {
        GuestAddress secondAddress = new GuestAddress();
        secondAddress.setCountryCode(Country.US);
        secondAddress.setType(AddressType.BUSINESS);
        secondAddress.setLine1("1 MADISON AVE");
        secondAddress.setLocality1("NEW YORK");
        secondAddress.setRegion1(Region.NY);
        secondAddress.setPostalCode("10010-3603");

        return secondAddress;
    }

    public static GuestAddress getIndonesiaAddress()
    {
        GuestAddress secondAddress = new GuestAddress();
        secondAddress.setCountryCode(Country.ID);
        secondAddress.setType(AddressType.BUSINESS);
        secondAddress.setLine1("Jakarta Selatan");
        secondAddress.setLocality1("KUNINGAN");
        secondAddress.setPostalCode("12950");

        return secondAddress;
    }

    public static GuestAddress getCanadaAddress()
    {
        GuestAddress secondAddress = new GuestAddress();
        secondAddress.setCountryCode(Country.CA);
        secondAddress.setType(AddressType.BUSINESS);
        secondAddress.setLine1("234 Laurier Avenue");
        secondAddress.setLocality1("Ottawa");
        secondAddress.setRegion2("ON");
        secondAddress.setPostalCode("K1A 0G9");

        return secondAddress;
    }

    public static GuestAddress getBelarusAddress()
    {
        GuestAddress address = new GuestAddress();
        address.setCountryCode(Country.BY);
        address.setType(AddressType.BUSINESS);
        address.setLine1("14 Nemiga str");
        address.setLocality1("Minsk");
        address.setRegion2("MI");
        address.setPostalCode("220004");

        return address;
    }

    public static GuestAddress getBelarusSecondAddress()
    {
        GuestAddress address = new GuestAddress();
        address.setCountryCode(Country.BY);
        address.setType(AddressType.BUSINESS);
        address.setLine1("46, Chervyakova str");
        address.setLocality1("Minsk");
        address.setRegion2("MI");
        address.setPostalCode("220053 ");

        return address;
    }

    public static GuestAddress getFrenchAddress()
    {
        GuestAddress address = new GuestAddress();
        address.setCountryCode(Country.FR);
        address.setType(AddressType.BUSINESS);
        address.setLine1("40-50 BD LANNES");
        address.setLocality1("Paris");
        address.setPostalCode("75116 ");

        return address;
    }

    public static GuestAddress getChinaAddress()
    {
        GuestAddress address = new GuestAddress();

        address.setCountryCode(Country.CN);
        address.setLocality1("Hong Kong");
        address.setLine1("150 Kennedy Road");
        address.setType(AddressType.RESIDENCE);

        return address;
    }

    public static GuestAddress getBrazilAddress()
    {
        GuestAddress address = new GuestAddress();

        address.setCountryCode(Country.BR);
        address.setLocality1("SALVADOR-BA");
        address.setLine1("Boulevard das Flores 255");
        address.setType(AddressType.RESIDENCE);

        return address;
    }

    public static GuestAddress getAustraliaAddress()
    {
        GuestAddress address = new GuestAddress();

        address.setCountryCode(Country.AU);
        address.setLocality1("MELBOURNE");
        address.setLine1("14 QUEENS RD");
        address.setPostalCode("3005");
        address.setType(AddressType.RESIDENCE);

        return address;
    }

    public static GuestEmail getRandomEmail()
    {
        GuestEmail email = new GuestEmail();
        email.setType(EmailType.PERSONAL);
        email.setEmail("ui-automation-email-" + RandomUtils.getRandomLetters(2) + "-" + RandomUtils.getTimeStamp()
                + "@yahoo.com");

        return email;
    }

    public static GuestPhone getPhone()
    {
        GuestPhone guestPhone = new GuestPhone();

        guestPhone.setType(PhoneType.BUSINESS);
        guestPhone.setPhoneSubType(PhoneSubType.PHONE);

        guestPhone.setNumber("+375-17-1234567-123");

        return guestPhone;
    }

    public static GuestPhone getRandomPhone()
    {
        GuestPhone guestPhone = new GuestPhone();

        guestPhone.setType(PhoneType.RESIDENTIAL);
        guestPhone.setPhoneSubType(PhoneSubType.PHONE);
        guestPhone.setNumber(getRandomNumber(7));

        return guestPhone;
    }

    public static MeetingPhone getMeetingPhone()
    {
        MeetingPhone meetingPhone = new MeetingPhone();

        meetingPhone.setCountryCode("375");
        meetingPhone.setCityCode("17");
        meetingPhone.setExtension(getRandomNumber(3));
        meetingPhone.setNumber(getRandomNumber(7));

        return meetingPhone;
    }

    public static GuestAddress getRandomAddress()
    {
        GuestAddress randomAddress = new GuestAddress();
        randomAddress.setCountryCode(Country.US);
        randomAddress.setType(AddressType.RESIDENCE);
        randomAddress.setLine1(getRandomNumber(3, false) + " MADISON AVE");
        randomAddress.setLocality1("NEW YORK");
        randomAddress.setRegion1(Region.NY);
        randomAddress.setPostalCode("10010");

        return randomAddress;
    }

}
