package com.ihg.automation.selenium.common.types;

import com.ihg.automation.selenium.gwt.components.EntityType;

public enum GoodwillReason implements EntityType
{
    PROGRAM_DISSATISFIED("A", "PROGRAM/DISSATISFIED"), //
    PROMOTION_CONCERN("B", "PROMOTION CONCERN"), //
    AGENT_ERROR("I", "AGENT ERROR");

    private GoodwillReason(String code, String value)
    {
        this.value = value;
        this.code = code;
    }

    private String code;
    private String value;

    @Override
    public String getCode()
    {
        return code;
    }

    @Override
    public String getValue()
    {
        return value;
    }

    @Override
    public String getCodeWithValue()
    {
        return code + " - " + value;
    }
}
