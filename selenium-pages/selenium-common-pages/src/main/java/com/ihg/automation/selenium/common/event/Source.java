package com.ihg.automation.selenium.common.event;

public class Source implements Cloneable
{
    private String channel;
    private String location;
    private String sourceType;
    private String employeeID;
    private String userID;

    public Source(String channel, String location, String sourceType, String employeeID, String userID)
    {
        this.channel = channel;
        this.location = location;
        this.sourceType = sourceType;
        this.employeeID = employeeID;
        this.userID = userID;
    }

    public void setChannel(String channel)
    {
        this.channel = channel;
    }

    public String getChannel()
    {
        return channel;
    }

    public void setSourceType(String sourceType)
    {
        this.sourceType = sourceType;
    }

    public String getSourceType()
    {
        return sourceType;
    }

    public void setUserID(String userID)
    {
        this.userID = userID;
    }

    public String getUserID()
    {
        return userID;
    }

    public void setLocation(String location)
    {
        this.location = location;
    }

    public String getLocation()
    {
        return location;
    }

    public void setEmployeeID(String employeeID)
    {
        this.employeeID = employeeID;
    }

    public String getEmployeeID()
    {
        return employeeID;
    }

    @Override
    public Source clone()
    {
        return new Source(channel, location, sourceType, employeeID, userID);
    }
}
