package com.ihg.automation.selenium.common.freenight;

public class GoodwillReason
{
    public static final String AGENT_ERROR = "Agent Error";
    public static final String COMMUNICATION_ERROR = "Communication Error";
    public static final String EXCEPTION = "Exception";
    public static final String HOTEL_ISSUE = "Hotel Issue";
    public static final String OTHER = "Other";
    public static final String SYSTEM_ERROR = "System Error/Issues";

    private GoodwillReason()
    {
    }
}
