package com.ihg.automation.selenium.common.enroll;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.assertThat;
import static com.ihg.automation.selenium.matchers.component.IsDisplayed.displayed;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.pages.PopUpBase;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.gwt.components.Button;
import com.ihg.automation.selenium.gwt.components.label.Label;
import com.ihg.automation.selenium.listener.Verifier;

public class SuccessEnrolmentPopUp extends PopUpBase
{
    private static final Logger logger = LoggerFactory.getLogger(SuccessEnrolmentPopUp.class);

    public SuccessEnrolmentPopUp()
    {
        super("Enrollment Successful");
    }

    public void clickDone(Verifier... verifiers)
    {
        container.getButton(Button.DONE).clickAndWait(verifiers);
    }

    public Button getEnrollAnother()
    {
        return container.getButton(Button.ENROLL_ANOTHER);
    }

    public void clickEnrollAnother()
    {
        getEnrollAnother().clickAndWait();
    }

    public Label getMemberId(Program program)
    {
        return container.getLabel(program.getCode());
    }

    public Map<Program, String> getMemberships()
    {
        Label label;
        Map<Program, String> memberships = new HashMap<Program, String>();

        assertThat(this, displayed(true));

        for (Program program : Program.values())
        {
            label = getMemberId(program);

            if (label.isDisplayed())
            {
                String memberId = label.getText();
                logger.debug("Program: [{}], memberId: [{}]", program.getCode(), memberId);
                memberships.put(program, memberId);
            }
        }

        return memberships;
    }

    public Member putPrograms(Member member)
    {
        for (Map.Entry<Program, String> entry : getMemberships().entrySet())
        {
            member.getPrograms().put(entry.getKey(), entry.getValue());
        }

        return member;
    }
}
