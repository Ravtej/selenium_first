package com.ihg.automation.selenium.common.event;

import static com.ihg.automation.common.DateUtils.dd_MMM_YYYY;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.joda.time.LocalDate;

import com.ihg.automation.common.DateUtils;

public class EarningEvent
{
    private String type = "";
    private String amount = "";
    private String qualifyAmount = "";
    private String unitType = "";
    private String dateEarned = DateUtils.getFormattedDate();
    private String allianceTransactionCode = "";
    private String status = "";
    private String award = "";
    private String orderNumber = "";
    private String allianceSentDate = "";

    public EarningEvent(String type, String amount, String qualifyAmount, String unitType, String status)
    {
        this(type, amount, qualifyAmount, unitType);
        this.status = status;
    }

    public EarningEvent(String type, String amount, String qualifyAmount, String unitType, String status, String award)
    {
        this(type, amount, qualifyAmount, unitType, status);
        this.award = award;
    }

    public EarningEvent(String type, String amount, String qualifyAmount, String unitType)
    {
        this.type = type;
        this.amount = amount;
        this.qualifyAmount = qualifyAmount;
        this.unitType = unitType;
        this.dateEarned = DateUtils.getFormattedDate(DateUtils.dd_MMM_YYYY);
    }

    public EarningEvent(String type, String amount, String qualifyAmount, String unitType, LocalDate dateEarned)
    {
        this(type, amount, qualifyAmount, unitType);
        this.dateEarned = dateEarned.toString(dd_MMM_YYYY);
    }

    public void setType(String type)
    {
        this.type = type;
    }

    public String getType()
    {
        return type;
    }

    public void setAmount(String amount)
    {
        this.amount = amount;
    }

    public String getAmount()
    {
        return amount;
    }

    public void setUnitType(String unitType)
    {
        this.unitType = unitType;
    }

    public String getUnitType()
    {
        return unitType;
    }

    public void setAllianceTransactionCode(String allianceTransactionCode)
    {
        this.allianceTransactionCode = allianceTransactionCode;
    }

    public String getAllianceTransactionCode()
    {
        return allianceTransactionCode;
    }

    public void setDateEarned(String dateEarned)
    {
        this.dateEarned = dateEarned;
    }

    public void setDateEarned(LocalDate dateEarned)
    {
        this.dateEarned = dateEarned.toString(DateUtils.dd_MMM_YYYY);
    }

    public String getDateEarned()
    {
        return dateEarned;
    }

    public void setStatus(String status)
    {
        this.status = status;
    }

    public String getStatus()
    {
        return status;
    }

    public void setAward(String award)
    {
        this.award = award;
    }

    public String getAward()
    {
        return award;
    }

    public void setOrderNumber(String orderNumber)
    {
        this.orderNumber = orderNumber;
    }

    public String getOrderNumber()
    {
        return orderNumber;
    }

    public void setAllianceSentDate(String allianceSentDate)
    {
        this.allianceSentDate = allianceSentDate;
    }

    public String getAllianceSentDate()
    {
        return allianceSentDate;
    }

    public String getQualifyAmount()
    {
        return qualifyAmount;
    }

    public void setQualifyAmount(String qualifyAmount)
    {
        this.qualifyAmount = qualifyAmount;
    }

    @Override
    public String toString()
    {
        return new ToStringBuilder(this).append("amount", amount).append("allianceSentDate", allianceSentDate)
                .append("allianceTransactionCode", allianceTransactionCode).append("award", award)
                .append("dateEarned", dateEarned).append("orderNumber", orderNumber)
                .append("qualifyAmount", qualifyAmount).append("status", status).append("type", type)
                .append("unitType", unitType).toString();
    }
}
