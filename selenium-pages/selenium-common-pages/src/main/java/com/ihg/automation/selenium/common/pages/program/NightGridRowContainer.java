package com.ihg.automation.selenium.common.pages.program;

import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.CustomContainerBase;
import com.ihg.automation.selenium.gwt.components.label.Label;
import com.ihg.automation.selenium.gwt.components.utils.ByText;

public class NightGridRowContainer extends CustomContainerBase
{
    public NightGridRowContainer(Container parent)
    {
        super(parent);
    }

    public Label getRewardNights()
    {
        return container.getLabel(ByText.startsWith("Reward Nights"));
    }

    public Label getQualifyingNights()
    {
        return container.getLabel("Qualifying Nights");
    }

    public Label getTotalQualifyingNights()
    {
        return container.getLabel("Total Tier-Level Qualifying Nights");
    }
}
