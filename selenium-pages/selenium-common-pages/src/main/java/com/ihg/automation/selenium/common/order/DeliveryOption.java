package com.ihg.automation.selenium.common.order;

public final class DeliveryOption
{
    public static final String PAPER = "Paper Fulfillment";
    public static final String EMAIL = "E-Fulfillment";
}
