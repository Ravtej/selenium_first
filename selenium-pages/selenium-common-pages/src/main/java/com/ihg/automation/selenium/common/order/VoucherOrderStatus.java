package com.ihg.automation.selenium.common.order;

public enum VoucherOrderStatus
{
    PROCESSING("PROCESSING"), AT_VENDOR("AT VENDOR"), CANCELED("CANCELED");

    private String name;

    private VoucherOrderStatus(String name)
    {
        this.name = name;
    }

    public String getName()
    {
        return name;
    }
}
