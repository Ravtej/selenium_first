package com.ihg.automation.selenium.common.business;

import static com.ihg.automation.selenium.common.event.EventTransactionType.BusinessRewards.BUSINESS_REWARDS;
import static org.apache.commons.lang3.StringUtils.EMPTY;

import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.types.Currency;

public class BusinessEvent
{
    private Member member;
    private String transactionDate;
    private String transactionType = BUSINESS_REWARDS;
    private String eventId;
    private String eventName;
    private String hotel;
    private Currency hotelCurrency;
    private String status;
    private String ihgUnits = EMPTY;
    private String allianceUnits = EMPTY;
    private String daysLeftToSubmit = EMPTY;
    private MeetingDetails meetingDetails;
    private GuestRoom guestRoom;
    private DiscretionaryPoints discretionaryPoints;
    private BusinessOffer offer;

    public BusinessEvent(String eventName, String hotel, Currency hotelCurrency)
    {
        this.eventName = eventName;
        this.hotel = hotel;
        this.hotelCurrency = hotelCurrency;
    }

    public Member getMember()
    {
        return member;
    }

    public void setMember(Member member)
    {
        this.member = member;
    }

    public String getTransactionDate()
    {
        return transactionDate;
    }

    public void setTransactionDate(String transactionDate)
    {
        this.transactionDate = transactionDate;
    }

    public String getTransactionType()
    {
        return transactionType;
    }

    public void setTransactionType(String transactionType)
    {
        this.transactionType = transactionType;
    }

    public String getEventId()
    {
        return eventId;
    }

    public void setEventId(String eventId)
    {
        this.eventId = eventId;
    }

    public String getEventName()
    {
        return eventName;
    }

    public void setEventName(String eventName)
    {
        this.eventName = eventName;
    }

    public String getHotel()
    {
        return hotel;
    }

    public void setHotel(String hotel)
    {
        this.hotel = hotel;
    }

    public Currency getHotelCurrency()
    {
        return hotelCurrency;
    }

    public void setHotelCurrency(Currency hotelCurrency)
    {
        this.hotelCurrency = hotelCurrency;
    }

    public MeetingDetails getMeetingDetails()
    {
        return meetingDetails;
    }

    public void setMeetingDetails(MeetingDetails meetingDetails)
    {
        this.meetingDetails = meetingDetails;
    }

    public String getStatus()
    {
        return status;
    }

    public void setStatus(String status)
    {
        this.status = status;
    }

    public String getIhgUnits()
    {
        return ihgUnits;
    }

    public void setIhgUnits(String ihgUnits)
    {
        this.ihgUnits = ihgUnits;
    }

    public String getAllianceUnits()
    {
        return allianceUnits;
    }

    public void setAllianceUnits(String allianceUnits)
    {
        this.allianceUnits = allianceUnits;
    }

    public String getDaysLeftToSubmit()
    {
        return daysLeftToSubmit;
    }

    public void setDaysLeftToSubmit(String daysLeftToSubmit)
    {
        this.daysLeftToSubmit = daysLeftToSubmit;
    }

    public GuestRoom getGuestRoom()
    {
        return guestRoom;
    }

    public void setGuestRoom(GuestRoom guestRoom)
    {
        this.guestRoom = guestRoom;
    }

    public DiscretionaryPoints getDiscretionaryPoints()
    {
        return discretionaryPoints;
    }

    public void setDiscretionaryPoints(DiscretionaryPoints discretionaryPoints)
    {
        this.discretionaryPoints = discretionaryPoints;
    }

    public BusinessOffer getOffer()
    {
        return offer;
    }

    public void setOffer(BusinessOffer offer)
    {
        this.offer = offer;
    }
}
