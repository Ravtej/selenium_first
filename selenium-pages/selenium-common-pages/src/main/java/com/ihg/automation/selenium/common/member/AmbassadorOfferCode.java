package com.ihg.automation.selenium.common.member;

import com.ihg.automation.selenium.gwt.components.EntityType;

public enum AmbassadorOfferCode implements EntityType
{
    CHNRO("CHNRO", "China Reservations"), //
    CHSRO("CHSRO", "CHARLESTON RESERVATIONS"), //
    CMAMB("CMAMB", "COMP RAM DGRD REACTIVATION"), //
    CMRAM("CMRAM", "COMP RAM LVL REACTIVATION"), //
    CNCRO("CNCRO", "GUANGZHOU RESERVATIONS"), //
    EMERO("EMERO", "AMSTERDAM RESERVATIONS"), //
    MECRO("MECRO", "DUBAI RESERVATIONS"), //
    MEXRO("MEXRO", "MEXICO RESERVATIONS"), //
    MTGPL("MTGPL", "MEETING REWARDS"), //
    PLTSA("PLTSA", "DUAL SCC AMBASSADOR"), //
    PLTSE("PLTSE", "DUAL SCC EXECUTIVE 1"), //
    SAORO("SAORO", "SAO PAULO CRO"), //
    SCBRS("SCBRS", "SERVICE CENTER BRISTOL"), //
    SCCAM("SCCAM", "SCC AMBASSADOR"), //
    SCCCL("SCCCL", "SCC CLUB"), //
    SLCRO("SLCRO", "SALT LAKE RESERVATIONS"), //
    SPCRO("SPCRO", "SYDNEY RESERVATIONS"), //
    TYORO("TYORO", "TOKYO RESERVATIONS"), //
    UKCRO("UKCRO", "UK RESERVATIONS");

    private String code;
    private String value;

    private AmbassadorOfferCode(String code, String value)
    {
        this.code = code;
        this.value = value;
    }

    @Override
    public String getCode()
    {
        return code;
    }

    @Override
    public String getValue()
    {
        return value;
    }

    @Override
    public String getCodeWithValue()
    {
        return code + " - " + value;
    }

}
