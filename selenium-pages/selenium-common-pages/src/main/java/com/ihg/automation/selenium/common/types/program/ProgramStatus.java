package com.ihg.automation.selenium.common.types.program;

public class ProgramStatus
{
    public static final String OPEN = "Open";
    public static final String CLOSED = "Closed";
    public static final String REMOVED = "Removed";

    private ProgramStatus()
    {
    }
}
