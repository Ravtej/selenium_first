package com.ihg.automation.selenium.common.earning;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.Constant.NOT_AVAILABLE;
import static com.ihg.automation.selenium.common.Constant.RC_POINTS;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;

import com.ihg.automation.selenium.common.Constant;
import com.ihg.automation.selenium.common.event.EarningEvent;
import com.ihg.automation.selenium.common.pages.PopUpBase;
import com.ihg.automation.selenium.common.pages.TabCustomContainerBase;
import com.ihg.automation.selenium.common.types.Alliance;
import com.ihg.automation.selenium.gwt.components.label.Label;

public class EventEarningDetailsTab extends TabCustomContainerBase
{
    public EventEarningDetailsTab(PopUpBase parent)
    {
        super(parent, "Earning Details");
    }

    public Label getEarningPreference()
    {
        return container.getLabel("Earning Preference");
    }

    public EventEarningAmount getTotalIHGUnits()
    {
        return new EventEarningAmount(container.getLabel("Total IHG Loyalty Units"));
    }

    public EventEarningAmount getTotalQualifyingUnits()
    {
        return new EventEarningAmount(container.getLabel("Total Qualifying Loyalty Units"));
    }

    public EventEarningAmount getTotalAllianceUnits()
    {
        return new EventEarningAmount(container.getLabel("Total Alliance Units"));
    }

    public EventEarningGrid getGrid()
    {
        return new EventEarningGrid(container);
    }

    public void verifyBasicDetails(String earningPreference, String loyaltyAmount, String qualifyingLoyaltyAmount,
            String allianceAmount, Alliance alliance)
    {
        verifyThat(getEarningPreference(), hasText(earningPreference));
        getTotalIHGUnits().verify(loyaltyAmount);
        getTotalQualifyingUnits().verify(qualifyingLoyaltyAmount);
        getTotalAllianceUnits().verify(allianceAmount, alliance);
    }

    /**
     * <pre>
     *     'Earning Preference'             = 'RC Points'
     *     'Total IHG Loyalty Units'        = loyaltyAmount
     *     'Total Qualifying Loyalty Units' = qualifyingLoyaltyAmount
     *     'Total Alliance Units'           = 'n/a'
     * </pre>
     */
    public void verifyBasicDetailsForPoints(String loyaltyAmount, String qualifyingLoyaltyAmount)
    {
        verifyBasicDetails(RC_POINTS, loyaltyAmount, qualifyingLoyaltyAmount, NOT_AVAILABLE, null);
    }

    /**
     * <pre>
     *     'Earning Preference'             = 'RC Points'
     *     'Total IHG Loyalty Units'        = 'n/a'
     *     'Total Qualifying Loyalty Units' = 'n/a'
     *     'Total Alliance Units'           = 'n/a'
     * </pre>
     */
    public void verifyBasicDetailsNoPointsAndMiles()
    {
        verifyBasicDetailsForPoints(NOT_AVAILABLE, NOT_AVAILABLE);
    }

    /**
     * <pre>
     *     'Earning Preference'             = alliance.value
     *     'Total IHG Loyalty Units'        = 'n/a'
     *     'Total Qualifying Loyalty Units' = 'n/a'
     *     'Total Alliance Units'           = allianceAmount + alliance.code
     * </pre>
     */
    public void verifyBasicDetailsForMiles(Alliance alliance, String allianceAmount)
    {
        verifyBasicDetails(alliance.getValue(), NOT_AVAILABLE, NOT_AVAILABLE, allianceAmount, alliance);
    }

    public void verifyForBaseUnitsEvent(final String loyaltyAmount, final String qualifyingLoyaltyAmount)
    {
        goTo();

        verifyBasicDetailsForPoints(loyaltyAmount, qualifyingLoyaltyAmount);
        EarningEvent baseUnits = new EarningEvent(Constant.BASE_UNITS, getAmount(loyaltyAmount),
                getAmount(qualifyingLoyaltyAmount), Constant.RC_POINTS);
        getGrid().verify(baseUnits);
    }

    public void verifyForBaseUnitsEvent(final Integer loyaltyAmount, final Integer qualifyingLoyaltyAmount)
    {
        verifyForBaseUnitsEvent(String.valueOf(loyaltyAmount), String.valueOf(qualifyingLoyaltyAmount));
    }

    public void verifyForEmptyBaseUnitsEvent()
    {
        verifyForBaseUnitsEvent(NOT_AVAILABLE, NOT_AVAILABLE);
    }

    private String getAmount(String amount)
    {
        return NOT_AVAILABLE.equals(amount) ? "0" : amount;
    }
}
