package com.ihg.automation.selenium.common.member;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.JdbcTemplate;

import com.ihg.automation.common.JdbcExpectedCondition;
import com.ihg.automation.common.JdbcWait;
import com.ihg.automation.selenium.common.types.program.Program;

public class MemberJdbcUtils
{
    public static final String UPDATE_EXP = "UPDATE DGST.MBRSHP"//
            + " SET EXPR_DT = (SELECT TRUNC (ADD_MONTHS(SYSDATE,%d), 'MONTH') FROM DUAL) "//
            + " WHERE MBRSHP_ID = '%s' "//
            + " AND LYTY_PGM_CD = '%s'";

    public static final String UPDATE_TL_EXP = "UPDATE DGST.MBRSHP"//
            + " SET TIER_LVL_EXPR_DT = (SELECT ADD_MONTHS(TRUNC (SYSDATE ,'YEAR'),%d)-1 FROM DUAL) "//
            + " WHERE MBRSHP_ID = '%s' "//
            + " AND LYTY_PGM_CD = '%s'";

    private static final String MEMBER_ID_OPENED = "SELECT MBRSHP_ID"//
            + " FROM DGST.MBRSHP"//
            + " WHERE DGST.MBRSHP.MBRSHP_STAT_CD = 'O'"//
            + " AND DGST.MBRSHP.LYTY_PGM_CD = 'PC'"//
            + " AND DGST.MBRSHP.LST_UPDT_TS < SYSDATE -1" //
            + " AND ROWNUM < 2";

    public static final String MEMBER_ID = "SELECT MBRSHP_ID"//
            + " FROM DGST.MBRSHP"//
            + " WHERE DGST.MBRSHP.MBRSHP_STAT_CD = '%s'"//
            + " AND DGST.MBRSHP.LYTY_PGM_CD = '%s'"//
            + " AND DGST.MBRSHP.LST_UPDT_TS < SYSDATE -1" //
            + " AND ROWNUM < 2";

    private static final String MEMBER_ID_WITHOUT_EMAIL = "SELECT MBRSHP_ID"//
            + " FROM DGST.MBRSHP m"//
            + " WHERE m.MBRSHP_STAT_CD = 'O'"//
            + " AND m.LYTY_PGM_CD = 'PC'"//
            + " AND m.DGST_MSTR_KEY NOT IN (SELECT c.DGST_MSTR_KEY FROM DGST.GST_CONTACT c"//
            + " WHERE c.CONTACT_MECH_CD = 'E')"//
            + " AND m.LST_UPDT_TS < SYSDATE -1" //
            + " AND ROWNUM < 2";

    public static final String BENEFIT_COUNTERS = "SELECT COUNT(*) COUNTERS_COUNT"//
            + " FROM LYTYBENEFIT.MBRSHP_PROMO_STAT"//
            + " WHERE MBRSHP_ID = '%s'";

    private static final Logger logger = LoggerFactory.getLogger(MemberJdbcUtils.class);

    public static void waitAnnualActivitiesCounter(JdbcTemplate jdbcTemplate, Member member)
    {
        waitAnnualActivitiesCounter(jdbcTemplate, member.getRCProgramId());
    }

    public static void waitPartnerBenefitCounter(JdbcTemplate jdbcTemplate, Member member)
    {
        JdbcWait wait = new JdbcWait(jdbcTemplate, 30, 1000L);
        wait.until(partnerBenefits(member.getRCProgramId()));
    }

    public static void waitAnnualActivitiesCounter(JdbcTemplate jdbcTemplate, String member)
    {
        JdbcWait wait = new JdbcWait(jdbcTemplate, 30, 1000L);
        wait.until(annualActivities(member));
    }

    public static String getMemberId(JdbcTemplate jdbcTemplate, String sql)
    {
        Map<String, Object> map = jdbcTemplate.queryForMap(sql);
        return map.get("MBRSHP_ID").toString();
    }

    public static String getOpenedMemberId(JdbcTemplate jdbcTemplate)
    {
        return getMemberId(jdbcTemplate, MEMBER_ID_OPENED);
    }

    public static String getMemberIdWithoutEmail(JdbcTemplate jdbcTemplate)
    {
        return getMemberId(jdbcTemplate, MEMBER_ID_WITHOUT_EMAIL);
    }

    public static void update(JdbcTemplate jdbcTemplate, String sql, Member member, Program program, int months)
    {
        update(jdbcTemplate, sql, member.getRCProgramId(), program, months);
    }

    public static void update(JdbcTemplate jdbcTemplate, String sql, String memberId, Program program, int months)
    {
        jdbcTemplate.update(String.format(sql, months, memberId, program.getServiceCode()));
    }

    private static JdbcExpectedCondition<Boolean> annualActivities(final String memberId)
    {
        final String ANNUAL_COUNTERS = "SELECT COUNT(*) COUNTERS_COUNT FROM DGST.MBRSHP_ANNUAL_ACTV WHERE MBRSHP_KEY = "//
                + "(SELECT m.MBRSHP_KEY FROM DGST.MBRSHP m WHERE m.MBRSHP_ID = '%s' AND m.LYTY_PGM_CD = 'PC' )";
        String annualCounters = String.format(ANNUAL_COUNTERS, memberId);

        return new JdbcExpectedCondition<Boolean>()
        {
            @Override
            public Boolean apply(JdbcTemplate jdbcTemplate)
            {
                return waitRulesCreatingData(jdbcTemplate, annualCounters, "initial counters");

            }

            @Override
            public String toString()
            {
                return "rules creating initial counters";
            }
        };
    }

    private static JdbcExpectedCondition<Boolean> partnerBenefits(final String memberId)
    {
        String benefitCounters = String.format(BENEFIT_COUNTERS, memberId);

        return new JdbcExpectedCondition<Boolean>()
        {
            @Override
            public Boolean apply(JdbcTemplate jdbcTemplate)
            {
                return waitRulesCreatingData(jdbcTemplate, benefitCounters, "partner benefits");
            }

            @Override
            public String toString()
            {
                return "rules creating partner benefits";
            }
        };
    }

    private static Boolean waitRulesCreatingData(JdbcTemplate jdbcTemplate, String sqlQuery, String logMessage)
    {
        int count = jdbcTemplate.queryForObject(sqlQuery, Integer.class);

        if (count != 0)
        {
            return true;
        }

        logger.debug("Wait rules creating {} ...", logMessage);
        return false;
    }

}
