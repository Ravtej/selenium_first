package com.ihg.automation.selenium.common;

import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyNoError;

import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.gwt.components.Component;
import com.ihg.automation.selenium.gwt.components.grid.GridCell;
import com.ihg.automation.selenium.gwt.components.grid.GridRow;
import com.ihg.automation.selenium.gwt.components.grid.GridRowXpath;
import com.ihg.automation.selenium.gwt.components.utils.ByText;

public class ProgramGridRow extends GridRow<ProgramGridCell>
{
    protected GridCell levelCode;

    public ProgramGridRow(ProgramsGrid parent, GridRowXpath xpath)
    {
        super(parent, xpath);
        levelCode = getCell(ProgramGridCell.LEVEL_CODE);// for cashing
        // purpose
    }

    public ProgramGridRow(ProgramsGrid parent, Program program)
    {
        this(parent, new GridRowXpath.GridRowXpathBuilder()
                .cell(ProgramGridCell.CODE, ByText.startsWithSelf(program.getValue())).build());
    }

    public Component getLevelCode()
    {
        return levelCode;
    }

    public void goToProgramPage()
    {
        getCell(1).clickAndWait(verifyNoError());
    }
}
