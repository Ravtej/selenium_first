package com.ihg.automation.selenium.common.types.address;

import com.ihg.automation.selenium.common.types.LabelEnumAware;

public enum Locality2Label implements LabelEnumAware
{
    LOCALITY("Locality"), DISTRICT("District"), NEIGHBORHOOD("Neighborhood");

    private String label;

    private Locality2Label(String label)
    {
        this.label = label;
    }

    @Override
    public String getLabel()
    {
        return label;
    }
}
