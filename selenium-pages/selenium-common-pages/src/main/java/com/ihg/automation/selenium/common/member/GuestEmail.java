package com.ihg.automation.selenium.common.member;

import com.ihg.automation.selenium.common.Constant;
import com.ihg.automation.selenium.common.types.EmailFormat;
import com.ihg.automation.selenium.common.types.EmailType;

public class GuestEmail extends GuestContact
{
    private EmailFormat format = EmailFormat.HTML;
    private String email = Constant.NOT_AVAILABLE;

    public GuestEmail()
    {
    }

    public GuestEmail(EmailType type, EmailFormat format, String email)
    {
        this.type = type;
        this.format = format;
        this.email = email;
    }

    public EmailFormat getFormat()
    {
        return format;
    }

    public void setFormat(EmailFormat format)
    {
        this.format = format;
    }

    public String getEmail()
    {
        return email;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }

    public String getEmailDomain()
    {
        return email.substring(email.indexOf("@"));
    }
}
