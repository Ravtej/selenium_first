package com.ihg.automation.selenium.common.payment;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static com.ihg.automation.selenium.matchers.text.AsFormattedDouble.asFormattedDouble;

import org.hamcrest.Matcher;

import com.ihg.automation.selenium.common.pages.PaymentMethod;
import com.ihg.automation.selenium.gwt.components.Componentable;
import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.CustomContainerBase;
import com.ihg.automation.selenium.gwt.components.Mode;
import com.ihg.automation.selenium.gwt.components.label.Label;

public class PaymentDetails extends CustomContainerBase
{
    public PaymentDetails(Container container)
    {
        super(container);
    }

    public Label getMethod()
    {
        return container.getLabel("Payment Method");
    }

    public CurrencyAmount getCurrencyAmount()
    {
        return new CurrencyAmount(container);
    }

    public CreditCardPaymentContainer getCreditCardPayment()
    {
        return new CreditCardPaymentContainer(container);
    }

    public CheckPaymentContainer getCheckPayment()
    {
        return new CheckPaymentContainer(container);
    }

    public CashPaymentContainer getCashPayment()
    {
        return new CashPaymentContainer(container);
    }

    public void verifyCommonDetails(Payment payment)
    {
        verifyThat(getMethod(), hasText(payment.getMethod().getLabel()));
        Matcher<Componentable> amountMatcher;
        if (PaymentMethod.LOYALTY_UNITS == payment.getMethod())
        {
            amountMatcher = hasText(payment.getAmount());
        }
        else
        {
            amountMatcher = hasText(asFormattedDouble(payment.getAmount()));
        }

        getCurrencyAmount().verify(amountMatcher, payment.getCurrency());

    }

    public void verify(CreditCardPayment creditCardPayment, Mode mode)
    {
        verifyCommonDetails(creditCardPayment);
        getCreditCardPayment().verify(creditCardPayment, mode);
    }

    public void verify(CashPayment cashPayment, Mode mode)
    {
        verifyCommonDetails(cashPayment);
        getCashPayment().verify(cashPayment, mode);
    }

    public void verify(CheckPayment checkPayment, Mode mode)
    {
        verifyCommonDetails(checkPayment);
        getCheckPayment().verify(checkPayment, mode);
    }

    public void verify(Payment payment, Mode mode)
    {
        if (payment instanceof CreditCardPayment)
        {
            verify((CreditCardPayment) payment, mode);
            return;
        }

        if (payment instanceof CashPayment)
        {
            verify((CashPayment) payment, mode);
            return;
        }

        if (payment instanceof CheckPayment)
        {
            verify((CheckPayment) payment, mode);
            return;
        }

        verifyCommonDetails(payment);
    }
}
