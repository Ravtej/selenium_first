package com.ihg.automation.selenium.common.types.name;

import com.ihg.automation.selenium.common.types.LabelEnumAware;

public enum SurnameLabel implements LabelEnumAware
{
    SURNAME("Surname"), FAMILY("Family Name"), LAST("Last Name");

    private String label;

    private SurnameLabel(String label)
    {
        this.label = label;
    }

    @Override
    public String getLabel()
    {
        return label;
    }
}
