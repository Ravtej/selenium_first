package com.ihg.automation.selenium.common.earning;

public interface EventEarningTabAware
{
    public EventEarningDetailsTab getEarningDetailsTab();
}
