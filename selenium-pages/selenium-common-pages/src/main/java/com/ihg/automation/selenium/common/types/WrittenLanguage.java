package com.ihg.automation.selenium.common.types;

import com.ihg.automation.selenium.gwt.components.EntityType;

public enum WrittenLanguage implements EntityType
{
    ITALIAN("it", "Italian"), //
    FRENCH("fr", "French"), //
    PORTUGUESE("pt", "Portuguese"), DUTCH("nl", "Dutch"), //
    SPANISH("es", "Spanish"), //
    JAPANESE("ja", "Japanese"), //
    GERMAN("de", "German"), //
    ENGLISH("en", "English"), //
    CHINESE("zn", "Simplified Chinese");

    private String code;
    private String value;

    private WrittenLanguage(String code, String value)
    {
        this.code = code;
        this.value = value;
    }

    @Override
    public String getCode()
    {
        return code;
    }

    @Override
    public String getValue()
    {
        return value;
    }

    @Override
    public String toString()
    {
        return getValue();
    }

    @Override
    public String getCodeWithValue()
    {
        return code + " - " + value;
    }
}
