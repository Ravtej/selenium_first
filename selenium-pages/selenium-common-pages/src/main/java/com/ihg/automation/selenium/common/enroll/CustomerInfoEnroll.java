package com.ihg.automation.selenium.common.enroll;

import com.ihg.automation.selenium.common.name.PersonNameEnrollFields;
import com.ihg.automation.selenium.common.name.PersonNameFields;
import com.ihg.automation.selenium.gwt.components.Container;

public class CustomerInfoEnroll extends CustomerInfo
{

    public CustomerInfoEnroll(Container container)
    {
        super(container);
    }

    @Override
    public PersonNameFields getName()
    {
        return new PersonNameEnrollFields(container);
    }
}
