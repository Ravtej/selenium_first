package com.ihg.automation.selenium.common.history;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.history.EventAuditConstant.BusinessReward.Meeting.FOOD_BEVERAGE;
import static com.ihg.automation.selenium.common.history.EventAuditConstant.BusinessReward.Meeting.MEETING_ROOM;
import static com.ihg.automation.selenium.common.history.EventAuditConstant.BusinessReward.Meeting.MISCELLANEOUS;
import static com.ihg.automation.selenium.common.types.Currency.USD;
import static com.ihg.automation.selenium.gwt.components.utils.FindStepUtils.byXpath;
import static com.ihg.automation.selenium.gwt.components.utils.FindStepUtils.byXpathWithWait;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasEmptyText;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasTextAsDouble;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static java.text.MessageFormat.format;

import org.hamcrest.Matcher;

import com.ihg.automation.selenium.common.business.BusinessRevenueBean;
import com.ihg.automation.selenium.gwt.components.Component;
import com.ihg.automation.selenium.gwt.components.Componentable;
import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.utils.ByText;

public class EventHistoryRecord extends Component
{
    private static final String AUDIT_PATTERN = "{0} ({1})";

    public EventHistoryRecord(Container container, String key)
    {
        super(container, "Audit Record", key, byXpathWithWait(format(".//td[child::span[{0}]]", ByText.exact(key))));
    }

    public EventHistoryRecord(Container container, int index)
    {
        super(container, "Audit Record", String.format("%d Audit Record", index),
                byXpathWithWait(format(".//tr[{0}]//td[1]", index)));
    }

    public Component getOldValue()
    {
        return new Component(this, getType(), format(AUDIT_PATTERN, getName(), "oldValue"),
                byXpath("./following-sibling::td[1]"));
    }

    public Component getNewValue()
    {
        return new Component(this, getType(), format(AUDIT_PATTERN, getName(), "newValue"),
                byXpath("./following-sibling::td[2]"));
    }

    public void verify(Matcher<Componentable> oldValueMatcher, Matcher<Componentable> newValueMatcher)
    {
        verifyThat(getOldValue(), oldValueMatcher);
        verifyThat(getNewValue(), newValueMatcher);
    }

    public void verifyAdd(String name, String newValue)
    {
        verifyThat(this, hasText(name));
        verifyAdd(newValue);
    }

    public void verifyAdd(String name, Matcher<Componentable> newValueMatcher)
    {
        verifyThat(this, hasText(name));
        verifyAdd(newValueMatcher);
    }

    public void verify(String oldValue, String newValue)
    {
        verify(hasText(oldValue), hasText(newValue));
    }

    public void verifyAdd(Matcher<Componentable> newValueMatcher)
    {
        verify(hasEmptyText(), newValueMatcher);
    }

    public void verifyAdd(String newValue)
    {
        verifyAdd(hasText(newValue));
    }
}
