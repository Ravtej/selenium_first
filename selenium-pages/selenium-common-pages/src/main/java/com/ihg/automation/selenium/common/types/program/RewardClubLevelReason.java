package com.ihg.automation.selenium.common.types.program;

import java.util.ArrayList;

public enum RewardClubLevelReason implements ProgramLevelReason
{
    AMBASSADOR("A", "Ambassador"), //
    BASE("S", "Base"), //
    BOUGHT_ELITE_STATUS("B", "Bought Elite Status"), //
    COMPETITOR("C", "Competitor"), //
    IHG_DINING_REWARDS("D", "IHG Dining Rewards"), //
    EXECUTIVE("E", "Executive"), //
    GRACE("G", "Grace"), //
    MEETINGS("M", "Meetings"), //
    POINTS("P", "Points"), //
    INNER_CIRCLE("KIC", "Inner Circle"), //
    VIP("V", "VIP");

    private String code;
    private String value;

    private RewardClubLevelReason(String code, String description)
    {
        this.code = code;
        this.value = description;
    }

    @Override
    public String getCode()
    {
        return code;
    }

    @Override
    public String getValue()
    {
        return value;
    }

    @Override
    public String getCodeWithValue()
    {
        return code + " - " + value;
    }

    public String toString()
    {
        return getValue();
    }

    public static ArrayList<String> getValuesList()
    {
        RewardClubLevelReason[] vals = values();
        ArrayList<String> res = new ArrayList<String>();
        for (RewardClubLevelReason val : vals)
        {
            res.add(val.getValue());
        }

        String env = System.getProperty("env");
        if (env.equals("qa") || env.equals("stg"))
        {
            res.remove(INNER_CIRCLE.getValue());
        }

        return res;
    }

}
