package com.ihg.automation.selenium.common.pages;

public interface Populate<T>
{
    void populate(T obj);
}
