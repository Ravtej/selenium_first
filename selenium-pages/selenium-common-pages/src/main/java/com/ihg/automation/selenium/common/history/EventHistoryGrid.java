package com.ihg.automation.selenium.common.history;

import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.grid.Grid;

public class EventHistoryGrid extends Grid<EventHistoryGridRow, EventHistoryGridRow.EventHistoryCell>
{
    public EventHistoryGrid(Container parent)
    {
        super(parent);
    }

}
