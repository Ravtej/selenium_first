package com.ihg.automation.selenium.common.contact;

import com.ihg.automation.selenium.common.member.MeetingPhone;
import com.ihg.automation.selenium.common.pages.Populate;
import com.ihg.automation.selenium.gwt.components.ComponentColumnPosition;
import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.CustomContainerBase;
import com.ihg.automation.selenium.gwt.components.input.Input;

public class MeetingPhoneFields extends CustomContainerBase implements Populate<MeetingPhone>
{
    private ComponentColumnPosition POSITION = ComponentColumnPosition.SINGLE_TD;
    private String labelText = "Sales Manager Phone";

    public MeetingPhoneFields(Container container)
    {
        super(container);
    }

    public Input getCountryCode()
    {
        return container.getLabel(labelText).getInput("Country Access Code", 1, POSITION);
    }

    public Input getCityCode()
    {
        return container.getLabel(labelText).getInput("Area City Code", 2, POSITION);
    }

    public Input getNumber()
    {
        return container.getLabel(labelText).getInput("Phone Number", 3, POSITION);
    }

    public Input getExtension()
    {
        return container.getLabel(labelText).getInput("Phone Extension", 4, POSITION);
    }

    @Override
    public void populate(MeetingPhone phone)
    {
        getCountryCode().type(phone.getCountryCode());
        getCityCode().type(phone.getCityCode());
        getNumber().type(phone.getNumber());
        getExtension().type(phone.getExtension());
    }

}
