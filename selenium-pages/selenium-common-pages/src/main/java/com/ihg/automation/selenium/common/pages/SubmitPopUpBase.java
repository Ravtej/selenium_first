package com.ihg.automation.selenium.common.pages;

import com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory;
import com.ihg.automation.selenium.gwt.components.Button;
import com.ihg.automation.selenium.gwt.components.PopUpWindow;
import com.ihg.automation.selenium.listener.Verifier;

public abstract class SubmitPopUpBase extends ClosablePopUpBase
{
    public SubmitPopUpBase(String header)
    {
        super(header);
    }

    public SubmitPopUpBase(PopUpWindow popUp)
    {
        super(popUp);
    }

    public Button getSubmit()
    {
        return container.getButton(Button.SUBMIT);
    }

    public void clickSubmitNoError(Verifier... verifiers)
    {
        getSubmit().clickAndWait(MessageBoxVerifierFactory.assertNoError().add(verifiers));
    }

    public void clickSubmit(Verifier... verifiers)
    {
        getSubmit().clickAndWait(verifiers);
    }

}
