package com.ihg.automation.selenium.common.hotel;

import com.ihg.automation.selenium.gwt.components.grid.CellsName;
import com.ihg.automation.selenium.gwt.components.grid.Grid;
import com.ihg.automation.selenium.gwt.components.grid.GridRow;
import com.ihg.automation.selenium.gwt.components.grid.GridRowXpath;

public class AdvancedHotelSearchGridRow extends GridRow<AdvancedHotelSearchGridRow.AdvancedHotelSearchCell>
{
    public AdvancedHotelSearchGridRow(Grid parent, GridRowXpath xpath)
    {
        super(parent, xpath);
    }

    enum AdvancedHotelSearchCell implements CellsName
    {
        HOTEL("hotelCode", "Hotel Code"), //
        NAME("hotelName", "Hotel Name"), //
        ADDRESS("addressLine", "Hotel Address"), //
        CITY("locality1", "Hotel City"), //
        STATE("countryRegion", "Hotel Region"), //
        COUNTRY("countryCode", "Hotel Country"), //
        BRAND("brandCode", "Hotel Brand"), //
        CHAIN("chainCode", "Hotel Chain"), //
        STATUS("hotelLoyaltyStatus", "Hotel Loyalty Status");

        private String name;
        private String value;

        AdvancedHotelSearchCell(String name, String value)
        {
            this.name = name;
            this.value = value;
        }

        public String getValue()
        {
            return value;
        }

        @Override
        public String getName()
        {
            return name;
        }
    }
}
