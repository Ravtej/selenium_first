package com.ihg.automation.selenium.common.contact;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.automation.selenium.matchers.component.ViewableMatcher.hasText;
import static org.hamcrest.core.StringContains.containsString;

import com.ihg.automation.selenium.common.components.Country;
import com.ihg.automation.selenium.common.components.CountrySelect;
import com.ihg.automation.selenium.common.member.GuestAddress;
import com.ihg.automation.selenium.common.types.address.AddressConfiguration;
import com.ihg.automation.selenium.common.types.address.AddressConfigurationBean;
import com.ihg.automation.selenium.common.types.address.Locality1Label;
import com.ihg.automation.selenium.common.types.address.Locality2Label;
import com.ihg.automation.selenium.common.types.address.RegionLabel;
import com.ihg.automation.selenium.common.types.address.ZipLabel;
import com.ihg.automation.selenium.gwt.components.ComponentColumnPosition;
import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.Link;
import com.ihg.automation.selenium.gwt.components.Mode;
import com.ihg.automation.selenium.gwt.components.checkbox.CheckBox;
import com.ihg.automation.selenium.gwt.components.input.Input;
import com.ihg.automation.selenium.gwt.components.input.Select;
import com.ihg.automation.selenium.gwt.components.panel.EditablePanel;
import com.ihg.automation.selenium.matchers.component.HasText;

public class Address extends ContactBase<GuestAddress>
{
    private ComponentColumnPosition POSITION = ComponentColumnPosition.FOLLOWING_TD;
    private AddressConfigurationBean addressConfiguration;
    private static final String NATIVE = "(native)";

    public Address(Container container, int index)
    {
        super(new EditablePanel(container.getSeparator("Address"), index));
    }

    public Address(Container container)
    {
        super(container);
        setAddressConfiguration(AddressConfiguration.getDefaultConfig());
    }

    private AddressConfigurationBean getAddressConfiguration()
    {
        if (addressConfiguration == null)
        {
            throw new RuntimeException("Address config is not set!");
        }
        return addressConfiguration;
    }

    public void setAddressConfiguration(AddressConfigurationBean config)
    {
        this.addressConfiguration = config;
    }

    public void setAddressConfiguration(Country country)
    {
        setAddressConfiguration(AddressConfiguration.getAddressConfiguration(country));
    }

    public CheckBox getDoNotClean()
    {
        return container.getCheckBox("Do not clean");
    }

    public CountrySelect getCountry()
    {
        return new CountrySelect(container);
    }

    public Input getCompanyName()
    {
        return container.getInputByLabel("Company Name");
    }

    public Input getAddress1()
    {
        return container.getInputByLabel("Address 1");
    }

    public Input getNativeAddress1()
    {
        return container.getLabel("Address 1").getInput("Address 1" + NATIVE, 2, POSITION);
    }

    public Input getAddress2()
    {
        return container.getInputByLabel("Address 2");
    }

    public Input getNativeAddress2()
    {
        return container.getLabel("Address 2").getInput("Address 2" + NATIVE, 2, POSITION);
    }

    public Input getAddress3()
    {
        return container.getInputByLabel("Address 3");
    }

    public Input getNativeAddress3()
    {
        return container.getLabel("Address 3").getInput("Address 3" + NATIVE, 2, POSITION);
    }

    public Input getAddress4()
    {
        return container.getInputByLabel("Address 4");
    }

    public Input getNativeAddress4()
    {
        return container.getLabel("Address 4").getInput("Address 4" + NATIVE, 2, POSITION);
    }

    public Input getAddress5()
    {
        return container.getInputByLabel("Address 5");
    }

    public Input getNativeAddress5()
    {
        return container.getLabel("Address 5").getInput("Address 5" + NATIVE, 2, POSITION);
    }

    public Select getRegion1(RegionLabel region)
    {
        return container.getSelectByLabel(region.getLabel());
    }

    public Select getNativeRegion1(RegionLabel region)
    {
        return container.getLabel(region.getLabel()).getSelect(region.getLabel() + NATIVE, 2, POSITION);
    }

    public Select getRegion1()
    {
        return getRegion1(getAddressConfiguration().getRegion());
    }

    public Select getNativeRegion1()
    {
        return getNativeRegion1(getAddressConfiguration().getRegion());
    }

    public Input getRegion2(RegionLabel region)
    {
        return container.getInputByLabel(region.getLabel());
    }

    public Input getNativeRegion2(RegionLabel region)
    {
        return container.getLabel(region.getLabel()).getInput(region.getLabel() + NATIVE, 2, POSITION);
    }

    public Input getRegion2()
    {
        return getRegion2(getAddressConfiguration().getRegion());
    }

    public Input getNativeRegion2()
    {
        return getNativeRegion2(getAddressConfiguration().getRegion());
    }

    public Input getLocality1(Locality1Label locality)
    {
        return container.getInputByLabel(locality.getLabel());
    }

    public Input getNativeLocality1(Locality1Label locality)
    {
        return container.getLabel(locality.getLabel()).getInput(locality.getLabel() + NATIVE, 2, POSITION);
    }

    public Input getLocality1()
    {
        return getLocality1(getAddressConfiguration().getLocality1());
    }

    public Input getNativeLocality1()
    {
        return getNativeLocality1(getAddressConfiguration().getLocality1());
    }

    public Input getLocality2(Locality2Label locality)
    {
        return container.getInputByLabel(locality.getLabel());
    }

    public Input getNativeLocality2(Locality2Label locality)
    {
        return container.getLabel(locality.getLabel()).getInput(locality.getLabel() + NATIVE, 2, POSITION);
    }

    public Input getLocality2()
    {
        return getLocality2(getAddressConfiguration().getLocality2());
    }

    public Input getNativeLocality2()
    {
        return getNativeLocality2(getAddressConfiguration().getLocality2());
    }

    public Input getZipCode(ZipLabel code)
    {
        return container.getInputByLabel(code.getLabel());
    }

    public Input getNativeZipCode(ZipLabel code)
    {
        return container.getLabel(code.getLabel()).getInput(code.getLabel() + NATIVE, 2, POSITION);
    }

    public Input getZipCode()
    {
        return getZipCode(getAddressConfiguration().getPostalCode());
    }

    public Input getNativeZipCode()
    {
        return getNativeZipCode(getAddressConfiguration().getPostalCode());
    }

    public Link getTransliterate()
    {
        return new Link(container, "Transliterate", Link.LINK_PATTERN_WITH_TEXT_NO_HYPER);
    }

    public Link getClearNativeAddress()
    {
        return new Link(container, "Clear Native Address", Link.LINK_PATTERN_WITH_TEXT_NO_HYPER);
    }

    @Override
    public void populate(GuestAddress address)
    {
        super.populate(address);

        setAddressConfiguration(address.getCountry());

        getCountry().selectByCode(address.getCountry());
        getCompanyName().type(address.getCompanyName());
        getAddress1().type(address.getLine1());
        getAddress2().type(address.getLine2());
        getAddress3().type(address.getLine3());
        getAddress4().type(address.getLine4());
        getAddress5().type(address.getLine5());
        getRegion2().type(address.getRegion2());
        getLocality1().type(address.getLocality1());
        getLocality2().type(address.getLocality2());
        getZipCode().type(address.getPostalCode());

        if (null != address.getRegion1())
        {
            if (null != address.getRegion1().getCode())
            {
                getRegion1().selectByCode(address.getRegion1());
            }
            else
            {
                getRegion1().type(address.getRegion1().getValue());
            }
        }
    }

    public void populateNativeFields(GuestAddress address)
    {
        super.populate(address);

        setAddressConfiguration(address.getCountry());
        getCountry().selectByCode(address.getCountry());

        if (address.getNativeAddress() != null)
        {
            getNativeAddress1().type(address.getNativeAddress().getLine1());
            getNativeAddress2().type(address.getNativeAddress().getLine2());
            getNativeAddress3().type(address.getNativeAddress().getLine3());
            getNativeAddress4().type(address.getNativeAddress().getLine4());
            getNativeAddress5().type(address.getNativeAddress().getLine5());
            getNativeRegion2().type(address.getNativeAddress().getRegion2());
            getNativeLocality1().type(address.getNativeAddress().getLocality1());
            getNativeLocality2().type(address.getNativeAddress().getLocality2());
            getNativeZipCode().type(address.getNativeAddress().getPostalCode());

            if (null != address.getNativeAddress().getRegion1())
            {
                if (null != address.getNativeAddress().getRegion1().getCode())
                {
                    getNativeRegion1().selectByCode(address.getNativeAddress().getRegion1());
                }
                else
                {
                    getNativeRegion1().type(address.getNativeAddress().getRegion1().getValue());
                }
            }
        }

        getTransliterate().clickAndWait();
    }

    public void verify(GuestAddress address, Mode mode)
    {
        super.verify(address, mode);

        setAddressConfiguration(address.getCountry());

        verifyThat(getCountry(), hasText(address.getCountry().getValue(), mode));
        verifyThat(getCompanyName(), hasText(address.getCompanyName(), mode));
        verifyThat(getAddress1(), hasText(address.getLine1(), mode));
        verifyThat(getAddress2(), hasText(address.getLine2(), mode));
        verifyThat(getAddress3(), hasText(address.getLine3(), mode));
        verifyThat(getAddress4(), hasText(address.getLine4(), mode));
        verifyThat(getAddress5(), hasText(address.getLine5(), mode));

        if (null != address.getRegion1())
        {
            verifyThat(getRegion1(), hasText(address.getRegion1().getValue(), mode));
        }

        if (null != address.getRegion2())
        {
            verifyThat(getRegion2(), hasText(address.getRegion2(), mode));
        }

        verifyThat(getLocality1(), hasText(address.getLocality1(), mode));
        verifyThat(getLocality2(), hasText(address.getLocality2(), mode));
        verifyThat(getZipCode(), hasText(address.getPostalCode(), mode));
    }

    public void verifyNativeFields(GuestAddress address, String nativeLanguage, Mode mode)
    {
        verifyNativeFieldsBase(nativeLanguage);

        GuestAddress nativeAddress = address.getNativeAddress();
        setAddressConfiguration(address.getCountry());

        verifyThat(getNativeAddress1(), hasText(nativeAddress.getLine1(), mode));
        verifyThat(getNativeAddress2(), hasText(nativeAddress.getLine2(), mode));
        verifyThat(getNativeAddress3(), hasText(nativeAddress.getLine3(), mode));
        verifyThat(getNativeAddress4(), hasText(nativeAddress.getLine4(), mode));
        verifyThat(getNativeAddress5(), hasText(nativeAddress.getLine5(), mode));

        if (null != nativeAddress.getRegion1())
        {
            verifyThat(getNativeRegion1(), hasText(nativeAddress.getRegion1().getValue(), mode));
        }

        if (null != nativeAddress.getRegion2())
        {
            verifyThat(getNativeRegion2(), hasText(nativeAddress.getRegion2(), mode));
        }

        verifyThat(getNativeLocality1(), hasText(nativeAddress.getLocality1(), mode));
        verifyThat(getNativeLocality2(), hasText(nativeAddress.getLocality2(), mode));
        verifyThat(getNativeZipCode(), hasText(nativeAddress.getPostalCode(), mode));
    }

    public void verifyNativeFieldsBase(String nativeLanguage)
    {
        verifyThat(this, HasText.hasText(containsString(nativeLanguage)));
        verifyThat(getTransliterate(), displayed(true));
        verifyThat(getClearNativeAddress(), displayed(true));
    }
}
