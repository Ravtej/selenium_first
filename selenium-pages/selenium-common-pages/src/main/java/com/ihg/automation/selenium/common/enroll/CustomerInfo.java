package com.ihg.automation.selenium.common.enroll;

import com.ihg.automation.selenium.common.BirthDate;
import com.ihg.automation.selenium.common.components.CountrySelect;
import com.ihg.automation.selenium.common.member.PersonalInfo;
import com.ihg.automation.selenium.common.name.PersonNameFields;
import com.ihg.automation.selenium.common.pages.Populate;
import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.CustomContainerBase;
import com.ihg.automation.selenium.gwt.components.checkbox.CheckBox;
import com.ihg.automation.selenium.gwt.components.input.Select;

public abstract class CustomerInfo extends CustomContainerBase
        implements CustomerInfoCommonFieldsAware, Populate<PersonalInfo>
{

    public CustomerInfo(Container container)
    {
        super(container);
    }

    @Override
    public abstract PersonNameFields getName();

    @Override
    public Select getGender()
    {
        return container.getSelectByLabel("Gender");
    }

    @Override
    public BirthDate getBirthDate()
    {
        return new BirthDate(container);
    }

    @Override
    public CountrySelect getResidenceCountry()
    {
        return new CountrySelect(container);
    }

    public Select getWrittenLanguage()
    {
        return container.getSelectByLabel("Written Language");
    }

    public CheckBox getDoNotContact()
    {
        return container.getLabel("Do Not Contact").getCheckBox();
    }

    @Override
    public void populate(PersonalInfo info)
    {
        getResidenceCountry().selectByCode(info.getResidenceCountry());
        PersonNameFields name = getName();
        name.setConfiguration(info.getResidenceCountry());
        name.populate(info.getName());

        getGender().selectByValue(info.getGenderCode());
        getBirthDate().populate(info.getBirthDate());
        // TODO getWrittenLanguage()
    }
}
