package com.ihg.automation.selenium.common;

public class Channel
{
    public final static String MSCUI = "MSCUI";
    public final static String MHUI = "MHUI";
    public final static String RULES = "Rules";
    public final static String ORM = "ORM";

    private Channel()
    {
    }
}
