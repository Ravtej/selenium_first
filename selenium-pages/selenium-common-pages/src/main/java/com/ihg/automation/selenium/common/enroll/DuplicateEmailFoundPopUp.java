package com.ihg.automation.selenium.common.enroll;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasText;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.isDisplayedWithWait;
import static org.hamcrest.core.StringContains.containsString;

import com.ihg.automation.selenium.common.pages.PopUpBase;
import com.ihg.automation.selenium.gwt.components.Button;

public class DuplicateEmailFoundPopUp extends PopUpBase
{
    public DuplicateEmailFoundPopUp()
    {
        super("Duplicate Email Found");
    }

    public Button getUpdateEmail()
    {
        return container.getButton("Update Email");
    }

    public Button getAbandonEnrollment()
    {
        return container.getButton("Abandon Enrollment");
    }

    public Button getAbandonReopen()
    {
        return container.getButton("Abandon Reopen");
    }

    public void clickUpdate()
    {
        container.getButton("Update Email").clickAndWait();
    }

    public void clickAbandonEnroll()
    {
        container.getButton("Abandon Enrollment").clickAndWait();
    }

    public void clickAbandonReopen()
    {
        getAbandonReopen().clickAndWait();
    }

    public DuplicateEmailGrid getDuplicateEmailsGrid()
    {
        return new DuplicateEmailGrid(this.container);
    }

    public void verifyWarningMessage(String message)
    {
        verifyThat(this, isDisplayedWithWait());
        verifyThat(getDuplicateEmailsGrid().getNavigationPanel(), displayed(false));
        verifyThat(this, hasText(containsString(message)));
    }

}
