package com.ihg.automation.selenium.common;

import java.text.MessageFormat;

public class User
{
    private String userId;
    private String password;
    private String domain;
    private String employeeId;
    private String location;
    private boolean isHelpDeskRole;

    private final String nameFull;

    public User(String userId, String employeeId, String location, String domain)
    {
        this.userId = userId;
        this.employeeId = employeeId;
        this.location = location;
        this.domain = domain;

        nameFull = MessageFormat.format("{0}\\{1}", domain, userId);
    }

    public User(String userId, String domain)
    {
        this(userId, null, null, domain);
    }

    public String getUserId()
    {
        return userId;
    }

    public void setUserId(String userId)
    {
        this.userId = userId;
    }

    public String getPassword()
    {
        return password;
    }

    public void setPassword(String password)
    {
        this.password = password;
    }

    public String getDomain()
    {
        return domain;
    }

    public void setDomain(String domain)
    {
        this.domain = domain;
    }

    public String getEmployeeId()
    {
        return employeeId;
    }

    public void setEmployeeId(String employeeId)
    {
        this.employeeId = employeeId;
    }

    public String getLocation()
    {
        return location;
    }

    public void setLocation(String location)
    {
        this.location = location;
    }

    public boolean isHelpDeskRole()
    {
        return isHelpDeskRole;
    }

    public void setIsHelpDeskRole(boolean isHelpDeskRole)
    {
        this.isHelpDeskRole = isHelpDeskRole;
    }

    public String getNameFull()
    {
        return nameFull;
    }
}
