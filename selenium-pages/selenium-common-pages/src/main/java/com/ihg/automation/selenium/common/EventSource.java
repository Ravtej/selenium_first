package com.ihg.automation.selenium.common;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;

import com.ihg.automation.selenium.common.event.Source;
import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.CustomContainerBase;
import com.ihg.automation.selenium.gwt.components.label.Label;

public class EventSource extends CustomContainerBase
{
    public EventSource(Container container)
    {
        super(container);
    }

    public Label getChannel()
    {
        return container.getLabel("Channel");
    }

    public Label getLocation()
    {
        return container.getLabel("Location");
    }

    public Label getType()
    {
        return container.getLabel("Source Type");
    }

    public Label getEmployeeId()
    {
        return container.getLabel("Employee ID");
    }

    public Label getUserId()
    {
        return container.getLabel("Employee User ID");
    }

    public void verifyNoType(Source source)
    {
        verifyThat(getChannel(), hasText(source.getChannel()));
        verifyThat(getLocation(), hasText(source.getLocation()));
        verifyThat(getEmployeeId(), hasText(source.getEmployeeID()));
        verifyThat(getUserId(), hasText(source.getUserID()));
    }

    public void verify(Source source)
    {
        verifyNoType(source);
        verifyThat(getType(), hasText(source.getSourceType()));
    }
}
