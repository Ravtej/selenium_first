package com.ihg.automation.selenium.common.order;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.Constant.NOT_AVAILABLE;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasDefault;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;

import com.ihg.automation.selenium.common.Constant;
import com.ihg.automation.selenium.common.annual.ActivityAmount;
import com.ihg.automation.selenium.common.payment.CurrencyAmount;
import com.ihg.automation.selenium.gwt.components.Component;
import com.ihg.automation.selenium.gwt.components.Componentable;
import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.CustomContainerBase;
import com.ihg.automation.selenium.gwt.components.HavingDefaultValue;
import com.ihg.automation.selenium.gwt.components.label.Label;
import com.ihg.automation.selenium.gwt.components.utils.FindStepUtils;


public abstract class OrderDetailsBase extends CustomContainerBase
{
    protected static final String ESTIMATED_DELIVERY_DATE_LABEL = "Estimated Delivery Date";
    protected static final String ACTUAL_DELIVERY_DATE_LABEL = "Actual Delivery Date";
    protected static final String DELIVERY_STATUS_LABEL = "Delivery Status";
    protected static final String TRACKING_NUMBER_LABEL = "Tracking Unique Number";
    protected static final String SIGNED_FOR_BY_LABEL = "Signed for by";
    protected static final String COMMENTS_LABEL = "Comments";

    public OrderDetailsBase(Container container)
    {
        super(container);
    }

    public OrderDetailsBase(Container container, String itemId)
    {
        super(new Container(container, "Order Item Container", itemId,
                FindStepUtils.byXpathWithWait(String.format(".//div/table/tr/td/span[text()='%s']", itemId))));
    }

    public ActivityAmount getLoyaltyUnits()
    {
        return new ActivityAmount(container.getLabel("Loyalty Units"));
    }

    public Label getDeliveryMethod()
    {
        return container.getLabel("Delivery Method");
    }

    public CurrencyAmount getCash()
    {
        return new CurrencyAmount(container.getLabel("Cash"));
    }

    public Label getQuantity()
    {
        return container.getLabel("Quantity");
    }

    abstract public Label getItemName();

    abstract public <D extends Componentable & HavingDefaultValue> D getEstimatedDeliveryDate();

    abstract public <D extends Componentable & HavingDefaultValue> D getActualDeliveryDate();

    abstract public Component getDeliveryStatus();

    abstract public <I extends Componentable & HavingDefaultValue> I getTrackingUniqueNumber();

    abstract public Component getSignedForBy();

    abstract public Component getComments();

    public void verify(OrderItem orderItem, String unitType)
    {
        verifyThat(getItemName(), hasText(orderItem.getItemName()));

        getCash().verify(NOT_AVAILABLE, null);
        getLoyaltyUnits().verify(orderItem.getLoyaltyUnits(), unitType);

        verifyThat(getDeliveryMethod(), hasText(orderItem.getDeliveryMethod()));
        verifyThat(getDeliveryStatus(), hasText(orderItem.getDeliveryStatus()));

        verifyThat(getEstimatedDeliveryDate(), hasText(orderItem.getEstimatedDeliveryDate()));
        verifyThat(getActualDeliveryDate(), hasText(orderItem.getActualDeliveryDate()));

        verifyThat(getTrackingUniqueNumber(), hasText(orderItem.getTrackingUniqueNumber()));
        verifyThat(getQuantity(), hasText(orderItem.getQuantity()));

        verifyThat(getSignedForBy(), hasText(orderItem.getSignedForBy()));
        verifyThat(getComments(), hasText(orderItem.getComments()));
    }

    public void verify(OrderItem orderItem)
    {
        verify(orderItem, Constant.RC_POINTS);
    }

    public void verify(VoucherOrder voucherOrder)
    {
        verifyThat(getItemName(), hasText(voucherOrder.getVoucherName()));

        getCash().verify(voucherOrder.getVoucherCostAmount(), voucherOrder.getCurrencyCode());
        getLoyaltyUnits().verify(NOT_AVAILABLE, null);

        verifyThat(getEstimatedDeliveryDate(), hasDefault());
        verifyThat(getActualDeliveryDate(), hasDefault());

        verifyThat(getDeliveryMethod(), hasDefault());
        verifyThat(getDeliveryStatus(), hasText(voucherOrder.getStatus()));

        verifyThat(getTrackingUniqueNumber(), hasText(voucherOrder.getTrackingNumber()));
        verifyThat(getQuantity(), hasText(voucherOrder.getNumberOfVouchers()));

        verifyThat(getComments(), hasText(voucherOrder.getComments()));
        verifyThat(getSignedForBy(), hasText(voucherOrder.getSignedFor()));
    }
}
