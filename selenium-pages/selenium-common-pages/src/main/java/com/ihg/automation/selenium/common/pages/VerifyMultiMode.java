package com.ihg.automation.selenium.common.pages;

import com.ihg.automation.selenium.gwt.components.Mode;

public interface VerifyMultiMode<T>
{
    void verify(T obj, Mode mode);
}
