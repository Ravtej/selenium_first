package com.ihg.automation.selenium.common.annual;

import com.ihg.automation.selenium.common.earning.AmountFieldBase;
import com.ihg.automation.selenium.gwt.components.ComponentColumnPosition;
import com.ihg.automation.selenium.gwt.components.label.Label;

public class ActivityAmount extends AmountFieldBase
{
    private static final ComponentColumnPosition POSITION = ComponentColumnPosition.SINGLE_TD;

    public ActivityAmount(Label label)
    {
        super(label, POSITION);
    }
}
