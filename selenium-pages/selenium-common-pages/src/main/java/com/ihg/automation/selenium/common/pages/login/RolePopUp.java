package com.ihg.automation.selenium.common.pages.login;

import com.ihg.automation.selenium.common.pages.PopUpBase;
import com.ihg.automation.selenium.gwt.components.Button;
import com.ihg.automation.selenium.gwt.components.input.Select;

public class RolePopUp extends PopUpBase
{
    public RolePopUp()
    {
        super("Select");
    }

    public Select getRole()
    {
        return container.getSelectByLabel("Role");
    }

    protected Button getSubmit()
    {
        return container.getButton("Select");
    }

    public void clickSelect()
    {
        getSubmit().click();
    }

    public void selectAndSubmit(String role)
    {
        getRole().select(role);
        getSubmit().clickAndWait();
    }
}
