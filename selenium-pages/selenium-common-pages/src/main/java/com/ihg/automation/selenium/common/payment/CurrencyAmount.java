package com.ihg.automation.selenium.common.payment;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static com.ihg.automation.selenium.matchers.component.HasText.hasTextNoWait;
import static org.hamcrest.text.IsEmptyString.isEmptyOrNullString;

import org.hamcrest.Matcher;

import com.ihg.automation.selenium.common.Constant;
import com.ihg.automation.selenium.common.event.CurrencyAmountFieldsAware;
import com.ihg.automation.selenium.common.pages.LabelContainerBase;
import com.ihg.automation.selenium.gwt.components.Component;
import com.ihg.automation.selenium.gwt.components.ComponentColumnPosition;
import com.ihg.automation.selenium.gwt.components.Componentable;
import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.label.Label;

public class CurrencyAmount extends LabelContainerBase implements CurrencyAmountFieldsAware
{
    private static final ComponentColumnPosition POSITION = ComponentColumnPosition.SINGLE_TD;

    public CurrencyAmount(Container container)
    {
        super(container.getLabel("Amount"));
    }

    public CurrencyAmount(Label label)
    {
        super(label);
    }

    @Override
    public Component getAmount()
    {
        return label.getComponent("Amount", 1, POSITION);
    }

    @Override
    public Component getCurrency()
    {
        return label.getComponent("Currency", 2, POSITION);
    }

    public void verify(String amount, String currency)
    {
        verifyThat(getAmount(), hasText(amount));

        if (Constant.NOT_AVAILABLE.equals(amount))
        {
            verifyThat(getCurrency(), hasTextNoWait(isEmptyOrNullString()));
        }
        else
        {
            verifyThat(getCurrency(), hasText(currency));
        }
    }

    public void verify(Matcher<Componentable> amountMatcher, String currency)
    {
        verifyThat(getAmount(), amountMatcher);
        verifyThat(getCurrency(), hasText(currency));
    }
}
