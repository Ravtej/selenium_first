package com.ihg.automation.selenium.common.contact;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.ViewableMatcher.hasText;

import com.ihg.automation.selenium.common.member.GuestContact;
import com.ihg.automation.selenium.common.pages.Populate;
import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.CustomContainerBase;
import com.ihg.automation.selenium.gwt.components.Mode;
import com.ihg.automation.selenium.gwt.components.input.Select;

public abstract class ContactBase<T extends GuestContact> extends CustomContainerBase implements Populate<T>
{
    protected final String TYPE_LABEL = "Type";

    public ContactBase(Container parent)
    {
        super(parent);
    }

    public Select getType()
    {
        return container.getSelectByLabel(TYPE_LABEL);
    }

    @Override
    public void populate(T contact)
    {
        getType().selectByValue(contact.getType());
    }

    public void verify(T contact, Mode mode)
    {
        verifyThat(getType(), hasText(contact.getType().getValue(), mode));
    }
}
