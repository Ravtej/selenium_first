package com.ihg.automation.selenium.common.business;

import java.util.ArrayList;
import java.util.Arrays;

public class BusinessStatus
{
    public static final String FUTURE = "Future";
    public static final String IN_PROGRESS = "In Progress";
    public static final String PENDING = "Pending";
    public static final String POSTED = "Posted";
    public static final String CANCELED = "Canceled";
    public static final String BLOCKED = "Blocked";

    public static ArrayList<String> BUSINESS_STATUS_LIST = new ArrayList<>(
            Arrays.asList(FUTURE, IN_PROGRESS, PENDING, POSTED, CANCELED, BLOCKED));

    public static ArrayList<String> BUSINESS_MANAGED_STATUS_LIST = new ArrayList<>(
            Arrays.asList(FUTURE, IN_PROGRESS, PENDING, CANCELED, BLOCKED));

    private BusinessStatus()
    {
    }
}
