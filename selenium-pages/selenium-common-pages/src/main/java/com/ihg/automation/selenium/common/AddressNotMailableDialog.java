package com.ihg.automation.selenium.common;

import com.ihg.automation.selenium.gwt.components.dialog.DialogBase;
import com.ihg.automation.selenium.gwt.components.utils.ByText;
import com.ihg.automation.selenium.listener.Verifier;

public class AddressNotMailableDialog extends DialogBase
{
    public AddressNotMailableDialog()
    {
        super(Type.DEFAULT, ByText.containsSelf("Address is not mailable"));
    }

    public void clickSaveAsIs(Verifier... verifiers)
    {
        container.getButton("Save address as is").clickAndWait(verifiers);
    }

    public void clickContinue()
    {
        container.getButton("Continue address editing").click();
    }

}
