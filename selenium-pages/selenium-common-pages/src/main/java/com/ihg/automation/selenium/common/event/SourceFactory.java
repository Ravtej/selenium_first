package com.ihg.automation.selenium.common.event;

import static com.ihg.automation.selenium.common.Constant.NOT_AVAILABLE;

import com.ihg.automation.selenium.common.Channel;
import com.ihg.automation.selenium.common.UI;
import com.ihg.automation.selenium.common.User;

public class SourceFactory
{
    private User user;
    private UI ui;

    public SourceFactory(User user, UI ui)
    {
        this.user = user;
        this.ui = ui;
    }

    public Source getSource()
    {
        return new Source(ui.getChannel(), user.getLocation(), ui.getType(), user.getEmployeeId(), user.getNameFull());
    }

    public Source getNoEmployeeIdSource()
    {
        return new Source(ui.getChannel(), user.getLocation(), ui.getType(), NOT_AVAILABLE, user.getNameFull());
    }

    public Source getRulesSource()
    {
        return new Source(Channel.RULES, user.getLocation(), ui.getType(), NOT_AVAILABLE, user.getNameFull());
    }

    public Source getOrderSource()
    {
        return new Source(ui.getChannel(), user.getLocation(), ui.getType(), NOT_AVAILABLE, NOT_AVAILABLE);
    }
}
