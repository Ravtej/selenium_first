package com.ihg.automation.selenium.common.types.program;

import java.util.ArrayList;

public enum RewardClubLevel implements ProgramLevel
{
    CLUB("CLUB", "CLUB"), GOLD("GOLD", "GOLD"), PLTN("PLTN", "PLATINUM"), SPRE("SPRE", "SPIRE");

    private String code;
    private String value;

    private RewardClubLevel(String code, String description)
    {
        this.code = code;
        this.value = description;
    }

    @Override
    public String getCode()
    {
        return code;
    }

    @Override
    public String getValue()
    {
        return value;
    }

    @Override
    public String getCodeWithValue()
    {
        return code + " - " + value;
    }

    public static ArrayList<String> getValuesList()
    {
        RewardClubLevel[] vals = values();
        ArrayList<String> res = new ArrayList<String>();
        for (RewardClubLevel val : vals)
        {
            res.add(val.getValue());
        }
        return res;
    }

    public static ArrayList<String> getDataList()
    {
        RewardClubLevel[] vals = values();
        ArrayList<String> res = new ArrayList<String>();
        for (RewardClubLevel val : vals)
        {
            res.add(val.getCodeWithValue());
        }

        return res;
    }

}
