package com.ihg.automation.selenium.common.member;

import static com.ihg.automation.selenium.common.catalog.CatalogItem.builder;
import static com.ihg.automation.selenium.common.types.Currency.AUD;
import static com.ihg.automation.selenium.common.types.Currency.CNY;
import static com.ihg.automation.selenium.common.types.Currency.USD;

import java.util.ArrayList;
import java.util.Arrays;

import com.ihg.automation.selenium.common.catalog.CatalogItem;

public class DiningRewardAward
{
    public static final CatalogItem FUL_WELCOME_TO_GOLD = builder("GLDNH", "Welcome to Gold IDR - Non Hotel")
            .itemDescription("Welcome to Gold IDR - Full Kit").awardCost("0", USD).buyerCost("0", USD).build();
    public static final CatalogItem FUL_WELCOME_TO_VIP_GOLD = builder("VGIDR", "Welcome to VIP Gold IDR")
            .itemDescription("Welcome to VIP Gold IDR - Full Kit").awardCost("0", USD).buyerCost("0", USD).build();
    public static final CatalogItem FUL_WELCOME_TO_PLATINUM = builder("PLTNH", "Welcome to Platinum IDR - X Hotel")
            .itemDescription("Welcome to Platinum IDR - Full Kit").awardCost("0", USD).buyerCost("0", USD).build();
    public static final CatalogItem GOLD_TO_PLAT_RENEWAL_NON_HOTEL = builder("GRTNH",
            "Gold to Plat Renewal Kit (Non Hotel)").itemDescription("Gold to Plat Renewal Kit (Non Hotel)")
                    .awardCost("0", USD).buyerCost("0", USD).build();

    // Enroll awards
    public static final CatalogItem ENROLL_COMPLIMENTARY = builder("CPAFC", "IHGDR Enrollment - Complimentary")
            .awardCost("0", USD).buyerCost("0", USD).build();

    public static final CatalogItem ENROLL_100000_POINTS_GC = builder("PPAFC", "IHGDR Enrollment - 100,000 Points - GC")
            .loyaltyUnitCost("100000").awardCost("0", USD).buyerCost("385", USD).build();

    public static final CatalogItem ENROLL_RMB2388 = builder("DPAFCS", "IHGDR Enrollment - RMB2388 - SC")
            .itemDescription("IHGDR Enrollment - RMB2388").awardCost("385", USD).buyerCost("2388", CNY).build();

    public static final CatalogItem ENROLL_100000_POINTS_GLOBAL = builder("PPAFGC",
            "IHGDR Enrollment - 100,000 Points Global").loyaltyUnitCost("100000").awardCost("385", USD)
                    .buyerCost("0", USD).build();

    public static final CatalogItem ENROLL_GLOBAL_PRICE_385USD_SC_AND_WEB = builder("DPAFXCS",
            "IHGDR Enrollment - Global Price $385USD - SC & WEB").awardCost("385", USD).buyerCost("385", USD).build();

    public static final CatalogItem ENROLL_75000_POINTS = builder("PPAFA", "IHGDR Enrollment - 75,000 Points")
            .loyaltyUnitCost("75000").awardCost("300", USD).buyerCost("0", USD).build();

    public static final CatalogItem ENROLL_40AUD_OFF_279AUD_13_MTH_2_EVOUCHER = builder("DPAFA2V13",
            "IHGDR Enrollment - 40AUD Off - 279AUD 13 MTH 2 eVoucher - Australia").awardCost("213", USD)
                    .buyerCost("279", AUD).build();

    public static final CatalogItem ENROLL_10_RMB2188_SC_2_EVOUCHER = builder("DPAFCS2EC",
            "IHGDR Enrollment - 10% - RMB2188 SC 2 eVoucher").awardCost("352", USD).buyerCost("2188", CNY).build();

    public static final CatalogItem ENROLL_RMB2388_SC_2_EVOUCHER = builder("DPAFCS2E",
            "IHGDR Enrollment - RMB 2388 - SC 2 eVoucher").awardCost("385", USD).buyerCost("2388", CNY).build();

    public static final CatalogItem ENROLL_20_RMB1988_BIG_SALE = builder("DPAFC20", "IHGDR Enrollment - 20% - RMB1988")
            .awardCost("320", USD).buyerCost("1988", CNY).build();

    public static final CatalogItem ENROLL_10_RMB2188 = builder("DPAFC10", "IHGDR Enrollment - 10% - RMB2188")
            .awardCost("352", USD).buyerCost("2188", CNY).build();

    // Renew awards
    public static final CatalogItem RENEW_90000_POINTS_SC = builder("PPAFCRN", "IHGDR Renewal - 90,000 POINTS - SC")
            .loyaltyUnitCost("90000").awardCost("360", USD).buyerCost("0", USD).build();

    public static final CatalogItem RENEW_RMB2188_SC = builder("DPAFCSRN", "IHGDR Renewal - RMB2188 - SC")
            .awardCost("353", USD).buyerCost("2188", CNY).build();

    public static final CatalogItem RENEW_EMP_270USD = builder("EPAFCR", "IHGDR Renewal- Emp - 270 USD")
            .awardCost("270", USD).buyerCost("270", USD).build();

    public static ArrayList<String> renewPaymentAmountListForChina = new ArrayList<String>(
            Arrays.asList(RENEW_90000_POINTS_SC.getItemDescription(), RENEW_RMB2188_SC.getItemDescription(),
                    RENEW_EMP_270USD.getItemDescription()));

}
