package com.ihg.automation.selenium.common.pages.program;

import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.CustomContainerBase;
import com.ihg.automation.selenium.gwt.components.label.Label;

public class PointGridRowContainer extends CustomContainerBase
{
    public PointGridRowContainer(Container parent)
    {
        super(parent);
    }

    public Label getTotalQualifyingPoints()
    {
        return container.getLabel("Total Tier-Level Qualifying Points");
    }
}
