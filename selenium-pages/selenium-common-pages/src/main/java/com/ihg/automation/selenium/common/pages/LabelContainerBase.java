package com.ihg.automation.selenium.common.pages;

import com.ihg.automation.selenium.gwt.components.MultiModeComponent;
import com.ihg.automation.selenium.gwt.components.Viewable;
import com.ihg.automation.selenium.gwt.components.label.Label;

public abstract class LabelContainerBase implements MultiModeComponent, Viewable
{
    protected Label label;

    public LabelContainerBase(Label label)
    {
        this.label = label;
    }

    @Override
    public Label getView()
    {
        return label;
    }
}
