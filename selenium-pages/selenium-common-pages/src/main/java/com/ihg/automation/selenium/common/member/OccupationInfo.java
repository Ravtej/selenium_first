package com.ihg.automation.selenium.common.member;

import org.apache.commons.lang3.StringUtils;

public class OccupationInfo
{
    private String corporateNumber;
    private String corporateName = StringUtils.EMPTY;
    private String jobTitle;
    private String companyName;

    public OccupationInfo(String corporateNumber, String corporateName)
    {
        this.corporateNumber = corporateNumber;
        this.corporateName = corporateName;
    }

    public String getCorporateNumber()
    {
        return corporateNumber;
    }

    public void setCorporateNumber(String corporateNumber)
    {
        this.corporateNumber = corporateNumber;
    }

    public String getCorporateName()
    {
        return corporateName;
    }

    public void setCorporateName(String corporateName)
    {
        this.corporateName = corporateName;
    }

    public String getJobTitle()
    {
        return jobTitle;
    }

    public void setJobTitle(String jobTitle)
    {
        this.jobTitle = jobTitle;
    }

    public String getCompanyName()
    {
        return companyName;
    }

    public void setCompanyName(String companyName)
    {
        this.companyName = companyName;
    }

}
