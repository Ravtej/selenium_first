package com.ihg.automation.selenium.common.member;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Set;

import com.ihg.automation.selenium.common.CoBrandedCreditCard;
import com.ihg.automation.selenium.common.catalog.CatalogItem;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.common.types.program.TierLevelFlag;

public class Member
{
    private PersonalInfo personalInfo = new PersonalInfo();
    private List<GuestName> names = new ArrayList<GuestName>();
    private List<GuestPhone> phones = new ArrayList<GuestPhone>();
    private List<GuestEmail> emails = new ArrayList<GuestEmail>();
    private List<GuestAddress> addresses = new ArrayList<GuestAddress>();
    private MemberAlliance milesEarningPreference;
    private LinkedHashMap<Program, String> programs = new LinkedHashMap<Program, String>();
    private Set<String> flags = new HashSet<String>();
    private OccupationInfo occupationInfo;
    private LifetimeActivity lifetimeActivity;
    private AnnualActivity annualActivity;
    private AmbassadorOfferCode ambassadorOfferCode;
    private String rewardClubOfferCode;
    private DiningRewardOfferCode diningRewardOfferCode;
    private CatalogItem ambassadorAmount;
    private String rewardClubReferredBy;
    private CoBrandedCreditCard coBrandedCreditCard;

    public LinkedHashMap<Program, String> getPrograms()
    {
        return programs;
    }

    public void addProgram(Program... programs)
    {
        for (Program program : programs)
        {
            this.programs.put(program, null);
        }
    }

    public PersonalInfo getPersonalInfo()
    {
        return personalInfo;
    }

    public void setPersonalInfo(PersonalInfo personalInfo)
    {
        this.personalInfo = personalInfo;
    }

    public List<GuestName> getNames()
    {
        return names;
    }

    public GuestName getName()
    {
        return personalInfo.getName();
    }

    public void addName(GuestName name)
    {
        names.add(name);
    }

    public List<GuestEmail> getEmails()
    {
        return emails;
    }

    public void addEmail(GuestEmail email)
    {
        emails.add(email);
    }

    public List<GuestAddress> getAddresses()
    {
        return addresses;
    }

    public void addAddress(GuestAddress address)
    {
        addresses.add(address);
    }

    public List<GuestPhone> getPhones()
    {
        return phones;
    }

    public void addPhone(GuestPhone phone)
    {
        phones.add(phone);
    }

    public MemberAlliance getMilesEarningPreference()
    {
        return milesEarningPreference;
    }

    public void setMilesEarningPreference(MemberAlliance milesEarningPreference)
    {
        this.milesEarningPreference = milesEarningPreference;
    }

    public Set<String> getFlags()
    {
        return flags;
    }

    public void addFlag(String flag)
    {
        flags.add(flag);
    }

    public void addFlag(TierLevelFlag flag)
    {
        flags.add(flag.getValue());
    }

    public void addFlags(String... flags)
    {
        this.flags.addAll(Arrays.asList(flags));
    }

    public void addFlags(Collection<String> flags)
    {
        this.flags.addAll(flags);
    }

    public OccupationInfo getOccupationInfo()
    {
        return occupationInfo;
    }

    public void setOccupationInfo(OccupationInfo occupationInfo)
    {
        this.occupationInfo = occupationInfo;
    }

    public GuestPhone getPreferredPhone()
    {
        return getPreferred(getPhones());
    }

    public GuestEmail getPreferredEmail()
    {
        return getPreferred(getEmails());
    }

    public GuestAddress getPreferredAddress()
    {
        return getPreferred(getAddresses());
    }

    private <E extends GuestContact> E getPreferred(List<E> contactList)
    {
        E res = null;
        if (null != contactList)
        {
            if (contactList.size() == 1)
            {
                res = contactList.get(0);
            }
            else
            {
                for (E contact : contactList)
                {
                    if (contact.isPreferred())
                    {
                        res = contact;
                        break;
                    }
                }
            }
        }
        return res;
    }

    public void setLifetimeActivity(LifetimeActivity lifetimeActivity)
    {
        this.lifetimeActivity = lifetimeActivity;
    }

    public LifetimeActivity getLifetimeActivity()
    {
        return lifetimeActivity;
    }

    public void setAnnualActivity(AnnualActivity annualActivity)
    {
        this.annualActivity = annualActivity;
    }

    public AnnualActivity getAnnualActivity()
    {
        return annualActivity;
    }

    public String getRCProgramId()
    {
        return getPrograms().get(Program.RC);
    }

    public String getEMPProgramId()
    {
        return getPrograms().get(Program.EMP);
    }

    public String getAMBProgramId()
    {
        return getPrograms().get(Program.AMB);
    }

    public String getBRProgramId()
    {
        return getPrograms().get(Program.BR);
    }

    public String getKARProgramId()
    {
        return getPrograms().get(Program.KAR);
    }

    public String getDRProgramId()
    {
        return getPrograms().get(Program.DR);
    }

    public AmbassadorOfferCode getAmbassadorOfferCode()
    {
        return ambassadorOfferCode;
    }

    public void setAmbassadorOfferCode(AmbassadorOfferCode ambOfferCode)
    {
        this.ambassadorOfferCode = ambOfferCode;
    }

    public CatalogItem getAmbassadorAmount()
    {
        return ambassadorAmount;
    }

    public void setAmbassadorAmount(CatalogItem ambAmount)
    {
        this.ambassadorAmount = ambAmount;
    }

    public String getRewardClubOfferCode()
    {
        return rewardClubOfferCode;
    }

    public void setRewardClubOfferCode(String rewardClubOfferCode)
    {
        this.rewardClubOfferCode = rewardClubOfferCode;
    }

    public String getRewardClubReferredBy()
    {
        return rewardClubReferredBy;
    }

    public void setRewardClubReferredBy(String rewardClubReferredBy)
    {
        this.rewardClubReferredBy = rewardClubReferredBy;
    }

    public DiningRewardOfferCode getDiningRewardOfferCode()
    {
        return diningRewardOfferCode;
    }

    public void setDiningRewardOfferCode(DiningRewardOfferCode diningRewardOfferCode)
    {
        this.diningRewardOfferCode = diningRewardOfferCode;
    }

    public CoBrandedCreditCard getCoBrandedCreditCard()
    {
        return coBrandedCreditCard;
    }

    public void setCoBrandedCreditCard(CoBrandedCreditCard coBrandedCreditCard)
    {
        this.coBrandedCreditCard = coBrandedCreditCard;
    }
}
