package com.ihg.automation.selenium.common.enroll;

import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.CustomContainerBase;
import com.ihg.automation.selenium.gwt.components.input.Input;

public class AdditionalInformation extends CustomContainerBase
{
    public AdditionalInformation(Container container)
    {
        super(container);
    }

    public Input getEMPMemberID()
    {
        return container.getInputByLabel(Program.EMP.getCode() + " Member ID (if known)");
    }

    public Input getRCMemberID()
    {
        return container.getInputByLabel(Program.RC.getCode() + " Member ID (if known)");
    }
}
