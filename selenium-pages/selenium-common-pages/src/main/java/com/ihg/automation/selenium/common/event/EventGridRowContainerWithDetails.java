package com.ihg.automation.selenium.common.event;

import com.ihg.automation.selenium.gwt.components.Button;
import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.CustomContainerBase;
import com.ihg.automation.selenium.gwt.components.Link;
import com.ihg.automation.selenium.listener.Verifier;

public class EventGridRowContainerWithDetails extends CustomContainerBase
{
    private static final String DETAILS_BUTTON = "Details";

    public EventGridRowContainerWithDetails(Container parent)
    {
        super(parent);
    }

    public Button getDetails()
    {
        return new Button(container, DETAILS_BUTTON);
    }

    public Link getEventDetails()
    {
        return new Link(container, "Event Details");
    }

    public void clickDetails(Verifier... verifiers)
    {
        getDetails().clickAndWait(verifiers);
    }
}
