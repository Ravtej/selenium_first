package com.ihg.automation.selenium.common.pages;

import com.ihg.automation.selenium.gwt.components.EntityType;

public enum PaymentType implements EntityType
{
    CASH("CA", "Cash"), //
    CREDIT_CARD("CC", "Credit Card"), //
    CHECK("CK", "Check"), //
    BILL("DB", "Direct bill"), //
    NO_CHARGE("NC", "No Charge"), //
    POSTING_MASTER("PM", "Posting Master");

    private String code;
    private String value;

    private PaymentType(String code, String value)
    {
        this.code = code;
        this.value = value;
    }

    @Override
    public String getCode()
    {
        return code;
    }

    @Override
    public String getValue()
    {
        return value;
    }

    @Override
    public String toString()
    {
        return getValue();
    }

    @Override
    public String getCodeWithValue()
    {
        return code + " - " + value;
    }
}
