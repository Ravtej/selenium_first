package com.ihg.automation.selenium.common.catalog;

import java.util.List;

import com.google.common.collect.ImmutableList;
import com.ihg.automation.selenium.common.Constant;
import com.ihg.automation.selenium.common.types.Currency;

public class CatalogItem
{
    private String itemId;
    private String itemName;
    private String itemDescription;
    private String vendor;
    private String deliveryMethod;
    private String estimatedDeliveryTime;
    private String startDate;
    private String endDate;
    private String status;
    private String orderLimit;

    private ImmutableList<String> regions;
    private ImmutableList<String> countries;
    private ImmutableList<String> tierLevels;
    private ImmutableList<String> colorList;
    private ImmutableList<String> sizeList;

    private String program;
    private String catalogId;

    private String loyaltyUnitCost;

    private String buyerCost;
    private String buyerCurrency;

    private String awardCost;
    private String awardCostCurrency;

    private CatalogItem(Builder builder)
    {
        itemId = builder.itemId;
        itemName = builder.itemName;
        itemDescription = builder.itemDescription;
        vendor = builder.vendor;
        deliveryMethod = builder.deliveryMethod;
        estimatedDeliveryTime = builder.estimatedDeliveryTime;
        startDate = builder.startDate;
        endDate = builder.endDate;
        status = builder.status;
        orderLimit = builder.orderLimit;
        regions = builder.regions;
        countries = builder.countries;
        tierLevels = builder.tierLevels;
        colorList = builder.colorList;
        sizeList = builder.sizeList;
        program = builder.program;
        catalogId = builder.catalogId;
        loyaltyUnitCost = builder.loyaltyUnitCost;
        buyerCost = builder.buyerCost;
        awardCost = builder.awardCost;
        buyerCurrency = builder.buyerCurrency;
        awardCostCurrency = builder.awardCostCurrency;
    }

    public static Builder builder()
    {
        return new Builder();
    }

    public static Builder builder(String itemId, String itemName)
    {
        return new Builder(itemId, itemName);
    }

    public String getItemId()
    {
        return itemId;
    }

    public String getItemName()
    {
        return itemName;
    }

    public String getItemDescription()
    {
        return itemDescription;
    }

    public String getVendor()
    {
        return vendor;
    }

    public String getDeliveryMethod()
    {
        return deliveryMethod;
    }

    public String getEstimatedDeliveryTime()
    {
        return estimatedDeliveryTime;
    }

    public String getStartDate()
    {
        return startDate;
    }

    public String getEndDate()
    {
        return endDate;
    }

    public String getStatus()
    {
        return status;
    }

    public String getOrderLimit()
    {
        return orderLimit;
    }

    public List<String> getRegions()
    {
        return regions;
    }

    public List<String> getCountries()
    {
        return countries;
    }

    public List<String> getTierLevels()
    {
        return tierLevels;
    }

    public List<String> getColorList()
    {
        return colorList;
    }

    public List<String> getSizeList()
    {
        return sizeList;
    }

    public String getProgram()
    {
        return program;
    }

    public String getCatalogId()
    {
        return catalogId;
    }

    public String getLoyaltyUnitCost()
    {
        return loyaltyUnitCost;
    }

    public String getBuyerCost()
    {
        return buyerCost;
    }

    public String getAwardCost()
    {
        return awardCost;
    }

    public String getBuyerCurrency()
    {
        return buyerCurrency;
    }

    public String getAwardCostCurrency()
    {
        return awardCostCurrency;
    }

    public static final class Builder
    {
        private String itemId;
        private String itemName;
        private String itemDescription;
        private String vendor = Constant.NOT_AVAILABLE;
        private String deliveryMethod = Constant.NOT_AVAILABLE;
        private String estimatedDeliveryTime = Constant.NOT_AVAILABLE;
        private String startDate = Constant.NOT_AVAILABLE;
        private String endDate = Constant.NOT_AVAILABLE;
        private String status = Constant.NOT_AVAILABLE;
        private String orderLimit = Constant.NOT_AVAILABLE;

        private ImmutableList<String> regions;
        private ImmutableList<String> countries;
        private ImmutableList<String> tierLevels;
        private ImmutableList<String> colorList;
        private ImmutableList<String> sizeList;

        private String program;
        private String catalogId;
        private String loyaltyUnitCost;

        private String buyerCost;
        private String buyerCurrency;

        private String awardCost;
        private String awardCostCurrency;

        private Builder()
        {
        }

        private Builder(String itemId, String itemName)
        {
            this.itemId = itemId;
            this.itemName = itemName;
            // Default assumption
            this.itemDescription = itemName;
        }

        public Builder itemId(String itemId)
        {
            this.itemId = itemId;
            return this;
        }

        public Builder itemName(String itemName)
        {
            this.itemName = itemName;
            return this;
        }

        public Builder itemDescription(String itemDescription)
        {
            this.itemDescription = itemDescription;
            return this;
        }

        public Builder vendor(String vendor)
        {
            this.vendor = vendor;
            return this;
        }

        public Builder deliveryMethod(String deliveryMethod)
        {
            this.deliveryMethod = deliveryMethod;
            return this;
        }

        public Builder estimatedDeliveryTime(String estimatedDeliveryTime)
        {
            this.estimatedDeliveryTime = estimatedDeliveryTime;
            return this;
        }

        public Builder startDate(String startDate)
        {
            this.startDate = startDate;
            return this;
        }

        public Builder endDate(String endDate)
        {
            this.endDate = endDate;
            return this;
        }

        public Builder status(String status)
        {
            this.status = status;
            return this;
        }

        public Builder orderLimit(String orderLimit)
        {
            this.orderLimit = orderLimit;
            return this;
        }

        public Builder regions(ImmutableList<String> regions)
        {
            this.regions = regions;
            return this;
        }

        public Builder countries(ImmutableList<String> countries)
        {
            this.countries = countries;
            return this;
        }

        public Builder tierLevels(ImmutableList<String> tierLevels)
        {
            this.tierLevels = tierLevels;
            return this;
        }

        public Builder colorList(ImmutableList<String> colorList)
        {
            this.colorList = colorList;
            return this;
        }

        public Builder sizeList(ImmutableList<String> sizeList)
        {
            this.sizeList = sizeList;
            return this;
        }

        public Builder program(String program)
        {
            this.program = program;
            return this;
        }

        public Builder catalogId(String catalogId)
        {
            this.catalogId = catalogId;
            return this;
        }

        public Builder loyaltyUnitCost(String loyaltyUnitCost)
        {
            this.loyaltyUnitCost = loyaltyUnitCost;
            return this;
        }

        public Builder buyerCost(String buyerCost, Currency buyerCurrency)
        {
            this.buyerCost = buyerCost;
            this.buyerCurrency = buyerCurrency.getCode();
            return this;
        }

        public Builder awardCost(String awardCost, Currency awardCostCurrency)
        {
            this.awardCost = awardCost;
            this.awardCostCurrency = awardCostCurrency.getCode();
            return this;
        }

        public CatalogItem build()
        {
            return new CatalogItem(this);
        }
    }
}
