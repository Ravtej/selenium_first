package com.ihg.automation.selenium.common.business;

import com.ihg.automation.selenium.common.billing.EventBillingDetailsTab;
import com.ihg.automation.selenium.common.billing.EventBillingTabAware;
import com.ihg.automation.selenium.common.earning.EventEarningDetailsTab;
import com.ihg.automation.selenium.common.earning.EventEarningTabAware;
import com.ihg.automation.selenium.common.history.EventHistoryTab;
import com.ihg.automation.selenium.common.history.EventHistoryTabAware;
import com.ihg.automation.selenium.common.pages.SavePopUpBase;
import com.ihg.automation.selenium.gwt.components.Button;
import com.ihg.automation.selenium.listener.Verifier;

public class BusinessRewardsEventDetailsPopUp extends SavePopUpBase
        implements EventEarningTabAware, EventBillingTabAware, EventHistoryTabAware
{
    public static final String CREATE_EVENT_SUCCESS = "IHG Business Rewards Event has been successfully created. Member is Pending and will not earn Points. Member must agree to Terms and Conditions.";
    public static final String UPDATE_EVENT_SUCCESS = "IHG Business Rewards Event has been successfully updated. Member is Pending and will not earn Points. Member must agree to Terms and Conditions.";

    public BusinessRewardsEventDetailsPopUp()
    {
        super("IHG Business Rewards Event Details");
    }

    public EventSummaryTab getEventSummaryTab()
    {
        return new EventSummaryTab(this);
    }

    public RoomsTab getRoomsTab()
    {
        return new RoomsTab(this);
    }

    @Override
    public EventEarningDetailsTab getEarningDetailsTab()
    {
        return new EventEarningDetailsTab(this);
    }

    @Override
    public EventBillingDetailsTab getBillingDetailsTab()
    {
        return new EventBillingDetailsTab(this);
    }

    @Override
    public EventHistoryTab getHistoryTab()
    {
        return new EventHistoryTab(this, "History");
    }

    public Button getCalculateEstimates()
    {
        return container.getButton("Calculate Estimates");
    }

    public void clickCalculateEstimates(Verifier... verifiers)
    {
        getCalculateEstimates().clickAndWait(verifiers);
    }

    public Button getCancel()
    {
        return container.getButton(Button.CANCEL);
    }

    public void clickCancel(Verifier... verifiers)
    {
        getCancel().clickAndWait(verifiers);
    }
}
