package com.ihg.automation.selenium.common.communication;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.CustomContainerBase;

public class ContactPermissionItems extends CustomContainerBase
{
    public ContactPermissionItems(Container container)
    {
        super(container);
    }

    public ContactPermissionItem getContactPermissionItem(int rowNumber)
    {
        return new ContactPermissionItem(container, rowNumber);
    }

    protected List<WebElement> getItems()
    {
        return findElements(By.xpath("./div/div/div/div"), "ContactPermissionItems list");
    }

    public int getContactPermissionItemsCount()
    {
        return getItems().size();
    }

    public ContactPermissionItem getContactPermissionItem(Communication communication)
    {
        return getContactPermissionItem(communication.getValue());
    }

    public ContactPermissionItem getContactPermissionItem(String item)
    {
        return new ContactPermissionItem(container, item);
    }

    public List<ContactPermissionItem> getContactPermissionItemsList()
    {
        List<ContactPermissionItem> contactPermissionItemsList = new ArrayList<ContactPermissionItem>();

        for (String comm : Communication.getValueList())
        {
            contactPermissionItemsList.add(getContactPermissionItem(comm));
        }
        return contactPermissionItemsList;
    }

}
