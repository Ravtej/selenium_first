package com.ihg.automation.selenium.common.communication;

import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.CustomContainerBase;
import com.ihg.automation.selenium.gwt.components.Image;
import com.ihg.automation.selenium.gwt.components.Link;
import com.ihg.automation.selenium.gwt.components.input.Select;
import com.ihg.automation.selenium.gwt.components.label.Label;

public class MarketingPreferences extends CustomContainerBase
{
    public MarketingPreferences(Container container)
    {
        super(container.getSeparator("Email Marketing Preferences"));
    }

    public Link getSubscribeAllLink()
    {
        return container.getLink("Subscribe All");
    }

    public Link getUnSubscribeAllLink()
    {
        return container.getLink("Unsubscribe All");
    }

    public Select getEmailAddress()
    {
        return container.getSelectByLabel("Email Address");
    }

    public Label getEmailAddressView()
    {
        return container.getLabel("Email Address");
    }

    public Image getInvalidEmailImage()
    {
        return getEmailAddressView().getImage("Not Valid Image");
    }

    public ContactPermissionItems getContactPermissionItems()
    {
        return new ContactPermissionItems(container);
    }
}
