package com.ihg.automation.selenium.common.types.name;

import java.util.List;

public class NameConfigurationBean
{
    private GivenLabel given;
    private MiddleLabel middle;
    private SurnameLabel surname;
    private SalutationLabel salutation;
    private TitleLabel title;
    private DegreeLabel degree;
    private List<String> order;

    NameConfigurationBean()
    {
    }

    public GivenLabel getGiven()
    {
        return given;
    }

    public void setGiven(GivenLabel given)
    {
        this.given = given;
    }

    public MiddleLabel getMiddle()
    {
        return middle;
    }

    public void setMiddle(MiddleLabel middle)
    {
        this.middle = middle;
    }

    public SurnameLabel getSurname()
    {
        return surname;
    }

    public void setSurname(SurnameLabel surname)
    {
        this.surname = surname;
    }

    public SalutationLabel getSalutation()
    {
        return salutation;
    }

    public void setSalutation(SalutationLabel salutation)
    {
        this.salutation = salutation;
    }

    public TitleLabel getTitle()
    {
        return title;
    }

    public void setTitle(TitleLabel title)
    {
        this.title = title;
    }

    public DegreeLabel getDegree()
    {
        return degree;
    }

    public void setDegree(DegreeLabel degree)
    {
        this.degree = degree;
    }

    public List<String> getOrder()
    {
        return order;
    }

    public void setOrder(List<String> order)
    {
        this.order = order;
    }

}
