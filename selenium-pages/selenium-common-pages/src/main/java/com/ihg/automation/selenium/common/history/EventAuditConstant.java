package com.ihg.automation.selenium.common.history;

public class EventAuditConstant
{
    public static final String YES = "Yes";
    public static final String NO = "No";
    public static final double ZERO_VALUE = 0.00;
    public static final String CREATED = "CREATED";
    public static final String UPDATE = "UPDATE";
    public static final String CREATE = "CREATE";

    public static class Stay
    {
        public static final String AVERAGE_ROOM_RATE = "Average Room Rate";
        public static final String CHECK_IN = "Check In";
        public static final String CHECK_OUT = "Check Out";
        public static final String CURRENCY = "Currency";
        public static final String EARNING_PREFERENCE = "Earning Preference";
        public static final String ENROLLING_STAY_INDICATOR = "Enrolling Stay Indicator";
        public static final String EVENT_TYPE = "Event Type";
        public static final String FOLIO_NUMBER = "Folio Number";
        public static final String HOTEL = "Hotel";
        public static final String NIGHTS = "Nights";
        public static final String OVERLAPPING_INDICATOR = "Overlapping Indicator";
        public static final String QUALIFYING_NIGHTS = "Qualifying Nights";
        public static final String RATE_CODE = "Rate Code";
        public static final String ROOM_TYPE = "Room Type";
        public static final String TRANSACTION_DATE = "Transaction Date";
        public static final String ROOM_NUMBER = "Room Number";

        public static class Guest
        {
            public static final String FIRST_NAME = "First Name";
            public static final String LAST_NAME = "Last Name";
            public static final String MEMBERSHIP_ID = "Membership ID";
            public static final String GUEST_BY_EID = "Guest (by EID)";
            public static final String GUEST_BY_MASTER_KEY = "Guest (by master key)";
        }

        public static class Booking
        {
            public static final String SOURCE = "Booking Source";
            public static final String DATA_SOURCE = "Data Source";
            public static final String PAYMENT_TYPE_CODE = "Payment Type Code";
            public static final String CREDIT_CARD_TYPE = "Credit Card Type";
        }

        public static class Revenue
        {
            public static final String TOTAL_ROOM = "Total Room";
            public static final String FOOD = "Food";
            public static final String BEVERAGE = "Beverage";
            public static final String PHONE = "Phone";
            public static final String MEETING = "Meeting";
            public static final String MISCELLANEOUS_MANDATORY_LOYALTY_REVENUE = "Miscellaneous MANDATORY Loyalty Revenue";
            public static final String MISCELLANEOUS_OTHER_LOYALTY_REVENUE = "Miscellaneous OTHER Loyalty Revenue";
            public static final String MISCELLANEOUS_NO_LOYALTY_REVENUE = "Miscellaneous NO Loyalty Revenue";
            public static final String ROOM_TAX_FEE_NON_REVENUE = "Room Tax/Fee/Non-Revenue";
        }
    }

    public static class Order
    {
        public static final String STATUS = "Status";
    }

    public static class RewardNight
    {
        public static final String CERTIFICATE_NUMBER = "Certificate Number";
        public static final String CHECKIN_DATE = "Checkin Date";
        public static final String CHECKOUT_DATE = "Checkout Date";
    }

    public static class BusinessReward
    {
        public static final String START_DATE = "Start Date";
        public static final String END_DATE = "End Date";
        public static final String ESTIMATED_BILLING_AMOUNT = "Estimated Billing Amount";
        public static final String ESTIMATED_CURRENCY = "Estimated Currency";

        public static class Event
        {
            public static final String EVENT_ID = "Event ID";
            public static final String EVENT_NAME = "Event Name";
            public static final String EVENT_STATUS = "Event Status";
            public static final String MEMBER_NUMBER = "Member Number";
            public static final String HOTEL = "Hotel";
            public static final String HOTEL_CURRENCY = "Hotel Currency";
        }

        public static class Meeting
        {
            public static final String NUMBER_OF_MEETING_ROOMS = "Number of Meeting Rooms";
            public static final String TOTAL_DELEGATES = "Total Delegates";
            public static final String MEETING_ROOM = "Meeting Room";
            public static final String FOOD_BEVERAGE = "Food & Beverage";
            public static final String MISCELLANEOUS = "Miscellaneous";
        }

        public static class Discretionary
        {
            public static final String POINTS = "Points";
            public static final String REASON = "Reason";
        }

        public static class Room
        {
            public static final String STAY_START_DATE = "Stay Start Date";
            public static final String STAY_END_DATE = "Stay End Date";
            public static final String TOTAL_GUEST_ROOMS = "Total Guest Rooms";
            public static final String TOTAL_ROOM_NIGHTS = "Total Room Nights";
            public static final String TOTAL_GUESTS = "Total Guests";
            public static final String QUALIFIED_INDICATOR = "Qualified Indicator";
            public static final String FORCE_QUALIFIED_INDICATOR = "Force Qualified Indicator";
        }

        public static class Offer
        {
            public static final String OFFER_NAME = "Offer Name";
            public static final String POINTS = "Points";
        }
    }
}
