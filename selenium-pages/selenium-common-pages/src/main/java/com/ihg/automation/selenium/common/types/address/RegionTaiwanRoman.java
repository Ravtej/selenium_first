package com.ihg.automation.selenium.common.types.address;

import com.ihg.automation.selenium.gwt.components.EntityType;

public enum RegionTaiwanRoman implements EntityType
{
    TAI_BEI_SHI(null, "tai bei shi"), //
    TAI_BEI(null, "tai bei"), //
    CHA("CHA", "Changhua"), //
    CYI("CYI", "Chiayi"), //
    CYQ("CYQ", "Chiayi");

    private String code;
    private String value;

    private RegionTaiwanRoman(String code, String value)
    {
        this.code = code;
        this.value = value;
    }

    @Override
    public String getCode()
    {
        return code;
    }

    @Override
    public String getValue()
    {
        return value;
    }

    @Override
    public String getCodeWithValue()
    {
        return code + " - " + value;
    }

    @Override
    public String toString()
    {
        return getValue();
    }
}
