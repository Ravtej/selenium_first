package com.ihg.automation.selenium.common.pages;

public interface Verify<T>
{
    void verify(T obj);
}
