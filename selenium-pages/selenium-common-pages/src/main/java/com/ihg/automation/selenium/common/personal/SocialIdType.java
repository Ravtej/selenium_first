package com.ihg.automation.selenium.common.personal;

public class SocialIdType
{
    public static final String INSTAGRAM = "Instagram";
    public static final String PINTEREST = "Pinterest";
    public static final String TWITTER = "Twitter";

    private SocialIdType()
    {
    }
}
