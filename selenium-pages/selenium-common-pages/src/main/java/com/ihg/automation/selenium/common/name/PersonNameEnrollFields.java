package com.ihg.automation.selenium.common.name;

import static com.ihg.automation.selenium.gwt.components.utils.FindStepUtils.byXpath;

import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.utils.ComponentFunctionUtils;
import com.ihg.automation.selenium.gwt.components.utils.FindStep;

public class PersonNameEnrollFields extends PersonNameFields
{
    private static final FindStep GO_TO_LABEL = byXpath("./../../../../following-sibling::div//tr[2]//td[{0}]",
            ComponentFunctionUtils.getPosition());

    public PersonNameEnrollFields(Container container)
    {
        super(container, GO_TO_LABEL);
    }

}
