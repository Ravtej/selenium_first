package com.ihg.automation.selenium.common.member;

import static com.ihg.automation.selenium.formatter.Converter.stringToInteger;

public class LifetimeActivity
{
    private int baseUnits, bonusUnits, adjustedUnits;
    private int totalUnits, redeemedUnits;
    private int currentBalance;

    public int getBaseUnits()
    {
        return baseUnits;
    }

    public void setBaseUnitsFromString(String baseUnits)
    {
        setBaseUnits(stringToInteger(baseUnits));
    }

    public void setBaseUnits(int baseUnits)
    {
        this.baseUnits = baseUnits;
    }

    public int getBonusUnits()
    {
        return bonusUnits;
    }

    public void setBonusUnitsFromString(String units)
    {
        setBonusUnits(stringToInteger(units));
    }

    public void setBonusUnits(int bonusUnits)
    {
        this.bonusUnits = bonusUnits;
    }

    public int getAdjustedUnits()
    {
        return adjustedUnits;
    }

    public void setAdjustedUnitsFromString(String adjustedUnits)
    {
        setAdjustedUnits(stringToInteger(adjustedUnits));
    }

    public void setAdjustedUnits(int adjustedUnits)
    {
        this.adjustedUnits = adjustedUnits;
    }

    public int getTotalUnits()
    {
        return totalUnits;
    }

    public void setTotalUnitsFromString(String totalUnits)
    {
        setTotalUnits(stringToInteger(totalUnits));
    }

    public void setTotalUnits(int totalUnits)
    {
        this.totalUnits = totalUnits;
    }

    public int getRedeemedUnits()
    {
        return redeemedUnits;
    }

    public void setRedeemedUnits(int redeemedUnits)
    {
        this.redeemedUnits = redeemedUnits;
    }

    public void setRedeemedUnitsFromString(String redeemedUnits)
    {
        setRedeemedUnits(stringToInteger(redeemedUnits));
    }

    public int getCurrentBalance()
    {
        return currentBalance;
    }

    public void setCurrentBalanceFromString(String currentBalance)
    {
        setCurrentBalance(stringToInteger(currentBalance));
    }

    public void setCurrentBalance(int currentBalance)
    {
        this.currentBalance = currentBalance;
    }

}
