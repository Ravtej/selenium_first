package com.ihg.automation.selenium.common.pages.login;

import com.ihg.automation.selenium.gwt.components.input.SearchInput;

public class HotelRolePopUp extends RolePopUp
{
    public HotelRolePopUp()
    {
        super();
    }

    public SearchInput getHotel()
    {
        return new SearchInput(container.getLabel("Hotel"));
    }

    public void selectAndSubmit(String role, String hotelCode, boolean isHelpDeskRole)
    {
        if (isHelpDeskRole)
        {
            getHotel().typeAndWait(hotelCode);
        }
        getRole().select(role);
        getSubmit().clickAndWait();
    }
}
