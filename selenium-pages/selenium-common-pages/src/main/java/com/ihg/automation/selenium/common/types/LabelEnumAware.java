package com.ihg.automation.selenium.common.types;

public interface LabelEnumAware
{
    public String getLabel();
}
