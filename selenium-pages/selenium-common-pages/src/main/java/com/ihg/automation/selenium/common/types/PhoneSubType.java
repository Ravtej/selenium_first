package com.ihg.automation.selenium.common.types;

import com.ihg.automation.selenium.gwt.components.EntityType;

public enum PhoneSubType implements EntityType
{
    PHONE("P", "Phone"), //
    MOBILE("M", "Mobile"), //
    FAX("F", "Fax");

    private String value;
    private String code;

    PhoneSubType(String code, String value)
    {
        this.code = code;
        this.value = value;
    }

    public String getValue()
    {
        return value;
    }

    public String getCode()
    {
        return code;
    }

    public String getCodeWithValue()
    {
        return code + " - " + value;
    }

    public String toString()
    {
        return getValue();
    }
}
