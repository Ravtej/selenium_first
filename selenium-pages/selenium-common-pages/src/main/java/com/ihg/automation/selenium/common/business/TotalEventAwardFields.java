package com.ihg.automation.selenium.common.business;

import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.CustomContainerBase;

public class TotalEventAwardFields extends CustomContainerBase
{
    private final static String BASE_POINTS = "Base Points";
    private final static String DISCRETIONARY_POINTS = "Discretionary Points";
    private final static String OFFER_POINTS = "Offer Points";
    private final static String TOTAL_POINTS = "Total Points";

    public TotalEventAwardFields(Container container)
    {
        super(container);
    }

    private TotalEventAwardItem getTotalEventAwardItem(String item)
    {
        return new TotalEventAwardItem(container, item);
    }

    public TotalEventAwardItem getBasePoints()
    {
        return getTotalEventAwardItem(BASE_POINTS);
    }

    public TotalEventAwardItem getDiscretionaryPoints()
    {
        return getTotalEventAwardItem(DISCRETIONARY_POINTS);
    }

    public TotalEventAwardItem getOfferPoints()
    {
        return getTotalEventAwardItem(OFFER_POINTS);
    }

    public TotalEventAwardItem getTotalPoints()
    {
        return getTotalEventAwardItem(TOTAL_POINTS);
    }
}
