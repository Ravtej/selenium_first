package com.ihg.automation.selenium.common.history;

import static com.ihg.automation.selenium.common.types.Currency.USD;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasTextAsDouble;

import com.ihg.automation.selenium.common.business.BusinessRevenueBean;
import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.CustomContainerBase;

public class EventHistoryRecords extends CustomContainerBase
{
    private static final String UNIT_TYPE = "HPC";
    public static final String REVENUE_ESTIMATED_COST_TO_HOTEL_CURRENCY = "Revenue Estimated Cost to Hotel Currency";
    public static final String REVENUE_ESTIMATED_COST_TO_HOTEL_AMOUNT = "Revenue Estimated Cost to Hotel Amount";
    public static final String REVENUE_ESTIMATED_UNIT_TYPE = "Revenue Estimated Unit Type";
    public static final String REVENUE_ESTIMATED_POINTS_EARNING = "Revenue Estimated Points Earning";
    public static final String REVENUE_LOCAL_AMOUNT = "Revenue Local Amount";

    public EventHistoryRecords(Container container)
    {
        super(container);
    }

    public EventHistoryRecord getRecord(String key)
    {
        return new EventHistoryRecord(container, key);
    }

    public EventHistoryRecord getRecord(int index)
    {
        return new EventHistoryRecord(container, index);
    }

    public EventHistoryRecord getStayAmountRecord(String key)
    {
        return new EventHistoryRecord(container, key + " Amount");
    }

    public EventHistoryRecord getStayQualifyingRecord(String key)
    {
        return new EventHistoryRecord(container, key + " Qualifying Revenue Indicator");
    }

    public EventHistoryRecord getStayBillHotelRecord(String key)
    {
        return new EventHistoryRecord(container, key + " Bill Hotel Indicator");
    }

    public EventHistoryRecord getBRLocalAmount(String key)
    {
        return new EventHistoryRecord(container, String.format("%s %s", key, REVENUE_LOCAL_AMOUNT));
    }

    public EventHistoryRecord getBREstimatedPointsEarning(String key)
    {
        return new EventHistoryRecord(container, String.format("%s %s", key, REVENUE_ESTIMATED_POINTS_EARNING));
    }

    public EventHistoryRecord getBREstimatedUnitType(String key)
    {
        return new EventHistoryRecord(container, String.format("%s %s", key, REVENUE_ESTIMATED_UNIT_TYPE));
    }

    public EventHistoryRecord getBREstimatedCostToAmount(String key)
    {
        return new EventHistoryRecord(container, String.format("%s %s", key, REVENUE_ESTIMATED_COST_TO_HOTEL_AMOUNT));
    }

    public EventHistoryRecord getBREstimatedCostToCurrency(String key)
    {
        return new EventHistoryRecord(container, String.format("%s %s", key, REVENUE_ESTIMATED_COST_TO_HOTEL_CURRENCY));
    }

    public EventHistoryRecord getBRLocalAmount()
    {
        return new EventHistoryRecord(container, REVENUE_LOCAL_AMOUNT);
    }

    public EventHistoryRecord getBREstimatedPointsEarning()
    {
        return new EventHistoryRecord(container, REVENUE_ESTIMATED_POINTS_EARNING);
    }

    public EventHistoryRecord getBREstimatedUnitType()
    {
        return new EventHistoryRecord(container, REVENUE_ESTIMATED_UNIT_TYPE);
    }

    public EventHistoryRecord getBREstimatedCostToAmount()
    {
        return new EventHistoryRecord(container, REVENUE_ESTIMATED_COST_TO_HOTEL_AMOUNT);
    }

    public EventHistoryRecord getBREstimatedCostToCurrency()
    {
        return new EventHistoryRecord(container, REVENUE_ESTIMATED_COST_TO_HOTEL_CURRENCY);
    }

    public void verifyAddBusinessRevenue(String revenueKey, BusinessRevenueBean businessRevenue)
    {
        getBRLocalAmount(revenueKey).verifyAdd(hasTextAsDouble(businessRevenue.getAmount()));
        getBREstimatedPointsEarning(revenueKey).verifyAdd(businessRevenue.getEstimatedPointEarning());
        getBREstimatedUnitType(revenueKey).verifyAdd(UNIT_TYPE);
        getBREstimatedCostToAmount(revenueKey).verifyAdd(businessRevenue.getEstimatedCostToHotel());
        getBREstimatedCostToCurrency(revenueKey).verifyAdd(USD.getCode());
    }

    public void verifyAddBusinessRevenue(BusinessRevenueBean businessRevenue)
    {
        getBRLocalAmount().verifyAdd(hasTextAsDouble(businessRevenue.getAmount()));
        getBREstimatedPointsEarning().verifyAdd(businessRevenue.getEstimatedPointEarning());
        getBREstimatedUnitType().verifyAdd(EventHistoryRecords.UNIT_TYPE);
        getBREstimatedCostToAmount().verifyAdd(businessRevenue.getEstimatedCostToHotel());
        getBREstimatedCostToCurrency().verifyAdd(USD.getCode());
    }

}
