package com.ihg.automation.selenium.common.types.name;

public class NameSuffix
{
    private NameSuffix()
    {
    }

    public static final String JR = "Jr";
    public static final String III = "III";
    public static final String SR = "Sr";
    public static final String II = "II";
    public static final String ESQ = "Esq";
}
