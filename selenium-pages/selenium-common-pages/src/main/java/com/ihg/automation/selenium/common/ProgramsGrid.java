package com.ihg.automation.selenium.common;

import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.grid.Grid;
import com.ihg.automation.selenium.gwt.components.grid.GridRowXpath;

public class ProgramsGrid extends Grid<ProgramGridRow, ProgramGridCell>
{
    public ProgramsGrid(Container parent)
    {
        super(parent);
    }

    public RewardClubGridRow getRewardClubProgram()
    {
        return new RewardClubGridRow(this);
    }

    public BusinessRewardsGridRow getBusinessRewardsProgram()
    {
        return new BusinessRewardsGridRow(this);
    }

    public ProgramGridRow getProgram(Program program)
    {
        return getRow(new GridRowXpath.GridRowXpathBuilder().cell(ProgramGridCell.CODE, program.getValue()).build());
    }
}
