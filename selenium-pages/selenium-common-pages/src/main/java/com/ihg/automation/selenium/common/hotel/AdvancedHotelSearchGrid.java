package com.ihg.automation.selenium.common.hotel;

import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.grid.Grid;
import com.ihg.automation.selenium.gwt.components.grid.GridRowXpath;

public class AdvancedHotelSearchGrid
        extends Grid<AdvancedHotelSearchGridRow, AdvancedHotelSearchGridRow.AdvancedHotelSearchCell>
{
    public AdvancedHotelSearchGrid(Container container)
    {
        super(container);
    }

    public AdvancedHotelSearchGridRow getRow(String hotelCode)
    {
        return getRow(new GridRowXpath.GridRowXpathBuilder()
                .cell(AdvancedHotelSearchGridRow.AdvancedHotelSearchCell.NAME, hotelCode).build());
    }
}
