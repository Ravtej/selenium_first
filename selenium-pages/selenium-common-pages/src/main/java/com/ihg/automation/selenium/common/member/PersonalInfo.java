package com.ihg.automation.selenium.common.member;

import org.joda.time.LocalDate;

import com.ihg.automation.selenium.common.components.Country;
import com.ihg.automation.selenium.common.types.Gender;

public class PersonalInfo
{
    private GuestName name;
    private Country residenceCountry = Country.US;
    private String birthCityName;
    private Country birthCountry;
    private LocalDate birthDate;
    private Gender genderCode;
    private String nameOnCard;

    public GuestName getName()
    {
        return name;
    }

    public void setName(GuestName name)
    {
        this.name = name;
    }

    public Country getResidenceCountry()
    {
        return residenceCountry;
    }

    public void setResidenceCountry(Country residenceCountry)
    {
        this.residenceCountry = residenceCountry;
    }

    public String getBirthCityName()
    {
        return birthCityName;
    }

    public void setBirthCityName(String birthCityName)
    {
        this.birthCityName = birthCityName;
    }

    public Country getBirthCountry()
    {
        return birthCountry;
    }

    public void setBirthCountry(Country birthCountry)
    {
        this.birthCountry = birthCountry;
    }

    public LocalDate getBirthDate()
    {
        return birthDate;
    }

    public void setBirthDate(LocalDate birthDate)
    {
        this.birthDate = birthDate;
    }

    public Gender getGenderCode()
    {
        return genderCode;
    }

    public void setGenderCode(Gender genderCode)
    {
        this.genderCode = genderCode;
    }

    public String getNameOnCard()
    {
        if ((nameOnCard == null) && (name != null))
        {
            return name.getNameOnCard();
        }

        return nameOnCard;
    }

    public void setNameOnCard(String nameOnCard)
    {
        this.nameOnCard = nameOnCard;
    }
}
