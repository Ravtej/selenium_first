package com.ihg.automation.selenium.common.earning;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.Constant.NOT_AVAILABLE;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasTextAsFormattedInt;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.isDisplayedAndEnabled;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static com.ihg.automation.selenium.matchers.text.EntityMatcherFactory.isValue;

import com.ihg.automation.selenium.common.types.Alliance;
import com.ihg.automation.selenium.gwt.components.ComponentColumnPosition;
import com.ihg.automation.selenium.gwt.components.label.Label;

public class EventEarningAmount extends AmountFieldBase
{
    private static final ComponentColumnPosition POSITION = ComponentColumnPosition.FOLLOWING_TD;

    public EventEarningAmount(Label label)
    {
        super(label, POSITION);
    }

    public void verify(String allianceAmount, Alliance alliance)
    {
        if (NOT_AVAILABLE.equals(allianceAmount))
        {
            verifyThat(getAmount(), isDisplayedAndEnabled(false));
            verifyThat(getUnitType(), hasText(NOT_AVAILABLE));
        }
        else
        {
            verifyThat(getAmount(), hasTextAsFormattedInt(allianceAmount));
            verifyThat(getUnitType(), hasText(isValue(alliance)));
        }
    }
}
