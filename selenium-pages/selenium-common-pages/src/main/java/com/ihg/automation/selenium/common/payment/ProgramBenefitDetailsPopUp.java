package com.ihg.automation.selenium.common.payment;

import static com.ihg.automation.selenium.gwt.components.utils.FindStepUtils.byXpathWithWait;
import static java.text.MessageFormat.format;

import com.ihg.automation.selenium.common.pages.ClosablePopUpBase;
import com.ihg.automation.selenium.gwt.components.Component;
import com.ihg.automation.selenium.gwt.components.panel.Panel;

public class ProgramBenefitDetailsPopUp extends ClosablePopUpBase
{
    private static final String TYPE = "Label Text";
    private static final String XPATH_PATTERN = ".//span[normalize-space(text())={0}]/following-sibling::span";

    public ProgramBenefitDetailsPopUp()
    {
        super("Program Benefit Details");
    }

    public Component getPackage()
    {
        return new Component(container, "Package", TYPE, byXpathWithWait(format(XPATH_PATTERN, "'Package'")));
    }

    public Component getNumberOfMonths()
    {
        return new Component(container, "Number of Months", TYPE,
                byXpathWithWait(format(XPATH_PATTERN, "'Number of Months'")));
    }

    public Panel getBenefitsPanel()
    {
        return container.getPanel("Benefits");
    }

    public BenefitsDetailGrid getBenefitsDetailGrid()
    {
        return new BenefitsDetailGrid(getBenefitsPanel());
    }
}
