package com.ihg.automation.selenium.common.billing;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;

import com.ihg.automation.selenium.common.event.CurrencyAmountFieldsAware;
import com.ihg.automation.selenium.common.pages.LabelContainerBase;
import com.ihg.automation.selenium.common.types.Currency;
import com.ihg.automation.selenium.gwt.components.Component;
import com.ihg.automation.selenium.gwt.components.ComponentColumnPosition;
import com.ihg.automation.selenium.gwt.components.label.Label;

public class EventBillingAmount extends LabelContainerBase implements CurrencyAmountFieldsAware
{
    private static final ComponentColumnPosition POSITION = ComponentColumnPosition.FOLLOWING_TD;

    public EventBillingAmount(Label label)
    {
        super(label);
    }

    @Override
    public Component getAmount()
    {
        return label.getComponent("Amount", 1, POSITION);
    }

    @Override
    public Component getCurrency()
    {
        return label.getComponent("Currency", 2, POSITION);
    }

    public Component getBillingTo()
    {
        return label.getComponent("Billing To", 4, POSITION);
    }

    public void verify(String unitsAmount, String unitsCurrency, String unitsBillingTo)
    {
        verifyThat(getAmount(), hasText(unitsAmount));
        verifyThat(getCurrency(), hasText(unitsCurrency));
        verifyThat(getBillingTo(), hasText(unitsBillingTo));
        // TODO add necessary verifications
    }

    public void verifyUSDAmount(String usdAmount, String billingTo)
    {
        verify(usdAmount, Currency.USD.getCode(), billingTo);
    }
}
