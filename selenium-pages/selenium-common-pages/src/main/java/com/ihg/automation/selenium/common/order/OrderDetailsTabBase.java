package com.ihg.automation.selenium.common.order;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.Constant.NOT_AVAILABLE;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;

import com.ihg.automation.selenium.common.Constant;
import com.ihg.automation.selenium.common.annual.ActivityAmount;
import com.ihg.automation.selenium.common.pages.PopUpBase;
import com.ihg.automation.selenium.common.pages.TabCustomContainerBase;
import com.ihg.automation.selenium.common.payment.CurrencyAmount;
import com.ihg.automation.selenium.gwt.components.label.Label;

public class OrderDetailsTabBase extends TabCustomContainerBase
{
    public OrderDetailsTabBase(PopUpBase popUp, String tabName)
    {
        super(popUp, tabName);
    }

    public Label getTransactionDate()
    {
        return container.getLabel("Transaction Date");
    }

    public Label getTransactionType()
    {
        return container.getLabel("Transaction Type");
    }

    public Label getOrderNumber()
    {
        return container.getLabel("Order Number");
    }

    public Label getOrderedDate()
    {
        return container.getLabel("Ordered Date");
    }

    public Label getOrderedBy()
    {
        return container.getLabel("Ordered by");
    }

    public Label getChannelOfOrder()
    {
        return container.getLabel("Channel of Order");
    }

    public CurrencyAmount getTotalCash()
    {
        return new CurrencyAmount(container.getLabel("Total Cash"));
    }

    public ActivityAmount getTotalLoyaltyUnits()
    {
        return new ActivityAmount(container.getLabel("Total Loyalty Units"));
    }

    public ShippingInfoEdit getShippingInfo()
    {
        return new ShippingInfoEdit(container);
    }

    public void verify(Order order)
    {
        verifyThat(getChannelOfOrder(), hasText(order.getChannelOfOrder()));
        verifyThat(getOrderedBy(), hasText(order.getOrderedBy()));
        verifyThat(getOrderedDate(), hasText(order.getOrderedDate()));
        verifyThat(getOrderNumber(), hasText(order.getOrderNumber()));
        getTotalCash().verify(NOT_AVAILABLE, null);
        getTotalLoyaltyUnits().verify(order.getTotalLoyaltyUnits(), Constant.RC_POINTS);
        verifyThat(getTransactionDate(), hasText(order.getTransactionDate()));
        verifyThat(getTransactionType(), hasText(order.getTransactionType()));
    }

    public void verify(VoucherOrder voucherOrder)
    {
        verifyThat(getChannelOfOrder(), hasText(voucherOrder.getUserId()));
        verifyThat(getOrderedBy(), hasText(voucherOrder.getSource().getUserID()));
        verifyThat(getOrderedDate(), hasText(voucherOrder.getTransactionDate()));
        verifyThat(getOrderNumber(), hasText(NOT_AVAILABLE));
        getTotalCash().verify(voucherOrder.getVoucherCostAmount(), voucherOrder.getCurrencyCode());
        getTotalLoyaltyUnits().verify(NOT_AVAILABLE, null);
        verifyThat(getTransactionDate(), hasText(voucherOrder.getTransactionDate()));
        verifyThat(getTransactionType(), hasText(VoucherOrder.TRANSACTION_TYPE));
    }
}
