package com.ihg.automation.selenium.common.types.address;

import com.ihg.automation.selenium.common.types.LabelEnumAware;

public enum ZipLabel implements LabelEnumAware
{
    POST("Post Code"), POSTAL("Postal code"), ZIP("Zip Code");

    private String label;

    private ZipLabel(String label)
    {
        this.label = label;
    }

    @Override
    public String getLabel()
    {
        return label;
    }
}
