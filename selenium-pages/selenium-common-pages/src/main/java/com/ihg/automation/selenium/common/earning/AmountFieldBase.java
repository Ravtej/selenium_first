package com.ihg.automation.selenium.common.earning;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.Constant.NOT_AVAILABLE;
import static com.ihg.automation.selenium.common.Constant.RC_POINTS;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasTextAsFormattedInt;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static com.ihg.automation.selenium.matchers.component.HasText.hasTextNoWait;
import static org.hamcrest.text.IsEmptyString.isEmptyOrNullString;

import org.apache.commons.lang3.StringUtils;

import com.ihg.automation.selenium.common.AmountFieldsAware;
import com.ihg.automation.selenium.common.pages.LabelContainerBase;
import com.ihg.automation.selenium.gwt.components.Component;
import com.ihg.automation.selenium.gwt.components.ComponentColumnPosition;
import com.ihg.automation.selenium.gwt.components.label.Label;

public abstract class AmountFieldBase extends LabelContainerBase implements AmountFieldsAware
{
    private ComponentColumnPosition position;

    public AmountFieldBase(Label label, ComponentColumnPosition position)
    {
        super(label);
        this.position = position;
    }

    @Override
    public Component getAmount()
    {
        return label.getComponent("Amount", 1, position);
    }

    @Override
    public Component getUnitType()
    {
        return label.getComponent("Unit type", 2, position);
    }

    public void verify(String amount, String unitType)
    {
        if (StringUtils.isEmpty(amount) || NOT_AVAILABLE.equals(amount))
        {
            verifyThat(getAmount(), hasText(NOT_AVAILABLE));
            verifyThat(getUnitType(), hasTextNoWait(isEmptyOrNullString()));
        }
        else
        {
            verifyThat(getAmount(), hasTextAsFormattedInt(amount));
            verifyThat(getUnitType(), hasText(unitType));
        }
    }

    public void verify(String amount)
    {
        verify(amount, RC_POINTS);
    }

}
