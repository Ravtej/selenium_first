package com.ihg.automation.selenium.common.freenight;

import com.ihg.automation.selenium.common.offer.GuestOffer;

public class FreeNight
{
    private String confirmationNumber = "";
    private String hotel = "";
    private String confirmationDate = "";
    private String numberOfRooms = "";
    private String checkIn = "";
    private String checkOut = "";
    private String nights = "";
    private GuestOffer guestOffer;
    private String future = "";
    private String available;
    private String redeemed = "";
    private String expired = "";

    public String getConfirmationNumber()
    {
        return confirmationNumber;
    }

    public void setConfirmationNumber(String confirmationNumber)
    {
        this.confirmationNumber = confirmationNumber;
    }

    public String getHotel()
    {
        return hotel;
    }

    public void setHotel(String hotel)
    {
        this.hotel = hotel;
    }

    public String getConfirmationDate()
    {
        return confirmationDate;
    }

    public void setConfirmationDate(String confirmationDate)
    {
        this.confirmationDate = confirmationDate;
    }

    public String getNumberOfRooms()
    {
        return numberOfRooms;
    }

    public void setNumberOfRooms(String numberOfRooms)
    {
        this.numberOfRooms = numberOfRooms;
    }

    public String getCheckIn()
    {
        return checkIn;
    }

    public void setCheckIn(String checkIn)
    {
        this.checkIn = checkIn;
    }

    public String getCheckOut()
    {
        return checkOut;
    }

    public void setCheckOut(String checkOut)
    {
        this.checkOut = checkOut;
    }

    public String getNights()
    {
        return nights;
    }

    public void setNights(String nights)
    {
        this.nights = nights;
    }

    public String getFuture()
    {
        return future;
    }

    public void setFuture(String future)
    {
        this.future = future;
    }

    public String getAvailable()
    {
        return available;
    }

    public void setAvailable(String available)
    {
        this.available = available;
    }

    public String getRedeemed()
    {
        return redeemed;
    }

    public void setRedeemed(String redeemed)
    {
        this.redeemed = redeemed;
    }

    public String getExpired()
    {
        return expired;
    }

    public void setExpired(String expired)
    {
        this.expired = expired;
    }

    public GuestOffer getGuestOffer()
    {
        return guestOffer;
    }

    public void setGuestOffer(GuestOffer guestOffer)
    {
        this.guestOffer = guestOffer;
    }
}
