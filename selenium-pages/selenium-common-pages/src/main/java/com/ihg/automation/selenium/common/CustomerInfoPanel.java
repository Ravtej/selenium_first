package com.ihg.automation.selenium.common;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.gwt.components.utils.FindStepUtils.byXpathWithWait;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static java.text.MessageFormat.format;

import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.gwt.components.Component;
import com.ihg.automation.selenium.gwt.components.CustomContainerBase;
import com.ihg.automation.selenium.gwt.components.panel.FieldSet;

public class CustomerInfoPanel extends CustomContainerBase
{
    public CustomerInfoPanel()
    {
        super(new FieldSet("Customer Information"));
    }

    public Component getFullName()
    {
        return new Component(container, "Label", "Full Name", byXpathWithWait(".//div[contains(@class, 'full-name')]"));
    }

    public Component getAddress()
    {
        return new Component(container, "Label", "Address", byXpathWithWait(".//div[contains(@class, 'address')]"));
    }

    public CustomerInfoFlags getFlags()
    {
        return new CustomerInfoFlags(container);
    }

    public Component getMemberNumber(Program program)
    {
        String xpath = format(".//td[contains(normalize-space(),''{0} #'')]/following-sibling::td/span",
                program.getCode());

        return new Component(container, "Label", format("{0} #", program.getCode()), byXpathWithWait(xpath));
    }

    public Component getEmail()
    {
        return new Component(container, "Label", "Email", byXpathWithWait(".//div[contains(@class, 'email')]"));
    }

    public Component getPhone()
    {
        return new Component(container, "Label", "Phone", byXpathWithWait(".//div[contains(@class, 'phone')]"));
    }

    public Component getStatus()
    {
        return new Component(container, "Label", "Status", byXpathWithWait(".//div[contains(@class, 'status')]"));
    }

    public void verify(Member member)
    {
        verifyThat(getFullName(), hasText(member.getPersonalInfo().getName().getNameOnPanel()));
        verifyThat(getPhone(), hasText(member.getPreferredPhone().getFullPhone()));
        verifyThat(getEmail(), hasText(member.getPreferredEmail().getEmail()));
    }

    public void verifyStatus(String status)
    {
        verifyThat(getStatus(), hasText(status.toUpperCase()));
    }
}
