package com.ihg.automation.selenium.common.reward;

public class RewardNight
{
    private String confirmationNumber = "";
    private String hotel = "";
    private String hotelCurrency;
    private String roomType = "";
    private String numberOfRooms = "";
    private String checkIn = "";
    private String checkOut = "";
    private String nights = "";

    private String transactionDate = "";
    private String transactionType = "";
    private String certificateNumber = "";
    private String bookingSource = "";
    private String loyaltyUnits = "";
    private String transactionDescription = "";

    public void setNights(String nights)
    {
        this.nights = nights;
    }

    public String getNights()
    {
        return nights;
    }

    public void setCheckOut(String checkOut)
    {
        this.checkOut = checkOut;
    }

    public String getCheckOut()
    {
        return checkOut;
    }

    public void setCheckIn(String checkIn)
    {
        this.checkIn = checkIn;
    }

    public String getCheckIn()
    {
        return checkIn;
    }

    public void setNumberOfRooms(String numberOfRooms)
    {
        this.numberOfRooms = numberOfRooms;
    }

    public String getNumberOfRooms()
    {
        return numberOfRooms;
    }

    public void setRoomType(String roomType)
    {
        this.roomType = roomType;
    }

    public String getRoomType()
    {
        return roomType;
    }

    public void setHotel(String hotel)
    {
        this.hotel = hotel;
    }

    public String getHotel()
    {
        return hotel;
    }

    public String getHotelCurrency()
    {
        return hotelCurrency;
    }

    public void setHotelCurrency(String hotelCurrency)
    {
        this.hotelCurrency = hotelCurrency;
    }

    public void setConfirmationNumber(String confirmationNumber)
    {
        this.confirmationNumber = confirmationNumber;
    }

    public String getConfirmationNumber()
    {
        return confirmationNumber;
    }

    public String getTransactionDate()
    {
        return transactionDate;
    }

    public void setTransactionDate(String transactionDate)
    {
        this.transactionDate = transactionDate;
    }

    public String getTransactionType()
    {
        return transactionType;
    }

    public void setTransactionType(String transactionType)
    {
        this.transactionType = transactionType;
    }

    public String getCertificateNumber()
    {
        return certificateNumber;
    }

    public void setCertificateNumber(String certificateNumber)
    {
        this.certificateNumber = certificateNumber;
    }

    public String getBookingSource()
    {
        return bookingSource;
    }

    public void setBookingSource(String bookingSource)
    {
        this.bookingSource = bookingSource;
    }

    public String getLoyaltyUnits()
    {
        return loyaltyUnits;
    }

    public void setLoyaltyUnits(String loyaltyUnits)
    {
        this.loyaltyUnits = loyaltyUnits;
    }

    public String getTransactionDescription()
    {
        return transactionDescription;
    }

    public void setTransactionDescription(String transactionDescription)
    {
        this.transactionDescription = transactionDescription;
    }

    @Override
    public String toString()
    {
        return "RewardNight [confirmationNumber=" + confirmationNumber + ", hotel=" + hotel + ", checkIn=" + checkIn
                + ", checkOut=" + checkOut + ", certificateNumber=" + certificateNumber + "]";
    }

}
