package com.ihg.automation.selenium.common.business;

import com.ihg.automation.selenium.gwt.components.CustomContainerBase;

public class MeetingRevenueDetails extends CustomContainerBase
{
    private final static String MEETING_ROOM_LABEL = "Meeting Room";
    private final static String FOOD_BEVERAGE_LABEL = "Food/Beverage";
    private final static String MISCELLANEOUS_LABEL = "Miscellaneous";
    private final static String TOTAL_MEETING_REVENUE_LABEL = "Total Meeting Revenue";

    public MeetingRevenueDetails(CustomContainerBase parent)
    {
        super(parent.container);
    }

    public BusinessRevenue getMeetingRoom()
    {
        return new BusinessRevenue(container, MEETING_ROOM_LABEL);
    }

    public BusinessRevenue getFoodAndBeverage()
    {
        return new BusinessRevenue(container, FOOD_BEVERAGE_LABEL);
    }

    public BusinessRevenue getMiscellaneous()
    {
        return new BusinessRevenue(container, MISCELLANEOUS_LABEL);
    }

    public BusinessRevenueView getTotalMeetingRevenue()
    {
        return new BusinessRevenueView(container, TOTAL_MEETING_REVENUE_LABEL);
    }

    public void populateLocalAmount(MeetingDetails meetingDetails)
    {
        getMeetingRoom().populateLocalAmount(meetingDetails.getMeetingRoom());
        getFoodAndBeverage().populateLocalAmount(meetingDetails.getFoodBeverage());
        getMiscellaneous().populateLocalAmount(meetingDetails.getMiscellaneous());
    }

    public void verifyUSDamount(MeetingDetails meetingDetails)
    {
        getMeetingRoom().verifyUSDAmount(meetingDetails.getMeetingRoom());
        getFoodAndBeverage().verifyUSDAmount(meetingDetails.getFoodBeverage());
        getMiscellaneous().verifyUSDAmount(meetingDetails.getMiscellaneous());

        BusinessRevenueBean totalMeetingRevenue = meetingDetails.getTotalMeetingRevenue();
        getTotalMeetingRevenue().verifyLocalAmount(totalMeetingRevenue);
        getTotalMeetingRevenue().verifyUSDAmount(totalMeetingRevenue);
    }
}
