package com.ihg.automation.selenium.common.event;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;

import com.ihg.automation.selenium.common.EventSource;
import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.label.Label;

public class EventSourceExtended extends EventSource
{
    public EventSourceExtended(Container container)
    {
        super(container);
    }

    public Label getOfferCode()
    {
        return container.getLabel("Loyalty Offer Code");
    }

    public void verify(Source source)
    {
        verify(source, getOfferCode().getDefaultValue());
    }

    public void verify(Source source, String offerCode)
    {
        super.verify(source);
        verifyThat(getOfferCode(), hasText(offerCode));
    }

}
