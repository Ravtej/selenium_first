package com.ihg.automation.selenium.common.types.address;

import java.util.HashMap;
import java.util.Map;

import com.ihg.automation.selenium.common.components.Country;

public class AddressConfiguration
{
    private static final Map<Country, AddressConfigurationBean> CONFIGS = initConfigs();

    private AddressConfiguration()
    {
    }

    private static Map<Country, AddressConfigurationBean> initConfigs()
    {
        Map<Country, AddressConfigurationBean> configs = new HashMap<Country, AddressConfigurationBean>();

        configs.put(Country.US, getUSConfig());
        configs.put(Country.CA, getCAConfig());
        configs.put(Country.ES, getDefaultConfig());
        configs.put(Country.FR, getDefaultConfig());
        configs.put(Country.BY, getBYConfig());
        configs.put(Country.RU, getDefaultConfig());
        configs.put(Country.CN, getDefaultConfig());
        configs.put(Country.JP, getDefaultConfig());
        configs.put(Country.TW, getTaiwanConfig());
        configs.put(Country.HK, getTaiwanConfig());
        configs.put(Country.MO, getTaiwanConfig());
        configs.put(Country.AU, getAustraliaConfig());
        configs.put(Country.UA, getDefaultConfig());
        configs.put(Country.ID, getDefaultConfig());
        configs.put(Country.AR, getDefaultConfig());
        configs.put(Country.ER, getBYConfig());
        configs.put(Country.BR, getBrazilConfig());

        return configs;
    }

    public static AddressConfigurationBean getAddressConfiguration(Country country)
    {
        AddressConfigurationBean bean = CONFIGS.get(country);

        if (bean == null)
        {
            throw new RuntimeException(String.format("No address configuration is found for %s", country));
        }

        return bean;
    }

    public static AddressConfigurationBean getDefaultConfig()
    {
        AddressConfigurationBean bean = new AddressConfigurationBean();
        bean.setLocality1(Locality1Label.CITY_TOWN);
        bean.setLocality2(Locality2Label.LOCALITY);
        bean.setRegion(RegionLabel.PROVINCE);
        bean.setPostalCode(ZipLabel.POSTAL);

        return bean;
    }

    public static AddressConfigurationBean getUSConfig()
    {
        AddressConfigurationBean bean = getDefaultConfig();
        bean.setLocality1(Locality1Label.CITY);
        bean.setRegion(RegionLabel.STATE);
        bean.setPostalCode(ZipLabel.ZIP);

        return bean;
    }

    public static AddressConfigurationBean getCAConfig()
    {
        AddressConfigurationBean bean = getDefaultConfig();
        bean.setLocality1(Locality1Label.CITY);
        bean.setRegion(RegionLabel.PROVINCE);

        return bean;
    }

    public static AddressConfigurationBean getBYConfig()
    {
        AddressConfigurationBean bean = getDefaultConfig();
        bean.setRegion(RegionLabel.REGION);

        return bean;
    }

    public static AddressConfigurationBean getTaiwanConfig()
    {
        AddressConfigurationBean bean = getDefaultConfig();
        bean.setRegion(RegionLabel.DISTRICT);

        return bean;
    }

    public static AddressConfigurationBean getAustraliaConfig()
    {
        AddressConfigurationBean bean = getDefaultConfig();
        bean.setLocality1(Locality1Label.LOCALITY);
        bean.setRegion(RegionLabel.STATE_TERRITORY);
        bean.setPostalCode(ZipLabel.POST);

        return bean;
    }

    public static AddressConfigurationBean getITConfig()
    {
        AddressConfigurationBean bean = getDefaultConfig();
        bean.setLocality1(Locality1Label.CITY_TOWN);
        bean.setRegion(RegionLabel.REGION);
        bean.setPostalCode(ZipLabel.POSTAL);

        return bean;
    }

    public static AddressConfigurationBean getBrazilConfig()
    {
        AddressConfigurationBean bean = new AddressConfigurationBean();
        bean.setLocality1(Locality1Label.CITY);
        bean.setLocality2(Locality2Label.DISTRICT);
        bean.setRegion(RegionLabel.STATE);
        bean.setPostalCode(ZipLabel.ZIP);

        return bean;
    }

}
