package com.ihg.automation.selenium.common.member;

import org.springframework.jdbc.core.JdbcTemplate;

import com.ihg.automation.selenium.common.types.program.Program;

public class AmbassadorJdbcUtils
{
    private static final String OPEN = "O";
    private static final String CLOSED = "C";

    private static final String RENEW = "R";
    private static final String EXPIRED = "E";

    private static final int RENEW_DAYS_SHIFT = 90;
    private static final int EXPIRATION_DATE_SHIFT = -32;

    private static final String UPDATE = "UPDATE DGST.MBRSHP SET"//
            + " MBRSHP_STAT_CD = '%s',"//
            + " MBRSHP_STAT_RSN_CD = '%s',"//
            + " EXPR_DT = (SYSDATE + %d),"//
            + " LST_UPDT_USR_ID = '%s', LST_UPDT_TS = SYSDATE"//
            + " WHERE LYTY_PGM_CD = '%s' AND MBRSHP_ID = '%s'";

    private JdbcTemplate jdbc;
    private String lastUpdateUserId;

    public AmbassadorJdbcUtils(JdbcTemplate jdbc, String lastUpdateUserId)
    {
        this.jdbc = jdbc;
        this.lastUpdateUserId = lastUpdateUserId;
    }

    private int update(final String membershipStatusCode, final String membershipStatusReasonCode,
            final int expirationDateShift, final Member member)
    {
        jdbc.update(String.format(UPDATE, membershipStatusCode, membershipStatusReasonCode, expirationDateShift,
                lastUpdateUserId, Program.AMB.getCode(), member.getRCProgramId()));
        return expirationDateShift;
    }

    public int updateForRenew(final Member member)
    {
        return update(OPEN, RENEW, RENEW_DAYS_SHIFT, member);
    }

    public int updateForReactivate(final Member member)
    {
        return update(CLOSED, EXPIRED, EXPIRATION_DATE_SHIFT, member);
    }
}
