package com.ihg.automation.selenium.common.business;

public class DiscretionaryPoints
{
    private String additionalPointsToAward;
    private String reason;
    private String comments;
    private String estimatedCostToHotel;

    public DiscretionaryPoints(String additionalPointsToAward, String reason)
    {
        this.additionalPointsToAward = additionalPointsToAward;
        this.reason = reason;
    }

    public String getAdditionalPointsToAward()
    {
        return additionalPointsToAward;
    }

    public void setAdditionalPointsToAward(String additionalPointsToAward)
    {
        this.additionalPointsToAward = additionalPointsToAward;
    }

    public String getReason()
    {
        return reason;
    }

    public void setReason(String reason)
    {
        this.reason = reason;
    }

    public String getComments()
    {
        return comments;
    }

    public void setComments(String comments)
    {
        this.comments = comments;
    }

    public String getEstimatedCostToHotel()
    {
        return estimatedCostToHotel;
    }

    public void setEstimatedCostToHotel(String estimatedCostToHotel)
    {
        this.estimatedCostToHotel = estimatedCostToHotel;
    }
}
