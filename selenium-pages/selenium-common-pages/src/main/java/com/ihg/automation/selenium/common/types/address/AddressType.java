package com.ihg.automation.selenium.common.types.address;

import com.ihg.automation.selenium.gwt.components.EntityType;

public enum AddressType implements EntityType
{

    BUSINESS("B", "Business"), RESIDENCE("H", "Residence");

    private String code;
    private String value;

    private AddressType(String code, String value)
    {
        this.code = code;
        this.value = value;
    }

    @Override
    public String getCode()
    {
        return code;
    }

    @Override
    public String getValue()
    {
        return value;
    }

    @Override
    public String toString()
    {
        return getValue();
    }

    @Override
    public String getCodeWithValue()
    {
        return code + " - " + value;
    }
}
