package com.ihg.automation.selenium.common.enroll;

import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.assertNoError;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.assertThat;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ihg.automation.selenium.common.AddressNotMailableDialog;
import com.ihg.automation.selenium.common.EmailNotMailableDialog;
import com.ihg.automation.selenium.common.contact.Address;
import com.ihg.automation.selenium.common.contact.Email;
import com.ihg.automation.selenium.common.contact.Phone;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.pages.NavigatePage;
import com.ihg.automation.selenium.common.pages.Populate;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.gwt.components.Button;
import com.ihg.automation.selenium.gwt.components.Component;
import com.ihg.automation.selenium.gwt.components.CustomContainerBase;
import com.ihg.automation.selenium.gwt.components.panel.Panel;
import com.ihg.automation.selenium.listener.Verifier;

public abstract class EnrollmentPageBase extends CustomContainerBase implements Populate<Member>, NavigatePage
{
    private static final Logger logger = LoggerFactory.getLogger(EnrollmentPageBase.class);
    private Component enrollment;

    public EnrollmentPageBase(Component enrollment)
    {
        super(new Panel("Enroll"));
        this.enrollment = enrollment;
    }

    @Override
    public void goTo()
    {
        if (!isDisplayed())
        {
            enrollment.clickAndWait();

            flush();
            assertThat(this, displayed(true));
        }
        else
        {
            logger.debug("Page {} has already displayed. Nothing to do", this.getDescription());
        }
    }

    public CustomerInfo getCustomerInfo()
    {
        return new CustomerInfoEnroll(container.getSeparator("Customer Information"));
    }

    public Address getAddress()
    {
        return new Address(container.getSeparator("Address"));
    }

    public Phone getPhone()
    {
        return new Phone(container.getSeparator("Phone"));
    }

    public Email getEmail()
    {
        return new Email(container.getSeparator("Email"));
    }

    public AdditionalInformation getAdditionalInformation()
    {
        return new AdditionalInformation(container.getSeparator("Additional Information"));
    }

    public void clickSubmit(Verifier... verifiers)
    {
        container.getButton(Button.SUBMIT).clickAndWait(verifiers);
    }

    /**
     * Method skips possible "Address not mailable" and "Email Address not
     * mailable" pop ups and continues enrollment
     */
    public void clickSubmitAndSkipWarnings(Verifier... verifiers)
    {
        clickSubmit(verifiers);

        AddressNotMailableDialog addressNotMailableDialog = new AddressNotMailableDialog();
        if (addressNotMailableDialog.isDisplayed())
        {
            logger.error(
                    "Most likely Trillium is not available. Assuming address is correct and continuing enrollment");
            addressNotMailableDialog.clickSaveAsIs();
        }

        EmailNotMailableDialog emailNotMailableDialog = new EmailNotMailableDialog();
        if (emailNotMailableDialog.isDisplayed())
        {
            logger.error(
                    "Most likely Email Validation Service is not available. Assuming the email address is correct and continuing enrollment");
            emailNotMailableDialog.clickSaveEmailAsIs();
        }
    }

    public void clickCancel(Verifier... verifiers)
    {
        container.getButton(Button.CANCEL).clickAndWait(verifiers);
    }

    @Override
    public void populate(Member member)
    {
        for (Program program : member.getPrograms().keySet())
        {
            getPrograms().selectProgram(program);
        }

        getCustomerInfo().populate(member.getPersonalInfo());

        if (CollectionUtils.isNotEmpty(member.getAddresses()))
        {
            getAddress().populate(member.getAddresses().get(0));
        }

        if (CollectionUtils.isNotEmpty(member.getPhones()))
        {
            getPhone().populate(member.getPhones().get(0));
        }

        if (CollectionUtils.isNotEmpty(member.getEmails()))
        {
            getEmail().populate(member.getEmails().get(0));
        }
    }

    public EnrollProgramContainer getPrograms()
    {
        return new EnrollProgramContainer(container);
    }

    public Member successEnrollmentPopUp(Member member, Verifier... verifiers)
    {
        SuccessEnrolmentPopUp popUp = new SuccessEnrolmentPopUp();

        member = popUp.putPrograms(member);
        popUp.clickDone(verifiers);

        return member;
    }

    public Member completeEnrollWithDuplicatePopUp(Member member)
    {
        clickSubmitAndSkipWarnings(assertNoError());

        PossibleDuplicateFoundPopUp possibleDuplicateFoundPopUp = new PossibleDuplicateFoundPopUp();
        if (possibleDuplicateFoundPopUp.isDisplayed())
        {
            possibleDuplicateFoundPopUp.clickContinue();
        }

        member = successEnrollmentPopUp(member);

        return member;
    }

    public void verifyDefault()
    {
        CustomerInfo customerInfo = getCustomerInfo();
        verifyThat(customerInfo, displayed(true));
        verifyThat(customerInfo.getName(), displayed(true));
        verifyThat(customerInfo.getResidenceCountry(), displayed(true));
        verifyThat(customerInfo.getWrittenLanguage(), displayed(true));

        Address address = getAddress();
        verifyThat(address, displayed(true));

        verifyThat(address.getCountry(), displayed(true));
        verifyThat(address.getAddress1(), displayed(true));
        verifyThat(address.getType(), displayed(true));

        Phone phone = getPhone();
        verifyThat(phone, displayed(true));
        verifyThat(phone.getNumber(), displayed(true));
        verifyThat(phone.getType(), displayed(true));
        verifyThat(phone.getPhoneSubType(), displayed(true));

        Email email = getEmail();
        verifyThat(email, displayed(true));
        verifyThat(email.getType(), displayed(true));
        verifyThat(email.getFormat(), displayed(true));
        verifyThat(email.getEmail(), displayed(true));
    }
}
