package com.ihg.automation.selenium.common.personal;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.gwt.components.utils.FindStepUtils.byXpath;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.isSelected;

import com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory;
import com.ihg.automation.selenium.common.contact.ContactBase;
import com.ihg.automation.selenium.common.member.GuestContact;
import com.ihg.automation.selenium.common.pages.MultiModeBase;
import com.ihg.automation.selenium.gwt.components.Button;
import com.ihg.automation.selenium.gwt.components.Component;
import com.ihg.automation.selenium.gwt.components.Mode;
import com.ihg.automation.selenium.gwt.components.checkbox.CheckBox;
import com.ihg.automation.selenium.gwt.components.dialog.ConfirmDialog;
import com.ihg.automation.selenium.gwt.components.panel.EditablePanel;
import com.ihg.automation.selenium.listener.Verifier;

public abstract class EditableContactBase<E extends GuestContact, T extends ContactBase<E>> extends MultiModeBase
        implements EditableComponent<E>
{
    private T baseContact;

    public EditableContactBase(EditablePanel parent, T baseContact)
    {
        super(parent);

        this.baseContact = baseContact;
    }

    public T getBaseContact()
    {
        return baseContact;
    }

    public CheckBox getPreferred()
    {
        return container.getCheckBox("Mark Preferred");
    }

    public Component getPreferredIcon()
    {
        return new Component(container, "Icon", "Preferred", byXpath(".//div[contains(@class, 'preferred-label')]"));
    }

    public CheckBox getInvalid()
    {
        return container.getCheckBox("Mark Invalid");
    }

    public Button getRemove()

    {
        return container.getButton("Remove");
    }

    public void clickRemove()

    {
        getRemove().clickAndWait();
    }

    @Override
    public void expand()
    {
        ((EditablePanel) container).expand();
    }

    @Override
    public void populate(E contact)
    {
        getBaseContact().populate(contact);

        if (!getPreferredIcon().isDisplayed())
        {
            getPreferred().check(contact.isPreferred());
        }
        if (getInvalid().isDisplayed())
        {
            getInvalid().check(!contact.isValid());
        }
    }

    @Override
    public void update(E contact, Verifier... verifiers)
    {
        clickEdit();
        populate(contact);
        clickSave(MessageBoxVerifierFactory.verifyNoError().add(verifiers));
    }

    @Override
    public void remove(Verifier... verifiers)
    {
        clickRemove();
        new ConfirmDialog().clickYes(MessageBoxVerifierFactory.verifyNoError().add(verifiers));
    }

    @Override
    public void verify(E contact, Mode mode)
    {
        getBaseContact().verify(contact, mode);

        if (mode == Mode.EDIT)
        {
            verifyPreferredInEdit(contact.isPreferred());
            boolean status = !contact.isValid();
            verifyThat(getInvalid(), isSelected(status));
        }
        else if (mode == Mode.VIEW)
        {
            verifyThat(getPreferredIcon(), displayed(contact.isPreferred()));
        }
    }

    public void verifyPreferredInEdit(Boolean preferred)
    {
        if (preferred)
        {
            verifyThat(getPreferredIcon(), displayed(true));
            verifyThat(getPreferred(), displayed(false));
        }
        else
        {
            verifyThat(getPreferredIcon(), displayed(false));
            verifyThat(getPreferred(), displayed(true));
            verifyThat(getPreferred(), isSelected(false));
        }
    }
}
