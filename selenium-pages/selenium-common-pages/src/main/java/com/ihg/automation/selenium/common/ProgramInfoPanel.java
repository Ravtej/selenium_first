package com.ihg.automation.selenium.common;

import com.ihg.automation.selenium.gwt.components.CustomContainerBase;
import com.ihg.automation.selenium.gwt.components.panel.FieldSet;

public class ProgramInfoPanel extends CustomContainerBase
{
    public ProgramInfoPanel()
    {
        super(new FieldSet("Program Information"));
    }

    public ProgramsGrid getPrograms()
    {
        return new ProgramsGrid(container);
    }

}
