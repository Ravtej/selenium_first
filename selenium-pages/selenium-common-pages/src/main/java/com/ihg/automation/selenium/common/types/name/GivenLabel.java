package com.ihg.automation.selenium.common.types.name;

import com.ihg.automation.selenium.common.types.LabelEnumAware;

public enum GivenLabel implements LabelEnumAware
{
    FIRST("First Name"), GIVEN("Given Name");

    private String label;

    private GivenLabel(String label)
    {
        this.label = label;
    }

    @Override
    public String getLabel()
    {
        return label;
    }
}
