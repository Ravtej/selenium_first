package com.ihg.automation.selenium.common.types.name;

public class Title
{
    private Title()
    {
    }

    public static final String OFFICIER = "Officier";
    public static final String SENATOR = "Senator";
    public static final String PROFESSEUR = "Professeur";
    public static final String MINISTRE = "Ministre";
    public static final String SENADOR = "Senador";
    public static final String DIRETTORE = "Direttore";
    public static final String PROF = "Prof.";
    public static final String THE_HON = "The Hon.";
    public static final String REP = "Rep.";
    public static final String AMBASSADEUR = "Ambassadeur";
    public static final String BURGEMEESTER = "Burgemeester";
    public static final String DIRECTEUR = "Directeur";
    public static final String EMBAJADOR = "Embajador";
    public static final String PROFESSORE = "Professore";
    public static final String PROFESSOR = "Professor";
    public static final String MINISTER = "Minister";
    public static final String DIRECTOR = "Director";
    public static final String EMBAIXADOR = "Embaixador";
    public static final String SINDACO = "Sindaco";
    public static final String AMBASCIATORE = "Ambasciatore";
    public static final String PARLEMENTAIRE = "Parlementaire";
    public static final String SENATORE = "Senatore";
    public static final String AMB = "Amb.";
    public static final String OFFICER = "Officer";
    public static final String MAIRE = "Maire";
    public static final String MINISTRO = "Ministro";
    public static final String ALCALDE = "Alcalde";
    public static final String MAYOR = "Mayor";
    public static final String PREFEITO = "Prefeito";
    public static final String PROFESOR = "Profesor";
    public static final String OFFIZIER = "Offizier";
}
