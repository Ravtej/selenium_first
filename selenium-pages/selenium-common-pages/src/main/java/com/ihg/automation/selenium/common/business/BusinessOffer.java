package com.ihg.automation.selenium.common.business;

public class BusinessOffer
{
    private String offerName;
    private String pointsToAward;
    private String estimatedCostToHotel;

    public BusinessOffer(String offerName, String pointsToAward)
    {
        this.offerName = offerName;
        this.pointsToAward = pointsToAward;
    }

    public String getOfferName()
    {
        return offerName;
    }

    public void setOfferName(String offerName)
    {
        this.offerName = offerName;
    }

    public String getPointsToAward()
    {
        return pointsToAward;
    }

    public void setPointsToAward(String pointsToAward)
    {
        this.pointsToAward = pointsToAward;
    }

    public String getEstimatedCostToHotel()
    {
        return estimatedCostToHotel;
    }

    public void setEstimatedCostToHotel(String estimatedCostToHotel)
    {
        this.estimatedCostToHotel = estimatedCostToHotel;
    }
}
