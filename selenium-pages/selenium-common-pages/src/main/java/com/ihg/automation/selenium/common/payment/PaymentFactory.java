package com.ihg.automation.selenium.common.payment;

import static com.ihg.automation.selenium.common.Constant.PCR;

import com.ihg.automation.selenium.common.Constant;
import com.ihg.automation.selenium.common.catalog.CatalogItem;
import com.ihg.automation.selenium.common.pages.PaymentMethod;
import com.ihg.automation.selenium.common.types.Currency;

public class PaymentFactory
{
    public static final Payment COMPLIMENTARY = new Payment(PaymentMethod.COMPLIMENTARY, "0.00",
            Constant.NOT_AVAILABLE);

    private PaymentFactory()
    {
    }

    public static Payment getPointsPayment(CatalogItem item)
    {
        return new Payment(PaymentMethod.LOYALTY_UNITS, item.getLoyaltyUnitCost(), PCR);
    }

    public static Payment getComplimentaryPayment(String usdAmount)
    {
        return new Payment(PaymentMethod.COMPLIMENTARY, usdAmount, Currency.USD.getCode());
    }
}
