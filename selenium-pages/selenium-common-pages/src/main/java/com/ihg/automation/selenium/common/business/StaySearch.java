package com.ihg.automation.selenium.common.business;

import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.CustomContainerBase;
import com.ihg.automation.selenium.gwt.components.input.DateInput;
import com.ihg.automation.selenium.gwt.components.input.Input;
import com.ihg.automation.selenium.gwt.components.label.Label;

public class StaySearch extends CustomContainerBase
{
    public StaySearch(Container container)
    {
        super(container);
    }

    public Input getGuestName()
    {
        return container.getInputByLabel("Guest Name");
    }

    public Input getConfirmationNumber()
    {
        return container.getInputByLabel("Confirmation Number");
    }

    public Label getHotel()
    {
        return container.getLabel("Hotel");
    }

    public DateInput getFromDate()
    {
        return new DateInput(container.getLabel("Date of Stay From"));
    }

    public DateInput getToDate()
    {
        return new DateInput(container.getLabel("To"));
    }

}
