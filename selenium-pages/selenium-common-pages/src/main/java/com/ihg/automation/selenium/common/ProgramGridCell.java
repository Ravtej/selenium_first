package com.ihg.automation.selenium.common;

import com.ihg.automation.selenium.gwt.components.grid.CellsName;

public enum ProgramGridCell implements CellsName
{
    CODE("code"), LEVEL_CODE("levelCode");

    private String name;

    private ProgramGridCell(String name)
    {
        this.name = name;
    }

    @Override
    public String getName()
    {
        return name;
    }
}
