package com.ihg.automation.selenium.common.name;

import com.ihg.automation.selenium.gwt.components.CustomContainerBase;

public interface PersonNameAware
{
    public CustomContainerBase getName();
}
