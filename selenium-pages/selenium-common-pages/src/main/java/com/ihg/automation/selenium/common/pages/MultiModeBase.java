package com.ihg.automation.selenium.common.pages;

import static com.ihg.automation.selenium.gwt.components.Button.CANCEL;
import static com.ihg.automation.selenium.gwt.components.Button.EDIT;
import static com.ihg.automation.selenium.gwt.components.Button.SAVE;

import com.ihg.automation.selenium.gwt.components.Button;
import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.CustomContainerBase;
import com.ihg.automation.selenium.gwt.components.utils.FindStep;
import com.ihg.automation.selenium.listener.Verifier;

public class MultiModeBase extends CustomContainerBase
{
    public MultiModeBase(Container container)
    {
        super(container);
    }

    public MultiModeBase(Container container, String type, String name, FindStep step)
    {
        super(container, type, name, step);
    }

    public Button getEdit()
    {
        return container.getButton(EDIT);
    }

    public void clickEdit(Verifier... verifiers)
    {
        getEdit().clickAndWait(verifiers);
    }

    public Button getSave()
    {
        return container.getButton(SAVE);
    }

    public void clickSave(Verifier... verifiers)
    {
        getSave().clickAndWait(verifiers);
    }

    public Button getCancel()
    {
        return container.getButton(CANCEL);
    }

    public void clickCancel(Verifier... verifiers)
    {
        getCancel().clickAndWait(verifiers);
    }
}
