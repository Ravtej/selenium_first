package com.ihg.automation.selenium.common;

import com.ihg.automation.selenium.common.pages.CreditCardType;
import com.ihg.automation.selenium.common.types.program.RewardClubLevel;

public class CoBrandedCreditCard
{
    private CreditCardType type;
    private String partner;
    private String creditCardName;
    private RewardClubLevel level;
    private String levelDuration;
    private String statusDate;
    private String status;

    public CreditCardType getType()
    {
        return type;
    }

    public void setType(CreditCardType type)
    {
        this.type = type;
    }

    public String getPartner()
    {
        return partner;
    }

    public void setPartner(String partner)
    {
        this.partner = partner;
    }

    public String getCreditCardName()
    {
        return creditCardName;
    }

    public void setCreditCardName(String creditCardName)
    {
        this.creditCardName = creditCardName;
    }

    public RewardClubLevel getLevel()
    {
        return level;
    }

    public void setLevel(RewardClubLevel level)
    {
        this.level = level;
    }

    public String getLevelDuration()
    {
        return levelDuration;
    }

    public void setLevelDuration(String levelDuration)
    {
        this.levelDuration = levelDuration;
    }

    public String getStatusDate()
    {
        return statusDate;
    }

    public void setStatusDate(String statusDate)
    {
        this.statusDate = statusDate;
    }

    public String getStatus()
    {
        return status;
    }

    public void setStatus(String status)
    {
        this.status = status;
    }
}
