package com.ihg.automation.selenium.common.order;

import com.ihg.automation.selenium.common.types.Currency;

public class VoucherRule
{
    public static final String DEFAULT_AMOUNT = "25";

    private String ruleType;
    private String promotionId;
    private String orderDescription;
    private String voucherName;
    private String costAmount;
    private String tierLevelCodeOfUpgrade;
    private String totalCostAmount;
    private String currencyCode = Currency.USD.getCode();
    private String unitsAmount;
    private String quantity = DEFAULT_AMOUNT;
    private String pointVoucherDenomination;

    public VoucherRule(String promotionId, String orderDescription)
    {
        this.promotionId = promotionId;
        this.orderDescription = orderDescription;
    }

    public String getRuleType()
    {
        return ruleType;
    }

    public void setRuleType(VoucherRuleType ruleType)
    {
        this.ruleType = ruleType.toString();
    }

    public String getPromotionId()
    {
        return promotionId;
    }

    public void setPromotionId(String promotionId)
    {
        this.promotionId = promotionId;
    }

    public String getOrderDescription()
    {
        return orderDescription;
    }

    public void setOrderDescription(String voucherName)
    {
        this.orderDescription = voucherName;
    }

    public String getVoucherName()
    {
        return voucherName;
    }

    public void setVoucherName(String voucherName)
    {
        this.voucherName = voucherName;
    }

    public String getCostAmount()
    {
        return costAmount;
    }

    public String getCostAmountWithCurrency()
    {
        return String.format("%s %s", costAmount, currencyCode);
    }

    public void setCostAmount(String costAmount)
    {
        this.costAmount = costAmount;
    }

    public String getTierLevelCodeOfUpgrade()
    {
        return tierLevelCodeOfUpgrade;
    }

    public void setTierLevelCodeOfUpgrade(String tierLevelCodeOfUpgrade)
    {
        this.tierLevelCodeOfUpgrade = tierLevelCodeOfUpgrade;
    }

    public String getCurrencyCode()
    {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode)
    {
        this.currencyCode = currencyCode;
    }

    public String getQuantity()
    {
        return quantity;
    }

    public void setQuantity(String quantity)
    {
        this.quantity = quantity;
    }

    public String getUnitsAmount()
    {
        return unitsAmount;
    }

    public void setUnitsAmount(String unitsAmount)
    {
        this.unitsAmount = unitsAmount;
    }

    public String getTotalCostAmount()
    {
        return totalCostAmount;
    }

    public String getTotalCostAmountWithCurrency()
    {
        return String.format("%s %s", totalCostAmount, currencyCode);
    }

    public void setTotalCostAmount(String totalCostAmount)
    {
        this.totalCostAmount = totalCostAmount;
    }

    public String getPointVoucherDenomination()
    {
        return pointVoucherDenomination;
    }

    public void setPointVoucherDenomination(String pointVoucherDenomination)
    {
        this.pointVoucherDenomination = pointVoucherDenomination;
    }
}
