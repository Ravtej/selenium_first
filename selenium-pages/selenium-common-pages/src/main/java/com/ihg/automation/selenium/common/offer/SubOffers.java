package com.ihg.automation.selenium.common.offer;

import org.apache.commons.lang3.StringUtils;

public class SubOffers
{
    private String name;
    private String code;
    private String description;
    private String reward;
    private String goal;
    private String events;
    private String status;
    private String lastUpdatedDate;
    private String awardPosted = StringUtils.EMPTY;

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getCode()
    {
        return code;
    }

    public void setCode(String code)
    {
        this.code = code;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public String getReward()
    {
        return reward;
    }

    public void setReward(String reward)
    {
        this.reward = reward;
    }

    public String getGoal()
    {
        return goal;
    }

    public void setGoal(String goal)
    {
        this.goal = goal;
    }

    public String getEvents()
    {
        return events;
    }

    public void setEvents(String events)
    {
        this.events = events;
    }

    public String getStatus()
    {
        return status;
    }

    public void setStatus(String status)
    {
        this.status = status;
    }

    public String getLastUpdatedDate()
    {
        return lastUpdatedDate;
    }

    public void setLastUpdatedDate(String lastUpdatedDate)
    {
        this.lastUpdatedDate = lastUpdatedDate;
    }

    public String getAwardPosted()
    {
        return awardPosted;
    }

    public void setAwardPosted(String awardPosted)
    {
        this.awardPosted = awardPosted;
    }

}
