package com.ihg.automation.selenium.common.types.name;

import com.ihg.automation.selenium.common.types.LabelEnumAware;

public enum DegreeLabel implements LabelEnumAware
{
    DEGREE("Degree"), TITLE("Title"), HONORIFIC("Honorific");

    private String label;

    private DegreeLabel(String label)
    {
        this.label = label;
    }

    @Override
    public String getLabel()
    {
        return label;
    }
}
