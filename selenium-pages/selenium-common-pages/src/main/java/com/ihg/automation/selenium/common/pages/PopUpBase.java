package com.ihg.automation.selenium.common.pages;

import com.ihg.automation.selenium.gwt.components.CustomContainerBase;
import com.ihg.automation.selenium.gwt.components.PopUpWindow;

public abstract class PopUpBase extends CustomContainerBase
{
    public PopUpBase()
    {
    }

    public PopUpBase(PopUpWindow popUp)
    {
        super(popUp);
    }

    public PopUpBase(String header)
    {
        this(PopUpWindow.withHeader(header));
    }

}
