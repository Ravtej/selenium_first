package com.ihg.automation.selenium.common.hotel;

import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.CustomContainerBase;
import com.ihg.automation.selenium.gwt.components.label.Label;

public class HotelDetails extends CustomContainerBase
{
    public HotelDetails(Container parent)
    {
        super(parent);
    }

    public Label getHotel()
    {
        return container.getLabel("Hotel");
    }

    public Label getLocationNumber()
    {
        return container.getLabel("Location Number");
    }

    public Label getFacilityId()
    {
        return container.getLabel("Facility ID");
    }

    public Label getCountryCurrency()
    {
        return container.getLabel("Country Currency");
    }

    public Label getHotelCurrency()
    {
        return container.getLabel("Hotel Currency");
    }

    public Label getOpenDate()
    {
        return container.getLabel("Open Date");
    }

    public Label getNoOfRooms()
    {
        return container.getLabel("No. of Rooms");
    }

    public Label getCountryVat()
    {
        return container.getLabel("Country's VAT");
    }

    public Label getHotelVat()
    {
        return container.getLabel("Hotel's VAT");
    }

    public Label getBrand()
    {
        return container.getLabel("Brand");
    }

    public Label getChain()
    {
        return container.getLabel("Chain");
    }

    public Label getStatus()
    {
        return container.getLabel("Status");
    }

    public Label getPhone()
    {
        return container.getLabel("Phone");
    }

    public Label getFax()
    {
        return container.getLabel("Fax");
    }
}
