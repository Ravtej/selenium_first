package com.ihg.automation.selenium.common.payment;

public interface PaymentDetailsAware
{
    public PaymentDetails getPaymentDetails();
}
