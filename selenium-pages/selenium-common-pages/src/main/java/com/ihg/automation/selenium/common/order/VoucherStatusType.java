package com.ihg.automation.selenium.common.order;

public enum VoucherStatusType
{
    Created, Active, Deposited, Expired, Voided
}
