package com.ihg.automation.selenium.common.offer;

import static com.ihg.automation.selenium.common.offer.Offer.builder;

public class OfferFactory
{
    public static final String FREE_NIGHT_OFFER_CODE = "8630441";

    public static final Offer DR_FREE_NIGHT = builder(FREE_NIGHT_OFFER_CODE, "IHG Dining Rewards - Free Night")
            .description("Enroll into IHG Dining Rewards and get a Free Night").freeNight("Yes")
            .rateCategoryCode("IVDD1").maximumWinsAllowed("Unlimited").maxDaysToPlay("Unlimited").status("Open")
            .build();

    public static final Offer DELAYED_FULFILMENT_GLOBAL = builder("8050504", "DELAYED FULFILMENT GLOBAL")
            .description("DELAYED FULFILMENT GLOBAL").build();

}
