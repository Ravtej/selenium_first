package com.ihg.automation.selenium.common;

import static com.ihg.automation.selenium.common.Constant.NOT_AVAILABLE;
import static com.ihg.automation.selenium.common.Constant.RC_POINTS;

import com.ihg.automation.selenium.common.member.GuestEmail;
import com.ihg.automation.selenium.common.member.GuestName;
import com.ihg.automation.selenium.common.member.MeetingPhone;
import com.ihg.automation.selenium.common.pages.PaymentType;

public class MeetingDetails
{
    private String meetingEventName = NOT_AVAILABLE;
    private String contractBookingDate = null;
    private String meetingEventType = NOT_AVAILABLE;
    private String totalNumberOfRoomNights = NOT_AVAILABLE;
    private GuestName salesManager = new GuestName();
    private PaymentType paymentType = null;
    private GuestEmail salesManagerEmail = new GuestEmail();
    private MeetingPhone salesManagerPhone = new MeetingPhone();
    private String totalRoomsRevenue = NOT_AVAILABLE;
    private String totalQualifiedNonRoomsRevenue = NOT_AVAILABLE;
    private String negotiatedRoomRate = null;

    private String hotel = null;
    private String checkInDate = null;
    private String checkOutDate = null;
    private String totalLoyaltyUnitsToAwardAmount = null;
    private String totalLoyaltyUnitsToAwardType = null;

    public MeetingDetails(String hotel, String checkInDate, String checkOutDate, String awardUnitsAmount,
            String awardUnitsType, String roomRateUSD)
    {
        setHotel(hotel);
        setCheckInDate(checkInDate);
        setCheckOutDate(checkOutDate);
        setTotalLoyaltyUnitsToAwardAmount(awardUnitsAmount);
        setTotalLoyaltyUnitsToAwardType(awardUnitsType);
        setNegotiatedRoomRate(roomRateUSD);
    }

    public void setMeetingEventName(String meetingEventName)
    {
        this.meetingEventName = meetingEventName;
    }

    public String getMeetingEventName()
    {
        return meetingEventName;
    }

    public void setContractBookingDate(String contractBookingDate)
    {
        this.contractBookingDate = contractBookingDate;
    }

    public String getContractBookingDate()
    {
        return contractBookingDate;
    }

    public void setMeetingEventType(String meetingEventType)
    {
        this.meetingEventType = meetingEventType;
    }

    public String getMeetingEventType()
    {
        return meetingEventType;
    }

    public void setTotalNumberOfRoomNights(String totalNumberOfRoomNights)
    {
        this.totalNumberOfRoomNights = totalNumberOfRoomNights;
    }

    public String getTotalNumberOfRoomNights()
    {
        return totalNumberOfRoomNights;
    }

    public void setSalesManager(GuestName salesManager)
    {
        this.salesManager = salesManager;
    }

    public GuestName getSalesManager()
    {
        return salesManager;
    }

    public void setPaymentType(PaymentType paymentType)
    {
        this.paymentType = paymentType;
    }

    public PaymentType getPaymentType()
    {
        return paymentType;
    }

    public void setSalesManagerEmail(GuestEmail salesManagerEmail)
    {
        this.salesManagerEmail = salesManagerEmail;
    }

    public GuestEmail getSalesManagerEmail()
    {
        return salesManagerEmail;
    }

    public void setNegotiatedRoomRate(String negotiatedRoomRate)
    {
        this.negotiatedRoomRate = negotiatedRoomRate;
    }

    public String getNegotiatedRoomRate()
    {
        return negotiatedRoomRate;
    }

    public void setSalesManagerPhone(MeetingPhone salesManagerPhone)
    {
        this.salesManagerPhone = salesManagerPhone;
    }

    public MeetingPhone getSalesManagerPhone()
    {
        return salesManagerPhone;
    }

    public void setTotalRoomsRevenue(String totalRoomsRevenue)
    {
        this.totalRoomsRevenue = totalRoomsRevenue;
    }

    public String getTotalRoomsRevenue()
    {
        return totalRoomsRevenue;
    }

    public void setTotalQualifiedNonRoomsRevenue(String totalQualifiedNonRoomsRevenue)
    {
        this.totalQualifiedNonRoomsRevenue = totalQualifiedNonRoomsRevenue;
    }

    public String getTotalQualifiedNonRoomsRevenue()
    {
        return totalQualifiedNonRoomsRevenue;
    }

    public void setHotel(String hotel)
    {
        this.hotel = hotel;
    }

    public String getHotel()
    {
        return hotel;
    }

    public void setCheckInDate(String checkInDate)
    {
        this.checkInDate = checkInDate;
    }

    public String getCheckInDate()
    {
        return checkInDate;
    }

    public void setCheckOutDate(String checkOutDate)
    {
        this.checkOutDate = checkOutDate;
    }

    public String getCheckOutDate()
    {
        return checkOutDate;
    }

    public void setTotalLoyaltyUnitsToAwardAmount(String totalLoyaltyUnitsToAwardAmount)
    {
        this.totalLoyaltyUnitsToAwardAmount = totalLoyaltyUnitsToAwardAmount;
    }

    public String getTotalLoyaltyUnitsToAwardAmount()
    {
        return totalLoyaltyUnitsToAwardAmount;
    }

    public void setTotalLoyaltyUnitsToAwardType(String totalLoyaltyUnitsToAwardType)
    {
        this.totalLoyaltyUnitsToAwardType = totalLoyaltyUnitsToAwardType;
    }

    public String getTotalLoyaltyUnitsToAwardType()
    {
        return totalLoyaltyUnitsToAwardType;
    }

    public void setDefaults()
    {
        this.meetingEventName = NOT_AVAILABLE;
        this.contractBookingDate = NOT_AVAILABLE;
        this.meetingEventType = NOT_AVAILABLE;
        this.totalNumberOfRoomNights = NOT_AVAILABLE;
        this.paymentType = PaymentType.CASH;
        this.totalRoomsRevenue = NOT_AVAILABLE;
        this.totalQualifiedNonRoomsRevenue = NOT_AVAILABLE;
        this.totalLoyaltyUnitsToAwardType = RC_POINTS;
    }

}
