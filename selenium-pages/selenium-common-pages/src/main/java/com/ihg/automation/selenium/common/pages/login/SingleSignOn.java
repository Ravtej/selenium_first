package com.ihg.automation.selenium.common.pages.login;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.assertThat;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;

import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.Alert;
import org.openqa.selenium.NoAlertPresentException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ihg.automation.selenium.common.User;
import com.ihg.automation.selenium.gwt.components.Component;
import com.ihg.automation.selenium.gwt.components.CustomContainerBase;
import com.ihg.automation.selenium.gwt.components.PopUpWindow;
import com.ihg.automation.selenium.gwt.components.utils.FindStepUtils;
import com.ihg.automation.selenium.gwt.components.utils.WaitUtils;
import com.ihg.automation.selenium.html.components.HtmlInput;
import com.ihg.automation.selenium.html.components.HtmlSelect;
import com.ihg.automation.selenium.runtime.Selenium;

public class SingleSignOn extends CustomContainerBase
{
    private static final Logger logger = LoggerFactory.getLogger(SingleSignOn.class);

    private static final String BLANK_URL = "about:blank";

    protected Component getSubmit()
    {
        return new Component(container, "Button", "Continue",
                FindStepUtils.byXpathWithWait(".//input[@name='Submit']"));
    }

    public HtmlInput getUser()
    {
        return new HtmlInput(container, "USER");
    }

    public HtmlInput getPassword()
    {
        HtmlInput password = new HtmlInput(container, "PASSWORD");
        password.setIsHiddenField(true);
        return password;
    }

    public HtmlSelect getDomain()
    {
        return new HtmlSelect(container, "DOMAIN");
    }

    public void clickSubmit()
    {
        getSubmit().clickAndWait();
    }

    public void submitLoginDetails(String username, String password, String domain)
    {
        String noLogin = System.getProperty("noLogin");

        if (noLogin == null)
        {
            getUser().type(username);
            getPassword().type(password);
            getDomain().select(domain);
            clickSubmit();
        }
        else
        {
            assertThat(getUser(), displayed(false), "No DSSO page is expected (please remove 'noLogin' param)");
        }
    }

    public void submitLoginDetails(User user)
    {
        submitLoginDetails(user.getUserId(), user.getPassword(), user.getDomain());

        assertThat(PopUpWindow.withHeader("Authentication Error"), displayed(false));
    }

    public void signOff(String url)
    {
        if (Selenium.getInstance().getDriver().getCurrentUrl().equals(BLANK_URL) && getUser().isDisplayed())
        {
            return;
        }

        if (!StringUtils.endsWith(url, "/"))
        {
            throw new RuntimeException(String.format("URL address [%s] must ends with '/'", url));
        }

        url += "signoff";
        logger.debug("Sign Off by opening [{}] page", url);
        Selenium.getInstance().getDriver().get(url);

        try
        {
            Alert alert = Selenium.getInstance().getDriver().switchTo().alert();
            logger.debug("Accept the alert: " + alert.getText());
            alert.accept();
            Selenium.getInstance().getDriver().switchTo().window(Selenium.getInstance().getDriver().getWindowHandle());
        }
        catch (NoAlertPresentException e)
        {
            logger.debug("No alerts during Sign Off");
        }

        new WaitUtils().waitExecutingRequest();
    }
}
