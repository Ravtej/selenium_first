package com.ihg.automation.selenium.common.types;

public class ExtendReason
{
    public static final String AGENT_ERROR = "Agent Error";
    public static final String COBRAND_CARDHOLDER = "Cobrand Cardholder";
    public static final String COMMUNICATION_ERROR = "Communication Error";
    public static final String CUSTOMER_RETENTION = "Customer Retention";
    public static final String SYSTEM_ERROR_ISSUES = "System Error/Issues";

    private ExtendReason()
    {
    }
}
