package com.ihg.automation.selenium.common.personal;

import com.ihg.automation.selenium.common.member.GuestEmail;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.gwt.components.Button;
import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.panel.EditablePanel;

public class EmailContactList extends ContactListBase<GuestEmail, EmailContact>
{
    public EmailContactList(Container container)
    {
        super(container.getSeparator("E-mail"));
    }

    @Override
    public EmailContact getContact(int index)
    {
        return new EmailContact(new EditablePanel(container, index));
    }

    @Override
    public Button getAddButton()
    {
        return container.getButton("Add Email");
    }

    @Override
    protected GuestEmail getRandomContact()
    {
        return MemberPopulateHelper.getRandomEmail();
    }
}
