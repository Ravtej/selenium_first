package com.ihg.automation.selenium.common.hotel;

import com.ihg.automation.selenium.common.event.SearchAware;
import com.ihg.automation.selenium.common.event.SearchButtonBar;
import com.ihg.automation.selenium.common.pages.ClosablePopUpBase;
import com.ihg.automation.selenium.gwt.components.Button;

public class AdvancedHotelSearchPopUp extends ClosablePopUpBase implements SearchAware
{
    public AdvancedHotelSearchPopUp()
    {
        super("Advanced Hotel Search");
    }

    @Override
    public AdvancedHotelSearch getSearchFields()
    {
        return new AdvancedHotelSearch(container);
    }

    @Override
    public SearchButtonBar getButtonBar()
    {
        return new SearchButtonBar(container);
    }

    public Button getCancel()
    {
        return container.getButton("Cancel");
    }

    public Button getSelectHotel()
    {
        return container.getButton("Select Hotel");
    }

    public AdvancedHotelSearchGrid getHotelSearchGrid()
    {
        return new AdvancedHotelSearchGrid(container);
    }

    public void clickCancel()
    {
        getCancel().clickAndWait();
    }

    public void clickSelectHotel()
    {
        getSelectHotel().clickAndWait();
    }
}
