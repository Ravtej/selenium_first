package com.ihg.automation.selenium.common.payment;

import com.ihg.automation.selenium.common.catalog.CatalogItem;
import com.ihg.automation.selenium.common.pages.PaymentMethod;

public class CashPayment extends Payment
{
    private String name = "";
    private String location = "";
    private String depositDate = "";
    private String depositNumber = "";

    public CashPayment(String amount, String currency, String name, String location, String depositDate,
            String depositNumber)
    {
        super(PaymentMethod.CASH, amount, currency);
        this.name = name;
        this.location = location;
        this.depositDate = depositDate;
        this.depositNumber = depositNumber;
    }

    public CashPayment(CatalogItem catalogItem, String name, String location, String depositDate, String depositNumber)
    {
        super(PaymentMethod.CASH, catalogItem);
        this.name = name;
        this.location = location;
        this.depositDate = depositDate;
        this.depositNumber = depositNumber;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getLocation()
    {
        return location;
    }

    public void setLocation(String location)
    {
        this.location = location;
    }

    public String getDepositDate()
    {
        return depositDate;
    }

    public void setDepositDate(String depositDate)
    {
        this.depositDate = depositDate;
    }

    public String getDepositNumber()
    {
        return depositNumber;
    }

    public void setDepositNumber(String depositNumber)
    {
        this.depositNumber = depositNumber;
    }
}
