package com.ihg.automation.selenium.common.types.program;

import java.util.ArrayList;

public enum KarmaLevelReason implements ProgramLevelReason
{
    BASE("S", "Base"), //
    EXECUTIVE("E", "Executive"), //
    GRACE("G", "Grace"), //
    POINTS("P", "Points"), //
    VIP("V", "VIP");

    private String code;
    private String value;

    private KarmaLevelReason(String code, String description)
    {
        this.code = code;
        this.value = description;
    }

    @Override
    public String getCode()
    {
        return code;
    }

    @Override
    public String getValue()
    {
        return value;
    }

    @Override
    public String getCodeWithValue()
    {
        return code + " - " + value;
    }

    public String toString()
    {
        return getValue();
    }

    public static ArrayList<String> getValuesList()
    {
        KarmaLevelReason[] vals = values();
        ArrayList<String> res = new ArrayList<String>();
        for (KarmaLevelReason val : vals)
        {
            res.add(val.getValue());
        }

        return res;
    }
}
