package com.ihg.automation.selenium.common.hotel;

import com.ihg.automation.selenium.common.pages.ClosablePopUpBase;

public class HotelDetailsPopUp extends ClosablePopUpBase
{
    public HotelDetailsPopUp()
    {
        super("Hotel Details");
    }

    public HotelDetails getHotelDetails(){
        return new HotelDetails(container);
    }
}
