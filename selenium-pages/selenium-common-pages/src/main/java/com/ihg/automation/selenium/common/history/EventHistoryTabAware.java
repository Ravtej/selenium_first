package com.ihg.automation.selenium.common.history;

public interface EventHistoryTabAware
{
    public EventHistoryTab getHistoryTab();
}
