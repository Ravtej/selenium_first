package com.ihg.automation.selenium.common.payment;

import com.ihg.automation.selenium.common.catalog.CatalogItem;
import com.ihg.automation.selenium.common.pages.PaymentMethod;

public class CheckPayment extends Payment
{
    private String checkNumber;
    private String nameOnAccount = "";
    private String issuingBank = "";
    private String depositDate = "";
    private String depositNumber = "";

    public CheckPayment(String amount, String currency, String checkNumber, String nameOnAccount, String issuingBank,
            String depositDate, String depositNumber)
    {
        super(PaymentMethod.CHECK, amount, currency);
        this.checkNumber = checkNumber;
        this.nameOnAccount = nameOnAccount;
        this.issuingBank = issuingBank;
        this.depositDate = depositDate;
        this.depositNumber = depositNumber;
    }

    public CheckPayment(CatalogItem catalogItem, String checkNumber, String nameOnAccount, String issuingBank,
            String depositDate, String depositNumber)
    {
        super(PaymentMethod.CHECK, catalogItem);
        this.checkNumber = checkNumber;
        this.nameOnAccount = nameOnAccount;
        this.issuingBank = issuingBank;
        this.depositDate = depositDate;
        this.depositNumber = depositNumber;
    }

    public String getCheckNumber()
    {
        return checkNumber;
    }

    public void setCheckNumber(String checkNumber)
    {
        this.checkNumber = checkNumber;
    }

    public String getNameOnAccount()
    {
        return nameOnAccount;
    }

    public void setNameOnAccount(String nameOnAccount)
    {
        this.nameOnAccount = nameOnAccount;
    }

    public String getIssuingBank()
    {
        return issuingBank;
    }

    public void setIssuingBank(String issuingBank)
    {
        this.issuingBank = issuingBank;
    }

    public String getDepositDate()
    {
        return depositDate;
    }

    public void setDepositDate(String depositDate)
    {
        this.depositDate = depositDate;
    }

    public String getDepositNumber()
    {
        return depositNumber;
    }

    public void setDepositNumber(String depositNumber)
    {
        this.depositNumber = depositNumber;
    }
}
