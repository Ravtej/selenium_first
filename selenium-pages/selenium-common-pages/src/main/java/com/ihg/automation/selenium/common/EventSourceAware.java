package com.ihg.automation.selenium.common;

public interface EventSourceAware
{
    public EventSource getSource();
}
