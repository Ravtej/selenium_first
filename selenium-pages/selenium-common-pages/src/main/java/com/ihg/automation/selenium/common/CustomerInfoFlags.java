package com.ihg.automation.selenium.common;

import static com.ihg.automation.selenium.gwt.components.utils.FindStepUtils.byXpath;
import static java.text.MessageFormat.format;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.ihg.automation.selenium.gwt.components.Component;
import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.CustomContainerBase;
import com.ihg.automation.selenium.gwt.components.utils.ByText;

public class CustomerInfoFlags extends CustomContainerBase
{
    protected CustomerInfoFlags(Container container)
    {
        super(new Container(container.getFieldSet("Profile Flags"), "Separator Area", "Flags",
                byXpath("./parent::div")));
    }

    public Component getFlag(String flag)
    {
        String xpath = format(".//div[contains(@class, 'flag') and {0}]", ByText.exact(flag));

        return new Component(container, "Label", "Flag: " + flag, byXpath(xpath));
    }

    public List<String> getFlags()
    {
        By xpath = By.xpath(".//div[contains(@class, 'flag')]");
        List<WebElement> flagList = findElements(xpath, "Flags list");
        List<String> elements = new ArrayList<String>();
        for (WebElement element : flagList)
        {
            elements.add(element.getText());
        }
        return elements;
    }
}
