package com.ihg.automation.selenium.common.award;

public class AwardDetails
{
    private String nameOfItem;
    private String itemID;
    private String description;
    private String deliveryMethod;
    private String estimatedDeliveryTime;
    private String vendor;
    private String status;
    private String startDateOfItem;
    private String endDateOfItem;

    public String getNameOfItem()
    {
        return nameOfItem;
    }

    public void setNameOfItem(String nameOfItem)
    {
        this.nameOfItem = nameOfItem;
    }

    public String getItemID()
    {
        return itemID;
    }

    public void setItemID(String itemID)
    {
        this.itemID = itemID;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public String getEstimatedDeliveryTime()
    {
        return estimatedDeliveryTime;
    }

    public void setEstimatedDeliveryTime(String estimatedDeliveryTime)
    {
        this.estimatedDeliveryTime = estimatedDeliveryTime;
    }

    public String getDeliveryMethod()
    {
        return deliveryMethod;
    }

    public void setDeliveryMethod(String deliveryMethod)
    {
        this.deliveryMethod = deliveryMethod;
    }

    public String getVendor()
    {
        return vendor;
    }

    public void setVendor(String vendor)
    {
        this.vendor = vendor;
    }

    public String getStatus()
    {
        return status;
    }

    public void setStatus(String status)
    {
        this.status = status;
    }

    public String getStartDateOfItem()
    {
        return startDateOfItem;
    }

    public void setStartDateOfItem(String startDateOfItem)
    {
        this.startDateOfItem = startDateOfItem;
    }

    public String getEndDateOfItem()
    {
        return endDateOfItem;
    }

    public void setEndDateOfItem(String endDateOfItem)
    {
        this.endDateOfItem = endDateOfItem;
    }

}
