package com.ihg.automation.selenium.common.payment;

import com.ihg.automation.selenium.gwt.components.grid.CellsName;
import com.ihg.automation.selenium.gwt.components.grid.Grid;
import com.ihg.automation.selenium.gwt.components.grid.GridRow;
import com.ihg.automation.selenium.gwt.components.grid.GridRowXpath;

public class BenefitsDetailGridRow extends GridRow<BenefitsDetailGridRow.BenefitsDetailGridCell>
{
    public BenefitsDetailGridRow(Grid parent, GridRowXpath xpath)
    {
        super(parent, xpath);
    }

    public enum BenefitsDetailGridCell implements CellsName
    {
        BENEFIT_AWARD("awardId"), NAME("awardName"), QUANTITY("quantity");

        private String name;

        private BenefitsDetailGridCell(String name)
        {
            this.name = name;
        }

        @Override
        public String getName()
        {
            return name;
        }
    }
}
