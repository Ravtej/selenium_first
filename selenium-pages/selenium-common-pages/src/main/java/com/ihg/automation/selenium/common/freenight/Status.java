package com.ihg.automation.selenium.common.freenight;

public class Status
{
    public static final String AVAILABLE = "Available";
    public static final String REDEEMED = "Redeemed";
    public static final String CANCELED = "Canceled";

    private Status()
    {
    }
}
