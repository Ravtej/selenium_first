package com.ihg.automation.selenium.common.pages.login;

import static com.ihg.automation.selenium.gwt.components.utils.FindStepUtils.byXpathWithWait;

import com.ihg.automation.selenium.gwt.components.CustomContainerBase;

public class LoginErrorBox extends CustomContainerBase
{
    private static final String ERROR_TYPE = "Error message";

    public static final String USER_ERROR_MESSAGE = "Please complete all the fields above!";
    public static final String CREDENTIALS_MISMATCH_ERROR_MESSAGE = "Looks like you have entered invalid credentials!";
    public static final String CREDENTIALS_MISMATCH_NEW_ERROR_MESSAGE = "Invalid credentials!";

    public LoginErrorBox()
    {
        super(ERROR_TYPE, "All errors", byXpathWithWait(".//div[@id='errMessage2' or @id='errMessage3']"));
    }
}
