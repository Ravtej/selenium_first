package com.ihg.automation.selenium.common.hotel;

public class HotelCertificateStatus
{
    private HotelCertificateStatus()
    {
    }

    public static String INITIATED = "INITIATED";
    public static String ACCEPTED = "ACCEPTED";
    public static String PAID = "PAID";
    public static String CANCELED = "CANCELED";
}
