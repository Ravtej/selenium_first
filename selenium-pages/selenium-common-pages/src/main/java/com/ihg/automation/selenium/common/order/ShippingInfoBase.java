package com.ihg.automation.selenium.common.order;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.order.DeliveryOption.EMAIL;
import static com.ihg.automation.selenium.common.order.DeliveryOption.PAPER;
import static com.ihg.automation.selenium.matchers.component.HasDefaultText.hasDefault;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;

import com.ihg.automation.selenium.common.CustomerInfoPanel;
import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.CustomContainerBase;
import com.ihg.automation.selenium.gwt.components.label.Label;

public abstract class ShippingInfoBase extends CustomContainerBase
{
    private final static int ORDER_RECIPIENT_COLUMN_LIMIT = 50;

    public ShippingInfoBase(Container container)
    {
        super(container);
    }

    public Label getName()
    {
        return container.getLabel("Name");
    }

    public Label getAddress()
    {
        return container.getLabel("Delivery Address");
    }

    abstract public Label getEmail();

    /**
     * Truncate full name because of the limit of 50 elements in column
     * fmds.TMORDER in DB
     */
    public void verify(Order order, Boolean isNameTruncate)
    {
        String fullName = order.getName();

        if (isNameTruncate)
        {
            fullName = fullName.substring(0, ORDER_RECIPIENT_COLUMN_LIMIT);
        }

        verifyThat(getEmail(), hasText(order.getDeliveryEmail()));

        verifyThat(getName(), hasText(fullName));

        verifyThat(getAddress(), hasText(order.getDeliveryAddress().getFormattedAddress()));
    }

    public void verify(Order order)
    {
        verify(order, false);
    }

    /**
     * Truncate full name because of the limit of 50 elements in column
     * fmds.TMORDER in DB
     */
    public void verifyAsOnCustomerInformation(Boolean isNameTruncate)
    {
        CustomerInfoPanel customerInfo = new CustomerInfoPanel();
        String fullName = customerInfo.getFullName().getText();

        if (isNameTruncate)
        {
            fullName = fullName.substring(0, ORDER_RECIPIENT_COLUMN_LIMIT);
        }

        verifyThat(getEmail(), hasText(customerInfo.getEmail().getText()));
        verifyThat(getName(), hasText(fullName));
        verifyThat(getAddress(), hasText(customerInfo.getAddress().getText()));
    }

    public void verifyAsOnCustomerInformation()
    {
        verifyAsOnCustomerInformation(false);
    }

    public void verifyAsOnCustomerInformation(String deliveryOption)
    {
        CustomerInfoPanel customerInfo = new CustomerInfoPanel();

        verifyThat(getName(), hasText(customerInfo.getFullName().getText()));

        if (EMAIL.equals(deliveryOption))
        {
            verifyThat(getEmail(), hasText(customerInfo.getEmail().getText()));
            verifyThat(getAddress(), hasDefault());
        }
        else if (PAPER.equals(deliveryOption))
        {
            verifyThat(getEmail(), hasDefault());
            verifyThat(getAddress(), hasText(customerInfo.getAddress().getText()));
        }
    }
}
