package com.ihg.automation.selenium.common.business;

import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.label.Label;

public class OffersFieldsView extends OffersFieldsBase
{
    public OffersFieldsView(Container container)
    {
        super(container.getSeparator("Offers"));
    }

    @Override
    public Label getOffer()
    {
        return container.getLabel("Offer");
    }

    @Override
    public Label getPointsToAward()
    {
        return container.getLabel("Points to Award");
    }
}
