package com.ihg.automation.selenium.common.types;

import com.ihg.automation.selenium.gwt.components.EntityType;

public enum Alliance implements EntityType
{
    DJA("DJA", "VIRGIN BLUE"), //
    BAA("BAA", "BRITISH AIRWAYS"), //
    AZL("AZL", "ALITALIA"), //
    JAL("JAL", "JAPAN AIRLINES"), //
    SUA("SUA", "AEROFLOT RUSSIAN AIRLINES"), //
    NZA("NZA", "AIR NEW ZEALAND");

    private String code;
    private String value;

    private Alliance(String code, String value)
    {
        this.code = code;
        this.value = value;
    }

    @Override
    public String getCode()
    {
        return code;
    }

    @Override
    public String getValue()
    {
        return value;
    }

    @Override
    public String toString()
    {
        return getValue();
    }

    @Override
    public String getCodeWithValue()
    {
        return code + " - " + value;
    }
}
