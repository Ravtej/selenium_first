package com.ihg.automation.selenium.common.offer;

import com.ihg.automation.common.DateUtils;
import com.ihg.automation.selenium.common.event.Source;

public class GuestOffer
{
    private Offer offer;

    private String registrationCode;

    private String registrationDate;
    private String lastDayToPlay;

    private String retroDate;

    private String status;
    private String replaysCount;
    private String achievedToDate;

    private Source source;

    public GuestOffer(Offer offer)
    {
        this.offer = offer;
        registrationDate = DateUtils.getFormattedDate();
    }

    public Offer getOffer()
    {
        return offer;
    }

    public void setOffer(Offer offer)
    {
        this.offer = offer;
    }

    public String getRegistrationCode()
    {
        return registrationCode;
    }

    public void setRegistrationCode(String registrationCode)
    {
        this.registrationCode = registrationCode;
    }

    public String getRegistrationDate()
    {
        return registrationDate;
    }

    public void setRegistrationDate(String registrationDate)
    {
        this.registrationDate = registrationDate;
    }

    public String getLastDayToPlay()
    {
        return lastDayToPlay;
    }

    public void setLastDayToPlay(String lastDayToPlay)
    {
        this.lastDayToPlay = lastDayToPlay;
    }

    public String getRetroDate()
    {
        return retroDate;
    }

    public void setRetroDate(String retroDate)
    {
        this.retroDate = retroDate;
    }

    public String getStatus()
    {
        return status;
    }

    public void setStatus(String status)
    {
        this.status = status;
    }

    public String getReplaysCount()
    {
        return replaysCount;
    }

    public void setReplaysCount(String replaysCount)
    {
        this.replaysCount = replaysCount;
    }

    public String getAchievedToDate()
    {
        return achievedToDate;
    }

    public void setAchievedToDate(String achievedToDate)
    {
        this.achievedToDate = achievedToDate;
    }

    public Source getSource()
    {
        return source;
    }

    public void setSource(Source source)
    {
        this.source = source;
    }
}
