package com.ihg.automation.selenium.common.offer;

import com.ihg.automation.selenium.common.award.AwardDetails;

public class Offer
{
    // Details
    private String code;
    private String name;
    private String description;
    private String maxDaysToPlay;
    private String maximumWinsAllowed;
    private String fmdsPromoID;
    private String adjustmentDaysAllowed;
    private String webDisplayIndicator;
    private String lastDayToAdjust;
    private String externalTrackingIndicator;
    private String award;
    private String eventType;
    private String status;

    // Dates
    private String startDate;
    private String endDate;
    private String bookingStartDate;
    private String bookingEndDate;

    // Free night
    private String rateCategoryCode;
    private String reimbursementType;
    private String freeNight;

    // Eligibility
    private String region;
    private String country;
    private String program;

    private AwardDetails awardDetails;

    private Offer(Builder builder)
    {
        code = builder.code;
        name = builder.name;
        description = builder.description;
        maxDaysToPlay = builder.maxDaysToPlay;
        maximumWinsAllowed = builder.maximumWinsAllowed;
        fmdsPromoID = builder.fmdsPromoID;
        adjustmentDaysAllowed = builder.adjustmentDaysAllowed;
        webDisplayIndicator = builder.webDisplayIndicator;
        lastDayToAdjust = builder.lastDayToAdjust;
        externalTrackingIndicator = builder.externalTrackingIndicator;
        award = builder.award;
        eventType = builder.eventType;
        status = builder.status;

        startDate = builder.startDate;
        endDate = builder.endDate;
        bookingStartDate = builder.bookingStartDate;
        bookingEndDate = builder.bookingEndDate;

        rateCategoryCode = builder.rateCategoryCode;
        reimbursementType = builder.reimbursementType;
        freeNight = builder.freeNight;

        region = builder.region;
        country = builder.country;
        program = builder.program;

        awardDetails = builder.awardDetails;
    }

    public static Builder builder()
    {
        return new Builder();
    }

    public static Builder builder(String code, String name)
    {
        return new Builder(code, name);
    }

    public String getCode()
    {
        return code;
    }

    public String getName()
    {
        return name;
    }

    public String getDescription()
    {
        return description;
    }

    public String getMaxDaysToPlay()
    {
        return maxDaysToPlay;
    }

    public String getMaximumWinsAllowed()
    {
        return maximumWinsAllowed;
    }

    public String getFmdsPromoID()
    {
        return fmdsPromoID;
    }

    public String getAdjustmentDaysAllowed()
    {
        return adjustmentDaysAllowed;
    }

    public String getWebDisplayIndicator()
    {
        return webDisplayIndicator;
    }

    public String getLastDayToAdjust()
    {
        return lastDayToAdjust;
    }

    public String getExternalTrackingIndicator()
    {
        return externalTrackingIndicator;
    }

    public String getAward()
    {
        return award;
    }

    public String getEventType()
    {
        return eventType;
    }

    public String getStatus()
    {
        return status;
    }

    public String getStartDate()
    {
        return startDate;
    }

    public String getEndDate()
    {
        return endDate;
    }

    public String getBookingStartDate()
    {
        return bookingStartDate;
    }

    public String getBookingEndDate()
    {
        return bookingEndDate;
    }

    public String getRateCategoryCode()
    {
        return rateCategoryCode;
    }

    public String getReimbursementType()
    {
        return reimbursementType;
    }

    public String getFreeNight()
    {
        return freeNight;
    }

    public String getRegion()
    {
        return region;
    }

    public String getCountry()
    {
        return country;
    }

    public String getProgram()
    {
        return program;
    }

    public AwardDetails getAwardDetails()
    {
        return awardDetails;
    }

    @Override
    public String toString()
    {
        return "Offer{" + "code='" + code + '\'' + ", name='" + name + '\'' + '}';
    }

    public static final class Builder
    {
        private String code;
        private String name;
        private String description;
        private String maxDaysToPlay;
        private String maximumWinsAllowed;
        private String fmdsPromoID;
        private String adjustmentDaysAllowed;
        private String webDisplayIndicator;
        private String lastDayToAdjust;
        private String externalTrackingIndicator;
        private String award;
        private String eventType;
        private String status;

        private String startDate;
        private String endDate;
        private String bookingStartDate;
        private String bookingEndDate;

        private String rateCategoryCode;
        private String reimbursementType;
        private String freeNight;

        private String region;
        private String country;
        private String program;

        private AwardDetails awardDetails;

        private Builder()
        {
        }

        private Builder(String code, String name)
        {
            this.code = code;
            this.name = name;
        }

        public Builder code(String code)
        {
            this.code = code;
            return this;
        }

        public Builder name(String name)
        {
            this.name = name;
            return this;
        }

        public Builder description(String description)
        {
            this.description = description;
            return this;
        }

        public Builder maxDaysToPlay(String maxDaysToPlay)
        {
            this.maxDaysToPlay = maxDaysToPlay;
            return this;
        }

        public Builder maximumWinsAllowed(String maximumWinsAllowed)
        {
            this.maximumWinsAllowed = maximumWinsAllowed;
            return this;
        }

        public Builder fmdsPromoID(String fmdsPromoID)
        {
            this.fmdsPromoID = fmdsPromoID;
            return this;
        }

        public Builder adjustmentDaysAllowed(String adjustmentDaysAllowed)
        {
            this.adjustmentDaysAllowed = adjustmentDaysAllowed;
            return this;
        }

        public Builder webDisplayIndicator(String webDisplayIndicator)
        {
            this.webDisplayIndicator = webDisplayIndicator;
            return this;
        }

        public Builder lastDayToAdjust(String lastDayToAdjust)
        {
            this.lastDayToAdjust = lastDayToAdjust;
            return this;
        }

        public Builder externalTrackingIndicator(String externalTrackingIndicator)
        {
            this.externalTrackingIndicator = externalTrackingIndicator;
            return this;
        }

        public Builder award(String award)
        {
            this.award = award;
            return this;
        }

        public Builder eventType(String eventType)
        {
            this.eventType = eventType;
            return this;
        }

        public Builder status(String status)
        {
            this.status = status;
            return this;
        }

        public Builder startDate(String startDate)
        {
            this.startDate = startDate;
            return this;
        }

        public Builder endDate(String endDate)
        {
            this.endDate = endDate;
            return this;
        }

        public Builder bookingStartDate(String bookingStartDate)
        {
            this.bookingStartDate = bookingStartDate;
            return this;
        }

        public Builder bookingEndDate(String bookingEndDate)
        {
            this.bookingEndDate = bookingEndDate;
            return this;
        }

        public Builder rateCategoryCode(String rateCategoryCode)
        {
            this.rateCategoryCode = rateCategoryCode;
            return this;
        }

        public Builder reimbursementType(String reimbursementType)
        {
            this.reimbursementType = reimbursementType;
            return this;
        }

        public Builder freeNight(String freeNight)
        {
            this.freeNight = freeNight;
            return this;
        }

        public Builder region(String region)
        {
            this.region = region;
            return this;
        }

        public Builder country(String country)
        {
            this.country = country;
            return this;
        }

        public Builder program(String program)
        {
            this.program = program;
            return this;
        }

        public Builder awardDetails(AwardDetails awardDetails)
        {
            this.awardDetails = awardDetails;
            return this;
        }

        public Offer build()
        {
            return new Offer(this);
        }
    }
}
