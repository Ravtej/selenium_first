package com.ihg.automation.selenium.common.business;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.business.EventStaysGridRow.EventStaysCell.CHECK_IN;
import static com.ihg.automation.selenium.common.business.EventStaysGridRow.EventStaysCell.CONF_NUMBER;
import static com.ihg.automation.selenium.common.business.EventStaysGridRow.EventStaysCell.ELIGIBLE_FOR_POINTS;
import static com.ihg.automation.selenium.common.business.EventStaysGridRow.EventStaysCell.GUEST_NAME;
import static com.ihg.automation.selenium.common.business.EventStaysGridRow.EventStaysCell.MEMBER_NUMBER;
import static com.ihg.automation.selenium.common.business.EventStaysGridRow.EventStaysCell.NIGHTS;
import static com.ihg.automation.selenium.common.business.EventStaysGridRow.EventStaysCell.RATE_CODE;
import static com.ihg.automation.selenium.common.business.EventStaysGridRow.EventStaysCell.REASON;
import static com.ihg.automation.selenium.common.business.EventStaysGridRow.EventStaysCell.ROOM_NUMBER;
import static com.ihg.automation.selenium.common.business.EventStaysGridRow.EventStaysCell.ROOM_RATE;
import static com.ihg.automation.selenium.common.business.EventStaysGridRow.EventStaysCell.TOTAL_ELIGIBLE_REVENUE;

import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.grid.Grid;
import com.ihg.automation.selenium.gwt.components.grid.GridHeader;
import com.ihg.automation.selenium.gwt.components.grid.GridRowXpath;
import com.ihg.automation.selenium.matchers.component.HasText;

public class EventStaysGrid extends Grid<EventStaysGridRow, EventStaysGridRow.EventStaysCell>
{
    public EventStaysGrid(Container parent)
    {
        super(parent);
    }

    public EventStaysGridRow getRow(String memberNumber)
    {
        return getRow(new GridRowXpath.GridRowXpathBuilder().cell(MEMBER_NUMBER, memberNumber).build());
    }

    public void verifyHeader()
    {
        GridHeader<EventStaysGridRow.EventStaysCell> header = getHeader();
        verifyThat(header.getCell(GUEST_NAME), HasText.hasText(GUEST_NAME.getValue()));
        verifyThat(header.getCell(MEMBER_NUMBER), HasText.hasText(MEMBER_NUMBER.getValue()));
        verifyThat(header.getCell(CONF_NUMBER), HasText.hasText(CONF_NUMBER.getValue()));
        verifyThat(header.getCell(CHECK_IN), HasText.hasText(CHECK_IN.getValue()));
        verifyThat(header.getCell(NIGHTS), HasText.hasText(NIGHTS.getValue()));
        verifyThat(header.getCell(ROOM_NUMBER), HasText.hasText(ROOM_NUMBER.getValue()));
        verifyThat(header.getCell(RATE_CODE), HasText.hasText(RATE_CODE.getValue()));
        verifyThat(header.getCell(ROOM_RATE), HasText.hasText(ROOM_RATE.getValue()));
        verifyThat(header.getCell(ELIGIBLE_FOR_POINTS), HasText.hasText(ELIGIBLE_FOR_POINTS.getValue()));
        verifyThat(header.getCell(REASON), HasText.hasText(REASON.getValue()));
        verifyThat(header.getCell(TOTAL_ELIGIBLE_REVENUE), HasText.hasText(TOTAL_ELIGIBLE_REVENUE.getValue()));
    }
}
