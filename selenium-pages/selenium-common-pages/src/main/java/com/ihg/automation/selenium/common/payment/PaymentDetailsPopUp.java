package com.ihg.automation.selenium.common.payment;

import com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory;
import com.ihg.automation.selenium.common.catalog.CatalogItem;
import com.ihg.automation.selenium.common.pages.PaymentMethod;
import com.ihg.automation.selenium.common.pages.SubmitPopUpBase;
import com.ihg.automation.selenium.common.unit.MemberFieldsAware;
import com.ihg.automation.selenium.gwt.components.Button;
import com.ihg.automation.selenium.gwt.components.ComponentColumnPosition;
import com.ihg.automation.selenium.gwt.components.RadioButton;
import com.ihg.automation.selenium.gwt.components.checkbox.CheckBox;
import com.ihg.automation.selenium.gwt.components.input.InputXpath;
import com.ihg.automation.selenium.gwt.components.input.Select;
import com.ihg.automation.selenium.gwt.components.label.Label;
import com.ihg.automation.selenium.gwt.components.utils.ByText;

public class PaymentDetailsPopUp extends SubmitPopUpBase implements MemberFieldsAware
{
    protected static final String TOTAL_AMOUNT = "Total Amount";
    public static final String PAYMENT_SUCCESSFULLY_SUBMITTED = "Payment Details has been successfully submitted";

    public PaymentDetailsPopUp()
    {
        super("Payment Details");
    }

    @Override
    public Label getMembershipId()
    {
        return container.getLabel("Recipient Membership ID");
    }

    @Override
    public Label getMemberName()
    {
        return container.getLabel("Member Name");
    }

    public Select getTotalAmount()
    {
        return container.getSelectByLabel(TOTAL_AMOUNT);
    }

    public CheckBox getOfferCode()
    {
        return container.getLabel("Offer Code?").getCheckBox();
    }

    public Select getOffer()
    {
        return new Select(container,
                new InputXpath.InputXpathBuilder().column(3, ComponentColumnPosition.TABLE_TR).build(), "Offer Code");
    }

    public Button getApply()
    {
        return container.getButton("Apply");
    }

    public Button getDetails()
    {
        return container.getButton("Details");
    }

    public void clickDetails()
    {
        getDetails().clickAndWait();
    }

    public void selectPaymentType(PaymentMethod type)
    {
        new RadioButton(container, ByText.exact(type.getLabel())).select();
    }

    public RadioButton getPaymentTypeButton(PaymentMethod type)
    {
        return new RadioButton(container, ByText.exact(type.getLabel()));
    }

    public CreditCardPaymentContainer getCreditCardPayment()
    {
        return new CreditCardPaymentContainer(container);
    }

    public CheckPaymentContainer getCheckPayment()
    {
        return new CheckPaymentContainer(container);
    }

    public CashPaymentContainer getCashPayment()
    {
        return new CashPaymentContainer(container);
    }

    public void clickSubmitAndVerifySuccessMsg()
    {
        clickSubmitNoError(MessageBoxVerifierFactory.verifySuccess(PAYMENT_SUCCESSFULLY_SUBMITTED));
    }

    public void selectAmountAndPaymentMethod(String totalAmount, PaymentMethod method)
    {
        getTotalAmount().select(totalAmount);
        getPaymentTypeButton(method).select();
    }

    public void selectAmountAndPaymentMethod(CatalogItem catalogItem, PaymentMethod method)
    {
        selectAmount(catalogItem);
        getPaymentTypeButton(method).select();
    }

    public void selectAmount(CatalogItem catalogItem)
    {
        getTotalAmount().select(catalogItem.getItemDescription());
    }

    public void selectOfferCode(String offerCode)
    {
        getOfferCode().check();
        getOffer().select(ByText.startsWith(offerCode));
        getApply().clickAndWait();
    }

}
