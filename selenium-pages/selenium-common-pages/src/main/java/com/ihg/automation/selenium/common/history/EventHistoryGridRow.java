package com.ihg.automation.selenium.common.history;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static org.hamcrest.core.StringContains.containsString;

import com.ihg.automation.common.DateUtils;
import com.ihg.automation.selenium.gwt.components.grid.CellsName;
import com.ihg.automation.selenium.gwt.components.grid.GridRow;
import com.ihg.automation.selenium.gwt.components.grid.GridRowXpath;

public class EventHistoryGridRow extends GridRow<EventHistoryGridRow.EventHistoryCell>
{
    public EventHistoryGridRow(EventHistoryGrid parent, GridRowXpath xpath)
    {
        super(parent, xpath);
    }

    public EventHistoryRecords getAllRecords()
    {
        return expand(EventHistoryRecords.class);
    }

    public void verify(String type, String userName)
    {
        verifyThat(getCell(EventHistoryCell.EVENT_TIME), hasText(containsString(DateUtils.getFormattedDate())));
        verifyThat(getCell(EventHistoryCell.CHANGE_TYPE), hasText(type));
        verifyThat(getCell(EventHistoryCell.UPDATED_BY), hasText(userName));
    }

    public enum EventHistoryCell implements CellsName
    {
        EVENT_TIME("modificationDate"), CHANGE_TYPE("changeType"), UPDATED_BY("modifier");

        private String name;

        private EventHistoryCell(String name)
        {
            this.name = name;
        }

        @Override
        public String getName()
        {
            return name;
        }
    }
}
