package com.ihg.automation.selenium.common.payment;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.automation.selenium.matchers.component.ViewableMatcher.hasText;

import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.CustomContainerBase;
import com.ihg.automation.selenium.gwt.components.Mode;
import com.ihg.automation.selenium.gwt.components.input.Input;
import com.ihg.automation.selenium.gwt.components.input.Select;

public class CreditCardPaymentContainer extends CustomContainerBase
{
    public CreditCardPaymentContainer(Container container)
    {
        super(container);
    }

    public Select getType()
    {
        return container.getSelectByLabel("Credit Card Type");
    }

    public Input getNumber()
    {
        return container.getInputByLabel("CC Authorization Number");
    }

    public void verify(CreditCardPayment creditCardPayment, Mode mode)
    {
        verifyThat(getType(), hasText(creditCardPayment.getType().getCode(), mode));
        verifyThat(getNumber(), hasText(creditCardPayment.getNumber(), mode));
    }

    public void verifyPaymentFieldsDisplayed()
    {
        verifyThat(getType(), displayed(true));
        verifyThat(getNumber(), displayed(true));
    }

    public void populate(CreditCardPayment creditCardPayment)
    {
        getType().select(creditCardPayment.getType().getCodeWithValue());
        getNumber().type(creditCardPayment.getNumber());
    }
}
