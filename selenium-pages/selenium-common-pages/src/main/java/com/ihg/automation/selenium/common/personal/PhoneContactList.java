package com.ihg.automation.selenium.common.personal;

import com.ihg.automation.selenium.common.member.GuestPhone;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.gwt.components.Button;
import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.panel.EditablePanel;

public class PhoneContactList extends ContactListBase<GuestPhone, PhoneContact>
{
    public PhoneContactList(Container container)
    {
        super(container.getSeparator("Phone"));
    }

    @Override
    public PhoneContact getContact(int index)
    {
        return new PhoneContact(new EditablePanel(container, index));
    }

    @Override
    public Button getAddButton()
    {
        return container.getButton("Add Phone");
    }

    @Override
    protected GuestPhone getRandomContact()
    {
        GuestPhone phone = MemberPopulateHelper.getRandomPhone();
        phone.setPreferred(false);
        return phone;
    }
}
