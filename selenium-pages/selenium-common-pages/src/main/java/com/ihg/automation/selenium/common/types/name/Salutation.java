package com.ihg.automation.selenium.common.types.name;

public class Salutation
{
    private Salutation()
    {
    }

    public static final String MS = "Ms";
    public static final String HR = "Hr";
    public static final String SR = "Sr";
    public static final String SRA = "Sra";
    public static final String MISS = "Miss";
    public static final String SIG_RA = "Sig.ra";
    public static final String MLLE = "Mlle";
    public static final String HERR = "Herr";
    public static final String SHEIKH = "Sheikh";
    public static final String M = "M";
    public static final String DR = "Dr";
    public static final String MRS = "Mrs";
    public static final String SIG = "Sig";
    public static final String PAN = "Pan";
    public static final String PANI = "Pani";
    public static final String MR = "Mr";
    public static final String MME = "Mme";
    public static final String MNR = "Mnr";
    public static final String FRAU = "Frau";
    public static final String DHR = "Dhr";
    public static final String FR = "Fr";
    public static final String SRTA = "Srta";
    public static final String MEVR = "Mevr";

}
