package com.ihg.automation.selenium.servicecenter.pages.programs.rc;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static com.ihg.automation.selenium.servicecenter.pages.programs.rc.AmazonBenefitsGridRow.AmazonBenefitsCell.BENEFIT_COUNT;
import static com.ihg.automation.selenium.servicecenter.pages.programs.rc.AmazonBenefitsGridRow.AmazonBenefitsCell.BENEFIT_FREQUENCY;
import static com.ihg.automation.selenium.servicecenter.pages.programs.rc.AmazonBenefitsGridRow.AmazonBenefitsCell.NAME;
import static com.ihg.automation.selenium.servicecenter.pages.programs.rc.AmazonBenefitsGridRow.AmazonBenefitsCell.TIER_LEVEL;

import com.ihg.automation.selenium.common.types.program.RewardClubLevel;
import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.grid.Grid;
import com.ihg.automation.selenium.gwt.components.grid.GridHeader;
import com.ihg.automation.selenium.gwt.components.grid.GridRowXpath;
import com.ihg.automation.selenium.gwt.components.utils.ByText;

public class AmazonBenefitsGrid extends Grid<AmazonBenefitsGridRow, AmazonBenefitsGridRow.AmazonBenefitsCell>
{
    public AmazonBenefitsGrid(Container parent)
    {
        super(parent);
    }

    public AmazonBenefitsGridRow getRow(RewardClubLevel level)
    {
        return getRow(new GridRowXpath.GridRowXpathBuilder()
                .cell(AmazonBenefitsGridRow.AmazonBenefitsCell.TIER_LEVEL, ByText.exactSelf(level.getValue())).build());
    }

    public void verifyHeader()
    {
        GridHeader<AmazonBenefitsGridRow.AmazonBenefitsCell> header = getHeader();
        verifyThat(header.getCell(NAME), hasText(NAME.getValue()));
        verifyThat(header.getCell(TIER_LEVEL), hasText(TIER_LEVEL.getValue()));
        verifyThat(header.getCell(BENEFIT_COUNT), hasText(BENEFIT_COUNT.getValue()));
        verifyThat(header.getCell(BENEFIT_FREQUENCY), hasText(BENEFIT_FREQUENCY.getValue()));
    }
}
