package com.ihg.automation.selenium.servicecenter.pages.hotel.freenight;

import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.grid.Grid;
import com.ihg.automation.selenium.gwt.components.grid.GridRowXpath;
import com.ihg.automation.selenium.servicecenter.pages.hotel.freenight.FreeNightReimbursementSearchGridRow.FreeNightReimbursementCell;

public class FreeNightReimbursementSearchGrid
        extends Grid<FreeNightReimbursementSearchGridRow, FreeNightReimbursementCell>
{
    public FreeNightReimbursementSearchGrid(Container parent)
    {
        super(parent);
    }

    public FreeNightReimbursementSearchGridRow getRow(String date)
    {
        return getRow(new GridRowXpath.GridRowXpathBuilder().cell(FreeNightReimbursementCell.STAY_DATE, date).build());
    }

}
