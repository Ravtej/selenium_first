package com.ihg.automation.selenium.servicecenter.pages.voucher;

import com.ihg.automation.selenium.common.pages.PopUpBase;

public class VoucherPopUp extends PopUpBase
{
    public VoucherPopUp()
    {
        super("Voucher");
    }

    public VoucherDepositTab getVoucherDepositTab()
    {
        return new VoucherDepositTab(this);
    }

    public VoucherLookupTab getVoucherLookupTab()
    {
        return new VoucherLookupTab(this);
    }
}
