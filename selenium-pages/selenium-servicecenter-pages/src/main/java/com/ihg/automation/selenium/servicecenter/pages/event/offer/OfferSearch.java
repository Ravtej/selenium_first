package com.ihg.automation.selenium.servicecenter.pages.event.offer;

import com.ihg.automation.selenium.common.components.CountrySelect;
import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.CustomContainerBase;
import com.ihg.automation.selenium.gwt.components.checkbox.CheckBox;
import com.ihg.automation.selenium.gwt.components.input.Input;
import com.ihg.automation.selenium.servicecenter.pages.event.EventDateRange;

public class OfferSearch extends CustomContainerBase
{
    public OfferSearch(Container container)
    {
        super(container.getSeparator("Offer Search"));
    }

    public Input geKeywords()
    {
        return container.getInputByLabel("Keywords");
    }

    public Input getOfferCode()
    {
        return container.getInputByLabel("Loyalty Offer Code or Registration Code");
    }

    public Input getFmdsPromoId()
    {
        return container.getInputByLabel("FMDS Promo ID");
    }

    public CheckBox getEligible()
    {
        return new CheckBox(container);
    }

    public CountrySelect getCountry()
    {
        return new CountrySelect(container);
    }

    public EventDateRange getOfferActiveDate()
    {
        return new EventDateRange(container, "Offer Active Date");
    }

}
