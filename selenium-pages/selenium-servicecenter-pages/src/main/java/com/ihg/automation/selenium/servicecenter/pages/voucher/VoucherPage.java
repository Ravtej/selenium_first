package com.ihg.automation.selenium.servicecenter.pages.voucher;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.assertThat;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;

import com.ihg.automation.selenium.common.event.SearchAware;
import com.ihg.automation.selenium.common.event.SearchButtonBar;
import com.ihg.automation.selenium.common.pages.NavigatePage;
import com.ihg.automation.selenium.gwt.components.Button;
import com.ihg.automation.selenium.gwt.components.CustomContainerBase;
import com.ihg.automation.selenium.gwt.components.panel.Panel;
import com.ihg.automation.selenium.servicecenter.pages.TopMenu;
import com.ihg.automation.selenium.servicecenter.pages.TopMenu.VoucherMenu;

public class VoucherPage extends CustomContainerBase implements NavigatePage, SearchAware
{
    TopMenu menu = new TopMenu();

    public VoucherPage()
    {
        super(new Panel("Voucher"));
    }

    @Override
    public void goTo()
    {
        if (!isDisplayed())
        {
            VoucherMenu voucher = menu.getVoucher();
            voucher.clickAndWait();
            voucher.getOrder().clickAndWait();

            flush();
            assertThat(this, displayed(true));
        }
    }

    @Override
    public VoucherSearchFields getSearchFields()
    {
        return new VoucherSearchFields(container);
    }

    @Override
    public SearchButtonBar getButtonBar()
    {
        return new SearchButtonBar(container);
    }

    public VoucherGrid getVouchers()
    {
        return new VoucherGrid(container);
    }

    public Button getClose()
    {
        return container.getButton(Button.CLOSE);
    }

    public void clickClose()
    {
        getClose().clickAndWait();
    }

    public void search(String type)
    {
        getSearchFields().getType().select(type);
        getButtonBar().clickSearch();
    }
}
