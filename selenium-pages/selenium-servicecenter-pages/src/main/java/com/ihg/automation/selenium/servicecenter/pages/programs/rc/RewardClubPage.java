package com.ihg.automation.selenium.servicecenter.pages.programs.rc;

import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyNoError;
import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifySuccess;

import com.ihg.automation.common.DateUtils;
import com.ihg.automation.selenium.common.member.MemberAlliance;
import com.ihg.automation.selenium.common.pages.program.EmployeeRateEligibility;
import com.ihg.automation.selenium.common.pages.program.RewardClubTierLevelActivityGrid;
import com.ihg.automation.selenium.common.types.Alliance;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.gwt.components.Button;
import com.ihg.automation.selenium.gwt.components.dialog.ConfirmDialog;
import com.ihg.automation.selenium.gwt.components.panel.Panel;
import com.ihg.automation.selenium.gwt.components.utils.ByText;
import com.ihg.automation.selenium.servicecenter.pages.programs.ProgramPageBase;

public class RewardClubPage extends ProgramPageBase
{
    public RewardClubPage()
    {
        super(Program.RC);
    }

    @Override
    public RewardClubSummary getSummary()
    {
        return new RewardClubSummary(container);
    }

    public EmployeeRateEligibility getEmployeeRateEligibility()
    {
        return new EmployeeRateEligibility(container);
    }

    public Panel getAnnualActivitiesPanel(int year)
    {
        return new Panel(container, ByText.exact("Tier Level Annual Activity From January 1, " + year));
    }

    public RewardClubTierLevelActivityGrid getAnnualActivities(int year)
    {
        return new RewardClubTierLevelActivityGrid(getAnnualActivitiesPanel(year));
    }

    public RewardClubTierLevelActivityGrid getCurrentYearAnnualActivities()
    {
        return getAnnualActivities(DateUtils.currentYear());
    }

    public RewardClubTierLevelActivityGrid getPreviousYearAnnualActivities()
    {
        return getAnnualActivities(DateUtils.previousYear());
    }

    public EarningPreference getEarningDetails()
    {
        return new EarningPreference(container);
    }

    public Panel getAlliancesPanel()
    {
        return container.getPanel("Alliance Partners");
    }

    public AllianceGrid getAlliances()
    {
        return new AllianceGrid(getAlliancesPanel());
    }

    public AlternateLoyaltyIDGrid getAlternateLoyaltyID()
    {
        return new AlternateLoyaltyIDGrid(container.getPanel("Alternate Loyalty ID"));
    }

    public CoBrandedCreditCardsGrid getCoBrandedCreditCards()
    {
        return new CoBrandedCreditCardsGrid(container.getPanel("Co-Branded Credit Cards"));
    }

    public Button getAddAlternateID()
    {
        return container.getButton("Add Alternate");
    }

    public void clickAddAlternateID()
    {
        getAddAlternateID().clickAndWait();
    }

    public Button getAddAlliance()
    {
        return container.getButton("Add Alliance");
    }

    public void clickAddAlliance()
    {
        getAddAlliance().clickAndWait();
    }

    public void resetEarningPreference()
    {
        goTo();
        getEarningDetails().resetEarningPreference();
    }

    public void addAlliance(MemberAlliance alliance)
    {
        goTo();

        clickAddAlliance();
        new AllianceAddPopUp().addAlliance(alliance);
    }

    public void addAlternateLoyaltyId(String number)
    {
        goTo();

        clickAddAlternateID();
        AddAlternateLoyaltyIDPopUp alternateLoyaltyIDPopUp = new AddAlternateLoyaltyIDPopUp();
        alternateLoyaltyIDPopUp.populateHertzID(number);
        alternateLoyaltyIDPopUp.clickSave(verifyNoError(), verifySuccess(SUCCESS_MESSAGE));
    }

    public void removeAlliance(Alliance alliance)
    {
        resetEarningPreference();
        getAlliances().getRow(alliance).delete();
        // Are you sure you want to delete the {} alliance?
        new ConfirmDialog().clickYes(verifyNoError());
    }

    public void setEarningPreference(MemberAlliance alliance)
    {
        addAlliance(alliance);
        getEarningDetails().setEarningPreference(alliance.getAlliance());
    }
}
