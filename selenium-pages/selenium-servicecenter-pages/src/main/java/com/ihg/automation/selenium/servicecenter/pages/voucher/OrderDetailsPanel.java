package com.ihg.automation.selenium.servicecenter.pages.voucher;

import com.ihg.automation.selenium.common.order.VoucherOrder;
import com.ihg.automation.selenium.common.pages.Verify;
import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.CustomContainerBase;

public class OrderDetailsPanel extends CustomContainerBase implements Verify<VoucherOrder>
{
    public OrderDetailsPanel(Container container)
    {
        super(container.getSeparator("Order Details"));
    }

    public OrderTotalValues getOrderTotalValues()
    {
        return new OrderTotalValues(container);
    }

    public OrderDetails getOrderDetails()
    {
        return new OrderDetails(container);
    }

    public VoucherOrderDetails getVoucherOrderDetails()
    {
        return new VoucherOrderDetails(container);
    }

    public OrderDeliveryInfo getOrderDeliveryInfo()
    {
        return new OrderDeliveryInfo(container);
    }

    @Override
    public void verify(VoucherOrder voucherOrder)
    {
        getOrderTotalValues().verify(voucherOrder);
        getOrderDetails().verify(voucherOrder);
        getVoucherOrderDetails().verify(voucherOrder);
        getOrderDeliveryInfo().verifyAsOnCustomerInformation();
    }
}
