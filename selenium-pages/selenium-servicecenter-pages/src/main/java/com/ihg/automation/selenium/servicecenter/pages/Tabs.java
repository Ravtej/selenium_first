package com.ihg.automation.selenium.servicecenter.pages;

import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.gwt.components.CustomContainerBase;
import com.ihg.automation.selenium.gwt.components.Tab;

public class Tabs extends CustomContainerBase
{

    public CustomerInfoTab getCustomerInfo()
    {
        return new CustomerInfoTab();
    }

    public ProgramInfoTab getProgramInfo()
    {
        return new ProgramInfoTab();
    }

    public EventsTab getEvents()
    {
        return new EventsTab();
    }

    public ReimbursementTab getReimbursement()
    {
        return new ReimbursementTab();
    }

    public class CustomerInfoTab extends Tab
    {

        public CustomerInfoTab()
        {
            super(container, "Customer Information");
        }

        public Tab getPersonalInfo()
        {
            return new Tab(this, "Personal Information");
        }

        public Tab getStayPreferences()
        {
            return new Tab(this, "Stay Preferences");
        }

        public Tab getCommunicationPref()
        {
            return new Tab(this, "Communication Preferences");
        }

        public Tab getGuestRecordData()
        {
            return new Tab(this, "Guest Record Data");
        }

        public Tab getAccountInfo()
        {
            return new Tab(this, "Account Information");
        }

        public Tab getProfileHistory()
        {
            return new Tab(this, "Profile History");
        }
    }

    public class ProgramInfoTab extends Tab
    {

        public ProgramInfoTab()
        {
            super(container, "Program Information");
        }

        public Tab getProgram(Program type)
        {
            return new Tab(this, type.getValue());
        }

        public Tab getAnnualActivity()
        {
            return new Tab(this, "Annual Activity");
        }
    }

    public class EventsTab extends Tab
    {

        public EventsTab()
        {
            super(container, "Events (view/create)");
        }

        public Tab getAll()
        {
            return new Tab(this, "All");
        }

        public Tab getBusiness()
        {
            return new Tab(this, "Business");
        }

        public Tab getStay()
        {
            return new Tab(this, "Stay");
        }

        public Tab getOffer()
        {
            return new Tab(this, "Offer");
        }

        public Tab getRewardNight()
        {
            return new Tab(this, "Reward Night");
        }

        public Tab getFreeNight()
        {
            return new Tab(this, "Free Night");
        }

        public Tab getCoPartner()
        {
            return new Tab(this, "Co-Partner");
        }

        public Tab getMeeting()
        {
            return new Tab(this, "Meeting");
        }

        public Tab getOrder()
        {
            return new Tab(this, "Order");
        }
    }

    public class ReimbursementTab extends Tab
    {

        public ReimbursementTab()
        {
            super(container, "Reimbursement");
        }

        public Tab getNightReimbursement()
        {
            return this.getContainer().getTab("Reward/Free Night Reimbursement");
        }

        public Tab getOtherCertificates()
        {
            return this.getContainer().getTab("In-Hotel/Other Certificates");
        }

        public Tab getNightFlagReimbursement()
        {
            return this.getContainer().getTab("Free Night Flat Reimbursement");
        }
    }
}
