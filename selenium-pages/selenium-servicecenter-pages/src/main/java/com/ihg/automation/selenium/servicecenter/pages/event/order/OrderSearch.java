package com.ihg.automation.selenium.servicecenter.pages.event.order;

import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.CustomContainerBase;
import com.ihg.automation.selenium.gwt.components.input.DateInput;
import com.ihg.automation.selenium.gwt.components.input.Input;
import com.ihg.automation.selenium.gwt.components.input.Select;

public class OrderSearch extends CustomContainerBase
{
    public OrderSearch(Container container)
    {
        super(container.getSeparator("Order Search"));
    }

    public Input getItemId()
    {
        return container.getInputByLabel("Item ID");
    }

    public Input getItemName()
    {
        return container.getInputByLabel("Item Name");
    }

    public Select getStatus()
    {
        return container.getSelectByLabel("Status");
    }

    public Input getOrderNumber()
    {
        return container.getInputByLabel("Order Number");
    }

    public DateInput getFromDate()
    {
        return new DateInput(container.getLabel("From"));
    }

    public DateInput getToDate()
    {
        return new DateInput(container.getLabel("To"));
    }
}
