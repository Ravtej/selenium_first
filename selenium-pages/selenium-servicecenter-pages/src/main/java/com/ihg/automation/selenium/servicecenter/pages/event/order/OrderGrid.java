package com.ihg.automation.selenium.servicecenter.pages.event.order;

import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.grid.Grid;
import com.ihg.automation.selenium.gwt.components.grid.GridRowXpath;
import com.ihg.automation.selenium.servicecenter.pages.event.order.OrderGridRow.OrderCell;

public class OrderGrid extends Grid<OrderGridRow, OrderCell>
{
    public OrderGrid(Container parent)
    {
        super(parent);
    }

    public OrderGridRow getRow(String transType, String itemName)
    {
        return getRow(new GridRowXpath.GridRowXpathBuilder().cell(OrderCell.TRANS_TYPE, transType)
                .cell(OrderCell.ITEM_NAME, itemName).build());
    }
}
