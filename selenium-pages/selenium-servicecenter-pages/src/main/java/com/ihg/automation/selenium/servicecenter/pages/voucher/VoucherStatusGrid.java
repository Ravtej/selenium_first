package com.ihg.automation.selenium.servicecenter.pages.voucher;

import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.grid.Grid;

public class VoucherStatusGrid extends Grid<VoucherStatusGridRow, VoucherStatusGridRow.VoucherStatusCell>
{
    public VoucherStatusGrid(Container parent)
    {
        super(parent, "Current Status");
    }

}
