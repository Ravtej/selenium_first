package com.ihg.automation.selenium.servicecenter.pages.event.business;

import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.CustomContainerBase;
import com.ihg.automation.selenium.gwt.components.input.Input;
import com.ihg.automation.selenium.gwt.components.input.SearchInput;
import com.ihg.automation.selenium.gwt.components.input.Select;
import com.ihg.automation.selenium.servicecenter.pages.event.EventDateRange;

public class BusinessSearch extends CustomContainerBase
{
    public BusinessSearch(Container container)
    {
        super(container.getSeparator("Events Search"));
    }

    public SearchInput getHotel()
    {
        return new SearchInput(container.getLabel("Hotel"));
    }

    public Select getStatus()
    {
        return container.getSelectByLabel("Status");
    }

    public Input getEventID()
    {
        return container.getInputByLabel("Event ID");
    }

    public Input getEventName()
    {
        return container.getInputByLabel("Event Name ");
    }

    public EventDateRange getEventActiveDate()
    {
        return new EventDateRange(container, "Date of Event");
    }
}
