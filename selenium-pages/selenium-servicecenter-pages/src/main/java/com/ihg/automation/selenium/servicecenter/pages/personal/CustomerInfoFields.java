package com.ihg.automation.selenium.servicecenter.pages.personal;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.gwt.components.utils.FindStepUtils.byXpathWithWait;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static com.ihg.automation.selenium.matchers.component.ViewableMatcher.hasText;

import com.ihg.automation.selenium.common.BirthDate;
import com.ihg.automation.selenium.common.components.Country;
import com.ihg.automation.selenium.common.enroll.CustomerInfoCommonFieldsAware;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.PersonalInfo;
import com.ihg.automation.selenium.common.name.PersonNameCustomerFields;
import com.ihg.automation.selenium.common.name.PersonNameFields;
import com.ihg.automation.selenium.common.pages.MultiModeBase;
import com.ihg.automation.selenium.common.pages.Populate;
import com.ihg.automation.selenium.common.pages.VerifyMultiMode;
import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.Mode;
import com.ihg.automation.selenium.gwt.components.input.Input;
import com.ihg.automation.selenium.gwt.components.input.Select;
import com.ihg.automation.selenium.gwt.components.label.Label;
import com.ihg.automation.selenium.listener.Verifier;
import com.ihg.automation.selenium.matchers.text.EntityMatcherFactory;
import com.ihg.automation.selenium.servicecenter.pages.occupation.OccupationInfoAware;
import com.ihg.automation.selenium.servicecenter.pages.occupation.OccupationInfoFields;

public class CustomerInfoFields extends MultiModeBase
        implements CustomerInfoCommonFieldsAware, OccupationInfoAware, Populate<Member>, VerifyMultiMode<Member>
{
    private static final String TYPE = "Customer container";
    private static final String PATTERN = "./div[1]";

    public CustomerInfoFields(Container container)
    {
        super(container, TYPE, "Customer fields", byXpathWithWait(PATTERN));
    }

    @Override
    public void clickEdit(Verifier... verifiers)
    {
        getResidenceCountry().clickAndWait(verifiers);
    }

    @Override
    public PersonNameFields getName()
    {
        return new PersonNameCustomerFields(container);
    }

    @Override
    public OccupationInfoFields getOccupationInfo()
    {
        return new OccupationInfoFields(container);
    }

    public Input getNameOnCard()
    {
        return container.getInputByLabel("Name on Card");
    }

    public Label getNativeLanguage()
    {
        return container.getLabel("Native Language");
    }

    @Override
    public Select getGender()
    {
        return container.getSelectByLabel(GENDER_LABEL);
    }

    @Override
    public BirthDate getBirthDate()
    {
        return new BirthDate(container);
    }

    @Override
    public Label getResidenceCountry()
    {
        return container.getLabel(COUNTRY_LABEL);
    }

    @Override
    public void populate(Member member)
    {
        PersonNameFields name = getName();
        PersonalInfo personalInfo = member.getPersonalInfo();
        name.setConfiguration(personalInfo.getResidenceCountry());
        name.populate(personalInfo.getName());

        getNameOnCard().type(personalInfo.getNameOnCard());
        getGender().selectByValue(personalInfo.getGenderCode());
        getBirthDate().populate(personalInfo.getBirthDate());

        getOccupationInfo().populate(member.getOccupationInfo());
    }

    @Override
    public void verify(Member member, Mode mode)
    {
        PersonalInfo personalInfo = member.getPersonalInfo();
        Country residenceCountry = personalInfo.getResidenceCountry();

        verifyThat(getResidenceCountry(), hasText(residenceCountry.getValue()));

        PersonNameFields name = getName();
        name.setConfiguration(residenceCountry);
        name.verify(personalInfo.getName(), mode);

        verifyThat(getNameOnCard(), hasText(personalInfo.getNameOnCard(), mode));
        verifyThat(getGender(), hasText(EntityMatcherFactory.isValue(personalInfo.getGenderCode()), mode));
        getBirthDate().verify(personalInfo.getBirthDate(), mode);

        getOccupationInfo().verify(member.getOccupationInfo(), mode);
    }
}
