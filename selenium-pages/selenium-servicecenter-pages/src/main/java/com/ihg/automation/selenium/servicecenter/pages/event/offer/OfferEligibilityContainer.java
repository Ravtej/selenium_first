package com.ihg.automation.selenium.servicecenter.pages.event.offer;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;

import com.ihg.automation.selenium.common.offer.Offer;
import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.CustomContainerBase;
import com.ihg.automation.selenium.gwt.components.label.Label;

public class OfferEligibilityContainer extends CustomContainerBase
{

    public OfferEligibilityContainer(Container container)
    {
        super(container.getSeparator("Offer Eligibility"));
    }

    public Label getRegion()
    {
        return container.getLabel("Region");
    }

    public Label getCountry()
    {
        return container.getLabel("Country");
    }

    public Label getProgram()
    {
        return container.getLabel("Program");
    }

    public Label getTargetExpiry()
    {
        return container.getLabel("Target Expiry");
    }

    public void verify(Offer offer)
    {
        verifyThat(getRegion(), hasText(offer.getRegion()));
        verifyThat(getCountry(), hasText(offer.getCountry()));
        verifyThat(getProgram(), hasText(offer.getProgram()));
    }

    public void capture(Offer.Builder builder)
    {
        builder.region(getRegion().getText());
        builder.country(getCountry().getText());
        builder.program(getProgram().getText());
    }
}
