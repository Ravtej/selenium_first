package com.ihg.automation.selenium.servicecenter.pages.search;

import com.ihg.automation.selenium.gwt.components.grid.CellsName;

public enum CustomerSearchGridCell implements CellsName
{
    MEMBER_ID("membership.memberId"), //
    NAME("name"), //
    ADDRESS("address.addressLine"), //
    CITY("address.locality1"), //
    STATE("address.region1"), //
    COUNTRY("address.countryCode"), //
    PHONE("phone"), //
    EMAIL("email.email"), //
    STATUS("statusCode");

    private String name;

    private CustomerSearchGridCell(String name)
    {
        this.name = name;
    }

    @Override
    public String getName()
    {
        return name;
    }
}
