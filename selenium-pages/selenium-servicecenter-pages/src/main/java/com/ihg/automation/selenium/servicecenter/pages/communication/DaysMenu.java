package com.ihg.automation.selenium.servicecenter.pages.communication;

import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.Menu;
import com.ihg.automation.selenium.gwt.components.checkbox.ItemCheckBox;

public class DaysMenu extends Menu
{
    public final static String WEEKDAYS = "Weekdays";
    public final static String WEEKENDS = "Weekends";
    public final static String ANY = "Any";
    public final static String SUNDAY = "Sunday";
    public final static String MONDAY = "Monday";
    public final static String TUESDAY = "Tuesday";
    public final static String WEDNESDAY = "Wednesday";
    public final static String THURSDAY = "Thursday";
    public final static String FRIDAY = "Friday";
    public final static String SATURDAY = "Saturday";

    public final static String WEEKDAYS_AUDIT = "Mon, Tue, Wed, Thu, Fri";
    public final static String WEEKENDS_AUDIT = "Sun, Sat";
    public final static String ANY_AUDIT = "Sun, Mon, Tue, Wed, Thu, Fri, Sat";
    public final static String SUNDAY_AUDIT = "Sun";
    public final static String MONDAY_AUDIT = "Mon";
    public final static String TUESDAY_AUDIT = "Tue";
    public final static String WEDNESDAY_AUDIT = "Wed";
    public final static String THURSDAY_AUDIT = "Thu";
    public final static String FRIDAY_AUDIT = "Fri";
    public final static String SATURDAY_AUDIT = "Sat";

    public DaysMenu(Container container)
    {
        super(container, "Days", ".//table[contains(@class, 'x-btn x-component x-btn-noicon')]");

    }

    public Item getWeekdays()
    {
        return this.getSubMenu(WEEKDAYS);
    }

    public Item getWeekends()
    {
        return this.getSubMenu(WEEKENDS);
    }

    public Item getAny()
    {
        return this.getSubMenu(ANY);
    }

    public ItemCheckBox getSunday()
    {
        return this.getItemCheckBox(SUNDAY);
    }

    public ItemCheckBox getMonday()
    {
        return this.getItemCheckBox(MONDAY);
    }

    public ItemCheckBox getTuesday()
    {
        return this.getItemCheckBox(TUESDAY);
    }

    public ItemCheckBox getWednesday()
    {
        return this.getItemCheckBox(WEDNESDAY);
    }

    public ItemCheckBox getThursday()
    {
        return this.getItemCheckBox(THURSDAY);
    }

    public ItemCheckBox getFriday()
    {
        return this.getItemCheckBox(FRIDAY);
    }

    public ItemCheckBox getSaturday()
    {
        return this.getItemCheckBox(SATURDAY);
    }

    public void uncheckAll()
    {
        click();
        getSunday().uncheck();
        getMonday().uncheck();
        getTuesday().uncheck();
        getWednesday().uncheck();
        getThursday().uncheck();
        getFriday().uncheck();
        getSaturday().uncheck();
    }

    @Override
    public boolean isArrow()
    {
        return true;
    }
}
