package com.ihg.automation.selenium.servicecenter.pages.event.level;

import com.ihg.automation.selenium.common.billing.EventBillingDetailsTab;
import com.ihg.automation.selenium.common.billing.EventBillingTabAware;
import com.ihg.automation.selenium.common.earning.EventEarningDetailsTab;
import com.ihg.automation.selenium.common.earning.EventEarningTabAware;
import com.ihg.automation.selenium.common.pages.ClosablePopUpBase;

public class TierLevelDetailsPopUp extends ClosablePopUpBase implements EventEarningTabAware, EventBillingTabAware
{

    public TierLevelDetailsPopUp()
    {
        super("Tier-Level Details");
    }

    @Override
    public EventEarningDetailsTab getEarningDetailsTab()
    {
        return new EventEarningDetailsTab(this);
    }

    @Override
    public EventBillingDetailsTab getBillingDetailsTab()
    {
        return new EventBillingDetailsTab(this);
    }

    public TierLevelDetailsTab getTierLevelDetailsTab()
    {
        return new TierLevelDetailsTab(this);
    }
}
