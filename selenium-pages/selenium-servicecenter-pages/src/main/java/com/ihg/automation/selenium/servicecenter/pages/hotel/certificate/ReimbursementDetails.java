package com.ihg.automation.selenium.servicecenter.pages.hotel.certificate;

import static com.ihg.automation.selenium.gwt.components.utils.FindStepUtils.byXpath;

import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.CustomContainerBase;
import com.ihg.automation.selenium.gwt.components.label.Label;

public class ReimbursementDetails extends CustomContainerBase
{
    public ReimbursementDetails(Container container)
    {
        super(container, "Container", "ReimbursementDetails", byXpath(".//hr/following-sibling::table"));
    }

    public Label getReimbursementMethod()
    {
        return container.getLabel("Reimbursement Method");
    }

    public Label getReimbursementDate()
    {
        return container.getLabel("Reimbursement Date");
    }

    public Label getReimbursementAmount()
    {
        return container.getLabel("Reimbursement Amount");
    }

    public Label getCurrency()
    {
        return container.getLabel("Currency");
    }

}
