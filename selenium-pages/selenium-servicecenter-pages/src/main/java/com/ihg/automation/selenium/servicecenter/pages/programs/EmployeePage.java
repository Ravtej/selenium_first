package com.ihg.automation.selenium.servicecenter.pages.programs;

import com.ihg.automation.selenium.common.types.program.Program;

public class EmployeePage extends SimpleProgramPageBase
{
    public EmployeePage()
    {
        super(Program.EMP);
    }
}
