package com.ihg.automation.selenium.servicecenter.pages.event.stay;

import com.ihg.automation.selenium.gwt.components.CustomContainerBase;
import com.ihg.automation.selenium.gwt.components.label.Label;

public class GuestInfo extends CustomContainerBase
{
    public GuestInfo(CustomContainerBase parent)
    {
        super(parent.container.getSeparator("Guest Details"));
    }

    public Label getGuestName()
    {
        return container.getLabel("Guest Name");
    }

    public Label getPhone()
    {
        return container.getLabel("Phone");
    }

    public Label getEmail()
    {
        return container.getLabel("Email");
    }

    public Label getMemberName()
    {
        return container.getLabel("Member Name");
    }

    public Label getMemberNumber()
    {
        return container.getLabel("Member Number");
    }

    public Label getAddress()
    {
        return container.getLabel("Address");
    }

    public Label getProgram()
    {
        return container.getLabel("Program");
    }
}
