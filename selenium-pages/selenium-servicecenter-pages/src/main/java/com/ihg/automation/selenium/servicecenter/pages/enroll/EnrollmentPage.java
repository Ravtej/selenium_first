package com.ihg.automation.selenium.servicecenter.pages.enroll;

import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.assertNoError;
import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyNoError;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.isDisplayedWithWait;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ihg.automation.selenium.common.catalog.CatalogItem;
import com.ihg.automation.selenium.common.enroll.EnrollProgramContainer;
import com.ihg.automation.selenium.common.enroll.EnrollmentPageBase;
import com.ihg.automation.selenium.common.enroll.PossibleDuplicateFoundPopUp;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberAlliance;
import com.ihg.automation.selenium.common.payment.PaymentDetailsPopUp;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.gwt.components.DualList;
import com.ihg.automation.selenium.gwt.components.input.Select;
import com.ihg.automation.selenium.gwt.components.panel.FieldSet;
import com.ihg.automation.selenium.gwt.components.utils.WaitUtils;
import com.ihg.automation.selenium.servicecenter.pages.TopMenu;

public class EnrollmentPage extends EnrollmentPageBase
{
    private static final Logger logger = LoggerFactory.getLogger(EnrollmentPage.class);

    public EnrollmentPage()
    {
        super(new TopMenu().getEnrollment());
    }

    public DualList getFlags()
    {
        FieldSet flags = container.getFieldSet("Flags");
        flags.expand();

        return flags.getDualList("Flags");
    }

    public EarningPreference getEarningPreferenceSeparator()
    {
        return new EarningPreference(container);
    }

    @Override
    public void populate(Member member)
    {
        super.populate(member);

        MemberAlliance alliance = member.getMilesEarningPreference();

        if (alliance != null)
        {
            EarningPreference preference = getEarningPreferenceSeparator();
            preference.getEarningPreference().select("Miles");

            AllianceDetails allianceDetails = preference.getAllianceDetails();
            allianceDetails.populate(alliance);
        }

        if (!member.getFlags().isEmpty())
        {
            DualList flags = getFlags();
            flags.select(member.getFlags());
        }

        if (null != member.getAmbassadorOfferCode())
        {
            getAMBOfferCode().selectByCode(member.getAmbassadorOfferCode());
        }

        if (null != member.getDiningRewardOfferCode())
        {
            getDROfferCode().selectByCode(member.getDiningRewardOfferCode());
        }

        if (null != member.getRewardClubOfferCode())
        {
            getRCOfferCode().type(member.getRewardClubOfferCode());
        }

        if (null != member.getRewardClubReferredBy())
        {
            getReferredMember().getRCReferredBy().typeAndWait(member.getRewardClubReferredBy());
        }
    }

    public void submitEnrollment(Member member)
    {
        submitEnrollment(member, true);
    }

    public void submitEnrollment(Member member, boolean confirm)
    {
        goTo();
        populate(member);
        if (confirm)
        {
            clickSubmitAndSkipWarnings(assertNoError());

            // Sleep one second waiting for the hotel request
            if (member.getPrograms().containsKey(Program.HTL))
            {
                try
                {
                    Thread.sleep(1000);
                    new WaitUtils().waitExecutingRequest();
                }
                catch (InterruptedException e)
                {
                    logger.error("Sleep exception", e);
                }

            }

            if (null != member.getAmbassadorOfferCode())
            {
                PaymentDetailsPopUp ambEnrollPaymentDetailsPopUp = new PaymentDetailsPopUp();
                ambEnrollPaymentDetailsPopUp.selectAmount(member.getAmbassadorAmount());
                ambEnrollPaymentDetailsPopUp.clickSubmitNoError();
            }

        }
        else
        {
            clickCancel(assertNoError());
        }
    }

    public Member enroll(Member member)
    {
        submitEnrollment(member);

        member = successEnrollmentPopUp(member);

        return member;
    }

    public Member enrollToAnotherProgram(Member member, Program... programs)
    {
        member.addProgram(programs);

        goTo();

        for (Program program : programs)
        {
            new EnrollProgramContainer(container).selectProgram(program);
        }

        clickSubmitAndSkipWarnings(assertNoError());

        member = successEnrollmentPopUp(member);

        return member;
    }

    public void enrollToDROrAMBProgramWithoutProfileChange(Member member, Program program, CatalogItem payment)
    {
        goTo();
        new EnrollProgramContainer(container).selectProgram(program);

        if (program.equals(Program.AMB))
        {
            getAMBOfferCode().selectByCode(member.getAmbassadorOfferCode());
        }

        clickSubmitAndSkipWarnings(verifyNoError());
        PaymentDetailsPopUp paymentDetailsPopUp = new PaymentDetailsPopUp();
        verifyThat(paymentDetailsPopUp, isDisplayedWithWait());

        paymentDetailsPopUp.selectAmount(payment);
        paymentDetailsPopUp.clickSubmitNoError();
        successEnrollmentPopUp(member);
    }

    public Select getAMBOfferCode()
    {
        return container.getSelectByLabel("AMB Offer or Registration Code");
    }

    public Select getDROfferCode()
    {
        return container.getSelectByLabel("DR Offer or Registration Code");
    }

    public Select getIHGOfferCode()
    {
        return container.getSelectByLabel("IHG Offer or Registration Code");
    }

    public Select getRCOfferCode()
    {
        return container.getSelectByLabel("RC Offer or Registration Code");
    }

    public ReferredMember getReferredMember()
    {
        return new ReferredMember(container.getLabel("RC Referred By"));
    }

    public AlternateLoyaltyIDDR getAlternateLoyaltyIDDR()
    {
        return new AlternateLoyaltyIDDR(container.getSeparator("Alternate Loyalty ID - DR"));
    }
}
