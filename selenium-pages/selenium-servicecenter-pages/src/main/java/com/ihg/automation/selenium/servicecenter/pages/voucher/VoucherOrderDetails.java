package com.ihg.automation.selenium.servicecenter.pages.voucher;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static org.hamcrest.core.Is.is;

import com.ihg.automation.selenium.common.order.VoucherOrder;
import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.CustomContainerBase;
import com.ihg.automation.selenium.gwt.components.label.Label;

public class VoucherOrderDetails extends CustomContainerBase
{
    public VoucherOrderDetails(Container container)
    {
        super(container);
    }

    public Label getNumberOfVouchers()
    {
        return container.getLabel("Number of Vouchers");
    }

    public Label getNumberOfVouchersDeposited()
    {
        return container.getLabel("Number of Vouchers Deposited");
    }

    public Label getVoucherNumberRange()
    {
        return container.getLabel("Voucher Number Range");
    }

    public void verify(VoucherOrder voucherOrder)
    {
        verifyThat(getNumberOfVouchers(), hasText(voucherOrder.getNumberOfVouchers()));
        verifyThat(getNumberOfVouchersDeposited(), hasText(voucherOrder.getNumberOfVouchersDeposited()));
        verifyThat(getVoucherNumberRange(), hasText(is(String.format("%s - %s", voucherOrder.getVoucherNumberMinValue(),
                voucherOrder.getVoucherNumberMaxValue()))));
    }
}
