package com.ihg.automation.selenium.servicecenter.pages.event.freenight;

import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.grid.Grid;
import com.ihg.automation.selenium.gwt.components.grid.GridRowXpath;
import com.ihg.automation.selenium.servicecenter.pages.event.freenight.FreeNightVoucherGridRow.FreeNightVoucherCell;

public class FreeNightVoucherGrid extends Grid<FreeNightVoucherGridRow, FreeNightVoucherCell>
{
    public FreeNightVoucherGrid(Container parent)
    {
        super(parent);
    }

    public FreeNightVoucherGridRow getRowByVoucherCode(String voucherCode)
    {
        return new FreeNightVoucherGridRow(this,
                new GridRowXpath.GridRowXpathBuilder().cell(FreeNightVoucherCell.VOUCHER_NUMBER, voucherCode).build());
    }

}
