package com.ihg.automation.selenium.servicecenter.pages.event.stay;

public enum StayEventType
{
    PENDING, SYSTEM, GUEST, HOTEL_CREATED, CREATED, ADJUSTED, TRANSFERRED_TO, TRANSFERRED_FROM, UNASSIGNED, MERGE,

    /**
     * Synthetic event type. Will be displayed for stays which have no
     * membership details but assigned to a PCR member.
     */
    NOT_POSTED
}
