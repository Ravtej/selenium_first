package com.ihg.automation.selenium.servicecenter.pages.account;

import com.ihg.automation.selenium.common.pages.MultiModeBase;
import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.TextArea;
import com.ihg.automation.selenium.gwt.components.label.Label;

public class Comments extends MultiModeBase
{
    public Comments(Container parent)
    {
        super(parent.getSeparator("Comments"));
    }

    public TextArea getComments()
    {
        return new TextArea(container, "Comments");
    }

    public Label getCommentsLabel()
    {
        return container.getLabel("Comments");
    }
}
