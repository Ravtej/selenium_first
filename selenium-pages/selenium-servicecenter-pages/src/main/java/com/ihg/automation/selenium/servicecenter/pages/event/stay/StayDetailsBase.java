package com.ihg.automation.selenium.servicecenter.pages.event.stay;

import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.CustomContainerBase;

public abstract class StayDetailsBase extends CustomContainerBase implements StayDetailsAware
{
    public StayDetailsBase(Container parent)
    {
        super(parent);
    }

    @Override
    public GuestInfo getGuestInfo()
    {
        return new GuestInfo(this);
    }

    @Override
    public StayBookingInfoEdit getBookingInfo()
    {
        return new StayBookingInfoEdit(this);
    }

    @Override
    public RevenueDetailsEdit getRevenueDetails()
    {
        return new RevenueDetailsEdit(this);
    }
}
