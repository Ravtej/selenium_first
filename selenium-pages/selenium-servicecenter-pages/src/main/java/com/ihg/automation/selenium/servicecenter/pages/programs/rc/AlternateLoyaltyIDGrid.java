package com.ihg.automation.selenium.servicecenter.pages.programs.rc;

import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.grid.Grid;
import com.ihg.automation.selenium.gwt.components.grid.GridRowXpath;
import com.ihg.automation.selenium.gwt.components.utils.ByText;

public class AlternateLoyaltyIDGrid extends Grid<AlternateLoyaltyIDRow, AlternateLoyaltyIDRow.AlternateCell>
{
    public AlternateLoyaltyIDGrid(Container container)
    {
        super(container);
    }

    public AlternateLoyaltyIDRow getRow(String number)
    {
        return getRow(new GridRowXpath.GridRowXpathBuilder()
                .cell(AlternateLoyaltyIDRow.AlternateCell.NUMBER, ByText.exactSelf(number)).build());
    }
}
