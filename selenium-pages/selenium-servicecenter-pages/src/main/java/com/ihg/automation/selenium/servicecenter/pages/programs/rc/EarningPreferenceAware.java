package com.ihg.automation.selenium.servicecenter.pages.programs.rc;

import com.ihg.automation.selenium.gwt.components.Component;

public interface EarningPreferenceAware
{
    public static final String PREFERENCE_LABEL = "Earning Preference";

    public Component getEarningPreference();
}
