package com.ihg.automation.selenium.servicecenter.pages;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasDefault;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasText;
import static com.ihg.automation.selenium.matchers.text.EntityMatcherFactory.isValue;
import static org.hamcrest.CoreMatchers.containsString;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ihg.automation.selenium.common.components.Country;
import com.ihg.automation.selenium.common.components.CountrySelect;
import com.ihg.automation.selenium.common.components.RangeSelect;
import com.ihg.automation.selenium.common.pages.SearchBase;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.CustomContainerBase;
import com.ihg.automation.selenium.gwt.components.input.DateInput;
import com.ihg.automation.selenium.gwt.components.input.Input;
import com.ihg.automation.selenium.gwt.components.input.InputXpath.InputXpathBuilder;
import com.ihg.automation.selenium.gwt.components.input.SearchInput;
import com.ihg.automation.selenium.gwt.components.input.Select;
import com.ihg.automation.selenium.gwt.components.panel.FieldSet;
import com.ihg.automation.selenium.gwt.components.panel.Panel;
import com.ihg.automation.selenium.listener.Verifier;

public class GuestSearch extends SearchBase
{
    private static final Logger logger = LoggerFactory.getLogger(GuestSearch.class);

    public GuestSearch()
    {
        super(new Panel("Guest Search"));
    }

    public CustomerSearch getCustomerSearch()
    {
        return new CustomerSearch(container);
    }

    public SearchByStay getSearchByStay()
    {
        return new SearchByStay(container);
    }

    public void byMemberNumber(String memberNumber, Verifier... verifiers)
    {
        logger.info("Enter membership id and search");
        clickClear();
        getCustomerSearch().getMemberNumber().type(memberNumber);
        clickSearch(verifiers);
    }

    public void byNumber(String number, Verifier... verifiers)
    {
        logger.info("Enter number and search");
        clickClear();
        getCustomerSearch().getNumber().type(number);
        clickSearch(verifiers);
    }

    public void byName(String lastName, String firstName, Verifier... verifiers)
    {
        logger.info("Enter names and search");
        clickClear();
        CustomerSearch customerSearch = getCustomerSearch();
        customerSearch.getLastName().type(lastName);
        customerSearch.getFirstName().type(firstName);
        clickSearch(verifiers);
    }

    public class CustomerSearch extends CustomContainerBase
    {
        public CustomerSearch(Container container)
        {
            super(container.getFieldSet("Customer Search"));
        }

        public Input getMemberNumber()
        {
            return container.getInput("Member Number", 1);
        }

        public Input getNumber()
        {
            return container.getInput("Number", 2);
        }

        public Input getSocialID()
        {
            return container.getInput("Social ID", 3);
        }

        public Input getLastName()
        {
            return container.getInput("Last Name", 4);
        }

        public Input getFirstName()
        {
            return container.getInput("First Name", 5);
        }

        public Input getStreetAddress()
        {
            return container.getInput("Street Address", 6);
        }

        public Input getCity()
        {
            return container.getInput("City, State or Province", 7);
        }

        public Input getZip()
        {
            return container.getInput("Zip", 8);
        }

        public CountrySelect getCountry()
        {
            return new CountrySelect(container, new InputXpathBuilder().row(9).build(), "Country");
        }

        public Input getPhone()
        {
            return container.getInput("Phone", 10);
        }

        public Select getEmail()
        {
            return container.getSelect("Email", 11);
        }

        public Select getProgram()
        {
            return container.getSelect("Program", 12);
        }

        public Select getStatus()
        {
            return container.getSelect("Status", 13);
        }

        public void verifyDefault()
        {
            logger.info("verify default search criteria");
            verifyThat(getMemberNumber(), hasText("Member Number"));
            verifyThat(getNumber(), hasText("Number"));
            verifyThat(getLastName(), hasText("Last Name"));
            verifyThat(getFirstName(), hasText("First Name"));
            verifyThat(getStreetAddress(), hasText(containsString("Street Address")));
            verifyThat(getCity(), hasText(containsString("City, State or Province")));
            verifyThat(getZip(), hasText(containsString("ZIP or Postal Code")));
            verifyThat(getCountry(), hasText(isValue(Country.US)));
            verifyThat(getPhone(), hasText("Phone"));
            verifyThat(getEmail(), hasText("Email"));
            verifyThat(getProgram(), hasText(Program.RC.getCode()));
            verifyThat(getStatus(), hasDefault());
        }
    }

    public class SearchByStay extends CustomContainerBase
    {
        public SearchByStay(Container container)
        {
            super(container.getFieldSet("Search by Stay"));
        }

        public SearchInput getHotel()
        {
            return new SearchInput(container, new InputXpathBuilder().row(1).build(), "Hotel");
        }

        public Input getHotelName()
        {
            return container.getInput("Hotel Name", 2);
        }

        public DateInput getStayDate()
        {
            return new DateInput(container, new InputXpathBuilder().row(3).column(1).build(), "Stay Date");
        }

        public RangeSelect getStayDateRange()
        {
            return new RangeSelect(container, new InputXpathBuilder().row(3).column(2).build(), "Stay Date Range");
        }

        public Input getFolioNumber()
        {
            return container.getInput("Folio Number", 4);
        }

        public Input getRoomNumber()
        {
            return container.getInput("Room Number", 5);
        }

        public Input getCreditCardNumber()
        {
            return container.getInput("CC Number", 6);
        }

        public Input getConfirmationNumber()
        {
            return container.getInput("Confirmation Number", 7);
        }

        public void expand()
        {
            ((FieldSet) container).expand();
        }
    }

}
