package com.ihg.automation.selenium.servicecenter.pages.hotel.freenight;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasTextWithWait;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;

import com.ihg.automation.selenium.common.hotel.HotelNightDetails;
import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.CustomContainerBase;
import com.ihg.automation.selenium.gwt.components.label.Label;
import com.ihg.automation.selenium.gwt.components.label.TableLabel;
import com.ihg.automation.selenium.gwt.components.utils.ByText;
import com.ihg.automation.selenium.gwt.components.utils.FindStepUtils;

public class FreeNightReimbursementSearchRowDetail extends CustomContainerBase
{
    public FreeNightReimbursementSearchRowDetail(Container parent, String xPath)
    {
        super(new Container(parent, "Night Reimbursement Detail", "Container", FindStepUtils.byXpathWithWait(xPath)));
    }

    public Label getConfirmationNumber()
    {
        return container.getLabel("Confirmation Number");
    }

    public Label getCertificateNumber()
    {
        return container.getLabel("Certificate Number");
    }

    public Label getReimbursementAmount()
    {
        return container.getLabel("Reimbursement Amount");
    }

    public Label getMemberID()
    {
        return container.getLabel("Member ID");
    }

    public Label getSuffix()
    {
        return container.getLabel("Suffix");
    }

    public Label getReimbursementMethod()
    {
        return container.getLabel("Reimbursement Method");
    }

    public Label getLastName()
    {
        return container.getLabel("Last Name");
    }

    public Label getStatus()
    {
        return container.getLabel("Status");
    }

    public Label getReimbursementDate()
    {
        return container.getLabel("Reimbursement Date");
    }

    public Label getCertificateType()
    {
        return container.getLabel("Certificate Type");
    }

    public Label getOfferName()
    {
        return container.getLabel("Offer Name");
    }

    public TableLabel getCheckInDate()
    {
        return new TableLabel(container, ByText.exact("Check In Date"));
    }

    public TableLabel getOCC()
    {
        return new TableLabel(container, ByText.exact("OCC %"));
    }

    public TableLabel getBase()
    {
        return new TableLabel(container, ByText.exact("Base"));
    }

    public TableLabel getTax()
    {
        return new TableLabel(container, ByText.exact("Tax"));
    }

    public TableLabel getFee()
    {
        return new TableLabel(container, ByText.exact("Fee"));
    }

    public TableLabel getTotal()
    {
        return new TableLabel(container, ByText.exact("Total"));
    }

    public TableLabel getCurrency()
    {
        return new TableLabel(container, ByText.exact("Currency"));
    }

    public void verify(HotelNightDetails detail)
    {
        verifyThat(getConfirmationNumber(), hasText(detail.getConfirmationNumber()));
        verifyThat(getCertificateNumber(), hasText(detail.getCertificateNumber()));
        verifyThat(getMemberID(), hasText(detail.getMemberID()));
        verifyThat(getSuffix(), hasText(detail.getSuffix()));
        verifyThat(getLastName(), hasText(detail.getLastName()));
        verifyThat(getStatus(), hasText(detail.getStatus()));
        verifyThat(getCertificateType(), hasTextWithWait(detail.getCertificateType()));
        verifyThat(getOfferName(), hasText(detail.getOfferName()));

        verifyThat(getReimbursementAmount(), hasText(detail.getReimbursementAmount()));
        verifyThat(getReimbursementMethod(), hasText(detail.getReimbursementMethod()));
        verifyThat(getReimbursementDate(), hasText(detail.getReimbursementDate()));

        verifyThat(getCheckInDate(), hasText(detail.getCheckInDate()));
        verifyThat(getOCC(), hasText(detail.getOCC()));
        verifyThat(getBase(), hasText(detail.getBase()));
        verifyThat(getTax(), hasText(detail.getTax()));
        verifyThat(getFee(), hasText(detail.getFee()));
        verifyThat(getTotal(), hasText(detail.getTotal()));
        verifyThat(getCurrency(), hasText(detail.getCurrency()));
    }

}
