package com.ihg.automation.selenium.servicecenter.pages.event.all;

import com.ihg.automation.selenium.common.EventSourceAware;
import com.ihg.automation.selenium.common.event.EventSourceExtended;
import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.CustomContainerBase;

public class RegionTransferContainer extends CustomContainerBase implements EventSourceAware
{
    public RegionTransferContainer(Container parent)
    {
        super(parent);
    }

    @Override
    public EventSourceExtended getSource()
    {
        return new EventSourceExtended(container);
    }
}
