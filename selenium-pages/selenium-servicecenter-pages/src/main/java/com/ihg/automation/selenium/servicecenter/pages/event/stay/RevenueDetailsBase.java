package com.ihg.automation.selenium.servicecenter.pages.event.stay;

import com.ihg.automation.selenium.common.pages.Verify;
import com.ihg.automation.selenium.common.stay.Revenue;
import com.ihg.automation.selenium.common.stay.Revenues;
import com.ihg.automation.selenium.gwt.components.CustomContainerBase;
import com.ihg.automation.selenium.gwt.components.MultiModeComponent;

public abstract class RevenueDetailsBase extends CustomContainerBase implements Verify<Revenues>
{
    protected final static String TOTAL_ROOM_LABEL = "Total Room";
    protected final static String FOOD_LABEL = "Food";
    protected final static String BEVERAGE_LABEL = "Beverage";
    protected final static String PHONE_LABEL = "Phone";
    protected final static String MEETING_LABEL = "Meeting";
    protected final static String MANDATORY_REVENUE_LABEL = "Miscellaneous MANDATORY Loyalty Revenue";
    protected final static String OTHER_REVENUE_LABEL = "Miscellaneous OTHER Loyalty Revenue";
    protected final static String NO_REVENUE_LABEL = "Miscellaneous NO Loyalty Revenue";
    protected final static String ROOM_TAX_LABEL = "Room Tax/Fee/Non-Revenue";
    protected final static String TOTAL_REVENUE_LABEL = "Overall Total Revenue";
    protected final static String TOTAL_NON_QUALIFYING_REVENUE_LABEL = "Total Non Qualifying Loyalty Revenue";
    protected final static String TOTAL_QUALIFYING_REVENUE_LABEL = "Total Qualifying Loyalty Revenue";

    public RevenueDetailsBase(CustomContainerBase parent)
    {
        this(parent, "Revenue Details");
    }

    protected RevenueDetailsBase(CustomContainerBase parent, String separator)
    {
        super(parent.container.getSeparator(separator));
    }

    public abstract MultiModeComponent getTotalRoom();

    public abstract MultiModeComponent getFood();

    public abstract MultiModeComponent getBeverage();

    public abstract MultiModeComponent getPhone();

    public abstract MultiModeComponent getMeeting();

    public abstract MultiModeComponent getMandatoryRevenue();

    public abstract MultiModeComponent getOtherRevenue();

    public abstract MultiModeComponent getNoRevenue();

    public abstract MultiModeComponent getRoomTax();

    public abstract MultiModeComponent getTotalRevenue();

    public abstract MultiModeComponent getTotalNonQualifyingRevenue();

    public abstract MultiModeComponent getTotalQualifyingRevenue();
}
