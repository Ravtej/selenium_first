package com.ihg.automation.selenium.servicecenter.pages.unit;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.assertNoErrors;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.assertThat;
import static com.ihg.automation.selenium.common.Constant.RC_POINTS;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;

import com.ihg.automation.selenium.common.AmountFieldsAware;
import com.ihg.automation.selenium.common.pages.NavigatePage;
import com.ihg.automation.selenium.common.pages.SubmitPopUpBase;
import com.ihg.automation.selenium.common.types.GoodwillReason;
import com.ihg.automation.selenium.gwt.components.ComponentColumnPosition;
import com.ihg.automation.selenium.gwt.components.input.Input;
import com.ihg.automation.selenium.gwt.components.input.Select;
import com.ihg.automation.selenium.servicecenter.pages.TopMenu;

public class GoodwillPopUp extends SubmitPopUpBase implements NavigatePage, AmountFieldsAware
{
    public static final String GOODWILL_SUCCESS = "New Goodwill Event was successfully submitted";

    private TopMenu menu = new TopMenu();

    private static final ComponentColumnPosition POSITION = ComponentColumnPosition.FOLLOWING_TD;

    public GoodwillPopUp()
    {
        super("Goodwill Loyalty Units");
    }

    @Override
    public void goTo()
    {
        if (!isDisplayed())
        {
            menu.getUnitManagement().getGoodwillUnits().clickAndWait();

            flush();
            assertThat(this, displayed(true));
        }
    }

    @Override
    public Input getAmount()
    {
        return container.getInputByLabel("Amount", "Quantity", 1, POSITION);
    }

    @Override
    public Select getUnitType()
    {
        return container.getSelectByLabel("Amount", "Units Type", 2, POSITION);
    }

    public Select getReason()
    {
        return container.getSelectByLabel("Reason Code");
    }

    public void populate(String amount, String pointsType, GoodwillReason reason)
    {
        getAmount().type(amount);
        getUnitType().select(pointsType);
        getReason().selectByCode(reason);
    }

    public void goodwillUnits(String amount, String pointsType, GoodwillReason reason)
    {
        goTo();
        assertNoErrors();
        populate(amount, pointsType, reason);
        clickSubmit();
    }

    public void goodwillPoints(String amount, GoodwillReason reason)
    {
        goodwillUnits(amount, RC_POINTS, reason);
    }

    public void goodwillPoints(String amount)
    {
        goodwillPoints(amount, GoodwillReason.AGENT_ERROR);
    }

    public void goodwillPoints(Integer amount)
    {
        goodwillPoints(String.valueOf(amount));
    }
}
