package com.ihg.automation.selenium.servicecenter.pages.programs;

import com.ihg.automation.selenium.common.EventSource;
import com.ihg.automation.selenium.common.EventSourceAware;
import com.ihg.automation.selenium.common.SiteHelperBase;
import com.ihg.automation.selenium.common.pages.MultiModeBase;
import com.ihg.automation.selenium.common.pages.TabCustomContainerBase;
import com.ihg.automation.selenium.common.payment.PaymentDetails;
import com.ihg.automation.selenium.common.payment.PaymentDetailsAware;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.Link;
import com.ihg.automation.selenium.gwt.components.input.DateInput;
import com.ihg.automation.selenium.gwt.components.label.Label;
import com.ihg.automation.selenium.gwt.components.panel.FieldSet;
import com.ihg.automation.selenium.servicecenter.pages.Tabs;

public abstract class ProgramPageBase extends TabCustomContainerBase
{
    private Program program;
    public final static String WARNING_MESSAGE = "Current editing view is modified\nPlease save or cancel the updates";
    public final static String SUCCESS_MESSAGE = "Program info has been successfully saved.";
    public final static String BALANCE_MESSAGE = "Loyalty Unit Balance will be reduced to Zero if you continue";

    public ProgramPageBase(Program program)
    {
        super(new Tabs().getProgramInfo().getProgram(program));
        this.program = program;
    }

    abstract public ProgramSummary getSummary();

    public FieldSet getProgramOverview()
    {
        return container.getFieldSet(program.getValue() + " Overview");
    }

    public EnrollmentDetails getEnrollmentDetails()
    {
        return new EnrollmentDetails(container);
    }

    public class EnrollmentDetails extends MultiModeBase implements EventSourceAware, PaymentDetailsAware
    {
        EnrollmentDetails(Container container)
        {
            super(container.getSeparator("Enrollment Details"));
        }

        @Override
        public EventSource getSource()
        {
            return new EventSource(container);
        }

        @Override
        public PaymentDetails getPaymentDetails()
        {
            return new PaymentDetails(container);
        }

        public DateInput getMemberSince()
        {
            DateInput birthDate = new DateInput(container.getLabel("Member Since"));
            return birthDate;
        }

        public Label getEnrollDate()
        {
            return container.getLabel("Enrollment Date");
        }

        public Label getRenewalDate()
        {
            return container.getLabel("Renewal date");
        }

        public Label getOfferCode()
        {
            return container.getLabel("Offer Code");
        }

        public Link getReferringMember()
        {
            return new Link(container.getLabel("Referring Member"), Link.LINK_XPATH_NO_HYPER_LINK_IN_CLASS);
        }

        public void verifyScSource(SiteHelperBase siteHelper)
        {
            getSource().verify(siteHelper.getSourceFactory().getSource());
        }
    }
}
