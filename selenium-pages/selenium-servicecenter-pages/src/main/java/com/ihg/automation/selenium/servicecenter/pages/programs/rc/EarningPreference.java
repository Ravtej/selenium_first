package com.ihg.automation.selenium.servicecenter.pages.programs.rc;

import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyNoError;

import com.ihg.automation.selenium.common.Constant;
import com.ihg.automation.selenium.common.pages.MultiModeBase;
import com.ihg.automation.selenium.common.types.Alliance;
import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.input.Select;

public class EarningPreference extends MultiModeBase
{
    public EarningPreference(Container container)
    {
        super(container.getSeparator("Earning Details"));
    }

    public Select getEarningPreference()
    {
        return container.getSelectByLabel("Earning Preference");
    }

    public void resetEarningPreference()
    {
        clickEdit();
        getEarningPreference().select(Constant.RC_POINTS);
        clickSave(verifyNoError());
    }

    public void setEarningPreference(Alliance alliance)
    {
        clickEdit();
        getEarningPreference().selectByCode(alliance);
        clickSave(verifyNoError());
    }
}
