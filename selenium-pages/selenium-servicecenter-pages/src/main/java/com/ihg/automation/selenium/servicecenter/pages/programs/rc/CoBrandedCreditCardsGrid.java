package com.ihg.automation.selenium.servicecenter.pages.programs.rc;

import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.grid.Grid;

public class CoBrandedCreditCardsGrid
        extends Grid<CoBrandedCreditCardsRow, CoBrandedCreditCardsRow.CoBrandedCreditCardsCell>
{
    public CoBrandedCreditCardsGrid(Container container)
    {
        super(container);
    }
}
