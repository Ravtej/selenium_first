package com.ihg.automation.selenium.servicecenter.pages.programs.rc;

import com.ihg.automation.selenium.common.pages.SavePopUpBase;

public class AllianceEditPopUp extends SavePopUpBase
{
    public AllianceEditPopUp()
    {
        super("Edit Alliance");
    }

    public AllianceDetailsEdit getAllianceDetails()
    {
        return new AllianceDetailsEdit(container);
    }
}
