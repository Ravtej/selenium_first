package com.ihg.automation.selenium.servicecenter.pages.unit;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.assertThat;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;

import com.ihg.automation.selenium.common.AmountFieldsAware;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.pages.NavigatePage;
import com.ihg.automation.selenium.common.pages.PaymentMethod;
import com.ihg.automation.selenium.common.pages.SubmitPopUpBase;
import com.ihg.automation.selenium.common.payment.PaymentDetailsPopUp;
import com.ihg.automation.selenium.common.unit.MemberFieldsAware;
import com.ihg.automation.selenium.gwt.components.ComponentColumnPosition;
import com.ihg.automation.selenium.gwt.components.checkbox.CheckBox;
import com.ihg.automation.selenium.gwt.components.input.Input;
import com.ihg.automation.selenium.gwt.components.input.Select;
import com.ihg.automation.selenium.gwt.components.label.Label;
import com.ihg.automation.selenium.servicecenter.pages.TopMenu;

public class TransferUnitsPopUp extends SubmitPopUpBase implements NavigatePage, MemberFieldsAware, AmountFieldsAware
{
    private TopMenu menu = new TopMenu();

    private static final ComponentColumnPosition POSITION = ComponentColumnPosition.SINGLE_TD_SWITCH_MODE;

    public TransferUnitsPopUp()
    {
        super("Transfer Loyalty Units");
    }

    @Override
    public void goTo()
    {
        if (!isDisplayed())
        {
            menu.getUnitManagement().getTransferUnits().clickAndWait();

            flush();
            assertThat(this, displayed(true));
        }
    }

    @Override
    public Input getMembershipId()
    {
        return container.getInputByLabel("Membership ID");
    }

    @Override
    public Label getMemberName()
    {
        return container.getLabel("Member Name");
    }

    @Override
    public Select getAmount()
    {
        return container.getSelectByLabel("Quantity to Transfer", "Quantity", 1, POSITION);
    }

    @Override
    public Select getUnitType()
    {
        return container.getSelectByLabel("Quantity to Transfer", "Units Type", 2, POSITION);
    }

    public CheckBox getBalanceTransfer()
    {
        return container.getLabel("Balance Transfer").getCheckBox();
    }

    public void transferPoints(Member recipient, String pointAmount)
    {
        goTo();

        getMembershipId().typeAndSubmit(recipient.getRCProgramId());
        getAmount().select(pointAmount);
        clickSubmitNoError();

        PaymentDetailsPopUp paymentDetailsPopUp = new PaymentDetailsPopUp();
        paymentDetailsPopUp.selectPaymentType(PaymentMethod.COMPLIMENTARY);
        paymentDetailsPopUp.clickSubmitNoError();
    }
}
