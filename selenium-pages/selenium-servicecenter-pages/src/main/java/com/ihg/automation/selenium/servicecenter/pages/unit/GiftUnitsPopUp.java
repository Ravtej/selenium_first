package com.ihg.automation.selenium.servicecenter.pages.unit;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.assertThat;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;

import com.ihg.automation.selenium.common.AmountFieldsAware;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.pages.NavigatePage;
import com.ihg.automation.selenium.common.pages.PaymentMethod;
import com.ihg.automation.selenium.common.pages.SubmitPopUpBase;
import com.ihg.automation.selenium.common.payment.PaymentDetailsPopUp;
import com.ihg.automation.selenium.common.unit.MemberFieldsAware;
import com.ihg.automation.selenium.gwt.components.input.Input;
import com.ihg.automation.selenium.gwt.components.input.Select;
import com.ihg.automation.selenium.gwt.components.label.Label;
import com.ihg.automation.selenium.gwt.components.utils.ByText;
import com.ihg.automation.selenium.servicecenter.pages.TopMenu;

public class GiftUnitsPopUp extends SubmitPopUpBase implements NavigatePage, MemberFieldsAware, AmountFieldsAware
{
    private TopMenu menu = new TopMenu();

    public GiftUnitsPopUp()
    {
        super("Gift Loyalty Units");
    }

    @Override
    public void goTo()
    {
        if (!isDisplayed())
        {
            menu.getUnitManagement().getPurchaseGift().clickAndWait();

            flush();
            assertThat(this, displayed(true));
        }
    }

    public Label getMembershipIdLabel()
    {
        return new Label(container, ByText.exact("Membership ID"));
    }

    @Override
    public Input getMembershipId()
    {
        return container.getInputByLabel("Membership ID");
    }

    @Override
    public Label getMemberName()
    {
        return container.getLabel("Member Name");
    }

    @Override
    public Select getAmount()
    {
        return container.getSelectByLabel("Quantity to Gift", "Quantity", 1);
    }

    @Override
    public Select getUnitType()
    {
        return container.getSelectByLabel("Quantity to Gift", "Units Type", 2);
    }

    public void giftPoints(Member recipient, String pointAmount)
    {
        goTo();

        getMembershipId().typeAndSubmit(recipient.getRCProgramId());
        getAmount().select(pointAmount);
        clickSubmitNoError();

        PaymentDetailsPopUp paymentDetailsPopUp = new PaymentDetailsPopUp();
        paymentDetailsPopUp.selectPaymentType(PaymentMethod.COMPLIMENTARY);
        paymentDetailsPopUp.clickSubmitNoError();
    }
}
