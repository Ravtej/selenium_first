package com.ihg.automation.selenium.servicecenter.pages.personal;

import com.ihg.automation.selenium.common.pages.TabCustomContainerBase;
import com.ihg.automation.selenium.common.personal.EmailContactList;
import com.ihg.automation.selenium.common.personal.PhoneContactList;
import com.ihg.automation.selenium.common.personal.SmsContactList;
import com.ihg.automation.selenium.servicecenter.pages.Tabs;

public class PersonalInfoPage extends TabCustomContainerBase
{
    public static final String SUCCESS_MESSAGE = "Program info has been successfully saved.";

    public PersonalInfoPage()
    {
        super(new Tabs().getCustomerInfo().getPersonalInfo());
    }

    public CustomerInfoFields getCustomerInfo()
    {
        return new CustomerInfoFields(container);
    }

    public AddressContactList getAddressList()
    {
        return new AddressContactList(container);
    }

    public PhoneContactList getPhoneList()
    {
        return new PhoneContactList(container);
    }

    public SmsContactList getSmsList()
    {
        return new SmsContactList(container);
    }

    public EmailContactList getEmailList()
    {
        return new EmailContactList(container);
    }

    public Passport getPassport()
    {
        return new Passport(container);
    }

    public SocialId getSocialId()
    {
        return new SocialId(container);
    }
}
