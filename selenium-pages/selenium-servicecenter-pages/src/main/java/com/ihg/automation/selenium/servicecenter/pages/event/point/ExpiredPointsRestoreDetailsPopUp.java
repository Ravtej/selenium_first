package com.ihg.automation.selenium.servicecenter.pages.event.point;

public class ExpiredPointsRestoreDetailsPopUp extends PointExpirationDetailsPopUpBase
{
    public ExpiredPointsRestoreDetailsPopUp()
    {
        super("Expired Points Restore Details");
    }
}
