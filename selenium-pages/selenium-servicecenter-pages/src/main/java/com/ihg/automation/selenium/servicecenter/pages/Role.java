package com.ihg.automation.selenium.servicecenter.pages;

public class Role
{
    private Role()
    {
    }

    public static final String AGENT = "AGENT";
    public static final String AGENT_SPECIALTY_PROGRAM_SALES = "AGENT-SPECIALTY PROGRAM SALES";
    public static final String AGENT_TIER2 = "AGENT-TIER 2";
    public static final String HOTEL_HELP_DESK = "HOTEL HELP DESK";
    public static final String ADMINISTRATION_AWARD_SUPPORT = "ADMINISTRATION-AWARD SUPPORT";
    public static final String ADMINISTRATION_OPERATIONS_MANAGER = "ADMINISTRATION-OPERATIONS MANAGER";
    public static final String ADMINISTRATION_CENTRAL_PROCESSING = "ADMINISTRATION-CENTRAL PROCESSING";
    public static final String FRAUD_TEAM = "FRAUD TEAM";
    public static final String AGENT_READ_ONLY_LOOKUP = "AGENT-READ ONLY LOOKUP";
    public static final String SYSTEMS_ADMINISTRATION = "SYSTEMS-ADMINISTRATION";
    public static final String SYSTEMS_MANAGER = "SYSTEMS-MANAGER";
    public static final String SYSTEMS_COORDINATOR = "SYSTEMS-COORDINATOR";
    public static final String MANAGER_MARKETING = "MANAGER-MARKETING";
    public static final String READ_ONLY_MARKETING = "READ ONLY-MARKETING";
    public static final String READ_ONLY_VENDOR = "READ ONLY-VENDOR";
    public static final String CORPORATE_OFFICE_HUMAN_RESOURCES = "CORPORATE OFFICE-HUMAN RESOURCES";
    public static final String BUSINESS_REWARDS = "BUSINESS REWARDS";
    public static final String PARTNER_VENDOR = "PARTNER-VENDOR";
}
