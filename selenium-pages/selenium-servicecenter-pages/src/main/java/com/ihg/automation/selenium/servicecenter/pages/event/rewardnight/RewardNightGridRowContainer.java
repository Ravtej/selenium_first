package com.ihg.automation.selenium.servicecenter.pages.event.rewardnight;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;

import com.ihg.automation.selenium.common.EventSource;
import com.ihg.automation.selenium.common.EventSourceAware;
import com.ihg.automation.selenium.common.event.EventGridRowContainerWithDetails;
import com.ihg.automation.selenium.common.event.Source;
import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.label.Label;

public class RewardNightGridRowContainer extends EventGridRowContainerWithDetails implements EventSourceAware
{

    public RewardNightGridRowContainer(Container parent)
    {
        super(parent);
    }

    @Override
    public EventSource getSource()
    {
        return new EventSource(container);
    }

    public Label getConfirmationNumber()
    {
        return container.getLabel("Confirmation Number");
    }

    public Label getCancellationNumber()
    {
        return container.getLabel("Cancellation Number");
    }

    public Label getRoomType()
    {
        return container.getLabel("Room Type");
    }

    public Label getType()
    {
        return container.getLabel("Type");
    }

    public Label getCertificateNumber()
    {
        return container.getLabel("Certificate Number");
    }

    public Label getNumberOfRooms()
    {
        return container.getLabel("Number of Rooms");
    }

    public void verify(String confirmationNumber, String roomType, String type, String certificateNumber,
            String numberOfRooms)
    {
        verifyThat(getRoomType(), hasText(roomType));
        verifyThat(getType(), hasText(type));
        verifyThat(getCertificateNumber(), hasText(certificateNumber));
        verifyThat(getNumberOfRooms(), hasText(numberOfRooms));

        if (getConfirmationNumber().isDisplayed())
        {
            verifyThat(getConfirmationNumber(), hasText(confirmationNumber));
        }

        if (getCancellationNumber().isDisplayed())
        {
            verifyThat(getCancellationNumber(), hasText(confirmationNumber));
        }
    }

    public void verify(String confirmationNumber, String roomType, String type, String certificateNumber,
            String numberOfRooms, Source source)
    {
        verify(confirmationNumber, roomType, type, certificateNumber, numberOfRooms);
        getSource().verify(source);
    }
}
