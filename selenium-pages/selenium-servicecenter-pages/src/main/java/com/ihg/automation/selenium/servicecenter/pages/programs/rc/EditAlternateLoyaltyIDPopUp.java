package com.ihg.automation.selenium.servicecenter.pages.programs.rc;

import com.ihg.automation.selenium.gwt.components.Button;
import com.ihg.automation.selenium.listener.Verifier;

public class EditAlternateLoyaltyIDPopUp extends AlternateLoyaltyIDPopUpBase
{
    public EditAlternateLoyaltyIDPopUp()
    {
        super("Edit Alternate Loyalty ID");
    }

    public Button getUpgrade()
    {
        return container.getButton("Upgrade");
    }

    public void clickUpgrade(Verifier... verifiers)
    {
        getUpgrade().clickAndWait(verifiers);
    }
}
