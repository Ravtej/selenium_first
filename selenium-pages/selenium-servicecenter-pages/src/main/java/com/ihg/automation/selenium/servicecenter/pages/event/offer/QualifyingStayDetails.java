package com.ihg.automation.selenium.servicecenter.pages.event.offer;

import java.util.List;

import org.openqa.selenium.WebElement;

import com.ihg.automation.selenium.gwt.components.Axis;
import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.CustomContainerBase;
import com.ihg.automation.selenium.gwt.components.Separator;

public class QualifyingStayDetails extends CustomContainerBase
{
    public QualifyingStayDetails(Container container)
    {
        super(container.getSeparator("Qualifying Stay Details", Separator.FIELDSET_PATTERN_NEXT_TR));
    }

    public HotelDetails getStayDetailsItem(int rowNumber)
    {
        return new HotelDetails(container, rowNumber);
    }

    public HotelDetails getStayDetailsItem(String hotel)
    {
        return new HotelDetails(container, hotel);
    }

    protected List<WebElement> getItems()
    {
        return findElements(Axis.DESCENDANT.getXpathLocator(".//div[descendant::tr]//tr"), "StayDetailsItems list");
    }

    public int getHotelDetailsItemsCount()
    {
        return getItems().size() - 1;
    }

}
