package com.ihg.automation.selenium.servicecenter.pages.deposit;

import com.ihg.automation.selenium.common.pages.TabCustomContainerBase;
import com.ihg.automation.selenium.servicecenter.pages.event.deposit.DepositDetails;
import com.ihg.automation.selenium.servicecenter.pages.event.deposit.DepositDetailsAware;

public class DepositDetailsTab extends TabCustomContainerBase implements DepositDetailsAware
{
    public DepositDetailsTab(DepositDetailsPopUp parent)
    {
        super(parent, "Deposit Details");
    }

    @Override
    public DepositDetails getDepositDetails()
    {
        return new DepositDetails(container);
    }
}
