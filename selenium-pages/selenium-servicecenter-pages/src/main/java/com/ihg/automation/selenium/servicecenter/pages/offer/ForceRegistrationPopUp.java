package com.ihg.automation.selenium.servicecenter.pages.offer;

import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyNoError;
import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifySuccess;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static org.hamcrest.CoreMatchers.containsString;

import com.ihg.automation.selenium.common.pages.PopUpBase;
import com.ihg.automation.selenium.gwt.components.Button;
import com.ihg.automation.selenium.gwt.components.input.Select;

public class ForceRegistrationPopUp extends PopUpBase
{
    public ForceRegistrationPopUp()
    {
        super("Force registration");
    }

    public Button getCancel()
    {
        return container.getButton(Button.CANCEL);
    }

    public Button getForceRegister()
    {
        return container.getButton("Force Register");
    }

    public Select getReason()
    {
        return container.getSelect("Please select a reason");
    }

    public void forceRegisterToOffer(String reason)
    {
        getReason().select(reason);
        getForceRegister().clickAndWait(verifyNoError(),
                verifySuccess(hasText(containsString("Congratulations guest has been successfully registered"))));
    }
}
