package com.ihg.automation.selenium.servicecenter.pages.personal;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.servicecenter.pages.personal.SocialIdGridRow.SocialIdCell.DELETE;
import static com.ihg.automation.selenium.servicecenter.pages.personal.SocialIdGridRow.SocialIdCell.TYPE;
import static com.ihg.automation.selenium.servicecenter.pages.personal.SocialIdGridRow.SocialIdCell.USER_NAME;

import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.grid.Grid;
import com.ihg.automation.selenium.gwt.components.grid.GridHeader;
import com.ihg.automation.selenium.gwt.components.grid.GridRowXpath;
import com.ihg.automation.selenium.matchers.component.HasText;

public class SocialIdGrid extends Grid<SocialIdGridRow, SocialIdGridRow.SocialIdCell>
{
    public SocialIdGrid(Container parent)
    {
        super(parent);
    }

    public SocialIdGridRow getRow(String type)
    {
        return getRow(new GridRowXpath.GridRowXpathBuilder().cell(TYPE, type).build());
    }

    public void verifyHeader()
    {
        GridHeader<SocialIdGridRow.SocialIdCell> header = getHeader();
        verifyThat(header.getCell(TYPE), HasText.hasText(TYPE.getValue()));
        verifyThat(header.getCell(USER_NAME), HasText.hasText(USER_NAME.getValue()));
        verifyThat(header.getCell(DELETE), HasText.hasText(DELETE.getValue()));
    }
}
