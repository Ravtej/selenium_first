package com.ihg.automation.selenium.servicecenter.pages.programs.rc;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasText;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasTextAsInt;

import org.joda.time.YearMonth;

import com.ihg.automation.selenium.common.pages.LabelContainerBase;
import com.ihg.automation.selenium.common.pages.Populate;
import com.ihg.automation.selenium.gwt.components.ComponentColumnPosition;
import com.ihg.automation.selenium.gwt.components.input.Select;
import com.ihg.automation.selenium.gwt.components.label.Label;

public class ExpirationDate extends LabelContainerBase implements Populate<YearMonth>
{

    public ExpirationDate(Label label)
    {
        super(label);
    }

    public Select getMonth()
    {
        return label.getSelect("Expiration Date Month", 1);
    }

    public Select getYear()
    {
        return label.getSelect("Expiration Date Year", 2, ComponentColumnPosition.SINGLE_TD_INNER_DIV);
    }

    @Override
    public void populate(YearMonth expirationDate)
    {
        getMonth().select(expirationDate.toString("MM"));
        getYear().select(expirationDate.getYear());
    }

    public void verify(YearMonth expirationDate)
    {
        verifyThat(getMonth(), hasText(expirationDate.toString("MM")));
        verifyThat(getYear(), hasTextAsInt(expirationDate.getYear()));
    }
}
