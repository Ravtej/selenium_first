package com.ihg.automation.selenium.servicecenter.pages.event.all;

import com.ihg.automation.selenium.common.event.SearchAware;
import com.ihg.automation.selenium.common.event.SearchButtonBar;
import com.ihg.automation.selenium.common.pages.NavigatePage;
import com.ihg.automation.selenium.common.pages.TabCustomContainerBase;
import com.ihg.automation.selenium.servicecenter.pages.Tabs;

public class AllEventsPage extends TabCustomContainerBase implements NavigatePage, SearchAware
{
    public AllEventsPage()
    {
        super(new Tabs().getEvents().getAll());
    }

    @Override
    public AllEventsSearch getSearchFields()
    {
        return new AllEventsSearch(container);
    }

    @Override
    public SearchButtonBar getButtonBar()
    {
        return new SearchButtonBar(container);
    }

    public AllEventsGrid getGrid()
    {
        return new AllEventsGrid(container);
    }

    public void searchEvent(String eventType, String eventTransactionType, String date, String keywords,
            boolean unitsOnly)
    {
        SearchButtonBar buttonBar = getButtonBar();

        buttonBar.clickClearCriteria();
        getSearchFields().searchEvent(eventType, eventTransactionType, date, keywords, unitsOnly);
        buttonBar.clickSearch();
    }

    public void searchByEventType(String eventType)
    {
        searchEvent(eventType, null, null, null, false);
    }
}
