package com.ihg.automation.selenium.servicecenter.pages.catalog;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasTextAsCollection;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasTextWithWait;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.isDisplayedWithWait;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static org.hamcrest.Matchers.containsString;

import com.ihg.automation.selenium.common.catalog.CatalogItem;
import com.ihg.automation.selenium.common.pages.ClosablePopUpBase;
import com.ihg.automation.selenium.gwt.components.Button;
import com.ihg.automation.selenium.gwt.components.input.Select;
import com.ihg.automation.selenium.gwt.components.label.Label;
import com.ihg.automation.selenium.listener.Verifier;
import com.ihg.automation.selenium.servicecenter.pages.order.OrderSummaryPopUp;

public class CatalogItemDetailsPopUp extends ClosablePopUpBase
{
    public CatalogItemDetailsPopUp()
    {
        super("Catalog Item Details");
    }

    public Label getVendor()
    {
        return container.getLabel("Vendor");
    }

    public Label getDeliveryMethod()
    {
        return container.getLabel("Delivery Method");
    }

    public Label getEstimatedDeliveryTime()
    {
        return container.getLabel("Estimated Delivery Time");
    }

    public Label getStartDate()
    {
        return container.getLabel("Start Date");
    }

    public Label getEndDate()
    {
        return container.getLabel("End Date");
    }

    public Label getStatus()
    {
        return container.getLabel("Status");
    }

    public Label getOrderLimit()
    {
        return container.getLabel("Order Limit");
    }

    public Label getRegions()
    {
        return container.getLabel("Regions");
    }

    public Label getCountries()
    {
        return container.getLabel("Countries");
    }

    public Label getTierLevels()
    {
        return container.getLabel("Tier-Levels");
    }

    public Select getQuantity()
    {
        return container.getSelectByLabel("Quantity");
    }

    public Select getColor()
    {
        return container.getSelectByLabel("Color");
    }

    public Select getSize()
    {
        return container.getSelectByLabel("Size");
    }

    public Button getAddToCart()
    {
        return container.getButton("Add to Cart");
    }

    public void clickAddToCart(Verifier... verifiers)
    {
        getAddToCart().click(verifiers);
    }

    public void verify(CatalogItem catalogItem)
    {
        verifyThat(container, hasText(containsString(catalogItem.getItemId())));
        verifyThat(container, hasText(containsString(catalogItem.getLoyaltyUnitCost())));
        verifyThat(container, hasText(containsString(catalogItem.getItemName())));

        verifyThat(getVendor(), hasText(catalogItem.getVendor()));
        verifyThat(getDeliveryMethod(), hasText(catalogItem.getDeliveryMethod()));
        verifyThat(getEstimatedDeliveryTime(), hasText(catalogItem.getEstimatedDeliveryTime()));
        verifyThat(getStartDate(), hasText(catalogItem.getStartDate()));
        verifyThat(getEndDate(), hasText(catalogItem.getEndDate()));
        verifyThat(getStatus(), hasTextWithWait(catalogItem.getStatus()));
        verifyThat(getOrderLimit(), hasText(catalogItem.getOrderLimit()));
        verifyThat(getRegions(), hasTextAsCollection(catalogItem.getRegions()));
        verifyThat(getCountries(), hasTextAsCollection(catalogItem.getCountries()));
        verifyThat(getTierLevels(), hasTextAsCollection(catalogItem.getTierLevels()));
    }

    public void verifyContainsText(String text)
    {
        verifyThat(container, hasText(containsString(text)));
    }

    public OrderSummaryPopUp openOrderSummaryPopUp()
    {
        clickAddToCart();
        OrderSummaryPopUp orderSummaryPopUp = new OrderSummaryPopUp();
        verifyThat(orderSummaryPopUp, isDisplayedWithWait());
        return orderSummaryPopUp;
    }
}
