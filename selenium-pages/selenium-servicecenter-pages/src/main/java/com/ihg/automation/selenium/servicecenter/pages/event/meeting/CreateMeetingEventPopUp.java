package com.ihg.automation.selenium.servicecenter.pages.event.meeting;

import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.assertNoError;
import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifySuccess;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;

import com.ihg.automation.selenium.common.MeetingDetails;
import com.ihg.automation.selenium.common.contact.MeetingPhoneFields;
import com.ihg.automation.selenium.common.pages.ClosablePopUpBase;
import com.ihg.automation.selenium.gwt.components.Button;
import com.ihg.automation.selenium.gwt.components.ComponentColumnPosition;
import com.ihg.automation.selenium.gwt.components.input.DateInput;
import com.ihg.automation.selenium.gwt.components.input.Input;
import com.ihg.automation.selenium.gwt.components.input.SearchInput;
import com.ihg.automation.selenium.gwt.components.input.Select;

public class CreateMeetingEventPopUp extends ClosablePopUpBase
{
    private static final ComponentColumnPosition POSITION = ComponentColumnPosition.TABLE;

    public CreateMeetingEventPopUp()
    {
        super("Meeting Event Details");
    }

    public SearchInput getHotel()
    {
        return new SearchInput(container.getLabel("Hotel"));
    }

    public DateInput getMeetingCheckInDate()
    {
        return new DateInput(container.getLabel("Meeting Event Check-In Date"));
    }

    public DateInput getMeetingCheckOutDate()
    {
        return new DateInput(container.getLabel("Meeting Event Check-Out Date"));
    }

    public Input getAwardUnitsAmount()
    {
        return container.getInputByLabel("Total Loyalty Units to Award", "Award Units Amount", 1, POSITION);
    }

    public Select getAwardUnitsType()
    {
        return container.getSelectByLabel("Total Loyalty Units to Award", "Award Units Type", 2, POSITION);
    }

    public Input getMeetingEventName()
    {
        return container.getInputByLabel("Meeting Event Name");
    }

    public Select getMeetingEventType()
    {
        return container.getSelectByLabel("Meeting Event Type");
    }

    public DateInput getContractBookingDate()
    {
        return new DateInput(container.getLabel("Meeting Event Contract Booking Date"));
    }

    public Input getTotalNumberOfRoomNights()
    {
        return container.getInputByLabel("Total Number of Room Nights");
    }

    public Input getNegotiatedRoomRateInUSD()
    {
        return container.getInputByLabel("Negotiated Room Rate in USD");
    }

    public Input getTotalQualifiedMeetingRevenueInUSD()
    {
        return container.getInputByLabel("Total Qualified Non-Rooms Meeting Revenue in USD");
    }

    public Select getPaymentType()
    {
        return container.getSelectByLabel("Payment Type");
    }

    public Input getSalesManagerFirstName()
    {
        return container.getInputByLabel("Sales Manager Name", "Sales Manager First Name", 1, POSITION);
    }

    public Input getSalesManagerLastName()
    {
        return container.getInputByLabel("Sales Manager Name", "Sales Manager Last Name", 2, POSITION);
    }

    public Input getSalesManagerEmail()
    {
        return container.getInputByLabel("Sales Manager Email");
    }

    public MeetingPhoneFields getSalesManagerPhone()
    {
        return new MeetingPhoneFields(container);
    }

    public Button getCreateMeetingEvent()
    {
        return container.getButton("Create Meeting Event");
    }

    public void setMeetingData(MeetingDetails meeting)
    {
        getHotel().typeAndWait(meeting.getHotel());
        getMeetingCheckInDate().typeAndWait(meeting.getCheckInDate());
        getMeetingCheckOutDate().typeAndWait(meeting.getCheckOutDate());

        if (!getAwardUnitsType().getText().equals(meeting.getTotalLoyaltyUnitsToAwardType()))
        {
            getAwardUnitsType().select(meeting.getTotalLoyaltyUnitsToAwardType());
        }
        getAwardUnitsAmount().typeAndWait(meeting.getTotalLoyaltyUnitsToAwardAmount());

        getMeetingEventName().typeAndWait(meeting.getMeetingEventName());
        getMeetingEventType().select(meeting.getMeetingEventType());
        getContractBookingDate().typeAndWait(meeting.getContractBookingDate());
        getTotalNumberOfRoomNights().typeAndWait(meeting.getTotalNumberOfRoomNights());
        getNegotiatedRoomRateInUSD().typeAndWait(meeting.getNegotiatedRoomRate());
        getTotalQualifiedMeetingRevenueInUSD().typeAndWait(meeting.getTotalRoomsRevenue());
        getPaymentType().selectByCode(meeting.getPaymentType());
        getSalesManagerFirstName().typeAndWait(meeting.getSalesManager().getGiven());
        getSalesManagerLastName().typeAndWait(meeting.getSalesManager().getSurname());
        getSalesManagerEmail().typeAndWait(meeting.getSalesManagerEmail().getEmail());
        getSalesManagerPhone().populate(meeting.getSalesManagerPhone());
    }

    public MeetingEventPage create(MeetingDetails meeting)
    {
        setMeetingData(meeting);
        getCreateMeetingEvent().clickAndWait(assertNoError(), verifySuccess("New Meeting Event was created"));
        verifyThat(this, displayed(false));

        return new MeetingEventPage();
    }

}
