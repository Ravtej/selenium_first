package com.ihg.automation.selenium.servicecenter.pages.event.freenight;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.isDisplayedWithWait;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;

import com.ihg.automation.selenium.common.freenight.FreeNight;
import com.ihg.automation.selenium.common.offer.GuestOffer;
import com.ihg.automation.selenium.common.offer.Offer;
import com.ihg.automation.selenium.gwt.components.grid.CellsName;
import com.ihg.automation.selenium.gwt.components.grid.GridRow;
import com.ihg.automation.selenium.gwt.components.grid.GridRowXpath;

public class FreeNightGridRow extends GridRow<FreeNightGridRow.FreeNightCell>
{
    public FreeNightGridRow(FreeNightGrid parent, GridRowXpath xpath)
    {
        super(parent, xpath);
    }

    public FreeNightDetailsPopUp getFreeNightDetails()
    {
        getCell(FreeNightCell.OFFER_NAME).clickByLabel();
        FreeNightDetailsPopUp freeNightPopUp = new FreeNightDetailsPopUp();
        verifyThat(freeNightPopUp, isDisplayedWithWait());
        return freeNightPopUp;
    }

    public void verify(FreeNight night)
    {
        GuestOffer guestOffer = night.getGuestOffer();
        Offer offer = guestOffer.getOffer();

        verifyThat(getCell(FreeNightCell.OFFER_CODE), hasText(offer.getCode()));
        verifyThat(getCell(FreeNightCell.OFFER_NAME), hasText(offer.getName()));

        verifyThat(getCell(FreeNightCell.REG_DATE), hasText(guestOffer.getRegistrationDate()));

        verifyThat(getCell(FreeNightCell.START_DATE), hasText(offer.getStartDate()));
        verifyThat(getCell(FreeNightCell.END_DATE), hasText(offer.getEndDate()));

        verifyThat(getCell(FreeNightCell.RATE_CATEGORY), hasText(offer.getRateCategoryCode()));
        verifyThat(getCell(FreeNightCell.FUTURE), hasText(night.getFuture()));
        verifyThat(getCell(FreeNightCell.AVAILABLE), hasText(night.getAvailable()));
        verifyThat(getCell(FreeNightCell.REDEEMED), hasText(night.getRedeemed()));
        verifyThat(getCell(FreeNightCell.EXPIRED), hasText(night.getExpired()));
    }

    public enum FreeNightCell implements CellsName
    {
        OFFER_CODE("offer.unicaOffer.offerCode1"), OFFER_NAME("offer.unicaOffer.name"), REG_DATE("effectiveDate"), START_DATE(
                "offer.effectiveDate "), END_DATE("offer.lastEffectiveDate"), RATE_CATEGORY("offer.rateCategoryCode"), FUTURE(
                "future "), AVAILABLE("available"), REDEEMED("redeemed"), EXPIRED("expired");

        private String name;

        private FreeNightCell(String name)
        {
            this.name = name;
        }

        @Override
        public String getName()
        {
            return name;
        }
    }
}
