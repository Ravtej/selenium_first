package com.ihg.automation.selenium.servicecenter.pages.programs.rc;

public enum BenefitFrequency
{
    QUARTER("Quarterly"), //
    BIYEAR("Bi-Yearly"), //
    YEAR("Yarly"), //
    MONTH("Monthly");

    private String value;

    private BenefitFrequency(String description)
    {

        this.value = description;
    }

    public String getValue()
    {
        return value;
    }
}
