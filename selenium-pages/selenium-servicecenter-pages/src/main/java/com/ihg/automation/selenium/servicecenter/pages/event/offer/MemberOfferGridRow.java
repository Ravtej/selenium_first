package com.ihg.automation.selenium.servicecenter.pages.event.offer;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.isDisplayedWithWait;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ihg.automation.selenium.common.offer.GuestOffer;
import com.ihg.automation.selenium.common.offer.Offer;
import com.ihg.automation.selenium.gwt.components.grid.CellsName;
import com.ihg.automation.selenium.gwt.components.grid.GridRow;
import com.ihg.automation.selenium.gwt.components.grid.GridRowXpath;
import com.ihg.automation.selenium.gwt.components.utils.WaitUtils;

public class MemberOfferGridRow extends GridRow<MemberOfferGridRow.MemberOfferCell>
{
    private static final Logger logger = LoggerFactory.getLogger(MemberOfferGridRow.class);

    public MemberOfferGridRow(MemberOffersGrid parent, GridRowXpath xpath)
    {
        super(parent, xpath);
    }

    public OfferPopUp getOfferDetails()
    {
        getCell(MemberOfferGridRow.MemberOfferCell.OFFER_NAME).clickAndWait();
        OfferPopUp offerPopUp = new OfferPopUp();
        verifyThat(offerPopUp, isDisplayedWithWait());
        // Waiting for Offer details elements to be loaded
        try
        {
            Thread.sleep(1000);
        }
        catch (InterruptedException e)
        {
            logger.error("Sleep exception", e);
        }
        new WaitUtils().waitExecutingRequest();
        return offerPopUp;
    }

    public void verify(GuestOffer guestOffer)
    {
        Offer offer = guestOffer.getOffer();

        verifyThat(getCell(MemberOfferCell.OFFER_CODE), hasText(offer.getCode()));
        verifyThat(getCell(MemberOfferCell.OFFER_NAME), hasText(offer.getName()));
        verifyThat(getCell(MemberOfferCell.REG_DATE), hasText(guestOffer.getRegistrationDate()));
        verifyThat(getCell(MemberOfferCell.START_DATE), hasText(offer.getStartDate()));
        verifyThat(getCell(MemberOfferCell.END_DATE), hasText(offer.getEndDate()));
        verifyThat(getCell(MemberOfferCell.PROGRAM), hasText(offer.getProgram()));
        verifyThat(getCell(MemberOfferCell.ACHIEVED_TO_DATE), hasText(guestOffer.getAchievedToDate()));
        verifyThat(getCell(MemberOfferCell.WINS), hasText(guestOffer.getReplaysCount()));
        verifyThat(getCell(MemberOfferCell.MAX_WINS), hasText(offer.getMaximumWinsAllowed()));
        verifyThat(getCell(MemberOfferCell.STATUS), hasText(guestOffer.getStatus()));
    }

    public enum MemberOfferCell implements CellsName
    {
        OFFER_CODE("offer.unicaOffer.offerCode1"), OFFER_NAME("offer.unicaOffer.name"), REG_DATE(
                "effectiveDate"), START_DATE("offer.effectiveDate"), END_DATE("offer.lastEffectiveDate"), PROGRAM(
                        "offer.offerExtension"), ACHIEVED_TO_DATE("achievedToDate"), WINS(
                                "replaysCount"), MAX_WINS("offer.maxAllowedReplays"), STATUS("guestOfferStatus");

        private String name;

        private MemberOfferCell(String name)
        {
            this.name = name;
        }

        @Override
        public String getName()
        {
            return name;
        }
    }

}
