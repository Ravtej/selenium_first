package com.ihg.automation.selenium.servicecenter.pages.event.rewardnight;

import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.assertNoError;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.isDisplayedWithWait;

import com.ihg.automation.selenium.common.event.SearchAware;
import com.ihg.automation.selenium.common.pages.TabCustomContainerBase;
import com.ihg.automation.selenium.common.reward.RewardNight;
import com.ihg.automation.selenium.gwt.components.Button;
import com.ihg.automation.selenium.gwt.components.panel.Panel;
import com.ihg.automation.selenium.gwt.components.utils.ByText;
import com.ihg.automation.selenium.servicecenter.pages.Tabs;

public class RewardNightEventPage extends TabCustomContainerBase implements SearchAware
{
    private static Tabs tabs = new Tabs();

    public RewardNightEventPage()
    {
        super(tabs.getEvents().getRewardNight());
    }

    @Override
    public RewardNightSearch getSearchFields()
    {
        return new RewardNightSearch(container);
    }

    @Override
    public RewardNightSearchButtonBar getButtonBar()
    {
        return new RewardNightSearchButtonBar(this);
    }

    public Button getCreateNewRewardNight()
    {
        return container.getButton("Create New Reward Night");
    }

    public void clickCreateNewRewardNight()
    {
        getCreateNewRewardNight().clickAndWait(assertNoError());
    }

    private Panel getGridPanel()
    {
        return container.getPanel(ByText.contains("Reward nights"));
    }

    public RewardNightEventGrid getGrid()
    {
        return new RewardNightEventGrid(getGridPanel());
    }

    public CreateRewardNightPopUp openRewardNightsDialog()
    {
        goTo();
        clickCreateNewRewardNight();
        CreateRewardNightPopUp createRewardNightPopUp = new CreateRewardNightPopUp();
        verifyThat(createRewardNightPopUp, isDisplayedWithWait());

        return createRewardNightPopUp;
    }

    public void createRewardNight(RewardNight night)
    {
        openRewardNightsDialog().create(night);
    }
}
