package com.ihg.automation.selenium.servicecenter.pages.event.voucher;

import com.ihg.automation.selenium.common.event.EventGridRowContainerWithDetails;
import com.ihg.automation.selenium.gwt.components.Container;

public class VoucherGridRowContainer extends EventGridRowContainerWithDetails
{
    public VoucherGridRowContainer(Container parent)
    {
        super(parent);
    }

    public VoucherDetails getVoucherDetails()
    {
        return new VoucherDetails(container);
    }
}
