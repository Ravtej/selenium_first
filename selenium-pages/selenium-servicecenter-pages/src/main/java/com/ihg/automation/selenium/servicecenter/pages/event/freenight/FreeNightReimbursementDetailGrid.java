package com.ihg.automation.selenium.servicecenter.pages.event.freenight;

import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.grid.Grid;

public class FreeNightReimbursementDetailGrid extends
        Grid<FreeNightReimbursementDetailGridRow, FreeNightReimbursementDetailGridRow.FreeNightReimbursementDetailCell>
{

    public FreeNightReimbursementDetailGrid(Container parent)
    {
        super(parent);
    }

}
