package com.ihg.automation.selenium.servicecenter.pages.programs.rc;

import com.ihg.automation.selenium.common.catalog.CatalogItem;
import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.grid.Grid;
import com.ihg.automation.selenium.gwt.components.grid.GridRowXpath;

public class BenefitsGrid extends Grid<BenefitsGridRow, BenefitsGridRow.BenefitsCell>
{
    public BenefitsGrid(Container parent)
    {
        super(parent);
    }

    public BenefitsGridRow getRow(CatalogItem catalogItem)
    {
        return getRow(new GridRowXpath.GridRowXpathBuilder()
                .cell(BenefitsGridRow.BenefitsCell.BENEFIT_AWARD, catalogItem.getItemId()).build());
    }
}
