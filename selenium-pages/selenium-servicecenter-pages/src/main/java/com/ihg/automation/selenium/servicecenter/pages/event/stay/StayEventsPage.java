package com.ihg.automation.selenium.servicecenter.pages.event.stay;

import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyNoError;
import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifySuccess;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasText;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.isDisplayedWithWait;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.hamcrest.Matchers;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ihg.automation.selenium.assertions.MessageBoxVerifier;
import com.ihg.automation.selenium.common.event.SearchAware;
import com.ihg.automation.selenium.common.event.SearchButtonBar;
import com.ihg.automation.selenium.common.pages.QualifyingWarningDialog;
import com.ihg.automation.selenium.common.pages.TabCustomContainerBase;
import com.ihg.automation.selenium.common.stay.Stay;
import com.ihg.automation.selenium.gwt.components.Button;
import com.ihg.automation.selenium.gwt.components.panel.Panel;
import com.ihg.automation.selenium.gwt.components.utils.ByText;
import com.ihg.automation.selenium.listener.Verifier;
import com.ihg.automation.selenium.runtime.Selenium;
import com.ihg.automation.selenium.servicecenter.pages.Tabs;

public class StayEventsPage extends TabCustomContainerBase implements SearchAware
{
    private static final Logger logger = LoggerFactory.getLogger(StayEventsPage.class);

    public static final String STAY_SUCCESS_CREATED = "Stay has been successfully created.";
    public static final String STAY_SUCCESS_ADJUSTED = "Stay has been successfully adjusted.";
    public static final String STAY_SUCCESS_CREATED_WITH_REASON = STAY_SUCCESS_CREATED
            + "\nStay Qualifying indicator was set to NO by System Rules. %s: %s.";
    public static final String FORCE_QUALIFY_REASON = STAY_SUCCESS_CREATED
            + "\nStay Qualifying indicator was set to YES by System Rules. Reason: %s";

    public StayEventsPage()
    {
        super(new Tabs().getEvents().getStay());
    }

    @Override
    public StaySearch getSearchFields()
    {
        return new StaySearch(container);
    }

    @Override
    public StayEventsPageButtonBar getButtonBar()
    {
        return new StayEventsPageButtonBar(this);
    }

    public Panel getGuestStaysPanel()
    {
        return container.getPanel(ByText.contains("Stays"));
    }

    public StayGuestGrid getGuestGrid()
    {
        return new StayGuestGrid(getGuestStaysPanel());
    }

    public Panel getUniverseStaysPanel()
    {
        return container.getPanel("Universe");
    }

    public void createStay(Stay stay, String successMessage)
    {
        goTo();
        getButtonBar().clickCreateStay(verifyNoError());

        CreateStayPopUp popUp = new CreateStayPopUp();
        popUp.getStayDetails().populate(stay);

        MessageBoxVerifier successMessageBox = null;

        if (StringUtils.isNotEmpty(successMessage))
        {
            successMessageBox = verifySuccess(successMessage);
        }

        if (stay.getIsQualifying())
        {
            popUp.clickCreateStay(verifyNoError(), successMessageBox);
        }

        else
        {
            popUp.clickCreateStay(verifyNoError());

            QualifyingWarningDialog warningDialog = new QualifyingWarningDialog();
            verifyThat(popUp, isDisplayedWithWait());
            warningDialog.clickSubmit(verifyNoError(), successMessageBox);
        }

        waitUntilFirstStayProcess();
    }

    public void createStayAndVerifySuccessMessage(Stay stay, String... disqualifyReasons)
    {
        String message;
        if (ArrayUtils.isEmpty(disqualifyReasons))
        {
            message = STAY_SUCCESS_CREATED;
        }
        else
        {
            if (disqualifyReasons.length == 1)
            {
                message = String.format(STAY_SUCCESS_CREATED_WITH_REASON, "Reason", disqualifyReasons[0]);
            }
            else
            {
                message = String.format(STAY_SUCCESS_CREATED_WITH_REASON, "Reasons",
                        StringUtils.join(disqualifyReasons, ", "));
            }

        }
        createStay(stay, message);
    }

    public void createStay(Stay stay)
    {
        createStay(stay, null);
    }

    public void waitUntilFirstStayProcess()
    {
        WebDriverWait wait = (new WebDriverWait(Selenium.getInstance().getDriver(), 30, 1000L));
        wait.until(stayProcessing());
    }

    private static ExpectedCondition<Boolean> stayProcessing()
    {
        return new ExpectedCondition<Boolean>()
        {
            @Override
            public Boolean apply(WebDriver driver)
            {
                StayEventsPage page = new StayEventsPage();

                page.getButtonBar().getShowStays().clickAndWait();

                StayGuestGridRow row = page.getGuestGrid().getRow(1);
                StayGuestGridRowContainer rowContainer = row.expand(StayGuestGridRowContainer.class);

                boolean isStillProcessing = hasText(Matchers.containsString("Stay is being processed"))
                        .matches(rowContainer);
                if (isStillProcessing)
                {
                    logger.debug("Wait stay is being processed ...");
                }
                else
                {
                    logger.debug("Stay has been processed");
                    // Re-read grid row details if stay has been processed
                    // between click on 'Show Customer's Stays' and row
                    // expanding
                    page.getButtonBar().getShowStays().clickAndWait();
                }
                return !isStillProcessing;
            }

            @Override
            public String toString()
            {
                return "stay is being processed";
            }
        };
    }

    public class StayEventsPageButtonBar extends SearchButtonBar
    {
        public StayEventsPageButtonBar(StayEventsPage page)
        {
            super(page.container);
        }

        public Button getShowStays()
        {
            return container.getButton("Show Customer's Stays");
        }

        public Button getCreateStay()
        {
            return container.getButton("Create Stay");
        }

        public void clickShowStays()
        {
            getShowStays().clickAndWait();
        }

        public void clickCreateStay(Verifier... verifiers)
        {
            getCreateStay().clickAndWait(verifiers);
        }
    }
}
