package com.ihg.automation.selenium.servicecenter.pages.programs.rc;

import com.ihg.automation.common.DateUtils;
import com.ihg.automation.selenium.common.pages.ClosablePopUpBase;
import com.ihg.automation.selenium.gwt.components.panel.Panel;
import com.ihg.automation.selenium.gwt.components.utils.ByText;

public class PartnerBenefitDetailsPopUp extends ClosablePopUpBase
{
    public PartnerBenefitDetailsPopUp()
    {
        super("Partner Benefit Details");
    }

    public Panel getPartnerBenefitsPanel(int year)
    {
        return container.getPanel(ByText.exact("Partner Benefits - " + year));
    }

    public PartnerBenefitsGrid getPartnerBenefitsGrid(int year)
    {
        return new PartnerBenefitsGrid(getPartnerBenefitsPanel(year));
    }

    public PartnerBenefitsGrid getCurrentYearPartnerBenefitsGrid()
    {
        return getPartnerBenefitsGrid(DateUtils.currentYear());
    }
}
