package com.ihg.automation.selenium.servicecenter.pages.event.meeting;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;

import com.ihg.automation.selenium.common.components.CountrySelect;
import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.CustomContainerBase;
import com.ihg.automation.selenium.gwt.components.input.Input;
import com.ihg.automation.selenium.gwt.components.input.SearchInput;
import com.ihg.automation.selenium.gwt.components.input.Select;
import com.ihg.automation.selenium.servicecenter.pages.event.EventDateRange;

public class MeetingSearch extends CustomContainerBase
{
    public MeetingSearch(Container container)
    {
        super(container.getSeparator("Meeting Search"));
    }

    public Input getHotelName()
    {
        return container.getInputByLabel("Hotel Name");
    }

    public Input getMeetingEventName()
    {
        return container.getInputByLabel("Meeting Event Name");
    }

    public SearchInput getHotel()
    {
        return new SearchInput(container.getLabel("Hotel"));
    }

    public CountrySelect getCountry()
    {
        return new CountrySelect(container);
    }

    public Select getState()
    {
        return container.getSelectByLabel("State/Province");
    }

    public EventDateRange getMeetingDate()
    {
        return new EventDateRange(container, "Date of Meeting");
    }

    public void verifyDefaults()
    {
        verifyThat(getHotelName(), hasText(""));
        verifyThat(getMeetingEventName(), hasText(""));
        verifyThat(getHotel(), hasText("Hotel"));
        verifyThat(getCountry(), hasText("Select"));
        verifyThat(getState(), hasText("Select"));
        verifyThat(getMeetingDate().getDate(), hasText("ddMMMyy"));
        verifyThat(getMeetingDate().getRange(), hasText("+/- 0 Month"));
    }
}
