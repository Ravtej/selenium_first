package com.ihg.automation.selenium.servicecenter.pages.event.stay;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;

import com.ihg.automation.selenium.common.pages.Populate;
import com.ihg.automation.selenium.common.stay.Stay;
import com.ihg.automation.selenium.gwt.components.CustomContainerBase;
import com.ihg.automation.selenium.gwt.components.input.Input;
import com.ihg.automation.selenium.gwt.components.input.Select;

public class StayBookingInfoEdit extends StayBookingInfoBase implements Populate<Stay>
{
    public StayBookingInfoEdit(CustomContainerBase parent)
    {
        super(parent);
    }

    @Override
    public Input getBookingSource()
    {
        return container.getInputByLabel(BOOKING_SOURCE_LABEL);
    }

    public Select getStayPayment()
    {
        return container.getSelectByLabel("Stay Payment");
    }

    @Override
    public void populate(Stay stay)
    {
        getBookingSource().type(stay.getBookingDetails().getBookingSource());
        getStayPayment().selectByCode(stay.getPayment());
    }

    public void verify(Stay stay)
    {
        super.verify(stay);
        verifyThat(getStayPayment(), hasText(stay.getBookingDetails().getBookingSource()));
    }
}
