package com.ihg.automation.selenium.servicecenter.pages.search;

import com.ihg.automation.selenium.gwt.components.grid.GridRow;
import com.ihg.automation.selenium.gwt.components.grid.GridRowXpath;

public class StaySearchGridRow extends GridRow<StaySearchGridCell>
{
    public StaySearchGridRow(StaySearchGrid parent, GridRowXpath xpath)
    {
        super(parent, xpath);
    }

}
