package com.ihg.automation.selenium.servicecenter.pages.event.benefit;

import com.ihg.automation.selenium.common.billing.EventBillingDetailsTab;
import com.ihg.automation.selenium.common.billing.EventBillingTabAware;
import com.ihg.automation.selenium.common.earning.EventEarningDetailsTab;
import com.ihg.automation.selenium.common.earning.EventEarningTabAware;
import com.ihg.automation.selenium.common.pages.ClosablePopUpBase;

public class BenefitUpgradeDetailsPopUp extends ClosablePopUpBase implements EventEarningTabAware, EventBillingTabAware
{
    public BenefitUpgradeDetailsPopUp()
    {
        super("Benefit Upgrade Details");
    }

    public BenefitUpgradeDetailsTab getBenefitUpgradeDetailsTab()
    {
        return new BenefitUpgradeDetailsTab(this);
    }

    @Override
    public EventEarningDetailsTab getEarningDetailsTab()
    {
        return new EventEarningDetailsTab(this);
    }

    @Override
    public EventBillingDetailsTab getBillingDetailsTab()
    {
        return new EventBillingDetailsTab(this);
    }

}
