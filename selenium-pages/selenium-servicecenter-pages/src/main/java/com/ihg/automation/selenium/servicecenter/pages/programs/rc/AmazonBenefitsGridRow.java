package com.ihg.automation.selenium.servicecenter.pages.programs.rc;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static com.ihg.automation.selenium.matchers.text.EntityMatcherFactory.isValue;

import com.ihg.automation.selenium.gwt.components.grid.CellsName;
import com.ihg.automation.selenium.gwt.components.grid.GridRow;
import com.ihg.automation.selenium.gwt.components.grid.GridRowXpath;

public class AmazonBenefitsGridRow extends GridRow<AmazonBenefitsGridRow.AmazonBenefitsCell>
{
    public AmazonBenefitsGridRow(AmazonBenefitsGrid parent, GridRowXpath xpath)
    {
        super(parent, xpath);
    }

    public enum AmazonBenefitsCell implements CellsName
    {
        NAME("partner", "Name"), //
        TIER_LEVEL("tierLevel", "Tier-Level"), //
        BENEFIT_COUNT("benefitsPerPeriod", "Benefit Count"), //
        BENEFIT_FREQUENCY("benefitPeriod", "Benefit Frequency");

        private String name;
        private String value;

        private AmazonBenefitsCell(String name, String value)
        {
            this.name = name;
            this.value = value;
        }

        public String getValue()
        {
            return value;
        }

        @Override
        public String getName()
        {
            return name;
        }
    }

    public void verify(BenefitRow benefit)
    {
        verifyThat(getCell(AmazonBenefitsCell.NAME), hasText(benefit.getName()));
        verifyThat(getCell(AmazonBenefitsCell.TIER_LEVEL), hasText(isValue(benefit.getTierLevel())));
        verifyThat(getCell(AmazonBenefitsCell.BENEFIT_COUNT), hasText(benefit.getBenefitCount()));
        verifyThat(getCell(AmazonBenefitsCell.BENEFIT_FREQUENCY), hasText(benefit.getBenefitFrequency().getValue()));
    }
}
