package com.ihg.automation.selenium.servicecenter.pages.account;

import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyNoError;

import com.ihg.automation.selenium.common.pages.MultiModeBase;
import com.ihg.automation.selenium.gwt.components.Button;
import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.Link;
import com.ihg.automation.selenium.gwt.components.input.Input;
import com.ihg.automation.selenium.gwt.components.input.Select;
import com.ihg.automation.selenium.gwt.components.label.Label;

public class Security extends MultiModeBase
{
    public static final String PIN_LABEL = "PIN";
    public static final String LOCKOUT_LABEL = "Self Service Lockout Indicator";
    public static final String USERNAME_LABEL = "Username:";

    public Security(Container parent)
    {
        super(parent.getSeparator("Security"));
    }

    public Input getPin()
    {
        return container.getInputByLabel(PIN_LABEL);
    }

    public Select getLockout()
    {
        return container.getSelectByLabel(LOCKOUT_LABEL);
    }

    public Label getUserName()
    {
        return container.getLabel(USERNAME_LABEL);
    }

    public Button getRemovePin()
    {
        return container.getButton("Remove PIN");
    }

    public Button getSendPin()
    {
        return container.getButton("Send PIN");
    }

    public Link getClear()
    {
        return container.getLink("Clear");
    }

    public void clickClear()
    {
        getClear().click();
    }

    public void addPIN(String pin)
    {
        clickEdit();
        getPin().type(pin);
        clickSave(verifyNoError());
    }
}
