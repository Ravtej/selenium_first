package com.ihg.automation.selenium.servicecenter.pages.account;

import com.ihg.automation.selenium.common.pages.SavePopUpBase;
import com.ihg.automation.selenium.gwt.components.PopUpWindow;
import com.ihg.automation.selenium.gwt.components.input.Input;

public class EmailPopUp extends SavePopUpBase
{
    public EmailPopUp()
    {
        super(PopUpWindow.withoutHeader("Send Pin"));
    }

    public Input getEmail()
    {
        return container.getInputByLabel("Email");
    }
}
