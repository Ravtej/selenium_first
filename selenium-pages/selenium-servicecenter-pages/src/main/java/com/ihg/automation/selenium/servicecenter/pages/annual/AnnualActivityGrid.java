package com.ihg.automation.selenium.servicecenter.pages.annual;

import com.ihg.automation.common.DateUtils;
import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.grid.Grid;
import com.ihg.automation.selenium.gwt.components.grid.GridRowXpath;
import com.ihg.automation.selenium.servicecenter.pages.annual.AnnualActivityRow.AnnualActivityCell;

public class AnnualActivityGrid extends Grid<AnnualActivityRow, AnnualActivityCell>
{

    public AnnualActivityGrid(Container container)
    {
        super(container.getPanel("Annual Activity"));
    }

    public AnnualActivityRow getRowByYear(int year)
    {
        String startYearDate = DateUtils.now().withYear(year).withDayOfYear(1).toString(DateUtils.DATE_FORMAT);

        return getRow(new GridRowXpath.GridRowXpathBuilder().cell(AnnualActivityCell.YEAR, startYearDate).build());
    }

    public AnnualActivityRow getCurrentYearRow()
    {
        return getRowByYear(DateUtils.currentYear());
    }

    public AnnualActivityRow getPreviousYearRow()
    {
        return getRowByYear(DateUtils.previousYear());
    }

}
