package com.ihg.automation.selenium.servicecenter.pages.occupation;

import com.ihg.automation.selenium.gwt.components.grid.CellsName;
import com.ihg.automation.selenium.gwt.components.grid.GridRow;
import com.ihg.automation.selenium.gwt.components.grid.GridRowXpath;

public class AdvancedCorporateAccountGridRow
        extends GridRow<AdvancedCorporateAccountGridRow.AdvancedCorporateAccountGridCell>
{
    public AdvancedCorporateAccountGridRow(AdvancedCorporateAccountGrid parent, GridRowXpath xpath)
    {
        super(parent, xpath);
    }

    public enum AdvancedCorporateAccountGridCell implements CellsName
    {
        NUMBER("number"), NAME("name");

        private String name;

        private AdvancedCorporateAccountGridCell(String name)
        {
            this.name = name;
        }

        @Override
        public String getName()
        {
            return name;
        }
    }
}
