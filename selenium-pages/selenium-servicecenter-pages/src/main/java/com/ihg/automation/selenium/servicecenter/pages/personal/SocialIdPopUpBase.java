package com.ihg.automation.selenium.servicecenter.pages.personal;

import com.ihg.automation.selenium.common.pages.SavePopUpBase;
import com.ihg.automation.selenium.gwt.components.input.Input;
import com.ihg.automation.selenium.gwt.components.input.Select;

public abstract class SocialIdPopUpBase extends SavePopUpBase
{
    public SocialIdPopUpBase(String header)
    {
        super(header);
    }

    public Select getType()
    {
        return container.getSelectByLabel("Type");
    }

    public Input getUsername()
    {
        return container.getInputByLabel("Username");
    }
}
