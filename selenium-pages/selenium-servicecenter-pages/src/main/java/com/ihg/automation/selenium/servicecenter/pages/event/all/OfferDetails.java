package com.ihg.automation.selenium.servicecenter.pages.event.all;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;

import com.ihg.automation.selenium.common.EventSourceAware;
import com.ihg.automation.selenium.common.SiteHelperBase;
import com.ihg.automation.selenium.common.event.EventSourceExtended;
import com.ihg.automation.selenium.common.offer.Offer;
import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.CustomContainerBase;
import com.ihg.automation.selenium.gwt.components.Separator;
import com.ihg.automation.selenium.gwt.components.label.Label;
import com.ihg.automation.selenium.gwt.components.utils.ByText;

public class OfferDetails extends CustomContainerBase implements EventSourceAware
{
    public OfferDetails(Container container)
    {
        super(container);
    }

    public Label getOfferCode()
    {
        return new Label(container, ByText.exactSelf("Loyalty Offer Code"), 1);
    }

    public Label getOfferDescription()
    {
        return container.getLabel("Offer Description");
    }

    @Override
    public EventSourceExtended getSource()
    {
        return new EventSourceExtended(new Separator(container));
    }

    public void verify(Offer offer, SiteHelperBase siteHelper)
    {
        verifyThat(getOfferCode(), hasText(offer.getCode()));
        verifyThat(getOfferDescription(), hasText(offer.getDescription()));

        getSource().verify(siteHelper.getSourceFactory().getRulesSource());

    }
}
