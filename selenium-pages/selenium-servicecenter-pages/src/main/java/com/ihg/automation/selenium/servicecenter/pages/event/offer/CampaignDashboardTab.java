package com.ihg.automation.selenium.servicecenter.pages.event.offer;

import com.ihg.automation.selenium.common.pages.PopUpBase;
import com.ihg.automation.selenium.common.pages.TabCustomContainerBase;
import com.ihg.automation.selenium.gwt.components.panel.Panel;

public class CampaignDashboardTab extends TabCustomContainerBase
{
    public CampaignDashboardTab(PopUpBase parent)
    {
        super(parent, "Campaign Dashboard");
    }

    public CampaignDashboardDetails getExternalTrackingDetails()
    {
        return new CampaignDashboardDetails(container);
    }

    public Panel getSubOffersPanel()
    {
        return container.getPanel("Sub Offers");
    }

    public SubOffersGrid getMemberOffersGrid()
    {
        return new SubOffersGrid(getSubOffersPanel());
    }
}
