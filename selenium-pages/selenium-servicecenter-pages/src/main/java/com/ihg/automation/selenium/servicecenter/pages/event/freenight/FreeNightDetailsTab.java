package com.ihg.automation.selenium.servicecenter.pages.event.freenight;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;

import com.ihg.automation.selenium.common.freenight.FreeNight;
import com.ihg.automation.selenium.common.offer.GuestOffer;
import com.ihg.automation.selenium.common.offer.Offer;
import com.ihg.automation.selenium.common.pages.PopUpBase;
import com.ihg.automation.selenium.common.pages.TabCustomContainerBase;
import com.ihg.automation.selenium.gwt.components.label.Label;
import com.ihg.automation.selenium.gwt.components.panel.Panel;

public class FreeNightDetailsTab extends TabCustomContainerBase
{
    public FreeNightDetailsTab(PopUpBase parent)
    {
        super(parent, "Free Night Details");
    }

    public Label getOfferCode()
    {
        return container.getLabel("Loyalty Offer Code");
    }

    public Label getNameOfItem()
    {
        return container.getLabel("Offer Name");
    }

    public Label getRateCategory()
    {
        return container.getLabel("Rate Category");
    }

    public Label getRegistrationDate()
    {
        return container.getLabel("Registration Date");
    }

    public Label getOfferStartDate()
    {
        return container.getLabel("Offer Start Date");
    }

    public Label getOfferEndDate()
    {
        return container.getLabel("Offer End Date");
    }

    public Panel getFreeNightVoucherPanel()
    {
        return container.getPanel("Free Night Vouchers");
    }

    public FreeNightVoucherGrid getFreeNightVoucherGrid()
    {
        return new FreeNightVoucherGrid(getFreeNightVoucherPanel());
    }

    public void verify(FreeNight night)
    {
        GuestOffer guestOffer = night.getGuestOffer();
        Offer offer = guestOffer.getOffer();

        verifyThat(getOfferCode(), hasText(offer.getCode()));
        verifyThat(getNameOfItem(), hasText(offer.getName()));

        verifyThat(getRateCategory(), hasText(offer.getRateCategoryCode()));
        verifyThat(getRegistrationDate(), hasText(guestOffer.getRegistrationDate()));

        verifyThat(getOfferStartDate(), hasText(offer.getStartDate()));
        verifyThat(getOfferEndDate(), hasText(offer.getEndDate()));
    }
}
