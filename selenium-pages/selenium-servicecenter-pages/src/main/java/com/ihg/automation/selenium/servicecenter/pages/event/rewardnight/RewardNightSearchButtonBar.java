package com.ihg.automation.selenium.servicecenter.pages.event.rewardnight;

import com.ihg.automation.selenium.common.event.SearchButtonBar;
import com.ihg.automation.selenium.gwt.components.Button;

public class RewardNightSearchButtonBar extends SearchButtonBar
{
    public RewardNightSearchButtonBar(RewardNightEventPage page)
    {
        super(page.container);
    }

    public Button getCreateNewRewardNight()
    {
        return container.getButton("Create New Reward Night");
    }

    public void clickCreateNewRewardNight()
    {
        getCreateNewRewardNight().clickAndWait();
    }

}
