package com.ihg.automation.selenium.servicecenter.pages.programs.rc;

import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyNoError;

import com.ihg.automation.selenium.common.member.MemberAlliance;
import com.ihg.automation.selenium.common.pages.SavePopUpBase;

public class AllianceAddPopUp extends SavePopUpBase
{
    public AllianceAddPopUp()
    {
        super("Add Alliance");
    }

    public AllianceDetailsAdd getAllianceDetails()
    {
        return new AllianceDetailsAdd(container);
    }

    public void addAlliance(MemberAlliance alliance)
    {
        getAllianceDetails().populate(alliance);
        clickSave(verifyNoError());
    }
}
