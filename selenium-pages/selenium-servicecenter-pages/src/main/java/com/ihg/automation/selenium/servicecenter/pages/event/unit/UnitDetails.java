package com.ihg.automation.selenium.servicecenter.pages.event.unit;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;

import com.ihg.automation.selenium.common.EventSourceAware;
import com.ihg.automation.selenium.common.SiteHelperBase;
import com.ihg.automation.selenium.common.event.EventSourceExtended;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.payment.PaymentDetails;
import com.ihg.automation.selenium.common.payment.PaymentDetailsAware;
import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.CustomContainerBase;
import com.ihg.automation.selenium.gwt.components.Link;

public class UnitDetails extends CustomContainerBase implements PaymentDetailsAware, EventSourceAware
{
    public UnitDetails(Container container)
    {
        super(container);
    }

    public Link getRelatedMemberId()
    {
        return new Link(container.getLabel("Related Member ID"), Link.LINK_XPATH_NO_HYPER_LINK_IN_CLASS);
    }

    @Override
    public PaymentDetails getPaymentDetails()
    {
        return new PaymentDetails(container);
    }

    @Override
    public EventSourceExtended getSource()
    {
        return new EventSourceExtended(container);
    }

    public void verifyRelatedMemberID(Member member)
    {
        verifyThat(getRelatedMemberId(), hasText(member.getRCProgramId()));
    }

    public void verifySource(SiteHelperBase siteHelper)
    {
        getSource().verify(siteHelper.getSourceFactory().getNoEmployeeIdSource());
    }
}
