package com.ihg.automation.selenium.servicecenter.pages.event.all;

import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.CustomContainerBase;

public class OfferGridRowContainer extends CustomContainerBase
{
    public OfferGridRowContainer(Container parent)
    {
        super(parent);
    }

    public OfferDetails getOfferDetails()
    {
        return new OfferDetails(container);
    }
}
