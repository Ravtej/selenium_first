package com.ihg.automation.selenium.servicecenter.pages.programs.rc;

import static com.ihg.automation.selenium.common.types.program.AlternateLoyaltyIDType.HERTZ;

import com.ihg.automation.selenium.common.pages.SavePopUpBase;
import com.ihg.automation.selenium.gwt.components.Button;
import com.ihg.automation.selenium.gwt.components.input.Input;
import com.ihg.automation.selenium.gwt.components.input.Select;
import com.ihg.automation.selenium.listener.Verifier;

public class AlternateLoyaltyIDPopUpBase extends SavePopUpBase
{
    public AlternateLoyaltyIDPopUpBase(String header)
    {
        super(header);
    }

    public Select getType()
    {
        return container.getSelectByLabel("Type");
    }

    public Input getNumber()
    {
        return container.getInputByLabel("Number");
    }

    public void populate(String type, String number)
    {
        getType().select(type);
        getNumber().type(number);
    }

    public void populateHertzID(String number)
    {
        populate(HERTZ, number);
    }

}
