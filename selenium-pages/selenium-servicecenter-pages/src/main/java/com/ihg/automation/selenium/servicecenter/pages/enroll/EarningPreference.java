package com.ihg.automation.selenium.servicecenter.pages.enroll;

import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.CustomContainerBase;
import com.ihg.automation.selenium.gwt.components.input.Select;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.EarningPreferenceAware;

public class EarningPreference extends CustomContainerBase implements EarningPreferenceAware
{
    public EarningPreference(Container container)
    {
        super(container.getSeparator("Earning Preference"));
    }

    @Override
    public Select getEarningPreference()
    {
        return container.getSelectByLabel(PREFERENCE_LABEL);
    }

    public AllianceDetails getAllianceDetails()
    {
        return new AllianceDetails(container);
    }
}
