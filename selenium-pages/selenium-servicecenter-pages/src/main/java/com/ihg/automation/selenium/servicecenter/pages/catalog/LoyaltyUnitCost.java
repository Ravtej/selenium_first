package com.ihg.automation.selenium.servicecenter.pages.catalog;

import com.ihg.automation.selenium.common.pages.LabelContainerBase;
import com.ihg.automation.selenium.gwt.components.ComponentColumnPosition;
import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.input.Input;

public class LoyaltyUnitCost extends LabelContainerBase
{
    private ComponentColumnPosition POSITION = ComponentColumnPosition.TABLE;

    public LoyaltyUnitCost(Container container)
    {
        super(container.getLabel("Loyalty Unit Cost"));
    }

    public Input getLoyaltyUnitCostMin()
    {
        return label.getInput("Loyalty Unit Cost From", 1, POSITION);
    }

    public Input getLoyaltyUnitCostMax()
    {
        return label.getInput("Loyalty Unit Cost To", 3, POSITION);
    }

    public void populate(String loyaltyUnitCostMin, String loyaltyUnitCostMax)
    {
        getLoyaltyUnitCostMin().type(loyaltyUnitCostMin);
        getLoyaltyUnitCostMax().type(loyaltyUnitCostMax);
    }
}
