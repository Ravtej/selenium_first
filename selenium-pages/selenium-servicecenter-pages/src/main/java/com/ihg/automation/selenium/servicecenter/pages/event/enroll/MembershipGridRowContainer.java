package com.ihg.automation.selenium.servicecenter.pages.event.enroll;

import com.ihg.automation.selenium.common.event.EventGridRowContainerWithDetails;
import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.label.Label;

public class MembershipGridRowContainer extends EventGridRowContainerWithDetails
{
    public MembershipGridRowContainer(Container parent)
    {
        super(parent);
    }

    public MembershipDetails getMembershipDetails()
    {
        return new MembershipDetails(container);
    }

    /**
     * Only for Membership-Open, Membership-Close and Membership-Extension
     */
    public Label getReason()
    {
        return container.getLabel("Reason");
    }
}
