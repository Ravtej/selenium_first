package com.ihg.automation.selenium.servicecenter.pages.event.offer;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;

import com.ihg.automation.selenium.common.offer.Offer;
import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.CustomContainerBase;
import com.ihg.automation.selenium.gwt.components.label.Label;

public class OfferDetailsContainer extends CustomContainerBase
{
    public OfferDetailsContainer(Container container)
    {
        super(container.getSeparator("Offer Details"));
    }

    public Label getName()
    {
        return container.getLabel("Name");
    }

    public Label getOfferDescription()
    {
        return container.getLabel("Offer Description");
    }

    public Label getOfferCode()
    {
        return container.getLabel("Loyalty Offer Code");
    }

    public Label getMaxWins()
    {
        return container.getLabel("Maximum Wins Allowed");
    }

    public Label getAdjustmentDays()
    {
        return container.getLabel("Adjustment Days Allowed");
    }

    public Label getLastDayToAdjust()
    {
        return container.getLabel("Last Day to Adjust");
    }

    public Label getAward()
    {
        return container.getLabel("Award");
    }

    public Label getFmdsPromoID()
    {
        return container.getLabel("FMDS Promo ID");
    }

    public Label getWebDisplayIndicator()
    {
        return container.getLabel("Web Display Indicator");
    }

    public Label getExternalTrackingIndicator()
    {
        return container.getLabel("External Tracking Indicator");
    }

    public Label getEventType()
    {
        return container.getLabel("Event Type");
    }

    public void verify(Offer offer)
    {
        verifyThat(getName(), hasText(offer.getName()));
        verifyThat(getOfferDescription(), hasText(offer.getDescription()));
        verifyThat(getOfferCode(), hasText(offer.getCode()));
        verifyThat(getMaxWins(), hasText(offer.getMaximumWinsAllowed()));
        verifyThat(getAdjustmentDays(), hasText(offer.getAdjustmentDaysAllowed()));
        verifyThat(getLastDayToAdjust(), hasText(offer.getLastDayToAdjust()));
        verifyThat(getAward(), hasText(offer.getAward()));
        verifyThat(getFmdsPromoID(), hasText(offer.getFmdsPromoID()));
        verifyThat(getWebDisplayIndicator(), hasText(offer.getWebDisplayIndicator()));
        verifyThat(getExternalTrackingIndicator(), hasText(offer.getExternalTrackingIndicator()));
        verifyThat(getEventType(), hasText(offer.getEventType()));
    }

    public void capture(Offer.Builder builder)
    {
        builder.name(getName().getText());
        builder.description(getOfferDescription().getText());
        builder.code(getOfferCode().getText());
        builder.maximumWinsAllowed(getMaxWins().getText());
        builder.adjustmentDaysAllowed(getAdjustmentDays().getText());
        builder.lastDayToAdjust(getLastDayToAdjust().getText());
        builder.award(getAward().getText());
        builder.fmdsPromoID(getFmdsPromoID().getText());
        builder.webDisplayIndicator(getWebDisplayIndicator().getText());
        builder.externalTrackingIndicator(getExternalTrackingIndicator().getText());
        builder.eventType(getEventType().getText());
    }
}
