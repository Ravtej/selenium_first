package com.ihg.automation.selenium.servicecenter.pages.event.order;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;

import com.ihg.automation.selenium.common.order.OrderDetailsViewBase;
import com.ihg.automation.selenium.common.order.OrderItem;
import com.ihg.automation.selenium.common.order.VoucherOrder;
import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.Link;

public class OrderDetailsView extends OrderDetailsViewBase
{
    public OrderDetailsView(Container container)
    {
        super(container);
    }

    public OrderDetailsView(Container container, String itemId)
    {
        super(container);
    }

    @Override
    public Link getItemId()
    {
        return new Link(container.getLabel("Item ID"), Link.LINK_XPATH_NO_HYPER_LINK_IN_CLASS);
    }

    @Override
    public void verify(VoucherOrder voucherOrder)
    {
        super.verify(voucherOrder);
        verifyThat(getItemId(), hasText(voucherOrder.getPromotionId()));
    }

    @Override
    public void verify(OrderItem orderItem, String unitType)
    {
        super.verify(orderItem, unitType);
        verifyThat(getItemId(), hasText(orderItem.getItemID()));
    }
}
