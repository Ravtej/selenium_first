package com.ihg.automation.selenium.servicecenter.pages.search;

import com.ihg.automation.selenium.gwt.components.grid.GridRow;
import com.ihg.automation.selenium.gwt.components.grid.GridRowXpath;

public class CustomerSearchGridRow extends GridRow<CustomerSearchGridCell>
{
    public CustomerSearchGridRow(CustomerSearchGrid parent, GridRowXpath xpath)
    {
        super(parent, xpath);
    }
}
