package com.ihg.automation.selenium.servicecenter.pages.event.all;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasTextAsFormattedInt;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;

import org.hamcrest.Matcher;

import com.ihg.automation.selenium.common.event.AllEventsRow;
import com.ihg.automation.selenium.gwt.components.Componentable;
import com.ihg.automation.selenium.gwt.components.grid.CellsName;
import com.ihg.automation.selenium.gwt.components.grid.GridRow;
import com.ihg.automation.selenium.gwt.components.grid.GridRowXpath;

public class AllEventsGridRow extends GridRow<AllEventsGridRow.AllEventsCell>
{
    public AllEventsGridRow(AllEventsGrid parent, GridRowXpath xpath)
    {
        super(parent, xpath);
    }

    public void verify(AllEventsRow rowData)
    {
        verify(rowData, hasText(rowData.getDetail()));
    }

    public void verify(AllEventsRow rowData, Matcher<Componentable> detailMatcher)
    {
        verifyThat(getCell(AllEventsCell.TRANS_TYPE), hasText(rowData.getTransType()));
        verifyThat(getCell(AllEventsCell.TRANS_DATE), hasText(rowData.getTransDate()));
        verifyThat(getCell(AllEventsCell.DETAIL), detailMatcher);
        verifyThat(getCell(AllEventsCell.IHG_UNITS), hasTextAsFormattedInt(rowData.getIhgUnits()));
        verifyThat(getCell(AllEventsCell.ALLIANCE_UNITS), hasTextAsFormattedInt(rowData.getAllianceUnits()));
    }

    public enum AllEventsCell implements CellsName
    {
        TRANS_DATE("eventDate"), TRANS_TYPE("transactionType"), DETAIL("details"), IHG_UNITS(
                "loyaltyUnits.ihgUnits.amount"), ALLIANCE_UNITS("loyaltyUnits.allianceUnits.amount");

        private String name;

        private AllEventsCell(String name)
        {
            this.name = name;
        }

        @Override
        public String getName()
        {
            return name;
        }
    }
}
