package com.ihg.automation.selenium.servicecenter.pages.hotel.freenight;

import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.CustomContainerBase;
import com.ihg.automation.selenium.gwt.components.input.DateInput;
import com.ihg.automation.selenium.gwt.components.input.SearchInput;
import com.ihg.automation.selenium.gwt.components.input.Select;

public class FreeNightReimbursementSearch extends CustomContainerBase
{
    public FreeNightReimbursementSearch(Container container)
    {
        super(container.getSeparator("Reward/Free Night Reimbursement Search",
                ".//fieldset[child::legend[{0}]]/ancestor::table[1]/following-sibling::table[1]"));
    }

    public SearchInput getHotelCode()
    {
        return new SearchInput(container.getLabel("Hotel Code"));
    }

    public Select getStatus()
    {
        return container.getSelectByLabel("Status");
    }

    public DateInput getFrom()
    {
        return new DateInput(container.getLabel("From"));
    }

    public DateInput getTo()
    {
        return new DateInput(container.getLabel("To"));
    }

    public Select getType()
    {
        return container.getSelectByLabel("Type");
    }

    public void populateFields(String hotel, String status, String fromDate, String toDate, String type)
    {
        getHotelCode().typeAndWait(hotel);
        getStatus().select(status);
        getFrom().typeAndWait(fromDate);
        getTo().typeAndWait(toDate);
        getType().select(type);
    }

}
