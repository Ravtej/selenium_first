package com.ihg.automation.selenium.servicecenter.pages.event.freenight;

import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.assertNoError;

import com.ihg.automation.selenium.common.pages.ClosablePopUpBase;
import com.ihg.automation.selenium.gwt.components.Button;
import com.ihg.automation.selenium.gwt.components.input.Select;
import com.ihg.automation.selenium.gwt.components.label.Label;
import com.ihg.automation.selenium.listener.Verifier;

public class CancelFreeNightVoucherPopUp extends ClosablePopUpBase
{

    public CancelFreeNightVoucherPopUp()
    {
        super("Cancel Free Night Voucher");
    }

    public Button getSubmit()
    {
        return container.getButton("Submit");
    }

    public void clickSubmit(Verifier... verifiers)
    {
        getSubmit().clickAndWait(verifiers);
    }

    public Label getVoucherNumber()
    {
        return container.getLabel("Voucher Number");
    }

    public Select getReasonForCancel()
    {
        return container.getSelectByLabel("Reason for Cancel");
    }

    public void cancelFreeNightVoucher(String reason)
    {
        getReasonForCancel().select(reason);
        clickSubmit(assertNoError());
    }
}
