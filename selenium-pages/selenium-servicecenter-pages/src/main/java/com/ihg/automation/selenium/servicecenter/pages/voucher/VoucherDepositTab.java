package com.ihg.automation.selenium.servicecenter.pages.voucher;

import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyNoError;

import com.ihg.automation.selenium.gwt.components.Button;
import com.ihg.automation.selenium.gwt.components.Menu.Item;
import com.ihg.automation.selenium.listener.Verifier;
import com.ihg.automation.selenium.servicecenter.pages.TopMenu;

public class VoucherDepositTab extends VoucherTabBase
{
    public VoucherDepositTab(VoucherPopUp voucherPopUp)
    {
        super(voucherPopUp, "Deposit");
    }

    @Override
    protected String getVoucherNumberLabel()
    {
        return "Voucher Number";
    }

    @Override
    protected Item getMenuItem()
    {
        return new TopMenu().getVoucher().getDeposit();
    }

    public Button getDeposit()
    {
        return container.getButton("Deposit");
    }

    public void clickDeposit(Verifier... verifiers)
    {
        getDeposit().clickAndWait(verifiers);
    }

    public void deposit(String voucherNumber)
    {
        goTo();
        getVoucherNumber().type(voucherNumber);
        clickDeposit(verifyNoError());
    }

}
