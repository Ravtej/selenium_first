package com.ihg.automation.selenium.servicecenter.pages.programs.dr;

import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.grid.Grid;
import com.ihg.automation.selenium.gwt.components.grid.GridRowXpath;
import com.ihg.automation.selenium.servicecenter.pages.programs.dr.DiningBenefitGridRow.DiningBenefitCell;

public class DiningBenefitGrid extends Grid<DiningBenefitGridRow, DiningBenefitCell>
{
    public DiningBenefitGrid(Container parent)
    {
        super(parent);
    }

    public DiningBenefitGridRow getRowByBenefit(String benefit)
    {
        return getRow(new GridRowXpath.GridRowXpathBuilder().cell(DiningBenefitCell.BENEFIT, benefit).build());
    }
}
