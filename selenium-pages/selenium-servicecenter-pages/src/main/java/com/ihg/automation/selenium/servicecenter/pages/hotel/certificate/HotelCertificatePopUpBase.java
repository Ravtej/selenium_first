package com.ihg.automation.selenium.servicecenter.pages.hotel.certificate;

import com.ihg.automation.selenium.common.pages.ClosablePopUpBase;
import com.ihg.automation.selenium.gwt.components.Button;
import com.ihg.automation.selenium.gwt.components.PopUpWindow;

public abstract class HotelCertificatePopUpBase extends ClosablePopUpBase
{
    public HotelCertificatePopUpBase(String name)
    {
        super(PopUpWindow.withBodyText(name, "In-Hotel/Other Certificate Details"));
    }

    public Button getSaveApprove()
    {
        return container.getButton("Save & Approve");
    }

    public Button getSaveChanges()
    {
        return container.getButton("Save Changes");
    }

    abstract public HotelCertificateDetailsBase getDetails();

}
