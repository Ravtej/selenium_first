package com.ihg.automation.selenium.servicecenter.pages.event.offer;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasEmptyText;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static org.hamcrest.core.IsNot.not;

import com.ihg.automation.selenium.common.offer.GuestOffer;
import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.CustomContainerBase;
import com.ihg.automation.selenium.gwt.components.Separator;
import com.ihg.automation.selenium.gwt.components.label.Label;

public class CampaignDetailsContainer extends CustomContainerBase
{
    public CampaignDetailsContainer(Container container)
    {
        super(container.getSeparator("Campaign Details", Separator.FIELDSET_PATTERN_NEXT_TR));
    }

    public Label getName()
    {
        return container.getLabel("Name");
    }

    public Label getCode()
    {
        return container.getLabel("Code");
    }

    public Label getRegistrationDate()
    {
        return container.getLabel("Registration Date");
    }

    public Label getStartDate()
    {
        return container.getLabel("Start Date");
    }

    public Label getEndDate()
    {
        return container.getLabel("End Date");
    }

    public void verify(GuestOffer guestOffer)
    {
        verifyThat(getCode(), hasText(guestOffer.getOffer().getCode()));
        verifyThat(getName(), hasText(guestOffer.getOffer().getName()));
        verifyThat(getRegistrationDate(), not(hasEmptyText()));
        verifyThat(getStartDate(), not(hasEmptyText()));
        verifyThat(getEndDate(), not(hasEmptyText()));
    }

}
