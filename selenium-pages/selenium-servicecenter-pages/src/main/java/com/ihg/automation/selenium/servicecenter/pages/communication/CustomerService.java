package com.ihg.automation.selenium.servicecenter.pages.communication;

import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.CustomContainerBase;

public class CustomerService extends CustomContainerBase
{
    public CustomerService(Container container)
    {
        super(container.getSeparator("Customer Service"));
    }

    public CustomerServicePhone getPhone()
    {
        return new CustomerServicePhone(container);
    }

    public CustomerServiceEmail getEmail()
    {
        return new CustomerServiceEmail(container);
    }
}
