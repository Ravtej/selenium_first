package com.ihg.automation.selenium.servicecenter.pages.search;

import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.grid.Grid;
import com.ihg.automation.selenium.gwt.components.grid.GridRowXpath;

public class CustomerSearchGrid extends Grid<CustomerSearchGridRow, CustomerSearchGridCell>
{
    public CustomerSearchGrid(Container parent)
    {
        super(parent);
    }

    public CustomerSearchGridRow getRow(String memberId)
    {
        return getRow(new GridRowXpath.GridRowXpathBuilder().cell(CustomerSearchGridCell.MEMBER_ID, memberId).build());
    }
}
