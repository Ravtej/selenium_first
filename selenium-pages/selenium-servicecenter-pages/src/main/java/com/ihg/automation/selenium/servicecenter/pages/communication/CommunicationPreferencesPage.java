package com.ihg.automation.selenium.servicecenter.pages.communication;

import com.ihg.automation.selenium.common.pages.TabCustomContainerBase;
import com.ihg.automation.selenium.servicecenter.pages.Tabs;

public class CommunicationPreferencesPage extends TabCustomContainerBase
{
    public CommunicationPreferencesPage()
    {
        super(new Tabs().getCustomerInfo().getCommunicationPref());
    }

    public CommunicationPreferences getCommunicationPreferences()
    {
        return new CommunicationPreferences(container);
    }
}
