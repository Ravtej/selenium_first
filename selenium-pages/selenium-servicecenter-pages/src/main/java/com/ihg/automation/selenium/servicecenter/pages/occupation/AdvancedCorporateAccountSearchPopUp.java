package com.ihg.automation.selenium.servicecenter.pages.occupation;

import com.ihg.automation.selenium.common.event.SearchAware;
import com.ihg.automation.selenium.common.event.SearchButtonBar;
import com.ihg.automation.selenium.common.pages.ClosablePopUpBase;
import com.ihg.automation.selenium.gwt.components.Button;
import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.CustomContainerBase;
import com.ihg.automation.selenium.gwt.components.input.Input;

public class AdvancedCorporateAccountSearchPopUp extends ClosablePopUpBase implements SearchAware
{
    public AdvancedCorporateAccountSearchPopUp()
    {
        super("Advanced Corporate Account Search");
    }

    @Override
    public CorporateAccountSearch getSearchFields()
    {
        return new CorporateAccountSearch(container);
    }

    @Override
    public CorporateAccountSearchButtonBar getButtonBar()
    {
        return new CorporateAccountSearchButtonBar(container);
    }

    public AdvancedCorporateAccountGrid getAdvancedCorporateAccountGrid()
    {
        return new AdvancedCorporateAccountGrid(container);
    }

    public class CorporateAccountSearch extends CustomContainerBase
    {
        public CorporateAccountSearch(Container container)
        {
            super(container);
        }

        public Input getCorporateAccountName()
        {
            return container.getInputByLabel("Corporate Account Name");
        }
    }

    public class CorporateAccountSearchButtonBar extends SearchButtonBar
    {
        public CorporateAccountSearchButtonBar(Container container)
        {
            super(container);
        }

        public Button getCancel()
        {
            return container.getButton(Button.CANCEL);
        }

        public void clickCancel()
        {
            getCancel().clickAndWait();
        }

        public Button getSelectCorporateAccount()
        {
            return container.getButton("Select Corporate Account");
        }

        public void clickSelectCorporateAccount()
        {
            getSelectCorporateAccount().clickAndWait();
        }
    }
}
