package com.ihg.automation.selenium.servicecenter.pages.event.freenight;

import com.ihg.automation.selenium.gwt.components.grid.CellsName;
import com.ihg.automation.selenium.gwt.components.grid.GridRow;
import com.ihg.automation.selenium.gwt.components.grid.GridRowXpath;

public class FreeNightReimbursementDetailGridRow
        extends GridRow<FreeNightReimbursementDetailGridRow.FreeNightReimbursementDetailCell>
{
    public FreeNightReimbursementDetailGridRow(FreeNightReimbursementDetailGrid parent, GridRowXpath xpath)
    {
        super(parent, xpath);
    }

    public enum FreeNightReimbursementDetailCell implements CellsName
    {
        SUFFIX("suffix"), STATUS("reimbursementStatus"), FREE_NIGHT_VOUCHER("voucherKey");

        private String name;

        private FreeNightReimbursementDetailCell(String name)
        {
            this.name = name;
        }

        @Override
        public String getName()
        {
            return name;
        }
    }

}
