package com.ihg.automation.selenium.servicecenter.pages.event.stay;

import com.ihg.automation.selenium.common.Revenue;
import com.ihg.automation.selenium.common.pages.Populate;
import com.ihg.automation.selenium.common.pages.VerifyMultiMode;
import com.ihg.automation.selenium.common.stay.Revenues;
import com.ihg.automation.selenium.gwt.components.CustomContainerBase;
import com.ihg.automation.selenium.gwt.components.Mode;

public class RevenueDetailsEdit extends RevenueDetailsBase implements Populate<Revenues>, VerifyMultiMode<Revenues>
{
    public RevenueDetailsEdit(CustomContainerBase parent)
    {
        super(parent);
    }

    @Override
    public RevenueExtended getTotalRoom()
    {
        return new RevenueExtended(container, TOTAL_ROOM_LABEL);
    }

    @Override
    public RevenueExtended getFood()
    {
        return new RevenueExtended(container, FOOD_LABEL);
    }

    @Override
    public RevenueExtended getBeverage()
    {
        return new RevenueExtended(container, BEVERAGE_LABEL);
    }

    @Override
    public RevenueExtended getPhone()
    {
        return new RevenueExtended(container, PHONE_LABEL);
    }

    @Override
    public RevenueExtended getMeeting()
    {
        return new RevenueExtended(container, MEETING_LABEL);
    }

    @Override
    public RevenueExtended getMandatoryRevenue()
    {
        return new RevenueExtended(container, MANDATORY_REVENUE_LABEL);
    }

    @Override
    public RevenueExtended getOtherRevenue()
    {
        return new RevenueExtended(container, OTHER_REVENUE_LABEL);
    }

    @Override
    public RevenueExtended getNoRevenue()
    {
        return new RevenueExtended(container, NO_REVENUE_LABEL);
    }

    @Override
    public Revenue getRoomTax()
    {
        return new Revenue(container, ROOM_TAX_LABEL);
    }

    @Override
    public Revenue getTotalRevenue()
    {
        return new Revenue(container, TOTAL_REVENUE_LABEL);
    }

    @Override
    public Revenue getTotalNonQualifyingRevenue()
    {
        return new Revenue(container, TOTAL_NON_QUALIFYING_REVENUE_LABEL);
    }

    @Override
    public Revenue getTotalQualifyingRevenue()
    {
        return new Revenue(container, TOTAL_QUALIFYING_REVENUE_LABEL);
    }

    @Override
    public void populate(Revenues revenues)
    {
        getTotalRoom().populate(revenues.getRoom());
        getFood().populate(revenues.getFood());
        getBeverage().populate(revenues.getBeverage());
        getPhone().populate(revenues.getPhone());
        getMeeting().populate(revenues.getMeeting());
        getMandatoryRevenue().populate(revenues.getMandatoryLoyalty());
        getOtherRevenue().populate(revenues.getOtherLoyalty());
        getNoRevenue().populate(revenues.getNoLoyalty());
    }

    @Override
    public void verify(Revenues revenues, Mode mode)
    {
        getTotalRoom().verify(revenues.getRoom(), mode);
        getFood().verify(revenues.getFood(), mode);
        getBeverage().verify(revenues.getBeverage(), mode);
        getPhone().verify(revenues.getPhone(), mode);
        getMeeting().verify(revenues.getMeeting(), mode);
        getMandatoryRevenue().verify(revenues.getMandatoryLoyalty(), mode);
        getOtherRevenue().verify(revenues.getOtherLoyalty(), mode);
        getNoRevenue().verify(revenues.getNoLoyalty(), mode);
        getRoomTax().verify(revenues.getTax());
        getTotalRevenue().verify(revenues.getOverallTotal());
        getTotalNonQualifyingRevenue().verify(revenues.getTotalNonQualifying());
        getTotalQualifyingRevenue().verify(revenues.getTotalQualifying());
    }

    @Override
    public void verify(Revenues revenues)
    {
        verify(revenues, Mode.EDIT);
    }
}
