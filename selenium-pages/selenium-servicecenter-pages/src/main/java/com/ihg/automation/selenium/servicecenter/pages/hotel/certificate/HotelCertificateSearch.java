package com.ihg.automation.selenium.servicecenter.pages.hotel.certificate;

import com.ihg.automation.selenium.gwt.components.Button;
import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.CustomContainerBase;
import com.ihg.automation.selenium.gwt.components.checkbox.CheckBox;
import com.ihg.automation.selenium.gwt.components.input.DateInput;
import com.ihg.automation.selenium.gwt.components.input.Input;
import com.ihg.automation.selenium.gwt.components.input.SearchInput;
import com.ihg.automation.selenium.gwt.components.input.Select;
import com.ihg.automation.selenium.gwt.components.utils.ByText;

public class HotelCertificateSearch extends CustomContainerBase
{
    public HotelCertificateSearch(Container container)
    {
        super(container.getSeparator("In-Hotel/Other Certificate Search"));
    }

    public SearchInput getHotelCode()
    {
        return new SearchInput(container.getLabel("Hotel"));
    }

    public Input getCertificateNumber()
    {
        return container.getInputByLabel("Certificate Number");
    }

    public Input getConfirmationNumber()
    {
        return container.getInputByLabel("Confirmation Number");
    }

    public Input getItemId()
    {
        return container.getInputByLabel("Item ID");
    }

    public Select getStatusOfRequest()
    {
        return container.getSelectByLabel("Status of Request");
    }

    public CheckBox getCheckIn()
    {
        return new CheckBox(container, ByText.exact("Check In"), CheckBox.CHECK_BOX_PATTERN2);
    }

    public CheckBox getCheckOut()
    {
        return new CheckBox(container, ByText.exact("Check Out"), CheckBox.CHECK_BOX_PATTERN2);
    }

    public DateInput getFromDate()
    {
        return new DateInput(container.getLabel("From"));
    }

    public DateInput getToDate()
    {
        return new DateInput(container.getLabel("To"));
    }

    public Button getEnterCertificate()
    {
        return new Button(container.getParent(), "Enter Certificate");
    }

    public CreateHotelCertificatePopUp clickEnterCertificate()
    {
        getEnterCertificate().clickAndWait();
        return new CreateHotelCertificatePopUp();
    }
}
