package com.ihg.automation.selenium.servicecenter.pages.event.offer;

import com.ihg.automation.selenium.common.offer.GuestOffer;
import com.ihg.automation.selenium.gwt.components.Container;

public class OfferDetailsViewExtended extends OfferDetailsView
{
    public OfferDetailsViewExtended(Container parent)
    {
        super(parent.getSeparator("Offer Details"));
    }

    public RegistrationDetailsContainer getRegistrationDetails()
    {
        return new RegistrationDetailsContainer(container);
    }

    public void verify(GuestOffer guestOffer)
    {
        verify(guestOffer.getOffer());
        getRegistrationDetails().verify(guestOffer);
    }
}
