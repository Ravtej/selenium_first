package com.ihg.automation.selenium.servicecenter.pages.event.order;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasAnyText;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasTextAsFormattedInt;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasTextAsInt;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.text.IsEmptyString.isEmptyString;

import org.openqa.selenium.support.Color;

import com.ihg.automation.common.DateUtils;
import com.ihg.automation.selenium.common.order.Order;
import com.ihg.automation.selenium.common.order.VoucherOrder;
import com.ihg.automation.selenium.common.order.VoucherOrderStatus;
import com.ihg.automation.selenium.gwt.components.grid.CellsName;
import com.ihg.automation.selenium.gwt.components.grid.GridCell;
import com.ihg.automation.selenium.gwt.components.grid.GridRow;
import com.ihg.automation.selenium.gwt.components.grid.GridRowXpath;

public class OrderGridRow extends GridRow<OrderGridRow.OrderCell>
{
    private final static String GREEN_COLOR = "#00aa00";

    public OrderGridRow(OrderGrid parent, GridRowXpath xpath)
    {
        super(parent, xpath);
    }

    public void verify(VoucherOrder voucherOrder)
    {
        verifyThat(getCell(OrderCell.TRANS_DATE), hasText(voucherOrder.getTransactionDate()));
        verifyThat(getCell(OrderCell.TRANS_TYPE), hasText(VoucherOrder.TRANSACTION_TYPE));
        verifyThat(getCell(OrderCell.ORDER_NUMBER), hasText(isEmptyString()));
        verifyThat(getCell(OrderCell.ITEM_NAME), hasText(voucherOrder.getVoucherName()));
        verifyThat(getCell(OrderCell.STATUS), hasText(voucherOrder.getStatus()));
        verifyThat(getCell(OrderCell.CASH), hasText(voucherOrder.getVoucherCostAmount()));
        verifyThat(getCell(OrderCell.IHG_UNITS), hasText(isEmptyString()));
        verifyThat(getCell(OrderCell.ALLIANCE_UNITS), hasText(isEmptyString()));
    }

    public void verify(Order order)
    {
        String itemName = "";

        if (order.getOrderItems().size() == 1)
        {
            itemName = order.getOrderItems().get(0).getItemName();
        }
        else if (order.getOrderItems().size() > 1)
        {
            itemName = "Multiple items";
        }

        verifyThat(getCell(OrderCell.ITEM_NAME), hasText(itemName));
        verifyThat(getCell(OrderCell.IHG_UNITS), hasTextAsInt(Integer.parseInt(order.getTotalLoyaltyUnits())));
        verifyThat(getCell(OrderCell.TRANS_TYPE), hasText(order.getTransactionType()));
        verifyThat(getCell(OrderCell.TRANS_DATE), hasText(DateUtils.getFormattedDate()));
        verifyThat(getCell(OrderCell.STATUS), hasText(VoucherOrderStatus.PROCESSING.getName()));
        verifyThat(getCell(OrderCell.ORDER_NUMBER), hasAnyText());
        verifyThat(getCell(OrderCell.ALLIANCE_UNITS), hasTextAsFormattedInt(order.getTotalAllianceUnits()));
    }

    public void verifyRowGreenColor()
    {
        GridCell itemCell = getCell(OrderCell.ITEM_NAME);
        Color color = Color.fromString(itemCell.getCssValue("color"));
        verifyThat(itemCell, color.asHex(), is(GREEN_COLOR), "The item is displayed in a row in green color");
    }

    public enum OrderCell implements CellsName
    {
        TRANS_DATE("eventDate"), TRANS_TYPE("transactionType"), ORDER_NUMBER("orderNumber"), ITEM_NAME(
                "itemName"), STATUS("status"), CASH("cash"), IHG_UNITS(
                        "loyaltyUnits.ihgUnits.amount"), ALLIANCE_UNITS("loyaltyUnits.allianceUnits.amount");

        private String name;

        private OrderCell(String name)
        {
            this.name = name;
        }

        @Override
        public String getName()
        {
            return name;
        }
    }
}
