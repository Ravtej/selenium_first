package com.ihg.automation.selenium.servicecenter.pages.personal;

import com.ihg.automation.selenium.gwt.components.Button;
import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.CustomContainerBase;

public class SocialId extends CustomContainerBase
{
    public SocialId(Container container)
    {
        super(container.getSeparator("Social ID"));
    }

    public SocialIdGrid getSocialIdGrid()
    {
        return new SocialIdGrid(container);
    }

    public Button getAddSocial()
    {
        return container.getButton("Add Social");
    }
}
