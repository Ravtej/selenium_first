package com.ihg.automation.selenium.servicecenter.pages.event.rewardnight;

import com.ihg.automation.selenium.common.pages.SubmitPopUpBase;
import com.ihg.automation.selenium.gwt.components.PopUpWindow;
import com.ihg.automation.selenium.gwt.components.input.Input;

public class ReservationCancellationPopUp extends SubmitPopUpBase
{
    public ReservationCancellationPopUp()
    {
        super(PopUpWindow.withBodyText("Cancel Reservation", "Please enter reservation cancellation number"));
    }

    public Input getCancellationNumber()
    {
        return container.getInputByLabel("Cancellation Number");
    }

}
