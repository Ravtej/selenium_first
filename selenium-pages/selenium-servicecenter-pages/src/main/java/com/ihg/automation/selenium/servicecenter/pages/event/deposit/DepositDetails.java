package com.ihg.automation.selenium.servicecenter.pages.event.deposit;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasDefaultOrText;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;

import com.ihg.automation.selenium.common.deposit.Deposit;
import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.CustomContainerBase;
import com.ihg.automation.selenium.gwt.components.Link;
import com.ihg.automation.selenium.gwt.components.label.Label;
import com.ihg.automation.selenium.gwt.components.label.SecurityLabel;
import com.ihg.automation.selenium.servicecenter.pages.deposit.DepositDetailsFieldsAware;

public class DepositDetails extends CustomContainerBase implements DepositDetailsFieldsAware
{
    public DepositDetails(Container container)
    {
        super(container);
    }

    @Override
    public Label getTransactionId()
    {
        return container.getLabel(DepositDetailsFieldsAware.TRANSACTION_ID);
    }

    @Override
    public Link getHotel()
    {
        return new Link(container.getLabel(DepositDetailsFieldsAware.HOTEL));
    }

    @Override
    public Label getCheckInDate()
    {
        return container.getLabel(DepositDetailsFieldsAware.CHECKIN_DATE);
    }

    @Override
    public Label getCheckOutDate()
    {
        return container.getLabel(DepositDetailsFieldsAware.CHECKOUT_DATE);
    }

    @Override
    public Label getPartnerTransactionDate()
    {
        return container.getLabel(DepositDetailsFieldsAware.TRANSACTION_DATE);
    }

    @Override
    public Label getOrderTrackingNumber()
    {
        return container.getLabel(DepositDetailsFieldsAware.TRACKING_NUMBER);
    }

    @Override
    public Label getTransactionName()
    {
        return container.getLabel(DepositDetailsFieldsAware.TRANSACTION_NAME);
    }

    @Override
    public Label getTransactionDescription()
    {
        return container.getLabel(DepositDetailsFieldsAware.TRANSACTION_DESCRIPTION);
    }

    @Override
    public Label getPartnerComment()
    {
        return container.getLabel("Partner Description");
    }

    public Label getCustomerServiceComment()
    {
        return new SecurityLabel(container, CUSTOMER_SERVICE_COMMENTS);
    }

    public Label getReason()
    {
        return container.getLabel("Reason");
    }

    public void verify(Deposit deposit)
    {
        verifyThat(getTransactionId(), hasText(deposit.getTransactionId()));
        verifyThat(getHotel(), hasText(deposit.getHotel()));
        verifyThat(getCheckInDate(), hasDefaultOrText(deposit.getCheckInDate()));
        verifyThat(getCheckOutDate(), hasDefaultOrText(deposit.getCheckOutDate()));
        verifyThat(getPartnerTransactionDate(), hasDefaultOrText(deposit.getPartnerTransactionDate()));
        verifyThat(getOrderTrackingNumber(), hasDefaultOrText(deposit.getOrderTrackingNumber()));
        verifyThat(getTransactionName(), hasText(deposit.getTransactionName()));
        verifyThat(getTransactionDescription(), hasText(deposit.getTransactionDescription()));
        verifyThat(getPartnerComment(), hasDefaultOrText(deposit.getPartnerComment()));
        verifyThat(getReason(), hasDefaultOrText(deposit.getReason()));
        verifyThat(getCustomerServiceComment(), hasDefaultOrText(deposit.getCustomerServiceComment()));
    }

}
