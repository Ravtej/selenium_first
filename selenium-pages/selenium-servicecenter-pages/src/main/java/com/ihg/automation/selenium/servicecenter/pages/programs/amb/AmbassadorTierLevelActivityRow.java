package com.ihg.automation.selenium.servicecenter.pages.programs.amb;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;

import com.ihg.automation.selenium.gwt.components.grid.CellsName;
import com.ihg.automation.selenium.gwt.components.grid.Grid;
import com.ihg.automation.selenium.gwt.components.grid.GridRow;
import com.ihg.automation.selenium.gwt.components.grid.GridRowXpath;

public class AmbassadorTierLevelActivityRow extends GridRow<AmbassadorTierLevelActivityRow.AmbassadorTierLevelCell>
{
    public AmbassadorTierLevelActivityRow(Grid parent, GridRowXpath xpath)
    {
        super(parent, xpath);
    }

    public void verify(String currentNights, String achieveNights)
    {
        verifyThat(getCell(AmbassadorTierLevelCell.CURRENT), hasText(currentNights));
        verifyThat(getCell(AmbassadorTierLevelCell.ACHIEVE), hasText(achieveNights));
    }

    public enum AmbassadorTierLevelCell implements CellsName
    {
        ACCUMULATOR("accumulatorDescription "), CURRENT("currentValue"), ACHIEVE("amountNeededToAchieveThreshold");

        private String name;

        private AmbassadorTierLevelCell(String name)
        {
            this.name = name;
        }

        @Override
        public String getName()
        {
            return name;
        }
    }

}
