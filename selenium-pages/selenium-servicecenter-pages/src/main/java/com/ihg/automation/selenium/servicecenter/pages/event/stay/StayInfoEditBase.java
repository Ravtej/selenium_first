package com.ihg.automation.selenium.servicecenter.pages.event.stay;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasTextAsDouble;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;

import com.ihg.automation.selenium.common.components.IndicatorSelect;
import com.ihg.automation.selenium.common.pages.Populate;
import com.ihg.automation.selenium.common.stay.Stay;
import com.ihg.automation.selenium.gwt.components.Component;
import com.ihg.automation.selenium.gwt.components.CustomContainerBase;
import com.ihg.automation.selenium.gwt.components.input.DateInput;
import com.ihg.automation.selenium.gwt.components.input.Input;
import com.ihg.automation.selenium.gwt.components.input.Select;

public abstract class StayInfoEditBase extends StayInfoBase implements Populate<Stay>
{
    protected final static String HOTEL_LABEL = "Hotel";

    public StayInfoEditBase(CustomContainerBase parent)
    {
        super(parent);
    }

    @Override
    public DateInput getCheckIn()
    {
        return new DateInput(container.getLabel(CHECK_IN_LABEL));
    }

    @Override
    public DateInput getCheckOut()
    {
        return new DateInput(container.getLabel(CHECK_OUT_LABEL));
    }

    @Override
    public Input getRoomNumber()
    {
        return container.getInputByLabel(ROOM_NUMBER_LABEL);
    }

    @Override
    public Input getRoomType()
    {
        return container.getInputByLabel(ROOM_TYPE_LABEL);
    }

    @Override
    public Input getRateCode()
    {
        return container.getInputByLabel(RATE_CODE_LABEL);
    }

    @Override
    public Input getCorpAcctNumber()
    {
        return container.getInputByLabel(CORP_ACCT_NUMBER_LABEL);
    }

    @Override
    public Input getIATANumber()
    {
        return container.getInputByLabel(IATA_NUMBER_LABEL);
    }

    @Override
    public IndicatorSelect getEnrollingStay()
    {
        return new IndicatorSelect(container.getLabel(ENROLLING_STAY_LABEL));
    }

    @Override
    public Input getNights()
    {
        return container.getInputByLabel(NIGHTS_LABEL);
    }

    @Override
    public Select getHotelCurrency()
    {
        return container.getSelectByLabel(HOTEL_CURRENCY_LABEL);
    }

    public abstract Component getHotel();

    public Input getAvgRoomRate()
    {
        return container.getInputByLabel("Avg Room Rate");
    }

    public Select getEarningPreference()
    {
        return container.getSelectByLabel("Earning Preference");
    }

    @Override
    public void populate(Stay stay)
    {
        getCheckIn().typeAndWait(stay.getCheckIn());
        getCheckOut().typeAndWait(stay.getCheckOut());

        getAvgRoomRate().type(stay.getAvgRoomRate());
        getHotelCurrency().selectByCode(stay.getHotelCurrency());
        getEarningPreference().select(stay.getEarningPreference());

        getRateCode().type(stay.getRateCode());
        getRoomType().type(stay.getRoomType());
        getRoomNumber().type(stay.getRoomNumber());

        getCorpAcctNumber().type(stay.getCorporateAccountNumber());
        getIATANumber().type(stay.getIataCode());

        getEnrollingStay().select(stay.getEnrollingStay());
    }

    @Override
    public void verify(Stay stay)
    {
        verifyThat(getHotel(), hasText(stay.getHotelCode()));
        verifyThat(getAvgRoomRate(), hasTextAsDouble(stay.getAvgRoomRate()));
        verifyThat(getEarningPreference(), hasText(stay.getEarningPreference()));
        verifyThat(getHotelCurrency(), hasText(stay.getHotelCurrency().getCodeWithValue()));

        super.verify(stay);
    }
}
