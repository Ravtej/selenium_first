package com.ihg.automation.selenium.servicecenter.pages.event.offer;

import com.ihg.automation.selenium.common.history.EventHistoryTab;
import com.ihg.automation.selenium.common.pages.ClosablePopUpBase;
import com.ihg.automation.selenium.gwt.components.CustomContainerBase;
import com.ihg.automation.selenium.gwt.components.PopUpWindow;

public abstract class OfferPopUpBase extends ClosablePopUpBase
{
    public OfferPopUpBase()
    {
        super(PopUpWindow.withoutHeader("Offer Details"));
    }

    public abstract CustomContainerBase getOfferDetailsTab();

    public OfferEventTab getOfferEventTab()
    {
        return new OfferEventTab(this);
    }

    public EventHistoryTab getHistoryTab()
    {
        return new EventHistoryTab(this, "History");
    }
}
