package com.ihg.automation.selenium.servicecenter.pages.event;

import com.ihg.automation.selenium.common.components.RangeSelect;
import com.ihg.automation.selenium.gwt.components.input.DateInput;

public interface DateWithRangeFieldsAware
{
    public DateInput getDate();

    public RangeSelect getRange();
}
