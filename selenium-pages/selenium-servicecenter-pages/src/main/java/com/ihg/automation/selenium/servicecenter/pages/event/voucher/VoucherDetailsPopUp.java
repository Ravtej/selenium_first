package com.ihg.automation.selenium.servicecenter.pages.event.voucher;

import com.ihg.automation.selenium.common.billing.EventBillingDetailsTab;
import com.ihg.automation.selenium.common.billing.EventBillingTabAware;
import com.ihg.automation.selenium.common.earning.EventEarningDetailsTab;
import com.ihg.automation.selenium.common.earning.EventEarningTabAware;
import com.ihg.automation.selenium.common.pages.ClosablePopUpBase;

public class VoucherDetailsPopUp extends ClosablePopUpBase implements EventEarningTabAware, EventBillingTabAware
{
    public VoucherDetailsPopUp()
    {
        super("Voucher Details");
    }

    public VoucherDetailsTab getVoucherDetailsTab()
    {
        return new VoucherDetailsTab(this);
    }

    @Override
    public EventEarningDetailsTab getEarningDetailsTab()
    {
        return new EventEarningDetailsTab(this);
    }

    @Override
    public EventBillingDetailsTab getBillingDetailsTab()
    {
        return new EventBillingDetailsTab(this);
    }
}
