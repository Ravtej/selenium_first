package com.ihg.automation.selenium.servicecenter.pages.event.rewardnight;

import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.assertNoError;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasTextAsInt;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ihg.automation.common.DateUtils;
import com.ihg.automation.selenium.common.pages.ClosablePopUpBase;
import com.ihg.automation.selenium.common.reward.RewardNight;
import com.ihg.automation.selenium.gwt.components.Button;
import com.ihg.automation.selenium.gwt.components.input.DateInput;
import com.ihg.automation.selenium.gwt.components.input.Input;
import com.ihg.automation.selenium.gwt.components.input.SearchInput;

public class CreateRewardNightPopUp extends ClosablePopUpBase
{
    private static final Logger logger = LoggerFactory.getLogger(CreateRewardNightPopUp.class);

    public CreateRewardNightPopUp()
    {
        super("Reward Night Details");
    }

    public Input getConfirmationNumber()
    {
        return container.getInputByLabel("Confirmation Number");
    }

    public SearchInput getHotel()
    {
        return new SearchInput(container.getLabel("Hotel"));
    }

    public Input getRoomType()
    {
        return container.getInputByLabel("Room Type");
    }

    public Input getNumberOfRooms()
    {
        return container.getInputByLabel("Number of Rooms");
    }

    public DateInput getCheckInDate()
    {
        return new DateInput(container.getLabel("Check In"));
    }

    public DateInput getCheckOutDate()
    {
        return new DateInput(container.getLabel("Check Out"));
    }

    public Input getNights()
    {
        return container.getInputByLabel("Nights");
    }

    public Button getCreateRewardNight()
    {
        return container.getButton("Create Reward Night");
    }

    public void clickCreateRewardNight()
    {
        getCreateRewardNight().clickAndWait(assertNoError());
    }

    public void populate(RewardNight night)
    {
        getConfirmationNumber().typeAndWait(night.getConfirmationNumber());
        getHotel().typeAndWait(night.getHotel());
        getRoomType().typeAndWait(night.getRoomType());
        getNumberOfRooms().typeAndWait(night.getNumberOfRooms());
        getCheckInDate().typeAndWait(night.getCheckIn());
        getCheckOutDate().typeAndWait(night.getCheckOut());
        if (getNights().isEnabled())
        {
            getNights().typeAndWait(night.getNights());
        }
        else
        {
            verifyThat(getNights(),
                    hasTextAsInt(DateUtils.getDaysBetween(night.getCheckIn(), night.getCheckOut(), true)));
        }
    }

    public void create(RewardNight night)
    {
        logger.debug("Create {}", night);
        populate(night);
        getCreateRewardNight().clickAndWait(assertNoError());
        verifyThat(this, displayed(false));
    }

}
