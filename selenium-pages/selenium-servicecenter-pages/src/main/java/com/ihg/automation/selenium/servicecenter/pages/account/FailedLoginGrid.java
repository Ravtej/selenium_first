package com.ihg.automation.selenium.servicecenter.pages.account;

import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.grid.Grid;

public class FailedLoginGrid extends Grid<FailedLoginGridRow, FailedLoginGridRow.FailedLoginCell>
{
    public FailedLoginGrid(Container parent)
    {
        super(parent);
    }

}
