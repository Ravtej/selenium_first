package com.ihg.automation.selenium.servicecenter.pages.event.order;

import com.ihg.automation.selenium.common.Constant;
import com.ihg.automation.selenium.common.EventSourceAware;
import com.ihg.automation.selenium.common.event.EventGridRowContainerWithDetails;
import com.ihg.automation.selenium.common.event.EventSourceExtended;
import com.ihg.automation.selenium.common.order.VoucherOrder;
import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.common.order.ShippingInfoView;
import com.ihg.automation.selenium.servicecenter.pages.voucher.VoucherOrderDetails;

public class VoucherOrderGridRowContainer extends EventGridRowContainerWithDetails implements EventSourceAware
{
    public VoucherOrderGridRowContainer(Container parent)
    {
        super(parent);
    }

    public OrderDetailsView getOrderDetails()
    {
        return new OrderDetailsView(container);
    }

    public VoucherOrderDetails getVoucherDetails()
    {
        return new VoucherOrderDetails(container);
    }

    public ShippingInfoView getShippingInfo()
    {
        return new ShippingInfoView(container);
    }

    @Override
    public EventSourceExtended getSource()
    {
        return new EventSourceExtended(container);
    }

    public void verify(VoucherOrder voucherOrder)
    {
        getOrderDetails().verify(voucherOrder);
        getVoucherDetails().verify(voucherOrder);
        getShippingInfo().verifyAsOnCustomerInformation(voucherOrder.getDeliveryOption());
        getSource().verify(voucherOrder.getSource(), Constant.NOT_AVAILABLE);
    }
}
