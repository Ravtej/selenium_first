package com.ihg.automation.selenium.servicecenter.pages.event.offer;

import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.CustomContainerBase;

public class SubOfferGridRowContainer extends CustomContainerBase
{
    public SubOfferGridRowContainer(Container parent)
    {
        super(parent);
    }

    public SubOfferDetails getSubOfferDetails()
    {
        return new SubOfferDetails(container);
    }

    public QualifyingStayDetails getQualifyingStayDetails()
    {
        return new QualifyingStayDetails(container);
    }
}
