package com.ihg.automation.selenium.servicecenter.pages.event.business;

import com.ihg.automation.selenium.common.business.BusinessEvent;
import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.grid.Grid;
import com.ihg.automation.selenium.gwt.components.grid.GridRowXpath;

public class BusinessEventsGrid extends Grid<BusinessEventsGridRow, BusinessEventsGridRow.BusinessEventsCell>
{
    public BusinessEventsGrid(Container parent)
    {
        super(parent);
    }

    public BusinessEventsGridRow getRow(String transType)
    {
        return getRow(new GridRowXpath.GridRowXpathBuilder()
                .cell(BusinessEventsGridRow.BusinessEventsCell.TRANS_TYPE, transType).build());
    }

    public BusinessEventsGridRow getRowById(String eventId)
    {
        return getRow(new GridRowXpath.GridRowXpathBuilder()
                .cell(BusinessEventsGridRow.BusinessEventsCell.EVENT_ID, eventId).build());
    }

    public BusinessEventsGridRow getRow(BusinessEvent businessEvent)
    {
        return getRow(new GridRowXpath.GridRowXpathBuilder()
                .cell(BusinessEventsGridRow.BusinessEventsCell.TRANS_TYPE, businessEvent.getTransactionType())
                .cell(BusinessEventsGridRow.BusinessEventsCell.EVENT_NAME, businessEvent.getEventName()).build());
    }
}
