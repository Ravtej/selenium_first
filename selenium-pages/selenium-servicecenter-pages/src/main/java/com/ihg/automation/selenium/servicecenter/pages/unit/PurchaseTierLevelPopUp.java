package com.ihg.automation.selenium.servicecenter.pages.unit;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.assertThat;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;

import com.ihg.automation.selenium.common.pages.NavigatePage;
import com.ihg.automation.selenium.common.pages.SubmitPopUpBase;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.gwt.components.input.Select;
import com.ihg.automation.selenium.servicecenter.pages.TopMenu;

public class PurchaseTierLevelPopUp extends SubmitPopUpBase implements NavigatePage
{
    private TopMenu menu = new TopMenu();

    public PurchaseTierLevelPopUp()
    {
        super("Purchase Tier-Level");
    }

    @Override
    public void goTo()
    {
        if (!isDisplayed())
        {
            menu.getUnitManagement().getPurchaseTierLevel().clickAndWait();

            flush();
            assertThat(this, displayed(true));
        }
    }

    public Select getProgram()
    {
        return container.getSelectByLabel("Program");
    }

    public Select getTierLevel()
    {
        return container.getSelectByLabel("Tier Level");
    }

    public void setLevel(Program program, String level)
    {
        getProgram().selectByValue(program);
        getTierLevel().select(level);
        clickSubmitNoError();
    }
}
