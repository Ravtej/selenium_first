package com.ihg.automation.selenium.servicecenter.pages.event.offer;

import com.ihg.automation.selenium.common.event.SearchAware;
import com.ihg.automation.selenium.common.pages.TabCustomContainerBase;
import com.ihg.automation.selenium.gwt.components.panel.Panel;
import com.ihg.automation.selenium.gwt.components.utils.ByText;
import com.ihg.automation.selenium.servicecenter.pages.Tabs;

public class OfferEventPage extends TabCustomContainerBase implements SearchAware
{
    public OfferEventPage()
    {
        super(new Tabs().getEvents().getOffer());
    }

    @Override
    public OfferSearch getSearchFields()
    {
        return new OfferSearch(container);
    }

    @Override
    public OfferSearchButtonBar getButtonBar()
    {
        return new OfferSearchButtonBar(this);
    }

    public Panel getMemberOffersPanel()
    {
        return container.getPanel(ByText.contains("'s Offers"));
    }

    public MemberOffersGrid getMemberOffersGrid()
    {
        return new MemberOffersGrid(getMemberOffersPanel());
    }

    public Panel getUniverseOffersPanel()
    {
        return container.getPanel("Universe Offers");
    }

    public UniverseOffersGrid getUniverseOffersGrid()
    {
        return new UniverseOffersGrid(getUniverseOffersPanel());
    }

    public void searchByOfferId(String offerId, Boolean isEligible)
    {
        getSearchFields().getEligible().check(isEligible);
        getSearchFields().getOfferCode().typeAndWait(offerId);
        getButtonBar().clickSearch();
    }
}
