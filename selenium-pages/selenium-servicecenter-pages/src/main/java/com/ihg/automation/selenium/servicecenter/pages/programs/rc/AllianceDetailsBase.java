package com.ihg.automation.selenium.servicecenter.pages.programs.rc;

import com.ihg.automation.selenium.common.components.Country;
import com.ihg.automation.selenium.common.member.MemberAlliance;
import com.ihg.automation.selenium.common.name.PersonNameAware;
import com.ihg.automation.selenium.common.name.PersonNameCustomerFields;
import com.ihg.automation.selenium.common.name.PersonNameFields;
import com.ihg.automation.selenium.common.pages.Populate;
import com.ihg.automation.selenium.gwt.components.Component;
import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.CustomContainerBase;
import com.ihg.automation.selenium.gwt.components.input.Input;

public abstract class AllianceDetailsBase extends CustomContainerBase
        implements PersonNameAware, Populate<MemberAlliance>
{
    protected static final String ALLIANCE_LABEL = "Alliance";

    public AllianceDetailsBase(Container container)
    {
        super(container);
    }

    public abstract Component getAlliance();

    public Input getAllianceNumber()
    {
        return container.getInputByLabel("Alliance Number");
    }

    @Override
    public PersonNameFields getName()
    {
        return new PersonNameCustomerFields(container, Country.US);
    }

    @Override
    public void populate(MemberAlliance alliance)
    {
        getAllianceNumber().type(alliance.getNumber());
        getName().populate(alliance.getName());
    }
}
