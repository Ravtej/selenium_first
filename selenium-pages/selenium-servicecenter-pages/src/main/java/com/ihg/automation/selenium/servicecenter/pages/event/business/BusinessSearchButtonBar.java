package com.ihg.automation.selenium.servicecenter.pages.event.business;

import com.ihg.automation.selenium.common.event.SearchButtonBar;
import com.ihg.automation.selenium.gwt.components.Button;

public class BusinessSearchButtonBar extends SearchButtonBar
{
    public BusinessSearchButtonBar(BusinessEventsPage page)
    {
        super(page.container);
    }

    public Button getCreateEvent()
    {
        return container.getButton("Create Event");
    }

    public void clickCreateEvent()
    {
        getCreateEvent().clickAndWait();
    }

}
