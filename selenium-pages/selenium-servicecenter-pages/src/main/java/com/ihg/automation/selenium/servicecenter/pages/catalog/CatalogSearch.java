package com.ihg.automation.selenium.servicecenter.pages.catalog;

import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.CustomContainerBase;
import com.ihg.automation.selenium.gwt.components.checkbox.CheckBox;
import com.ihg.automation.selenium.gwt.components.input.Input;
import com.ihg.automation.selenium.gwt.components.input.Select;

public class CatalogSearch extends CustomContainerBase
{
    public CatalogSearch(Container container)
    {
        super(container.getSeparator("Catalog Search"));
    }

    public Select getCatalogID()
    {
        return container.getSelectByLabel("Catalog ID");
    }

    public Input getItemID()
    {
        return container.getInputByLabel("Item ID");
    }

    public Input getItemName()
    {
        return container.getInputByLabel("Item Name");
    }

    public LoyaltyUnitCost getLoyaltyUnitCost()
    {
        return new LoyaltyUnitCost(container);
    }

    public CheckBox getDisplayExpiredItems()
    {
        return container.getLabel("Display Expired Items").getCheckBox();
    }

    public void populate(String catalogId, String itemId, String itemName, String loyaltyUnitCostMin,
            String loyaltyUnitCostMax)
    {
        getCatalogID().select(catalogId);
        getItemID().type(itemId);
        getItemName().type(itemName);
        getLoyaltyUnitCost().populate(loyaltyUnitCostMin, loyaltyUnitCostMax);
    }
}
