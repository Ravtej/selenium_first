package com.ihg.automation.selenium.servicecenter.pages.voucher;

import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.CustomContainerBase;
import com.ihg.automation.selenium.gwt.components.input.Select;

public class VoucherSearchFields extends CustomContainerBase
{
    public VoucherSearchFields(Container container)
    {
        super(container.getSeparator("Voucher Search"));
    }

    public Select getType()
    {
        return container.getSelect("Type");
    }
}
