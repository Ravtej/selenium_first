package com.ihg.automation.selenium.servicecenter.pages.event.order;

import com.ihg.automation.selenium.common.event.SearchAware;
import com.ihg.automation.selenium.common.event.SearchButtonBar;
import com.ihg.automation.selenium.common.pages.TabCustomContainerBase;
import com.ihg.automation.selenium.gwt.components.panel.Panel;
import com.ihg.automation.selenium.gwt.components.utils.ByText;
import com.ihg.automation.selenium.servicecenter.pages.Tabs;

public class OrderEventsPage extends TabCustomContainerBase implements SearchAware
{
    public OrderEventsPage()
    {
        super(new Tabs().getEvents().getOrder());
    }

    @Override
    public OrderSearch getSearchFields()
    {
        return new OrderSearch(container);
    }

    @Override
    public SearchButtonBar getButtonBar()
    {
        return new SearchButtonBar(container);
    }

    public Panel getOrdersPanel()
    {
        return container.getPanel(ByText.contains("Orders"));
    }

    public OrderGrid getOrdersGrid()
    {
        return new OrderGrid(getOrdersPanel());
    }
}
