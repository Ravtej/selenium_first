package com.ihg.automation.selenium.servicecenter.pages.event.stay;

import com.ihg.automation.selenium.gwt.components.CustomContainerBase;
import com.ihg.automation.selenium.gwt.components.label.Label;

public class StayInfoView extends StayInfoBase
{
    public StayInfoView(CustomContainerBase parent)
    {
        super(parent);
    }

    @Override
    public Label getCheckIn()
    {
        return container.getLabel(CHECK_IN_LABEL);
    }

    @Override
    public Label getCheckOut()
    {
        return container.getLabel(CHECK_OUT_LABEL);
    }

    @Override
    public Label getRoomNumber()
    {
        return container.getLabel(ROOM_NUMBER_LABEL);
    }

    @Override
    public Label getRoomType()
    {
        return container.getLabel(ROOM_TYPE_LABEL);
    }

    @Override
    public Label getRateCode()
    {
        return container.getLabel(RATE_CODE_LABEL);
    }

    @Override
    public Label getCorpAcctNumber()
    {
        return container.getLabel(CORP_ACCT_NUMBER_LABEL);
    }

    @Override
    public Label getIATANumber()
    {
        return container.getLabel(IATA_NUMBER_LABEL);
    }

    @Override
    public Label getEnrollingStay()
    {
        return container.getLabel(ENROLLING_STAY_LABEL);
    }

    @Override
    public Label getNights()
    {
        return container.getLabel(NIGHTS_LABEL);
    }

    @Override
    public Label getHotelCurrency()
    {
        return container.getLabel(HOTEL_CURRENCY_LABEL);
    }

    public Label getStayPayment()
    {
        return container.getLabel("Stay Payment");
    }
}
