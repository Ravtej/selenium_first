package com.ihg.automation.selenium.servicecenter.pages.event.level;

import com.ihg.automation.selenium.common.event.EventGridRowContainerWithDetails;
import com.ihg.automation.selenium.gwt.components.Container;

public class TierLevelGridRowContainer extends EventGridRowContainerWithDetails
{

    public TierLevelGridRowContainer(Container parent)
    {
        super(parent);
    }

    public TierLevelDetails getTierLevelDetails()
    {
        return new TierLevelDetails(container);
    }

}
