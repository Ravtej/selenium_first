package com.ihg.automation.selenium.servicecenter.pages.annual;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasTextAsInt;

import com.ihg.automation.selenium.common.member.AnnualActivity;
import com.ihg.automation.selenium.gwt.components.grid.CellsName;
import com.ihg.automation.selenium.gwt.components.grid.Grid;
import com.ihg.automation.selenium.gwt.components.grid.GridRow;
import com.ihg.automation.selenium.gwt.components.grid.GridRowXpath;

public class AnnualActivityRow extends GridRow<AnnualActivityRow.AnnualActivityCell>
{
    public AnnualActivityRow(Grid parent, GridRowXpath xpath)
    {
        super(parent, xpath);
    }

    public void verify(AnnualActivity activity)
    {

        verifyThat(getCell(AnnualActivityCell.TIER_LEVEL_NIGHT), hasTextAsInt(activity.getTierLevelNights()));
        verifyThat(getCell(AnnualActivityCell.REWARD_NIGHT), hasTextAsInt(activity.getRewardNights()));
        verifyThat(getCell(AnnualActivityCell.QUALIFIED_ROOM), hasTextAsInt(activity.getQualifiedNights()));
        verifyThat(getCell(AnnualActivityCell.NON_QUALIFIED_ROOM), hasTextAsInt(activity.getNonQualifiedNights()));
        verifyThat(getCell(AnnualActivityCell.QUALIFIED_CHAIN), hasTextAsInt(activity.getQualifiedChains()));
        verifyThat(getCell(AnnualActivityCell.TOTAL_QUALIFIED), hasTextAsInt(activity.getTotalQualifiedUnits()));
        verifyThat(getCell(AnnualActivityCell.TOTAL_UNIT), hasTextAsInt(activity.getTotalUnits()));
    }

    public enum AnnualActivityCell implements CellsName
    {
        YEAR("year"), TIER_LEVEL_NIGHT("tierLevelNights"), REWARD_NIGHT("rewardNights"), QUALIFIED_ROOM(
                "qualifiedNights"), NON_QUALIFIED_ROOM("nonQualifiedNights"), QUALIFIED_CHAIN("qualifiedChains"), TOTAL_QUALIFIED(
                "totalQualifiedUnits"), TOTAL_UNIT("totalUnits");

        private String name;

        private AnnualActivityCell(String name)
        {
            this.name = name;
        }

        @Override
        public String getName()
        {
            return name;
        }
    }
}
