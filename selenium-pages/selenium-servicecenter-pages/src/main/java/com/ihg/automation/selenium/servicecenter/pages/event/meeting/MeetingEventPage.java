package com.ihg.automation.selenium.servicecenter.pages.event.meeting;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static org.hamcrest.core.StringContains.containsString;

import com.ihg.automation.selenium.common.event.SearchAware;
import com.ihg.automation.selenium.common.event.SearchButtonBar;
import com.ihg.automation.selenium.common.pages.TabCustomContainerBase;
import com.ihg.automation.selenium.gwt.components.Button;
import com.ihg.automation.selenium.gwt.components.panel.Panel;
import com.ihg.automation.selenium.gwt.components.utils.ByText;
import com.ihg.automation.selenium.servicecenter.pages.Tabs;

public class MeetingEventPage extends TabCustomContainerBase implements SearchAware
{
    public MeetingEventPage()
    {
        super(new Tabs().getEvents().getMeeting());
    }

    @Override
    public MeetingSearch getSearchFields()
    {
        return new MeetingSearch(container);
    }

    @Override
    public SearchButtonBar getButtonBar()
    {
        return new SearchButtonBar(container);
    }

    public Button getCreateMeetingBonus()
    {
        return container.getButton("Create Meeting Bonus");
    }

    public Button getCreateMeeting()
    {
        return container.getButton("Create Meeting");
    }

    private Panel getGridPanel()
    {
        return container.getPanel(ByText.contains("Meetings"));
    }

    public void verifyGridPanelText(String fullName)
    {
        verifyThat(getGridPanel(), hasText(containsString(fullName)));
    }

    public MeetingEventGrid getGrid()
    {
        return new MeetingEventGrid(getGridPanel());
    }
}
