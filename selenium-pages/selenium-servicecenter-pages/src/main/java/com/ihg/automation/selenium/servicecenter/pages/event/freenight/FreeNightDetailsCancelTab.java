package com.ihg.automation.selenium.servicecenter.pages.event.freenight;

import com.ihg.automation.selenium.common.EventSource;
import com.ihg.automation.selenium.common.EventSourceAware;
import com.ihg.automation.selenium.common.event.EventSourceExtended;
import com.ihg.automation.selenium.common.pages.PopUpBase;
import com.ihg.automation.selenium.common.pages.TabCustomContainerBase;
import com.ihg.automation.selenium.gwt.components.input.DateInput;
import com.ihg.automation.selenium.gwt.components.input.Input;
import com.ihg.automation.selenium.gwt.components.input.SearchInput;
import com.ihg.automation.selenium.gwt.components.label.Label;
import com.ihg.automation.selenium.gwt.components.panel.Panel;

public class FreeNightDetailsCancelTab extends TabCustomContainerBase implements EventSourceAware
{
    public FreeNightDetailsCancelTab(PopUpBase parent)
    {
        super(parent, "Free Night Details");
    }

    public Label getConfirmationNumber()
    {
        return container.getLabel("Confirmation Number");
    }

    public Label getTransactionDate()
    {
        return container.getLabel("Transaction Date");
    }

    public Label getTransactionType()
    {
        return container.getLabel("Transaction Type");
    }

    public Label getCertificateNumber()
    {
        return container.getLabel("Certificate Number");
    }

    public Label getTotalFreeNights()
    {
        return container.getLabel("Total Free Nights");
    }

    public SearchInput getHotel()
    {
        return new SearchInput(container.getLabel("Hotel"));
    }

    public Input getNumberOfRooms()
    {
        return container.getInputByLabel("Number of Rooms");
    }

    public DateInput getCheckInDate()
    {
        return new DateInput(container.getLabel("Check In"));
    }

    public DateInput getCheckOutDate()
    {
        return new DateInput(container.getLabel("Check Out"));
    }

    public Input getNights()
    {
        return container.getInputByLabel("Nights");
    }

    public Panel getFreeNightReimbursementDetailPanel()
    {
        return container.getPanel("Free Night Reimbursement Detail");
    }

    public FreeNightReimbursementDetailGrid getFreeNightReimbursementDetailGrid()
    {
        return new FreeNightReimbursementDetailGrid(getFreeNightReimbursementDetailPanel());
    }

    @Override
    public EventSource getSource()
    {
        return new EventSourceExtended(container);
    }
}
