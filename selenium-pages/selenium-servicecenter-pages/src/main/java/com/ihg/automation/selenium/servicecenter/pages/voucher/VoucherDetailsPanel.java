package com.ihg.automation.selenium.servicecenter.pages.voucher;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasTextWithWait;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;

import com.ihg.automation.selenium.common.order.Voucher;
import com.ihg.automation.selenium.common.pages.Verify;
import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.CustomContainerBase;
import com.ihg.automation.selenium.gwt.components.Link;
import com.ihg.automation.selenium.gwt.components.label.Label;

public class VoucherDetailsPanel extends CustomContainerBase implements Verify<Voucher>
{
    public VoucherDetailsPanel(Container container)
    {
        super(container.getSeparator("Voucher Details"));
    }

    public Label getStatus()
    {
        return container.getLabel("Status");
    }

    public Link getMember()
    {
        return new Link(container.getLabel("Member"), Link.LINK_XPATH_NO_HYPER_LINK_IN_CLASS);
    }

    public Label getVoucherNumber()
    {
        return container.getLabel("Voucher number");
    }

    public Label getItemName()
    {
        return container.getLabel("Name of Item");
    }

    @Override
    public void verify(Voucher voucher)
    {
        verifyThat(getStatus(), hasTextWithWait(voucher.getStatus()));
        verifyThat(getMember(), hasText(voucher.getMemberName()));
        verifyThat(getVoucherNumber(), hasText(voucher.getVoucherNumber()));
        verifyThat(getItemName(), hasText(voucher.getItemName()));
    }
}
