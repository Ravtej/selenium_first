package com.ihg.automation.selenium.servicecenter.pages.voucher;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;

import com.ihg.automation.selenium.common.order.VoucherRule;
import com.ihg.automation.selenium.common.pages.Verify;
import com.ihg.automation.selenium.gwt.components.grid.CellsName;
import com.ihg.automation.selenium.gwt.components.grid.Grid;
import com.ihg.automation.selenium.gwt.components.grid.GridRow;
import com.ihg.automation.selenium.gwt.components.grid.GridRowXpath;

public class VoucherGridRow extends GridRow<VoucherGridRow.VoucherCell> implements Verify<VoucherRule>
{
    public VoucherGridRow(Grid parent, GridRowXpath xpath)
    {
        super(parent, xpath);
    }

    @Override
    public void verify(VoucherRule voucherRule)
    {
        verifyThat(getCell(VoucherCell.ID), hasText(voucherRule.getPromotionId()));
        verifyThat(getCell(VoucherCell.NAME), hasText(voucherRule.getOrderDescription()));
        verifyThat(getCell(VoucherCell.TYPE), hasText(voucherRule.getRuleType()));
        verifyThat(getCell(VoucherCell.AMOUNT), hasText(voucherRule.getUnitsAmount()));
        verifyThat(getCell(VoucherCell.TIER_LEVEL), hasText(voucherRule.getTierLevelCodeOfUpgrade()));
        verifyThat(getCell(VoucherCell.COST), hasText(voucherRule.getCostAmountWithCurrency()));
    }

    public void clickById()
    {
        getCell(VoucherCell.ID).asLink().clickAndWait();
    }

    public enum VoucherCell implements CellsName
    {
        ID("voucherId"), NAME("orderDescription"), TYPE("voucherRuleType"), AMOUNT("amountOfUnits"), TIER_LEVEL(
                "tierLevelOfUpgrade"), COST("costPerVoucher");

        private String name;

        private VoucherCell(String name)
        {
            this.name = name;
        }

        @Override
        public String getName()
        {
            return name;
        }
    }
}
