package com.ihg.automation.selenium.servicecenter.pages.programs;

import com.ihg.automation.selenium.common.types.program.Program;

public abstract class SimpleProgramPageBase extends ProgramPageBase
{
    public SimpleProgramPageBase(Program program)
    {
        super(program);
    }

    @Override
    public ProgramSummary getSummary()
    {
        return new ProgramSummary(container);
    }

}
