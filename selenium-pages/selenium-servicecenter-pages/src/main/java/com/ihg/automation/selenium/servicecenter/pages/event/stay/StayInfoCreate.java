package com.ihg.automation.selenium.servicecenter.pages.event.stay;

import com.ihg.automation.selenium.common.stay.Stay;
import com.ihg.automation.selenium.gwt.components.CustomContainerBase;
import com.ihg.automation.selenium.gwt.components.input.SearchInput;

public class StayInfoCreate extends StayInfoEditBase
{
    public StayInfoCreate(CustomContainerBase parent)
    {
        super(parent);
    }

    @Override
    public SearchInput getHotel()
    {
        return new SearchInput(container.getLabel(HOTEL_LABEL));
    }

    @Override
    public void populate(Stay stay)
    {
        getHotel().typeAndWait(stay.getHotelCode());

        super.populate(stay);
    }
}
