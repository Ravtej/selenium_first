package com.ihg.automation.selenium.servicecenter.pages.order;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static org.hamcrest.core.Is.is;

import java.text.MessageFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.openqa.selenium.WebElement;

import com.ihg.automation.selenium.common.contact.Address;
import com.ihg.automation.selenium.common.contact.Email;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.name.PersonNameCustomerFields;
import com.ihg.automation.selenium.common.name.PersonNameFields;
import com.ihg.automation.selenium.common.pages.PopUpBase;
import com.ihg.automation.selenium.formatter.Converter;
import com.ihg.automation.selenium.gwt.components.Axis;
import com.ihg.automation.selenium.gwt.components.Button;
import com.ihg.automation.selenium.gwt.components.Component;
import com.ihg.automation.selenium.gwt.components.Mode;
import com.ihg.automation.selenium.gwt.components.checkbox.CheckBox;
import com.ihg.automation.selenium.gwt.components.input.Select;
import com.ihg.automation.selenium.gwt.components.label.Label;
import com.ihg.automation.selenium.gwt.components.utils.ByText;
import com.ihg.automation.selenium.listener.Verifier;
import com.ihg.automation.selenium.runtime.SeleniumException;

public class OrderSummaryPopUp extends PopUpBase
{
    private static final String FIELDSET_PATTERN = ".//fieldset//following-sibling::div[{0}]/following-sibling::*[2]";
    private static final String NAME_PATTERN = ".//div[{0}]/following-sibling::*[1]";

    public OrderSummaryPopUp()
    {
        super("Order Summary");
    }

    public ItemRow getRow(int index)
    {
        return new ItemRow(container, index);
    }

    protected List<WebElement> getRows()
    {
        return findElements(Axis.DESCENDANT.getXpathLocator(".//div[3]/div"), "Rows list");
    }

    public int getRowCount()
    {
        return getRows().size();
    }

    public Label getLoyaltyUnitsAvailable()
    {
        return container.getLabel("Loyalty Units Available");
    }

    public Label getLoyaltyUnitsToBeRedeemed()
    {
        return container.getLabel("Total Loyalty Units to Be Redeemed");
    }

    public Label getLoyaltyUnitsAfterRedemption()
    {
        return container.getLabel("Loyalty Unit Balance After Redemption");
    }

    public PersonNameFields getName()
    {
        return new PersonNameCustomerFields(container.getSeparator("Name", NAME_PATTERN));
    }

    public Address getAddress()
    {
        return new Address(container.getSeparator("Address", FIELDSET_PATTERN));
    }

    public Select getUseExisting()
    {
        return container.getSelectByLabel("Use Existing");
    }

    public CheckBox getSaveAddress()
    {
        return new CheckBox(container, ByText.exact("Save Address"), CheckBox.CHECK_BOX_PATTERN2);
    }

    public Select getSelectEmail()
    {
        return container.getSelectByLabel("Email");
    }

    public Email getEmail()
    {
        return new Email(container.getSeparator("Email", FIELDSET_PATTERN));
    }

    public CheckBox getSaveEmail()
    {
        return new CheckBox(container, ByText.exact("Save Email"), CheckBox.CHECK_BOX_PATTERN2);
    }

    public Button getContinueShopping()
    {
        return container.getButton("Continue Shopping");
    }

    public void clickContinueShopping()
    {
        getContinueShopping().clickAndWait();
    }

    public Button getCheckout()
    {
        return container.getButton("Checkout");
    }

    public void clickCheckout(Verifier... verifiers)
    {
        getCheckout().clickAndWait(verifiers);
    }

    public Button getRemove()
    {
        return container.getButton("Remove");
    }

    public void clickRemove()
    {
        getRemove().clickAndWait();
    }

    public void verify(Member member, int availablePoints)
    {
        int totalAmount = 0;
        Label labelLoyaltyUnitsAvailable = getLoyaltyUnitsAvailable();
        Label labelLoyaltyUnitsToBeRedeemed = getLoyaltyUnitsToBeRedeemed();
        Label labelLoyaltyUnitsAfterRedemption = getLoyaltyUnitsAfterRedemption();

        int loyaltyUnitsAvailable = getPoints(labelLoyaltyUnitsAvailable);
        int loyaltyUnitsToBeRedeemed = getPoints(labelLoyaltyUnitsToBeRedeemed);
        int loyaltyUnitsAfterRedemption = getPoints(labelLoyaltyUnitsAfterRedemption);

        for (int i = 1; i <= getRowCount(); i++)
        {
            totalAmount += Converter.stringToInteger(getRow(i).getTotalAmount().getText());
        }

        verifyThat(labelLoyaltyUnitsAvailable, loyaltyUnitsAvailable, is(availablePoints),
                "Assert that LoyaltyUnitsAvailable is equals to RC Points");

        verifyThat(labelLoyaltyUnitsToBeRedeemed, loyaltyUnitsToBeRedeemed, is(-totalAmount),
                "Assert that LoyaltyUnitsToBeRedeemed is equals to minus TotalAmount");

        verifyThat(labelLoyaltyUnitsAfterRedemption, loyaltyUnitsAfterRedemption,
                is(loyaltyUnitsAvailable + loyaltyUnitsToBeRedeemed),
                "Assert that Loyalty Units After Redemption is equals to LoyaltyUnitsAvailable plus LoyaltyUnitsToBeRedeemed");

        getName().verify(member.getName(), Mode.EDIT);
        getAddress().verify(member.getPreferredAddress(), Mode.EDIT);
    }

    public int getPoints(Component component)
    {
        Pattern pattern = Pattern.compile("([-0-9,]+) RC Points");
        Matcher matcher = pattern.matcher(component.getText());
        if (matcher.matches())
        {
            try
            {
                return NumberFormat.getInstance(Locale.US).parse(matcher.group(1)).intValue();
            }
            catch (ParseException e)
            {
                return 0;
            }
        }
        throw new SeleniumException(
                MessageFormat.format("RC POINTS balance can not be identified for {0}", getDescription()),
                getElement());
    }
}
