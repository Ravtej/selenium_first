package com.ihg.automation.selenium.servicecenter.pages.search;

import com.ihg.automation.selenium.gwt.components.grid.Grid;

public class StaySearchGrid extends Grid<StaySearchGridRow, StaySearchGridCell>
{
    public StaySearchGrid()
    {
        super("Stay Details");
    }

}
