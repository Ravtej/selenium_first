package com.ihg.automation.selenium.servicecenter.pages.hotel.certificate;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;

import org.hamcrest.Matcher;

import com.ihg.automation.selenium.common.hotel.HotelCertificate;
import com.ihg.automation.selenium.common.types.Currency;
import com.ihg.automation.selenium.gwt.components.Component;
import com.ihg.automation.selenium.gwt.components.Componentable;
import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.CustomContainerBase;
import com.ihg.automation.selenium.gwt.components.label.Label;

public abstract class HotelCertificateDetailsBase extends CustomContainerBase
{
    protected final static String HOTEL = "Hotel";
    protected final static String CONFIRMATION_NUMBER = "Confirmation Number";
    protected final static String FOLIO_NUMBER = "Folio Number";
    protected final static String CHECK_IN = "Check In";
    protected final static String CHECK_OUT = "Check Out";
    protected final static String ITEM_ID = "Item Id";
    protected final static String BUNDLE = "Bundle";
    protected final static String DESCRIPTION_GUEST_NAME = "Description/Guest Name";
    protected final static String REIMBURSEMENT_AMOUNT = "Reimbursement Amount";
    protected final static String BILLING_ENTITY = "Billing Entity";
    protected final static String ITEM_NAME = "Item Name";
    protected final static String STATUS = "Status";
    protected final static String CERTIFICATE_NUMBER = "Certificate Number";
    protected final static String CURRENCY = "Currency";

    public HotelCertificateDetailsBase(Container container)
    {
        super(container);
    }

    abstract public Component getHotel();

    abstract public Component getConfirmationNumber();

    abstract public Component getFolioNumber();

    abstract public Component getCheckIn();

    abstract public Component getCheckOut();

    abstract public Component getItemId();

    abstract public Component getBundle();

    abstract public Component getDescriptionGuestName();

    abstract public Component getCertificateNumber();

    abstract public Component getReimbursementAmount();

    abstract public Component getBillingEntity();

    abstract public Component getCurrency();

    abstract public Matcher<Componentable> getCurrencyMatcher(Currency currency);

    public Label getItemName()
    {
        return container.getLabel(ITEM_NAME);
    }

    public Label getStatus()
    {
        return container.getLabel(STATUS);
    }

    public void verify(HotelCertificate detail)
    {
        verifyThat(getHotel(), hasText(detail.getHotelCode()));
        verifyThat(getConfirmationNumber(), hasText(detail.getConfirmationNumber()));
        verifyThat(getFolioNumber(), hasText(detail.getFolioNumber()));
        verifyThat(getCheckIn(), hasText(detail.getCheckInDate()));
        verifyThat(getCheckOut(), hasText(detail.getCheckOutDate()));
        verifyThat(getItemId(), hasText(detail.getItemId()));
        verifyThat(getItemName(), hasText(detail.getItemName()));
        verifyThat(getBundle(), hasText(detail.getBundleNumber()));
        verifyThat(getDescriptionGuestName(), hasText(detail.getDescriptionGuestName()));
        verifyThat(getCertificateNumber(), hasText(detail.getCertificateNumber()));
        verifyThat(getReimbursementAmount(), hasText(detail.getReimbursementAmount()));
        verifyThat(getCurrency(), getCurrencyMatcher(detail.getCurrency()));
        verifyThat(getBillingEntity(), hasText(detail.getBillingEntity()));

        verifyThat(getStatus(), hasText(detail.getStatus()));
    }
}
