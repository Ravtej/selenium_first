package com.ihg.automation.selenium.servicecenter.pages.event.offer;

import com.ihg.automation.selenium.gwt.components.Button;
import com.ihg.automation.selenium.listener.Verifier;

public class OfferRegistrationPopUp extends OfferPopUpBase
{

    public OfferRegistrationPopUp()
    {
        super();
    }

    @Override
    public OfferDetailsTab getOfferDetailsTab()
    {
        return new OfferDetailsTab(this);
    }

    public Button getRegister()
    {
        return container.getButton("Register");
    }

    public void clickRegister(Verifier... verifiers)
    {
        getRegister().clickAndWait(verifiers);
    }
}
