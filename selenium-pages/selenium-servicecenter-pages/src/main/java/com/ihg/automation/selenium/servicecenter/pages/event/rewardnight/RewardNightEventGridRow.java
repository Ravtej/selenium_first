package com.ihg.automation.selenium.servicecenter.pages.event.rewardnight;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;

import com.ihg.automation.selenium.gwt.components.grid.Grid;
import com.ihg.automation.selenium.gwt.components.grid.GridRow;
import com.ihg.automation.selenium.gwt.components.grid.GridRowXpath;
import com.ihg.automation.selenium.servicecenter.pages.event.rewardnight.RewardNightEventGrid.RewardNightCell;

public class RewardNightEventGridRow extends GridRow<RewardNightCell>
{
    public RewardNightEventGridRow(Grid parent, GridRowXpath xpath)
    {
        super(parent, xpath);
    }

    public void verify(RewardNightEventRow eventRow)
    {
        verifyThat(getCell(RewardNightCell.TRANS_DATE), hasText(eventRow.getTransactionDate()));
        verifyThat(getCell(RewardNightCell.TRANS_TYPE), hasText(eventRow.getTransactionType()));
        verifyThat(getCell(RewardNightCell.HOTEL), hasText(eventRow.getHotel()));
        verifyThat(getCell(RewardNightCell.CHECK_IN_DATE), hasText(eventRow.getCheckInDate()));
        verifyThat(getCell(RewardNightCell.CHECK_OUT_DATE), hasText(eventRow.getCheckOutDate()));
        verifyThat(getCell(RewardNightCell.NTS), hasText(eventRow.getNights()));
        verifyThat(getCell(RewardNightCell.CERTIFICATE_NUMBER), hasText(eventRow.getCertificateNumber()));
        verifyThat(getCell(RewardNightCell.IHG_UNITS), hasText(eventRow.getIhgUnits()));
    }

}
