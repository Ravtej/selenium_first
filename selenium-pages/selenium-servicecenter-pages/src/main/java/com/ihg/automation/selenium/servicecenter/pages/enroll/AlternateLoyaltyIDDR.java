package com.ihg.automation.selenium.servicecenter.pages.enroll;

import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.CustomContainerBase;
import com.ihg.automation.selenium.gwt.components.input.Input;
import com.ihg.automation.selenium.gwt.components.input.Select;

public class AlternateLoyaltyIDDR extends CustomContainerBase
{
    public AlternateLoyaltyIDDR(Container container)
    {
        super(container);
    }

    public Select getType()
    {
        return container.getSelectByLabel("Type");
    }

    public Input getNumber()
    {
        return container.getInputByLabel("Number");
    }
}
