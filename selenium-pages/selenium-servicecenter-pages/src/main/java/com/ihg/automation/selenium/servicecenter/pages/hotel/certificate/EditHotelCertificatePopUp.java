package com.ihg.automation.selenium.servicecenter.pages.hotel.certificate;

import com.ihg.automation.selenium.common.hotel.HotelCertificate;
import com.ihg.automation.selenium.listener.Verifier;

public class EditHotelCertificatePopUp extends HotelCertificatePopUpBase
{
    public EditHotelCertificatePopUp()
    {
        super("Edit Hotel Certificate");
    }

    @Override
    public HotelCertificateDetailsEdit getDetails()
    {
        return new HotelCertificateDetailsEdit(container);
    }

    public void editCertificate(HotelCertificate details, Verifier... verifiers)
    {
        getDetails().populate(details);
        getSaveChanges().clickAndWait(verifiers);
    }

}
