package com.ihg.automation.selenium.servicecenter.pages;

import com.ihg.automation.selenium.common.SiteHelperBase;
import com.ihg.automation.selenium.common.UI;
import com.ihg.automation.selenium.common.User;
import com.ihg.automation.selenium.common.pages.login.RolePopUp;

public class ServiceCenterHelper extends SiteHelperBase
{
    public ServiceCenterHelper(String url)
    {
        super(url, UI.SC);
    }

    @Override
    protected void selectRole(User user, String role, boolean isRoleMandatory)
    {
        RolePopUp rolepopUp = new RolePopUp();
        if (isRoleMandatory || rolepopUp.isDisplayed())
        {
            rolepopUp.selectAndSubmit(role);
        }

    }
}
