package com.ihg.automation.selenium.servicecenter.pages.occupation;

import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.grid.Grid;

public class AdvancedCorporateAccountGrid
        extends Grid<AdvancedCorporateAccountGridRow, AdvancedCorporateAccountGridRow.AdvancedCorporateAccountGridCell>
{
    public AdvancedCorporateAccountGrid(Container parent)
    {
        super(parent);
    }

}
