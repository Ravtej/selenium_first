package com.ihg.automation.selenium.servicecenter.pages.account;

import com.ihg.automation.selenium.common.pages.MultiModeBase;
import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.DualList;

public class Flags extends MultiModeBase
{
    public Flags(Container parent)
    {
        super(parent.getSeparator("Profile Flags"));
    }

    public DualList getFlags()
    {
        return container.getDualList("Flags");
    }
}
