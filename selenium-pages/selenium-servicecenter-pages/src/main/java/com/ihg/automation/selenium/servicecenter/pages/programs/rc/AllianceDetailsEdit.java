package com.ihg.automation.selenium.servicecenter.pages.programs.rc;

import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.label.Label;

public class AllianceDetailsEdit extends AllianceDetailsBase
{
    public AllianceDetailsEdit(Container container)
    {
        super(container);
    }

    @Override
    public Label getAlliance()
    {
        return container.getLabel("Alliance");
    }
}
