package com.ihg.automation.selenium.servicecenter.pages.event.order;

import com.ihg.automation.selenium.common.event.EventGridRowContainerWithDetails;
import com.ihg.automation.selenium.common.order.Order;
import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.common.order.ShippingInfoBase;
import com.ihg.automation.selenium.common.order.ShippingInfoView;

public class OrderSystemDetailsGridRowContainer extends EventGridRowContainerWithDetails
{

    public OrderSystemDetailsGridRowContainer(Container parent)
    {
        super(parent);
    }

    public OrderDetailsView getOrderDetails()
    {
        return new OrderDetailsView(container);
    }

    public ShippingInfoBase getShippingInfo()
    {
        return new ShippingInfoView(container);
    }

    public void verify(Order order)
    {
        getOrderDetails().verify(order.getOrderItems().get(0));
        getShippingInfo().verify(order);
    }

    public void verifyWithShippingAsOnLeftPanel(Order order)
    {
        getOrderDetails().verify(order.getOrderItems().get(0));
        getShippingInfo().verifyAsOnCustomerInformation();
    }
}
