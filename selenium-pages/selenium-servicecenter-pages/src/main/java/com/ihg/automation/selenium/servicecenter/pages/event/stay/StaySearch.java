package com.ihg.automation.selenium.servicecenter.pages.event.stay;

import com.ihg.automation.selenium.common.components.CountrySelect;
import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.CustomContainerBase;
import com.ihg.automation.selenium.gwt.components.input.Input;
import com.ihg.automation.selenium.gwt.components.input.SearchInput;
import com.ihg.automation.selenium.gwt.components.input.Select;
import com.ihg.automation.selenium.servicecenter.pages.event.EventDateRange;

public class StaySearch extends CustomContainerBase
{
    public StaySearch(Container container)
    {
        super(container.getSeparator("Stay Search"));
    }

    public Input getGuestName()
    {
        return container.getInputByLabel("Guest Name");
    }

    public Input getConfirmationNumber()
    {
        return container.getInputByLabel("Confirmation Number");
    }

    public Input getCCNumber()
    {
        return container.getInputByLabel("CC Number");
    }

    public SearchInput getHotel()
    {
        return new SearchInput(container.getLabel("Hotel"));
    }

    public Select getHotelBrand()
    {
        return container.getSelectByLabel("Hotel Brand");
    }

    public CountrySelect getCountry()
    {
        return new CountrySelect(container);
    }

    public EventDateRange getStayDate()
    {
        return new EventDateRange(container, "Date of Stay");
    }

    public Input getCity()
    {
        return container.getInputByLabel("City");
    }

    public Select getState()
    {
        return container.getSelectByLabel("State/Province");
    }
}
