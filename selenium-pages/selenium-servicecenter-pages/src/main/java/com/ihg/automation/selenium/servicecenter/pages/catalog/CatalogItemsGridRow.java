package com.ihg.automation.selenium.servicecenter.pages.catalog;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasTextAsCollection;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasTextAsFormattedInt;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;

import com.ihg.automation.selenium.common.catalog.CatalogItem;
import com.ihg.automation.selenium.gwt.components.grid.CellsName;
import com.ihg.automation.selenium.gwt.components.grid.GridRow;
import com.ihg.automation.selenium.gwt.components.grid.GridRowXpath;

public class CatalogItemsGridRow extends GridRow<CatalogItemsGridRow.CatalogItemsGridCell>
{
    public CatalogItemsGridRow(CatalogItemsGrid parent, GridRowXpath xpath)
    {
        super(parent, xpath);
    }

    public void verify(CatalogItem catalogItem)
    {
        verifyThat(getCell(CatalogItemsGridCell.PROGRAM), hasText(catalogItem.getProgram()));
        verifyThat(getCell(CatalogItemsGridCell.REGION), hasTextAsCollection(catalogItem.getRegions()));
        verifyThat(getCell(CatalogItemsGridCell.CATALOG_ID), hasText(catalogItem.getCatalogId()));
        verifyThat(getCell(CatalogItemsGridCell.ITEM_ID), hasText(catalogItem.getItemId()));
        verifyThat(getCell(CatalogItemsGridCell.ITEM_NAME), hasText(catalogItem.getItemName()));
        verifyThat(getCell(CatalogItemsGridCell.END_DATE), hasText(catalogItem.getEndDate()));
        verifyThat(getCell(CatalogItemsGridCell.LOYALTY_UNIT_COST),
                hasTextAsFormattedInt(catalogItem.getLoyaltyUnitCost()));
    }

    public enum CatalogItemsGridCell implements CellsName
    {
        PROGRAM("program"), REGION("region"), CATALOG_ID("catalogId"), ITEM_ID("itemId"), ITEM_NAME("itemName"), END_DATE(
                "endDate"), LOYALTY_UNIT_COST("cost");

        private String name;

        private CatalogItemsGridCell(String name)
        {
            this.name = name;
        }

        @Override
        public String getName()
        {
            return name;
        }
    }
}
