package com.ihg.automation.selenium.servicecenter.pages.programs.rc;

import com.ihg.automation.selenium.common.pages.ClosablePopUpBase;
import com.ihg.automation.selenium.gwt.components.PopUpWindow;
import com.ihg.automation.selenium.gwt.components.panel.Panel;
import com.ihg.automation.selenium.gwt.components.utils.ByText;

public abstract class RedeemedPartnerBenefitPopUpBase extends ClosablePopUpBase
{
    private static final String BENEFITS = "Benefits ";
    private static final String POP_UP_HEADER = "Partner Benefit Details";
    private String bodyText;

    public RedeemedPartnerBenefitPopUpBase(String bodyText)
    {
        super(PopUpWindow.withHeaderAndPosition(POP_UP_HEADER, 2));
        this.bodyText = bodyText;
    }

    public Panel getPartnerBenefitsPanel()
    {
        return container.getPanel(ByText.startsWith(BENEFITS + bodyText));
    }

    public PartnerBenefitsRedeemedGrid getPartnerBenefitsGrid()
    {
        return new PartnerBenefitsRedeemedGrid(getPartnerBenefitsPanel());
    }
}
