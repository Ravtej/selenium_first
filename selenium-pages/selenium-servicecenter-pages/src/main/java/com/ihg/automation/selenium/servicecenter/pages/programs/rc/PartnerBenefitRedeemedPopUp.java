package com.ihg.automation.selenium.servicecenter.pages.programs.rc;

import static com.ihg.automation.common.DateUtils.DATE_FORMAT;
import static com.ihg.automation.common.DateUtils.now;

public class PartnerBenefitRedeemedPopUp extends RedeemedPartnerBenefitPopUpBase
{
    private static final String THE_BEGINNING_OF_CURRENT_YEAR = now().withMonthOfYear(1).withDayOfMonth(1)
            .toString(DATE_FORMAT);

    public PartnerBenefitRedeemedPopUp()
    {
        super(THE_BEGINNING_OF_CURRENT_YEAR);
    }
}
