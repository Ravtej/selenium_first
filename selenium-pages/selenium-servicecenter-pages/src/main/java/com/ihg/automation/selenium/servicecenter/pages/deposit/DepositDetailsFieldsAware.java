package com.ihg.automation.selenium.servicecenter.pages.deposit;

import com.ihg.automation.selenium.gwt.components.Component;
import com.ihg.automation.selenium.gwt.components.MultiModeComponent;
import com.ihg.automation.selenium.gwt.components.label.Label;

public interface DepositDetailsFieldsAware
{

    public static final String TRANSACTION_ID = "Transaction ID";
    public static final String HOTEL = "Hotel";
    public static final String CHECKIN_DATE = "Check-In Date";
    public static final String CHECKOUT_DATE = "Check-Out Date";
    public static final String TRANSACTION_DATE = "Partner Transaction Date";
    public static final String TRACKING_NUMBER = "Partner Order/Tracking Number";
    public static final String PARTNER_COMMENT = "Partner Comment";
    public static final String TRANSACTION_TYPE = "Transaction Type";
    public static final String TRANSACTION_NAME = "Transaction Name";
    public static final String TRANSACTION_DESCRIPTION = "Transaction Description";
    public static final String CUSTOMER_SERVICE_COMMENT = "Customer Service Comment";
    public static final String CUSTOMER_SERVICE_COMMENTS = "Customer Service Comments";
    public static final String REASON = "Reason";

    public MultiModeComponent getTransactionId();

    public Component getHotel();

    public MultiModeComponent getCheckInDate();

    public MultiModeComponent getCheckOutDate();

    public MultiModeComponent getPartnerTransactionDate();

    public MultiModeComponent getOrderTrackingNumber();

    public MultiModeComponent getPartnerComment();

    public Label getTransactionName();

    public Label getTransactionDescription();
}
