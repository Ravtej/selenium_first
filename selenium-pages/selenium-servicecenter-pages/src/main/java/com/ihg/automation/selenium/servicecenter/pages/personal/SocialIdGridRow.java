package com.ihg.automation.selenium.servicecenter.pages.personal;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;

import com.ihg.automation.selenium.gwt.components.grid.CellsName;
import com.ihg.automation.selenium.gwt.components.grid.GridRow;
import com.ihg.automation.selenium.gwt.components.grid.GridRowXpath;

public class SocialIdGridRow extends GridRow<SocialIdGridRow.SocialIdCell>
{
    public SocialIdGridRow(SocialIdGrid parent, GridRowXpath xpath)
    {
        super(parent, xpath);
    }

    public void clickDelete()
    {
        getCell(SocialIdCell.DELETE).asLink().clickAndWait();
    }

    public EditSocialIdPopUp getEditSocialIdPopUp()
    {
        getCell(SocialIdCell.TYPE).asLink().clickAndWait();

        return new EditSocialIdPopUp();
    }

    public void verify(String type, String name)
    {
        verifyThat(getCell(SocialIdCell.TYPE), hasText(type));
        verifyThat(getCell(SocialIdCell.USER_NAME), hasText(name));
        verifyThat(getCell(SocialIdCell.DELETE).asLink(), displayed(true));
        verifyThat(getCell(SocialIdCell.DELETE), hasText("Delete"));
    }

    public enum SocialIdCell implements CellsName
    {
        TYPE("memberType", "Type"), //
        USER_NAME("memberNumber", "Username/Handle"), //
        DELETE("delete", "");

        private String name;
        private String value;

        private SocialIdCell(String name, String value)
        {
            this.name = name;
            this.value = value;
        }

        public String getValue()
        {
            return value;
        }

        @Override
        public String getName()
        {
            return name;
        }
    }
}
