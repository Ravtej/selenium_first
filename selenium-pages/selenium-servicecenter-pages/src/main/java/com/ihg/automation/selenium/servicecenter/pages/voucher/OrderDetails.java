package com.ihg.automation.selenium.servicecenter.pages.voucher;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.Constant.NOT_AVAILABLE;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasDefault;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;

import com.ihg.automation.selenium.common.annual.ActivityAmount;
import com.ihg.automation.selenium.common.order.VoucherOrder;
import com.ihg.automation.selenium.common.payment.CurrencyAmount;
import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.CustomContainerBase;
import com.ihg.automation.selenium.gwt.components.Link;
import com.ihg.automation.selenium.gwt.components.label.Label;

public class OrderDetails extends CustomContainerBase
{
    public OrderDetails(Container container)
    {
        super(container);
    }

    public Link getItemID()
    {
        return new Link(container.getLabel("Item ID"), Link.LINK_XPATH_NO_HYPER_LINK_IN_CLASS);
    }

    public Label getItemName()
    {
        return container.getLabel("Name Of Item");
    }

    public ActivityAmount getLoyaltyUnits()
    {
        return new ActivityAmount(container.getLabel("Loyalty Units"));
    }

    public CurrencyAmount getCash()
    {
        return new CurrencyAmount(container.getLabel("Cash"));
    }

    public Label getDeliveryMethod()
    {
        return container.getLabel("Delivery Method");
    }

    public Label getDeliveryStatus()
    {
        return container.getLabel("DeliveryStatus");
    }

    public Label getEstimatedDeliveryDate()
    {
        return container.getLabel("Estimated Delivery Date");
    }

    public Label getActualDeliveryDate()
    {
        return container.getLabel("Actual Delivery Date");
    }

    public void verify(VoucherOrder voucherOrder)
    {
        verifyThat(getItemID(), hasText(voucherOrder.getPromotionId()));
        verifyThat(getItemName(), hasText(voucherOrder.getVoucherName()));

        getCash().verify(voucherOrder.getVoucherCostAmount(), voucherOrder.getCurrencyCode());
        getLoyaltyUnits().verify(NOT_AVAILABLE, null);

        verifyThat(getEstimatedDeliveryDate(), hasDefault());
        verifyThat(getActualDeliveryDate(), hasDefault());

        verifyThat(getDeliveryMethod(), hasDefault());
        verifyThat(getDeliveryStatus(), hasDefault());
    }
}
