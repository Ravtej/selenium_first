package com.ihg.automation.selenium.servicecenter.pages.event.enroll;

import com.ihg.automation.selenium.common.pages.TabCustomContainerBase;

public class MembershipDetailsTab extends TabCustomContainerBase
{
    public MembershipDetailsTab(MembershipDetailsPopUp parent)
    {
        super(parent, "Membership Details");
    }

    public MembershipDetails getMembershipDetails()
    {
        return new MembershipDetails(container);
    }
}
