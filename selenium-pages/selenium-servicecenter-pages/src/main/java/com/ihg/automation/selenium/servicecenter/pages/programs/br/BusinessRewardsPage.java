package com.ihg.automation.selenium.servicecenter.pages.programs.br;

import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.servicecenter.pages.programs.ProgramPageBase;

public class BusinessRewardsPage extends ProgramPageBase
{
    public BusinessRewardsPage()
    {
        super(Program.BR);
    }

    @Override
    public BusinessRewardsSummary getSummary()
    {
        return new BusinessRewardsSummary(container);
    }
}
