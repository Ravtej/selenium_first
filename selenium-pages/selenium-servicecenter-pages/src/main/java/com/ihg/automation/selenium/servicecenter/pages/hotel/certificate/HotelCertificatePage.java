package com.ihg.automation.selenium.servicecenter.pages.hotel.certificate;

import com.ihg.automation.selenium.common.event.SearchAware;
import com.ihg.automation.selenium.common.event.SearchButtonBar;
import com.ihg.automation.selenium.common.hotel.HotelCertificate;
import com.ihg.automation.selenium.common.pages.NavigatePage;
import com.ihg.automation.selenium.gwt.components.Button;
import com.ihg.automation.selenium.gwt.components.CustomContainerBase;
import com.ihg.automation.selenium.listener.Verifier;
import com.ihg.automation.selenium.servicecenter.pages.Tabs;
import com.ihg.automation.selenium.servicecenter.pages.TopMenu;

public class HotelCertificatePage extends CustomContainerBase implements NavigatePage, SearchAware
{
    public static final String CERTIFICATES_SUCCESSFULLY_ACCEPTED = "Certificates have been successfully accepted";
    public static final String CERTIFICATES_SUCCESSFULLY_CANCELED = "Certificates have been successfully canceled";
    public static final String RECORDS_CAN_NOT_BE_CANCELED = "There are no selected records that can be canceled";
    public static final String RECORDS_CAN_NOT_BE_ACCEPTED = "There are no selected records that can be accepted";

    TopMenu menu = new TopMenu();

    public HotelCertificatePage()
    {
        super(new Tabs().getReimbursement().getOtherCertificates().getContainer());
    }

    @Override
    public void goTo()
    {
        menu.getHotelOperations().clickAndWait();
        menu.getHotelOperations().getHotelAndOtherCertificates().clickAndWait();
    }

    @Override
    public HotelCertificateSearch getSearchFields()
    {
        return new HotelCertificateSearch(container);
    }

    public HotelCertificateSearchGrid getGrid()
    {
        return new HotelCertificateSearchGrid(container);
    }

    public void populateSearch(String hotel, String certificateNum, String confirmationNum, String itemId,
            String status, Boolean checkIn, Boolean checkOut, String fromDate, String toDate)
    {
        HotelCertificateSearch search = getSearchFields();
        search.getHotelCode().typeAndWait(hotel);
        search.getCertificateNumber().typeAndWait(certificateNum);
        search.getConfirmationNumber().typeAndWait(confirmationNum);
        search.getItemId().typeAndWait(itemId);
        search.getFromDate().typeAndWait(fromDate);
        search.getToDate().typeAndWait(toDate);
        search.getStatusOfRequest().select(status);
        search.getCheckIn().check(checkIn);
        search.getCheckOut().check(checkOut);
    }

    public void searchByHotelCode(String hotelCode, Verifier... verifiers)
    {
        HotelCertificateSearchButtonBar buttonBar = getButtonBar();
        buttonBar.getClearCriteria().clickAndWait();
        getSearchFields().getHotelCode().typeAndWait(hotelCode);
        buttonBar.getSearch().clickAndWait(verifiers);
    }

    public void search(HotelCertificate certificate)
    {
        search(certificate.getHotelCode(), certificate.getCertificateNumber(), certificate.getConfirmationNumber(),
                certificate.getItemId(), certificate.getStatus(), true, false, certificate.getCheckInDate(),
                certificate.getCheckOutDate());
    }

    public void search(String hotel, String certificateNum, String confirmationNum, String itemId, String status,
            Boolean checkIn, Boolean checkOut, String fromDate, String toDate)
    {
        HotelCertificateSearchButtonBar buttonBar = getButtonBar();
        buttonBar.getClearCriteria().clickAndWait();
        populateSearch(hotel, certificateNum, confirmationNum, itemId, status, checkIn, checkOut, fromDate, toDate);
        buttonBar.getSearch().clickAndWait();
    }

    public void createCertificate(HotelCertificate certificate)
    {
        getSearchFields().clickEnterCertificate().createCertificate(certificate);
    }

    public void createApprovedCertificate(HotelCertificate certificate)
    {
        getSearchFields().clickEnterCertificate().createApprovedCertificate(certificate);
    }

    public void cancelCertificate(HotelCertificateSearchGridRow row)
    {
        row.check();
        getButtonBar().clickCancelCertificate();
    }

    @Override
    public HotelCertificateSearchButtonBar getButtonBar()
    {
        return new HotelCertificateSearchButtonBar(this);
    }

    public class HotelCertificateSearchButtonBar extends SearchButtonBar
    {

        public HotelCertificateSearchButtonBar(HotelCertificatePage page)
        {
            super(page.container);
        }

        public Button getCancelCertificate()
        {
            return container.getButton("Cancel Certificate");
        }

        public void clickCancelCertificate(Verifier... verifiers)
        {
            getCancelCertificate().clickAndWait(verifiers);
        }

        public Button getAccept()
        {
            return container.getButton("Accept");
        }

        public void clickAccept(Verifier... verifiers)
        {
            getAccept().clickAndWait(verifiers);
        }

        public Button getEnterCertificate()
        {
            return container.getButton("Enter Certificate");
        }

        public void clickEnterCertificate()
        {
            getEnterCertificate().clickAndWait();
        }
    }
}
