package com.ihg.automation.selenium.servicecenter.pages.voucher;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasTextWithWait;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;

import com.ihg.automation.selenium.common.order.Voucher;
import com.ihg.automation.selenium.common.pages.Verify;
import com.ihg.automation.selenium.gwt.components.grid.CellsName;
import com.ihg.automation.selenium.gwt.components.grid.Grid;
import com.ihg.automation.selenium.gwt.components.grid.GridRow;
import com.ihg.automation.selenium.gwt.components.grid.GridRowXpath;

public class VoucherStatusGridRow extends GridRow<VoucherStatusGridRow.VoucherStatusCell> implements Verify<Voucher>
{
    public VoucherStatusGridRow(Grid parent, GridRowXpath xpath)
    {
        super(parent, xpath);
    }

    @Override
    public void verify(Voucher voucher)
    {
        verifyThat(getCell(VoucherStatusCell.TRANS_DATE), hasText(voucher.getUpdatedDate()));
        verifyThat(getCell(VoucherStatusCell.STATUS), hasTextWithWait(voucher.getStatus()));
        verifyThat(getCell(VoucherStatusCell.UPDATED_BY), hasText(voucher.getUpdatedBy()));
    }

    public enum VoucherStatusCell implements CellsName
    {
        TRANS_DATE("updatedTimeStamp"), STATUS("status"), UPDATED_BY("updatedBy"), DEPARTMENT("department"), LOCATION(
                "location");

        private String name;

        private VoucherStatusCell(String name)
        {
            this.name = name;
        }

        @Override
        public String getName()
        {
            return name;
        }
    }
}
