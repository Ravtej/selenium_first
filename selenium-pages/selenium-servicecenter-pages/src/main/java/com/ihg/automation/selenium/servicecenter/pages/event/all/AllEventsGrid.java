package com.ihg.automation.selenium.servicecenter.pages.event.all;

import static com.ihg.automation.selenium.common.event.EventTransactionType.Membership.ENROLL;
import static com.ihg.automation.selenium.common.event.EventTransactionType.Profile.REGION_TRANSFER;

import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.grid.Grid;
import com.ihg.automation.selenium.gwt.components.grid.GridRowXpath;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsGridRow.AllEventsCell;

public class AllEventsGrid extends Grid<AllEventsGridRow, AllEventsCell>
{
    public AllEventsGrid(Container parent)
    {
        super(parent);
    }

    public AllEventsGridRow getRow(String transType, String detail)
    {
        AllEventsGridRow row;
        if (null == detail)
        {
            row = getRow(transType);
        }
        else
        {
            row = getRow(new GridRowXpath.GridRowXpathBuilder().cell(AllEventsCell.TRANS_TYPE, transType)
                    .cell(AllEventsCell.DETAIL, detail).build());
        }

        return row;
    }

    public AllEventsGridRow getRow(String transType)
    {
        return getRow(new GridRowXpath.GridRowXpathBuilder().cell(AllEventsCell.TRANS_TYPE, transType).build());
    }

    public AllEventsGridRow getEnrollRow(Program programCode)
    {
        return getRow(ENROLL, programCode.getCode());
    }

    public AllEventsGridRow getRegionTransferRow(String detail)
    {
        return getRow(REGION_TRANSFER, detail);
    }

    public AllEventsGridRow getRegionTransferRow()
    {
        return getRow(REGION_TRANSFER);
    }

}
