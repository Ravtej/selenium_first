package com.ihg.automation.selenium.servicecenter.pages.event.deposit;

public interface DepositDetailsAware
{
    public DepositDetails getDepositDetails();
}
