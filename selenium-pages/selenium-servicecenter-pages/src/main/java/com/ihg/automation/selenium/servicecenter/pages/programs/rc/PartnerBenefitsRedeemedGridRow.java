package com.ihg.automation.selenium.servicecenter.pages.programs.rc;

import static com.ihg.automation.common.DateUtils.getFormattedDate;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;

import com.ihg.automation.selenium.gwt.components.grid.CellsName;
import com.ihg.automation.selenium.gwt.components.grid.GridRow;
import com.ihg.automation.selenium.gwt.components.grid.GridRowXpath;

public class PartnerBenefitsRedeemedGridRow extends GridRow<PartnerBenefitsRedeemedGridRow.PartnerBenefitsRedeemedCell>
{
    public PartnerBenefitsRedeemedGridRow(PartnerBenefitsRedeemedGrid parent, GridRowXpath xpath)
    {
        super(parent, xpath);
    }

    public void verify(String benefitName)
    {
        verifyThat(getCell(PartnerBenefitsRedeemedCell.NAME), hasText(benefitName));
        verifyThat(getCell(PartnerBenefitsRedeemedCell.REDEEMED), hasText(getFormattedDate()));
    }

    public enum PartnerBenefitsRedeemedCell implements CellsName
    {
        NAME("partnerId", "Name"), //
        REDEEMED("redeemedDate", "Redeemed");

        private String name;
        private String value;

        private PartnerBenefitsRedeemedCell(String name, String value)
        {
            this.name = name;
            this.value = value;
        }

        public String getValue()
        {
            return value;
        }

        @Override
        public String getName()
        {
            return name;
        }
    }
}
