package com.ihg.automation.selenium.servicecenter.pages.event.benefit;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;

import com.ihg.automation.selenium.common.Constant;
import com.ihg.automation.selenium.common.EventSourceAware;
import com.ihg.automation.selenium.common.SiteHelperBase;
import com.ihg.automation.selenium.common.catalog.CatalogItem;
import com.ihg.automation.selenium.common.event.EventSourceExtended;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.gwt.components.Component;
import com.ihg.automation.selenium.gwt.components.ComponentColumnPosition;
import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.CustomContainerBase;
import com.ihg.automation.selenium.gwt.components.label.Label;

public class BenefitRedeemDetails extends CustomContainerBase implements EventSourceAware
{
    private static final ComponentColumnPosition POSITION = ComponentColumnPosition.FOLLOWING_TD;

    public BenefitRedeemDetails(Container container)
    {
        super(container);
    }

    public Label getItemDescription()
    {
        return container.getLabel("Item Description");
    }

    public Component getReceivingGiftingMemberId()
    {
        return container.getLabel("Receiving Member").getComponent("Member Id", 2, POSITION);
    }

    public Component getReceivingGiftingMemberName()
    {
        return container.getLabel("Receiving Member").getComponent("Member Name", 3, POSITION);
    }

    @Override
    public EventSourceExtended getSource()
    {
        return new EventSourceExtended(container);
    }

    public void verifyForLevelBenefit(CatalogItem catalogItem, Member member, SiteHelperBase siteHelper)
    {
        verifyBasic(catalogItem, siteHelper);
        verifyThat(getReceivingGiftingMemberId(), hasText(member.getRCProgramId()));
        verifyThat(getReceivingGiftingMemberName(), hasText(member.getName().getFullName()));
    }

    public void verifyForPointsBenefit(CatalogItem catalogItem, SiteHelperBase siteHelper)
    {
        verifyBasic(catalogItem, siteHelper);
        verifyThat(getReceivingGiftingMemberId(), hasText(Constant.NOT_AVAILABLE));
    }

    private void verifyBasic(CatalogItem catalogItem, SiteHelperBase siteHelper)
    {
        verifyThat(getItemDescription(), hasText(catalogItem.getItemDescription()));
        getSource().verify(siteHelper.getSourceFactory().getNoEmployeeIdSource());
    }

}
