package com.ihg.automation.selenium.servicecenter.pages.programs;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.enabled;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static com.ihg.automation.selenium.matchers.component.ViewableMatcher.hasTextInView;

import com.ihg.automation.selenium.common.member.AmbassadorUtils;
import com.ihg.automation.selenium.gwt.components.Button;
import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.label.Label;
import com.ihg.automation.selenium.servicecenter.pages.programs.amb.ExtendMembershipPopUp;

public class RenewExtendProgramSummary extends ProgramSummary
{

    public RenewExtendProgramSummary(Container container)
    {
        super(container);
    }

    public Label getExpirationDate()
    {
        return container.getLabel("Expiration Date");
    }

    public Button getRenew()
    {
        return container.getButton("Renew");
    }

    public void clickRenew()
    {
        getRenew().clickAndWait();
    }

    public Button getExtend()
    {
        return container.getButton("Extend");
    }

    public void clickExtend()
    {
        getExtend().clickAndWait();
    }

    public void extend(int month, String reasonCode)
    {
        clickExtend();
        new ExtendMembershipPopUp().extend(month, reasonCode);
    }

    public void verifyRenewAndExtendButtons(Boolean isRenew, Boolean isExtend)
    {
        verifyThat(getRenew(), enabled(isRenew));
        verifyThat(getExtend(), enabled(isExtend));
    }

    public void verifyStatus(String status, String statusReason)
    {
        verifyThat(getStatus(), hasTextInView(status));
        if (statusReason == null)
        {
            verifyThat(getStatusReason(), displayed(false));
        }
        else
        {
            verifyThat(getStatusReason(), hasTextInView(statusReason));
        }
    }

    public void verifyStatus(String status)
    {
        verifyStatus(status, null);
    }

    public void verifyExpDateAfterRenew(int daysShift)
    {
        verifyThat(getExpirationDate(), hasText(AmbassadorUtils.getExpirationDateAfterRenew(daysShift)));
    }
}
