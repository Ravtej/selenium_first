package com.ihg.automation.selenium.servicecenter.pages;

import static com.ihg.automation.selenium.common.Constant.LINE_SEPARATOR;
import static java.text.MessageFormat.format;

public class Messages
{
    private static final String AND = " and ";
    public static final String CONTACT_SUCCESS_SAVE = "Contact info has been successfully saved";
    public static final String CONTACT_SUCCESS_UPDATE = CONTACT_SUCCESS_SAVE + ".";
    public static final String UPDATE_INFO_AT_TAB = "Please update {0} at {1} tab.";
    public static final String CONCURRENT_CONTACT_EDIT_ERROR = "Please complete editing or wait till save operation completed";

    public static final String CUSTOMER_SERVICE_PREFERENCES = "Customer Service Preferences";
    public static final String MARKETING_PREFERENCES = "Marketing Preferences";

    public static final String DELETED_TEMPLATE = "Deleted contact was used in {0}.";
    public static final String UPDATED_TEMPLATE = "Updated contact was used in {0}.";
    public static final String PREFERRED_TEMPLATE = "Previous preferred contact was used in {0}.";
    public static final String EMAIL_ADDRESS_ALREADY_EXISTS = "Email address already exists, try another email address";
    public static final String HIGHLIGHTED_FIELDS_DID_NOT_PASS_VALIDATION = "Highlighted fields did not pass validation. Move your mouse over each field for details and fix entered data to proceed.";
    public static final String EXCEEDS_THE_LIMIT = "The number of returned results exceeds the limit. Displaying the first 10 results.";

    public static String getSimpleSuccess()
    {
        return CONTACT_SUCCESS_SAVE;
    }

    public static String getUpdatedMessage(boolean usedMarketing, boolean usedCustomer)
    {
        return getMessage(UPDATED_TEMPLATE, usedMarketing, usedCustomer);
    }

    public static String getDeletedMessage(boolean usedMarketing, boolean usedCustomer)
    {
        return getMessage(DELETED_TEMPLATE, usedMarketing, usedCustomer);
    }

    public static String getPreferredMessage(boolean usedMarketing, boolean usedCustomer)
    {
        return getMessage(PREFERRED_TEMPLATE, usedMarketing, usedCustomer);
    }

    private static String getMessage(String template, boolean usedMarketing, boolean usedCustomer)
    {
        StringBuilder result = new StringBuilder();
        result.append(CONTACT_SUCCESS_SAVE).append(". ");

        String usedIn = getUsedIn(usedMarketing, usedCustomer);
        result.append(format(template, usedIn)).append(LINE_SEPARATOR);
        result.append(format(UPDATE_INFO_AT_TAB, usedIn, "Communication Preferences"));

        return result.toString();
    }

    private static String getUsedIn(boolean usedMarketing, boolean usedCustomer)
    {
        StringBuilder result = new StringBuilder();
        if (usedMarketing)
        {
            result.append(MARKETING_PREFERENCES);
        }
        if (usedMarketing && usedCustomer)
        {
            result.append(AND);
        }
        if (usedCustomer)
        {
            result.append(CUSTOMER_SERVICE_PREFERENCES);
        }
        return result.toString();
    }
}
