package com.ihg.automation.selenium.servicecenter.pages.programs.rc;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static com.ihg.automation.selenium.servicecenter.pages.programs.rc.PartnerBenefitsGridRow.PartnerBenefitsCell.AVAILABLE;
import static com.ihg.automation.selenium.servicecenter.pages.programs.rc.PartnerBenefitsGridRow.PartnerBenefitsCell.BENEFIT_NAME;
import static com.ihg.automation.selenium.servicecenter.pages.programs.rc.PartnerBenefitsGridRow.PartnerBenefitsCell.REDEEMED;
import static com.ihg.automation.selenium.servicecenter.pages.programs.rc.PartnerBenefitsGridRow.PartnerBenefitsCell.REFRESH_DATE;
import static com.ihg.automation.selenium.servicecenter.pages.programs.rc.PartnerBenefitsGridRow.PartnerBenefitsCell.TIER_LEVEL;
import static com.ihg.automation.selenium.servicecenter.pages.programs.rc.PartnerBenefitsGridRow.PartnerBenefitsCell.YTD_REDEEMED;

import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.grid.Grid;
import com.ihg.automation.selenium.gwt.components.grid.GridHeader;

public class PartnerBenefitsGrid extends Grid<PartnerBenefitsGridRow, PartnerBenefitsGridRow.PartnerBenefitsCell>
{
    public PartnerBenefitsGrid(Container parent)
    {
        super(parent);
    }

    public void verifyHeader()
    {
        GridHeader<PartnerBenefitsGridRow.PartnerBenefitsCell> header = getHeader();
        verifyThat(header.getCell(TIER_LEVEL), hasText(TIER_LEVEL.getValue()));
        verifyThat(header.getCell(BENEFIT_NAME), hasText(BENEFIT_NAME.getValue()));
        verifyThat(header.getCell(REFRESH_DATE), hasText(REFRESH_DATE.getValue()));
        verifyThat(header.getCell(REDEEMED), hasText(REDEEMED.getValue()));
        verifyThat(header.getCell(AVAILABLE), hasText(AVAILABLE.getValue()));
        verifyThat(header.getCell(YTD_REDEEMED), hasText(YTD_REDEEMED.getValue()));
    }
}
