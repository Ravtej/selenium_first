package com.ihg.automation.selenium.servicecenter.pages.event.offer;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;

import com.ihg.automation.selenium.common.offer.SubOffers;
import com.ihg.automation.selenium.gwt.components.grid.CellsName;
import com.ihg.automation.selenium.gwt.components.grid.GridRow;
import com.ihg.automation.selenium.gwt.components.grid.GridRowXpath;

public class SubOffersGridRow extends GridRow<SubOffersGridRow.SubOfferCell>
{
    public SubOffersGridRow(SubOffersGrid parent, GridRowXpath xpath)
    {
        super(parent, xpath);
    }

    public void verify(SubOffers offer)
    {
        verifyThat(getCell(SubOffersGridRow.SubOfferCell.OFFER_NAME), hasText(offer.getName()));
        verifyThat(getCell(SubOffersGridRow.SubOfferCell.STATUS), hasText(offer.getStatus()));
        verifyThat(getCell(SubOffersGridRow.SubOfferCell.REWARD), hasText(offer.getReward()));
        verifyThat(getCell(SubOffersGridRow.SubOfferCell.Q_EVENTS), hasText(offer.getEvents()));
        verifyThat(getCell(SubOffersGridRow.SubOfferCell.GOAL), hasText(offer.getGoal()));
        verifyThat(getCell(SubOffersGridRow.SubOfferCell.AWARD_POSTED), hasText(offer.getAwardPosted()));
    }

    public enum SubOfferCell implements CellsName
    {
        OFFER_CODE("subOfferCode", "Code"), OFFER_NAME("subOfferName", "Name"), STATUS("participationStatus", "Status"), REWARD(
                "points", "Reward"), GOAL("hurdle", "Goal"), Q_EVENTS("progress", "Q Events"), AWARD_POSTED(
                "datePosted", "Award Posted");

        private String name;
        private String value;

        private SubOfferCell(String name, String value)
        {
            this.name = name;
            this.value = value;
        }

        public String getValue()
        {
            return value;
        }

        @Override
        public String getName()
        {
            return name;
        }
    }
}
