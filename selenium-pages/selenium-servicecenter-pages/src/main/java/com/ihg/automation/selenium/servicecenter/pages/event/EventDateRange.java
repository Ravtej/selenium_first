package com.ihg.automation.selenium.servicecenter.pages.event;

import static java.text.MessageFormat.format;

import com.ihg.automation.selenium.common.components.RangeSelect;
import com.ihg.automation.selenium.common.components.RangeSelect.Range;
import com.ihg.automation.selenium.common.pages.LabelContainerBase;
import com.ihg.automation.selenium.gwt.components.ComponentColumnPosition;
import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.input.DateInput;
import com.ihg.automation.selenium.gwt.components.input.InputXpath.InputXpathBuilder;

public class EventDateRange extends LabelContainerBase implements DateWithRangeFieldsAware
{
    private static final ComponentColumnPosition POSITION = ComponentColumnPosition.SINGLE_TD_INNER_DIV;
    private static final String DESCR_PATTERN = "{0} ({1})";

    public EventDateRange(Container container, String label)
    {
        super(container.getLabel(label));
    }

    @Override
    public DateInput getDate()
    {
        return new DateInput(label, new InputXpathBuilder().column(1, POSITION).build(),
                format(DESCR_PATTERN, label.getName(), "date"));
    }

    @Override
    public RangeSelect getRange()
    {
        return new RangeSelect(label, new InputXpathBuilder().column(2, POSITION).build(),
                format(DESCR_PATTERN, label.getName(), "range"));
    }

    public void populate(String date, Range range)
    {
        getDate().type(date);
        getRange().select(range);
    }
}
