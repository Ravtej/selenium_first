package com.ihg.automation.selenium.servicecenter.pages.preferences;

import com.ihg.automation.selenium.gwt.components.Button;
import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.CustomContainerBase;
import com.ihg.automation.selenium.gwt.components.Separator;
import com.ihg.automation.selenium.gwt.components.checkbox.CheckBox;
import com.ihg.automation.selenium.gwt.components.input.Input;
import com.ihg.automation.selenium.gwt.components.label.Label;
import com.ihg.automation.selenium.gwt.components.utils.ByText;
import com.ihg.automation.selenium.listener.Verifier;

public class VatFapiaoDetails extends CustomContainerBase
{
    public VatFapiaoDetails(Container parent)
    {
        super(new Separator(parent, ByText.exact("VAT Fapiao"), Separator.FIELDSET_PATTERN_SECOND_SIBLING));
    }

    public CheckBox getFapiao()
    {
        return new CheckBox(container);
    }

    public Label getFapiaoLabel()
    {
        return container.getLabel("Fapiao");
    }

    public Input getCompanyName()
    {
        return container.getInputByLabel("Company Name");
    }

    public Input getTaxIdentificationNo()
    {
        return container.getInputByLabel("Tax Identification No.");
    }

    public Input getCompanyAddress()
    {
        return container.getInputByLabel("Company Address");
    }

    public Input getCompanyTelephone()
    {
        return container.getInputByLabel("Company Telephone");
    }

    public Input getCompanyBank()
    {
        return container.getInputByLabel("Company Bank");
    }

    public Input getBankAccountNo()
    {
        return container.getInputByLabel("Bank Account No.");
    }

    public Button getReset()
    {
        return container.getButton("Reset");
    }

    public void clickReset(Verifier... verifiers)
    {
        getReset().clickAndWait(verifiers);
    }

}
