package com.ihg.automation.selenium.servicecenter.pages.programs.amb;

import com.ihg.automation.common.DateUtils;
import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.grid.CellsText;
import com.ihg.automation.selenium.gwt.components.grid.Grid;
import com.ihg.automation.selenium.gwt.components.grid.GridRowXpath;

public class AmbassadorTierLevelActivityGrid
        extends Grid<AmbassadorTierLevelActivityRow, AmbassadorTierLevelActivityRow.AmbassadorTierLevelCell>
{
    public AmbassadorTierLevelActivityGrid(Container container)
    {
        super(container.getPanel("Tier Level Annual Activity From January 1, " + DateUtils.currentYear()),
                "Tier Level Annual Activity");
    }

    public AmbassadorTierLevelActivityRow getRow(AmbassadorTierLevelAccumulator accumulator)
    {
        return getRow(new GridRowXpath.GridRowXpathBuilder()
                .cell(AmbassadorTierLevelActivityRow.AmbassadorTierLevelCell.ACCUMULATOR, accumulator).build());
    }

    public static enum AmbassadorTierLevelAccumulator implements CellsText
    {
        QUALIFYING_SPEND("Qualifying Spend (USD)"), IC_HOTELS("Numbers of IC Hotels Stayed At");

        private String text;

        private AmbassadorTierLevelAccumulator(String text)
        {
            this.text = text;
        }

        @Override
        public String getText()
        {
            return text;
        }
    }

}
