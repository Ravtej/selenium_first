package com.ihg.automation.selenium.servicecenter.pages.deposit;

import com.ihg.automation.selenium.common.billing.EventBillingDetailsTab;
import com.ihg.automation.selenium.common.billing.EventBillingTabAware;
import com.ihg.automation.selenium.common.earning.EventEarningDetailsTab;
import com.ihg.automation.selenium.common.earning.EventEarningTabAware;
import com.ihg.automation.selenium.common.pages.ClosablePopUpBase;

public class DepositDetailsPopUp extends ClosablePopUpBase implements EventEarningTabAware, EventBillingTabAware
{

    public DepositDetailsPopUp()
    {
        super("Deposit Details");
    }

    public DepositDetailsTab getDepositDetailsTab()
    {
        return new DepositDetailsTab(this);
    }

    @Override
    public EventBillingDetailsTab getBillingDetailsTab()
    {
        return new EventBillingDetailsTab(this);
    }

    @Override
    public EventEarningDetailsTab getEarningDetailsTab()
    {
        return new EventEarningDetailsTab(this);
    }

}
