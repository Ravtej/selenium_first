package com.ihg.automation.selenium.servicecenter.pages.event.stay;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;

import org.apache.commons.lang3.StringUtils;

import com.ihg.automation.selenium.common.pages.PopUpBase;
import com.ihg.automation.selenium.gwt.components.Button;

public class DisqualifyReasonPopUp extends PopUpBase
{
    public static final String CHECK_TIER_LEVEL = "Nights may qualify for Tier-Level. Check Tier-Level Night Count.";

    public DisqualifyReasonPopUp()
    {
        super("Disqualification Reason");
    }

    public Button getClose()
    {
        return container.getButton(Button.CLOSE);
    }

    public void clickClose()
    {
        getClose().clickAndWait();
    }

    public void verifyReasonAndClose(String... reason)
    {
        verifyThat(this, hasText(StringUtils.join(reason, "\n") + "\n" + CHECK_TIER_LEVEL));
        clickClose();
    }
}
