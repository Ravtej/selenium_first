package com.ihg.automation.selenium.servicecenter.pages.programs.dr;

import com.ihg.automation.selenium.gwt.components.grid.CellsName;
import com.ihg.automation.selenium.gwt.components.grid.GridRow;
import com.ihg.automation.selenium.gwt.components.grid.GridRowXpath;

public class DiningBenefitGridRow extends GridRow<DiningBenefitGridRow.DiningBenefitCell>
{
    public DiningBenefitGridRow(DiningBenefitGrid parent, GridRowXpath xpath)
    {
        super(parent, xpath);
    }

    public enum DiningBenefitCell implements CellsName
    {
        BENEFIT("description"), EXPIRATION_DATE("expirationDate");

        private String name;

        private DiningBenefitCell(String name)
        {
            this.name = name;
        }

        @Override
        public String getName()
        {
            return name;
        }
    }
}
