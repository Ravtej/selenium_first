package com.ihg.automation.selenium.servicecenter.pages.programs.rc;

import com.ihg.automation.selenium.common.member.MemberAlliance;
import com.ihg.automation.selenium.common.pages.Populate;
import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.input.Select;

public class AllianceDetailsAdd extends AllianceDetailsBase implements Populate<MemberAlliance>
{
    public AllianceDetailsAdd(Container container)
    {
        super(container);
    }

    @Override
    public Select getAlliance()
    {
        return container.getSelectByLabel(ALLIANCE_LABEL);
    }

    @Override
    public void populate(MemberAlliance alliance)
    {
        getAlliance().selectByCode(alliance.getAlliance());
        super.populate(alliance);
    }
}
