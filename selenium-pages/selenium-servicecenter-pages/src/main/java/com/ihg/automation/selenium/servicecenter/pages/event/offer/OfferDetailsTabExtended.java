package com.ihg.automation.selenium.servicecenter.pages.event.offer;

import com.ihg.automation.selenium.common.pages.PopUpBase;
import com.ihg.automation.selenium.common.pages.TabCustomContainerBase;

public class OfferDetailsTabExtended extends TabCustomContainerBase
{
    public OfferDetailsTabExtended(PopUpBase parent)
    {
        super(parent, "Details");
    }

    public OfferDetailsViewExtended getOfferDetails()
    {
        return new OfferDetailsViewExtended(container);
    }
}
