package com.ihg.automation.selenium.servicecenter.pages.preferences;

import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.grid.Grid;
import com.ihg.automation.selenium.gwt.components.grid.GridRowXpath;

public class TravelProfileGrid extends Grid<TravelProfileGridRow, TravelProfileGridRow.TravelProfileCell>
{
    public TravelProfileGrid(Container parent)
    {
        super(parent);
    }

    public TravelProfileGridRow getRowByName(String profileName)
    {
        return getRow(new GridRowXpath.GridRowXpathBuilder()
                .cell(TravelProfileGridRow.TravelProfileCell.PROFILE_NAME, profileName).build());
    }

    public TravelProfileGridRow getRowByNameAndType(String profileName, String profileType)
    {
        return getRow(new GridRowXpath.GridRowXpathBuilder()
                .cell(TravelProfileGridRow.TravelProfileCell.PROFILE_NAME, profileName)
                .cell(TravelProfileGridRow.TravelProfileCell.PROFILE_TYPE, profileType).build());
    }

}
