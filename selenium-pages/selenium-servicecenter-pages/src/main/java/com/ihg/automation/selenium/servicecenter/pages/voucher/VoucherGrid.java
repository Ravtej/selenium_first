package com.ihg.automation.selenium.servicecenter.pages.voucher;

import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.grid.Grid;
import com.ihg.automation.selenium.gwt.components.grid.GridRowXpath;
import com.ihg.automation.selenium.servicecenter.pages.voucher.VoucherGridRow.VoucherCell;

public class VoucherGrid extends Grid<VoucherGridRow, VoucherCell>
{
    public VoucherGrid(Container parent)
    {
        super(parent, "Vouchers");
    }

    public VoucherGridRow getRow(String id)
    {
        return getRow(new GridRowXpath.GridRowXpathBuilder().cell(VoucherCell.ID, id).build());
    }
}
