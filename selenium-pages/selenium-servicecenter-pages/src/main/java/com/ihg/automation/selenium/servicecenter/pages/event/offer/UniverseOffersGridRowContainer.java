package com.ihg.automation.selenium.servicecenter.pages.event.offer;

import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.CustomContainerBase;

public class UniverseOffersGridRowContainer extends CustomContainerBase
{

    public UniverseOffersGridRowContainer(Container parent)
    {
        super(parent);
    }

    public OfferDetailsViewBase getOfferDetails()
    {
        return new OfferDetailsViewBase(container);
    }
}
