package com.ihg.automation.selenium.servicecenter.pages.event.offer;

import com.ihg.automation.selenium.common.pages.PopUpBase;
import com.ihg.automation.selenium.common.pages.TabCustomContainerBase;

public class OfferDetailsTab extends TabCustomContainerBase
{
    public OfferDetailsTab(PopUpBase parent)
    {
        super(parent, "Details");
    }

    public OfferDetailsView getOfferDetails()
    {
        return new OfferDetailsView(container);
    }
}
