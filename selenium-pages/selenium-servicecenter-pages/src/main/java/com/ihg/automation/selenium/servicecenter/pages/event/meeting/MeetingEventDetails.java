package com.ihg.automation.selenium.servicecenter.pages.event.meeting;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasDefault;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasEmptyText;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.core.IsNot.not;

import com.ihg.automation.selenium.common.Constant;
import com.ihg.automation.selenium.common.EventSource;
import com.ihg.automation.selenium.common.EventSourceAware;
import com.ihg.automation.selenium.common.MeetingDetails;
import com.ihg.automation.selenium.common.SiteHelperBase;
import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.CustomContainerBase;
import com.ihg.automation.selenium.gwt.components.label.Label;

public class MeetingEventDetails extends CustomContainerBase implements EventSourceAware
{
    public MeetingEventDetails(Container container)
    {
        super(container);
    }

    public Label getMeetingEventName()
    {
        return container.getLabel("Meeting Event Name");
    }

    public Label getContractBookingDate()
    {
        return container.getLabel("Contract Booking Date");
    }

    public Label getMeetingEventType()
    {
        return container.getLabel("Meeting Event Type");
    }

    public Label getTotalNumberOfRoomNights()
    {
        return container.getLabel("Total Number of Room Nights");
    }

    public Label getSalesManager()
    {
        return container.getLabel("Sales Manager");
    }

    public Label getPaymentType()
    {
        return container.getLabel("Payment Type");
    }

    public Label getSalesManagerEmail()
    {
        return container.getLabel("Sales Manager E-mail");
    }

    public Label getNegotiatedRoomRate()
    {
        return container.getLabel("Negotiated Room Rate");
    }

    public Label getSalesManagerPhone()
    {
        return container.getLabel("Sales Manager Phone");
    }

    public Label getTotalRoomsRevenue()
    {
        return container.getLabel("Total Rooms Revenue");
    }

    public Label getTotalQualifiedNonRoomsRevenue()
    {
        return container.getLabel("Total Qualified Non-Rooms Revenue");
    }

    public void verify(MeetingDetails meeting, SiteHelperBase siteHelper)
    {
        verify(meeting);
        getSource().verify(siteHelper.getSourceFactory().getNoEmployeeIdSource());
    }

    public void verify(MeetingDetails meeting)
    {
        verifyWithoutSalesManagerDetails(meeting);

        verifyThat(getSalesManager(), allOf(not(hasText(Constant.NOT_AVAILABLE)), not(hasEmptyText())));
        verifyThat(getSalesManagerEmail(), allOf(not(hasText(Constant.NOT_AVAILABLE)), not(hasEmptyText())));
        verifyThat(getSalesManagerPhone(), allOf(not(hasText(Constant.NOT_AVAILABLE)), not(hasEmptyText())));
    }

    public void verifyWithoutSalesManagerDetails(MeetingDetails meeting)
    {
        verifyThat(getMeetingEventName(), hasText(meeting.getMeetingEventName()));
        verifyThat(getMeetingEventType(), hasText(meeting.getMeetingEventType()));

        verifyThat(getContractBookingDate(), hasText(meeting.getContractBookingDate()));
        verifyThat(getTotalNumberOfRoomNights(), hasText(meeting.getTotalNumberOfRoomNights()));
        verifyThat(getNegotiatedRoomRate(), hasText(meeting.getNegotiatedRoomRate()));
        verifyThat(getTotalRoomsRevenue(), hasText(meeting.getTotalRoomsRevenue()));
        verifyThat(getTotalQualifiedNonRoomsRevenue(), hasText(meeting.getTotalQualifiedNonRoomsRevenue()));

        if (meeting.getPaymentType() != null)
        {
            verifyThat(getPaymentType(), hasText(meeting.getPaymentType().getCode()));
        }
        else
        {
            verifyThat(getPaymentType(), hasDefault());
        }
    }

    @Override
    public EventSource getSource()
    {
        return new EventSource(container);
    }
}
