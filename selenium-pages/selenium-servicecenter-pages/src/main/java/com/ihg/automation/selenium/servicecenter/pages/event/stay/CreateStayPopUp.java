package com.ihg.automation.selenium.servicecenter.pages.event.stay;

import com.ihg.automation.selenium.common.pages.ClosablePopUpBase;
import com.ihg.automation.selenium.listener.Verifier;

public class CreateStayPopUp extends ClosablePopUpBase
{
    public CreateStayPopUp()
    {
        super("Stay Details");
    }

    public StayDetailsCreate getStayDetails()
    {
        return new StayDetailsCreate(this);
    }

    public void clickCreateStay(Verifier... verifiers)
    {
        container.getButton("Create Stay").clickAndWait(verifiers);
    }
}
