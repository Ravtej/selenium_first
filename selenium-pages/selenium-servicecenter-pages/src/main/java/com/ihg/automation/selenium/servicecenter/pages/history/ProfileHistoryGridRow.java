package com.ihg.automation.selenium.servicecenter.pages.history;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasEmptyText;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static org.apache.commons.lang3.StringUtils.EMPTY;

import org.hamcrest.Matcher;

import com.ihg.automation.selenium.gwt.components.Componentable;
import com.ihg.automation.selenium.gwt.components.grid.CellsName;
import com.ihg.automation.selenium.gwt.components.grid.TreeGrid;
import com.ihg.automation.selenium.gwt.components.grid.TreeGridRow;
import com.ihg.automation.selenium.gwt.components.utils.ByStringText;

public class ProfileHistoryGridRow extends TreeGridRow<ProfileHistoryGridRow, ProfileHistoryGridRow.ProfileHistoryCell>
{
    public ProfileHistoryGridRow(TreeGrid component, int index)
    {
        super(component, index);
    }

    public ProfileHistoryGridRow(ProfileHistoryGridRow component, ProfileHistoryCell cellName, ByStringText text)
    {
        super(component, cellName, text);
    }

    public ProfileHistoryGridRow(ProfileHistoryGridRow component, int index)
    {
        super(component, index);
    }

    public ProfileHistoryGridRow getSubRowByName(ByStringText cellText)
    {
        return getSubRow(ProfileHistoryCell.NAME, cellText);
    }

    public ProfileHistoryGridRow getSubRowByName(String cellText)
    {
        return getSubRow(ProfileHistoryCell.NAME, ByStringText.equals(cellText));
    }

    public void verify(Matcher<Componentable> oldValueMatcher, Matcher<Componentable> newValueMatcher)
    {
        verifyThat(getCell(ProfileHistoryCell.OLD_VALUE), oldValueMatcher);
        verifyThat(getCell(ProfileHistoryCell.NEW_VALUE), newValueMatcher);
    }

    public void verify(String oldValue, String newValue)
    {
        verify(hasText(oldValue), hasText(newValue));
    }

    public void verifyAddAction(String newValue)
    {
        verify(EMPTY, newValue);
    }

    public void verifyAddAction(Matcher<Componentable> newValueMatcher)
    {
        verify(hasEmptyText(), newValueMatcher);
    }

    public void verifyDeleteAction(String oldValue)
    {
        verify(oldValue, EMPTY);
    }

    public void verifyDeleteAction(Matcher<Componentable> oldValueMatcher)
    {
        verify(oldValueMatcher, hasEmptyText());
    }

    public enum ProfileHistoryCell implements CellsName
    {
        NAME("name"), OLD_VALUE("oldValue"), NEW_VALUE("newValue");

        private String name;

        private ProfileHistoryCell(String name)
        {
            this.name = name;
        }

        @Override
        public String getName()
        {
            return name;
        }
    }
}
