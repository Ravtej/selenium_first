package com.ihg.automation.selenium.servicecenter.pages.event.stay;

import com.ihg.automation.selenium.common.billing.EventBillingDetailsTab;
import com.ihg.automation.selenium.common.billing.EventBillingTabAware;
import com.ihg.automation.selenium.common.earning.EventEarningDetailsTab;
import com.ihg.automation.selenium.common.earning.EventEarningTabAware;
import com.ihg.automation.selenium.common.history.EventHistoryTab;
import com.ihg.automation.selenium.common.history.EventHistoryTabAware;
import com.ihg.automation.selenium.common.pages.ClosablePopUpBase;
import com.ihg.automation.selenium.gwt.components.Button;
import com.ihg.automation.selenium.listener.Verifier;

public class AdjustStayPopUp extends ClosablePopUpBase
        implements EventHistoryTabAware, EventEarningTabAware, EventBillingTabAware
{
    public AdjustStayPopUp()
    {
        super("Stay Details");
    }

    public StayDetailsTab getStayDetailsTab()
    {
        return new StayDetailsTab(this);
    }

    @Override
    public EventHistoryTab getHistoryTab()
    {
        return new EventHistoryTab(this, "Stay History");
    }

    @Override
    public EventEarningDetailsTab getEarningDetailsTab()
    {
        return new EventEarningDetailsTab(this);
    }

    @Override
    public EventBillingDetailsTab getBillingDetailsTab()
    {
        return new EventBillingDetailsTab(this);
    }

    /**
     * <b><i>Note:</i></b> Available only if 'Stay Details' tab is 'Active'
     */
    public Button getSaveChanges()
    {
        return container.getButton("Save Changes");
    }

    /**
     * <b><i>Note:</i></b> Works only if 'Stay Details' tab is 'Active'
     */
    public void clickSaveChanges(Verifier... verifiers)
    {
        getSaveChanges().clickAndWait(verifiers);
    }
}
