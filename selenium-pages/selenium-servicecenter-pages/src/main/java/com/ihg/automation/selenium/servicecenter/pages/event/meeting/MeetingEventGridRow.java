package com.ihg.automation.selenium.servicecenter.pages.event.meeting;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasEmptyText;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;

import org.apache.commons.lang3.StringUtils;

import com.ihg.automation.selenium.gwt.components.grid.Grid;
import com.ihg.automation.selenium.gwt.components.grid.GridRow;
import com.ihg.automation.selenium.gwt.components.grid.GridRowXpath;
import com.ihg.automation.selenium.servicecenter.pages.event.meeting.MeetingEventGrid.MeetingEventCell;

public class MeetingEventGridRow extends GridRow<MeetingEventCell>
{
    public MeetingEventGridRow(Grid parent, GridRowXpath xpath)
    {
        super(parent, xpath);
    }

    public void verify(MeetingEventRow row)
    {
        verifyThat(getCell(MeetingEventCell.TRANS_DATE), hasText(row.getTransDate()));
        verifyThat(getCell(MeetingEventCell.TRANS_TYPE), hasText(row.getTransType()));
        verifyThat(getCell(MeetingEventCell.HOTEL), hasText(row.getHotel()));
        verifyThat(getCell(MeetingEventCell.CHECK_IN_DATE), hasText(row.getCheckInDate()));
        verifyThat(getCell(MeetingEventCell.CHECK_OUT_DATE), hasText(row.getCheckOutDate()));
        verifyThat(getCell(MeetingEventCell.MEETING_EVENT_NAME), hasText(row.getMeetingName()));

        if (StringUtils.isEmpty(row.getIhgUnits()))
        {
            verifyThat(getCell(MeetingEventCell.IHG_UNITS), hasEmptyText());
        }
        else
        {
            verifyThat(getCell(MeetingEventCell.IHG_UNITS), hasText(row.getIhgUnits()));
        }

        if (StringUtils.isEmpty(row.getAllianceUnits()))
        {
            verifyThat(getCell(MeetingEventCell.ALLIANCE_UNITS), hasEmptyText());
        }
        else
        {
            verifyThat(getCell(MeetingEventCell.ALLIANCE_UNITS), hasText(row.getAllianceUnits()));
        }
    }
}
