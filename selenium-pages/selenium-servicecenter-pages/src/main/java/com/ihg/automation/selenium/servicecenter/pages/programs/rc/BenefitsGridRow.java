package com.ihg.automation.selenium.servicecenter.pages.programs.rc;

import static com.ihg.automation.common.DateUtils.DATE_FORMAT;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.types.program.TierLevelExpiration.LIFETIME;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static org.apache.commons.lang3.StringUtils.EMPTY;

import org.joda.time.DateTime;

import com.ihg.automation.selenium.common.catalog.CatalogItem;
import com.ihg.automation.selenium.gwt.components.grid.CellsName;
import com.ihg.automation.selenium.gwt.components.grid.GridRow;
import com.ihg.automation.selenium.gwt.components.grid.GridRowXpath;

public class BenefitsGridRow extends GridRow<BenefitsGridRow.BenefitsCell>
{
    public BenefitsGridRow(BenefitsGrid parent, GridRowXpath xpath)
    {
        super(parent, xpath);
    }

    public void verifyBeforeRedeem(CatalogItem catalogItem)
    {
        verifyThat(getCell(BenefitsCell.TIER_LEVEL), hasText(catalogItem.getTierLevels().get(0)));
        verifyThat(getCell(BenefitsCell.BENEFIT_AWARD), hasText(catalogItem.getItemId()));
        verifyThat(getCell(BenefitsCell.NAME), hasText(catalogItem.getItemName()));
        verifyThat(getCell(BenefitsCell.EXPIRATION), hasText(catalogItem.getEndDate()));
        verifyThat(getCell(BenefitsCell.REDEEMED), hasText(EMPTY));
        verifyThat(getCell(BenefitsCell.REDEEM).asLink(), displayed(true));
        verifyThat(getCell(BenefitsCell.REDEEM), hasText("Redeem"));
    }

    public void verifyAfterRedeemForRedeemedBenefit(CatalogItem catalogItem, String redeemedDate)
    {
        verifyThat(getCell(BenefitsCell.TIER_LEVEL), hasText(catalogItem.getTierLevels().get(0)));
        verifyThat(getCell(BenefitsCell.BENEFIT_AWARD), hasText(catalogItem.getItemId()));
        verifyThat(getCell(BenefitsCell.NAME), hasText(catalogItem.getItemName()));
        verifyThat(getCell(BenefitsCell.EXPIRATION), hasText(catalogItem.getEndDate()));
        verifyThat(getCell(BenefitsCell.REDEEMED), hasText(redeemedDate));
        verifyThat(getCell(BenefitsCell.REDEEM).asLink(), displayed(false));
    }

    public void verifyExpirationDate(int expirationYearShift)
    {
        verifyThat(getCell(BenefitsCell.EXPIRATION), hasText((DateTime.now().monthOfYear().withMaximumValue()
                .dayOfMonth().withMaximumValue().plusYears(expirationYearShift).toString(DATE_FORMAT))));
    }

    public void verifyLifetimeExpirationDate()
    {
        verifyThat(getCell(BenefitsCell.EXPIRATION), hasText(LIFETIME));
    }

    public void verifyAfterRedeemForNotRedeemedBenefit(CatalogItem catalogItem)
    {
        verifyAfterRedeemForRedeemedBenefit(catalogItem, EMPTY);
    }

    public void clickRedeem()
    {
        getCell(BenefitsCell.REDEEM).asLink().clickAndWait();
    }

    public enum BenefitsCell implements CellsName
    {
        TIER_LEVEL("levelCode"), BENEFIT_AWARD("benefit.id"), NAME("benefit.name"), EXPIRATION(
                "expirationDate"), REDEEMED("orderDate"), REDEEM(" ");

        private String name;

        private BenefitsCell(String name)
        {
            this.name = name;
        }

        @Override
        public String getName()
        {
            return name;
        }
    }
}
