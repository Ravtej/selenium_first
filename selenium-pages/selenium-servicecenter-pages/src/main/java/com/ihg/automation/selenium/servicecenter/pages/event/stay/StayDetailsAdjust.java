package com.ihg.automation.selenium.servicecenter.pages.event.stay;

public class StayDetailsAdjust extends StayDetailsBase
{
    public StayDetailsAdjust(StayDetailsTab parent)
    {
        super(parent.container);
    }

    @Override
    public StayInfoEdit getStayInfo()
    {
        return new StayInfoEdit(this);
    }
}
