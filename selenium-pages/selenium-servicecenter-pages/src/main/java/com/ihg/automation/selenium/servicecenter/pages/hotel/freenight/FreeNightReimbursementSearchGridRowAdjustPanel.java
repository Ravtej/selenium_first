package com.ihg.automation.selenium.servicecenter.pages.hotel.freenight;

import static com.ihg.automation.selenium.gwt.components.utils.FindStepUtils.byXpath;

import com.ihg.automation.selenium.gwt.components.Button;
import com.ihg.automation.selenium.gwt.components.Component;
import com.ihg.automation.selenium.gwt.components.grid.RowEditor;
import com.ihg.automation.selenium.gwt.components.input.Input;

public class FreeNightReimbursementSearchGridRowAdjustPanel extends RowEditor
{
    private static final String COMPONENT_XPATH_PATTERN = ".//div[@gxt-dindex='%s']";

    public FreeNightReimbursementSearchGridRowAdjustPanel(FreeNightReimbursementSearchGridRow row)
    {
        super(row);
    }

    public Input getActualOcc()
    {
        return new Input(new Component(container, "Input", "Actual OCC",
                byXpath(String.format(COMPONENT_XPATH_PATTERN, "actualOcc"))), "OCC");
    }

    public Input getActualAdr()
    {
        return new Input(new Component(container, "Input", "Actual ADR",
                byXpath(String.format(COMPONENT_XPATH_PATTERN, "actualAdr"))), "ADR");
    }

    public Component getStayDate()
    {
        return new Component(container, "Label", "Stay Date", byXpath(String.format(COMPONENT_XPATH_PATTERN, "date")));
    }

    public Component getNightsCount()
    {
        return new Component(container, "Label", "Nights Count",
                byXpath(String.format(COMPONENT_XPATH_PATTERN, "nightsCount")));
    }

    public Component getOCC()
    {
        return new Component(container, "Label", "OCC", byXpath(String.format(COMPONENT_XPATH_PATTERN, "occ")));
    }

    public Component getADR()
    {
        return new Component(container, "Label", "ADR", byXpath(String.format(COMPONENT_XPATH_PATTERN, "adr")));
    }

    public Component getStatus()
    {
        return new Component(container, "Label", "Status", byXpath(String.format(COMPONENT_XPATH_PATTERN, "status")));
    }

    public Component getTotalReimbursement()
    {
        return new Component(container, "Label", "Total Reimbursement",
                byXpath(String.format(COMPONENT_XPATH_PATTERN, "totalReimbursement")));
    }

    public Component getHotelCurrency()
    {
        return new Component(container, "Label", "Hotel Currency",
                byXpath(String.format(COMPONENT_XPATH_PATTERN, "hotelCurrency")));
    }

    public Component getDaysLeft()
    {
        return new Component(container, "Label", "Days Left",
                byXpath(String.format(COMPONENT_XPATH_PATTERN, "leftToSubmit")));
    }

    public Button getSaveAndApprove()
    {
        return new Button(container, "Save and Approve Request");
    }

    public void populate(String overrideOccupancyPercent, String overrideADRAmount)
    {
        getActualOcc().type(overrideOccupancyPercent);
        getActualAdr().type(overrideADRAmount);
    }
}
