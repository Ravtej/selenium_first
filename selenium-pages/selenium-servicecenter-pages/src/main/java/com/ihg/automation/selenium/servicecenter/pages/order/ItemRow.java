package com.ihg.automation.selenium.servicecenter.pages.order;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.formatter.Converter.stringToInteger;
import static com.ihg.automation.selenium.gwt.components.utils.FindStepUtils.byXpathWithWait;
import static com.ihg.automation.selenium.gwt.components.utils.XPathUtils.getFormattedXpath;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasTextAsFormattedInt;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasTextAsInt;

import com.ihg.automation.selenium.common.catalog.CatalogItem;
import com.ihg.automation.selenium.gwt.components.Component;
import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.CustomContainerBase;
import com.ihg.automation.selenium.gwt.components.input.Select;

public class ItemRow extends CustomContainerBase
{
    private static final String ROW_TYPE = "Item Row";
    private static final String TYPE = "Label Text";
    private static final String ROW_PATTERN = ".//div[3]/div[{0}]/table/tbody/tr";

    public ItemRow(Container container, int index)
    {
        super(container, ROW_TYPE, null, byXpathWithWait(getFormattedXpath(ROW_PATTERN, index)));
    }

    public ItemDescription getItemDescription()
    {
        return new ItemDescription(container, byXpathWithWait(".//td[1]"));
    }

    public Component getLoyaltyUnitsRequired()
    {
        return new Component(container, "Loyalty Units Required", TYPE, byXpathWithWait("./td[2]/span"));
    }

    public Select getQuantity()
    {
        return new Select(container, ".//td[3]//input", "Quantity");
    }

    public Component getTotalAmount()
    {
        return new Component(container, "Total Amount", TYPE, byXpathWithWait(".//td[4]/span"));
    }

    public void verify(CatalogItem catalogItem, String expectedTime)
    {
        getItemDescription().verify(catalogItem, expectedTime);
        Component loyaltyUnitsRequired = getLoyaltyUnitsRequired();
        verifyThat(loyaltyUnitsRequired, hasTextAsFormattedInt(catalogItem.getLoyaltyUnitCost()));
        verifyThat(getTotalAmount(), hasTextAsInt(
                stringToInteger(getQuantity().getText()) * stringToInteger(loyaltyUnitsRequired.getText())));
    }
}
