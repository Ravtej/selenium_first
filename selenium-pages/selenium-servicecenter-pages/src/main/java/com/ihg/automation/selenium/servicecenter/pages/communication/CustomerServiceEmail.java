package com.ihg.automation.selenium.servicecenter.pages.communication;

import com.ihg.automation.selenium.common.member.GuestEmail;
import com.ihg.automation.selenium.common.pages.Populate;
import com.ihg.automation.selenium.gwt.components.Component;
import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.CustomContainerBase;
import com.ihg.automation.selenium.gwt.components.checkbox.CheckBox;
import com.ihg.automation.selenium.gwt.components.input.Select;
import com.ihg.automation.selenium.gwt.components.utils.FindStepUtils;

public class CustomerServiceEmail extends CustomContainerBase implements Populate<GuestEmail>
{
    public CustomerServiceEmail(Container container)
    {
        super(container);
    }

    public CheckBox getEmailCheckBox()
    {
        return container.getCheckBox("Email");
    }

    public Select getEmail()
    {
        final String name = "Email";
        Component parent = new Component(container, "Email Area", name,
                FindStepUtils.byXpathWithWait(".//span[@class='email']"));
        return new Select(parent, name);
    }

    @Override
    public void populate(GuestEmail email)
    {
        getEmailCheckBox().check();
        getEmail().select(email.getEmail());
    }

}
