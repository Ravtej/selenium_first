package com.ihg.automation.selenium.servicecenter.pages.event.copartner;

import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.CustomContainerBase;
import com.ihg.automation.selenium.gwt.components.input.Input;
import com.ihg.automation.selenium.servicecenter.pages.event.EventDateRange;

public class CoPartnerSearch extends CustomContainerBase
{
    public CoPartnerSearch(Container container)
    {
        super(container.getSeparator("Co-Partner Search"));
    }

    public Input getPartnerName()
    {
        return container.getInputByLabel("Partner Name");
    }

    public Input getTransactionId()
    {
        return container.getInputByLabel("Transaction ID");
    }

    public Input getKeywords()
    {
        return container.getInputByLabel("Keywords");
    }

    public EventDateRange getTransactionDate()
    {
        return new EventDateRange(container, "Transaction Date");
    }

}
