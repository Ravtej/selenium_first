package com.ihg.automation.selenium.servicecenter.pages.event.freenight;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;

import com.ihg.automation.selenium.common.offer.Offer;
import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.CustomContainerBase;
import com.ihg.automation.selenium.gwt.components.label.Label;

public class FreeNightDetailsContainer extends CustomContainerBase
{
    public FreeNightDetailsContainer(Container container)
    {
        super(container.getSeparator("Free Night Details"));
    }

    public Label getFreeNight()
    {
        return container.getLabel("Free Night");
    }

    public Label getRateCategoryCode()
    {
        return container.getLabel("Rate Category Code");
    }

    public Label getReimbursementType()
    {

        return container.getLabel("Reimbursement Type");
    }

    public void verify(Offer offer)
    {
        verifyThat(getFreeNight(), hasText(offer.getFreeNight()));
        verifyThat(getRateCategoryCode(), hasText(offer.getRateCategoryCode()));
        verifyThat(getReimbursementType(), hasText(offer.getReimbursementType()));
    }

    public void capture(Offer.Builder builder)
    {
        builder.freeNight(getFreeNight().getText());
        builder.rateCategoryCode(getRateCategoryCode().getText());
        builder.reimbursementType(getReimbursementType().getText());
    }

}
