package com.ihg.automation.selenium.servicecenter.pages.voucher;

import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyNoError;

import com.ihg.automation.selenium.gwt.components.Button;
import com.ihg.automation.selenium.gwt.components.Menu.Item;
import com.ihg.automation.selenium.listener.Verifier;
import com.ihg.automation.selenium.servicecenter.pages.TopMenu;

public class VoucherLookupTab extends VoucherTabBase
{
    public VoucherLookupTab(VoucherPopUp voucherPopUp)
    {
        super(voucherPopUp, "Lookup");
    }

    @Override
    protected String getVoucherNumberLabel()
    {
        return "Voucher number";
    }

    @Override
    protected Item getMenuItem()
    {
        return new TopMenu().getVoucher().getLookup();
    }

    public Button getLookup()
    {
        return container.getButton("Lookup");
    }

    public void clickLookup(Verifier... verifiers)
    {
        getLookup().clickAndWait(verifiers);
    }

    public Button getVoid()
    {
        return container.getButton("Void");
    }

    public Button getUnvoid()
    {
        return container.getButton("Unvoid");
    }

    public void lookup(String voucherNumber)
    {
        getVoucherNumber().type(voucherNumber);
        clickLookup(verifyNoError());
    }

    public VoucherDetailsPanel getVoucherDetailsPanel()
    {
        return new VoucherDetailsPanel(container);
    }

    public VoucherStatusGrid getStatusGrid()
    {
        return new VoucherStatusGrid(container.getPanel("Current Status"));
    }

    public OrderDetailsPanel getOrderDetailsPanel()
    {
        return new OrderDetailsPanel(container);
    }
}
