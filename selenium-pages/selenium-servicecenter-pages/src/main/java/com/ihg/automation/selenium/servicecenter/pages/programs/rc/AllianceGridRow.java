package com.ihg.automation.selenium.servicecenter.pages.programs.rc;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasText;
import static org.hamcrest.CoreMatchers.containsString;

import com.ihg.automation.selenium.common.member.GuestName;
import com.ihg.automation.selenium.common.member.MemberAlliance;
import com.ihg.automation.selenium.gwt.components.grid.CellsName;
import com.ihg.automation.selenium.gwt.components.grid.Grid;
import com.ihg.automation.selenium.gwt.components.grid.GridRow;
import com.ihg.automation.selenium.gwt.components.grid.GridRowXpath;
import com.ihg.automation.selenium.listener.Verifier;

public class AllianceGridRow extends GridRow<AllianceGridRow.AllianceCell>
{
    public AllianceGridRow(Grid parent, GridRowXpath xpath)
    {
        super(parent, xpath);
    }

    public void edit()
    {
        getCell(AllianceCell.ALLIANCE).clickByLabel();
    }

    public void delete(Verifier... verifiers)
    {
        getCell(AllianceCell.DELETE).clickByLabel(verifiers);
    }

    public void verify(MemberAlliance alliance)
    {
        verifyThat(getCell(AllianceCell.ALLIANCE), hasText(containsString(alliance.getAlliance().getCode())));
        verifyThat(getCell(AllianceCell.NUMBER), hasText(alliance.getNumber()));
        GuestName guestName = alliance.getName();
        if (guestName != null)
        {
            verifyThat(getCell(AllianceCell.NAME), hasText(guestName.getShortName()));
        }
        verifyThat(getCell(AllianceCell.DELETE), displayed(true));
    }

    public enum AllianceCell implements CellsName
    {
        ALLIANCE("allianceString"), NUMBER("memberId"), NAME("name"), DELETE("delete");

        private String name;

        private AllianceCell(String name)
        {
            this.name = name;
        }

        @Override
        public String getName()
        {
            return name;
        }
    }
}
