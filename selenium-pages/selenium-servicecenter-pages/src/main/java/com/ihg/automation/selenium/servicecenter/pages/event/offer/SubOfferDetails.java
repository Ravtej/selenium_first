package com.ihg.automation.selenium.servicecenter.pages.event.offer;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasEmptyText;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static org.hamcrest.core.IsNot.not;

import com.ihg.automation.selenium.common.offer.SubOffers;
import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.CustomContainerBase;
import com.ihg.automation.selenium.gwt.components.Separator;
import com.ihg.automation.selenium.gwt.components.label.Label;

public class SubOfferDetails extends CustomContainerBase
{
    public SubOfferDetails(Container container)
    {
        super(container.getSeparator("Sub Offer Details", Separator.FIELDSET_PATTERN_NEXT_TR));
    }

    public Label getSubOfferName()
    {
        return container.getLabel("Name");
    }

    public Label getSubOfferDescription()
    {
        return container.getLabel("Description");
    }

    public Label getLastUpdate()
    {
        return container.getLabel("Last Update");
    }

    public void verify(SubOffers offer)
    {
        verifyThat(getSubOfferName(), hasText(offer.getName()));
        verifyThat(getSubOfferDescription(), hasText(offer.getDescription()));
        verifyThat(getLastUpdate(), not(hasEmptyText()));
    }

}
