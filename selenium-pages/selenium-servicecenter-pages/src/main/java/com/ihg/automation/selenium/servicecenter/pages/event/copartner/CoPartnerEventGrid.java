package com.ihg.automation.selenium.servicecenter.pages.event.copartner;

import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.grid.Grid;
import com.ihg.automation.selenium.gwt.components.grid.GridRowXpath;
import com.ihg.automation.selenium.servicecenter.pages.event.copartner.CoPartnerEventGridRow.CoPartnerEventCell;

public class CoPartnerEventGrid extends Grid<CoPartnerEventGridRow, CoPartnerEventCell>
{
    public CoPartnerEventGrid(Container parent)
    {
        super(parent);
    }

    public CoPartnerEventGridRow getRow(String transType, String transId)
    {
        return getRow(new GridRowXpath.GridRowXpathBuilder().cell(CoPartnerEventCell.TRANS_TYPE, transType)
                .cell(CoPartnerEventCell.TRANSACTION_ID, transId).build());
    }

}
