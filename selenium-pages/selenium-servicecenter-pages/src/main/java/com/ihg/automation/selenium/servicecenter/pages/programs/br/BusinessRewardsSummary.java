package com.ihg.automation.selenium.servicecenter.pages.programs.br;

import com.ihg.automation.selenium.gwt.components.Button;
import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.label.Label;
import com.ihg.automation.selenium.servicecenter.pages.programs.ProgramSummary;

public class BusinessRewardsSummary extends ProgramSummary
{

    public BusinessRewardsSummary(Container container)
    {
        super(container);
    }

    public Label getTermsAndConditions()
    {
        return container.getLabel("Terms and Conditions");
    }

    public Label getDateOfResponse()
    {
        return container.getLabel("Date of Response");
    }

    public Label getSource()
    {
        return container.getLabel("Source");
    }

    public Button getDeclinedTsAndCs()
    {
        return container.getButton("Decline Ts&Cs");
    }

    public void clickDeclinedTsAndCs()
    {
        getDeclinedTsAndCs().clickAndWait();
    }
}
