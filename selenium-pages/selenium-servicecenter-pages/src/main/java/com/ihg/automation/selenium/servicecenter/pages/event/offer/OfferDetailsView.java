package com.ihg.automation.selenium.servicecenter.pages.event.offer;

import com.ihg.automation.selenium.common.offer.Offer;
import com.ihg.automation.selenium.gwt.components.Container;

public class OfferDetailsView extends OfferDetailsViewBase
{

    public OfferDetailsView(Container parent)
    {
        super(parent);
    }

    public AwardDetailsContainer getAwardDetails()
    {
        return new AwardDetailsContainer(container);
    }

    public void verify(Offer offer)
    {
        super.verify(offer);
        getAwardDetails().verify(offer.getAwardDetails());
    }
}
