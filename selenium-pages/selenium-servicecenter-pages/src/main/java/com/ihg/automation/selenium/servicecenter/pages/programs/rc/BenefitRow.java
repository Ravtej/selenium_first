package com.ihg.automation.selenium.servicecenter.pages.programs.rc;

import com.ihg.automation.selenium.common.types.program.RewardClubLevel;

public class BenefitRow
{
    private String name = "AMAZON KINDLE";
    private RewardClubLevel tierLevel;
    private String benefitCount;
    private BenefitFrequency benefitFrequency;

    public BenefitRow(RewardClubLevel tierLevel, String benefitCount, BenefitFrequency benefitFrequency)
    {
        this.tierLevel = tierLevel;
        this.benefitCount = benefitCount;
        this.benefitFrequency = benefitFrequency;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public RewardClubLevel getTierLevel()
    {
        return tierLevel;
    }

    public void setTierLevel(RewardClubLevel tierLevel)
    {
        this.tierLevel = tierLevel;
    }

    public String getBenefitCount()
    {
        return benefitCount;
    }

    public void setBenefitCount(String benefitCount)
    {
        this.benefitCount = benefitCount;
    }

    public BenefitFrequency getBenefitFrequency()
    {
        return benefitFrequency;
    }

    public void setBenefitFrequency(BenefitFrequency benefitFrequency)
    {
        this.benefitFrequency = benefitFrequency;
    }
}
