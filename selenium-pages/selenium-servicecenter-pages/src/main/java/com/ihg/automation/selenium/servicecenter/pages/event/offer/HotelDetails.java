package com.ihg.automation.selenium.servicecenter.pages.event.offer;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.gwt.components.utils.FindStepUtils.byXpath;
import static com.ihg.automation.selenium.gwt.components.utils.FindStepUtils.byXpathWithWait;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static java.text.MessageFormat.format;

import com.ihg.automation.selenium.gwt.components.Component;
import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.CustomContainerBase;
import com.ihg.automation.selenium.gwt.components.utils.ByText;

public class HotelDetails extends CustomContainerBase
{
    private static final String TYPE = "Qualifying Stay Details";
    private static final String PATTERN = ".//div[descendant::tr]//tr[descendant::span[{0}]]";

    public HotelDetails(Container container, String hotel)
    {
        super(new Container(container, TYPE, hotel, byXpathWithWait(format(PATTERN, ByText.exact(hotel)))));
    }

    public HotelDetails(Container container, int rowNumber)
    {
        super(new Container(container, TYPE, "Hotel",
                byXpathWithWait(format(".//div[descendant::tr]//tr[descendant::span][{0}]", rowNumber))));
    }

    public Component getHotel()
    {
        return new Component(container, TYPE, "Hotel", byXpath(".//td[2]/span"));
    }

    public Component getCheckIn()
    {
        return new Component(container, TYPE, "Check In", byXpath(".//td[4]/span"));
    }

    public Component getCheckOut()
    {
        return new Component(container, TYPE, "Check Out", byXpath(".//td[6]/span"));
    }

    public void verify(String hotel, String checkIn, String checkOut)
    {
        verifyThat(getHotel(), hasText(hotel));
        verifyThat(getCheckIn(), hasText(checkIn));
        verifyThat(getCheckOut(), hasText(checkOut));
    }
}
