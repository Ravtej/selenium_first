package com.ihg.automation.selenium.servicecenter.pages.programs;

import static com.ihg.automation.common.DateUtils.DATE_FORMAT;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.gwt.components.utils.FindStepUtils.byXpath;
import static com.ihg.automation.selenium.matchers.component.ViewableMatcher.hasTextInView;
import static org.hamcrest.core.StringStartsWith.startsWith;

import org.joda.time.DateTime;

import com.ihg.automation.selenium.common.pages.MultiModeBase;
import com.ihg.automation.selenium.common.types.program.ProgramLevel;
import com.ihg.automation.selenium.common.types.program.ProgramLevelReason;
import com.ihg.automation.selenium.common.types.program.ProgramStatus;
import com.ihg.automation.selenium.common.types.program.TierLevelExpiration;
import com.ihg.automation.selenium.gwt.components.Component;
import com.ihg.automation.selenium.gwt.components.ComponentColumnPosition;
import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.DualList;
import com.ihg.automation.selenium.gwt.components.input.Input;
import com.ihg.automation.selenium.gwt.components.input.Select;
import com.ihg.automation.selenium.gwt.components.label.Label;
import com.ihg.automation.selenium.listener.Verifier;

public class ProgramSummary extends MultiModeBase
{
    public static final int SHIFT_FOR_CURRENT_YEAR = 0;
    public static final int SHIFT_FOR_NEXT_YEAR = 1;

    public ProgramSummary(Container container)
    {
        super(container.getSeparator("Program Summary"));
    }

    public Label getMemberId()
    {
        return container.getLabel("Member ID");
    }

    public Select getStatus()
    {
        return container.getSelectByLabel("Status");
    }

    public Select getTierLevel()
    {
        return container.getSelectByLabel("Tier-Level", "Tier Level", 1, ComponentColumnPosition.TABLE);
    }

    public DualList getFlags()
    {
        return container.getDualList("flags");
    }

    public Select getTierLevelReason()
    {
        return container.getSelectByLabel("Tier-Level Reason");
    }

    public Select getTierLevelExpiration()
    {
        return container.getSelectByLabel("Tier-Level Expiration");
    }

    public Label getUnitsBalance()
    {
        return container.getLabel("Current Loyalty Unit Balance");
    }

    public Select getStatusReason()
    {
        final String xpath = ".//span[normalize-space(text())='Status Reason' or descendant::*[normalize-space(text())='Status Reason']]/ancestor::td/following-sibling::td";
        Component parent = new Component(container, "Select", "Status Reason", byXpath(xpath));

        return new Select(parent, "Status Reason");
    }

    public Input getReferringMember()
    {
        return container.getInputByLabel("Referring Member");
    }

    public Component getReferringMemberName()
    {
        final String xpath = ".//ancestor::td/following-sibling::td[div[contains(@class,'x-form-label')]]";
        return new Component(getReferringMember(), "Label", "ReferringMemberName", byXpath(xpath));
    }

    // TODO add flags,
    // memberID
    public void verify(String status, ProgramLevel tierLevel)
    {
        verifyThat(getStatus(), hasTextInView(status));
        verifyThat(getTierLevel(), hasTextInView(startsWith(tierLevel.getValue())));
    }

    /**
     * <b>Before AMB renewal:</b><br>
     * <code>AMB expiration date = now + shiftDays</code> <br>
     * <b>After AMB renewal:</b> <br>
     * <code>RC tier-level expiration date = AMB expiration date(before renewal) + 1 year</code>
     */
    public void verify(String status, ProgramLevel tierLevel, int tierLevelExpirationYearShift, int shiftDays)
    {
        verify(status, tierLevel);

        verifyThat(getTierLevelExpiration(),
                hasTextInView(DateTime.now().plusDays(shiftDays).monthOfYear().withMaximumValue().dayOfMonth()
                        .withMaximumValue().plusYears(tierLevelExpirationYearShift).toString(DATE_FORMAT)));

    }

    public void verify(String status, ProgramLevel tierLevel, int tierLevelExpirationYearShift)
    {
        verify(status, tierLevel, tierLevelExpirationYearShift, 0);

    }

    public void verifyLifetimeTierLevel(String status, ProgramLevel tierLevel)
    {
        verify(status, tierLevel);
        verifyThat(getTierLevelExpiration(), hasTextInView(TierLevelExpiration.LIFETIME));
    }

    public void verifyLifetimeTierLevel(String status, ProgramLevel tierLevel, ProgramLevelReason tierLevelReason)
    {
        verify(status, tierLevel, tierLevelReason);
        verifyThat(getTierLevelExpiration(), hasTextInView(TierLevelExpiration.LIFETIME));
    }

    public void verify(String status, ProgramLevel tierLevel, ProgramLevelReason tierLevelReason)
    {
        verify(status, tierLevel);
        verifyThat(getTierLevelReason(), hasTextInView(tierLevelReason.getValue()));
    }

    public void verify(String status, ProgramLevel tierLevel, int tierLevelExpirationYearShift,
            ProgramLevelReason tierLevelReason)
    {
        verify(status, tierLevel, tierLevelExpirationYearShift, 0);
        verifyThat(getTierLevelReason(), hasTextInView(tierLevelReason.getValue()));
    }

    public void populateStatus(String status, String reason)
    {
        getStatus().select(status);
        getStatusReason().select(reason);
    }

    public void setStatus(String status, String reason, Verifier... verifiers)
    {
        clickEdit();
        populateStatus(status, reason);
        clickSave(verifiers);
    }

    public void setClosedStatus(String reason, Verifier... verifiers)
    {
        setStatus(ProgramStatus.CLOSED, reason, verifiers);
    }

    public void setOpenStatus(Verifier... verifiers)
    {
        setStatus(ProgramStatus.OPEN, null, verifiers);
    }

    public void populateTierLevel(ProgramLevel level, ProgramLevelReason reason, String expiration)
    {
        populateTierLevel(level, reason);
        getTierLevelExpiration().select(expiration);
    }

    public void populateTierLevel(ProgramLevel level, ProgramLevelReason reason)
    {
        getTierLevel().selectByValue(level);
        getTierLevelReason().selectByValue(reason);
    }

    public void populateTierLevelWithExpirationAndNoReason(ProgramLevel level, String expiration)
    {
        getTierLevel().selectByValue(level);
        getTierLevelExpiration().select(expiration);
    }

    public void setTierLevelWithExpiration(ProgramLevel level, ProgramLevelReason reason, String expiration,
            Verifier... verifiers)
    {
        clickEdit();
        populateTierLevel(level, reason, expiration);
        clickSave(verifiers);
    }

    public void setTierLevelWithExpiration(ProgramLevel level, ProgramLevelReason reason, Verifier... verifiers)
    {
        clickEdit();
        populateTierLevel(level, reason, TierLevelExpiration.EXPIRE_NEXT_YEAR);
        clickSave(verifiers);
    }

    public void setTierLevelWithoutExpiration(ProgramLevel level, ProgramLevelReason reason, Verifier... verifiers)
    {
        clickEdit();
        populateTierLevel(level, reason);
        clickSave(verifiers);
    }
}
