package com.ihg.automation.selenium.servicecenter.pages.event.business;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;

import com.ihg.automation.selenium.common.business.BusinessEvent;
import com.ihg.automation.selenium.common.business.BusinessRewardsEventDetailsPopUp;
import com.ihg.automation.selenium.gwt.components.grid.CellsName;
import com.ihg.automation.selenium.gwt.components.grid.GridRow;
import com.ihg.automation.selenium.gwt.components.grid.GridRowXpath;

public class BusinessEventsGridRow extends GridRow<BusinessEventsGridRow.BusinessEventsCell>
{
    public BusinessEventsGridRow(BusinessEventsGrid parent, GridRowXpath xpath)
    {
        super(parent, xpath);
    }

    public void verify(BusinessEvent businessEvent)
    {
        verifyThat(getCell(BusinessEventsCell.TRANS_DATE), hasText(businessEvent.getTransactionDate()));
        verifyThat(getCell(BusinessEventsCell.TRANS_TYPE), hasText(businessEvent.getTransactionType()));
        verifyThat(getCell(BusinessEventsCell.EVENT_NAME), hasText(businessEvent.getEventName()));
        verifyThat(getCell(BusinessEventsCell.EVENT_ID), hasText(businessEvent.getEventId()));
        verifyThat(getCell(BusinessEventsCell.HOTEL), hasText(businessEvent.getHotel()));
        verifyThat(getCell(BusinessEventsCell.START_DATE), hasText(businessEvent.getMeetingDetails().getStartDate()));
        verifyThat(getCell(BusinessEventsCell.END_DATE), hasText(businessEvent.getMeetingDetails().getEndDate()));
        verifyThat(getCell(BusinessEventsCell.STATUS), hasText(businessEvent.getStatus()));
        verifyThat(getCell(BusinessEventsCell.IHG_UNITS), hasText(businessEvent.getIhgUnits()));
        verifyThat(getCell(BusinessEventsCell.ALLIANCE_UNITS), hasText(businessEvent.getAllianceUnits()));
    }

    public BusinessRewardsEventDetailsPopUp openBRDetailsPopUp()
    {
        getCell(BusinessEventsGridRow.BusinessEventsCell.EVENT_ID).asLink().clickAndWait();

        return new BusinessRewardsEventDetailsPopUp();
    }

    public enum BusinessEventsCell implements CellsName
    {
        TRANS_DATE("transactionDate", "Trans Date"), //
        TRANS_TYPE("transactionType", "Trans Type"), //
        EVENT_ID("eventId", "Event ID"), //
        EVENT_NAME("eventName", "Event Name"), //
        HOTEL("hotelCode", "Hotel"), //
        START_DATE("startDate", "Start Date"), //
        END_DATE("endDate", "End Date"), //
        STATUS("status", "Status"), //
        IHG_UNITS("earnedUnits", "IHG Units"), //
        ALLIANCE_UNITS("allianceUnits", "Alliance Units");

        private String name;
        private String value;

        private BusinessEventsCell(String name, String value)
        {
            this.name = name;
            this.value = value;
        }

        public String getValue()
        {
            return value;
        }

        @Override
        public String getName()
        {
            return name;
        }
    }

}
