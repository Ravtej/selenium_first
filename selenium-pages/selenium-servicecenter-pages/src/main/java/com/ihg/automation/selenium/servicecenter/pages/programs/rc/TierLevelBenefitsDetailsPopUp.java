package com.ihg.automation.selenium.servicecenter.pages.programs.rc;

import com.ihg.automation.selenium.common.pages.ClosablePopUpBase;
import com.ihg.automation.selenium.gwt.components.panel.Panel;
import com.ihg.automation.selenium.gwt.components.utils.ByText;

public class TierLevelBenefitsDetailsPopUp extends ClosablePopUpBase
{
    public TierLevelBenefitsDetailsPopUp()
    {
        super("Tier-Level Benefits Details");
    }

    public Panel getBenefitsPanel()
    {
        return container.getPanel(ByText.contains("Benefits"));
    }

    public BenefitsGrid getBenefitsGrid()
    {
        return new BenefitsGrid(getBenefitsPanel());
    }
}
