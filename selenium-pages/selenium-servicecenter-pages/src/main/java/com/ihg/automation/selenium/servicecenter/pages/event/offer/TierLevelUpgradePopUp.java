package com.ihg.automation.selenium.servicecenter.pages.event.offer;

import com.ihg.automation.selenium.common.pages.SubmitPopUpBase;
import com.ihg.automation.selenium.gwt.components.input.Input;

public class TierLevelUpgradePopUp extends SubmitPopUpBase
{
    public TierLevelUpgradePopUp()
    {
        super("Tier-Level Upgrade");
    }

    public Input getMemberId()
    {
        return container.getInputByLabel("Member ID");
    }

}
