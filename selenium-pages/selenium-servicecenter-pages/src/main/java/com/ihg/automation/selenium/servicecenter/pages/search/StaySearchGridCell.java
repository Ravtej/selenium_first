package com.ihg.automation.selenium.servicecenter.pages.search;

import com.ihg.automation.selenium.gwt.components.grid.CellsName;

public enum StaySearchGridCell implements CellsName
{
    MEMBER_NUMBER("loyaltyMembershipId"), //
    PROGRAM("loyaltyProgramCode"), //
    GUEST_NAME("personName"), //
    HOTEL("hotelCode"), //
    BRAND("brandCode"), //
    CHECK_IN("checkIn"), //
    CHECK_OUT("checkOut"), //
    NIGHTS("nights"), //
    ROOM_NUMBER("roomId"), //
    CONFIRMATION_NUMBER("confirmationId"), //
    EVENT_TYPE("stayEventType");

    private String name;

    private StaySearchGridCell(String name)
    {
        this.name = name;
    }

    @Override
    public String getName()
    {
        return name;
    }
}
