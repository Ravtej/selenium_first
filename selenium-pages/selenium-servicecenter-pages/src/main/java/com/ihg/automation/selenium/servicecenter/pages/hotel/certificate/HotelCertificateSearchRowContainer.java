package com.ihg.automation.selenium.servicecenter.pages.hotel.certificate;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasDefaultOrText;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;

import com.ihg.automation.selenium.common.event.EventGridRowContainerWithDetails;
import com.ihg.automation.selenium.common.hotel.HotelCertificate;
import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.label.Label;

public class HotelCertificateSearchRowContainer extends EventGridRowContainerWithDetails
{
    public HotelCertificateSearchRowContainer(Container parent)
    {
        super(parent);
    }

    public Label getHotel()
    {
        return container.getLabel("Hotel");
    }

    public Label getConfirmationNumber()
    {
        return container.getLabel("Confirmation Number");
    }

    public Label getFolioNumber()
    {
        return container.getLabel("Folio Number");
    }

    public Label getCheckIn()
    {
        return container.getLabel("Check In Date");
    }

    public Label getCheckOut()
    {
        return container.getLabel("Check Out Date");
    }

    public Label getItemId()
    {
        return container.getLabel("Item Id");
    }

    public Label getItemName()
    {
        return container.getLabel("Item Name");
    }

    public Label getBundle()
    {
        return container.getLabel("Bundle Number");
    }

    public Label getDescriptionGuestName()
    {
        return container.getLabel("Description/Guest Name");
    }

    public Label getCertificateNumber()
    {
        return container.getLabel("Certificate Number");
    }

    public Label getReimbursementAmount()
    {
        return container.getLabel("Reimbursement Amount");
    }

    public Label getReimbursementMethod()
    {
        return container.getLabel("Reimbursement Method");
    }

    public Label getReimbursementDate()
    {
        return container.getLabel("Reimbursement Date");
    }

    public Label getBillingEntity()
    {
        return container.getLabel("Billing Entity");
    }

    public Label getStatus()
    {
        return container.getLabel("Status");
    }

    public void verify(HotelCertificate detail)
    {
        verifyThat(getBillingEntity(), hasText(detail.getBillingEntity()));

        verifyThat(getHotel(), hasText(detail.getHotelCode()));
        verifyThat(getCheckIn(), hasText(detail.getCheckInDate()));
        verifyThat(getCheckOut(), hasText(detail.getCheckOutDate()));

        verifyThat(getItemId(), hasText(detail.getItemId()));
        verifyThat(getItemName(), hasText(detail.getItemName()));

        verifyThat(getCertificateNumber(), hasText(detail.getCertificateNumber()));
        verifyThat(getConfirmationNumber(), hasText(detail.getConfirmationNumber()));
        verifyThat(getFolioNumber(), hasText(detail.getFolioNumber()));
        verifyThat(getBundle(), hasDefaultOrText(detail.getBundleNumber()));
        verifyThat(getDescriptionGuestName(), hasText(detail.getDescriptionGuestName()));

        verifyThat(getReimbursementAmount(), hasDefaultOrText(detail.getReimbursementAmount()));
        verifyThat(getReimbursementMethod(), hasDefaultOrText(detail.getReimbursementMethod()));
        verifyThat(getReimbursementDate(), hasDefaultOrText(detail.getReimbursementDate()));

        verifyThat(getStatus(), hasText(detail.getStatus()));
    }
}
