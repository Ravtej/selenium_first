package com.ihg.automation.selenium.servicecenter.pages.event.benefit;

import com.ihg.automation.selenium.common.event.EventGridRowContainerWithDetails;
import com.ihg.automation.selenium.gwt.components.Container;

public class BenefitContainer extends EventGridRowContainerWithDetails
{
    public BenefitContainer(Container parent)
    {
        super(parent);
    }

    public BenefitRedeemDetails getBenefitRedeemDetails()
    {
        return new BenefitRedeemDetails(container);
    }
}
