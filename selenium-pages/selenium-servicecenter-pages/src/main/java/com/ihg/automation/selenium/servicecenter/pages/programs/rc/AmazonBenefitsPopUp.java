package com.ihg.automation.selenium.servicecenter.pages.programs.rc;

import com.ihg.automation.selenium.common.pages.ClosablePopUpBase;
import com.ihg.automation.selenium.gwt.components.PopUpWindow;
import com.ihg.automation.selenium.gwt.components.panel.Panel;
import com.ihg.automation.selenium.gwt.components.utils.ByText;

public class AmazonBenefitsPopUp extends ClosablePopUpBase
{
    public AmazonBenefitsPopUp()
    {
        super(PopUpWindow.withBodyText("Amazon Partner Benefit Details", "Amazon Benefits"));
    }

    public Panel getAmazonBenefitsPanel()
    {
        return container.getPanel(ByText.exact("Amazon Benefits"));
    }

    public AmazonBenefitsGrid getAmazonBenefitsGrid()
    {
        return new AmazonBenefitsGrid(getAmazonBenefitsPanel());
    }
}
