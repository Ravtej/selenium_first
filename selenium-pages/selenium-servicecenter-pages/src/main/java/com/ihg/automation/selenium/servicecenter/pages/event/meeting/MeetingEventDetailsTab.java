package com.ihg.automation.selenium.servicecenter.pages.event.meeting;

import com.ihg.automation.selenium.common.pages.TabCustomContainerBase;

public class MeetingEventDetailsTab extends TabCustomContainerBase
{
    public MeetingEventDetailsTab(MeetingEventDetailsPopUp parent)
    {
        super(parent, "Meeting Event Details");
    }

    public MeetingEventDetails getDetails()
    {
        return new MeetingEventDetails(container);
    }
}
