package com.ihg.automation.selenium.servicecenter.pages.event.voucher;

import com.ihg.automation.selenium.common.pages.TabCustomContainerBase;

public class VoucherDetailsTab extends TabCustomContainerBase
{
    public VoucherDetailsTab(VoucherDetailsPopUp parent)
    {
        super(parent, "Voucher Details");
    }

    public VoucherDetails getVoucherDetails()
    {
        return new VoucherDetails(container);
    }
}
