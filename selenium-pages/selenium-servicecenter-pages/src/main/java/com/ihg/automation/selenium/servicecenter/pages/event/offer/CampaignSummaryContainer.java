package com.ihg.automation.selenium.servicecenter.pages.event.offer;

import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.CustomContainerBase;
import com.ihg.automation.selenium.gwt.components.Separator;
import com.ihg.automation.selenium.gwt.components.label.Label;

public class CampaignSummaryContainer extends CustomContainerBase
{
    public CampaignSummaryContainer(Container container)
    {
        super(container.getSeparator("Campaign Summary", Separator.FIELDSET_PATTERN_NEXT_TR));
    }

    public Label getEarned()
    {
        return container.getLabel("Earned");
    }

    public Label getOffersCompleted()
    {
        return container.getLabel("Offers Completed");
    }

    public Label getDaysLeftToEarn()
    {
        return container.getLabel("Days Left To Earn");
    }

}
