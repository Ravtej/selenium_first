package com.ihg.automation.selenium.servicecenter.pages.event.stay;

import com.ihg.automation.selenium.common.pages.TabCustomContainerBase;

public class StayDetailsTab extends TabCustomContainerBase
{
    public StayDetailsTab(AdjustStayPopUp parent)
    {
        super(parent, "Stay Details");
    }

    public StayDetailsAdjust getStayDetails()
    {
        return new StayDetailsAdjust(this);
    }
}
