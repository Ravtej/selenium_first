package com.ihg.automation.selenium.servicecenter.pages.personal;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.isSelected;

import com.ihg.automation.selenium.common.contact.Address;
import com.ihg.automation.selenium.common.member.GuestAddress;
import com.ihg.automation.selenium.common.personal.EditableContactBase;
import com.ihg.automation.selenium.gwt.components.Mode;
import com.ihg.automation.selenium.gwt.components.checkbox.CheckBox;
import com.ihg.automation.selenium.gwt.components.panel.EditablePanel;

public class AddressContact extends EditableContactBase<GuestAddress, Address>
{
    public AddressContact(EditablePanel parent)
    {
        super(parent, new Address(parent));
    }

    public CheckBox getDoNotClean()
    {
        return container.getCheckBox("Do not clean");
    }

    @Override
    public void populate(GuestAddress contact)
    {
        super.populate(contact);
        getDoNotClean().check(contact.isDoNotChangeLock());
    }

    @Override
    public void verify(GuestAddress contact, Mode mode)
    {
        super.verify(contact, mode);

        if (mode == Mode.EDIT)
        {
            verifyThat(getDoNotClean(), isSelected(contact.isDoNotChangeLock()));
        }
    }
}
