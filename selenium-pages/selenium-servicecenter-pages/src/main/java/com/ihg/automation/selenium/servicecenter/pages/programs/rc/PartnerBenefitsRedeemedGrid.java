package com.ihg.automation.selenium.servicecenter.pages.programs.rc;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static com.ihg.automation.selenium.servicecenter.pages.programs.rc.PartnerBenefitsRedeemedGridRow.PartnerBenefitsRedeemedCell.NAME;
import static com.ihg.automation.selenium.servicecenter.pages.programs.rc.PartnerBenefitsRedeemedGridRow.PartnerBenefitsRedeemedCell.REDEEMED;

import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.grid.Grid;
import com.ihg.automation.selenium.gwt.components.grid.GridHeader;

public class PartnerBenefitsRedeemedGrid
        extends Grid<PartnerBenefitsRedeemedGridRow, PartnerBenefitsRedeemedGridRow.PartnerBenefitsRedeemedCell>
{
    public PartnerBenefitsRedeemedGrid(Container parent)
    {
        super(parent);
    }

    public void verifyHeader()
    {
        GridHeader<PartnerBenefitsRedeemedGridRow.PartnerBenefitsRedeemedCell> header = getHeader();
        verifyThat(header.getCell(NAME), hasText(NAME.getValue()));
        verifyThat(header.getCell(REDEEMED), hasText(REDEEMED.getValue()));
    }

}
