package com.ihg.automation.selenium.servicecenter.pages.event.meeting;

import com.ihg.automation.selenium.gwt.components.Component;
import com.ihg.automation.selenium.gwt.components.grid.CellsName;
import com.ihg.automation.selenium.gwt.components.grid.Grid;
import com.ihg.automation.selenium.gwt.components.grid.GridRowXpath;

public class MeetingEventGrid extends Grid<MeetingEventGridRow, MeetingEventGrid.MeetingEventCell>
{

    public MeetingEventGrid(Component parent)
    {
        super(parent);
    }

    public MeetingEventGridRow getRow(String transType)
    {
        return getRow(new GridRowXpath.GridRowXpathBuilder().cell(MeetingEventCell.TRANS_TYPE, transType).build());
    }

    public MeetingEventGridRow getRow(MeetingEventRow meetingEventRow)
    {
        return getRow(
                new GridRowXpath.GridRowXpathBuilder().cell(MeetingEventCell.TRANS_TYPE, meetingEventRow.getTransType())
                        .cell(MeetingEventCell.MEETING_EVENT_NAME, meetingEventRow.getMeetingName()).build());
    }

    public enum MeetingEventCell implements CellsName
    {
        TRANS_DATE("transactionDate"), TRANS_TYPE("transactionType"), HOTEL("hotel"), CHECK_IN_DATE(
                "checkInDate"), CHECK_OUT_DATE("checkOutDate"), MEETING_EVENT_NAME("meetingName"), IHG_UNITS(
                        "loyaltyUnits.ihgUnits.amount"), ALLIANCE_UNITS("loyaltyUnits.allianceUnits.amount");

        private String name;

        private MeetingEventCell(String name)
        {
            this.name = name;
        }

        @Override
        public String getName()
        {
            return name;
        }
    }

}
