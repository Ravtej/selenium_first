package com.ihg.automation.selenium.servicecenter.pages.event.goodwill;

import com.ihg.automation.selenium.common.event.EventGridRowContainerWithDetails;
import com.ihg.automation.selenium.gwt.components.Container;

public class GoodwillGridRowContainer extends EventGridRowContainerWithDetails
{
    public GoodwillGridRowContainer(Container parent)
    {
        super(parent);
    }

    public GoodwillDetails getGoodwillDetails()
    {
        return new GoodwillDetails(container);
    }
}
