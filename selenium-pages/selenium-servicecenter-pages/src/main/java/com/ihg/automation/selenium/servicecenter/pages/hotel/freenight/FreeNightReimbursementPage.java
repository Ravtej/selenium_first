package com.ihg.automation.selenium.servicecenter.pages.hotel.freenight;

import com.ihg.automation.selenium.common.event.SearchAware;
import com.ihg.automation.selenium.common.event.SearchButtonBar;
import com.ihg.automation.selenium.common.pages.NavigatePage;
import com.ihg.automation.selenium.gwt.components.Button;
import com.ihg.automation.selenium.gwt.components.CustomContainerBase;
import com.ihg.automation.selenium.listener.Verifier;
import com.ihg.automation.selenium.servicecenter.pages.Tabs;
import com.ihg.automation.selenium.servicecenter.pages.TopMenu;

public class FreeNightReimbursementPage extends CustomContainerBase implements NavigatePage, SearchAware
{
    TopMenu menu = new TopMenu();

    public FreeNightReimbursementPage()
    {
        super(new Tabs().getReimbursement().getContainer());
    }

    @Override
    public void goTo()
    {
        menu.getHotelOperations().clickAndWait();
        menu.getHotelOperations().getRewardAndFreeNightReimb().clickAndWait();
    }

    public FreeNightReimbursementSearchGrid getGrid()
    {
        return new FreeNightReimbursementSearchGrid(container);
    }

    @Override
    public FreeNightReimbursementSearch getSearchFields()
    {
        return new FreeNightReimbursementSearch(container);
    }

    public void search(String hotel, String status, String fromDate, String toDate, String type, Verifier... verifiers)
    {
        getButtonBar().getClearCriteria().clickAndWait();
        getSearchFields().populateFields(hotel, status, fromDate, toDate, type);
        getButtonBar().getSearch().clickAndWait();
    }

    public void searchByHotel(String hotel, Verifier... verifiers)
    {
        getButtonBar().getClearCriteria().clickAndWait();
        getSearchFields().getHotelCode().typeAndWait(hotel);
        getButtonBar().getSearch().clickAndWait(verifiers);
    }

    public void searchByHotelAndDate(String hotel, String specificDate, Verifier... verifiers)
    {
        search(hotel, null, specificDate, specificDate, null, verifiers);
    }

    @Override
    public FreeNightReimbursementSearchButtonBar getButtonBar()
    {
        return new FreeNightReimbursementSearchButtonBar(this);
    }

    public class FreeNightReimbursementSearchButtonBar extends SearchButtonBar
    {

        public FreeNightReimbursementSearchButtonBar(FreeNightReimbursementPage page)
        {
            super(page.container);
        }

        public Button getApproveSelected()
        {
            return container.getButton("Approve Selected");
        }

        public void clickApproveSelected()
        {
            getApproveSelected().clickAndWait();
        }

    }

}
