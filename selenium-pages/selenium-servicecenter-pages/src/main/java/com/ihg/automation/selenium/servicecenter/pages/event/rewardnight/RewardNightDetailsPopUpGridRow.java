package com.ihg.automation.selenium.servicecenter.pages.event.rewardnight;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;

import org.hamcrest.Matcher;

import com.ihg.automation.selenium.gwt.components.grid.Grid;
import com.ihg.automation.selenium.gwt.components.grid.GridRow;
import com.ihg.automation.selenium.gwt.components.grid.GridRowXpath;
import com.ihg.automation.selenium.servicecenter.pages.event.rewardnight.RewardNightDetailsPopUpGrid.RewardNightPopUpCell;

public class RewardNightDetailsPopUpGridRow extends GridRow<RewardNightPopUpCell>
{

    public RewardNightDetailsPopUpGridRow(Grid parent, GridRowXpath xpath)
    {
        super(parent, xpath);
    }

    public void verify(Matcher<String> status, String suffix, String loyaltyUnits)
    {
        verifyThat(getCell(RewardNightPopUpCell.STATUS), hasText(status));
        verifyThat(getCell(RewardNightPopUpCell.SUFFIX), hasText(suffix));
        verifyThat(getCell(RewardNightPopUpCell.LOYALTY_UNITS), hasText(loyaltyUnits));
    }
}
