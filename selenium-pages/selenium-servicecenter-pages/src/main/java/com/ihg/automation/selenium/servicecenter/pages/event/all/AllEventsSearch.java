package com.ihg.automation.selenium.servicecenter.pages.event.all;

import static com.ihg.automation.selenium.common.components.RangeSelect.Range.ZERO_MONTH;
import static com.ihg.automation.selenium.gwt.components.checkbox.CheckBox.CHECK_BOX_PATTERN2;

import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.CustomContainerBase;
import com.ihg.automation.selenium.gwt.components.checkbox.CheckBox;
import com.ihg.automation.selenium.gwt.components.input.Input;
import com.ihg.automation.selenium.gwt.components.input.Select;
import com.ihg.automation.selenium.gwt.components.utils.ByText;
import com.ihg.automation.selenium.servicecenter.pages.event.EventDateRange;

public class AllEventsSearch extends CustomContainerBase
{
    public AllEventsSearch(Container container)
    {
        super(container.getSeparator("All Events Search"));
    }

    public Select getEventType()
    {
        return container.getSelectByLabel("Event Type");
    }

    public EventDateRange getTransactionDate()
    {
        return new EventDateRange(container, "Transaction Date");
    }

    public CheckBox getLoyaltyEventsOnly()
    {
        return new CheckBox(container, ByText.exact("Loyalty Unit Events Only"), CHECK_BOX_PATTERN2);
    }

    public Select getTransactionType()
    {
        return container.getSelectByLabel("Transaction Type");
    }

    public Input getKeywords()
    {
        return container.getInputByLabel("Keywords");
    }

    public void searchEvent(String eventType, String eventTransactionType, String date, String keywords,
            boolean unitsOnly)
    {
        getEventType().select(eventType);
        getTransactionType().select(eventTransactionType);
        getTransactionDate().populate(date, ZERO_MONTH);
        getKeywords().type(keywords);
        getLoyaltyEventsOnly().check(unitsOnly);
    }
}
