package com.ihg.automation.selenium.servicecenter.pages.event.level;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;

import com.ihg.automation.selenium.common.EventSource;
import com.ihg.automation.selenium.common.EventSourceAware;
import com.ihg.automation.selenium.common.SiteHelperBase;
import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.CustomContainerBase;
import com.ihg.automation.selenium.gwt.components.label.Label;

public class ManualTierLevelChangeGridRowContainer extends CustomContainerBase implements EventSourceAware
{

    public ManualTierLevelChangeGridRowContainer(Container parent)
    {
        super(parent);
    }

    @Override
    public EventSource getSource()
    {
        return new EventSource(container);
    }

    public Label getReferringMember()
    {
        return container.getLabel("Referring Member");
    }

    public Label getReasonCode()
    {
        return container.getLabel("Reason Code");
    }

    public Label getCollateralItem()
    {
        return container.getLabel("Collateral Item");
    }

    public void verify(String referringMember, String reasonCode, String collateralItem, SiteHelperBase siteHelperBase)
    {
        verifyThat(getReferringMember(), hasText(referringMember));
        verifyThat(getReasonCode(), hasText(reasonCode));
        verifyThat(getCollateralItem(), hasText(collateralItem));
        getSource().verify(siteHelperBase.getSourceFactory().getNoEmployeeIdSource());
    }
}
