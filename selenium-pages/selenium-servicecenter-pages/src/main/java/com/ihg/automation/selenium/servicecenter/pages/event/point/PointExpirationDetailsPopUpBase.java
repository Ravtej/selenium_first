package com.ihg.automation.selenium.servicecenter.pages.event.point;

import com.ihg.automation.selenium.common.billing.EventBillingDetailsTab;
import com.ihg.automation.selenium.common.billing.EventBillingTabAware;
import com.ihg.automation.selenium.common.earning.EventEarningDetailsTab;
import com.ihg.automation.selenium.common.earning.EventEarningTabAware;
import com.ihg.automation.selenium.common.pages.ClosablePopUpBase;

public class PointExpirationDetailsPopUpBase extends ClosablePopUpBase
        implements EventEarningTabAware, EventBillingTabAware
{

    public PointExpirationDetailsPopUpBase(String header)
    {
        super(header);
    }

    @Override
    public EventEarningDetailsTab getEarningDetailsTab()
    {
        return new EventEarningDetailsTab(this);
    }

    @Override
    public EventBillingDetailsTab getBillingDetailsTab()
    {
        return new EventBillingDetailsTab(this);
    }

    public PointExpirationDetailsTab getPointExpirationDetailsTab()
    {
        return new PointExpirationDetailsTab(this);
    }
}
