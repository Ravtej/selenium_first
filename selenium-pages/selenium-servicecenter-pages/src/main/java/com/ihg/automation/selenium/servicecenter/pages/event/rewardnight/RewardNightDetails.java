package com.ihg.automation.selenium.servicecenter.pages.event.rewardnight;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;

import com.ihg.automation.selenium.common.reward.RewardNight;
import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.CustomContainerBase;
import com.ihg.automation.selenium.gwt.components.Link;
import com.ihg.automation.selenium.gwt.components.input.DateInput;
import com.ihg.automation.selenium.gwt.components.input.Input;
import com.ihg.automation.selenium.gwt.components.label.Label;
import com.ihg.automation.selenium.matchers.component.ComponentMatcher;

public class RewardNightDetails extends CustomContainerBase
{

    public RewardNightDetails(Container container)
    {
        super(container);
    }

    public Label getConfirmationNumber()
    {
        return container.getLabel("Confirmation Number");
    }

    public Link getHotel()
    {
        return new Link(container.getLabel("Hotel"));
    }

    public Label getRoomType()
    {
        return container.getLabel("Room Type");
    }

    public Input getNumberOfRooms()
    {
        return container.getInputByLabel("Number of Rooms");
    }

    public DateInput getCheckInDate()
    {
        return new DateInput(container.getLabel("Check In"));
    }

    public DateInput getCheckOutDate()
    {
        return new DateInput(container.getLabel("Check Out"));
    }

    public Input getNights()
    {
        return container.getInputByLabel("Nights");
    }

    public Label getTransactionDate()
    {
        return container.getLabel("Transaction Date");
    }

    public Label getTransactionType()
    {
        return container.getLabel("Transaction Type");
    }

    public Label getCertificateNumber()
    {
        return container.getLabel("Certificate Number");
    }

    public Label getBookingSource()
    {
        return container.getLabel("Booking Source");
    }

    public Label getLoyaltyUnits()
    {
        return container.getLabel("Total Loyalty Units");
    }

    public Label getTransactionDescription()
    {
        return container.getLabel("Description");
    }

    public void verify(RewardNight night)
    {
        verifyThat(getConfirmationNumber(), hasText(night.getConfirmationNumber()));
        verifyThat(getHotel(), hasText(night.getHotel()));
        verifyThat(getRoomType(), hasText(night.getRoomType()));
        verifyThat(getNumberOfRooms(), hasText(night.getNumberOfRooms()));
        verifyThat(getCheckInDate(), hasText(night.getCheckIn()));
        verifyThat(getCheckOutDate(), hasText(night.getCheckOut()));
        verifyThat(getNights(), hasText(night.getNights()));

        verifyThat(getTransactionDate(), hasText(night.getTransactionDate()));
        verifyThat(getTransactionType(), hasText(night.getTransactionType()));
        verifyThat(getCertificateNumber(), hasText(night.getCertificateNumber()));
        verifyThat(getBookingSource(), hasText(night.getBookingSource()));
        verifyThat(getLoyaltyUnits(), hasText(night.getLoyaltyUnits()));
        verifyThat(getTransactionDescription(), hasText(night.getTransactionDescription()));
    }

    public void updateCheckInDate(String checkInDate, String nightsBeforeChange, String nightsAfterChange)
    {
        verifyThat(getNights(), ComponentMatcher.hasText(nightsBeforeChange));
        getCheckInDate().typeAndWait(checkInDate);
        verifyThat(getNights(), ComponentMatcher.hasText(nightsAfterChange));
    }
}
