package com.ihg.automation.selenium.servicecenter.pages.annual;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.Constant.NOT_AVAILABLE;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasTextAsInt;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;

import org.springframework.jdbc.core.JdbcTemplate;

import com.ihg.automation.common.DateUtils;
import com.ihg.automation.selenium.common.Constant;
import com.ihg.automation.selenium.common.annual.ActivityAmount;
import com.ihg.automation.selenium.common.member.AnnualActivity;
import com.ihg.automation.selenium.common.member.LifetimeActivity;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberJdbcUtils;
import com.ihg.automation.selenium.common.pages.TabCustomContainerBase;
import com.ihg.automation.selenium.gwt.components.Component;
import com.ihg.automation.selenium.servicecenter.pages.Tabs;
import com.ihg.automation.selenium.servicecenter.pages.annual.AnnualActivityRow.AnnualActivityCell;

public class AnnualActivityPage extends TabCustomContainerBase
{

    public AnnualActivityPage()
    {
        super(new Tabs().getProgramInfo().getAnnualActivity());
    }

    public ActivityAmount getBaseUnits()
    {
        return new ActivityAmount(container.getLabel("Base Loyalty Units"));
    }

    public ActivityAmount getBonusUnits()
    {
        return new ActivityAmount(container.getLabel("Bonus Loyalty Units"));
    }

    public ActivityAmount getAdjustedUnits()
    {
        return new ActivityAmount(container.getLabel("Adjusted Loyalty Units"));
    }

    public ActivityAmount getTotalUnits()
    {
        return new ActivityAmount(container.getLabel("Total Loyalty Units"));
    }

    public ActivityAmount getRedeemedUnits()
    {
        return new ActivityAmount(container.getLabel("Redeemed Loyalty Units"));
    }

    public ActivityAmount getCurrentBalance()
    {
        return new ActivityAmount(container.getLabel("Current Balance"));
    }

    public AnnualActivityGrid getAnnualActivities()
    {
        return new AnnualActivityGrid(container);
    }

    public LifetimeActivity getLifetimeActivityCounters()
    {
        LifetimeActivity lifetime = new LifetimeActivity();

        lifetime.setBaseUnitsFromString(getBaseUnits().getAmount().getText());
        lifetime.setBonusUnitsFromString(getBonusUnits().getAmount().getText());
        lifetime.setAdjustedUnitsFromString(getAdjustedUnits().getAmount().getText());
        lifetime.setTotalUnitsFromString(getTotalUnits().getAmount().getText());
        lifetime.setRedeemedUnitsFromString(getRedeemedUnits().getAmount().getText());
        lifetime.setCurrentBalanceFromString(getCurrentBalance().getAmount().getText());

        return lifetime;
    }

    public AnnualActivity getAnnualActivityCounters(int year)
    {
        AnnualActivity annual = new AnnualActivity();
        AnnualActivityRow row = getAnnualActivities().getRowByYear(year);

        annual.setDate(year);
        annual.setTierLevelNightsFromString(row.getCell(AnnualActivityCell.TIER_LEVEL_NIGHT).getText());
        annual.setRewardNightsFromString(row.getCell(AnnualActivityCell.REWARD_NIGHT).getText());
        annual.setQualifiedNightsFromString(row.getCell(AnnualActivityCell.QUALIFIED_ROOM).getText());
        annual.setNonQualifiedNightsFromString(row.getCell(AnnualActivityCell.NON_QUALIFIED_ROOM).getText());
        annual.setQualifiedChainsFromString(row.getCell(AnnualActivityCell.QUALIFIED_CHAIN).getText());
        annual.setTotalQualifiedUnitsFromString(row.getCell(AnnualActivityCell.TOTAL_QUALIFIED).getText());
        annual.setTotalUnitsFromString(row.getCell(AnnualActivityCell.TOTAL_UNIT).getText());

        return annual;
    }

    public AnnualActivity getAnnualActivityCounters()
    {
        return getAnnualActivityCounters(DateUtils.currentYear());
    }

    public void verifyLifetimeActivities(LifetimeActivity activity)
    {
        final String UNIT_TYPE = Constant.RC_POINTS;
        verifyThat(getAdjustedUnits().getAmount(), hasTextAsInt(activity.getAdjustedUnits()));
        verifyThat(getBaseUnits().getAmount(), hasTextAsInt(activity.getBaseUnits()));
        Component amount = getBonusUnits().getAmount();
        verifyThat(amount, hasTextAsInt(activity.getBonusUnits()));
        if (!amount.getText().equals(NOT_AVAILABLE))
        {
            verifyThat(getBonusUnits().getUnitType(), hasText(UNIT_TYPE));
        }
        verifyThat(getTotalUnits().getAmount(), hasTextAsInt(activity.getTotalUnits()));
        verifyThat(getTotalUnits().getUnitType(), hasText(UNIT_TYPE));
        verifyThat(getRedeemedUnits().getAmount(), hasTextAsInt(activity.getRedeemedUnits()));
        verifyThat(getCurrentBalance().getAmount(), hasTextAsInt(activity.getCurrentBalance()));
        verifyThat(getCurrentBalance().getUnitType(), hasText(UNIT_TYPE));
    }

    public Member captureCounters(Member member, JdbcTemplate jdbcTemplate)
    {
        MemberJdbcUtils.waitAnnualActivitiesCounter(jdbcTemplate, member);

        goTo();
        member.setLifetimeActivity(getLifetimeActivityCounters());
        member.setAnnualActivity(getAnnualActivityCounters());

        return member;
    }

    public void verifyCounters(Member member)
    {
        goTo();
        verifyLifetimeActivities(member.getLifetimeActivity());
        getAnnualActivities().getCurrentYearRow().verify(member.getAnnualActivity());
    }
}
