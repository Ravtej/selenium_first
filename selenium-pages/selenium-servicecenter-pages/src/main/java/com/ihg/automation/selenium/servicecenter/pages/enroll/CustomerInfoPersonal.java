package com.ihg.automation.selenium.servicecenter.pages.enroll;

import com.ihg.automation.selenium.common.enroll.CustomerInfo;
import com.ihg.automation.selenium.common.name.PersonNameCustomerFields;
import com.ihg.automation.selenium.common.name.PersonNameFields;
import com.ihg.automation.selenium.gwt.components.Container;

public class CustomerInfoPersonal extends CustomerInfo
{

    public CustomerInfoPersonal(Container container)
    {
        super(container);
    }

    @Override
    public PersonNameFields getName()
    {
        return new PersonNameCustomerFields(container);
    }
}
