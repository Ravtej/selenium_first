package com.ihg.automation.selenium.servicecenter.pages.preferences;

import com.ihg.automation.selenium.common.member.CreditCard;
import com.ihg.automation.selenium.common.member.OccupationInfo;
import com.ihg.automation.selenium.common.preferences.TravelProfileType;

public class TravelProfile
{
    private String profileName;
    private TravelProfileType profileType;
    private CreditCard creditCard;
    private OccupationInfo corporateId;
    private boolean possibleToDelete;

    public TravelProfile(String profileName, TravelProfileType profileType, boolean possibleToDelete)
    {
        this.profileName = profileName;
        this.profileType = profileType;
        this.possibleToDelete = possibleToDelete;

    }

    public TravelProfile(String profileName, TravelProfileType profileType)
    {
        this(profileName, profileType, false);
    }

    public String getProfileName()
    {
        return profileName;
    }

    public void setProfileName(String profileName)
    {
        this.profileName = profileName;
    }

    public TravelProfileType getProfileType()
    {
        return profileType;
    }

    public void setProfileType(TravelProfileType profileType)
    {
        this.profileType = profileType;
    }

    public CreditCard getCreditCard()
    {
        return creditCard;
    }

    public void setCreditCard(CreditCard creditCard)
    {
        this.creditCard = creditCard;
    }

    public OccupationInfo getCorporateId()
    {
        return corporateId;
    }

    public void setCorporateId(OccupationInfo corporateId)
    {
        this.corporateId = corporateId;
    }

    public boolean isPossibleToDelete()
    {
        return possibleToDelete;
    }

    public void setPossibleToDelete(boolean possibleToDelete)
    {
        this.possibleToDelete = possibleToDelete;
    }

    public static TravelProfile getDefaultTravelProfile()
    {
        return new TravelProfile(TravelProfileType.LEISURE.getValue(), TravelProfileType.LEISURE);
    }
}
