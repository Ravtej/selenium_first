package com.ihg.automation.selenium.servicecenter.pages.account;

import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyNoError;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;

import com.ihg.automation.selenium.common.pages.TabCustomContainerBase;
import com.ihg.automation.selenium.common.types.program.ProgramStatus;
import com.ihg.automation.selenium.gwt.components.Button;
import com.ihg.automation.selenium.gwt.components.dialog.ConfirmDialog;
import com.ihg.automation.selenium.gwt.components.label.Label;
import com.ihg.automation.selenium.servicecenter.pages.Tabs;

public class AccountInfoPage extends TabCustomContainerBase
{
    public AccountInfoPage()
    {
        super(new Tabs().getCustomerInfo().getAccountInfo());
    }

    public Label getAccountStatus()
    {
        return container.getLabel("Account Status");
    }

    public Label getStatusReason()
    {
        return container.getLabel("Status Reason");
    }

    public Button getRemoveAccount()
    {
        return container.getButton(Button.REMOVE_ACCOUNT);
    }

    public void clickRemoveAccount()
    {
        getRemoveAccount().clickAndWait();
    }

    public Security getSecurity()
    {
        return new Security(container);
    }

    public Comments getComments()
    {
        return new Comments(container);
    }

    public Flags getFlags()
    {
        return new Flags(container);
    }

    public void verify(String accountStatus)
    {
        verifyThat(getAccountStatus(), hasText(accountStatus));
    }

    public Button getReinstateAccount()
    {
        return container.getButton(Button.REINSTATE_ACCOUNT);
    }

    public void clickReinstateAccount()
    {
        getReinstateAccount().clickAndWait();
    }

    public FailedSelfServiceLoginAttempts getFailedSelfServiceLoginAttempts()
    {
        return new FailedSelfServiceLoginAttempts(container);
    }

    public void verifyStatus(String status)
    {
        goTo();
        verifyThat(getAccountStatus(), hasText(status));
    }

    public void removedAccount(boolean isConfirm)
    {
        goTo();
        clickRemoveAccount();

        if (isConfirm)
        {
            ConfirmDialog dialog = new ConfirmDialog();
            dialog.clickYes(verifyNoError());
        }

        verifyThat(getAccountStatus(), hasText(ProgramStatus.REMOVED));
    }

    public void removedAccount()
    {
        removedAccount(true);
    }
}
