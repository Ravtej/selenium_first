package com.ihg.automation.selenium.servicecenter.pages.event.offer;

import com.ihg.automation.selenium.common.offer.Offer;
import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.CustomContainerBase;
import com.ihg.automation.selenium.servicecenter.pages.event.freenight.FreeNightDetailsContainer;

public class OfferDetailsViewBase extends CustomContainerBase
{

    public OfferDetailsViewBase(Container parent)
    {
        super(parent);
    }

    public OfferDetailsContainer getOfferDetails()
    {
        return new OfferDetailsContainer(container);
    }

    public FreeNightDetailsContainer getFreeNightDetails()
    {
        return new FreeNightDetailsContainer(container);
    }

    public OfferDatesContainer getOfferDates()
    {
        return new OfferDatesContainer(container);
    }

    public OfferEligibilityContainer getOfferEligibility()
    {
        return new OfferEligibilityContainer(container);
    }

    public void verify(Offer offer)
    {
        getOfferDetails().verify(offer);
        getFreeNightDetails().verify(offer);
        getOfferDates().verify(offer);
        getOfferEligibility().verify(offer);
    }

    public Offer capture()
    {
        Offer.Builder builder = Offer.builder();
        getOfferDetails().capture(builder);
        getOfferDates().capture(builder);
        getOfferEligibility().capture(builder);
        getFreeNightDetails().capture(builder);

        return builder.build();
    }
}
