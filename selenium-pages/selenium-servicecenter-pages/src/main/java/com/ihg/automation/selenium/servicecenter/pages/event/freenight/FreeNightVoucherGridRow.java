package com.ihg.automation.selenium.servicecenter.pages.event.freenight;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasAnyText;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasTextWithWait;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static org.hamcrest.CoreMatchers.allOf;
import static org.hamcrest.Matchers.endsWith;
import static org.hamcrest.Matchers.startsWith;

import com.ihg.automation.selenium.common.freenight.VoucherRedemption;
import com.ihg.automation.selenium.gwt.components.grid.CellsName;
import com.ihg.automation.selenium.gwt.components.grid.GridRow;
import com.ihg.automation.selenium.gwt.components.grid.GridRowXpath;

public class FreeNightVoucherGridRow extends GridRow<FreeNightVoucherGridRow.FreeNightVoucherCell>
{
    public FreeNightVoucherGridRow(FreeNightVoucherGrid parent, GridRowXpath xpath)
    {
        super(parent, xpath);
    }

    public void verify(VoucherRedemption voucher)
    {
        verifyThat(getCell(FreeNightVoucherCell.EARN_DATE), hasText(voucher.getEarningDate()));
        // TODO add verification from DB
        verifyThat(getCell(FreeNightVoucherCell.VOUCHER_NUMBER), hasAnyText());
        // TODO add verification from DB
        verifyThat(getCell(FreeNightVoucherCell.BOOK_DATES), hasAnyText());
        verifyThat(getCell(FreeNightVoucherCell.STATUS), hasTextWithWait(voucher.getStatus()));
        verifyThat(getCell(FreeNightVoucherCell.REDEEMED_DATE), hasText(voucher.getRedeemDate()));
        verifyThat(getCell(FreeNightVoucherCell.CONFIRMATION), hasText(voucher.getConfirmation()));
        verifyThat(getCell(FreeNightVoucherCell.HOTEL), hasText(voucher.getHotel()));
    }

    public void verifyBookingDates(VoucherRedemption voucher)
    {
        verifyThat(getCell(FreeNightVoucherCell.BOOK_DATES),
                allOf(hasText(startsWith(voucher.getStartBookDates())), hasText(endsWith(voucher.getEndBookDates()))));
    }

    public enum FreeNightVoucherCell implements CellsName
    {
        EARN_DATE("earnDate"), VOUCHER_NUMBER("voucherNumber"), BOOK_DATES("bookDates"), STATUS(
                "status"), REDEEMED_DATE("redeemDate"), CONFIRMATION("confirmationNumber"), HOTEL("hotelCode");

        private String name;

        private FreeNightVoucherCell(String name)
        {
            this.name = name;
        }

        @Override
        public String getName()
        {
            return name;
        }
    }

}
