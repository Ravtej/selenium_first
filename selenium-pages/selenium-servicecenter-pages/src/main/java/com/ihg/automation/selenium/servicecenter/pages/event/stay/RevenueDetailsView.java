package com.ihg.automation.selenium.servicecenter.pages.event.stay;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasTextAsFormattedDouble;

import com.ihg.automation.selenium.common.stay.Revenues;
import com.ihg.automation.selenium.common.stay.Stay;
import com.ihg.automation.selenium.gwt.components.CustomContainerBase;
import com.ihg.automation.selenium.gwt.components.label.Label;

public class RevenueDetailsView extends RevenueDetailsBase
{
    public RevenueDetailsView(CustomContainerBase parent)
    {
        super(parent, "Revenue details");
    }

    @Override
    public Label getTotalRoom()
    {
        return container.getLabel(TOTAL_ROOM_LABEL);
    }

    @Override
    public Label getFood()
    {
        return container.getLabel(FOOD_LABEL);
    }

    @Override
    public Label getBeverage()
    {
        return container.getLabel(BEVERAGE_LABEL);
    }

    @Override
    public Label getPhone()
    {
        return container.getLabel(PHONE_LABEL);
    }

    @Override
    public Label getMeeting()
    {
        return container.getLabel(MEETING_LABEL);
    }

    @Override
    public Label getMandatoryRevenue()
    {
        return container.getLabel(MANDATORY_REVENUE_LABEL);
    }

    @Override
    public Label getOtherRevenue()
    {
        return container.getLabel(OTHER_REVENUE_LABEL);
    }

    @Override
    public Label getNoRevenue()
    {
        return container.getLabel(NO_REVENUE_LABEL);
    }

    @Override
    public Label getRoomTax()
    {
        return container.getLabel(ROOM_TAX_LABEL);
    }

    @Override
    public Label getTotalRevenue()
    {
        return container.getLabel(TOTAL_REVENUE_LABEL);
    }

    @Override
    public Label getTotalNonQualifyingRevenue()
    {
        return container.getLabel(TOTAL_NON_QUALIFYING_REVENUE_LABEL);
    }

    @Override
    public Label getTotalQualifyingRevenue()
    {
        return container.getLabel(TOTAL_QUALIFYING_REVENUE_LABEL);
    }

    public Label getAverageRoomRate()
    {
        return container.getLabel("Average Room Rate");
    }

    @Override
    public void verify(Revenues revenues)
    {
        verifyThat(getTotalRoom(), hasTextAsFormattedDouble(revenues.getRoom().getAmount()));
        verifyThat(getFood(), hasTextAsFormattedDouble(revenues.getFood().getAmount()));
        verifyThat(getBeverage(), hasTextAsFormattedDouble(revenues.getBeverage().getAmount()));
        verifyThat(getPhone(), hasTextAsFormattedDouble(revenues.getPhone().getAmount()));
        verifyThat(getMeeting(), hasTextAsFormattedDouble(revenues.getMeeting().getAmount()));
        verifyThat(getMandatoryRevenue(), hasTextAsFormattedDouble(revenues.getMandatoryLoyalty().getAmount()));
        verifyThat(getOtherRevenue(), hasTextAsFormattedDouble(revenues.getOtherLoyalty().getAmount()));
        verifyThat(getNoRevenue(), hasTextAsFormattedDouble(revenues.getNoLoyalty().getAmount()));
        verifyThat(getRoomTax(), hasTextAsFormattedDouble(revenues.getTax().getAmount()));
        verifyThat(getTotalRevenue(), hasTextAsFormattedDouble(revenues.getOverallTotal().getAmount()));
        verifyThat(getTotalNonQualifyingRevenue(),
                hasTextAsFormattedDouble(revenues.getTotalNonQualifying().getAmount()));
        verifyThat(getTotalQualifyingRevenue(), hasTextAsFormattedDouble(revenues.getTotalQualifying().getAmount()));
    }

    public void verify(Stay stay)
    {
        verifyThat(getAverageRoomRate(), hasTextAsFormattedDouble(stay.getAvgRoomRate()));
        verify(stay.getRevenues());
    }
}
