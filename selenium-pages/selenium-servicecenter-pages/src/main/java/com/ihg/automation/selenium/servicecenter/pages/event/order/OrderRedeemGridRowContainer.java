package com.ihg.automation.selenium.servicecenter.pages.event.order;

import com.ihg.automation.selenium.common.Constant;
import com.ihg.automation.selenium.common.EventSourceAware;
import com.ihg.automation.selenium.common.event.EventGridRowContainerWithDetails;
import com.ihg.automation.selenium.common.event.EventSourceExtended;
import com.ihg.automation.selenium.common.order.Order;
import com.ihg.automation.selenium.common.order.OrderItem;
import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.common.order.ShippingInfoView;

public class OrderRedeemGridRowContainer extends EventGridRowContainerWithDetails implements EventSourceAware
{
    public OrderRedeemGridRowContainer(Container parent)
    {
        super(parent);
    }

    public OrderDetailsView getOrderDetails()
    {
        return new OrderDetailsView(container);
    }

    public OrderDetailsView getOrderDetails(String itemId)
    {
        return new OrderDetailsView(container, itemId);
    }

    public ShippingInfoView getShippingInfo()
    {
        return new ShippingInfoView(container);
    }

    @Override
    public EventSourceExtended getSource()
    {
        return new EventSourceExtended(container);
    }

    public void verify(Order order)
    {
        if (order == null)
        {
            return;
        }

        for (OrderItem item : order.getOrderItems())
        {
            getOrderDetails().verify(item);
        }

        getShippingInfo().verify(order, true);

        if (order.getSource() == null)
        {
            return;
        }

        getSource().verify(order.getSource(), Constant.NOT_AVAILABLE);

    }
}
