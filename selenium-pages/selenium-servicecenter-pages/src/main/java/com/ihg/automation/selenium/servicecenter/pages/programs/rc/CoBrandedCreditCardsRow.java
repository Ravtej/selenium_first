package com.ihg.automation.selenium.servicecenter.pages.programs.rc;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;

import com.ihg.automation.selenium.common.CoBrandedCreditCard;
import com.ihg.automation.selenium.gwt.components.grid.CellsName;
import com.ihg.automation.selenium.gwt.components.grid.GridRow;
import com.ihg.automation.selenium.gwt.components.grid.GridRowXpath;

public class CoBrandedCreditCardsRow extends GridRow<CoBrandedCreditCardsRow.CoBrandedCreditCardsCell>
{
    public CoBrandedCreditCardsRow(CoBrandedCreditCardsGrid parent, GridRowXpath xpath)
    {
        super(parent, xpath);
    }

    public void verify(CoBrandedCreditCard coBrandedCreditCard)
    {
        verifyThat(getCell(CoBrandedCreditCardsRow.CoBrandedCreditCardsCell.PARTNER),
                hasText(coBrandedCreditCard.getPartner()));
        verifyThat(getCell(CoBrandedCreditCardsRow.CoBrandedCreditCardsCell.CARD_PRODUCT),
                hasText(coBrandedCreditCard.getCreditCardName()));
        verifyThat(getCell(CoBrandedCreditCardsRow.CoBrandedCreditCardsCell.ELITE_LEVEL_GIVEN),
                hasText(coBrandedCreditCard.getLevel().getValue()));
        verifyThat(getCell(CoBrandedCreditCardsRow.CoBrandedCreditCardsCell.ELITE_LEVEL_DURATION),
                hasText(coBrandedCreditCard.getLevelDuration()));
        verifyThat(getCell(CoBrandedCreditCardsRow.CoBrandedCreditCardsCell.CARD_STATUS_DATE),
                hasText(coBrandedCreditCard.getStatusDate()));
        verifyThat(getCell(CoBrandedCreditCardsRow.CoBrandedCreditCardsCell.STATUS),
                hasText(coBrandedCreditCard.getStatus()));
    }

    public enum CoBrandedCreditCardsCell implements CellsName
    {
        PARTNER("partnerName", "Partner"), //
        CARD_PRODUCT("cardProduct", "Card Product"), //
        ELITE_LEVEL_GIVEN("eliteLevelGiven", "Elite Level Given"), //
        ELITE_LEVEL_DURATION("eliteLevelDuration", "Elite Level Duration"), //
        CARD_STATUS_DATE("cardStatusDate", "Card Status Date"), //
        STATUS("status", "Status");

        private String name;
        private String value;

        private CoBrandedCreditCardsCell(String name, String value)
        {
            this.name = name;
            this.value = value;
        }

        public String getValue()
        {
            return value;
        }

        @Override
        public String getName()
        {
            return name;
        }

    }
}
