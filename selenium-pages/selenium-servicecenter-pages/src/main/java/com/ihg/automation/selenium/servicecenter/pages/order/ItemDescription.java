package com.ihg.automation.selenium.servicecenter.pages.order;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static org.hamcrest.Matchers.containsString;

import com.ihg.automation.selenium.common.catalog.CatalogItem;
import com.ihg.automation.selenium.gwt.components.Component;
import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.CustomContainerBase;
import com.ihg.automation.selenium.gwt.components.input.Select;
import com.ihg.automation.selenium.gwt.components.utils.FindStep;

public class ItemDescription extends CustomContainerBase
{
    public ItemDescription(Container container, FindStep xPath)
    {
        super(container, null, null, xPath);
    }

    public Component getItemID()
    {
        return container.getLabel("Item ID:");
    }

    public Select getColor()
    {
        return container.getSelectByLabel("Color:");
    }

    public Select getSize()
    {
        return container.getSelectByLabel("Size:");
    }

    public void verify(CatalogItem item, String transDetail)
    {
        verifyThat(container, hasText(containsString(item.getItemName())));
        verifyThat(container, hasText(containsString(transDetail)));
        verifyThat(getItemID(), hasText(item.getItemId()));
    }
}
