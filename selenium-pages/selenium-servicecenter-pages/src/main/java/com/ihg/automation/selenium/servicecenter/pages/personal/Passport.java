package com.ihg.automation.selenium.servicecenter.pages.personal;

import com.ihg.automation.selenium.common.components.CountrySelect;
import com.ihg.automation.selenium.common.pages.MultiModeBase;
import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.input.Input;

public class Passport extends MultiModeBase
{
    public Passport(Container container)
    {
        super(container.getSeparator("Passport"));
    }

    public Input getNumber()
    {
        return container.getInputByLabel("Passport Number");
    }

    public CountrySelect getIssuanceCountry()
    {
        return new CountrySelect(container, "Passport Country of Issuance");
    }

    public Input getBirthCity()
    {
        return container.getInputByLabel("Birth City");
    }

    public CountrySelect getBirthCountry()
    {
        return new CountrySelect(container, "Birth Country");
    }
}
