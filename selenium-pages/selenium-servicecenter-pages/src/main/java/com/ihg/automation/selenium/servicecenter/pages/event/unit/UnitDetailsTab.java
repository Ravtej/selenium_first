package com.ihg.automation.selenium.servicecenter.pages.event.unit;

import com.ihg.automation.selenium.common.pages.TabCustomContainerBase;

public class UnitDetailsTab extends TabCustomContainerBase
{
    public UnitDetailsTab(UnitDetailsPopUp parent)
    {
        super(parent, "Loyalty Units Details");
    }

    public UnitDetails getUnitDetails()
    {
        return new UnitDetails(container);
    }
}
