package com.ihg.automation.selenium.servicecenter.pages.event.status;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;

import com.ihg.automation.selenium.common.EventSource;
import com.ihg.automation.selenium.common.EventSourceAware;
import com.ihg.automation.selenium.common.SiteHelperBase;
import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.CustomContainerBase;
import com.ihg.automation.selenium.gwt.components.label.Label;

public class ProfileStatusGridRowContainer extends CustomContainerBase implements EventSourceAware
{
    public ProfileStatusGridRowContainer(Container parent)
    {
        super(parent);
    }

    @Override
    public EventSource getSource()
    {
        return new EventSource(container);
    }

    public Label getReason()
    {
        return container.getLabel("Reason");
    }

    public void verify(SiteHelperBase siteHelper, String reason)
    {
        verifyThat(getReason(), hasText(reason));
        getSource().verify(siteHelper.getSourceFactory().getNoEmployeeIdSource());
    }
}
