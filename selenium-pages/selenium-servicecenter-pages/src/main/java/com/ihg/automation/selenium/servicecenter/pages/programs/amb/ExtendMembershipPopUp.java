package com.ihg.automation.selenium.servicecenter.pages.programs.amb;

import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyNoError;

import com.ihg.automation.selenium.common.pages.ClosablePopUpBase;
import com.ihg.automation.selenium.gwt.components.Button;
import com.ihg.automation.selenium.gwt.components.input.Select;
import com.ihg.automation.selenium.listener.Verifier;

public class ExtendMembershipPopUp extends ClosablePopUpBase
{
    public ExtendMembershipPopUp()
    {
        super("Extend Membership");
    }

    public Select getExtendByMonth()
    {
        return container.getSelectByLabel("Extend By");
    }

    public Select getReasonCode()
    {
        return container.getSelectByLabel("Reason code");
    }

    public Button getSubmit()
    {
        return container.getButton("Submit");
    }

    public void clickSubmit(Verifier... verifiers)
    {
        getSubmit().clickAndWait(verifiers);
    }

    public void extend(int month, String reasonCode)
    {
        getExtendByMonth().select(month);
        getReasonCode().select(reasonCode);

        clickSubmit(verifyNoError());
    }
}
