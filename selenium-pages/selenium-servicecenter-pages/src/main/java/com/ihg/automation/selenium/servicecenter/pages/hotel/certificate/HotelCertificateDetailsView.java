package com.ihg.automation.selenium.servicecenter.pages.hotel.certificate;

import org.hamcrest.Matcher;

import com.ihg.automation.selenium.common.types.Currency;
import com.ihg.automation.selenium.gwt.components.Componentable;
import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.label.Label;
import com.ihg.automation.selenium.matchers.component.ComponentMatcher;

public class HotelCertificateDetailsView extends HotelCertificateDetailsBase
{

    public HotelCertificateDetailsView(Container container)
    {
        super(container);
    }

    @Override
    public Label getHotel()
    {
        return container.getLabel(HOTEL);
    }

    @Override
    public Label getConfirmationNumber()
    {
        return container.getLabel(CONFIRMATION_NUMBER);
    }

    @Override
    public Label getFolioNumber()
    {
        return container.getLabel(FOLIO_NUMBER);
    }

    @Override
    public Label getCheckIn()
    {
        return container.getLabel(CHECK_IN);
    }

    @Override
    public Label getCheckOut()
    {
        return container.getLabel(CHECK_OUT);
    }

    @Override
    public Label getItemId()
    {
        return container.getLabel(ITEM_ID);
    }

    @Override
    public Label getBundle()
    {
        return container.getLabel(BUNDLE);
    }

    @Override
    public Label getDescriptionGuestName()
    {
        return container.getLabel("Guest Name");
    }

    @Override
    public Label getCertificateNumber()
    {
        return container.getLabel(CERTIFICATE_NUMBER);
    }

    @Override
    public Label getReimbursementAmount()
    {
        return container.getLabel(REIMBURSEMENT_AMOUNT);
    }

    @Override
    public Label getBillingEntity()
    {
        return container.getLabel(BILLING_ENTITY);
    }

    @Override
    public Label getCurrency()
    {
        return container.getLabel(CURRENCY);
    }

    @Override
    public Matcher<Componentable> getCurrencyMatcher(Currency currency)
    {
        return ComponentMatcher.hasText(currency.getCode());
    }

}
