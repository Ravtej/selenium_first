package com.ihg.automation.selenium.servicecenter.pages.event.stay;

import com.ihg.automation.selenium.common.pages.Populate;
import com.ihg.automation.selenium.common.stay.Stay;

public class StayDetailsCreate extends StayDetailsBase implements Populate<Stay>
{
    public StayDetailsCreate(CreateStayPopUp parent)
    {
        super(parent.container);
    }

    @Override
    public StayInfoCreate getStayInfo()
    {
        return new StayInfoCreate(this);
    }

    public void clickSaveChanges()
    {
        container.getButton("Save Changes").clickAndWait();
    }

    @Override
    public void populate(Stay stay)
    {
        getStayInfo().populate(stay);
        getBookingInfo().populate(stay);
        getRevenueDetails().populate(stay.getRevenues());
    }
}
