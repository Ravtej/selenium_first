package com.ihg.automation.selenium.servicecenter.pages.event.rewardnight;

import com.ihg.automation.selenium.gwt.components.Component;
import com.ihg.automation.selenium.gwt.components.grid.CellsName;
import com.ihg.automation.selenium.gwt.components.grid.Grid;
import com.ihg.automation.selenium.gwt.components.grid.GridRowXpath;

public class RewardNightEventGrid extends Grid<RewardNightEventGridRow, RewardNightEventGrid.RewardNightCell>
{

    public RewardNightEventGrid(Component parent)
    {
        super(parent);
    }

    public RewardNightEventGridRow getRow(String transType)
    {
        return getRow(new GridRowXpath.GridRowXpathBuilder().cell(RewardNightCell.TRANS_TYPE, transType).build());
    }

    public RewardNightEventGridRow getRow(String transType, String certNumber)
    {
        return getRow(new GridRowXpath.GridRowXpathBuilder().cell(RewardNightCell.TRANS_TYPE, transType)
                .cell(RewardNightCell.CERTIFICATE_NUMBER, certNumber).build());
    }

    public enum RewardNightCell implements CellsName
    {
        TRANS_DATE("transactionDate"), TRANS_TYPE("transactionType"), HOTEL("hotel"), CHECK_IN_DATE("checkInDate"), CHECK_OUT_DATE(
                "checkOutDate"), NTS("nights"), CERTIFICATE_NUMBER("certificateNumber"), IHG_UNITS("ihgUnits");

        private String name;

        private RewardNightCell(String name)
        {
            this.name = name;
        }

        @Override
        public String getName()
        {
            return this.name;
        }
    }

}
