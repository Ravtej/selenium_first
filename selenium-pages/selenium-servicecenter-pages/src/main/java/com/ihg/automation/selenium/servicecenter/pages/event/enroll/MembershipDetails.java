package com.ihg.automation.selenium.servicecenter.pages.event.enroll;

import com.ihg.automation.selenium.common.Constant;
import com.ihg.automation.selenium.common.EventSourceAware;
import com.ihg.automation.selenium.common.SiteHelperBase;
import com.ihg.automation.selenium.common.event.EventSourceExtended;
import com.ihg.automation.selenium.common.payment.Payment;
import com.ihg.automation.selenium.common.payment.PaymentDetails;
import com.ihg.automation.selenium.common.payment.PaymentDetailsAware;
import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.CustomContainerBase;

public class MembershipDetails extends CustomContainerBase implements PaymentDetailsAware, EventSourceAware
{
    public MembershipDetails(Container container)
    {
        super(container);
    }

    @Override
    public PaymentDetails getPaymentDetails()
    {
        return new PaymentDetails(container);
    }

    @Override
    public EventSourceExtended getSource()
    {
        return new EventSourceExtended(container);
    }

    public void verifyScSource(SiteHelperBase siteHelper)
    {
        getSource().verify(siteHelper.getSourceFactory().getSource(), Constant.NOT_AVAILABLE);
    }

    public void verify(Payment payment, SiteHelperBase siteHelper)
    {
        verifyScSource(siteHelper);
        getPaymentDetails().verifyCommonDetails(payment);
    }
}
