package com.ihg.automation.selenium.servicecenter.pages.personal;

public class EditSocialIdPopUp extends SocialIdPopUpBase
{
    public EditSocialIdPopUp()
    {
        super("Edit Social Id");
    }
}
