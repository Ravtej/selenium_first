package com.ihg.automation.selenium.servicecenter.pages.event.level;

import com.ihg.automation.selenium.common.pages.TabCustomContainerBase;

public class TierLevelDetailsTab extends TabCustomContainerBase
{

    public TierLevelDetailsTab(TierLevelDetailsPopUp parent)
    {
        super(parent, "Tier-Level Details");
    }

    public TierLevelDetails getDetails()
    {
        return new TierLevelDetails(container);
    }

}
