package com.ihg.automation.selenium.servicecenter.pages.event.offer;

import com.ihg.automation.selenium.common.event.SearchButtonBar;
import com.ihg.automation.selenium.gwt.components.Button;
import com.ihg.automation.selenium.listener.Verifier;

public class OfferSearchButtonBar extends SearchButtonBar
{
    public OfferSearchButtonBar(OfferEventPage page)
    {
        super(page.container);
    }

    public Button getShowCustomerOffers()
    {
        return container.getButton("Show Customer's Offers");
    }

    public Button getQuickRegister()
    {
        return container.getButton("Quick Register");
    }

    public void clickShowCustomerOffers()
    {
        getShowCustomerOffers().clickAndWait();
    }

    public void clickQuickRegister(Verifier... verifiers)
    {
        getQuickRegister().clickAndWait(verifiers);
    }
}
