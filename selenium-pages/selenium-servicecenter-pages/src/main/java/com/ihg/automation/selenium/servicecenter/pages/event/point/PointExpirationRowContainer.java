package com.ihg.automation.selenium.servicecenter.pages.event.point;

import com.ihg.automation.selenium.common.EventSourceAware;
import com.ihg.automation.selenium.common.event.EventGridRowContainerWithDetails;
import com.ihg.automation.selenium.common.event.EventSourceExtended;
import com.ihg.automation.selenium.gwt.components.Container;

public class PointExpirationRowContainer extends EventGridRowContainerWithDetails implements EventSourceAware
{

    public PointExpirationRowContainer(Container parent)
    {
        super(parent);
    }

    @Override
    public EventSourceExtended getSource()
    {
        return new EventSourceExtended(container);
    }
}
