package com.ihg.automation.selenium.servicecenter.pages.programs.rc;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;

import com.ihg.automation.selenium.common.catalog.CatalogItem;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.gwt.components.Component;
import com.ihg.automation.selenium.gwt.components.ComponentColumnPosition;
import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.CustomContainerBase;
import com.ihg.automation.selenium.gwt.components.label.Label;

public class BenefitsGridRowContainer extends CustomContainerBase
{
    private ComponentColumnPosition POSITION = ComponentColumnPosition.TABLE_TR;

    public BenefitsGridRowContainer(Container container)
    {
        super(container);
    }

    public Label getItemDescription()
    {
        return container.getLabel("Item Description");
    }

    public Label getDateOrdered()
    {
        return container.getLabel("Date Ordered");
    }

    public Component getReceivingMemberId()
    {
        return container.getLabel("Receiving Member").getComponent(" Member Id", 1, POSITION);
    }

    public Component getReceivingMemberName()
    {
        return container.getLabel("Receiving Member").getComponent("Member Name", 2, POSITION);
    }

    public void verifyForPointsBenefit(CatalogItem item, String dateOrdered)
    {
        verifyThat(getItemDescription(), hasText(item.getItemDescription()));
        verifyThat(getDateOrdered(), hasText(dateOrdered));
    }

    public void verifyForLevelBenefit(CatalogItem item, String dateOrdered, Member member)
    {
        verifyForPointsBenefit(item, dateOrdered);
        verifyThat(getReceivingMemberId(), hasText(member.getRCProgramId()));
        verifyThat(getReceivingMemberName(), hasText(member.getName().getFullName()));
    }
}
