package com.ihg.automation.selenium.servicecenter.pages.event.rewardnight;

import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.assertNoError;

import com.ihg.automation.selenium.common.billing.EventBillingDetailsTab;
import com.ihg.automation.selenium.common.billing.EventBillingTabAware;
import com.ihg.automation.selenium.common.earning.EventEarningDetailsTab;
import com.ihg.automation.selenium.common.earning.EventEarningTabAware;
import com.ihg.automation.selenium.common.history.EventHistoryTab;
import com.ihg.automation.selenium.common.history.EventHistoryTabAware;
import com.ihg.automation.selenium.common.pages.ClosablePopUpBase;
import com.ihg.automation.selenium.gwt.components.Button;
import com.ihg.automation.selenium.gwt.components.panel.Panel;
import com.ihg.automation.selenium.gwt.components.utils.ByText;

public class RewardNightDetailsPopUp extends ClosablePopUpBase
        implements EventBillingTabAware, EventEarningTabAware, EventHistoryTabAware
{
    public RewardNightDetailsPopUp()
    {
        super("Reward Night Event Details");
    }

    public RewardNightDetailsTab getRewardNightDetailsTab()
    {
        return new RewardNightDetailsTab(this);
    }

    @Override
    public EventEarningDetailsTab getEarningDetailsTab()
    {
        return new EventEarningDetailsTab(this);
    }

    @Override
    public EventBillingDetailsTab getBillingDetailsTab()
    {
        return new EventBillingDetailsTab(this);
    }

    @Override
    public EventHistoryTab getHistoryTab()
    {
        return new EventHistoryTab(this, "History");
    }

    private Panel getGridPanel()
    {
        return container.getPanel(ByText.contains("Reward Night Reimbursement Detail"));
    }

    public RewardNightDetailsPopUpGrid getGrid()
    {
        return new RewardNightDetailsPopUpGrid(getGridPanel());
    }

    public Button getCancelRewardNight()
    {
        return container.getButton("Cancel Reward Night");
    }

    public Button getSaveChanges()
    {
        return container.getButton("Save Changes");
    }

    public ReservationCancellationPopUp clickCancelRewardNight()
    {
        getCancelRewardNight().clickAndWait(assertNoError());
        return new ReservationCancellationPopUp();
    }
}
