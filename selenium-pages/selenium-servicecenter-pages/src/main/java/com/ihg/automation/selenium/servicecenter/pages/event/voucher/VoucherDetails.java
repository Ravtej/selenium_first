package com.ihg.automation.selenium.servicecenter.pages.event.voucher;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;

import com.ihg.automation.selenium.common.EventSourceAware;
import com.ihg.automation.selenium.common.SiteHelperBase;
import com.ihg.automation.selenium.common.event.EventSourceExtended;
import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.CustomContainerBase;
import com.ihg.automation.selenium.gwt.components.label.Label;

public class VoucherDetails extends CustomContainerBase implements EventSourceAware
{
    public VoucherDetails(Container container)
    {
        super(container);
    }

    public Label getVoucherNumber()
    {
        return container.getLabel("Voucher Number");
    }

    @Override
    public EventSourceExtended getSource()
    {
        return new EventSourceExtended(container);
    }

    public void verify(String voucherNumber, SiteHelperBase siteHelper)
    {
        verifyThat(getVoucherNumber(), hasText(voucherNumber));
        getSource().verify(siteHelper.getSourceFactory().getNoEmployeeIdSource());
    }
}
