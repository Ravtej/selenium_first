package com.ihg.automation.selenium.servicecenter.pages.event.unit;

import com.ihg.automation.selenium.common.event.EventGridRowContainerWithDetails;
import com.ihg.automation.selenium.gwt.components.Container;

public class UnitGridRowContainer extends EventGridRowContainerWithDetails
{
    public UnitGridRowContainer(Container parent)
    {
        super(parent);
    }

    public UnitDetails getUnitDetails()
    {
        return new UnitDetails(container);
    }
}
