package com.ihg.automation.selenium.servicecenter.pages.event.goodwill;

import com.ihg.automation.selenium.common.billing.EventBillingDetailsTab;
import com.ihg.automation.selenium.common.billing.EventBillingTabAware;
import com.ihg.automation.selenium.common.earning.EventEarningDetailsTab;
import com.ihg.automation.selenium.common.earning.EventEarningTabAware;
import com.ihg.automation.selenium.common.pages.ClosablePopUpBase;

public class GoodwillDetailsPopUp extends ClosablePopUpBase implements EventEarningTabAware, EventBillingTabAware
{
    public GoodwillDetailsPopUp()
    {
        super("Goodwill Details");
    }

    public GoodwillDetailsTab getGoodwillDetailsTab()
    {
        return new GoodwillDetailsTab(this);
    }

    @Override
    public EventEarningDetailsTab getEarningDetailsTab()
    {
        return new EventEarningDetailsTab(this);
    }

    @Override
    public EventBillingDetailsTab getBillingDetailsTab()
    {
        return new EventBillingDetailsTab(this);
    }
}
