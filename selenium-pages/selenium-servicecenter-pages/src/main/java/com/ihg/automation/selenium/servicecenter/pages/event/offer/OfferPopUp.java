package com.ihg.automation.selenium.servicecenter.pages.event.offer;

import com.ihg.automation.selenium.gwt.components.Button;
import com.ihg.automation.selenium.gwt.components.utils.WaitUtils;

public class OfferPopUp extends OfferPopUpBase
{
    public OfferPopUp()
    {
        super();
    }

    @Override
    public OfferDetailsTabExtended getOfferDetailsTab()
    {
        return new OfferDetailsTabExtended(this);
    }

    public Button getAdjustOffer()
    {
        return container.getButton("Adjust Offer");
    }

    public void clickAdjustOffer()
    {
        getAdjustOffer().clickAndWait();
    }

    public Button getForceRegister()
    {
        return container.getButton("Force Register");
    }

    public Button getRegister()
    {
        return container.getButton("Register");
    }

    public void clickForceRegister()
    {
        getForceRegister().clickAndWait();
    }

    public CampaignDashboardTab getCampaignDashboardTab()
    {
        return new CampaignDashboardTab(this);
    }

    public void waitAndClose()
    {
        new WaitUtils().waitExecutingRequest();
        close();
    }
}
