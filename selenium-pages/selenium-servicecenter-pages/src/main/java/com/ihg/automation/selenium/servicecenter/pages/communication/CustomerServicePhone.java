package com.ihg.automation.selenium.servicecenter.pages.communication;

import com.ihg.automation.selenium.common.member.GuestPhone;
import com.ihg.automation.selenium.common.pages.Populate;
import com.ihg.automation.selenium.gwt.components.Component;
import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.CustomContainerBase;
import com.ihg.automation.selenium.gwt.components.checkbox.CheckBox;
import com.ihg.automation.selenium.gwt.components.input.Select;
import com.ihg.automation.selenium.gwt.components.label.TopLabel;
import com.ihg.automation.selenium.gwt.components.utils.ByText;
import com.ihg.automation.selenium.gwt.components.utils.FindStepUtils;

public class CustomerServicePhone extends CustomContainerBase implements Populate<GuestPhone>
{
    public final static String AFTERNOON = "Afternoon";
    public final static String AFTERNOON_START_TIME = "12:00";
    public final static String AFTERNOON_END_TIME = "17:00";
    public final static String AFTERNOON_AUDIT = AFTERNOON_START_TIME + "-" + AFTERNOON_END_TIME;

    public final static String EVENING = "Evening";
    public final static String EVENING_START_TIME = "17:00";
    public final static String EVENING_END_TIME = "21:00";
    public final static String EVENING_AUDIT = EVENING_START_TIME + "-" + EVENING_END_TIME;

    public final static String MORNING = "Morning";
    public final static String MORNING_START_TIME = "08:00";
    public final static String MORNING_END_TIME = "12:00";
    public final static String MORNING_AUDIT = MORNING_START_TIME + "-" + MORNING_END_TIME;

    public final static String NIGHT = "Night";
    public final static String NIGHT_START_TIME = "21:00";
    public final static String NIGHT_END_TIME = "08:00";
    public final static String NIGHT_AUDIT = NIGHT_START_TIME + "-" + NIGHT_END_TIME;

    public CustomerServicePhone(Container container)
    {
        super(container);
    }

    public CheckBox getPhoneCheckBox()
    {
        return container.getCheckBox("Phone");
    }

    public Select getPhone()
    {
        final String name = "Phone";
        Component parent = new Component(container, "Phone Area", name,
                FindStepUtils.byXpathWithWait(".//span[@class='phone']"));
        return new Select(parent, name);
    }

    public Select getDayPart()
    {
        return new TopLabel(container, ByText.exact("Part of Day"), TopLabel.GO_TO_LABEL).getSelect();
    }

    public Select getStartTime()
    {
        return new TopLabel(container, ByText.exact("Start Time"), TopLabel.GO_TO_LABEL).getSelect();
    }

    public Select getEndTime()
    {
        return new TopLabel(container, ByText.exact("End Time"), TopLabel.GO_TO_LABEL).getSelect();
    }

    public TopLabel getDaysInView()
    {
        return new TopLabel(container, ByText.exact("Days"), TopLabel.GO_TO_LABEL);
    }

    public DaysMenu getDays()
    {
        return new DaysMenu(container);
    }

    @Override
    public void populate(GuestPhone phone)
    {
        getPhoneCheckBox().check();
        getPhone().select(phone.getFullPhone());
    }

}
