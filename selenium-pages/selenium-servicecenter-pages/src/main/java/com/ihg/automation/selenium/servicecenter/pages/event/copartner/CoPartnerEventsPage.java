package com.ihg.automation.selenium.servicecenter.pages.event.copartner;

import com.ihg.automation.selenium.common.event.SearchAware;
import com.ihg.automation.selenium.common.event.SearchButtonBar;
import com.ihg.automation.selenium.common.pages.TabCustomContainerBase;
import com.ihg.automation.selenium.gwt.components.Button;
import com.ihg.automation.selenium.gwt.components.ComponentColumnPosition;
import com.ihg.automation.selenium.gwt.components.input.Input;
import com.ihg.automation.selenium.gwt.components.input.Select;
import com.ihg.automation.selenium.servicecenter.pages.Tabs;

public class CoPartnerEventsPage extends TabCustomContainerBase implements SearchAware
{
    public static final String TRANSACTION_DATE = "Transaction Date";
    private static final ComponentColumnPosition POSITION = ComponentColumnPosition.SINGLE_TD_INNER_DIV;

    public CoPartnerEventsPage()
    {
        super(new Tabs().getEvents().getCoPartner());
    }

    @Override
    public CoPartnerEventsButtonBar getSearchFields()
    {
        return new CoPartnerEventsButtonBar(this);
    }

    @Override
    public SearchButtonBar getButtonBar()
    {
        return new SearchButtonBar(container);
    }

    public CoPartnerEventGrid getGrid()
    {
        return new CoPartnerEventGrid(container);
    }

    public class CoPartnerEventsButtonBar extends SearchButtonBar
    {
        public CoPartnerEventsButtonBar(CoPartnerEventsPage page)
        {
            super(page.container);
        }

        public Button getCreateCoPartner()
        {
            return new Button(container, "Create Co-partner");
        }

        public void clickCreateCoPartner()
        {
            getCreateCoPartner().clickAndWait();
        }

        public Input getPartnerName()
        {
            return container.getInputByLabel("Partner Name");
        }

        public Input getTransactionId()
        {
            return container.getInputByLabel("Transaction ID");
        }

        public Input getKeywords()
        {
            return container.getInputByLabel("Keywords");
        }

        public Select getTransactionDatePeriod()
        {
            return container.getSelectByLabel(TRANSACTION_DATE, TRANSACTION_DATE, 2, POSITION);
        }

        public Input geTransactionDate()
        {
            return container.getInputByLabel(TRANSACTION_DATE, TRANSACTION_DATE, 1, POSITION);
        }
    }
}
