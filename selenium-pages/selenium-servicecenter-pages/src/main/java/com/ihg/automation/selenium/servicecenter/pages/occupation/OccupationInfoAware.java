package com.ihg.automation.selenium.servicecenter.pages.occupation;

import com.ihg.automation.selenium.gwt.components.CustomContainerBase;

public interface OccupationInfoAware
{
    public CustomContainerBase getOccupationInfo();
}
