package com.ihg.automation.selenium.servicecenter.pages.preferences;

import com.ihg.automation.selenium.common.pages.SavePopUpBase;
import com.ihg.automation.selenium.gwt.components.DualList;
import com.ihg.automation.selenium.gwt.components.checkbox.CheckBox;
import com.ihg.automation.selenium.gwt.components.input.Input;
import com.ihg.automation.selenium.gwt.components.input.Select;
import com.ihg.automation.selenium.servicecenter.pages.occupation.OccupationInfoFields;

public abstract class TravelProfilePopUpBase extends SavePopUpBase
{
    public static final String SUCCESS_MESSAGE_CREATE = "Travel Profile has been created";
    public static final String SUCCESS_MESSAGE_UPDATE = "Travel Profile has been updated";
    public static final String ABANDON_MESSAGE = "Are you sure you want to abandon your changes?";
    public static final String NO_CHANGES = "No changes were made.";
    public static final String STAY_PREF_SUCCESS_MESSAGE = "Thank you. Your preferences have been saved.";

    public TravelProfilePopUpBase(String name)
    {
        super(name);
    }

    public Input getTravelProfileName()
    {
        return container.getInputByLabel("Travel Profile Name");
    }

    public Select getType()
    {
        return container.getSelectByLabel("Type");
    }

    public OccupationInfoFields getOccupationInfoFields()
    {
        return new OccupationInfoFields(container);
    }

    public DualList getRatePreferences()
    {
        return container.getDualList("Rate Preferences");
    }

    public Input getIataNumber()
    {
        return container.getInputByLabel("IATA Number");
    }

    public CreditCardFields getCreditCardFields()
    {
        return new CreditCardFields(container);
    }

    public CheckBox getDefaultCheckbox()
    {
        return container.getLabel("Default").getCheckBox();
    }
}
