package com.ihg.automation.selenium.servicecenter.pages;

import com.ihg.automation.selenium.common.LeftPanelBase;
import com.ihg.automation.selenium.common.member.Member;

public class LeftPanel extends LeftPanelBase
{
    public void reOpenMemberProfile(Member member)
    {
        reOpenMemberProfile(member.getRCProgramId());
    }

    public void reOpenMemberProfile(String memberId)
    {
        goToNewSearch();
        new GuestSearch().byMemberNumber(memberId);
    }

}
