package com.ihg.automation.selenium.servicecenter.pages.event.copartner;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasDefaultOrText;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static com.ihg.automation.selenium.matchers.text.EntityMatcherFactory.isValue;

import com.ihg.automation.selenium.common.deposit.Deposit;
import com.ihg.automation.selenium.gwt.components.grid.CellsName;
import com.ihg.automation.selenium.gwt.components.grid.Grid;
import com.ihg.automation.selenium.gwt.components.grid.GridRow;
import com.ihg.automation.selenium.gwt.components.grid.GridRowXpath;
import com.ihg.automation.selenium.servicecenter.pages.event.deposit.DepositGridRowContainer;

public class CoPartnerEventGridRow extends GridRow<CoPartnerEventGridRow.CoPartnerEventCell>
{

    public CoPartnerEventGridRow(Grid parent, GridRowXpath xpath)
    {
        super(parent, xpath);
    }

    public DepositGridRowContainer getDetails()
    {
        return expand(DepositGridRowContainer.class);
    }

    public void verify(Deposit deposit)
    {
        verifyThat(getCell(CoPartnerEventCell.TRANS_DATE), hasText(deposit.getTransactionDate()));
        verifyThat(getCell(CoPartnerEventCell.TRANS_TYPE), hasText(deposit.getTransactionType()));
        verifyThat(getCell(CoPartnerEventCell.TRANSACTION_ID), hasText(deposit.getTransactionId()));
        verifyThat(getCell(CoPartnerEventCell.TRANSACTION_NAME), hasText(deposit.getTransactionName()));
        verifyThat(getCell(CoPartnerEventCell.PARTNER_NAME), hasText(isValue(deposit.getCoPartner())));
        verifyThat(getCell(CoPartnerEventCell.IHG_UNITS), hasDefaultOrText(deposit.getLoyaltyUnitsAmount()));
        verifyThat(getCell(CoPartnerEventCell.ALLIANCE_UNITS),
                hasDefaultOrText(deposit.getAllianceLoyaltyUnitsAmount()));
    }

    public enum CoPartnerEventCell implements CellsName
    {
        TRANS_DATE("transactionDate"), TRANS_TYPE("transactionType"), TRANSACTION_ID("transactionId"), TRANSACTION_NAME(
                "transactionName"), PARTNER_NAME("partnerName"), IHG_UNITS("loyaltyUnits.ihgUnits.amount"), ALLIANCE_UNITS(
                "loyaltyUnits.allianceUnits.amount");

        private String name;

        private CoPartnerEventCell(String name)
        {
            this.name = name;
        }

        @Override
        public String getName()
        {
            return name;
        }
    }

}
