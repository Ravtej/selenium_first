package com.ihg.automation.selenium.servicecenter.pages.hotel.certificate;

import org.hamcrest.Matcher;

import com.ihg.automation.selenium.common.hotel.HotelCertificate;
import com.ihg.automation.selenium.common.types.Currency;
import com.ihg.automation.selenium.gwt.components.Componentable;
import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.input.DateInput;
import com.ihg.automation.selenium.gwt.components.input.Input;
import com.ihg.automation.selenium.gwt.components.input.SearchInput;
import com.ihg.automation.selenium.gwt.components.input.Select;
import com.ihg.automation.selenium.matchers.component.ComponentMatcher;

public abstract class HotelCertificateDetailsEditBase extends HotelCertificateDetailsBase
{

    public HotelCertificateDetailsEditBase(Container container)
    {
        super(container);
    }

    @Override
    public SearchInput getHotel()
    {
        return new SearchInput(container.getLabel(HOTEL));
    }

    @Override
    public Input getConfirmationNumber()
    {
        return container.getInputByLabel(CONFIRMATION_NUMBER);
    }

    @Override
    public Input getFolioNumber()
    {
        return container.getInputByLabel(FOLIO_NUMBER);
    }

    @Override
    public DateInput getCheckIn()
    {
        return new DateInput(container.getLabel(CHECK_IN));
    }

    @Override
    public DateInput getCheckOut()
    {
        return new DateInput(container.getLabel(CHECK_OUT));
    }

    @Override
    public Input getItemId()
    {
        return container.getInputByLabel(ITEM_ID);
    }

    @Override
    public Input getBundle()
    {
        return container.getInputByLabel(BUNDLE);
    }

    @Override
    public Input getDescriptionGuestName()
    {
        return container.getInputByLabel(DESCRIPTION_GUEST_NAME);
    }

    @Override
    public Input getReimbursementAmount()
    {
        return container.getInputByLabel(REIMBURSEMENT_AMOUNT);
    }

    public Select getCurrency()
    {
        return container.getSelectByLabel(CURRENCY);
    }

    @Override
    public Matcher<Componentable> getCurrencyMatcher(Currency currency)
    {
        return ComponentMatcher.hasText(currency.getValue());
    }

    public void populate(HotelCertificate details)
    {
        getHotel().typeAndWait(details.getHotelCode());
        getConfirmationNumber().type(details.getConfirmationNumber());
        getFolioNumber().type(details.getFolioNumber());
        getCheckIn().typeAndWait(details.getCheckInDate());
        getCheckOut().typeAndWait(details.getCheckOutDate());
        getItemId().type(details.getItemId());
        getBundle().type(details.getBundleNumber());
        getDescriptionGuestName().type(details.getDescriptionGuestName());
        getReimbursementAmount().type(details.getReimbursementAmount());
        getCurrency().selectByValue(details.getCurrency());
    }

}
