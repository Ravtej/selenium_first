package com.ihg.automation.selenium.servicecenter.pages.event.offer;

import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.grid.Grid;

public class UniverseOffersGrid extends Grid<UniverseOfferGridRow, UniverseOfferGridRow.UniverseOfferCell>
{
    public UniverseOffersGrid(Container parent)
    {
        super(parent);
    }

}
