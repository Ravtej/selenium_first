package com.ihg.automation.selenium.servicecenter.pages.offer;

import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyNoError;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.assertThat;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.gwt.components.utils.FindStepUtils.byXpath;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;

import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.offer.Offer;
import com.ihg.automation.selenium.common.pages.ClosablePopUpBase;
import com.ihg.automation.selenium.common.pages.NavigatePage;
import com.ihg.automation.selenium.gwt.components.Button;
import com.ihg.automation.selenium.gwt.components.Component;
import com.ihg.automation.selenium.gwt.components.input.Input;
import com.ihg.automation.selenium.gwt.components.label.Label;
import com.ihg.automation.selenium.gwt.components.utils.ByText;
import com.ihg.automation.selenium.listener.Verifier;
import com.ihg.automation.selenium.servicecenter.pages.TopMenu;

public class OfferRegistrationPopUp extends ClosablePopUpBase implements NavigatePage
{

    public OfferRegistrationPopUp()
    {
        super("Offer Registration");
    }

    @Override
    public void goTo()
    {
        if (!isDisplayed())
        {
            new TopMenu().getOffer().getRegistration().clickAndWait();

            flush();
            assertThat(this, displayed(true));
        }
    }

    public Input getMembershipId()
    {
        return container.getInputByLabel("Membership ID");
    }

    public Input getRegistrationCode()
    {
        return container.getInputByLabel("Offer or Registration Code");
    }

    public Component getFullName()
    {
        return new Component(container, "Label", "Full Name", byXpath(".//div[contains(@class, 'full-name')]"));
    }

    public Component getPhone()
    {
        return new Component(container, "Label", "Phone", byXpath(".//div[contains(@class, 'phone')]"));
    }

    public Component getEmail()
    {
        return new Component(container, "Label", "Email", byXpath(".//div[contains(@class, 'email')]"));
    }

    public Component getAddress()
    {
        return new Component(container, "Label", "Address", byXpath(".//div[contains(@class, 'address')]"));
    }

    public Button getRegisterButton()
    {
        return container.getButton(Button.REGISTER);
    }

    public void clickRegister(Verifier... verifiers)
    {
        getRegisterButton().clickAndWait(verifiers);
    }

    public Label getOfferCode()
    {
        return new Label(container, ByText.exact("Offer Code"));
    }

    public Label getOfferDescription()
    {
        return new Label(container, ByText.exact("Description"));
    }

    public Label getOfferStartDate()
    {
        return new Label(container, ByText.exact("Start Date"));
    }

    public Label getOfferEndDate()
    {
        return new Label(container, ByText.exact("End Date"));
    }

    public Label getOfferName()
    {
        return new Label(container, ByText.exact("Name"));
    }

    public Label getTargetExpiry()
    {
        return new Label(container, ByText.exact("Target Expiry"));
    }

    public void verifyOfferDetails(Offer offer)
    {
        verifyThat(getOfferCode(), hasText(offer.getCode()));
        verifyThat(getOfferName(), hasText(offer.getName()));
        verifyThat(getOfferDescription(), hasText(offer.getDescription()));

        verifyThat(getOfferStartDate(), hasText(offer.getStartDate()));
        verifyThat(getOfferEndDate(), hasText(offer.getEndDate()));
    }

    public void verifyPersonalInfo(Member member)
    {
        verifyPersonalInfo(member.getPersonalInfo().getName().getNameOnPanel(),
                member.getPreferredPhone().getFullPhone(), member.getPreferredEmail().getEmail());
    }

    public void verifyPersonalInfo(String fullName, String phone, String email)
    {
        verifyThat(getFullName(), hasText(fullName));
        verifyThat(getPhone(), hasText(phone));
        verifyThat(getEmail(), hasText(email));
    }

    public void verify(Member member, Offer offer)
    {
        verifyOfferDetails(offer);
        verifyPersonalInfo(member);
    }

    public void registerToTargetingOffer(Member member, String offerId, String regEndDate)
    {
        registerToTargetingOffer(member, offerId, regEndDate, verifyNoError());
    }

    public void registerToTargetingOffer(Member member, String offerId, String regEndDate, Verifier verifier)
    {
        goTo();

        if (member != null)
        {
            getMembershipId().typeAndWait(member.getRCProgramId());
        }
        getRegistrationCode().typeAndWait(offerId);
        verifyThat(getTargetExpiry(), hasText(regEndDate));

        clickRegister(verifier);
    }

    public void registerInOffer(String offerId, Verifier verifier)
    {
        goTo();
        getRegistrationCode().typeAndWait(offerId);
        clickRegister(verifier);
    }

    public void registerInOffer( String offerId)
    {
        registerInOffer(offerId, verifyNoError());
    }
}
