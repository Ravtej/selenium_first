package com.ihg.automation.selenium.servicecenter.pages.event.freenight;

import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.assertNoError;
import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifySuccess;

import com.ihg.automation.selenium.common.pages.ClosablePopUpBase;
import com.ihg.automation.selenium.gwt.components.Button;
import com.ihg.automation.selenium.gwt.components.input.Select;
import com.ihg.automation.selenium.gwt.components.label.Label;
import com.ihg.automation.selenium.listener.Verifier;

public class CancelFreeNightBookingPopUp extends ClosablePopUpBase
{
    public CancelFreeNightBookingPopUp()
    {
        super("Cancel Free Night Booking");
    }

    public Button getSubmit()
    {
        return container.getButton("Submit");
    }

    public void clickSubmit(Verifier... verifiers)
    {
        getSubmit().clickAndWait(verifiers);
    }

    public Label getBookingNumber()
    {
        return container.getLabel("Booking Number");
    }

    public Select getReasonForCancel()
    {
        return container.getSelectByLabel("Cancellation Reason");
    }

    public void cancelFreeNight(String reason)
    {
        getReasonForCancel().select(reason);
        clickSubmit(assertNoError(), verifySuccess("Free Night Booking has been successfully canceled."));
    }
}
