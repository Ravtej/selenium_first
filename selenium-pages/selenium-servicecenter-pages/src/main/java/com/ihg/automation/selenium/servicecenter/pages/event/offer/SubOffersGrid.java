package com.ihg.automation.selenium.servicecenter.pages.event.offer;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;

import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.grid.Grid;
import com.ihg.automation.selenium.gwt.components.grid.GridHeader;
import com.ihg.automation.selenium.gwt.components.grid.GridRowXpath;

public class SubOffersGrid extends Grid<SubOffersGridRow, SubOffersGridRow.SubOfferCell>
{
    public SubOffersGrid(Container parent)
    {
        super(parent);
    }

    public SubOffersGridRow getRowByOfferCode(String offerCode)
    {
        return getRow(new GridRowXpath.GridRowXpathBuilder().cell(SubOffersGridRow.SubOfferCell.OFFER_CODE, offerCode)
                .build());
    }

    public void verifyHeader()
    {
        GridHeader<SubOffersGridRow.SubOfferCell> header = getHeader();
        verifyThat(header.getCell(SubOffersGridRow.SubOfferCell.OFFER_CODE),
                hasText(SubOffersGridRow.SubOfferCell.OFFER_CODE.getValue()));
        verifyThat(header.getCell(SubOffersGridRow.SubOfferCell.OFFER_NAME),
                hasText(SubOffersGridRow.SubOfferCell.OFFER_NAME.getValue()));
        verifyThat(header.getCell(SubOffersGridRow.SubOfferCell.STATUS),
                hasText(SubOffersGridRow.SubOfferCell.STATUS.getValue()));
        verifyThat(header.getCell(SubOffersGridRow.SubOfferCell.REWARD),
                hasText(SubOffersGridRow.SubOfferCell.REWARD.getValue()));
        verifyThat(header.getCell(SubOffersGridRow.SubOfferCell.GOAL),
                hasText(SubOffersGridRow.SubOfferCell.GOAL.getValue()));
        verifyThat(header.getCell(SubOffersGridRow.SubOfferCell.Q_EVENTS),
                hasText(SubOffersGridRow.SubOfferCell.Q_EVENTS.getValue()));
        verifyThat(header.getCell(SubOffersGridRow.SubOfferCell.AWARD_POSTED),
                hasText(SubOffersGridRow.SubOfferCell.AWARD_POSTED.getValue()));
    }

}
