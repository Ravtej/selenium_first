package com.ihg.automation.selenium.servicecenter.pages.event.freenight;

import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.assertNoError;
import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifySuccess;

import com.ihg.automation.selenium.common.pages.ClosablePopUpBase;
import com.ihg.automation.selenium.gwt.components.Button;
import com.ihg.automation.selenium.gwt.components.input.SearchInput;
import com.ihg.automation.selenium.gwt.components.input.Select;
import com.ihg.automation.selenium.gwt.components.label.Label;
import com.ihg.automation.selenium.listener.Verifier;

public class GoodwillFreeNightPopUp extends ClosablePopUpBase
{

    public GoodwillFreeNightPopUp()
    {
        super("Goodwill Free Night Voucher");
    }

    public Button getSave()
    {
        return container.getButton("Save");
    }

    public void clickSave(Verifier... verifiers)
    {
        getSave().clickAndWait(verifiers);
    }

    public Label getOfferName()
    {
        return container.getLabel("Offer");
    }

    public SearchInput getHotel()
    {
        return new SearchInput(container.getLabel("Hotel"));
    }

    public Select getReasonForGoodwill()
    {
        return container.getSelectByLabel("Reason");
    }

    public void goodwillFreeNightVoucher(String reason)
    {
        getReasonForGoodwill().select(reason);
        clickSave(assertNoError(), verifySuccess("Free Night Goodwill created."));
    }

}
