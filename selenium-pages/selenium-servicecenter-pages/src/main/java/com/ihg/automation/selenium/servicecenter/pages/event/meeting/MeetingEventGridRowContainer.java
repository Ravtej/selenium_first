package com.ihg.automation.selenium.servicecenter.pages.event.meeting;

import com.ihg.automation.selenium.common.event.EventGridRowContainerWithDetails;
import com.ihg.automation.selenium.gwt.components.Container;

public class MeetingEventGridRowContainer extends EventGridRowContainerWithDetails
{

    public MeetingEventGridRowContainer(Container parent)
    {
        super(parent);
    }

    public MeetingEventDetails getMeetingDetails()
    {
        return new MeetingEventDetails(container);
    }

}
