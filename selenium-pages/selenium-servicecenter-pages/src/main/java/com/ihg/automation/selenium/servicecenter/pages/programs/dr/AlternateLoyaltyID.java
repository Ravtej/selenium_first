package com.ihg.automation.selenium.servicecenter.pages.programs.dr;

import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.CustomContainerBase;
import com.ihg.automation.selenium.gwt.components.label.Label;

public class AlternateLoyaltyID extends CustomContainerBase
{
    public AlternateLoyaltyID(Container container)
    {
        super(container.getSeparator("Alternate Loyalty ID"));
    }

    public Label getType()
    {
        return container.getLabel("Type");
    }

    public Label getNumber()
    {
        return container.getLabel("Number");
    }
}
