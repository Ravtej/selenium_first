package com.ihg.automation.selenium.servicecenter.pages.event.goodwill;

import com.ihg.automation.selenium.common.pages.TabCustomContainerBase;

public class GoodwillDetailsTab extends TabCustomContainerBase
{
    public GoodwillDetailsTab(GoodwillDetailsPopUp parent)
    {
        super(parent, "Goodwill Details");
    }

    public GoodwillDetails getGoodwillDetails()
    {
        return new GoodwillDetails(container);
    }
}
