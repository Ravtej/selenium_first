package com.ihg.automation.selenium.servicecenter.pages.event.point;

public class ExpiredPointsRemovalDetailsPopUp extends PointExpirationDetailsPopUpBase
{
    public ExpiredPointsRemovalDetailsPopUp()
    {
        super("Expired Points Removal Details");
    }
}
