package com.ihg.automation.selenium.servicecenter.pages.programs.rc;

import static com.ihg.automation.common.DateUtils.currentYear;

public class YearToDateRedeemedBenefitPopUp extends RedeemedPartnerBenefitPopUpBase
{
    private static final String CURRENT_YEAR = "- " + currentYear();

    public YearToDateRedeemedBenefitPopUp()
    {
        super(CURRENT_YEAR);
    }
}
