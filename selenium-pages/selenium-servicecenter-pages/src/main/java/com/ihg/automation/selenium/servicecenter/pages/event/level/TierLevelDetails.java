package com.ihg.automation.selenium.servicecenter.pages.event.level;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasDefault;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;

import com.ihg.automation.selenium.common.EventSource;
import com.ihg.automation.selenium.common.EventSourceAware;
import com.ihg.automation.selenium.common.SiteHelperBase;
import com.ihg.automation.selenium.common.catalog.CatalogItem;
import com.ihg.automation.selenium.common.event.EventSourceExtended;
import com.ihg.automation.selenium.common.payment.Payment;
import com.ihg.automation.selenium.common.payment.PaymentDetails;
import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.CustomContainerBase;
import com.ihg.automation.selenium.gwt.components.Link;
import com.ihg.automation.selenium.gwt.components.Mode;
import com.ihg.automation.selenium.gwt.components.label.Label;

public class TierLevelDetails extends CustomContainerBase implements EventSourceAware
{
    public TierLevelDetails(Container container)
    {
        super(container);
    }

    @Override
    public EventSource getSource()
    {
        return new EventSourceExtended(container);
    }

    public Link getCollateralItem()
    {
        return new Link(container.getLabel("Collateral Item"), Link.LINK_XPATH_NO_HYPER_LINK_IN_CLASS);
    }

    public Label getReferringMember()
    {
        return container.getLabel("Referring Member");
    }

    public PaymentDetails getPaymentDetails()
    {
        return new PaymentDetails(container);
    }

    public void verify(CatalogItem upgradeAward, Payment payment, SiteHelperBase siteHelper)
    {
        verifyThat(getCollateralItem(), hasText(upgradeAward.getItemId()));
        verifyThat(getReferringMember(), hasDefault());
        getPaymentDetails().verify(payment, Mode.VIEW);
        getSource().verify(siteHelper.getSourceFactory().getNoEmployeeIdSource());
    }
}
