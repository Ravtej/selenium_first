package com.ihg.automation.selenium.servicecenter.pages.unit;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.assertNoErrors;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.assertThat;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;

import com.ihg.automation.selenium.common.AmountFieldsAware;
import com.ihg.automation.selenium.common.pages.NavigatePage;
import com.ihg.automation.selenium.common.pages.PaymentMethod;
import com.ihg.automation.selenium.common.pages.SubmitPopUpBase;
import com.ihg.automation.selenium.common.payment.PaymentDetailsPopUp;
import com.ihg.automation.selenium.gwt.components.input.Select;
import com.ihg.automation.selenium.servicecenter.pages.TopMenu;

public class PurchaseUnitsPopUp extends SubmitPopUpBase implements NavigatePage, AmountFieldsAware
{
    private TopMenu menu = new TopMenu();

    public PurchaseUnitsPopUp()
    {
        super("Purchase Loyalty Units");
    }

    @Override
    public void goTo()
    {
        if (!isDisplayed())
        {
            menu.getUnitManagement().getPurchaseUnits().clickAndWait();

            flush();
            assertThat(this, displayed(true));
        }
    }

    @Override
    public Select getAmount()
    {
        return container.getSelectByLabel("Quantity to Purchase", "Quantity", 1);
    }

    @Override
    public Select getUnitType()
    {
        return container.getSelectByLabel("Quantity to Purchase", "Units Type", 2);
    }

    public void purchaseComplimentary(String amount)
    {
        goTo();
        assertNoErrors();
        getAmount().select(amount);
        clickSubmitNoError();

        PaymentDetailsPopUp payment = new PaymentDetailsPopUp();
        payment.selectPaymentType(PaymentMethod.COMPLIMENTARY);
        payment.clickSubmitNoError();
    }

}
