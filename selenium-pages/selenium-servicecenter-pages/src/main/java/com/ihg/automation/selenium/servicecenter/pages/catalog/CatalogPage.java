package com.ihg.automation.selenium.servicecenter.pages.catalog;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.assertThat;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.automation.selenium.matchers.component.GridSize.size;
import static org.hamcrest.core.IsNot.not;

import com.ihg.automation.selenium.common.catalog.CatalogItem;
import com.ihg.automation.selenium.common.event.SearchAware;
import com.ihg.automation.selenium.common.event.SearchButtonBar;
import com.ihg.automation.selenium.common.pages.NavigatePage;
import com.ihg.automation.selenium.gwt.components.Button;
import com.ihg.automation.selenium.gwt.components.CustomContainerBase;
import com.ihg.automation.selenium.gwt.components.grid.navigation.GridNavigationPanel;
import com.ihg.automation.selenium.gwt.components.panel.Panel;
import com.ihg.automation.selenium.gwt.components.utils.ByText;
import com.ihg.automation.selenium.servicecenter.pages.TopMenu;
import com.ihg.automation.selenium.servicecenter.pages.catalog.CatalogItemsGridRow.CatalogItemsGridCell;

public class CatalogPage extends CustomContainerBase implements NavigatePage, SearchAware
{
    TopMenu menu = new TopMenu();

    public CatalogPage()
    {
        super(new Panel("Catalog"));
    }

    @Override
    public void goTo()
    {
        if (!isDisplayed())
        {
            menu.getCatalog().clickAndWait();

            flush();
            assertThat(this, displayed(true));
        }
    }

    @Override
    public CatalogSearch getSearchFields()
    {
        return new CatalogSearch(container);
    }

    @Override
    public SearchButtonBar getButtonBar()
    {
        return new SearchButtonBar(container);
    }

    public Panel getCatalogItemsPanel()
    {
        return container.getPanel(ByText.contains("Catalog Items"));
    }

    public CatalogItemsGrid getCatalogGrid()
    {
        return new CatalogItemsGrid(getCatalogItemsPanel());
    }

    public GridNavigationPanel getNavigationPanel()
    {
        return getCatalogGrid().getNavigationPanel();
    }

    public Button getClose()
    {
        return container.getButton(Button.CLOSE);
    }

    public void clickClose()
    {
        getClose().click();
    }

    public void search(String catalogId, String itemId, String itemName, String loyaltyUnitCostMin,
            String loyaltyUnitCostMax)
    {
        getButtonBar().clickClearCriteria();
        getSearchFields().populate(catalogId, itemId, itemName, loyaltyUnitCostMin, loyaltyUnitCostMax);
        getButtonBar().clickSearch();
    }

    public void searchByItemId(String itemId)
    {
        search(null, itemId, null, null, null);
    }

    public void searchByItemId(CatalogItem catalogItem)
    {
        searchByItemId(catalogItem.getItemId());
    }

    public CatalogItemDetailsPopUp openCatalogItemDetailsPopUp()
    {
        verifyThat(getCatalogGrid(), size(not(0)));

        getCatalogGrid().getRow(1).getCell(CatalogItemsGridCell.ITEM_ID).clickByLabel();
        CatalogItemDetailsPopUp catalogItemDetailsPopUp = new CatalogItemDetailsPopUp();
        verifyThat(catalogItemDetailsPopUp, displayed(true));
        return catalogItemDetailsPopUp;
    }

}
