package com.ihg.automation.selenium.servicecenter.pages.hotel.certificate;

import com.ihg.automation.selenium.common.hotel.HotelCertificate;
import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.input.Input;
import com.ihg.automation.selenium.gwt.components.input.Select;

public class HotelCertificateDetailsCreate extends HotelCertificateDetailsEditBase
{

    public HotelCertificateDetailsCreate(Container container)
    {
        super(container);
    }

    @Override
    public Input getCertificateNumber()
    {
        return container.getInputByLabel(CERTIFICATE_NUMBER);
    }

    @Override
    public Select getBillingEntity()
    {
        return container.getSelectByLabel(BILLING_ENTITY);
    }

    @Override
    public void populate(HotelCertificate details)
    {
        super.populate(details);
        getBillingEntity().select(details.getBillingEntity());
        getCertificateNumber().typeAndWait(details.getCertificateNumber());
    }
}
