package com.ihg.automation.selenium.servicecenter.pages.event.offer;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;

import com.ihg.automation.selenium.gwt.components.grid.CellsName;
import com.ihg.automation.selenium.gwt.components.grid.GridRow;
import com.ihg.automation.selenium.gwt.components.grid.GridRowXpath;

public class UniverseOfferGridRow extends GridRow<UniverseOfferGridRow.UniverseOfferCell>
{
    public UniverseOfferGridRow(UniverseOffersGrid parent, GridRowXpath xpath)
    {
        super(parent, xpath);
    }

    public enum UniverseOfferCell implements CellsName
    {
        OFFER_CODE("offerType.unicaOffer.offerCode1"), OFFER_NAME("offerType.unicaOffer.name"), START_DATE(
                "offerType.effectiveDate"), END_DATE("offerType.lastEffectiveDate"), PROGRAM("offerType.offerExtension"), REGION(
                "offerType.offerExtension"), ELIGIBLE("contextType.eligible");

        private String name;

        private UniverseOfferCell(String name)
        {
            this.name = name;
        }

        @Override
        public String getName()
        {
            return name;
        }
    }

    public UniverseOffersGridRowContainer getRowContainer()
    {
        return expand(UniverseOffersGridRowContainer.class);
    }

    public void verify(String offerCode, String eligible)
    {
        verifyThat(getCell(UniverseOfferGridRow.UniverseOfferCell.OFFER_CODE), hasText(offerCode));
        verifyThat(getCell(UniverseOfferGridRow.UniverseOfferCell.ELIGIBLE), hasText(eligible));
    }
}
