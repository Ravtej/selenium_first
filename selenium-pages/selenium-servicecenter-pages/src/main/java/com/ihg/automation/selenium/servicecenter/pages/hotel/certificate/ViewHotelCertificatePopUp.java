package com.ihg.automation.selenium.servicecenter.pages.hotel.certificate;

public class ViewHotelCertificatePopUp extends HotelCertificatePopUpBase
{
    public ViewHotelCertificatePopUp()
    {
        super("View Hotel Certificate");
    }

    @Override
    public HotelCertificateDetailsView getDetails()
    {
        return new HotelCertificateDetailsView(container);
    }

    public ReimbursementDetails getReimbursementDetails()
    {
        return new ReimbursementDetails(container);
    }

}
