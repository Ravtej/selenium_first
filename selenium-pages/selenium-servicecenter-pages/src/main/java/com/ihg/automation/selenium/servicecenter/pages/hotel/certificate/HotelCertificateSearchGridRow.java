package com.ihg.automation.selenium.servicecenter.pages.hotel.certificate;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasDefaultOrText;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;

import com.ihg.automation.selenium.common.hotel.HotelCertificate;
import com.ihg.automation.selenium.gwt.components.grid.CellsName;
import com.ihg.automation.selenium.gwt.components.grid.Grid;
import com.ihg.automation.selenium.gwt.components.grid.GridRow;
import com.ihg.automation.selenium.gwt.components.grid.GridRowXpath;

public class HotelCertificateSearchGridRow
        extends GridRow<HotelCertificateSearchGridRow.HotelCertificateSearchGridRowCell>
{

    public HotelCertificateSearchGridRow(Grid parent, GridRowXpath xpath)
    {
        super(parent, xpath);
    }

    public void verify(HotelCertificate rowData)
    {
        verifyThat(getCell(HotelCertificateSearchGridRowCell.HOTEL), hasText(rowData.getHotelCode()));
        verifyThat(getCell(HotelCertificateSearchGridRowCell.CHECK_IN), hasText(rowData.getCheckInDate()));
        verifyThat(getCell(HotelCertificateSearchGridRowCell.GUEST_NAME), hasText(rowData.getDescriptionGuestName()));
        verifyThat(getCell(HotelCertificateSearchGridRowCell.ITEM_NAME), hasText(rowData.getItemName()));
        verifyThat(getCell(HotelCertificateSearchGridRowCell.CERTIFICATE_NUMBER),
                hasText(rowData.getCertificateNumber()));
        verifyThat(getCell(HotelCertificateSearchGridRowCell.STATUS), hasText(rowData.getStatus()));
        verifyThat(getCell(HotelCertificateSearchGridRowCell.AMOUNT), hasText(rowData.getAmount()));
        verifyThat(getCell(HotelCertificateSearchGridRowCell.CURR), hasText(rowData.getCurrency().getCode()));
        verifyThat(getCell(HotelCertificateSearchGridRowCell.BILLING_ENTITY), hasText(rowData.getBillingEntity()));
        verifyThat(getCell(HotelCertificateSearchGridRowCell.FOLIO), hasDefaultOrText(rowData.getFolioNumber()));
        verifyThat(getCell(HotelCertificateSearchGridRowCell.BUNDLE), hasDefaultOrText(rowData.getBundleNumber()));
    }

    public HotelCertificateSearchRowContainer getRowContainer()
    {
        return expand(HotelCertificateSearchRowContainer.class);
    }

    public enum HotelCertificateSearchGridRowCell implements CellsName
    {
        HOTEL("hotelCode", "Hotel"), //
        CHECK_IN("checkInDate", "Check In"), //
        GUEST_NAME("descriptionGuestName", "Guest Name"), //
        ITEM_NAME("itemName", "Item Name"), //
        CERTIFICATE_NUMBER("certificateNumber", "Certificate #"), //
        STATUS("status", "Status"), //
        AMOUNT("amount", "Amount"), //
        CURR("currency", "Curr"), //
        BILLING_ENTITY("billingEntity", "Billing Entity"), //
        FOLIO("folioNumber", "Folio #"), //
        BUNDLE("bundleNumber", "Bundle");

        private String name;
        private String value;

        private HotelCertificateSearchGridRowCell(String name, String value)
        {
            this.name = name;
            this.value = value;
        }

        public String getValue()
        {
            return value;
        }

        @Override
        public String getName()
        {
            return name;
        }
    }
}
