package com.ihg.automation.selenium.servicecenter.pages.voucher;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.assertThat;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;

import com.ihg.automation.selenium.common.pages.TabCustomContainerBase;
import com.ihg.automation.selenium.gwt.components.Button;
import com.ihg.automation.selenium.gwt.components.Menu.Item;
import com.ihg.automation.selenium.gwt.components.input.Input;
import com.ihg.automation.selenium.servicecenter.pages.TopMenu;
import com.ihg.automation.selenium.servicecenter.pages.TopMenu.VoucherMenu;

public abstract class VoucherTabBase extends TabCustomContainerBase
{
    private TopMenu menu = new TopMenu();
    private VoucherPopUp voucherPopUp;

    public VoucherTabBase(VoucherPopUp voucherPopUp, String tabName)
    {
        super(voucherPopUp, tabName);
        this.voucherPopUp = voucherPopUp;
    }

    abstract protected Item getMenuItem();

    abstract protected String getVoucherNumberLabel();

    public Input getVoucherNumber()
    {
        return container.getInputByLabel(getVoucherNumberLabel());
    }

    public Button getClose()
    {
        return container.getButton(Button.CLOSE);
    }

    public void clickClose()
    {
        getClose().clickAndWait();
    }

    @Override
    public void goTo()
    {
        if (!voucherPopUp.isDisplayed())
        {
            VoucherMenu voucher = menu.getVoucher();
            voucher.clickAndWait();
            getMenuItem().clickAndWait();

            voucherPopUp.flush();
            assertThat(voucherPopUp, displayed(true));
        }

        super.goTo();
    }
}
