package com.ihg.automation.selenium.servicecenter.pages.event.offer;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;

import com.ihg.automation.selenium.common.EventSource;
import com.ihg.automation.selenium.common.EventSourceAware;
import com.ihg.automation.selenium.common.event.EventSourceExtended;
import com.ihg.automation.selenium.common.offer.GuestOffer;
import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.CustomContainerBase;
import com.ihg.automation.selenium.gwt.components.label.Label;

public class RegistrationDetailsContainer extends CustomContainerBase implements EventSourceAware
{

    public RegistrationDetailsContainer(Container container)
    {
        super(container.getSeparator("Registration Details"));
    }

    public Label getRegistrationCode()
    {
        return container.getLabel("Registration Code");
    }

    public Label getRegistrationDate()
    {
        return container.getLabel("Registration Date");
    }

    public Label getRetroDate()
    {
        return container.getLabel("Retro Date");
    }

    public Label getMaxDaysToPlay()
    {
        return container.getLabel("Max Days to Play");
    }

    public Label getLastDayToPlay()
    {
        return container.getLabel("Last Day to Play");
    }

    @Override
    public EventSource getSource()
    {
        return new EventSourceExtended(container);
    }

    public void verify(GuestOffer guestOffer)
    {
        verifyThat(getRegistrationCode(), hasText(guestOffer.getRegistrationCode()));
        verifyThat(getRegistrationDate(), hasText(guestOffer.getRegistrationDate()));
        verifyThat(getRetroDate(), hasText(guestOffer.getRetroDate()));
        verifyThat(getMaxDaysToPlay(), hasText(guestOffer.getOffer().getMaxDaysToPlay()));
        verifyThat(getLastDayToPlay(), hasText(guestOffer.getLastDayToPlay()));
        getSource().verify(guestOffer.getSource());
    }
}
