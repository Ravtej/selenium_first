package com.ihg.automation.selenium.servicecenter.pages.history;

public class AuditConstant
{
    public static final String SET = "Set";
    public static final String NOT_SET = "Not set";
    public static final String OPT_IN = "Opt In";
    public static final String OPT_OUT = "Opt Out";
    public static final String FALSE = "false";
    public static final String TRUE = "true";
    public static final String UNLOCKED = "Unlocked";

    public static class PersonalInformation
    {
        public static final String PERSONAL_INFORMATION = "Personal Information";

        public static final String DATE_OF_BIRTH = "Date of Birth";
        public static final String COUNTRY = "Country";
        public static final String NAME = "Name";
        public static final String NATIVE_NAME = "Native Name";
        public static final String GENDER = "Gender";
    }

    public static class CommunicationPreferences
    {
        public static final String COMMUNICATION_PREFERENCES = "Communication Preferences";
        public static final String COMMUNICATIONS = "Communications";
        public static final String PHONE_FOR_CUSTOMER_SERVICE = "Phone for Customer Service";
        public static final String EMAIL_FOR_CUSTOMER_SERVICE = "Email for Customer Service";

        public static final String STATUS = "Status";
        public static final String EMAIL = "Email";
        public static final String WRITTEN_LANGUAGE = "Written Language";
        public static final String SPOKEN_LANGUAGE = "Spoken Language";
        public static final String DAYS_OF_THE_WEEK = "Days of the Week";
        public static final String PHONE = "Phone";
        public static final String THIRD_PARTY_SHARE = "Third Party Share";
        public static final String PREFERRED_TIME = "Preferred Time";
    }

    public static class ContactInformation
    {
        public static final String CONTACT_INFORMATION = "Contact Information";

        public static final String PREFERRED = "Preferred";
        public static final String NON_PREFERRED = "Non-Preferred";
        public static final String MARK_INVALID = "Mark invalid";
        public static final String TYPE = "Type";
        public static final String USERNAME_HANDLE = "Username/Handle";

        public static class Address
        {
            public static final String ADDRESS_ADDED = "Address Added";
            public static final String NATIVE_ADDRESS_ADDED = "Native Address Added";
            public static final String ADDRESS_CHANGED = "Address Changed";
            public static final String ADDRESS_DELETED = "Address Deleted";

            public static final String STATE = "State";
            public static final String ZIP_CODE = "Zip Code";
            public static final String COUNTRY = "Country";
            public static final String DO_NOT_CLEAN = "Do not clean";
            public static final String ADDRESS_1 = "Address 1";
            public static final String CITY = "City";
        }

        public static class Email
        {
            public static final String EMAIL_ADDED = "Email Added";
            public static final String EMAIL_CHANGED = "Email Changed";
            public static final String EMAIL_DELETED = "Email Deleted";

            public static final String EMAIL = "Email";
            public static final String FORMAT = "Format";
        }

        public static class Phone
        {
            public static final String PHONE_ADDED = "Phone Added";
            public static final String PHONE_CHANGED = "Phone Changed";
            public static final String PHONE_DELETED = "Phone Deleted";
            public static final String SMS_ADDED = "SMS Added";

            public static final String FULL_NUMBER = "Full Number";
        }

    }

    public static class AccountInformation
    {
        public static final String ACCOUNT_INFORMATION = "Account Information";

        public static final String ACCOUNT_STATUS = "Account Status";
        public static final String SELF_SERVICE_LOCKOUT_INDICATOR = "Self Service Lockout Indicator";
        public static final String PIN = "PIN";

        public static final String ALTERNATE_LOYALTY_ID_HERTZ_ADDED = "Alternate Loyalty ID Hertz Added";
        public static final String ALTERNATE_LOYALTY_ID_HERTZ_CHANGED = "Alternate Loyalty ID Hertz Changed";
        public static final String ALTERNATE_LOYALTY_ID_HERTZ_DELETED = "Alternate Loyalty ID Hertz Deleted";

        public static final String TYPE = "Type";
        public static final String NUMBER = "Number";
    }

    public static class TravelProfile
    {
        public static final String TRAVEL_PROFILE = "Travel Profile";

        public static final String DEFAULT = "Default";
        public static final String TYPE = "Travel Profile Type";
        public static final String NAME = "Travel Profile Name";
        public static final String IATA_NUMBER = "Travel Profile IATA Number";
        public static final String CORPORATE_ACCOUNT_NUMBER = "Corporate Account Number";
    }

    public static class CreditCard
    {
        public static final String CREDIT_CARD = "Credit Card";

        public static final String NUMBER = "Number";
        public static final String TYPE = "Type";
        public static final String EXPIRATION_DATE = "Expiration Date";
    }

    public static class StayPreferences
    {
        public static final String STAY_PREFERENCES = "Stay Preferences";
        public static final String VAT_FAPIAO = "VAT Fapiao";

        public static final String FAPIAO = "Fapiao";
        public static final String COMPANY_NAME = "Company Name";
        public static final String TAX_IDENTIFICATION_NO = "Tax Identification No.";
        public static final String COMPANY_ADDRESS = "Company Address";
        public static final String COMPANY_TELEPHONE = "Company Telephone";
        public static final String COMPANY_BANK = "Company Bank";
        public static final String BANK_ACCOUNT_NO = "Bank Account No.";
    }

    public static class ProgramInformation
    {
        public static final String PROGRAM_INFORMATION = "Program Information";

        public static final String RC_MEMBER_SINCE_DATE = "RC Member Since Date";
        public static final String KARMA_MEMBER_SINCE_DATE = "Karma Member Since Date";
    }
}
