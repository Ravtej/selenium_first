package com.ihg.automation.selenium.servicecenter.pages.event.stay;

import com.ihg.automation.selenium.gwt.components.CustomContainerBase;
import com.ihg.automation.selenium.gwt.components.label.Label;

public class StayBookingInfoView extends StayBookingInfoBase
{
    public StayBookingInfoView(CustomContainerBase parent)
    {
        super(parent);
    }

    @Override
    public Label getBookingSource()
    {
        return container.getLabel(BOOKING_SOURCE_LABEL);
    }
}
