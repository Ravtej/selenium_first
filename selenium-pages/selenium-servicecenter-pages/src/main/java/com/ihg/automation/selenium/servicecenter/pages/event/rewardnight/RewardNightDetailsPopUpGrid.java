package com.ihg.automation.selenium.servicecenter.pages.event.rewardnight;

import com.ihg.automation.selenium.gwt.components.Component;
import com.ihg.automation.selenium.gwt.components.grid.CellsName;
import com.ihg.automation.selenium.gwt.components.grid.Grid;

public class RewardNightDetailsPopUpGrid
        extends Grid<RewardNightDetailsPopUpGridRow, RewardNightDetailsPopUpGrid.RewardNightPopUpCell>
{

    public RewardNightDetailsPopUpGrid(Component parent)
    {
        super(parent);
    }

    public enum RewardNightPopUpCell implements CellsName
    {
        SUFFIX("suffix"), STATUS("status"), LOYALTY_UNITS("loyaltyUnits");

        private String name;

        private RewardNightPopUpCell(String name)
        {
            this.name = name;
        }

        @Override
        public String getName()
        {
            return this.name;
        }
    }

}
