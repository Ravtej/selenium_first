package com.ihg.automation.selenium.servicecenter.pages.event.rewardnight;

import com.ihg.automation.selenium.common.pages.PopUpBase;
import com.ihg.automation.selenium.common.pages.TabCustomContainerBase;
import com.ihg.automation.selenium.gwt.components.panel.Panel;

public class RewardNightDetailsTab extends TabCustomContainerBase
{

    public RewardNightDetailsTab(PopUpBase popUp)
    {
        super(popUp, "Reward Night Details");
    }

    public RewardNightDetails getDetails()
    {
        return new RewardNightDetails(container);
    }

    private Panel getPanel()
    {
        return container.getPanel("Reward Night Reimbursement Detail");
    }

    public RewardNightDetailsPopUpGrid getGrid()
    {
        return new RewardNightDetailsPopUpGrid(getPanel());
    }
}
