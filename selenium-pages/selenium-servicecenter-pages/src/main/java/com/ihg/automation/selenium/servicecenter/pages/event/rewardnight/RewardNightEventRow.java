package com.ihg.automation.selenium.servicecenter.pages.event.rewardnight;

import com.ihg.automation.common.DateUtils;

public class RewardNightEventRow
{
    private String transactionDate = DateUtils.getFormattedDate();
    private String transactionType = "";
    private String hotel = "";
    private String checkInDate = "";
    private String checkOutDate = "";
    private String nights = "";
    private String certificateNumber;
    private String ihgUnits = "";

    public void setTransactionDate(String transactionDate)
    {
        this.transactionDate = transactionDate;
    }

    public String getTransactionDate()
    {
        return transactionDate;
    }

    public void setTransactionType(String transactionType)
    {
        this.transactionType = transactionType;
    }

    public String getTransactionType()
    {
        return transactionType;
    }

    public void setHotel(String hotel)
    {
        this.hotel = hotel;
    }

    public String getHotel()
    {
        return hotel;
    }

    public void setCheckOutDate(String checkOutDate)
    {
        this.checkOutDate = checkOutDate;
    }

    public String getCheckOutDate()
    {
        return checkOutDate;
    }

    public void setNights(String nights)
    {
        this.nights = nights;
    }

    public String getNights()
    {
        return nights;
    }

    public void setCheckInDate(String checkInDate)
    {
        this.checkInDate = checkInDate;
    }

    public String getCheckInDate()
    {
        return checkInDate;
    }

    public void setCertificateNumber(String certificateNumber)
    {
        this.certificateNumber = certificateNumber;
    }

    public String getCertificateNumber()
    {
        return certificateNumber;
    }

    public void setIhgUnits(String ihgUnits)
    {
        this.ihgUnits = ihgUnits;
    }

    public String getIhgUnits()
    {
        return ihgUnits;
    }

}
