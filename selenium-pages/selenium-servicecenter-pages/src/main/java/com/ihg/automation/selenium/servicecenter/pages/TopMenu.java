package com.ihg.automation.selenium.servicecenter.pages;

import com.ihg.automation.selenium.gwt.components.Button;
import com.ihg.automation.selenium.gwt.components.CustomContainerBase;
import com.ihg.automation.selenium.gwt.components.Menu;
import com.ihg.automation.selenium.gwt.components.utils.ByText;

public class TopMenu extends CustomContainerBase
{

    public HotelOperationsMenu getHotelOperations()
    {
        return new HotelOperationsMenu();
    }

    public Menu getCatalog()
    {
        return container.getMenu("Catalog");
    }

    public Button getPopInfoAvailable()
    {
        return container.getButton("Pop Info Available");
    }

    public Menu getEnrollment()
    {
        return container.getMenu("Enrollment");
    }

    public VoucherMenu getVoucher()
    {
        return new VoucherMenu();
    }

    public Menu getDeposit()
    {
        return container.getMenu("Deposit");
    }

    public OfferMenu getOffer()
    {
        return new OfferMenu();
    }

    public UnitManagementMenu getUnitManagement()
    {
        return new UnitManagementMenu();
    }

    public Menu getShoppingCart()
    {
        return container.getMenu(ByText.startsWith("Shopping Cart"));
    }

    public class HotelOperationsMenu extends Menu
    {

        public HotelOperationsMenu()
        {
            super(container, "Hotel Operations");
        }

        public Item getRewardAndFreeNightReimb()
        {
            return this.getSubMenu("Reward/Free Night Reimbursement");
        }

        public Item getHotelAndOtherCertificates()
        {
            return this.getSubMenu("In-Hotel Certificates/Other Reimbursement");
        }

        public Item getFreeNightFlatReimb()
        {
            return this.getSubMenu("Free Night Flat Reimbursement");
        }

        public Item getHotelStartupFees()
        {
            return this.getSubMenu("Hotel Startup Fees");
        }
    }

    public class VoucherMenu extends Menu
    {

        public VoucherMenu()
        {
            super(container, "Voucher");
        }

        public Item getLookup()
        {
            return this.getSubMenu("Lookup voucher");
        }

        public Item getDeposit()
        {
            return this.getSubMenu("Deposit voucher");
        }

        public Item getOrder()
        {
            return this.getSubMenu("Order voucher");
        }
    }

    public class OfferMenu extends Menu
    {

        public OfferMenu()
        {
            super(container, "Offer");
        }

        public Item getSearch()
        {
            return this.getSubMenu("Offers Search");
        }

        public Item getRegistration()
        {
            return this.getSubMenu("Offers Registration");
        }
    }

    public class UnitManagementMenu extends Menu
    {

        public UnitManagementMenu()
        {
            super(container, "Unit Management");
        }

        public Item getGoodwillUnits()
        {
            return this.getSubMenu("Goodwill Units");
        }

        public Item getPurchaseUnits()
        {
            return this.getSubMenu("Purchase Units");
        }

        public Item getPurchaseGift()
        {
            return this.getSubMenu("Purchase Gift");
        }

        public Item getTransferUnits()
        {
            return this.getSubMenu("Transfer Units");
        }

        public Item getPurchaseTierLevel()
        {
            return this.getSubMenu("Purchase Tier-Level");
        }

        public Item getTransferQNights()
        {
            return this.getSubMenu("Transfer Qualifying Nights");
        }
    }
}
