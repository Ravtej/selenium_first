package com.ihg.automation.selenium.servicecenter.pages.unit;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.assertThat;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;

import com.ihg.automation.selenium.common.pages.NavigatePage;
import com.ihg.automation.selenium.common.pages.SubmitPopUpBase;
import com.ihg.automation.selenium.common.unit.MemberFieldsAware;
import com.ihg.automation.selenium.gwt.components.input.Input;
import com.ihg.automation.selenium.gwt.components.input.Select;
import com.ihg.automation.selenium.gwt.components.label.Label;
import com.ihg.automation.selenium.servicecenter.pages.TopMenu;

public class TransferNightsPopUp extends SubmitPopUpBase implements NavigatePage, MemberFieldsAware
{
    private TopMenu menu = new TopMenu();

    public TransferNightsPopUp()
    {
        super("Transfer Annual Qualifying Nights");
    }

    @Override
    public void goTo()
    {
        if (!isDisplayed())
        {
            menu.getUnitManagement().getTransferQNights().clickAndWait();

            flush();
            assertThat(this, displayed(true));
        }
    }

    @Override
    public Input getMembershipId()
    {
        return container.getInputByLabel("Transfer To Membership ID");
    }

    @Override
    public Label getMemberName()
    {
        return container.getLabel("Member Name");
    }

    public Select getYear()
    {
        return container.getSelectByLabel("Year");
    }

    public Input getQuantity()
    {
        return container.getInputByLabel("Qualifying Nights");
    }

    public void verify(String year, String nightsAmount)
    {
        verifyThat(getQuantity(), hasText(nightsAmount));
        verifyThat(getYear(), hasText(year));
        // TODO mandatory fields
    }
}
