package com.ihg.automation.selenium.servicecenter.pages.occupation;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static com.ihg.automation.selenium.matchers.component.ViewableMatcher.hasText;

import com.ihg.automation.selenium.common.member.OccupationInfo;
import com.ihg.automation.selenium.common.pages.Populate;
import com.ihg.automation.selenium.common.pages.VerifyMultiMode;
import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.CustomContainerBase;
import com.ihg.automation.selenium.gwt.components.Mode;
import com.ihg.automation.selenium.gwt.components.input.Input;
import com.ihg.automation.selenium.gwt.components.input.SearchInput;
import com.ihg.automation.selenium.gwt.components.label.Label;

public class OccupationInfoFields extends CustomContainerBase
        implements Populate<OccupationInfo>, VerifyMultiMode<OccupationInfo>
{
    public OccupationInfoFields(Container container)
    {
        super(container);
    }

    public SearchInput getCorporateNumber()
    {
        return new SearchInput(container.getLabel("Corporate Account Number"));
    }

    public Label getCorporateName()
    {
        return container.getLabel("Corporate Account Name");
    }

    public Input getCompanyName()
    {
        return container.getInputByLabel("Company Name");
    }

    public Input getJobTitle()
    {
        return container.getInputByLabel("Job Title");
    }

    @Override
    public void populate(OccupationInfo occupationInfo)
    {
        if (occupationInfo != null)
        {
            getCorporateNumber().typeAndWait(occupationInfo.getCorporateNumber());
            getCompanyName().type(occupationInfo.getCompanyName());
            getJobTitle().type(occupationInfo.getJobTitle());
        }
    }

    @Override
    public void verify(OccupationInfo occupationInfo, Mode mode)
    {
        if (occupationInfo != null)
        {
            verifyThat(getCorporateName(), hasText(occupationInfo.getCorporateName()));

            verifyThat(getCorporateNumber(), hasText(occupationInfo.getCorporateNumber(), mode));
            verifyThat(getCompanyName(), hasText(occupationInfo.getCompanyName(), mode));
            verifyThat(getJobTitle(), hasText(occupationInfo.getJobTitle(), mode));
        }
    }
}
