package com.ihg.automation.selenium.servicecenter.pages.event.offer;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;

import com.ihg.automation.selenium.common.offer.Offer;
import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.CustomContainerBase;
import com.ihg.automation.selenium.gwt.components.label.Label;

public class OfferDatesContainer extends CustomContainerBase
{
    public OfferDatesContainer(Container container)
    {
        super(container.getSeparator("Offer Dates"));
    }

    public Label getOfferStartDate()
    {
        return container.getLabel("Offer Start Date");
    }

    public Label getOfferEndDate()
    {
        return container.getLabel("Offer End Date");
    }

    public Label getBookingStartDate()
    {
        return container.getLabel("Booking Start Date");
    }

    public Label getBookingEndDate()
    {
        return container.getLabel("Booking End Date");
    }

    public void verify(Offer offer)
    {
        verifyThat(getOfferStartDate(), hasText(offer.getStartDate()));
        verifyThat(getOfferEndDate(), hasText(offer.getEndDate()));
        verifyThat(getBookingStartDate(), hasText(offer.getBookingStartDate()));
        verifyThat(getBookingEndDate(), hasText(offer.getBookingEndDate()));
    }

    public void capture(Offer.Builder builder)
    {
        builder.startDate(getOfferStartDate().getText());
        builder.endDate(getOfferEndDate().getText());
        builder.bookingStartDate(getBookingStartDate().getText());
        builder.bookingEndDate(getBookingEndDate().getText());
    }
}
