package com.ihg.automation.selenium.servicecenter.pages.voucher;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;

import com.ihg.automation.selenium.common.Constant;
import com.ihg.automation.selenium.common.annual.ActivityAmount;
import com.ihg.automation.selenium.common.order.VoucherOrder;
import com.ihg.automation.selenium.common.pages.Verify;
import com.ihg.automation.selenium.common.payment.CurrencyAmount;
import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.CustomContainerBase;
import com.ihg.automation.selenium.gwt.components.label.Label;

public class OrderTotalValues extends CustomContainerBase implements Verify<VoucherOrder>
{
    public OrderTotalValues(Container container)
    {
        super(container);
    }

    public ActivityAmount getTotalLoyaltyUnits()
    {
        return new ActivityAmount(container.getLabel("Total Loyalty Units"));
    }

    public CurrencyAmount getTotalCash()
    {
        return new CurrencyAmount(container.getLabel("Total Cash"));
    }

    public Label getOrderDate()
    {
        return container.getLabel("Order Date");
    }

    @Override
    public void verify(VoucherOrder voucherOrder)
    {
        getTotalLoyaltyUnits().verify(Constant.NOT_AVAILABLE, null);
        getTotalCash().verify(voucherOrder.getVoucherCostAmount(), voucherOrder.getCurrencyCode());
        verifyThat(getOrderDate(), hasText(voucherOrder.getTransactionDate()));
    }
}
