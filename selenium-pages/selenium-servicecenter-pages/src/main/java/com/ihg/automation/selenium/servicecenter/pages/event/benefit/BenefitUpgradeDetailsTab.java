package com.ihg.automation.selenium.servicecenter.pages.event.benefit;

import com.ihg.automation.selenium.common.pages.PopUpBase;
import com.ihg.automation.selenium.common.pages.TabCustomContainerBase;

public class BenefitUpgradeDetailsTab extends TabCustomContainerBase
{
    public BenefitUpgradeDetailsTab(PopUpBase popUp)
    {
        super(popUp, "Benefit Upgrade Details");
    }

    public BenefitRedeemDetails getBenefitUpgradeDetails()
    {
        return new BenefitRedeemDetails(container);
    }
}
