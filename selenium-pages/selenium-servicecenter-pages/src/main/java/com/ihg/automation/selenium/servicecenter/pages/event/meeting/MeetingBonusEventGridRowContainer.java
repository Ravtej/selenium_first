package com.ihg.automation.selenium.servicecenter.pages.event.meeting;

import com.ihg.automation.selenium.common.EventSource;
import com.ihg.automation.selenium.common.EventSourceAware;
import com.ihg.automation.selenium.common.SiteHelperBase;
import com.ihg.automation.selenium.common.deposit.Deposit;
import com.ihg.automation.selenium.common.event.EventGridRowContainerWithDetails;
import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.servicecenter.pages.event.deposit.DepositDetails;

public class MeetingBonusEventGridRowContainer extends EventGridRowContainerWithDetails implements EventSourceAware
{
    public MeetingBonusEventGridRowContainer(Container parent)
    {
        super(parent);
    }

    public DepositDetails getDepositDetails()
    {
        return new DepositDetails(container);
    }

    @Override
    public EventSource getSource()
    {
        return new EventSource(container);
    }

    public void verify(Deposit deposit, SiteHelperBase siteHelper)
    {
        getDepositDetails().verify(deposit);
        getSource().verify(siteHelper.getSourceFactory().getNoEmployeeIdSource());
    }
}
