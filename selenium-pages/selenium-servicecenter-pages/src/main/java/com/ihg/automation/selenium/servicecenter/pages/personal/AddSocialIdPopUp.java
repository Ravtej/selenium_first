package com.ihg.automation.selenium.servicecenter.pages.personal;

public class AddSocialIdPopUp extends SocialIdPopUpBase
{
    public AddSocialIdPopUp()
    {
        super("Add Social Id");
    }
}
