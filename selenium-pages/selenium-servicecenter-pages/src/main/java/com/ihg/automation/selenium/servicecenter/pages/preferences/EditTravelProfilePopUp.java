package com.ihg.automation.selenium.servicecenter.pages.preferences;

import com.ihg.automation.selenium.gwt.components.Image;

public class EditTravelProfilePopUp extends TravelProfilePopUpBase
{
    public EditTravelProfilePopUp()
    {
        super("Edit Travel Profile");
    }

    public Image getDefaultIndicator()
    {
        return new Image(container.getLabel("Default"));
    }
}
