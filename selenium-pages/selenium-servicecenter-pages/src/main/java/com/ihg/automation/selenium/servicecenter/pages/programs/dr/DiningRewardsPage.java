package com.ihg.automation.selenium.servicecenter.pages.programs.dr;

import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.servicecenter.pages.programs.ProgramPageBase;
import com.ihg.automation.selenium.servicecenter.pages.programs.RenewExtendProgramSummary;

public class DiningRewardsPage extends ProgramPageBase
{

    public DiningRewardsPage()
    {
        super(Program.DR);
    }

    @Override
    public RenewExtendProgramSummary getSummary()
    {
        return new RenewExtendProgramSummary(container);
    }

    public AlternateLoyaltyID getAlternateLoyaltyID()
    {
        return new AlternateLoyaltyID(container);
    }
}
