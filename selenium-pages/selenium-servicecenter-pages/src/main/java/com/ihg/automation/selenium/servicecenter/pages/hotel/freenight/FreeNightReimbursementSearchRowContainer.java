package com.ihg.automation.selenium.servicecenter.pages.hotel.freenight;

import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.CustomContainerBase;

public class FreeNightReimbursementSearchRowContainer extends CustomContainerBase
{
    public FreeNightReimbursementSearchRowContainer(Container parent)
    {
        super(parent);
    }

    public FreeNightReimbursementSearchRowDetail getDetail(String confirmationNumber)
    {
        return new FreeNightReimbursementSearchRowDetail(container,
                String.format(".//span[normalize-space(text())='%s']/ancestor::table[1]", confirmationNumber));
    }

}
