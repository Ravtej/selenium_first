package com.ihg.automation.selenium.servicecenter.pages.event.stay;

import com.ihg.automation.selenium.gwt.components.CustomContainerBase;
import com.ihg.automation.selenium.gwt.components.Link;

public class StayInfoEdit extends StayInfoEditBase
{
    public StayInfoEdit(CustomContainerBase parent)
    {
        super(parent);
    }

    @Override
    public Link getHotel()
    {
        return new Link(container.getLabel(HOTEL_LABEL));
    }
}
