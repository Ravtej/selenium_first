package com.ihg.automation.selenium.servicecenter.pages.preferences;

import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyNoError;
import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifySuccess;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.enabled;

import com.ihg.automation.selenium.common.pages.TabCustomContainerBase;
import com.ihg.automation.selenium.common.preferences.TravelProfileType;
import com.ihg.automation.selenium.gwt.components.Button;
import com.ihg.automation.selenium.gwt.components.panel.Panel;
import com.ihg.automation.selenium.servicecenter.pages.Tabs;

public class StayPreferencesPage extends TabCustomContainerBase
{

    public static final int TRAVEL_PROFILE_MAX_COUNT = 5;

    public StayPreferencesPage()
    {
        super(new Tabs().getCustomerInfo().getStayPreferences());
    }

    public VatFapiaoDetails getVatFapiaoDetails()
    {
        return new VatFapiaoDetails(container);
    }

    public Button getEdit()
    {
        return container.getButton("Edit");
    }

    public Button getReset()
    {
        return container.getButton("Reset");
    }

    public Button getSave()
    {
        return container.getButton("Save");
    }

    public Button getCancel()
    {
        return container.getButton("Cancel");
    }

    public Panel getTravelProfiles()
    {
        return container.getPanel("Travel Profiles");
    }

    public Button getAddTravelProfile()
    {
        return container.getButton("Add Travel Profile");
    }

    private Panel getTravelProfilesPanel()
    {
        return container.getPanel("Travel Profiles");
    }

    public TravelProfileGrid getTravelProfilesGrid()
    {
        return new TravelProfileGrid(getTravelProfilesPanel());
    }

    public void addMaxNumberOfTravelProfiles()
    {
        for (int count = 1; count < TRAVEL_PROFILE_MAX_COUNT; count++)
        {
            TravelProfilePopUpBase travelProfilePopUp = new AddTravelProfilePopUp();
            getAddTravelProfile().clickAndWait();
            travelProfilePopUp.getTravelProfileName().type("Profile" + count);
            travelProfilePopUp.getType().select(TravelProfileType.LEISURE.getCodeWithValue());
            travelProfilePopUp.getSave().clickAndWait(verifyNoError(),
                    verifySuccess(TravelProfilePopUpBase.SUCCESS_MESSAGE_CREATE));
        }
        verifyThat(getAddTravelProfile(), enabled(false));
    }
}
