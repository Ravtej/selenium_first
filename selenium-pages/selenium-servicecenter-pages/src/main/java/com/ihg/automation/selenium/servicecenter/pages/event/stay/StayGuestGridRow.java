package com.ihg.automation.selenium.servicecenter.pages.event.stay;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasTextAsFormattedInt;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.isDisplayedWithWait;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;

import com.ihg.automation.selenium.common.stay.Stay;
import com.ihg.automation.selenium.gwt.components.grid.CellsName;
import com.ihg.automation.selenium.gwt.components.grid.GridRow;
import com.ihg.automation.selenium.gwt.components.grid.GridRowXpath;

public class StayGuestGridRow extends GridRow<StayGuestGridRow.StayCell>
{
    public StayGuestGridRow(StayGuestGrid parent, GridRowXpath xpath)
    {
        super(parent, xpath);
    }

    public StayGuestGridRowContainer getDetails()
    {
        return expand(StayGuestGridRowContainer.class);
    }

    public enum StayCell implements CellsName
    {
        TRANS_DATE("eventDate"), TRANS_TYPE("stayEventType"), HOTEL("hotelCode"), BRAND("brandCode"), CHECK_IN(
                "checkIn"), CHECK_OUT("checkOut"), NIGHTS("nights"), QUALIFYING_NIGHTS("qualifyingNights"), TIER_LEVEL_NIGHTS(
                "tierLevelNights"), IHG_UNITS("loyaltyUnits.ihgUnits.amount"), ALLIANCE_UNITS(
                "loyaltyUnits.allianceUnits.amount");

        private String name;

        private StayCell(String name)
        {
            this.name = name;
        }

        @Override
        public String getName()
        {
            return name;
        }
    }

    public void verify(Stay stay)
    {
        verifyThat(getCell(StayCell.TRANS_DATE), hasText(stay.getEventDate()));
        verifyThat(getCell(StayCell.TRANS_TYPE), hasText(stay.getStayEventType()));
        verifyThat(getCell(StayCell.HOTEL), hasText(stay.getHotelCode()));
        verifyThat(getCell(StayCell.BRAND), hasText(stay.getBrandCode()));
        verifyThat(getCell(StayCell.CHECK_IN), hasText(stay.getCheckIn()));
        verifyThat(getCell(StayCell.CHECK_OUT), hasText(stay.getCheckOut()));
        verifyThat(getCell(StayCell.NIGHTS), hasText(stay.getNightsAsString()));
        verifyThat(getCell(StayCell.QUALIFYING_NIGHTS), hasText(stay.getQualifyingNightsAsString()));
        verifyThat(getCell(StayCell.TIER_LEVEL_NIGHTS), hasText(stay.getTierLevelNights()));
        verifyThat(getCell(StayCell.IHG_UNITS), hasTextAsFormattedInt(stay.getIhgUnits()));
        verifyThat(getCell(StayCell.ALLIANCE_UNITS), hasTextAsFormattedInt(stay.getAllianceUnits()));
    }

    public void verifyDisqualifyPopUp(String... reason)
    {
        getCell(StayCell.QUALIFYING_NIGHTS).asLink().clickAndWait();
        DisqualifyReasonPopUp popUp = new DisqualifyReasonPopUp();
        verifyThat(popUp, isDisplayedWithWait());
        popUp.verifyReasonAndClose(reason);
    }

    public AdjustStayPopUp openAdjustPopUp()
    {
        getCell(StayGuestGridRow.StayCell.TRANS_DATE).clickAndWait();

        AdjustStayPopUp adjustStayPopUp = new AdjustStayPopUp();
        verifyThat(adjustStayPopUp, displayed(true));

        return adjustStayPopUp;
    }
}
