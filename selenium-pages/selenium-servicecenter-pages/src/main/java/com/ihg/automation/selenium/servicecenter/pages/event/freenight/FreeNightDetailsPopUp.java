package com.ihg.automation.selenium.servicecenter.pages.event.freenight;

import com.ihg.automation.selenium.common.billing.EventBillingDetailsTab;
import com.ihg.automation.selenium.common.billing.EventBillingTabAware;
import com.ihg.automation.selenium.common.pages.ClosablePopUpBase;
import com.ihg.automation.selenium.gwt.components.Button;
import com.ihg.automation.selenium.gwt.components.PopUpWindow;

public class FreeNightDetailsPopUp extends ClosablePopUpBase implements EventBillingTabAware
{

    public FreeNightDetailsPopUp()
    {
        super(PopUpWindow.withoutHeader("Free Night Details"));
    }

    public FreeNightDetailsTab getFreeNightDetailsTab()
    {
        return new FreeNightDetailsTab(this);
    }

    public OfferDetailsFreeNightTab getOfferDetailsTab()
    {
        return new OfferDetailsFreeNightTab(this);
    }

    public Button getBookFreeNightButton()
    {
        return container.getButton("Book Free Night");
    }

    public void clickBookFreeNightButton()
    {
        getBookFreeNightButton().clickAndWait();
    }

    public Button getGoodwillFreeNightButton()
    {
        return container.getButton("Goodwill Free Night");
    }

    public void clickGoodwillFreeNightButton()
    {
        getGoodwillFreeNightButton().clickAndWait();
    }

    @Override
    public EventBillingDetailsTab getBillingDetailsTab()
    {
        return new EventBillingDetailsTab(this);
    }
}
