package com.ihg.automation.selenium.servicecenter.pages.event.stay;

import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyNoError;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.assertThat;
import static com.ihg.automation.selenium.common.Constant.NOT_AVAILABLE;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;

import com.ihg.automation.selenium.common.EventSourceAware;
import com.ihg.automation.selenium.common.SiteHelperBase;
import com.ihg.automation.selenium.common.event.EventGridRowContainerWithDetails;
import com.ihg.automation.selenium.common.event.EventSourceExtended;
import com.ihg.automation.selenium.common.stay.Revenue;
import com.ihg.automation.selenium.common.stay.Stay;
import com.ihg.automation.selenium.gwt.components.Button;
import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.Mode;

public class StayGuestGridRowContainer extends EventGridRowContainerWithDetails
        implements StayDetailsAware, EventSourceAware
{
    public StayGuestGridRowContainer(Container parent)
    {
        super(parent);
    }

    @Override
    public StayInfoView getStayInfo()
    {
        return new StayInfoView(this);
    }

    @Override
    public GuestInfo getGuestInfo()
    {
        return new GuestInfo(this);
    }

    @Override
    public RevenueDetailsView getRevenueDetails()
    {
        return new RevenueDetailsView(this);
    }

    @Override
    public StayBookingInfoView getBookingInfo()
    {
        return new StayBookingInfoView(this);
    }

    @Override
    public EventSourceExtended getSource()
    {
        return new EventSourceExtended(container);
    }

    public Button getRemove()
    {
        return new Button(container, "Remove");
    }

    public void clickRemove()
    {
        getRemove().clickAndWait();
    }

    public AdjustStayPopUp openAdjustPopUp()
    {
        clickDetails(verifyNoError());

        AdjustStayPopUp adjustStayPopUp = new AdjustStayPopUp();
        assertThat(adjustStayPopUp, displayed(true));

        return adjustStayPopUp;
    }

    public void verifyRoomRevenueDetails(Revenue room, Mode mode)
    {
        AdjustStayPopUp adjustPopUp = openAdjustPopUp();
        adjustPopUp.getStayDetailsTab().getStayDetails().getRevenueDetails().getTotalRoom().verify(room, mode);
        adjustPopUp.clickClose();
    }

    public void verify(Stay stay, SiteHelperBase helper)
    {
        getStayInfo().verify(stay);
        getBookingInfo().verify(stay);
        getRevenueDetails().verify(stay);
        getSource().verify(helper.getSourceFactory().getNoEmployeeIdSource(), NOT_AVAILABLE);
    }
}
