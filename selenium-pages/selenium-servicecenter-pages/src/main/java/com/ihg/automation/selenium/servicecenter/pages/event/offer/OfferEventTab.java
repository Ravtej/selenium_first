package com.ihg.automation.selenium.servicecenter.pages.event.offer;

import com.ihg.automation.selenium.common.pages.PopUpBase;
import com.ihg.automation.selenium.common.pages.TabCustomContainerBase;

public class OfferEventTab extends TabCustomContainerBase
{
    public OfferEventTab(PopUpBase parent)
    {
        super(parent, "Events");
    }
}
