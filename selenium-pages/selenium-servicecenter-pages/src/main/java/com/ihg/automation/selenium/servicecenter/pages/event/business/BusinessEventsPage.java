package com.ihg.automation.selenium.servicecenter.pages.event.business;

import com.ihg.automation.selenium.common.business.BusinessRewardsEventDetailsPopUp;
import com.ihg.automation.selenium.common.event.SearchAware;
import com.ihg.automation.selenium.common.event.SearchButtonBar;
import com.ihg.automation.selenium.common.pages.TabCustomContainerBase;
import com.ihg.automation.selenium.gwt.components.Button;
import com.ihg.automation.selenium.listener.Verifier;
import com.ihg.automation.selenium.servicecenter.pages.Tabs;

public class BusinessEventsPage extends TabCustomContainerBase implements SearchAware
{
    public BusinessEventsPage()
    {
        super(new Tabs().getEvents().getBusiness());
    }

    @Override
    public BusinessSearch getSearchFields()
    {
        return new BusinessSearch(container);
    }

    @Override
    public BusinessEventsButtonBar getButtonBar()
    {
        return new BusinessEventsButtonBar(this);
    }

    public BusinessEventsGrid getGrid()
    {
        return new BusinessEventsGrid(container);
    }

    public class BusinessEventsButtonBar extends SearchButtonBar
    {
        public BusinessEventsButtonBar(BusinessEventsPage page)
        {
            super(page.container);
        }

        public Button getCreateEvent()
        {
            return container.getButton("Create Event");
        }

        public void clickCreateEvent(Verifier... verifiers)
        {
            getCreateEvent().clickAndWait(verifiers);
        }
    }

    public BusinessRewardsEventDetailsPopUp createEvent()
    {
        getButtonBar().clickCreateEvent();
        return new BusinessRewardsEventDetailsPopUp();
    }

}
