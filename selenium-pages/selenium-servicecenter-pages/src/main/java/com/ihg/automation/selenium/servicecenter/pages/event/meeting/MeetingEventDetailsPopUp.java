package com.ihg.automation.selenium.servicecenter.pages.event.meeting;

import com.ihg.automation.selenium.common.billing.EventBillingDetailsTab;
import com.ihg.automation.selenium.common.billing.EventBillingTabAware;
import com.ihg.automation.selenium.common.earning.EventEarningDetailsTab;
import com.ihg.automation.selenium.common.earning.EventEarningTabAware;
import com.ihg.automation.selenium.common.pages.ClosablePopUpBase;

public class MeetingEventDetailsPopUp extends ClosablePopUpBase implements EventEarningTabAware, EventBillingTabAware
{

    public MeetingEventDetailsPopUp()
    {
        super("Meeting Event Details");
    }

    public MeetingEventDetailsTab getMeetingDetailsTab()
    {
        return new MeetingEventDetailsTab(this);
    }

    @Override
    public EventBillingDetailsTab getBillingDetailsTab()
    {
        return new EventBillingDetailsTab(this);
    }

    @Override
    public EventEarningDetailsTab getEarningDetailsTab()
    {
        return new EventEarningDetailsTab(this);
    }

}
