package com.ihg.automation.selenium.servicecenter.pages.history;

import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.grid.TreeGrid;

public class ProfileHistoryGrid extends TreeGrid<ProfileHistoryGridRow, ProfileHistoryGridRow.ProfileHistoryCell>
{
    public ProfileHistoryGrid(Container parent)
    {
        super(parent, "Profile History Grid");
    }
}
