package com.ihg.automation.selenium.servicecenter.pages.enroll;

import com.ihg.automation.selenium.common.member.MemberAlliance;
import com.ihg.automation.selenium.common.pages.Populate;
import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.input.Input;
import com.ihg.automation.selenium.gwt.components.input.Select;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.AllianceDetailsBase;

public class AllianceDetails extends AllianceDetailsBase implements Populate<MemberAlliance>
{
    public AllianceDetails(Container container)
    {
        super(container);
    }

    @Override
    public Select getAlliance()
    {
        return container.getSelectByLabel("Carrier");
    }

    @Override
    public Input getAllianceNumber()
    {
        return container.getInputByLabel("ID Number");
    }

    @Override
    public void populate(MemberAlliance alliance)
    {
        getAlliance().selectByCode(alliance.getAlliance());
        super.populate(alliance);
    }
}
