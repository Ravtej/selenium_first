package com.ihg.automation.selenium.servicecenter.pages.programs;


import com.ihg.automation.selenium.common.types.program.Program;

public class KarmaPage  extends SimpleProgramPageBase
{
    public KarmaPage()
    {
        super(Program.KAR);
    }
}
