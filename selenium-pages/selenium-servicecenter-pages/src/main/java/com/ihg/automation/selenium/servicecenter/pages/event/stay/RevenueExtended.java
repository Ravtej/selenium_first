package com.ihg.automation.selenium.servicecenter.pages.event.stay;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.formatter.Converter.booleanToString;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasTextAsFormattedDouble;
import static com.ihg.automation.selenium.matchers.component.ViewableMatcher.hasText;
import static com.ihg.automation.selenium.matchers.text.AsFormattedDouble.asFormattedDouble;

import com.ihg.automation.selenium.common.Revenue;
import com.ihg.automation.selenium.common.components.IndicatorSelect;
import com.ihg.automation.selenium.common.pages.Populate;
import com.ihg.automation.selenium.common.pages.VerifyMultiMode;
import com.ihg.automation.selenium.gwt.components.Component;
import com.ihg.automation.selenium.gwt.components.ComponentColumnPosition;
import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.Mode;
import com.ihg.automation.selenium.gwt.components.input.Input;
import com.ihg.automation.selenium.gwt.components.input.Select;

public class RevenueExtended extends Revenue implements Populate<com.ihg.automation.selenium.common.stay.Revenue>,
        VerifyMultiMode<com.ihg.automation.selenium.common.stay.Revenue>
{
    public RevenueExtended(Container container, String labelText)
    {
        super(container, labelText);
    }

    @Override
    public Input getLocalAmount()
    {
        return new Input(super.getLocalAmount(), LOCAL_AMOUNT);
    }

    public IndicatorSelect getQualifying()
    {
        Component parent = label.getComponent("Qualifying?", 3, getPosition());
        return new IndicatorSelect(parent);
    }

    public IndicatorSelect getBilHotel()
    {
        Component parent = label.getComponent("Bill Hotel", 4, getPosition());
        return new IndicatorSelect(parent);
    }

    public Select getFlagChangeReason()
    {
        Component parent = label.getComponent("Flag Change Reason", 5, getPosition());
        return new IndicatorSelect(parent);
    }

    @Override
    protected ComponentColumnPosition getPosition()
    {
        return ComponentColumnPosition.TABLE;
    }

    @Override
    public void populate(com.ihg.automation.selenium.common.stay.Revenue revenue)
    {
        getLocalAmount().typeAndWait(revenue.getAmount());
        if (revenue.isAllowOverride())
        {
            getQualifying().select(booleanToString(revenue.isQualifying()));
            getBilHotel().select(booleanToString(revenue.isBillHotel()));
            getFlagChangeReason().select(revenue.getReasonCode());
        }
    }

    @Override
    public void verify(com.ihg.automation.selenium.common.stay.Revenue revenue, Mode mode)
    {
        verifyThat(getLocalAmount(), hasText(asFormattedDouble(revenue.getAmount()), mode));
        verifyThat(getUSDAmount(), hasTextAsFormattedDouble(revenue.getUsdAmount()));

        Mode overridingMode = mode;
        if (mode == Mode.EDIT && !revenue.isAllowOverride())
        {
            overridingMode = Mode.VIEW;
        }
        verifyThat(getQualifying(), hasText(booleanToString(revenue.isQualifying()), overridingMode));
        verifyThat(getBilHotel(), hasText(booleanToString(revenue.isBillHotel()), overridingMode));
        verifyThat(getFlagChangeReason(), hasText(revenue.getReasonCode(), overridingMode));
    }
}
