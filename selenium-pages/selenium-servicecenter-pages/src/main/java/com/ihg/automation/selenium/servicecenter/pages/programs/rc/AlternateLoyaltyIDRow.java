package com.ihg.automation.selenium.servicecenter.pages.programs.rc;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasTextWithWait;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;

import com.ihg.automation.selenium.common.types.program.AlternateLoyaltyIDType;
import com.ihg.automation.selenium.gwt.components.grid.CellsName;
import com.ihg.automation.selenium.gwt.components.grid.GridRow;
import com.ihg.automation.selenium.gwt.components.grid.GridRowXpath;
import com.ihg.automation.selenium.listener.Verifier;

public class AlternateLoyaltyIDRow extends GridRow<AlternateLoyaltyIDRow.AlternateCell>
{
    public AlternateLoyaltyIDRow(AlternateLoyaltyIDGrid parent, GridRowXpath xpath)
    {
        super(parent, xpath);
    }

    public EditAlternateLoyaltyIDPopUp openEditAlternateIdPopUp()
    {
        getCell(AlternateCell.TYPE).clickByLabel();

        return new EditAlternateLoyaltyIDPopUp();
    }

    public void clickDelete(Verifier... verifiers)
    {
        getCell(AlternateCell.DELETE).clickByLabel(verifiers);
    }

    public void verify(String type, String number)
    {
        verifyThat(getCell(AlternateCell.TYPE).asLink(), displayed(true));
        verifyThat(getCell(AlternateCell.TYPE), hasTextWithWait(type));
        verifyThat(getCell(AlternateCell.NUMBER), hasText(number));
    }

    public void verifyHertz(String number)
    {
        verify(AlternateLoyaltyIDType.HERTZ, number);
    }

    public enum AlternateCell implements CellsName
    {
        TYPE("memberType"), NUMBER("memberNumber"), DELETE("delete");

        private String name;

        private AlternateCell(String name)
        {
            this.name = name;
        }

        @Override
        public String getName()
        {
            return name;
        }

    }

}
