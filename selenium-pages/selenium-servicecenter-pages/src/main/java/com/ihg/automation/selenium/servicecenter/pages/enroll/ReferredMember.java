package com.ihg.automation.selenium.servicecenter.pages.enroll;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasTextWithWait;

import com.ihg.automation.selenium.common.pages.LabelContainerBase;
import com.ihg.automation.selenium.gwt.components.Component;
import com.ihg.automation.selenium.gwt.components.ComponentColumnPosition;
import com.ihg.automation.selenium.gwt.components.input.Input;
import com.ihg.automation.selenium.gwt.components.label.Label;

public class ReferredMember extends LabelContainerBase
{
    private static final ComponentColumnPosition POSITION = ComponentColumnPosition.FOLLOWING_TD;

    public ReferredMember(Label label)
    {
        super(label);
    }

    public Input getRCReferredBy()
    {
        return label.getInput("Member ID", 1, POSITION);
    }

    public Component getName()
    {
        return label.getComponent("Member Name", 2, POSITION);
    }

    public void verify(String fullName)
    {
        verifyThat(getName(), hasTextWithWait(fullName));
    }

}
