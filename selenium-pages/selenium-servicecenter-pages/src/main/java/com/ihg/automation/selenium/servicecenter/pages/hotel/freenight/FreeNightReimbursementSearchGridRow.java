package com.ihg.automation.selenium.servicecenter.pages.hotel.freenight;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasTextWithWait;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasTextWithWaitAndFlush;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.isDisplayedWithWait;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;

import com.ihg.automation.selenium.common.hotel.HotelNight;
import com.ihg.automation.selenium.gwt.components.grid.CellsName;
import com.ihg.automation.selenium.gwt.components.grid.GridRow;
import com.ihg.automation.selenium.gwt.components.grid.GridRowXpath;

public class FreeNightReimbursementSearchGridRow
        extends GridRow<FreeNightReimbursementSearchGridRow.FreeNightReimbursementCell>
{
    public FreeNightReimbursementSearchGridRow(FreeNightReimbursementSearchGrid parent, GridRowXpath xpath)
    {
        super(parent, xpath);
    }

    public void verify(HotelNight hotelNight)
    {
        verifyThat(getCell(FreeNightReimbursementCell.STAY_DATE), hasText(hotelNight.getStayDate()));
        verifyThat(getCell(FreeNightReimbursementCell.FREE_NIGHTS_COUNT), hasText(hotelNight.getNightsCount()));
        verifyThat(getCell(FreeNightReimbursementCell.OCC), hasText(hotelNight.getOccupancyPercent()));
        verifyThat(getCell(FreeNightReimbursementCell.ADR), hasText(hotelNight.getADRAmount()));
        verifyThat(getCell(FreeNightReimbursementCell.OVERRIDE_OCC), hasText(hotelNight.getOverrideOccupancyPercent()));
        verifyThat(getCell(FreeNightReimbursementCell.OVERRIDE_ADR), hasText(hotelNight.getOverrideADRAmount()));
        verifyThat(getCell(FreeNightReimbursementCell.STATUS), hasTextWithWait(hotelNight.getStatus()));
        verifyThat(getCell(FreeNightReimbursementCell.TOTAL_REIMBURSEMENT),
                hasText(hotelNight.getReimbursementAmount()));
        verifyThat(getCell(FreeNightReimbursementCell.HOTEL_CURRENCY), hasText(hotelNight.getCurrencyCode()));
        verifyThat(getCell(FreeNightReimbursementCell.DAYS_LEFT), hasText(hotelNight.getLeftToSubmit()));
    }

    public void verifyAdjusted(String status, String overrideOccupancyPercent, String overrideADRAmount)
    {
        verifyThat(getCell(FreeNightReimbursementCell.STATUS), hasTextWithWaitAndFlush(status));
        verifyThat(getCell(FreeNightReimbursementCell.OVERRIDE_OCC), hasTextWithWait(overrideOccupancyPercent));
        verifyThat(getCell(FreeNightReimbursementCell.OVERRIDE_ADR), hasTextWithWait(overrideADRAmount));
    }

    public FreeNightReimbursementSearchGridRowAdjustPanel openAdjustPanel()
    {
        getCell(FreeNightReimbursementCell.ADJUST).asLink().click();
        FreeNightReimbursementSearchGridRowAdjustPanel panel = new FreeNightReimbursementSearchGridRowAdjustPanel(this);
        verifyThat(panel, isDisplayedWithWait());
        return panel;
    }

    public enum FreeNightReimbursementCell implements CellsName
    {
        STAY_DATE("date", "Stay Date"), FREE_NIGHTS_COUNT("nightsCount", "Reward/Free Nights Count"), //
        OCC("occ", "OCC %"), ADR("adr", "ADR"), OVERRIDE_OCC("actualOcc", "Override OCC %"), //
        OVERRIDE_ADR("actualAdr", "Override ADR"), STATUS("status", "Status"), //
        TOTAL_REIMBURSEMENT("totalReimbursement", "Total Reimbursement"), //
        HOTEL_CURRENCY("hotelCurrency", "Hotel Currency"), //
        ADJUST("adjustAction", "Adjust"), //
        DAYS_LEFT("leftToSubmit", "Days Left to Submit");

        private String name;
        private String value;

        private FreeNightReimbursementCell(String name, String value)
        {
            this.name = name;
            this.value = value;
        }

        @Override
        public String getName()
        {
            return name;
        }

        public String getValue()
        {
            return value;
        }

    }
}
