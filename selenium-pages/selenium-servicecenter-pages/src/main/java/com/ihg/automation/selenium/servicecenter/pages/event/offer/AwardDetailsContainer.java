package com.ihg.automation.selenium.servicecenter.pages.event.offer;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;

import com.ihg.automation.selenium.common.award.AwardDetails;
import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.CustomContainerBase;
import com.ihg.automation.selenium.gwt.components.label.Label;

public class AwardDetailsContainer extends CustomContainerBase
{
    public AwardDetailsContainer(Container container)
    {
        super(container.getSeparator("Award Details"));
    }

    public Label getNameOfItem()
    {
        return container.getLabel("Name of Item");
    }

    public Label getDescriptions()
    {
        return container.getLabel("Description");
    }

    public Label getDeliveryMethod()
    {
        return container.getLabel("Delivery Method");
    }

    public Label getVendor()
    {
        return container.getLabel("Vendor");
    }

    public Label getStartDateOfItem()
    {
        return container.getLabel("Start Date of Item");
    }

    public Label getItemID()
    {
        return container.getLabel("Item ID");
    }

    public Label getEstimatedDeliveryTime()
    {
        return container.getLabel("Estimated Delivery Time");
    }

    public Label getStatus()
    {
        return container.getLabel("Status");
    }

    public Label getEndDateOfItem()
    {
        return container.getLabel("End Date of Item");
    }

    public void verify(AwardDetails award)
    {
        if (award == null)
        {
            return;
        }
        verifyThat(getNameOfItem(), hasText(award.getNameOfItem()));
        verifyThat(getDescriptions(), hasText(award.getDescription()));
        verifyThat(getDeliveryMethod(), hasText(award.getDeliveryMethod()));
        verifyThat(getVendor(), hasText(award.getVendor()));
        verifyThat(getStartDateOfItem(), hasText(award.getStartDateOfItem()));
        verifyThat(getItemID(), hasText(award.getItemID()));
        verifyThat(getEstimatedDeliveryTime(), hasText(award.getEstimatedDeliveryTime()));
        verifyThat(getStatus(), hasText(award.getStatus()));
        verifyThat(getEndDateOfItem(), hasText(award.getEndDateOfItem()));
    }
}
