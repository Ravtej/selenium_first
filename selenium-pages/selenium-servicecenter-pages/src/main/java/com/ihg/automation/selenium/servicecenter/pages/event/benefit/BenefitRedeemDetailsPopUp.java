package com.ihg.automation.selenium.servicecenter.pages.event.benefit;

import com.ihg.automation.selenium.common.billing.EventBillingDetailsTab;
import com.ihg.automation.selenium.common.billing.EventBillingTabAware;
import com.ihg.automation.selenium.common.earning.EventEarningDetailsTab;
import com.ihg.automation.selenium.common.earning.EventEarningTabAware;
import com.ihg.automation.selenium.common.pages.ClosablePopUpBase;

public class BenefitRedeemDetailsPopUp extends ClosablePopUpBase implements EventEarningTabAware, EventBillingTabAware
{
    public BenefitRedeemDetailsPopUp()
    {
        super("Benefit Redeem Details");
    }

    public BenefitRedeemDetailsTab getBenefitRedeemDetailsTab()
    {
        return new BenefitRedeemDetailsTab(this);
    }

    @Override
    public EventEarningDetailsTab getEarningDetailsTab()
    {
        return new EventEarningDetailsTab(this);
    }

    @Override
    public EventBillingDetailsTab getBillingDetailsTab()
    {
        return new EventBillingDetailsTab(this);
    }

}
