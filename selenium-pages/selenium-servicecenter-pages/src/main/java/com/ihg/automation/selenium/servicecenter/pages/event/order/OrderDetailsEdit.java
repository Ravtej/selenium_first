package com.ihg.automation.selenium.servicecenter.pages.event.order;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;

import com.ihg.automation.selenium.common.order.OrderDetailsEditBase;
import com.ihg.automation.selenium.common.order.OrderItem;
import com.ihg.automation.selenium.common.order.VoucherOrder;
import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.Link;
import com.ihg.automation.selenium.gwt.components.label.Label;

public class OrderDetailsEdit extends OrderDetailsEditBase
{
    public OrderDetailsEdit(Container container)
    {
        super(container);
    }

    public OrderDetailsEdit(Container container, String itemId)
    {
        super(container, itemId);
    }

    public Link getItemId()
    {
        return new Link(container.getLabel("Item ID"), Link.LINK_XPATH_NO_HYPER_LINK_IN_CLASS);
    }

    public Label getColor()
    {
        return container.getLabel("Color");
    }

    public Label getSize()
    {
        return container.getLabel("Size");
    }

    public void verify(OrderItem orderItem)
    {
        super.verify(orderItem);
        verifyThat(getItemId(), hasText(orderItem.getItemID()));
        verifyThat(getColor(), hasText(orderItem.getColor()));
        verifyThat(getSize(), hasText(orderItem.getSize()));
    }

    @Override
    public void verify(VoucherOrder voucherOrder)
    {
        super.verify(voucherOrder);
        verifyThat(getItemId(), hasText(voucherOrder.getPromotionId()));
    }
}
