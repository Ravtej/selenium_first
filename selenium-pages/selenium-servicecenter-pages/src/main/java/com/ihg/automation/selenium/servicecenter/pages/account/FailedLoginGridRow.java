package com.ihg.automation.selenium.servicecenter.pages.account;

import com.ihg.automation.selenium.gwt.components.grid.CellsName;
import com.ihg.automation.selenium.gwt.components.grid.GridRow;
import com.ihg.automation.selenium.gwt.components.grid.GridRowXpath;

public class FailedLoginGridRow extends GridRow<FailedLoginGridRow.FailedLoginCell>
{
    public FailedLoginGridRow(FailedLoginGrid parent, GridRowXpath xpath)
    {
        super(parent, xpath);
    }

    public enum FailedLoginCell implements CellsName
    {
        DATE_OF_LAST_ATTEMPT("lastInvalidLoginTime"), NUMBER_OF_FAILED_ATTEMPTS("invalidLoginAttempts");

        private String name;

        private FailedLoginCell(String name)
        {
            this.name = name;
        }

        @Override
        public String getName()
        {
            return name;
        }
    }

}
