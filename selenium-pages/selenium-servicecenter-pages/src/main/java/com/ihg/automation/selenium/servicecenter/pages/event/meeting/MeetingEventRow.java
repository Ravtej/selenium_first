package com.ihg.automation.selenium.servicecenter.pages.event.meeting;

import com.ihg.automation.common.DateUtils;
import com.ihg.automation.selenium.common.MeetingDetails;

public class MeetingEventRow
{
    private String transactionDate = DateUtils.getFormattedDate();
    private String transactionType;
    private String hotel;
    private String checkInDate;
    private String checkOutDate;
    private String meetingName;
    private String ihgUnits;
    private String allianceUnits;

    public MeetingEventRow()
    {
        this("", "", "", "", "", "", "");
    }

    public MeetingEventRow(String transactionType, String hotel, String checkInDate, String checkOutDate,
            String meetingName, String ihgUnits, String allianceUnits)
    {
        this.transactionType = transactionType;
        this.hotel = hotel;
        this.checkInDate = checkInDate;
        this.checkOutDate = checkOutDate;
        this.meetingName = meetingName;
        this.ihgUnits = ihgUnits;
        this.allianceUnits = allianceUnits;
    }

    public MeetingEventRow(MeetingDetails meetingDetails)
    {
        this("Meeting", meetingDetails.getHotel(), meetingDetails.getCheckInDate(), meetingDetails.getCheckOutDate(),
                meetingDetails.getMeetingEventName(), meetingDetails.getTotalLoyaltyUnitsToAwardAmount(), "");
    }

    public void setTransDate(String transactionDate)
    {
        this.transactionDate = transactionDate;
    }

    public String getTransDate()
    {
        return transactionDate;
    }

    public void setTransType(String transactionType)
    {
        this.transactionType = transactionType;
    }

    public String getTransType()
    {
        return transactionType;
    }

    public void setHotel(String hotel)
    {
        this.hotel = hotel;
    }

    public String getHotel()
    {
        return hotel;
    }

    public void setCheckInDate(String checkInDate)
    {
        this.checkInDate = checkInDate;
    }

    public String getCheckInDate()
    {
        return checkInDate;
    }

    public void setCheckOutDate(String checkOutDate)
    {
        this.checkOutDate = checkOutDate;
    }

    public String getCheckOutDate()
    {
        return checkOutDate;
    }

    public void setMeetingName(String meetingName)
    {
        this.meetingName = meetingName;
    }

    public String getMeetingName()
    {
        return meetingName;
    }

    public void setIhgUnits(String ihgUnits)
    {
        this.ihgUnits = ihgUnits;
    }

    public String getIhgUnits()
    {
        return ihgUnits;
    }

    public void setAllianceUnits(String allianceUnits)
    {
        this.allianceUnits = allianceUnits;
    }

    public String getAllianceUnits()
    {
        return allianceUnits;
    }

}
