package com.ihg.automation.selenium.servicecenter.pages.event.point;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.Constant.NOT_AVAILABLE;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasText;

import com.ihg.automation.selenium.common.EventSourceAware;
import com.ihg.automation.selenium.common.SiteHelperBase;
import com.ihg.automation.selenium.common.earning.EventEarningAmount;
import com.ihg.automation.selenium.common.event.AllEventsRow;
import com.ihg.automation.selenium.common.event.EventSourceExtended;
import com.ihg.automation.selenium.common.event.Source;
import com.ihg.automation.selenium.common.pages.TabCustomContainerBase;
import com.ihg.automation.selenium.gwt.components.label.Label;

public class PointExpirationDetailsTab extends TabCustomContainerBase implements EventSourceAware
{

    public PointExpirationDetailsTab(PointExpirationDetailsPopUpBase parent)
    {
        super(parent, "Details");
    }

    public Label getTransactionDate()
    {
        return container.getLabel("Transaction Date");
    }

    public Label getTransactionType()
    {
        return container.getLabel("Transaction Type");
    }

    public EventEarningAmount getTotalLoyaltyUnits()
    {
        return new EventEarningAmount(container.getLabel("Total Loyalty Units"));
    }

    @Override
    public EventSourceExtended getSource()
    {
        return new EventSourceExtended(container);
    }

    public void verify(AllEventsRow row, Source source)
    {
        verifyThat(getTransactionDate(), hasText(row.getTransDate()));
        verifyThat(getTransactionType(), hasText(row.getTransType()));
        getTotalLoyaltyUnits().verify(row.getIhgUnits());
        getSource().verify(source, NOT_AVAILABLE);
    }

    public void verify(AllEventsRow row, SiteHelperBase siteHelper)
    {
        verify(row, siteHelper.getSourceFactory().getNoEmployeeIdSource());
    }
}
