package com.ihg.automation.selenium.servicecenter.pages.event.stay;

import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.grid.Grid;
import com.ihg.automation.selenium.gwt.components.grid.GridRowXpath;
import com.ihg.automation.selenium.servicecenter.pages.event.stay.StayGuestGridRow.StayCell;

public class StayGuestGrid extends Grid<StayGuestGridRow, StayCell>
{
    public StayGuestGrid(Container parent)
    {
        super(parent);
    }

    public StayGuestGridRow getRow(String transType, String hotel)
    {
        return getRow(new GridRowXpath.GridRowXpathBuilder().cell(StayCell.TRANS_TYPE, transType)
                .cell(StayCell.HOTEL, hotel).build());
    }

}
