package com.ihg.automation.selenium.servicecenter.pages.event.goodwill;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static com.ihg.automation.selenium.matchers.text.EntityMatcherFactory.isValue;

import com.ihg.automation.selenium.common.Constant;
import com.ihg.automation.selenium.common.EventSourceAware;
import com.ihg.automation.selenium.common.SiteHelperBase;
import com.ihg.automation.selenium.common.event.EventSourceExtended;
import com.ihg.automation.selenium.common.types.GoodwillReason;
import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.CustomContainerBase;
import com.ihg.automation.selenium.gwt.components.label.Label;

public class GoodwillDetails extends CustomContainerBase implements EventSourceAware
{
    public GoodwillDetails(Container container)
    {
        super(container);
    }

    public Label getReasonCode()
    {
        return container.getLabel("Reason Code");
    }

    @Override
    public EventSourceExtended getSource()
    {
        return new EventSourceExtended(container);
    }

    public void verify(GoodwillReason reason, SiteHelperBase siteHelper)
    {
        verifyThat(getReasonCode(), hasText(isValue(reason)));
        getSource().verify(siteHelper.getSourceFactory().getNoEmployeeIdSource(), Constant.NOT_AVAILABLE);
    }
}
