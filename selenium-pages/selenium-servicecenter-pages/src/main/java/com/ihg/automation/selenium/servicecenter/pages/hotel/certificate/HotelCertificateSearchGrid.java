package com.ihg.automation.selenium.servicecenter.pages.hotel.certificate;

import com.ihg.automation.selenium.gwt.components.Component;
import com.ihg.automation.selenium.gwt.components.grid.Grid;
import com.ihg.automation.selenium.gwt.components.grid.GridRowXpath;
import com.ihg.automation.selenium.servicecenter.pages.hotel.certificate.HotelCertificateSearchGridRow.HotelCertificateSearchGridRowCell;

public class HotelCertificateSearchGrid extends Grid<HotelCertificateSearchGridRow, HotelCertificateSearchGridRowCell>
{

    public HotelCertificateSearchGrid(Component parent)
    {
        super(parent);
    }

    public HotelCertificateSearchGridRow getRow(String certificateNumber)
    {
        return getRow(new GridRowXpath.GridRowXpathBuilder()
                .cell(HotelCertificateSearchGridRowCell.CERTIFICATE_NUMBER, certificateNumber).build());
    }

    public HotelCertificateSearchGridRow getRowByStatus(String status)
    {
        return getRow(
                new GridRowXpath.GridRowXpathBuilder().cell(HotelCertificateSearchGridRowCell.STATUS, status).build());
    }

}
