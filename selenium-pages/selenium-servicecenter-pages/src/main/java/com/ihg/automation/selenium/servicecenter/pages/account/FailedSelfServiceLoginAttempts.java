package com.ihg.automation.selenium.servicecenter.pages.account;

import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.CustomContainerBase;
import com.ihg.automation.selenium.gwt.components.panel.FieldSet;

public class FailedSelfServiceLoginAttempts extends CustomContainerBase
{

    public FailedSelfServiceLoginAttempts(Container container)
    {
        super(container.getFieldSet("Failed Self Service Login Attempts"));
    }

    public FailedLoginGrid getGrid()
    {
        return new FailedLoginGrid(container);
    }

    public void expand()
    {
        ((FieldSet) container).expand();
    }

}
