package com.ihg.automation.selenium.servicecenter.pages.event.freenight;

import com.ihg.automation.selenium.common.pages.PopUpBase;
import com.ihg.automation.selenium.common.pages.TabCustomContainerBase;
import com.ihg.automation.selenium.servicecenter.pages.event.offer.OfferDetailsViewBase;

public class OfferDetailsFreeNightTab extends TabCustomContainerBase
{
    public OfferDetailsFreeNightTab(PopUpBase parent)
    {
        super(parent, "Offer Details");
    }

    public OfferDetailsViewBase getOfferDetails()
    {
        return new OfferDetailsViewBase(container);
    }

}
