package com.ihg.automation.selenium.servicecenter.pages.event.enroll;

import com.ihg.automation.selenium.common.billing.EventBillingDetailsTab;
import com.ihg.automation.selenium.common.billing.EventBillingTabAware;
import com.ihg.automation.selenium.common.earning.EventEarningDetailsTab;
import com.ihg.automation.selenium.common.earning.EventEarningTabAware;
import com.ihg.automation.selenium.common.pages.ClosablePopUpBase;

public class MembershipDetailsPopUp extends ClosablePopUpBase implements EventEarningTabAware, EventBillingTabAware
{
    public MembershipDetailsPopUp()
    {
        super("Membership Details");
    }

    public MembershipDetailsTab getMembershipDetailsTab()
    {
        return new MembershipDetailsTab(this);
    }

    @Override
    public EventEarningDetailsTab getEarningDetailsTab()
    {
        return new EventEarningDetailsTab(this);
    }

    @Override
    public EventBillingDetailsTab getBillingDetailsTab()
    {
        return new EventBillingDetailsTab(this);
    }
}
