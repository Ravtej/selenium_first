package com.ihg.automation.selenium.servicecenter.pages.event.stay;

public class StayHistoryKey
{
    private static final String BOOLEAN_MARKER = "Indicator";

    public static final String STAY_FIRST_NAME = "First Name";
    public static final String STAY_LAST_NAME = "Last Name";
    public static final String STAY_ENTERPRISE_ID = "Guest (by EID)";
    public static final String STAY_GUEST_ID = "Guest (by master key)";
    public static final String STAY_CHECK_IN = "Check In";
    public static final String STAY_CHECK_OUT = "Check Out";
    public static final String STAY_HOTEL_CODE = "Hotel";
    public static final String STAY_CONFIRMATION_NUMBER = "Confirmation Number";
    public static final String STAY_FOLIO_NUMBER = "Folio Number";
    public static final String STAY_QUALIFYING_NIGHTS = "Qualifying Nights";
    public static final String STAY_OVERLAPPING = "Overlapping " + BOOLEAN_MARKER;
    public static final String STAY_ENROLLING = "Enrolling Stay " + BOOLEAN_MARKER;
    public static final String STAY_ROOM_NUMBER = "Room Number";
    public static final String STAY_ROOM_TYPE = "Room Type";
    public static final String STAY_CURRENCY = "Currency";
    public static final String STAY_CORPORATE_ACCOUNT_NUMBER = "Corporate Account Number";
    public static final String STAY_AVERAGE_ROOM_RATE = "Average Room Rate";
    public static final String STAY_RATE_CODE = "Rate Code";
    public static final String STAY_IATA_CODE = "IATA Number";
    public static final String STAY_CREDIT_CARD_NUMBER = "Credit Card Number";
    public static final String STAY_NIGHTS = "Nights";
    public static final String STAY_PAYMENT_TYPE_CODE = "Payment Type Code";
    public static final String STAY_TOTAL_REVENUE_AMOUNT = "Total Revenue Amount";
    public static final String STAY_TRANSACTION_DATE = "Transaction Date";
    public static final String STAY_CREDIT_CARD_TYPE = "Credit Card Type";
    public static final String STAY_DATA_SOURCE = "Data Source";
    public static final String STAY_EVENT_TYPE = "Event Type";
    public static final String STAY_EARNING_PREFERENCE = "Earning Preference";
    public static final String STAY_BOOKING_SOURCE = "Booking Source";

    public static final String REVENUE_TOTAL_ROOM = "Total Room";
    public static final String REVENUE_FOOD = "Food";
    public static final String REVENUE_BEVERAGE = "Beverage";
    public static final String REVENUE_PHONE = "Phone";
    public static final String REVENUE_MEETING = "Meeting";
    public static final String REVENUE_MANDATORY_POINTS = "Miscellaneous MANDATORY Loyalty Revenue";
    public static final String REVENUE_OPTIONAL_POINTS = "Miscellaneous OTHER Loyalty Revenue";
    public static final String REVENUE_NO_POINTS_AWARDED = "Miscellaneous NO Loyalty Revenue";
    public static final String REVENUE_TAX = "Room Tax/Fee/Non-Revenue";

    public static final String REVENUE_AMOUNT = "Amount";
    public static final String REVENUE_ADJUSTMENT_REASON = "Adjustment Reason";
    public static final String REVENUE_QUALIFYING = "Qualifying Revenue " + BOOLEAN_MARKER;
    public static final String REVENUE_BILL_HOTEL = "Bill Hotel " + BOOLEAN_MARKER;

    public static final String LOYALTY_PCR_MEMBERSHIP_ID = "Membership ID";
}
