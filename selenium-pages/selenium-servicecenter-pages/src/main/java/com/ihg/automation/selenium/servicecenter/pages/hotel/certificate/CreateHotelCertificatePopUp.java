package com.ihg.automation.selenium.servicecenter.pages.hotel.certificate;

import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyNoError;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;

import com.ihg.automation.selenium.common.hotel.HotelCertificate;

public class CreateHotelCertificatePopUp extends HotelCertificatePopUpBase
{
    public CreateHotelCertificatePopUp()
    {
        super("Create Hotel Certificate");
    }

    @Override
    public HotelCertificateDetailsCreate getDetails()
    {
        return new HotelCertificateDetailsCreate(container);
    }

    public void populate(HotelCertificate details)
    {
        getDetails().populate(details);
    }

    public void createCertificate(HotelCertificate certificate)
    {
        populate(certificate);
        getSaveChanges().clickAndWait(verifyNoError());
        verifyThat(this, displayed(false));
    }

    public void createApprovedCertificate(HotelCertificate certificate)
    {
        populate(certificate);
        getSaveApprove().clickAndWait(verifyNoError());
        verifyThat(this, displayed(false));
    }

}
