package com.ihg.automation.selenium.servicecenter.pages.programs;

import com.ihg.automation.selenium.common.types.program.Program;

public class HotelPage extends SimpleProgramPageBase
{
    public HotelPage()
    {
        super(Program.HTL);
    }
}
