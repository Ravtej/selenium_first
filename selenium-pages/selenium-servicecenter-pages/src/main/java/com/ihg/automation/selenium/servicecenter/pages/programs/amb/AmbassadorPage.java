package com.ihg.automation.selenium.servicecenter.pages.programs.amb;

import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.servicecenter.pages.programs.ProgramPageBase;
import com.ihg.automation.selenium.servicecenter.pages.programs.RenewExtendProgramSummary;

public class AmbassadorPage extends ProgramPageBase
{
    public AmbassadorPage()
    {
        super(Program.AMB);
    }

    @Override
    public RenewExtendProgramSummary getSummary()
    {
        return new RenewExtendProgramSummary(container);
    }

    public AmbassadorTierLevelActivityGrid getAnnualActivities()
    {
        return new AmbassadorTierLevelActivityGrid(container);
    }
}
