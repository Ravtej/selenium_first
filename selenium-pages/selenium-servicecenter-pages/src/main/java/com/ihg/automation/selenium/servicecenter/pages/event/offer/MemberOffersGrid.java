package com.ihg.automation.selenium.servicecenter.pages.event.offer;

import com.ihg.automation.selenium.common.offer.Offer;
import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.grid.Grid;
import com.ihg.automation.selenium.gwt.components.grid.GridRowXpath;
import com.ihg.automation.selenium.servicecenter.pages.event.offer.MemberOfferGridRow.MemberOfferCell;

public class MemberOffersGrid extends Grid<MemberOfferGridRow, MemberOfferCell>
{
    public MemberOffersGrid(Container parent)
    {
        super(parent);
    }

    public MemberOfferGridRow getRowByOfferCode(String offerCode)
    {
        return getRow(new GridRowXpath.GridRowXpathBuilder().cell(MemberOfferCell.OFFER_CODE, offerCode).build());
    }

    public MemberOfferGridRow getRowByOfferCode(Offer offer)
    {
        return getRowByOfferCode(offer.getCode());
    }

    public OfferPopUp getOfferPopUp(String offerCode)
    {
        MemberOfferGridRow row = getRowByOfferCode(offerCode);
        return row.getOfferDetails();
    }
}
