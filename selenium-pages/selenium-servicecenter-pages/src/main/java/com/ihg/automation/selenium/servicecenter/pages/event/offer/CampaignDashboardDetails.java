package com.ihg.automation.selenium.servicecenter.pages.event.offer;

import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.CustomContainerBase;

public class CampaignDashboardDetails extends CustomContainerBase
{
    public CampaignDashboardDetails(Container parent)
    {
        super(parent.getSeparator("Campaign Dashboard Details"));
    }

    public CampaignDetailsContainer getCampaignDetails()
    {
        return new CampaignDetailsContainer(container);
    }

    public CampaignSummaryContainer getCampaignSummary()
    {
        return new CampaignSummaryContainer(container);
    }
}
