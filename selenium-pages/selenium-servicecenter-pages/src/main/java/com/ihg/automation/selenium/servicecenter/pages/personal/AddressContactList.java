package com.ihg.automation.selenium.servicecenter.pages.personal;

import com.ihg.automation.common.RandomUtils;
import com.ihg.automation.selenium.common.member.GuestAddress;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.personal.ContactListBase;
import com.ihg.automation.selenium.gwt.components.Button;
import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.panel.EditablePanel;

public class AddressContactList extends ContactListBase<GuestAddress, AddressContact>
{
    public AddressContactList(Container container)
    {
        super(container.getSeparator("Address"));
    }

    @Override
    public AddressContact getContact(int index)
    {
        return new AddressContact(new EditablePanel(container, index));
    }

    @Override
    public Button getAddButton()
    {
        return container.getButton("Add address");
    }

    @Override
    protected GuestAddress getRandomContact()
    {
        GuestAddress address = MemberPopulateHelper.getUnitedStatesAddress();
        address.setPreferred(false);
        address.setDoNotChangeLock(true);
        address.setLine2(RandomUtils.getTimeStamp());

        return address;
    }
}
