package com.ihg.automation.selenium.servicecenter.pages.preferences;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasDefault;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasTextWithWait;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static com.ihg.automation.selenium.matchers.text.EntityMatcherFactory.isValue;

import com.ihg.automation.selenium.gwt.components.grid.CellsName;
import com.ihg.automation.selenium.gwt.components.grid.Grid;
import com.ihg.automation.selenium.gwt.components.grid.GridRow;
import com.ihg.automation.selenium.gwt.components.grid.GridRowXpath;
import com.ihg.automation.selenium.listener.Verifier;

public class TravelProfileGridRow extends GridRow<TravelProfileGridRow.TravelProfileCell>
{

    public TravelProfileGridRow(Grid parent, GridRowXpath xpath)
    {
        super(parent, xpath);
    }

    public void verify(TravelProfile travelProfile)
    {
        String deleteCell = "";

        verifyThat(getCell(TravelProfileCell.PROFILE_NAME), hasText(travelProfile.getProfileName()));
        verifyThat(getCell(TravelProfileCell.PROFILE_TYPE), hasText(isValue(travelProfile.getProfileType())));
        if (travelProfile.getCreditCard() != null)
        {
            verifyThat(getCell(TravelProfileCell.PROFILE_CREDIT_CARD),
                    hasText(isValue(travelProfile.getCreditCard().getType())));
        }
        else
        {
            verifyThat(getCell(TravelProfileCell.PROFILE_CREDIT_CARD), hasDefault());
        }

        if (travelProfile.getCorporateId() != null)
        {
            verifyThat(getCell(TravelProfileCell.PROFILE_CORPORATE_ID),
                    hasTextWithWait(travelProfile.getCorporateId().getCorporateName()));
        }
        if (travelProfile.isPossibleToDelete())
        {
            deleteCell = "Delete";
        }
        verifyThat(getCell(TravelProfileCell.DELETE), hasText(deleteCell));

    }

    public enum TravelProfileCell implements CellsName
    {
        PROFILE_NAME("name"), PROFILE_TYPE("type"), PROFILE_CREDIT_CARD(
                "creditCard.creditCardTypeCode"), PROFILE_CORPORATE_ID("corporateAccount.documentId"), DELETE(
                        "possibleToDelete");

        private String name;

        TravelProfileCell(String name)
        {
            this.name = name;
        }

        @Override
        public String getName()
        {
            return name;
        }

    }

    public EditTravelProfilePopUp openTravelProfile()
    {
        getCell(TravelProfileCell.PROFILE_NAME).asLink().clickAndWait();

        return new EditTravelProfilePopUp();
    }

    public void clickDelete(Verifier verifiers)
    {
        getCell(TravelProfileCell.DELETE).asLink().clickAndWait(verifiers);
    }
}
