package com.ihg.automation.selenium.servicecenter.pages.programs.rc;

public class AddAlternateLoyaltyIDPopUp extends AlternateLoyaltyIDPopUpBase
{
    public AddAlternateLoyaltyIDPopUp()
    {
        super("Add Alternate Loyalty ID");
    }
}
