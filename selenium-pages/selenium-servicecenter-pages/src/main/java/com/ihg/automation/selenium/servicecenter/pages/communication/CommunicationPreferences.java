package com.ihg.automation.selenium.servicecenter.pages.communication;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.ViewableMatcher.hasTextInView;
import static com.ihg.automation.selenium.matchers.text.EntityMatcherFactory.isValue;

import com.ihg.automation.selenium.common.communication.MarketingPreferences;
import com.ihg.automation.selenium.common.communication.SmsMobileTextingMarketingPreferences;
import com.ihg.automation.selenium.common.pages.MultiModeBase;
import com.ihg.automation.selenium.common.types.WrittenLanguage;
import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.checkbox.CheckBox;
import com.ihg.automation.selenium.gwt.components.input.Input;
import com.ihg.automation.selenium.gwt.components.input.Select;
import com.ihg.automation.selenium.gwt.components.label.Label;
import com.ihg.automation.selenium.gwt.components.label.SecurityLabel;

public class CommunicationPreferences extends MultiModeBase
{
    public CommunicationPreferences(Container container)
    {
        super(container);
    }

    public Select getPreferredLanguage()
    {
        return new SecurityLabel(container, "Preferred Language").getSelect();
    }

    public Select getThirdPartyShare()
    {
        return new SecurityLabel(container, "Third Party Share").getSelect();
    }

    public Label getDoNotContactLabel()
    {
        return new SecurityLabel(container, "Do Not Contact");
    }

    public CheckBox getDoNotContact()
    {
        return getDoNotContactLabel().getCheckBox();
    }

    public Label getDoNotCallLabel()
    {
        return new SecurityLabel(container, "Do Not Call");
    }

    public CheckBox getDoNotCall()
    {
        return getDoNotCallLabel().getCheckBox();
    }

    public MarketingPreferences getMarketingPreferences()
    {
        return new MarketingPreferences(container);
    }

    public SmsMobileTextingMarketingPreferences getSmsMobileTextingMarketingPreferences()
    {
        return new SmsMobileTextingMarketingPreferences(container);
    }

    public CustomerService getCustomerService()
    {
        return new CustomerService(container);
    }

    public Input getComments()
    {
        return container.getInputByLabel("Customer Service Comments");
    }

    public void verify(WrittenLanguage written)
    {
        verifyThat(getPreferredLanguage(), hasTextInView(isValue(written)));
    }
}
