package com.ihg.automation.selenium.servicecenter.pages.event.freenight;

import com.ihg.automation.selenium.common.offer.Offer;
import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.grid.Grid;
import com.ihg.automation.selenium.gwt.components.grid.GridRowXpath;
import com.ihg.automation.selenium.servicecenter.pages.event.freenight.FreeNightGridRow.FreeNightCell;

public class FreeNightGrid extends Grid<FreeNightGridRow, FreeNightCell>
{

    public FreeNightGrid(Container parent)
    {
        super(parent);
    }

    public FreeNightGridRow getRowByOfferCode(String offerCode)
    {
        return getRow(new GridRowXpath.GridRowXpathBuilder().cell(FreeNightCell.OFFER_CODE, offerCode).build());
    }

    public FreeNightGridRow getRowByOfferCode(Offer offer)
    {
        return getRowByOfferCode(offer.getCode());
    }

    public FreeNightDetailsPopUp getFreeNightDetailsPopUp(String offerCode)
    {
        FreeNightGridRow row = getRowByOfferCode(offerCode);
        return row.getFreeNightDetails();
    }
}
