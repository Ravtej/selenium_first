package com.ihg.automation.selenium.servicecenter.pages.preferences;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasDefault;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasText;

import com.ihg.automation.selenium.common.member.CreditCard;
import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.CustomContainerBase;
import com.ihg.automation.selenium.gwt.components.Link;
import com.ihg.automation.selenium.gwt.components.input.Input;
import com.ihg.automation.selenium.gwt.components.input.Select;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.ExpirationDate;

public class CreditCardFields extends CustomContainerBase
{

    public CreditCardFields(Container container)
    {
        super(container);
    }

    public Select getCreditCardType()
    {
        return container.getSelectByLabel("Credit Card Type");
    }

    public Input getCreditCardNumber()
    {
        return container.getInputByLabel("Credit Card Number");
    }

    public Link getClearCreditCard()
    {
        return container.getLink("Clear credit card");
    }

    public void clickClear()
    {
        getClearCreditCard().click();
    }

    public ExpirationDate getExpirationDate()
    {
        return new ExpirationDate(container.getLabel("Expiration Date"));
    }

    public void populate(CreditCard creditCard)
    {
        getCreditCardType().select(creditCard.getType().getCodeWithValue());
        getCreditCardNumber().type(creditCard.getNumber());
        getExpirationDate().populate(creditCard.getExpirationDate());
    }

    public void verify(CreditCard creditCard)
    {
        verifyThat(getCreditCardNumber(), hasText(creditCard.getMaskedNumber()));
        verifyThat(getCreditCardType(), hasText(creditCard.getType().getValue()));
        getExpirationDate().verify(creditCard.getExpirationDate());
        verifyThat(getClearCreditCard(), displayed(true));
    }

    public void verifyEmptyFields()
    {
        verifyThat(getCreditCardNumber(), hasDefault());
        verifyThat(getCreditCardType(), hasDefault());
        verifyThat(getExpirationDate().getMonth(), hasDefault());
        verifyThat(getExpirationDate().getYear(), hasDefault());
    }

}
