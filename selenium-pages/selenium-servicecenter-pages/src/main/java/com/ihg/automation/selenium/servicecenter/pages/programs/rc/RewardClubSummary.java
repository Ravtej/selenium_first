package com.ihg.automation.selenium.servicecenter.pages.programs.rc;

import static com.ihg.automation.common.DateUtils.DATE_FORMAT;
import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyNoError;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasDefault;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasText;
import static com.ihg.automation.selenium.matchers.component.IsDisplayed.displayed;

import org.joda.time.LocalDate;

import com.ihg.automation.selenium.gwt.components.Button;
import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.Link;
import com.ihg.automation.selenium.gwt.components.label.Label;
import com.ihg.automation.selenium.servicecenter.pages.programs.ProgramSummary;

public class RewardClubSummary extends ProgramSummary
{
    public static final String NO_TIER_LEVEL_BENEFITS_MESSAGE = "No Tier-Level Benefits available";
    public static final int EXPIRATION_MONTH_SHIFT = 12;

    public RewardClubSummary(Container container)
    {
        super(container);
    }

    public Label getBalance()
    {
        return container.getLabel("Current Loyalty Unit Balance");
    }

    public Label getLastActivityDate()
    {
        return container.getLabel("Last Activity Date");
    }

    public Label getExpiredLoyaltyUnitBalance()
    {
        return container.getLabel("Expired Loyalty Unit Balance");
    }

    public Button getRestore()
    {
        return container.getButton("Restore");
    }

    public Label getBalanceExpirationDate()
    {
        return container.getLabel("Balance Expiration Date");
    }

    public Button getExtend()
    {
        return container.getButton("Extend");
    }

    public Label getLastAdjusted()
    {
        return container.getLabel("Last Adjusted");
    }

    public Button getPurchase()
    {
        return container.getButton("Purchase");
    }

    public Link getTierLevelBenefits()
    {
        return new Link(container, "Tier-Level Benefits", Link.LINK_PATTERN_WITH_TEXT_NO_HYPER);
    }

    public Link getPartnerBenefits()
    {
        return new Link(container, "Partner Benefits", Link.LINK_PATTERN_WITH_TEXT_NO_HYPER);
    }

    public TierLevelBenefitsDetailsPopUp openTierLevelBenefitsDetailsPopUp()
    {
        getTierLevelBenefits().clickAndWait(verifyNoError());
        TierLevelBenefitsDetailsPopUp tierLevelBenefitsDetailsPopUp = new TierLevelBenefitsDetailsPopUp();
        verifyThat(tierLevelBenefitsDetailsPopUp, displayed(true));

        return tierLevelBenefitsDetailsPopUp;
    }

    public void clickPurchase()
    {
        getPurchase().clickAndWait();
    }

}
