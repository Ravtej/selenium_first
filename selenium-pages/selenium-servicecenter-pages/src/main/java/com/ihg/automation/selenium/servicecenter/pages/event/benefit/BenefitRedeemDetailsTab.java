package com.ihg.automation.selenium.servicecenter.pages.event.benefit;

import com.ihg.automation.selenium.common.pages.PopUpBase;
import com.ihg.automation.selenium.common.pages.TabCustomContainerBase;

public class BenefitRedeemDetailsTab extends TabCustomContainerBase
{
    public BenefitRedeemDetailsTab(PopUpBase popUp)
    {
        super(popUp, "Benefit Redeem Details");
    }

    public BenefitRedeemDetails getBenefitRedeemDetails()
    {
        return new BenefitRedeemDetails(container);
    }
}
