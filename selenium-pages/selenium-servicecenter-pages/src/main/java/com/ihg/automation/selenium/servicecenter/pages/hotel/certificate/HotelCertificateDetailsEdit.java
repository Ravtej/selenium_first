package com.ihg.automation.selenium.servicecenter.pages.hotel.certificate;

import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.label.Label;

public class HotelCertificateDetailsEdit extends HotelCertificateDetailsEditBase
{

    public HotelCertificateDetailsEdit(Container container)
    {
        super(container);
    }

    @Override
    public Label getCertificateNumber()
    {
        return container.getLabel(CERTIFICATE_NUMBER);
    }

    @Override
    public Label getBillingEntity()
    {
        return container.getLabel(BILLING_ENTITY);
    }

}
