package com.ihg.automation.selenium.servicecenter.pages.event.stay;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;

import com.ihg.automation.selenium.common.pages.Verify;
import com.ihg.automation.selenium.common.stay.BookingDetails;
import com.ihg.automation.selenium.common.stay.Stay;
import com.ihg.automation.selenium.gwt.components.Component;
import com.ihg.automation.selenium.gwt.components.CustomContainerBase;
import com.ihg.automation.selenium.gwt.components.label.Label;

public abstract class StayBookingInfoBase extends CustomContainerBase implements Verify<Stay>
{
    protected final static String BOOKING_DATE_LABEL = "Booking Date";
    protected final static String BOOKING_SOURCE_LABEL = "Booking Source";
    protected final static String DATA_SOURCE_LABEL = "Data Source";

    public StayBookingInfoBase(CustomContainerBase parent)
    {
        super(parent.container.getSeparator("Booking Details"));
    }

    public Label getBookingDate()
    {
        return container.getLabel(BOOKING_DATE_LABEL);
    }

    public Label getDataSource()
    {
        return container.getLabel(DATA_SOURCE_LABEL);
    }

    public abstract Component getBookingSource();

    @Override
    public void verify(Stay stay)
    {
        BookingDetails details = stay.getBookingDetails();

        verifyThat(getBookingDate(), hasText(details.getBookingDate()));
        verifyThat(getBookingSource(), hasText(details.getBookingSource()));
        verifyThat(getDataSource(), hasText(details.getDataSource()));
    }
}
