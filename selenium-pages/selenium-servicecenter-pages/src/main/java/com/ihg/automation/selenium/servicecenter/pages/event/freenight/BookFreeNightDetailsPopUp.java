package com.ihg.automation.selenium.servicecenter.pages.event.freenight;

import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.assertNoError;
import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifySuccess;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.enabled;

import com.ihg.automation.selenium.common.freenight.Redemption;
import com.ihg.automation.selenium.common.pages.ClosablePopUpBase;
import com.ihg.automation.selenium.gwt.components.Button;
import com.ihg.automation.selenium.gwt.components.input.DateInput;
import com.ihg.automation.selenium.gwt.components.input.Input;
import com.ihg.automation.selenium.gwt.components.input.SearchInput;
import com.ihg.automation.selenium.listener.Verifier;

public class BookFreeNightDetailsPopUp extends ClosablePopUpBase
{
    public BookFreeNightDetailsPopUp()
    {
        super("Free Night Details");
    }

    public Button getBookFreeNightButton()
    {
        return container.getButton("Book Free Night");
    }

    public void clickBookFreeNightButton(Verifier... verifiers)
    {
        getBookFreeNightButton().clickAndWait(verifiers);
    }

    public Input getConfirmationNumber()
    {
        return container.getInputByLabel("Confirmation Number");
    }

    public SearchInput getHotel()
    {
        return new SearchInput(container.getLabel("Hotel"));
    }

    public Input getConfirmationDate()
    {
        return container.getInputByLabel("Confirmation Date");
    }

    public Input getNumberOfRooms()
    {
        return container.getInputByLabel("Number of Rooms");
    }

    public DateInput getCheckInDate()
    {
        return new DateInput(container.getLabel("Check In"));
    }

    public DateInput getCheckOutDate()
    {
        return new DateInput(container.getLabel("Check Out"));
    }

    public Input getNights()
    {
        return container.getInputByLabel("Nights");
    }

    public void populate(Redemption redemption)
    {
        getConfirmationNumber().type(redemption.getConfirmationNumber());
        getHotel().type(redemption.getHotel());
        getConfirmationDate().type(redemption.getConfirmationDate());
        getNumberOfRooms().type(redemption.getNumberOfRooms());
        getCheckInDate().type(redemption.getCheckIn());
        getCheckOutDate().type(redemption.getCheckOut());
        getNights().type(redemption.getNights());
    }

    public void verifyEnabledFields()
    {
        verifyThat(getCheckInDate(), enabled(true));
        verifyThat(getCheckOutDate(), enabled(true));
        verifyThat(getConfirmationNumber(), enabled(true));
        verifyThat(getHotel(), enabled(true));
        verifyThat(getNumberOfRooms(), enabled(true));
        verifyThat(getConfirmationDate(), enabled(true));
        verifyThat(getNights(), enabled(false));
    }

    public void create(Redemption redemption)
    {
        populate(redemption);
        clickBookFreeNightButton(assertNoError(), verifySuccess("Free Night successfully booked"));
        verifyThat(this, displayed(false));
    }
}
