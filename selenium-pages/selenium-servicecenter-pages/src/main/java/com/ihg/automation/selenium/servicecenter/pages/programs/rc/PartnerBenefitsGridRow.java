package com.ihg.automation.selenium.servicecenter.pages.programs.rc;

import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyNoError;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasTextAsInt;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasTextWithWait;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.isDisplayedWithWait;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static com.ihg.automation.selenium.matchers.text.EntityMatcherFactory.isValue;

import com.ihg.automation.selenium.common.pages.ClosablePopUpBase;
import com.ihg.automation.selenium.common.types.program.RewardClubLevel;
import com.ihg.automation.selenium.gwt.components.grid.CellsName;
import com.ihg.automation.selenium.gwt.components.grid.GridRow;
import com.ihg.automation.selenium.gwt.components.grid.GridRowXpath;

public class PartnerBenefitsGridRow extends GridRow<PartnerBenefitsGridRow.PartnerBenefitsCell>
{
    public PartnerBenefitsGridRow(PartnerBenefitsGrid parent, GridRowXpath xpath)
    {
        super(parent, xpath);
    }

    public enum PartnerBenefitsCell implements CellsName
    {
        TIER_LEVEL("tierLevel", "Tier-Level"), //
        BENEFIT_NAME("benefitName", "Name"), //
        REFRESH_DATE("refreshDate", "Refresh Date"), //
        REDEEMED("redeemed", "Redeemed"), //
        AVAILABLE("availableCount", "Available"), //
        YTD_REDEEMED("ytdRedeemed", "YTD Redeemed"), //
        BENEFIT_PERIOD("", "Benefit Period");

        private String name;
        private String value;

        private PartnerBenefitsCell(String name, String value)
        {
            this.name = name;
            this.value = value;
        }

        public String getValue()
        {
            return value;
        }

        @Override
        public String getName()
        {
            return name;
        }
    }

    public void verify(RewardClubLevel level, String benefitName, int redeemed, int ytdRedeemed)
    {
        verifyThat(getCell(PartnerBenefitsGridRow.PartnerBenefitsCell.TIER_LEVEL), hasText(isValue(level)));
        verifyThat(getCell(PartnerBenefitsGridRow.PartnerBenefitsCell.BENEFIT_NAME), hasTextWithWait(benefitName));
        verifyThat(getCell(PartnerBenefitsGridRow.PartnerBenefitsCell.REDEEMED), hasTextAsInt(redeemed));
        verifyThat(getCell(PartnerBenefitsGridRow.PartnerBenefitsCell.YTD_REDEEMED), hasTextAsInt(ytdRedeemed));
    }

    private <C extends ClosablePopUpBase> C openPopUp(PartnerBenefitsCell cell, C popUp)
    {
        getCell(cell).asLink().clickAndWait(verifyNoError());
        verifyThat(popUp, isDisplayedWithWait());

        return popUp;
    }

    public AmazonBenefitsPopUp openAmazonBenefitsPopUp()
    {
        return openPopUp(PartnerBenefitsCell.BENEFIT_NAME, new AmazonBenefitsPopUp());
    }

    public PartnerBenefitRedeemedPopUp openRedeemedBenefitPopUp()
    {
        return openPopUp(PartnerBenefitsCell.REDEEMED, new PartnerBenefitRedeemedPopUp());
    }

    public YearToDateRedeemedBenefitPopUp openYTDRedeemedBenefitPopUp()
    {
        return openPopUp(PartnerBenefitsCell.YTD_REDEEMED, new YearToDateRedeemedBenefitPopUp());
    }
}
