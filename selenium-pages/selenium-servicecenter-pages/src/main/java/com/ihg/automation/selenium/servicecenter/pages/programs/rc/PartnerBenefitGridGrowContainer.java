package com.ihg.automation.selenium.servicecenter.pages.programs.rc;

import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.CustomContainerBase;
import com.ihg.automation.selenium.gwt.components.label.Label;

public class PartnerBenefitGridGrowContainer extends CustomContainerBase
{
    public PartnerBenefitGridGrowContainer(Container container)
    {
        super(container);
    }

    public Label getItemDescription()
    {
        return container.getLabel("Item Description");
    }
}
