package com.ihg.automation.selenium.servicecenter.pages.deposit;

import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.assertNoError;
import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifySuccess;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.assertThat;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasTextWithWait;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;

import com.ihg.automation.selenium.common.Constant;
import com.ihg.automation.selenium.common.deposit.Deposit;
import com.ihg.automation.selenium.common.pages.ClosablePopUpBase;
import com.ihg.automation.selenium.common.pages.NavigatePage;
import com.ihg.automation.selenium.gwt.components.Button;
import com.ihg.automation.selenium.gwt.components.Component;
import com.ihg.automation.selenium.gwt.components.ComponentColumnPosition;
import com.ihg.automation.selenium.gwt.components.input.Input;
import com.ihg.automation.selenium.gwt.components.input.Select;
import com.ihg.automation.selenium.gwt.components.label.Label;
import com.ihg.automation.selenium.gwt.components.utils.FindStepUtils;
import com.ihg.automation.selenium.listener.Verifier;
import com.ihg.automation.selenium.servicecenter.pages.TopMenu;

public class CreateDepositPopUp extends ClosablePopUpBase implements NavigatePage, DepositDetailsFieldsAware
{
    public static final String LOYALTY_UNITS = "Loyalty Units";
    public static final String CUSTOMER_IS_NOT_ELIGIBLE = "Customer is not eligible for this Transaction.";
    public static final String NOTE_TRANSACTION_IS_NOT_CO_PARTNER = "NOTE: This transaction is not a Co-Partner";
    public static final int CO_PARTNER_CREATION_IHG_UNITS_LIMIT = 50000;

    private static final ComponentColumnPosition POSITION = ComponentColumnPosition.TABLE;
    public static final String SUCCESS_MESSAGE = "New Deposit Event was created";

    public CreateDepositPopUp()
    {
        super("Deposit Details");
    }

    @Override
    public void goTo()
    {
        if (!this.isDisplayed())
        {
            new TopMenu().getDeposit().clickAndWait();

            flush();
            assertThat(this, displayed(true));
        }
    }

    public Input getTransactionId()
    {
        return container.getInputByLabel(TRANSACTION_ID);
    }

    public Input getHotel()
    {
        Component parent = new Component(container.getLabel("Hotel"), "Input", "Hotel",
                FindStepUtils.byXpathWithWait(".//ancestor::td/following-sibling::td"));

        return new Input(parent, "Hotel");
    }

    public Input getCheckInDate()
    {
        return container.getInputByLabel(CHECKIN_DATE);
    }

    public Input getCheckOutDate()
    {
        return container.getInputByLabel(CHECKOUT_DATE);
    }

    public Input getPartnerTransactionDate()
    {
        return container.getInputByLabel(TRANSACTION_DATE);
    }

    public Input getOrderTrackingNumber()
    {
        return container.getInputByLabel(TRACKING_NUMBER);
    }

    public Input getPartnerComment()
    {
        return container.getInputByLabel(PARTNER_COMMENT);
    }

    public Input getCustomerServiceComment()
    {
        return container.getInputByLabel(CUSTOMER_SERVICE_COMMENT);
    }

    public Label getOfferRegistration()
    {
        return container.getLabel("Offer Registration");
    }

    public Label getTransactionType()
    {
        return container.getLabel(TRANSACTION_TYPE);
    }

    public Label getTransactionName()
    {
        return container.getLabel(TRANSACTION_NAME);
    }

    public Label getTransactionDescription()
    {
        return container.getLabel(TRANSACTION_DESCRIPTION);
    }

    public Button getCreateDeposit()
    {
        return container.getButton(Button.CREATE_DEPOSIT);
    }

    public void clickCreateDeposit(Verifier... verifiers)
    {
        getCreateDeposit().clickAndWait(verifiers);
    }

    public Select getLoyaltyUnitsSelect()
    {
        return container.getSelectByLabel(LOYALTY_UNITS, LOYALTY_UNITS, 2, POSITION);
    }

    public Select getReason()
    {
        return container.getSelectByLabel(REASON);
    }

    public Input getLoyaltyUnitsInput()
    {
        return container.getInputByLabel(LOYALTY_UNITS, LOYALTY_UNITS, 1, POSITION);
    }

    public Label getCatalogItem()
    {
        return container.getLabel("Catalog Item");
    }

    public Label getTierLevel()
    {
        return container.getLabel("Tier-Level");
    }

    public void setDepositData(Deposit deposit)
    {
        getTransactionId().typeAndWait(deposit.getTransactionId());
        getHotel().typeAndWait(deposit.getHotel());
        getPartnerTransactionDate().typeAndWait(deposit.getPartnerTransactionDate());
        getOrderTrackingNumber().typeAndWait(deposit.getOrderTrackingNumber());
        getPartnerComment().typeAndWait(deposit.getPartnerComment());
        getCheckInDate().typeAndWait(deposit.getCheckInDate());
        getCheckOutDate().typeAndWait(deposit.getCheckOutDate());
        getLoyaltyUnitsSelect().select(deposit.getLoyaltyUnitsType());
        if (getLoyaltyUnitsInput().isEnabled())
        {
            getLoyaltyUnitsInput().typeAndWait(deposit.getLoyaltyUnitsAmount());
        }

        if (getReason().isDisplayed())
        {
            getReason().select(deposit.getReason());
        }

        if (getCustomerServiceComment().isDisplayed())
        {
            getCustomerServiceComment().typeAndWait(deposit.getCustomerServiceComment());
        }
    }

    public void createDeposit(Deposit deposit, boolean isVerifyTransactionDetails)
    {
        goTo();
        createDepositWithoutGoTo(deposit, isVerifyTransactionDetails);
    }

    public void createDeposit(Deposit deposit)
    {
        createDeposit(deposit, true);
    }

    public void createDepositWithoutGoTo(Deposit deposit, boolean isVerifyTransactionDetails)
    {
        setDepositData(deposit);
        if (isVerifyTransactionDetails)
        {
            verifyTransactionInfo(deposit);
        }
        getCreateDeposit().clickAndWait(assertNoError(), verifySuccess(SUCCESS_MESSAGE));
    }

    public void createDepositWithoutGoTo(Deposit deposit)
    {
        createDepositWithoutGoTo(deposit, true);
    }

    public void verifyTransactionInfo(Deposit deposit)
    {
        verifyThat(getTransactionType(), hasTextWithWait(deposit.getTransactionType()));
        verifyThat(getTransactionName(), hasTextWithWait(deposit.getTransactionName()));
        verifyThat(getTransactionDescription(), hasTextWithWait(deposit.getTransactionDescription()));
    }

    public void verifyControls()
    {
        verifyThat(getCheckInDate(), displayed(true));
        verifyThat(getCheckOutDate(), displayed(true));
        verifyThat(getClose(), displayed(true));
        verifyThat(getCreateDeposit(), displayed(true));
        verifyThat(getHotel(), displayed(true));
        verifyThat(getLoyaltyUnitsInput(), displayed(true));
        verifyThat(getLoyaltyUnitsSelect(), displayed(true));
        verifyThat(getOrderTrackingNumber(), displayed(true));
        verifyThat(getPartnerComment(), displayed(true));
        verifyThat(getPartnerTransactionDate(), displayed(true));
        verifyThat(getTransactionId(), displayed(true));
        verifyThat(getTransactionDescription(), displayed(true));
        verifyThat(getTransactionDescription(), hasText(Constant.NOT_AVAILABLE));
        verifyThat(getTransactionName(), displayed(true));
        verifyThat(getTransactionName(), hasText(Constant.NOT_AVAILABLE));
        verifyThat(getTransactionType(), displayed(true));
        verifyThat(getTransactionType(), hasText(Constant.NOT_AVAILABLE));
    }
}
