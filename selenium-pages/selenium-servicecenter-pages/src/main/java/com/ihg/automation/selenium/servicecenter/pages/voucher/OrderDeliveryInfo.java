package com.ihg.automation.selenium.servicecenter.pages.voucher;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static org.hamcrest.core.StringContains.containsString;

import com.ihg.automation.selenium.common.Constant;
import com.ihg.automation.selenium.common.CustomerInfoPanel;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.CustomContainerBase;
import com.ihg.automation.selenium.gwt.components.Link;
import com.ihg.automation.selenium.gwt.components.label.Label;

public class OrderDeliveryInfo extends CustomContainerBase
{
    public OrderDeliveryInfo(Container container)
    {
        super(container);
    }

    public Label getEmail()
    {
        return container.getLabel("Delivery E-mail");
    }

    public Label getAddress()
    {
        return container.getLabel("Delivery Address");
    }

    public Label getChannelOfOrder()
    {
        return container.getLabel("Channel of Order");
    }

    public Link getOrderedBy()
    {
        return new Link(container.getLabel("Ordered by"), Link.LINK_XPATH_NO_HYPER_LINK_IN_CLASS);
    }

    public void verifyAsOnCustomerInformation()
    {
        CustomerInfoPanel customerInfo = new CustomerInfoPanel();

        verifyThat(getEmail(), hasText(customerInfo.getEmail().getText()));
        verifyThat(getAddress(), hasText(Constant.NOT_AVAILABLE));
        verifyThat(getChannelOfOrder(), hasText(Constant.NOT_AVAILABLE));
        verifyThat(getOrderedBy(), hasText(customerInfo.getMemberNumber(Program.HTL).getText()));

        verifyThat(container, hasText(containsString(customerInfo.getFullName().getText())));
    }
}
