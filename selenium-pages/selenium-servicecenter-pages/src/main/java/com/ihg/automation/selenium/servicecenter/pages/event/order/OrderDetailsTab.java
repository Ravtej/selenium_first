package com.ihg.automation.selenium.servicecenter.pages.event.order;

import com.ihg.automation.selenium.common.order.Order;
import com.ihg.automation.selenium.common.order.OrderDetailsTabBase;
import com.ihg.automation.selenium.common.order.VoucherOrder;
import com.ihg.automation.selenium.common.pages.PopUpBase;
import com.ihg.automation.selenium.servicecenter.pages.voucher.VoucherOrderDetails;

public class OrderDetailsTab extends OrderDetailsTabBase
{
    public OrderDetailsTab(PopUpBase popUp)
    {
        super(popUp, "Edit");
    }

    public OrderDetailsEdit getOrderDetails()
    {
        return new OrderDetailsEdit(container);
    }

    public OrderDetailsEdit getOrderDetails(String itemId)
    {
        return new OrderDetailsEdit(container, itemId);
    }

    public VoucherOrderDetails getVoucherOrderDetails()
    {
        return new VoucherOrderDetails(container);
    }

    public void verify(Order order)
    {
        super.verify(order);
        getShippingInfo().verify(order);
    }

    public void verify(VoucherOrder voucherOrder)
    {
        super.verify(voucherOrder);

        getOrderDetails().verify(voucherOrder);
        getVoucherOrderDetails().verify(voucherOrder);
        getShippingInfo().verifyAsOnCustomerInformation(voucherOrder.getDeliveryOption());
    }
}
