package com.ihg.automation.selenium.servicecenter.pages.history;

import com.ihg.automation.selenium.common.pages.TabCustomContainerBase;
import com.ihg.automation.selenium.servicecenter.pages.Tabs;

public class ProfileHistoryPage extends TabCustomContainerBase
{
    public ProfileHistoryPage()
    {
        super(new Tabs().getCustomerInfo().getProfileHistory());
    }

    public ProfileHistoryGrid getProfileHistoryGrid()
    {
        return new ProfileHistoryGrid(container);
    }
}
