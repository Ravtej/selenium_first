package com.ihg.automation.selenium.servicecenter.pages.catalog;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.servicecenter.pages.catalog.CatalogItemsGridRow.CatalogItemsGridCell.LOYALTY_UNIT_COST;

import org.hamcrest.Matcher;

import com.ihg.automation.selenium.gwt.components.Componentable;
import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.grid.Grid;

public class CatalogItemsGrid extends Grid<CatalogItemsGridRow, CatalogItemsGridRow.CatalogItemsGridCell>
{
    public CatalogItemsGrid(Container parent)
    {
        super(parent);
    }

    public void verify(CatalogItemsGridRow.CatalogItemsGridCell cell, Matcher<Componentable> matcher)
    {
        for (int i = 1; i <= getRowCount(); i++)
        {
            verifyThat(getRow(i).getCell(cell), matcher);
        }
    }

    public void verifyUnitCost(Matcher<Componentable> matcher)
    {
        verify(LOYALTY_UNIT_COST, matcher);
    }
}
