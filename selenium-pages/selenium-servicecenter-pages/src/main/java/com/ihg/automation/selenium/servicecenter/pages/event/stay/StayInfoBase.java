package com.ihg.automation.selenium.servicecenter.pages.event.stay;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasTextAsInt;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;

import com.ihg.automation.selenium.common.pages.Verify;
import com.ihg.automation.selenium.common.stay.Stay;
import com.ihg.automation.selenium.gwt.components.Component;
import com.ihg.automation.selenium.gwt.components.CustomContainerBase;
import com.ihg.automation.selenium.gwt.components.label.Label;

public abstract class StayInfoBase extends CustomContainerBase implements Verify<Stay>
{
    protected static final String CHECK_IN_LABEL = "Check In";
    protected static final String CHECK_OUT_LABEL = "Check Out";
    protected static final String CONFIRMATION_NUMBER_LABEL = "Confirmation Number";
    protected static final String FOLIO_NUMBER_LABEL = "Folio Number";
    protected static final String ROOM_NUMBER_LABEL = "Room Number";
    protected static final String ROOM_TYPE_LABEL = "Room Type";
    protected static final String RATE_CODE_LABEL = "Rate Code";
    protected static final String CORP_ACCT_NUMBER_LABEL = "Corp Acct Number";
    protected static final String IATA_NUMBER_LABEL = "IATA Number";
    protected static final String OVERLAPPING_STAY_LABEL = "Overlapping Stay";
    protected static final String ENROLLING_STAY_LABEL = "Enrolling Stay";
    protected static final String NIGHTS_LABEL = "Nights";
    protected static final String Q_NIGHTS_LABEL = "Q-Nights";
    protected static final String HOTEL_CURRENCY_LABEL = "Hotel Currency";

    public StayInfoBase(CustomContainerBase parent)
    {
        super(parent.container.getSeparator("Stay Details"));
    }

    public Label getConfirmationNumber()
    {
        return container.getLabel(CONFIRMATION_NUMBER_LABEL);
    }

    public Label getFolioNumber()
    {
        return container.getLabel(FOLIO_NUMBER_LABEL);
    }

    public Label getOverlappingStay()
    {
        return container.getLabel(OVERLAPPING_STAY_LABEL);
    }

    public Label getQualifiedNights()
    {
        return container.getLabel(Q_NIGHTS_LABEL);
    }

    public abstract Component getCheckIn();

    public abstract Component getCheckOut();

    public abstract Component getRoomNumber();

    public abstract Component getRoomType();

    public abstract Component getRateCode();

    public abstract Component getCorpAcctNumber();

    public abstract Component getIATANumber();

    public abstract Component getEnrollingStay();

    public abstract Component getNights();

    public abstract Component getHotelCurrency();

    @Override
    public void verify(Stay stay)
    {
        verifyThat(getConfirmationNumber(), hasText(stay.getConfirmationNumber()));
        verifyThat(getFolioNumber(), hasText(stay.getFolioNumber()));
        verifyThat(getOverlappingStay(), hasText(stay.getOverlappingStay()));
        verifyThat(getQualifiedNights(), hasTextAsInt(stay.getQualifyingNights()));
        verifyThat(getCheckIn(), hasText(stay.getCheckIn()));
        verifyThat(getCheckOut(), hasText(stay.getCheckOut()));
        verifyThat(getRoomNumber(), hasText(stay.getRoomNumber()));
        verifyThat(getRoomType(), hasText(stay.getRoomType()));
        verifyThat(getRateCode(), hasText(stay.getRateCode()));
        verifyThat(getIATANumber(), hasText(stay.getIataCode()));
        verifyThat(getEnrollingStay(), hasText(stay.getEnrollingStay()));
        verifyThat(getNights(), hasTextAsInt(stay.getNights()));
        // Currency should be overridden
    }
}
