package com.ihg.automation.selenium.servicecenter.pages.event.stay;

interface StayDetailsAware
{
    public GuestInfo getGuestInfo();

    public StayInfoBase getStayInfo();

    public StayBookingInfoBase getBookingInfo();

    public RevenueDetailsBase getRevenueDetails();
}
