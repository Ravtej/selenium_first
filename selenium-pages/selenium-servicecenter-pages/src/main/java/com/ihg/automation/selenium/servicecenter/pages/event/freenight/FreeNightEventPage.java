package com.ihg.automation.selenium.servicecenter.pages.event.freenight;

import com.ihg.automation.selenium.common.pages.TabCustomContainerBase;
import com.ihg.automation.selenium.gwt.components.panel.Panel;
import com.ihg.automation.selenium.gwt.components.utils.ByText;
import com.ihg.automation.selenium.servicecenter.pages.Tabs;

public class FreeNightEventPage extends TabCustomContainerBase
{

    public FreeNightEventPage()
    {
        super(new Tabs().getEvents().getFreeNight());
    }

    public Panel getFreeNightPanel()
    {
        return container.getPanel(ByText.contains("Free Nights"));
    }

    public FreeNightGrid getFreeNightGrid()
    {
        return new FreeNightGrid(getFreeNightPanel());
    }

}
