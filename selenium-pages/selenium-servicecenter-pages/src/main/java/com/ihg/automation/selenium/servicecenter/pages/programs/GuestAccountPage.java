package com.ihg.automation.selenium.servicecenter.pages.programs;

import com.ihg.automation.selenium.common.types.program.Program;

public class GuestAccountPage extends SimpleProgramPageBase
{
    public GuestAccountPage()
    {
        super(Program.IHG);
    }
}
