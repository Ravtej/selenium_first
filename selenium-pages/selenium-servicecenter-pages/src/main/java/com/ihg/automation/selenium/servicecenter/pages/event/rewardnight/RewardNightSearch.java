package com.ihg.automation.selenium.servicecenter.pages.event.rewardnight;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.HasDefaultText.hasDefault;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;

import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.CustomContainerBase;
import com.ihg.automation.selenium.gwt.components.input.DateInput;
import com.ihg.automation.selenium.gwt.components.input.SearchInput;
import com.ihg.automation.selenium.gwt.components.input.Select;
import com.ihg.automation.selenium.gwt.components.label.Label;
import com.ihg.automation.selenium.gwt.components.utils.ByText;

public class RewardNightSearch extends CustomContainerBase
{
    public RewardNightSearch(Container container)
    {
        super(container.getSeparator("Reward Night Search"));
    }

    public Select getTransactionType()
    {
        return container.getSelectByLabel("Transaction Type");
    }

    public SearchInput getHotel()
    {
        return new SearchInput(container.getLabel("Hotel"));
    }

    public DateInput getFromTransactionDate()
    {
        return new DateInput(new Label(container, ByText.exact("From"), 1));
    }

    public DateInput getToTransactionDate()
    {
        return new DateInput(new Label(container, ByText.exact("To"), 1));
    }

    public DateInput getFromDateOfStay()
    {
        return new DateInput(new Label(container, ByText.exact("From"), 2));
    }

    public DateInput getToDateOfStay()
    {
        return new DateInput(new Label(container, ByText.exact("To"), 2));
    }

    public void verifyDefaults()
    {
        verifyThat(getTransactionType(), hasDefault());
        verifyThat(getHotel(), hasText("Hotel"));
        verifyThat(getFromTransactionDate(), hasDefault());
        verifyThat(getToTransactionDate(), hasDefault());
        verifyThat(getFromDateOfStay(), hasDefault());
        verifyThat(getToDateOfStay(), hasDefault());
    }
}
