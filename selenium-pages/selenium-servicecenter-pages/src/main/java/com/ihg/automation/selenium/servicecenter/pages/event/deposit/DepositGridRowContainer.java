package com.ihg.automation.selenium.servicecenter.pages.event.deposit;

import com.ihg.automation.selenium.common.Constant;
import com.ihg.automation.selenium.common.EventSourceAware;
import com.ihg.automation.selenium.common.SiteHelperBase;
import com.ihg.automation.selenium.common.deposit.Deposit;
import com.ihg.automation.selenium.common.event.EventGridRowContainerWithDetails;
import com.ihg.automation.selenium.common.event.EventSourceExtended;
import com.ihg.automation.selenium.gwt.components.Container;

public class DepositGridRowContainer extends EventGridRowContainerWithDetails
        implements EventSourceAware, DepositDetailsAware
{

    public DepositGridRowContainer(Container parent)
    {
        super(parent);
    }

    @Override
    public DepositDetails getDepositDetails()
    {
        return new DepositDetails(container);
    }

    @Override
    public EventSourceExtended getSource()
    {
        return new EventSourceExtended(container);
    }

    public void verify(Deposit deposit, SiteHelperBase siteHelper)
    {
        getDepositDetails().verify(deposit);
        getSource().verify(siteHelper.getSourceFactory().getNoEmployeeIdSource(), Constant.NOT_AVAILABLE);
    }
}
