package com.ihg.automation.selenium.servicecenter.pages.event.freenight;

import com.ihg.automation.selenium.common.billing.EventBillingDetailsTab;
import com.ihg.automation.selenium.common.billing.EventBillingTabAware;
import com.ihg.automation.selenium.common.pages.ClosablePopUpBase;
import com.ihg.automation.selenium.gwt.components.Button;
import com.ihg.automation.selenium.gwt.components.PopUpWindow;

public class CancelFreeNightPopUp extends ClosablePopUpBase implements EventBillingTabAware
{
    public CancelFreeNightPopUp()
    {
        super(PopUpWindow.withBodyText("Cancel Free Night", "Transaction Date"));
    }

    public FreeNightDetailsCancelTab getFreeNightDetailsCancelTab()
    {
        return new FreeNightDetailsCancelTab(this);
    }

    @Override
    public EventBillingDetailsTab getBillingDetailsTab()
    {
        return new EventBillingDetailsTab(this);
    }

    public Button getCancelFreeNightButton()
    {
        return container.getButton("Cancel Free Night");
    }

    public void clickCancelFreeNightButton()
    {
        getCancelFreeNightButton().clickAndWait();
    }

    public Button getSaveChangesButton()
    {
        return container.getButton("Save Changes");
    }

    public void clickSaveChangesButton()
    {
        getSaveChangesButton().clickAndWait();
    }
}
