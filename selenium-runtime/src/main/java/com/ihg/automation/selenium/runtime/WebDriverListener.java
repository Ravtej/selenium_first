package com.ihg.automation.selenium.runtime;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.AbstractWebDriverEventListener;

public class WebDriverListener extends AbstractWebDriverEventListener
{

    public WebDriverListener()
    {
    }

    @Override
    public void afterFindBy(By by, WebElement element, WebDriver driver)
    {
        new Highlighter(element).highlight(HighlightStyle.ON_INIT, 400);
    }

    @Override
    public void beforeClickOn(WebElement element, WebDriver driver)
    {
        new Highlighter(element).highlight(HighlightStyle.ON_CLICK, 400);
    }

    @Override
    public void afterChangeValueOf(WebElement element, WebDriver driver, CharSequence[] keysToSend)
    {
        new Highlighter(element).highlight(HighlightStyle.WARNING, 400);
    }

}
