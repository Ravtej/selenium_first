package com.ihg.automation.selenium.runtime;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;

import org.apache.commons.lang3.StringUtils;
import org.apache.http.client.fluent.Request;
import org.apache.http.client.utils.URIBuilder;
import org.json.JSONException;
import org.json.JSONObject;
import org.openqa.selenium.remote.HttpCommandExecutor;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RemoteWebDriverUtils
{
    private static final Logger logger = LoggerFactory.getLogger(RemoteWebDriverUtils.class);

    private static final String URI_SCHEMA = "http";
    private static final String URI_PATH = "/grid/api/testsession";
    private static final String URI_SESSION_PARAM = "session";
    public static final String PROXY_ID = "proxyId";
    public static final String HTTP_PREF = "http://";
    public static final int TIMEOUT = 3000;

    public static String getNodeURL(final RemoteWebDriver remoteDriver)
    {
        logger.debug("Starting node URL identification");

        URL remoteServer = ((HttpCommandExecutor) remoteDriver.getCommandExecutor()).getAddressOfRemoteServer();
        logger.debug("Hub URL is {}", remoteServer);

        URI uri = getSessionURI(remoteDriver, remoteServer);
        logger.debug("Session URI is {}", uri);

        String httpResponse = executeRequest(uri);
        logger.debug("HTTP response is {}", httpResponse);

        JSONObject response = getJSON(httpResponse);
        logger.debug("JSON response is {}", response);

        String nodeURL = getNodeURL(response);
        logger.debug("Node URL is {}", nodeURL);

        return StringUtils.remove(nodeURL, HTTP_PREF);
    }

    private static URI getSessionURI(RemoteWebDriver remoteDriver, URL remoteServer)
    {
        try
        {
            return new URIBuilder().setScheme(URI_SCHEMA).setHost(remoteServer.getHost())
                    .setPort(remoteServer.getPort()).setPath(URI_PATH)
                    .setParameter(URI_SESSION_PARAM, remoteDriver.getSessionId().toString()).build();
        }
        catch (URISyntaxException e)
        {
            logger.error("URISyntaxException", e);
        }
        return null;
    }

    private static String executeRequest(URI uri)
    {
        try
        {
            return Request.Get(uri).connectTimeout(TIMEOUT).socketTimeout(TIMEOUT).execute().returnContent().asString();
        }
        catch (IOException e)
        {
            logger.error("Unexpected IOException", e);
        }
        return null;
    }

    private static JSONObject getJSON(String response)
    {
        if (response != null)
        {
            try
            {
                return new JSONObject(response);
            }
            catch (JSONException e)
            {
                logger.error("Unexpected JSONException", e);
            }
        }

        return null;
    }

    private static String getNodeURL(JSONObject json)
    {
        if (json != null)
        {
            try
            {
                return json.getString(PROXY_ID);
            }
            catch (JSONException e)
            {
                logger.error("Unexpected JSONException", e);
            }
        }

        return null;
    }

    public static URL setHubUrl(String envUrl, URL hubUrl)
    {
        if (StringUtils.isNotEmpty(envUrl))
        {
            try
            {
                hubUrl = new URL(envUrl);
            }
            catch (IOException e)
            {
                logger.error("Catch IOException", e);
                throw new RuntimeException(e);
            }
        }
        else
        {
            hubUrl = null;
        }
        return hubUrl;
    }
}
