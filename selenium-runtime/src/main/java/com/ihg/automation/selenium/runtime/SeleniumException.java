package com.ihg.automation.selenium.runtime;

import java.nio.file.Path;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebElement;

public class SeleniumException extends RuntimeException implements ScreenException
{
    private static final long serialVersionUID = 7158972177160681744L;

    private String screenshotBase64;
    private Path screenshotPath;

    public SeleniumException(String description, String screenshotBase64)
    {
        super(description);
        this.screenshotBase64 = screenshotBase64;
    }

    public SeleniumException(String description, WebElement element)
    {
        this(description, new Cameron().getScreenshotAs(element, OutputType.BASE64));
    }

    public SeleniumException(Throwable cause, String screenshotBase64)
    {
        super(cause);
        this.screenshotBase64 = screenshotBase64;
    }

    @Override
    public String getScreenshotBase64()
    {
        return screenshotBase64;
    }

    @Override
    public void setScreenshotPath(Path screenshotPath)
    {
        this.screenshotPath = screenshotPath;
    }

    @Override
    public Path getScreenshotPath()
    {
        return screenshotPath;
    }
}
