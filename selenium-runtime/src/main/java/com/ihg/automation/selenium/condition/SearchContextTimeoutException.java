package com.ihg.automation.selenium.condition;

import com.ihg.automation.selenium.runtime.SeleniumException;

public class SearchContextTimeoutException extends SeleniumException
{
    private static final long serialVersionUID = 3477591155615570439L;

    public SearchContextTimeoutException(String description, String screenshot)
    {
        super(description, screenshot);
    }
}
