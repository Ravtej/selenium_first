package com.ihg.automation.selenium.runtime;

import java.nio.file.Path;

public interface ScreenException
{
    public String getScreenshotBase64();

    public void setScreenshotPath(Path screenshotPath);

    public Path getScreenshotPath();
}
