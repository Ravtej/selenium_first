package com.ihg.automation.selenium.runtime;

import java.net.URL;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RemoteWebDriverWrapper extends WebDriverWrapperBase
{
    private static final Logger logger = LoggerFactory.getLogger(RemoteWebDriverWrapper.class);

    private static final int MAX_ATTEMPT = 3;

    private URL hubUrl;
    private int attempt;

    public RemoteWebDriverWrapper(String envUrl)
    {
        super();
        hubUrl = RemoteWebDriverUtils.setHubUrl(envUrl, this.hubUrl);
        setDriver(setRemoteWebDriver(DesiredCapabilities.firefox()));
    }

    private WebDriver setRemoteWebDriver(DesiredCapabilities capability)
    {
        WebDriver driver = null;
        try
        {
            driver = new RemoteWebDriver(hubUrl, capability);
        }
        catch (WebDriverException e)
        {
            attempt++;
            logger.error("Catch WebDriverException", e);

            if (attempt < MAX_ATTEMPT)
            {
                setRemoteWebDriver(capability);
            }
            else
            {
                throw e;
            }
        }
        return driver;
    }

    @Override
    public void openUrl(String url)
    {
        super.openUrl(url);

        logger.debug("Browser is running on '{}' node", RemoteWebDriverUtils.getNodeURL((RemoteWebDriver) driver));
    }
}
