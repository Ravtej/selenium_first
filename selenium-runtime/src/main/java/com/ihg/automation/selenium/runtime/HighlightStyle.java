package com.ihg.automation.selenium.runtime;

public class HighlightStyle
{
    public static final String ERROR = "border: 3px solid red";
    public static final String WARNING = "border: 3px solid magenta";

    public static final String ON_INIT = "border: 3px solid green";
    public static final String ON_GET_TEXT = "border: 3px dotted green";
    public static final String ON_CLICK = "border: 3px dotted green; background: lime ";

    private HighlightStyle()
    {
    }
}
