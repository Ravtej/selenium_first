package com.ihg.automation.selenium.runtime;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.remote.DesiredCapabilities;

public class LocalWebDriverWrapper extends WebDriverWrapperBase
{
    public LocalWebDriverWrapper(WebDriver driver)
    {
        super(driver);
    }

    public LocalWebDriverWrapper()
    {
        this(new FirefoxDriver(
                new FirefoxOptions().setProfile(new FirefoxProfile()).addTo(DesiredCapabilities.firefox())));
    }
}
