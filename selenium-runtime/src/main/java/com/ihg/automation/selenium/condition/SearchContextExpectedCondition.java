package com.ihg.automation.selenium.condition;

import org.openqa.selenium.SearchContext;

import com.google.common.base.Function;

public interface SearchContextExpectedCondition<T> extends Function<SearchContext, T>
{
}
