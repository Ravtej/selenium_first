package com.ihg.automation.selenium.runtime;

import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Selenium
{
    private static final Logger logger = LoggerFactory.getLogger(Selenium.class);
    private static ThreadLocal<Selenium> instance = new ThreadLocal<Selenium>();
    private WebDriverWrapperBase driver;

    private Selenium()
    {
        String envUrl = System.getProperty("hubUrl");
        if (StringUtils.isNotEmpty(envUrl))
        {
            driver = new RemoteWebDriverWrapper(envUrl);
        }
        else
        {
            String highlight = System.getProperty("highlight");
            if (StringUtils.isNotEmpty(highlight))
            {
                WebDriver webDriver = new EventFiringWebDriver(new FirefoxDriver()).register(new WebDriverListener());
                driver = new LocalWebDriverWrapper(webDriver);
            }
            else
            {
                driver = new LocalWebDriverWrapper();
            }
        }
    }

    public WebDriver getDriver()
    {
        return driver.getDriver();
    }

    public WebDriverWrapperBase getDriverWrapper()
    {
        return driver;
    }

    private static boolean isInstance()
    {
        return (instance.get() != null);
    }

    public static boolean isInstanceAlive()
    {
        boolean isAlive = false;
        if (isInstance())
        {
            isAlive = getInstance().getDriverWrapper().isAlive();
        }
        else
        {
            logger.debug("WebDriver instance hasn't initialized yet");
        }

        return isAlive;
    }

    public static Selenium getInstance()
    {
        if (!isInstance())
        {
            instance.set(new Selenium());
        }
        return instance.get();
    }

    public void shutDown()
    {
        getInstance().getDriverWrapper().shutDown();
        logger.debug("Remove selenium instance");
        instance.remove();
    }
}
