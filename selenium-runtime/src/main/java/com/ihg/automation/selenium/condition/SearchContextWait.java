package com.ihg.automation.selenium.condition;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.NotFoundException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.SearchContext;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.FluentWait;

import com.ihg.automation.selenium.runtime.Cameron;
import com.ihg.automation.selenium.runtime.HighlightStyle;

public class SearchContextWait extends FluentWait<SearchContext>
{
    private SearchContext context;

    public SearchContextWait(SearchContext context, long timeOutInSeconds, long sleepTimeOut)
    {
        super(context);
        this.context = context;
        withTimeout(timeOutInSeconds, TimeUnit.SECONDS);
        pollingEvery(sleepTimeOut, TimeUnit.MILLISECONDS);
        ignoring(NotFoundException.class);
    }

    @Override
    protected RuntimeException timeoutException(String message, Throwable lastException)
    {
        if (context instanceof WebElement)
        {
            throw new SearchContextTimeoutException(message, new Cameron().getScreenshotAs((WebElement) context,
                    HighlightStyle.WARNING, OutputType.BASE64));
        }

        throw new SearchContextTimeoutException(message, new Cameron().getScreenshotAs(OutputType.BASE64));
    }
}
