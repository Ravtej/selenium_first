package com.ihg.automation.selenium.runtime;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebElement;

public class Cameron
{
    public <T> T getScreenshotAs(OutputType<T> target)
    {
        if (!Selenium.isInstanceAlive())
        {
            return null;
        }

        return ((TakesScreenshot) Selenium.getInstance().getDriver()).getScreenshotAs(target);
    }

    public <T> T getScreenshotAs(WebElement element, String style, OutputType<T> target)
    {
        if (!Selenium.isInstanceAlive())
        {
            return null;
        }

        if (element == null)
        {
            return getScreenshotAs(target);
        }
        Highlighter highlighter = new Highlighter(element);
        highlighter.on(style);
        T screenshot = getScreenshotAs(target);
        highlighter.off();

        return screenshot;
    }

    public <T> T getScreenshotAs(WebElement element, OutputType<T> target)
    {
        return getScreenshotAs(element, HighlightStyle.ERROR, target);
    }
}
