package com.ihg.automation.selenium.runtime;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Highlighter
{
    private static final Logger logger = LoggerFactory.getLogger(Highlighter.class);

    private static final String STYLE = "style";
    private static final String SET_STYLE_SCRIPT = "arguments[0].setAttribute('style', arguments[1]);";

    private WebElement element;
    private JavascriptExecutor js;
    private String originStyle;

    public Highlighter(WebElement element)
    {
        this.element = element;
    }

    private void changeStyle(String style)
    {
        js.executeScript(SET_STYLE_SCRIPT, element, style);
    }

    public void on(String style)
    {
        js = (JavascriptExecutor) Selenium.getInstance().getDriver();
        originStyle = element.getAttribute(STYLE);
        changeStyle(originStyle + style);
    }

    public void off()
    {
        if ((js != null) && (originStyle != null))
        {
            changeStyle(originStyle);
        }
        else
        {
            logger.warn("'on' method must be used first");
        }
    }

    public void highlight(final String style, final long duration)
    {
        if (!Selenium.isInstanceAlive() || (element == null))
        {
            return;
        }

        on(style);

        try
        {
            TimeUnit.MILLISECONDS.sleep(duration);
        }
        catch (InterruptedException e)
        {
            logger.debug("Unexpected InterruptedException", e);
        }

        off();
    }

}
