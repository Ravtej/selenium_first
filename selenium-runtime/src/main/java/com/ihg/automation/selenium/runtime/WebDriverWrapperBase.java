package com.ihg.automation.selenium.runtime;

import org.openqa.selenium.WebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class WebDriverWrapperBase
{
    private static final Logger logger = LoggerFactory.getLogger(WebDriverWrapperBase.class);

    protected WebDriver driver;

    public WebDriverWrapperBase()
    {
    }

    public WebDriverWrapperBase(WebDriver driver)
    {
        this.driver = driver;
    }

    public void setDriver(WebDriver driver)
    {
        this.driver = driver;
    }

    public WebDriver getDriver()
    {
        return driver;
    }

    public void shutDown()
    {
        try
        {
            driver.close();
            driver.quit();
        }
        catch (Exception e)
        {
            logger.error("Exception during WebDriver closing", e);
        }
    }

    public void openUrl(String url)
    {
        logger.debug("Open [{}] page", url);
        driver.get(url);
    }

    public boolean isAlive()
    {
        boolean isAlive = false;
        try
        {
            getDriver().getCurrentUrl();
            isAlive = true;
        }
        catch (Exception e)
        {
            logger.warn("WebDriver instance has already died");
        }

        return isAlive;
    }
}
