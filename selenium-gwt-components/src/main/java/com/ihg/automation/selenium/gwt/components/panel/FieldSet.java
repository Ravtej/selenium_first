package com.ihg.automation.selenium.gwt.components.panel;

import static com.ihg.automation.selenium.gwt.components.utils.FindStepUtils.byXpath;
import static com.ihg.automation.selenium.gwt.components.utils.FindStepUtils.byXpathWithWait;
import static com.ihg.automation.selenium.gwt.components.utils.XPathUtils.getFormattedXpath;

import com.ihg.automation.selenium.gwt.components.Component;
import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.utils.ByText;

public class FieldSet extends PanelBase
{
    private static final String TYPE = "Field Set";

    private static final String FIELDSET_PATTERN = ".//fieldset[child::legend/span[{0}]]";

    public FieldSet(Component parent, ByText text)
    {
        super(parent, TYPE, text.getText(), byXpathWithWait(getFormattedXpath(FIELDSET_PATTERN, text)));
    }

    public FieldSet(Component parent, String text)
    {
        this(parent, ByText.exact(text));
    }

    public FieldSet(String text)
    {
        this(null, text);
    }

    @Override
    public Expander getExpander()
    {
        return new Expander(this);
    }

    public class Expander extends PanelExpanderBase
    {
        private static final String EXPANDER_RELATIVE_XPATH = ".//legend/div[@role='checkbox']";

        protected Expander(FieldSet parent)
        {
            super(parent);
        }

        @Override
        protected Component getClickableComponent()
        {
            return new Component(this, getType(), getName(), byXpath(EXPANDER_RELATIVE_XPATH));
        }

        @Override
        public Container getExpandedArea()
        {
            return new Container(this, EXPANDED_AREA_TYPE, getName(), byXpath("./div[1]"));
        }
    }
}
