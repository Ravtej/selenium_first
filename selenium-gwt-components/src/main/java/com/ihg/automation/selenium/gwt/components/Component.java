package com.ihg.automation.selenium.gwt.components;

import static java.text.MessageFormat.format;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.SearchContext;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.Color;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ihg.automation.selenium.gwt.components.utils.ComponentSearchMode;
import com.ihg.automation.selenium.gwt.components.utils.FindStep;
import com.ihg.automation.selenium.gwt.components.utils.SearchContextUtils;
import com.ihg.automation.selenium.gwt.components.utils.WaitUtils;
import com.ihg.automation.selenium.gwt.components.utils.WebElementUtils;
import com.ihg.automation.selenium.listener.Verifier;
import com.ihg.automation.selenium.listener.VerifierUtils;
import com.ihg.automation.selenium.runtime.Cameron;
import com.ihg.automation.selenium.runtime.HighlightStyle;
import com.ihg.automation.selenium.runtime.Selenium;
import com.ihg.automation.selenium.runtime.SeleniumException;

public class Component implements Componentable
{
    private Logger logger = LoggerFactory.getLogger(Component.class);

    private static final String INVALID_INPUT = "#cc3300";

    private String type = "Component";
    private String name = NOT_SET;
    private WebElement element;
    private Component parent;

    private List<FindStep> steps = new ArrayList<FindStep>();

    protected static final String NOT_SET = "not set";
    private static final String COMPONENT_DESCR_PATTERN = "{0} (''{1}'')";

    protected Component(String type, String name)
    {
        this.type = type;
        this.name = name;
    }

    public Component(Component parent, String type, String name, List<FindStep> steps)
    {
        this.parent = parent;
        this.type = type;
        this.name = name;
        this.steps.addAll(steps);
    }

    public Component(Component parent, String type, String name, FindStep step)
    {
        this(parent, type, name, Arrays.asList(step));
    }

    private void initElement(boolean isWait)
    {
        if (isInitialized())
        {
            return;
        }

        if (hasParent())
        {
            parent.initElement(isWait);

            if (!parent.isInitialized() && hasSearchableParent())
            {
                element = null;
                return;
            }
        }

        element = hasParent() ? parent.element : null;

        for (Iterator<FindStep> iterator = steps.iterator(); iterator.hasNext();)
        {
            element = iterator.next().apply(new ComponentSearchMode(this, isWait));
            if ((element == null) && (iterator.hasNext()))
            {
                logger.warn("Find step returns 'null'. Other steps are skipped");
                return;
            }
        }
    }

    protected void initElement()
    {
        initElement(true);
    }

    @Override
    public void flush()
    {
        this.element = null;

        if (hasParent())
        {
            parent.flush();
        }
    }

    public SearchContext getSearchContext()
    {
        if (isInitialized())
        {
            return element;
        }

        if (hasParent())
        {
            return parent.getSearchContext();
        }

        return Selenium.getInstance().getDriver();
    }

    protected WebElement getElement(boolean isWait)
    {
        initElement(isWait);
        return element;
    }

    @Override
    public WebElement getElement()
    {
        return getElement(true);
    }

    public String getType()
    {
        return type;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    protected void setElement(WebElement element)
    {
        this.element = element;
    }

    protected List<WebElement> findElements(By paramBy, String elementsDescription)
    {
        return SearchContextUtils.findElements(getElement(), paramBy,
                format("{0} for {1}", elementsDescription, getDescription()));
    }

    @Override
    public String getDescription()
    {
        return format(COMPONENT_DESCR_PATTERN, type, name);
    }

    @Override
    public String toString()
    {
        return getDescription();
    }

    public void click()
    {
        initElement();

        if (isDisplayed() && isEnabled())
        {
            logger.debug("Click by {}", getDescription());
            element.click();
            return;
        }

        String message = format("Click by {0} impossible due to element is not displayed or disabled", getDescription());
        throw new SeleniumException(message, getScreenshotAs(OutputType.BASE64));
    }

    public void click(Verifier... verifiers)
    {
        click();
        VerifierUtils.verify(false, verifiers);
    }

    public void click(boolean isWait, Verifier... verifiers)
    {
        if (isWait)
        {
            clickAndWait(verifiers);
        }
        else
        {
            click(verifiers);
        }
    }

    public void clickAndWait(Verifier... verifiers)
    {
        if ((verifiers != null) && (verifiers.length > 0))
        {
            MessageBox.closeAll();
        }

        click();
        new WaitUtils().waitExecutingRequest(verifiers);
    }

    public Component scroll()
    {
        if (getElement(false) == null)
        {
            String message = format("Scroll to {0} impossible due to element is null", getDescription());
            throw new SeleniumException(message, getScreenshotAs(OutputType.BASE64));
        }

        logger.debug("Scroll to {}", getDescription());
        ((JavascriptExecutor) Selenium.getInstance().getDriver()).executeScript("arguments[0].scrollIntoView(true);",
                element);
        return this;
    }

    public void moveTo()
    {
        if (getElement() == null)
        {
            String message = format("Move to {0} impossible due to element is null", getDescription());
            throw new SeleniumException(message, getScreenshotAs(OutputType.BASE64));
        }

        logger.debug("Move to {}", getDescription());
        Actions actions = new Actions(Selenium.getInstance().getDriver());
        actions.moveToElement(element).perform();
    }

    @Override
    public String getText()
    {
        return getText(true);
    }

    @Override
    public String getText(boolean isWait)
    {
        return WebElementUtils.getText(getElement(isWait), getDescription());
    }

    @Override
    public boolean isDisplayed()
    {
        return isDisplayed(false);
    }

    @Override
    public boolean isDisplayed(boolean bWait)
    {
        return WebElementUtils.isDisplayed(getElement(bWait), getDescription());
    }

    public boolean isEnabled()
    {
        return WebElementUtils.isEnabled(getElement(), getDescription());
    }

    public boolean isInitialized()
    {
        return element != null;
    }

    private boolean hasParent()
    {
        return parent != null;
    }

    @Override
    public Component getParent()
    {
        return parent;
    }

    private boolean hasSearchableParent()
    {
        if (hasParent())
        {
            return CollectionUtils.isNotEmpty(parent.steps) || parent.hasSearchableParent();
        }

        return false;
    }

    public String getAttribute(String attribute)
    {
        try
        {
            return getElement().getAttribute(attribute);
        }
        catch (StaleElementReferenceException e)
        {
            flush();
            initElement(true);
            return getElement().getAttribute(attribute);
        }
    }

    public String getClassAttribute()
    {
        return getAttribute("class");
    }

    public String getCssValue(String propertyName)
    {
        return getElement().getCssValue(propertyName);
    }

    public int getPosition()
    {
        List<WebElement> precedingSiblings = findElements(By.xpath("./preceding-sibling::*"), "Preceding elements");

        return precedingSiblings.size() + 1;
    }

    public boolean isInvalid()
    {
        return getCssBorderColor().equals(INVALID_INPUT);
    }

    protected String getCssBorderColor()
    {
        return Color.fromString(getElement().getCssValue("border-top-color")).asHex();
    }

    public void addSteps(List<FindStep> steps)
    {
        this.steps.addAll(steps);
    }

    protected Component clone()
    {
        return new Component(parent, type, name, steps);
    }

    @Override
    public <T> T getScreenshotAs(OutputType<T> target)
    {
        WebElement element = getElement(false);
        String style = HighlightStyle.ERROR;

        if (element == null)
        {
            if (hasParent())
            {
                element = getParent().getElement(false);
                style = HighlightStyle.WARNING;
            }
        }

        return new Cameron().getScreenshotAs(element, style, target);
    }
}
