package com.ihg.automation.selenium.gwt.components;

import static com.ihg.automation.selenium.gwt.components.utils.FindStepUtils.byXpathWithWait;
import static java.text.MessageFormat.format;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;

import com.ihg.automation.selenium.gwt.components.utils.ByText;
import com.ihg.automation.selenium.runtime.SeleniumException;

public class RadioButton extends Component implements Selectable
{
    private static final Logger logger = LoggerFactory.getLogger(RadioButton.class);

    private static final String TYPE = "Radio Button";
    private static final String RADIO_BUTTON_PATTERN = ".//span[child::span[{0}]]/span[1]";

    public RadioButton(Component parent, ByText label)
    {
        super(parent, TYPE, label.getText(), byXpathWithWait(format(RADIO_BUTTON_PATTERN, label)));
    }

    public void select()
    {
        if (!isSelected())
        {
            click();
            Assert.assertEquals(isSelected(), true, format("{0} check-box status must be changed", getDescription()));
        }
        else
        {
            logger.warn("Element {} has already [isSelected=true]. Nothing to do.", getDescription());
        }
    }

    @Override
    public boolean isSelected()
    {
        String position = getCssValue("background-position");

        Pattern positionPattern = Pattern.compile("([-, 0-9]++)px ([-, 0-9]++)px");
        Matcher matcher = positionPattern.matcher(position);

        if ((position != null) && (matcher.matches()))
        {
            int positionY = Integer.parseInt(matcher.group(2));
            return positionY != 0;
        }

        throw new SeleniumException(format("Radio-Button status can not be identified for {0}", getDescription()),
                getElement());
    }
}
