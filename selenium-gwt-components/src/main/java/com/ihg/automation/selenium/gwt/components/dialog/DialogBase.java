package com.ihg.automation.selenium.gwt.components.dialog;

import static com.ihg.automation.selenium.gwt.components.utils.FindStepUtils.byXpath;
import static com.ihg.automation.selenium.gwt.components.utils.FindStepUtils.byXpathWithWait;

import com.ihg.automation.selenium.gwt.components.Component;
import com.ihg.automation.selenium.gwt.components.CustomContainerBase;
import com.ihg.automation.selenium.gwt.components.utils.ByText;
import com.ihg.automation.selenium.gwt.components.utils.FindStep;

public abstract class DialogBase extends CustomContainerBase
{
    private static final String TYPE = "Dialog";
    private static final String TEXT_BODY_TYPE = "Dialog Text Body";
    private static final FindStep BODY_TEXT_STEP = byXpath(".//div[normalize-space(@class)='ext-mb-text']");

    private static final String DIALOG_WITH_TEXT_PATTERN = "%s[%s]";
    private static final String DIALOG_WITH_TEXT_NAME_PATTERN = "type: %s, text: %s";

    public enum Type
    {
        ALERT("ext-mb-warning"), CONFIRM("ext-mb-question"), ERROR("ext-mb-error"), INFO("ext-mb-info"), DEFAULT(
                "x-hidden");

        String BY_STYLE = "//div[contains(@class, 'x-window-dlg')][descendant::div[normalize-space(@class)='ext-mb-icon %s']]";

        private String findXpath;
        private FindStep findStep;

        Type(String stylePostfix)
        {
            this.findXpath = String.format(BY_STYLE, stylePostfix);
            this.findStep = byXpathWithWait(findXpath);
        }

        public FindStep getFindStep()
        {
            return findStep;
        }

        public String getFindXpath()
        {
            return findXpath;
        }
    }

    public DialogBase(Type type)
    {
        super(TYPE, type.name(), type.getFindStep());
    }

    public DialogBase(Type type, ByText bodyText)
    {
        super(TYPE, String.format(DIALOG_WITH_TEXT_NAME_PATTERN, type.name(), bodyText.getText()),
                byXpathWithWait(String.format(DIALOG_WITH_TEXT_PATTERN, type.getFindXpath(), bodyText.toString())));
    }

    @Override
    public String getText(boolean isWait)
    {
        Component textArea = new Component(container, TEXT_BODY_TYPE, container.getName(), BODY_TEXT_STEP);
        return textArea.getText(isWait);
    }

}
