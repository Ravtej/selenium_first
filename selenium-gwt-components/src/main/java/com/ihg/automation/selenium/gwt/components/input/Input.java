package com.ihg.automation.selenium.gwt.components.input;

import com.ihg.automation.selenium.gwt.components.Component;
import com.ihg.automation.selenium.gwt.components.ComponentColumnPosition;
import com.ihg.automation.selenium.gwt.components.HavingDefaultValue;
import com.ihg.automation.selenium.gwt.components.input.InputXpath.InputXpathBuilder;

public class Input extends InputBase implements HavingDefaultValue
{
    private static final String DEFAULT_VALUE = "";
    private String defaultValue;

    private static final String TYPE = "Input";

    public Input(Component parent, String description)
    {
        super(parent, TYPE, description);
    }

    public Input(Component parent, InputXpath xpath, String description)
    {
        super(parent, xpath, TYPE, description);
    }

    public Input(Component parent, String description, int index, ComponentColumnPosition position)
    {
        this(parent, new InputXpathBuilder().column(index, position).build(), description);
    }

    @Override
    public String getDefaultValue()
    {
        return (defaultValue == null) ? DEFAULT_VALUE : defaultValue;
    }

    public void setDefaultValue(String defaultValue)
    {
        this.defaultValue = defaultValue;
    }
}
