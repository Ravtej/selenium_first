package com.ihg.automation.selenium.gwt.components.utils;

import static java.text.MessageFormat.format;

import org.apache.commons.lang3.StringUtils;

public abstract class ByText
{
    private String formattedText;
    private String text;

    protected ByText(String text, String formattedText)
    {
        this.text = text;
        this.formattedText = formattedText;
    }

    public abstract boolean compare(String elementText);

    public static ByText exact(String text)
    {
        return new ByExact(text);
    }

    public static ByText exactSelf(String text)
    {
        return new ByExactSelf(text);
    }

    public static ByText startsWith(String text)
    {
        return new ByStartsWith(text);
    }

    public static ByText startsWithSelf(String text)
    {
        return new ByStartsWithSelf(text);
    }

    public static ByText contains(String text)
    {
        return new ByContains(text);
    }

    public static ByText containsSelf(String text)
    {
        return new ByContainsSelf(text);
    }

    public String getText()
    {
        return text;
    }

    @Override
    public String toString()
    {
        return formattedText;
    }

    public static class ByExact extends ByText
    {
        public ByExact(String text)
        {
            super(text, format("normalize-space(text())=\"{0}\"", text));
        }

        @Override
        public boolean compare(String elementText)
        {
            return StringUtils.equalsIgnoreCase(elementText, getText());
        }
    }

    public static class ByExactSelf extends ByText
    {
        public ByExactSelf(String text)
        {
            super(text, format("normalize-space()=\"{0}\"", text));
        }

        @Override
        public boolean compare(String elementText)
        {
            return StringUtils.equalsIgnoreCase(elementText, getText());
        }
    }

    public static class ByStartsWith extends ByText
    {
        public ByStartsWith(String text)
        {
            super(format("starts-with({0})", text), format("starts-with(normalize-space(text()), \"{0}\")", text));
        }

        @Override
        public boolean compare(String elementText)
        {
            return StringUtils.startsWith(elementText, getText());
        }
    }

    public static class ByStartsWithSelf extends ByText
    {
        public ByStartsWithSelf(String text)
        {
            super(format("starts-with({0})", text), format("starts-with(normalize-space(), ''{0}'')", text));
        }

        @Override
        public boolean compare(String elementText)
        {
            return StringUtils.startsWith(elementText, getText());
        }
    }

    public static class ByContains extends ByText
    {
        public ByContains(String text)
        {
            super(format("contains({0})", text), format("contains(text(), \"{0}\")", text));
        }

        @Override
        public boolean compare(String elementText)
        {
            return StringUtils.contains(elementText, getText());
        }
    }

    public static class ByContainsSelf extends ByText
    {
        public ByContainsSelf(String text)
        {
            super(format("contains({0})", text), format("contains(., \"{0}\")", text));
        }

        @Override
        public boolean compare(String elementText)
        {
            return StringUtils.contains(elementText, getText());
        }
    }
}
