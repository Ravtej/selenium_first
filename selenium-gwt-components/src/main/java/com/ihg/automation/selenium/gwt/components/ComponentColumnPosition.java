package com.ihg.automation.selenium.gwt.components;

import static java.text.MessageFormat.format;

/**
 * Child html tags inside that searched elements can be found : <li>span - for
 * labels (e.g. <code>span/span</code>) and "pure" input (without disabled img)
 * (e.g. <code>span/span[child::input and not(child::img)]</code>) <li>div - for
 * select components (e.g. <code>div/div[child::input and child::img]</code>
 */
public enum ComponentColumnPosition
{
    /**
     * <b>Default</b>, all child components are placed in one <code>td</code>
     */
    SINGLE_TD("*[{0}]", Axis.CHILD, false),
    /**
     * When child components are placed in one <code>td</code> but there is
     * child <code>div</code>
     */
    SINGLE_TD_INNER_DIV("div/div[{0}]", Axis.DESCENDANT, false),
    /**
     * When <code>input</code> switch to <code>select</code> and vice versa.<br>
     * Input elements can be placed in <code>div</code> or <code>span</code>.
     */
    SINGLE_TD_SWITCH_MODE("*[not(contains(@class, ''hide'')) and not(contains(@style, ''none''))][{0}]", Axis.CHILD,
            false),
    /**
     * When child components are placed in separate table, e.g. phone number
     * component
     */
    TABLE("td[{0}]", Axis.DESCENDANT, false),
    /**
     * When child component is placed in separate <code>td</code>, preceding td in second position.
     */
    TABLE_TD("following-sibling::td[2]//td[{0}]", Axis.DESCENDANT, true),
    /**
     * When child components are placed in separate table, and child separate tr
     */
    TABLE_TR("tr[{0}]", Axis.DESCENDANT, false),
    /**
     * When child components are placed in separate <code>td</code>.
     */
    FOLLOWING_TD("following-sibling::td[{0}]", Axis.CHILD, true);

    private String xpathPattern;
    private Axis axis;
    private boolean useInitialLabel;

    private ComponentColumnPosition(String xpathPattern, Axis axis, boolean useInitialLabel)
    {
        this.xpathPattern = xpathPattern;
        this.axis = axis;
        this.useInitialLabel = useInitialLabel;
    }

    public Axis getAxis()
    {
        return axis;
    }

    public String getXpath(int index)
    {
        return format(xpathPattern, index);
    }

    public boolean isUseInitialLabel()
    {
        return useInitialLabel;
    }
}
