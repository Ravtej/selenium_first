package com.ihg.automation.selenium.gwt.components.grid;

import static com.ihg.automation.selenium.gwt.components.utils.FindStepUtils.byXpathWithWait;

import java.lang.reflect.ParameterizedType;
import java.text.MessageFormat;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.reflect.ConstructorUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ihg.automation.selenium.gwt.components.Component;
import com.ihg.automation.selenium.gwt.components.utils.ByStringText;
import com.ihg.automation.selenium.gwt.components.utils.ComponentSearchMode;
import com.ihg.automation.selenium.gwt.components.utils.FindStep;
import com.ihg.automation.selenium.gwt.components.utils.SearchContextUtils;
import com.ihg.automation.selenium.runtime.SeleniumException;

public class TreeGridRow<R extends TreeGridRow<R, C>, C extends CellsName> extends GridRowBase<C>
{
    private static final Logger logger = LoggerFactory.getLogger(TreeGridRow.class);

    private static final String IMG_XPATH = "./descendant::img";
    private static final String STYLE_ATTRIBUTE = "style";
    private static final String CHILD_LIST_XPATH = "./following-sibling::div";
    private static final String TYPE_PATTERN = "Tree Grid Row [{0}]";

    private static final String GRID_ROW_RELATIVE_XPATH_PATTERN = ".//div[contains(@class, ''x-grid3-row'') and descendant::img[contains(@style, ''width:{1}px'')]][{0}]";

    protected static final int WIDTH_SHIFT = 18; // 18px for each level

    protected static final int TOP_LEVEL = 0;

    private static Pattern widthPattern = Pattern.compile("(?<=width: )([0-9]+)");

    protected TreeGridRow(R parent, C cellsName, ByStringText cellText)
    {
        super(parent, StringUtils.replace(parent.getType(), "]", "->" + cellText + "]"), parent.getName(),
                byCellText(cellsName, cellText));
    }

    protected TreeGridRow(R parent, int index)
    {
        super(parent, StringUtils.replace(parent.getType(), "]", "->" + index + "]"), parent.getName(), byIndex(index));
    }

    private TreeGridRow(WebElement element)
    {
        super(null, null, null, new FindStep()
        {
            @Override
            public WebElement apply(ComponentSearchMode input)
            {
                return element;
            }
        });
    }

    protected TreeGridRow(TreeGrid<R, C> parent, int index)
    {
        super(parent, MessageFormat.format(TYPE_PATTERN, index), parent.getName(), byXpathWithWait(
                MessageFormat.format(GRID_ROW_RELATIVE_XPATH_PATTERN, index, (TOP_LEVEL * WIDTH_SHIFT))));
    }

    public void expand()
    {
        new TreeExpander(this).expand();
    }

    public R getSubRow(C cellName, ByStringText cellText)
    {
        Class<R> rowClass = (Class<R>) ((ParameterizedType) getClass().getGenericSuperclass())
                .getActualTypeArguments()[0];

        expand();
        try
        {
            return ConstructorUtils.invokeConstructor(rowClass, this, cellName, cellText);
        }
        catch (Exception e)
        {
            throw new SeleniumException("Exception during Grid Row object initialization", getElement());
        }
    }

    public R getSubRow(int index)
    {
        Class<R> rowClass = (Class<R>) ((ParameterizedType) getClass().getGenericSuperclass())
                .getActualTypeArguments()[0];

        expand();
        try
        {
            return ConstructorUtils.invokeConstructor(rowClass, this, index);
        }
        catch (Exception e)
        {
            throw new SeleniumException("Exception during Grid Row object initialization", getElement());
        }
    }

    public int getPosition()
    {
        String childWidth = getElement().findElement(By.xpath(IMG_XPATH)).getAttribute(STYLE_ATTRIBUTE);
        Matcher matcher = widthPattern.matcher(childWidth);

        if ((childWidth != null) && (matcher.find()))
        {
            return Integer.parseInt(matcher.group(0)) / WIDTH_SHIFT;
        }
        else
        {
            return -1;
        }
    }

    @Override
    protected String getCellTypePattern()
    {
        return MessageFormat.format("Cell [{0},{1}]", getType(), "{0}");
    }

    public static <C extends CellsName> FindStep byCellText(C cellName, ByStringText cellText)
    {
        return new FindStep()
        {
            @Override
            public WebElement apply(ComponentSearchMode input)
            {
                TreeGridRow treeGridRow = null;
                Component component = input.getComponent();
                List<WebElement> elementList = SearchContextUtils.findElements(component.getSearchContext(),
                        By.xpath(CHILD_LIST_XPATH), "Child list");

                int parentPosition = component.getPosition();

                for (WebElement element : elementList)
                {
                    treeGridRow = new TreeGridRow(element);
                    int parsedLevel = treeGridRow.getPosition();
                    if (parsedLevel == parentPosition + 1)
                    {
                        if (cellText.compare(treeGridRow.getCell(cellName).getText()))
                        {
                            logger.debug("Child row for {} is found by cell text {} for the {} cell", new Object[] {
                                    component.getDescription(), cellText.getText(), cellName.getName() });
                            break;
                        }
                    }
                    else if (parsedLevel == parentPosition)
                    {
                        logger.debug("Stop searching child row for {} by cell text {} for the {} cell",
                                new Object[] { component.getDescription(), cellText.getText(), cellName.getName() });
                        break;
                    }
                }
                return treeGridRow.getElement();
            }
        };
    }

    public static FindStep byIndex(int index)
    {
        return new FindStep()
        {
            @Override
            public WebElement apply(ComponentSearchMode input)
            {
                TreeGridRow treeGridRow = null;
                int finalIndex = 0;
                Component component = input.getComponent();
                List<WebElement> elementList = SearchContextUtils.findElements(component.getSearchContext(),
                        By.xpath(CHILD_LIST_XPATH), "Child list");

                int parentPosition = component.getPosition();

                for (WebElement element : elementList)
                {
                    treeGridRow = new TreeGridRow(element);
                    int parsedLevel = treeGridRow.getPosition();
                    if (parsedLevel == parentPosition + 1)
                    {
                        finalIndex++;
                        if (finalIndex == index)
                        {
                            logger.debug("Child for {} is found by {} index", component.getDescription(), index);
                            break;
                        }
                    }
                    else if (parsedLevel == parentPosition)
                    {
                        logger.debug("Stop searching child for {} by {} index", component.getDescription(), index);
                        break;
                    }
                }
                return treeGridRow.getElement();
            }
        };
    }
}
