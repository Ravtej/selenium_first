package com.ihg.automation.selenium.gwt.components.label;

import com.ihg.automation.selenium.gwt.components.Component;
import com.ihg.automation.selenium.gwt.components.utils.ByText;
import com.ihg.automation.selenium.gwt.components.utils.ComponentFunctionUtils;
import com.ihg.automation.selenium.gwt.components.utils.FindStep;
import com.ihg.automation.selenium.gwt.components.utils.FindStepUtils;

public class TableLabel extends LabelBase
{
    private static final String TYPE = "Table Label";

    private static final FindStep EXTRA_STEP = FindStepUtils.byXpathWithWait(
            "./parent::tr/following-sibling::tr/td[{0}]", ComponentFunctionUtils.getPosition());

    public TableLabel(Component parent, ByText label)
    {
        super(parent, label, TYPE, EXTRA_STEP);
    }
}
