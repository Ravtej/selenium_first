package com.ihg.automation.selenium;

public class Settings
{
    private Settings()
    {
    }

    /**
     * The amount of time to wait before giving up; the default is 30 seconds
     */
    public static final long DEFAULT_TIMEOUT = 30L;
    /**
     * The interval to pause between checking; the default is 500 milliseconds
     */
    public static final long DEFAULT_INTERVAL = 500L;
    /**
     * The amount of time to wait executing request before giving up; the
     * default is 60 seconds
     */
    public static final long DEFAULT_EXECUTE_REQUEST_TIMEOUT = 60L;
}
