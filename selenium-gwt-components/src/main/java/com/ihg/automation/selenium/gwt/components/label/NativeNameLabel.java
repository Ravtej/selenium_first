package com.ihg.automation.selenium.gwt.components.label;

import static com.ihg.automation.selenium.gwt.components.utils.FindStepUtils.byAncestor;

import java.util.Arrays;

import com.ihg.automation.selenium.gwt.components.Component;
import com.ihg.automation.selenium.gwt.components.utils.Ancestor;
import com.ihg.automation.selenium.gwt.components.utils.ByText;
import com.ihg.automation.selenium.gwt.components.utils.FindStep;

public class NativeNameLabel extends LabelBase
{
    private static final String TYPE = "Native Name Label";
    private static final FindStep GO_TO_ROW = byAncestor(new Ancestor.Builder().parentTag("tr").build());
    private static final FindStep GO_TO_CELL = byAncestor(new Ancestor.Builder().parentTag("td").build());

    public NativeNameLabel(Component parent, ByText label, FindStep step)
    {
        super(parent, label, TYPE, Arrays.asList(GO_TO_ROW, GO_TO_CELL, step));
    }
}
