package com.ihg.automation.selenium.gwt.components.utils;

import java.text.MessageFormat;

import org.apache.commons.lang3.StringUtils;

public class XPathUtils
{
    public static final String ID_PATTERN = "x-auto-";

    private XPathUtils()
    {

    }

    public static String wrapInSquareBrackets(String str)
    {
        return !StringUtils.isEmpty(str) ? "[" + str + "]" : StringUtils.EMPTY;
    }

    public static String singleQuoteDuplication(String str)
    {
        return str.replaceAll("'", "''");
    }

    public static String getIdByIndex(int index)
    {
        return ID_PATTERN + String.valueOf(index);
    }

    public static String getFormattedXpath(String pattern, Object... arguments)
    {
        return MessageFormat.format(singleQuoteDuplication(pattern), arguments);
    }
}
