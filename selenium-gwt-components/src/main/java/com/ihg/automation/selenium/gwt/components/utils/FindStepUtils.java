package com.ihg.automation.selenium.gwt.components.utils;

import static com.ihg.automation.selenium.Settings.DEFAULT_INTERVAL;
import static com.ihg.automation.selenium.Settings.DEFAULT_TIMEOUT;

import java.text.MessageFormat;

import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.SearchContext;
import org.openqa.selenium.WebElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ihg.automation.selenium.condition.SearchContextExpectedCondition;
import com.ihg.automation.selenium.condition.SearchContextWait;
import com.ihg.automation.selenium.gwt.components.Component;

public class FindStepUtils
{
    private static final Logger logger = LoggerFactory.getLogger(FindStepUtils.class);

    private FindStepUtils()
    {
    }

    public static FindStep byXpath(final String xpath, final ComponentFunction<?> function)
    {
        return new FindStep()
        {
            @Override
            public WebElement apply(ComponentSearchMode input)
            {
                return byXpath(input.getComponent(), xpath, function, false);
            }
        };
    }

    public static FindStep byXpath(final String xpath)
    {
        return byXpath(xpath, null);
    }

    public static FindStep byXpath(final String xpath, final boolean isWait)
    {
        return new FindStep()
        {
            @Override
            public WebElement apply(ComponentSearchMode input)
            {
                return byXpath(input.getComponent(), xpath, null, (isWait && input.isWait()));
            }
        };
    }

    public static FindStep byXpathWithWait(final String xpath, final ComponentFunction<?> function)
    {
        return new FindStep()
        {
            @Override
            public WebElement apply(ComponentSearchMode input)
            {
                return byXpath(input.getComponent(), xpath, function, input.isWait());
            }
        };
    }

    public static FindStep byXpathWithWait(final String xpath)
    {
        return byXpathWithWait(xpath, null);
    }

    public static FindStep byXpathWithWait(final String xpath, final int position, final ComponentFunction<?> function)
    {
        return new FindStep()
        {
            @Override
            public WebElement apply(ComponentSearchMode input)
            {
                final Component component = input.getComponent();

                String searchXpath = getXpath(component, xpath, function);

                if (StringUtils.isEmpty(searchXpath))
                {
                    logger.error("Find step for {} is skipped due to xpath is not set", component.getDescription());
                    return component.getElement();
                }

                SearchContextExpectedCondition<WebElement> elementIsFound = Conditions
                        .elementIsFound(By.xpath(searchXpath), position, component.getDescription());

                return byCondition(elementIsFound, component, input.isWait());
            }
        };
    }

    public static FindStep byXpathWithWait(final String xpath, final int position)
    {
        return byXpathWithWait(xpath, position, null);
    }

    public static FindStep byAncestor(final Ancestor ancestor)
    {
        return new FindStep()
        {
            @Override
            public WebElement apply(ComponentSearchMode input)
            {
                WebElement element;
                String ancestorLocator = ".";// = XPath;
                String resultAncestorLocator = ancestorLocator;

                String parentPrefixWithoutAttr = "/..";
                String parentPrefixWithAttr = "/parent::" + ancestor.getXpath();
                int maxAncestorLevel = ancestor.getMaxAncestorLevel();

                final Component component = input.getComponent();
                SearchContext searchContext = component.getSearchContext();

                for (int i = 0; i < maxAncestorLevel; i++)
                {
                    ancestorLocator = resultAncestorLocator + parentPrefixWithAttr;
                    resultAncestorLocator = resultAncestorLocator + parentPrefixWithoutAttr;

                    element = Conditions.elementIsFound(By.xpath(ancestorLocator), component.getDescription())
                            .apply(searchContext);

                    if (element != null)
                    {
                        return element;
                    }
                }

                return null;
            }
        };
    }

    private static String getXpath(final Component component, final String xpath, final ComponentFunction<?> function)
    {
        if (function != null)
        {
            return MessageFormat.format(xpath, function.apply(component));
        }
        return xpath;
    }

    private static WebElement byCondition(final SearchContextExpectedCondition<WebElement> searchCondition,
            final Component component, final boolean isWait)
    {
        SearchContext searchContext = component.getSearchContext();

        if (isWait)
        {
            logger.debug("Wait {}", component.getDescription());
            return new SearchContextWait(searchContext, DEFAULT_TIMEOUT, DEFAULT_INTERVAL).until(searchCondition);
        }

        return searchCondition.apply(searchContext);
    }

    private static WebElement byXpath(final Component component, final String xpath,
            final ComponentFunction<?> function, final boolean isWait)
    {

        String searchXpath = getXpath(component, xpath, function);

        if (StringUtils.isEmpty(searchXpath))
        {
            logger.error("Find step for {} is skipped due to xpath is not set", component.getDescription());
            return component.getElement();
        }

        SearchContextExpectedCondition<WebElement> elementIsFound = Conditions.elementIsFound(By.xpath(searchXpath),
                component.getDescription());

        return byCondition(elementIsFound, component, isWait);
    }
}
