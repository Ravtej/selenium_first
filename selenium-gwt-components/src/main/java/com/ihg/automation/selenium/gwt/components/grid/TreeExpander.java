package com.ihg.automation.selenium.gwt.components.grid;

import org.apache.commons.lang3.StringUtils;

import com.ihg.automation.selenium.gwt.components.Component;
import com.ihg.automation.selenium.gwt.components.expander.ExpanderLiteBase;
import com.ihg.automation.selenium.gwt.components.expander.ExpanderStatus;
import com.ihg.automation.selenium.gwt.components.utils.FindStep;
import com.ihg.automation.selenium.gwt.components.utils.FindStepUtils;

public class TreeExpander extends ExpanderLiteBase
{
    private static final FindStep EXPANDER_RELATIVE_STEP = FindStepUtils.byXpath(".//td[@class='x-tree3-el-jnt']/img");

    protected TreeExpander(TreeGridRow<?, ?> parent)
    {
        super(parent);
    }

    @Override
    protected Component getClickableComponent()
    {
        return new Component(this, getType(), getName(), EXPANDER_RELATIVE_STEP);
    }

    @Override
    public ExpanderStatus getExpanderStatus()
    {
        String background = getClickableComponent().getCssValue("background");

        if (background != null)
        {
            if (StringUtils.contains(background, "scroll -66px"))
            {
                return ExpanderStatus.COLLAPSED;
            }

            if (StringUtils.contains(background, "scroll -34px"))
            {
                return ExpanderStatus.EXPANDED;
            }
        }

        return ExpanderStatus.UNKNOWN;
    }
}
