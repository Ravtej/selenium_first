package com.ihg.automation.selenium.gwt.components;

public interface Selectable
{
    public boolean isSelected();
}
