package com.ihg.automation.selenium.gwt.components.grid.navigation;

import static com.ihg.automation.selenium.gwt.components.utils.FindStepUtils.byXpathWithWait;

import com.ihg.automation.selenium.gwt.components.Button;
import com.ihg.automation.selenium.gwt.components.Component;
import com.ihg.automation.selenium.gwt.components.ComponentColumnPosition;
import com.ihg.automation.selenium.gwt.components.input.Input;

public class LeftPanel extends Component
{
    public LeftPanel(Component component)
    {
        super(component, "Panel", "Left Panel", byXpathWithWait(".//td[@class='x-toolbar-left']/table/tbody/tr"));
    }

    private ComponentColumnPosition POSITION = ComponentColumnPosition.TABLE;

    public Button getFirstPage()
    {
        return new Button(this, "First Page Button", 1, POSITION);
    }

    public Button getPreviousPage()
    {
        return new Button(this, "Previous Page Button", 2, POSITION);
    }

    public Input getPageNumber()
    {
        return new Input(this, "Page Number", 5, POSITION);
    }

    public PagesAmount getPagesAmount()
    {
        return new PagesAmount(this);
    }

    public Button getNextPage()
    {
        return new Button(this, "Next Page Button", 8, POSITION);
    }

    public Button getLastPage()
    {
        return new Button(this, "Last Page Button", 9, POSITION);
    }
}
