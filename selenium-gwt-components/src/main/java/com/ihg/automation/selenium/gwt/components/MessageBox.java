package com.ihg.automation.selenium.gwt.components;

import static com.ihg.automation.selenium.Settings.DEFAULT_INTERVAL;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ihg.automation.selenium.gwt.components.utils.FindStep;
import com.ihg.automation.selenium.gwt.components.utils.FindStepUtils;
import com.ihg.automation.selenium.runtime.Selenium;

public class MessageBox extends Component
{
    private static final Logger logger = LoggerFactory.getLogger(MessageBox.class);

    private static final String TYPE = "Message Box";
    protected static final String TEXT_BODY_TYPE = "Message Box Text Body";
    private static final FindStep TEXT_BODY_STEP = FindStepUtils.byXpath(".//div[@class='y-mb-body']//div[text()]/..");

    private static final long CLOSE_MESSAGE_BOX_TIMEOUT = 10L;

    public MessageBox(MessageBoxType type)
    {
        super(null, TYPE, type.getName(), type.getFindStep());
    }

    @Override
    public String getText(boolean isWait)
    {
        Component textArea = new Component(this, TEXT_BODY_TYPE, getName(), TEXT_BODY_STEP);
        return textArea.getText(isWait);
    }

    /**
     * Close message box by pressing 'Close' button or just wait while it is
     * disappear
     */
    public void close()
    {
        close(true);
    }

    public void close(boolean doWait)
    {
        if (isDisplayed(doWait))
        {
            logger.info("Close {}", getDescription());

            new WebDriverWait(Selenium.getInstance().getDriver(), CLOSE_MESSAGE_BOX_TIMEOUT, DEFAULT_INTERVAL)
                    .until(closeMessageBox(this));
        }
    }

    public static void closeAll()
    {
        for (MessageBoxType messageBoxType : MessageBoxType.values())
        {
            new MessageBox(messageBoxType).close(false);
        }
    }

    private static ExpectedCondition<Boolean> closeMessageBox(final MessageBox msgBox)
    {
        return new ExpectedCondition<Boolean>()
        {
            boolean result;

            @Override
            public Boolean apply(WebDriver driver)
            {
                result = (!msgBox.isDisplayed() || !msgBox.isEnabled());

                if (!result)
                {
                    Button close = new Button(msgBox, Button.CLOSE);

                    if (close.isDisplayed(false))
                    {
                        try
                        {
                            close.click();
                        }
                        catch (Exception e)
                        {
                            logger.warn(e.getMessage());
                        }
                    }

                    logger.debug("Wait until {} is visible ...", msgBox.getDescription());
                }

                return result;
            }

            @Override
            public String toString()
            {
                return String.format("missing element(s) [%s] on the page", msgBox.getDescription());
            }
        };
    }
}
