package com.ihg.automation.selenium.gwt.components.grid.navigation;

import static com.ihg.automation.selenium.gwt.components.utils.FindStepUtils.byAncestor;
import static com.ihg.automation.selenium.gwt.components.utils.FindStepUtils.byXpath;

import java.util.Arrays;

import com.ihg.automation.selenium.gwt.components.Component;
import com.ihg.automation.selenium.gwt.components.grid.Grid;
import com.ihg.automation.selenium.gwt.components.utils.Ancestor;
import com.ihg.automation.selenium.gwt.components.utils.FindStep;

public class GridNavigationPanel extends Component
{
    public static final FindStep GO_TO_PANEL = byAncestor(new Ancestor.Builder().attributes(
            "contains(@class, 'x-panel-bwrap')").build());
    public static final FindStep GO_TO_NAVIGATE_PANEL = byXpath(".//div[@class='x-panel-bbar']");

    public GridNavigationPanel(Grid parent)
    {
        super(parent, "Panel", "Navigation Panel", Arrays.asList(GO_TO_PANEL, GO_TO_NAVIGATE_PANEL));
    }

    public LeftPanel getLeftPanel()
    {
        return new LeftPanel(this);
    }

    public RightPanel getRightPanel()
    {
        return new RightPanel(this);
    }
}
