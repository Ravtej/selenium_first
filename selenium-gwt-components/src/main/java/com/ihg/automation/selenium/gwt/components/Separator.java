package com.ihg.automation.selenium.gwt.components;

import static com.ihg.automation.selenium.gwt.components.utils.FindStepUtils.byXpathWithWait;
import static com.ihg.automation.selenium.gwt.components.utils.XPathUtils.getFormattedXpath;

import com.ihg.automation.selenium.gwt.components.utils.ByText;

public class Separator extends Container
{
    private static final String TYPE = "Separator";

    public static final String FIELDSET_PATTERN = ".//fieldset[child::legend[{0}]]/following-sibling::*[1]";
    public static final String FIELDSET_PATTERN_SECOND_SIBLING = ".//fieldset[child::legend[{0}]]/following-sibling::*[2]";
    public static final String FIELDSET_PATTERN_NEXT_TR = ".//tr[descendant::fieldset[child::legend[{0}]]]/following-sibling::*[1]";
    private static final String FIELDSET_PATTERN_NO_TEXT = ".//fieldset/following-sibling::*[1]";

    public Separator(Component parent, ByText text)
    {
        super(parent, TYPE, text.getText(), byXpathWithWait(getFormattedXpath(FIELDSET_PATTERN, text)));
    }

    public Separator(Component parent, ByText text, String xPath)
    {
        super(parent, TYPE, text.getText(), byXpathWithWait(getFormattedXpath(xPath, text)));
    }

    public Separator(Component parent)
    {
        super(parent, TYPE, null, byXpathWithWait(FIELDSET_PATTERN_NO_TEXT));
    }
}
