package com.ihg.automation.selenium.gwt.components;

import java.util.List;

import com.ihg.automation.selenium.gwt.components.checkbox.CheckBox;
import com.ihg.automation.selenium.gwt.components.checkbox.InputCheckBox;
import com.ihg.automation.selenium.gwt.components.input.Input;
import com.ihg.automation.selenium.gwt.components.input.InputXpath.InputXpathBuilder;
import com.ihg.automation.selenium.gwt.components.input.Select;
import com.ihg.automation.selenium.gwt.components.label.Label;
import com.ihg.automation.selenium.gwt.components.label.TopLabel;
import com.ihg.automation.selenium.gwt.components.panel.FieldSet;
import com.ihg.automation.selenium.gwt.components.panel.Panel;
import com.ihg.automation.selenium.gwt.components.utils.ByText;
import com.ihg.automation.selenium.gwt.components.utils.FindStep;

public class Container extends Component
{
    private static final String WIN_TYPE = "Window";
    private static final String WIN_NAME = "Main";

    public Container()
    {
        super(WIN_TYPE, WIN_NAME);
    }

    protected Container(String type, String name)
    {
        super(type, name);
    }

    public Container(Component parent, String type, String name, List<FindStep> steps)
    {
        super(parent, type, name, steps);
    }

    public Container(Component parent, String type, String name, FindStep step)
    {
        super(parent, type, name, step);
    }

    public Container(String type, String name, FindStep step)
    {
        super(null, type, name, step);
    }

    public Input getInput(String description, int index)
    {
        return new Input(this, new InputXpathBuilder().row(index).build(), description);
    }

    public Input getInputByLabel(String labelText)
    {
        return getLabel(labelText).getInput();
    }

    public Input getInputByLabel(String labelText, String inputDescription, int columnIndex,
            ComponentColumnPosition position)
    {
        return getLabel(labelText).getInput(inputDescription, columnIndex, position);
    }

    public Input getInputByLabel(String labelText, String inputDescription, int columnIndex)
    {
        return getInputByLabel(labelText, inputDescription, columnIndex, ComponentColumnPosition.SINGLE_TD);
    }

    public Input getInputByTopLabel(String labelText)
    {
        return getTopLabel(labelText).getInput();
    }

    public FieldSet getFieldSet(ByText headerText)
    {
        return new FieldSet(this, headerText);
    }

    public FieldSet getFieldSet(String headerText)
    {
        return getFieldSet(ByText.exact(headerText));
    }

    public Separator getSeparator(ByText headerText, String xPath)
    {
        return new Separator(this, headerText, xPath);
    }

    public Separator getSeparator(ByText headerText)
    {
        return getSeparator(headerText, Separator.FIELDSET_PATTERN);
    }

    public Separator getSeparator(String headerText)
    {
        return getSeparator(headerText, Separator.FIELDSET_PATTERN);
    }

    public Separator getSeparator(String headerText, String xPath)
    {
        return getSeparator(ByText.exact(headerText), xPath);
    }

    public Panel getPanel(ByText headerText)
    {
        return new Panel(this, headerText);
    }

    public Panel getPanel(String headerText)
    {
        return getPanel(ByText.exact(headerText));
    }

    public Button getButton(String name)
    {
        return new Button(this, name);
    }

    public Menu getMenu(String name)
    {
        return new Menu(this, name);
    }

    public Menu getMenu(ByText text)
    {
        return new Menu(this, text);
    }

    public Select getSelect(String description)
    {
        return new Select(this, description);
    }

    public Select getSelect(String description, int index)
    {
        return new Select(this, new InputXpathBuilder().row(index).build(), description);
    }

    public Select getSelect(String description, int rowIndex, int columnIndex)
    {
        return new Select(this, new InputXpathBuilder().row(rowIndex).column(columnIndex).build(), description);
    }

    public Select getSelectByLabel(String labelText)
    {
        return getLabel(labelText).getSelect();
    }

    public Select getSelectByLabel(String labelText, String inputDescription, int columnIndex,
            ComponentColumnPosition position)
    {
        return getLabel(labelText).getSelect(inputDescription, columnIndex, position);
    }

    public Select getSelectByLabel(String labelText, String inputDescription, int columnIndex)
    {
        return getSelectByLabel(labelText, inputDescription, columnIndex, ComponentColumnPosition.SINGLE_TD);
    }

    public Select getSelectByTopLabel(String labelText)
    {
        return getTopLabel(labelText).getSelect();
    }

    public Tab getTab(String tabText)
    {
        return new Tab(this, tabText);
    }

    public CheckBox getCheckBox(ByText label)
    {
        return new CheckBox(this, label);
    }

    public CheckBox getCheckBox(String label)
    {
        return getCheckBox(ByText.exact(label));
    }

    public InputCheckBox getInputCheckBox(ByText label)
    {
        return new InputCheckBox(this, label);
    }

    public InputCheckBox getInputCheckBox(String label)
    {
        return getInputCheckBox(ByText.exact(label));
    }

    public Label getLabel(ByText label)
    {
        return new Label(this, label);
    }

    public Label getLabel(String label)
    {
        return getLabel(ByText.exact(label));
    }

    public TopLabel getTopLabel(ByText label)
    {
        return new TopLabel(this, label);
    }

    public TopLabel getTopLabel(String label)
    {
        return getTopLabel(ByText.exact(label));
    }

    public Link getLink(String linkText)
    {
        return new Link(this, linkText);
    }

    public DualList getDualList(String descr)
    {
        return new DualList(this, descr);
    }
}
