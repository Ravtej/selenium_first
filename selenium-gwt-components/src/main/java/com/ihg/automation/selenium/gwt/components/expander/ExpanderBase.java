package com.ihg.automation.selenium.gwt.components.expander;

import com.ihg.automation.selenium.gwt.components.Component;
import com.ihg.automation.selenium.gwt.components.Container;

public abstract class ExpanderBase extends ExpanderLiteBase
{
    protected static final String EXPANDED_AREA_TYPE = "Expanded Area";

    protected ExpanderBase(Component parent)
    {
        super(parent);
    }

    public abstract Container getExpandedArea();
}
