package com.ihg.automation.selenium.gwt.components.tip;

import java.text.MessageFormat;

import com.ihg.automation.selenium.gwt.components.utils.FindStep;
import com.ihg.automation.selenium.gwt.components.utils.FindStepUtils;

public enum TipType
{
    INFO("Info tip", "x-tip x-component"), WARNING("Warning tip", "x-tip x-form-invalid-tip x-component");

    private String name;
    private String classAttribute;

    TipType(String name, String classAttribute)
    {
        this.name = name;
        this.classAttribute = classAttribute;
    }

    public String getName()
    {
        return name;
    }

    private String getXpath()
    {
        return MessageFormat.format("//div[contains(@class, ''{0}'')]", classAttribute);
    }

    public FindStep getFindStep()
    {
        return FindStepUtils.byXpathWithWait(getXpath());
    }
}
