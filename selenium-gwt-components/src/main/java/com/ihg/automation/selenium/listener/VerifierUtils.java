package com.ihg.automation.selenium.listener;

public class VerifierUtils
{
    public static void verify(boolean checkPrecondition, Verifier... verifiers)
    {
        if (verifiers == null)
        {
            return;
        }

        for (Verifier verifier : verifiers)
        {
            if (verifier != null)
            {
                verifier.verify(checkPrecondition);
            }
        }
    }

}
