package com.ihg.automation.selenium.gwt.components;

public interface HavingDefaultValue
{
    String getDefaultValue();
}
