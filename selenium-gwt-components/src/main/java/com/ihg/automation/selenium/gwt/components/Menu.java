package com.ihg.automation.selenium.gwt.components;

import static com.ihg.automation.selenium.gwt.components.utils.FindStepUtils.byXpath;
import static com.ihg.automation.selenium.gwt.components.utils.FindStepUtils.byXpathWithWait;
import static java.text.MessageFormat.format;

import org.apache.commons.lang3.StringUtils;

import com.ihg.automation.selenium.gwt.components.checkbox.ItemCheckBox;
import com.ihg.automation.selenium.gwt.components.utils.ByText;
import com.ihg.automation.selenium.gwt.components.utils.ComponentFunction;
import com.ihg.automation.selenium.gwt.components.utils.ComponentFunctionUtils;
import com.ihg.automation.selenium.gwt.components.utils.XPathUtils;
import com.ihg.automation.selenium.runtime.SeleniumException;

public class Menu extends Component
{
    private static final String TYPE = "Menu";
    private static final String MENU_PATTERN = ".//table[contains(@class, ''x-btn x-component x-btn-noicon'') and descendant::button[{0}]]";

    public Menu(Component parent, String text)
    {
        this(parent, ByText.exact(text));
    }

    public Menu(Component parent, ByText text)
    {
        super(parent, TYPE, text.getText(), byXpathWithWait(format(MENU_PATTERN, text)));
    }

    public Menu(Component parent, String name, String xpath)
    {
        super(parent, TYPE, name, byXpathWithWait(xpath));
    }

    public boolean isArrow()
    {
        Component emTag = new Component(this, "Menu EM tag", getName(), byXpath(".//em[child::button]"));
        return StringUtils.contains(emTag.getClassAttribute(), "x-btn-arrow");
    }

    @Override
    public void click()
    {
        MessageBox.closeAll();
        super.click();
    }

    public Item getSubMenu(String listItemText)
    {
        return new List(this).getItem(ByText.exact(listItemText));
    }

    public void openList()
    {
        if (isArrow())
        {
            click();
        }
        else
        {
            throw new SeleniumException(format("{0} has no list items", getDescription()), getElement());
        }
    }

    protected ItemCheckBox getItemCheckBox(String label)
    {
        List listItems = new List(this);
        return listItems.getItemCheckBox(ByText.exact(label));
    }

    private class List extends Component
    {
        private static final String TYPE = "Menu Items List";

        private List(Menu parent)
        {
            super(parent, TYPE, parent.getName(), byXpathWithWait("//div[@id=''{0}'']", getMenuListIndex()));
        }

        protected Item getItem(ByText text)
        {
            return new Item(this, text);
        }

        protected ItemCheckBox getItemCheckBox(ByText label)
        {
            return new ItemCheckBox(this, label);
        }

        protected void open()
        {
            ((Menu) getParent()).openList();
        };
    }

    public class Item extends Component
    {
        private static final String TYPE = "Menu Item";
        private static final String MENU_ITEM_PATTERN = ".//*[contains(@class, ''x-menu-item'') and {0}]";

        private Item(List parent, ByText text)
        {
            super(parent, TYPE, text.getText(), byXpath(format(MENU_ITEM_PATTERN, text)));
        }

        @Override
        public boolean isEnabled()
        {
            return !StringUtils.contains(getClassAttribute(), "x-item-disabled");
        }
    }

    private static ComponentFunction<String> getMenuListIndex()
    {
        return new ComponentFunction<String>()
        {
            @Override
            public String apply(Component input)
            {
                ((List) input).open();
                return XPathUtils.getIdByIndex(ComponentFunctionUtils.getIdIndex().apply(input) + 1);
            }
        };
    }

}
