package com.ihg.automation.selenium.gwt.components;

import com.ihg.automation.selenium.gwt.components.input.InputBase;

public class TextArea extends InputBase
{

    private static final String TYPE = "Text Area";
    public static final String TEXT_AREA = ".//textarea";

    public TextArea(Component parent, String description)
    {
        super(parent, TEXT_AREA, TYPE, description);
    }

}
