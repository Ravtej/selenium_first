package com.ihg.automation.selenium.gwt.components.grid;

public interface CellsText
{
    public String getText();
}
