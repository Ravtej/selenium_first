package com.ihg.automation.selenium.gwt.components.label;

import com.ihg.automation.selenium.gwt.components.Component;
import com.ihg.automation.selenium.gwt.components.utils.ByText;

public class SecurityLabel extends Label
{
    protected static final String SECURITY_LABEL_PATTERN = ".//td[*[child::*[{0}]]]";

    public SecurityLabel(Component parent, String label)
    {
        super(parent, ByText.exact(label), SECURITY_LABEL_PATTERN);
    }
}
