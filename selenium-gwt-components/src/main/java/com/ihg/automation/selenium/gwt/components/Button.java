package com.ihg.automation.selenium.gwt.components;

import static com.ihg.automation.selenium.gwt.components.utils.FindStepUtils.byXpath;
import static com.ihg.automation.selenium.gwt.components.utils.FindStepUtils.byXpathWithWait;

import java.text.MessageFormat;

import org.apache.commons.lang3.StringUtils;

import com.ihg.automation.selenium.gwt.components.utils.ByText;

public class Button extends Component
{
    private static final String TYPE = "Button";
    private static final String BUTTON_PATTERN = ".//button[{0}]";

    public static final String SAVE = "Save";
    public static final String CANCEL = "Cancel";
    public static final String SEARCH = "Search";
    public static final String CREATE = "Create";
    public static final String CLOSE = "Close";
    public static final String CLOSE_WITHOUT_CHANGES = "Close without changes";
    public static final String YES = "Yes";
    public static final String NO = "No";
    public static final String DONE = "Done";
    public static final String ENROLL_ANOTHER = "Enroll Another";
    public static final String SUBMIT = "Submit";
    public static final String EDIT = "Edit";
    public static final String REMOVE_ACCOUNT = "Remove Account";
    public static final String REINSTATE_ACCOUNT = "Reinstate Account";
    public static final String CLEAR = "Clear";
    public static final String CLEAR_CRITERIA = "Clear Criteria";
    public static final String NEW_SEARCH = "New Search";
    public static final String BACK_TO_RESULTS = "Back To Results";
    public static final String REGISTER = "Register";
    public static final String CREATE_DEPOSIT = "Create Deposit";
    public static final String END_EDIT_SESSION = "End Edit Session";

    public Button(Component parent, String text, boolean isWait)
    {
        super(parent, TYPE, text, byXpath(MessageFormat.format(BUTTON_PATTERN, ByText.exact(text)), isWait));
    }

    public Button(Component parent, ByText text)
    {
        super(parent, TYPE, text.getText(), byXpath(MessageFormat.format(BUTTON_PATTERN, text), true));
    }

    public Button(Component parent, String description, int index, ComponentColumnPosition position)
    {
        super(parent, TYPE, description, byXpathWithWait(Axis.DESCENDANT.add(position.getXpath(index), "/button")));
    }

    public Button(Component parent, String text)
    {
        this(parent, text, true);
    }

    @Override
    public boolean isEnabled()
    {
        String disabled = getElement().getAttribute("aria-disabled");
        return !StringUtils.equals(disabled, "true");
    }
}
