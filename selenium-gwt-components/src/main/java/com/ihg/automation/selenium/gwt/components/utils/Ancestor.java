package com.ihg.automation.selenium.gwt.components.utils;

import org.apache.commons.lang3.StringUtils;

public class Ancestor
{
    private String parentTag;
    private String attributes;
    private int maxAncestorLevel;

    private Ancestor(Builder builder)
    {
        this.parentTag = builder.parentTag;
        this.attributes = builder.attributes;
        this.maxAncestorLevel = builder.maxAncestorLevel;
    }

    public String getXpath()
    {
        return parentTag + XPathUtils.wrapInSquareBrackets(attributes);
    }

    public int getMaxAncestorLevel()
    {
        return maxAncestorLevel;
    }

    public static class Builder
    {
        private String parentTag = "div";
        private String attributes = StringUtils.EMPTY;
        private int maxAncestorLevel = 5;

        public Builder parentTag(String parentTag)
        {
            this.parentTag = parentTag;
            return this;
        }

        public Builder attributes(String attributes)
        {
            this.attributes = attributes;
            return this;
        }

        public Builder maxLevel(int level)
        {
            this.maxAncestorLevel = level;
            return this;
        }

        public Ancestor build()
        {
            return new Ancestor(this);
        }
    }
}
