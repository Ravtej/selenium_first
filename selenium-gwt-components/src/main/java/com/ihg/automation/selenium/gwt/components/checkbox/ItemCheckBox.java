package com.ihg.automation.selenium.gwt.components.checkbox;

import java.text.MessageFormat;

import org.apache.commons.lang3.StringUtils;

import com.ihg.automation.selenium.gwt.components.Component;
import com.ihg.automation.selenium.gwt.components.utils.ByText;

public class ItemCheckBox extends CheckBoxBase
{
    private static final String TYPE = "Check Box";
    private static final String CHECK_BOX_XPATH = ".//*[contains(@class, ''x-menu-check-item'') and {0}]";

    public ItemCheckBox(Component parent, ByText label)
    {
        super(parent, MessageFormat.format(CHECK_BOX_XPATH, label), TYPE, label.getText());
    }

    @Override
    public boolean isSelected()
    {
        return StringUtils.contains(getClassAttribute(), "x-menu-checked");
    }

    @Override
    public boolean isEnabled()
    {
        return !StringUtils.contains(getClassAttribute(), "x-item-disabled");
    }
}
