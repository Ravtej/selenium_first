package com.ihg.automation.selenium.gwt.components.checkbox;

import java.text.MessageFormat;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;

import com.ihg.automation.selenium.gwt.components.Component;
import com.ihg.automation.selenium.gwt.components.Selectable;
import com.ihg.automation.selenium.gwt.components.utils.FindStepUtils;

public abstract class CheckBoxBase extends Component implements Selectable
{
    private static final Logger logger = LoggerFactory.getLogger(CheckBoxBase.class);

    public CheckBoxBase(Component parent, String xpath, String type, String name)
    {
        super(parent, type, name, FindStepUtils.byXpathWithWait(xpath));
    }

    public void check(Boolean status)
    {
        if (null == status)
        {
            return;
        }

        if (isSelected() != status)
        {
            click();
            Assert.assertEquals(isSelected(), status.booleanValue(),
                    MessageFormat.format("{0} check-box status must be changed", getDescription()));
        }
        else
        {
            logger.warn("Element {} has already [isSelected={}]. Nothing to do.", getDescription(), status);
        }
    }

    public void check()
    {
        check(true);
    }

    public void uncheck()
    {
        check(false);
    }
}
