package com.ihg.automation.selenium.html.components;

import static java.text.MessageFormat.format;

import org.openqa.selenium.support.ui.Select;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ihg.automation.selenium.gwt.components.Component;
import com.ihg.automation.selenium.gwt.components.input.InputBase;

public class HtmlSelect extends InputBase
{
    private static final Logger logger = LoggerFactory.getLogger(HtmlSelect.class);

    private static final String TYPE = "Html Select";

    public HtmlSelect(Component parent, String name)
    {
        super(parent, format(".//select[@name=''{0}'']", name), TYPE, name);
    }

    public void select(String text)
    {
        logger.debug("Select [{}] in {}", text, getDescription());
        new Select(getElement()).selectByVisibleText(text);
    }
}
