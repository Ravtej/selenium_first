package com.ihg.automation.selenium.gwt.components.input;

import org.joda.time.LocalDate;

import com.ihg.automation.selenium.gwt.components.Component;
import com.ihg.automation.selenium.gwt.components.HavingDefaultValue;
import com.ihg.automation.selenium.gwt.components.label.Label;

public class DateInput extends InputBase implements HavingDefaultValue
{
    private static final String DEFAULT_DATE_FORMAT = "ddMMMyy";
    private static final String TYPE = "Date Input";
    private String format;

    public DateInput(Label parent)
    {
        super(parent, TYPE, parent.getName());
    }

    public DateInput(Component parent, InputXpath xpath, String description)
    {
        super(parent, xpath, TYPE, description);
    }

    public String getFormat()
    {
        return (format == null) ? DEFAULT_DATE_FORMAT : format;
    }

    public void setFormat(String format)
    {
        this.format = format;
    }

    public void type(LocalDate date)
    {
        if (date == null)
        {
            return;
        }
        super.type(date.toString(getFormat()));
    }

    @Override
    public String getDefaultValue()
    {
        return getFormat();
    }
}
