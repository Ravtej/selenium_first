package com.ihg.automation.selenium.gwt.components.expander;

public enum ExpanderStatus
{
    EXPANDED, COLLAPSED, UNKNOWN
}
