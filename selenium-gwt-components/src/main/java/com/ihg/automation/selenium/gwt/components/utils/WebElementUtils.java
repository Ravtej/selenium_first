package com.ihg.automation.selenium.gwt.components.utils;

import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class WebElementUtils
{
    private static final Logger logger = LoggerFactory.getLogger(WebElementUtils.class);

    private static final String STALE_ELEMENT_EXCEPTION_MSG = "Catch 'Element is no longer attached to the DOM' exception. Make assumption {} is not longer {}";
    private static final String STALE_ELEMENT_EXCEPTION_MSG_SHORT = "Catch 'Element is no longer attached to the DOM' exception for {}";

    public static boolean isEnabled(WebElement element, String description)
    {
        if (element == null)
        {
            return false;
        }
        try
        {
            return element.isEnabled();
        }
        catch (StaleElementReferenceException e)
        {
            logger.debug(STALE_ELEMENT_EXCEPTION_MSG, description, "enabled");
            return false;
        }
    }

    public static boolean isDisplayed(WebElement element, String description)
    {
        if (element == null)
        {
            return false;
        }
        try
        {
            return element.isDisplayed();
        }
        catch (StaleElementReferenceException e)
        {
            logger.debug(STALE_ELEMENT_EXCEPTION_MSG, description, "visible/displayed");
            return false;
        }
    }

    public static String getText(WebElement element, String description)
    {
        if (element == null)
        {
            return null;
        }
        try
        {
            return element.getText().trim();
        }
        catch (StaleElementReferenceException e)
        {
            logger.debug(STALE_ELEMENT_EXCEPTION_MSG_SHORT, description);
            return null;
        }
    }
}
