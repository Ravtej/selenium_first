package com.ihg.automation.selenium.gwt.components.input;

import static com.ihg.automation.selenium.gwt.components.utils.FindStepUtils.byXpathWithWait;

import org.openqa.selenium.Keys;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ihg.automation.selenium.gwt.components.Component;
import com.ihg.automation.selenium.gwt.components.MultiModeComponent;
import com.ihg.automation.selenium.gwt.components.Viewable;
import com.ihg.automation.selenium.gwt.components.label.Label;
import com.ihg.automation.selenium.gwt.components.label.LabelBase;
import com.ihg.automation.selenium.gwt.components.utils.WaitUtils;

public abstract class InputBase extends Component implements MultiModeComponent, Viewable
{
    private boolean isHiddenField = false;

    private static final Logger logger = LoggerFactory.getLogger(InputBase.class);

    private static final String INPUT = ".//input";

    public InputBase(Component parent, String type, String description)
    {
        super(parent, type, description, byXpathWithWait(INPUT));
    }

    public InputBase(Component parent, InputXpath xpath, String type, String description)
    {
        super(getParent(parent, xpath), type + xpath.getTypeExt(), description,
                byXpathWithWait(xpath.getFormattedXpath()));
    }

    public InputBase(Component parent, String xpath, String type, String description)
    {
        super(parent, type, description, byXpathWithWait(xpath));
    }

    private static Component getParent(Component parent, InputXpath xpath)
    {

        if (xpath.getPosition() != null && xpath.getPosition().isUseInitialLabel() && (parent instanceof Label))
        {
            return ((Label) parent).getInitialLabel();
        }

        return parent;
    }

    public void setIsHiddenField(boolean isHiddenField)
    {
        this.isHiddenField = isHiddenField;
    }

    public void type(String text)
    {
        String maskedText = text;

        if (isHiddenField)
        {
            maskedText = "****";
        }

        if (text == null)
        {
            return;
        }

        if (getText().equals(text))
        {
            logger.debug("{} already has [{}] text", getDescription(), maskedText);
            return;
        }

        logger.debug("Type [{}] value to [{}]", maskedText, getDescription());
        getElement().click();
        getElement().clear();
        getElement().sendKeys(text);
    }

    public void type(int amount)
    {
        type(String.valueOf(amount));
    }

    public void type(Double amount)
    {
        if (amount == null)
        {
            return;
        }

        type(String.valueOf(amount));
    }

    public void type(String text, Keys... keys)
    {
        type(text);

        if (keys == null || !isEnabled())
        {
            return;
        }

        for (Keys key : keys)
        {
            logger.debug("Send [{}] key for [{}]", key.name(), getDescription());
            getElement().sendKeys(key);

            if (key == Keys.ENTER)
            {
                new WaitUtils().waitExecutingRequest();
            }
        }
    }

    public void typeAndWait(String text)
    {
        type(text, Keys.TAB);
        new WaitUtils().waitExecutingRequest();
    }

    public void typeAndWait(int amount)
    {
        typeAndWait(String.valueOf(amount));
    }

    public void typeAndWait(Double amount)
    {
        if (amount == null)
        {
            return;
        }

        typeAndWait(String.valueOf(amount));
    }

    public void typeAndSubmit(String text)
    {
        type(text, Keys.ENTER);
    }

    public void clear()
    {
        logger.debug("Clear value for [{}]", getDescription());
        getElement().clear();
    }

    @Override
    public String getText(boolean isWait)
    {
        return getElement(isWait).getAttribute("value");
    }

    @Override
    public Component getView()
    {
        return getParent();
    }

    public LabelBase getLabel()
    {
        return (LabelBase) getParent();
    }

}
