package com.ihg.automation.selenium.gwt.components.label;

import static com.ihg.automation.selenium.gwt.components.utils.FindStepUtils.byAncestor;
import static com.ihg.automation.selenium.gwt.components.utils.FindStepUtils.byXpath;

import java.util.Arrays;

import com.ihg.automation.selenium.gwt.components.Component;
import com.ihg.automation.selenium.gwt.components.utils.Ancestor;
import com.ihg.automation.selenium.gwt.components.utils.ByText;
import com.ihg.automation.selenium.gwt.components.utils.ComponentFunctionUtils;
import com.ihg.automation.selenium.gwt.components.utils.FindStep;

public class TopLabel extends LabelBase
{
    private static final String TYPE = "Top Label";

    private static final FindStep GO_TO_ROW = byAncestor(new Ancestor.Builder().parentTag("tr").build());
    private static final FindStep GO_TO_CELL = byAncestor(new Ancestor.Builder().parentTag("td").build());
    public static final FindStep GO_TO_LABEL = byXpath("./../following-sibling::tr/td[{0}]",
            ComponentFunctionUtils.getPosition());

    public TopLabel(Component parent, ByText label)
    {
        super(parent, label, TYPE, Arrays.asList(GO_TO_ROW, GO_TO_CELL, GO_TO_LABEL));
    }

    public TopLabel(Component parent, ByText label, FindStep step)
    {
        super(parent, label, TYPE, step);
    }

    public TopLabel(Component parent, String label, FindStep step)
    {
        this(parent, ByText.exact(label), step);
    }
}
