package com.ihg.automation.selenium.gwt.components;

import org.openqa.selenium.By;

public enum Axis
{
    ROOT("//", "//"), //
    CHILD("./", "/"), //
    DESCENDANT(".//", "//"), //
    PARENT("./../", "/../");

    private final String xpath;
    private final String relativeXpath;

    private Axis(String xpath, String relativeXpath)
    {
        this.xpath = xpath;
        this.relativeXpath = relativeXpath;
    }

    public String add(String... xpathParts)
    {
        if (xpathParts == null)
        {
            return null;
        }

        StringBuffer str = new StringBuffer(xpath);

        int length = xpathParts.length;
        for (int i = 0; i < length; i++)
        {
            str.append(xpathParts[i]);

            if ((length > 1) && (i < length - 1))
                str.append(CHILD.relativeXpath);
        }

        return str.toString();
    }

    public By getXpathLocator(String... xpathParts)
    {
        return By.xpath(add(xpathParts));
    }

    public String getXpath()
    {
        return xpath;
    }

    public String getRelativeXpath()
    {
        return relativeXpath;
    }
}
