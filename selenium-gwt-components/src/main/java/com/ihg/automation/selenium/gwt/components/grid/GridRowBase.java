package com.ihg.automation.selenium.gwt.components.grid;

import com.ihg.automation.selenium.gwt.components.Component;
import com.ihg.automation.selenium.gwt.components.utils.FindStep;

public abstract class GridRowBase<C extends CellsName> extends Component
{
    public GridRowBase(Component parent, String type, String name, FindStep step)
    {
        super(parent, type, name, step);
    }

    protected abstract String getCellTypePattern();

    public GridCell getCell(int index)
    {
        return new GridCell(this, index);
    }

    public GridCell getCell(String columnName)
    {
        return new GridCell(this, columnName);
    }

    public GridCell getCell(C columnName)
    {
        return getCell(columnName.getName());
    }
}
