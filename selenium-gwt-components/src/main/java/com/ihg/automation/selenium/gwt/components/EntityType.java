package com.ihg.automation.selenium.gwt.components;

public interface EntityType
{
    public String getCode();

    public String getValue();

    public String getCodeWithValue();
}
