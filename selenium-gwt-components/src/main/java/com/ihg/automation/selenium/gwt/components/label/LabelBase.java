package com.ihg.automation.selenium.gwt.components.label;

import static com.ihg.automation.selenium.gwt.components.utils.FindStepUtils.byXpathWithWait;
import static java.text.MessageFormat.format;

import java.text.MessageFormat;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.ihg.automation.selenium.gwt.components.Component;
import com.ihg.automation.selenium.gwt.components.HavingDefaultValue;
import com.ihg.automation.selenium.gwt.components.Menu;
import com.ihg.automation.selenium.gwt.components.TextArea;
import com.ihg.automation.selenium.gwt.components.input.Input;
import com.ihg.automation.selenium.gwt.components.input.Select;
import com.ihg.automation.selenium.gwt.components.utils.ByText;
import com.ihg.automation.selenium.gwt.components.utils.FindStep;

public abstract class LabelBase extends Component implements HavingDefaultValue
{
    protected static final String LABEL_PATTERN = ".//td[{0} or child::*[{0}]]";
    private Component initialLabel;

    private LabelBase(Component parent, String type, String name, FindStep innerLabelFindStep, List<FindStep> steps)
    {
        super(parent, type, name, innerLabelFindStep);
        this.initialLabel = clone();
        addSteps(steps);
    }

    public LabelBase(Component parent, ByText label, String type, List<FindStep> steps)
    {
        this(parent, label, type, LABEL_PATTERN, steps);
    }

    public LabelBase(Component parent, ByText label, String type, String labelPattern, List<FindStep> steps)
    {
        this(parent, type, label.getText(), byXpathWithWait(format(labelPattern, label)), steps);
    }

    public LabelBase(Component parent, ByText label, String type, int position, FindStep step)
    {
        this(parent, type, MessageFormat.format("{0} (position: {1})", label.getText(), position),
                byXpathWithWait(format(LABEL_PATTERN, label), position), Arrays.asList(step));
    }

    public LabelBase(Component parent, ByText label, String type, FindStep step)
    {
        this(parent, label, type, Arrays.asList(step));
    }

    public LabelBase(Component parent, ByText label, String type, String labelPattern, FindStep step)
    {
        this(parent, label, type, labelPattern, Arrays.asList(step));
    }

    @Override
    public void flush()
    {
        super.flush();

        initialLabel.flush();
    }

    public Input getInput()
    {
        return new Input(this, getName());
    }

    public TextArea getTextArea()
    {
        return new TextArea(this, getName());
    }

    public Select getSelect()
    {
        return new Select(this, getName());
    }

    public Menu getMenu()
    {
        return new Menu(this, getName(), ".//table[contains(@class, 'x-btn x-component x-btn-noicon')]");
    }

    @Override
    public String getDefaultValue()
    {
        return "n/a";
    }

    public boolean isRequired()
    {
        Component textTag = new Component(initialLabel, "Label text", getName(), byXpathWithWait(".//*[text()]"));
        return StringUtils.contains(textTag.getClassAttribute(), "required-field");
    }

    public Component getInitialLabel()
    {
        return initialLabel;
    }
}
