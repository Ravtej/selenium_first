package com.ihg.automation.selenium.gwt.components.panel;

import java.util.List;

import com.ihg.automation.selenium.gwt.components.Component;
import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.expander.ExpanderAware;
import com.ihg.automation.selenium.gwt.components.utils.FindStep;

public abstract class PanelBase extends Container implements ExpanderAware
{
    protected PanelBase(Component parent, String type, String name, FindStep step)
    {
        super(parent, type, name, step);
    }

    protected PanelBase(Component parent, String type, String name, List<FindStep> steps)
    {
        super(parent, type, name, steps);
    }

    public void expand()
    {
        getExpander().expand();
    }

    public void collapse()
    {
        getExpander().collapse();
    }
}
