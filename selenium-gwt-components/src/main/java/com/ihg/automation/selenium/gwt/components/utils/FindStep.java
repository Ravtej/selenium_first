package com.ihg.automation.selenium.gwt.components.utils;

import org.openqa.selenium.WebElement;

import com.google.common.base.Function;

public interface FindStep extends Function<ComponentSearchMode, WebElement>
{
}
