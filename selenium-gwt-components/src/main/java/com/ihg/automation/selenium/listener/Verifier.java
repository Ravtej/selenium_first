package com.ihg.automation.selenium.listener;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public abstract class Verifier
{
    public abstract void verify(boolean checkPrecondition);

    public Verifier[] add(Verifier... verifiers)
    {
        List<Verifier> verifierList = new ArrayList<Verifier>();
        verifierList.add(this);
        verifierList.addAll(Arrays.asList(verifiers));
        Verifier[] verifierArr = new Verifier[verifierList.size()];

        return verifierList.toArray(verifierArr);
    }
}
