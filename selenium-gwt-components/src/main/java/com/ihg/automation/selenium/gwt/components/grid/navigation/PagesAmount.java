package com.ihg.automation.selenium.gwt.components.grid.navigation;

import static com.ihg.automation.selenium.gwt.components.utils.FindStepUtils.byXpathWithWait;

import java.text.MessageFormat;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.ihg.automation.selenium.gwt.components.Component;
import com.ihg.automation.selenium.runtime.SeleniumException;

public class PagesAmount extends Component
{
    public PagesAmount(Component component)
    {
        super(component, "Text", "Pages Amount", byXpathWithWait(".//td[6]"));
    }

    @Override
    public String getText()
    {
        String pageAmount = super.getText();

        Pattern positionPattern = Pattern.compile("of ([0-9]+)");
        Matcher matcher = positionPattern.matcher(pageAmount);

        if ((pageAmount != null) && (matcher.matches()))
        {
            return matcher.group(1);
        }

        throw new SeleniumException(
                MessageFormat.format("Pages amount can not be identified for {0}", getDescription()), getElement());
    }
}
