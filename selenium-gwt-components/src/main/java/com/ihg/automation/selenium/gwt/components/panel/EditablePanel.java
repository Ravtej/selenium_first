package com.ihg.automation.selenium.gwt.components.panel;

import static com.ihg.automation.selenium.gwt.components.utils.FindStepUtils.byXpath;
import static com.ihg.automation.selenium.gwt.components.utils.FindStepUtils.byXpathWithWait;
import static com.ihg.automation.selenium.gwt.components.utils.XPathUtils.getFormattedXpath;
import static java.text.MessageFormat.format;

import org.apache.commons.lang3.StringUtils;

import com.ihg.automation.selenium.gwt.components.Component;
import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.expander.ExpanderBase;
import com.ihg.automation.selenium.gwt.components.expander.ExpanderStatus;

public class EditablePanel extends PanelBase
{

    private static final String TYPE = "Editable Panel";

    public static final String GENERAL_XPATH = ".//div[contains(@class, 'editable-panel x-component')]";
    public static final String HOTEL_XPATH = ".//div[{0}]/div[contains(@class, 'editable-panel x-component')]";
    private static final String XPATH_PATTERN = ".//tr[{0}]/td/div/div[contains(@class, 'editable-panel x-component')]";

    public EditablePanel(Container parent, int index)
    {
        this(parent, index, XPATH_PATTERN);
    }

    public EditablePanel(Container parent, int index, String xpath)
    {
        super(parent, TYPE, format("{0}[{1}]", parent.getName(), index),
                byXpathWithWait(getFormattedXpath(xpath, index)));
    }

    @Override
    public Expander getExpander()
    {
        return new Expander(this);
    }

    public class Expander extends ExpanderBase
    {
        protected Expander(EditablePanel parent)
        {
            super(parent);
        }

        @Override
        public ExpanderStatus getExpanderStatus()
        {
            String classAttribute = getClickableComponent().getClassAttribute();

            if (StringUtils.contains(classAttribute, "tool-collapse"))
            {
                return ExpanderStatus.EXPANDED;
            }

            if (StringUtils.contains(classAttribute, "tool-expand"))
            {
                return ExpanderStatus.COLLAPSED;
            }

            return ExpanderStatus.UNKNOWN;
        }

        @Override
        protected Component getClickableComponent()
        {
            return new Component(this, getType(), getName(), byXpath(".//div[contains(@class, 'collapsible-tool')]"));
        }

        @Override
        public Container getExpandedArea()
        {
            return new Container(getClickableComponent(), EXPANDED_AREA_TYPE, getName(),
                    byXpath("./parent::td/following-sibling::td[1]//tr[1]/td[1]/div"));
        }

    }
}
