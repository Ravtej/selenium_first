package com.ihg.automation.selenium.gwt.components.grid.navigation;

import static com.ihg.automation.selenium.gwt.components.utils.FindStepUtils.byXpathWithWait;

import com.ihg.automation.selenium.gwt.components.Component;

public class RightPanel extends Component
{
    public RightPanel(Component component)
    {
        super(component, "Panel", "Right Panel", byXpathWithWait(".//td[@class='x-toolbar-right']//td/div"));
    }

    public RowNumber getItemsAmount()
    {
        return new RowNumber(this);
    }
}
