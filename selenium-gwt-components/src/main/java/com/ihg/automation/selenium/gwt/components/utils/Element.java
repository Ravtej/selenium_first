package com.ihg.automation.selenium.gwt.components.utils;

import org.openqa.selenium.By;

public class Element
{
    private By locator;
    private String description;

    public Element(By by, String description)
    {
        this.locator = by;
        this.description = description;
    }

    public By getLocator()
    {
        return locator;
    }

    public String getDescription()
    {
        return description;
    }

    @Override
    public String toString()
    {
        return String.format("%s (%s)", description, locator);
    }
}
