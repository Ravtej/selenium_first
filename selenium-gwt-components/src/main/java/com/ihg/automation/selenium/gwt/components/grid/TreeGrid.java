package com.ihg.automation.selenium.gwt.components.grid;

import java.util.List;

import org.apache.commons.lang3.reflect.ConstructorUtils;
import org.openqa.selenium.WebElement;

import com.ihg.automation.selenium.gwt.components.Axis;
import com.ihg.automation.selenium.gwt.components.Component;
import com.ihg.automation.selenium.runtime.SeleniumException;

public class TreeGrid<R extends TreeGridRow<R, C>, C extends CellsName> extends GridBase<R, C>
{
    private static final String TYPE = "Tree Grid";
    private static final String GRID_ROWS_RELATIVE_XPATH = "div[@class='x-grid3-body']/div[contains(@class, 'x-grid3-row')]";

    public TreeGrid(Component parent, String description)
    {
        super(parent, TYPE, description);
    }

    @Override
    protected List<WebElement> getRawRows()
    {
        return findElements(Axis.DESCENDANT.getXpathLocator(GRID_ROWS_RELATIVE_XPATH), "Rows list");
    }

    @Override
    public R getRow(int index)
    {
        checkRowIndex(index);

        try
        {
            return ConstructorUtils.invokeConstructor(rowClass, this, index);
        }
        catch (Exception e)
        {
            throw new SeleniumException("Exception during Grid Row object initialization", getElement());
        }
    }

}
