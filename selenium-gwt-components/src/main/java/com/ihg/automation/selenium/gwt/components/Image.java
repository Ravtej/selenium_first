package com.ihg.automation.selenium.gwt.components;

import static com.ihg.automation.selenium.gwt.components.utils.FindStepUtils.byXpath;

public class Image extends Component
{
    private static final String TYPE = "Image";

    public Image(Component parent)
    {
        super(parent, TYPE, parent.getName(), byXpath(".//img"));
    }
}
