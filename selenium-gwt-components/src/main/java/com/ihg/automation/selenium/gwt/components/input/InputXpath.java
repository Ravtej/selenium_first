package com.ihg.automation.selenium.gwt.components.input;

import static java.text.MessageFormat.format;

import java.text.MessageFormat;

import org.apache.commons.lang3.StringUtils;

import com.ihg.automation.selenium.gwt.components.Axis;
import com.ihg.automation.selenium.gwt.components.ComponentColumnPosition;

public class InputXpath
{
    private Axis axis;
    private String xpath;
    private String typeExt;
    private ComponentColumnPosition position;

    private InputXpath(InputXpathBuilder builder)
    {
        this.xpath = builder.xpath.toString();
        this.typeExt = builder.getTypeExtra();
        this.axis = builder.axis;
        this.position = builder.position;
    }

    public Axis getAxis()
    {
        return axis;
    }

    public String getXpath()
    {
        return xpath;
    }

    public String getFormattedXpath()
    {
        return axis.add(xpath);
    }

    public String getTypeExt()
    {
        return typeExt;
    }

    public ComponentColumnPosition getPosition()
    {
        return position;
    }

    public static class InputXpathBuilder
    {
        private Axis axis = Axis.DESCENDANT;
        private int row;
        private int column;
        private ComponentColumnPosition position;

        private StringBuilder xpath = new StringBuilder();

        public InputXpathBuilder row(int index)
        {
            this.row = index;

            return this;
        }

        public InputXpathBuilder column(int index, ComponentColumnPosition position)
        {
            this.column = index;
            this.position = position;

            return this;
        }

        public InputXpathBuilder column(int index)
        {
            return column(index, ComponentColumnPosition.SINGLE_TD);
        }

        private String getTypeExtra()
        {
            if ((row > 0) && (column > 0))
            {
                return MessageFormat.format("(row: {0}, column: {1}", row, column);
            }

            if (row > 0)
            {
                return MessageFormat.format("(row: {0})", row);
            }

            if (column > 0)
            {
                return MessageFormat.format("(column: {0})", column);
            }

            return StringUtils.EMPTY;
        }

        public InputXpath build()
        {
            if (row > 0)
            {
                axis = Axis.CHILD;
                xpath.append(format("div/*[{0}]", row));
            }

            if (column > 0)
            {

                final String positionXpath = position.getXpath(column);
                if (StringUtils.isNotEmpty(positionXpath))
                {
                    if (row > 0)
                    {
                        xpath.append(position.getAxis().getRelativeXpath());
                    }
                    else
                    {
                        axis = position.getAxis();
                    }

                    xpath.append(positionXpath);
                }

            }

            if (xpath.length() != 0)
            {
                xpath.append(Axis.DESCENDANT.getRelativeXpath());
            }

            xpath.append("input");

            return new InputXpath(this);
        }
    }
}
