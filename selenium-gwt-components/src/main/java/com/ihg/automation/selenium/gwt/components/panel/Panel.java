package com.ihg.automation.selenium.gwt.components.panel;

import static com.ihg.automation.selenium.gwt.components.utils.FindStepUtils.byAncestor;
import static com.ihg.automation.selenium.gwt.components.utils.FindStepUtils.byXpath;
import static com.ihg.automation.selenium.gwt.components.utils.FindStepUtils.byXpathWithWait;
import static com.ihg.automation.selenium.gwt.components.utils.XPathUtils.getFormattedXpath;

import java.util.Arrays;

import com.ihg.automation.selenium.gwt.components.Component;
import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.utils.Ancestor;
import com.ihg.automation.selenium.gwt.components.utils.ByText;
import com.ihg.automation.selenium.gwt.components.utils.FindStep;

public class Panel extends PanelBase
{
    private static final String TYPE = "Panel";

    private static final String PANEL_HEADER_TEXT_PATTERN = ".//span[contains(@class,'x-panel-header-text') and {0}]";
    private static final FindStep PANEL_BODY_FIND_STEP = byAncestor(new Ancestor.Builder().attributes(
            "child::div[@class='x-panel-bwrap']").build());

    public Panel(Component parent, ByText text)
    {
        super(parent, TYPE, text.getText(), Arrays.asList(
                byXpathWithWait(getFormattedXpath(PANEL_HEADER_TEXT_PATTERN, text)), PANEL_BODY_FIND_STEP));
    }

    public Panel(Component parent, ByText text, String xpath)
    {
        super(parent, TYPE, text.getText(), Arrays.asList(
                byXpathWithWait(getFormattedXpath(xpath, text)), PANEL_BODY_FIND_STEP));
    }

    public Panel(Component parent, String text)
    {
        this(parent, ByText.exact(text));
    }

    public Panel(String text)
    {
        this(null, text);
    }

    @Override
    public Expander getExpander()
    {
        return new Expander(this);
    }

    public class Expander extends PanelExpanderBase
    {
        final FindStep EXPANDER_RELATIVE_STEP = byXpath("./div[1]//div[contains(@class,'x-nodrag x-tool-toggle x-tool')]");
        final FindStep EXPANDED_AREA_STEP = byXpath("./div[2]");

        public Expander(Panel parent)
        {
            super(parent);
        }

        @Override
        protected Component getClickableComponent()
        {
            return new Component(this, getType(), getName(), EXPANDER_RELATIVE_STEP);
        }

        @Override
        public Container getExpandedArea()
        {
            return new Container(this, getType(), getName(), EXPANDED_AREA_STEP);
        }
    }

    public Container getBody()
    {
        return new Container(this, "Panel Body", getName(), byXpath(".//div[@class='x-panel-body']"));
    }
}
