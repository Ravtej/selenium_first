package com.ihg.automation.selenium.gwt.components.dialog;

import com.ihg.automation.selenium.gwt.components.Button;
import com.ihg.automation.selenium.listener.Verifier;

public class ConfirmDialog extends DialogBase
{
    public ConfirmDialog()
    {
        super(Type.CONFIRM);
    }

    public void clickNo(Verifier... verifiers)
    {
        container.getButton(Button.NO).clickAndWait(verifiers);
    }

    public void clickYes(Verifier... verifiers)
    {
        container.getButton(Button.YES).clickAndWait(verifiers);
    }

    public void close(boolean bConfirm)
    {
        if (bConfirm)
        {
            clickYes();
        }
        else
        {
            clickNo();
        }
    }
}
