package com.ihg.automation.selenium.gwt.components.checkbox;

import java.text.MessageFormat;

import com.ihg.automation.selenium.gwt.components.Component;
import com.ihg.automation.selenium.gwt.components.utils.ByText;

public class InputCheckBox extends CheckBoxBase
{
    private static final String TYPE = "Input Check Box";
    private static final String CHECK_BOX_PATTERN = ".//div[child::label[{0}]]/input[@type=''checkbox'']";

    public InputCheckBox(Component parent, ByText label)
    {
        super(parent, MessageFormat.format(CHECK_BOX_PATTERN, label), TYPE, label.getText());
    }

    @Override
    public boolean isSelected()
    {
        return getElement().isSelected();
    }
}
