package com.ihg.automation.selenium.gwt.components.dialog;

import com.ihg.automation.selenium.listener.Verifier;

public class WarningDialog extends DialogBase
{
    public static final String OK_BUTTON = "Ok";

    public WarningDialog()
    {
        super(Type.ALERT);
    }

    public void clickOk(Verifier... verifiers)
    {
        container.getButton(OK_BUTTON).clickAndWait(verifiers);
    }
}
