package com.ihg.automation.selenium.gwt.components;

public interface Viewable
{
    public Component getView();
}
