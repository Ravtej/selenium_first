package com.ihg.automation.selenium.gwt.components.grid;

public enum CellType
{
    EXPANDER, CHECKBOX, TEXT
}
