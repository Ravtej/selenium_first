package com.ihg.automation.selenium.gwt.components.utils;

import org.apache.commons.lang3.StringUtils;

public abstract class ByStringText
{
    private String text;

    protected ByStringText(String text)
    {
        this.text = text;
    }

    public abstract boolean compare(String elementText);

    public String getText()
    {
        return text;
    }

    public static ByStringText equals(String text)
    {
        return new ByEquals(text);
    }

    public static ByStringText contains(String text)
    {
        return new ByContains(text);
    }

    @Override
    public String toString()
    {
        return text;
    }

    public static class ByEquals extends ByStringText
    {
        public ByEquals(String text)
        {
            super(text);
        }

        @Override
        public boolean compare(String elementText)
        {
            return StringUtils.equals(elementText, getText());
        }
    }

    public static class ByContains extends ByStringText
    {
        public ByContains(String text)
        {
            super(text);
        }

        @Override
        public boolean compare(String elementText)
        {
            return StringUtils.contains(elementText, getText());
        }
    }
}
