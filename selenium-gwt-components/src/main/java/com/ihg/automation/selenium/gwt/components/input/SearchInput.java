package com.ihg.automation.selenium.gwt.components.input;

import static com.ihg.automation.selenium.gwt.components.utils.FindStepUtils.byXpath;

import com.ihg.automation.selenium.gwt.components.Component;
import com.ihg.automation.selenium.gwt.components.label.Label;

public class SearchInput extends InputBase
{
    private static final String TYPE = "Search Input";

    public SearchInput(Label parent)
    {
        super(parent, TYPE, parent.getName());
    }

    public SearchInput(Component parent, InputXpath xpath, String description)
    {
        super(parent, xpath, TYPE, description);
    }

    public void clickButton()
    {
        new Component(this, "Button", "Advanced Search Button", byXpath("./../following-sibling::div")).click();
    }
}
