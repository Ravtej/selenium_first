package com.ihg.automation.selenium.gwt.components.utils;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.SearchContext;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SearchContextUtils
{
    private static final Logger logger = LoggerFactory.getLogger(SearchContextUtils.class);
    private static final String STALE_ELEMENT_EXCEPTION_MSG = "Catch 'Element is no longer attached to the DOM' exception for {}";

    public static List<WebElement> findElements(SearchContext sc, By by, String description)
    {
        List<WebElement> toReturnList;
        try
        {
            toReturnList = sc.findElements(by);
        }
        catch (StaleElementReferenceException e)
        {
            logger.debug(STALE_ELEMENT_EXCEPTION_MSG, description);
            return null;
        }
        return toReturnList;
    }
}
