package com.ihg.automation.selenium.gwt.components;

import static com.ihg.automation.selenium.gwt.components.utils.FindStepUtils.byXpathWithWait;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ihg.automation.selenium.gwt.components.utils.ByText;
import com.ihg.automation.selenium.gwt.components.utils.ComponentFunction;
import com.ihg.automation.selenium.gwt.components.utils.ComponentFunctionUtils;
import com.ihg.automation.selenium.gwt.components.utils.XPathUtils;

public class Tab extends Component implements Selectable
{
    private static final Logger logger = LoggerFactory.getLogger(Tab.class);

    private static final String TYPE = "Tab";
    private static final String TAB_PATTERN = ".//li[descendant::span[contains(@class, 'x-tab-strip-text') and {0}]]";

    private Tab parentTab;

    public Tab(Component parent, String tabText)
    {
        super(parent, TYPE, tabText, byXpathWithWait(XPathUtils.getFormattedXpath(TAB_PATTERN, ByText.exact(tabText))));
    }

    public Tab(Tab parentTab, String tabText)
    {
        this(parentTab.getContainer(), tabText);
        this.parentTab = parentTab;
    }

    public TabContainer getContainer()
    {
        return new TabContainer(this);
    }

    @Override
    public void click()
    {
        if (!isSelected())
        {
            super.click();
        }
        else
        {
            logger.debug("{} is active. Nothing to do.", getDescription());
        }
    }

    public void goTo()
    {
        if (parentTab != null)
        {
            parentTab.clickAndWait();
        }

        clickAndWait();
    }

    @Override
    public boolean isSelected()
    {
        boolean result = true;
        if (parentTab != null)
        {
            result = parentTab.isSelected();
        }

        if (result)
        {
            // x-component x-tab-strip-active
            return (StringUtils.contains(getClassAttribute(), "x-tab-strip-active"));
        }

        return false;
    }

    public class TabContainer extends Container
    {
        private static final String TYPE = "Tab container";

        public TabContainer(Tab tab)
        {
            super(tab, TYPE, tab.getName(), byXpathWithWait("//div[@id=''{0}'']", getTabContainerDivId()));
        }
    }

    private static ComponentFunction<String> getTabContainerDivId()
    {
        final String TAB_ID_SEPARATOR = "__";

        return new ComponentFunction<String>()
        {
            @Override
            public String apply(Component input)
            {
                String[] splitIds = ComponentFunctionUtils.getId().apply(input).split(TAB_ID_SEPARATOR);// x-auto-142__x-auto-145
                return splitIds[splitIds.length - 1];
            }
        };
    }

}
