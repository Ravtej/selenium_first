package com.ihg.automation.selenium.gwt.components.label;

import static com.ihg.automation.selenium.gwt.components.utils.FindStepUtils.byXpathWithWait;
import static java.text.MessageFormat.format;

import com.ihg.automation.selenium.gwt.components.Axis;
import com.ihg.automation.selenium.gwt.components.Component;
import com.ihg.automation.selenium.gwt.components.ComponentColumnPosition;
import com.ihg.automation.selenium.gwt.components.Image;
import com.ihg.automation.selenium.gwt.components.MultiModeComponent;
import com.ihg.automation.selenium.gwt.components.checkbox.CheckBox;
import com.ihg.automation.selenium.gwt.components.input.Input;
import com.ihg.automation.selenium.gwt.components.input.InputXpath.InputXpathBuilder;
import com.ihg.automation.selenium.gwt.components.input.Select;
import com.ihg.automation.selenium.gwt.components.utils.ByText;
import com.ihg.automation.selenium.gwt.components.utils.FindStep;

public class Label extends LabelBase implements MultiModeComponent
{
    private static final String TYPE = "Left Label";
    private static final FindStep LABEL_CONTAINER_FIND_STEP = byXpathWithWait(
            "./following-sibling::td[not(contains(@style,'display: none'))][1]");

    public Label(Component parent, ByText label)
    {
        super(parent, label, TYPE, LABEL_CONTAINER_FIND_STEP);
    }


    public Label(Component parent, ByText label, int position)
    {
        super(parent, label, TYPE, position, LABEL_CONTAINER_FIND_STEP);
    }


    public Label(Component parent, ByText label, String labelPattern)
    {
        super(parent, label, TYPE, labelPattern, LABEL_CONTAINER_FIND_STEP);
    }


    public Input getInput(String description, int index)
    {
        return new Input(this, new InputXpathBuilder().column(index).build(), description);
    }


    public Input getInput(String description, int index, ComponentColumnPosition position)
    {
        return new Input(this, new InputXpathBuilder().column(index, position).build(), description);
    }

    public Select getSelect(String description, int index)
    {
        return new Select(this, new InputXpathBuilder().column(index).build(), description);
    }

    public Select getSelect(String description, int index, ComponentColumnPosition position)
    {
        return new Select(this, new InputXpathBuilder().column(index, position).build(), description);
    }

    public CheckBox getCheckBox()
    {
        return new CheckBox(this);
    }

    public Component getComponent(String description, int index, ComponentColumnPosition position)
    {
        return new Component(getParent(position), this.getType(), format("{0} [column: {1} ({2})]", this.getName(),
                index, description), byXpathWithWait(Axis.DESCENDANT.add(position.getXpath(index))));
    }

    public Image getImage(String description)
    {
        return new Image(getComponent(description, 2, ComponentColumnPosition.TABLE));
    }

    private Component getParent(ComponentColumnPosition position)
    {
        if (position.isUseInitialLabel())
        {
            return getInitialLabel();
        }
        return this;
    }
}
