package com.ihg.automation.selenium.gwt.components.grid;

import static java.text.MessageFormat.format;

import com.ihg.automation.selenium.gwt.components.Axis;
import com.ihg.automation.selenium.gwt.components.utils.ByText;

public class GridRowXpath
{
    private final String rowXpath;
    private final String rowType;

    private GridRowXpath(GridRowXpathBuilder builder)
    {
        this.rowXpath = builder.rowXpath.toString();
        this.rowType = builder.rowType.toString();
    }

    public String getRowXpath()
    {
        return rowXpath;
    }

    public String getRowType()
    {
        return rowType;
    }

    public String getXpath()
    {
        return Axis.DESCENDANT.add(rowXpath);
    }

    public static class GridRowXpathBuilder
    {
        protected static final String GRID_ROW_PREFIX_XPATH = "div[contains(@class, 'x-grid3-body')]/div[contains(@class, 'x-grid3-row')";

        private static final String CELL_TYPE_PATTERN = "cell({0}, {1})";
        private static final String CELL_XPATH_PATTERN = " and descendant::td[contains(@class, ''{0}{1}'') and {2}]";

        private int index;
        private StringBuilder rowXpath = new StringBuilder();
        private StringBuilder rowType = new StringBuilder();
        private boolean isType = false;

        public GridRowXpathBuilder()
        {
            rowXpath.append(GRID_ROW_PREFIX_XPATH);
            rowType.append("Grid Row [");
        }

        public GridRowXpathBuilder index(int index)
        {
            this.index = index;

            addDelimiter();
            rowType.append("index: ").append(index);

            return this;
        }

        public GridRowXpathBuilder cell(String name, ByText text)
        {

            addDelimiter();

            rowType.append(format(CELL_TYPE_PATTERN, name, text.getText()));
            rowXpath.append(format(CELL_XPATH_PATTERN, GridCell.CELL_NAME_PREFIX, name, text));

            return this;
        }

        public GridRowXpathBuilder cell(String name, String exactText)
        {
            return cell(name, ByText.exactSelf(exactText));
        }

        public GridRowXpathBuilder cell(CellsName name, String text)
        {
            return cell(name.getName(), text);
        }

        public GridRowXpathBuilder cell(CellsName name, CellsText text)
        {
            return cell(name, text.getText());
        }

        public GridRowXpathBuilder cell(CellsName name, ByText text)
        {
            return cell(name.getName(), text);
        }

        private void addDelimiter()
        {
            if (isType)
            {
                rowType.append(" , ");
            }
            else
            {
                isType = true;
            }
        }

        public GridRowXpath build()
        {
            rowXpath.append("]");
            rowType.append("]");

            if (index > 0)
            {
                rowXpath.append("[").append(index).append("]");
            }

            return new GridRowXpath(this);
        }
    }

}
