package com.ihg.automation.selenium.gwt.components.grid;

import static com.ihg.automation.selenium.gwt.components.utils.FindStepUtils.byXpathWithWait;

import com.ihg.automation.selenium.gwt.components.Button;
import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.CustomContainerBase;
import com.ihg.automation.selenium.listener.Verifier;

public class RowEditor extends CustomContainerBase
{
    public RowEditor(GridRow row)
    {
        super(new Container("Panel", String.format("Row Editor for %s", row.getDescription()),
                byXpathWithWait(".//div[contains(@class, 'x-row-editor x-small-editor x-component')]")));
    }

    public Button getCancel()
    {
        return new Button(container, "Cancel");
    }

    public void clickCancel(Verifier... verifiers)
    {
        getCancel().clickAndWait(verifiers);
    }

    public Button getSave()
    {
        return new Button(container, "Save");
    }

    public void clickSave(Verifier... verifiers)
    {
        getSave().clickAndWait(verifiers);
    }
}
