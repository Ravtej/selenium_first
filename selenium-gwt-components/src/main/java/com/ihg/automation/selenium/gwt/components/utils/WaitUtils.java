package com.ihg.automation.selenium.gwt.components.utils;

import static com.ihg.automation.selenium.Settings.DEFAULT_EXECUTE_REQUEST_TIMEOUT;
import static com.ihg.automation.selenium.Settings.DEFAULT_INTERVAL;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.ihg.automation.selenium.listener.Verifier;
import com.ihg.automation.selenium.listener.VerifierUtils;
import com.ihg.automation.selenium.runtime.Selenium;

public class WaitUtils
{
    private static final Element LOADING = new Element(By.xpath("//div[@class='indicator']"), "Loading indicator ...");
    private static final Element PLEASE_WAIT = new Element(By.xpath("//span[text()='Please wait...']"),
            "Please wait span");
    private static final Element MASKER = new Element(By.xpath("//div[contains(@class, 'x-masked')]"), "Masked div(s)");
    private static final List<Element> EXECUTION_REQUEST_ELEMENT_LIST = new ArrayList<>(
            Arrays.asList(LOADING, PLEASE_WAIT, MASKER));

    public void waitExecutingRequest(Verifier... verifiers)
    {
        waitExecutingRequest(DEFAULT_EXECUTE_REQUEST_TIMEOUT, DEFAULT_INTERVAL, verifiers);
    }

    public void waitExecutingRequest(long timeOutInSeconds, long intervalInMilliseconds, Verifier... verifiers)
    {
        WebDriverWait wait = (new WebDriverWait(Selenium.getInstance().getDriver(), timeOutInSeconds,
                intervalInMilliseconds));
        wait.until(Conditions.pageLoad());
        wait.until(Conditions.elementsNotFound(EXECUTION_REQUEST_ELEMENT_LIST, verifiers));

        VerifierUtils.verify(false, verifiers);
    }
}
