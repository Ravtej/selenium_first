package com.ihg.automation.selenium.gwt.components.grid;

import com.ihg.automation.selenium.gwt.components.utils.FindStep;
import com.ihg.automation.selenium.gwt.components.utils.FindStepUtils;

public class GridHeader<T extends CellsName> extends GridRowBase<T>
{
    private static final String TYPE = "Grid Header";

    private static final FindStep GRID_HEADER_FIND_STEP = FindStepUtils
            .byXpath(".//div[@class='x-grid3-header' or @class='x-grid3-hd-row']");

    protected GridHeader(GridBase parent)
    {
        super(parent, TYPE, parent.getName(), GRID_HEADER_FIND_STEP);
    }

    @Override
    protected String getCellTypePattern()
    {
        return "Header Cell [{0}]";
    }
}
