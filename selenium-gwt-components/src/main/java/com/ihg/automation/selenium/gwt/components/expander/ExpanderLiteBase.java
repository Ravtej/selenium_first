package com.ihg.automation.selenium.gwt.components.expander;

import static com.ihg.automation.selenium.Settings.DEFAULT_INTERVAL;
import static com.ihg.automation.selenium.Settings.DEFAULT_TIMEOUT;

import java.text.MessageFormat;

import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ihg.automation.selenium.gwt.components.Component;
import com.ihg.automation.selenium.gwt.components.utils.Conditions;
import com.ihg.automation.selenium.gwt.components.utils.FindStepUtils;
import com.ihg.automation.selenium.runtime.Selenium;
import com.ihg.automation.selenium.runtime.SeleniumException;

public abstract class ExpanderLiteBase extends Component
{
    private static final Logger logger = LoggerFactory.getLogger(ExpanderLiteBase.class);

    private static final String EXPANDER_TYPE = "{0} Expander";

    protected ExpanderLiteBase(Component parent)
    {
        super(parent, MessageFormat.format(EXPANDER_TYPE, parent.getType()), parent.getName(),
                FindStepUtils.byXpath(null));
    }

    protected abstract Component getClickableComponent();

    public abstract ExpanderStatus getExpanderStatus();

    private void expand(ExpanderStatus status)
    {
        ExpanderStatus actualStatus = getExpanderStatus();

        if (actualStatus == ExpanderStatus.UNKNOWN)
        {
            throw new SeleniumException(
                    MessageFormat.format("Expander status can not be identified for {0}", getDescription()),
                    getElement());
        }

        if (actualStatus != status)
        {
            clickAndWait();

            new WebDriverWait(Selenium.getInstance().getDriver(), DEFAULT_TIMEOUT, DEFAULT_INTERVAL)
                    .until(Conditions.expanderStatus(this, status));

            logger.debug("Element {} is successfully [{}]", getDescription(), status.toString());
        }
        else
        {
            logger.warn("Element {} has already [{}]. Nothing to do.", getDescription(), status.toString());
        }
    }

    public void expand()
    {
        expand(ExpanderStatus.EXPANDED);
    }

    public void collapse()
    {
        expand(ExpanderStatus.COLLAPSED);
    }

    @Override
    public void click()
    {
        getClickableComponent().click();
    }

}
