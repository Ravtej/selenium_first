package com.ihg.automation.selenium.gwt.components.expander;

public interface ExpanderAware
{
    public ExpanderBase getExpander();
}
