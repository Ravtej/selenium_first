package com.ihg.automation.selenium.gwt.components.utils;

import com.google.common.base.Function;
import com.ihg.automation.selenium.gwt.components.Component;

public interface ComponentFunction<T> extends Function<Component, T>
{
}
