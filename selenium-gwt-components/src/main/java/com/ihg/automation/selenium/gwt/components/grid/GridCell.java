package com.ihg.automation.selenium.gwt.components.grid;

import static com.ihg.automation.selenium.gwt.components.utils.FindStepUtils.byXpath;
import static java.text.MessageFormat.format;

import org.apache.commons.lang3.StringUtils;

import com.ihg.automation.selenium.gwt.components.Component;
import com.ihg.automation.selenium.gwt.components.HavingDefaultValue;
import com.ihg.automation.selenium.gwt.components.Image;
import com.ihg.automation.selenium.gwt.components.Link;
import com.ihg.automation.selenium.gwt.components.checkbox.CheckBox;
import com.ihg.automation.selenium.listener.Verifier;
import com.ihg.automation.selenium.runtime.SeleniumException;

public class GridCell extends Component implements HavingDefaultValue
{
    private int index = -1;
    private CellType cellType;
    private String cellName = "UNKNOWN";
    private boolean isFirstRun = true;

    protected static final String CELL_NAME_PREFIX = "x-grid3-td-";
    private static final String CELL_BY_INDEX_XPATH_PATTERN = ".//tr/td[contains(@class, ''x-grid3-cell'')][{0}]";
    private static final String CELL_BY_VALUE_XPATH_PATTERN = ".//tr/td[contains(@class, ''{0}{1}'')]";

    public GridCell(GridRowBase parent, int index)
    {
        super(parent, format(parent.getCellTypePattern(), index), parent.getName(), byXpath(format(
                CELL_BY_INDEX_XPATH_PATTERN, index)));
        this.index = index;
    }

    public GridCell(GridRowBase parent, String name)
    {
        super(parent, format(parent.getCellTypePattern(), name), parent.getName(), byXpath(format(
                CELL_BY_VALUE_XPATH_PATTERN, CELL_NAME_PREFIX, name)));
    }

    public int getIndex()
    {
        return index;
    }

    @Override
    protected void initElement()
    {
        super.initElement();

        if (isInitialized() && (isFirstRun))
        {
            isFirstRun = false;

            setCellType();
            setCellName();
        }
    }

    @Override
    public void flush()
    {
        super.flush();

        isFirstRun = true;
    }

    private void setCellType()
    {
        String classAttribute = getClassAttribute();

        if (StringUtils.contains(classAttribute, "x-grid3-td-expander"))
        {
            cellType = CellType.EXPANDER;
            return;
        }

        if (StringUtils.contains(classAttribute, "x-grid3-td-check-box"))
        {
            cellType = CellType.CHECKBOX;
            return;
        }

        cellType = CellType.TEXT;
    }

    private void setCellName()
    {
        /*
         * x-grid3-col x-grid3-cell x-grid3-td-hotelCode x-grid3-header
         * x-grid3-hd x-grid3-cell x-grid3-td-hotelCode x-grid3-header
         * x-grid3-hd x-grid3-cell x-grid3-td-expander x-grid3-col x-grid3-cell
         * x-grid3-td-expander x-grid-cell-first
         */

        String[] splittedString = getClassAttribute().split(" ");
        for (String part : splittedString)
        {
            if (StringUtils.contains(part, CELL_NAME_PREFIX))
            {
                cellName = StringUtils.removeStart(part, CELL_NAME_PREFIX);
                break;
            }
        }
    }

    public GridRowExpander asExpander()
    {
        initElement();

        if (cellType == CellType.EXPANDER)
        {
            return new GridRowExpander(this);
        }

        throw new SeleniumException(format("{0} is not EXPANDER", getDescription()), getElement());
    }

    public CheckBox asCheckBox()
    {
        initElement();

        if (cellType == CellType.CHECKBOX)
        {
            return new CheckBox(this);
        }

        throw new SeleniumException(format("{0} is not CHECK-BOX", getDescription()), getElement());
    }

    public Link asLink()
    {
        return new Link(this, getText());
    }

    public Image getImage()
    {
        return new Image(this);
    }

    @Override
    public String getText()
    {
        initElement();

        switch (cellType)
        {
        case EXPANDER:
            return asExpander().getExpanderStatus().toString();
        case CHECKBOX:
            return String.valueOf(asCheckBox().isSelected());
        default:
            return super.getText();
        }
    }

    @Override
    public void click()
    {
        initElement();

        switch (cellType)
        {
        case EXPANDER:
            asExpander().click();
        case CHECKBOX:
            asCheckBox().click();
        default:
            super.click();
        }
    }

    public void clickByLabel(Verifier... verifiers)
    {
        asLink().clickAndWait(verifiers);
    }

    @Override
    public String getDescription()
    {
        if ((cellType != null))
        {
            return super.getDescription() + format(" [type: {0}, name: {1}]", cellType, cellName);
        }

        return super.getDescription();
    }

    @Override
    public String getDefaultValue()
    {
        return StringUtils.EMPTY;
    }

}
