package com.ihg.automation.selenium.gwt.components;

import static java.text.MessageFormat.format;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebElement;

import com.ihg.automation.selenium.gwt.components.utils.FindStep;
import com.ihg.automation.selenium.gwt.components.utils.SearchContextUtils;

public abstract class CustomContainerBase implements Componentable
{
    public Container container;

    public CustomContainerBase(Container container)
    {
        this.container = container;
    }

    public CustomContainerBase()
    {
        this.container = new Container();
    }

    protected CustomContainerBase(String type, String name, FindStep step)
    {
        this(null, type, name, step);
    }

    protected CustomContainerBase(Container container, String type, String name, FindStep step)
    {
        this.container = new Container(container, type, name, step);
    }

    @Override
    public void flush()
    {
        container.flush();
    }

    @Override
    public boolean isDisplayed()
    {
        return container.isDisplayed();
    }

    @Override
    public boolean isDisplayed(boolean bWait)
    {
        return container.isDisplayed(bWait);
    }

    @Override
    public String getText()
    {
        return container.getText();
    }

    @Override
    public String getText(boolean isWait)
    {
        return container.getText(isWait);
    }

    @Override
    public String getDescription()
    {
        return container.getDescription();
    }

    @Override
    public String toString()
    {
        return getDescription();
    }

    @Override
    public WebElement getElement()
    {
        return container.getElement();
    }

    @Override
    public Component getParent()
    {
        return container.getParent();
    }

    @Override
    public <T> T getScreenshotAs(OutputType<T> target)
    {
        return container.getScreenshotAs(target);
    }

    protected List<WebElement> findElements(By by, String elementsDescription)
    {
        return SearchContextUtils.findElements(getElement(), by,
                format("{0} for {1}", elementsDescription, getDescription()));
    }
}
