package com.ihg.automation.selenium.gwt.components.checkbox;

import java.text.MessageFormat;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.ihg.automation.selenium.gwt.components.Component;
import com.ihg.automation.selenium.gwt.components.utils.ByText;
import com.ihg.automation.selenium.runtime.SeleniumException;

public class CheckBox extends CheckBoxBase
{
    private static final String TYPE = "Check Box";
    private static final String CHECK_BOX_PATTERN = ".//span[child::span[2][{0}]]/span[1]";
    public static final String CHECK_BOX_PATTERN2 = ".//tr[child::td[{0}]]/td/{1}";
    public static final String CHECK_BOX_PATTERN_LC = ".//span[parent::div[{0}]]/span[1]";
    private static final String CHECK_BOX_XPATH = ".//span[not(child::*)][1]";

    public CheckBox(Component parent, ByText label, String pattern)
    {
        super(parent, MessageFormat.format(pattern, label, CHECK_BOX_XPATH), TYPE, label.getText());
    }

    public CheckBox(Component parent, ByText label)
    {
        super(parent, MessageFormat.format(CHECK_BOX_PATTERN, label), TYPE, label.getText());
    }

    public CheckBox(Component parent)
    {
        super(parent, CHECK_BOX_XPATH, parent.getType(), parent.getName());
    }

    @Override
    public boolean isSelected()
    {
        return getBackgroundPosition().getPositionY() != 0;
    }

    @Override
    public boolean isEnabled()
    {
        return getBackgroundPosition().getPositionX() != -39;
    }

    private Position getBackgroundPosition()
    {
        String position = getCssValue("background-position");

        Pattern positionPattern = Pattern.compile("([-, 0-9]++)px ([-, 0-9]++)px");
        Matcher matcher = positionPattern.matcher(position);

        if ((position != null) && (matcher.matches()))
        {
            return new Position(Integer.parseInt(matcher.group(1)), Integer.parseInt(matcher.group(2)));
        }

        throw new SeleniumException(MessageFormat.format("Check-box status can not be identified for {0}",
                getDescription()), getElement());
    }

    public class Position
    {
        private int positionX;
        private int positionY;

        public Position(int positionX, int positionY)
        {
            this.positionX = positionX;
            this.positionY = positionY;
        }

        public int getPositionX()
        {
            return positionX;
        }

        public void setPositionX(int positionX)
        {
            this.positionX = positionX;
        }

        public int getPositionY()
        {
            return positionY;
        }

        public void setPositionY(int positionY)
        {
            this.positionY = positionY;
        }
    }
}
