package com.ihg.automation.selenium.gwt.components;

import static com.ihg.automation.selenium.gwt.components.utils.FindStepUtils.byXpath;
import static com.ihg.automation.selenium.gwt.components.utils.FindStepUtils.byXpathWithWait;

import com.ihg.automation.selenium.gwt.components.utils.ByText;
import com.ihg.automation.selenium.gwt.components.utils.FindStep;

public class APopUpWindow extends Container
{
    private static final String TYPE = "Pop Up Window";
    private static final String TEXT_BODY_TYPE = "Pop Up Text Body";

    private static final String BY_HEADER = "//div[child::div[@class='x-window-tl' and %s]]";
    private static final FindStep SKIP_HEADER_STEP = byXpathWithWait("//div[child::div[@class='x-window-tl']]");
    private static final String BY_BODY_TEXT = "//div[child::div[@class='x-window-bwrap' and contains(., '%s')]]";

    private static final FindStep BODY_TEXT_STEP = byXpath("./div[@class='x-window-bwrap']/div[1]");

    private PopUpWindow(String name, FindStep step)
    {
        super(TYPE, name, step);
    }

    protected PopUpWindow(ByText header)
    {
        this(header.getText(), byXpathWithWait(String.format(BY_HEADER, header)));
    }

    @Override
    public String getText(boolean isWait)
    {
        Component textArea = new Component(this, TEXT_BODY_TYPE, getName(), BODY_TEXT_STEP);
        return textArea.getText(isWait);
    }

    public static PopUpWindow withHeader(ByText header)
    {
        return new PopUpWindow(header);
    }

    public static PopUpWindow withHeader(String header)
    {
        return withHeader(ByText.exactSelf(header));
    }

    public static PopUpWindow withHeaderAndPosition(String header, int position)
    {
        return new PopUpWindow(header, byXpathWithWait(String.format(BY_HEADER, ByText.exactSelf(header)), position));
    }

    public static PopUpWindow withoutHeader(String name)
    {
        return new PopUpWindow(name, SKIP_HEADER_STEP);
    }

    public static PopUpWindow withBodyText(String name, String bodyText)
    {
        return new PopUpWindow(name, byXpathWithWait(String.format(BY_BODY_TEXT, bodyText)));
    }
}
