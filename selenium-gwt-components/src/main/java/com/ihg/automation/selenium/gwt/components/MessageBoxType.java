package com.ihg.automation.selenium.gwt.components;

import java.text.MessageFormat;

import com.ihg.automation.selenium.gwt.components.utils.FindStep;
import com.ihg.automation.selenium.gwt.components.utils.FindStepUtils;

public enum MessageBoxType
{
    SUCCESS("Success message box", "y-mbs"), INFO("Info message box", "y-mbi"), WARNING("Warning message box", "y-mbw"), DEFAULT(
            "Default message box", " y-mb x-component");

    private String name;
    private String classAttribute;

    MessageBoxType(String name, String classAttribute)
    {
        this.name = name;
        this.classAttribute = classAttribute;
    }

    public String getName()
    {
        return name;
    }

    private String getXpath()
    {
        return MessageFormat.format("//div[contains(@class, ''{0}'') and not(contains(@class,''x-hide-display''))]",
                classAttribute);
    }

    public FindStep getFindStep()
    {
        return FindStepUtils.byXpathWithWait(getXpath());
    }
}
