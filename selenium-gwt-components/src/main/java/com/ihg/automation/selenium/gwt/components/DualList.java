package com.ihg.automation.selenium.gwt.components;

import static com.ihg.automation.selenium.gwt.components.utils.FindStepUtils.byAncestor;
import static com.ihg.automation.selenium.gwt.components.utils.FindStepUtils.byXpath;
import static java.text.MessageFormat.format;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;

import com.ihg.automation.selenium.gwt.components.utils.Ancestor;
import com.ihg.automation.selenium.gwt.components.utils.ByText;
import com.ihg.automation.selenium.gwt.components.utils.FindStep;

public class DualList extends Component
{
    private static final Logger logger = LoggerFactory.getLogger(DualList.class);

    private static final String TYPE = "Dual List Field";

    private static final String RIGHT_ARROW_CLASS = "arrow-right x-component";
    private static final String LEFT_ARROW_CLASS = "arrow-left x-component";
    private static final String DOUBLE_LEFT_ARROW_CLASS = "arrow-double-left x-component";

    private static final String ARROW_XPATH_PATTERN = ".//div[contains(@class, ''{0}'')]";
    private static final FindStep LEFT_ARROW_STEP = byXpath(format(ARROW_XPATH_PATTERN, LEFT_ARROW_CLASS));
    private static final FindStep RIGHT_ARROW_STEP = byXpath(format(ARROW_XPATH_PATTERN, RIGHT_ARROW_CLASS));
    private static final FindStep DOUBLE_LEFT_STEP = byXpath(format(ARROW_XPATH_PATTERN, DOUBLE_LEFT_ARROW_CLASS));

    private static final String DUAL_LIST_FIELD_XPATH_PATTERN = ".//div[child::div[contains(@class, ''{0}'')] and child::div[contains(@class, ''{1}'')] and child::div[contains(@class, ''{0}'')]]";
    private static final String DUAL_LIST_FIELD_XPATH = format(DUAL_LIST_FIELD_XPATH_PATTERN, RIGHT_ARROW_CLASS,
            LEFT_ARROW_CLASS, DOUBLE_LEFT_ARROW_CLASS);

    private static final FindStep DUAL_LIST_STEP = byXpath(DUAL_LIST_FIELD_XPATH);
    private static final FindStep ROW_STEP = byAncestor(new Ancestor.Builder().parentTag("tr").build());

    private static final FindStep LEFT_PANEL_STEP = byXpath(".//td[1]/table");
    private static final FindStep RIGHT_PANEL_STEP = byXpath(".//td[3]/table");

    private List leftList, rightList;

    private Component leftArrow, rightArrow, doubleLeftArrow;

    protected DualList(Component parent, String name)
    {
        super(parent, TYPE, name, Arrays.asList(DUAL_LIST_STEP, ROW_STEP));

        leftList = new List(this, "Left List", LEFT_PANEL_STEP);
        rightList = new List(this, "Right List", RIGHT_PANEL_STEP);

        leftArrow = new Component(this, TYPE + " (Left Arrow)", getName(), LEFT_ARROW_STEP);
        rightArrow = new Component(this, TYPE + " (Right Arrow)", getName(), RIGHT_ARROW_STEP);
        doubleLeftArrow = new Component(this, TYPE + " (Double Left Arrow)", getName(), DOUBLE_LEFT_STEP);
    }

    @Override
    public void flush()
    {
        super.flush();

        leftList.flush();
        rightList.flush();

        leftArrow.flush();
        rightArrow.flush();
        doubleLeftArrow.flush();
    }

    public void removeAll()
    {
        doubleLeftArrow.click();
        Assert.assertEquals(rightList.getItemsCount(), 0,
                "All items must be removed from " + rightList.getDescription());
        logger.debug("All items are successfully removed from {} right panel", getDescription());
    }

    public void select(String text)
    {
        move(leftList, rightList, rightArrow, ByText.exactSelf(text));
    }

    public void select(ByText text)
    {
        move(leftList, rightList, rightArrow, text);
    }

    public void select(Set<String> items)
    {
        for (String item : items)
        {
            select(item);
        }
    }

    public void remove(String text)
    {
        move(rightList, leftList, leftArrow, ByText.exactSelf(text));
    }

    public void remove(ByText text)
    {
        move(rightList, leftList, leftArrow, text);
    }

    private void move(List fromList, List toList, Component arrow, ByText text)
    {
        int fromListSize = fromList.getItemsCount();
        int toListSize = toList.getItemsCount();

        ListItem fromItem, toItem;
        fromItem = fromList.getItem(text);

        fromItem.scroll().click();
        arrow.clickAndWait();

        fromItem = fromList.getItem(text);
        toItem = toList.getItem(text);

        Assert.assertEquals(fromList.getItemsCount(), fromListSize - 1, "Items count must be decremented for "
                + fromList.getDescription());
        Assert.assertEquals(toList.getItemsCount(), toListSize + 1,
                "Items count must be incremented for " + toList.getDescription());
        Assert.assertFalse(fromItem.isInitialized(),
                format("{0} mustn''t exist inside {1}", fromItem.getDescription(), fromList.getDescription()));
        Assert.assertTrue(toItem.isDisplayed(),
                format("{0} must exist inside {1}", toItem.getDescription(), fromList.getDescription()));

        logger.debug("{} is successfully moved from [{}] to [{}]",
                new Object[] { fromItem.getDescription(), fromList.getDescription(), toList.getDescription() });
    }

    public List getRightList()
    {
        return rightList;
    }

    public List getLeftList()
    {
        return leftList;
    }

    public class List extends Component
    {
        private static final String ITEMS_XPATH = ".//div[contains(@class, 'x-view-item')]";
        private static final String BOLD_ITEMS_XPATH = ".//div[contains(@class, 'x-view-item') and b]";

        public List(DualList parent, String type, FindStep step)
        {
            super(parent, format("{0} ({1})", parent.getType(), type), parent.getName(), step);
        }

        public int getItemsCount()
        {
            return getItemWebElements(ITEMS_XPATH).size();
        }

        public ListItem getItem(ByText text)
        {
            return new ListItem(this, text);
        }

        public ListItem getItem(String text)
        {
            return getItem(ByText.exactSelf(text));
        }

        private java.util.List<WebElement> getItemWebElements(String xPath)
        {
            return findElements(By.xpath(xPath), "Items list");
        }

        private java.util.List<ListItem> getItems(String xPath)
        {
            java.util.List<WebElement> elements = getItemWebElements(xPath);
            java.util.List<ListItem> items = new ArrayList<DualList.ListItem>();

            for (WebElement element : elements)
            {
                items.add(new ListItem(this, ByText.exactSelf(element.getText())));
            }

            return items;
        }

        public java.util.List<ListItem> getItems()
        {
            return getItems(ITEMS_XPATH);
        }

        public java.util.List<ListItem> getBoldItems()
        {
            return getItems(BOLD_ITEMS_XPATH);
        }

        private java.util.List<String> getItemsText(String xPath)
        {
            java.util.List<ListItem> lstItems = getItems(xPath);
            ArrayList<String> res = new ArrayList<String>();

            for (ListItem item : lstItems)
            {
                res.add(item.getText());
            }

            return res;
        }

        public java.util.List<String> getBoldItemsText()
        {
            return getItemsText(BOLD_ITEMS_XPATH);
        }

        public java.util.List<String> getItemsText()
        {
            return getItemsText(ITEMS_XPATH);
        }
    }

    public class ListItem extends Component
    {
        private static final String ITEM_XPATH_PATTERN = ".//div[contains(@class, ''x-view-item'') and {0}]";

        public ListItem(List parent, ByText text)
        {
            super(parent, "List Item", text.getText(), byXpath(format(ITEM_XPATH_PATTERN, text)));
        }
    }

}
