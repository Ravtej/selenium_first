package com.ihg.automation.selenium.gwt.components;

import static com.ihg.automation.selenium.gwt.components.utils.FindStepUtils.byXpathWithWait;

import java.text.MessageFormat;

import com.ihg.automation.selenium.gwt.components.label.Label;
import com.ihg.automation.selenium.gwt.components.utils.ByText;

public class Link extends Component
{
    private static final String TYPE = "Hyper Link";
    public static final String LINK_PATTERN = ".//span[contains(@class, ''hyper-link'') and {0}]";
    public static final String LINK_PATTERN_WITH_TEXT_NO_HYPER = ".//span[{0}]";
    public static final String LINK_XPATH_NO_TEXT = ".//span[contains(@class, 'hyper-link')]";
    public static final String LINK_XPATH_NO_HYPER_LINK_IN_CLASS = ".//span";

    public Link(Component parent, String text)
    {
        super(parent, TYPE, text, byXpathWithWait(MessageFormat.format(LINK_PATTERN, ByText.exact(text))));
    }

    public Link(Component parent, String text, String xPath)
    {
        super(parent, TYPE, text, byXpathWithWait(MessageFormat.format(xPath, ByText.exact(text))));
    }

    public Link(Label label)
    {
        this(label, LINK_XPATH_NO_TEXT);
    }

    public Link(Label label, String xPath)
    {
        super(label, TYPE, label.getName(), byXpathWithWait(xPath));
    }
}
