package com.ihg.automation.selenium.gwt.components.grid.navigation;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.ihg.automation.selenium.runtime.SeleniumException;

public class RowNumber
{
    private int pageStartRow;
    private int pageEndRow;
    private int totalRow;

    public RowNumber(RightPanel parent)
    {
        getItemsNumberMatcher(parent);
    }

    public int getPageStartRow()
    {
        return pageStartRow;
    }

    public int getPageEndRow()
    {
        return pageEndRow;
    }

    public int getTotalRow()
    {
        return totalRow;
    }

    private void getItemsNumberMatcher(RightPanel parent)
    {
        String panelText = parent.getText();

        Pattern positionPattern = Pattern.compile("Displaying ([0-9]+) - ([0-9]+) of ([0-9]+)");
        Matcher matcher = positionPattern.matcher(panelText);

        if ((panelText != null) && (matcher.matches()))
        {
            this.pageStartRow = Integer.parseInt(matcher.group(1));
            this.pageEndRow = Integer.parseInt(matcher.group(2));
            this.totalRow = Integer.parseInt(matcher.group(3));
        }

        else
        {
            throw new SeleniumException("Number of rows can not be identified for Right Navigation Panel",
                    parent.getElement());
        }
    }
}
