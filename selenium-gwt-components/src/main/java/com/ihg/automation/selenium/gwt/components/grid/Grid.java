package com.ihg.automation.selenium.gwt.components.grid;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.reflect.ConstructorUtils;
import org.openqa.selenium.WebElement;

import com.ihg.automation.selenium.gwt.components.Axis;
import com.ihg.automation.selenium.gwt.components.Component;
import com.ihg.automation.selenium.gwt.components.grid.navigation.GridNavigationPanel;
import com.ihg.automation.selenium.runtime.SeleniumException;

public abstract class Grid<R extends GridRow<C>, C extends CellsName> extends GridBase<R, C>
{
    private static final String TYPE = "Grid";

    public Grid(String description)
    {
        this(null, description);
    }

    public Grid(Component parent, String description)
    {
        super(parent, TYPE, description);
    }

    public Grid(Component parent)
    {
        this(parent, parent.getName());
    }

    @Override
    protected List<WebElement> getRawRows()
    {
        String rowsXpath = new GridRowXpath.GridRowXpathBuilder().build().getRowXpath();
        return findElements(Axis.DESCENDANT.getXpathLocator(rowsXpath), "Rows list");
    }

    protected final R getRow(GridRowXpath builder)
    {
        try
        {
            return ConstructorUtils.invokeConstructor(rowClass, this, builder);
        }
        catch (Exception e)
        {
            throw new SeleniumException("Exception during Grid Row object initialization", getElement());
        }
    }

    @Override
    public R getRow(int index)
    {
        checkRowIndex(index);
        return getRow(new GridRowXpath.GridRowXpathBuilder().index(index).build());
    }

    /** Get all visible grid rows from 1st page **/
    public List<R> getRowList()
    {
        int size = getRowCount();
        List<R> rows = new ArrayList<R>();

        for (int i = 1; i <= size; i++)
        {
            rows.add(getRow(i));
        }

        return rows;
    }

    public GridNavigationPanel getNavigationPanel()
    {
        return new GridNavigationPanel(this);
    }
}
