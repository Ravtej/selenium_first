package com.ihg.automation.selenium.gwt.components.utils;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.SearchContext;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ihg.automation.selenium.condition.SearchContextExpectedCondition;
import com.ihg.automation.selenium.gwt.components.expander.ExpanderLiteBase;
import com.ihg.automation.selenium.gwt.components.expander.ExpanderStatus;
import com.ihg.automation.selenium.listener.Verifier;
import com.ihg.automation.selenium.listener.VerifierUtils;

public class Conditions
{
    private static final Logger logger = LoggerFactory.getLogger(Conditions.class);
    private static final String MULTIPLE_ELEMENTS_FOUND = "Multiple elements ({}) are found for {} {}. {}";

    private Conditions()
    {
    }

    public static SearchContextExpectedCondition<Boolean> elementsNotFound(final List<Element> elementList,
            final Verifier... verifiers)
    {
        return new SearchContextExpectedCondition<Boolean>()
        {
            boolean result = true;

            @Override
            public Boolean apply(SearchContext context)
            {
                for (Element element : elementList)
                {
                    result = elementIsNotFoundOrInvisible(context, element.getLocator());

                    if (!result)
                    {
                        logger.debug("Wait until element(s) [{}] is visible ...", element);
                        VerifierUtils.verify(true, verifiers);
                        break;
                    }
                }

                return result;
            }

            @Override
            public String toString()
            {
                return String.format("missing element(s) %s on the page", elementList.toString());
            }
        };
    }

    public static SearchContextExpectedCondition<WebElement> elementIsFound(final By locator, final String description)
    {
        return new SearchContextExpectedCondition<WebElement>()
        {
            @Override
            public WebElement apply(SearchContext context)
            {
                List<WebElement> toReturnList = SearchContextUtils.findElements(context, locator, description);

                if (CollectionUtils.isNotEmpty(toReturnList))
                {
                    if (toReturnList.size() > 1)
                    {
                        logger.warn(MULTIPLE_ELEMENTS_FOUND, new Object[] { toReturnList.size(), description, locator,
                                "Try to find the 1st displayed element" });
                    }

                    for (WebElement element : toReturnList)
                    {
                        if (WebElementUtils.isDisplayed(element, description))
                        {
                            logger.debug("{} is successfully found by {}", description, locator);
                            return element;
                        }
                    }
                }

                logger.debug("{} is NOT found by {}", description, locator);
                return null;
            }

            @Override
            public String toString()
            {
                return String.format("presence of %s located %s", description, locator);
            }
        };
    }

    public static SearchContextExpectedCondition<WebElement> elementIsFound(final By locator, final int position,
            final String description)
    {
        return new SearchContextExpectedCondition<WebElement>()
        {
            @Override
            public WebElement apply(SearchContext context)
            {
                List<WebElement> elementList = SearchContextUtils.findElements(context, locator, description);

                if (CollectionUtils.isNotEmpty(elementList) && (elementList.size() >= position))
                {
                    logger.debug("{} is successfully found by {} (position: {})",
                            new Object[] { description, locator, position });
                    return elementList.get(position - 1);
                }

                logger.debug("{} is NOT found by {} (position: {})", new Object[] { description, locator, position });

                return null;
            }

            @Override
            public String toString()
            {
                return String.format("presence of %s located %s (position: %d)", description, locator, position);
            }
        };
    }

    public static ExpectedCondition<Boolean> expanderStatus(final ExpanderLiteBase expander,
            final ExpanderStatus status)
    {
        return new ExpectedCondition<Boolean>()
        {
            ExpanderStatus currentExpanderStatus = null;

            @Override
            public Boolean apply(WebDriver driver)
            {
                currentExpanderStatus = expander.getExpanderStatus();
                return currentExpanderStatus == status;
            }

            @Override
            public String toString()
            {
                return String.format("changing of expander status for %s. Current status is %s",
                        expander.getDescription(), currentExpanderStatus);
            }
        };
    }

    public static ExpectedCondition<Boolean> pageLoad()
    {
        return new ExpectedCondition<Boolean>()
        {
            @Override
            public Boolean apply(WebDriver driver)
            {
                logger.debug("Wait page loading (readyState=complete) ...");
                return ((JavascriptExecutor) driver).executeScript("return document.readyState").equals("complete");
            }

            @Override
            public String toString()
            {
                return "page loading (readyState=complete)";
            }
        };
    }

    private static boolean elementIsNotFoundOrInvisible(SearchContext context, By by)
    {
        List<WebElement> elementsList = SearchContextUtils.findElements(context, by, by.toString());

        if (CollectionUtils.isEmpty(elementsList))
        {
            return true;
        }

        for (WebElement webElement : elementsList)
        {
            if (WebElementUtils.isDisplayed(webElement, by.toString()))
            {
                return false;
            }
        }

        return true;
    }

}
