package com.ihg.automation.selenium.gwt.components.grid;

import static com.ihg.automation.selenium.gwt.components.utils.FindStepUtils.byXpath;

import org.apache.commons.lang3.StringUtils;

import com.ihg.automation.selenium.gwt.components.Component;
import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.expander.ExpanderBase;
import com.ihg.automation.selenium.gwt.components.expander.ExpanderStatus;

public class GridRowExpander extends ExpanderBase
{
    protected GridRowExpander(GridCell parent)
    {
        super(parent);
    }

    @Override
    protected Component getClickableComponent()
    {
        return new Component(this, getType(), getName(), byXpath(".//div[contains(@class, 'x-grid3-row-expander')]"));
    }

    @Override
    public ExpanderStatus getExpanderStatus()
    {
        Component divWithStatus = new Component(this, getType(), getName(), byXpath("./div"));
        String expandDivClass = divWithStatus.getClassAttribute();

        if (StringUtils.contains(expandDivClass, "x-grid3-row-expanded"))
        {
            return ExpanderStatus.EXPANDED;
        }

        if (StringUtils.contains(expandDivClass, "x-grid3-row-collapsed"))
        {
            return ExpanderStatus.COLLAPSED;
        }

        return ExpanderStatus.UNKNOWN;
    }

    @Override
    public Container getExpandedArea()
    {
        return new Container(this, EXPANDED_AREA_TYPE, getName(), byXpath("./../../tr[2]"));
    }
}
