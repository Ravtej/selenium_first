package com.ihg.automation.selenium.gwt.components.grid;

import static java.text.MessageFormat.format;

import java.lang.reflect.ParameterizedType;
import java.util.List;

import org.openqa.selenium.WebElement;

import com.ihg.automation.selenium.gwt.components.Component;
import com.ihg.automation.selenium.gwt.components.utils.FindStep;
import com.ihg.automation.selenium.gwt.components.utils.FindStepUtils;
import com.ihg.automation.selenium.runtime.SeleniumException;

public abstract class GridBase<R extends GridRowBase<C>, C extends CellsName> extends Component
{
    private static final FindStep GRID_FIND_STEP = FindStepUtils.byXpathWithWait(".//div[@class='x-grid3']");
    protected Class<R> rowClass;

    public GridBase(Component parent, String type, String description)
    {
        super(parent, type, description, GRID_FIND_STEP);
        this.rowClass = (Class<R>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
    }

    protected abstract List<WebElement> getRawRows();

    public int getRowCount()
    {
        return getRawRows().size();
    }

    protected void checkRowIndex(int index)
    {
        int rowCount = getRowCount();

        if (rowCount == 0)
        {
            throw new SeleniumException(format("{0} is empty", getDescription()), getElement());
        }

        if (rowCount < index)
        {
            throw new SeleniumException(format("{0} has {1} row(s) but {2} row is requested", getDescription(),
                    rowCount, index), getElement());
        }
    }

    public abstract R getRow(int index);

    public GridHeader<C> getHeader()
    {
        return new GridHeader<C>(this);
    }
}
