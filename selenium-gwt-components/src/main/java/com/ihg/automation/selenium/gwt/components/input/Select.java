package com.ihg.automation.selenium.gwt.components.input;

import static com.ihg.automation.selenium.gwt.components.utils.FindStepUtils.byXpath;
import static com.ihg.automation.selenium.gwt.components.utils.FindStepUtils.byXpathWithWait;
import static java.text.MessageFormat.format;

import java.util.ArrayList;

import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ihg.automation.selenium.gwt.components.Component;
import com.ihg.automation.selenium.gwt.components.EntityType;
import com.ihg.automation.selenium.gwt.components.HavingDefaultValue;
import com.ihg.automation.selenium.gwt.components.MessageBox;
import com.ihg.automation.selenium.gwt.components.utils.ByText;
import com.ihg.automation.selenium.gwt.components.utils.ComponentFunctionUtils;

public class Select extends InputBase implements HavingDefaultValue
{
    private static final Logger logger = LoggerFactory.getLogger(Select.class);

    private static final String TYPE = "Select";
    private static final String DEFAULT_VALUE = "Select";

    private static final String SKIP_MSG = "{} already has [{}] selected value";

    public Select(Component parent, String description)
    {
        super(parent, TYPE, description);
    }

    public Select(Component parent, InputXpath xpath, String description)
    {
        super(parent, xpath, TYPE, description);
    }

    public Select(Component parent, String xpath, String description)
    {
        super(parent, xpath, TYPE, description);
    }

    public List openElementsList()
    {
        List items = new List(this);
        if (!items.isDisplayed(false))
        {
            MessageBox.closeAll();

            ArrowImg img = new ArrowImg(this);
            img.click();
        }

        return items;
    }

    public boolean select(ByText text, boolean isWait)
    {
        if (isSkipSelection(text))
        {
            return false;
        }

        boolean result = true;
        List list = openElementsList();
        list.getItem(text).scroll().click(isWait);

        return result;
    }

    public boolean select(ByText text)
    {
        return select(text, true);
    }
    public boolean select(String text)
    {
        return select(ByText.exact(text));
    }

    public boolean select(int amount)
    {
        return select(ByText.exact(String.valueOf(amount)));
    }

    public boolean selectByCode(EntityType entity)
    {
        if (isSkipSelection(entity))
        {
            return false;
        }

        return select(ByText.startsWith(entity.getCode()));
    }

    public boolean selectByValue(EntityType entity, boolean isWait)
    {
        if (isSkipSelection(entity))
        {
            return false;
        }

        return select(ByText.startsWith(entity.getValue()), isWait);
    }

    public boolean selectByValue(EntityType entity)
    {
        return selectByValue(entity, true);
    }

    private boolean isSkipSelection(EntityType entity)
    {
        boolean result = false;
        if (entity == null)
        {
            result = true;
        }
        else if (StringUtils.equalsIgnoreCase(entity.getValue(), getText()))
        {
            result = true;
            logger.debug(SKIP_MSG, getDescription(), getText());
        }

        return result;
    }

    private boolean isSkipSelection(ByText byText)
    {
        boolean result = false;
        if ((byText == null) || (byText.getText() == null))
        {
            result = true;
        }
        else if (byText.compare(getText()))
        {
            result = true;
            logger.debug(SKIP_MSG, getDescription(), getText());
        }

        return result;
    }

    @Override
    public String getDefaultValue()
    {
        return DEFAULT_VALUE;
    }

    private class ArrowImg extends Component
    {
        private static final String IMG_XPATH = "./following-sibling::img";
        private static final String TYPE = "Arrow Image for ";

        private ArrowImg(Select parent)
        {
            super(parent, TYPE + parent.getType(), parent.getName(), byXpath(IMG_XPATH));
        }
    }

    public class List extends Component
    {
        private static final String LIST_ITEM_XPATH = ".//div[starts-with(@class, 'x-combo-list-item') and text()]";
        private static final String TYPE = "Items List for ";

        private List(Select parent)
        {
            super(parent, TYPE + parent.getType(), parent.getName(), byXpathWithWait("//div[@id=''{0}'']",
                    ComponentFunctionUtils.getAttribute("aria-owns")));
        }

        protected Item getItem(ByText text)
        {
            return new Item(this, text);
        }

        private java.util.List<WebElement> getItemWebElements()
        {
            return findElements(By.xpath(LIST_ITEM_XPATH), "Items list");
        }

        public int getItemsCount()
        {
            return getItemWebElements().size();
        }

        public java.util.List<Item> getItems()
        {
            java.util.List<Item> items = new ArrayList<Item>();

            for (WebElement element : getItemWebElements())
            {
                items.add(new Item(this, ByText.exactSelf(element.getText())));
            }

            return items;
        }

        public java.util.List<String> getItemsText()
        {
            java.util.List<String> items = new ArrayList<String>();

            for (WebElement element : getItemWebElements())
            {
                items.add(element.getText().trim());
            }

            return items;
        }

    }

    private class Item extends Component
    {
        private static final String TYPE = "Item in ";
        private final static String ITEM_DIV_PATTERN = ".//div[{0}]";

        private Item(List parent, ByText text)
        {
            super(parent, TYPE + parent.getType(), text.getText(), byXpath(format(ITEM_DIV_PATTERN, text)));
        }
    }
}
