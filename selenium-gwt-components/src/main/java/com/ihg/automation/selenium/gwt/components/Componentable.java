package com.ihg.automation.selenium.gwt.components;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebElement;

public interface Componentable
{
    WebElement getElement();

    Componentable getParent();

    boolean isDisplayed();

    boolean isDisplayed(boolean bWait);

    String getText();

    String getText(boolean isWait);

    String getDescription();

    void flush();

    <T> T getScreenshotAs(OutputType<T> target);
}
