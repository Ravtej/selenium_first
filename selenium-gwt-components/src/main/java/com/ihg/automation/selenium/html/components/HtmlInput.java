package com.ihg.automation.selenium.html.components;

import static java.text.MessageFormat.format;

import com.ihg.automation.selenium.gwt.components.Component;
import com.ihg.automation.selenium.gwt.components.input.InputBase;

public class HtmlInput extends InputBase
{
    private static final String TYPE = "Html Input";

    public HtmlInput(Component parent, String name)
    {
        super(parent, format(".//input[@name=''{0}'']", name), TYPE, name);
    }
}
