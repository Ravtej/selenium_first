package com.ihg.automation.selenium.gwt.components.utils;

import com.ihg.automation.selenium.gwt.components.Component;

/**
 * Helper class that combines
 * {@link com.ihg.automation.selenium.gwt.components.Component} object that
 * needs to be found with <code>isWait</code> searching mode.
 */
public class ComponentSearchMode
{
    private Component component;
    private boolean isWait;

    public ComponentSearchMode(Component component, boolean isWait)
    {
        this.component = component;
        this.isWait = isWait;
    }

    public Component getComponent()
    {
        return component;
    }

    public boolean isWait()
    {
        return isWait;
    }
}
