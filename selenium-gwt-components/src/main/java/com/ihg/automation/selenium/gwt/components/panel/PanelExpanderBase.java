package com.ihg.automation.selenium.gwt.components.panel;

import org.apache.commons.lang3.StringUtils;

import com.ihg.automation.selenium.gwt.components.Component;
import com.ihg.automation.selenium.gwt.components.expander.ExpanderBase;
import com.ihg.automation.selenium.gwt.components.expander.ExpanderStatus;

public abstract class PanelExpanderBase extends ExpanderBase
{
    protected PanelExpanderBase(Component parent)
    {
        super(parent);
    }

    @Override
    public ExpanderStatus getExpanderStatus()
    {
        String classAttribute = getClassAttribute();

        if (StringUtils.contains(classAttribute, "x-panel-collapsed"))
        {
            return ExpanderStatus.COLLAPSED;
        }

        return ExpanderStatus.EXPANDED;
    }
}
