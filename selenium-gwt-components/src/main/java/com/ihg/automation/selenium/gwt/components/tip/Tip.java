package com.ihg.automation.selenium.gwt.components.tip;

import com.ihg.automation.selenium.gwt.components.Component;
import com.ihg.automation.selenium.gwt.components.utils.FindStep;
import com.ihg.automation.selenium.gwt.components.utils.FindStepUtils;

public class Tip extends Component
{

    private static final String TYPE = "Tip";
    private static final String TEXT_BODY_TYPE = "Tip Text Body";
    private static final FindStep TEXT_BODY_STEP = FindStepUtils.byXpath(".//div[@class='x-tip-body ']");

    public Tip(TipType type)
    {
        super(null, TYPE, type.getName(), type.getFindStep());
    }

    @Override
    public String getText(boolean isWait)
    {
        Component textArea = new Component(this, TEXT_BODY_TYPE, getName(), TEXT_BODY_STEP);
        return textArea.getText(isWait);
    }

}
