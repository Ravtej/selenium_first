package com.ihg.automation.selenium.gwt.components.grid;

import java.text.MessageFormat;
import java.util.List;

import org.apache.commons.lang3.reflect.ConstructorUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.ihg.automation.selenium.gwt.components.Container;
import com.ihg.automation.selenium.gwt.components.CustomContainerBase;
import com.ihg.automation.selenium.gwt.components.checkbox.CheckBox;
import com.ihg.automation.selenium.gwt.components.utils.FindStepUtils;
import com.ihg.automation.selenium.runtime.SeleniumException;

public class GridRow<C extends CellsName> extends GridRowBase<C>
{
    private GridRowXpath xpath;

    protected <R extends GridRow<C>> GridRow(Grid<R, C> parent, GridRowXpath xpath)
    {
        super(parent, xpath.getRowType(), parent.getName(), FindStepUtils.byXpathWithWait(xpath.getXpath()));
        this.xpath = xpath;
    }

    @Override
    protected String getCellTypePattern()
    {
        return MessageFormat.format("Cell [{0}, {1}]", xpath.getRowType(), "{0}");
    }

    public <T extends CustomContainerBase> T expand(Class<T> expandedArea)
    {
        GridRowExpander expander = getExpander();
        expander.expand();
        try
        {
            return ConstructorUtils.invokeConstructor(expandedArea, expander.getExpandedArea());
        }
        catch (Exception e)
        {
            throw new SeleniumException("Exception during Grid Row Container object initialization", getElement());
        }
    }

    public Container expand()
    {
        GridRowExpander expander = getExpander();
        expander.expand();
        return expander.getExpandedArea();
    }

    public List<WebElement> getCells()
    {
        return findElements(By.xpath(".//tr/td[contains(@class, 'x-grid3-cell')]"), "GridCells list");
    }

    public GridRowExpander getExpander()
    {
        return getCell("expander").asExpander();
    }

    public CheckBox getCellAsCheckBox()
    {
        return getCell("check-box").asCheckBox();
    }

    public void check()
    {
        CheckBox checkBox = getCellAsCheckBox();
        checkBox.check();
    }

}
