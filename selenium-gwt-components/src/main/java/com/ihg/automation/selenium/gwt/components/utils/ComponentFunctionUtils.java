package com.ihg.automation.selenium.gwt.components.utils;

import org.apache.commons.lang3.StringUtils;

import com.ihg.automation.selenium.gwt.components.Component;

public class ComponentFunctionUtils
{
    public static ComponentFunction<Integer> getPosition()
    {
        return new ComponentFunction<Integer>()
        {
            @Override
            public Integer apply(Component input)
            {
                return input.getPosition();
            }
        };
    }

    public static ComponentFunction<String> getAttribute(final String attribute)
    {
        return new ComponentFunction<String>()
        {
            @Override
            public String apply(Component input)
            {
                return input.getAttribute(attribute);
            }
        };
    }

    public static ComponentFunction<String> getId()
    {
        return ComponentFunctionUtils.getAttribute("id");
    }

    public static ComponentFunction<Integer> getIdIndex()
    {
        return new ComponentFunction<Integer>()
        {
            @Override
            public Integer apply(Component input)
            {

                String idValue;
                int idIndex = -1;

                idValue = getId().apply(input);
                if (StringUtils.isNotEmpty(idValue))
                    idIndex = Integer.parseInt(idValue.substring(XPathUtils.ID_PATTERN.length()).trim());

                return idIndex;
            }
        };
    }

}
