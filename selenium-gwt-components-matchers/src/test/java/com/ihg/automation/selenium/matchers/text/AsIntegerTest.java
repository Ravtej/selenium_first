package com.ihg.automation.selenium.matchers.text;

import static com.ihg.automation.selenium.matchers.text.AsInteger.asInt;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsNot.not;

import org.testng.annotations.Test;

public class AsIntegerTest
{

    @Test
    public void textAsInteger()
    {
        assertThat("12,000.9", asInt(12000));
        assertThat("12,000,005.9", asInt(12000005));
        assertThat("12000", asInt(12000));
        assertThat("12000.08", asInt(12000));
    }

    @Test
    public void notAvailableText()
    {
        assertThat("n/a", asInt(0));
    }

    @Test
    public void hasNotIntegerText()
    {
        Integer nullValue = null;

        assertThat("text", not(asInt(0)));
        assertThat("text", asInt(nullValue));
    }

    @Test(expectedExceptions = AssertionError.class)
    public void textAsIntegerNotMatch()
    {
        assertThat("12,000.9", asInt(12001));
    }

}
