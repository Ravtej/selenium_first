package com.ihg.automation.selenium.matchers.text;

import static com.ihg.automation.selenium.matchers.text.AsLocalDate.asDate;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.testng.annotations.Test;

public class AsLocalDateTest
{
    final static DateTimeFormatter DATE_PATTERN = DateTimeFormat.forPattern("ddMMMyyyy");

    @Test
    public void textAsLocalDate()
    {
        assertThat("05Jun14", asDate("ddMMMyy", is(LocalDate.parse("05Jun2014", DATE_PATTERN))));
        assertThat("05Jun2014", asDate("ddMMMyy", is(LocalDate.parse("05Jun2014", DATE_PATTERN))));
        assertThat("June 5, 2014", asDate("MMMM d, YYYY", is(LocalDate.parse("05Jun2014", DATE_PATTERN))));
    }

    @Test(expectedExceptions = AssertionError.class)
    public void textAsLocalDateNotMatch()
    {
        assertThat("05Jun14", asDate("ddMMMyy", is(LocalDate.parse("06Jun2014", DATE_PATTERN))));
    }

    @Test(expectedExceptions = AssertionError.class)
    public void textAsLocalDateCompareWithNull()
    {
        LocalDate nullValue = null;
        assertThat("05Jun14", asDate("ddMMMyy", is(nullValue)));
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void nonDateTextAsLocalDate()
    {
        assertThat("text", asDate("ddMMMyy", is(LocalDate.parse("05Jun2014", DATE_PATTERN))));
    }
}
