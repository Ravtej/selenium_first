package com.ihg.automation.selenium.matchers.date;

import static com.ihg.automation.selenium.matchers.date.IsBeforeOrEqualsDate.isBeforeOrEquals;
import static org.hamcrest.MatcherAssert.assertThat;

import org.joda.time.LocalDate;
import org.testng.annotations.Test;

public class IsBeforeOrEqualsDateTest
{
    @Test
    public void nowIsEqualOrBeforeNow()
    {
        assertThat(LocalDate.now(), isBeforeOrEquals(LocalDate.now()));
    }

    @Test
    public void nowIsEqualOrBeforeTomorrow()
    {
        assertThat(LocalDate.now(), isBeforeOrEquals(LocalDate.now().plusDays(1)));
    }

    @Test(expectedExceptions = AssertionError.class)
    public void nowIsEqualOrBeforeYesterday()
    {
        assertThat(LocalDate.now(), isBeforeOrEquals(LocalDate.now().minusDays(1)));
    }
}
