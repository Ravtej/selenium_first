package com.ihg.automation.selenium.matchers.component;

import static com.ihg.automation.selenium.matchers.component.IsInvalid.isHighlightedAsInvalid;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsNot.not;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.gwt.components.Component;

public class IsInvalidTest
{
    private Component component;

    @BeforeClass
    public void beforeClass()
    {
        component = mock(Component.class);
        when(component.isInvalid()).thenReturn(true);
        when(component.getDescription()).thenReturn("Component");
    }

    @Test
    public void isInvalidTest()
    {
        assertThat(component, isHighlightedAsInvalid(true));
        assertThat(component, not(isHighlightedAsInvalid(false)));
    }

    @Test(expectedExceptions = { AssertionError.class })
    public void isInvalidExceptionTest()
    {
        assertThat(component, isHighlightedAsInvalid(false));
    }

}
