package com.ihg.automation.selenium.matchers.text;

import static com.ihg.automation.selenium.matchers.HasSameItemsAsCollection.hasSameItemsAsCollection;
import static com.ihg.automation.selenium.matchers.text.AsCollection.asCollection;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

import java.util.Arrays;
import java.util.Collection;

import org.testng.annotations.Test;

public class AsCollectionTest
{

    @Test
    public void textAsCollection()
    {
        assertThat("12, 11, 13", asCollection(is((Collection<String>) Arrays.asList("12", "11", "13"))));

        assertThat("12, 11, 13", asCollection(hasSameItemsAsCollection(Arrays.asList("12", "11", "13"))));
        assertThat("12, 11, 13", asCollection(hasSameItemsAsCollection(Arrays.asList("13", "12", "11"))));
    }

    @Test(expectedExceptions = { AssertionError.class })
    public void textAsCollectionException()
    {
        assertThat("12, 11, 13", asCollection(hasSameItemsAsCollection(Arrays.asList("12", "11", "14"))));
    }
}
