package com.ihg.automation.selenium.matchers.component;

import static com.ihg.automation.selenium.matchers.component.ComponentFactory.getComponentWithText;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static com.ihg.automation.selenium.matchers.text.AsLocalDate.asDate;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNot.not;

import org.joda.time.LocalDate;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.gwt.components.Component;

public class HasTextAsDateTest
{

    @Test
    public void hasTextAsDateTest()
    {
        Component component = getComponentWithText("05Jun14");

        assertThat(component, hasText("05Jun14"));
        assertThat(component, hasText(asDate("ddMMMyy", not(is(new LocalDate())))));
    }

}
