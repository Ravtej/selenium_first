package com.ihg.automation.selenium.matchers.component;

import static com.ihg.automation.selenium.matchers.component.IsEnabled.enabled;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsNot.not;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.gwt.components.Component;

public class IsEnabledTest
{
    private Component component;

    @BeforeClass
    public void beforeClass()
    {
        component = mock(Component.class);
        when(component.isEnabled()).thenReturn(true);
        when(component.getDescription()).thenReturn("Component");
    }

    @Test
    public void isDisplayedTest()
    {
        assertThat(component, enabled(true));
        assertThat(component, not(enabled(false)));
    }

    @Test(expectedExceptions = { AssertionError.class })
    public void isEnabledExceptionTest()
    {
        assertThat(component, enabled(false));
    }

}
