package com.ihg.automation.selenium.matchers.component;

import static com.ihg.automation.selenium.matchers.component.IsDisplayedAndEnabled.isDisplayedAndEnabled;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.testng.annotations.Test;

import com.ihg.automation.selenium.gwt.components.Component;

public class IsDisplayedEnabledTest
{

    @Test
    public void isDisplayedAndEnabledTest()
    {
        assertThat(getComponent(true, true), isDisplayedAndEnabled(true));

        assertThat(getComponent(false, true), isDisplayedAndEnabled(false));
        assertThat(getComponent(true, false), isDisplayedAndEnabled(false));
        assertThat(getComponent(false, false), isDisplayedAndEnabled(false));
    }

    @Test(expectedExceptions = { AssertionError.class })
    public void isDisplayedAndEnabledExceptionTest()
    {
        assertThat(getComponent(true, false), isDisplayedAndEnabled(true));
    }

    @Test
    public void isEnabledNotExecutedTest()
    {
        Component component = mock(Component.class);
        when(component.isDisplayed(false)).thenReturn(false);
        when(component.isDisplayed()).thenReturn(false);
        when(component.isEnabled()).thenThrow(new RuntimeException());
        when(component.getDescription()).thenReturn("Component");

        assertThat(component, isDisplayedAndEnabled(false));
    }

    private Component getComponent(boolean isDisplayed, boolean isEnabled)
    {
        Component component = mock(Component.class);
        when(component.isDisplayed(false)).thenReturn(isDisplayed);
        when(component.isDisplayed()).thenReturn(isDisplayed);
        when(component.isEnabled()).thenReturn(isEnabled);
        when(component.getDescription()).thenReturn("Component");

        return component;
    }
}
