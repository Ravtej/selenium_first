package com.ihg.automation.selenium.matchers;

import static com.ihg.automation.selenium.matchers.HasSameItemsAsCollection.hasSameItemsAsCollection;
import static java.util.Arrays.asList;
import static org.hamcrest.MatcherAssert.assertThat;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.hamcrest.Description;
import org.hamcrest.StringDescription;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.matchers.text.RegexMatcher;

public class HasSameItemsAsCollectionTest
{
    @DataProvider
    public static Object[][] matchingLists()
    {
        List<String> actualList = Arrays.asList("item1", "item2", "item3");
        List<String> duplicateItemList = Arrays.asList("item1", "item1", "item2", "item3");

        return new Object[][] { { "The same order", actualList, actualList },
                { "Duplicate item in expected", actualList, duplicateItemList },
                { "Duplicate item in actual", duplicateItemList, actualList },
                { "Mixed order", actualList, asList("item2", "item1", "item3") },
                { "Reversed order", actualList, asList("item3", "item2", "item1") } };
    }

    @Test(dataProvider = "matchingLists")
    public void hasSameItemsAsCollectionTest(String reason, java.util.List<String> actual,
            java.util.List<String> expected)
    {
        assertThat(reason, actual, hasSameItemsAsCollection(expected));
    }

    @DataProvider
    public static Object[][] mismatchingLists()
    {
        List<String> initialList = asList("item1", "item2", "item3");
        List<String> extendedList = new ArrayList<String>(initialList);
        extendedList.add("item4");

        return new Object[][] {
                { "There is deleted item", initialList, extendedList },
                { "There is new item", extendedList, initialList },
                { "There is deleted and new item", asList("item0", "item1", "item2", "item3"),
                        asList("item1", "item2", "item3", "item5") } };
    }

    @Test(expectedExceptions = AssertionError.class, dataProvider = "mismatchingLists")
    public void hasSameItemsAsCollectionExceptionTest(String reason, java.util.List<String> actual,
            java.util.List<String> expected)
    {
        assertThat(reason, actual, hasSameItemsAsCollection(expected));
    }

    @DataProvider
    public static Object[][] mismatchDescriptionList()
    {
        return new Object[][] {
                { asList("item1"), asList("item1", "item2"),
                        "\\n.*<1> item\\(s\\) in Expected but not in Actual:\\n.*\"item2\"" },
                { asList("item1", "item2", "item3"), asList("item2"),
                        "\\n.*<2> item\\(s\\) in Actual but not in Expected:\\n.*\"item1\"\\n.*\"item3\"" },
                {
                        asList("item0"),
                        asList("item1"),
                        "\\n.*<1> item\\(s\\) in Actual but not in Expected:\\n.*\"item0\"\\n.*<1> item\\(s\\) in Expected but not in Actual:\\n.*\"item1\"" } };
    }

    @Test(dataProvider = "mismatchDescriptionList")
    public void describeMismatch(java.util.List<String> actual, java.util.List<String> expected, String regexp)
    {

        HasSameItemsAsCollection matcher = new HasSameItemsAsCollection(expected);
        Description description = new StringDescription();
        matcher.describeMismatch(actual, description);

        assertThat(description.toString(), RegexMatcher.matches(regexp));
    }

}
