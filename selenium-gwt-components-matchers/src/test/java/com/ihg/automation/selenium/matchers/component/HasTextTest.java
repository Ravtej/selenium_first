package com.ihg.automation.selenium.matchers.component;

import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsNot.not;
import static org.hamcrest.core.StringContains.containsString;
import static org.hamcrest.core.StringStartsWith.startsWith;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.gwt.components.Component;

public class HasTextTest
{
    private static final String ENTERED_TEXT = "some text";
    private Component component;

    @BeforeClass
    public void beforeClass()
    {
        component = mock(Component.class);
        when(component.getText(true)).thenReturn(ENTERED_TEXT);
        when(component.getDescription()).thenReturn("Component");
    }

    @Test
    public void hasTextTest()
    {
        assertThat(component, hasText(ENTERED_TEXT));

        assertThat(component, not(hasText("privet")));
        String nullString = null;
        assertThat(component, not(hasText(nullString)));
    }

    @Test
    public void hasTextMatcherTest()
    {
        assertThat(component, hasText(containsString("me te")));
        assertThat(component, hasText(startsWith("some")));
    }

    @Test(expectedExceptions = { AssertionError.class })
    public void hasTextMatcherExceptionTest()
    {
        assertThat(component, hasText(startsWith("test")));
    }
}
