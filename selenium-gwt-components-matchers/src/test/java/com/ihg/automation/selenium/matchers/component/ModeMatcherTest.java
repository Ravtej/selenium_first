package com.ihg.automation.selenium.matchers.component;

import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static com.ihg.automation.selenium.matchers.component.ModeMatcher.inMode;
import static com.ihg.automation.selenium.matchers.component.ModeMatcher.inView;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.hamcrest.Description;
import org.hamcrest.StringDescription;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.gwt.components.Mode;
import com.ihg.automation.selenium.gwt.components.input.Input;
import com.ihg.automation.selenium.gwt.components.label.Label;

public class ModeMatcherTest
{
    Input input;
    Label label;

    ModeMatcher<Input> editMatcher = new ModeMatcher<Input>(hasText("test"), Mode.EDIT);
    ModeMatcher<Input> viewMatcher = new ModeMatcher<Input>(hasText("test"), Mode.VIEW);

    @BeforeClass
    public void beforeClass()
    {
        input = mock(Input.class);
        when(input.getDescription()).thenReturn("Input");
        when(input.getText(true)).thenReturn("edit mode text");

        label = mock(Label.class);
        when(label.getDescription()).thenReturn("Label");
        when(label.getText(true)).thenReturn("view mode text");

        when(input.getView()).thenReturn(label);
    }

    @Test
    public void hasTextInEditMode()
    {
        assertThat(input, inMode(hasText("edit mode text"), Mode.EDIT));
    }

    @Test
    public void hasTextInViewMode()
    {
        assertThat(input, inMode(hasText("view mode text"), Mode.VIEW));
        assertThat(input, inView(hasText("view mode text")));
    }

    @Test
    public void desribeToInEditMode()
    {
        Description description = new StringDescription();
        editMatcher.describeTo(description);

        assertThat(description.toString(), is("text is \"test\" in <EDIT> mode"));
    }

    @Test
    public void desribeToInViewMode()
    {
        Description description = new StringDescription();
        viewMatcher.describeTo(description);

        assertThat(description.toString(), is("text is \"test\" in <VIEW> mode"));
    }

    @Test
    public void desribeMismatchInEditMode()
    {
        Description description = new StringDescription();
        editMatcher.describeMismatch(input, description);

        assertThat(description.toString(), is("text was \"edit mode text\""));
    }

    @Test
    public void desribeMismatchInViewMode()
    {
        Description description = new StringDescription();
        viewMatcher.describeMismatch(input, description);

        assertThat(description.toString(), is("text was \"view mode text\""));
    }

}
