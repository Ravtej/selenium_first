package com.ihg.automation.selenium.matchers.component;

import static com.ihg.automation.selenium.matchers.component.IsSelected.isSelected;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsNot.not;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.gwt.components.checkbox.CheckBox;

public class IsSelectedTest
{
    private CheckBox component;

    @BeforeClass
    public void beforeClass()
    {
        component = mock(CheckBox.class);
        when(component.isSelected()).thenReturn(true);
        when(component.getDescription()).thenReturn("Component");
    }

    @Test
    public void isSelectedTest()
    {
        assertThat(component, isSelected(true));
        assertThat(component, not(isSelected(false)));
    }

    @Test(expectedExceptions = { AssertionError.class })
    public void isSelectedExceptionTest()
    {
        assertThat(component, isSelected(false));
    }

}
