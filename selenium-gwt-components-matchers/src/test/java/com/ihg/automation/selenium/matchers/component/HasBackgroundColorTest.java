package com.ihg.automation.selenium.matchers.component;

import static com.ihg.automation.selenium.matchers.component.HasBackgroundColor.hasBackgroundColor;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsNot.not;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.gwt.components.Component;

public class HasBackgroundColorTest
{
    private static final String COLOR = "#ffb0b0";
    private Component component;

    @BeforeClass
    public void beforeClass()
    {
        component = mock(Component.class);

        when(component.getCssValue("background-color")).thenReturn(COLOR);
        when(component.getDescription()).thenReturn("Component");
    }

    @Test
    public void hasBackgroundColorTest()
    {
        assertThat(component, hasBackgroundColor(COLOR));

        assertThat(component, not(hasBackgroundColor("red")));
        String nullString = null;
        assertThat(component, not(hasBackgroundColor(nullString)));
    }
}
