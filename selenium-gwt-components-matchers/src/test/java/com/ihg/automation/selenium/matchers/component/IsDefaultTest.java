package com.ihg.automation.selenium.matchers.component;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.testng.annotations.Test;

import com.ihg.automation.selenium.gwt.components.label.Label;

public class IsDefaultTest
{

    @Test
    public void hasDefaultTest()
    {
        assertThat(getComponent("n/a"), HasDefaultText.hasDefault());
    }

    @Test(expectedExceptions = { AssertionError.class })
    public void hasDefaultExceptionTest()
    {
        assertThat(getComponent("test"), HasDefaultText.hasDefault());
    }

    public static Label getComponent(String text)
    {
        Label label = mock(Label.class);
        when(label.getText()).thenReturn(text);
        when(label.getDefaultValue()).thenCallRealMethod();
        when(label.getDescription()).thenReturn("Component");

        return label;
    }
}
