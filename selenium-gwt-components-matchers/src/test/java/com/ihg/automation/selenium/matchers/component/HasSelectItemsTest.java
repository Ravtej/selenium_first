package com.ihg.automation.selenium.matchers.component;

import static org.hamcrest.CoreMatchers.sameInstance;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.hamcrest.Description;
import org.hamcrest.StringDescription;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.gwt.components.input.Select;
import com.ihg.automation.selenium.gwt.components.input.Select.List;
import com.ihg.automation.selenium.matchers.HasSameItemsAsCollectionTest;
import com.ihg.automation.selenium.matchers.text.RegexMatcher;

public class HasSelectItemsTest
{
    HasSelectItems matcher = new HasSelectItems(null);

    @Test(dataProvider = "matchingLists", dataProviderClass = HasSameItemsAsCollectionTest.class)
    public void hasSelectItems(String reason, java.util.List<String> actual, java.util.List<String> expected)
    {
        assertThat(reason, getSelect(actual), HasSelectItems.hasSelectItems(expected));
    }

    @Test(expectedExceptions = { AssertionError.class }, dataProvider = "mismatchingLists", dataProviderClass = HasSameItemsAsCollectionTest.class)
    public void hasSelectItemsException(String reason, java.util.List<String> actual, java.util.List<String> expected)
    {
        assertThat(reason, getSelect(actual), HasSelectItems.hasSelectItems(expected));
    }

    @Test(dataProvider = "mismatchDescriptionList", dataProviderClass = HasSameItemsAsCollectionTest.class)
    public void describeMismatch(java.util.List<String> actual, java.util.List<String> expected, String regexp)
    {
        HasSelectItems matcher = new HasSelectItems(expected);
        Description description = new StringDescription();
        matcher.describeMismatch(getSelect(actual), description);

        assertThat(description.toString(), RegexMatcher.matches(regexp));
    }

    @Test
    public void getComponentTest()
    {
        Select select = mock(Select.class);
        List selectList = mock(List.class);
        when(select.openElementsList()).thenReturn(selectList);

        List result = matcher.getComponent(select);

        assertThat(result, sameInstance(selectList));
    }

    @Test
    public void describeToTest()
    {
        Description description = new StringDescription();
        matcher.describeTo(description);

        assertThat(description.toString(), is("select items collections contains same items"));
    }

    private Select getSelect(java.util.List<String> actualItems)
    {
        Select select = mock(Select.class);
        List selectList = mock(List.class);
        when(select.openElementsList()).thenReturn(selectList);
        when(selectList.getItemsText()).thenReturn(actualItems);

        return select;
    }
}
