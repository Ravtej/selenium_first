package com.ihg.automation.selenium.matchers.date;

import static com.ihg.automation.selenium.matchers.date.IsAfterOrEqualsDate.isAfterOrEquals;
import static org.hamcrest.MatcherAssert.assertThat;

import org.joda.time.LocalDate;
import org.testng.annotations.Test;

public class IsAfterOrEqualsDateTest
{
    @Test
    public void nowIsEqualNow()
    {
        assertThat(LocalDate.now(), isAfterOrEquals(LocalDate.now()));
    }

    @Test(expectedExceptions = AssertionError.class)
    public void nowIsAfterOrEqualsNowPlusMonths()
    {
        assertThat(LocalDate.now(), isAfterOrEquals(LocalDate.now().plusMonths(1)));
    }

    @Test()
    public void nowIsAfterOrEqualsNowMinusMonths()
    {
        assertThat(LocalDate.now(), isAfterOrEquals(LocalDate.now().minusMonths(1)));
    }
}
