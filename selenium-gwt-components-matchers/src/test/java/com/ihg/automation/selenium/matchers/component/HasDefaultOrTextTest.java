package com.ihg.automation.selenium.matchers.component;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.testng.annotations.Test;

import com.ihg.automation.selenium.gwt.components.label.Label;

public class HasDefaultOrTextTest
{

    @Test
    public void hasDefaultOrTextTest()
    {
        assertThat(getComponent("n/a"), ComponentMatcher.hasDefaultOrText(null));
        assertThat(getComponent("text"), ComponentMatcher.hasDefaultOrText("text"));
    }

    @Test(expectedExceptions = { AssertionError.class })
    public void hasDefaultExceptionTest()
    {
        assertThat(getComponent("actual"), ComponentMatcher.hasDefaultOrText(null));
    }

    @Test(expectedExceptions = { AssertionError.class })
    public void hasTextExceptionTest()
    {
        assertThat(getComponent("actual"), ComponentMatcher.hasDefaultOrText("expected"));
    }

    public static Label getComponent(String text)
    {
        Label label = mock(Label.class);
        when(label.getText()).thenReturn(text);
        when(label.getText(true)).thenReturn(text);
        when(label.getDefaultValue()).thenCallRealMethod();
        when(label.getDescription()).thenReturn("Component");

        return label;
    }
}
