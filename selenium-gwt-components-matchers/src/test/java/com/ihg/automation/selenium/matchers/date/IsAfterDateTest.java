package com.ihg.automation.selenium.matchers.date;

import static com.ihg.automation.selenium.matchers.date.IsAfterDate.isAfter;
import static com.ihg.automation.selenium.matchers.date.IsAfterDate.isAfterToday;
import static org.hamcrest.MatcherAssert.assertThat;

import org.joda.time.DateTimeZone;
import org.joda.time.LocalDate;
import org.testng.annotations.Test;

public class IsAfterDateTest
{

    @Test
    public void nowIsAfterYesterday()
    {
        assertThat(LocalDate.now(), isAfter(LocalDate.now().minusDays(1)));
    }

    @Test
    public void tomorrowIsAfterToday()
    {
        assertThat(LocalDate.now(DateTimeZone.UTC).plusDays(1), isAfterToday());
    }

    @Test(expectedExceptions = AssertionError.class)
    public void todayIsAfterToday()
    {
        assertThat(LocalDate.now(DateTimeZone.UTC), isAfterToday());
    }

    @Test(expectedExceptions = AssertionError.class)
    public void isAfterEqualDate()
    {
        LocalDate now = LocalDate.now();
        assertThat(now, isAfter(now));
    }

    @Test(expectedExceptions = AssertionError.class)
    public void isAfterTodayEqualDate()
    {
        assertThat(LocalDate.now(DateTimeZone.UTC), isAfterToday());
    }

}
