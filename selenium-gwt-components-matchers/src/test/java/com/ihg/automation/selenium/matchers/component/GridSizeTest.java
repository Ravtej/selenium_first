package com.ihg.automation.selenium.matchers.component;

import static com.ihg.automation.selenium.matchers.component.GridSize.size;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsNot.not;
import static org.hamcrest.number.OrderingComparison.greaterThan;
import static org.hamcrest.number.OrderingComparison.lessThan;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.gwt.components.grid.Grid;

public class GridSizeTest
{
    private Grid grid;

    @BeforeClass
    public void beforeClass()
    {
        grid = mock(Grid.class);
        when(grid.getRowCount()).thenReturn(2);
        when(grid.getDescription()).thenReturn("Grid");
    }

    @Test
    public void sizeTest()
    {
        assertThat(grid, size(2));
        assertThat(grid, not(size(3)));
    }

    @Test
    public void sizeMatcherTest()
    {
        assertThat(grid, size(greaterThan(1)));
        assertThat(grid, size(lessThan(3)));
    }

    @Test(expectedExceptions = { AssertionError.class })
    public void sizeMatcherExceptionTest()
    {
        assertThat(grid, size(greaterThan(3)));
    }
}
