package com.ihg.automation.selenium.matchers.text;

import static com.ihg.automation.selenium.matchers.text.AsDouble.asDouble;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsNot.not;

import org.testng.annotations.Test;

public class AsDoubleTest
{

    @Test
    public void textAsDouble()
    {
        assertThat("12,000.90", asDouble(12000.9));
        assertThat("12,000,005.9", asDouble(12000005.9));
        assertThat("12000", asDouble(12000.0));
        assertThat("12000.08", asDouble(12000.08));
    }

    @Test
    public void notAvailableText()
    {
        assertThat("n/a", asDouble(0.0));
    }

    @Test
    public void hasNotDoubleText()
    {
        Double nullValue = null;

        assertThat("text", not(asDouble(0.0)));
        assertThat("text", asDouble(nullValue));
    }

    @Test(expectedExceptions = AssertionError.class)
    public void textAsIntegerNotMatch()
    {
        assertThat("12,000.9", asDouble(12001.08));
    }

}
