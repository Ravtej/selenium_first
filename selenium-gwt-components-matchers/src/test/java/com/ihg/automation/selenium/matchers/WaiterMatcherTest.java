package com.ihg.automation.selenium.matchers;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.concurrent.TimeUnit;

import org.testng.annotations.Test;

import com.ihg.automation.selenium.gwt.components.Component;
import com.ihg.automation.selenium.matchers.component.HasText;

public class WaiterMatcherTest
{
    @Test
    public void waitComponentTest()
    {
        Component component = mock(Component.class);
        when(component.getText(true)).thenReturn("2", "3");
        when(component.getDescription()).thenReturn("Component");
        assertThat(component, WaiterMatcher.wait(HasText.hasText("3")));
    }

    @Test(expectedExceptions = { AssertionError.class })
    public void waitComponentExceptionTest()
    {
        Component component = mock(Component.class);
        when(component.getText(true)).thenReturn("2");
        when(component.getDescription()).thenReturn("Component");
        assertThat(component, WaiterMatcher.wait(HasText.hasText("3"), TimeUnit.SECONDS.toMillis(1)));
    }
}
