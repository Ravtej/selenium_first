package com.ihg.automation.selenium.matchers.text;

import static com.ihg.automation.selenium.matchers.text.AsFormattedDouble.asFormattedDouble;
import static org.hamcrest.MatcherAssert.assertThat;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class AsFormattedDoubleTest
{
    @DataProvider
    public Object[][] valid()
    {
        return new Object[][] { { "12,000.00", "12000" }, { "12,000.00", "12000.00" }, { "12,000.00", "12,000.00" },
                { "text", "text" }, { "n/a", "n/a" } };
    }

    @Test(dataProvider = "valid")
    public void textIsMatchToFormattedDouble(String actual, String expected)
    {
        assertThat(actual, asFormattedDouble(expected));
    }

    @DataProvider
    public Object[][] validDouble()
    {
        return new Object[][] { { "12,000.00", 12000.00 }, { "12,000.00", 12000.001 }, { "12,000.01", 12000.01 },
                { "12,000.00", 12000.005 } };
    }

    @Test(dataProvider = "validDouble")
    public void textIsMatchToFormattedDouble(String actual, Double expected)
    {
        assertThat(actual, asFormattedDouble(expected));
    }

    @DataProvider
    public Object[][] invalid()
    {
        return new Object[][] { { null, "12000" }, { "12000", null }, { "", "text" }, { "text", "" },
                { "12,000.00", "12001.00" }, { "12,000.00", "12,000" } };
    }

    @Test(dataProvider = "invalid", expectedExceptions = AssertionError.class)
    public void textIsNotMatchToFormattedDouble(String actual, String expected)
    {
        assertThat(actual, asFormattedDouble(expected));
    }

    @DataProvider
    public Object[][] invalidDouble()
    {
        return new Object[][] { { "12,000.00", 12000.01 }, { "12,000.00", 12000.006 }, { "12,000.00", 12000.009 },
                { "12,000.01", 12000.02 }, { "12,000.01", null } };
    }

    @Test(dataProvider = "invalidDouble", expectedExceptions = AssertionError.class)
    public void textIsNotMatchToFormattedDouble(String actual, Double expected)
    {
        assertThat(actual, asFormattedDouble(expected));
    }

}