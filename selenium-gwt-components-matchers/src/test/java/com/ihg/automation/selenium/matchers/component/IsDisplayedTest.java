package com.ihg.automation.selenium.matchers.component;

import static com.ihg.automation.selenium.matchers.component.IsDisplayed.displayed;
import static com.ihg.automation.selenium.matchers.component.IsDisplayed.isDisplayedWithWait;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsNot.not;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.gwt.components.Component;

public class IsDisplayedTest
{
    private Component component;

    @BeforeClass
    public void beforeClass()
    {
        component = mock(Component.class);
        when(component.isDisplayed(false)).thenReturn(true);
        when(component.isDisplayed(true)).thenReturn(false);
        when(component.getDescription()).thenReturn("Component");
    }

    @Test
    public void isDisplayedTest()
    {
        assertThat(component, displayed(true));
        assertThat(component, not(displayed(false)));
    }

    @Test
    public void isDisplayedWithWaitTest()
    {
        assertThat(component, not(isDisplayedWithWait()));
    }

    @Test(expectedExceptions = { AssertionError.class })
    public void isDisplayedExceptionTest()
    {
        assertThat(component, displayed(false));
    }

    @Test(expectedExceptions = { AssertionError.class })
    public void isDisplayedWithWaitExceptionTest()
    {
        assertThat(component, isDisplayedWithWait());
    }
}
