package com.ihg.automation.selenium.matchers.component;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import com.ihg.automation.selenium.gwt.components.Component;

public class ComponentFactory
{
    public static Component getComponentWithText(String text)
    {
        Component component = mock(Component.class);
        when(component.getText()).thenReturn(text);
        when(component.getText(true)).thenReturn(text);
        when(component.getText(false)).thenReturn(text);
        when(component.getDescription()).thenReturn("Component");

        return component;
    }

}
