package com.ihg.automation.selenium.matchers.date;

import static com.ihg.automation.selenium.matchers.date.IsBeforeDate.isBefore;
import static com.ihg.automation.selenium.matchers.date.IsBeforeDate.isBeforeToday;
import static org.hamcrest.MatcherAssert.assertThat;

import org.joda.time.DateTimeZone;
import org.joda.time.LocalDate;
import org.testng.annotations.Test;

public class IsBeforeDateTest
{

    @Test
    public void nowIsBeforeTomorrow()
    {
        assertThat(LocalDate.now(), isBefore(LocalDate.now().plusDays(1)));
    }

    @Test
    public void yesterdayIsBeforeToday()
    {
        assertThat(LocalDate.now(DateTimeZone.UTC).minusDays(1), isBeforeToday());
    }

    @Test(expectedExceptions = AssertionError.class)
    public void tomorrowIsBeforeToday()
    {
        assertThat(LocalDate.now(DateTimeZone.UTC).plusDays(1), isBeforeToday());
    }

    @Test(expectedExceptions = AssertionError.class)
    public void isBeforeEqualDate()
    {
        LocalDate now = LocalDate.now();
        assertThat(now, isBefore(now));
    }

    @Test(expectedExceptions = AssertionError.class)
    public void isBeforeTodayEqualDate()
    {
        assertThat(LocalDate.now(DateTimeZone.UTC), isBeforeToday());
    }

}
