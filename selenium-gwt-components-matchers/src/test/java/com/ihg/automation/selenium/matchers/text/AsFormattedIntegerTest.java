package com.ihg.automation.selenium.matchers.text;

import static com.ihg.automation.selenium.matchers.text.AsFormattedInteger.asFormattedInt;
import static org.hamcrest.MatcherAssert.assertThat;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class AsFormattedIntegerTest
{
    @DataProvider
    public Object[][] valid()
    {
        return new Object[][] { { "12,000", "12000" }, { "12,000", "12,000" }, { "text", "text" }, { "n/a", "n/a" } };
    }

    @Test(dataProvider = "valid")
    public void textIsMatchToFormattedInteger(String actual, String expected)
    {
        assertThat(actual, asFormattedInt(expected));
    }

    @DataProvider
    public Object[][] invalid()
    {
        return new Object[][] { { null, "12000" }, { "12000", null }, { "", "text" }, { "text", "" },
                { "12,000", "12001" } };
    }

    @Test(dataProvider = "invalid", expectedExceptions = AssertionError.class)
    public void textIsNotMatchToFormattedInteger(String actual, String expected)
    {
        assertThat(actual, asFormattedInt(expected));
    }
}
