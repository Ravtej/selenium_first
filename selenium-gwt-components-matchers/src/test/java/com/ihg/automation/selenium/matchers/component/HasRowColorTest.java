package com.ihg.automation.selenium.matchers.component;

import static com.ihg.automation.selenium.matchers.component.HasRowColor.isColor;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsNot.not;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.gwt.components.grid.GridCell;
import com.ihg.automation.selenium.gwt.components.grid.GridRow;

public class HasRowColorTest
{
    private static final String COLOR = "#00aa00";
    private GridRow gridRow;

    @BeforeClass
    public void beforeClass()
    {
        gridRow = mock(GridRow.class);
        GridCell cell = mock(GridCell.class);
        WebElement cellElement = mock(WebElement.class);

        List<WebElement> cellList = new ArrayList<>();
        cellList.add(cellElement);
        cellList.add(cellElement);

        when(gridRow.getCells()).thenReturn(cellList);

        for (int cellIndex = 1; cellIndex <= gridRow.getCells().size(); cellIndex++)
        {
            when(gridRow.getCell(cellIndex)).thenReturn(cell);
            when(gridRow.getCell(cellIndex).getCssValue("color")).thenReturn(COLOR);
            when(gridRow.getDescription()).thenReturn("GridRow");
        }
    }

    @Test
    public void hasRowColorTest()
    {
        assertThat(gridRow, isColor(COLOR));

        assertThat(gridRow, not(isColor("red")));
        String nullString = null;
        assertThat(gridRow, not(isColor(nullString)));
    }
}
