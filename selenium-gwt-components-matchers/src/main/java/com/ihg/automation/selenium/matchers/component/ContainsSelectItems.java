package com.ihg.automation.selenium.matchers.component;

import java.util.Collection;

import org.hamcrest.Description;
import org.hamcrest.Factory;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;

import com.ihg.automation.selenium.gwt.components.Componentable;
import com.ihg.automation.selenium.gwt.components.input.Select;
import com.ihg.automation.selenium.matchers.ContainsSameItemsAsCollection;

public class ContainsSelectItems extends TypeSafeMatcher<Select> implements HighlightDifferent<Select.List>
{
    private ContainsSameItemsAsCollection matcher;

    protected ContainsSelectItems(Collection<String> items)
    {
        this.matcher = ContainsSameItemsAsCollection.containsSameItemsAsCollection(items);
    }

    @Override
    public Select.List getComponent(Componentable matcherComponent)
    {
        return ((Select) matcherComponent).openElementsList();
    }

    @Override
    public void describeTo(Description description)
    {
        description.appendText("select items ").appendDescriptionOf(matcher);
    }

    @Override
    protected void describeMismatchSafely(Select select, Description mismatchDescription)
    {
        matcher.describeMismatch(getComponent(select).getItemsText(), mismatchDescription);
    }

    @Override
    protected boolean matchesSafely(Select select)
    {
        return matcher.matches(getComponent(select).getItemsText());
    }

    @Factory
    public static Matcher<Select> containsSelectItems(final Collection<String> items)
    {
        return new ContainsSelectItems(items);
    }

}
