package com.ihg.automation.selenium.formatter;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;
import java.util.Locale;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

public class Converter
{
    private static final DecimalFormatSymbols DECIMAL_FORMAT_SYMBOLS = new DecimalFormatSymbols(Locale.US);
    public static final DecimalFormat INT_FORMAT = new DecimalFormat("###,##0", DECIMAL_FORMAT_SYMBOLS);
    public static final DecimalFormat DECIMAL_FORMAT = new DecimalFormat("###,##0.##", DECIMAL_FORMAT_SYMBOLS);
    public static final DecimalFormat CURRENCY_AMOUNT_DECIMAL_FORMAT = new DecimalFormat("###,##0.00",
            DECIMAL_FORMAT_SYMBOLS);
    public static final String DEFAULT_DATE_FORMAT = "ddMMMyy";

    public static Integer stringToInteger(String text)
    {
        if (StringUtils.contains(text, "n/a"))
        {
            return 0;
        }

        try
        {
            return INT_FORMAT.parse(text).intValue();
        }
        catch (ParseException e)
        {
            return null;
        }
    }

    public static String integerToString(Integer value)
    {
        try
        {
            return INT_FORMAT.format(value);
        }
        catch (IllegalArgumentException e)
        {
            return "0";
        }
    }

    public static Double stringToDouble(String text)
    {
        if (StringUtils.contains(text, "n/a"))
        {
            return 0.0;
        }

        try
        {
            return DECIMAL_FORMAT.parse(text).doubleValue();
        }
        catch (ParseException e)
        {
            return null;
        }
    }

    public static Double ignoreZeroDouble(Double value)
    {
        if (value == 0.0)
        {
            return null;
        }
        return value;
    }

    public static String doubleToString(Double value)
    {
        if (value == null)
        {
            return null;
        }
        return CURRENCY_AMOUNT_DECIMAL_FORMAT.format(value);
    }

    public static String booleanToString(Boolean bool)
    {
        if (bool == null)
        {
            return null;
        }

        if (bool)
        {
            return "Yes";
        }
        else
        {
            return "No";
        }
    }

    public static LocalDate stringToLocalDate(String text, String format)
    {
        DateTimeFormatter datePattern = DateTimeFormat.forPattern(format);
        return datePattern.parseLocalDate(text);
    }

    public static LocalDate stringToLocalDate(String text)
    {
        return stringToLocalDate(text, DEFAULT_DATE_FORMAT);
    }
}
