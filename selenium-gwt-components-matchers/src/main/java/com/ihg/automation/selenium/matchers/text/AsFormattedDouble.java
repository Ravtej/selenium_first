package com.ihg.automation.selenium.matchers.text;

import org.hamcrest.Factory;

import com.ihg.automation.selenium.formatter.StringFormatter;

public class AsFormattedDouble extends AsFormattedMatcherBase
{
    protected AsFormattedDouble(String text)
    {
        super(text, StringFormatter.asFormattedDouble(text));
    }

    protected AsFormattedDouble(Double text)
    {
        super(text, StringFormatter.asFormattedDouble(text));
    }

    @Factory
    public static AsFormattedDouble asFormattedDouble(final String value)
    {
        return new AsFormattedDouble(value);
    }

    @Factory
    public static AsFormattedDouble asFormattedDouble(final Double value)
    {
        return new AsFormattedDouble(value);
    }

}
