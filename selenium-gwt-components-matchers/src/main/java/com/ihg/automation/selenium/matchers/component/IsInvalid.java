package com.ihg.automation.selenium.matchers.component;

import static org.hamcrest.core.Is.is;

import org.hamcrest.Description;
import org.hamcrest.Factory;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;

import com.ihg.automation.selenium.gwt.components.Component;

public class IsInvalid<T extends Component> extends TypeSafeMatcher<T>
{
    private Matcher<Boolean> matcher;

    protected IsInvalid(Matcher<Boolean> matcher)
    {
        this.matcher = matcher;
    }

    @Override
    public void describeTo(Description description)
    {
        description.appendText("invalid ").appendDescriptionOf(matcher);
    }

    @Override
    protected void describeMismatchSafely(T item, Description mismatchDescription)
    {
        mismatchDescription.appendText("was ").appendValue(item.isInvalid());
    }

    @Override
    protected boolean matchesSafely(T component)
    {
        return matcher.matches(component.isInvalid());
    }

    @Factory
    public static <T extends Component> Matcher<T> isHighlightedAsInvalid(final boolean isSelected)
    {
        return new IsInvalid<T>(is(isSelected));
    }

}
