package com.ihg.automation.selenium.matchers.component;

import static org.hamcrest.core.Is.is;

import org.hamcrest.Description;
import org.hamcrest.Factory;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;

import com.ihg.automation.selenium.gwt.components.Componentable;

public class IsDisplayed extends TypeSafeMatcher<Componentable>
{
    private Matcher<Boolean> matcher;
    private boolean isWait;

    protected IsDisplayed(Matcher<Boolean> matcher, boolean isWait)
    {
        this.matcher = matcher;
        this.isWait = isWait;
    }

    @Override
    public void describeTo(Description description)
    {
        description.appendText("displayed ").appendDescriptionOf(matcher);
    }

    @Override
    protected void describeMismatchSafely(Componentable item, Description mismatchDescription)
    {
        mismatchDescription.appendText("displayed was ").appendValue(item.isDisplayed(isWait));
    }

    @Override
    protected boolean matchesSafely(Componentable component)
    {
        return matcher.matches(component.isDisplayed(isWait));
    }

    @Factory
    public static Matcher<Componentable> displayed(final boolean isDisplayed)
    {
        return new IsDisplayed(is(isDisplayed), false);
    }

    @Factory
    public static Matcher<Componentable> displayed(final boolean isDisplayed, final boolean isWait)
    {
        return new IsDisplayed(is(isDisplayed), isWait);
    }

    @Factory
    public static Matcher<Componentable> isDisplayedWithWait()
    {
        return new IsDisplayed(is(true), true);
    }
}
