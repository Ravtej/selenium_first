package com.ihg.automation.selenium.matchers.component;

import static org.hamcrest.core.Is.is;

import org.hamcrest.Description;
import org.hamcrest.Factory;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.openqa.selenium.support.Color;

import com.ihg.automation.selenium.gwt.components.grid.GridCell;
import com.ihg.automation.selenium.gwt.components.grid.GridRow;

public class HasRowColor extends TypeSafeMatcher<GridRow>
{
    private final static String GREEN_COLOR = "#00aa00";

    private Matcher<String> matcher;

    protected HasRowColor(Matcher<String> matcher)
    {
        this.matcher = matcher;
    }

    @Override
    public void describeTo(Description description)
    {
        description.appendText("row color ").appendDescriptionOf(matcher);
    }

    @Override
    protected void describeMismatchSafely(GridRow gridRow, Description mismatchDescription)
    {
        for (int cellIndex = 1; cellIndex <= gridRow.getCells().size(); cellIndex++)
        {
            GridCell cell = gridRow.getCell(cellIndex);
            Color color = Color.fromString(cell.getCssValue("color"));
            mismatchDescription.appendText(cell.getDescription() + " color was ").appendValue(color.asHex());
        }
    }

    @Override
    protected boolean matchesSafely(GridRow gridRow)
    {
        boolean result = true;
        for (int cellIndex = 1; cellIndex <= gridRow.getCells().size(); cellIndex++)
        {
            Color color = Color.fromString(gridRow.getCell(cellIndex).getCssValue("color"));
            result = result && matcher.matches(color.asHex());
        }
        return result;
    }

    @Factory
    public static Matcher<GridRow> isColor(String color)
    {
        return new HasRowColor(is(color));
    }

    @Factory
    public static Matcher<GridRow> isGreenColor()
    {
        return new HasRowColor(is(GREEN_COLOR));
    }
}
