package com.ihg.automation.selenium.matchers.text;

import org.apache.commons.lang3.StringUtils;
import org.hamcrest.Description;
import org.hamcrest.Factory;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;

public class ContainsIgnoreCase extends TypeSafeMatcher<String>
{
    private String value;

    public ContainsIgnoreCase(final String value)
    {
        this.value = value;
    }

    @Override
    public void describeTo(Description description)
    {
        description.appendText("is string contains ignoring case ").appendValue(value);
    }

    @Override
    protected boolean matchesSafely(final String text)
    {
        return StringUtils.containsIgnoreCase(text, value);
    }

    @Factory
    public static Matcher<String> containsIgnoreCase(final String value)
    {
        return new ContainsIgnoreCase(value);
    }
}
