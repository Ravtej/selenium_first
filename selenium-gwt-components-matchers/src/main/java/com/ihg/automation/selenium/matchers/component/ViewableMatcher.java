package com.ihg.automation.selenium.matchers.component;

import org.hamcrest.Matcher;

import com.ihg.automation.selenium.gwt.components.Componentable;
import com.ihg.automation.selenium.gwt.components.Mode;
import com.ihg.automation.selenium.gwt.components.Viewable;

public class ViewableMatcher
{

    public static <T extends Componentable & Viewable> Matcher<T> hasText(final String text, final Mode mode)
    {
        return ModeMatcher.inMode(HasText.hasText(text), mode);
    }

    public static <T extends Componentable & Viewable> Matcher<T> hasText(final Matcher<String> matcher, final Mode mode)
    {
        return ModeMatcher.inMode(HasText.hasText(matcher), mode);
    }

    public static <T extends Componentable & Viewable> Matcher<T> hasTextInView(final String text)
    {
        return ModeMatcher.inView(HasText.hasText(text));
    }

    public static <T extends Componentable & Viewable> Matcher<T> hasTextInView(final Matcher<String> matcher)
    {
        return ModeMatcher.inView(HasText.hasText(matcher));
    }

}
