package com.ihg.automation.selenium.matchers.component;

import java.util.Collection;

import org.hamcrest.Description;
import org.hamcrest.Factory;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;

import com.ihg.automation.selenium.gwt.components.Componentable;
import com.ihg.automation.selenium.gwt.components.input.Select;
import com.ihg.automation.selenium.gwt.components.input.Select.List;
import com.ihg.automation.selenium.matchers.HasSameItemsAsCollection;

public class HasSelectItems extends TypeSafeMatcher<Select> implements HighlightDifferent<List>
{
    private HasSameItemsAsCollection matcher;

    protected HasSelectItems(Collection<String> items)
    {
        this.matcher = HasSameItemsAsCollection.hasSameItemsAsCollection(items);
    }

    @Override
    public List getComponent(Componentable matcherComponent)
    {
        return ((Select) matcherComponent).openElementsList();
    }

    @Override
    public void describeTo(Description description)
    {
        description.appendText("select items ").appendDescriptionOf(matcher);
    }

    @Override
    protected void describeMismatchSafely(Select select, Description mismatchDescription)
    {
        matcher.describeMismatch(getComponent(select).getItemsText(), mismatchDescription);
    }

    @Override
    protected boolean matchesSafely(Select select)
    {
        return matcher.matches(getComponent(select).getItemsText());
    }

    @Factory
    public static Matcher<Select> hasSelectItems(final Collection<String> items)
    {
        return new HasSelectItems(items);
    }

}
