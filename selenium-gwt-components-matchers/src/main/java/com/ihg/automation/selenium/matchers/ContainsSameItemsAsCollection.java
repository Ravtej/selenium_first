package com.ihg.automation.selenium.matchers;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.hamcrest.Description;
import org.hamcrest.Factory;

public class ContainsSameItemsAsCollection extends HasSameItemsAsCollection
{
    private Collection<String> expectedList;

    protected ContainsSameItemsAsCollection(Collection<String> expectedList)
    {
        this.expectedList = expectedList;
    }

    @Override
    protected boolean matchesSafely(Collection<String> actualList, Description mismatchDescription)
    {
        List<String> missedValues = new ArrayList<String>(expectedList);
        missedValues.removeAll(actualList);
        appendMismatch(mismatchDescription, "in Expected but not in Actual", missedValues);

        return (missedValues.size() == 0);
    }

    @Factory
    public static ContainsSameItemsAsCollection containsSameItemsAsCollection(final Collection<String> expectedList)
    {
        return new ContainsSameItemsAsCollection(expectedList);
    }
}
