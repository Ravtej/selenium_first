package com.ihg.automation.selenium.matchers.component;

import static org.hamcrest.core.Is.is;

import org.hamcrest.Description;
import org.hamcrest.Factory;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;

import com.ihg.automation.selenium.gwt.components.Componentable;
import com.ihg.automation.selenium.gwt.components.HavingDefaultValue;

public class HasDefaultText<T extends Componentable & HavingDefaultValue> extends TypeSafeMatcher<T>
{
    private Matcher<String> matcher;

    protected HasDefaultText()
    {
    }

    @Override
    public void describeTo(Description description)
    {
        description.appendText("default value");
        if (matcher != null)
        {
            description.appendText(" ").appendDescriptionOf(matcher);
        }
    }

    @Override
    protected void describeMismatchSafely(T item, Description mismatchDescription)
    {
        mismatchDescription.appendText("text was ").appendValue(item.getText());
    }

    @Override
    protected boolean matchesSafely(T component)
    {
        matcher = is(component.getDefaultValue());
        return matcher.matches(component.getText());
    }

    @Factory
    public static <T extends Componentable & HavingDefaultValue> Matcher<T> hasDefault()
    {
        return new HasDefaultText<T>();
    }

}
