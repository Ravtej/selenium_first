package com.ihg.automation.selenium.matchers;

import static java.util.concurrent.TimeUnit.SECONDS;

import org.hamcrest.Description;
import org.hamcrest.SelfDescribing;

public class Waiter implements SelfDescribing
{
    private static final long DEFAULT_TIMEOUT = SECONDS.toMillis(5);
    private static final long DEFAULT_POLLING_INTERVAL = SECONDS.toMillis(1);

    private long pollingInterval = DEFAULT_POLLING_INTERVAL;
    private long startTime;
    private final long timeout;

    public Waiter()
    {
        this.timeout = DEFAULT_TIMEOUT;
    }

    public Waiter(long timeout)
    {
        this.timeout = timeout;
    }

    public void startWaiting()
    {
        startTime = System.currentTimeMillis();
    }

    public long getPollingInterval()
    {
        return pollingInterval;
    }

    public Waiter withPollingInterval(long pollingInterval)
    {
        setPollingInterval(pollingInterval);
        return this;
    }

    protected void setPollingInterval(long pollingInterval)
    {
        this.pollingInterval = pollingInterval;
    }

    public boolean shouldStopWaiting()
    {
        long currentTime = System.currentTimeMillis();
        return currentTime >= startTime + timeout;
    }

    @Override
    public void describeTo(Description description)
    {
        description.appendValue(timeout).appendText(" milliseconds");
    }
}
