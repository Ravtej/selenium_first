package com.ihg.automation.selenium.matchers.component;

import static org.hamcrest.core.Is.is;

import org.hamcrest.Description;
import org.hamcrest.Factory;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeDiagnosingMatcher;

import com.ihg.automation.selenium.gwt.components.Component;

public class IsDisplayedAndEnabled extends TypeSafeDiagnosingMatcher<Component>
{
    private Matcher<Boolean> matcher;
    private boolean isWait;

    protected IsDisplayedAndEnabled(Matcher<Boolean> matcher, boolean isWait)
    {
        this.matcher = matcher;
        this.isWait = isWait;
    }

    @Override
    public void describeTo(Description description)
    {
        description.appendText("displayed && enabled ").appendDescriptionOf(matcher);
    }

    @Override
    protected boolean matchesSafely(Component item, Description mismatchDescription)
    {
        boolean result = item.isDisplayed(isWait);
        mismatchDescription.appendText(" displayed is ").appendValue(result);
        if (result)
        {
            result = item.isEnabled();
            mismatchDescription.appendText(" and enabled is ").appendValue(result);
        }

        return matcher.matches(result);
    }

    @Factory
    public static Matcher<Component> isDisplayedAndEnabled(final boolean isDisplayedAndEnabled)
    {
        return new IsDisplayedAndEnabled(is(isDisplayedAndEnabled), false);
    }

    @Factory
    public static Matcher<Component> isDisplayedAndEnabledWithWait(final boolean isDisplayed)
    {
        return new IsDisplayedAndEnabled(is(isDisplayed), true);
    }

}
