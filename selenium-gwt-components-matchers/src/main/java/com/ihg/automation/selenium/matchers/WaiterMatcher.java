package com.ihg.automation.selenium.matchers;

import org.hamcrest.Description;
import org.hamcrest.Factory;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;

public class WaiterMatcher<T> extends TypeSafeMatcher<T>
{
    private final Matcher<? super T> matcher;
    private final Waiter waiter;

    private WaiterMatcher(Matcher<? super T> matcher, Waiter waiter)
    {
        this.matcher = matcher;
        this.waiter = waiter;
    }

    @Override
    protected boolean matchesSafely(T item)
    {
        waiter.startWaiting();

        while (!waiter.shouldStopWaiting())
        {
            if (matcher.matches(item))
            {
                return true;
            }

            try
            {
                Thread.sleep(waiter.getPollingInterval());
            }
            catch (InterruptedException e)
            {
                break;
            }
        }

        return matcher.matches(item);
    }

    @Override
    public void describeTo(Description description)
    {
        description.appendDescriptionOf(matcher).appendText(" while waiting for ").appendDescriptionOf(waiter);
    }

    @Override
    protected void describeMismatchSafely(T item, Description mismatchDescription)
    {
        matcher.describeMismatch(item, mismatchDescription);
        mismatchDescription.appendText(" while waiting for ").appendDescriptionOf(waiter);
    }

    @Factory
    public static <T> Matcher<T> wait(final Matcher<? super T> matcher)
    {
        return new WaiterMatcher<T>(matcher, new Waiter());
    }

    @Factory
    public static <T> Matcher<T> wait(final Matcher<? super T> matcher, long timeout)
    {
        return new WaiterMatcher<T>(matcher, new Waiter(timeout));
    }
}
