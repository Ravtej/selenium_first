package com.ihg.automation.selenium.matchers.component;

import static com.ihg.automation.selenium.matchers.component.ViewableMatcher.hasTextInView;
import static org.hamcrest.core.IsNot.not;
import static org.hamcrest.text.IsEmptyString.isEmptyOrNullString;
import static org.hamcrest.text.IsEmptyString.isEmptyString;

import java.util.Collection;

import org.hamcrest.Matcher;
import org.joda.time.LocalDate;

import com.ihg.automation.selenium.gwt.components.Component;
import com.ihg.automation.selenium.gwt.components.Componentable;
import com.ihg.automation.selenium.gwt.components.HavingDefaultValue;
import com.ihg.automation.selenium.gwt.components.Selectable;
import com.ihg.automation.selenium.gwt.components.Viewable;
import com.ihg.automation.selenium.matchers.HasSameItemsAsCollection;
import com.ihg.automation.selenium.matchers.WaiterMatcher;
import com.ihg.automation.selenium.matchers.text.AsCollection;
import com.ihg.automation.selenium.matchers.text.AsDouble;
import com.ihg.automation.selenium.matchers.text.AsFormattedDouble;
import com.ihg.automation.selenium.matchers.text.AsFormattedInteger;
import com.ihg.automation.selenium.matchers.text.AsInteger;
import com.ihg.automation.selenium.matchers.text.AsLocalDate;

public class ComponentMatcher
{
    private final static String RED_COLOR = "#ffb0b0";

    private ComponentMatcher()
    {
    }

    public static Matcher<Componentable> hasText(final String text)
    {
        return HasText.hasText(text);
    }

    public static Matcher<Componentable> hasText(final Matcher<String> matcher)
    {
        return HasText.hasText(matcher);
    }

    public static Matcher<Componentable> hasTextNoWait(final Matcher<String> matcher)
    {
        return HasText.hasTextNoWait(matcher);
    }

    public static Matcher<Componentable> hasEmptyText()
    {
        return HasText.hasTextNoWait(isEmptyString());
    }

    public static Matcher<Componentable> hasEmptyOrNullText()
    {
        return HasText.hasTextNoWait(isEmptyOrNullString());
    }

    public static Matcher<Componentable> hasTextAsInt(final Integer value)
    {
        return HasText.hasText(AsInteger.asInt(value));
    }

    public static Matcher<Componentable> hasTextAsInt(final Matcher<Integer> matcher)
    {
        return HasText.hasText(AsInteger.asInt(matcher));
    }

    public static Matcher<Componentable> hasTextAsFormattedInt(final String text)
    {
        return HasText.hasText(AsFormattedInteger.asFormattedInt(text));
    }

    public static Matcher<Componentable> hasTextAsDouble(final Double value)
    {
        return HasText.hasText(AsDouble.asDouble(value));
    }

    public static Matcher<Componentable> hasTextAsDouble(final Matcher<Double> matcher)
    {
        return HasText.hasText(AsDouble.asDouble(matcher));
    }

    public static Matcher<Componentable> hasAnyText()
    {
        return HasText.hasText(not(isEmptyString()));
    }

    public static <T extends Componentable & Viewable> Matcher<T> hasAnyTextInView()
    {
        return hasTextInView(not(isEmptyString()));
    }

    public static Matcher<Componentable> hasTextAsFormattedDouble(final Double value)
    {
        return HasText.hasText(AsFormattedDouble.asFormattedDouble(value));
    }

    public static Matcher<Componentable> hasTextAsDate(final String format, final Matcher<LocalDate> matcher)
    {
        return HasText.hasText(AsLocalDate.asDate(format, matcher));
    }

    public static Matcher<Componentable> hasTextAsCollection(final Collection<String> expectedValues)
    {
        return HasText
                .hasText(AsCollection.asCollection(HasSameItemsAsCollection.hasSameItemsAsCollection(expectedValues)));
    }

    public static Matcher<Componentable> hasTextWithWait(final String text)
    {
        return WaiterMatcher.wait(HasText.hasText(text));
    }

    /**
     * Use when parent component(s) might change the state
     */
    public static Matcher<Componentable> hasTextWithWaitAndFlush(final String text)
    {
        return WaiterMatcher.wait(FlushComponent.flush(HasText.hasText(text)));
    }

    public static Matcher<Componentable> displayed(final boolean isDisplayed)
    {
        return IsDisplayed.displayed(isDisplayed);
    }

    public static Matcher<Componentable> displayed(final boolean isDisplayed, final boolean isWait)
    {
        return IsDisplayed.displayed(isDisplayed, isWait);
    }

    public static Matcher<Componentable> isDisplayedWithWait()
    {
        return IsDisplayed.isDisplayedWithWait();
    }

    public static Matcher<Component> enabled(final boolean isEnabled)
    {
        return IsEnabled.enabled(isEnabled);
    }

    public static Matcher<Component> isDisplayedAndEnabled(final boolean isDisplayedAndEnabled)
    {
        return IsDisplayedAndEnabled.isDisplayedAndEnabled(isDisplayedAndEnabled);
    }

    public static <T extends Componentable & Selectable> Matcher<T> isSelected(final boolean isSelected)
    {
        return IsSelected.isSelected(isSelected);
    }

    public static <T extends Componentable & HavingDefaultValue> Matcher<T> hasDefault()
    {
        return HasDefaultText.hasDefault();
    }

    public static <T extends Componentable & HavingDefaultValue> Matcher<T> hasDefaultOrText(final String text)
    {
        return HasDefaultOrText.hasDefaultOrText(text);
    }

    public static Matcher<Componentable> hasDefaultOrText(final String text, final String defaultText)
    {
        if (text != null)
        {
            return HasText.hasText(text);
        }
        else
        {
            return HasText.hasText(defaultText);
        }
    }

    public static <T extends Component> Matcher<T> isHighlightedAsInvalid(final boolean isSelected)
    {
        return IsInvalid.isHighlightedAsInvalid(isSelected);
    }

    public static Matcher<Component> isRedColor()
    {
        return HasBackgroundColor.hasBackgroundColor(RED_COLOR);
    }
}
