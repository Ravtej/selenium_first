package com.ihg.automation.selenium.matchers.component;

import java.util.Collection;

import org.hamcrest.Description;
import org.hamcrest.Factory;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;

import com.ihg.automation.selenium.gwt.components.DualList;
import com.ihg.automation.selenium.matchers.HasSameItemsAsCollection;

public class HasDualListItems extends TypeSafeMatcher<DualList.List>
{
    private HasSameItemsAsCollection matcher;
    private boolean onlyBoldItems;

    private HasDualListItems(Collection<String> items, boolean onlyBoldItems)
    {
        this.matcher = HasSameItemsAsCollection.hasSameItemsAsCollection(items);
        this.onlyBoldItems = onlyBoldItems;
    }

    @Override
    public void describeTo(Description description)
    {
        description.appendText("items ");

        if (onlyBoldItems)
        {
            description.appendText("(bold) ");
        }

        description.appendDescriptionOf(matcher);
    }

    @Override
    protected void describeMismatchSafely(DualList.List dualList, Description mismatchDescription)
    {
        mismatchDescription.appendText("items do not match:");
        matcher.describeMismatch(getList(dualList), mismatchDescription);
    }

    @Override
    protected boolean matchesSafely(DualList.List dualList)
    {
        return matcher.matches(getList(dualList));
    }

    private java.util.List<String> getList(DualList.List dualList)
    {
        if (onlyBoldItems)
        {
            return dualList.getBoldItemsText();
        }
        return dualList.getItemsText();
    }

    @Factory
    public static Matcher<DualList.List> hasItems(final Collection<String> items)
    {
        return new HasDualListItems(items, false);
    }

    @Factory
    public static Matcher<DualList.List> hasBoldItems(final Collection<String> items)
    {
        return new HasDualListItems(items, true);
    }
}
