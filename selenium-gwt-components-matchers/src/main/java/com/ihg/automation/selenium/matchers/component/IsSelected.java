package com.ihg.automation.selenium.matchers.component;

import static org.hamcrest.core.Is.is;

import org.hamcrest.Description;
import org.hamcrest.Factory;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;

import com.ihg.automation.selenium.gwt.components.Componentable;
import com.ihg.automation.selenium.gwt.components.Selectable;

public class IsSelected<T extends Componentable & Selectable> extends TypeSafeMatcher<T>
{
    private Matcher<Boolean> matcher;

    protected IsSelected(Matcher<Boolean> matcher)
    {
        this.matcher = matcher;
    }

    @Override
    public void describeTo(Description description)
    {
        description.appendText("selected ").appendDescriptionOf(matcher);
    }

    @Override
    protected void describeMismatchSafely(T item, Description mismatchDescription)
    {
        mismatchDescription.appendText("selected was ").appendValue(item.isSelected());
    }

    @Override
    protected boolean matchesSafely(T component)
    {
        return matcher.matches(component.isSelected());
    }

    @Factory
    public static <T extends Componentable & Selectable> Matcher<T> isSelected(final boolean isSelected)
    {
        return new IsSelected<T>(is(isSelected));
    }

}
