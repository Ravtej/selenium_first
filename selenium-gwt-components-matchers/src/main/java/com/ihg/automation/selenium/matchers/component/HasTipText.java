package com.ihg.automation.selenium.matchers.component;

import static org.hamcrest.core.Is.is;

import org.hamcrest.Description;
import org.hamcrest.Factory;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;

import com.ihg.automation.selenium.gwt.components.Component;
import com.ihg.automation.selenium.gwt.components.Componentable;
import com.ihg.automation.selenium.gwt.components.tip.Tip;
import com.ihg.automation.selenium.gwt.components.tip.TipType;
import com.ihg.automation.selenium.matchers.WaiterMatcher;

public class HasTipText extends TypeSafeMatcher<Component> implements HighlightDifferent<Tip>
{
    private Matcher<String> matcher;
    private TipType tipType;
    private boolean isWait;

    /** Only for caching purpose */
    private String actualText;

    private HasTipText(Matcher<String> matcher, final TipType tipType, boolean isWait)
    {
        this.matcher = matcher;
        this.tipType = tipType;
        this.isWait = isWait;
    }

    private HasTipText(Matcher<String> matcher, final TipType tipType)
    {
        this(matcher, tipType, true);
    }

    @Override
    public Tip getComponent(Componentable matcherComponent)
    {
        return new Tip(tipType);
    }

    @Override
    public void describeTo(Description description)
    {
        description.appendText("tool tip text ").appendDescriptionOf(matcher);
    }

    @Override
    protected void describeMismatchSafely(Component item, Description mismatchDescription)
    {
        matcher.describeMismatch(actualText, mismatchDescription);
    }

    @Override
    protected boolean matchesSafely(Component component)
    {
        component.moveTo();
        actualText = getComponent(null).getText(isWait);
        return matcher.matches(actualText);
    }

    @Factory
    public static Matcher<Component> hasTipText(final Matcher<String> matcher, final TipType tipType)
    {
        return new HasTipText(matcher, tipType);
    }

    @Factory
    public static Matcher<Component> hasTipText(final String text, final TipType tipType)
    {
        return new HasTipText(is(text), tipType);
    }

    @Factory
    public static Matcher<Component> hasInfoTipText(final String text)
    {
        return new HasTipText(is(text), TipType.INFO);
    }

    @Factory
    public static Matcher<Component> hasWarningTipText(final String text)
    {
        return new HasTipText(is(text), TipType.WARNING);
    }

    @Factory
    public static Matcher<Component> hasTipTextWithWait(final String text, final TipType tipType)
    {
        return WaiterMatcher.wait(HasTipText.hasTipText(text, tipType));
    }

    @Factory
    public static Matcher<Component> hasInfoTipTextWithWait(final String text)
    {
        return WaiterMatcher.wait(HasTipText.hasInfoTipText(text));
    }

    @Factory
    public static Matcher<Component> hasWarningTipTextWithWait(final String text)
    {
        return WaiterMatcher.wait(HasTipText.hasWarningTipText(text));
    }
}
