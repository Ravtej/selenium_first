package com.ihg.automation.selenium.matchers.text;

import static org.hamcrest.core.Is.is;

import org.hamcrest.Description;
import org.hamcrest.Factory;
import org.hamcrest.Matcher;

import com.ihg.automation.selenium.formatter.Converter;

public class AsDouble extends StringConverterMatcherBase<Double>
{

    protected AsDouble(Matcher<Double> matcher)
    {
        super(matcher);
    }

    @Override
    public Double convert(String item)
    {
        return Converter.stringToDouble(item);
    }

    @Override
    public void appendConvertToDescription(Description description)
    {
        description.appendText("Double");
    }

    @Factory
    public static AsDouble asDouble(final Double value)
    {
        return new AsDouble(is(value));
    }

    @Factory
    public static AsDouble asDouble(final Matcher<Double> matcher)
    {
        return new AsDouble(matcher);
    }

}
