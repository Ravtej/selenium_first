package com.ihg.automation.selenium.formatter;

import static com.ihg.automation.selenium.formatter.Converter.CURRENCY_AMOUNT_DECIMAL_FORMAT;
import static com.ihg.automation.selenium.formatter.Converter.INT_FORMAT;

import org.apache.commons.lang3.StringUtils;

public class StringFormatter
{
    public static String asFormattedInt(final String text)
    {
        if (StringUtils.isNotEmpty(text) || !StringUtils.contains(text, "n/a"))
        {
            try
            {
                return INT_FORMAT.format(Integer.valueOf(text));
            }
            catch (Exception e)
            {
            }
        }

        return text;
    }

    public static String asFormattedDouble(final String text)
    {
        if (StringUtils.isNotEmpty(text) || !StringUtils.contains(text, "n/a"))
        {
            try
            {
                return CURRENCY_AMOUNT_DECIMAL_FORMAT.format(Double.valueOf(text));
            }
            catch (Exception e)
            {
            }
        }

        return text;
    }

    public static String asFormattedDouble(final Double value)
    {
        return (value == null) ? null : CURRENCY_AMOUNT_DECIMAL_FORMAT.format(value);
    }

}
