package com.ihg.automation.selenium.matchers.component;

import static org.hamcrest.core.Is.is;

import org.hamcrest.Description;
import org.hamcrest.Factory;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;

import com.ihg.automation.selenium.gwt.components.Component;

public class IsEnabled extends TypeSafeMatcher<Component>
{
    private Matcher<Boolean> matcher;

    protected IsEnabled(Matcher<Boolean> matcher)
    {
        this.matcher = matcher;
    }

    @Override
    public void describeTo(Description description)
    {
        description.appendText("enabled ").appendDescriptionOf(matcher);
    }

    @Override
    protected void describeMismatchSafely(Component item, Description mismatchDescription)
    {
        mismatchDescription.appendText("enabled was ").appendValue(item.isEnabled());
    }

    @Override
    protected boolean matchesSafely(Component component)
    {
        return matcher.matches(component.isEnabled());
    }

    @Factory
    public static Matcher<Component> enabled(final boolean isEnabled)
    {
        return new IsEnabled(is(isEnabled));
    }
}
