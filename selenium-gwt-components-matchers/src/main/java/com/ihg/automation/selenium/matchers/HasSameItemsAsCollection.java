package com.ihg.automation.selenium.matchers;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.hamcrest.Description;
import org.hamcrest.Factory;
import org.hamcrest.TypeSafeDiagnosingMatcher;

public class HasSameItemsAsCollection extends TypeSafeDiagnosingMatcher<Collection<String>>
{
    private Collection<String> expectedList;
    private final static String LEFT_INDENTION = "          ";

    protected HasSameItemsAsCollection()
    {
    }

    protected HasSameItemsAsCollection(Collection<String> expectedList)
    {
        this.expectedList = expectedList;
    }

    @Override
    public void describeTo(Description description)
    {
        description.appendText("collections contains same items");
    }

    @Override
    protected boolean matchesSafely(Collection<String> actualList, Description mismatchDescription)
    {
        List<String> newValues = new ArrayList<String>(actualList);
        newValues.removeAll(expectedList);
        appendMismatch(mismatchDescription, "in Actual but not in Expected", newValues);

        List<String> missedValues = new ArrayList<String>(expectedList);
        missedValues.removeAll(actualList);
        appendMismatch(mismatchDescription, "in Expected but not in Actual", missedValues);

        return (newValues.size() == 0) && (missedValues.size() == 0);
    }

    protected void appendMismatch(Description mismatchDescription, String comment, List<String> mismatchList)
    {
        if (CollectionUtils.isNotEmpty(mismatchList))
        {
            mismatchDescription.appendText("\n").appendText(LEFT_INDENTION).appendValue(mismatchList.size())
                    .appendText(" item(s) ").appendText(comment).appendText(":\n").appendText(LEFT_INDENTION)
                    .appendValueList(" -> ", "\n " + LEFT_INDENTION + "-> ", "", mismatchList);
        }
    }

    @Factory
    public static HasSameItemsAsCollection hasSameItemsAsCollection(final Collection<String> expectedList)
    {
        return new HasSameItemsAsCollection(expectedList);
    }
}
