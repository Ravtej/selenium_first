package com.ihg.automation.selenium.matchers.component;

import static org.hamcrest.core.Is.is;

import org.hamcrest.Description;
import org.hamcrest.Factory;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;

import com.ihg.automation.selenium.gwt.components.grid.GridBase;

public class GridSize extends TypeSafeMatcher<GridBase>
{
    private Matcher<Integer> matcher;

    protected GridSize(Matcher<Integer> matcher)
    {
        this.matcher = matcher;
    }

    @Override
    public void describeTo(Description description)
    {
        description.appendText("row count ").appendDescriptionOf(matcher);
    }

    @Override
    protected void describeMismatchSafely(GridBase grid, Description mismatchDescription)
    {
        mismatchDescription.appendText("count was ").appendValue(grid.getRowCount());
    }

    @Override
    protected boolean matchesSafely(GridBase grid)
    {
        return matcher.matches(grid.getRowCount());
    }

    @Factory
    public static Matcher<GridBase> size(final int size)
    {
        return new GridSize(is(size));
    }

    @Factory
    public static Matcher<GridBase> size(final Matcher<Integer> matcher)
    {
        return new GridSize(matcher);
    }
}
