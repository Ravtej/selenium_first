package com.ihg.automation.selenium.matchers.component;

import org.hamcrest.Description;
import org.hamcrest.Factory;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;

import com.ihg.automation.selenium.gwt.components.Componentable;
import com.ihg.automation.selenium.gwt.components.Mode;
import com.ihg.automation.selenium.gwt.components.Viewable;

public class ModeMatcher<T extends Componentable & Viewable> extends TypeSafeMatcher<T> implements
        HighlightDifferent<Componentable>
{
    private Matcher<Componentable> matcher;
    private Mode mode;

    protected ModeMatcher(Matcher<Componentable> matcher, Mode mode)
    {
        this.matcher = matcher;
        this.mode = mode;
    }

    @SuppressWarnings("unchecked")
    @Override
    public Componentable getComponent(Componentable matcherComponent)
    {
        if (mode == Mode.VIEW)
        {
            return ((T) matcherComponent).getView();
        }

        return matcherComponent;
    }

    @Override
    public void describeTo(Description description)
    {
        description.appendDescriptionOf(matcher).appendText(" in ").appendValue(mode).appendText(" mode");
    }

    @Override
    protected void describeMismatchSafely(T item, Description mismatchDescription)
    {
        matcher.describeMismatch(getComponent(item), mismatchDescription);
    }

    @Override
    protected boolean matchesSafely(T component)
    {
        return matcher.matches(getComponent(component));
    }

    @Factory
    public static <T extends Componentable & Viewable> Matcher<T> inMode(final Matcher<Componentable> matcher,
            final Mode mode)
    {
        return new ModeMatcher<T>(matcher, mode);
    }

    @Factory
    public static <T extends Componentable & Viewable> Matcher<T> inView(final Matcher<Componentable> matcher)
    {
        return new ModeMatcher<T>(matcher, Mode.VIEW);
    }
}
