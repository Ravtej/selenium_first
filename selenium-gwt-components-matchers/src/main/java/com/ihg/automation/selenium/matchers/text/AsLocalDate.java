package com.ihg.automation.selenium.matchers.text;

import org.hamcrest.Description;
import org.hamcrest.Factory;
import org.hamcrest.Matcher;
import org.joda.time.LocalDate;

import com.ihg.automation.selenium.formatter.Converter;

public class AsLocalDate extends StringConverterMatcherBase<LocalDate>
{
    private String format;

    protected AsLocalDate(Matcher<LocalDate> matcher, String format)
    {
        super(matcher);
        this.format = format;
    }

    @Override
    public LocalDate convert(String item)
    {
        return Converter.stringToLocalDate(item, format);
    }

    @Override
    public void appendConvertToDescription(Description description)
    {
        description.appendText("Local Date in ").appendValue(format).appendText(" format");
    }

    @Factory
    public static AsLocalDate asDate(String format, Matcher<LocalDate> matcher)
    {
        return new AsLocalDate(matcher, format);
    }

}
