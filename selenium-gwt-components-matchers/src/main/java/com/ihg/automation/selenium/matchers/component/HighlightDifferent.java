package com.ihg.automation.selenium.matchers.component;

import com.ihg.automation.selenium.gwt.components.Componentable;

public interface HighlightDifferent<T extends Componentable>
{
    T getComponent(Componentable matcherComponent);
}
