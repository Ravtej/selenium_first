package com.ihg.automation.selenium.matchers.date;

import org.hamcrest.Description;
import org.hamcrest.Factory;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.joda.time.LocalDate;

public class IsBeforeOrEqualsDate extends TypeSafeMatcher<LocalDate>
{
    private final LocalDate expected;

    public IsBeforeOrEqualsDate(final LocalDate expected)
    {
        this.expected = expected;
    }

    @Override
    protected boolean matchesSafely(final LocalDate actual)
    {
        return actual.isBefore(expected) || actual.isEqual(expected);
    }

    @Override
    protected void describeMismatchSafely(LocalDate item, Description mismatchDescription)
    {
        mismatchDescription.appendText("date is ").appendValue(item);
    }

    public void describeTo(final Description description)
    {
        description.appendText("date is before ").appendValue(expected);
    }

    @Factory
    public static Matcher<LocalDate> isBeforeOrEquals(final LocalDate date)
    {
        return new IsBeforeOrEqualsDate(date);
    }
}
