package com.ihg.automation.selenium.matchers.component;

import java.util.Collection;

import org.hamcrest.Description;
import org.hamcrest.Factory;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;

import com.ihg.automation.selenium.gwt.components.DualList;
import com.ihg.automation.selenium.matchers.ContainsSameItemsAsCollection;

public class ContainsDualListItems extends TypeSafeMatcher<DualList.List>
{
    private ContainsSameItemsAsCollection matcher;
    private boolean onlyBoldItems;

    private ContainsDualListItems(Collection<String> items, boolean onlyBoldItems)
    {
        this.matcher = ContainsSameItemsAsCollection.containsSameItemsAsCollection(items);
        this.onlyBoldItems = onlyBoldItems;
    }

    @Override
    public void describeTo(Description description)
    {
        description.appendText("items ");

        if (onlyBoldItems)
        {
            description.appendText("(bold) ");
        }

        description.appendDescriptionOf(matcher);
    }

    @Override
    protected void describeMismatchSafely(DualList.List dualList, Description mismatchDescription)
    {
        mismatchDescription.appendText("items do not match:");
        matcher.describeMismatch(getList(dualList), mismatchDescription);
    }

    @Override
    protected boolean matchesSafely(DualList.List dualList)
    {
        return matcher.matches(getList(dualList));
    }

    private java.util.List<String> getList(DualList.List dualList)
    {
        if (onlyBoldItems)
        {
            return dualList.getBoldItemsText();
        }
        return dualList.getItemsText();
    }

    @Factory
    public static Matcher<DualList.List> containsItems(final Collection<String> items)
    {
        return new ContainsDualListItems(items, false);
    }

    @Factory
    public static Matcher<DualList.List> containsBoldItems(final Collection<String> items)
    {
        return new ContainsDualListItems(items, true);
    }
}
