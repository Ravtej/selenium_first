package com.ihg.automation.selenium.matchers.text;

import static org.hamcrest.core.Is.is;

import org.hamcrest.Matcher;

import com.ihg.automation.selenium.gwt.components.EntityType;

public class EntityMatcherFactory
{
    private EntityMatcherFactory()
    {
    }

    public static Matcher<String> isValue(EntityType entity)
    {
        String value = (entity == null) ? null : entity.getValue();
        return is(value);
    }
}
