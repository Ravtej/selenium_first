package com.ihg.automation.selenium.matchers.text;

import org.hamcrest.Factory;

import com.ihg.automation.selenium.formatter.StringFormatter;

public class AsFormattedInteger extends AsFormattedMatcherBase
{
    protected AsFormattedInteger(String text)
    {
        super(text, StringFormatter.asFormattedInt(text));
    }

    @Factory
    public static AsFormattedInteger asFormattedInt(final String value)
    {
        return new AsFormattedInteger(value);
    }

}
