package com.ihg.automation.selenium.matchers.component;

import org.hamcrest.Description;
import org.hamcrest.Factory;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;

import com.ihg.automation.selenium.gwt.components.Componentable;

public class FlushComponent<T extends Componentable> extends TypeSafeMatcher<T>
{
    private Matcher<T> matcher;

    public FlushComponent(Matcher<T> matcher)
    {
        this.matcher = matcher;
    }

    @Override
    protected boolean matchesSafely(T item)
    {
        item.flush();
        return matcher.matches(item);
    }

    @Override
    public void describeTo(Description description)
    {
        matcher.describeTo(description);
    }

    @Factory
    public static Matcher<Componentable> flush(Matcher<Componentable> matcher)
    {
        return new FlushComponent<Componentable>(matcher);
    }
}
