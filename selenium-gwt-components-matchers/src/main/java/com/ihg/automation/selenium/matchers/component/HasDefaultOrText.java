package com.ihg.automation.selenium.matchers.component;

import org.hamcrest.Description;
import org.hamcrest.Factory;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;

import com.ihg.automation.selenium.gwt.components.Componentable;
import com.ihg.automation.selenium.gwt.components.HavingDefaultValue;

public class HasDefaultOrText<T extends Componentable & HavingDefaultValue> extends TypeSafeMatcher<T>
{
    private Matcher<?> matcher;

    protected HasDefaultOrText(Matcher<?> matcher)
    {
        this.matcher = matcher;
    }

    @Override
    public void describeTo(Description description)
    {
        description.appendDescriptionOf(matcher);
    }

    @Override
    protected void describeMismatchSafely(T item, Description mismatchDescription)
    {
        matcher.describeMismatch(item, mismatchDescription);
    }

    @Override
    protected boolean matchesSafely(T component)
    {
        return matcher.matches(component);
    }

    @Factory
    public static <T extends Componentable & HavingDefaultValue> Matcher<T> hasDefaultOrText(final String text)
    {
        Matcher matcher;
        if (text == null)
        {
            matcher = HasDefaultText.hasDefault();
        }
        else
        {
            matcher = HasText.hasText(text);
        }

        return new HasDefaultOrText<T>(matcher);
    }

}
