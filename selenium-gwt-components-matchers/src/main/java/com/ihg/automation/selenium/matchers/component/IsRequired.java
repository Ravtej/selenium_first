package com.ihg.automation.selenium.matchers.component;

import static org.hamcrest.core.Is.is;

import org.hamcrest.Description;
import org.hamcrest.Factory;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;

import com.ihg.automation.selenium.gwt.components.input.InputBase;

public class IsRequired extends TypeSafeMatcher<InputBase>
{
    private Matcher<Boolean> matcher;

    protected IsRequired(Matcher<Boolean> matcher)
    {
        this.matcher = matcher;
    }

    @Override
    public void describeTo(Description description)
    {
        description.appendText("required ").appendDescriptionOf(matcher);
    }

    @Override
    protected void describeMismatchSafely(InputBase item, Description mismatchDescription)
    {
        mismatchDescription.appendText("required was ").appendValue(item.getLabel().isRequired());
    }

    @Override
    protected boolean matchesSafely(InputBase component)
    {
        return matcher.matches(component.getLabel().isRequired());
    }

    @Factory
    public static Matcher<InputBase> required(final boolean isRequired)
    {
        return new IsRequired(is(isRequired));
    }
}
