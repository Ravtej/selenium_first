package com.ihg.automation.selenium.matchers.text;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;

public abstract class StringConverterMatcherBase<T> extends TypeSafeMatcher<String>
{
    private Matcher<T> matcher;
    private T convertedValue;

    protected StringConverterMatcherBase(Matcher<T> matcher)
    {
        this.matcher = matcher;
    }

    public T getConvertedString()
    {
        return convertedValue;
    }

    public Matcher<T> getMatcher()
    {
        return matcher;
    }

    public void describeMismatchForParent(Description mismatchDescription)
    {
        mismatchDescription.appendText(" converted to ");
        appendConvertToDescription(mismatchDescription);
        mismatchDescription.appendText(" is ").appendValue(convertedValue);
    }

    @Override
    public void describeTo(Description description)
    {
        description.appendText("as ");
        appendConvertToDescription(description);
        description.appendText(" ").appendDescriptionOf(getMatcher());
    }

    @Override
    protected void describeMismatchSafely(String item, Description mismatchDescription)
    {
        mismatchDescription.appendValue(item).appendText(" converted to ");
        appendConvertToDescription(mismatchDescription);
        mismatchDescription.appendText(" is ").appendValue(convertedValue);
    }

    public abstract T convert(String item);

    public abstract void appendConvertToDescription(Description mismatchDescription);

    @Override
    protected boolean matchesSafely(String item)
    {
        convertedValue = convert(item);
        return getMatcher().matches(convert(item));
    }

}
