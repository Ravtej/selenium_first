package com.ihg.automation.selenium.matchers.text;

import static org.hamcrest.core.Is.is;

import org.hamcrest.Description;
import org.hamcrest.Factory;
import org.hamcrest.Matcher;

import com.ihg.automation.selenium.formatter.Converter;

public class AsInteger extends StringConverterMatcherBase<Integer>
{

    protected AsInteger(Matcher<Integer> matcher)
    {
        super(matcher);
    }

    @Override
    public Integer convert(String item)
    {
        return Converter.stringToInteger(item);
    }

    @Override
    public void appendConvertToDescription(Description description)
    {
        description.appendText("Integer");
    }

    @Factory
    public static AsInteger asInt(final Integer value)
    {
        return new AsInteger(is(value));
    }

    @Factory
    public static AsInteger asInt(final Matcher<Integer> matcher)
    {
        return new AsInteger(matcher);
    }

}
