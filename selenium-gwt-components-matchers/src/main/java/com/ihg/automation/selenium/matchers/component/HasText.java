package com.ihg.automation.selenium.matchers.component;

import static org.hamcrest.core.Is.is;

import org.hamcrest.Description;
import org.hamcrest.Factory;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;

import com.ihg.automation.selenium.gwt.components.Componentable;
import com.ihg.automation.selenium.matchers.text.StringConverterMatcherBase;

public class HasText extends TypeSafeMatcher<Componentable>
{
    private Matcher<String> matcher;
    private boolean isWait;

    private HasText(Matcher<String> matcher, boolean isWait)
    {
        this.matcher = matcher;
        this.isWait = isWait;
    }

    private HasText(Matcher<String> matcher)
    {
        this(matcher, true);
    }

    @Override
    public void describeTo(Description description)
    {
        description.appendText("text ").appendDescriptionOf(matcher);
    }

    @Override
    protected void describeMismatchSafely(Componentable item, Description mismatchDescription)
    {
        mismatchDescription.appendText("text was ").appendValue(item.getText(isWait));

        if (matcher instanceof StringConverterMatcherBase<?>)
        {
            ((StringConverterMatcherBase<?>) matcher).describeMismatchForParent(mismatchDescription);
        }
    }

    @Override
    protected boolean matchesSafely(Componentable component)
    {
        return matcher.matches(component.getText(isWait));
    }

    @Factory
    public static Matcher<Componentable> hasText(final String text)
    {
        return new HasText(is(text));
    }

    @Factory
    public static Matcher<Componentable> hasText(final Matcher<String> matcher)
    {
        return new HasText(matcher);
    }

    @Factory
    public static Matcher<Componentable> hasTextNoWait(final Matcher<String> matcher)
    {
        return new HasText(matcher, false);
    }

}
