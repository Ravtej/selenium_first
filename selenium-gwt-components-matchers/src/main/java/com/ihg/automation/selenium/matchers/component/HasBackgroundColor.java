package com.ihg.automation.selenium.matchers.component;

import static org.hamcrest.core.Is.is;

import org.hamcrest.Description;
import org.hamcrest.Factory;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.openqa.selenium.support.Color;

import com.ihg.automation.selenium.gwt.components.Component;

public class HasBackgroundColor extends TypeSafeMatcher<Component>
{
    private Matcher<String> matcher;

    private HasBackgroundColor(Matcher<String> matcher)
    {
        this.matcher = matcher;
    }

    @Override
    public void describeTo(Description description)
    {
        description.appendText("background-color ").appendDescriptionOf(matcher);
    }

    @Override
    protected void describeMismatchSafely(Component item, Description mismatchDescription)
    {
        String colorAsHex = getColorAsHex(item);
        mismatchDescription.appendText("background-color was ").appendValue(colorAsHex);
    }

    @Override
    protected boolean matchesSafely(Component component)
    {
        String colorAsHex = getColorAsHex(component);
        return matcher.matches(colorAsHex);
    }

    @Factory
    public static Matcher<Component> hasBackgroundColor(final String text)
    {
        return new HasBackgroundColor(is(text));
    }

    private String getColorAsHex(Component component)
    {
        Color color = Color.fromString(component.getCssValue("background-color"));
        return color.asHex();
    }
}
