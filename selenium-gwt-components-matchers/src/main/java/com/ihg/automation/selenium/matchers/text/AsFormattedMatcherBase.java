package com.ihg.automation.selenium.matchers.text;

import static org.hamcrest.core.Is.is;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;

public class AsFormattedMatcherBase extends TypeSafeMatcher<String>
{
    private Object expectedObject;
    private String expectedFormattedText;

    private Matcher<String> matcher;

    protected AsFormattedMatcherBase(Object object, String formattedText)
    {
        expectedObject = object;
        expectedFormattedText = formattedText;
        matcher = is(expectedFormattedText);
    }

    @Override
    protected boolean matchesSafely(String item)
    {
        return matcher.matches(item);
    }

    @Override
    public void describeTo(Description description)
    {
        description.appendValue(expectedObject).appendText(" formatted as ").appendDescriptionOf(matcher);
    }

}
