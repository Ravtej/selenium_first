package com.ihg.automation.selenium.matchers.date;

import org.hamcrest.Description;
import org.hamcrest.Factory;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.joda.time.LocalDate;

public class IsAfterOrEqualsDate extends TypeSafeMatcher<LocalDate>
{
    private final LocalDate expected;

    public IsAfterOrEqualsDate(final LocalDate expected)
    {
        this.expected = expected;
    }

    @Override
    protected boolean matchesSafely(final LocalDate actual)
    {
        return actual.isAfter(expected) || actual.isEqual(expected);
    }

    @Override
    protected void describeMismatchSafely(LocalDate item, Description mismatchDescription)
    {
        mismatchDescription.appendText("date is ").appendValue(item);
    }

    public void describeTo(final Description description)
    {
        description.appendText("date is after ").appendValue(expected);
    }

    @Factory
    public static Matcher<LocalDate> isAfterOrEquals(final LocalDate date)
    {
        return new IsAfterOrEqualsDate(date);
    }
}
