package com.ihg.automation.selenium.matchers.text;

import java.util.Arrays;
import java.util.Collection;

import org.hamcrest.Description;
import org.hamcrest.Factory;
import org.hamcrest.Matcher;

public class AsCollection extends StringConverterMatcherBase<Collection<String>>
{
    public static final String DEFAULT_LIST_SEPARATOR = ", ";
    private String separator;

    protected AsCollection(Matcher<Collection<String>> matcher, String separator)
    {
        super(matcher);
        this.separator = separator;
    }

    @Override
    public Collection<String> convert(String item)
    {
        return Arrays.asList(item.split(separator));
    }

    @Override
    public void describeMismatchForParent(Description mismatchDescription)
    {
        super.describeMismatchForParent(mismatchDescription);
        mismatchDescription.appendText(":");
        getMatcher().describeMismatch(getConvertedString(), mismatchDescription);
    }

    @Override
    public void appendConvertToDescription(Description description)
    {
        description.appendText("Collection split by ").appendValue(separator).appendText(" separator");
    }

    @Factory
    public static AsCollection asCollection(Matcher<Collection<String>> matcher)
    {
        return new AsCollection(matcher, DEFAULT_LIST_SEPARATOR);
    }

    @Factory
    public static AsCollection asCollection(String separator, Matcher<Collection<String>> matcher)
    {
        return new AsCollection(matcher, separator);
    }

}
