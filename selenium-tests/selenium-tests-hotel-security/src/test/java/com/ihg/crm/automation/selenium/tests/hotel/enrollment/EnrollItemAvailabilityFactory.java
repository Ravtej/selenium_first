package com.ihg.crm.automation.selenium.tests.hotel.enrollment;

import static com.ihg.crm.automation.selenium.tests.hotel.PermissionConstant.Enroll.ENROLLMENT_ENROLL;
import static com.ihg.crm.automation.selenium.tests.hotel.PermissionConstant.Enroll.ENROLL_AMB;
import static com.ihg.crm.automation.selenium.tests.hotel.PermissionConstant.Enroll.ENROLL_BR;
import static com.ihg.crm.automation.selenium.tests.hotel.PermissionConstant.Enroll.ENROLL_DR;
import static com.ihg.crm.automation.selenium.tests.hotel.PermissionConstant.Enroll.ENROLL_EMP;
import static com.ihg.crm.automation.selenium.tests.hotel.PermissionConstant.Enroll.ENROLL_HTL;
import static com.ihg.crm.automation.selenium.tests.hotel.PermissionConstant.Enroll.ENROLL_IHG;
import static com.ihg.crm.automation.selenium.tests.hotel.PermissionConstant.Enroll.ENROLL_PC;

import java.util.HashMap;
import java.util.Map;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Factory;

import com.ihg.crm.automation.selenium.tests.hotel.SecurityMatrix;

public class EnrollItemAvailabilityFactory
{
    @DataProvider(name = "verifyCatalogItemDetailsAvailabilityProvider")
    public Object[][] verifyCatalogItemDetailsAvailabilityProvider()
    {
        Map<String, Boolean> defaultPermissions = setPermissionMap();

        SecurityMatrix permissions = new SecurityMatrix(defaultPermissions);

        permissions.getHotelSecurity().put(ENROLLMENT_ENROLL, false);
        permissions.getHotelSecurity().put(ENROLL_PC, false);
        permissions.getHotelSecurity().put(ENROLL_EMP, false);
        permissions.getHotelSecurity().put(ENROLL_AMB, false);
        permissions.getHotelSecurity().put(ENROLL_DR, false);
        permissions.getHotelSecurity().put(ENROLL_BR, false);
        permissions.getHotelSecurity().put(ENROLL_IHG, false);
        permissions.getHotelSecurity().put(ENROLL_HTL, false);

        permissions.getHotelFrontDeskStandard().put(ENROLL_AMB, false);
        permissions.getHotelFrontDeskStandard().put(ENROLL_DR, false);
        permissions.getHotelFrontDeskStandard().put(ENROLL_IHG, false);
        permissions.getHotelFrontDeskStandard().put(ENROLL_HTL, false);

        permissions.getHotelBackOffice().put(ENROLLMENT_ENROLL, false);
        permissions.getHotelBackOffice().put(ENROLL_PC, false);
        permissions.getHotelBackOffice().put(ENROLL_EMP, false);
        permissions.getHotelBackOffice().put(ENROLL_AMB, false);
        permissions.getHotelBackOffice().put(ENROLL_DR, false);
        permissions.getHotelBackOffice().put(ENROLL_BR, false);
        permissions.getHotelBackOffice().put(ENROLL_IHG, false);
        permissions.getHotelBackOffice().put(ENROLL_HTL, false);

        permissions.getHotelManager().put(ENROLL_IHG, false);
        permissions.getHotelManager().put(ENROLL_HTL, false);

        permissions.getHotelOperationsManager().put(ENROLLMENT_ENROLL, false);
        permissions.getHotelOperationsManager().put(ENROLL_PC, false);
        permissions.getHotelOperationsManager().put(ENROLL_EMP, false);
        permissions.getHotelOperationsManager().put(ENROLL_AMB, false);
        permissions.getHotelOperationsManager().put(ENROLL_DR, false);
        permissions.getHotelOperationsManager().put(ENROLL_BR, false);
        permissions.getHotelOperationsManager().put(ENROLL_IHG, false);
        permissions.getHotelOperationsManager().put(ENROLL_HTL, false);

        permissions.getHotelFrontDeskFeeBased().put(ENROLL_IHG, false);
        permissions.getHotelFrontDeskFeeBased().put(ENROLL_HTL, false);

        permissions.getSalesManager().put(ENROLL_AMB, false);
        permissions.getSalesManager().put(ENROLL_DR, false);
        permissions.getSalesManager().put(ENROLL_IHG, false);
        permissions.getSalesManager().put(ENROLL_HTL, false);

        return permissions.getPermissionArray();
    }

    private Map<String, Boolean> setPermissionMap()
    {
        Map<String, Boolean> permissions = new HashMap<String, Boolean>();

        permissions.put(ENROLLMENT_ENROLL, true);
        permissions.put(ENROLL_PC, true);
        permissions.put(ENROLL_EMP, true);
        permissions.put(ENROLL_AMB, true);
        permissions.put(ENROLL_DR, true);
        permissions.put(ENROLL_BR, true);
        permissions.put(ENROLL_IHG, true);
        permissions.put(ENROLL_HTL, true);

        return permissions;
    }

    @Factory(dataProvider = "verifyCatalogItemDetailsAvailabilityProvider")
    public Object[] createInstances(String role, Map<String, Boolean> permissions)
    {
        return new Object[] { new EnrollItemAvailabilityFactoryTest(role, permissions) };
    }
}
