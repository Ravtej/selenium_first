package com.ihg.crm.automation.selenium.tests.hotel.operations;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.crm.automation.selenium.tests.hotel.PermissionConstant.HotelOperations.HOTEL_OPERATION_BUTTON;
import static com.ihg.crm.automation.selenium.tests.hotel.PermissionConstant.HotelOperations.LPU_SCREEN_V;
import static com.ihg.crm.automation.selenium.tests.hotel.PermissionConstant.HotelOperations.MANAGE_EMP_RATE_ELIGIBILITY_M;
import static com.ihg.crm.automation.selenium.tests.hotel.PermissionConstant.HotelOperations.ORDER_POINT_VOUCHERS_A;
import static com.ihg.crm.automation.selenium.tests.hotel.PermissionConstant.HotelOperations.SECURITY_V;
import static com.ihg.crm.automation.selenium.tests.hotel.PermissionConstant.HotelOperations.STAY_A;
import static com.ihg.crm.automation.selenium.tests.hotel.PermissionConstant.HotelOperations.STAY_V;

import java.util.Map;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.hotel.pages.LeftPanel;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.HotelOperationsPage;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.stay.lpu.LoyaltyPendingUpdatesPage;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.stay.stays.ReviewGuestStaysSearchPage;
import com.ihg.automation.selenium.hotel.pages.security.security.UserManagementPage;
import com.ihg.crm.automation.selenium.tests.hotel.LoginLogout;

public class StayAvailabilityFactoryTest extends LoginLogout
{
    private LeftPanel leftPanel;
    private HotelOperationsPage hotelOperationsPage;

    public StayAvailabilityFactoryTest(String role, Map<String, Boolean> permissions)
    {
        super(role, permissions);
    }

    @BeforeClass
    public void beforeClass()
    {
        login();
    }

    @BeforeMethod
    public void beforeMethod()
    {
        leftPanel = new LeftPanel();
        leftPanel.getGuestSearch().clickAndWait();
        leftPanel.getHotelOperations().clickAndWait();
    }

    @Test
    public void verifyButtonsOnLeftPanel()
    {
        verifyThat(leftPanel.getHotelOperations(), displayed(permissions.get(HOTEL_OPERATION_BUTTON)));
        verifyThat(leftPanel.getOrderPointVoucher(), displayed(permissions.get(ORDER_POINT_VOUCHERS_A)));
        verifyThat(leftPanel.getManageEmpRateEligibility(), displayed(permissions.get(MANAGE_EMP_RATE_ELIGIBILITY_M)));
    }

    @Test
    public void verifyHotelSecurity()
    {
        verifyThat(leftPanel.getHotelSecurity(), displayed(permissions.get(SECURITY_V)));

        if (leftPanel.getHotelSecurity().isDisplayed())
        {
            leftPanel.getHotelSecurity().click();
            verifyThat(new UserManagementPage(), displayed(permissions.get(SECURITY_V)));
        }
    }

    @Test
    public void verifyViewStayTab()
    {
        hotelOperationsPage = new HotelOperationsPage();
        verifyThat(hotelOperationsPage.getReviewGuestStays(), displayed(permissions.get(STAY_V)));

        if (permissions.get(STAY_V))
        {
            hotelOperationsPage.getReviewGuestStays().clickAndWait();

            ReviewGuestStaysSearchPage staysPage = new ReviewGuestStaysSearchPage();
            verifyThat(staysPage, displayed(true));
            verifyThat(staysPage.getButtonBar().getSearch(), displayed(true));
            verifyThat(staysPage.getStayGrid(), displayed(true));
        }

    }

    @Test
    public void verifyLoyaltyPendingUpdatesView()
    {
        hotelOperationsPage = new HotelOperationsPage();

        verifyThat(hotelOperationsPage.getLoyaltyPendingUpdates(), displayed(permissions.get(LPU_SCREEN_V)));

        if (permissions.get(LPU_SCREEN_V))
        {
            hotelOperationsPage.getLoyaltyPendingUpdates().clickAndWait();

            verifyThat(new LoyaltyPendingUpdatesPage(), displayed(true));
        }
    }

    @Test
    public void verifyLoyaltyPendingUpdatesEdit()
    {
        if (permissions.get(LPU_SCREEN_V))
        {
            new HotelOperationsPage().getLoyaltyPendingUpdates().clickAndWait();
            LoyaltyPendingUpdatesPage loyaltyPendingUpdatesPage = new LoyaltyPendingUpdatesPage();
            // TODO M
        }
    }

    @Test
    public void verifyCreateMissingStay()
    {
        if (permissions.get(LPU_SCREEN_V))
        {
            new HotelOperationsPage().getLoyaltyPendingUpdates().clickAndWait();

            verifyThat(new LoyaltyPendingUpdatesPage().getCreateMissingStay(), displayed(permissions.get(STAY_A)));
        }
    }
}
