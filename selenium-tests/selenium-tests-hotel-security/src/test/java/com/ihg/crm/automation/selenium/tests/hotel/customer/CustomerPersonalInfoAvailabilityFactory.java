package com.ihg.crm.automation.selenium.tests.hotel.customer;

import static com.ihg.crm.automation.selenium.tests.hotel.PermissionConstant.Guest.CONTACT_EMAIL_M;
import static com.ihg.crm.automation.selenium.tests.hotel.PermissionConstant.Guest.GUEST_CONTACT_PHONE_ADDRESS_BIRTH_A;
import static com.ihg.crm.automation.selenium.tests.hotel.PermissionConstant.Guest.GUEST_CONTACT_PHONE_ADDRESS_BIRTH_M;
import static com.ihg.crm.automation.selenium.tests.hotel.PermissionConstant.Guest.GUEST_CONTACT_PHONE_ADDRESS_BIRTH_V;
import static com.ihg.crm.automation.selenium.tests.hotel.PermissionConstant.Guest.GUEST_PERSONAL_V;

import java.util.HashMap;
import java.util.Map;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Factory;

import com.ihg.crm.automation.selenium.tests.hotel.SecurityMatrix;

public class CustomerPersonalInfoAvailabilityFactory
{
    @DataProvider(name = "verifyCustomerAvailabilityProvider")
    public Object[][] verifyCustomerAvailabilityProvider()
    {
        Map<String, Boolean> defaultPermissions = setPermissionMap();
        SecurityMatrix permissions = new SecurityMatrix(defaultPermissions);

        permissions.getHotelSecurity().put(GUEST_CONTACT_PHONE_ADDRESS_BIRTH_A, false);
        permissions.getHotelSecurity().put(GUEST_CONTACT_PHONE_ADDRESS_BIRTH_M, false);
        permissions.getHotelSecurity().put(CONTACT_EMAIL_M, false);

        permissions.getHotelBackOffice().put(GUEST_CONTACT_PHONE_ADDRESS_BIRTH_A, false);
        permissions.getHotelBackOffice().put(GUEST_CONTACT_PHONE_ADDRESS_BIRTH_M, false);
        permissions.getHotelBackOffice().put(CONTACT_EMAIL_M, false);

        permissions.getHotelOperationsManager().put(GUEST_CONTACT_PHONE_ADDRESS_BIRTH_A, false);
        permissions.getHotelOperationsManager().put(GUEST_CONTACT_PHONE_ADDRESS_BIRTH_M, false);
        permissions.getHotelOperationsManager().put(CONTACT_EMAIL_M, false);

        return permissions.getPermissionArray();
    }

    private Map<String, Boolean> setPermissionMap()
    {
        Map<String, Boolean> permissions = new HashMap<String, Boolean>();

        permissions.put(GUEST_PERSONAL_V, true);
        permissions.put(GUEST_CONTACT_PHONE_ADDRESS_BIRTH_A, true);
        permissions.put(GUEST_CONTACT_PHONE_ADDRESS_BIRTH_M, true);
        permissions.put(GUEST_CONTACT_PHONE_ADDRESS_BIRTH_V, true);
        permissions.put(CONTACT_EMAIL_M, true);

        return permissions;
    }

    @Factory(dataProvider = "verifyCustomerAvailabilityProvider")
    public Object[] createInstances(String role, Map<String, Boolean> permissions)
    {
        return new Object[] { new CustomerPersonalInfoAvailabilityFactoryTest(role, permissions) };
    }
}
