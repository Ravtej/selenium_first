package com.ihg.crm.automation.selenium.tests.hotel.program;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.isDisplayedAndEnabled;
import static com.ihg.crm.automation.selenium.tests.hotel.PermissionConstant.Program.ALLIANCE_V;
import static com.ihg.crm.automation.selenium.tests.hotel.PermissionConstant.Program.AMB_TAB_V;
import static com.ihg.crm.automation.selenium.tests.hotel.PermissionConstant.Program.BR_TAB_V;
import static com.ihg.crm.automation.selenium.tests.hotel.PermissionConstant.Program.DR_TAB_V;
import static com.ihg.crm.automation.selenium.tests.hotel.PermissionConstant.Program.EMPLOYEE_RATE_ELIGIBILITY_M;
import static com.ihg.crm.automation.selenium.tests.hotel.PermissionConstant.Program.EMPLOYEE_RATE_ELIGIBILITY_V;
import static com.ihg.crm.automation.selenium.tests.hotel.PermissionConstant.Program.EMP_TAB_V;
import static com.ihg.crm.automation.selenium.tests.hotel.PermissionConstant.Program.PC_TAB_V;
import static com.ihg.crm.automation.selenium.tests.hotel.PermissionConstant.Program.PROGRAM_MEMBER_POINT_BALANCE_V;
import static com.ihg.crm.automation.selenium.tests.hotel.PermissionConstant.Program.RENEW_IN_PROGRAM;
import static com.ihg.crm.automation.selenium.tests.hotel.PermissionConstant.Program.YEARLY_ACHIEVEMENT_SUMMARY;

import java.util.Map;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.pages.login.TopUserSection;
import com.ihg.automation.selenium.common.pages.program.EmployeeRateEligibility;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.hotel.pages.Tabs;
import com.ihg.automation.selenium.hotel.pages.programs.amb.AmbassadorPage;
import com.ihg.automation.selenium.hotel.pages.programs.dr.DiningRewardsPage;
import com.ihg.automation.selenium.hotel.pages.programs.rc.EarningPreference;
import com.ihg.automation.selenium.hotel.pages.programs.rc.RewardClubPage;
import com.ihg.crm.automation.selenium.tests.hotel.LoginLogout;

public class ProgramAvailabilityFactoryTest extends LoginLogout
{
    private Tabs.ProgramInfoTab programInfoTab = new Tabs().new ProgramInfoTab();
    private RewardClubPage rewardClubPage = new RewardClubPage();

    public ProgramAvailabilityFactoryTest(String role, Map<String, Boolean> permissions)
    {
        super(role, permissions);
    }

    @BeforeClass
    private void beforeClass()
    {
        loginAndOpenMember(memberId);
    }

    @Test(priority = 10)
    public void verifyViewProgramInfo()
    {
        verifyThat(programInfoTab, displayed(permissions.get(PROGRAM_MEMBER_POINT_BALANCE_V)));
    }

    @Test(priority = 20)
    public void verifyPriorityClubRewardsTab()
    {
        if (permissions.get(PROGRAM_MEMBER_POINT_BALANCE_V))
        {
            programInfoTab.goTo();
            verifyThat(programInfoTab.getProgram(Program.RC), displayed(permissions.get(PC_TAB_V)));
        }
    }

    @Test(priority = 20)
    public void verifyAllianceView()
    {
        if (permissions.get(PC_TAB_V))
        {
            rewardClubPage.goTo();
            EarningPreference earningPreference = rewardClubPage.getEarningDetails();
            verifyThat(earningPreference, displayed(permissions.get(ALLIANCE_V)));
            verifyThat(earningPreference.getAlliances(), displayed(permissions.get(ALLIANCE_V)));
        }
    }

    @Test(priority = 20)
    public void verifyYearlyAchievementSummary()
    {
        if (permissions.get(PC_TAB_V))
        {
            rewardClubPage.goTo();
            verifyThat(rewardClubPage.getCurrentYearAnnualActivities(),
                    displayed(permissions.get(YEARLY_ACHIEVEMENT_SUMMARY)));
        }
    }

    @Test(priority = 20)
    public void verifyEmployeeRateEligibility()
    {
        if (permissions.get(PC_TAB_V))
        {
            EmployeeRateEligibility employeeRateEligibility = rewardClubPage.getEmployeeRateEligibility();
            rewardClubPage.goTo();
            verifyThat(employeeRateEligibility, displayed(permissions.get(EMPLOYEE_RATE_ELIGIBILITY_V)));
            verifyThat(employeeRateEligibility.getEdit(),
                    isDisplayedAndEnabled(permissions.get(EMPLOYEE_RATE_ELIGIBILITY_M)));
        }
    }

    @Test(priority = 20)
    public void verifyEmployeeAccountTab()
    {
        if (permissions.get(PROGRAM_MEMBER_POINT_BALANCE_V))
        {
            programInfoTab.goTo();
            verifyThat(programInfoTab.getProgram(Program.EMP), displayed(permissions.get(EMP_TAB_V)));
        }
    }

    @Test(priority = 30)
    public void verifyAmbassadorAccountTab()
    {
        if (permissions.get(PROGRAM_MEMBER_POINT_BALANCE_V))
        {
            new TopUserSection().getHotel().typeAndWait("SHGHA");
            searchNewMember(memberId);

            programInfoTab.goTo();
            verifyThat(programInfoTab.getProgram(Program.AMB), displayed(permissions.get(AMB_TAB_V)));
        }
    }

    @Test(priority = 40)
    public void verifyRenewAMB()
    {
        if (permissions.get(AMB_TAB_V))
        {
            AmbassadorPage ambassadorPage = new AmbassadorPage();
            ambassadorPage.goTo();
            verifyThat(ambassadorPage.getSummary().getRenew(), displayed(permissions.get(RENEW_IN_PROGRAM)));
        }
    }

    @Test(priority = 50)
    public void verifyDiningRewardTab()
    {
        if (permissions.get(PROGRAM_MEMBER_POINT_BALANCE_V))
        {
            programInfoTab.goTo();
            verifyThat(programInfoTab.getProgram(Program.DR), displayed(permissions.get(DR_TAB_V)));
        }
    }

    @Test(priority = 60)
    public void verifyRenewDR()
    {
        if (permissions.get(DR_TAB_V))
        {
            DiningRewardsPage diningRewardsPage = new DiningRewardsPage();
            diningRewardsPage.goTo();
            verifyThat(diningRewardsPage.getSummary().getRenew(), displayed(false));
        }
    }

    @Test(priority = 70)
    public void verifyBusinessRewardView()
    {
        if (permissions.get(PROGRAM_MEMBER_POINT_BALANCE_V))
        {
            programInfoTab.goTo();
            verifyThat(programInfoTab.getProgram(Program.BR), displayed(permissions.get(BR_TAB_V)));
        }
    }
}
