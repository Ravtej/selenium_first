package com.ihg.crm.automation.selenium.tests.hotel.customer;

import static com.ihg.crm.automation.selenium.tests.hotel.PermissionConstant.Communication.GUEST_MARKET_PREF_M;
import static com.ihg.crm.automation.selenium.tests.hotel.PermissionConstant.Communication.GUEST_MARKET_PREF_V;

import java.util.HashMap;
import java.util.Map;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Factory;

import com.ihg.crm.automation.selenium.tests.hotel.SecurityMatrix;

public class CustomerCommunicationPreferencesFactory
{
    @DataProvider(name = "verifyCommunicationPreferencesProvider")
    public Object[][] verifyCommunicationPreferencesProvider()
    {
        Map<String, Boolean> defaultPermissions = setPermissionMap();

        SecurityMatrix permissions = new SecurityMatrix(defaultPermissions);

        permissions.getHotelSecurity().put(GUEST_MARKET_PREF_M, false);
        permissions.getHotelSecurity().put(GUEST_MARKET_PREF_V, false);

        permissions.getHotelBackOffice().put(GUEST_MARKET_PREF_M, false);
        permissions.getHotelBackOffice().put(GUEST_MARKET_PREF_V, false);

        permissions.getHotelOperationsManager().put(GUEST_MARKET_PREF_M, false);
        permissions.getHotelOperationsManager().put(GUEST_MARKET_PREF_V, false);

        return permissions.getPermissionArray();
    }

    private Map<String, Boolean> setPermissionMap()
    {
        Map<String, Boolean> permissions = new HashMap<String, Boolean>();

        permissions.put(GUEST_MARKET_PREF_M, true);
        permissions.put(GUEST_MARKET_PREF_V, true);

        return permissions;
    }

    @Factory(dataProvider = "verifyCommunicationPreferencesProvider")
    public Object[] createInstances(String role, Map<String, Boolean> permissions)
    {
        return new Object[] { new CustomerCommunicationPreferencesFactoryTest(role, permissions) };
    }
}
