package com.ihg.crm.automation.selenium.tests.hotel;

import static com.ihg.automation.selenium.hotel.pages.Role.HOTEL_BACK_OFFICE;
import static com.ihg.automation.selenium.hotel.pages.Role.HOTEL_FRONT_DESK_FEE_BASED;
import static com.ihg.automation.selenium.hotel.pages.Role.HOTEL_FRONT_DESK_STANDARD;
import static com.ihg.automation.selenium.hotel.pages.Role.HOTEL_MANAGER;
import static com.ihg.automation.selenium.hotel.pages.Role.HOTEL_OPERATIONS_MANAGER;
import static com.ihg.automation.selenium.hotel.pages.Role.HOTEL_SECURITY;
import static com.ihg.automation.selenium.hotel.pages.Role.SALES_MANAGER;

import java.util.HashMap;
import java.util.Map;

import com.ihg.automation.security.SecurityMatrixBase;

public class SecurityMatrix extends SecurityMatrixBase
{
    public SecurityMatrix(Map<String, Boolean> defaultPermissions)
    {
        securityMatrix.put(HOTEL_BACK_OFFICE, new HashMap<>(defaultPermissions));
        securityMatrix.put(HOTEL_FRONT_DESK_FEE_BASED, new HashMap<>(defaultPermissions));
        securityMatrix.put(HOTEL_FRONT_DESK_STANDARD, new HashMap<>(defaultPermissions));
        securityMatrix.put(HOTEL_MANAGER, new HashMap<>(defaultPermissions));
        securityMatrix.put(HOTEL_OPERATIONS_MANAGER, new HashMap<>(defaultPermissions));
        securityMatrix.put(HOTEL_SECURITY, new HashMap<>(defaultPermissions));
        securityMatrix.put(SALES_MANAGER, new HashMap<>(defaultPermissions));
    }

    public Map<String, Boolean> getHotelBackOffice()
    {
        return securityMatrix.get(HOTEL_BACK_OFFICE);
    }

    public Map<String, Boolean> getHotelFrontDeskFeeBased()
    {
        return securityMatrix.get(HOTEL_FRONT_DESK_FEE_BASED);
    }

    public Map<String, Boolean> getHotelFrontDeskStandard()
    {
        return securityMatrix.get(HOTEL_FRONT_DESK_STANDARD);
    }

    public Map<String, Boolean> getHotelManager()
    {
        return securityMatrix.get(HOTEL_MANAGER);
    }

    public Map<String, Boolean> getHotelOperationsManager()
    {
        return securityMatrix.get(HOTEL_OPERATIONS_MANAGER);
    }

    public Map<String, Boolean> getHotelSecurity()
    {
        return securityMatrix.get(HOTEL_SECURITY);
    }

    public Map<String, Boolean> getSalesManager()
    {
        return securityMatrix.get(SALES_MANAGER);
    }
}
