package com.ihg.crm.automation.selenium.tests.hotel.operations;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasDefault;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static com.ihg.crm.automation.selenium.tests.hotel.PermissionConstant.HotelOperations.LOW_OCCUPANCY_RATES_V;
import static com.ihg.crm.automation.selenium.tests.hotel.PermissionConstant.HotelOperations.OCCUPANCY_M;
import static com.ihg.crm.automation.selenium.tests.hotel.PermissionConstant.HotelOperations.OCCUPANCY_V;
import static com.ihg.crm.automation.selenium.tests.hotel.PermissionConstant.HotelOperations.REIMBURSEMENT_AND_CERTIFICATE_V;
import static com.ihg.crm.automation.selenium.tests.hotel.PermissionConstant.HotelOperations.TAX_AND_FEE_V;

import java.util.Map;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.gwt.components.grid.GridCell;
import com.ihg.automation.selenium.hotel.pages.LeftPanel;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.HotelOperationsPage;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.reimbursement.certificate.CertificateSearchPage;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.reimbursement.occupancy.OccupancyAndADREntryPage;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.reimbursement.occupancy.OccupancyAndADREntrySearchGridRow;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.reimbursement.rewardnight.RewardNightsSettingsPage;
import com.ihg.crm.automation.selenium.tests.hotel.LoginLogout;

public class RewardNightAvailabilityFactoryTest extends LoginLogout
{
    private HotelOperationsPage hotelOperationsPage;

    public RewardNightAvailabilityFactoryTest(String role, Map<String, Boolean> permissions)
    {
        super(role, permissions);
    }

    @BeforeClass
    public void beforeClass()
    {
        login();
    }

    @BeforeMethod
    public void beforeMethod()
    {
        LeftPanel leftPanel = new LeftPanel();
        leftPanel.getGuestSearch().clickAndWait();
        leftPanel.getHotelOperations().clickAndWait();
    }

    @Test
    public void verifyViewOccupancy()
    {
        hotelOperationsPage = new HotelOperationsPage();

        verifyThat(hotelOperationsPage.getProcessHotelReimbForRewardFreeNights(),
                displayed(permissions.get(OCCUPANCY_V)));

        if (permissions.get(OCCUPANCY_V))
        {
            hotelOperationsPage.getProcessHotelReimbForRewardFreeNights().clickAndWait();

            verifyThat(new OccupancyAndADREntryPage(), displayed(true));
        }
    }

    @Test
    public void verifyEditOccupancyTab()
    {
        if (permissions.get(OCCUPANCY_V))
        {
            new HotelOperationsPage().getProcessHotelReimbForRewardFreeNights().clickAndWait();

            GridCell adjustCell = new OccupancyAndADREntryPage().getOccupancyAndADREntrySearchGrid().getRow(1)
                    .getCell(OccupancyAndADREntrySearchGridRow.OccupancyAndADREntrySearchGridCell.ADJUST_ACTION);

            // TODO adjust link is not in every row
            verifyThat(adjustCell.asLink(), displayed(permissions.get(OCCUPANCY_M)));

            if (permissions.get(OCCUPANCY_M))
            {
                verifyThat(adjustCell, hasText("Adjust"));
            }
            else
            {
                verifyThat(adjustCell, hasDefault());
            }
        }
    }

    @Test
    public void verifyViewRewardNightsSettingsPage()
    {
        hotelOperationsPage = new HotelOperationsPage();

        verifyThat(hotelOperationsPage.getHotelsRewardNightsSettings(), displayed(permissions.get(TAX_AND_FEE_V)));

        if (permissions.get(TAX_AND_FEE_V))
        {
            hotelOperationsPage.getHotelsRewardNightsSettings().clickAndWait();

            RewardNightsSettingsPage rewardNightsSettingsPage = new RewardNightsSettingsPage();

            verifyThat(rewardNightsSettingsPage, displayed(true));
            verifyThat(rewardNightsSettingsPage.getTaxesAndFees(), displayed(true));
            verifyThat(rewardNightsSettingsPage.getLowOccupancyReimbursementAmount(),
                    displayed(permissions.get(LOW_OCCUPANCY_RATES_V)));
        }
    }

    @Test
    public void verifyViewCertificatePage()
    {
        hotelOperationsPage = new HotelOperationsPage();

        verifyThat(hotelOperationsPage.getSearchRewardFreeNightsStayByCertificate(),
                displayed(permissions.get(REIMBURSEMENT_AND_CERTIFICATE_V)));

        if (permissions.get(REIMBURSEMENT_AND_CERTIFICATE_V))
        {
            hotelOperationsPage.getSearchRewardFreeNightsStayByCertificate().clickAndWait();

            verifyThat(new CertificateSearchPage(), displayed(true));
        }
    }
}
