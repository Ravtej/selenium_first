package com.ihg.crm.automation.selenium.tests.hotel.enrollment;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.crm.automation.selenium.tests.hotel.PermissionConstant.Enroll.ENROLLMENT_ENROLL;
import static com.ihg.crm.automation.selenium.tests.hotel.PermissionConstant.Enroll.ENROLL_AMB;
import static com.ihg.crm.automation.selenium.tests.hotel.PermissionConstant.Enroll.ENROLL_BR;
import static com.ihg.crm.automation.selenium.tests.hotel.PermissionConstant.Enroll.ENROLL_EMP;
import static com.ihg.crm.automation.selenium.tests.hotel.PermissionConstant.Enroll.ENROLL_HTL;
import static com.ihg.crm.automation.selenium.tests.hotel.PermissionConstant.Enroll.ENROLL_IHG;
import static com.ihg.crm.automation.selenium.tests.hotel.PermissionConstant.Enroll.ENROLL_PC;

import java.util.Map;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.pages.login.TopUserSection;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.hotel.pages.LeftPanel;
import com.ihg.automation.selenium.hotel.pages.enroll.EnrollmentPage;
import com.ihg.crm.automation.selenium.tests.hotel.LoginLogout;

public class EnrollItemAvailabilityFactoryTest extends LoginLogout
{
    private LeftPanel leftMenu = new LeftPanel();

    public EnrollItemAvailabilityFactoryTest(String role, Map<String, Boolean> permissions)
    {
        super(role, permissions);
    }

    @BeforeClass
    private void beforeClass()
    {
        login();
    }

    @Test(priority = 10)
    public void verifyMenuEnrollButtonAvailability()
    {
        verifyThat(leftMenu.getEnrollment(), displayed(permissions.get(ENROLLMENT_ENROLL)));
    }

    @Test(priority = 20)
    public void verifyCanEnrollPCRMember()
    {
        verifyCanEnrollMember(Program.RC, permissions.get(ENROLL_PC));
    }

    @Test(priority = 20)
    public void verifyCanEnrollEMPMember()
    {
        verifyCanEnrollMember(Program.EMP, permissions.get(ENROLL_EMP));
    }

    @Test(priority = 20)
    public void verifyCanEnrollBRMember()
    {
        verifyCanEnrollMember(Program.BR, permissions.get(ENROLL_BR));
    }

    @Test(priority = 20)
    public void verifyCanEnrollIHGMember()
    {
        verifyCanEnrollMember(Program.IHG, permissions.get(ENROLL_IHG));
    }

    @Test(priority = 20)
    public void verifyCanEnrollHTLMember()
    {
        verifyCanEnrollMember(Program.HTL, permissions.get(ENROLL_HTL));
    }

    @Test(priority = 30)
    public void verifyCanEnrollAMBMember()
    {
        changeHotel();
        verifyCanEnrollMember(Program.AMB, permissions.get(ENROLL_AMB));
    }

    private void verifyCanEnrollMember(Program program, boolean displayed)
    {
        if (permissions.get(ENROLLMENT_ENROLL))
        {
            EnrollmentPage enrollmentPage = new EnrollmentPage();
            enrollmentPage.goTo();
            verifyThat(enrollmentPage.getPrograms().getProgramCheckbox(program), displayed(displayed));
        }
    }

    private void changeHotel()
    {
        if (permissions.get(ENROLLMENT_ENROLL))
        {
            new TopUserSection().getHotel().typeAndWait("SHGHA");
        }
    }
}
