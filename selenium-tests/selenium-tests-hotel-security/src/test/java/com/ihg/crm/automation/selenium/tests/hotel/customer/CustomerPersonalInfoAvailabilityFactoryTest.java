package com.ihg.crm.automation.selenium.tests.hotel.customer;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.assertThat;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.isDisplayedAndEnabled;
import static com.ihg.automation.selenium.matchers.component.ModeMatcher.inView;
import static com.ihg.crm.automation.selenium.tests.hotel.PermissionConstant.Guest.CONTACT_EMAIL_M;
import static com.ihg.crm.automation.selenium.tests.hotel.PermissionConstant.Guest.GUEST_CONTACT_PHONE_ADDRESS_BIRTH_A;
import static com.ihg.crm.automation.selenium.tests.hotel.PermissionConstant.Guest.GUEST_CONTACT_PHONE_ADDRESS_BIRTH_M;
import static com.ihg.crm.automation.selenium.tests.hotel.PermissionConstant.Guest.GUEST_CONTACT_PHONE_ADDRESS_BIRTH_V;
import static com.ihg.crm.automation.selenium.tests.hotel.PermissionConstant.Guest.GUEST_PERSONAL_V;

import java.util.Map;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.BirthDate;
import com.ihg.automation.selenium.hotel.pages.personal.CustomerInfoFields;
import com.ihg.automation.selenium.hotel.pages.personal.PersonalInfoPage;
import com.ihg.crm.automation.selenium.tests.hotel.LoginLogout;

public class CustomerPersonalInfoAvailabilityFactoryTest extends LoginLogout
{
    private PersonalInfoPage personalInfoPage;

    public CustomerPersonalInfoAvailabilityFactoryTest(String role, Map<String, Boolean> permissions)
    {
        super(role, permissions);
    }

    @BeforeClass
    private void beforeClass()
    {
        loginAndOpenMember(memberId);
        personalInfoPage = new PersonalInfoPage();
        personalInfoPage.goTo();
    }

    @Test
    public void verifyViewPersonalInformation()
    {
        personalInfoPage = new PersonalInfoPage();
        verifyThat(personalInfoPage, displayed(permissions.get(GUEST_PERSONAL_V)));

        CustomerInfoFields customerInfoFields = personalInfoPage.getCustomerInfo();
        verifyThat(customerInfoFields.getFullName(), displayed(permissions.get(GUEST_PERSONAL_V)));
        verifyThat(customerInfoFields.getBirthDate().getFullDate(), displayed(permissions.get(GUEST_PERSONAL_V)));
        verifyThat(customerInfoFields.getGender(), inView(displayed(permissions.get(GUEST_PERSONAL_V))));
    }

    @Test
    public void verifyEditBirthDate()
    {
        CustomerInfoFields customerInfoFields = new PersonalInfoPage().getCustomerInfo();

        assertThat(customerInfoFields.getEdit(), displayed(permissions.get(GUEST_CONTACT_PHONE_ADDRESS_BIRTH_M)));

        if (permissions.get(GUEST_CONTACT_PHONE_ADDRESS_BIRTH_M))
        {
            customerInfoFields.clickEdit();
            BirthDate birthDate = customerInfoFields.getBirthDate();
            verifyThat(birthDate.getMonth(), isDisplayedAndEnabled(true));
            verifyThat(birthDate.getDay(), displayed(true));
            customerInfoFields.clickCancel();
        }
    }

    @Test
    public void verifyAddressView()
    {
        verifyThat(new PersonalInfoPage().getAddressList(),
                displayed(permissions.get(GUEST_CONTACT_PHONE_ADDRESS_BIRTH_V)));
    }

    @Test
    public void verifyAddAddress()
    {
        verifyThat(new PersonalInfoPage().getAddressList().getAddButton(),
                isDisplayedAndEnabled(permissions.get(GUEST_CONTACT_PHONE_ADDRESS_BIRTH_A)));
    }

    @Test
    public void verifyEditAddress()
    {
        verifyThat(new PersonalInfoPage().getAddressList().getContact().getEdit(),
                isDisplayedAndEnabled(permissions.get(GUEST_CONTACT_PHONE_ADDRESS_BIRTH_M)));
    }

    @Test
    public void verifyPhoneView()
    {
        verifyThat(new PersonalInfoPage().getPhoneList(),
                displayed(permissions.get(GUEST_CONTACT_PHONE_ADDRESS_BIRTH_V)));
    }

    @Test
    public void verifyAddPhone()
    {
        verifyThat(new PersonalInfoPage().getPhoneList().getAddButton(),
                displayed(permissions.get(GUEST_CONTACT_PHONE_ADDRESS_BIRTH_A)));
    }

    @Test
    public void verifyEditPhone()
    {
        verifyThat(new PersonalInfoPage().getPhoneList().getContact().getEdit(),
                displayed(permissions.get(GUEST_CONTACT_PHONE_ADDRESS_BIRTH_M)));
    }

    @Test
    public void verifyPhoneSmsTypeView()
    {
        verifyThat(new PersonalInfoPage().getSmsList(), displayed(permissions.get(GUEST_CONTACT_PHONE_ADDRESS_BIRTH_V)));
    }

    @Test
    public void verifyPhoneSmsTypeEdit()
    {
        verifyThat(new PersonalInfoPage().getSmsList().getContact().getEdit(),
                displayed(permissions.get(GUEST_CONTACT_PHONE_ADDRESS_BIRTH_M)));
    }

    @Test
    public void verifyAddPhoneSmsType()
    {
        verifyThat(new PersonalInfoPage().getSmsList().getAddButton(),
                displayed(permissions.get(GUEST_CONTACT_PHONE_ADDRESS_BIRTH_A)));
    }

    @Test
    public void verifyAddEmail()
    {
        verifyThat(new PersonalInfoPage().getEmailList().getAddButton(), displayed(permissions.get(CONTACT_EMAIL_M)));
    }

    @Test
    public void verifyEditEmail()
    {
        verifyThat(new PersonalInfoPage().getEmailList().getContact().getEdit(),
                isDisplayedAndEnabled(permissions.get(CONTACT_EMAIL_M)));
    }
}
