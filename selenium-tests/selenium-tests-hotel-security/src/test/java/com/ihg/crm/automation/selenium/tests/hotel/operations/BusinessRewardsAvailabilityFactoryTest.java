package com.ihg.crm.automation.selenium.tests.hotel.operations;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.isDisplayedWithWait;
import static com.ihg.automation.selenium.matchers.component.IsEnabled.enabled;
import static com.ihg.crm.automation.selenium.tests.hotel.PermissionConstant.HotelOperations.APPROVE_M;
import static com.ihg.crm.automation.selenium.tests.hotel.PermissionConstant.HotelOperations.OFFERS_V;
import static com.ihg.crm.automation.selenium.tests.hotel.PermissionConstant.HotelOperations.POSTED_EVENTS_V;
import static com.ihg.crm.automation.selenium.tests.hotel.PermissionConstant.HotelOperations.REVIEW_AND_MANAGE_M;
import static com.ihg.crm.automation.selenium.tests.hotel.PermissionConstant.HotelOperations.REVIEW_AND_MANAGE_V;

import java.util.Map;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.business.BusinessRewardsEventDetailsPopUp;
import com.ihg.automation.selenium.hotel.pages.LeftPanel;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.HotelOperationsPage;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.br.manage.ManageEventsGridRow;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.br.manage.ManageEventsPage;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.br.posted.PostedEventsPage;
import com.ihg.crm.automation.selenium.tests.hotel.LoginLogout;

public class BusinessRewardsAvailabilityFactoryTest extends LoginLogout
{
    private HotelOperationsPage hotelOperationsPage;

    public BusinessRewardsAvailabilityFactoryTest(String role, Map<String, Boolean> permissions)
    {
        super(role, permissions);
    }

    @BeforeClass
    public void beforeClass()
    {
        login();
    }

    @BeforeMethod
    public void beforeMethod()
    {
        LeftPanel leftPanel = new LeftPanel();
        leftPanel.getGuestSearch().clickAndWait();
        leftPanel.getHotelOperations().clickAndWait();
    }

    @Test
    public void verifyViewManageEvents()
    {
        hotelOperationsPage = new HotelOperationsPage();
        verifyThat(hotelOperationsPage.getIHGBusinessRewardsEventsCreateReviewAndApprove(),
                displayed(permissions.get(REVIEW_AND_MANAGE_V)));

        if (permissions.get(REVIEW_AND_MANAGE_V))
        {
            hotelOperationsPage.getIHGBusinessRewardsEventsCreateReviewAndApprove().clickAndWait();

            verifyThat(new ManageEventsPage(), displayed(true));
        }
    }

    @Test
    public void verifyViewPostedEvents()
    {
        hotelOperationsPage = new HotelOperationsPage();
        verifyThat(hotelOperationsPage.getIHGBusinessRewardsEventsPosted(),
                displayed(permissions.get(POSTED_EVENTS_V)));

        if (permissions.get(POSTED_EVENTS_V))
        {
            hotelOperationsPage.getIHGBusinessRewardsEventsPosted().clickAndWait();

            PostedEventsPage postedEventsPage = new PostedEventsPage();
            verifyThat(postedEventsPage, displayed(true));
        }
    }

    @Test
    public void verifyEditBusinessDetails()
    {
        if (permissions.get(REVIEW_AND_MANAGE_V))
        {
            new HotelOperationsPage().getIHGBusinessRewardsEventsCreateReviewAndApprove().clickAndWait();

            verifyThat(new ManageEventsPage().getCreateEvent(), enabled(permissions.get(REVIEW_AND_MANAGE_M)));
        }
    }

    @Test
    public void verifyApprove()
    {
        if (permissions.get(REVIEW_AND_MANAGE_V))
        {
            new HotelOperationsPage().getIHGBusinessRewardsEventsCreateReviewAndApprove().clickAndWait();
            ManageEventsPage manageEventsPage = new ManageEventsPage();

            manageEventsPage.getSearchFields().getEventStatus().select("Pending");
            manageEventsPage.getButtonBar().getSearch().clickAndWait();

            ManageEventsGridRow row = manageEventsPage.getManageEventsGrid().getRow(1);
            verifyThat(row.getCellAsCheckBox(), enabled(permissions.get(APPROVE_M)));

            if (permissions.get(APPROVE_M))
            {
                manageEventsPage.getManageEventsGrid().getRow(1).getCellAsCheckBox().click();
                verifyThat(manageEventsPage.getApproveSelected(), enabled(true));
            }
        }
    }

    @Test(enabled = false)
    public void verifyOffer()
    {
        if (permissions.get(REVIEW_AND_MANAGE_V))
        {
            new HotelOperationsPage().getIHGBusinessRewardsEventsCreateReviewAndApprove().clickAndWait();

            new ManageEventsPage().getManageEventsGrid().getRow(1)
                    .getCell(ManageEventsGridRow.ManageEventsGridCell.EVENT_ID).asLink().clickAndWait();

            BusinessRewardsEventDetailsPopUp businessRewardsEventDetailsPopUp = new BusinessRewardsEventDetailsPopUp();
            verifyThat(businessRewardsEventDetailsPopUp, isDisplayedWithWait());

            verifyThat(businessRewardsEventDetailsPopUp.getEventSummaryTab().getOffers(),
                    displayed(permissions.get(OFFERS_V)));
        }
    }
}
