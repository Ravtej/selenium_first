package com.ihg.crm.automation.selenium.tests.hotel.customer;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.isDisplayedAndEnabled;
import static com.ihg.crm.automation.selenium.tests.hotel.PermissionConstant.Communication.GUEST_MARKET_PREF_M;
import static com.ihg.crm.automation.selenium.tests.hotel.PermissionConstant.Communication.GUEST_MARKET_PREF_V;

import java.util.Map;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.communication.ContactPermissionItems;
import com.ihg.automation.selenium.common.communication.MarketingPreferences;
import com.ihg.automation.selenium.hotel.pages.Tabs;
import com.ihg.automation.selenium.hotel.pages.communication.CommunicationPreferences;
import com.ihg.automation.selenium.hotel.pages.communication.CommunicationPreferencesPage;
import com.ihg.crm.automation.selenium.tests.hotel.LoginLogout;

public class CustomerCommunicationPreferencesFactoryTest extends LoginLogout
{
    private CommunicationPreferencesPage communicationPreferencesPage = new CommunicationPreferencesPage();
    private Tabs.CustomerInfoTab customerInfoTab = new Tabs().getCustomerInfo();

    public CustomerCommunicationPreferencesFactoryTest(String role, Map<String, Boolean> permissions)
    {
        super(role, permissions);
    }

    @BeforeClass
    private void beforeClass()
    {
        loginAndOpenMember(memberId);
        customerInfoTab.goTo();
    }

    @Test(priority = 10)
    public void verifyCommunicationPreferencesTab()
    {
        verifyThat(customerInfoTab.getCommunicationPref(), displayed(permissions.get(GUEST_MARKET_PREF_V)));
    }

    @Test(priority = 20)
    public void verifyMarketingSection()
    {
        if (permissions.get(GUEST_MARKET_PREF_V))
        {
            communicationPreferencesPage.goTo();

            CommunicationPreferences communicationPreferences = communicationPreferencesPage
                    .getCommunicationPreferences();
            verifyThat(communicationPreferences.getMarketingPreferences(), displayed(true));
            verifyThat(communicationPreferences.getSmsMobileTextingMarketingPreferences(), displayed(true));
        }
    }

    @Test(priority = 20)
    public void verifyEditButton()
    {
        if (permissions.get(GUEST_MARKET_PREF_V))
        {
            communicationPreferencesPage.goTo();

            verifyThat(communicationPreferencesPage.getCommunicationPreferences().getEdit(),
                    isDisplayedAndEnabled(permissions.get(GUEST_MARKET_PREF_M)));
        }
    }

    @Test(priority = 30)
    public void verifyEditMarketingPreferences()
    {
        if (permissions.get(GUEST_MARKET_PREF_M))
        {
            CommunicationPreferences communicationPreferences = communicationPreferencesPage
                    .getCommunicationPreferences();
            communicationPreferences.clickEdit();

            MarketingPreferences marketingPreferences = communicationPreferencesPage.getCommunicationPreferences()
                    .getMarketingPreferences();

            verifyThat(marketingPreferences.getEmailAddress(), displayed(permissions.get(GUEST_MARKET_PREF_M)));

            ContactPermissionItems marketingPermissionItems = marketingPreferences.getContactPermissionItems();
            for (int index = 1; index <= marketingPermissionItems.getContactPermissionItemsCount(); index++)
            {
                verifyThat(marketingPermissionItems.getContactPermissionItem(index).getSubscribe(),
                        displayed(permissions.get(GUEST_MARKET_PREF_M)));
            }

            ContactPermissionItems smsPermissionItems = communicationPreferencesPage.getCommunicationPreferences()
                    .getSmsMobileTextingMarketingPreferences().getContactPermissionItems();
            for (int index = 1; index <= smsPermissionItems.getContactPermissionItemsCount(); index++)
            {
                verifyThat(smsPermissionItems.getContactPermissionItem(index).getSubscribe(),
                        displayed(permissions.get(GUEST_MARKET_PREF_M)));
            }

            communicationPreferences.clickCancel();
        }
    }
}
