package com.ihg.crm.automation.selenium.tests.hotel.operations;

import static com.ihg.crm.automation.selenium.tests.hotel.PermissionConstant.HotelOperations.APPROVE_M;
import static com.ihg.crm.automation.selenium.tests.hotel.PermissionConstant.HotelOperations.FREE_NIGHT_FLAT_REIMBURSEMENT_V;
import static com.ihg.crm.automation.selenium.tests.hotel.PermissionConstant.HotelOperations.HOTEL_OPERATION_BUTTON;
import static com.ihg.crm.automation.selenium.tests.hotel.PermissionConstant.HotelOperations.LIMIT_VIEW_POINT_AWARD_A;
import static com.ihg.crm.automation.selenium.tests.hotel.PermissionConstant.HotelOperations.LOW_OCCUPANCY_RATES_V;
import static com.ihg.crm.automation.selenium.tests.hotel.PermissionConstant.HotelOperations.LPU_SCREEN_M;
import static com.ihg.crm.automation.selenium.tests.hotel.PermissionConstant.HotelOperations.LPU_SCREEN_V;
import static com.ihg.crm.automation.selenium.tests.hotel.PermissionConstant.HotelOperations.MANAGE_EMP_RATE_ELIGIBILITY_M;
import static com.ihg.crm.automation.selenium.tests.hotel.PermissionConstant.HotelOperations.OCCUPANCY_M;
import static com.ihg.crm.automation.selenium.tests.hotel.PermissionConstant.HotelOperations.OCCUPANCY_V;
import static com.ihg.crm.automation.selenium.tests.hotel.PermissionConstant.HotelOperations.OFFERS_V;
import static com.ihg.crm.automation.selenium.tests.hotel.PermissionConstant.HotelOperations.ORDER_POINT_VOUCHERS_A;
import static com.ihg.crm.automation.selenium.tests.hotel.PermissionConstant.HotelOperations.OTHER_ACTIVITY_V;
import static com.ihg.crm.automation.selenium.tests.hotel.PermissionConstant.HotelOperations.OVERRIDE_POINT_AWARD_M;
import static com.ihg.crm.automation.selenium.tests.hotel.PermissionConstant.HotelOperations.POINT_AWARD_A;
import static com.ihg.crm.automation.selenium.tests.hotel.PermissionConstant.HotelOperations.POINT_AWARD_V;
import static com.ihg.crm.automation.selenium.tests.hotel.PermissionConstant.HotelOperations.POSTED_EVENTS_V;
import static com.ihg.crm.automation.selenium.tests.hotel.PermissionConstant.HotelOperations.REIMBURSEMENT_AND_CERTIFICATE_V;
import static com.ihg.crm.automation.selenium.tests.hotel.PermissionConstant.HotelOperations.REJECT_POINT_AWARD_M;
import static com.ihg.crm.automation.selenium.tests.hotel.PermissionConstant.HotelOperations.REVIEW_AND_MANAGE_M;
import static com.ihg.crm.automation.selenium.tests.hotel.PermissionConstant.HotelOperations.REVIEW_AND_MANAGE_V;
import static com.ihg.crm.automation.selenium.tests.hotel.PermissionConstant.HotelOperations.SECURITY_V;
import static com.ihg.crm.automation.selenium.tests.hotel.PermissionConstant.HotelOperations.STAY_A;
import static com.ihg.crm.automation.selenium.tests.hotel.PermissionConstant.HotelOperations.STAY_V;
import static com.ihg.crm.automation.selenium.tests.hotel.PermissionConstant.HotelOperations.TAX_AND_FEE_A;
import static com.ihg.crm.automation.selenium.tests.hotel.PermissionConstant.HotelOperations.TAX_AND_FEE_V;

import java.util.HashMap;
import java.util.Map;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Factory;

import com.ihg.crm.automation.selenium.tests.hotel.SecurityMatrix;

public class HotelOperationAvailabilityFactory
{
    @DataProvider(name = "verifyStayAvailabilityProvider")
    public Object[][] verifyStayAvailabilityProvider()
    {
        Map<String, Boolean> defaultPermissions = setPermissionMap();
        SecurityMatrix permissions = new SecurityMatrix(defaultPermissions);

        permissions.getHotelSecurity().put(HOTEL_OPERATION_BUTTON, false);
        permissions.getHotelSecurity().put(MANAGE_EMP_RATE_ELIGIBILITY_M, true);
        permissions.getHotelSecurity().put(SECURITY_V, true);

        permissions.getHotelBackOffice().put(STAY_V, true);
        permissions.getHotelBackOffice().put(LPU_SCREEN_V, true);
        permissions.getHotelBackOffice().put(LPU_SCREEN_M, true);
        permissions.getHotelBackOffice().put(STAY_A, true);
        permissions.getHotelBackOffice().put(OTHER_ACTIVITY_V, true);
        permissions.getHotelBackOffice().put(FREE_NIGHT_FLAT_REIMBURSEMENT_V, true);
        permissions.getHotelBackOffice().put(TAX_AND_FEE_V, true);
        permissions.getHotelBackOffice().put(OCCUPANCY_V, true);
        permissions.getHotelBackOffice().put(OCCUPANCY_M, true);
        permissions.getHotelBackOffice().put(LOW_OCCUPANCY_RATES_V, true);
        permissions.getHotelBackOffice().put(REIMBURSEMENT_AND_CERTIFICATE_V, true);
        permissions.getHotelBackOffice().put(REVIEW_AND_MANAGE_V, true);
        permissions.getHotelBackOffice().put(REVIEW_AND_MANAGE_M, true);
        permissions.getHotelBackOffice().put(POSTED_EVENTS_V, true);

        permissions.getHotelFrontDeskStandard().put(POINT_AWARD_V, true);
        permissions.getHotelFrontDeskStandard().put(POINT_AWARD_A, true);
        permissions.getHotelFrontDeskStandard().put(LIMIT_VIEW_POINT_AWARD_A, true);

        permissions.getHotelFrontDeskFeeBased().put(POINT_AWARD_V, true);
        permissions.getHotelFrontDeskFeeBased().put(POINT_AWARD_A, true);
        permissions.getHotelFrontDeskFeeBased().put(LIMIT_VIEW_POINT_AWARD_A, true);

        permissions.getHotelManager().put(STAY_V, true);
        permissions.getHotelManager().put(LPU_SCREEN_V, true);
        permissions.getHotelManager().put(LPU_SCREEN_M, true);
        permissions.getHotelManager().put(STAY_A, true);
        permissions.getHotelManager().put(OTHER_ACTIVITY_V, true);
        permissions.getHotelManager().put(FREE_NIGHT_FLAT_REIMBURSEMENT_V, true);
        permissions.getHotelManager().put(TAX_AND_FEE_V, true);
        permissions.getHotelManager().put(OCCUPANCY_V, true);
        permissions.getHotelManager().put(OCCUPANCY_M, true);
        permissions.getHotelManager().put(LOW_OCCUPANCY_RATES_V, true);
        permissions.getHotelManager().put(REIMBURSEMENT_AND_CERTIFICATE_V, true);
        permissions.getHotelManager().put(REVIEW_AND_MANAGE_V, true);
        permissions.getHotelManager().put(APPROVE_M, true);
        permissions.getHotelManager().put(POSTED_EVENTS_V, true);
        permissions.getHotelManager().put(OFFERS_V, true);

        permissions.getHotelOperationsManager().put(STAY_V, true);
        permissions.getHotelOperationsManager().put(ORDER_POINT_VOUCHERS_A, true);
        permissions.getHotelOperationsManager().put(POINT_AWARD_V, true);
        permissions.getHotelOperationsManager().put(POINT_AWARD_A, true);
        permissions.getHotelOperationsManager().put(OVERRIDE_POINT_AWARD_M, true);
        permissions.getHotelOperationsManager().put(REJECT_POINT_AWARD_M, true);

        permissions.getSalesManager().put(STAY_V, true);
        permissions.getSalesManager().put(REVIEW_AND_MANAGE_M, true);
        permissions.getSalesManager().put(REVIEW_AND_MANAGE_V, true);
        permissions.getSalesManager().put(POSTED_EVENTS_V, true);
        permissions.getSalesManager().put(OFFERS_V, true);

        Map<String, Boolean> filterCriteria = new HashMap<String, Boolean>();
        filterCriteria.put(HOTEL_OPERATION_BUTTON, true);

        return permissions.getPermissionArray(filterCriteria);
    }

    private Map<String, Boolean> setPermissionMap()
    {
        Map<String, Boolean> permissions = new HashMap<String, Boolean>();

        permissions.put(HOTEL_OPERATION_BUTTON, true);
        permissions.put(MANAGE_EMP_RATE_ELIGIBILITY_M, false);
        permissions.put(ORDER_POINT_VOUCHERS_A, false);

        permissions.put(STAY_V, false);
        permissions.put(LPU_SCREEN_V, false);
        permissions.put(LPU_SCREEN_M, false);
        permissions.put(STAY_A, false);

        permissions.put(TAX_AND_FEE_V, false);
        permissions.put(TAX_AND_FEE_A, false);
        permissions.put(OCCUPANCY_V, false);
        permissions.put(OCCUPANCY_M, false);
        permissions.put(LOW_OCCUPANCY_RATES_V, false);
        permissions.put(REIMBURSEMENT_AND_CERTIFICATE_V, false);

        permissions.put(OTHER_ACTIVITY_V, false);
        permissions.put(FREE_NIGHT_FLAT_REIMBURSEMENT_V, false);

        permissions.put(POINT_AWARD_V, false);
        permissions.put(POINT_AWARD_A, false);
        permissions.put(LIMIT_VIEW_POINT_AWARD_A, false);
        permissions.put(OVERRIDE_POINT_AWARD_M, false);
        permissions.put(REJECT_POINT_AWARD_M, false);

        permissions.put(REVIEW_AND_MANAGE_V, false);
        permissions.put(REVIEW_AND_MANAGE_M, false);
        permissions.put(APPROVE_M, false);
        permissions.put(POSTED_EVENTS_V, false);
        permissions.put(OFFERS_V, false);

        permissions.put(SECURITY_V, false);

        return permissions;
    }

    @Factory(dataProvider = "verifyStayAvailabilityProvider")
    public Object[] createInstances(String role, Map<String, Boolean> permissions)
    {
        return new Object[] { new StayAvailabilityFactoryTest(role, permissions), //
                new RewardNightAvailabilityFactoryTest(role, permissions), //
                new FreeNightFlatReimbursementAndOtherActivityFactoryTest(role, permissions), //
                new PointAwardFactoryTest(role, permissions), //
                new BusinessRewardsAvailabilityFactoryTest(role, permissions) };
    }
}
