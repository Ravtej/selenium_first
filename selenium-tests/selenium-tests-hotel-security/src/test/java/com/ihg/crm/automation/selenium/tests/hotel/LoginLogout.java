package com.ihg.crm.automation.selenium.tests.hotel;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;

import java.util.Map;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.test.context.ContextConfiguration;

import com.ihg.automation.security.SecurityTestNGBase;
import com.ihg.automation.selenium.common.SiteHelperBase;
import com.ihg.automation.selenium.common.User;
import com.ihg.automation.selenium.hotel.pages.HotelHelper;
import com.ihg.automation.selenium.hotel.pages.LeftPanel;
import com.ihg.automation.selenium.hotel.pages.search.GuestSearchByNumber;

@ContextConfiguration(locations = { "classpath:app-context-hotel-security.xml" })
public class LoginLogout extends SecurityTestNGBase
{
    @Value("${loginUrl}")
    private String loginUrl;

    @Resource
    protected Map<String, User> users;

    @Value("${memberId}")
    protected String memberId;

    protected LoginLogout(String role, Map<String, Boolean> permissions)
    {
        super(role, permissions);
    }

    @Override
    protected SiteHelperBase getHelper()
    {
        return new HotelHelper(loginUrl);
    }

    @Override
    protected Map<String, User> getUsers()
    {
        return users;
    }

    protected void loginAndOpenMember(String memberNumber)
    {
        login();
        searchNewMember(memberNumber);
    }

    private void openMemberContext(String memberNumber)
    {
        GuestSearchByNumber guestSearchByNumber = new GuestSearchByNumber();
        verifyThat(guestSearchByNumber, displayed(true));

        guestSearchByNumber.getMemberNumber().type(memberNumber);
        guestSearchByNumber.clickSearch();
    }

    protected void searchNewMember(String memberNumber)
    {
        LeftPanel leftPanel = new LeftPanel();

        leftPanel.getGuestSearch().clickAndWait();
        openMemberContext(memberNumber);
    }
}
