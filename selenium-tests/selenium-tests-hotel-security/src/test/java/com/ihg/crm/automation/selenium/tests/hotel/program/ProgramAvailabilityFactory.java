package com.ihg.crm.automation.selenium.tests.hotel.program;

import static com.ihg.crm.automation.selenium.tests.hotel.PermissionConstant.Program.ALLIANCE_V;
import static com.ihg.crm.automation.selenium.tests.hotel.PermissionConstant.Program.AMB_TAB_V;
import static com.ihg.crm.automation.selenium.tests.hotel.PermissionConstant.Program.BR_TAB_V;
import static com.ihg.crm.automation.selenium.tests.hotel.PermissionConstant.Program.DR_TAB_V;
import static com.ihg.crm.automation.selenium.tests.hotel.PermissionConstant.Program.EMPLOYEE_RATE_ELIGIBILITY_M;
import static com.ihg.crm.automation.selenium.tests.hotel.PermissionConstant.Program.EMPLOYEE_RATE_ELIGIBILITY_V;
import static com.ihg.crm.automation.selenium.tests.hotel.PermissionConstant.Program.EMP_TAB_V;
import static com.ihg.crm.automation.selenium.tests.hotel.PermissionConstant.Program.HTL_TAB_V;
import static com.ihg.crm.automation.selenium.tests.hotel.PermissionConstant.Program.IHG_TAB_V;
import static com.ihg.crm.automation.selenium.tests.hotel.PermissionConstant.Program.PC_TAB_V;
import static com.ihg.crm.automation.selenium.tests.hotel.PermissionConstant.Program.PROGRAM_MEMBER_POINT_BALANCE_V;
import static com.ihg.crm.automation.selenium.tests.hotel.PermissionConstant.Program.RENEW_IN_PROGRAM;
import static com.ihg.crm.automation.selenium.tests.hotel.PermissionConstant.Program.YEARLY_ACHIEVEMENT_SUMMARY;

import java.util.HashMap;
import java.util.Map;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Factory;

import com.ihg.crm.automation.selenium.tests.hotel.SecurityMatrix;

public class ProgramAvailabilityFactory
{
    @DataProvider(name = "verifyProgramAvailabilityProvider")
    public Object[][] verifyProgramAvailabilityProvider()
    {
        Map<String, Boolean> defaultPermissions = setPermissionMap();
        SecurityMatrix permissions = new SecurityMatrix(defaultPermissions);

        permissions.getHotelSecurity().put(RENEW_IN_PROGRAM, false);

        permissions.getHotelFrontDeskStandard().put(EMPLOYEE_RATE_ELIGIBILITY_M, false);
        permissions.getHotelFrontDeskStandard().put(RENEW_IN_PROGRAM, false);

        permissions.getHotelBackOffice().put(EMPLOYEE_RATE_ELIGIBILITY_M, false);
        permissions.getHotelBackOffice().put(RENEW_IN_PROGRAM, false);

        permissions.getHotelManager().put(EMPLOYEE_RATE_ELIGIBILITY_M, false);

        permissions.getHotelManager().put(EMPLOYEE_RATE_ELIGIBILITY_M, false);

        permissions.getHotelManager().put(EMPLOYEE_RATE_ELIGIBILITY_M, false);

        permissions.getHotelOperationsManager().put(EMPLOYEE_RATE_ELIGIBILITY_M, false);
        permissions.getHotelOperationsManager().put(RENEW_IN_PROGRAM, false);

        permissions.getHotelFrontDeskFeeBased().put(EMPLOYEE_RATE_ELIGIBILITY_M, false);

        permissions.getSalesManager().put(EMPLOYEE_RATE_ELIGIBILITY_M, false);
        permissions.getSalesManager().put(RENEW_IN_PROGRAM, false);

        return permissions.getPermissionArray();
    }

    private Map<String, Boolean> setPermissionMap()
    {
        Map<String, Boolean> permissions = new HashMap<String, Boolean>();

        permissions.put(PROGRAM_MEMBER_POINT_BALANCE_V, true);
        permissions.put(PC_TAB_V, true);
        permissions.put(EMP_TAB_V, true);
        permissions.put(AMB_TAB_V, true);
        permissions.put(DR_TAB_V, true);
        permissions.put(BR_TAB_V, true);
        permissions.put(IHG_TAB_V, true);
        permissions.put(HTL_TAB_V, true);
        permissions.put(EMPLOYEE_RATE_ELIGIBILITY_V, true);
        permissions.put(EMPLOYEE_RATE_ELIGIBILITY_M, true);
        permissions.put(ALLIANCE_V, true);
        permissions.put(RENEW_IN_PROGRAM, true);
        permissions.put(YEARLY_ACHIEVEMENT_SUMMARY, true);

        return permissions;
    }

    @Factory(dataProvider = "verifyProgramAvailabilityProvider")
    public Object[] createInstances(String role, Map<String, Boolean> permissions)
    {
        return new Object[] { new ProgramAvailabilityFactoryTest(role, permissions) };
    }
}
