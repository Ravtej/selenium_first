package com.ihg.crm.automation.selenium.tests.hotel.operations;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.PointAwardsStatus.DISQUALIFIED;
import static com.ihg.automation.selenium.common.PointAwardsStatus.PENDING;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasDefault;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static com.ihg.crm.automation.selenium.tests.hotel.PermissionConstant.HotelOperations.LIMIT_VIEW_POINT_AWARD_A;
import static com.ihg.crm.automation.selenium.tests.hotel.PermissionConstant.HotelOperations.OVERRIDE_POINT_AWARD_M;
import static com.ihg.crm.automation.selenium.tests.hotel.PermissionConstant.HotelOperations.POINT_AWARD_A;
import static com.ihg.crm.automation.selenium.tests.hotel.PermissionConstant.HotelOperations.POINT_AWARD_V;
import static com.ihg.crm.automation.selenium.tests.hotel.PermissionConstant.HotelOperations.REJECT_POINT_AWARD_M;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.core.IsNot.not;

import java.util.Map;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.pages.login.TopUserSection;
import com.ihg.automation.selenium.gwt.components.dialog.ConfirmDialog;
import com.ihg.automation.selenium.gwt.components.grid.GridCell;
import com.ihg.automation.selenium.hotel.pages.LeftPanel;
import com.ihg.automation.selenium.hotel.pages.Tabs;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.HotelOperationsPage;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.awards.CreatePointAwardPopUp;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.awards.PointAward;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.awards.manage.ManagePointAwardsGridRow;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.awards.manage.ManagePointAwardsPage;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.awards.manage.ManagePointAwardsSearch;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.awards.posted.PostedPointAward;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.awards.posted.PostedPointAwardsPage;
import com.ihg.crm.automation.selenium.tests.hotel.LoginLogout;

public class PointAwardFactoryTest extends LoginLogout
{
    private HotelOperationsPage hotelOperationsPage;
    private ManagePointAwardsPage managePointAwardsPage;
    private PostedPointAwardsPage postedPointAwardsPage;

    public PointAwardFactoryTest(String role, Map<String, Boolean> permissions)
    {
        super(role, permissions);
    }

    @BeforeClass
    public void beforeClass()
    {
        login();
    }

    @BeforeMethod
    public void beforeMethod()
    {
        LeftPanel leftPanel = new LeftPanel();
        leftPanel.getGuestSearch().clickAndWait();
        leftPanel.getHotelOperations().clickAndWait();
    }

    @Test(priority = 10)
    public void verifyViewPointsAward()
    {
        HotelOperationsPage hotelOperationsPage = new HotelOperationsPage();
        verifyThat(hotelOperationsPage.getPointAwards(), displayed(permissions.get(POINT_AWARD_V)));

        if (permissions.get(POINT_AWARD_V))
        {
            hotelOperationsPage.getPointAwards().clickAndWait();

            verifyThat(new ManagePointAwardsPage(), displayed(true));
            verifyThat(new Tabs().getAwardsTab().getPostedPointAwards(), displayed(true));
        }
    }

    @Test(priority = 10)
    public void verifyCreateAward()
    {
        if (permissions.get(POINT_AWARD_V))
        {
            new HotelOperationsPage().getPointAwards().clickAndWait();

            verifyThat(new ManagePointAwardsPage().getCreatePointAward(), displayed(permissions.get(POINT_AWARD_A)));
            postedPointAwardsPage = new PostedPointAwardsPage();
            postedPointAwardsPage.goTo();
            verifyThat(postedPointAwardsPage.getCreatePointAward(), displayed(permissions.get(POINT_AWARD_A)));
        }
    }

    @Test(priority = 10)
    public void verifyLimitCreateAward()
    {
        PostedPointAward pointAward = new PostedPointAward();
        pointAward.setMemberNumber("103668373");
        pointAward.setTransaction(PointAward.HOTEL_PROMOTION);
        pointAward.setPoints("2000");

        if (permissions.get(POINT_AWARD_A))
        {
            new HotelOperationsPage().getPointAwards().clickAndWait();

            postedPointAwardsPage = new PostedPointAwardsPage();
            postedPointAwardsPage.goTo();
            postedPointAwardsPage.getCreatePointAward().clickAndWait();

            CreatePointAwardPopUp createPointAwardPopUp = new CreatePointAwardPopUp();
            createPointAwardPopUp.populate(pointAward);
            createPointAwardPopUp.clickSubmit();
            ConfirmDialog warningPopUp = new ConfirmDialog();
            verifyThat(warningPopUp, displayed(true));

            if (permissions.get(LIMIT_VIEW_POINT_AWARD_A))
            {
                verifyThat(warningPopUp, hasText(containsString("This point amount exceeds your limit.")));
            }
            else
            {
                verifyThat(warningPopUp, not(hasText(containsString("This point amount exceeds your limit."))));
            }

            warningPopUp.clickYes();
        }
    }

    @Test(priority = 20)
    public void verifyRejectPointAward()
    {
        if (permissions.get(POINT_AWARD_V))
        {
            new HotelOperationsPage().getPointAwards().clickAndWait();
            managePointAwardsPage = new ManagePointAwardsPage();

            managePointAwardsPage.getSearchFields().getStatus().select(PENDING);
            managePointAwardsPage.getButtonBar().clickSearch();

            GridCell cell = managePointAwardsPage.getManagePointAwardsGrid().getRow(1)
                    .getCell(ManagePointAwardsGridRow.ManagePointAwardsGridCell.ACTION);
            verifyThat(cell.asLink(), displayed(permissions.get(REJECT_POINT_AWARD_M)));

            if (permissions.get(REJECT_POINT_AWARD_M))
            {
                verifyThat(cell, hasText("Reject"));
            }
            else
            {
                verifyThat(cell, hasDefault());
            }
        }
    }

    @Test(priority = 30)
    public void verifyOverridePointAward()
    {
        if (permissions.get(POINT_AWARD_V))
        {
            new TopUserSection().getHotel().typeAndWait("SXBGE");
            LeftPanel leftPanel = new LeftPanel();
            leftPanel.getGuestSearch().clickAndWait();
            leftPanel.getHotelOperations().clickAndWait();
            new HotelOperationsPage().getPointAwards().clickAndWait();

            managePointAwardsPage = new ManagePointAwardsPage();
            managePointAwardsPage.goTo();
            ManagePointAwardsSearch searchFields = managePointAwardsPage.getSearchFields();
            searchFields.getStatus().select(DISQUALIFIED);
            searchFields.getCheckIn().uncheck();
            searchFields.getRequested().uncheck();
            searchFields.getFromDate().clear();
            searchFields.getToDate().clear();
            managePointAwardsPage.getButtonBar().clickSearch();

            verifyThat(
                    managePointAwardsPage.getManagePointAwardsGrid().getRow(1)
                            .getCell(ManagePointAwardsGridRow.ManagePointAwardsGridCell.ACTION).asLink(),
                    displayed(permissions.get(OVERRIDE_POINT_AWARD_M)));

            if (permissions.get(OVERRIDE_POINT_AWARD_M))
            {
                verifyThat(managePointAwardsPage.getManagePointAwardsGrid().getRow(1)
                        .getCell(ManagePointAwardsGridRow.ManagePointAwardsGridCell.ACTION), hasText("Override"));
            }
        }
    }

}
