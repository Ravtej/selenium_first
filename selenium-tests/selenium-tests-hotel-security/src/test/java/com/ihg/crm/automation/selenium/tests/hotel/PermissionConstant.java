package com.ihg.crm.automation.selenium.tests.hotel;

public class PermissionConstant
{
    public static class Enroll
    {
        public static final String ENROLLMENT_ENROLL = "ENROLLMENT_ENROLL";
        public static final String ENROLL_PC = "ENROLL_PC";
        public static final String ENROLL_EMP = "ENROLL_EMP";
        public static final String ENROLL_AMB = "ENROLL_AMB";
        public static final String ENROLL_DR = "ENROLL_DR";
        public static final String ENROLL_BR = "ENROLL_BR";
        public static final String ENROLL_IHG = "ENROLL_IHG";
        public static final String ENROLL_HTL = "ENROLL_HTL";
    }

    public static class Program
    {
        public static final String PROGRAM_MEMBER_POINT_BALANCE_V = "PROGRAM_MEMBER_POINT_BALANCE_V";
        public static final String PC_TAB_V = "PC_TAB_V";
        public static final String EMP_TAB_V = "EMP_TAB_V";
        public static final String AMB_TAB_V = "AMB_TAB_V";
        public static final String DR_TAB_V = "DR_TAB_V";
        public static final String BR_TAB_V = "BR_TAB_V";
        public static final String IHG_TAB_V = "IHG_TAB_V";
        public static final String HTL_TAB_V = "HTL_TAB_V";
        public static final String EMPLOYEE_RATE_ELIGIBILITY_V = "EMPLOYEE_RATE_ELIGIBILITY_V";
        public static final String EMPLOYEE_RATE_ELIGIBILITY_M = "EMPLOYEE_RATE_ELIGIBILITY_M";
        public static final String ALLIANCE_V = "ALLIANCE_V";
        public static final String RENEW_IN_PROGRAM = "RENEW_IN_PROGRAM";
        public static final String YEARLY_ACHIEVEMENT_SUMMARY = "YEARLY_ACHIEVEMENT_SUMMARY";
    }

    public static class Communication
    {
        public static final String GUEST_MARKET_PREF_M = "GUEST_MARKET_PREF_M";
        public static final String GUEST_MARKET_PREF_V = "GUEST_MARKET_PREF_V";
    }

    public static class Guest
    {
        // TODO email validation in Strikeiron
        public static final String GUEST_PERSONAL_V = "GUEST_PERSONAL_V";

        public static final String GUEST_CONTACT_PHONE_ADDRESS_BIRTH_M = "GUEST_CONTACT_PHONE_ADDRESS_BIRTH_M";
        public static final String GUEST_CONTACT_PHONE_ADDRESS_BIRTH_V = "GUEST_CONTACT_PHONE_ADDRESS_BIRTH_V";
        public static final String GUEST_CONTACT_PHONE_ADDRESS_BIRTH_A = "GUEST_CONTACT_PHONE_ADDRESS_BIRTH_A";
        public static final String CONTACT_EMAIL_M = "CONTACT_EMAIL_M";
    }

    public static class HotelOperations
    {
        public static final String HOTEL_OPERATION_BUTTON = "HOTEL_OPERATION_BUTTON";

        public static final String STAY_V = "STAY_V";
        public static final String LPU_SCREEN_V = "LPU_SCREEN_V";
        public static final String LPU_SCREEN_M = "LPU_SCREEN_M";
        public static final String STAY_A = "STAY_A";

        public static final String TAX_AND_FEE_V = "TAX_AND_FEE_V";
        public static final String TAX_AND_FEE_A = "TAX_AND_FEE_A";
        public static final String OCCUPANCY_V = "OCCUPANCY_V";
        public static final String OCCUPANCY_M = "OCCUPANCY_M";
        public static final String LOW_OCCUPANCY_RATES_V = "LOW_OCCUPANCY_RATES_V";
        public static final String REIMBURSEMENT_AND_CERTIFICATE_V = "REIMBURSEMENT_AND_CERTIFICATE_V";

        public static final String OTHER_ACTIVITY_V = "OTHER_ACTIVITY_V";

        public static final String FREE_NIGHT_FLAT_REIMBURSEMENT_V = "FREE_NIGHT_FLAT_REIMBURSEMENT_V";

        public static final String MANAGE_EMP_RATE_ELIGIBILITY_M = "MANAGE_EMP_RATE_ELIGIBILITY_M";

        public static final String ORDER_POINT_VOUCHERS_A = "ORDER_POINT_VOUCHERS_A";

        public static final String POINT_AWARD_V = "POINT_AWARD_V";
        public static final String LIMIT_VIEW_POINT_AWARD_A = "LIMIT_VIEW_POINT_AWARD_A";
        public static final String POINT_AWARD_A = "POINT_AWARD_A";
        public static final String OVERRIDE_POINT_AWARD_M = "OVERRIDE_POINT_AWARD_M";
        public static final String REJECT_POINT_AWARD_M = "REJECT_POINT_AWARD_M";

        public static final String REVIEW_AND_MANAGE_V = "REVIEW_AND_MANAGE_V";
        public static final String REVIEW_AND_MANAGE_M = "REVIEW_AND_MANAGE_M";
        public static final String APPROVE_M = "APPROVE_M";
        public static final String POSTED_EVENTS_V = "POSTED_EVENTS_V";
        public static final String OFFERS_V = "OFFERS_V";

        public static final String SECURITY_V = "SECURITY_V";

    }
}
