package com.ihg.crm.automation.selenium.tests.hotel.operations;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.crm.automation.selenium.tests.hotel.PermissionConstant.HotelOperations.FREE_NIGHT_FLAT_REIMBURSEMENT_V;
import static com.ihg.crm.automation.selenium.tests.hotel.PermissionConstant.HotelOperations.OTHER_ACTIVITY_V;

import java.util.Map;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.hotel.pages.LeftPanel;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.HotelOperationsPage;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.reimbursement.freenight.FreeNightFlatReimbursementPage;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.reimbursement.otheractivity.OtherActivityPage;
import com.ihg.crm.automation.selenium.tests.hotel.LoginLogout;

public class FreeNightFlatReimbursementAndOtherActivityFactoryTest extends LoginLogout
{
    private HotelOperationsPage hotelOperationsPage;

    public FreeNightFlatReimbursementAndOtherActivityFactoryTest(String role, Map<String, Boolean> permissions)
    {
        super(role, permissions);
    }

    @BeforeClass
    public void beforeClass()
    {
        login();
    }

    @BeforeMethod
    public void beforeMethod()
    {
        LeftPanel leftPanel = new LeftPanel();
        leftPanel.getGuestSearch().clickAndWait();
        leftPanel.getHotelOperations().clickAndWait();
    }

    @Test
    public void verifyViewFreeNightFlatReimbursement()
    {
        hotelOperationsPage = new HotelOperationsPage();
        verifyThat(hotelOperationsPage.getHotelsFlatReimbursement(),
                displayed(permissions.get(FREE_NIGHT_FLAT_REIMBURSEMENT_V)));

        if (permissions.get(FREE_NIGHT_FLAT_REIMBURSEMENT_V))
        {
            hotelOperationsPage.getHotelsFlatReimbursement().clickAndWait();

            verifyThat(new FreeNightFlatReimbursementPage(), displayed(true));
        }
    }

    @Test
    public void verifyViewOtherActivity()
    {
        hotelOperationsPage = new HotelOperationsPage();
        verifyThat(hotelOperationsPage.getOtherActivity(), displayed(permissions.get(OTHER_ACTIVITY_V)));

        if (permissions.get(OTHER_ACTIVITY_V))
        {
            hotelOperationsPage.getOtherActivity().clickAndWait();

            verifyThat(new OtherActivityPage(), displayed(true));
        }
    }

}
