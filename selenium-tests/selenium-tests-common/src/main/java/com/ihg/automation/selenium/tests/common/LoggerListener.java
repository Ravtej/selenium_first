package com.ihg.automation.selenium.tests.common;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.IInvokedMethod;
import org.testng.IInvokedMethodListener;
import org.testng.ITestResult;

public class LoggerListener implements IInvokedMethodListener
{
    private static final Logger logger = LoggerFactory.getLogger(LoggerListener.class);

    private static final String IGNORE_METHOD = "spring";

    @Override
    public void beforeInvocation(IInvokedMethod method, ITestResult testResult)
    {
    }

    @Override
    public void afterInvocation(IInvokedMethod method, ITestResult testResult)
    {
        if (!StringUtils.contains(method.getTestMethod().getMethodName(), IGNORE_METHOD))
        {
            logger.debug("{} {}", TestNGUtils.getMethodDescription(method), TestNGUtils.getStatus(testResult));
        }
    }

}
