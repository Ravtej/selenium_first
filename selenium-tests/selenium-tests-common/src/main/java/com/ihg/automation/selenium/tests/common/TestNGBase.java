package com.ihg.automation.selenium.tests.common;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.AfterClass;

import com.ihg.automation.selenium.assertions.AssertionAccumulator;
import com.ihg.automation.selenium.common.SiteHelperBase;
import com.ihg.automation.selenium.common.User;
import com.ihg.automation.selenium.runtime.Selenium;

public abstract class TestNGBase extends AbstractTestNGSpringContextTests
{
    private static final Logger logger = LoggerFactory.getLogger(TestNGBase.class);

    protected SiteHelperBase helper;

    @PostConstruct
    public void init()
    {
        helper = getHelper();
    }

    protected abstract SiteHelperBase getHelper();

    @AfterClass(alwaysRun = true)
    public void shutdown()
    {
        try
        {
            logout();
        }
        catch (Exception e)
        {
            logger.error("Exception during logout", e);
        }

        Selenium.getInstance().shutDown();
        AssertionAccumulator.getExceptions().clear();
    }

    public void openLoginPage()
    {
        helper.openLoginPage();
    }

    public void login(User user, String role)
    {
        helper.login(user, role, true);
    }

    public void logout()
    {
        helper.logout();
    }
}
