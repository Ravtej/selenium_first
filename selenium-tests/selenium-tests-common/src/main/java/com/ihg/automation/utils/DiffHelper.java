package com.ihg.automation.utils;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.core.IsNot.not;

import com.ihg.atp.schema.crm.guest.guest.datatypes.v2.ProfileType;
import com.ihg.atp.schema.crm.guest.guest.servicetypes.v2.RetrieveGuestResponseType;

public class DiffHelper
{
    public void verifyEntitiesSizeAfterUpdate(RetrieveGuestResponseType retrieveGuestBefore,
            RetrieveGuestResponseType retrieveGuestAfter, int addressDiff, int phoneDiff, int emailDiff)
    {
        ProfileType profileBefore = retrieveGuestBefore.getProfile();
        ProfileType profileAfter = retrieveGuestAfter.getProfile();

        verifyThat(null, retrieveGuestAfter.getProfile().getLastModifyDateTime(),
                is(not(retrieveGuestBefore.getProfile().getLastModifyDateTime())),
                "lastModifyDateTime values must be different");

        verifyThat(null, profileAfter.getAddresses().getAddress().size(),
                is(profileBefore.getAddresses().getAddress().size() + addressDiff),
                String.format("Address amount must have diff='%s'", addressDiff));
        verifyThat(null, profileAfter.getPhones().getPhone().size(),
                is(profileBefore.getPhones().getPhone().size() + phoneDiff),
                String.format("Phone amount must have diff='%s'", phoneDiff));
        verifyThat(null, profileAfter.getEmails().getEmail().size(),
                is(profileBefore.getEmails().getEmail().size() + emailDiff),
                String.format("Email amount must have diff='%s'", emailDiff));

        verifyThat(null, profileAfter.getNames().getName().size(), is(profileBefore.getNames().getName().size()),
                "Name amount must be the same");
        verifyThat(null, profileAfter.getCredentials().getCredential().size(),
                is(profileBefore.getCredentials().getCredential().size()), "Credentials amount must be the same");
    }
}
