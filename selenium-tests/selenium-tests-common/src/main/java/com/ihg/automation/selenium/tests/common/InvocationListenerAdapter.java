package com.ihg.automation.selenium.tests.common;

import java.nio.file.Path;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.openqa.selenium.OutputType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.IInvokedMethod;
import org.testng.IInvokedMethodListener;
import org.testng.ITestResult;

import com.ihg.automation.selenium.assertions.AssertionAccumulator;
import com.ihg.automation.selenium.assertions.OnScreenException;
import com.ihg.automation.selenium.assertions.ScreenUtils;
import com.ihg.automation.selenium.runtime.Cameron;
import com.ihg.automation.selenium.runtime.ScreenException;
import com.ihg.automation.selenium.runtime.SeleniumException;

public class InvocationListenerAdapter implements IInvokedMethodListener
{
    private static final Logger logger = LoggerFactory.getLogger(InvocationListenerAdapter.class);

    public static final String SCREEN_DIR = "screenshots";

    @Override
    public void beforeInvocation(IInvokedMethod invokedMethod, ITestResult testResult)
    {
    }

    @Override
    public void afterInvocation(IInvokedMethod invokedMethod, ITestResult testResult)
    {
        if (testResult.getStatus() == ITestResult.SKIP)
        {
            // Ignore all errors in any case
            AssertionAccumulator.getExceptions().clear();

            Throwable skipThrowable = testResult.getThrowable();
            if (skipThrowable instanceof ScreenException)
            {
                ScreenUtils.save((ScreenException) skipThrowable,
                        TestNGUtils.getRootOutputDirectory(testResult).resolve(SCREEN_DIR));
            }

            return;
        }

        OnScreenException onScreenException = new OnScreenException();
        List<SeleniumException> verifyExceptions = AssertionAccumulator.getExceptionsAndClear();
        if (CollectionUtils.isNotEmpty(verifyExceptions))
        {
            onScreenException.getExceptionList().addAll(verifyExceptions);
        }

        if (testResult.getStatus() == ITestResult.FAILURE)
        {
            Throwable throwable = testResult.getThrowable();

            if (throwable instanceof OnScreenException)
            {
                onScreenException.getExceptionList().addAll(((OnScreenException) throwable).getExceptionList());
            }
            else if (throwable instanceof SeleniumException)
            {
                onScreenException.getExceptionList().add((SeleniumException) throwable);
            }
            else
            {
                try
                {
                    onScreenException.getExceptionList()
                            .add(new SeleniumException(throwable, new Cameron().getScreenshotAs(OutputType.BASE64)));
                }
                catch (Exception e)
                {
                    onScreenException.getExceptionList().add(new SeleniumException(throwable, null));
                    logger.error("Catch unexpected exception", e);
                }
            }
        }

        if (CollectionUtils.isNotEmpty(onScreenException.getExceptionList()))
        {
            Path screenDir = TestNGUtils.getRootOutputDirectory(testResult).resolve(SCREEN_DIR);
            for (SeleniumException exception : onScreenException.getExceptionList())
            {
                ScreenUtils.save(exception, screenDir);
            }

            testResult.setStatus(ITestResult.FAILURE);
            testResult.setThrowable(onScreenException);

            if (invokedMethod.isConfigurationMethod())
            {
                // Exception is not overridden by setThrowable()
                throw onScreenException;
            }
        }
    }
}
