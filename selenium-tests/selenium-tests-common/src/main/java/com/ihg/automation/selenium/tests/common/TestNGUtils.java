package com.ihg.automation.selenium.tests.common;

import java.nio.file.Path;
import java.nio.file.Paths;

import org.testng.IInvokedMethod;
import org.testng.ITestContext;
import org.testng.ITestResult;

public class TestNGUtils
{
    public static String getMethodDescription(IInvokedMethod method)
    {
        String description;
        if (method.isTestMethod())
        {
            description = "TEST";
        }
        else
        {
            description = "CONFIG";
        }

        return description;
    }

    public static String getStatus(ITestResult testResult)
    {
        String status;
        switch (testResult.getStatus())
        {
        case ITestResult.SUCCESS:
            status = "SUCCESS";
            break;
        case ITestResult.FAILURE:
            status = "FAILURE";
            break;
        case ITestResult.SKIP:
            status = "SKIP";
            break;
        default:
            status = "UNKNOWN";
            break;
        }

        return status;
    }

    public static Path getRootOutputDirectory(ITestContext context)
    {
        return Paths.get(context.getOutputDirectory()).getParent();
    }

    public static Path getRootOutputDirectory(ITestResult testResult)
    {
        return getRootOutputDirectory(testResult.getTestContext());
    }

}
