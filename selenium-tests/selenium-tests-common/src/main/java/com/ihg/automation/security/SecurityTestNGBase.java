package com.ihg.automation.security;

import java.util.Map;

import com.ihg.automation.selenium.common.User;
import com.ihg.automation.selenium.tests.common.TestNGBase;

public abstract class SecurityTestNGBase extends TestNGBase
{
    protected String role;
    protected Map<String, Boolean> permissions;

    protected SecurityTestNGBase(String role, Map<String, Boolean> permissions)
    {
        this.role = role;
        this.permissions = permissions;
    }

    protected void login()
    {
        login(getUsers().get(role), role);
    }

    protected abstract Map<String, User> getUsers();

    @Override
    public String toString()
    {
        return "[" + "role=" + role + "]";
    }
}
