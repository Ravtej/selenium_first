package com.ihg.automation.selenium.tests.common;

public class SeleniumReporterShort extends SeleniumReporterBase
{
    public SeleniumReporterShort()
    {
        super("selenium-emailable-report.html", false);
    }
}
