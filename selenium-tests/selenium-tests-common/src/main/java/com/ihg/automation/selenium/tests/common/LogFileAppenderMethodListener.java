package com.ihg.automation.selenium.tests.common;

import java.nio.file.Path;
import java.util.concurrent.atomic.AtomicInteger;

import org.slf4j.LoggerFactory;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

import com.ihg.automation.logger.LogThreadLocalFilter;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;
import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.encoder.PatternLayoutEncoder;
import ch.qos.logback.core.FileAppender;

public class LogFileAppenderMethodListener implements ITestListener
{
    private static final String LOG_EVENT_PATTERN = "[%d{yyyy.MM.dd HH:mm:ss.SSS z}] [%-5level] [%tid] [%X{testMethod}] [%.-4X{testType}] [%lo{1}] [%method] [%msg]%n";
    private static final String LOG_FOLDER = "logs";
    private static AtomicInteger id = new AtomicInteger(0);

    @Override
    public void onTestStart(ITestResult result)
    {
    }

    @Override
    public void onTestSuccess(ITestResult result)
    {
    }

    @Override
    public void onTestFailure(ITestResult result)
    {
    }

    @Override
    public void onTestSkipped(ITestResult result)
    {
    }

    @Override
    public void onTestFailedButWithinSuccessPercentage(ITestResult result)
    {
    }

    @Override
    public void onStart(ITestContext context)
    {
        LoggerContext lc = (LoggerContext) LoggerFactory.getILoggerFactory();
        PatternLayoutEncoder ple = new PatternLayoutEncoder();

        ple.setPattern(LOG_EVENT_PATTERN);
        ple.setContext(lc);
        ple.start();

        FileAppender fileAppender = new FileAppender();
        String name = context.getName();
        fileAppender.setName(name);
        Path path = TestNGUtils.getRootOutputDirectory(context).resolve(LOG_FOLDER).resolve(getLogFileName(name));
        fileAppender.setFile(path.toString());
        fileAppender.setEncoder(ple);
        fileAppender.addFilter(new LogThreadLocalFilter());
        fileAppender.setContext(lc);
        fileAppender.start();

        Logger logger = (Logger) LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME);
        logger.addAppender(fileAppender);
        logger.setLevel(Level.DEBUG);
    }

    @Override
    public void onFinish(ITestContext context)
    {
        ((Logger) LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME)).detachAppender(context.getName());
    }

    private String getLogFileName(String name)
    {
        return String.format("%03d-%s.log", id.incrementAndGet(), name);
    }
}
