package com.ihg.automation.selenium.tests.common;

import org.slf4j.MDC;
import org.testng.IInvokedMethod;
import org.testng.IInvokedMethodListener;
import org.testng.ITestNGMethod;
import org.testng.ITestResult;

import ch.qos.logback.classic.pattern.TargetLengthBasedClassNameAbbreviator;

public class MDCInvokedMethodListener implements IInvokedMethodListener
{
    public static final String METHOD_NAME = "testMethod";
    public static final String TEST_CLASS_NAME = "testClass";
    public static final String TEST_TYPE = "testType";

    @Override
    public void beforeInvocation(IInvokedMethod method, ITestResult testResult)
    {
        ITestNGMethod testMethod = method.getTestMethod();
        TargetLengthBasedClassNameAbbreviator abbreviator = new TargetLengthBasedClassNameAbbreviator(0);
        MDC.put(TEST_CLASS_NAME, abbreviator.abbreviate(testMethod.getRealClass().getName()));
        MDC.put(METHOD_NAME, testMethod.getMethodName());
        MDC.put(TEST_TYPE, TestNGUtils.getMethodDescription(method));
    }

    @Override
    public void afterInvocation(IInvokedMethod method, ITestResult testResult)
    {
        MDC.remove(TEST_CLASS_NAME);
        MDC.remove(METHOD_NAME);
        MDC.remove(TEST_TYPE);
    }
}
