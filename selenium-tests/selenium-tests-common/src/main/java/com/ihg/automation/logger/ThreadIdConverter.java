package com.ihg.automation.logger;

import ch.qos.logback.classic.pattern.ClassicConverter;
import ch.qos.logback.classic.spi.ILoggingEvent;

public class ThreadIdConverter extends ClassicConverter
{
    private static int nextId = 0;
    private static final ThreadLocal<String> threadId = new ThreadLocal<String>()
    {
        @Override
        protected String initialValue()
        {
            int nextId = nextId();
            return String.format("%03d", nextId);
        }
    };

    private static synchronized int nextId()
    {
        return ++nextId;
    }

    public static String getThreadId()
    {
        return threadId.get();
    }

    @Override
    public String convert(ILoggingEvent event)
    {
        return ThreadIdConverter.getThreadId();
    }
}
