package com.ihg.automation.selenium.tests.common;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.MessageFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import org.testng.ITestNGMethod;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.internal.Utils;
import org.testng.reporters.EmailableReporter2;

import com.ihg.automation.selenium.assertions.OnScreenException;
import com.ihg.automation.selenium.assertions.OnScreenSkipException;
import com.ihg.automation.selenium.runtime.ScreenException;
import com.ihg.automation.selenium.runtime.SeleniumException;

public abstract class SeleniumReporterBase extends EmailableReporter2
{
    private String reportName;
    private Path outputPath;
    private boolean printStackTraceIndicator;
    private SimpleDateFormat simpleDateFormat;

    public SeleniumReporterBase()
    {
    }

    public SeleniumReporterBase(String reportName, boolean printStackTraceIndicator)
    {
        simpleDateFormat = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss zzz");
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));

        this.reportName = reportName;
        this.printStackTraceIndicator = printStackTraceIndicator;
    }

    protected static final Comparator<MethodResult> TIME_RESULT_COMPARATOR = new Comparator<MethodResult>()
    {
        @Override
        public int compare(MethodResult o1, MethodResult o2)
        {
            int result = Long.compare(o1.getResults().iterator().next().getStartMillis(),
                    o2.getResults().iterator().next().getStartMillis());
            return result;
        }
    };

    public boolean getPrintStackTrace()
    {
        return printStackTraceIndicator;
    }

    public String getReportName()
    {
        return reportName;
    }

    public String getDeltaTimeInMinutesAndSeconds(long millis)
    {
        long minutes = TimeUnit.MILLISECONDS.toMinutes(millis);
        millis -= TimeUnit.MINUTES.toMillis(minutes);
        long seconds = TimeUnit.MILLISECONDS.toSeconds(millis);
        millis -= TimeUnit.SECONDS.toMillis(seconds);

        StringBuilder stringBuilder = new StringBuilder();
        if (minutes > 0)
        {
            stringBuilder.append(minutes).append(" minutes, ");
        }
        stringBuilder.append(seconds).append(".").append(String.format("%03d", millis)).append(" seconds");
        return stringBuilder.toString();
    }

    @Override
    protected PrintWriter createWriter(String outdir) throws IOException
    {
        outputPath = Paths.get(outdir);
        Files.createDirectories(outputPath);
        return new PrintWriter(new BufferedWriter(new FileWriter(outputPath.resolve(getReportName()).toFile())));
    }

    @Override
    protected void writeSuiteSummary()
    {
        NumberFormat integerFormat = NumberFormat.getIntegerInstance();

        int totalPassedTests = 0;
        int totalSkippedTests = 0;
        int totalFailedTests = 0;
        long totalDuration = 0;

        writer.print("<table>");
        writer.print("<tr>");
        writer.print("<th>Test</th>");
        writer.print("<th># Passed</th>");
        writer.print("<th># Skipped</th>");
        writer.print("<th># Failed</th>");
        writer.print("<th>Time</th>");
        writer.print("</tr>");

        int testIndex = 0;
        for (SuiteResult suiteResult : suiteResults)
        {
            writer.print("<tr><th colspan=\"5\">");
            writer.print(Utils.escapeHtml(suiteResult.getSuiteName()));
            writer.print("</th></tr>");

            for (TestResult testResult : suiteResult.getTestResults())
            {
                int passedTests = testResult.getPassedTestCount();
                int skippedTests = testResult.getSkippedTestCount();
                int failedTests = testResult.getFailedTestCount();
                long duration = testResult.getDuration();

                writer.print("<tr");
                if ((testIndex % 2) == 1)
                {
                    writer.print(" class=\"stripe\"");
                }
                writer.print(">");

                StringBuffer stringBuilder = new StringBuffer();
                writeTableData(stringBuilder.append("<a href=\"#t").append(testIndex).append("\">")
                        .append(Utils.escapeHtml(testResult.getTestName())).append("</a>").toString());
                writeTableData(integerFormat.format(passedTests), "num");
                writeTableData(integerFormat.format(skippedTests), (skippedTests > 0 ? "num attn" : "num"));
                writeTableData(integerFormat.format(failedTests), (failedTests > 0 ? "num attn" : "num"));
                writeTableData(getDeltaTimeInMinutesAndSeconds(duration));

                writer.print("</tr>");

                totalPassedTests += passedTests;
                totalSkippedTests += skippedTests;
                totalFailedTests += failedTests;
                totalDuration += duration;

                testIndex++;
            }
        }

        // Print totals if there was more than one test
        if (testIndex > 1)
        {
            writer.print("<tr>");
            writer.print("<th>Total</th>");
            writeTableHeader(integerFormat.format(totalPassedTests), "num");
            writeTableHeader(integerFormat.format(totalSkippedTests), (totalSkippedTests > 0 ? "num attn" : "num"));
            writeTableHeader(integerFormat.format(totalFailedTests), (totalFailedTests > 0 ? "num attn" : "num"));
            writeTableHeader(getDeltaTimeInMinutesAndSeconds(totalDuration), null);
            writer.print("</tr>");
        }

        writer.print("</table>");
    }

    @Override
    protected void writeScenarioSummary()
    {
        writer.print("<table>");
        writer.print("<thead>");
        writer.print("<tr>");
        writer.print("<th>Class</th>");
        writer.print("<th>Method</th>");
        writer.print("<th>Start</th>");
        writer.print("<th>Time</th>");
        writer.print("</tr>");
        writer.print("</thead>");

        int testIndex = 0;
        int scenarioIndex = 0;
        for (SuiteResult suiteResult : suiteResults)
        {
            writer.print("<tbody><tr><th colspan=\"4\">");
            writer.print(Utils.escapeHtml(suiteResult.getSuiteName()));
            writer.print("</th></tr></tbody>");

            for (TestResult testResult : suiteResult.getTestResults())
            {
                writer.print("<tbody id=\"t");
                writer.print(testIndex);
                writer.print("\">");

                String testName = Utils.escapeHtml(testResult.getTestName());

                scenarioIndex += writeScenarioSummary(testName + " &#8212; failed (configuration methods)",
                        testResult.getFailedConfigurationResults(), "failed", scenarioIndex);
                scenarioIndex += writeScenarioSummary(testName + " &#8212; failed", testResult.getFailedTestResults(),
                        "failed", scenarioIndex);
                scenarioIndex += writeScenarioSummary(testName + " &#8212; skipped (configuration methods)",
                        testResult.getSkippedConfigurationResults(), "skipped", scenarioIndex);
                scenarioIndex += writeScenarioSummary(testName + " &#8212; skipped", testResult.getSkippedTestResults(),
                        "skipped", scenarioIndex);
                scenarioIndex += writeScenarioSummary(testName + " &#8212; passed", testResult.getPassedTestResults(),
                        "passed", scenarioIndex);

                writer.print("</tbody>");

                testIndex++;
            }
        }

        writer.print("</table>");
    }

    private int writeScenarioSummary(String description, List<ClassResult> classResults, String cssClassPrefix,
            int startingScenarioIndex)
    {
        int scenarioCount = 0;
        if (!classResults.isEmpty())
        {
            writer.print("<tr><th colspan=\"4\">");
            writer.print(description);
            writer.print("</th></tr>");

            int scenarioIndex = startingScenarioIndex;
            int classIndex = 0;
            for (ClassResult classResult : classResults)
            {
                String cssClass = cssClassPrefix + ((classIndex % 2) == 0 ? "even" : "odd");

                StringBuffer buffer = new StringBuffer();

                int scenariosPerClass = 0;
                int methodIndex = 0;

                List<MethodResult> methodResults = classResult.getMethodResults();
                Collections.sort(methodResults, TIME_RESULT_COMPARATOR);
                for (MethodResult methodResult : methodResults)
                {
                    List<ITestResult> results = methodResult.getResults();
                    int resultsCount = results.size();
                    assert resultsCount > 0;

                    ITestResult firstResult = results.iterator().next();
                    ITestNGMethod method = firstResult.getMethod();
                    String methodName = Utils.escapeHtml(method.getMethodName());
                    String methodDescription = Utils.escapeHtml(method.getDescription());
                    long start = firstResult.getStartMillis();
                    long duration = firstResult.getEndMillis() - start;

                    // The first method per class shares a row with the class
                    // header
                    if (methodIndex > 0)
                    {
                        buffer.append("<tr class=\"").append(cssClass).append("\">");
                    }

                    // Write the timing information with the first scenario per
                    // method
                    buffer.append("<td><a href=\"#m").append(scenarioIndex).append("\">").append(methodName)
                            .append("</a>");

                    if (methodDescription != null && !methodDescription.isEmpty())
                    {
                        buffer.append("<div style=\"width:350px\"><i>").append(methodDescription).append("</i></div>");
                    }

                    buffer.append("</td>").append("<td rowspan=\"").append(resultsCount).append("\">")
                            .append(simpleDateFormat.format(new Date(start))).append("</td>").append("<td rowspan=\"")
                            .append(resultsCount).append("\">").append(getDeltaTimeInMinutesAndSeconds(duration))
                            .append("</td></tr>");

                    scenarioIndex++;

                    // Write the remaining scenarios for the method
                    for (int i = 1; i < resultsCount; i++)
                    {
                        buffer.append("<tr class=\"").append(cssClass).append("\">").append("<td><a href=\"#m")
                                .append(scenarioIndex).append("\">").append(methodName).append("</a></td></tr>");
                        scenarioIndex++;
                    }

                    scenariosPerClass += resultsCount;
                    methodIndex++;
                }

                // Write the test results for the class
                writer.print("<tr class=\"");
                writer.print(cssClass);
                writer.print("\">");
                writer.print("<td rowspan=\"");
                writer.print(scenariosPerClass);
                writer.print("\">");
                writer.print(Utils.escapeHtml(classResult.getClassName()));
                writer.print("</td>");
                writer.print(buffer);

                classIndex++;
            }
            scenarioCount = scenarioIndex - startingScenarioIndex;
        }
        return scenarioCount;
    }

    @Override
    protected void writeScenarioDetails()
    {
        int scenarioIndex = 0;
        for (SuiteResult suiteResult : suiteResults)
        {
            for (TestResult testResult : suiteResult.getTestResults())
            {
                if (testResult.getFailedTestCount() + testResult.getSkippedTestCount() > 0)
                {
                    writer.print("<h2>");
                    writer.print(Utils.escapeHtml(testResult.getTestName()));
                    writer.print("</h2>");

                    scenarioIndex += writeScenarioDetails(testResult.getFailedConfigurationResults(), scenarioIndex);
                    scenarioIndex += writeScenarioDetails(testResult.getFailedTestResults(), scenarioIndex);
                    scenarioIndex += writeScenarioDetails(testResult.getSkippedConfigurationResults(), scenarioIndex);
                    scenarioIndex += writeScenarioDetails(testResult.getSkippedTestResults(), scenarioIndex);
                }
            }
        }
    }

    private int writeScenarioDetails(List<ClassResult> classResults, int startingScenarioIndex)
    {
        int scenarioIndex = startingScenarioIndex;
        for (ClassResult classResult : classResults)

        {
            String className = classResult.getClassName();
            for (MethodResult methodResult : classResult.getMethodResults())
            {
                List<ITestResult> results = methodResult.getResults();
                assert !results.isEmpty();

                for (ITestResult result : results)
                {
                    ITestResult testResult = results.iterator().next();
                    String label = Utils.escapeHtml(className + "#" + testResult.getName());
                    String description = Utils.escapeHtml(testResult.getMethod().getDescription());
                    writeScenario(scenarioIndex, label, description, result);
                    scenarioIndex++;
                }
            }
        }

        return scenarioIndex - startingScenarioIndex;
    }

    private void writeScenario(int scenarioIndex, String label, String description, ITestResult result)
    {
        writer.print("<h3 id=\"m");
        writer.print(scenarioIndex);
        writer.print("\">");
        writer.print(label);
        writer.print("</h3>");

        if (description != null && !description.isEmpty())
        {
            writer.print("<div>");
            writer.print("<i>");
            writer.print(description);
            writer.print("</i>");
            writer.print("</div>");
        }

        writer.print("<table class=\"result\">");

        // Write test parameters (if any)
        Object[] parameters = result.getParameters();
        int parameterCount = (parameters == null ? 0 : parameters.length);
        if (parameterCount > 0)
        {
            writer.print("<tr class=\"param\">");
            for (int i = 1; i <= parameterCount; i++)
            {
                writer.print("<th>Parameter #");
                writer.print(i);
                writer.print("</th>");
            }
            writer.print("</tr><tr class=\"param stripe\">");
            for (Object parameter : parameters)
            {
                writer.print("<td>");
                writer.print(Utils.escapeHtml(Utils.toString(parameter)));
                writer.print("</td>");
            }
            writer.print("</tr>");
        }

        // Write reporter messages (if any)
        List<String> reporterMessages = Reporter.getOutput(result);
        if (!reporterMessages.isEmpty())
        {
            writer.print("<tr><th");
            if (parameterCount > 1)
            {
                writer.print(" colspan=\"");
                writer.print(parameterCount);
                writer.print("\"");
            }
            writer.print(">Messages</th></tr>");

            writer.print("<tr><td");
            if (parameterCount > 1)
            {
                writer.print(" colspan=\"");
                writer.print(parameterCount);
                writer.print("\"");
            }
            writer.print(">");
            writeReporterMessages(reporterMessages);
            writer.print("</td></tr>");
        }

        // Write exception (if any)
        Throwable throwable = result.getThrowable();
        if (throwable != null)
        {
            writer.print("<tr><th");
            if (parameterCount > 1)
            {
                writer.print(" colspan=\"");
                writer.print(parameterCount);
                writer.print("\"");
            }
            writer.print(">");
            writer.print((result.getStatus() == ITestResult.SUCCESS ? "Expected Exception" : "Exception"));
            writer.print("</th></tr>");

            writer.print("<tr><td");
            if (parameterCount > 1)
            {
                writer.print(" colspan=\"");
                writer.print(parameterCount);
                writer.print("\"");
            }
            writer.print(">");

            if (throwable instanceof OnScreenException)
            {
                for (SeleniumException exception : ((OnScreenException) throwable).getExceptionList())
                {
                    writeStackTraceWithScreen(exception);
                }
            }
            else if (throwable instanceof OnScreenSkipException)
            {
                writeStackTraceWithScreen((OnScreenSkipException) throwable);
            }
            else
            {
                writeStackTrace(throwable);
            }

            writer.print("</td></tr>");
        }

        writer.print("</table>");
    }

    private <E extends Throwable & ScreenException> void writeStackTraceWithScreen(E throwable)
    {
        writer.print("<div class=\"stacktrace\">" + Utils.escapeHtml(throwable.getMessage()) + "<br>");
        String imageLink;

        Path screenPath = throwable.getScreenshotPath();
        if (screenPath != null)
        {
            imageLink = MessageFormat.format(
                    "<a href=''{0}''><img src=''{0}'' width=''10%'' height=''10%'' border=''1''></a>",
                    outputPath.toAbsolutePath().relativize(screenPath));
        }
        else
        {
            imageLink = "<br><div><i><b>No screenshot available</b></i></div><br>";
        }

        writer.print(imageLink + "</div>");

        if (getPrintStackTrace())
        {
            writeStackTrace(throwable);
        }

        writer.print("<br>");
    }

}
