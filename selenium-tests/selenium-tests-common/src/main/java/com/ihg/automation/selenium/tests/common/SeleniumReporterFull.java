package com.ihg.automation.selenium.tests.common;

public class SeleniumReporterFull extends SeleniumReporterBase
{
    public SeleniumReporterFull()
    {
        super("selenium-report.html", true);
    }
}
