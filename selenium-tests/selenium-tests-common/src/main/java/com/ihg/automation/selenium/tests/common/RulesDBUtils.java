package com.ihg.automation.selenium.tests.common;

import java.util.Map;

import org.springframework.jdbc.core.JdbcTemplate;

import com.ihg.automation.selenium.common.ProgramCounter;
import com.ihg.automation.selenium.common.types.program.RewardClubLevel;

public class RulesDBUtils
{
    private JdbcTemplate jdbcTemplate;

    public RulesDBUtils(JdbcTemplate jdbcTemplate)
    {
        this.jdbcTemplate = jdbcTemplate;
    }

    private final static String RULE_VALUE = "WITH UPGRADE_RULE AS"//
            + " (SELECT * FROM RULE.RULE WHERE RULE_KEY IN"//
            + " (SELECT RULE_KEY FROM RULE.RULE_ATTR"//
            + " WHERE (RULE_ATTR_TYP_CD='Guest.Program.Level' AND RULE_ATTR_VAL = 'PC,%1$s')"//
            + " OR (RULE_ATTR_TYP_CD='Award.Program.Level' AND RULE_ATTR_VAL = 'PC,%2$s')"//
            + " OR (RULE_ATTR_TYP_CD='ProgramCounter.Code' AND RULE_ATTR_VAL = '%3$s')"//
            + " GROUP BY RULE_KEY HAVING COUNT(*) = 3)"//
            + " AND TRUNC(EFF_TS, 'YEAR')<TRUNC(ADD_MONTHS(SYSDATE,%4$d), 'YEAR') AND TRUNC(LST_EFF_TS, 'YEAR')>=TRUNC(ADD_MONTHS(SYSDATE,%4$d), 'YEAR'))"//
            + " SELECT ra.RULE_ATTR_VAL FROM RULE.RULE_ATTR ra"//
            + " JOIN UPGRADE_RULE ur ON ur.RULE_KEY = ra.RULE_KEY"//
            + " WHERE RULE_ATTR_TYP_CD='ProgramCounter.HurdleValue'"//
            + " AND ROWNUM <2";

    private final static String RULE_VALUE_CURRENT = "WITH UPGRADE_RULE AS"//
            + " (SELECT * FROM RULE.RULE WHERE RULE_KEY IN"//
            + " (SELECT RULE_KEY FROM RULE.RULE_ATTR"//
            + " WHERE (RULE_ATTR_TYP_CD='Guest.Program.Level' AND RULE_ATTR_VAL = 'PC,%1$s')"//
            + " OR (RULE_ATTR_TYP_CD='Award.Program.Level' AND RULE_ATTR_VAL = 'PC,%2$s')"//
            + " OR (RULE_ATTR_TYP_CD='ProgramCounter.Code' AND RULE_ATTR_VAL = '%3$s')"//
            + " GROUP BY RULE_KEY HAVING COUNT(*) = 3)"//
            + " ORDER BY EFF_TS DESC)"//
            + " SELECT ra.RULE_ATTR_VAL FROM RULE.RULE_ATTR ra"//
            + " JOIN UPGRADE_RULE ur ON ur.RULE_KEY = ra.RULE_KEY"//
            + " WHERE RULE_ATTR_TYP_CD='ProgramCounter.HurdleValue'"//
            + " AND ROWNUM <2 ORDER BY EFF_TS DESC";

    private Integer getRule(RewardClubLevel guestLevel, RewardClubLevel awardLevel, String programCounter, int month)
    {
        Map<String, Object> map = jdbcTemplate.queryForMap(
                String.format(RULE_VALUE, guestLevel.getCode(), awardLevel.getCode(), programCounter, month));
        return Integer.parseInt(map.get("RULE_ATTR_VAL").toString());
    }

    private Integer getCurrentRule(RewardClubLevel guestLevel, RewardClubLevel awardLevel, String programCounter)
    {
        Map<String, Object> map = jdbcTemplate.queryForMap(
                String.format(RULE_VALUE_CURRENT, guestLevel.getCode(), awardLevel.getCode(), programCounter));
        return Integer.parseInt(map.get("RULE_ATTR_VAL").toString());
    }

    public Integer getPreviousYearRuleNights(RewardClubLevel guestLevel, RewardClubLevel awardLevel)
    {
        return getRule(guestLevel, awardLevel, ProgramCounter.TIER_LEVEL_NIGHT, -12);
    }

    public Integer getCurrentYearRuleNights(RewardClubLevel guestLevel, RewardClubLevel awardLevel)
    {
        return getCurrentRule(guestLevel, awardLevel, ProgramCounter.TIER_LEVEL_NIGHT);
    }

    public Integer getPreviousYearRulePoints(RewardClubLevel guestLevel, RewardClubLevel awardLevel)
    {
        return getRule(guestLevel, awardLevel, ProgramCounter.TOTAL_QUALIFIED, -12);
    }

    public Integer getCurrentYearRulePoints(RewardClubLevel guestLevel, RewardClubLevel awardLevel)
    {
        return getCurrentRule(guestLevel, awardLevel, ProgramCounter.TOTAL_QUALIFIED);
    }
}
