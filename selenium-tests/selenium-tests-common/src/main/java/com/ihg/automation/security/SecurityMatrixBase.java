package com.ihg.automation.security;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import com.google.common.base.Predicate;
import com.google.common.collect.Maps;

public class SecurityMatrixBase
{
    private static final String ONLY_ROLE_SYSTEM_PROPERTY = "role";

    protected Map<String, Map<String, Boolean>> securityMatrix = new HashMap<>();

    public Object[][] getPermissionArray(Map<String, Boolean> filterCriteria)
    {
        Map<String, Map<String, Boolean>> securityMap;
        String onlyRole = System.getProperty(ONLY_ROLE_SYSTEM_PROPERTY);

        if (StringUtils.isNotEmpty(onlyRole))
        {
            securityMap = new HashMap<>();
            securityMap.put(onlyRole, securityMatrix.get(onlyRole));
        }
        else
        {
            securityMap = securityMatrix;
        }

        if (filterCriteria != null)
        {
            securityMap = Maps.filterValues(securityMatrix, filteringPredicateMap(filterCriteria));
        }

        return mapToArray(securityMap);
    }

    public static Predicate<Map<String, Boolean>> filteringPredicateMap(final Map<String, Boolean> filterMap)
    {
        return new Predicate<Map<String, Boolean>>()
        {
            @Override
            public boolean apply(Map<String, Boolean> input)
            {
                return input.entrySet().containsAll(filterMap.entrySet());
            }
        };
    }

    public Object[][] getPermissionArray()
    {
        return getPermissionArray(null);
    }

    private Object[][] mapToArray(Map<String, Map<String, Boolean>> map)
    {
        Object[][] permissionArray = new Object[map.size()][2];
        int index = 0;

        for (String mapEntry : map.keySet())
        {
            permissionArray[index][0] = mapEntry;
            permissionArray[index][1] = map.get(mapEntry);
            index++;
        }

        return permissionArray;
    }
}
