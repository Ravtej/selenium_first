package com.ihg.automation.logger;

import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.filter.Filter;
import ch.qos.logback.core.spi.FilterReply;

public class LogThreadLocalFilter extends Filter<ILoggingEvent>
{
    private String threadName;

    public LogThreadLocalFilter()
    {
        super();
        threadName = ThreadIdConverter.getThreadId();
    }

    @Override
    public FilterReply decide(ILoggingEvent event)
    {
        if (threadName.equals(ThreadIdConverter.getThreadId()))
        {
            return FilterReply.ACCEPT;
        }
        else
        {
            return FilterReply.DENY;
        }
    }
}
