package com.ihg.crm.automation.selenium.tests.sanitycheck.hotel.sanitycheck;

import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyNoError;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyNoErrors;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasAnyText;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasText;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasTextAsDouble;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.isDisplayedAndEnabled;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.isSelected;
import static com.ihg.automation.selenium.matchers.component.GridSize.size;
import static org.hamcrest.core.IsNot.not;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Value;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.event.SearchButtonBar;
import com.ihg.automation.selenium.common.member.GuestName;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.PersonalInfo;
import com.ihg.automation.selenium.common.stay.BookingDetails;
import com.ihg.automation.selenium.common.stay.Stay;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.gwt.components.grid.GridCell;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.stay.GuestDetailsView;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.stay.RevenueDetailsView;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.stay.StayBookingInfoView;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.stay.StayDetailsView;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.stay.lpu.LoyaltyPendingUpdatesGridRow;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.stay.lpu.LoyaltyPendingUpdatesPage;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.stay.lpu.LpuGridRowContainer;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.stay.stays.ReviewGuestStaysSearchPage;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.stay.stays.StayDetailsPopUp;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.stay.stays.StayGridRowContainer;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.stay.stays.StaysGrid;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.stay.stays.StaysGridRow;
import com.ihg.crm.automation.selenium.tests.sanitycheck.hotel.LoginLogout;

public class LoyaltyPendingUpdatesAndStayTest extends LoginLogout
{
    @Resource(name = "stay")
    protected Stay stay;

    @Value("${stay.member}")
    protected String stayMember;

    private Member member = new Member();

    @BeforeClass
    public void before()
    {
        GuestName name = new GuestName();
        name.setGiven("Christine");
        name.setSurname("Bathier");
        PersonalInfo personalInfo = new PersonalInfo();
        personalInfo.setName(name);
        member.setPersonalInfo(personalInfo);
        member.getPrograms().put(Program.RC, stayMember);

        login(stay.getHotelCode());
    }

    @Test(priority = 1)
    public void validateLPUPage()
    {
        LoyaltyPendingUpdatesPage loyaltyPendingUpdatesPage = new LoyaltyPendingUpdatesPage().goToByHotelOperations();

        LoyaltyPendingUpdatesPage.LoyaltyPendingUpdatesSearch searchFields = loyaltyPendingUpdatesPage
                .getSearchFields();
        verifyThat(searchFields.getHotelCode(), hasText(helper.getUser().getLocation()));
        verifyThat(searchFields.getAvailableDates(), hasAnyText());
        verifyThat(searchFields.getLastDateToAdjust(), hasAnyText());

        SearchButtonBar buttonBar = loyaltyPendingUpdatesPage.getButtonBar();
        verifyThat(buttonBar.getClearCriteria(), isDisplayedAndEnabled(true));
        verifyThat(buttonBar.getSearch(), isDisplayedAndEnabled(true));
        verifyThat(loyaltyPendingUpdatesPage.getCreateMissingStay(), displayed(true));
        verifyThat(loyaltyPendingUpdatesPage.getLoyaltyPendingUpdatesGrid(), size(not(0)));

        loyaltyPendingUpdatesPage.getResultsFilter().verifyDefault();
    }

    @Test(priority = 20)
    public void validateLPUDetails()
    {
        LoyaltyPendingUpdatesPage loyaltyPendingUpdatesPage = new LoyaltyPendingUpdatesPage();
        LoyaltyPendingUpdatesGridRow row = loyaltyPendingUpdatesPage.getLoyaltyPendingUpdatesGrid().getRow(1);
        GridCell cell = row.getCell(LoyaltyPendingUpdatesGridRow.LoyaltyPendingUpdatesGridCell.ADJUST);
        verifyThat(cell.asLink(), displayed(true));
        verifyThat(cell, hasText("Adjust"));

        LpuGridRowContainer rowContainer = row.expand(LpuGridRowContainer.class);
        StayDetailsView stayInfo = rowContainer.getStayInfo();
        verifyThat(stayInfo.getCheckIn(), hasAnyText());
        verifyThat(stayInfo.getCheckOut(), hasAnyText());
        verifyThat(stayInfo.getConfirmationNumber(), hasAnyText());
        verifyThat(stayInfo.getCorpAcctNumber(), hasAnyText());
        verifyThat(stayInfo.getRoomNumber(), hasAnyText());
        verifyThat(stayInfo.getRoomType(), hasAnyText());
        verifyThat(stayInfo.getCorpAcctNumber(), hasAnyText());
        verifyThat(stayInfo.getIATANumber(), hasAnyText());
        verifyThat(stayInfo.getOverlappingStay(), hasAnyText());
        verifyThat(stayInfo.getQualifiedNights(), hasAnyText());
        verifyThat(stayInfo.getHotelCurrency(), hasAnyText());
        verifyThat(stayInfo.getStayPayment(), hasAnyText());

        GuestDetailsView guestDetails = rowContainer.getGuestDetails();
        verifyThat(guestDetails.getMemberName(), hasAnyText());
        verifyThat(guestDetails.getMemberNumber(), hasAnyText());

        StayBookingInfoView stayBookingInfo = rowContainer.getStayBookingInfo();
        verifyThat(stayBookingInfo.getBookingDate(), hasAnyText());
        verifyThat(stayBookingInfo.getBookingSource(), hasAnyText());
        verifyThat(stayBookingInfo.getDataSource(), hasAnyText());

        RevenueDetailsView revenueDetails = rowContainer.getRevenueDetails();
        verifyThat(revenueDetails.getAvgRoomRate(), hasAnyText());
        verifyThat(revenueDetails.getTotalRoom(), hasAnyText());
        verifyThat(revenueDetails.getFood(), hasAnyText());
        verifyThat(revenueDetails.getBeverage(), hasAnyText());
        verifyThat(revenueDetails.getPhone(), hasAnyText());
        verifyThat(revenueDetails.getMeeting(), hasAnyText());
        verifyThat(revenueDetails.getMandatoryRevenue(), hasAnyText());
        verifyThat(revenueDetails.getTotalNonQualifyingRevenue(), hasAnyText());
        verifyThat(revenueDetails.getTotalQualifyingRevenue(), hasAnyText());
        verifyThat(revenueDetails.getNoRevenue(), hasAnyText());
        verifyThat(revenueDetails.getOtherRevenue(), hasAnyText());
    }

    @Test(priority = 20)
    public void validateStayPage()
    {
        ReviewGuestStaysSearchPage reviewGuestStaysSearchPage = new ReviewGuestStaysSearchPage();
        reviewGuestStaysSearchPage.getTab().goTo();
        verifyNoErrors();

        ReviewGuestStaysSearchPage.LoyaltyStaySearch searchFields = reviewGuestStaysSearchPage.getSearchFields();
        reviewGuestStaysSearchPage.getResultsFilter().verifyDefault();

        SearchButtonBar buttonBar = reviewGuestStaysSearchPage.getButtonBar();
        verifyThat(buttonBar.getClearCriteria(), isDisplayedAndEnabled(true));
        verifyThat(buttonBar.getSearch(), isDisplayedAndEnabled(true));

        verifyThat(searchFields.getHotelSearch(), hasText(helper.getUser().getLocation()));
        verifyThat(searchFields.getServiceCenterCreatedStay(), isSelected(true));
        verifyThat(searchFields.getSystemStay(), isSelected(true));
        verifyThat(searchFields.getEnrollingStay(), isSelected(false));
        verifyThat(searchFields.getCheckIn(), isSelected(false));
        verifyThat(searchFields.getCheckOut(), isSelected(true));
        verifyThat(searchFields.getIHGRewardsClubStay(), isSelected(false));
        verifyThat(searchFields.getFromDate(), hasAnyText());
        verifyThat(searchFields.getToDate(), hasAnyText());

        StaysGrid staysGrid = reviewGuestStaysSearchPage.getStayGrid();
        verifyThat(staysGrid, displayed(true));

    }

    @Test(priority = 30)
    public void validateStayDetails()
    {
        ReviewGuestStaysSearchPage reviewGuestStaysSearchPage = new ReviewGuestStaysSearchPage();
        reviewGuestStaysSearchPage.getSearchFields().getFromDate().type(stay.getCheckIn());
        reviewGuestStaysSearchPage.getSearchFields().getToDate().type(stay.getCheckOut());
        reviewGuestStaysSearchPage.getResultsFilter().getConfirmationNumber().type(stay.getConfirmationNumber());
        reviewGuestStaysSearchPage.getButtonBar().clickSearch();
        StaysGrid staysGrid = reviewGuestStaysSearchPage.getStayGrid();
        verifyThat(staysGrid, size(1));

        StaysGridRow row = staysGrid.getRow(1);
        row.verify(member, stay);

        StayGridRowContainer rowContainer = row.expand(StayGridRowContainer.class);
        StayDetailsView stayInfo = rowContainer.getStayInfo();
        stayInfo.verify(stay);

        GuestDetailsView guestDetails = rowContainer.getGuestDetails();
        guestDetails.verify(member);

        StayBookingInfoView stayBookingInfo = rowContainer.getStayBookingInfo();
        BookingDetails bookingDetails = stay.getBookingDetails();
        verifyThat(stayBookingInfo.getBookingSource(), hasText(bookingDetails.getBookingSource()));
        verifyThat(stayBookingInfo.getDataSource(), hasText(bookingDetails.getDataSource()));

        RevenueDetailsView revenueDetails = rowContainer.getRevenueDetails();
        verifyThat(revenueDetails.getAvgRoomRate(), hasTextAsDouble(stay.getAvgRoomRate()));
        revenueDetails.verify(stay);

        rowContainer.getViewStayDetails().clickAndWait(verifyNoError());
        new StayDetailsPopUp().getViewStayDetailsTab().getStayRevenueDetailsView().verifyRevenuesForNewStay(stay);

    }

}
