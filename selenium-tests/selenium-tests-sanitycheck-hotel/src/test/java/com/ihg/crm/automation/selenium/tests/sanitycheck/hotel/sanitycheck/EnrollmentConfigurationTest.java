package com.ihg.crm.automation.selenium.tests.sanitycheck.hotel.sanitycheck;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyNoErrors;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.types.program.Program.AMB;
import static com.ihg.automation.selenium.common.types.program.Program.BR;
import static com.ihg.automation.selenium.common.types.program.Program.EMP;
import static com.ihg.automation.selenium.common.types.program.Program.RC;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.isSelected;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.enroll.EnrollProgramContainer;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.hotel.pages.enroll.EnrollmentPage;
import com.ihg.crm.automation.selenium.tests.sanitycheck.hotel.LoginLogout;

public class EnrollmentConfigurationTest extends LoginLogout
{
    private EnrollmentPage enrollmentPage = new EnrollmentPage();

    @BeforeClass
    public void before()
    {
        login("SHGHA");
    }

    @DataProvider(name = "programProvider")
    protected Object[][] programProvider()
    {
        return new Object[][] { { RC, true, true, true }, { AMB, true, true, true }, { BR, true, true, true },
                { EMP, false, false, false } };
    }

    @Test(dataProvider = "programProvider", priority = 10)
    public void programConfig(Program program, boolean isEarningPreference, boolean isEmployeeID, boolean isRCselected)
    {
        enrollmentPage.goTo();
        verifyNoErrors();

        EnrollProgramContainer programs = enrollmentPage.getPrograms();
        programs.selectProgram(program);
        verifyNoErrors();
        verifyThat(programs.getProgramCheckbox(program), isSelected(true));
        verifyThat(programs.getProgramCheckbox(RC), isSelected(isRCselected));
        enrollmentPage.verifyDefault();

        verifyThat(enrollmentPage.getAdditionalInformation(), displayed(true));

        verifyThat(enrollmentPage.getHotelCode(), displayed(true));
        verifyThat(enrollmentPage.getEarningPreference().getEarningPreference(), displayed(isEarningPreference));
        verifyThat(enrollmentPage.getYourEmployeeID(), displayed(isEmployeeID));
    }

    @AfterMethod
    public void clearProgram()
    {
        enrollmentPage.clickCancel();
    }
}
