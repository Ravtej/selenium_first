package com.ihg.crm.automation.selenium.tests.sanitycheck.hotel.sanitycheck;

import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyNoError;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyNoErrors;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.business.BusinessStatus.BUSINESS_MANAGED_STATUS_LIST;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasAnyText;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasAnyTextInView;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasDefault;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.isDisplayedAndEnabled;
import static com.ihg.automation.selenium.matchers.component.GridSize.size;
import static com.ihg.automation.selenium.matchers.component.HasSelectItems.hasSelectItems;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static com.ihg.automation.selenium.matchers.component.IsDisplayed.displayed;
import static com.ihg.automation.selenium.matchers.component.IsDisplayed.isDisplayedWithWait;
import static com.ihg.automation.selenium.matchers.component.ViewableMatcher.hasTextInView;
import static org.hamcrest.core.IsNot.not;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.business.BusinessRewardsDetails;
import com.ihg.automation.selenium.common.business.BusinessRewardsEventDetailsPopUp;
import com.ihg.automation.selenium.common.business.BusinessRewardsGridRowContainer;
import com.ihg.automation.selenium.common.business.DiscretionaryPointsFields;
import com.ihg.automation.selenium.common.business.EventInformationFields;
import com.ihg.automation.selenium.common.business.EventStaysGrid;
import com.ihg.automation.selenium.common.business.EventSummaryTab;
import com.ihg.automation.selenium.common.business.GuestRoomsFields;
import com.ihg.automation.selenium.common.business.MeetingFields;
import com.ihg.automation.selenium.common.business.MeetingRevenueDetails;
import com.ihg.automation.selenium.common.business.OffersFieldsEdit;
import com.ihg.automation.selenium.common.business.OffersFieldsView;
import com.ihg.automation.selenium.common.business.RoomsTab;
import com.ihg.automation.selenium.common.business.StaySearch;
import com.ihg.automation.selenium.common.business.TotalEligibleRoomRevenue;
import com.ihg.automation.selenium.common.business.TotalEventAwardFields;
import com.ihg.automation.selenium.common.event.SearchButtonBar;
import com.ihg.automation.selenium.common.types.Currency;
import com.ihg.automation.selenium.gwt.components.input.Select;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.br.manage.ManageEventsGrid;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.br.manage.ManageEventsPage;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.br.manage.ManageEventsSearch;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.br.posted.PostedEventsGrid;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.br.posted.PostedEventsPage;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.br.posted.PostedEventsSearch;
import com.ihg.crm.automation.selenium.tests.sanitycheck.hotel.LoginLogout;

public class BusinessRewardsTest extends LoginLogout
{
    private BusinessRewardsEventDetailsPopUp popUp;
    private EventSummaryTab eventSummaryTab;
    private BusinessRewardsGridRowContainer businessContainer;

    @BeforeClass
    public void before()
    {
        login("ATLFX");
    }

    @Test
    public void verifyManageEventsFields()
    {
        ManageEventsPage manageEventsPage = new ManageEventsPage();
        manageEventsPage.goToByOperation();
        verifyThat(manageEventsPage.getCreateEvent(), displayed(true));

        ManageEventsSearch searchFields = manageEventsPage.getSearchFields();
        verifyThat(searchFields.getHotelCode(), hasText(helper.getUser().getLocation()));

        Select eventStatus = searchFields.getEventStatus();
        verifyThat(eventStatus, hasDefault());

        verifyThat(eventStatus, hasSelectItems(BUSINESS_MANAGED_STATUS_LIST));

        verifyThat(searchFields.getFromDate(), hasDefault());
        verifyThat(searchFields.getToDate(), hasDefault());
        verifyThat(searchFields.getEventsWithLimitDays(), displayed(true));

        SearchButtonBar buttonBar = manageEventsPage.getButtonBar();
        verifyThat(buttonBar.getClearCriteria(), isDisplayedAndEnabled(true));
        verifyThat(buttonBar.getSearch(), isDisplayedAndEnabled(true));

        ManageEventsGrid manageEventsGrid = manageEventsPage.getManageEventsGrid();
        verifyThat(manageEventsGrid, size(not(0)));

        manageEventsGrid.verifyHeader();
    }

    @Test(dependsOnMethods = { "verifyManageEventsFields" }, alwaysRun = true)
    public void verifyBREventContainer()
    {
        businessContainer = new ManageEventsPage().getManageEventsGrid().getRow(1)
                .expand(BusinessRewardsGridRowContainer.class);

        verifyBReventContainer(businessContainer);
    }

    @Test(dependsOnMethods = { "verifyBREventContainer" }, alwaysRun = true)
    public void verifyEventInfoSectionInDetailPopUp()
    {
        businessContainer.getEventDetails().clickAndWait();
        popUp = new BusinessRewardsEventDetailsPopUp();
        verifyThat(popUp, displayed(true));
        eventSummaryTab = popUp.getEventSummaryTab();

        EventInformationFields eventInformationFields = eventSummaryTab.getEventInformation();

        verifyThat(eventInformationFields.getMemberNumber(), hasAnyText());
        verifyThat(eventInformationFields.getHotel(), hasText(helper.getUser().getLocation()));
        verifyThat(eventInformationFields.getHotelCurrency(), hasAnyText());
        verifyThat(eventInformationFields.getEventContractDate(), hasAnyText());
        verifyThat(eventInformationFields.getEventName(), hasAnyText());
        verifyThat(eventInformationFields.getHotelContactEmail(), displayed(true));
        verifyThat(eventInformationFields.getHotelContactName(), displayed(true));
        verifyThat(eventInformationFields.getHotelContactPhone(), displayed(true));
        verifyThat(eventInformationFields.getEventType(), hasAnyText());
    }

    @Test(dependsOnMethods = { "verifyEventInfoSectionInDetailPopUp" }, alwaysRun = true)
    public void verifyMeetingSectionInDetailPopUp()
    {
        MeetingFields meetingFields = eventSummaryTab.getEventDetails().getMeetingFields();
        meetingFields.expand();

        verifyThat(meetingFields.getIncludeMeeting(), displayed(true));
        verifyThat(meetingFields.getStartDate(), displayed(true));
        verifyThat(meetingFields.getEndDate(), displayed(true));
        verifyThat(meetingFields.getNumberOfMeetingRooms(), displayed(true));
        verifyThat(meetingFields.getTotalDelegates(), displayed(true));

        MeetingRevenueDetails meetingRevenueDetail = meetingFields.getRevenueDetails();
        meetingRevenueDetail.getMeetingRoom().verifyIsPrePopulated();
        meetingRevenueDetail.getFoodAndBeverage().verifyIsPrePopulated();
        meetingRevenueDetail.getMiscellaneous().verifyIsPrePopulated();
        meetingRevenueDetail.getTotalMeetingRevenue().verifyIsPrePopulated();
    }

    @Test(dependsOnMethods = { "verifyMeetingSectionInDetailPopUp" }, alwaysRun = true)
    public void verifyGuestRoomsSectionInDetailsPopUp()
    {
        GuestRoomsFields guestRoomsFields = eventSummaryTab.getEventDetails().getGuestRoomsFields();
        guestRoomsFields.expand();

        guestRoomsFields.getEligibleRoomRevenue().verifyIsPrePopulated();
        guestRoomsFields.getTotalRoom().verifyIsPrePopulated();

        verifyThat(guestRoomsFields.getCorporateID(), displayed(true));
        verifyThat(guestRoomsFields.getIATANumber(), displayed(true));
        verifyThat(guestRoomsFields.getIncludeGuestRoomsRevenue(), displayed(true));
        verifyThat(guestRoomsFields.getTotalGuestRooms(), displayed(true));
        verifyThat(guestRoomsFields.getStayEndDate(), displayed(true));
        verifyThat(guestRoomsFields.getStayStartDate(), displayed(true));
        verifyThat(guestRoomsFields.getTotalGuests(), displayed(true));
        verifyThat(guestRoomsFields.getTotalRoomNights(), displayed(true));
    }

    @Test(dependsOnMethods = { "verifyGuestRoomsSectionInDetailsPopUp" }, alwaysRun = true)
    public void verifyTotalEventRevenueSectionInDetailPopUp()
    {
        eventSummaryTab.getTotalEligibleRevenue().verifyIsPrePopulated();
    }

    @Test(dependsOnMethods = { "verifyTotalEventRevenueSectionInDetailPopUp" }, alwaysRun = true)
    public void verifyDiscretionaryPointsSectionInDetailPopUp()
    {
        DiscretionaryPointsFields discretionaryPointsFields = eventSummaryTab.getDiscretionaryPoints();
        discretionaryPointsFields.expand();

        verifyThat(discretionaryPointsFields.getAdditionalPointsToAward(), displayed(true));
        verifyThat(discretionaryPointsFields.getComments(), displayed(true));
        verifyThat(discretionaryPointsFields.getIncludeDiscretionaryPoints(), displayed(true));
        verifyThat(discretionaryPointsFields.getReason(), displayed(true));
        verifyThat(discretionaryPointsFields.getEstimatedCost(), hasAnyText());
    }

    @Test(dependsOnMethods = { "verifyDiscretionaryPointsSectionInDetailPopUp" }, alwaysRun = true)
    public void verifyOfferFieldsInDetailPopUp()
    {
        OffersFieldsEdit offerFields = eventSummaryTab.getOffers();
        offerFields.expand();

        verifyThat(offerFields.getIncludeOffers(), displayed(true));
        verifyThat(offerFields.getOffer(), displayed(true));
        verifyThat(offerFields.getPointsToAward(), displayed(true));
        verifyThat(offerFields.getEstimatedCost(), displayed(true));
    }

    @Test(dependsOnMethods = { "verifyOfferFieldsInDetailPopUp" }, alwaysRun = true)
    public void verifyTotalEventAwardSectionInDetailPopUp()
    {
        TotalEventAwardFields totalEventAwardFields = eventSummaryTab.getTotalEventAward();
        totalEventAwardFields.getBasePoints().verifyIsPrePopulated();
        totalEventAwardFields.getDiscretionaryPoints().verifyIsPrePopulated();
        totalEventAwardFields.getOfferPoints().verifyIsPrePopulated();
        totalEventAwardFields.getTotalPoints().verifyIsPrePopulated();
    }

    @Test(dependsOnMethods = { "verifyTotalEventAwardSectionInDetailPopUp" }, alwaysRun = true)
    public void verifyRoomsTabInDetailPopUp()
    {
        RoomsTab roomTab = popUp.getRoomsTab();
        roomTab.goTo();
        verifyNoErrors();

        verifyThat(roomTab.getEventID(), hasAnyText());
        verifyThat(roomTab.getEventStatus(), hasAnyText());
        verifyThat(roomTab.getEventName(), hasAnyText());
        verifyThat(roomTab.getEndDate(), hasAnyText());
        verifyThat(roomTab.getStartDate(), hasAnyText());

        EventStaysGrid eventStaysGrid = roomTab.getEventStaysGrid();
        verifyThat(eventStaysGrid, displayed(true));
        eventStaysGrid.verifyHeader();

        StaySearch staySearch = roomTab.getSearchFields();
        verifyThat(staySearch.getHotel(), hasText(helper.getUser().getLocation()));
        TotalEligibleRoomRevenue totalEligibleRoomRevenue = roomTab.getTotalEligibleRoomRevenue();
        verifyThat(totalEligibleRoomRevenue.getAmount(), hasAnyText());
        verifyThat(totalEligibleRoomRevenue.getCurrency(), hasText(Currency.USD.getCode()));

        verifyThat(staySearch.getGuestName(), displayed(true));
        verifyThat(staySearch.getConfirmationNumber(), displayed(true));
        verifyThat(staySearch.getToDate(), displayed(true));
        verifyThat(staySearch.getFromDate(), displayed(true));

        popUp.clickClose(verifyNoError());
    }

    @Test(dependsOnMethods = { "verifyRoomsTabInDetailPopUp" }, alwaysRun = true)
    public void verifyPostedPointAwardsFields()
    {
        PostedEventsPage postedEventsPage = new PostedEventsPage();
        postedEventsPage.goTo();
        verifyNoErrors();

        PostedEventsSearch searchFields = postedEventsPage.getSearchFields();
        verifyThat(searchFields.getHotelCode(), hasText(helper.getUser().getLocation()));
        verifyThat(searchFields.getFromDate(), hasDefault());
        verifyThat(searchFields.getToDate(), hasAnyText());

        SearchButtonBar buttonBar = postedEventsPage.getButtonBar();
        verifyThat(buttonBar.getClearCriteria(), isDisplayedAndEnabled(true));
        verifyThat(buttonBar.getSearch(), isDisplayedAndEnabled(true));

        PostedEventsGrid manageEventsGrid = postedEventsPage.getManageEventsGrid();
        verifyThat(manageEventsGrid, size(not(0)));
        manageEventsGrid.verifyHeader();

        businessContainer = manageEventsGrid.getRow(1).expand(BusinessRewardsGridRowContainer.class);

        verifyBReventContainer(businessContainer);

        businessContainer.getEventDetails().clickAndWait(verifyNoError());
        popUp = new BusinessRewardsEventDetailsPopUp();
        verifyThat(popUp, isDisplayedWithWait());
        verifyNoErrors();
        verifyThat(popUp.getEventSummaryTab(), isDisplayedWithWait());
        RoomsTab roomsTab = popUp.getRoomsTab();
        roomsTab.goTo();
        verifyNoErrors();
        verifyThat(roomsTab, isDisplayedWithWait());
    }

    private void verifyBReventContainer(BusinessRewardsGridRowContainer businessContainer)
    {
        BusinessRewardsDetails businessRewardsDetails = businessContainer.getBusinessRewardsDetails();

        EventInformationFields eventInformation = businessRewardsDetails.getEventInformation();
        verifyThat(eventInformation.getHotel(), hasTextInView(helper.getUser().getLocation()));
        verifyThat(eventInformation.getHotelCurrency(), hasAnyText());
        verifyThat(eventInformation.getEventContractDate(), hasAnyTextInView());
        verifyThat(eventInformation.getEventType(), hasAnyTextInView());
        verifyThat(eventInformation.getEventName(), hasAnyTextInView());
        verifyThat(eventInformation.getHotelContactEmail(), hasAnyTextInView());
        verifyThat(eventInformation.getHotelContactName(), hasAnyTextInView());
        verifyThat(eventInformation.getHotelContactPhone(), hasAnyTextInView());

        businessRewardsDetails.getTotalEligibleRevenue().verifyIsPrePopulated();

        DiscretionaryPointsFields discretionaryPoints = businessRewardsDetails.getDiscretionaryPoints();
        verifyThat(discretionaryPoints.getAdditionalPointsToAward().getView(), displayed(true));
        verifyThat(discretionaryPoints.getComments().getView(), displayed(true));
        verifyThat(discretionaryPoints.getReason().getView(), displayed(true));
        verifyThat(discretionaryPoints.getEstimatedCost(), hasAnyText());

        OffersFieldsView offers = businessRewardsDetails.getOffers();
        verifyThat(offers.getOffer(), displayed(true));
        verifyThat(offers.getPointsToAward(), displayed(true));
        verifyThat(offers.getEstimatedCost(), hasAnyText());

        TotalEventAwardFields totalEventAwardFields = businessRewardsDetails.getTotalEventAward();
        totalEventAwardFields.getBasePoints().verifyIsPrePopulated();
        totalEventAwardFields.getDiscretionaryPoints().verifyIsPrePopulated();
        totalEventAwardFields.getOfferPoints().verifyIsPrePopulated();
        totalEventAwardFields.getTotalPoints().verifyIsPrePopulated();
    }
}
