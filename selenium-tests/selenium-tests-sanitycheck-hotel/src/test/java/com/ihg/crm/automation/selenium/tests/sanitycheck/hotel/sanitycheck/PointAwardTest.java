package com.ihg.crm.automation.selenium.tests.sanitycheck.hotel.sanitycheck;

import static com.ihg.automation.common.DateUtils.getFormattedDate;
import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyNoError;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyNoErrors;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.hotel.pages.hoteloperations.awards.manage.ManagePointAwardsSearch.DEFAULT_FROM_DATE_SHIFT;
import static com.ihg.automation.selenium.hotel.pages.hoteloperations.awards.manage.ManagePointAwardsSearch.STATUS_LIST;
import static com.ihg.automation.selenium.hotel.pages.hoteloperations.awards.manage.ManagePointAwardsSearch.TYPE_LIST;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasAnyText;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasDefault;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasText;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.isSelected;
import static com.ihg.automation.selenium.matchers.component.GridSize.size;
import static com.ihg.automation.selenium.matchers.component.HasSelectItems.hasSelectItems;
import static org.hamcrest.core.IsNot.not;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.EventSource;
import com.ihg.automation.selenium.gwt.components.input.Select;
import com.ihg.automation.selenium.hotel.pages.Role;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.awards.CreatePointAwardPopUp;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.awards.manage.ManagePointAwardContainer;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.awards.manage.ManagePointAwardDetails;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.awards.manage.ManagePointAwardsGrid;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.awards.manage.ManagePointAwardsGridRow;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.awards.manage.ManagePointAwardsPage;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.awards.manage.ManagePointAwardsSearch;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.awards.posted.PostedPointAwardContainer;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.awards.posted.PostedPointAwardDetails;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.awards.posted.PostedPointAwardsGrid;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.awards.posted.PostedPointAwardsGridRow;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.awards.posted.PostedPointAwardsPage;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.awards.posted.PostedPointAwardsSearch;
import com.ihg.crm.automation.selenium.tests.sanitycheck.hotel.LoginLogout;

public class PointAwardTest extends LoginLogout
{
    @BeforeClass
    public void before()
    {
        login("ATLCP", Role.HOTEL_OPERATIONS_MANAGER);
    }

    @Test
    public void verifyManagePointAwardsPage()
    {
        ManagePointAwardsPage managePointAwardsPage = new ManagePointAwardsPage();
        managePointAwardsPage.goToByOperation();
        verifyNoErrors();

        ManagePointAwardsSearch searchFields = managePointAwardsPage.getSearchFields();
        verifyThat(searchFields.getHotelCode(), hasText(helper.getUser().getLocation()));
        verifyThat(searchFields.getMemberNumber(), hasDefault());
        verifyThat(searchFields.getGuestName(), hasDefault());
        Select type = searchFields.getType();
        verifyThat(type, hasText("All Types"));
        verifyThat(type, hasSelectItems(TYPE_LIST));
        type.click();
        Select status = searchFields.getStatus();
        verifyThat(status, hasSelectItems(STATUS_LIST));

        verifyThat(searchFields.getCheckIn(), isSelected(true));
        verifyThat(searchFields.getRequested(), isSelected(true));

        verifyThat(searchFields.getToDate(), hasText(getFormattedDate()));
        verifyThat(searchFields.getFromDate(), hasText(getFormattedDate(DEFAULT_FROM_DATE_SHIFT)));

        ManagePointAwardsGrid managePointAwardsGrid = managePointAwardsPage.getManagePointAwardsGrid();
        verifyThat(managePointAwardsGrid, displayed(true));

        status.select("Declined");
        managePointAwardsPage.getButtonBar().getSearch().clickAndWait(verifyNoError());
        verifyThat(managePointAwardsGrid, size(not(0)));
        ManagePointAwardsGridRow row = managePointAwardsGrid.getRow(1);
        verifyThat(row.getCell(ManagePointAwardsGridRow.ManagePointAwardsGridCell.STATUS), hasText("Declined"));
        ManagePointAwardContainer container = row.expand(ManagePointAwardContainer.class);

        ManagePointAwardDetails pointAwardDetails = container.getManagePointAwardDetails();
        verifyThat(pointAwardDetails.getBookingDate(), hasAnyText());
        verifyThat(pointAwardDetails.getBookingSource(), hasAnyText());
        verifyThat(pointAwardDetails.getTransactionID(), hasAnyText());
        verifyThat(pointAwardDetails.getTransactionName(), hasAnyText());
        verifyThat(pointAwardDetails.getTransactionDescription(), hasAnyText());
        verifyThat(pointAwardDetails.getConfirmationNumber(), hasAnyText());
        verifyThat(pointAwardDetails.getRateCode(), hasAnyText());
        verifyThat(pointAwardDetails.getRoomRate(), hasAnyText());
        verifyThat(pointAwardDetails.getHotelCurrency(), hasAnyText());
        verifyThat(pointAwardDetails.getIATANumber(), hasAnyText());

        EventSource source = container.getSource();
        verifyThat(source.getChannel(), hasAnyText());
        verifyThat(source.getLocation(), hasAnyText());
        verifyThat(source.getEmployeeId(), hasAnyText());
        verifyThat(source.getUserId(), hasAnyText());

        managePointAwardsPage.getCreatePointAward().clickAndWait(verifyNoError());

        CreatePointAwardPopUp createPointAwardPopUp = new CreatePointAwardPopUp();
        verifyThat(createPointAwardPopUp.getMembershipID(), hasDefault());
        verifyThat(createPointAwardPopUp.getHotel(), hasText(helper.getUser().getLocation()));
        verifyThat(createPointAwardPopUp.getPointAwardType(), hasDefault());
        verifyThat(createPointAwardPopUp.getAwardDescription(), hasDefault());

        verifyThat(createPointAwardPopUp.getClose(), displayed(true));
        verifyThat(createPointAwardPopUp.getSubmit(), displayed(true));
        verifyThat(createPointAwardPopUp.getClear(), displayed(true));
        createPointAwardPopUp.clickClose(verifyNoError());
    }

    @Test(dependsOnMethods = { "verifyManagePointAwardsPage" }, alwaysRun = true)
    public void verifyPostedPointAwardsPage()
    {
        PostedPointAwardsPage postedPointAwardsPage = new PostedPointAwardsPage();
        postedPointAwardsPage.goTo();
        verifyNoErrors();

        PostedPointAwardsSearch searchFields = postedPointAwardsPage.getSearchFields();
        verifyThat(searchFields.getHotelCode(), hasText(helper.getUser().getLocation()));
        verifyThat(searchFields.getMemberNumber(), hasDefault());
        verifyThat(searchFields.getGuestName(), hasDefault());
        Select type = searchFields.getType();
        verifyThat(type, hasText("All Types"));
        verifyThat(type, hasSelectItems(TYPE_LIST));
        verifyThat(searchFields.getAmount(), hasDefault());

        verifyThat(searchFields.getToDate(), hasAnyText());
        verifyThat(searchFields.getFromDate(), hasAnyText());

        PostedPointAwardsGrid postedPointAwardsGrid = postedPointAwardsPage.getPostedPointAwardsGrid();
        verifyThat(postedPointAwardsGrid, displayed(true));

        type.select("Welcome Amenity");
        postedPointAwardsPage.getButtonBar().getSearch().clickAndWait(verifyNoError());
        verifyThat(postedPointAwardsGrid, size(not(0)));
        PostedPointAwardsGridRow row = postedPointAwardsGrid.getRow(1);
        verifyThat(row.getCell(PostedPointAwardsGridRow.PostedPointAwardsGridCell.AWARD_TYPE),
                hasText("Welcome Amenity"));
        PostedPointAwardContainer container = row.expand(PostedPointAwardContainer.class);

        PostedPointAwardDetails pointAwardDetails = container.getPointAwardDetails();
        verifyThat(pointAwardDetails.getCheckInDate(), hasAnyText());
        verifyThat(pointAwardDetails.getCheckOutDate(), hasAnyText());
        verifyThat(pointAwardDetails.getTransactionID(), hasAnyText());
        verifyThat(pointAwardDetails.getTransactionName(), hasAnyText());
        verifyThat(pointAwardDetails.getTransactionDescription(), hasAnyText());
        verifyThat(pointAwardDetails.getHotel(), hasAnyText());
        verifyThat(pointAwardDetails.getCostOfPoints(), hasAnyText());

        EventSource source = container.getSource();
        verifyThat(source.getChannel(), hasAnyText());
        verifyThat(source.getLocation(), hasAnyText());
        verifyThat(source.getEmployeeId(), hasAnyText());
        verifyThat(source.getUserId(), hasAnyText());

    }
}
