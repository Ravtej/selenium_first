package com.ihg.crm.automation.selenium.tests.sanitycheck.hotel.sanitycheck;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.types.WrittenLanguage.ENGLISH;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasAnyText;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static com.ihg.automation.selenium.matchers.component.IsDisplayed.displayed;
import static com.ihg.automation.selenium.matchers.component.ViewableMatcher.hasTextInView;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNot.not;

import org.springframework.beans.factory.annotation.Value;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.communication.ContactPermissionItems;
import com.ihg.automation.selenium.common.communication.MarketingPreferences;
import com.ihg.automation.selenium.common.communication.SmsMobileTextingMarketingPreferences;
import com.ihg.automation.selenium.common.components.Country;
import com.ihg.automation.selenium.common.contact.Address;
import com.ihg.automation.selenium.common.name.PersonNameFields;
import com.ihg.automation.selenium.common.types.Gender;
import com.ihg.automation.selenium.common.types.address.AddressType;
import com.ihg.automation.selenium.hotel.pages.communication.CommunicationPreferences;
import com.ihg.automation.selenium.hotel.pages.communication.CommunicationPreferencesPage;
import com.ihg.automation.selenium.hotel.pages.personal.AddressContact;
import com.ihg.automation.selenium.hotel.pages.personal.CustomerInfoFields;
import com.ihg.automation.selenium.hotel.pages.personal.PersonalInfoPage;
import com.ihg.automation.selenium.hotel.pages.search.GuestSearchByNumber;
import com.ihg.crm.automation.selenium.tests.sanitycheck.hotel.LoginLogout;

public class ProfileTest extends LoginLogout
{
    @Value("${fullName}")
    protected String fullName;

    @Value("${email}")
    protected String email;

    @Value("${smsStatus}")
    protected String smsStatus;

    @BeforeClass
    public void before()
    {
        login("ATLCP");
        new GuestSearchByNumber().byMemberNumber(memberId);
    }

    @Test
    public void validatePersonalInformation()
    {
        PersonalInfoPage personalInfoPage = new PersonalInfoPage();
        personalInfoPage.goTo();

        CustomerInfoFields customerInfo = personalInfoPage.getCustomerInfo();
        verifyThat(customerInfo.getResidenceCountry(), hasText(Country.US.getValue()));
        PersonNameFields name = customerInfo.getName();
        name.setConfiguration(Country.US);
        verifyThat(name.getFullName(), hasText(fullName));
        verifyThat(customerInfo.getGender(), hasTextInView(Gender.MALE.getValue()));
        verifyThat(customerInfo.getBirthDate().getFullDate(), hasAnyText());

        AddressContact addressContact = personalInfoPage.getAddressList().getExpandedContact();
        Address address = addressContact.getBaseContact();
        verifyThat(address.getType(), hasTextInView(AddressType.RESIDENCE.getValue()));
        address.setAddressConfiguration(Country.US);

        verifyThat(address.getCountry(), hasTextInView(Country.US.getValue()));
        verifyThat(address.getAddress1(), hasTextInView("6358 VERNON WOODS DR"));
        verifyThat(address.getRegion1(), hasTextInView("Georgia"));
        verifyThat(address.getLocality1(), hasTextInView("ATLANTA"));
        verifyThat(address.getZipCode(), hasTextInView("30328-3331"));

        verifyThat(personalInfoPage.getPhoneList().getExpandedContact().getBaseContact().getFullPhone(),
                hasText("(678) 488 8695"));
        verifyThat(personalInfoPage.getEmailList().getExpandedContact().getBaseContact().getEmail(),
                hasTextInView(email));
    }

    @Test
    public void verifyCommunicationPreferences()
    {
        CommunicationPreferencesPage commPrefPage = new CommunicationPreferencesPage();
        commPrefPage.goTo();
        CommunicationPreferences commPrefs = commPrefPage.getCommunicationPreferences();
        commPrefs.verify(ENGLISH);

        MarketingPreferences marketingPrefs = commPrefs.getMarketingPreferences();
        verifyThat(marketingPrefs.getEmailAddress(), hasTextInView(email));
        verifyThat(commPrefs.getMarketingPreferences().getInvalidEmailImage(), displayed(false));

        ContactPermissionItems marketingPermissionItems = marketingPrefs.getContactPermissionItems();
        int marketingItemsCount = marketingPermissionItems.getContactPermissionItemsCount();
        verifyThat(marketingPrefs, marketingItemsCount, not(is(0)), "There are items in marketing preferences");

        SmsMobileTextingMarketingPreferences smsMobileTextingMarketingPreferences = commPrefs
                .getSmsMobileTextingMarketingPreferences();
        verifyThat(smsMobileTextingMarketingPreferences.getSmsNumber(), hasTextInView("Provided"));
        verifyThat(smsMobileTextingMarketingPreferences.getResend(), displayed(true));
        verifyThat(smsMobileTextingMarketingPreferences.getSMSNumberConfirmationStatus(), hasText(smsStatus));

        ContactPermissionItems smsPermissionItems = smsMobileTextingMarketingPreferences.getContactPermissionItems();
        int smsItemsCount = smsPermissionItems.getContactPermissionItemsCount();
        verifyThat(smsPermissionItems, smsItemsCount, not(is(0)), "There are items in sms preferences");
    }
}
