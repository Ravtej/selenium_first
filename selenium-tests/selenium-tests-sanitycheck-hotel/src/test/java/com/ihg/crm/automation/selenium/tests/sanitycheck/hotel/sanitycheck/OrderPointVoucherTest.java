package com.ihg.crm.automation.selenium.tests.sanitycheck.hotel.sanitycheck;

import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyNoError;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.event.EventTransactionType.Order.ORDER_VOUCHER;
import static com.ihg.automation.selenium.hotel.pages.order.OrdersGridRow.OrdersGridCell.BILLING_AMT_USE;
import static com.ihg.automation.selenium.hotel.pages.order.OrdersGridRow.OrdersGridCell.ITEM_NAME;
import static com.ihg.automation.selenium.hotel.pages.order.OrdersGridRow.OrdersGridCell.STATUS;
import static com.ihg.automation.selenium.hotel.pages.order.OrdersGridRow.OrdersGridCell.TRANS_TYPE;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.enabled;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasText;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.isDisplayedAndEnabled;
import static com.ihg.automation.selenium.matchers.component.GridSize.size;
import static com.ihg.automation.selenium.matchers.text.EntityMatcherFactory.isValue;
import static org.hamcrest.Matchers.greaterThan;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Value;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.components.Country;
import com.ihg.automation.selenium.common.order.VoucherOrder;
import com.ihg.automation.selenium.common.order.VoucherRule;
import com.ihg.automation.selenium.hotel.pages.LeftPanel;
import com.ihg.automation.selenium.hotel.pages.Role;
import com.ihg.automation.selenium.hotel.pages.Tabs;
import com.ihg.automation.selenium.hotel.pages.order.NewOrdersPage;
import com.ihg.automation.selenium.hotel.pages.order.OrderDetailsPopUp;
import com.ihg.automation.selenium.hotel.pages.order.OrderDetailsTab;
import com.ihg.automation.selenium.hotel.pages.order.OrderHistoryPage;
import com.ihg.automation.selenium.hotel.pages.order.OrdersGridRow;
import com.ihg.automation.selenium.hotel.pages.order.OrdersSearch;
import com.ihg.automation.selenium.hotel.pages.order.VoucherOrderGridRowContainer;
import com.ihg.automation.selenium.hotel.pages.order.VoucherOrderPopUp;
import com.ihg.automation.selenium.hotel.pages.order.VouchersGridRow;
import com.ihg.automation.selenium.hotel.pages.personal.CustomerInfoFields;
import com.ihg.automation.selenium.hotel.pages.personal.HotelAddressContactList;
import com.ihg.automation.selenium.hotel.pages.personal.PersonalInfoPage;
import com.ihg.crm.automation.selenium.tests.sanitycheck.hotel.LoginLogout;

public class OrderPointVoucherTest extends LoginLogout
{
    @Resource(name = "voucherOrder")
    protected VoucherOrder voucherOrder;

    @Resource(name = "voucherRule")
    protected VoucherRule voucherRule;

    @Value("${voucher.hotel}")
    protected String hotel;

    @Value("${voucher.hotelBrand}")
    protected String hotelBrand;

    @Value("${voucher.hotelCountry}")
    protected Country hotelCountry;

    @BeforeClass
    public void before()
    {
        login(hotel, Role.HOTEL_OPERATIONS_MANAGER);
        new LeftPanel().getOrderPointVoucher().clickAndWait(verifyNoError());
    }

    @Test(priority = 1)
    public void verifyPersonalInfoPage()
    {
        PersonalInfoPage personalInfoPage = new PersonalInfoPage();
        CustomerInfoFields customerInfo = personalInfoPage.getCustomerInfo();
        verifyThat(customerInfo.getFullName(), hasText(hotelBrand + " " + helper.getUser().getLocation()));
        verifyThat(customerInfo.getResidenceCountry(), hasText(isValue(hotelCountry)));
        verifyThat(customerInfo.getEdit(), isDisplayedAndEnabled(false));

        HotelAddressContactList hotelAddressContactList = personalInfoPage.getAddressList();
        verifyThat(hotelAddressContactList.getAddButton(), enabled(false));

        verifyThat(personalInfoPage.getPhoneList().getAddButton(), enabled(false));
        verifyThat(personalInfoPage.getSmsList(), displayed(false));
        verifyThat(personalInfoPage.getEmailList().getAddButton(), enabled(false));
    }

    @Test(priority = 5)
    public void verifyOrderHistoryFields()
    {
        new Tabs().getEventsTab().clickAndWait(verifyNoError());

        OrderHistoryPage orderHistoryPage = new OrderHistoryPage();
        OrdersSearch searchFields = orderHistoryPage.getSearchFields();
        verifyThat(searchFields.getItemId(), isDisplayedAndEnabled(true));
        verifyThat(searchFields.getItemName(), isDisplayedAndEnabled(true));
        verifyThat(searchFields.getDateFrom(), isDisplayedAndEnabled(true));
        verifyThat(searchFields.getDateTo(), isDisplayedAndEnabled(true));
        verifyThat(searchFields.getStatus(), isDisplayedAndEnabled(true));

        verifyThat(orderHistoryPage.getOrdersGrid(), size(greaterThan(0)));
    }

    @Test(priority = 10)
    public void verifyNewOrdersTabFields()
    {
        new Tabs().getEventsTab().getNewOrders().clickAndWait(verifyNoError());
        verifyThat(new NewOrdersPage().getVouchersGrid(), size(greaterThan(0)));
    }

    @Test(priority = 15)
    public void verifyVoucherOrderPopUp()
    {
        VouchersGridRow vouchersGridRow = new NewOrdersPage().getVouchersGrid().getRow(voucherRule.getPromotionId());
        vouchersGridRow.verify(voucherRule);
        vouchersGridRow.clickById();

        VoucherOrderPopUp voucherOrderPopUp = new VoucherOrderPopUp();
        voucherOrderPopUp.verify(voucherRule);
        voucherOrderPopUp.verifyShippingInfoPaperFulfillment();
        voucherOrderPopUp.clickCancel();
    }

    @Test(priority = 30)
    public void verifyCreatedOrder()
    {
        OrderHistoryPage orderHistoryPage = new OrderHistoryPage();
        orderHistoryPage.goTo();
        verifyNoError();
        orderHistoryPage.searchOrder(voucherOrder);

        OrdersGridRow gridRow = orderHistoryPage.getOrdersGrid().getRow(1);
        verifyThat(gridRow.getCell(TRANS_TYPE), hasText(ORDER_VOUCHER));
        verifyThat(gridRow.getCell(ITEM_NAME), hasText(voucherOrder.getVoucherName()));
        verifyThat(gridRow.getCell(STATUS), hasText(voucherOrder.getStatus()));
        verifyThat(gridRow.getCell(BILLING_AMT_USE), hasText(voucherOrder.getVoucherCostAmount()));

        VoucherOrderGridRowContainer rowContainer = gridRow.expand(VoucherOrderGridRowContainer.class);
        rowContainer.getOrderDetailsView().verify(voucherOrder);
        rowContainer.getShippingInfo().verifyAsOnCustomerInformation(voucherOrder.getDeliveryOption());

        rowContainer.clickDetails();
        OrderDetailsTab orderDetailsTab = new OrderDetailsPopUp().getOrderDetailsTab();
        orderDetailsTab.getOrderDetails().verify(voucherOrder);
        orderDetailsTab.getShippingInfoView().verifyAsOnCustomerInformation(voucherOrder.getDeliveryOption());
    }
}
