package com.ihg.crm.automation.selenium.tests.sanitycheck.hotel.sanitycheck;

import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyNoError;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyNoErrors;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.hotel.pages.hoteloperations.reimbursement.certificate.CertificateSearch.TYPES;
import static com.ihg.automation.selenium.hotel.pages.hoteloperations.reimbursement.freenight.FreeNightFlatReimbursementSearch.STATUS;
import static com.ihg.automation.selenium.hotel.pages.hoteloperations.reimbursement.occupancy.OccupancyAndADREntryPage.STATUS_LIST;
import static com.ihg.automation.selenium.hotel.pages.hoteloperations.reimbursement.occupancy.OccupancyAndADREntryPage.TYPE_LIST;
import static com.ihg.automation.selenium.hotel.pages.hoteloperations.reimbursement.otheractivity.OtherActivitySearch.OTHER_STATUS_LIST;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasAnyText;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasDefault;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.isDisplayedAndEnabled;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.isSelected;
import static com.ihg.automation.selenium.matchers.component.GridSize.size;
import static com.ihg.automation.selenium.matchers.component.HasSelectItems.hasSelectItems;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static com.ihg.automation.selenium.matchers.component.IsDisplayed.displayed;
import static com.ihg.automation.selenium.matchers.component.IsDisplayed.isDisplayedWithWait;
import static org.hamcrest.core.IsNot.not;

import org.joda.time.LocalDate;
import org.springframework.beans.factory.annotation.Value;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.business.BusinessRewardsGridRowContainer;
import com.ihg.automation.selenium.common.event.SearchButtonBar;
import com.ihg.automation.selenium.gwt.components.Button;
import com.ihg.automation.selenium.gwt.components.input.DateInput;
import com.ihg.automation.selenium.gwt.components.input.Select;
import com.ihg.automation.selenium.hotel.pages.LeftPanel;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.HotelOperationsPage;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.reimbursement.certificate.CertificateGridRowContainer;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.reimbursement.certificate.CertificateSearch;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.reimbursement.certificate.CertificateSearchGrid;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.reimbursement.certificate.CertificateSearchPage;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.reimbursement.freenight.FreeNightFlatReimbursementPage;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.reimbursement.freenight.FreeNightFlatReimbursementSearch;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.reimbursement.occupancy.OccupancyAndADREntryPage;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.reimbursement.occupancy.OccupancyAndADREntrySearch;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.reimbursement.occupancy.OccupancyAndADREntrySearchGrid;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.reimbursement.occupancy.OccupancyGridRowContainer;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.reimbursement.otheractivity.OtherActivityContainer;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.reimbursement.otheractivity.OtherActivityGrid;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.reimbursement.otheractivity.OtherActivityPage;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.reimbursement.otheractivity.OtherActivitySearch;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.reimbursement.rewardnight.LowOccupancyReimbursementAmount;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.reimbursement.rewardnight.RewardAndFreeNightsCount;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.reimbursement.rewardnight.RewardNightsSettingsPage;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.reimbursement.rewardnight.TaxesAndFees;
import com.ihg.crm.automation.selenium.tests.sanitycheck.hotel.LoginLogout;

public class ReimbursementTest extends LoginLogout
{
    @Value("${reimbursement.hotel}")
    protected String reimbursementHotel;

    private BusinessRewardsGridRowContainer businessContainer;

    @BeforeClass
    public void before()
    {
        login(reimbursementHotel);
    }

    @Test
    public void verifyOccupancyAndADREntryPage()
    {
        new LeftPanel().getHotelOperations().clickAndWait(verifyNoError());
        new HotelOperationsPage().getProcessHotelReimbForRewardFreeNights().clickAndWait(verifyNoError());

        OccupancyAndADREntryPage occupancyAndADREntryPage = new OccupancyAndADREntryPage();
        verifyThat(occupancyAndADREntryPage, displayed(true));

        OccupancyAndADREntrySearch searchFields = occupancyAndADREntryPage.getSearchFields();
        verifyThat(searchFields.getHotelCode(), hasText(helper.getUser().getLocation()));
        verifyThat(searchFields.getStatus(), hasText("All Statuses"));
        Select type = searchFields.getType();
        verifyThat(type, hasSelectItems(TYPE_LIST));
        verifyThat(searchFields.getStatus(), hasSelectItems(STATUS_LIST));
        verifyThat(searchFields.getFromDate(), hasAnyText());
        verifyThat(searchFields.getToDate(), hasAnyText());

        type.select("Reward Night");
        SearchButtonBar buttonBar = occupancyAndADREntryPage.getButtonBar();
        buttonBar.getSearch().clickAndWait(verifyNoError());

        OccupancyAndADREntrySearchGrid occupancyGrid = occupancyAndADREntryPage.getOccupancyAndADREntrySearchGrid();
        verifyThat(occupancyGrid, size(not(0)));
        OccupancyGridRowContainer rowContainer = occupancyGrid.getRow(1).expand(OccupancyGridRowContainer.class);
        verifyThat(rowContainer.getCertificateType(), hasText("Reward Night"));
        verifyThat(rowContainer.getConfirmationNumber(), hasAnyText());
        verifyThat(rowContainer.getMemberID(), hasAnyText());
        verifyThat(rowContainer.getLastName(), hasAnyText());
        verifyThat(rowContainer.getCertificateNumber(), hasAnyText());
        verifyThat(rowContainer.getSuffix(), hasAnyText());
        verifyThat(rowContainer.getStatus(), hasAnyText());
        verifyThat(rowContainer.getOfferName(), hasAnyText());
        verifyThat(rowContainer.getReimbursementAmount(), hasAnyText());
        verifyThat(rowContainer.getReimbursementMethod(), hasAnyText());
        verifyThat(rowContainer.getReimbursementDate(), hasAnyText());

        rowContainer.getStayView().clickAndWait(verifyNoError());
        verifyThat(new CertificateSearchPage(), isDisplayedWithWait());
        occupancyAndADREntryPage.goTo();
        verifyNoErrors();
    }

    @Test(dependsOnMethods = { "verifyOccupancyAndADREntryPage" }, alwaysRun = true)
    public void verifyCertificateSearchPage()
    {
        CertificateSearchPage certificateSearchPage = new CertificateSearchPage();
        certificateSearchPage.goTo();
        verifyNoErrors();

        CertificateSearch searchFields = certificateSearchPage.getSearchFields();
        verifyThat(searchFields.getHotelCode(), hasText(helper.getUser().getLocation()));
        verifyThat(searchFields.getCertificateNumber(), displayed(true));
        verifyThat(searchFields.getCheckIn(), isSelected(false));
        verifyThat(searchFields.getCheckOut(), isSelected(true));

        Select type = searchFields.getType();
        verifyThat(type, hasSelectItems(TYPES));

        DateInput fromDate = searchFields.getFromDate();
        verifyThat(fromDate, hasAnyText());
        verifyThat(searchFields.getToDate(), hasAnyText());

        type.select("Free Night");
        fromDate.type(LocalDate.now().minusMonths(3));
        SearchButtonBar buttonBar = certificateSearchPage.getButtonBar();
        buttonBar.getSearch().clickAndWait(verifyNoError());

        CertificateSearchGrid certificateSearchGrid = certificateSearchPage.getCertificateSearchGrid();
        verifyThat(certificateSearchGrid, size(not(0)));
        CertificateGridRowContainer rowContainer = certificateSearchGrid.getRow(1)
                .expand(CertificateGridRowContainer.class);
        verifyThat(rowContainer.getCertificateType(), hasText("Free Night"));
        verifyThat(rowContainer.getMemberID(), hasAnyText());
        verifyThat(rowContainer.getLastName(), hasAnyText());
        verifyThat(rowContainer.getCertificateNumber(), hasAnyText());
        verifyThat(rowContainer.getSuffix(), hasAnyText());
        verifyThat(rowContainer.getStatus(), hasAnyText());
        verifyThat(rowContainer.getOfferName(), hasAnyText());
        verifyThat(rowContainer.getReimbursementType(), hasAnyText());
        verifyThat(rowContainer.getReimbursementMethod(), hasAnyText());
        verifyThat(rowContainer.getReimbursementDate(), hasAnyText());
        verifyThat(rowContainer.getCheckIn(), hasAnyText());
        verifyThat(rowContainer.getCheckOut(), hasAnyText());
        verifyThat(rowContainer.getTotalActiveNights(), hasAnyText());
        verifyThat(rowContainer.getTotalReimbursed(), hasAnyText());
    }

    @Test(dependsOnMethods = { "verifyOccupancyAndADREntryPage" }, alwaysRun = true)
    public void verifyRewardNightsSettingsPage()
    {
        RewardNightsSettingsPage rewardNightsSettingsPage = new RewardNightsSettingsPage();
        rewardNightsSettingsPage.goTo();
        verifyNoErrors();

        LowOccupancyReimbursementAmount lowOccupancyReimbursementAmount = rewardNightsSettingsPage
                .getLowOccupancyReimbursementAmount();
        verifyThat(lowOccupancyReimbursementAmount, displayed(true));
        verifyThat(lowOccupancyReimbursementAmount.getLowOccupancyReimbursementAmountGrid(), size(not(0)));
        verifyThat(lowOccupancyReimbursementAmount.getExceptionsGrid(), displayed(true));

        RewardAndFreeNightsCount rewardAndFreeNightsCount = rewardNightsSettingsPage.getRewardAndFreeNightsCount();
        verifyThat(rewardAndFreeNightsCount, displayed(true));
        verifyThat(rewardAndFreeNightsCount.getRewardAndFreeNightsCountGrid(), size(not(0)));

        TaxesAndFees taxesAndFees = rewardNightsSettingsPage.getTaxesAndFees();
        verifyThat(taxesAndFees, displayed(true));
        verifyThat(taxesAndFees.getTaxesAndFeesGrid(), size(not(0)));
    }

    @Test(dependsOnMethods = { "verifyOccupancyAndADREntryPage" }, alwaysRun = true)
    public void verifyFreeNightFlatReimbursementPage()
    {
        FreeNightFlatReimbursementPage freeNightFlatReimbursementPage = new FreeNightFlatReimbursementPage();
        freeNightFlatReimbursementPage.goTo();
        verifyNoErrors();

        FreeNightFlatReimbursementSearch searchFields = freeNightFlatReimbursementPage.getSearchFields();
        verifyThat(searchFields.getHotelCode(), hasText(helper.getUser().getLocation()));
        Select status = searchFields.getStatus();
        verifyThat(status, hasText("All Statuses"));
        verifyThat(status, hasSelectItems(STATUS));
        verifyThat(searchFields.getFromDate(), hasAnyText());
        verifyThat(searchFields.getToDate(), hasAnyText());

        verifyThat(freeNightFlatReimbursementPage.getButtonBar().getSearch(), isDisplayedAndEnabled(true));
        verifyThat(freeNightFlatReimbursementPage.getFreeNightFlatReimbursementGrid(), displayed(true));
    }

    @Test(dependsOnMethods = { "verifyOccupancyAndADREntryPage" }, alwaysRun = true)
    public void verifyOtherActivitiesPage()
    {
        OtherActivityPage otherActivityPage = new OtherActivityPage();
        otherActivityPage.goTo();
        verifyNoErrors();

        OtherActivitySearch searchFields = otherActivityPage.getSearchFields();
        verifyThat(searchFields.getHotelCode(), hasText(helper.getUser().getLocation()));
        verifyThat(searchFields.getItemID(), displayed(true));
        verifyThat(searchFields.getStatusOfRequest(), hasSelectItems(OTHER_STATUS_LIST));
        verifyThat(searchFields.getCertificateNumber(), displayed(true));
        verifyThat(searchFields.getConfirmationNumber(), displayed(true));
        verifyThat(searchFields.getFromDate(), hasDefault());
        verifyThat(searchFields.getToDate(), hasDefault());
        verifyThat(searchFields.getCheckIn(), isSelected(true));
        verifyThat(searchFields.getCheckOut(), isSelected(false));

        SearchButtonBar buttonBar = otherActivityPage.getButtonBar();
        verifyThat(buttonBar.getClearCriteria(), isDisplayedAndEnabled(true));
        Button search = buttonBar.getSearch();
        verifyThat(search, isDisplayedAndEnabled(true));
        OtherActivityGrid otherActivityGrid = otherActivityPage.getOtherActivityGrid();
        searchFields.getStatusOfRequest().select("CANCELED");
        search.clickAndWait(verifyNoError());

        verifyThat(otherActivityGrid, size(not(0)));

        OtherActivityContainer rowContainer = otherActivityGrid.getRow(1).expand(OtherActivityContainer.class);
        verifyThat(rowContainer.getBillingEntity(), hasAnyText());
        verifyThat(rowContainer.getConfirmationNumber(), hasAnyText());
        verifyThat(rowContainer.getItemID(), hasAnyText());
        verifyThat(rowContainer.getDescriptionGuestName(), hasAnyText());
        verifyThat(rowContainer.getCertificateNumber(), hasAnyText());
        verifyThat(rowContainer.getFolioNumber(), hasAnyText());
        verifyThat(rowContainer.getStatus(), hasText("CANCELED"));
        verifyThat(rowContainer.getBundleNumber(), hasAnyText());
        verifyThat(rowContainer.getReimbursementAmount(), hasAnyText());
        verifyThat(rowContainer.getReimbursementMethod(), hasAnyText());
        verifyThat(rowContainer.getReimbursementDate(), hasAnyText());
        verifyThat(rowContainer.getCheckInDate(), hasAnyText());
        verifyThat(rowContainer.getCheckOutDate(), hasAnyText());
    }
}
