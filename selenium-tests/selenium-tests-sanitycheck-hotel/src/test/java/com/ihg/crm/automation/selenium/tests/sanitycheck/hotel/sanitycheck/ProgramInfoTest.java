package com.ihg.crm.automation.selenium.tests.sanitycheck.hotel.sanitycheck;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyNoErrors;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.Constant.NOT_AVAILABLE;
import static com.ihg.automation.selenium.common.types.program.AmbassadorLevel.AMB;
import static com.ihg.automation.selenium.common.types.program.ProgramStatus.OPEN;
import static com.ihg.automation.selenium.common.types.program.RewardClubLevel.SPRE;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasAnyText;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static com.ihg.automation.selenium.matchers.text.EntityMatcherFactory.isValue;

import org.springframework.beans.factory.annotation.Value;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.Constant;
import com.ihg.automation.selenium.common.pages.program.EmployeeRateEligibility;
import com.ihg.automation.selenium.hotel.pages.programs.EmployeeProgramPage;
import com.ihg.automation.selenium.hotel.pages.programs.ProgramPageBase;
import com.ihg.automation.selenium.hotel.pages.programs.ProgramSummary;
import com.ihg.automation.selenium.hotel.pages.programs.amb.AmbassadorPage;
import com.ihg.automation.selenium.hotel.pages.programs.amb.AmbassadorSummary;
import com.ihg.automation.selenium.hotel.pages.programs.br.BusinessRewardsPage;
import com.ihg.automation.selenium.hotel.pages.programs.br.BusinessRewardsSummary;
import com.ihg.automation.selenium.hotel.pages.programs.rc.RewardClubPage;
import com.ihg.automation.selenium.hotel.pages.programs.rc.RewardClubSummary;
import com.ihg.automation.selenium.hotel.pages.search.GuestSearchByNumber;
import com.ihg.crm.automation.selenium.tests.sanitycheck.hotel.LoginLogout;

public class ProgramInfoTest extends LoginLogout
{
    @Value("${employeeMemberId}")
    protected String employeeMemberId;

    @BeforeClass
    public void before()
    {
        login("SHGHA");
        new GuestSearchByNumber().byMemberNumber(memberId);
    }

    @Test(priority = 10)
    public void validateRewardClubPage()
    {
        RewardClubPage rewardClubPage = new RewardClubPage();
        rewardClubPage.goTo();
        verifyNoErrors();

        RewardClubSummary summary = rewardClubPage.getSummary();
        verifyThat(summary.getMemberId(), hasText(memberId));
        verifyThat(summary.getBalance(), hasAnyText());

        verifyThat(summary.getStatus(), hasText(OPEN));
        verifyThat(summary.getTierLevel(), hasText(isValue(SPRE)));

        EmployeeRateEligibility employeeRateEligibility = rewardClubPage.getEmployeeRateEligibility();
        verifyThat(employeeRateEligibility, displayed(true));
        employeeRateEligibility.verifyInViewMode("Yes", "Does not expire", NOT_AVAILABLE, "Merlin");

        verifyThat(rewardClubPage.getCurrentYearAnnualActivities(), displayed(true));
        verifyThat(rewardClubPage.getEarningDetails().getEarningPreference(), hasText(Constant.RC_POINTS));
        verifyThat(rewardClubPage.getAlliances(), displayed(true));

        ProgramPageBase.EnrollmentDetails enrollmentDetails = rewardClubPage.getEnrollmentDetails();
        verifyThat(enrollmentDetails.getEnrollDate(), hasAnyText());
        verifyThat(enrollmentDetails.getOfferCode(), hasText("SCCAL"));
        verifyThat(enrollmentDetails.getReferringMember(), hasText(NOT_AVAILABLE));
    }

    @Test(priority = 10)
    public void validateAmbassadorPage()
    {
        AmbassadorPage ambassadorPage = new AmbassadorPage();
        ambassadorPage.goTo();
        verifyNoErrors();

        AmbassadorSummary summary = ambassadorPage.getSummary();
        verifyThat(summary.getMemberId(), hasText(memberId));
        verifyThat(summary.getExpirationDate(), hasAnyText());
        verifyThat(summary.getStatus(), hasText(OPEN));
        verifyThat(summary.getTierLevel(), hasText(isValue(AMB)));

        ProgramPageBase.EnrollmentDetails details = ambassadorPage.getEnrollmentDetails();
        verifyThat(details.getEnrollDate(), hasAnyText());
        verifyThat(details.getOfferCode(), hasText("SCBRS"));
        verifyThat(details.getReferringMember(), hasText(NOT_AVAILABLE));
    }

    @Test(priority = 10)
    public void validateBusinessRewardsPage()
    {
        BusinessRewardsPage businessRewardsPage = new BusinessRewardsPage();
        businessRewardsPage.goTo();
        verifyNoErrors();

        BusinessRewardsSummary summary = businessRewardsPage.getSummary();
        verifyThat(summary.getMemberId(), hasText(memberId));
        verifyThat(summary.getStatus(), hasText(OPEN));
        verifyThat(summary.getTermsAndConditions(), hasAnyText());
        verifyThat(summary.getDateOfResponse(), hasAnyText());
        verifyThat(summary.getSource(), hasAnyText());

        ProgramPageBase.EnrollmentDetails enrollDetails = businessRewardsPage.getEnrollmentDetails();

        verifyThat(enrollDetails.getEnrollDate(), hasAnyText());
        verifyThat(enrollDetails.getReferringMember(), hasText(NOT_AVAILABLE));
        verifyThat(enrollDetails.getEnrollDate(), hasAnyText());
        verifyThat(enrollDetails.getOfferCode(), hasText(NOT_AVAILABLE));
    }

    @Test(priority = 10)
    public void validateEmployeePage()
    {
        EmployeeProgramPage employeeProgramPage = new EmployeeProgramPage();
        employeeProgramPage.goTo();
        verifyNoErrors();

        ProgramSummary summary = employeeProgramPage.getSummary();
        verifyThat(summary.getMemberId(), hasText(employeeMemberId));
        verifyThat(summary.getStatus(), hasText(OPEN));

        ProgramPageBase.EnrollmentDetails enrollDetails = employeeProgramPage.getEnrollmentDetails();

        verifyThat(enrollDetails.getEnrollDate(), hasAnyText());
        verifyThat(enrollDetails.getReferringMember(), hasText(NOT_AVAILABLE));
        verifyThat(enrollDetails.getEnrollDate(), hasAnyText());
        verifyThat(enrollDetails.getOfferCode(), hasText("SCCAL"));
    }
}
