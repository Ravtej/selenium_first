package com.ihg.crm.automation.selenium.tests.sanitycheck.hotel.sanitycheck;

import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyNoError;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyNoErrors;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;

import org.springframework.beans.factory.annotation.Value;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.CustomerInfoPanel;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.hotel.pages.LeftPanel;
import com.ihg.automation.selenium.hotel.pages.search.GuestSearch;
import com.ihg.automation.selenium.hotel.pages.search.GuestSearchByNumber;
import com.ihg.automation.selenium.hotel.pages.search.SearchByStay;
import com.ihg.automation.selenium.matchers.component.ComponentMatcher;
import com.ihg.crm.automation.selenium.tests.sanitycheck.hotel.LoginLogout;

public class SearchTest extends LoginLogout
{
    @Value("${stay.date}")
    protected String stayDate;

    @Value("${stay.folioNumber}")
    protected String folioNumber;

    @Value("${stay.confirmationNumber}")
    protected String confirmationNumber;

    @Value("${stay.hotel}")
    protected String stayHotel;

    @Value("${stay.memberId}")
    protected String memberId;

    private GuestSearch guestSearch = new GuestSearch();
    private SearchByStay searchByStay;

    @BeforeClass
    public void beforeClass()
    {
        login(stayHotel);
    }

    @Test(priority = 10)
    public void defaultSearchCriteria()
    {
        new GuestSearchByNumber().getAdvancedSearch().clickAndWait(verifyNoError());
        searchByStay = guestSearch.getSearchByStay();
        searchByStay.expand();

        new GuestSearch().getCustomerSearch().verifyDefault();
    }

    @Test(priority = 20)
    public void searchByMemberId()
    {
        guestSearch.byMemberNumber(memberId, verifyNoError());

        verifySearchResults();
    }

    @Test(priority = 20)
    public void searchByHotelCheckoutFolio()
    {
        guestSearch.clickClear();

        searchByStay.expand();
        searchByStay.getHotel().type(stayHotel);
        searchByStay.getStayDate().type(stayDate);
        searchByStay.getFolioNumber().type(folioNumber);
        guestSearch.clickSearch(verifyNoError());

        verifySearchResults();
    }

    @Test(priority = 20)
    public void searchByHotelCheckoutConfirmation()
    {
        guestSearch.clickClear();

        searchByStay.expand();
        searchByStay.getHotel().type(stayHotel);
        searchByStay.getStayDate().type(stayDate);
        searchByStay.getConfirmationNumber().type(confirmationNumber);
        guestSearch.clickSearch(verifyNoError());

        verifySearchResults();
    }

    private void verifySearchResults()
    {
        verifyThat(new CustomerInfoPanel().getMemberNumber(Program.RC), ComponentMatcher.hasText(memberId));

        new LeftPanel().goToNewSearch();
        verifyNoErrors();
    }
}
