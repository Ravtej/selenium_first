package com.ihg.crm.automation.selenium.tests.hotel.enrollment.br;

import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyNoError;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasText;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.isDisplayedWithWait;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.ProgramInfoPanel;
import com.ihg.automation.selenium.common.ProgramsGrid;
import com.ihg.automation.selenium.common.enroll.SuccessEnrolmentPopUp;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.common.types.program.ProgramStatusReason;
import com.ihg.automation.selenium.common.types.program.RewardClubLevel;
import com.ihg.automation.selenium.hotel.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.hotel.pages.programs.amb.PrivacyConfirmationPopUp;
import com.ihg.crm.automation.selenium.tests.hotel.LoginLogout;

public class BrEnrollmentEuropeTest extends LoginLogout
{
    @BeforeClass
    public void beforeClass()
    {
        Member member = new Member();
        member.addProgram(Program.BR);
        member.addEmail(MemberPopulateHelper.getRandomEmail());
        member.addPhone(MemberPopulateHelper.getRandomPhone());
        member.addAddress(MemberPopulateHelper.getUnitedStatesAddress());
        member.setPersonalInfo(MemberPopulateHelper.getSimplePersonalInfo());

        login("CEQHA");

        new EnrollmentPage().submitEnrollment(member, helper);
    }

    @Test
    public void acceptDataUsageAndEnrollInBr()
    {
        PrivacyConfirmationPopUp privacyConfirmationPopUp = new PrivacyConfirmationPopUp();
        verifyThat(privacyConfirmationPopUp, displayed(true));
        verifyThat(privacyConfirmationPopUp.getAgreementText(), hasText(PrivacyConfirmationPopUp.PRIVACY_AGREEMENT));
        privacyConfirmationPopUp.getAccepted().check();
        privacyConfirmationPopUp.getConfirm().clickAndWait(verifyNoError());

        SuccessEnrolmentPopUp popUp = new SuccessEnrolmentPopUp();
        verifyThat(popUp, isDisplayedWithWait());
        verifyThat(popUp.getMemberId(Program.RC), hasText(popUp.getMemberId(Program.BR).getText()));
        popUp.clickDone(verifyNoError());

        ProgramsGrid programs = new ProgramInfoPanel().getPrograms();
        programs.getRewardClubProgram().verify(RewardClubLevel.CLUB, "0");
        verifyThat(programs.getBusinessRewardsProgram().getStatus(), hasText(ProgramStatusReason.PENDING));
    }
}
