package com.ihg.crm.automation.selenium.tests.hotel.search;

import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyNoError;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static java.text.MessageFormat.format;

import java.util.Map;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.pages.login.TopUserSection;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.hotel.pages.LeftPanel;
import com.ihg.automation.selenium.hotel.pages.personal.PersonalInfoPage;
import com.ihg.automation.selenium.hotel.pages.search.GuestSearch;
import com.ihg.automation.selenium.hotel.pages.search.GuestSearchByNumber;
import com.ihg.automation.selenium.hotel.pages.search.SearchByStay;
import com.ihg.crm.automation.selenium.tests.hotel.LoginLogout;

public class SearchGuestByStayTest extends LoginLogout
{
    private final static String BY_UNIQUE_NBR = "SELECT HTL_CD, "//
            + "TO_CHAR(CK_OUT_DT, ''DDMonYY'') AS CK_OUT_DT, FOL_NBR, RM_NBR_TXT, CONF_NBR, FRST_NM, LST_NM, MBR.MBRSHP_ID " //
            + "FROM STY.STY_FOL FOL JOIN DGST.MBRSHP MBR ON FOL.DGST_MSTR_KEY = MBR.DGST_MSTR_KEY "//
            + "WHERE NOT EXISTS (SELECT 1 FROM STY.STY_FOL FOL2 WHERE FOL2.STY_FOL_KEY <> FOL.STY_FOL_KEY " //
            + "AND FOL2.{0} = FOL.{0} " //
            + "AND FOL2.HTL_CD = FOL.HTL_CD " //
            + "AND FOL.CK_OUT_DT BETWEEN FOL2.CK_IN_TS-1 AND FOL2.CK_OUT_DT)" //
            + "AND EXISTS (SELECT 1 FROM DGST.MBRSHP MBR "//
            + "WHERE MBR.DGST_MSTR_KEY = FOL.DGST_MSTR_KEY "//
            + "GROUP BY DGST_MSTR_KEY HAVING COUNT(LYTY_PGM_CD) = 1) "//
            + "AND MBR.DGST_MSTR_KEY = FOL.DGST_MSTR_KEY "//
            + "AND MBR.LYTY_PGM_CD =''PC'' AND MBR.MBRSHP_STAT_CD <>''C'' "//
            + "AND EVN_TYP IS NOT NULL "//
            + "AND FOL_NBR IS NOT NULL "//
            + "AND RM_NBR_TXT IS NOT NULL "//
            + "AND CONF_NBR IN (SELECT DISTINCT CONF_NBR FROM STY.STY_FOL WHERE CONF_NBR IS NOT NULL AND ROWNUM < 5) "//
            + "AND FRST_NM IS NOT NULL "//
            + "AND LST_NM IS NOT NULL " //
            + "AND ENTERPRISE_ID IN "//
            + "(SELECT M.ENTERPRISE_ID FROM DGST.GST_MSTR M WHERE M.ENTERPRISE_ID <> -1) " //
            + " AND FOL.LYTY_STAT_CD = ''POSTED'' AND FOL.EVN_TYP =''SYSTEM_STAY'' AND FOL.LYTY_SRC_IND = ''Y'' AND FOL.QUAL_NT > 0" //
            + "AND ROWNUM < 2";

    private GuestSearch guestSearch;
    private SearchByStay searchByStay;

    private String hotelCode;
    private String folioNumber;
    private String roomNumber;
    private String confirmationNumber;
    private String membershipId;
    private String firstName;
    private String lastName;
    private String stayDate;

    private void findStay(String column)
    {
        Map<String, Object> map = jdbcTemplate.queryForMap(format(BY_UNIQUE_NBR, column));
        hotelCode = map.get("HTL_CD").toString();
        folioNumber = map.get("FOL_NBR").toString();
        roomNumber = map.get("RM_NBR_TXT").toString();
        confirmationNumber = map.get("CONF_NBR").toString();
        membershipId = map.get("MBRSHP_ID").toString();
        firstName = map.get("FRST_NM").toString().trim();
        lastName = map.get("LST_NM").toString().trim();
        stayDate = map.get("CK_OUT_DT").toString().trim();
    }

    @BeforeClass
    public void before()
    {
        login();
    }

    @Test(priority = 10)
    public void searchByHotelCheckoutFolio()
    {
        findStay("FOL_NBR");

        goToSearchByStay();

        verifyThat(searchByStay.getHotel(), hasText(hotelCode));
        searchByStay.getStayDate().type(stayDate);
        searchByStay.getFolioNumber().type(folioNumber);
        guestSearch.clickSearch();

        verifySearchResults();
    }

    @Test(priority = 20)
    public void searchByHotelCheckoutRoom()
    {
        findStay("RM_NBR_TXT");

        goToSearchByStay();

        verifyThat(searchByStay.getHotel(), hasText(hotelCode));
        searchByStay.getStayDate().type(stayDate);
        searchByStay.getRoomNumber().type(roomNumber);
        guestSearch.clickSearch();

        verifySearchResults();
    }

    @Test(priority = 30)
    public void searchByHotelCheckoutConfirmation()
    {
        findStay("CONF_NBR");

        goToSearchByStay();

        verifyThat(searchByStay.getHotel(), hasText(hotelCode));
        searchByStay.getStayDate().type(stayDate);
        searchByStay.getConfirmationNumber().type(confirmationNumber);
        guestSearch.clickSearch();

        verifySearchResults();
    }

    private void goToSearchByStay()
    {
        if (new TopUserSection().switchHotel(hotelCode, false))
        {
            new GuestSearchByNumber().getAdvancedSearch().clickAndWait(verifyNoError());
            guestSearch = new GuestSearch();
            searchByStay = guestSearch.getSearchByStay();
            searchByStay.expand();
        }
    }

    private void verifySearchResults()
    {
        verifyThat(new PersonalInfoPage(), displayed(true));
        LeftPanel leftPanel = new LeftPanel();
        verifyThat(leftPanel.getCustomerInfoPanel().getMemberNumber(Program.RC), hasText(membershipId));
        leftPanel.clickNewSearch();
        guestSearch.clickClear();
    }
}
