package com.ihg.crm.automation.selenium.tests.hotel.enrollment;

import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyNoError;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.enabled;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasText;
import static com.ihg.automation.selenium.matchers.text.EntityMatcherFactory.isValue;
import static org.hamcrest.core.AllOf.allOf;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.contact.Address;
import com.ihg.automation.selenium.common.enroll.CustomerInfo;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.pages.login.TopUserSection;
import com.ihg.automation.selenium.common.types.WrittenLanguage;
import com.ihg.automation.selenium.common.types.address.AddressType;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.gwt.components.Mode;
import com.ihg.automation.selenium.hotel.pages.enroll.EnrollmentPage;
import com.ihg.crm.automation.selenium.tests.hotel.LoginLogout;

public class VerifyPopulatedFieldsOnEnrollmentTest extends LoginLogout
{
    private Member member = new Member();
    private EnrollmentPage enrollmentPage = new EnrollmentPage();

    @BeforeClass
    public void beforeClass()
    {
        member.addProgram(Program.RC);
        member.addEmail(MemberPopulateHelper.getRandomEmail());
        member.addPhone(MemberPopulateHelper.getRandomPhone());
        member.addAddress(MemberPopulateHelper.getUnitedStatesAddress());
        member.setPersonalInfo(MemberPopulateHelper.getSimplePersonalInfo());

        login();
        enrollmentPage.enroll(member, helper);
    }

    @Test
    public void verifyPopulatedCustomerInfo()
    {
        enrollmentPage.goTo();
        enrollmentPage.getPrograms().selectProgram(Program.EMP);

        CustomerInfo customerInfo = enrollmentPage.getCustomerInfo();
        verifyThat(customerInfo.getResidenceCountry(), hasText(isValue(member.getPreferredAddress().getCountry())));
        verifyThat(customerInfo.getWrittenLanguage(), hasText(isValue(WrittenLanguage.ENGLISH)));
        verifyThat(customerInfo.getName().getFullName(), hasText(member.getName().getFullName()));
    }

    @Test(dependsOnMethods = { "verifyPopulatedCustomerInfo" }, alwaysRun = true)
    public void verifyPopulatedAddress()
    {
        Address address = enrollmentPage.getAddress();
        verifyThat(address.getCountry(), hasText(isValue(member.getPreferredAddress().getCountry())));
        verifyThat(address.getType(), hasText(isValue(AddressType.RESIDENCE)));
        address.verify(member.getPreferredAddress(), Mode.EDIT);
    }

    @Test(dependsOnMethods = { "verifyPopulatedAddress" }, alwaysRun = true)
    public void verifyPopulatedPhone()
    {
        enrollmentPage.getPhone().verify(member.getPreferredPhone(), Mode.EDIT);
    }

    @Test(dependsOnMethods = { "verifyPopulatedPhone" }, alwaysRun = true)
    public void verifyPopulatedEmail()
    {
        enrollmentPage.getEmail().verifyLcEmail(member.getPreferredEmail(), Mode.VIEW);
    }

    @Test(dependsOnMethods = { "verifyPopulatedEmail" }, alwaysRun = true)
    public void verifyHotelAndEmployee()
    {
        TopUserSection section = new TopUserSection();
        verifyThat(enrollmentPage.getHotelCode(), allOf(hasText(section.getHotel().getText()), enabled(false)));
        verifyThat(enrollmentPage.getYourEmployeeID(), displayed(false));
        enrollmentPage.clickCancel(verifyNoError());
    }
}
