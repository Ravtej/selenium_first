package com.ihg.crm.automation.selenium.tests.hotel.enrollment.br;

import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyNoError;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.isDisplayedWithWait;
import static com.ihg.automation.selenium.matchers.component.HasDefaultText.hasDefault;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.BusinessRewardsGridRow;
import com.ihg.automation.selenium.common.Constant;
import com.ihg.automation.selenium.common.ProgramInfoPanel;
import com.ihg.automation.selenium.common.ProgramsGrid;
import com.ihg.automation.selenium.common.business.BusinessStatus;
import com.ihg.automation.selenium.common.contact.Address;
import com.ihg.automation.selenium.common.enroll.SuccessEnrolmentPopUp;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.common.types.program.ProgramStatus;
import com.ihg.automation.selenium.common.types.program.ProgramStatusReason;
import com.ihg.automation.selenium.common.types.program.RewardClubLevel;
import com.ihg.automation.selenium.gwt.components.Mode;
import com.ihg.automation.selenium.hotel.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.hotel.pages.personal.AddressContact;
import com.ihg.automation.selenium.hotel.pages.personal.PersonalInfoPage;
import com.ihg.automation.selenium.hotel.pages.programs.ProgramPageBase;
import com.ihg.automation.selenium.hotel.pages.programs.br.BusinessRewardsPage;
import com.ihg.automation.selenium.hotel.pages.programs.br.BusinessRewardsSummary;
import com.ihg.automation.selenium.matchers.component.HasBackgroundColor;
import com.ihg.automation.selenium.servicecenter.pages.LeftPanel;
import com.ihg.crm.automation.selenium.tests.hotel.LoginLogout;

public class EnrollmentToBRExistingRCMemberTest extends LoginLogout
{
    private Member member = new Member();

    @BeforeClass
    public void beforeClass()
    {
        member.addProgram(Program.RC);
        member.addEmail(MemberPopulateHelper.getRandomEmail());
        member.addPhone(MemberPopulateHelper.getRandomPhone());
        member.addAddress(MemberPopulateHelper.getUnitedStatesAddress());
        member.setPersonalInfo(MemberPopulateHelper.getSimplePersonalInfo());

        login();
        member = new EnrollmentPage().enroll(member, helper);
    }

    @Test
    public void enrollToBrProgram()
    {
        EnrollmentPage enrollmentPage = new EnrollmentPage();
        enrollmentPage.goTo();
        enrollmentPage.getPrograms().selectProgram(Program.BR);
        enrollmentPage.getYourEmployeeID().type(helper.getUser().getEmployeeId());

        enrollmentPage.clickSubmitAndSkipWarnings(verifyNoError());

        SuccessEnrolmentPopUp popUp = new SuccessEnrolmentPopUp();
        verifyThat(popUp, isDisplayedWithWait());
        verifyThat(popUp.getMemberId(Program.BR), hasText(member.getRCProgramId()));
        popUp.clickDone(verifyNoError());
    }

    @Test(dependsOnMethods = { "enrollToBrProgram" })
    public void verifyPersonalInformationTab()
    {
        PersonalInfoPage personalInfoPage = new PersonalInfoPage();
        verifyThat(personalInfoPage, isDisplayedWithWait());
        personalInfoPage.getCustomerInfo().verify(member, Mode.VIEW);

        AddressContact addressContact = personalInfoPage.getAddressList().getExpandedContact();
        Address address = addressContact.getBaseContact();
        address.verify(member.getPreferredAddress(), Mode.VIEW);
    }

    @Test(dependsOnMethods = { "verifyPersonalInformationTab" }, alwaysRun = true)
    public void verifyProgramSummary()
    {
        BusinessRewardsPage businessRewardsPage = new BusinessRewardsPage();
        businessRewardsPage.goTo();

        BusinessRewardsSummary businessRewardsSummary = new BusinessRewardsPage().getSummary();
        businessRewardsSummary.verifyProgramDetailsAfterEnroll(ProgramStatus.OPEN, member, BusinessStatus.PENDING);
    }

    @Test(dependsOnMethods = { "verifyProgramSummary" }, alwaysRun = true)
    public void validateEnrollmentDetails()
    {
        BusinessRewardsPage businessRewardsPage = new BusinessRewardsPage();

        ProgramPageBase.EnrollmentDetails enrollmentDetails = businessRewardsPage.getEnrollmentDetails();
        enrollmentDetails.verifyDetails(helper.getSourceFactory().getSource(), Constant.NOT_AVAILABLE);
        verifyThat(enrollmentDetails.getReferringMember(), hasDefault());
    }

    @Test(dependsOnMethods = { "validateEnrollmentDetails" }, alwaysRun = true)
    public void verifyProgramInformationPanel()
    {
        ProgramsGrid programs = new ProgramInfoPanel().getPrograms();
        programs.getRewardClubProgram().verify(RewardClubLevel.CLUB, "0");

        BusinessRewardsGridRow rowBR = programs.getBusinessRewardsProgram();
        verifyThat(rowBR.getStatus(), hasText(ProgramStatusReason.PENDING));
        verifyThat(rowBR, HasBackgroundColor.hasBackgroundColor(LeftPanel.YELLOW_COLOR));
    }
}
