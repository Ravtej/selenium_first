package com.ihg.crm.automation.selenium.tests.hotel.enrollment.ambassador;

import static com.ihg.automation.common.DateUtils.DATE_FORMAT;
import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyError;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.member.AmbassadorUtils.PURCHASE_AMBASSADOR_HOTEL_$200;
import static com.ihg.automation.selenium.common.member.MemberPopulateHelper.getRandomEmail;
import static com.ihg.automation.selenium.common.member.MemberPopulateHelper.getRandomPhone;
import static com.ihg.automation.selenium.common.member.MemberPopulateHelper.getSimplePersonalInfo;
import static com.ihg.automation.selenium.common.member.MemberPopulateHelper.getUnitedStatesAddress;
import static com.ihg.automation.selenium.common.types.program.AmbassadorLevel.AMB;
import static com.ihg.automation.selenium.common.types.program.ProgramStatus.OPEN;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasDefault;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasText;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.isDisplayedAndEnabled;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.isHighlightedAsInvalid;

import java.util.Map;

import org.springframework.beans.factory.annotation.Value;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.common.DateUtils;
import com.ihg.automation.selenium.common.member.AmbassadorJdbcUtils;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.hotel.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.hotel.pages.programs.amb.AmbassadorPage;
import com.ihg.automation.selenium.hotel.pages.programs.amb.AmbassadorSummary;
import com.ihg.automation.selenium.hotel.pages.programs.amb.RenewAmbMembershipPopUp;
import com.ihg.automation.selenium.hotel.pages.programs.amb.RenewEnrollmentSuccessfulPopUp;
import com.ihg.crm.automation.selenium.tests.hotel.LoginLogout;

public class RenewAmbassadorTest extends LoginLogout
{
    private AmbassadorPage ambassadorPage = new AmbassadorPage();
    private EnrollmentPage enrollmentPage = new EnrollmentPage();
    private Member member = new Member();

    private static final String HOTEL = "CEQHA";
    private int daysShift;

    @Value("${lastUpdateUserId}")
    private String lastUpdateUserId;

    @BeforeClass
    public void beforeClass()
    {
        member.addProgram(Program.AMB);
        member.setAmbassadorAmount(PURCHASE_AMBASSADOR_HOTEL_$200);
        member.setPersonalInfo(getSimplePersonalInfo());
        member.addAddress(getUnitedStatesAddress());
        member.addEmail(getRandomEmail());
        member.addPhone(getRandomPhone());

        login(HOTEL);

        enrollmentPage.enroll(member, helper, true);

        daysShift = new AmbassadorJdbcUtils(jdbcTemplateUpdate, lastUpdateUserId).updateForRenew(member);
    }

    @Test(priority = 10)
    public void verifyAmbProgramInfoFields()
    {
        ambassadorPage.goTo();
        AmbassadorSummary summary = ambassadorPage.getSummary();
        summary.verify(DateUtils.getFormattedDate(daysShift, DATE_FORMAT), OPEN, AMB, member.getRCProgramId());
        verifyThat(summary.getRenew(), isDisplayedAndEnabled(true));
        verifyThat(ambassadorPage.getEnrollmentDetails().getRenewalDate(), hasDefault());
    }

    @Test(priority = 20)
    public void cancelAmbRenewal()
    {
        AmbassadorSummary summary = ambassadorPage.getSummary();
        summary.getRenew().clickAndWait();
        new RenewAmbMembershipPopUp().clickClose();
        summary.verify(DateUtils.getFormattedDate(daysShift, DATE_FORMAT), OPEN, AMB, member.getRCProgramId());
        verifyThat(ambassadorPage.getEnrollmentDetails().getRenewalDate(), hasDefault());
    }

    @Test(priority = 30)
    public void tryToRenewWithoutEmployeeId()
    {
        ambassadorPage.getSummary().getRenew().clickAndWait();

        RenewAmbMembershipPopUp renewAmbMembershipPopUp = new RenewAmbMembershipPopUp();
        renewAmbMembershipPopUp.clickSubmit();
        verifyThat(renewAmbMembershipPopUp.getEmployeeId(), isHighlightedAsInvalid(true));
        verifyError("Employee ID is required");
    }

    @Test(priority = 35)
    public void tryToRenewWithNotExistingEmpId()
    {
        RenewAmbMembershipPopUp renewAmbMembershipPopUp = new RenewAmbMembershipPopUp();
        renewAmbMembershipPopUp.getEmployeeId().type("712313123");
        renewAmbMembershipPopUp.clickSubmit(verifyError("Employee ID does not exist"));
    }

    @Test(priority = 40)
    public void renewAmbassador()
    {
        RenewAmbMembershipPopUp renewAmbMembershipPopUp = new RenewAmbMembershipPopUp();
        renewAmbMembershipPopUp.getEmployeeId().typeAndWait(getEmployeeId());
        verifyThat(renewAmbMembershipPopUp.getEmployeeName(), hasText(getEmpName()));
        renewAmbMembershipPopUp.clickSubmit();

        RenewEnrollmentSuccessfulPopUp renewEnrollmentSuccessfulPopUp = new RenewEnrollmentSuccessfulPopUp();
        verifyThat(renewEnrollmentSuccessfulPopUp,
                hasText("Please remember to collect payment from guest. Cost:\n150.00 USD"));
        renewEnrollmentSuccessfulPopUp.clickOk();

        ambassadorPage.getSummary().verifySummaryAfterRenew(daysShift, OPEN, AMB, member.getRCProgramId());
        verifyThat(ambassadorPage.getEnrollmentDetails().getRenewalDate(), hasText(DateUtils.getFormattedDate()));
    }

    private String getEmployeeId()
    {
        return helper.getUser().getEmployeeId();
    }

    private String getEmpName()
    {
        final String SQL = "SELECT nm.GIVEN_NM, nm.SURNAME_NM " //
                + "FROM DGST.GST_NM nm " //
                + "JOIN DGST.MBRSHP m USING(DGST_MSTR_KEY) " //
                + "WHERE m.MBRSHP_ID = '%1$s' AND m.LYTY_PGM_CD = 'EMP'";

        Map<String, Object> map = jdbcTemplate.queryForMap(String.format(SQL, getEmployeeId()));
        return map.get("GIVEN_NM").toString() + " " + map.get("SURNAME_NM").toString();
    }
}
