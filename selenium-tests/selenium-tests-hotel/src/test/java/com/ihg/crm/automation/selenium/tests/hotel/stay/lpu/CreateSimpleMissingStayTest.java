package com.ihg.crm.automation.selenium.tests.hotel.stay.lpu;

import static com.ihg.automation.common.RandomUtils.getRandomLetters;
import static com.ihg.automation.common.RandomUtils.getRandomNumber;
import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyNoError;
import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifySuccess;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.assertThat;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.hotel.pages.hoteloperations.stay.lpu.missingstay.StayCreatePopUp.STAY_SUCCESS_CREATED;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasText;
import static com.ihg.automation.selenium.matchers.component.GridSize.size;
import static com.ihg.automation.selenium.matchers.component.IsDisplayedAndEnabled.isDisplayedAndEnabledWithWait;
import static org.hamcrest.Matchers.greaterThan;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.event.SearchButtonBar;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.pages.login.TopUserSection;
import com.ihg.automation.selenium.common.stay.Revenues;
import com.ihg.automation.selenium.common.stay.Stay;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.gwt.components.input.Input;
import com.ihg.automation.selenium.hotel.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.stay.ResultsFilter;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.stay.lpu.LoyaltyPendingUpdatesGrid;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.stay.lpu.LoyaltyPendingUpdatesPage;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.stay.lpu.LpuGridRowContainer;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.stay.lpu.missingstay.GuestDetailsEdit;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.stay.lpu.missingstay.StayCreatePopUp;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.stay.lpu.missingstay.StayDetailsEdit;
import com.ihg.crm.automation.selenium.tests.hotel.LoginLogout;
import com.ihg.crm.automation.selenium.tests.hotel.stay.HotelDBUtils;

public class CreateSimpleMissingStayTest extends LoginLogout
{
    private Member member = new Member();
    private Stay stay = new Stay();

    @BeforeClass
    public void beforeClass()
    {
        member.addProgram(Program.RC);
        member.addEmail(MemberPopulateHelper.getRandomEmail());
        member.addPhone(MemberPopulateHelper.getRandomPhone());
        member.addAddress(MemberPopulateHelper.getUnitedStatesAddress());
        member.setPersonalInfo(MemberPopulateHelper.getSimplePersonalInfo());

        new HotelDBUtils(jdbcTemplate).getGnrHotel(stay);
        stay.setRateCode("SRCH" + getRandomNumber(1));
        stay.setConfirmationNumber(1 + getRandomNumber(7));
        stay.setIataCode("9958309");
        stay.setFolioNumber(2 + getRandomNumber(5));
        stay.setRoomNumber(3 + getRandomNumber(3));
        stay.setCorporateAccountNumber("100230372");
        stay.setNights(5);
        stay.setCheckInDateByCheckout();
        stay.setAvgRoomRate(100.00);
        stay.setIsQualifying(true);

        Revenues revenues = stay.getRevenues();
        revenues.getOverallTotal().setAmount(500.00);
        revenues.getRoom().setAmount(500.00);
        revenues.getTotalQualifying().setAmount(500.00);

        login();

        member = new EnrollmentPage().enroll(member, helper, verifyNoError());

        new TopUserSection().switchHotel(stay.getHotelCode());
        LoyaltyPendingUpdatesPage loyaltyPendingUpdatesPage = new LoyaltyPendingUpdatesPage();
        loyaltyPendingUpdatesPage.goToByHotelOperations();
        assertThat(loyaltyPendingUpdatesPage.getCreateMissingStay(), isDisplayedAndEnabledWithWait(true));
    }

    @Test
    public void verifyRcMemberOnStay()
    {
        new LoyaltyPendingUpdatesPage().clickCreateMissingStay(verifyNoError());
        GuestDetailsEdit guestDetails = new StayCreatePopUp().getGuestDetails();
        guestDetails.addMemberId(member);

        verifyThat(guestDetails.getMemberName(), hasText(member.getName().getFullName()));
        verifyThat(guestDetails.getProgram(), hasText(Program.RC.getCode()));
    }

    @Test(dependsOnMethods = { "verifyRcMemberOnStay" })
    public void createMissingStay()
    {
        StayCreatePopUp stayDetailsCreate = new StayCreatePopUp();

        StayDetailsEdit stayDetails = stayDetailsCreate.getStayDetails();
        verifyThat(stayDetails.getHotel(), hasText(stay.getHotelCode()));

        stayDetails.populate(stay);
        stayDetailsCreate.getRevenueDetails().populate(stay);
        stayDetailsCreate.createStay(verifyNoError(), verifySuccess(STAY_SUCCESS_CREATED));
    }

    @Test(dependsOnMethods = { "createMissingStay" })
    public void verifyCreatedStay()
    {
        new LoyaltyPendingUpdatesPage().getLoyaltyPendingUpdatesGrid().getRow(1).verify(member, stay);
    }

    @Test(dependsOnMethods = { "verifyCreatedStay" }, alwaysRun = true)
    public void verifyStayDetailsInView()
    {
        LpuGridRowContainer container = new LoyaltyPendingUpdatesPage().getLoyaltyPendingUpdatesGrid().getRow(1)
                .expand(LpuGridRowContainer.class);
        container.verify(stay, member);
        verifyThat(container.getAdjustRevenueDetails(), displayed(true));
    }

    @DataProvider(name = "validSearchCriteria")
    private Object[][] validSearchCriteria()
    {
        ResultsFilter resultsFilter = new LoyaltyPendingUpdatesPage().getResultsFilter();

        return new Object[][] { { resultsFilter.getGuestName(), member.getName().getFullName() },
                { resultsFilter.getConfirmationNumber(), stay.getConfirmationNumber() },
                { resultsFilter.getRateCategory(), stay.getRateCode() },
                { resultsFilter.getFolioNumber(), stay.getFolioNumber() },
                { resultsFilter.getIataNumber(), stay.getIataCode() },
                { resultsFilter.getCorporateId(), stay.getCorporateAccountNumber() },
                { resultsFilter.getRoomNumber(), stay.getRoomNumber() } };
    }

    @DataProvider(name = "invalidSearchCriteria")
    private Object[][] invalidSearchCriteria()
    {
        ResultsFilter resultsFilter = new LoyaltyPendingUpdatesPage().getResultsFilter();

        return new Object[][] { { resultsFilter.getGuestName(), getRandomLetters(15) },
                { resultsFilter.getConfirmationNumber(), getRandomNumber(8) },
                { resultsFilter.getRateCategory(), getRandomLetters(5) },
                { resultsFilter.getFolioNumber(), getRandomNumber(8) },
                { resultsFilter.getIataNumber(), getRandomNumber(7) },
                { resultsFilter.getCorporateId(), getRandomNumber(9) },
                { resultsFilter.getRoomNumber(), getRandomNumber(3) } };
    }

    @Test(dataProvider = "validSearchCriteria", dependsOnMethods = { "verifyStayDetailsInView" })
    public void searchByValidCriteria(Input input, String text)
    {
        searchStay(input, text);
        LoyaltyPendingUpdatesGrid loyaltyPendingUpdatesGrid = new LoyaltyPendingUpdatesPage()
                .getLoyaltyPendingUpdatesGrid();
        verifyThat(loyaltyPendingUpdatesGrid, size(greaterThan(0)));
        loyaltyPendingUpdatesGrid.getRow(1).verify(member, stay);
    }

    @Test(dataProvider = "invalidSearchCriteria", dependsOnMethods = { "searchByValidCriteria" })
    public void searchByInvalidCriteria(Input input, String text)
    {
        searchStay(input, text);
        verifyThat(new LoyaltyPendingUpdatesPage().getLoyaltyPendingUpdatesGrid(), size(0));
    }

    private void searchStay(Input input, String text)
    {
        LoyaltyPendingUpdatesPage loyaltyPendingUpdatesPage = new LoyaltyPendingUpdatesPage();
        ResultsFilter resultsFilter = loyaltyPendingUpdatesPage.getResultsFilter();
        SearchButtonBar buttonBar = loyaltyPendingUpdatesPage.getButtonBar();
        buttonBar.clickClearCriteria();
        resultsFilter.expand();
        resultsFilter.getMemberNumber().type(member.getRCProgramId());
        input.type(text);

        buttonBar.clickSearch();
    }
}
