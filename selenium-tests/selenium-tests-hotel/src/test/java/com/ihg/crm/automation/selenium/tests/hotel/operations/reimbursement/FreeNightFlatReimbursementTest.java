package com.ihg.crm.automation.selenium.tests.hotel.operations.reimbursement;

import static com.ihg.automation.common.DateUtils.DATE_FORMAT;
import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyError;
import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyNoError;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.hotel.pages.hoteloperations.reimbursement.freenight.FreeNightFlatReimbursementGridRow.FreeNightFlatReimbursementGridCell.STATUS;
import static com.ihg.automation.selenium.hotel.pages.hoteloperations.reimbursement.freenight.FreeNightFlatReimbursementGridRow.FreeNightFlatReimbursementGridCell.STAY_DATE;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasTextAsDate;
import static com.ihg.automation.selenium.matchers.component.GridSize.size;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static com.ihg.automation.selenium.matchers.component.HasTipText.hasWarningTipTextWithWait;
import static com.ihg.automation.selenium.matchers.date.IsAfterOrEqualsDate.isAfterOrEquals;
import static com.ihg.automation.selenium.matchers.date.IsBeforeOrEqualsDate.isBeforeOrEquals;
import static com.ihg.automation.selenium.servicecenter.pages.Messages.HIGHLIGHTED_FIELDS_DID_NOT_PASS_VALIDATION;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.core.Is.is;
import static org.joda.time.LocalDate.now;

import java.util.List;

import org.joda.time.LocalDate;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.gwt.components.input.DateInput;
import com.ihg.automation.selenium.hotel.pages.LeftPanel;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.HotelOperationsPage;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.reimbursement.freenight.FreeNightFlatReimbursementGrid;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.reimbursement.freenight.FreeNightFlatReimbursementGridRow;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.reimbursement.freenight.FreeNightFlatReimbursementPage;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.reimbursement.freenight.FreeNightFlatReimbursementSearch;
import com.ihg.crm.automation.selenium.tests.hotel.LoginLogout;

public class FreeNightFlatReimbursementTest extends LoginLogout
{
    private FreeNightFlatReimbursementPage freeNightFlatReimbursementPage;
    private FreeNightFlatReimbursementSearch searchFields;
    private static final LocalDate VALID_FROM_DATE = now().minusYears(3);
    private static final LocalDate VALID_TO_DATE = now();

    @BeforeClass
    public void before()
    {
        login();

        new LeftPanel().getHotelOperations().clickAndWait(verifyNoError());
        new HotelOperationsPage().getHotelsFlatReimbursement().clickAndWait(verifyNoError());
    }

    @Test(priority = 1)
    public void verifyPageComponents()
    {
        freeNightFlatReimbursementPage = new FreeNightFlatReimbursementPage();

        searchFields = freeNightFlatReimbursementPage.getSearchFields();
        verifyThat(searchFields.getHotelCode(), hasText(helper.getUser().getLocation()));
        verifyThat(searchFields.getStatus(), hasText("All Statuses"));
        verifyThat(searchFields.getFromDate(), hasText(now().minusDays(31).toString(DATE_FORMAT)));
        verifyThat(searchFields.getToDate(), hasText(now().minusDays(1).toString(DATE_FORMAT)));
    }

    @Test(priority = 5, dataProvider = "statusesProvider")
    public void verifySearchByStatus(String status)
    {
        searchFields.getStatus().select(status);
        searchFields.getFromDate().type(now().minusYears(2).toString(DATE_FORMAT));
        freeNightFlatReimbursementPage.getButtonBar().clickSearch();

        FreeNightFlatReimbursementGrid freeNightReimbursementSearchGrid = freeNightFlatReimbursementPage
                .getFreeNightFlatReimbursementGrid();
        verifyThat(freeNightReimbursementSearchGrid, size(is(greaterThan(0))));

        List<FreeNightFlatReimbursementGridRow> gridRowList = freeNightReimbursementSearchGrid.getRowList();
        for (FreeNightFlatReimbursementGridRow row : gridRowList)
        {
            verifyThat(row.getCell(STATUS), hasText(status));
        }
    }

    @Test(priority = 10)
    public void verifyDatesRangeInvalid()
    {
        DateInput fromDate = searchFields.getFromDate();
        fromDate.type(now().plusDays(15).toString(DATE_FORMAT));

        searchFields.getToDate().type(now().minusDays(14).toString(DATE_FORMAT));

        freeNightFlatReimbursementPage.getButtonBar().clickSearch();

        verifyError(HIGHLIGHTED_FIELDS_DID_NOT_PASS_VALIDATION);
        verifyThat(fromDate, hasWarningTipTextWithWait("Start date should be before end date"));

    }

    @Test(priority = 15)
    public void verifyDatesRangeEmpty()
    {
        DateInput toDate = searchFields.getToDate();
        toDate.clear();

        freeNightFlatReimbursementPage.getButtonBar().clickSearch();

        verifyError(HIGHLIGHTED_FIELDS_DID_NOT_PASS_VALIDATION);
        verifyThat(toDate, hasWarningTipTextWithWait("This field is required"));
    }

    @Test(priority = 20)
    public void verifyDatesValidRange()
    {
        searchFields.getFromDate().type(VALID_FROM_DATE.toString(DATE_FORMAT));
        searchFields.getToDate().type(VALID_TO_DATE.toString(DATE_FORMAT));
        freeNightFlatReimbursementPage.getButtonBar().clickSearch();

        List<FreeNightFlatReimbursementGridRow> gridRowList = freeNightFlatReimbursementPage
                .getFreeNightFlatReimbursementGrid().getRowList();

        for (FreeNightFlatReimbursementGridRow row : gridRowList)
        {
            verifyThat(row.getCell(STAY_DATE), hasTextAsDate("ddMMMYY", isAfterOrEquals(VALID_FROM_DATE)));
            verifyThat(row.getCell(STAY_DATE), hasTextAsDate("ddMMMYY", isBeforeOrEquals(VALID_TO_DATE)));
        }
    }

    @DataProvider(name = "statusesProvider")
    public Object[][] statusesDataProvider()
    {
        return new Object[][] { { "ACCEPTED" }, { "PAID" } };
    }
}
