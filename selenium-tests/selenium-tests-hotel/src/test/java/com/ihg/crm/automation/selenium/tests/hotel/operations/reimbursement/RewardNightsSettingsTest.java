package com.ihg.crm.automation.selenium.tests.hotel.operations.reimbursement;

import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyNoError;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.hotel.pages.hoteloperations.reimbursement.rewardnight.LowOccupancyReimbursementAmountGridRow.LowOccupancyReimbursementAmountGridCell.CHECK_OUT_BEGINNING_DATE;
import static com.ihg.automation.selenium.hotel.pages.hoteloperations.reimbursement.rewardnight.LowOccupancyReimbursementAmountGridRow.LowOccupancyReimbursementAmountGridCell.CURRENCY;
import static com.ihg.automation.selenium.hotel.pages.hoteloperations.reimbursement.rewardnight.LowOccupancyReimbursementAmountGridRow.LowOccupancyReimbursementAmountGridCell.LOVIR_ADDITIONAL_REIMBURSEMENT_AMOUNT;
import static com.ihg.automation.selenium.hotel.pages.hoteloperations.reimbursement.rewardnight.LowOccupancyReimbursementAmountGridRow.LowOccupancyReimbursementAmountGridCell.LOW_OCC_REIMBURSEMENT_AMOUNT;
import static com.ihg.automation.selenium.hotel.pages.hoteloperations.reimbursement.rewardnight.RewardAndFreeNightsCountGridRow.RewardAndFreeNightsCountGridCell.LOVIR_THRESHOLD;
import static com.ihg.automation.selenium.hotel.pages.hoteloperations.reimbursement.rewardnight.RewardAndFreeNightsCountGridRow.RewardAndFreeNightsCountGridCell.PROCESSED_AND_PAID_HIGH_OCC;
import static com.ihg.automation.selenium.hotel.pages.hoteloperations.reimbursement.rewardnight.RewardAndFreeNightsCountGridRow.RewardAndFreeNightsCountGridCell.PROCESSED_AND_PAID_LOW_OCC;
import static com.ihg.automation.selenium.hotel.pages.hoteloperations.reimbursement.rewardnight.RewardAndFreeNightsCountGridRow.RewardAndFreeNightsCountGridCell.TOTAL_REIMB_AMOUNT;
import static com.ihg.automation.selenium.hotel.pages.hoteloperations.reimbursement.rewardnight.RewardAndFreeNightsCountGridRow.RewardAndFreeNightsCountGridCell.UNPROCESSED_BUT_PAID;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.automation.selenium.matchers.component.GridSize.size;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static org.apache.commons.lang3.StringUtils.EMPTY;
import static org.hamcrest.Matchers.not;

import java.util.Map;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.hotel.pages.LeftPanel;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.HotelOperationsPage;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.reimbursement.rewardnight.LowOccupancyReimbursementAmountGrid;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.reimbursement.rewardnight.LowOccupancyReimbursementAmountGridRow;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.reimbursement.rewardnight.RewardAndFreeNightsCountGridRow;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.reimbursement.rewardnight.RewardAndFreeNightsCountGridRow.RewardAndFreeNightsCountGridCell;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.reimbursement.rewardnight.RewardNightsSettingsPage;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.reimbursement.rewardnight.TaxesAndFeesGrid;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.reimbursement.rewardnight.TaxesAndFeesGridRow;
import com.ihg.crm.automation.selenium.tests.hotel.LoginLogout;

public class RewardNightsSettingsTest extends LoginLogout
{
    private static final String LOW_OCC_REIMB_AMOUNT_SQL = "SELECT * FROM ( " //
            + "SELECT TO_CHAR(EFF_STY_DT, 'DDMonYY') EFF_DT, " //
            + "CURR_CD, TO_CHAR(LOW_OCC_REIMB_AMT, 'FM99.00') LOW_OCC_REIMB_AMT, " //
            + "TO_CHAR(NVL(BOUNTY_AMT, 0), 'FM0.00') LOVIR_AMOUNT " //
            + "FROM CATLG.RN_REIMB_HTL_EXCP_RULE " //
            + "WHERE HTL_ID IN (SELECT HTL_ID FROM GROWTH.HOTEL WHERE HLDX_CD='%s') " //
            + "ORDER BY EFF_STY_DT DESC) " //
            + "WHERE ROWNUM < 2";

    private static final String TAXES_AND_FEES_SQL = "SELECT * FROM( " //
            + "SELECT TO_CHAR(CHECK_OUT_DATE, 'DDMonYY') EFF_DT, " //
            + "TO_CHAR( TAX_PCT, 'FM99.00')||' %%' TAX_PCT, TO_CHAR(FLAT_FEE, 'FM99.00') FLAT_FEE, FLAT_FEE_CURR_CD " //
            + "FROM FMDS.TMRMTAX WHERE FACILITY_ID IN (SELECT FAC_NBR FROM GROWTH.HOTEL WHERE HLDX_CD='%s') " //
            + "ORDER BY CHECK_OUT_DATE DESC) " //
            + "WHERE ROWNUM < 2";

    private static final String LOVIR_SQL = "SELECT TO_CHAR(LOVIR_THRES_QTY, 'FM99,000') LOVIR_THRES_QTY " //
            + "FROM LYTY.HTL_YR_VAL " //
            + "WHERE HTL_ID IN (SELECT HTL_ID FROM GROWTH.HOTEL WHERE HLDX_CD='%s') " //
            + "AND YR_NBR = TO_CHAR(SYSDATE, 'YYYY')";

    private static final int EXPECTED_GRID_SIZE_PER_PAGE = 5;

    private RewardNightsSettingsPage rewardNightsSettingsPage = new RewardNightsSettingsPage();
    private Map<String, Object> lowOccReimbAmountMap, taxesAndFeesMap, lovirMap;

    @BeforeClass
    public void before()
    {
        login();

        String hotelCode = helper.getSourceFactory().getSource().getLocation();

        lowOccReimbAmountMap = jdbcTemplate.queryForMap(String.format(LOW_OCC_REIMB_AMOUNT_SQL, hotelCode));
        taxesAndFeesMap = jdbcTemplate.queryForMap(String.format(TAXES_AND_FEES_SQL, hotelCode));
        lovirMap = jdbcTemplate.queryForMap(String.format(LOVIR_SQL, hotelCode));

        new LeftPanel().getHotelOperations().clickAndWait(verifyNoError());
        new HotelOperationsPage().getHotelsRewardNightsSettings().clickAndWait(verifyNoError());
    }

    @Test()
    public void verifyLowOccupancyReimbursementAmount()
    {
        verifyThat(rewardNightsSettingsPage.getHotelCode(),
                hasText(helper.getSourceFactory().getSource().getLocation()));

        LowOccupancyReimbursementAmountGrid lowOccupancyReimbursementAmountGrid = rewardNightsSettingsPage
                .getLowOccupancyReimbursementAmount().getLowOccupancyReimbursementAmountGrid();

        verifyThat(lowOccupancyReimbursementAmountGrid, size(EXPECTED_GRID_SIZE_PER_PAGE));

        LowOccupancyReimbursementAmountGridRow gridRow = lowOccupancyReimbursementAmountGrid.getRow(1);
        verifyThat(gridRow.getCell(CHECK_OUT_BEGINNING_DATE), hasText(lowOccReimbAmountMap.get("EFF_DT").toString()));
        verifyThat(gridRow.getCell(LOW_OCC_REIMBURSEMENT_AMOUNT),
                hasText(lowOccReimbAmountMap.get("LOW_OCC_REIMB_AMT").toString()));
        verifyThat(gridRow.getCell(LOVIR_ADDITIONAL_REIMBURSEMENT_AMOUNT),
                hasText(lowOccReimbAmountMap.get("LOVIR_AMOUNT").toString()));
        verifyThat(gridRow.getCell(CURRENCY), hasText(lowOccReimbAmountMap.get("CURR_CD").toString()));
    }

    @Test()
    public void verifyExceptions()
    {
        verifyThat(rewardNightsSettingsPage.getLowOccupancyReimbursementAmount().getExceptionsGrid(), displayed(true));
    }

    @Test()
    public void verifyTaxesAndFees()
    {
        TaxesAndFeesGrid taxesAndFeesGrid = rewardNightsSettingsPage.getTaxesAndFees().getTaxesAndFeesGrid();
        verifyThat(taxesAndFeesGrid, size(EXPECTED_GRID_SIZE_PER_PAGE));

        TaxesAndFeesGridRow gridRow = taxesAndFeesGrid.getRow(1);
        verifyThat(gridRow.getCell(TaxesAndFeesGridRow.TaxesAndFeesGridCell.CHECK_OUT_BEGINNING_DATE),
                hasText(taxesAndFeesMap.get("EFF_DT").toString()));
        verifyThat(gridRow.getCell(TaxesAndFeesGridRow.TaxesAndFeesGridCell.PERCENT),
                hasText(taxesAndFeesMap.get("TAX_PCT").toString()));
        verifyThat(gridRow.getCell(TaxesAndFeesGridRow.TaxesAndFeesGridCell.FEE),
                hasText(taxesAndFeesMap.get("FLAT_FEE").toString()));
        verifyThat(gridRow.getCell(TaxesAndFeesGridRow.TaxesAndFeesGridCell.CURRENCY),
                hasText(taxesAndFeesMap.get("FLAT_FEE_CURR_CD").toString()));
    }

    @Test
    public void verifyLovir()
    {

        RewardAndFreeNightsCountGridRow freeNightsCountGridRow = rewardNightsSettingsPage.getRewardAndFreeNightsCount()
                .getRewardAndFreeNightsCountGrid().getRow(1);

        verifyThat(freeNightsCountGridRow.getCell(LOVIR_THRESHOLD),
                hasText(lovirMap.get("LOVIR_THRES_QTY").toString()));
        verifyThat(freeNightsCountGridRow.getCell(PROCESSED_AND_PAID_LOW_OCC), hasText(not(EMPTY)));
        verifyThat(freeNightsCountGridRow.getCell(PROCESSED_AND_PAID_HIGH_OCC), hasText(not(EMPTY)));
        verifyThat(freeNightsCountGridRow.getCell(UNPROCESSED_BUT_PAID), hasText(not(EMPTY)));
        verifyThat(freeNightsCountGridRow.getCell(RewardAndFreeNightsCountGridCell.LOW_OCC_REIMBURSEMENT_AMOUNT),
                hasText(lowOccReimbAmountMap.get("LOW_OCC_REIMB_AMT").toString()));
        verifyThat(freeNightsCountGridRow.getCell(RewardAndFreeNightsCountGridCell.LOVIR_ADDITIONAL_REIMB_AMOUNT),
                hasText(not(EMPTY)));
        verifyThat(freeNightsCountGridRow.getCell(TOTAL_REIMB_AMOUNT), hasText(not(EMPTY)));
        verifyThat(freeNightsCountGridRow.getCell(RewardAndFreeNightsCountGridCell.CURRENCY),
                hasText(lowOccReimbAmountMap.get("CURR_CD").toString()));
    }
}
