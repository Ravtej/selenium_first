package com.ihg.crm.automation.selenium.tests.hotel;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.hotel.pages.Role.HOTEL_MANAGER;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ContextConfiguration;

import com.ihg.atp.wsdl.crm.guest.guest.serviceinterface.v2.GuestServiceDomainInterface;
import com.ihg.automation.selenium.common.SiteHelperBase;
import com.ihg.automation.selenium.common.User;
import com.ihg.automation.selenium.gwt.components.Component;
import com.ihg.automation.selenium.gwt.components.utils.FindStepUtils;
import com.ihg.automation.selenium.hotel.pages.HotelHelper;
import com.ihg.automation.selenium.tests.common.TestNGBase;

@ContextConfiguration(locations = { "classpath:app-context-hotel.xml" })
public class LoginLogout extends TestNGBase
{
    @Value("${loginUrl}")
    protected String loginUrl;

    @Resource(name = "user10T2")
    private User user10T2;

    @Value("${lastUpdateUserId}")
    protected String lastUpdateUserId;

    @Override
    protected SiteHelperBase getHelper()
    {
        return new HotelHelper(loginUrl);
    }

    public void login()
    {
        helper.login(user10T2, HOTEL_MANAGER, true);
    }

    public void login(String hotelCode)
    {
        user10T2.setLocation(hotelCode);
        helper.login(user10T2, HOTEL_MANAGER, true);
    }

    public void login(String hotelCode, String role)
    {
        user10T2.setLocation(hotelCode);
        helper.login(user10T2, role, true);
    }

    public Component getWelcome()
    {
        return new Component(null, "Welcome screen", "Welcome screen",
                FindStepUtils.byXpathWithWait(".//div[@id='y-cental-panel-id']/div[contains(.,'OR')]"));
    }

    public Component getSmallLogo()
    {
        return new Component(null, "Loyalty Connect Small Logo", "Loyalty Connect Small Logo",
                FindStepUtils.byXpathWithWait(".//img[@src='images/icon-logo.gif']"));
    }

    public Component getLargeLogo()
    {
        return new Component(null, "Loyalty Connect Large Logo", "Loyalty Connect Large Logo", FindStepUtils
                .byXpathWithWait(".//*[@id='y-cental-panel-id']/div[contains(@style, 'background-image')]"));
    }

    public void verifyWelcome()
    {
        verifyThat(getWelcome(), displayed(true));
    }

    public void verifyLogos()
    {
        verifyThat(getSmallLogo(), displayed(true));
        verifyThat(getLargeLogo(), displayed(true));
    }

    @Resource
    protected JdbcTemplate jdbcTemplateUpdate;

    @Resource
    protected JdbcTemplate jdbcTemplate;

    @Autowired
    protected GuestServiceDomainInterface guestClient;
}
