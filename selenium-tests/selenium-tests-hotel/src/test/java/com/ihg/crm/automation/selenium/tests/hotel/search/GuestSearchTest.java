package com.ihg.crm.automation.selenium.tests.hotel.search;

import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyError;
import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyNoError;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasText;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.isDisplayedAndEnabled;
import static com.ihg.automation.selenium.matchers.component.GridSize.size;
import static com.ihg.automation.selenium.matchers.text.EntityMatcherFactory.isValue;
import static org.hamcrest.Matchers.greaterThan;

import org.springframework.beans.factory.annotation.Value;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.ihg.automation.common.RandomUtils;
import com.ihg.automation.selenium.common.components.Country;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.hotel.pages.LeftPanel;
import com.ihg.automation.selenium.hotel.pages.personal.CustomerInfoFields;
import com.ihg.automation.selenium.hotel.pages.personal.PersonalInfoPage;
import com.ihg.automation.selenium.hotel.pages.search.GuestSearchByName;
import com.ihg.automation.selenium.hotel.pages.search.GuestSearchByNumber;
import com.ihg.automation.selenium.hotel.pages.search.GuestSearchResultsGrid;
import com.ihg.crm.automation.selenium.tests.hotel.LoginLogout;

public class GuestSearchTest extends LoginLogout
{
    @Value("${unique.search.memberId}")
    private String VALID_MEMBERSHIP_ID;

    @Value("${unique.search.given}")
    private String UNIQUE_NAME;

    @Value("${unique.search.surname}")
    private String UNIQUE_SURNAME;

    @Value("${unique.search.state}")
    private String UNIQUE_STATE;

    private static final String ERROR_INVALID_MEMBER_ID = "Membership Number invalid";
    private static final String ERROR_MORE_INFO = "Sorry, but the search you are performing requires that you "
            + "enter more information. Please review the form, add additional information, and then try again.";

    private GuestSearchByName guestSearchByName;
    private GuestSearchByNumber guestSearchByNumber;

    @BeforeClass
    public void before()
    {
        login();
    }

    @Test(priority = 1)
    public void verifySearchFields()
    {
        verifyWelcome();

        guestSearchByName = new GuestSearchByName();
        verifyThat(guestSearchByName.getLastName(), displayed(true));
        verifyThat(guestSearchByName.getFirstName(), displayed(true));
        verifyThat(guestSearchByName.getPhone(), displayed(true));
        verifyThat(guestSearchByName.getCity(), displayed(true));
        verifyThat(guestSearchByName.getZip(), displayed(true));
        verifyThat(guestSearchByName.getCountry(), displayed(true));
        verifyThat(guestSearchByName.getSearch(), isDisplayedAndEnabled(true));
        verifyThat(guestSearchByName.getClear(), isDisplayedAndEnabled(true));

        guestSearchByNumber = new GuestSearchByNumber();
        verifyThat(guestSearchByNumber.getMemberNumber(), displayed(true));
        verifyThat(guestSearchByNumber.getSearch(), isDisplayedAndEnabled(true));
        verifyThat(guestSearchByNumber.getAdvancedSearch(), displayed(true));
    }

    @DataProvider(name = "getInvalidNames")
    public Object[][] getInvalidNames()
    {
        return new Object[][] { { "Test", "Test" },
                { "1229baa50748245db216a6892507f268e4576286", "7da6f81dfd40caf7de01a52f69f129" },
                { " $^^^*&%&", " $^^#!@%&" } };
    }

    @Test(dataProvider = "getInvalidNames", priority = 10)
    public void searchByInvalidName(String lastName, String firstName)
    {
        guestSearchByName.byName(lastName, firstName, verifyError(ERROR_MORE_INFO));
    }

    @Test(priority = 20)
    public void searchByValidName()
    {
        guestSearchByName.byName("Cyarges", "Smoke", verifyNoError());
        GuestSearchResultsGrid grid = new GuestSearchResultsGrid();
        verifyThat(grid, size(greaterThan(1)));

        new LeftPanel().clickGuestSearch(verifyNoError());
        verifyThat(grid, displayed(false));
    }

    @Test(priority = 30)
    public void searchByUniqueName()
    {
        guestSearchByName.byName(UNIQUE_NAME, UNIQUE_SURNAME, verifyNoError());
        CustomerInfoFields customerInfo = new PersonalInfoPage().getCustomerInfo();
        verifyThat(customerInfo.getFullName(), hasText(UNIQUE_NAME + " " + UNIQUE_SURNAME));
        verifyThat(customerInfo.getResidenceCountry(), hasText(isValue(Country.US)));
        new LeftPanel().clickGuestSearch(verifyNoError());
    }

    @DataProvider(name = "getInvalidMemberIds")
    public Object[][] getInvalidMemberIds()
    {
        return new Object[][] { { "123456789" }, { "fnASSADif" }, { "^%$%&*^%*!" } };
    }

    @Test(priority = 40, dataProvider = "getInvalidMemberIds")
    public void searchByInvalidMemberId(String memberId)
    {
        guestSearchByNumber.byMemberNumber(memberId, verifyError(ERROR_INVALID_MEMBER_ID));
    }

    @Test(priority = 50)
    public void searchByInvalidNameAndValidNumber()
    {
        guestSearchByName.getFirstName().type(RandomUtils.getRandomLetters(7));
        guestSearchByName.getLastName().type(RandomUtils.getRandomLetters(7));
        guestSearchByNumber.getMemberNumber().type(VALID_MEMBERSHIP_ID);
        guestSearchByNumber.clickSearch(verifyNoError());
        LeftPanel leftPanel = new LeftPanel();
        verifyThat(leftPanel.getCustomerInfoPanel().getMemberNumber(Program.RC), hasText(VALID_MEMBERSHIP_ID));
        leftPanel.getNewSearch().clickAndWait(verifyNoError());
    }

    @Test(priority = 60)
    public void searchByNameAndState()
    {
        new LeftPanel().clickGuestSearch(verifyNoError());
        guestSearchByName.getFirstName().type(UNIQUE_NAME);
        guestSearchByName.getLastName().type(UNIQUE_SURNAME);
        guestSearchByName.getCity().type(UNIQUE_STATE);
        guestSearchByName.clickSearch(verifyNoError());
        verifyThat(new PersonalInfoPage().getCustomerInfo().getFullName(), hasText(UNIQUE_NAME + " " + UNIQUE_SURNAME));
        new LeftPanel().clickGuestSearch(verifyNoError());
    }
}
