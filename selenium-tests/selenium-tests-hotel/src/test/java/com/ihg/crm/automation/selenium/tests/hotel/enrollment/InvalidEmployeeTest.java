package com.ihg.crm.automation.selenium.tests.hotel.enrollment;

import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyError;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static com.ihg.automation.selenium.servicecenter.pages.Messages.HIGHLIGHTED_FIELDS_DID_NOT_PASS_VALIDATION;
import static org.apache.commons.lang3.StringUtils.EMPTY;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.gwt.components.input.Input;
import com.ihg.automation.selenium.hotel.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.matchers.component.ComponentMatcher;
import com.ihg.crm.automation.selenium.tests.hotel.LoginLogout;

public class InvalidEmployeeTest extends LoginLogout
{
    private EnrollmentPage enrollmentPage = new EnrollmentPage();

    @BeforeClass
    public void beforeClass()
    {
        Member member = new Member();
        member.addProgram(Program.RC);
        member.addAddress(MemberPopulateHelper.getUnitedStatesAddress());
        member.setPersonalInfo(MemberPopulateHelper.getSimplePersonalInfo());

        login();

        enrollmentPage.goTo();
        enrollmentPage.populate(member);
    }

    @DataProvider(name = "empIdInvalidProvider")
    protected Object[][] empIdInvalidProvider()
    {
        return new Object[][] { { "123456789" }, { "fefergfwegw" }, { "!#%$@#%$#!" } };
    }

    @DataProvider(name = "empIdEmptyProvider")
    protected Object[][] empIdEmptyProvider()
    {
        return new Object[][] { { EMPTY }, { " " } };
    }

    @Test(dataProvider = "empIdInvalidProvider")
    public void tryToEnrollWithInvalidEmpId(String employeeId)
    {
        verifyCombinations(employeeId, "Employee ID does not exist");
    }

    @Test
    public void tryToEnrollWithInactiveEmpId()
    {
        String closedEmployeeId = jdbcTemplate.queryForObject(
                "SELECT MBRSHP_ID FROM DGST.MBRSHP WHERE MBRSHP_LVL_CD = 'EMPL' AND MBRSHP_STAT_CD = 'C' AND ROWNUM < 2",
                String.class);
        verifyCombinations(closedEmployeeId, "Employee ID is not active");
    }

    @Test(dataProvider = "empIdEmptyProvider")
    public void tryToEnrollWithoutEmpId(String employeeId)
    {
        verifyCombinations(employeeId, HIGHLIGHTED_FIELDS_DID_NOT_PASS_VALIDATION);
    }

    private void verifyCombinations(String empId, String errorMessage)
    {
        Input employeeID = enrollmentPage.getYourEmployeeID();
        employeeID.type(empId);
        verifyThat(enrollmentPage.getHotelCode(), hasText(helper.getUser().getLocation()));
        enrollmentPage.clickSubmit(verifyError(errorMessage));
        verifyThat(employeeID, ComponentMatcher.isHighlightedAsInvalid(true));
    }
}
