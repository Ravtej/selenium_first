package com.ihg.crm.automation.selenium.tests.hotel.operations;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.number.OrderingComparison.greaterThan;

import javax.annotation.Resource;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.User;
import com.ihg.automation.selenium.common.pages.login.TopUserSection;
import com.ihg.automation.selenium.gwt.components.input.Select;
import com.ihg.automation.selenium.hotel.pages.Role;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.br.manage.ManageEventsPage;
import com.ihg.crm.automation.selenium.tests.hotel.LoginLogout;

public class WithoutRoleHelpDeskPermissionTest extends LoginLogout
{
    @Resource(name = "user5HT2")
    protected User user5HT2;

    @BeforeClass
    public void beforeClass()
    {
        login(user5HT2, Role.HOTEL_MANAGER);

    }

    @Test
    public void verifyHotelDropDown()
    {
        ManageEventsPage manageEventsPage = new ManageEventsPage();
        manageEventsPage.goToByOperation();

        Select hotelCode = manageEventsPage.getSearchFields().getHotelCode();
        verifyThat(hotelCode, hotelCode.openElementsList().getItems().size(), greaterThan(1),
                "Assert that all user’s hotels are displayed in the Hotel Drop Down");
    }

    @Test
    public void verifyHotelInTopMenu()
    {
        TopUserSection userSection = new TopUserSection();
        verifyThat(userSection.getHotel(), displayed(false));
        verifyThat(userSection, hasText(containsString(helper.getUser().getLocation())));
    }
}
