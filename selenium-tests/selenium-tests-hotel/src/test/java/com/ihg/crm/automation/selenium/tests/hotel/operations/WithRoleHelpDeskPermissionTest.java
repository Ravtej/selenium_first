package com.ihg.crm.automation.selenium.tests.hotel.operations;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.HasSelectItems.hasSelectItems;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static com.ihg.automation.selenium.matchers.component.IsDisplayedAndEnabled.isDisplayedAndEnabled;

import java.util.ArrayList;
import java.util.Arrays;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.pages.login.TopUserSection;
import com.ihg.automation.selenium.gwt.components.input.SearchInput;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.br.manage.ManageEventsPage;
import com.ihg.crm.automation.selenium.tests.hotel.LoginLogout;

public class WithRoleHelpDeskPermissionTest extends LoginLogout
{
    @BeforeClass
    public void beforeClass()
    {
        login();
    }

    @Test
    public void verifyHotelDropDown()
    {
        ManageEventsPage manageEventsPage = new ManageEventsPage();
        manageEventsPage.goToByOperation();

        verifyThat(manageEventsPage.getSearchFields().getHotelCode(),
                hasSelectItems(Arrays.asList(helper.getUser().getLocation())));
    }

    @Test
    public void verifyHotelInTopMenu()
    {
        SearchInput hotel = new TopUserSection().getHotel();
        verifyThat(hotel, isDisplayedAndEnabled(true));
        verifyThat(hotel, hasText(helper.getUser().getLocation()));
    }
}
