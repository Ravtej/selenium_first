package com.ihg.crm.automation.selenium.tests.hotel.enrollment.natives;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.components.NativeLanguage.SIMPLIFIED_CHINESE;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static org.hamcrest.core.StringContains.containsString;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.components.Country;
import com.ihg.automation.selenium.common.contact.Address;
import com.ihg.automation.selenium.common.member.GuestAddress;
import com.ihg.automation.selenium.common.member.GuestName;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.name.PersonNameFields;
import com.ihg.automation.selenium.common.types.address.AddressType;
import com.ihg.automation.selenium.common.types.address.RegionChinaNative;
import com.ihg.automation.selenium.common.types.address.RegionChinaRoman;
import com.ihg.automation.selenium.common.types.name.NameConfiguration;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.gwt.components.Mode;
import com.ihg.automation.selenium.hotel.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.hotel.pages.personal.AddressContact;
import com.ihg.automation.selenium.hotel.pages.personal.CustomerInfoFields;
import com.ihg.automation.selenium.hotel.pages.personal.HotelAddressContactList;
import com.ihg.automation.selenium.hotel.pages.personal.PersonalInfoPage;
import com.ihg.crm.automation.selenium.tests.hotel.LoginLogout;

public class EnrollmentWithMemberFromChinaTest extends LoginLogout
{
    private EnrollmentPage enrollmentPage = new EnrollmentPage();
    private GuestName name = new GuestName();
    private GuestAddress address = new GuestAddress();
    private PersonalInfoPage persInfo = new PersonalInfoPage();
    private CustomerInfoFields customerInfoFields;
    private HotelAddressContactList addressContactList;
    private Member member = new Member();

    @BeforeClass
    public void before()
    {
        GuestName nativeName = new GuestName();
        nativeName.setGiven("名字");
        nativeName.setSurname("姓");
        nativeName.setNativeLanguage(SIMPLIFIED_CHINESE);

        name.setGiven("ming zi");
        name.setSurname("xing");
        name.setNativeName(nativeName);

        GuestAddress nativeAddress = new GuestAddress();
        nativeAddress.setLine1("东直门北中街4");
        nativeAddress.setLocality1("北京市");
        nativeAddress.setRegion1(RegionChinaNative.BEI_JING_SHI);

        address.setCountryCode(Country.CN);
        address.setType(AddressType.RESIDENCE);
        address.setLocality1("bei jing shi");
        address.setLine1("dong zhi men bei zhong jie 4");
        address.setRegion1(RegionChinaRoman.BEI_JING_SHI);

        address.setNativeAddress(nativeAddress);

        login();
    }

    @Test
    public void populateNameNativeFields()
    {
        enrollmentPage.goTo();
        enrollmentPage.getPrograms().selectProgram(Program.RC);
        enrollmentPage.getCustomerInfo().getResidenceCountry().selectByCode(Country.CN);

        PersonNameFields nameFields = enrollmentPage.getCustomerInfo().getName();
        nameFields.setConfiguration(Country.CN);
        nameFields.populateNativeFields(name);
        nameFields.verify(name, Mode.EDIT);
    }

    @Test(dependsOnMethods = { "populateNameNativeFields" })
    public void populateAddressNativeFields()
    {
        Address addressFields = enrollmentPage.getAddress();
        verifyThat(addressFields, hasText(containsString(SIMPLIFIED_CHINESE)));
        addressFields.populateNativeFields(address);
        addressFields.verify(address, Mode.EDIT);

        addressFields.getClearNativeAddress().clickAndWait();

        addressFields.populateNativeFields(address);
    }

    @Test(dependsOnMethods = { "populateAddressNativeFields" })
    public void completeEnrollment()
    {
        enrollmentPage.getYourEmployeeID().type(helper.getUser().getEmployeeId());
        enrollmentPage.completeEnrollWithDuplicatePopUp(member);
    }

    @Test(dependsOnMethods = { "completeEnrollment" })
    public void verifyCustomerInfoDetailsViewMode()
    {
        persInfo.goTo();
        customerInfoFields = persInfo.getCustomerInfo();
        PersonNameFields personNameFields = customerInfoFields.getName();
        verifyThat(personNameFields.getFullName(),
                hasText(name.getFullName(NameConfiguration.getEasternAsiaConfiguration())));
        verifyThat(personNameFields.getFullNativeName(),
                hasText(name.getNativeName().getFullName(NameConfiguration.getEasternAsiaConfiguration())));
        verifyThat(customerInfoFields.getNativeLanguage(), hasText(SIMPLIFIED_CHINESE));
        verifyThat(personNameFields.getTransliterate(), displayed(false));
    }

    @Test(dependsOnMethods = { "verifyCustomerInfoDetailsViewMode" })
    public void verifyCustomerInfoDetailsEditMode()
    {
        customerInfoFields.clickEdit();
        PersonNameFields personNameFields = customerInfoFields.getName();
        personNameFields.setConfiguration(Country.CN);
        verifyThat(personNameFields.getNativeGivenName(), displayed(false));
        verifyThat(personNameFields.getNativeMiddleName(), displayed(false));
        verifyThat(personNameFields.getNativeSurname(), displayed(false));
        verifyThat(personNameFields.getTransliterate(), displayed(false));
        verifyThat(personNameFields.getTransliterate(), displayed(false));

        verifyThat(personNameFields.getFullName(),
                hasText(name.getFullName(NameConfiguration.getEasternAsiaConfiguration())));
        verifyThat(personNameFields.getFullNativeName(),
                hasText(name.getNativeName().getFullName(NameConfiguration.getEasternAsiaConfiguration())));
        verifyThat(customerInfoFields.getNativeLanguage(), hasText(SIMPLIFIED_CHINESE));

        customerInfoFields.clickCancel();
    }

    @Test(dependsOnMethods = { "verifyCustomerInfoDetailsEditMode" })
    public void verifyAddressDetailsInView()
    {
        addressContactList = persInfo.getAddressList();

        verifyThat(addressContactList.getContact(), hasText(containsString(address.getCollapseAddress())));
    }

    @Test(dependsOnMethods = { "verifyAddressDetailsInView" })
    public void verifyAddressDetailsExpanded()
    {
        addressContactList.getContact().expand();
        Address addressExpanded = addressContactList.getExpandedContact().getBaseContact();
        verifyThat(addressExpanded.getTransliterate(), displayed(false));
        verifyThat(addressExpanded.getClearNativeAddress(), displayed(false));
        verifyThat(addressExpanded, hasText(containsString(SIMPLIFIED_CHINESE)));
    }

    @Test(dependsOnMethods = { "verifyAddressDetailsExpanded" })
    public void verifyAddressDetailsEditMode()
    {
        AddressContact contact = addressContactList.getContact();
        contact.clickEdit();
        contact.getBaseContact().verifyNativeFields(address, SIMPLIFIED_CHINESE, Mode.EDIT);
    }
}
