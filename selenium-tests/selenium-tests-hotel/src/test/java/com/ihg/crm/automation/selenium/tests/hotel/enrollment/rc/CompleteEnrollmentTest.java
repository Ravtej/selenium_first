package com.ihg.crm.automation.selenium.tests.hotel.enrollment.rc;

import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyNoError;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.assertThat;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.Constant.NOT_AVAILABLE;
import static com.ihg.automation.selenium.common.Constant.RC_POINTS;
import static com.ihg.automation.selenium.common.types.program.ProgramStatus.OPEN;
import static com.ihg.automation.selenium.common.types.program.RewardClubLevel.CLUB;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.automation.selenium.matchers.component.GridSize.size;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static com.ihg.automation.selenium.matchers.component.ViewableMatcher.hasTextInView;
import static org.hamcrest.core.StringEndsWith.endsWith;

import org.joda.time.LocalDate;
import org.springframework.beans.factory.annotation.Value;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.member.GuestName;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.member.PersonalInfo;
import com.ihg.automation.selenium.common.pages.program.RewardClubTierLevelActivityGrid;
import com.ihg.automation.selenium.common.personal.PhoneContact;
import com.ihg.automation.selenium.common.types.Gender;
import com.ihg.automation.selenium.common.types.name.Degree;
import com.ihg.automation.selenium.common.types.name.NameSuffix;
import com.ihg.automation.selenium.common.types.name.Salutation;
import com.ihg.automation.selenium.common.types.name.Title;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.gwt.components.Mode;
import com.ihg.automation.selenium.gwt.components.panel.FieldSet;
import com.ihg.automation.selenium.hotel.pages.LeftPanel;
import com.ihg.automation.selenium.hotel.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.hotel.pages.personal.CustomerInfoFields;
import com.ihg.automation.selenium.hotel.pages.personal.HotelEmailContact;
import com.ihg.automation.selenium.hotel.pages.personal.PersonalInfoPage;
import com.ihg.automation.selenium.hotel.pages.programs.rc.RewardClubPage;
import com.ihg.crm.automation.selenium.tests.hotel.LoginLogout;

public class CompleteEnrollmentTest extends LoginLogout
{

    @Value("${achieveGoldNights}")
    private Integer achieveGoldNights;

    @Value("${achieveGoldPoints}")
    private Integer achieveGoldPoints;

    private Member member = new Member();
    private RewardClubPage rewardClubPage;
    private PersonalInfo personalInfo;
    private EnrollmentPage enrollmentPage = new EnrollmentPage();

    @BeforeClass
    public void before()
    {
        personalInfo = MemberPopulateHelper.getSimplePersonalInfo();
        GuestName guestName = personalInfo.getName();
        guestName.setSalutation(Salutation.DR);
        guestName.setSuffix(NameSuffix.ESQ);
        guestName.setTitle(Title.AMB);
        guestName.setDegree(Degree.DDS);
        personalInfo.setGenderCode(Gender.MALE);
        personalInfo.setBirthDate(new LocalDate());
        member.setPersonalInfo(personalInfo);

        member.addProgram(Program.RC);
        member.addAddress(MemberPopulateHelper.getUnitedStatesAddress());
        member.addEmail(MemberPopulateHelper.getRandomEmail());
        member.addPhone(MemberPopulateHelper.getRandomPhone());

        login();
    }

    @Test
    public void enrollmentIsCancelled()
    {
        enrollmentPage.goTo();
        enrollmentPage.populate(member);

        enrollmentPage.clickCancel(verifyNoError());
        assertThat(getWelcome(), displayed(true));
    }

    @Test(dependsOnMethods = { "enrollmentIsCancelled" }, alwaysRun = true)
    public void enrollMember()
    {
        enrollmentPage.enroll(member, helper);
    }

    @Test(dependsOnMethods = { "enrollMember" }, alwaysRun = true)
    public void customerInformation()
    {
        PersonalInfoPage personalInfoPage = new PersonalInfoPage();
        CustomerInfoFields customerInfoViewMode = personalInfoPage.getCustomerInfo();

        verifyThat(customerInfoViewMode.getGender(),
                hasTextInView(member.getPersonalInfo().getGenderCode().getValue()));
        customerInfoViewMode.getBirthDate().verify(personalInfo.getBirthDate(), Mode.VIEW);
        verifyThat(customerInfoViewMode.getResidenceCountry(), hasText(personalInfo.getResidenceCountry().getValue()));
        verifyThat(customerInfoViewMode.getName().getFullName(), hasText(personalInfo.getName().getFullName()));
    }

    @Test(dependsOnMethods = { "customerInformation" }, alwaysRun = true)
    public void validatePersonalInformation()
    {
        PersonalInfoPage personalInfoPage = new PersonalInfoPage();

        personalInfoPage.getCustomerInfo().verify(member, Mode.VIEW);
        personalInfoPage.getAddressList().getExpandedContact().verify(member.getPreferredAddress(), Mode.VIEW);

        PhoneContact phone = personalInfoPage.getPhoneList().getExpandedContact();
        phone.verify(member.getPreferredPhone(), Mode.VIEW);
        verifyThat(phone.getPreferredIcon(), displayed(true));

        HotelEmailContact email = personalInfoPage.getEmailList().getExpandedContact();
        email.verify(member.getPreferredEmail(), Mode.VIEW);
        verifyThat(email.getPreferredIcon(), displayed(true));
    }

    @Test(dependsOnMethods = { "validatePersonalInformation" })
    public void validateProgramInfoSummaryDetails()
    {
        rewardClubPage = new RewardClubPage();
        rewardClubPage.goTo();

        FieldSet programOverview = rewardClubPage.getProgramOverview();
        programOverview.expand();
        verifyThat(programOverview, hasText(endsWith("Base program")));

        rewardClubPage.getSummary().verify("0", OPEN, CLUB, NOT_AVAILABLE, member.getRCProgramId());
    }

    @Test(dependsOnMethods = { "validateProgramInfoSummaryDetails" }, alwaysRun = true)
    public void validateTierLevelAnnualActivity()
    {
        RewardClubTierLevelActivityGrid currentYearAnnualActivities = rewardClubPage.getCurrentYearAnnualActivities();

        currentYearAnnualActivities.getPointsRow().verify("0", achieveGoldPoints);
        currentYearAnnualActivities.getNightsRow().verify("0", achieveGoldNights);
    }

    @Test(dependsOnMethods = { "validateTierLevelAnnualActivity" }, alwaysRun = true)
    public void validateEmployeeRateEligibility()
    {
        rewardClubPage.getEmployeeRateEligibility().verifyInViewMode("No", NOT_AVAILABLE, NOT_AVAILABLE, NOT_AVAILABLE);
    }

    @Test(dependsOnMethods = { "validateEmployeeRateEligibility" }, alwaysRun = true)
    public void verifyEarningDetails()
    {
        verifyThat(rewardClubPage.getEarningDetails().getEarningPreference(), hasText(RC_POINTS));
    }

    @Test(dependsOnMethods = { "verifyEarningDetails" })
    public void validateAllianceGrid()
    {
        verifyThat(rewardClubPage.getAlliances(), size(0));
    }

    @Test(dependsOnMethods = { "validateAllianceGrid" }, alwaysRun = true)
    public void validateEnrollmentDetails()
    {
        rewardClubPage = new RewardClubPage();
        rewardClubPage.goTo();

        rewardClubPage.getEnrollmentDetails().verifyDetails(helper);
    }

    @Test(dependsOnMethods = { "validateEnrollmentDetails" }, alwaysRun = true)
    public void verifyLeftPanel()
    {
        new LeftPanel().getProgramInfoPanel().getPrograms().getRewardClubProgram().verify(CLUB, "0");
    }
}
