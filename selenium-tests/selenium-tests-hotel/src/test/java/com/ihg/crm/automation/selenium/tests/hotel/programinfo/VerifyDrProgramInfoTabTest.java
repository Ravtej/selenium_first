package com.ihg.crm.automation.selenium.tests.hotel.programinfo;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.member.DiningRewardAward.ENROLL_COMPLIMENTARY;
import static com.ihg.automation.selenium.common.member.MemberJdbcUtils.UPDATE_EXP;
import static com.ihg.automation.selenium.common.types.program.Program.DR;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasEmptyOrNullText;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static org.hamcrest.core.IsNot.not;
import static org.hamcrest.core.StringEndsWith.endsWith;

import org.springframework.beans.factory.annotation.Value;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.EventSource;
import com.ihg.automation.selenium.common.ProgramsGrid;
import com.ihg.automation.selenium.common.member.MemberJdbcUtils;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.common.types.program.ProgramStatus;
import com.ihg.automation.selenium.gwt.components.panel.FieldSet;
import com.ihg.automation.selenium.hotel.pages.LeftPanel;
import com.ihg.automation.selenium.hotel.pages.programs.ProgramPageBase;
import com.ihg.automation.selenium.hotel.pages.programs.dr.DiningRewardsPage;
import com.ihg.automation.selenium.hotel.pages.programs.dr.DiningRewardsSummary;
import com.ihg.automation.selenium.hotel.pages.search.GuestSearchByNumber;
import com.ihg.crm.automation.selenium.tests.hotel.LoginLogout;

public class VerifyDrProgramInfoTabTest extends LoginLogout
{
    private static final int MONTHS_SHIFT = 13;

    @Value("${memberProgramDR}")
    protected String memberProgramDR;

    @BeforeClass
    public void beforeClass()
    {
        MemberJdbcUtils.update(jdbcTemplateUpdate, UPDATE_EXP, memberProgramDR, DR, MONTHS_SHIFT);

        login("SHGHA");
        GuestSearchByNumber guestSearchByNumber = new GuestSearchByNumber();
        guestSearchByNumber.getMemberNumber().type(memberProgramDR);
        guestSearchByNumber.clickSearch();
    }

    @Test
    public void validateLeftPanelProgramInfoNavigation()
    {
        ProgramsGrid grid = new LeftPanel().getProgramInfoPanel().getPrograms();
        grid.getProgram(Program.DR).goToProgramPage();
        DiningRewardsPage diningRewardsPage = new DiningRewardsPage();
        verifyThat(diningRewardsPage, displayed(true));
    }

    @Test(dependsOnMethods = { "validateLeftPanelProgramInfoNavigation" }, alwaysRun = true)
    public void verifyOverview()
    {
        DiningRewardsPage diningRewardsPage = new DiningRewardsPage();
        verifyThat(diningRewardsPage, displayed(true));

        FieldSet programOverview = diningRewardsPage.getProgramOverview();
        programOverview.expand();
        verifyThat(programOverview, hasText(endsWith("IHG Dining Rewards program")));
    }

    @Test(dependsOnMethods = { "verifyOverview" }, alwaysRun = true)
    public void verifySummary()
    {
        DiningRewardsSummary diningRewardsSummary = new DiningRewardsPage().getSummary();
        diningRewardsSummary.verify(MONTHS_SHIFT, ProgramStatus.OPEN, memberProgramDR, ENROLL_COMPLIMENTARY);
    }

    @Test(dependsOnMethods = { "verifySummary" }, alwaysRun = true)
    public void verifyEnrollmentDetails()
    {
        DiningRewardsPage diningRewardsPage = new DiningRewardsPage();
        ProgramPageBase.EnrollmentDetails enrollmentDetails = diningRewardsPage.getEnrollmentDetails();
        EventSource source = enrollmentDetails.getSource();
        verifyThat(source.getEmployeeId(), not(hasEmptyOrNullText()));
        verifyThat(source.getChannel(), not(hasEmptyOrNullText()));
        verifyThat(source.getLocation(), not(hasEmptyOrNullText()));
        verifyThat(enrollmentDetails.getEnrollDate(), not(hasEmptyOrNullText()));
        verifyThat(enrollmentDetails.getRenewalDate(), not(hasEmptyOrNullText()));
        verifyThat(enrollmentDetails.getOfferCode(), not(hasEmptyOrNullText()));
        verifyThat(enrollmentDetails.getReferringMember(), not(hasEmptyOrNullText()));
        verifyThat(enrollmentDetails.getPurchaseAmount(), not(hasEmptyOrNullText()));
        verifyThat(enrollmentDetails.getPaymentType(), not(hasEmptyOrNullText()));
    }
}
