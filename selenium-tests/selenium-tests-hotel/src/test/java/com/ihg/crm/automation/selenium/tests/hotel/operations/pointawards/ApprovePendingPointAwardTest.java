package com.ihg.crm.automation.selenium.tests.hotel.operations.pointawards;

import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyNoError;
import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifySuccess;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.PointAwardsStatus.PENDING;
import static com.ihg.automation.selenium.common.PointAwardsStatus.PROCESSING;
import static com.ihg.automation.selenium.common.member.AmbassadorUtils.PURCHASE_AMBASSADOR_HOTEL_$200;
import static com.ihg.automation.selenium.common.types.program.RewardClubLevel.GOLD;
import static com.ihg.automation.selenium.hotel.pages.Role.HOTEL_FRONT_DESK_FEE_BASED;
import static com.ihg.automation.selenium.hotel.pages.Role.HOTEL_OPERATIONS_MANAGER;
import static com.ihg.automation.selenium.hotel.pages.hoteloperations.awards.manage.ManagePointAwardsGridRow.ManagePointAwardsGridCell.STATUS;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.enabled;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static org.hamcrest.Matchers.containsString;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.pages.login.TopUserSection;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.gwt.components.Button;
import com.ihg.automation.selenium.gwt.components.dialog.ConfirmDialog;
import com.ihg.automation.selenium.hotel.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.awards.CreatePointAwardPopUp;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.awards.PointAward;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.awards.manage.ManagePointAward;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.awards.manage.ManagePointAwardsGridRow;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.awards.manage.ManagePointAwardsPage;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.awards.posted.PostedPointAwardsPage;
import com.ihg.automation.selenium.matchers.component.ComponentMatcher;
import com.ihg.automation.selenium.matchers.component.HasRowColor;
import com.ihg.crm.automation.selenium.tests.hotel.LoginLogout;

public class ApprovePendingPointAwardTest extends LoginLogout
{
    private static final String HOTEL = "SHGHA";
    private String membershipId;
    private ManagePointAward managePointAward = new ManagePointAward();

    @BeforeClass
    public void before()
    {
        Member member = new Member();
        member.addProgram(Program.AMB);
        member.setPersonalInfo(MemberPopulateHelper.getSimplePersonalInfo());
        member.addAddress(MemberPopulateHelper.getUnitedStatesAddress());
        member.setAmbassadorAmount(PURCHASE_AMBASSADOR_HOTEL_$200);

        login(HOTEL);
        member = new EnrollmentPage().enroll(member, helper);
        membershipId = member.getAMBProgramId();

        managePointAward.setMemberNumber(membershipId);
        managePointAward.setTransaction(PointAward.HOTEL_PROMOTION);
        managePointAward.setMemberName(member);

        managePointAward.setPoints("2000");
        managePointAward.setMemberLevel(GOLD);
        managePointAward.setStatus(PENDING);
        managePointAward.setSource(helper, HOTEL);

        new TopUserSection().switchUserRole(HOTEL_FRONT_DESK_FEE_BASED);
    }

    @Test
    public void createPointAwardsInPendingStatus()
    {
        ManagePointAwardsPage managePointAwardsPage = new ManagePointAwardsPage();
        managePointAwardsPage.goToByOperation();
        managePointAwardsPage.getCreatePointAward().clickAndWait();

        CreatePointAwardPopUp createPointAwardPopUp = new CreatePointAwardPopUp();
        createPointAwardPopUp.populate(managePointAward);
        createPointAwardPopUp.clickSubmit(verifyNoError());

        ConfirmDialog warningPopUp = new ConfirmDialog();
        verifyThat(warningPopUp, displayed(true));

        verifyThat(warningPopUp, hasText(containsString("This point amount exceeds your limit.")));

        warningPopUp.clickYes(
                verifySuccess("Point Award for member " + membershipId + " is created and pending further approval"));
    }

    @Test(dependsOnMethods = { "createPointAwardsInPendingStatus" }, alwaysRun = true)
    public void verifyPointAwardsInPendingStatus()
    {
        PostedPointAwardsPage postedPointAwardsPage = new PostedPointAwardsPage();
        postedPointAwardsPage.goTo();
        verifyThat(postedPointAwardsPage.getPostedPointAwardsGrid().getRow(membershipId), displayed(false));

        ManagePointAwardsPage managePointAwardsPage = new ManagePointAwardsPage();
        managePointAwardsPage.goTo();
        ManagePointAwardsGridRow pendingAwardRow = managePointAwardsPage.getManagePointAwardsGrid()
                .getRow(membershipId);
        pendingAwardRow.verify(managePointAward);
    }

    @Test(dependsOnMethods = { "verifyPointAwardsInPendingStatus" }, alwaysRun = true)
    public void approvePointAwardInPendingStatus()
    {
        new TopUserSection().switchUserRole(HOTEL_OPERATIONS_MANAGER, false);

        ManagePointAwardsPage managePointAwardsPage = new ManagePointAwardsPage();
        managePointAwardsPage.goToByOperation();

        Button approveSelected = managePointAwardsPage.getApproveSelected();
        verifyThat(approveSelected, enabled(false));

        ManagePointAwardsGridRow pendingAwardRow = managePointAwardsPage.getManagePointAwardsGrid()
                .getRow(membershipId);
        pendingAwardRow.getCellAsCheckBox().click();
        verifyThat(approveSelected, enabled(true));
        approveSelected.clickAndWait(verifyNoError());

        pendingAwardRow = managePointAwardsPage.getManagePointAwardsGrid().getRow(membershipId);
        verifyThat(pendingAwardRow, HasRowColor.isGreenColor());
        verifyThat(pendingAwardRow.getCell(STATUS), ComponentMatcher.hasText(PROCESSING));

        PostedPointAwardsPage postedPointAwardsPage = new PostedPointAwardsPage();
        postedPointAwardsPage.goTo();
        verifyThat(postedPointAwardsPage.getPostedPointAwardsGrid().getRow(membershipId), displayed(true));
    }
}
