package com.ihg.crm.automation.selenium.tests.hotel.enrollment.natives;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.components.Country.US;
import static com.ihg.automation.selenium.common.components.NativeLanguage.SIMPLIFIED_CHINESE;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.member.GuestAddress;
import com.ihg.automation.selenium.common.member.GuestName;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.name.PersonNameFields;
import com.ihg.automation.selenium.common.types.name.NameConfiguration;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.gwt.components.Mode;
import com.ihg.automation.selenium.hotel.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.hotel.pages.personal.CustomerInfoFields;
import com.ihg.automation.selenium.hotel.pages.personal.PersonalInfoPage;
import com.ihg.crm.automation.selenium.tests.hotel.LoginLogout;

public class NonChineseMemberWithNativeLanguageTest extends LoginLogout
{
    private EnrollmentPage enrollmentPage = new EnrollmentPage();
    private GuestName name = new GuestName();
    private GuestAddress address = new GuestAddress();

    @BeforeClass
    public void before()
    {
        GuestName nativeName = new GuestName();
        nativeName.setGiven("名");
        nativeName.setSurname("姓");
        nativeName.setNativeLanguage(SIMPLIFIED_CHINESE);

        name.setGiven("ming");
        name.setSurname("xing");
        name.setNativeName(nativeName);

        address = MemberPopulateHelper.getUnitedStatesAddress();

        login();
    }

    @Test
    public void populateNameNativeFields()
    {
        enrollmentPage.goTo();
        enrollmentPage.getPrograms().selectProgram(Program.RC);
        enrollmentPage.getCustomerInfo().getResidenceCountry().selectByCode(US);
        PersonNameFields nameFields = enrollmentPage.getCustomerInfo().getName();
        nameFields.setConfiguration(US);
        nameFields.populateNativeFields(name);
        nameFields.verify(name, Mode.EDIT);
    }

    @Test(dependsOnMethods = { "populateNameNativeFields" })
    public void completeEnrollment()
    {
        enrollmentPage.getAddress().populate(address);
        enrollmentPage.getYourEmployeeID().type(helper.getUser().getEmployeeId());
        enrollmentPage.completeEnrollWithDuplicatePopUp(new Member());
    }

    @Test(dependsOnMethods = { "completeEnrollment" })
    public void verifyCustomerInfoDetails()
    {
        PersonalInfoPage persInfo = new PersonalInfoPage();
        persInfo.goTo();
        CustomerInfoFields customerInfoFields = persInfo.getCustomerInfo();
        PersonNameFields personNameFields = customerInfoFields.getName();
        verifyThat(personNameFields.getFullName(),
                hasText(name.getFullName(NameConfiguration.getDefaultConfiguration())));
        verifyThat(personNameFields.getFullNativeName(),
                hasText(name.getNativeName().getFullName(NameConfiguration.getDefaultConfiguration())));
        verifyThat(customerInfoFields.getNativeLanguage(), hasText(SIMPLIFIED_CHINESE));
        verifyThat(personNameFields.getTransliterate(), displayed(false));
    }
}
