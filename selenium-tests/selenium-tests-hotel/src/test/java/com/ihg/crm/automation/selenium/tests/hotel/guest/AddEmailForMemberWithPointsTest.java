package com.ihg.crm.automation.selenium.tests.hotel.guest;

import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyError;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasTextAsInt;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.isDisplayedAndEnabled;

import org.springframework.beans.factory.annotation.Value;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.gwt.components.Button;
import com.ihg.automation.selenium.hotel.pages.LeftPanel;
import com.ihg.automation.selenium.hotel.pages.personal.HotelEmailContact;
import com.ihg.automation.selenium.hotel.pages.personal.HotelEmailContactList;
import com.ihg.automation.selenium.hotel.pages.personal.PersonalInfoPage;
import com.ihg.automation.selenium.hotel.pages.search.GuestSearch;
import com.ihg.automation.selenium.hotel.pages.search.GuestSearchByNumber;
import com.ihg.crm.automation.selenium.tests.hotel.LoginLogout;

public class AddEmailForMemberWithPointsTest extends LoginLogout
{
    private static final String EMAIL_UPDATE_NOT_POSSIBLE = "An e-mail address can not be added through LoyaltyConnect for this account due to the point balance. "
            + "Please ask the guest to visit the IHG Rewards Club website or contact the Service Center for assistance adding an email to their account.";

    private static final String UPDATE_POINTS_SQL = "UPDATE DGST.MBRSHP m " //
            + "SET m.CURRENT_BAL_UNIT_AMT = %3$d, " //
            + "m.LST_UPDT_USR_ID='%2$s', " //
            + "m.LST_UPDT_TS = SYSDATE " //
            + "WHERE m.MBRSHP_ID = '%1$s' AND m.LYTY_PGM_CD='PC'";

    private static final int POINTS_LIMIT = 50000;
    private static final int VALID_POINTS_QUANTITY = POINTS_LIMIT - 1;

    @Value("${member.with.points}")
    private String membershipId;

    @Value("${lastUpdateUserId}")
    private String lastUpdateUserId;

    @BeforeClass
    public void before()
    {
        login();
    }

    @Test(priority = 10)
    public void tryToAddEmailForMemberWith50kPoints()
    {
        updateMemberPoints(POINTS_LIMIT);

        new GuestSearchByNumber().byMemberNumber(membershipId);

        verifyThat(new LeftPanel().getProgramInfoPanel().getPrograms().getRewardClubProgram().getPointsBalance(),
                hasTextAsInt(POINTS_LIMIT));

        Button addButton = new PersonalInfoPage().getEmailList().getAddButton();
        verifyThat(addButton, isDisplayedAndEnabled(true));
        addButton.clickAndWait(verifyError(EMAIL_UPDATE_NOT_POSSIBLE));

        new LeftPanel().getBackToResults().click();
    }

    @Test(priority = 20)
    public void addEmailToMemberWithLessThan50kPoints()
    {
        updateMemberPoints(VALID_POINTS_QUANTITY);

        new GuestSearch().clickSearch();

        verifyThat(new LeftPanel().getProgramInfoPanel().getPrograms().getRewardClubProgram().getPointsBalance(),
                hasTextAsInt(VALID_POINTS_QUANTITY));

        HotelEmailContactList emailList = new PersonalInfoPage().getEmailList();
        emailList.clickAddButton();

        HotelEmailContact contact = emailList.getContact();
        verifyThat(contact.getBaseContact().getEmail(), isDisplayedAndEnabled(true));
        verifyThat(contact.getSave(), isDisplayedAndEnabled(true));
    }

    private void updateMemberPoints(int points)
    {
        jdbcTemplateUpdate.update(String.format(UPDATE_POINTS_SQL, membershipId, lastUpdateUserId, points));
    }
}
