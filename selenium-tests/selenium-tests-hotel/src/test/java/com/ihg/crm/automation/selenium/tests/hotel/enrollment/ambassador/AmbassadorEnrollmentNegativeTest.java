package com.ihg.crm.automation.selenium.tests.hotel.enrollment.ambassador;

import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyNoError;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.assertThat;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.hotel.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.hotel.pages.programs.amb.PrivacyConfirmationPopUp;
import com.ihg.crm.automation.selenium.tests.hotel.LoginLogout;

public class AmbassadorEnrollmentNegativeTest extends LoginLogout
{
    private Member member = new Member();
    private static final String HOTEL = "CEQHA";

    @BeforeClass
    public void beforeClass()
    {
        member.addProgram(Program.AMB);
        member.addEmail(MemberPopulateHelper.getRandomEmail());
        member.addPhone(MemberPopulateHelper.getRandomPhone());
        member.addAddress(MemberPopulateHelper.getUnitedStatesAddress());
        member.setPersonalInfo(MemberPopulateHelper.getSimplePersonalInfo());

        login(HOTEL);
    }

    @Test
    public void enrollmentIsCancelled()
    {
        EnrollmentPage enrollmentPage = new EnrollmentPage();
        enrollmentPage.goTo();
        enrollmentPage.populate(member);

        enrollmentPage.clickCancel(verifyNoError());
        assertThat(getWelcome(), displayed(true));
    }

    @Test(dependsOnMethods = { "enrollmentIsCancelled" }, alwaysRun = true)
    public void startEnrollmentUsingEuropeanHotel()
    {
        EnrollmentPage enrollmentPage = new EnrollmentPage();
        enrollmentPage.goTo();
        enrollmentPage.populate(member);
        enrollmentPage.getYourEmployeeID().type(helper.getUser().getEmployeeId());
        enrollmentPage.clickSubmitAndSkipWarnings(verifyNoError());
    }

    @Test(dependsOnMethods = { "startEnrollmentUsingEuropeanHotel" }, alwaysRun = true)
    public void doNotAcceptAgreement()
    {
        new PrivacyConfirmationPopUp().getCancel().clickAndWait(verifyNoError());
        verifyThat(new EnrollmentPage(), displayed(true));
    }

    @Test(dependsOnMethods = { "doNotAcceptAgreement" }, alwaysRun = true)
    public void checkAgreeAndCancel()
    {
        EnrollmentPage enrollmentPage = new EnrollmentPage();
        enrollmentPage.clickSubmitAndSkipWarnings(verifyNoError());

        PrivacyConfirmationPopUp privacyConfirmationPopUp = new PrivacyConfirmationPopUp();
        privacyConfirmationPopUp.getAccepted().check();
        privacyConfirmationPopUp.getCancel().clickAndWait(verifyNoError());
        verifyThat(enrollmentPage, displayed(true));
    }
}
