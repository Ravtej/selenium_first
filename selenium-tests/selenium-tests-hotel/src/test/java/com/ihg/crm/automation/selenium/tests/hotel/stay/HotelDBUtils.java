package com.ihg.crm.automation.selenium.tests.hotel.stay;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.testng.SkipException;

import com.ihg.automation.selenium.common.stay.Stay;
import com.ihg.automation.selenium.common.types.Currency;

public class HotelDBUtils
{
    private static final Logger logger = LoggerFactory.getLogger(HotelDBUtils.class);

    private JdbcTemplate jdbcTemplate;

    public HotelDBUtils(JdbcTemplate jdbcTemplate)
    {
        this.jdbcTemplate = jdbcTemplate;
    }

    private final static String SELECT_GNR_HOTEL = "SELECT * FROM"
            + " (SELECT f.HTL_CD, h.USED_CURR_CD, TO_CHAR(f.CK_OUT_DT, 'DDMonYY') AS CK_OUT_DT"//
            + " FROM STY.STY_FOL f JOIN GROWTH_PRDHTL.HOTEL h ON f.HTL_CD = h.HLDX_CD"//
            + " WHERE f.LYTY_SRC_IND = 'Y' AND f.SRC_KEY = 4 AND f.LYTY_STAT_CD = 'PENDING'"//
            + " AND (h.DCO_EFF_DT IS NULL OR h.DCO_EFF_DT > SYSDATE) AND f.XACT_DT > SYSDATE" //
            + " ORDER BY f.CK_OUT_DT ASC) WHERE ROWNUM < 2";

    public Stay getGnrHotel(Stay stay)
    {
        Map<String, Object> hotelDataMap;
        try
        {
            hotelDataMap = jdbcTemplate.queryForMap(SELECT_GNR_HOTEL);
        }
        catch (DataAccessException e)
        {
            throw new SkipException("Hotel Data was not received");
        }

        String hotelCurrency = hotelDataMap.get("USED_CURR_CD").toString();
        try
        {
            stay.setHotelCurrency(Currency.valueOf(hotelCurrency));
        }
        catch (IllegalArgumentException e)
        {
            logger.error("{} does not exist in Currency enum", hotelCurrency);
        }
        stay.setCheckOut(hotelDataMap.get("CK_OUT_DT").toString());
        stay.setHotelCode(hotelDataMap.get("HTL_CD").toString());
        return stay;
    }
}
