package com.ihg.crm.automation.selenium.tests.hotel.enrollment.natives;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.components.NativeLanguage.SIMPLIFIED_CHINESE;
import static com.ihg.automation.selenium.common.components.NativeLanguage.TRADITIONAL_CHINESE;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static org.hamcrest.core.StringContains.containsString;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.components.Country;
import com.ihg.automation.selenium.common.contact.Address;
import com.ihg.automation.selenium.common.member.GuestAddress;
import com.ihg.automation.selenium.common.member.GuestName;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.name.PersonNameFields;
import com.ihg.automation.selenium.common.types.address.AddressType;
import com.ihg.automation.selenium.common.types.address.RegionTaiwanNative;
import com.ihg.automation.selenium.common.types.address.RegionTaiwanRoman;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.gwt.components.Mode;
import com.ihg.automation.selenium.hotel.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.hotel.pages.personal.AddressContact;
import com.ihg.automation.selenium.hotel.pages.personal.PersonalInfoPage;
import com.ihg.crm.automation.selenium.tests.hotel.LoginLogout;

public class EnrollmentWithMemberFromTaiwanTest extends LoginLogout
{
    private EnrollmentPage enrollmentPage = new EnrollmentPage();
    private GuestName name = new GuestName();
    private GuestAddress address = new GuestAddress();
    private Member member = new Member();

    @BeforeClass
    public void before()
    {
        GuestName nativeName = new GuestName();
        nativeName.setGiven("名字");
        nativeName.setSurname("姓");
        nativeName.setNativeLanguage(SIMPLIFIED_CHINESE);

        name.setGiven("ming zi");
        name.setSurname("xing");
        name.setNativeName(nativeName);

        GuestAddress nativeAddress = new GuestAddress();
        nativeAddress.setLine1("东直门北中街4");
        nativeAddress.setLocality1("北京市");
        nativeAddress.setRegion1(RegionTaiwanNative.TPE);

        address.setCountryCode(Country.TW);
        address.setType(AddressType.RESIDENCE);
        address.setLocality1("bei jing shi");
        address.setLine1("dong zhi men bei zhong jie 4");
        address.setRegion1(RegionTaiwanRoman.TAI_BEI_SHI);

        address.setNativeAddress(nativeAddress);

        login();
    }

    @Test
    public void populateNameNativeFields()
    {
        enrollmentPage.goTo();
        enrollmentPage.getPrograms().selectProgram(Program.RC);
        enrollmentPage.getCustomerInfo().getResidenceCountry().selectByCode(Country.TW);

        PersonNameFields nameFields = enrollmentPage.getCustomerInfo().getName();
        nameFields.setConfiguration(Country.TW);
        nameFields.populateNativeFields(name);
        nameFields.verify(name, Mode.EDIT);
    }

    @Test(dependsOnMethods = { "populateNameNativeFields" })
    public void populateAddressNativeFields()
    {
        Address addressFields = enrollmentPage.getAddress();
        verifyThat(addressFields, hasText(containsString(TRADITIONAL_CHINESE)));
        addressFields.populateNativeFields(address);
        addressFields.verify(address, Mode.EDIT);

        addressFields.getClearNativeAddress().clickAndWait();

        addressFields.populateNativeFields(address);
    }

    @Test(dependsOnMethods = { "populateAddressNativeFields" })
    public void completeEnrollment()
    {
        enrollmentPage.getYourEmployeeID().type(helper.getUser().getEmployeeId());
        enrollmentPage.completeEnrollWithDuplicatePopUp(member);
    }

    @Test(dependsOnMethods = { "completeEnrollment" })
    public void verifyAddressDetailsEditMode()
    {
        AddressContact contact = new PersonalInfoPage().getAddressList().getContact();
        contact.clickEdit();
        contact.getBaseContact().verifyNativeFields(address, TRADITIONAL_CHINESE, Mode.EDIT);
    }
}
