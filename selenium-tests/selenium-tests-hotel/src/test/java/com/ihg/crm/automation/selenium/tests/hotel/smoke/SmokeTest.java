package com.ihg.crm.automation.selenium.tests.hotel.smoke;

import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyNoError;
import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifySuccess;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyNoErrors;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.communication.CommunicationPreferencesConstants.OPT_IN;
import static com.ihg.automation.selenium.common.types.WrittenLanguage.ENGLISH;
import static com.ihg.automation.selenium.common.types.WrittenLanguage.SPANISH;
import static com.ihg.automation.selenium.common.types.program.Program.AMB;
import static com.ihg.automation.selenium.common.types.program.Program.BR;
import static com.ihg.automation.selenium.common.types.program.Program.EMP;
import static com.ihg.automation.selenium.common.types.program.Program.RC;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static com.ihg.automation.selenium.matchers.component.ViewableMatcher.hasTextInView;
import static org.hamcrest.Matchers.greaterThan;

import org.joda.time.LocalDate;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.ihg.automation.common.DateUtils;
import com.ihg.automation.common.RandomUtils;
import com.ihg.automation.selenium.common.RewardClubGridRow;
import com.ihg.automation.selenium.common.communication.ContactPermissionItem;
import com.ihg.automation.selenium.common.communication.ContactPermissionItems;
import com.ihg.automation.selenium.common.communication.MarketingPreferences;
import com.ihg.automation.selenium.common.member.AmbassadorUtils;
import com.ihg.automation.selenium.common.member.GuestPhone;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.pages.TabCustomContainerBase;
import com.ihg.automation.selenium.common.types.program.RewardClubLevel;
import com.ihg.automation.selenium.gwt.components.Mode;
import com.ihg.automation.selenium.hotel.pages.LeftPanel;
import com.ihg.automation.selenium.hotel.pages.communication.CommunicationPreferences;
import com.ihg.automation.selenium.hotel.pages.communication.CommunicationPreferencesPage;
import com.ihg.automation.selenium.hotel.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.hotel.pages.personal.HotelPhoneContactList;
import com.ihg.automation.selenium.hotel.pages.personal.HotelSmsContactList;
import com.ihg.automation.selenium.hotel.pages.personal.PersonalInfoPage;
import com.ihg.automation.selenium.hotel.pages.programs.EmployeeProgramPage;
import com.ihg.automation.selenium.hotel.pages.programs.amb.AmbassadorPage;
import com.ihg.automation.selenium.hotel.pages.programs.br.BusinessRewardsPage;
import com.ihg.automation.selenium.hotel.pages.programs.rc.RewardClubPage;
import com.ihg.crm.automation.selenium.tests.hotel.LoginLogout;

public class SmokeTest extends LoginLogout
{
    private Member member = new Member();
    private PersonalInfoPage personalInfoPage = new PersonalInfoPage();
    private static final String HOTEL = "SHGHA";
    private static final String CONTACT_SUCCESS_SAVE = "Contact info has been successfully saved.";

    @BeforeClass
    public void beforeClass()
    {
        member.addProgram(RC, AMB, BR, EMP);
        member.setPersonalInfo(MemberPopulateHelper.getSimplePersonalInfo());
        member.addAddress(MemberPopulateHelper.getUnitedStatesAddress());
        member.addEmail(MemberPopulateHelper.getRandomEmail());
        member.addPhone(MemberPopulateHelper.getRandomPhone());
        member.getPersonalInfo().setBirthDate(new LocalDate());
        member.setAmbassadorAmount(AmbassadorUtils.PURCHASE_AMBASSADOR_HOTEL_$200);

        login(HOTEL);
    }

    @Test(priority = 1)
    public void enrollToPrograms()
    {
        new EnrollmentPage().enroll(member, helper);
    }

    @Test(priority = 20)
    public void validatePersonalInformation()
    {
        personalInfoPage.goTo();

        personalInfoPage.getCustomerInfo().verify(member, Mode.VIEW);
        personalInfoPage.getAddressList().getExpandedContact().verify(member.getPreferredAddress(), Mode.VIEW);
        personalInfoPage.getPhoneList().getExpandedContact().verify(member.getPreferredPhone(), Mode.VIEW);
        personalInfoPage.getEmailList().getExpandedContact().verify(member.getPreferredEmail(), Mode.VIEW);
    }

    @Test(priority = 25)
    public void addSecondPhone()
    {
        GuestPhone secondPhone = MemberPopulateHelper.getRandomPhone();
        secondPhone.setPreferred(false);

        HotelPhoneContactList phoneContactList = personalInfoPage.getPhoneList();
        phoneContactList.add(secondPhone, verifySuccess(CONTACT_SUCCESS_SAVE));
        phoneContactList.getExpandedContact(2).verify(secondPhone, Mode.VIEW);
    }

    @Test(priority = 30)
    public void addThirdPhone()
    {
        GuestPhone thirdPhone = MemberPopulateHelper.getRandomPhone();
        thirdPhone.setPreferred(false);

        HotelPhoneContactList phoneContactList = personalInfoPage.getPhoneList();
        phoneContactList.add(thirdPhone, verifySuccess(CONTACT_SUCCESS_SAVE));
        phoneContactList.getExpandedContact(3).verify(thirdPhone, Mode.VIEW);
    }

    @Test(priority = 30)
    public void addSms()
    {
        GuestPhone smsPhone = new GuestPhone();
        smsPhone.setNumber(RandomUtils.getRandomNumber(7));

        HotelSmsContactList smsContactList = personalInfoPage.getSmsList();
        smsContactList.add(smsPhone, verifySuccess(CONTACT_SUCCESS_SAVE));
        smsContactList.getExpandedContact().verifyMasked(smsPhone, Mode.VIEW);
    }

    @DataProvider(name = "pageProvider")
    protected Object[][] pageProvider()
    {
        return new Object[][] { { new PersonalInfoPage() }, { new CommunicationPreferencesPage() },
                { new AmbassadorPage() }, { new RewardClubPage() }, { new EmployeeProgramPage() },
                { new BusinessRewardsPage() } };
        // TODO: Karma page
    }

    @Test(dataProvider = "pageProvider", priority = 40)
    public <T extends TabCustomContainerBase> void checkTabs(T page)
    {
        page.goTo();
        verifyNoErrors();
    }

    @Test(priority = 40)
    public void editCommPref()
    {
        CommunicationPreferencesPage communicationPreferencesPage = new CommunicationPreferencesPage();
        communicationPreferencesPage.goTo();

        CommunicationPreferences communicationPreferences = communicationPreferencesPage.getCommunicationPreferences();
        communicationPreferences.verify(ENGLISH);

        communicationPreferences.clickEdit(verifyNoError());
        communicationPreferences.getPreferredLanguage().select(SPANISH.getValue());
        communicationPreferences.clickSave(verifyNoError());
        communicationPreferences.verify(SPANISH);

        MarketingPreferences marketingPreferences = communicationPreferences.getMarketingPreferences();
        ContactPermissionItems marketingPermissionItems = marketingPreferences.getContactPermissionItems();
        int marketingItemsCount = marketingPermissionItems.getContactPermissionItemsCount();
        verifyThat(marketingPreferences, marketingItemsCount, greaterThan(0),
                "There are items in marketing preferences");
        String formattedDate = DateUtils.getFormattedDate();
        for (int index = 1; index <= marketingItemsCount; index++)
        {
            ContactPermissionItem contactPermissionItem = marketingPermissionItems.getContactPermissionItem(index);
            verifyThat(contactPermissionItem.getSubscribe(), hasTextInView(OPT_IN));
            verifyThat(contactPermissionItem.getDateUpdated(), hasText(formattedDate));
        }

        ContactPermissionItems smsPermissionItems = communicationPreferences.getSmsMobileTextingMarketingPreferences()
                .getContactPermissionItems();
        int smsItemsCount = smsPermissionItems.getContactPermissionItemsCount();
        verifyThat(smsPermissionItems, smsItemsCount, greaterThan(0), "There are items in sms preferences");
        for (int index = 1; index <= smsItemsCount; index++)
        {
            ContactPermissionItem contactPermissionItem = smsPermissionItems.getContactPermissionItem(index);
            verifyThat(contactPermissionItem.getSubscribe(), hasTextInView(OPT_IN));
            verifyThat(contactPermissionItem.getDateUpdated(), hasText(formattedDate));
        }
    }

    @Test(priority = 70)
    public void verifyTierLevel()
    {
        RewardClubGridRow leftPanel = new LeftPanel().getProgramInfoPanel().getPrograms().getRewardClubProgram();
        leftPanel.clickAndWait(verifyNoError());
        leftPanel.verify(RewardClubLevel.GOLD, "0");
    }
}
