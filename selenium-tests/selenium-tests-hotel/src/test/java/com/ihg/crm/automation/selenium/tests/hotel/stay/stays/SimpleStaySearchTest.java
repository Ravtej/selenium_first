package com.ihg.crm.automation.selenium.tests.hotel.stay.stays;

import static com.ihg.automation.common.RandomUtils.getRandomLetters;
import static com.ihg.automation.common.RandomUtils.getRandomNumber;
import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyNoError;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasAnyText;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasText;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.isSelected;
import static com.ihg.automation.selenium.matchers.component.GridSize.size;
import static org.hamcrest.Matchers.greaterThan;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.event.SearchButtonBar;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.pages.login.TopUserSection;
import com.ihg.automation.selenium.common.stay.Stay;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.gwt.components.checkbox.CheckBox;
import com.ihg.automation.selenium.gwt.components.input.Input;
import com.ihg.automation.selenium.hotel.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.stay.ResultsFilter;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.stay.lpu.LoyaltyPendingUpdatesPage;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.stay.stays.ReviewGuestStaysSearchPage;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.stay.stays.StaysGrid;
import com.ihg.crm.automation.selenium.tests.hotel.LoginLogout;
import com.ihg.crm.automation.selenium.tests.hotel.stay.HotelDBUtils;

public class SimpleStaySearchTest extends LoginLogout
{
    private Member member = new Member();
    private Stay stay = new Stay();
    private SearchButtonBar buttonBar;
    private ReviewGuestStaysSearchPage reviewGuestStaysSearchPage;
    private ResultsFilter resultsFilter;
    private ReviewGuestStaysSearchPage.LoyaltyStaySearch searchFields;
    private StaysGrid staysGrid;

    @BeforeClass
    public void beforeClass()
    {
        member.addProgram(Program.RC);
        member.addEmail(MemberPopulateHelper.getRandomEmail());
        member.addPhone(MemberPopulateHelper.getRandomPhone());
        member.addAddress(MemberPopulateHelper.getUnitedStatesAddress());
        member.setPersonalInfo(MemberPopulateHelper.getSimplePersonalInfo());

        new HotelDBUtils(jdbcTemplate).getGnrHotel(stay);
        stay.setRateCode("SRCH" + getRandomNumber(1));
        stay.setConfirmationNumber(1 + getRandomNumber(7));
        stay.setFolioNumber(2 + getRandomNumber(5));
        stay.setRoomNumber(3 + getRandomNumber(3));
        stay.setNights(5);
        stay.setCheckInDateByCheckout();
        stay.setAvgRoomRate(100.00);
        stay.setIsQualifying(true);
        stay.setEnrollingStay("No");

        login();

        member = new EnrollmentPage().enroll(member, helper, verifyNoError());

        new TopUserSection().switchHotel(stay.getHotelCode());
        new LoyaltyPendingUpdatesPage().goToByHotelOperations().createMissingStay(member, stay);

        reviewGuestStaysSearchPage = new ReviewGuestStaysSearchPage();
        reviewGuestStaysSearchPage.getTab().goTo();

        searchFields = reviewGuestStaysSearchPage.getSearchFields();
        resultsFilter = reviewGuestStaysSearchPage.getResultsFilter();
        resultsFilter.expand();
        buttonBar = reviewGuestStaysSearchPage.getButtonBar();

        staysGrid = reviewGuestStaysSearchPage.getStayGrid();
    }

    @Test
    public void verifyReviewGuestStaysSearchFilterElements()
    {
        verifyThat(searchFields.getHotelSearch(), hasText(stay.getHotelCode()));
        verifyThat(searchFields.getServiceCenterCreatedStay(), isSelected(true));
        verifyThat(searchFields.getSystemStay(), isSelected(true));
        verifyThat(searchFields.getEnrollingStay(), isSelected(false));
        verifyThat(searchFields.getCheckIn(), isSelected(false));
        verifyThat(searchFields.getCheckOut(), isSelected(true));
        verifyThat(searchFields.getIHGRewardsClubStay(), isSelected(false));
        verifyThat(searchFields.getFromDate(), hasAnyText());
        verifyThat(searchFields.getToDate(), hasAnyText());
    }

    @DataProvider(name = "validSearchCriteria")
    private Object[][] validSearchCriteria()
    {
        return new Object[][] { { resultsFilter.getGuestName(), member.getName().getFullName() },
                { resultsFilter.getConfirmationNumber(), stay.getConfirmationNumber() },
                { resultsFilter.getRateCategory(), stay.getRateCode() },
                { resultsFilter.getFolioNumber(), stay.getFolioNumber() },
                { resultsFilter.getIataNumber(), stay.getIataCode() },
                { resultsFilter.getCorporateId(), stay.getCorporateAccountNumber() },
                { resultsFilter.getRoomNumber(), stay.getRoomNumber() } };
    }

    @Test(dataProvider = "validSearchCriteria", dependsOnMethods = { "verifyReviewGuestStaysSearchFilterElements" })
    public void searchByValidCriteria(Input input, String text)
    {
        searchStay(input, text);

        verifyThat(staysGrid, size(greaterThan(0)));
        staysGrid.getRow(1).verify(member, stay);
    }

    @DataProvider(name = "invalidSearchCriteria")
    private Object[][] invalidSearchCriteria()
    {
        return new Object[][] { { resultsFilter.getGuestName(),  getRandomLetters(15)},
                { resultsFilter.getConfirmationNumber(), getRandomNumber(8) },
                { resultsFilter.getRateCategory(), getRandomLetters(5) },
                { resultsFilter.getFolioNumber(), "8192x423" },
                { resultsFilter.getIataNumber(), getRandomNumber(11) },
                { resultsFilter.getCorporateId(), "7124d2621" },
                { resultsFilter.getRoomNumber(), "7x67" } };
    }

    @Test(dataProvider = "invalidSearchCriteria", dependsOnMethods = { "searchByValidCriteria" })
    public void searchByInvalidCriteria(Input input, String text)
    {
        searchStay(input, text);
        verifyThat(staysGrid, size(0));
    }

    @DataProvider(name = "checkboxValidSearch")
    private Object[][] checkboxValidSearch()
    {
        return new Object[][] { { searchFields.getSystemStay() }, //
                { searchFields.getCheckIn() }, //
                { searchFields.getCheckOut() }, //
                { searchFields.getIHGRewardsClubStay() } };
    }

    @Test(dataProvider = "checkboxValidSearch", dependsOnMethods = { "searchByInvalidCriteria" })
    public void searchStayByValidCriteriaInCheckboxes(CheckBox checkBox)
    {
        searchStayUsingCheckboxCriteria(checkBox);

        verifyThat(staysGrid, size(greaterThan(0)));
        staysGrid.getRow(1).verify(member, stay);
    }

    @DataProvider(name = "checkboxNotValidSearch")
    private Object[][] checkboxNotValidSearch()
    {
        return new Object[][] { { searchFields.getServiceCenterCreatedStay() }, //
                { searchFields.getEnrollingStay() } };
    }

    @Test(dataProvider = "checkboxNotValidSearch", dependsOnMethods = { "searchStayByValidCriteriaInCheckboxes" })
    public void tryToSearchStayWithInvalidCriteria(CheckBox checkBox)
    {
        searchStayUsingCheckboxCriteria(checkBox);

        verifyThat(staysGrid, size(0));
    }

    private void searchStayUsingCheckboxCriteria(CheckBox checkBox)
    {
        buttonBar.clickClearCriteria();

        searchFields.getServiceCenterCreatedStay().uncheck();
        searchFields.getSystemStay().uncheck();
        searchFields.getCheckOut().uncheck();
        searchFields.getToDate().type(stay.getCheckOut());

        checkBox.check();

        resultsFilter.getMemberNumber().type(member.getRCProgramId());

        buttonBar.clickSearch();
    }

    private void searchStay(Input input, String text)
    {
        buttonBar.clickClearCriteria();

        resultsFilter.getMemberNumber().type(member.getRCProgramId());
        searchFields.getToDate().type(stay.getCheckOut());
        input.type(text);

        buttonBar.clickSearch();
    }
}
