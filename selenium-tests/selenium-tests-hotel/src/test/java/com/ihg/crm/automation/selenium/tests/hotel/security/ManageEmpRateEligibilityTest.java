package com.ihg.crm.automation.selenium.tests.hotel.security;

import static com.ihg.automation.common.DateUtils.DATE_FORMAT;
import static com.ihg.automation.common.DateUtils.getFormattedDate;
import static com.ihg.automation.common.DateUtils.getMonthsForward;
import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyNoError;
import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifySuccess;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.hotel.pages.Role.HOTEL_SECURITY;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.enabled;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasDefault;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasText;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.isDisplayedAndEnabled;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.isHighlightedAsInvalid;
import static com.ihg.automation.selenium.matchers.component.GridSize.size;
import static com.ihg.automation.selenium.matchers.component.HasTipText.hasWarningTipText;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.core.AllOf.allOf;

import java.util.Map;

import org.joda.time.LocalDate;
import org.springframework.beans.factory.annotation.Value;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.atp.schema.crm.common.loyalty.loyalty.datatypes.v2.SourceType;
import com.ihg.atp.schema.crm.guest.guest.datatypes.v2.GuestIdentityType;
import com.ihg.atp.schema.crm.guest.guest.member.v2.ProgramMembershipKey;
import com.ihg.atp.schema.crm.guest.guest.servicetypes.v2.UpdateEmployeeRateEligibilityRequestType;
import com.ihg.automation.common.DateUtils;
import com.ihg.automation.common.GregorianCalendarUtils;
import com.ihg.automation.selenium.common.Channel;
import com.ihg.automation.selenium.gwt.components.Button;
import com.ihg.automation.selenium.gwt.components.input.Input;
import com.ihg.automation.selenium.hotel.pages.LeftPanel;
import com.ihg.automation.selenium.hotel.pages.security.manageemprateeligibility.EmployeeRateEligibilityAddPopUp;
import com.ihg.automation.selenium.hotel.pages.security.manageemprateeligibility.EmployeeRateEligibilityEditPopUp;
import com.ihg.automation.selenium.hotel.pages.security.manageemprateeligibility.EmployeeRateEligibilityRow;
import com.ihg.automation.selenium.hotel.pages.security.manageemprateeligibility.ManageEmpRateEligibilityGridRow;
import com.ihg.automation.selenium.hotel.pages.security.manageemprateeligibility.ManageEmpRateEligibilityPage;
import com.ihg.crm.automation.selenium.tests.hotel.LoginLogout;

public class ManageEmpRateEligibilityTest extends LoginLogout
{
    private static final String ELIGIBILITY_DOES_NOT_EXIST = "Member Eligibility does not exist for your hotel";
    private static final String HOTEL = "ATLCP";
    public static final LocalDate EXPIRES_DATE = DateUtils.now().minusDays(1);

    private static final String MEMBERS_NAME = "SELECT nm.GIVEN_NM, nm.SURNAME_NM " //
            + "FROM  DGST.GST_NM nm WHERE nm.DGST_MSTR_KEY IN (SELECT DGST_MSTR_KEY " //
            + "FROM DGST.MBRSHP WHERE MBRSHP_ID = '%1$s') AND nm.DFLT_PRF_IND = 'Y'";

    private static final String ELIGIBILITY_DATA = "SELECT e.ELIG_IND, TO_CHAR(e.EXPR_DT, 'DDMonYY') AS EXPR_DT, " //
            + "e.SRC_LOC_CD, TO_CHAR(e.LST_UPDT_TS, 'DDMonYY') AS LST_UPDT_TS " //
            + "FROM DGST.GST_EMP_RATE_ELIG e " //
            + "WHERE e.DGST_MSTR_KEY IN (SELECT DGST_MSTR_KEY " //
            + "FROM DGST.MBRSHP WHERE MBRSHP_ID = '%1$s') AND e.SRC_LOC_CD = '%2$s'";

    private ManageEmpRateEligibilityPage manageEmpRateEligibilityPage = new ManageEmpRateEligibilityPage();
    private EmployeeRateEligibilityRow employeeRateEligibilityRow = new EmployeeRateEligibilityRow();
    private ManageEmpRateEligibilityPage.ManageEmpRateEligibilitySearch searchFields;
    private ManageEmpRateEligibilityPage.EmployeeSearchButtonBar searchButtonBar;
    private EmployeeRateEligibilityAddPopUp employeeRateEligibilityAddPopUp;
    private EmployeeRateEligibilityEditPopUp employeeRateEligibilityEditPopUp;

    @Value("${emp.eligibility.member}")
    private String memberId;

    @BeforeClass
    public void beforeClass()
    {
        Map<String, Object> map = jdbcTemplate.queryForMap(String.format(MEMBERS_NAME, memberId));
        employeeRateEligibilityRow.setMemberId(memberId);
        employeeRateEligibilityRow.setFirstName(map.get("GIVEN_NM").toString());
        employeeRateEligibilityRow.setLastName(map.get("SURNAME_NM").toString());

        login(HOTEL, HOTEL_SECURITY);
        new LeftPanel().getManageEmpRateEligibility().clickAndWait(verifyNoError());
    }

    @Test(priority = 1)
    public void verifyDefaultManageEmpEligibilityComponents()
    {
        searchButtonBar = new ManageEmpRateEligibilityPage().getButtonBar();
        searchButtonBar.clickClearCriteria();
        searchButtonBar.clickSearch();

        searchFields = manageEmpRateEligibilityPage.getSearchFields();
        verifyThat(searchFields.getHotel(), hasText(HOTEL));
        verifyThat(searchFields.getDateUpdatedFrom(), hasDefault());
        verifyThat(searchFields.getDateUpdatedTo(), hasDefault());
        verifyThat(searchFields.getMemberNumber(), hasDefault());

        verifyThat(searchButtonBar.getAddNew(), isDisplayedAndEnabled(true));
        verifyThat(searchButtonBar.getClearCriteria(), isDisplayedAndEnabled(true));
        verifyThat(searchButtonBar.getSearch(), isDisplayedAndEnabled(true));
        verifyThat(manageEmpRateEligibilityPage.getEmployeeRateEligibilityGrid(), size(greaterThan(0)));
    }

    @Test(priority = 3)
    public void tryToSearchNotExistingMember()
    {
        manageEmpRateEligibilityPage.searchMember(employeeRateEligibilityRow);

        Input memberNumber = searchFields.getMemberNumber();
        verifyThat(memberNumber, allOf(isHighlightedAsInvalid(true), hasWarningTipText(ELIGIBILITY_DOES_NOT_EXIST)));
    }

    @Test(priority = 5)
    public void tryToAddEligibilityToNotExistingRcMember()
    {
        searchButtonBar.getAddNew().clickAndWait();

        employeeRateEligibilityAddPopUp = new EmployeeRateEligibilityAddPopUp();
        Input memberNumber = employeeRateEligibilityAddPopUp.getMemberNumber();
        verifyThat(memberNumber, hasDefault());
        verifyThat(employeeRateEligibilityAddPopUp.getHotel(), hasText(HOTEL));

        memberNumber.type("123456789");
        employeeRateEligibilityAddPopUp.clickSave();
        verifyThat(memberNumber, allOf(isHighlightedAsInvalid(true), hasWarningTipText("Member Number is Invalid")));
    }

    @Test(priority = 10)
    public void addNewRateEligibility()
    {
        employeeRateEligibilityAddPopUp.getMemberNumber().typeAndWait(memberId);
        verifyThat(employeeRateEligibilityAddPopUp.getMemberName(),
                hasText(employeeRateEligibilityRow.getNameAndSurname()));
        employeeRateEligibilityAddPopUp.clickSave(verifyNoError());
    }

    @Test(priority = 15)
    public void verifyPopUp()
    {
        manageEmpRateEligibilityPage.searchMember(employeeRateEligibilityRow);

        employeeRateEligibilityRow = getMembersEligibilityData();

        ManageEmpRateEligibilityGridRow row = manageEmpRateEligibilityPage.getEmployeeRateEligibilityGrid()
                .getRow(memberId);
        row.verify(employeeRateEligibilityRow);

        employeeRateEligibilityEditPopUp = row.openEmployeeRateEligibilityEditPopUp();
        employeeRateEligibilityEditPopUp.verify(employeeRateEligibilityRow, true, HOTEL);
        employeeRateEligibilityEditPopUp.clickClose();
    }

    @Test(priority = 20)
    public void extendExpirationDate()
    {
        guestClient.updateEmployeeRateEligibility(getUpdateEmployeeRateEligibilityRequestType(memberId));

        searchFields = manageEmpRateEligibilityPage.getSearchFields();
        manageEmpRateEligibilityPage.searchMember(employeeRateEligibilityRow);

        ManageEmpRateEligibilityGridRow row = manageEmpRateEligibilityPage.getEmployeeRateEligibilityGrid()
                .getRow(memberId);

        employeeRateEligibilityEditPopUp = row.openEmployeeRateEligibilityEditPopUp();
        employeeRateEligibilityRow.setExpirationDate(getFormattedDate(-1));
        employeeRateEligibilityEditPopUp.verify(employeeRateEligibilityRow, true, HOTEL);
        employeeRateEligibilityEditPopUp.getExtend().clickAndWait(verifySuccess("Employee Rate Eligibility extended"));

        manageEmpRateEligibilityPage.searchMember(employeeRateEligibilityRow);

        row = manageEmpRateEligibilityPage.getEmployeeRateEligibilityGrid().getRow(memberId);
        employeeRateEligibilityRow.setExpirationDate(getMonthsForward(6, DATE_FORMAT));
        row.verify(employeeRateEligibilityRow);
    }

    @Test(priority = 25)
    public void removeRateEligibility()
    {
        employeeRateEligibilityEditPopUp = manageEmpRateEligibilityPage.getEmployeeRateEligibilityGrid()
                .getRow(memberId).openEmployeeRateEligibilityEditPopUp();

        Button save = employeeRateEligibilityEditPopUp.getSave();
        verifyThat(save, enabled(false));
        Button extend = employeeRateEligibilityEditPopUp.getExtend();
        verifyThat(extend, enabled(true));

        employeeRateEligibilityEditPopUp.getEligible().uncheck();
        verifyThat(save, enabled(true));
        verifyThat(extend, enabled(false));

        employeeRateEligibilityEditPopUp.clickSave(verifyNoError(), verifySuccess("Employee Rate Eligibility removed"));

        manageEmpRateEligibilityPage.searchMember(employeeRateEligibilityRow);
        verifyThat(searchFields.getMemberNumber(),
                allOf(isHighlightedAsInvalid(true), hasWarningTipText(ELIGIBILITY_DOES_NOT_EXIST)));
    }

    private EmployeeRateEligibilityRow getMembersEligibilityData()
    {
        Map<String, Object> map = jdbcTemplate.queryForMap(String.format(ELIGIBILITY_DATA, memberId, HOTEL));
        employeeRateEligibilityRow.setDateUpdated(map.get("LST_UPDT_TS").toString());
        employeeRateEligibilityRow.setExpirationDate(map.get("EXPR_DT").toString());
        return employeeRateEligibilityRow;
    }

    private UpdateEmployeeRateEligibilityRequestType getUpdateEmployeeRateEligibilityRequestType(String memberId)
    {
        com.ihg.atp.schema.crm.common.loyalty.loyalty.datatypes.v2.ActivitySourceType activitySourceType = new com.ihg.atp.schema.crm.common.loyalty.loyalty.datatypes.v2.ActivitySourceType();
        activitySourceType.setChannel(Channel.MHUI);
        activitySourceType.setLocationCode(HOTEL);
        activitySourceType.setCode(SourceType.HOTEL);

        UpdateEmployeeRateEligibilityRequestType updateEmployeeRateEligibilityRequestType = new UpdateEmployeeRateEligibilityRequestType();
        updateEmployeeRateEligibilityRequestType.setSource(activitySourceType);

        GuestIdentityType guestIdentityType = new GuestIdentityType();

        ProgramMembershipKey programMembershipKey = new ProgramMembershipKey();
        programMembershipKey.setMemberId(memberId);
        programMembershipKey.setCode("PC");
        guestIdentityType.setProgramMembershipKey(programMembershipKey);

        updateEmployeeRateEligibilityRequestType.setGuestIdentity(guestIdentityType);

        UpdateEmployeeRateEligibilityRequestType.Expiration expiration = new UpdateEmployeeRateEligibilityRequestType.Expiration();
        expiration.setExpires(true);

        expiration.setExpirationDate(GregorianCalendarUtils.convert(EXPIRES_DATE));

        updateEmployeeRateEligibilityRequestType.setExpiration(expiration);
        updateEmployeeRateEligibilityRequestType.setEligible(true);

        return updateEmployeeRateEligibilityRequestType;
    }
}
