package com.ihg.crm.automation.selenium.tests.hotel.stay.lpu;

import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyNoError;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.components.IndicatorSelect.NO;
import static com.ihg.automation.selenium.common.components.IndicatorSelect.YES;
import static com.ihg.automation.selenium.matchers.component.HasDefaultText.hasDefault;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static com.ihg.automation.selenium.matchers.component.ViewableMatcher.hasTextInView;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.pages.login.TopUserSection;
import com.ihg.automation.selenium.common.stay.Revenues;
import com.ihg.automation.selenium.common.stay.Stay;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.hotel.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.stay.lpu.LoyaltyPendingUpdatesGridRow;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.stay.lpu.LoyaltyPendingUpdatesPage;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.stay.lpu.LpuGridRowContainer;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.stay.lpu.missingstay.RevenueDetailsEdit;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.stay.lpu.missingstay.RevenueExtended;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.stay.lpu.missingstay.StayAdjustPopUp;
import com.ihg.crm.automation.selenium.tests.hotel.LoginLogout;
import com.ihg.crm.automation.selenium.tests.hotel.stay.HotelDBUtils;

public class CompleteAdjustStayTest extends LoginLogout
{
    private Member member = new Member();
    private Stay createStay = new Stay();
    private Stay adjustStay;

    @BeforeClass
    public void beforeClass()
    {
        member.addProgram(Program.RC);
        member.addEmail(MemberPopulateHelper.getRandomEmail());
        member.addPhone(MemberPopulateHelper.getRandomPhone());
        member.addAddress(MemberPopulateHelper.getUnitedStatesAddress());
        member.setPersonalInfo(MemberPopulateHelper.getSimplePersonalInfo());

        new HotelDBUtils(jdbcTemplate).getGnrHotel(createStay);
        createStay.setNights(5);
        createStay.setCheckInDateByCheckout();
        createStay.setRateCode("TEST");
        createStay.setAvgRoomRate(100.00);
        createStay.setIsQualifying(true);

        Revenues revenues = createStay.getRevenues();
        revenues.getOverallTotal().setAmount(1300.00);
        revenues.getRoom().setAmount(500.00);
        revenues.getFood().setAmount(100.00);
        revenues.getBeverage().setAmount(100.00);
        revenues.getPhone().setAmount(100.00);
        revenues.getMeeting().setAmount(100.00);
        revenues.getMandatoryLoyalty().setAmount(100.00);
        revenues.getOtherLoyalty().setAmount(100.00);
        revenues.getNoLoyalty().setAmount(100.00);
        revenues.getTax().setAmount(100.00);
        revenues.getTotalNonQualifying().setAmount(300.00);
        revenues.getTotalQualifying().setAmount(1000.00);

        adjustStay = createStay.clone();
        adjustStay.setAvgRoomRate(200.00);

        Revenues adjustRevenues = adjustStay.getRevenues();
        adjustRevenues.getOverallTotal().setAmount(2400.00);
        adjustRevenues.getRoom().setAmount(1000.00);
        adjustRevenues.getFood().setAmount(200.00);
        adjustRevenues.getBeverage().setAmount(200.00);
        adjustRevenues.getPhone().setAmount(200.00);
        adjustRevenues.getMeeting().setAmount(200.00);
        adjustRevenues.getMandatoryLoyalty().setAmount(200.00);
        adjustRevenues.getOtherLoyalty().setAmount(200.00);
        adjustRevenues.getTotalNonQualifying().setAmount(400.00);
        adjustRevenues.getTotalQualifying().setAmount(2000.00);

        login();

        member = new EnrollmentPage().enroll(member, helper, verifyNoError());

        new TopUserSection().switchHotel(createStay.getHotelCode());
        new LoyaltyPendingUpdatesPage().goToByHotelOperations().createMissingStay(member, createStay);
    }

    @Test
    public void verifyCreatedRevenuesAndAdjust()
    {
        LpuGridRowContainer gridRowContainer = new LoyaltyPendingUpdatesPage().getLoyaltyPendingUpdatesGrid().getRow(1)
                .expand(LpuGridRowContainer.class);
        gridRowContainer.getAdjustRevenueDetails().clickAndWait(verifyNoError());

        StayAdjustPopUp stayAdjustPopUp = new StayAdjustPopUp();
        RevenueDetailsEdit detailsEdit = stayAdjustPopUp.getAdjustRevenueDetailsTab().getRevenueDetails();
        detailsEdit.verifyCreateAmount(createStay);
        detailsEdit.populateAdjustRevenues(adjustStay);
        stayAdjustPopUp.clickSave(verifyNoError());
    }

    @Test(dependsOnMethods = { "verifyCreatedRevenuesAndAdjust" })
    public void verifyAdjustedStayView()
    {
        LoyaltyPendingUpdatesGridRow row = new LoyaltyPendingUpdatesPage().getLoyaltyPendingUpdatesGrid().getRow(1);
        row.verify(member, adjustStay);

        LpuGridRowContainer gridRowContainer = row.expand(LpuGridRowContainer.class);
        gridRowContainer.verify(adjustStay, member);
        gridRowContainer.getAdjustRevenueDetails().clickAndWait(verifyNoError());
    }

    @Test(dependsOnMethods = { "verifyAdjustedStayView" })
    public void verifyStayInAdjustMode()
    {
        new StayAdjustPopUp().getAdjustRevenueDetailsTab().getRevenueDetails().verifyCreateAndAdjustedAmount(createStay,
                adjustStay);
    }

    @Test(dependsOnMethods = { "verifyStayInAdjustMode" })
    public void verifyFlagsInAdjustMode()
    {
        RevenueDetailsEdit detailsEdit = new StayAdjustPopUp().getAdjustRevenueDetailsTab().getRevenueDetails();
        RevenueExtended avgRoomRate = detailsEdit.getAvgRoomRate();
        verifyThat(avgRoomRate.getQualifying(), hasText(YES));
        verifyThat(detailsEdit.getFood().getQualifying(), hasTextInView(YES));
        verifyThat(detailsEdit.getBeverage().getQualifying(), hasTextInView(YES));
        verifyThat(detailsEdit.getPhone().getQualifying(), hasTextInView(YES));

        RevenueExtended meeting = detailsEdit.getMeeting();
        verifyThat(meeting.getQualifying(), hasText(NO));
        verifyThat(detailsEdit.getMandatoryRevenue().getQualifying(), hasTextInView(YES));
        verifyThat(detailsEdit.getOtherRevenue().getQualifying(), hasTextInView(YES));

        verifyThat(avgRoomRate.getFlagChangeReason(), hasDefault());
        verifyThat(meeting.getFlagChangeReason(), hasDefault());
    }
}
