package com.ihg.crm.automation.selenium.tests.hotel.search;

import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyError;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasText;
import static com.ihg.automation.selenium.matchers.component.GridSize.size;
import static com.ihg.automation.selenium.matchers.component.IsDisplayedAndEnabled.isDisplayedAndEnabled;
import static com.ihg.automation.selenium.matchers.text.EntityMatcherFactory.isValue;
import static org.hamcrest.Matchers.greaterThan;

import org.springframework.beans.factory.annotation.Value;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.ihg.automation.common.RandomUtils;
import com.ihg.automation.selenium.common.components.Country;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.hotel.pages.LeftPanel;
import com.ihg.automation.selenium.hotel.pages.personal.CustomerInfoFields;
import com.ihg.automation.selenium.hotel.pages.personal.PersonalInfoPage;
import com.ihg.automation.selenium.hotel.pages.search.CustomerSearch;
import com.ihg.automation.selenium.hotel.pages.search.GuestSearch;
import com.ihg.automation.selenium.hotel.pages.search.GuestSearchByNumber;
import com.ihg.automation.selenium.hotel.pages.search.GuestSearchResultsGrid;
import com.ihg.crm.automation.selenium.tests.hotel.LoginLogout;

public class AdvancedGuestSearchTest extends LoginLogout
{
    private GuestSearch guestSearch;

    private static final String ERROR_INVALID_MEMBER_ID = "Membership Number invalid";
    private static final String ERROR_MORE_INFO = "Sorry, but the search you are performing requires that you "
            + "enter more information. Please review the form, add additional information, and then try again.";

    @Value("${unique.search.memberId}")
    private String VALID_MEMBERSHIP_ID;

    @Value("${unique.search.given}")
    private String UNIQUE_NAME;

    @Value("${unique.search.surname}")
    private String UNIQUE_SURNAME;

    @Value("${unique.search.state}")
    private String UNIQUE_STATE;

    @BeforeClass
    public void before()
    {
        login();
    }

    @Test(priority = 1)
    public void verifySearchFields()
    {
        verifyWelcome();

        GuestSearchByNumber guestSearchByNumber = new GuestSearchByNumber();
        verifyThat(guestSearchByNumber.getAdvancedSearch(), isDisplayedAndEnabled(true));
        guestSearchByNumber.getAdvancedSearch().clickAndWait();

        verifyLogos();

        guestSearch = new GuestSearch();
        CustomerSearch customerSearch = guestSearch.getCustomerSearch();
        verifyThat(customerSearch.getMemberNumber(), displayed(true));
        verifyThat(customerSearch.getNumber(), displayed(true));
        verifyThat(customerSearch.getLastName(), displayed(true));
        verifyThat(customerSearch.getFirstName(), displayed(true));
        verifyThat(customerSearch.getStreetAddress(), displayed(true));
        verifyThat(customerSearch.getCity(), displayed(true));
        verifyThat(customerSearch.getZip(), displayed(true));
        verifyThat(customerSearch.getCountry(), displayed(true));
        verifyThat(customerSearch.getPhone(), displayed(true));
        verifyThat(customerSearch.getEmail(), displayed(true));
        verifyThat(customerSearch.getProgram(), displayed(true));
    }

    @DataProvider(name = "getInvalidNames")
    public Object[][] getInvalidNames()
    {
        return new Object[][] { { "Test", "Test" },
                { "1229baa50748245db216a6892507f268e4576286", "7da6f81dfd40caf7de01a52f69f129" },
                { " $^^^*&%&", " $^^#!@%&" } };
    }

    @Test(dataProvider = "getInvalidNames", priority = 10)
    public void searchByInvalidName(String lastName, String firstName)
    {
        guestSearch.byName(lastName, firstName, verifyError(ERROR_MORE_INFO));
    }

    @Test(priority = 20)
    public void searchByValidName()
    {
        guestSearch.byName("Cyarges", "Smoke");
        verifyThat(new GuestSearchResultsGrid(), size(greaterThan(1)));
    }

    @Test(priority = 30)
    public void searchByUniqueName()
    {
        guestSearch.byName(UNIQUE_NAME, UNIQUE_SURNAME);
        verifyMember();
    }

    @DataProvider(name = "getInvalidMemberIds")
    public Object[][] getInvalidMemberIds()
    {
        return new Object[][] { { "1234567" }, { "fnASSADif" }, { "^%$%&*^%*!" } };
    }

    @Test(priority = 40, dataProvider = "getInvalidMemberIds")
    public void searchByInvalidMemberId(String memberId)
    {
        guestSearch.byMemberNumber(memberId, verifyError(ERROR_INVALID_MEMBER_ID));
    }

    @Test(priority = 50)
    public void searchByValidMemberNumber()
    {
        guestSearch.byNumber(VALID_MEMBERSHIP_ID);
        verifyMember();
    }

    @Test(priority = 60)
    public void searchByInvalidNameAndValidNumber()
    {
        CustomerSearch customerSearch = guestSearch.getCustomerSearch();
        customerSearch.getFirstName().type(RandomUtils.getRandomLetters(7));
        customerSearch.getLastName().type(RandomUtils.getRandomLetters(7));
        customerSearch.getMemberNumber().type(VALID_MEMBERSHIP_ID);
        guestSearch.clickSearch();
        LeftPanel leftPanel = new LeftPanel();
        verifyThat(leftPanel.getCustomerInfoPanel().getMemberNumber(Program.RC), hasText(VALID_MEMBERSHIP_ID));
        leftPanel.getNewSearch().clickAndWait();
    }

    @Test(priority = 70)
    public void searchByNameAndState()
    {
        CustomerSearch customerSearch = guestSearch.getCustomerSearch();
        customerSearch.getFirstName().type(UNIQUE_NAME);
        customerSearch.getLastName().type(UNIQUE_SURNAME);
        customerSearch.getCity().type(UNIQUE_STATE);
        guestSearch.clickSearch();
        verifyThat(new PersonalInfoPage().getCustomerInfo().getFullName(), hasText(UNIQUE_NAME + " " + UNIQUE_SURNAME));
        new LeftPanel().clickGuestSearch();
    }

    private void verifyMember()
    {
        CustomerInfoFields customerInfo = new PersonalInfoPage().getCustomerInfo();
        verifyThat(customerInfo.getFullName(), hasText(UNIQUE_NAME + " " + UNIQUE_SURNAME));
        verifyThat(customerInfo.getResidenceCountry(), hasText(isValue(Country.US)));
        new LeftPanel().clickGuestSearch();
        new GuestSearchByNumber().getAdvancedSearch().clickAndWait();
    }
}
