package com.ihg.crm.automation.selenium.tests.hotel.stay.lpu;

import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyNoError;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.pages.login.TopUserSection;
import com.ihg.automation.selenium.common.stay.Revenues;
import com.ihg.automation.selenium.common.stay.Stay;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.hotel.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.stay.lpu.LoyaltyPendingUpdatesGridRow;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.stay.lpu.LoyaltyPendingUpdatesPage;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.stay.lpu.LpuGridRowContainer;
import com.ihg.crm.automation.selenium.tests.hotel.LoginLogout;
import com.ihg.crm.automation.selenium.tests.hotel.stay.HotelDBUtils;

public class CreateCompleteStayTest extends LoginLogout
{
    private Member member = new Member();
    private Stay stay = new Stay();

    @BeforeClass
    public void beforeClass()
    {
        member.addProgram(Program.RC);
        member.addEmail(MemberPopulateHelper.getRandomEmail());
        member.addPhone(MemberPopulateHelper.getRandomPhone());
        member.addAddress(MemberPopulateHelper.getUnitedStatesAddress());
        member.setPersonalInfo(MemberPopulateHelper.getSimplePersonalInfo());

        new HotelDBUtils(jdbcTemplate).getGnrHotel(stay);
        stay.setNights(5);
        stay.setCheckInDateByCheckout();
        stay.setRateCode("TEST");
        stay.setAvgRoomRate(100.00);
        stay.setIsQualifying(true);

        Revenues revenues = stay.getRevenues();
        revenues.getOverallTotal().setAmount(1300.00);
        revenues.getRoom().setAmount(500.00);
        revenues.getFood().setAmount(100.00);
        revenues.getBeverage().setAmount(100.00);
        revenues.getPhone().setAmount(100.00);
        revenues.getMeeting().setAmount(100.00);
        revenues.getMandatoryLoyalty().setAmount(100.00);
        revenues.getOtherLoyalty().setAmount(100.00);
        revenues.getNoLoyalty().setAmount(100.00);
        revenues.getTax().setAmount(100.00);
        revenues.getTotalNonQualifying().setAmount(300.00);
        revenues.getTotalQualifying().setAmount(1000.00);

        login();

        member = new EnrollmentPage().enroll(member, helper, verifyNoError());

        new TopUserSection().switchHotel(stay.getHotelCode());
        new LoyaltyPendingUpdatesPage().goToByHotelOperations().createMissingStay(member, stay);
    }

    @Test
    public void verifyCreatedStayRow()
    {
        new LoyaltyPendingUpdatesPage().getLoyaltyPendingUpdatesGrid().getRow(1).verify(member, stay);
    }

    @Test(dependsOnMethods = { "verifyCreatedStayRow" })
    public void verifyStayAndRevenueDetails()
    {
        LoyaltyPendingUpdatesGridRow updatesGridRow = new LoyaltyPendingUpdatesPage().getLoyaltyPendingUpdatesGrid()
                .getRow(1);
        updatesGridRow.verify(member, stay);
        updatesGridRow.expand(LpuGridRowContainer.class).verify(stay, member);
    }
}
