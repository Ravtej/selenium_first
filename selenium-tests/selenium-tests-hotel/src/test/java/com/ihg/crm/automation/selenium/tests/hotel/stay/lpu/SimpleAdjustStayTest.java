package com.ihg.crm.automation.selenium.tests.hotel.stay.lpu;

import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyNoError;
import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifySuccess;
import static com.ihg.automation.selenium.common.stay.AdjustReasons.NOSHOW;
import static com.ihg.automation.selenium.hotel.pages.hoteloperations.stay.lpu.LoyaltyPendingUpdatesGridRow.LoyaltyPendingUpdatesGridCell.GUEST_NAME;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.pages.login.TopUserSection;
import com.ihg.automation.selenium.common.stay.AdjustReasons;
import com.ihg.automation.selenium.common.stay.Revenues;
import com.ihg.automation.selenium.common.stay.Stay;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.hotel.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.stay.lpu.LoyaltyPendingUpdatesGridRow;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.stay.lpu.LoyaltyPendingUpdatesPage;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.stay.lpu.LpuGridRowContainer;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.stay.lpu.missingstay.StayGridRowAdjustPanel;
import com.ihg.crm.automation.selenium.tests.hotel.LoginLogout;
import com.ihg.crm.automation.selenium.tests.hotel.stay.HotelDBUtils;

public class SimpleAdjustStayTest extends LoginLogout
{
    private Member member = new Member();
    private Stay createStay = new Stay();
    private Stay adjustStay;

    @BeforeClass
    public void beforeClass()
    {
        member.addProgram(Program.RC);
        member.addEmail(MemberPopulateHelper.getRandomEmail());
        member.addPhone(MemberPopulateHelper.getRandomPhone());
        member.addAddress(MemberPopulateHelper.getUnitedStatesAddress());
        member.setPersonalInfo(MemberPopulateHelper.getSimplePersonalInfo());

        new HotelDBUtils(jdbcTemplate).getGnrHotel(createStay);
        createStay.setNights(5);
        createStay.setCheckInDateByCheckout();
        createStay.setRateCode("TEST");
        createStay.setAvgRoomRate(100.00);
        createStay.setIsQualifying(true);

        Revenues revenues = createStay.getRevenues();
        revenues.getOverallTotal().setAmount(1300.00);
        revenues.getRoom().setAmount(500.00);
        revenues.getFood().setAmount(100.00);
        revenues.getBeverage().setAmount(100.00);
        revenues.getPhone().setAmount(100.00);
        revenues.getMeeting().setAmount(100.00);
        revenues.getMandatoryLoyalty().setAmount(100.00);
        revenues.getOtherLoyalty().setAmount(100.00);
        revenues.getNoLoyalty().setAmount(100.00);
        revenues.getTax().setAmount(100.00);
        revenues.getTotalNonQualifying().setAmount(300.00);
        revenues.getTotalQualifying().setAmount(1000.00);

        adjustStay = createStay.clone();
        adjustStay.setAvgRoomRate(200.00);
        adjustStay.setIsQualifying(true);
        adjustStay.setAdjustReason(AdjustReasons.NOSHOW);

        Revenues adjustRevenues = adjustStay.getRevenues();
        adjustRevenues.getOverallTotal().setAmount(1800.00);
        adjustRevenues.getRoom().setAmount(1000.00);
        adjustRevenues.getTotalQualifying().setAmount(1500.00);

        login();

        member = new EnrollmentPage().enroll(member, helper, verifyNoError());

        new TopUserSection().switchHotel(createStay.getHotelCode());
        new LoyaltyPendingUpdatesPage().goToByHotelOperations().createMissingStay(member, createStay);
    }

    @Test
    public void verifyCreatedStayRow()
    {
        new LoyaltyPendingUpdatesPage().getLoyaltyPendingUpdatesGrid().getRow(1).verify(member, createStay);
    }

    @Test(dependsOnMethods = { "verifyCreatedStayRow" })
    public void verifyStayDetailsInRow()
    {
        LoyaltyPendingUpdatesGridRow updatesGridRow = new LoyaltyPendingUpdatesPage().getLoyaltyPendingUpdatesGrid()
                .getRow(1);
        updatesGridRow.getCell(GUEST_NAME).click(verifyNoError());

        StayGridRowAdjustPanel stayGridRowAdjustPanel = updatesGridRow.getStayGridRowAdjustPanel();
        stayGridRowAdjustPanel.verify(member, createStay);
    }

    @Test(dependsOnMethods = { "verifyStayDetailsInRow" })
    public void adjustStayDetailsInGrid()
    {
        StayGridRowAdjustPanel stayGridRowAdjustPanel = new LoyaltyPendingUpdatesPage().getLoyaltyPendingUpdatesGrid()
                .getRow(1).getStayGridRowAdjustPanel();
        stayGridRowAdjustPanel.populate(adjustStay, NOSHOW);
        stayGridRowAdjustPanel.clickSave(verifyNoError(), verifySuccess("Your adjustment has been saved"));
    }

    @Test(dependsOnMethods = { "adjustStayDetailsInGrid" })
    public void verifyAdjustedStay()
    {
        LoyaltyPendingUpdatesGridRow row = new LoyaltyPendingUpdatesPage().getLoyaltyPendingUpdatesGrid().getRow(1);
        row.verify(member, adjustStay);

        LpuGridRowContainer gridRowContainer = row.expand(LpuGridRowContainer.class);
        gridRowContainer.verify(adjustStay, member);
    }
}
