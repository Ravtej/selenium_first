package com.ihg.crm.automation.selenium.tests.hotel.enrollment.br;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.LeftPanelBase.BLUE_COLOR;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.isDisplayedWithWait;
import static com.ihg.automation.selenium.matchers.component.HasDefaultText.hasDefault;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static org.hamcrest.core.StringEndsWith.endsWith;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.BusinessRewardsGridRow;
import com.ihg.automation.selenium.common.Constant;
import com.ihg.automation.selenium.common.ProgramInfoPanel;
import com.ihg.automation.selenium.common.ProgramsGrid;
import com.ihg.automation.selenium.common.business.BusinessStatus;
import com.ihg.automation.selenium.common.contact.Address;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.common.types.program.ProgramStatus;
import com.ihg.automation.selenium.common.types.program.ProgramStatusReason;
import com.ihg.automation.selenium.common.types.program.RewardClubLevel;
import com.ihg.automation.selenium.gwt.components.Mode;
import com.ihg.automation.selenium.gwt.components.panel.FieldSet;
import com.ihg.automation.selenium.hotel.pages.LeftPanel;
import com.ihg.automation.selenium.hotel.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.hotel.pages.personal.AddressContact;
import com.ihg.automation.selenium.hotel.pages.personal.PersonalInfoPage;
import com.ihg.automation.selenium.hotel.pages.programs.ProgramPageBase;
import com.ihg.automation.selenium.hotel.pages.programs.br.BusinessRewardsPage;
import com.ihg.automation.selenium.hotel.pages.programs.br.BusinessRewardsSummary;
import com.ihg.automation.selenium.matchers.component.HasBackgroundColor;
import com.ihg.crm.automation.selenium.tests.hotel.LoginLogout;

public class EnrollmentToBrTest extends LoginLogout
{
    private Member member = new Member();

    @BeforeClass
    public void beforeClass()
    {
        member.addProgram(Program.BR);
        member.addEmail(MemberPopulateHelper.getRandomEmail());
        member.addPhone(MemberPopulateHelper.getRandomPhone());
        member.addAddress(MemberPopulateHelper.getUnitedStatesAddress());
        member.setPersonalInfo(MemberPopulateHelper.getSimplePersonalInfo());

        login();
        new EnrollmentPage().enroll(member, helper);
    }

    @Test
    public void verifyPersonalInformationTab()
    {
        PersonalInfoPage personalInfoPage = new PersonalInfoPage();
        verifyThat(personalInfoPage, isDisplayedWithWait());
        personalInfoPage.getCustomerInfo().verify(member, Mode.VIEW);

        AddressContact addressContact = personalInfoPage.getAddressList().getExpandedContact();
        Address address = addressContact.getBaseContact();
        address.verify(member.getPreferredAddress(), Mode.VIEW);
    }

    @Test(dependsOnMethods = { "verifyPersonalInformationTab" }, alwaysRun = true)
    public void verifyBrOverviewSection()
    {
        BusinessRewardsPage businessRewardsPage = new BusinessRewardsPage();

        new LeftPanel().getProgramInfoPanel().getPrograms().getBusinessRewardsProgram().goToProgramPage();
        verifyThat(businessRewardsPage, displayed(true));

        FieldSet programOverview = businessRewardsPage.getProgramOverview();
        programOverview.expand();
        verifyThat(programOverview, hasText(endsWith("Business Rewards program")));
    }

    @Test(dependsOnMethods = { "verifyBrOverviewSection" }, alwaysRun = true)
    public void verifyProgramSummary()
    {
        BusinessRewardsSummary businessRewardsSummary = new BusinessRewardsPage().getSummary();
        businessRewardsSummary.verifyProgramDetailsAfterEnroll(ProgramStatus.OPEN, member, BusinessStatus.PENDING);
    }

    @Test(dependsOnMethods = { "verifyProgramSummary" }, alwaysRun = true)
    public void validateEnrollmentDetails()
    {
        BusinessRewardsPage businessRewardsPage = new BusinessRewardsPage();

        ProgramPageBase.EnrollmentDetails enrollmentDetails = businessRewardsPage.getEnrollmentDetails();
        enrollmentDetails.verifyDetails(helper.getSourceFactory().getSource(), Constant.NOT_AVAILABLE);
        verifyThat(enrollmentDetails.getReferringMember(), hasDefault());
    }

    @Test(dependsOnMethods = { "validateEnrollmentDetails" }, alwaysRun = true)
    public void verifyProgramInformationPanel()
    {
        ProgramsGrid programs = new ProgramInfoPanel().getPrograms();
        programs.getRewardClubProgram().verify(RewardClubLevel.CLUB, "0");

        BusinessRewardsGridRow rowBR = programs.getBusinessRewardsProgram();
        verifyThat(rowBR.getStatus(), hasText(ProgramStatusReason.PENDING));

        // TODO: TBD the exact behavior and color to be here (request to
        // customer)
        verifyThat(rowBR, HasBackgroundColor.hasBackgroundColor(BLUE_COLOR));
    }
}
