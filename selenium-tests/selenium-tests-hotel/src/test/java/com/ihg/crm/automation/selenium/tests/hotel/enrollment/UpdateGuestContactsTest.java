package com.ihg.crm.automation.selenium.tests.hotel.enrollment;

import static com.ihg.automation.common.RandomUtils.getRandomNumber;
import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyError;
import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyNoError;
import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifySuccess;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.components.Country.US;
import static com.ihg.automation.selenium.common.member.MemberPopulateHelper.getRandomEmail;
import static com.ihg.automation.selenium.common.member.MemberPopulateHelper.getRandomPhone;
import static com.ihg.automation.selenium.common.member.MemberPopulateHelper.getSimplePersonalInfo;
import static com.ihg.automation.selenium.common.member.MemberPopulateHelper.getUnitedStatesAddress;
import static com.ihg.automation.selenium.common.member.MemberPopulateHelper.getUsBusinessAddress;
import static com.ihg.automation.selenium.gwt.components.Mode.VIEW;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.enabled;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasDefault;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasText;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.isSelected;
import static com.ihg.automation.selenium.servicecenter.pages.Messages.CONTACT_SUCCESS_UPDATE;
import static com.ihg.automation.selenium.servicecenter.pages.Messages.HIGHLIGHTED_FIELDS_DID_NOT_PASS_VALIDATION;
import static org.apache.commons.lang3.StringUtils.EMPTY;
import static org.hamcrest.core.StringContains.containsString;

import org.joda.time.LocalDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.atp.schema.crm.guest.guest.servicetypes.v2.RetrieveGuestRequestType;
import com.ihg.atp.schema.crm.guest.guest.servicetypes.v2.RetrieveGuestResponseType;
import com.ihg.atp.wsdl.crm.guest.guest.serviceinterface.v2.GuestServiceDomainInterface;
import com.ihg.automation.selenium.common.contact.Address;
import com.ihg.automation.selenium.common.member.GuestAddress;
import com.ihg.automation.selenium.common.member.GuestEmail;
import com.ihg.automation.selenium.common.member.GuestPhone;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.personal.PhoneContact;
import com.ihg.automation.selenium.common.personal.SmsContact;
import com.ihg.automation.selenium.common.types.address.AddressType;
import com.ihg.automation.selenium.common.types.address.Region;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.gwt.components.Button;
import com.ihg.automation.selenium.gwt.components.Mode;
import com.ihg.automation.selenium.gwt.components.dialog.ConfirmDialog;
import com.ihg.automation.selenium.hotel.pages.CustomerInfoPanel;
import com.ihg.automation.selenium.hotel.pages.LeftPanel;
import com.ihg.automation.selenium.hotel.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.hotel.pages.personal.AddressContact;
import com.ihg.automation.selenium.hotel.pages.personal.CustomerInfoFields;
import com.ihg.automation.selenium.hotel.pages.personal.HotelAddressContactList;
import com.ihg.automation.selenium.hotel.pages.personal.HotelEmailContact;
import com.ihg.automation.selenium.hotel.pages.personal.HotelEmailContactList;
import com.ihg.automation.selenium.hotel.pages.personal.HotelPhoneContactList;
import com.ihg.automation.selenium.hotel.pages.personal.HotelSmsContactList;
import com.ihg.automation.selenium.hotel.pages.personal.PersonalInfoPage;
import com.ihg.automation.selenium.hotel.pages.search.GuestSearchByNumber;
import com.ihg.automation.utils.DiffHelper;
import com.ihg.crm.automation.selenium.tests.hotel.LoginLogout;
import com.ihg.crm.automation.service.guest.RetrieveGuestRequestHelper;

public class UpdateGuestContactsTest extends LoginLogout
{
    private Member member = new Member();
    private GuestAddress validAddress;
    private GuestAddress validAddressAfterCleansing;
    private GuestAddress addInvalidAddress;
    private RetrieveGuestRequestType retrieveGuestRequest;
    private GuestPhone updateGuestPhone = getRandomPhone();
    private DiffHelper diffHelper = new DiffHelper();
    private PersonalInfoPage personalInfoPage = new PersonalInfoPage();
    private CustomerInfoPanel customerInfoPanel = new CustomerInfoPanel();

    private static final String PERSONAL_INFO_UPDATE_SUCCESS = "Personal info has been successfully saved";

    @Autowired
    private GuestServiceDomainInterface guestClient;

    @BeforeClass
    public void before()
    {
        member.setPersonalInfo(getSimplePersonalInfo());
        member.getPersonalInfo().setBirthDate(new LocalDate());
        member.addProgram(Program.RC);
        member.addAddress(getUnitedStatesAddress());
        member.addPhone(getRandomPhone());

        validAddress = new GuestAddress();
        validAddress.setCountryCode(US);
        validAddress.setType(AddressType.RESIDENCE);
        validAddress.setPostalCode("10010");
        validAddress.setLocality1("New York");
        validAddress.setRegion1(Region.NY);
        validAddress.setLine1("71 Madison ave");

        validAddressAfterCleansing = validAddress.clone();
        validAddressAfterCleansing.setPostalCode("10016-8726");
        validAddressAfterCleansing.setLocality1("NEW YORK");
        validAddressAfterCleansing.setLine1("71 MADISON AVE");

        addInvalidAddress = new GuestAddress();
        addInvalidAddress.setCountryCode(US);
        addInvalidAddress.setType(AddressType.RESIDENCE);
        addInvalidAddress.setPostalCode("12345");
        addInvalidAddress.setLocality1("New York");
        addInvalidAddress.setRegion1(Region.NY);
        addInvalidAddress.setLine1("101 Borsch Ave");
        addInvalidAddress.setPreferred(false);

        login();

        member = new EnrollmentPage().enroll(member, helper);
        retrieveGuestRequest = RetrieveGuestRequestHelper.byMemberId(member.getRCProgramId());
    }

    @Test(priority = 1)
    public void verifyEndEditSessionButtonEnabled()
    {
        verifyThat(new LeftPanel().getEndEditSession(), enabled(true));
    }

    @Test(priority = 10)
    public void doNotCloseEditSession()
    {
        LeftPanel leftPanel = new LeftPanel();
        Button endEditSession = leftPanel.getEndEditSession();
        endEditSession.clickAndWait(verifyNoError());

        new ConfirmDialog().clickNo(verifyNoError());
        verifyThat(endEditSession, enabled(true));
        verifyThat(leftPanel.getCustomerInfoPanel().getMemberNumber(Program.RC), hasText(member.getRCProgramId()));
    }

    @Test(priority = 20)
    public void closeEditSession()
    {
        new LeftPanel().getEndEditSession().clickAndWait(verifyNoError());

        new ConfirmDialog().clickYes(verifyNoError());
        verifyWelcome();
    }

    @Test(priority = 30)
    public void addSecondAddressAsPreferred()
    {
        RetrieveGuestResponseType retrieveGuestBefore = guestClient.retrieveGuest(retrieveGuestRequest);

        new GuestSearchByNumber().byMemberNumber(member.getRCProgramId());

        HotelAddressContactList addressList = personalInfoPage.getAddressList();
        AddressContact addressContact = addressList.getContact(1);
        addressContact.clickEdit();
        verifyThat(addressContact.getSaveAsEntered(), isSelected(false));
        addressContact.clickCancel();

        addressList.addSecondContactAsPreferred(member.getPreferredAddress(), validAddressAfterCleansing,
                CONTACT_SUCCESS_UPDATE);
        verifyThat(customerInfoPanel.getAddress(), hasText(containsString(validAddressAfterCleansing.getLine1())));

        RetrieveGuestResponseType retrieveGuestAfter = guestClient.retrieveGuest(retrieveGuestRequest);
        diffHelper.verifyEntitiesSizeAfterUpdate(retrieveGuestBefore, retrieveGuestAfter, 1, 0, 0);
    }

    @Test(priority = 50)
    public void addMaxAddressesTest()
    {
        personalInfoPage.getAddressList().addMaxContactAmount(5);
    }

    @Test(priority = 60)
    public void updatePreferredAddress()
    {
        RetrieveGuestResponseType retrieveGuestBefore = guestClient.retrieveGuest(retrieveGuestRequest);

        GuestAddress guestAddress = getUsBusinessAddress();
        personalInfoPage.getAddressList().updateFirstContact(guestAddress, CONTACT_SUCCESS_UPDATE);
        verifyThat(customerInfoPanel.getAddress(), hasText(containsString(guestAddress.getLine1())));

        RetrieveGuestResponseType retrieveGuestAfter = guestClient.retrieveGuest(retrieveGuestRequest);
        diffHelper.verifyEntitiesSizeAfterUpdate(retrieveGuestBefore, retrieveGuestAfter, 0, 0, 0);
    }

    @Test(priority = 70)
    public void tryToSaveAddressWithEmptyFields()
    {
        AddressContact contactToRemove = personalInfoPage.getAddressList().getContact(1);
        contactToRemove.clickEdit(verifyNoError());

        Address contact = contactToRemove.getBaseContact();
        contact.setAddressConfiguration(US);
        contact.getAddress1().clear();
        contact.getLocality1().clear();
        contact.getRegion1().clear();
        contact.getZipCode().clear();
        contactToRemove.clickSave(verifyError(HIGHLIGHTED_FIELDS_DID_NOT_PASS_VALIDATION));
        contactToRemove.clickCancel(verifyNoError());
    }

    @Test(priority = 80)
    public void updatePhone()
    {
        RetrieveGuestResponseType retrieveGuestBefore = guestClient.retrieveGuest(retrieveGuestRequest);

        personalInfoPage.getPhoneList().updateFirstContact(updateGuestPhone, CONTACT_SUCCESS_UPDATE);
        verifyThat(customerInfoPanel.getPhone(), hasText(updateGuestPhone.getFullPhone()));

        RetrieveGuestResponseType retrieveGuestAfter = guestClient.retrieveGuest(retrieveGuestRequest);
        diffHelper.verifyEntitiesSizeAfterUpdate(retrieveGuestBefore, retrieveGuestAfter, 0, 0, 0);
    }

    @Test(priority = 90)
    public void addSecondPhoneAsPreferred()
    {
        RetrieveGuestResponseType retrieveGuestBefore = guestClient.retrieveGuest(retrieveGuestRequest);

        GuestPhone guestPhone = getRandomPhone();

        HotelPhoneContactList phoneList = personalInfoPage.getPhoneList();
        phoneList.addSecondContactAsPreferred(updateGuestPhone, guestPhone, CONTACT_SUCCESS_UPDATE);
        verifyThat(customerInfoPanel.getPhone(), hasText(guestPhone.getFullPhone()));

        verifyThat(phoneList.getAddButton(), enabled(true));

        RetrieveGuestResponseType retrieveGuestAfter = guestClient.retrieveGuest(retrieveGuestRequest);
        diffHelper.verifyEntitiesSizeAfterUpdate(retrieveGuestBefore, retrieveGuestAfter, 0, 1, 0);
    }

    @Test(priority = 100)
    public void tryToSavePhoneWithEmptyFields()
    {
        PhoneContact phoneContact = personalInfoPage.getPhoneList().getContact(1);
        phoneContact.clickEdit(verifyNoError());
        phoneContact.getBaseContact().getNumber().clear();
        phoneContact.clickSave(verifyError(HIGHLIGHTED_FIELDS_DID_NOT_PASS_VALIDATION));
        phoneContact.clickCancel(verifyNoError());
    }

    @Test(priority = 110)
    public void addMaxPhoneNumbers()
    {
        personalInfoPage.getPhoneList().addMaxContactAmount(3);
    }

    @Test(priority = 120)
    public void addMaxSmsAmount()
    {
        RetrieveGuestResponseType retrieveGuestBefore = guestClient.retrieveGuest(retrieveGuestRequest);

        HotelSmsContactList smsList = personalInfoPage.getSmsList();
        GuestPhone smsPhone = getRandomPhone();
        smsList.add(smsPhone);

        smsList.getExpandedContact().verifyMasked(smsPhone, Mode.VIEW);
        verifyThat(smsList.getAddButton(), enabled(false));

        RetrieveGuestResponseType retrieveGuestAfter = guestClient.retrieveGuest(retrieveGuestRequest);
        diffHelper.verifyEntitiesSizeAfterUpdate(retrieveGuestBefore, retrieveGuestAfter, 0, 1, 0);
    }

    @Test(priority = 130)
    public void updateSmsPhone()
    {
        RetrieveGuestResponseType retrieveGuestBefore = guestClient.retrieveGuest(retrieveGuestRequest);

        GuestPhone updatedSmsNumber = new GuestPhone();
        updatedSmsNumber.setNumber(getRandomNumber(7));

        SmsContact contact = personalInfoPage.getSmsList().getContact();
        contact.update(updatedSmsNumber, verifySuccess(CONTACT_SUCCESS_UPDATE));
        contact.expand();
        contact.verifyMasked(updatedSmsNumber, Mode.VIEW);

        RetrieveGuestResponseType retrieveGuestAfter = guestClient.retrieveGuest(retrieveGuestRequest);
        diffHelper.verifyEntitiesSizeAfterUpdate(retrieveGuestBefore, retrieveGuestAfter, 0, 0, 0);
    }

    @Test(priority = 140)
    public void tryToSaveSmsPhoneWithEmptyFields()
    {
        SmsContact smsContact = personalInfoPage.getSmsList().getContact(1);

        smsContact.clickEdit(verifyNoError());
        smsContact.getBaseContact().getNumber().clear();
        smsContact.clickSave(verifyError(HIGHLIGHTED_FIELDS_DID_NOT_PASS_VALIDATION));
        smsContact.clickCancel(verifyNoError());
    }

    @Test(priority = 145)
    public void addMaxEmails()
    {
        RetrieveGuestResponseType retrieveGuestBefore = guestClient.retrieveGuest(retrieveGuestRequest);

        HotelEmailContactList emailContactList = personalInfoPage.getEmailList();

        GuestEmail newEmail = getRandomEmail();
        emailContactList.add(newEmail);
        verifyThat(customerInfoPanel.getEmail(), hasText(newEmail.getEmailDomain()));

        verifyThat(emailContactList.getAddButton(), enabled(false));

        RetrieveGuestResponseType retrieveGuestAfter = guestClient.retrieveGuest(retrieveGuestRequest);
        diffHelper.verifyEntitiesSizeAfterUpdate(retrieveGuestBefore, retrieveGuestAfter, 0, 0, 1);
    }

    @Test(priority = 150)
    public void updateEmail()
    {
        RetrieveGuestResponseType retrieveGuestBefore = guestClient.retrieveGuest(retrieveGuestRequest);

        GuestEmail updateEmail = getRandomEmail();

        HotelEmailContactList emailContactList = personalInfoPage.getEmailList();
        HotelEmailContact contact = emailContactList.getContact();
        contact.update(updateEmail, verifySuccess(CONTACT_SUCCESS_UPDATE));

        contact.expand();
        contact.verify(updateEmail, VIEW);
        verifyThat(emailContactList.getAddButton(), enabled(false));
        verifyThat(customerInfoPanel.getEmail(), hasText(updateEmail.getEmailDomain()));

        RetrieveGuestResponseType retrieveGuestAfter = guestClient.retrieveGuest(retrieveGuestRequest);
        diffHelper.verifyEntitiesSizeAfterUpdate(retrieveGuestBefore, retrieveGuestAfter, 0, 0, 0);
    }

    @Test(priority = 160)
    public void tryToSaveEmailWithEmptyFields()
    {
        HotelEmailContact emailContact = personalInfoPage.getEmailList().getContact();

        emailContact.clickEdit();
        emailContact.getBaseContact().getEmail().clear();
        emailContact.clickSave(verifyError(HIGHLIGHTED_FIELDS_DID_NOT_PASS_VALIDATION));
        emailContact.clickCancel(verifyNoError());
    }

    @Test(priority = 170)
    public void removeBirthday()
    {
        RetrieveGuestResponseType retrieveGuestBefore = guestClient.retrieveGuest(retrieveGuestRequest);

        CustomerInfoFields customerInfo = personalInfoPage.getCustomerInfo();
        customerInfo.clickEdit();
        customerInfo.getBirthDate().getMonth().select(EMPTY);
        customerInfo.clickSave(verifySuccess(PERSONAL_INFO_UPDATE_SUCCESS));

        verifyThat(customerInfo.getBirthDate().getFullDate(), hasDefault());

        RetrieveGuestResponseType retrieveGuestAfter = guestClient.retrieveGuest(retrieveGuestRequest);
        diffHelper.verifyEntitiesSizeAfterUpdate(retrieveGuestBefore, retrieveGuestAfter, 0, 0, 0);
    }
}
