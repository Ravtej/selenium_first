package com.ihg.crm.automation.selenium.tests.hotel.operations.business;

import static com.ihg.automation.common.DateUtils.getFormattedDate;
import static com.ihg.automation.common.RandomUtils.getRandomLetters;
import static com.ihg.automation.common.RandomUtils.getRandomNumber;
import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyError;
import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyNoError;
import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifySuccess;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.business.BusinessRewardsEventDetailsPopUp.CREATE_EVENT_SUCCESS;
import static com.ihg.automation.selenium.common.business.BusinessRewardsEventDetailsPopUp.UPDATE_EVENT_SUCCESS;
import static com.ihg.automation.selenium.common.business.BusinessStatus.BLOCKED;
import static com.ihg.automation.selenium.common.types.Currency.USD;
import static com.ihg.automation.selenium.hotel.pages.Role.SALES_MANAGER;
import static com.ihg.automation.selenium.matchers.component.GridSize.size;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static com.ihg.automation.selenium.matchers.component.IsDisplayed.displayed;
import static com.ihg.automation.selenium.matchers.component.IsEnabled.enabled;
import static com.ihg.automation.selenium.servicecenter.pages.Messages.HIGHLIGHTED_FIELDS_DID_NOT_PASS_VALIDATION;
import static org.hamcrest.core.AllOf.allOf;

import org.testng.annotations.AfterGroups;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.business.BusinessEvent;
import com.ihg.automation.selenium.common.business.BusinessRevenueBean;
import com.ihg.automation.selenium.common.business.BusinessRewardsEventDetailsPopUp;
import com.ihg.automation.selenium.common.business.GuestRoom;
import com.ihg.automation.selenium.common.business.GuestRoomsFields;
import com.ihg.automation.selenium.common.business.MeetingDetails;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.pages.login.TopUserSection;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.gwt.components.dialog.ConfirmDialog;
import com.ihg.automation.selenium.hotel.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.br.manage.ManageEventsGrid;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.br.manage.ManageEventsGridRow;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.br.manage.ManageEventsPage;
import com.ihg.crm.automation.selenium.tests.hotel.LoginLogout;

public class CreateKnrBusinessEventTest extends LoginLogout
{
    private static final String HOTEL = "MIAHA";

    private ManageEventsPage manageEventsPage = new ManageEventsPage();
    private BusinessRewardsEventDetailsPopUp popUp;
    private Member member = new Member();
    private GuestRoom guestRoom;
    private GuestRoomsFields guestRoomsFields;
    private BusinessEvent businessEvent;

    @BeforeClass
    public void beforeClass()
    {
        member.addProgram(Program.BR);
        member.setPersonalInfo(MemberPopulateHelper.getSimplePersonalInfo());
        member.addAddress(MemberPopulateHelper.getUnitedStatesAddress());

        guestRoom = new GuestRoom();
        guestRoom.setTotalGuestRooms(1);
        guestRoom.setTotalRoomNights(2);
        guestRoom.setTotalGuests(2);
        guestRoom.setTotalRoom(new BusinessRevenueBean(1000.00));

        login(HOTEL);
        member = new EnrollmentPage().enroll(member, helper);

        new TopUserSection().switchUserRole(SALES_MANAGER, true);
        manageEventsPage.goToByOperation();
    }

    @Test(groups = "knrBrTests", dataProvider = "knrValidDataProvider", priority = 10)
    public void verifyKnrEventCreation(int daysStartDate, int daysEndDate, String knrCompanyName,
            String expectedKnrCompanyName)
    {
        businessEvent = new BusinessEvent("KnrEvent" + getRandomNumber(4), HOTEL, USD);
        MeetingDetails meetingDetails = new MeetingDetails();
        meetingDetails.setStartDate(getFormattedDate(daysStartDate));
        meetingDetails.setEndDate(getFormattedDate(daysEndDate));
        guestRoom.setStartDate(meetingDetails.getStartDate());
        guestRoom.setEndDate(meetingDetails.getEndDate());
        guestRoom.setKnr(knrCompanyName);
        businessEvent.setStatus(BLOCKED);
        businessEvent.setMeetingDetails(meetingDetails);
        businessEvent.setMember(member);
        businessEvent.setGuestRoom(guestRoom);

        expandGuestRoomAndPopulate(businessEvent);

        guestRoomsFields.populate(guestRoom);
        popUp.clickSave(verifySuccess(CREATE_EVENT_SUCCESS));

        popUp = findEventRowVerifyAndOpenEventDetails();

        popUp.getEventSummaryTab().getEventDetails().getGuestRoomsFields().verifyKnr(expectedKnrCompanyName, true);
        popUp.clickCancel();
    }

    @Test(dataProvider = "knrInvalidDataProvider", priority = 15)
    public void tryToUpdateEventWithInvalidValues(String knrCompanyName)
    {
        popUp = new BusinessRewardsEventDetailsPopUp();
        guestRoomsFields = popUp.getEventSummaryTab().getEventDetails().getGuestRoomsFields();
        guestRoomsFields.getKnr().type(knrCompanyName);

        popUp.clickSave(verifyError(HIGHLIGHTED_FIELDS_DID_NOT_PASS_VALIDATION));
    }

    @Test(priority = 20)
    public void verifyKnrInputDisabled()
    {
        popUp = new BusinessRewardsEventDetailsPopUp();
        popUp.clickCancel();

        ConfirmDialog confirmDialog = new ConfirmDialog();
        verifyThat(confirmDialog, hasText("Changes will not be Saved. Do you want to Continue?"));
        confirmDialog.clickYes(verifyNoError());

        BusinessEvent newBusinessEvent = new BusinessEvent("Test", HOTEL, USD);
        newBusinessEvent.setMember(member);
        expandGuestRoomAndPopulate(newBusinessEvent);

        verifyThat(guestRoomsFields.getKnrCheckBox(), allOf(enabled(false), displayed(true)));
        verifyThat(guestRoomsFields.getKnr(), allOf(enabled(false), displayed(true)));
    }

    @Test(groups = "closePopUp", dataProvider = "knrInvalidDataProvider", priority = 30)
    public void tryToCreateEventWithInvalidValues(String knrCompanyName)
    {
        BusinessEvent businessEventToBeUpdated = new BusinessEvent("KnrEvent" + getRandomNumber(4), HOTEL, USD);
        guestRoom.setKnr(knrCompanyName);
        businessEventToBeUpdated.setMember(member);
        businessEventToBeUpdated.setGuestRoom(guestRoom);

        guestRoomsFields.populate(guestRoom);
        popUp.clickSave(verifyError(HIGHLIGHTED_FIELDS_DID_NOT_PASS_VALIDATION));
    }

    @Test(priority = 40)
    public void updateCreatedEvent()
    {
        popUp.clickCancel();

        new ConfirmDialog().clickYes(verifyNoError());

        popUp = findEventRowVerifyAndOpenEventDetails();

        String knrUpdated = "ValidKnr" + getRandomNumber(5);
        guestRoom.setKnr(knrUpdated);
        businessEvent.setMember(member);
        businessEvent.setGuestRoom(guestRoom);

        guestRoomsFields = popUp.getEventSummaryTab().getEventDetails().getGuestRoomsFields();
        guestRoomsFields.populate(guestRoom);
        popUp.clickSave(verifySuccess(UPDATE_EVENT_SUCCESS));

        popUp = findEventRowVerifyAndOpenEventDetails();
        popUp.getEventSummaryTab().getEventDetails().getGuestRoomsFields().verifyKnr(knrUpdated, true);
    }

    private BusinessRewardsEventDetailsPopUp findEventRowVerifyAndOpenEventDetails()
    {
        manageEventsPage.searchByEventNameAndMemberId(businessEvent);

        ManageEventsGrid manageEventsGrid = manageEventsPage.getManageEventsGrid();
        verifyThat(manageEventsGrid, size(1));
        ManageEventsGridRow row = manageEventsGrid.getRow(1);
        row.verify(businessEvent);

        return row.openEventDetailsPopUp();
    }

    private void expandGuestRoomAndPopulate(BusinessEvent businessEvent)
    {
        popUp = manageEventsPage.openDetailsPopUp();
        popUp.getEventSummaryTab().getEventInformation().populateHotelEvent(businessEvent);

        guestRoomsFields = popUp.getEventSummaryTab().getEventDetails().getGuestRoomsFields();
        guestRoomsFields.expand();
    }

    @DataProvider(name = "knrValidDataProvider")
    protected Object[][] knrValidProvider()
    {
        String knrName19Letters = "19" + getRandomLetters(17);
        String knrName20Letters = "20" + getRandomLetters(18);
        String knrName21Letters = "21" + getRandomLetters(19);

        return new Object[][] { { -17, -15, knrName19Letters, knrName19Letters },
                { -14, -12, knrName20Letters, knrName20Letters },
                { -11, -9, knrName21Letters, knrName21Letters.substring(0, 20) } };
    }

    @DataProvider(name = "knrInvalidDataProvider")
    protected Object[][] knrInvalidProvider()
    {
        return new Object[][] { { "BizRewards20!" }, { " " }, { "бизнесРевордс" }, { "中山区中山北路" } };
    }

    @AfterGroups("knrBrTests")
    public void findRowToUpdate()
    {
        findEventRowVerifyAndOpenEventDetails();
    }
}
