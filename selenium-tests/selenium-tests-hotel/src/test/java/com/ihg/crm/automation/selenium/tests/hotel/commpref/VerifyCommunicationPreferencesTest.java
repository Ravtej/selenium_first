package com.ihg.crm.automation.selenium.tests.hotel.commpref;

import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyNoError;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.communication.CommunicationPreferencesConstants.OPT_IN;
import static com.ihg.automation.selenium.common.communication.CommunicationPreferencesConstants.OPT_OUT;
import static com.ihg.automation.selenium.common.types.WrittenLanguage.ENGLISH;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasDefault;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasText;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.EmailNotMailableDialog;
import com.ihg.automation.selenium.common.member.GuestEmail;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.hotel.pages.communication.CommunicationPreferences;
import com.ihg.automation.selenium.hotel.pages.communication.CommunicationPreferencesPage;
import com.ihg.automation.selenium.hotel.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.hotel.pages.personal.PersonalInfoPage;
import com.ihg.crm.automation.selenium.tests.hotel.LoginLogout;

public class VerifyCommunicationPreferencesTest extends LoginLogout
{
    private static final Logger logger = LoggerFactory.getLogger(VerifyCommunicationPreferencesTest.class);

    @BeforeClass
    public void beforeClass()
    {
        Member member = new Member();
        member.addProgram(Program.RC);
        member.setPersonalInfo(MemberPopulateHelper.getSimplePersonalInfo());
        member.addAddress(MemberPopulateHelper.getUnitedStatesAddress());

        login();
        new EnrollmentPage().enroll(member, helper);
    }

    @Test
    public void verifyCommPrefsWithoutEmailAndSms()
    {
        CommunicationPreferences communicationPreferences = new CommunicationPreferencesPage()
                .getCommunicationPreferences();
        communicationPreferences.verifyStatus(OPT_OUT, OPT_OUT);
        communicationPreferences.verify(ENGLISH);
        verifyThat(communicationPreferences.getSmsMobileTextingMarketingPreferences().getSmsNumberView(),
                hasText("Not Provided"));
        verifyThat(communicationPreferences.getMarketingPreferences().getEmailAddressView(), hasDefault());
    }

    @Test(dependsOnMethods = { "verifyCommPrefsWithoutEmailAndSms" }, alwaysRun = true)
    public void addEmailAndVerifyCommPrefsStatus()
    {
        PersonalInfoPage personalInfoPage = new PersonalInfoPage();
        personalInfoPage.goTo();
        GuestEmail randomEmail = MemberPopulateHelper.getRandomEmail();
        personalInfoPage.getEmailList().add(randomEmail, verifyNoError());

        EmailNotMailableDialog emailNotMailableDialog = new EmailNotMailableDialog();
        if (emailNotMailableDialog.isDisplayed())
        {
            logger.error(
                    "Most likely Email Validation Service is not available. Assuming the email address is correct and continuing enrollment");
            emailNotMailableDialog.clickSaveEmailAsIs();
        }

        CommunicationPreferences communicationPreferences = new CommunicationPreferencesPage()
                .getCommunicationPreferences();
        communicationPreferences.verifyStatus(OPT_IN, OPT_OUT);
        verifyThat(communicationPreferences.getMarketingPreferences().getEmailAddressView(),
                hasText(randomEmail.getEmailDomain()));
    }

    @Test(dependsOnMethods = { "addEmailAndVerifyCommPrefsStatus" }, alwaysRun = true)
    public void addSmsAndVerifyCommPrefsStatus()
    {
        PersonalInfoPage personalInfoPage = new PersonalInfoPage();
        personalInfoPage.goTo();
        personalInfoPage.getSmsList().add(MemberPopulateHelper.getRandomPhone(), verifyNoError());

        CommunicationPreferences communicationPreferences = new CommunicationPreferencesPage()
                .getCommunicationPreferences();
        communicationPreferences.verifyStatus(OPT_IN, OPT_IN);
        verifyThat(communicationPreferences.getSmsMobileTextingMarketingPreferences().getSmsNumberView(),
                hasText("Provided"));
    }
}
