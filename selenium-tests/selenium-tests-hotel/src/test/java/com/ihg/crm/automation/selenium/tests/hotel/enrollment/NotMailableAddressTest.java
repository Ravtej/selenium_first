package com.ihg.crm.automation.selenium.tests.hotel.enrollment;

import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyNoError;
import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifySuccess;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.components.Country.US;
import static com.ihg.automation.selenium.common.member.MemberPopulateHelper.getSimplePersonalInfo;
import static com.ihg.automation.selenium.gwt.components.Mode.VIEW;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.isSelected;
import static com.ihg.automation.selenium.servicecenter.pages.Messages.CONTACT_SUCCESS_UPDATE;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.AddressNotMailableDialog;
import com.ihg.automation.selenium.common.enroll.SuccessEnrolmentPopUp;
import com.ihg.automation.selenium.common.member.GuestAddress;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.types.address.AddressType;
import com.ihg.automation.selenium.common.types.address.Region;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.hotel.pages.LeftPanel;
import com.ihg.automation.selenium.hotel.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.hotel.pages.personal.AddressContact;
import com.ihg.automation.selenium.hotel.pages.personal.HotelAddressContactList;
import com.ihg.automation.selenium.hotel.pages.personal.PersonalInfoPage;
import com.ihg.crm.automation.selenium.tests.hotel.LoginLogout;

public class NotMailableAddressTest extends LoginLogout
{
    private Member member = new Member();
    private GuestAddress firstInvalidAddress;
    private GuestAddress secondInvalidAddress;
    private GuestAddress thirdInvalidAddress;
    private GuestAddress fourthInvalidAddress;
    private HotelAddressContactList addressList;

    @BeforeClass
    public void before()
    {
        member.setPersonalInfo(getSimplePersonalInfo());
        member.addProgram(Program.RC);

        firstInvalidAddress = new GuestAddress();
        firstInvalidAddress.setCountryCode(US);
        firstInvalidAddress.setType(AddressType.RESIDENCE);
        firstInvalidAddress.setPostalCode("12345");
        firstInvalidAddress.setLocality1("New York");
        firstInvalidAddress.setRegion1(Region.NY);
        firstInvalidAddress.setLine1("777 Borsch Ave");
        member.addAddress(firstInvalidAddress);

        secondInvalidAddress = firstInvalidAddress.clone();
        secondInvalidAddress.setPostalCode("66655");
        secondInvalidAddress.setLine1("555 Salo Str");
        secondInvalidAddress.setPreferred(false);

        thirdInvalidAddress = firstInvalidAddress.clone();
        thirdInvalidAddress.setPostalCode("99956");
        thirdInvalidAddress.setLine1("333 Onion Sqr");
        thirdInvalidAddress.setPreferred(false);

        fourthInvalidAddress = firstInvalidAddress.clone();
        fourthInvalidAddress.setPostalCode("10000");
        fourthInvalidAddress.setLine1("111 TestTest Sqr");
        fourthInvalidAddress.setPreferred(false);

        login();
    }

    @Test(priority = 1)
    public void enrollWithNotMailableAddress()
    {
        new LeftPanel().getEnrollment().clickAndWait();

        EnrollmentPage enrollmentPage = new EnrollmentPage();
        enrollmentPage.populate(member, helper);
        enrollmentPage.clickSubmit(verifyNoError());

        new AddressNotMailableDialog().clickSaveAsIs(verifyNoError());

        new SuccessEnrolmentPopUp().clickDone();
    }

    @Test(priority = 5)
    public void verifyNotMailableAddress()
    {
        addressList = new PersonalInfoPage().getAddressList();
        addressList.getExpandedContact().verify(firstInvalidAddress, VIEW);

        AddressContact addressContact = addressList.getContact(1);
        addressContact.clickEdit();

        verifyThat(addressContact.getSaveAsEntered(), isSelected(true));
        addressContact.clickCancel();
    }

    @Test(priority = 10)
    public void addSecondaryNotMailableAddress()
    {
        addressList.getAddButton().clickAndWait();

        AddressContact addressContact = addressList.getContact(2);
        addressContact.populate(secondInvalidAddress);
        addressContact.clickSave();

        new AddressNotMailableDialog().clickSaveAsIs(verifyNoError(), verifySuccess(CONTACT_SUCCESS_UPDATE));

        addressContact.expand();
        addressContact.verify(secondInvalidAddress, VIEW);
    }

    @Test(priority = 15)
    public void updateAddressToNotMailable()
    {
        AddressContact addressContact = addressList.getExpandedContact(2);
        addressContact.clickEdit();
        addressContact.populate(thirdInvalidAddress);
        addressContact.getSaveAsEntered().uncheck();

        addressContact.clickSave();

        new AddressNotMailableDialog().clickSaveAsIs(verifyNoError(), verifySuccess(CONTACT_SUCCESS_UPDATE));

        addressContact.expand();
        addressContact.verify(thirdInvalidAddress, VIEW);
        addressContact.clickEdit();

        verifyThat(addressContact.getSaveAsEntered(), isSelected(true));
        addressContact.clickCancel();
    }

    @Test(priority = 20)
    public void updateAddressAndDoNotUncheckSaveAddress()
    {
        AddressContact addressContact = addressList.getExpandedContact(2);
        addressContact.clickEdit();
        addressContact.populate(fourthInvalidAddress);

        addressContact.clickSave(verifyNoError(), verifySuccess(CONTACT_SUCCESS_UPDATE));
        verifyThat(new AddressNotMailableDialog(), displayed(false));

        addressContact.expand();
        addressContact.verify(fourthInvalidAddress, VIEW);
    }
}
