package com.ihg.crm.automation.selenium.tests.hotel.security;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.hotel.pages.Role.HOTEL_SECURITY;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasAnyText;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.isDisplayedAndEnabled;
import static com.ihg.automation.selenium.matchers.component.GridSize.size;
import static com.ihg.automation.selenium.matchers.component.HasDefaultText.hasDefault;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static org.hamcrest.Matchers.greaterThan;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.event.SearchButtonBar;
import com.ihg.automation.selenium.hotel.pages.LeftPanel;
import com.ihg.automation.selenium.hotel.pages.Role;
import com.ihg.automation.selenium.hotel.pages.security.security.UserManagement;
import com.ihg.automation.selenium.hotel.pages.security.security.UserManagementDetailsPopUp;
import com.ihg.automation.selenium.hotel.pages.security.security.UserManagementPage;
import com.ihg.automation.selenium.hotel.pages.security.security.UserManagementRoleCheckBoxGroup;
import com.ihg.automation.selenium.hotel.pages.security.security.UserManagementSearch;
import com.ihg.crm.automation.selenium.tests.hotel.LoginLogout;

public class UserManagementTest extends LoginLogout
{
    public UserManagementPage userManagementPage;

    @BeforeClass
    public void before()
    {
        login("ATLCP", HOTEL_SECURITY);
    }

    @Test(priority = 10)
    public void verifyUserManagementComponents()
    {
        new LeftPanel().getHotelSecurity().click();

        userManagementPage = new UserManagementPage();
        UserManagementSearch searchFields = userManagementPage.getSearchFields();
        verifyThat(searchFields.getHotel(), hasText(helper.getSourceFactory().getSource().getLocation()));
        verifyThat(searchFields.getFirstName(), hasDefault());
        verifyThat(searchFields.getLastName(), hasDefault());
        verifyThat(searchFields.getJobTitle(), hasDefault());
        verifyThat(searchFields.getRole(), hasText("All"));

        SearchButtonBar buttonBar = userManagementPage.getButtonBar();
        verifyThat(buttonBar.getSearch(), isDisplayedAndEnabled(true));
        verifyThat(buttonBar.getClearCriteria(), isDisplayedAndEnabled(true));

        verifyThat(userManagementPage.getUserManagementGrid(), size(greaterThan(0)));
    }

    @Test(dataProvider = "roleDataProvider", priority = 20)
    public void verifySearchByRole(String role)
    {
        UserManagement userManagement = new UserManagement();
        userManagementPage.searchMemberByRole(userManagement, role);

        verifyThat(userManagementPage.getUserManagementGrid(), size(greaterThan(0)));
        userManagementPage.getButtonBar().clickClearCriteria();
    }

    @Test(dataProvider = "jobAndRoleDataProvider", priority = 30)
    public void verifySearchByRoleAndJob(String jobTitle, String role)
    {
        UserManagement userManagement = new UserManagement();
        userManagement.setJobTitle(jobTitle);
        userManagementPage.searchMemberByRole(userManagement, role);

        verifyThat(userManagementPage.getUserManagementGrid(), size(greaterThan(0)));
        userManagementPage.getButtonBar().clickClearCriteria();
    }

    @Test(priority = 40)
    public void verifyUserDetailsPopUp()
    {
        userManagementPage.getButtonBar().clickSearch();

        userManagementPage.getUserManagementGrid().getRow(1).getUserManagementPopUp();

        UserManagementDetailsPopUp managementDetailsPopUp = new UserManagementDetailsPopUp();
        verifyThat(managementDetailsPopUp, displayed(true));

        verifyThat(managementDetailsPopUp.getUsername(), hasAnyText());
        verifyThat(managementDetailsPopUp.getFirstName(), hasAnyText());
        verifyThat(managementDetailsPopUp.getLastName(), hasAnyText());
        verifyThat(managementDetailsPopUp.getJobTitle(), hasAnyText());
        verifyThat(managementDetailsPopUp.getRcNumberForEmpRate(), hasAnyText());

        UserManagementRoleCheckBoxGroup managementRoleCheckBox = managementDetailsPopUp
                .getManagementRoleCheckBoxGroup();
        verifyThat(managementRoleCheckBox.getHotelBackOffice(), isDisplayedAndEnabled(true));
        verifyThat(managementRoleCheckBox.getHotelFrontDeskStandard(), isDisplayedAndEnabled(true));
        verifyThat(managementRoleCheckBox.getSalesManager(), isDisplayedAndEnabled(true));
        verifyThat(managementRoleCheckBox.getHotelManager(), isDisplayedAndEnabled(true));
        verifyThat(managementRoleCheckBox.getHotelOperationsManager(), isDisplayedAndEnabled(true));
        verifyThat(managementRoleCheckBox.getHotelSecurity(), isDisplayedAndEnabled(true));
    }

    @DataProvider(name = "roleDataProvider")
    protected Object[][] roleProvider()
    {
        return new Object[][] { { Role.ALL }, { Role.ANY_ROLE }, { Role.NO_ROLE }, { Role.HOTEL_BACK_OFFICE },
                { Role.HOTEL_FRONT_DESK_STANDARD }, { Role.HOTEL_FRONT_DESK_FEE_BASED }, { Role.SALES_MANAGER },
                { Role.HOTEL_MANAGER }, { Role.HOTEL_OPERATIONS_MANAGER }, { HOTEL_SECURITY } };
    }

    @DataProvider(name = "jobAndRoleDataProvider")
    protected Object[][] jobAndRoleProvider()
    {
        return new Object[][] { { "Agent Front Desk", Role.NO_ROLE },
                { "Administrative Assistant", Role.HOTEL_MANAGER } };
    }
}
