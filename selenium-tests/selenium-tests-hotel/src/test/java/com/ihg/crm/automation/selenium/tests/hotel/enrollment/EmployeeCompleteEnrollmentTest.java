package com.ihg.crm.automation.selenium.tests.hotel.enrollment;

import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyNoError;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.isDisplayedWithWait;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static org.hamcrest.core.StringEndsWith.endsWith;

import java.util.ArrayList;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.enroll.EnrollProgramContainer;
import com.ihg.automation.selenium.common.enroll.SuccessEnrolmentPopUp;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.personal.PhoneContact;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.common.types.program.ProgramStatus;
import com.ihg.automation.selenium.gwt.components.Mode;
import com.ihg.automation.selenium.gwt.components.panel.FieldSet;
import com.ihg.automation.selenium.hotel.pages.LeftPanel;
import com.ihg.automation.selenium.hotel.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.hotel.pages.personal.HotelEmailContact;
import com.ihg.automation.selenium.hotel.pages.personal.PersonalInfoPage;
import com.ihg.automation.selenium.hotel.pages.programs.EmployeeProgramPage;
import com.ihg.automation.selenium.hotel.pages.programs.ProgramSummary;
import com.ihg.crm.automation.selenium.tests.hotel.LoginLogout;

public class EmployeeCompleteEnrollmentTest extends LoginLogout
{
    private Member member = new Member();
    private EnrollmentPage enrollmentPage = new EnrollmentPage();

    @BeforeClass
    public void beforeClass()
    {
        member.addProgram(Program.EMP);
        member.addEmail(MemberPopulateHelper.getRandomEmail());
        member.addPhone(MemberPopulateHelper.getRandomPhone());
        member.addAddress(MemberPopulateHelper.getUnitedStatesAddress());
        member.setPersonalInfo(MemberPopulateHelper.getSimplePersonalInfo());

        login();
    }

    @Test
    public void additionalInfoCheckBoxVerify()
    {
        enrollmentPage.goTo();
        enrollmentPage.populate(member);
        verifyThat(enrollmentPage.getAdditionalInformation().getEMPMemberID(), displayed(true));
    }

    @Test(dependsOnMethods = { "additionalInfoCheckBoxVerify" }, alwaysRun = true)
    public void verifySuccessEnrollmentPopUp()
    {
        enrollmentPage.clickSubmitAndSkipWarnings(verifyNoError());

        SuccessEnrolmentPopUp popUp = new SuccessEnrolmentPopUp();
        verifyThat(popUp, isDisplayedWithWait());
        popUp.clickDone(verifyNoError());
    }

    @Test(dependsOnMethods = { "verifySuccessEnrollmentPopUp" }, alwaysRun = true)
    public void validatePersonalInformation()
    {
        PersonalInfoPage personalInfoPage = new PersonalInfoPage();

        personalInfoPage.getCustomerInfo().verify(member, Mode.VIEW);
        personalInfoPage.getAddressList().getExpandedContact().verify(member.getPreferredAddress(), Mode.VIEW);

        PhoneContact phone = personalInfoPage.getPhoneList().getExpandedContact();
        phone.verify(member.getPreferredPhone(), Mode.VIEW);
        verifyThat(phone.getPreferredIcon(), displayed(true));

        HotelEmailContact email = personalInfoPage.getEmailList().getExpandedContact();
        email.verify(member.getPreferredEmail(), Mode.VIEW);
        verifyThat(email.getPreferredIcon(), displayed(true));
    }

    @Test(dependsOnMethods = { "validatePersonalInformation" }, alwaysRun = true)
    public void verifyOverview()
    {
        EmployeeProgramPage employeeProgramPage = new EmployeeProgramPage();
        new LeftPanel().getProgramInfoPanel().getPrograms().getProgram(Program.EMP).goToProgramPage();
        verifyThat(employeeProgramPage, displayed(true));

        FieldSet programOverview = employeeProgramPage.getProgramOverview();
        programOverview.expand();
        verifyThat(programOverview, hasText(endsWith("Employee program")));
    }

    @Test(dependsOnMethods = { "verifyOverview" }, alwaysRun = true)
    public void verifySummary()
    {
        ProgramSummary programSummary = new EmployeeProgramPage().getSummary();
        verifyThat(programSummary.getStatus(), hasText(ProgramStatus.OPEN));
        verifyThat(programSummary.getMemberId(), hasText(member.getEMPProgramId()));
    }

    @Test(dependsOnMethods = { "verifySummary" }, alwaysRun = true)
    public void verifyEnrollmentDetails()
    {
        new EmployeeProgramPage().getEnrollmentDetails().verifyEmployeeDetails(helper);
    }

    @Test(dependsOnMethods = { "verifyEnrollmentDetails" }, alwaysRun = true)
    public void tryToEnrollToAnotherProgram()
    {
        EnrollmentPage enrollmentPage = new EnrollmentPage();
        enrollmentPage.goTo();

        EnrollProgramContainer enrollProgramContainer = enrollmentPage.getPrograms();

        ArrayList<Program> programs = Program.getProgramList();
        programs.remove(Program.EMP);

        for (Program pr : programs)
        {
            verifyThat(enrollProgramContainer.getProgramCheckbox(pr), displayed(false));
            verifyThat(enrollProgramContainer.getProgramChecked(pr), displayed(false));
        }
        verifyThat(enrollProgramContainer.getProgramChecked(Program.EMP), displayed(true));
    }
}
