package com.ihg.crm.automation.selenium.tests.hotel.operations.reimbursement;

import static com.ihg.automation.common.DateUtils.DATE_FORMAT;
import static com.ihg.automation.common.DateUtils.currentYear;
import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyError;
import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyNoError;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.hotel.pages.hoteloperations.reimbursement.certificate.CertificateSearchGridRow.CertificateSearchGridCell.CERTIFICATE_TYPE;
import static com.ihg.automation.selenium.hotel.pages.hoteloperations.reimbursement.certificate.CertificateSearchGridRow.CertificateSearchGridCell.CHECK_IN;
import static com.ihg.automation.selenium.hotel.pages.hoteloperations.reimbursement.certificate.CertificateSearchGridRow.CertificateSearchGridCell.CHECK_OUT;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasAnyText;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasTextAsDate;
import static com.ihg.automation.selenium.matchers.component.GridSize.size;
import static com.ihg.automation.selenium.matchers.component.HasDefaultText.hasDefault;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static com.ihg.automation.selenium.matchers.component.HasTipText.hasWarningTipTextWithWait;
import static com.ihg.automation.selenium.matchers.component.IsSelected.isSelected;
import static com.ihg.automation.selenium.matchers.date.IsAfterOrEqualsDate.isAfterOrEquals;
import static com.ihg.automation.selenium.matchers.date.IsBeforeOrEqualsDate.isBeforeOrEquals;
import static com.ihg.automation.selenium.servicecenter.pages.Messages.HIGHLIGHTED_FIELDS_DID_NOT_PASS_VALIDATION;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNot.not;
import static org.joda.time.LocalDate.now;

import java.util.List;

import org.joda.time.LocalDate;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.ihg.automation.common.DateUtils;
import com.ihg.automation.selenium.common.event.SearchButtonBar;
import com.ihg.automation.selenium.gwt.components.input.DateInput;
import com.ihg.automation.selenium.hotel.pages.LeftPanel;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.HotelOperationsPage;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.reimbursement.certificate.CertificateGridRowContainer;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.reimbursement.certificate.CertificateSearch;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.reimbursement.certificate.CertificateSearchGrid;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.reimbursement.certificate.CertificateSearchGridRow;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.reimbursement.certificate.CertificateSearchPage;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.reimbursement.certificate.ResultsFilter;
import com.ihg.crm.automation.selenium.tests.hotel.LoginLogout;

public class CertificateSearchTest extends LoginLogout
{
    private static final String DATE_WARNING_RANGE = "Date Range should not be longer than 6 months.";
    private static final String EMPTY_FIELDS_WARNING = "At least Certificate Number or Date Range criteria should be defined";
    private static final int INVALID_MONTHS_QUANTITY = 7;
    private static final LocalDate VALID_FROM_DATE = now().minusDays(50);
    private static final LocalDate VALID_TO_DATE = now().plusDays(20);

    private CertificateSearch certificateSearch;
    private CertificateSearchPage certificateSearchPage;
    private CertificateSearchGrid grid;

    @BeforeClass
    public void before()
    {
        login();

        new LeftPanel().getHotelOperations().clickAndWait(verifyNoError());
        new HotelOperationsPage().getSearchRewardFreeNightsStayByCertificate().clickAndWait(verifyNoError());
    }

    @Test(priority = 1)
    public void verifyPageComponents()
    {
        certificateSearchPage = new CertificateSearchPage();
        certificateSearch = certificateSearchPage.getSearchFields();

        verifyThat(certificateSearch.getHotelCode(), hasText(helper.getUser().getLocation()));
        verifyThat(certificateSearch.getCertificateNumber(), hasDefault());
        verifyThat(certificateSearch.getCheckIn(), isSelected(false));
        verifyThat(certificateSearch.getCheckOut(), isSelected(true));

        DateInput fromDate = certificateSearch.getFromDate();
        if (DateUtils.now().getDayOfMonth() < 26)
        {
            verifyThat(fromDate, hasText(
                    new LocalDate(currentYear(), now().minusMonths(1).getMonthOfYear(), 26).toString(DATE_FORMAT)));
        }
        else
        {
            verifyThat(fromDate,
                    hasText(new LocalDate(currentYear(), now().getMonthOfYear(), 26).toString(DATE_FORMAT)));
        }

        verifyThat(certificateSearch.getToDate(), hasText(now().toString(DATE_FORMAT)));
        verifyThat(certificateSearch.getType(), hasText("All"));

        ResultsFilter resultsFilter = certificateSearchPage.getResultsFilter();
        resultsFilter.expand();
        verifyThat(resultsFilter.getGuestName(), hasText("Guest Name"));
        verifyThat(resultsFilter.getConfirmation(), hasText("Confirmation#"));
        verifyThat(resultsFilter.getMemberNumber(), hasText("Member Number"));
    }

    @Test(priority = 5)
    public void verifyFirstRow()
    {
        grid = certificateSearchPage.getCertificateSearchGrid();
        verifyThat(grid, size(not(0)));

        CertificateGridRowContainer rowContainer = grid.getRow(1).expand(CertificateGridRowContainer.class);
        verifyThat(rowContainer.getCertificateType(), hasAnyText());
        verifyThat(rowContainer.getMemberID(), hasAnyText());
        verifyThat(rowContainer.getLastName(), hasAnyText());
        verifyThat(rowContainer.getCertificateNumber(), hasAnyText());
        verifyThat(rowContainer.getSuffix(), hasAnyText());
        verifyThat(rowContainer.getStatus(), hasAnyText());
        verifyThat(rowContainer.getOfferName(), hasAnyText());
        verifyThat(rowContainer.getReimbursementType(), hasAnyText());
        verifyThat(rowContainer.getReimbursementMethod(), hasAnyText());
        verifyThat(rowContainer.getReimbursementDate(), hasAnyText());
        verifyThat(rowContainer.getCheckIn(), hasAnyText());
        verifyThat(rowContainer.getCheckOut(), hasAnyText());
        verifyThat(rowContainer.getTotalActiveNights(), hasAnyText());
        verifyThat(rowContainer.getTotalReimbursed(), hasAnyText());
    }

    @Test(priority = 10)
    public void tryToSearchUsingInvalidMonthsQuantityRange()
    {
        DateInput fromDate = certificateSearch.getFromDate();
        fromDate.type(now().minusMonths(INVALID_MONTHS_QUANTITY).toString(DATE_FORMAT));

        SearchButtonBar buttonBar = certificateSearchPage.getButtonBar();
        buttonBar.clickSearch();

        verifyError(HIGHLIGHTED_FIELDS_DID_NOT_PASS_VALIDATION);
        verifyThat(fromDate, hasWarningTipTextWithWait(DATE_WARNING_RANGE));

        verifyThat(certificateSearch.getToDate(), hasWarningTipTextWithWait(DATE_WARNING_RANGE));

        buttonBar.clickClearCriteria();
    }

    @Test(priority = 13)
    public void tryToSearchWithEmptyDatesRange()
    {
        DateInput fromDate = certificateSearch.getFromDate();
        fromDate.clear();

        DateInput toDate = certificateSearch.getToDate();
        toDate.clear();

        SearchButtonBar buttonBar = certificateSearchPage.getButtonBar();
        buttonBar.clickSearch();

        verifyError(HIGHLIGHTED_FIELDS_DID_NOT_PASS_VALIDATION);
        verifyThat(fromDate, hasWarningTipTextWithWait(EMPTY_FIELDS_WARNING));
        verifyThat(toDate, hasWarningTipTextWithWait(EMPTY_FIELDS_WARNING));
        verifyThat(certificateSearch.getCertificateNumber(), hasWarningTipTextWithWait(EMPTY_FIELDS_WARNING));

        buttonBar.clickClearCriteria();
    }

    @Test(priority = 15)
    public void verifyDatesRangeInvalid()
    {
        certificateSearch.getFromDate().type(now().plusDays(15).toString(DATE_FORMAT));

        DateInput toDate = certificateSearch.getToDate();
        toDate.type(now().minusDays(14).toString(DATE_FORMAT));
        certificateSearchPage.getButtonBar().clickSearch();

        verifyError(HIGHLIGHTED_FIELDS_DID_NOT_PASS_VALIDATION);
        verifyThat(toDate, hasWarningTipTextWithWait("Date To should not be before Date From."));

        certificateSearchPage.getButtonBar().clickClearCriteria();
    }

    @Test(priority = 17)
    public void verifyValidDateRange()
    {
        certificateSearch.getFromDate().type(VALID_FROM_DATE.toString(DATE_FORMAT));
        certificateSearch.getToDate().type(VALID_TO_DATE.toString(DATE_FORMAT));
        certificateSearchPage.getButtonBar().clickSearch();

        List<CertificateSearchGridRow> gridRowList = grid.getRowList();
        for (CertificateSearchGridRow row : gridRowList)
        {
            verifyThat(row.getCell(CHECK_IN), hasTextAsDate("ddMMMYY", isAfterOrEquals(VALID_FROM_DATE)));
            verifyThat(row.getCell(CHECK_OUT), hasTextAsDate("ddMMMYY", isBeforeOrEquals(VALID_TO_DATE)));
        }
    }

    @Test(priority = 20, dataProvider = "typesProvider")
    public void verifySearchByType(String reimbursementType)
    {
        certificateSearch.getType().select(reimbursementType);

        SearchButtonBar buttonBar = certificateSearchPage.getButtonBar();
        buttonBar.clickSearch();

        grid = certificateSearchPage.getCertificateSearchGrid();
        verifyThat(grid, size(is(greaterThan(0))));

        List<CertificateSearchGridRow> gridRowList = grid.getRowList();
        for (CertificateSearchGridRow row : gridRowList)
        {
            verifyThat(row.getCell(CERTIFICATE_TYPE), hasText(reimbursementType));
        }

        buttonBar.clickClearCriteria();
    }

    @DataProvider(name = "typesProvider")
    public Object[][] typesDataProvider()
    {
        return new Object[][] { { "Free Night" }, { "Reward Night" } };
    }
}
