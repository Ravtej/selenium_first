package com.ihg.crm.automation.selenium.tests.hotel.refdata;

import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyNoError;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.components.Country.BY;
import static com.ihg.automation.selenium.common.components.Country.CA;
import static com.ihg.automation.selenium.common.components.Country.CN;
import static com.ihg.automation.selenium.common.components.Country.US;
import static com.ihg.automation.selenium.common.types.Currency.BYR;
import static com.ihg.automation.selenium.common.types.Currency.EUR;
import static com.ihg.automation.selenium.common.types.Currency.GBP;
import static com.ihg.automation.selenium.common.types.Currency.USD;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasText;
import static com.ihg.automation.selenium.matchers.text.EntityMatcherFactory.isValue;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.components.Country;
import com.ihg.automation.selenium.common.components.CountrySelect;
import com.ihg.automation.selenium.common.contact.Address;
import com.ihg.automation.selenium.common.event.SearchButtonBar;
import com.ihg.automation.selenium.common.hotel.AdvancedHotelSearchGridRowContainer;
import com.ihg.automation.selenium.common.hotel.AdvancedHotelSearchPopUp;
import com.ihg.automation.selenium.common.hotel.HotelAddress;
import com.ihg.automation.selenium.common.hotel.HotelDetails;
import com.ihg.automation.selenium.common.pages.login.TopUserSection;
import com.ihg.automation.selenium.common.types.Currency;
import com.ihg.automation.selenium.common.types.address.RegionLabel;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.gwt.components.input.Select;
import com.ihg.automation.selenium.hotel.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.hotel.pages.search.GuestSearch;
import com.ihg.automation.selenium.hotel.pages.search.GuestSearchByName;
import com.ihg.automation.selenium.hotel.pages.search.GuestSearchByNumber;
import com.ihg.crm.automation.selenium.tests.hotel.LoginLogout;

public class VerifyCountryAndCurrencyTest extends LoginLogout
{
    @BeforeClass
    public void before()
    {
        login();
    }

    @Test(priority = 10)
    public void verifyCountryOnEnrollmentPage()
    {
        EnrollmentPage enrollmentPage = new EnrollmentPage();
        enrollmentPage.goTo();
        enrollmentPage.getPrograms().getProgramCheckbox(Program.RC).check();

        CountrySelect residenceCountry = enrollmentPage.getCustomerInfo().getResidenceCountry();
        residenceCountry.select(CN);
        verifyThat(residenceCountry, hasText(isValue(CN)));

        Address address = enrollmentPage.getAddress();
        CountrySelect country = address.getCountry();
        country.select(US);
        verifyThat(country, hasText(isValue(US)));

        Select region1 = address.getRegion1(RegionLabel.STATE);
        region1.select("TX - Texas");
        verifyThat(region1, hasText("Texas"));

        enrollmentPage.clickCancel();
    }

    @Test(priority = 20)
    public void verifyCountryOnWelcomeScreen()
    {
        Select country = new GuestSearchByName().getCountry();
        country.select(Country.BY.getCodeWithValue());
        verifyThat(country, hasText(isValue(BY)));
    }

    @Test(priority = 30)
    public void verifyCountryOnCustomerSearchPanel()
    {
        new GuestSearchByNumber().getAdvancedSearch().clickAndWait(verifyNoError());

        CountrySelect country = new GuestSearch().getCustomerSearch().getCountry();
        verifyThat(country, hasText(isValue(US)));

        country.select(CA);
        verifyThat(country, hasText(isValue(CA)));
    }

    @Test(priority = 40)
    public void verifyCountryAndStateOnAdvancedHotelSearch() throws InterruptedException
    {
        TopUserSection topUserSection = new TopUserSection();
        topUserSection.getHotel().clickButton();

        HotelAddress hotelAddress = new AdvancedHotelSearchPopUp().getSearchFields().getHotelAddress();
        hotelAddress.populateCountryAndState(US, "GA - Georgia");

        verifyThat(hotelAddress.getCountry(), hasText(isValue(US)));
        verifyThat(hotelAddress.getState(), hasText("Georgia"));
    }

    @Test(dataProvider = "hotelCurrencyProvider", priority = 50)
    public void verifyCurrencyOnAdvancedHotelSearch(String hotelCode, Currency countryCurrency, Currency hotelCurrency)
    {
        AdvancedHotelSearchPopUp hotelSearchPopUp = new AdvancedHotelSearchPopUp();

        SearchButtonBar buttonBar = hotelSearchPopUp.getButtonBar();
        buttonBar.clickClearCriteria();
        hotelSearchPopUp.getSearchFields().getHotel().type(hotelCode);
        buttonBar.clickSearch();

        HotelDetails hotelDetails = hotelSearchPopUp.getHotelSearchGrid().getRow(1)
                .expand(AdvancedHotelSearchGridRowContainer.class).getHotelDetails();

        verifyThat(hotelDetails.getCountryCurrency(), hasText(countryCurrency.getCode()));
        verifyThat(hotelDetails.getHotelCurrency(), hasText(hotelCurrency.getCode()));
    }

    @DataProvider(name = "hotelCurrencyProvider")
    public Object[][] verifyHotelCurrencyProvider()
    {
        return new Object[][] { { "BODHA", EUR, EUR }, { "LONHB", GBP, GBP }, { "ATLCP", USD, USD },
                { "MSQMK", BYR, USD } };
    }
}
