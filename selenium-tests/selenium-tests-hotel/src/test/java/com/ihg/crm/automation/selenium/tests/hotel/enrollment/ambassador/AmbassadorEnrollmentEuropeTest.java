package com.ihg.crm.automation.selenium.tests.hotel.enrollment.ambassador;

import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyNoError;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.assertThat;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.enabled;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasText;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.isDisplayedAndEnabled;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.isDisplayedWithWait;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.isSelected;
import static org.hamcrest.CoreMatchers.allOf;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.member.AmbassadorUtils;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.personal.PhoneContact;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.gwt.components.Mode;
import com.ihg.automation.selenium.gwt.components.checkbox.CheckBox;
import com.ihg.automation.selenium.hotel.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.hotel.pages.personal.HotelEmailContact;
import com.ihg.automation.selenium.hotel.pages.personal.PersonalInfoPage;
import com.ihg.automation.selenium.hotel.pages.programs.amb.PrivacyConfirmationPopUp;
import com.ihg.automation.selenium.hotel.pages.programs.amb.PurchaseDetailsPopUp;
import com.ihg.automation.selenium.hotel.pages.programs.amb.SuccessAmbEnrollmentPopUp;
import com.ihg.automation.selenium.matchers.component.HasText;
import com.ihg.crm.automation.selenium.tests.hotel.LoginLogout;

public class AmbassadorEnrollmentEuropeTest extends LoginLogout
{
    private Member member = new Member();
    private static final String HOTEL = "CEQHA";

    @BeforeClass
    public void beforeClass()
    {
        member.addProgram(Program.AMB);
        member.addEmail(MemberPopulateHelper.getRandomEmail());
        member.addPhone(MemberPopulateHelper.getRandomPhone());
        member.addAddress(MemberPopulateHelper.getUnitedStatesAddress());
        member.setPersonalInfo(MemberPopulateHelper.getSimplePersonalInfo());

        login(HOTEL);

        EnrollmentPage enrollmentPage = new EnrollmentPage();
        enrollmentPage.goTo();
        enrollmentPage.populate(member);
        enrollmentPage.getYourEmployeeID().type(helper.getUser().getEmployeeId());
        enrollmentPage.clickSubmitAndSkipWarnings(verifyNoError());
    }

    @Test
    public void checkPrivacyConfirmationPopUpElements()
    {
        PrivacyConfirmationPopUp privacyConfirmationPopUp = new PrivacyConfirmationPopUp();
        verifyThat(privacyConfirmationPopUp, displayed(true));
        verifyThat(privacyConfirmationPopUp.getAgreementText(), hasText(PrivacyConfirmationPopUp.PRIVACY_AGREEMENT));
        CheckBox accepted = privacyConfirmationPopUp.getAccepted();
        verifyThat(accepted, isDisplayedAndEnabled(true));
        verifyThat(accepted, isSelected(false));
        verifyThat(privacyConfirmationPopUp.getCancel(), isDisplayedAndEnabled(true));
        verifyThat(privacyConfirmationPopUp.getConfirm(), allOf(displayed(true), enabled(false)));
    }

    @Test(dependsOnMethods = { "checkPrivacyConfirmationPopUpElements" }, alwaysRun = true)
    public void acceptAgreement()
    {
        PrivacyConfirmationPopUp privacyConfirmationPopUp = new PrivacyConfirmationPopUp();
        privacyConfirmationPopUp.getAccepted().check();
        assertThat(privacyConfirmationPopUp.getAccepted(), isDisplayedAndEnabled(true));
        privacyConfirmationPopUp.getConfirm().clickAndWait(verifyNoError());
    }

    @Test(dependsOnMethods = { "acceptAgreement" }, alwaysRun = true)
    public void verifySuccessEnrollmentPopUp()
    {
        PurchaseDetailsPopUp purchaseDetailsPopUp = new PurchaseDetailsPopUp();
        purchaseDetailsPopUp.selectAmount(AmbassadorUtils.PURCHASE_AMBASSADOR_HOTEL_$200);
        purchaseDetailsPopUp.clickSubmit(verifyNoError());

        SuccessAmbEnrollmentPopUp popUp = new SuccessAmbEnrollmentPopUp();
        verifyThat(popUp, isDisplayedWithWait());
        verifyThat(popUp.getMemberId(Program.RC), HasText.hasText(popUp.getMemberId(Program.AMB).getText()));
        verifyThat(popUp.getPaymentMessage(), displayed(true));
        popUp.clickDone(verifyNoError());
    }

    @Test(dependsOnMethods = { "verifySuccessEnrollmentPopUp" }, alwaysRun = true)
    public void verifyPersonalData()
    {
        {
            PersonalInfoPage personalInfoPage = new PersonalInfoPage();

            personalInfoPage.getCustomerInfo().verify(member, Mode.VIEW);
            personalInfoPage.getAddressList().getExpandedContact().verify(member.getPreferredAddress(), Mode.VIEW);

            PhoneContact phone = personalInfoPage.getPhoneList().getExpandedContact();
            phone.verify(member.getPreferredPhone(), Mode.VIEW);
            verifyThat(phone.getPreferredIcon(), displayed(true));

            HotelEmailContact email = personalInfoPage.getEmailList().getExpandedContact();
            email.verify(member.getPreferredEmail(), Mode.VIEW);
            verifyThat(email.getPreferredIcon(), displayed(true));
        }
    }
}
