package com.ihg.crm.automation.selenium.tests.hotel.security;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.Constant.NOT_AVAILABLE;
import static com.ihg.automation.selenium.hotel.pages.Role.HOTEL_SECURITY;
import static com.ihg.automation.selenium.hotel.pages.security.security.UserManagementGridRow.UserManagementGridCell.FIRST_NAME;
import static com.ihg.automation.selenium.hotel.pages.security.security.UserManagementGridRow.UserManagementGridCell.LAST_NAME;
import static com.ihg.automation.selenium.matchers.component.GridSize.size;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static org.hamcrest.Matchers.greaterThan;

import java.util.List;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.event.SearchButtonBar;
import com.ihg.automation.selenium.gwt.components.input.Input;
import com.ihg.automation.selenium.hotel.pages.LeftPanel;
import com.ihg.automation.selenium.hotel.pages.security.security.UserManagement;
import com.ihg.automation.selenium.hotel.pages.security.security.UserManagementGrid;
import com.ihg.automation.selenium.hotel.pages.security.security.UserManagementGridRow;
import com.ihg.automation.selenium.hotel.pages.security.security.UserManagementGridRow.UserManagementGridCell;
import com.ihg.automation.selenium.hotel.pages.security.security.UserManagementPage;
import com.ihg.automation.selenium.hotel.pages.security.security.UserManagementSearch;
import com.ihg.crm.automation.selenium.tests.hotel.LoginLogout;

public class UserSearchTest extends LoginLogout
{
    private UserManagementPage userManagementPage = new UserManagementPage();
    private UserManagementSearch searchFields = userManagementPage.getSearchFields();
    private SearchButtonBar searchButtonBar = userManagementPage.getButtonBar();
    private UserManagementGrid userManagementGrid = userManagementPage.getUserManagementGrid();
    private UserManagement userManagement = new UserManagement();

    private static final String USER_NAME = "mark";
    private static final String USER_SURNAME = "baker";
    private static final String HOTEL = "ATLCP";
    private static final String USERNAME = "GLOBAL\\BAKERM66";

    @BeforeClass
    public void before()
    {
        userManagement.setHotel(HOTEL);
        userManagement.setFirstName(USER_NAME);
        userManagement.setLastName(USER_SURNAME);
        userManagement.setJobTitle("General Manager");
        userManagement.setUsername(USERNAME);
        userManagement.setRcNumberForEmpRate(NOT_AVAILABLE);

        login(HOTEL, HOTEL_SECURITY);

        new LeftPanel().getHotelSecurity().click();
    }

    @AfterMethod
    public void after()
    {
        searchButtonBar.clickClearCriteria();
    }

    @DataProvider(name = "searchFieldsProvider")
    public Object[][] searchFieldsDataProvider()
    {
        return new Object[][] { { searchFields.getFirstName(), USER_NAME, FIRST_NAME },
                { searchFields.getLastName(), USER_SURNAME, LAST_NAME } };
    }

    @Test(dataProvider = "searchFieldsProvider")
    public void searchUserByName(Input fieldName, String name, UserManagementGridCell cell)
    {
        fieldName.type(name);
        searchButtonBar.clickSearch();

        verifyThat(userManagementGrid, size(greaterThan(0)));

        List<UserManagementGridRow> gridRowList = userManagementGrid.getRowList();
        for (UserManagementGridRow row : gridRowList)
        {
            verifyThat(row.getCell(cell), hasText(name));
        }
    }

    @Test
    public void searchByUserId()
    {
        searchFields.getJobTitle().type(userManagement.getJobTitle());
        searchButtonBar.clickSearch();

        userManagementGrid.getRowByUserName(userManagement).verify(userManagement);
    }
}
