package com.ihg.crm.automation.selenium.tests.hotel.enrollment.ambassador;

import static com.ihg.automation.common.DateUtils.DATE_FORMAT;
import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyNoError;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.member.AmbassadorUtils.PURCHASE_AMBASSADOR_HOTEL_$200;
import static com.ihg.automation.selenium.common.member.MemberPopulateHelper.getRandomEmail;
import static com.ihg.automation.selenium.common.member.MemberPopulateHelper.getRandomPhone;
import static com.ihg.automation.selenium.common.member.MemberPopulateHelper.getSimplePersonalInfo;
import static com.ihg.automation.selenium.common.member.MemberPopulateHelper.getUnitedStatesAddress;
import static com.ihg.automation.selenium.common.types.program.AmbassadorLevel.AMB;
import static com.ihg.automation.selenium.common.types.program.ProgramStatus.CLOSED;
import static com.ihg.automation.selenium.common.types.program.ProgramStatus.OPEN;
import static com.ihg.automation.selenium.gwt.components.Mode.EDIT;
import static com.ihg.automation.selenium.gwt.components.Mode.VIEW;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.enabled;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasDefault;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasText;
import static com.ihg.automation.selenium.matchers.text.EntityMatcherFactory.isValue;
import static org.hamcrest.core.AllOf.allOf;

import org.springframework.beans.factory.annotation.Value;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.common.DateUtils;
import com.ihg.automation.selenium.common.enroll.CustomerInfo;
import com.ihg.automation.selenium.common.member.AmbassadorJdbcUtils;
import com.ihg.automation.selenium.common.member.AmbassadorUtils;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.hotel.pages.LeftPanel;
import com.ihg.automation.selenium.hotel.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.hotel.pages.programs.amb.AmbassadorPage;
import com.ihg.automation.selenium.hotel.pages.programs.amb.AmbassadorSummary;
import com.ihg.automation.selenium.hotel.pages.programs.amb.PurchaseDetailsPopUp;
import com.ihg.automation.selenium.hotel.pages.programs.amb.SuccessAmbEnrollmentPopUp;
import com.ihg.crm.automation.selenium.tests.hotel.LoginLogout;

public class ReactivateAmbassadorTest extends LoginLogout
{
    private EnrollmentPage enrollmentPage;
    private Member member = new Member();

    private static final String HOTEL = "CEQHA";
    private int daysShift;

    @Value("${lastUpdateUserId}")
    private String lastUpdateUserId;

    @BeforeClass
    public void beforeClass()
    {
        member.addProgram(Program.AMB);
        member.setAmbassadorAmount(PURCHASE_AMBASSADOR_HOTEL_$200);
        member.setPersonalInfo(getSimplePersonalInfo());
        member.addAddress(getUnitedStatesAddress());
        member.addEmail(getRandomEmail());
        member.addPhone(getRandomPhone());

        login(HOTEL);

        new EnrollmentPage().enroll(member, helper, true);

        daysShift = new AmbassadorJdbcUtils(jdbcTemplateUpdate, lastUpdateUserId).updateForReactivate(member);
    }

    @Test(priority = 10)
    public void verifyAmbSummaryBeforeReactivate()
    {
        verifyAmbSummary(DateUtils.getFormattedDate(daysShift, DATE_FORMAT), CLOSED);
    }

    @Test(priority = 20)
    public void verifyMembersPersonalData()
    {
        new LeftPanel().getEnrollment().clickAndWait();
        enrollmentPage = new EnrollmentPage();
        enrollmentPage.getPrograms().getProgramCheckbox(Program.AMB).clickAndWait();

        CustomerInfo customerInfo = enrollmentPage.getCustomerInfo();
        verifyThat(customerInfo.getResidenceCountry(), hasText(isValue(member.getPreferredAddress().getCountry())));
        verifyThat(customerInfo.getName().getFullName(), hasText(member.getName().getFullName()));

        enrollmentPage.getAddress().verify(member.getPreferredAddress(), EDIT);
        enrollmentPage.getPhone().verify(member.getPreferredPhone(), EDIT);
        enrollmentPage.getEmail().verifyLcEmail(member.getPreferredEmail(), VIEW);

        verifyThat(enrollmentPage.getHotelCode(), allOf(enabled(false), hasText(HOTEL)));
        verifyThat(enrollmentPage.getYourEmployeeID(), hasDefault());
    }

    @Test(priority = 30)
    public void reactivateAmbassadorMember()
    {
        enrollmentPage.getYourEmployeeID().type(helper.getUser().getEmployeeId());
        enrollmentPage.clickSubmitAndSkipWarnings(verifyNoError());

        PurchaseDetailsPopUp detailsPopUp = new PurchaseDetailsPopUp();
        detailsPopUp.selectAmount(PURCHASE_AMBASSADOR_HOTEL_$200);
        detailsPopUp.clickSubmit(verifyNoError());

        SuccessAmbEnrollmentPopUp popUp = new SuccessAmbEnrollmentPopUp();
        verifyThat(popUp.getMemberId(Program.AMB), hasText(member.getRCProgramId()));
        verifyThat(popUp.getPaymentMessage(), displayed(true));

        popUp.clickDone();
    }

    @Test(priority = 40)
    public void verifyAmbSummaryAfterReactivate()
    {
        verifyAmbSummary(AmbassadorUtils.getExpirationDate(), OPEN);
    }

    private void verifyAmbSummary(String expirationDate, String programStatus)
    {
        AmbassadorPage ambassadorPage = new AmbassadorPage();
        ambassadorPage.goTo();

        AmbassadorSummary summary = ambassadorPage.getSummary();
        summary.verify(expirationDate, programStatus, AMB, member.getRCProgramId());
        verifyThat(summary.getRenew(), enabled(false));
        verifyThat(ambassadorPage.getEnrollmentDetails().getRenewalDate(), hasDefault());
    }
}
