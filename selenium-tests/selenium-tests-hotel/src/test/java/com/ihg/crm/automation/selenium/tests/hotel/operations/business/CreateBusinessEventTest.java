package com.ihg.crm.automation.selenium.tests.hotel.operations.business;

import static com.ihg.automation.common.DateUtils.getFormattedDate;
import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyError;
import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyNoError;
import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifySuccess;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.Constant.NOT_AVAILABLE;
import static com.ihg.automation.selenium.common.business.BusinessEventType.INCENTIVE;
import static com.ihg.automation.selenium.common.business.BusinessStatus.BLOCKED;
import static com.ihg.automation.selenium.common.types.Currency.USD;
import static com.ihg.automation.selenium.hotel.pages.Role.SALES_MANAGER;
import static com.ihg.automation.selenium.matchers.component.GridSize.size;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static com.ihg.automation.selenium.matchers.component.IsEnabled.enabled;
import static org.hamcrest.core.IsNot.not;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.business.BusinessEvent;
import com.ihg.automation.selenium.common.business.BusinessOffer;
import com.ihg.automation.selenium.common.business.BusinessRevenueBean;
import com.ihg.automation.selenium.common.business.BusinessRevenueView;
import com.ihg.automation.selenium.common.business.BusinessRewardsEventDetailsPopUp;
import com.ihg.automation.selenium.common.business.DiscretionaryPoints;
import com.ihg.automation.selenium.common.business.EventSummaryTab;
import com.ihg.automation.selenium.common.business.GuestRoom;
import com.ihg.automation.selenium.common.business.GuestRoomsFields;
import com.ihg.automation.selenium.common.business.MeetingDetails;
import com.ihg.automation.selenium.common.business.MeetingFields;
import com.ihg.automation.selenium.common.business.MeetingRevenueDetails;
import com.ihg.automation.selenium.common.business.OffersFieldsEdit;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.pages.login.TopUserSection;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.gwt.components.dialog.ConfirmDialog;
import com.ihg.automation.selenium.hotel.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.br.manage.ManageEventsGrid;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.br.manage.ManageEventsPage;
import com.ihg.crm.automation.selenium.tests.hotel.LoginLogout;

public class CreateBusinessEventTest extends LoginLogout
{
    private static final String OFFER_NAME = "TEST OFFER DON'T MIGRATE";
    private static final String HOTEL = "ATLFX";
    private BusinessRewardsEventDetailsPopUp popUp;
    private BusinessRevenueView totalEligibleRevenue;
    private EventSummaryTab eventSummaryTab;
    private BusinessEvent businessEvent;

    @BeforeClass
    public void beforeClass()
    {
        Member member = new Member();
        member.addProgram(Program.BR);
        member.setPersonalInfo(MemberPopulateHelper.getSimplePersonalInfo());
        member.addAddress(MemberPopulateHelper.getUnitedStatesAddress());

        businessEvent = new BusinessEvent("NewEvent001", HOTEL, USD);
        businessEvent.setStatus(BLOCKED);

        MeetingDetails meetingDetails = new MeetingDetails();
        meetingDetails.setStartDate(getFormattedDate(-7));
        meetingDetails.setEndDate(getFormattedDate(-5));
        meetingDetails.setNumberOfMeetingRooms(100);
        meetingDetails.setTotalDelegates(100);
        meetingDetails.setMeetingRoom(new BusinessRevenueBean(100.00, 100.00));
        meetingDetails.setFoodBeverage(new BusinessRevenueBean(100.00, 100.00));
        meetingDetails.setMiscellaneous(new BusinessRevenueBean(100.00, 100.00));
        meetingDetails.setTotalMeetingRevenue(new BusinessRevenueBean(300.00, 300.00));

        GuestRoom guestRoom = new GuestRoom();
        guestRoom.setStartDate(meetingDetails.getStartDate());
        guestRoom.setEndDate(meetingDetails.getEndDate());
        guestRoom.setTotalGuestRooms(12);
        guestRoom.setTotalRoomNights(10);
        guestRoom.setTotalGuests(12);
        guestRoom.setTotalRoom(new BusinessRevenueBean(1000.00));

        businessEvent.setMeetingDetails(meetingDetails);
        businessEvent.setGuestRoom(guestRoom);
        businessEvent.setDiscretionaryPoints(new DiscretionaryPoints("1111", INCENTIVE));
        businessEvent.setOffer(new BusinessOffer(OFFER_NAME, "500"));

        login(HOTEL);
        member = new EnrollmentPage().enroll(member, helper);
        businessEvent.setMember(member);

        new TopUserSection().switchUserRole(SALES_MANAGER, true);

        new ManageEventsPage().goToByOperation();
    }

    @Test
    public void populateEventInformation()
    {
        popUp = new ManageEventsPage().openDetailsPopUp();
        popUp.getEventSummaryTab().getEventInformation().populateHotelEvent(businessEvent);
    }

    @Test(dependsOnMethods = { "populateEventInformation" }, alwaysRun = true)
    public void checkIncludeMeeting()
    {
        MeetingFields meetingFields = popUp.getEventSummaryTab().getEventDetails().getMeetingFields();
        meetingFields.expand();
        meetingFields.verifyEnabled(false);
        meetingFields.getIncludeMeeting().check();
        meetingFields.verifyEnabled(true);
    }

    @Test(dependsOnMethods = { "checkIncludeMeeting" }, alwaysRun = true)
    public void createEvent()
    {
        String SUCCESS_CREATION_MESSAGE = "IHG Business Rewards Event has been successfully created. Member is Pending and will not earn Points. Member must agree to Terms and Conditions.";

        MeetingFields meetingFields = popUp.getEventSummaryTab().getEventDetails().getMeetingFields();
        meetingFields.populate(businessEvent.getMeetingDetails());

        MeetingRevenueDetails meetingRevenueDetail = meetingFields.getRevenueDetails();
        meetingRevenueDetail.populateLocalAmount(businessEvent.getMeetingDetails());
        meetingRevenueDetail.verifyUSDamount(businessEvent.getMeetingDetails());

        totalEligibleRevenue = popUp.getEventSummaryTab().getTotalEligibleRevenue();
        totalEligibleRevenue.verifyDefault();

        popUp.clickCalculateEstimates();
        verifyThat(totalEligibleRevenue.getEstimatedCost(), hasText(not(NOT_AVAILABLE)));
        verifyThat(totalEligibleRevenue.getEstimatedPointEarning(), hasText(not(NOT_AVAILABLE)));
        popUp.clickSave(verifyNoError(), verifySuccess(SUCCESS_CREATION_MESSAGE));

        ManageEventsPage manageEventsPage = new ManageEventsPage();
        manageEventsPage.searchByEventNameAndMemberId(businessEvent);

        ManageEventsGrid manageEventsGrid = manageEventsPage.getManageEventsGrid();
        verifyThat(manageEventsGrid, size(1));
        manageEventsGrid.getRow(1).verify(businessEvent);
    }

    @Test(dependsOnMethods = { "createEvent" }, alwaysRun = true)
    public void cancelEventCreation()
    {
        businessEvent.setEventName("NewEvent002");

        ManageEventsPage manageEventsPage = new ManageEventsPage();
        popUp = manageEventsPage.openDetailsPopUp();
        popUp.getEventSummaryTab().populateHotelEventAndMeetingFields(businessEvent);
        popUp.clickCancel();

        new ConfirmDialog().clickYes(verifyNoError());

        manageEventsPage.searchByEventNameAndMemberId(businessEvent);
        verifyThat(manageEventsPage.getManageEventsGrid(), size(0));
    }

    @Test(dependsOnMethods = { "cancelEventCreation" }, alwaysRun = true)
    public void createEventWithGuestRoomsAndDiscretionaryPoints()
    {
        businessEvent.setEventName("NewEvent003");

        popUp = new ManageEventsPage().openDetailsPopUp();
        eventSummaryTab = popUp.getEventSummaryTab();
        eventSummaryTab.getEventInformation().populateHotelEvent(businessEvent);
        eventSummaryTab.getDiscretionaryPoints().populate(businessEvent.getDiscretionaryPoints());

        verifyThat(popUp.getCalculateEstimates(), enabled(false));
        GuestRoomsFields guestRoomsFields = eventSummaryTab.getEventDetails().getGuestRoomsFields();
        guestRoomsFields.expand();

        guestRoomsFields.verifyEnabled(false);
        guestRoomsFields.getIncludeGuestRoomsRevenue().check();
        guestRoomsFields.verifyEnabled(true);

        GuestRoom guestRoom = businessEvent.getGuestRoom();
        guestRoomsFields.getStayStartDate().type(guestRoom.getStartDate());
        guestRoomsFields.getStayEndDate().type(guestRoom.getEndDate());
        guestRoomsFields.getTotalGuestRooms().type(guestRoom.getTotalGuestRooms());
        guestRoomsFields.getTotalRoomNights().type(guestRoom.getTotalRoomNights());
        guestRoomsFields.getTotalGuests().type(guestRoom.getTotalGuests());
        guestRoomsFields.getTotalRoom().populateLocalAmount(guestRoom.getTotalRoom());

        totalEligibleRevenue = popUp.getEventSummaryTab().getTotalEligibleRevenue();
        totalEligibleRevenue.verifyDefault();

        popUp.clickCalculateEstimates();
        verifyThat(totalEligibleRevenue.getEstimatedCost(), hasText(not(NOT_AVAILABLE)));
        verifyThat(totalEligibleRevenue.getEstimatedPointEarning(), hasText(not(NOT_AVAILABLE)));
        popUp.clickSave(verifyNoError());

        ManageEventsPage manageEventsPage = new ManageEventsPage();
        manageEventsPage.searchByEventNameAndMemberId(businessEvent);

        ManageEventsGrid manageEventsGrid = manageEventsPage.getManageEventsGrid();
        verifyThat(manageEventsGrid, size(1));
        manageEventsGrid.getRow(1).verify(businessEvent);
    }

    @Test(dependsOnMethods = { "createEventWithGuestRoomsAndDiscretionaryPoints" }, alwaysRun = true)
    public void tryToCreateEventWithoutMeeting()
    {
        businessEvent.setEventName("NewEvent004");

        eventSummaryTab = new ManageEventsPage().openDetailsPopUp().getEventSummaryTab();
        eventSummaryTab.getEventInformation().populateHotelEvent(businessEvent);

        OffersFieldsEdit offers = eventSummaryTab.getOffers();
        offers.expand();
        offers.getIncludeOffers().click(verifyError("A date range for this event must be selected first"));
    }

    @Test(dependsOnMethods = { "tryToCreateEventWithoutMeeting" }, alwaysRun = true)
    public void createEventWithOfferAndMeeting()
    {
        MeetingFields meetingFields = eventSummaryTab.getEventDetails().getMeetingFields();
        meetingFields.populate(businessEvent.getMeetingDetails());
        meetingFields.getRevenueDetails().populateLocalAmount(businessEvent.getMeetingDetails());
        eventSummaryTab.getOffers().populate(businessEvent.getOffer());

        totalEligibleRevenue = popUp.getEventSummaryTab().getTotalEligibleRevenue();
        totalEligibleRevenue.verifyDefault();

        popUp.clickCalculateEstimates();
        verifyThat(totalEligibleRevenue.getEstimatedCost(), hasText(not(NOT_AVAILABLE)));
        verifyThat(totalEligibleRevenue.getEstimatedPointEarning(), hasText(not(NOT_AVAILABLE)));
        popUp.clickSave(verifyNoError());

        ManageEventsPage manageEventsPage = new ManageEventsPage();
        manageEventsPage.searchByEventNameAndMemberId(businessEvent);

        ManageEventsGrid manageEventsGrid = manageEventsPage.getManageEventsGrid();
        verifyThat(manageEventsGrid, size(1));
        manageEventsGrid.getRow(1).verify(businessEvent);
    }

    @Test(dependsOnMethods = { "createEventWithOfferAndMeeting" }, alwaysRun = true)
    public void tryToCreateEventWithDiscretionaryPointsMoreThan20M()
    {
        String WARNING_MESSAGE = "The event you are trying to save exceeds the maximum of 20,000,000 discretionary bonus points. Please adjust your event and try to save again.";
        businessEvent.setEventName("NewEvent005");
        businessEvent.getDiscretionaryPoints().setAdditionalPointsToAward("20000001");

        popUp = new ManageEventsPage().openDetailsPopUp();
        eventSummaryTab = popUp.getEventSummaryTab();
        eventSummaryTab.populateHotelEventAndMeetingFields(businessEvent);
        eventSummaryTab.getDiscretionaryPoints().populate(businessEvent.getDiscretionaryPoints());

        popUp.clickCalculateEstimates(verifyError(WARNING_MESSAGE));
        popUp.clickSave(verifyError(WARNING_MESSAGE));

        verifyThat(totalEligibleRevenue.getEstimatedCost(), hasText(NOT_AVAILABLE));
        verifyThat(totalEligibleRevenue.getEstimatedPointEarning(), hasText(NOT_AVAILABLE));
    }

    @Test(dependsOnMethods = { "tryToCreateEventWithDiscretionaryPointsMoreThan20M" }, alwaysRun = true)
    public void createEventWithDiscretionaryPointsWith20M()
    {
        eventSummaryTab.getDiscretionaryPoints().getAdditionalPointsToAward().type("20000000");

        popUp.clickCalculateEstimates(verifyNoError());
        verifyThat(totalEligibleRevenue.getEstimatedCost(), hasText(not(NOT_AVAILABLE)));
        verifyThat(totalEligibleRevenue.getEstimatedPointEarning(), hasText(not(NOT_AVAILABLE)));
        popUp.clickSave(verifyNoError());

        ManageEventsPage manageEventsPage = new ManageEventsPage();
        manageEventsPage.searchByEventNameAndMemberId(businessEvent);

        ManageEventsGrid manageEventsGrid = manageEventsPage.getManageEventsGrid();
        verifyThat(manageEventsGrid, size(1));
        manageEventsGrid.getRow(1).verify(businessEvent);
    }

    @Test(dependsOnMethods = { "createEventWithDiscretionaryPointsWith20M" }, alwaysRun = true)
    public void tryToCreateEventWithOfferPointMoreThan20M()
    {
        String WARNING_MESSAGE = "The event you are trying to save exceeds the maximum of 20,000,000 offer bonus points. Please adjust your event and try to save again.";
        businessEvent.setEventName("NewEvent006");
        businessEvent.getOffer().setPointsToAward("20000001");

        popUp = new ManageEventsPage().openDetailsPopUp();
        eventSummaryTab = popUp.getEventSummaryTab();
        eventSummaryTab.populateHotelEventAndMeetingFields(businessEvent);
        eventSummaryTab.getOffers().populate(businessEvent.getOffer());

        popUp.clickCalculateEstimates(verifyError(WARNING_MESSAGE));
        verifyThat(totalEligibleRevenue.getEstimatedCost(), hasText(NOT_AVAILABLE));
        verifyThat(totalEligibleRevenue.getEstimatedPointEarning(), hasText(NOT_AVAILABLE));
        popUp.clickSave(verifyError(WARNING_MESSAGE));
    }

    @Test(dependsOnMethods = { "tryToCreateEventWithOfferPointMoreThan20M" }, alwaysRun = true)
    public void createEventWithOfferPoint20M()
    {
        eventSummaryTab.getOffers().getPointsToAward().typeAndWait(20000000);

        popUp.clickCalculateEstimates(verifyNoError());
        verifyThat(totalEligibleRevenue.getEstimatedCost(), hasText(not(NOT_AVAILABLE)));
        verifyThat(totalEligibleRevenue.getEstimatedPointEarning(), hasText(not(NOT_AVAILABLE)));
        popUp.clickSave(verifyNoError());

        ManageEventsPage manageEventsPage = new ManageEventsPage();
        manageEventsPage.searchByEventNameAndMemberId(businessEvent);

        ManageEventsGrid manageEventsGrid = manageEventsPage.getManageEventsGrid();
        verifyThat(manageEventsGrid, size(1));
        manageEventsGrid.getRow(1).verify(businessEvent);
    }
}
