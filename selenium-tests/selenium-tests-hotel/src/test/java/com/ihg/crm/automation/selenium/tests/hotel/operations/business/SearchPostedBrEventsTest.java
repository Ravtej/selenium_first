package com.ihg.crm.automation.selenium.tests.hotel.operations.business;

import static com.ihg.automation.common.DateUtils.DATE_FORMAT;
import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyNoError;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.types.Currency.USD;
import static com.ihg.automation.selenium.hotel.pages.hoteloperations.br.posted.PostedEventsGridRow.PostedEventsGridCell.END_DATE;
import static com.ihg.automation.selenium.hotel.pages.hoteloperations.br.posted.PostedEventsGridRow.PostedEventsGridCell.START_DATE;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasTextAsDate;
import static com.ihg.automation.selenium.matchers.component.GridSize.size;
import static com.ihg.automation.selenium.matchers.component.HasDefaultText.hasDefault;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static com.ihg.automation.selenium.matchers.component.HasTipText.hasWarningTipTextWithWait;
import static com.ihg.automation.selenium.matchers.date.IsAfterOrEqualsDate.isAfterOrEquals;
import static com.ihg.automation.selenium.matchers.date.IsBeforeOrEqualsDate.isBeforeOrEquals;
import static org.joda.time.LocalDate.now;

import java.util.List;
import java.util.Map;

import org.joda.time.LocalDate;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.ihg.automation.common.DateUtils;
import com.ihg.automation.common.MapUtils;
import com.ihg.automation.common.RandomUtils;
import com.ihg.automation.selenium.common.business.BusinessEvent;
import com.ihg.automation.selenium.common.business.BusinessOffer;
import com.ihg.automation.selenium.common.business.MeetingDetails;
import com.ihg.automation.selenium.common.event.SearchButtonBar;
import com.ihg.automation.selenium.common.member.GuestName;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.PersonalInfo;
import com.ihg.automation.selenium.gwt.components.input.DateInput;
import com.ihg.automation.selenium.hotel.pages.LeftPanel;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.HotelOperationsPage;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.br.posted.PostedEventsGrid;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.br.posted.PostedEventsGridRow;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.br.posted.PostedEventsPage;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.br.posted.PostedEventsSearch;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.br.posted.ResultsFilter;
import com.ihg.crm.automation.selenium.tests.hotel.LoginLogout;

public class SearchPostedBrEventsTest extends LoginLogout
{
    private PostedEventsPage postedEventsPage;
    private PostedEventsSearch searchFields;
    private PostedEventsGrid grid;
    private BusinessEvent businessEvent;

    private Map<String, String> postedEventsMap;
    private static final LocalDate VALID_FROM_DATE = now().minusDays(50);
    private static final LocalDate VALID_TO_DATE = now().plusDays(20);
    private static final String HOTEL = "ATLCP";
    private static final String GET_BR_EVENT = "SELECT DISTINCT nm.SURNAME_NM, nm.GIVEN_NM, m.MBRSHP_ID, e.BKR_EVN_KEY, " //
            + "e.BKR_EVN_NM, TO_CHAR(e.STRT_DT, 'ddMonYY') AS STRT_DT, TO_CHAR(e.END_DT, 'ddMonYY') AS END_DT, " //
            + "e.BASE_EST_LYTY_UNIT_QTY, e.BASE_EST_BILL_AMT, e.EST_BILL_CURR_CD " //
            + "FROM LYTY.BKR_EVN e " //
            + "JOIN GROWTH.HOTEL h " //
            + "ON e.HTL_ID = h.HTL_ID " //
            + "JOIN DGST.MBRSHP m " //
            + "ON m.MBRSHP_KEY = e.MBRSHP_KEY " //
            + "JOIN DGST.GST_NM nm " //
            + "ON m.DGST_MSTR_KEY = nm.DGST_MSTR_KEY " //
            + "WHERE e.STAT_CD='%s' " //
            + "AND h.HLDX_CD = '%s' " //
            + "AND e.BKR_EVN_NM IS NOT NULL " //
            + "AND e.EST_LYTY_UNIT_QTY IS NOT NULL " //
            + "AND e.BASE_EST_LYTY_UNIT_QTY IS NOT NULL " //
            + "AND e.BASE_EST_BILL_AMT IS NOT NULL " //
            + "AND e.EST_BILL_CURR_CD = 'USD' " //
            + "AND ROWNUM < 2";

    @BeforeClass
    public void beforeClass()
    {
        postedEventsMap = MapUtils.queryResult(jdbcTemplate.queryForMap(String.format(GET_BR_EVENT, "POSTED", HOTEL)));

        businessEvent = new BusinessEvent(postedEventsMap.get("BKR_EVN_NM"), HOTEL, USD);

        GuestName name = new GuestName();
        name.setGiven(postedEventsMap.get("GIVEN_NM"));
        name.setSurname(postedEventsMap.get("SURNAME_NM"));

        PersonalInfo personalInfo = new PersonalInfo();
        personalInfo.setName(name);

        Member member = new Member();
        member.setPersonalInfo(personalInfo);
        businessEvent.setMember(member);

        businessEvent.setEventId(postedEventsMap.get("BKR_EVN_KEY"));
        businessEvent.setEventName(postedEventsMap.get("BKR_EVN_NM"));

        MeetingDetails meetingDetails = new MeetingDetails();
        meetingDetails.setStartDate(postedEventsMap.get("STRT_DT"));
        meetingDetails.setEndDate(postedEventsMap.get("END_DT"));
        businessEvent.setMeetingDetails(meetingDetails);

        BusinessOffer businessOffer = new BusinessOffer("TestOffer", postedEventsMap.get("BASE_EST_LYTY_UNIT_QTY"));
        businessOffer.setEstimatedCostToHotel(postedEventsMap.get("BASE_EST_BILL_AMT"));
        businessEvent.setOffer(businessOffer);

        login(HOTEL);

        new LeftPanel().getHotelOperations().clickAndWait(verifyNoError());
        new HotelOperationsPage().getIHGBusinessRewardsEventsPosted().clickAndWait(verifyNoError());
    }

    @Test
    public void verifyCommonComponents()
    {
        postedEventsPage = new PostedEventsPage();

        searchFields = postedEventsPage.getSearchFields();
        verifyThat(searchFields.getHotelCode(), hasText(helper.getUser().getLocation()));
        verifyThat(searchFields.getFromDate(), hasDefault());
        verifyThat(searchFields.getToDate(), hasText(DateUtils.now().toString(DATE_FORMAT)));
    }

    @Test(priority = 3)
    public void verifyInvalidDatesRange()
    {
        searchFields.getFromDate().type(now().plusDays(15).toString(DATE_FORMAT));

        DateInput toDate = searchFields.getToDate();
        toDate.type(now().plusDays(14).toString(DATE_FORMAT));

        SearchButtonBar buttonBar = postedEventsPage.getButtonBar();
        buttonBar.clickSearch();

        verifyThat(toDate, hasWarningTipTextWithWait("This Date must be after Date From"));

        buttonBar.clickClearCriteria();
    }

    @Test(priority = 5)
    public void verifyValidDatesRange()
    {
        searchFields.getFromDate().type(VALID_FROM_DATE.toString(DATE_FORMAT));
        searchFields.getToDate().type(VALID_TO_DATE.toString(DATE_FORMAT));

        SearchButtonBar buttonBar = postedEventsPage.getButtonBar();
        buttonBar.clickSearch();

        grid = postedEventsPage.getManageEventsGrid();

        List<PostedEventsGridRow> gridRowList = grid.getRowList();
        for (PostedEventsGridRow row : gridRowList)
        {
            verifyThat(row.getCell(START_DATE), hasTextAsDate("ddMMMYY", isBeforeOrEquals(VALID_TO_DATE)));
            verifyThat(row.getCell(END_DATE), hasTextAsDate("ddMMMYY", isAfterOrEquals(VALID_FROM_DATE)));
        }

        buttonBar.clickClearCriteria();
    }

    @Test(priority = 10)
    public void verifySearchByCombinations()
    {
        searchBrEventByBookingId(postedEventsMap.get("BKR_EVN_KEY"), 1);

        grid.getRow(1).verify(businessEvent);

        postedEventsPage.getButtonBar().clickClearCriteria();
    }

    @Test(priority = 15)
    public void tryToSearchNotExistingEvent()
    {
        searchBrEventByBookingId(RandomUtils.getRandomNumber(15), 0);

        postedEventsPage.getButtonBar().clickClearCriteria();
    }

    @Test(priority = 20, dataProvider = "statusCodeProvider")
    public void tryToSearchNotPostedEvent(String statusCode)
    {
        Map<String, String> notPostedEventsMap = MapUtils
                .queryResult(jdbcTemplate.queryForMap(String.format(GET_BR_EVENT, statusCode, HOTEL)));

        searchBrEventByBookingId(notPostedEventsMap.get("BKR_EVN_KEY"), 0);

        postedEventsPage.getButtonBar().clickClearCriteria();
    }

    private void searchBrEventByBookingId(String bookerEventKey, int size)
    {
        ResultsFilter resultsFilter = postedEventsPage.getResultsFilter();
        resultsFilter.expand();
        resultsFilter.getEventId().type(bookerEventKey);

        postedEventsPage.getButtonBar().clickSearch();

        verifyThat(grid, size(size));
    }

    @DataProvider(name = "statusCodeProvider")
    public Object[][] statusCodeDataProvider()
    {
        return new Object[][] { { "PENDING" }, { "BLOCKED" }, { "FUTURE" }, { "IN_PROGRESS" }, { "CANCELED" } };
    }
}
