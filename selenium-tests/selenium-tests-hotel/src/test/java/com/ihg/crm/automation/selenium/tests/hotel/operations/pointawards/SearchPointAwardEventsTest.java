package com.ihg.crm.automation.selenium.tests.hotel.operations.pointawards;

import static com.ihg.automation.common.DateUtils.DATE_FORMAT;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.hotel.pages.Role.HOTEL_OPERATIONS_MANAGER;
import static com.ihg.automation.selenium.hotel.pages.hoteloperations.awards.manage.ManagePointAwardsGridRow.ManagePointAwardsGridCell.AWARD_TYPE;
import static com.ihg.automation.selenium.hotel.pages.hoteloperations.awards.manage.ManagePointAwardsGridRow.ManagePointAwardsGridCell.REQUESTED_DATE;
import static com.ihg.automation.selenium.hotel.pages.hoteloperations.awards.manage.ManagePointAwardsGridRow.ManagePointAwardsGridCell.STATUS;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasTextAsDate;
import static com.ihg.automation.selenium.matchers.component.GridSize.size;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static com.ihg.automation.selenium.matchers.component.HasTipText.hasWarningTipTextWithWait;
import static com.ihg.automation.selenium.matchers.date.IsAfterOrEqualsDate.isAfterOrEquals;
import static com.ihg.automation.selenium.matchers.date.IsBeforeOrEqualsDate.isBeforeOrEquals;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.core.AllOf.allOf;
import static org.hamcrest.core.Is.is;
import static org.joda.time.LocalDate.now;

import org.joda.time.LocalDate;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.event.SearchButtonBar;
import com.ihg.automation.selenium.gwt.components.input.DateInput;
import com.ihg.automation.selenium.gwt.components.input.Input;
import com.ihg.automation.selenium.gwt.components.input.Select;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.awards.manage.ManagePointAwardsGrid;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.awards.manage.ManagePointAwardsGridRow;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.awards.manage.ManagePointAwardsPage;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.awards.manage.ManagePointAwardsSearch;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.awards.manage.ResultsFilter;
import com.ihg.crm.automation.selenium.tests.hotel.LoginLogout;

public class SearchPointAwardEventsTest extends LoginLogout
{
    private static final LocalDate VALID_TO_DATE = now();
    private static final LocalDate VALID_FROM_DATE = now().minusDays(30);
    private static final LocalDate INVALID_FROM_DATE = now().minusDays(31);

    private static final String WARNING_DATE_PATTERN_BEFORE = "Date cannot be before %s";
    private static final String WARNING_DATE_PATTERN_AFTER = "Date cannot be after %s";
    private static final String FIELD_IS_REQUIRED = "This field is required";
    private static final String HOTEL = "ATLCP";

    private ManagePointAwardsPage managePointAwardsPage = new ManagePointAwardsPage();
    private SearchButtonBar buttonBar;
    private ManagePointAwardsGrid grid;
    private ManagePointAwardsSearch managePointAwardsSearch;
    private ResultsFilter resultsFilter;

    @BeforeClass
    public void before()
    {
        login(HOTEL, HOTEL_OPERATIONS_MANAGER);

        managePointAwardsPage.goToByOperation();

        managePointAwardsSearch = managePointAwardsPage.getSearchFields();

        resultsFilter = managePointAwardsPage.getResultsFilter();
        resultsFilter.expand();

        buttonBar = managePointAwardsPage.getButtonBar();

        grid = managePointAwardsPage.getManagePointAwardsGrid();
    }

    @AfterMethod
    public void after()
    {
        buttonBar.clickClearCriteria();
    }

    @Test(priority = 1)
    public void searchByDateAndVerifyRequested()
    {
        managePointAwardsSearch.fillDates(VALID_FROM_DATE, VALID_TO_DATE);

        buttonBar.clickSearch();

        verifyThat(grid, size(is(greaterThan(0))));

        for (ManagePointAwardsGridRow row : grid.getRowList())
        {
            verifyThat(row.getCell(REQUESTED_DATE),
                    hasTextAsDate("ddMMMYY", allOf(isBeforeOrEquals(VALID_TO_DATE), isAfterOrEquals(VALID_FROM_DATE))));
        }
    }

    @Test(priority = 5)
    public void searchInvalidDateRange()
    {
        managePointAwardsSearch.fillDates(INVALID_FROM_DATE, VALID_TO_DATE);
        buttonBar.clickSearch();

        verifyThat(grid, size(is(0)));

        verifyThat(managePointAwardsSearch.getFromDate(), hasWarningTipTextWithWait(
                String.format(WARNING_DATE_PATTERN_BEFORE, VALID_FROM_DATE.toString(DATE_FORMAT))));

    }

    @Test(priority = 7)
    public void replaceToWithFromDatesAndSearch()
    {
        managePointAwardsSearch.fillDates(VALID_TO_DATE, VALID_FROM_DATE);
        buttonBar.clickSearch();

        verifyThat(grid, size(is(0)));

        verifyThat(managePointAwardsSearch.getFromDate(), hasWarningTipTextWithWait(
                String.format(WARNING_DATE_PATTERN_AFTER, VALID_FROM_DATE.toString(DATE_FORMAT))));
        verifyThat(managePointAwardsSearch.getToDate(), hasWarningTipTextWithWait(
                String.format(WARNING_DATE_PATTERN_BEFORE, VALID_TO_DATE.toString(DATE_FORMAT))));
    }

    @Test(priority = 10)
    public void clearToAndFromDatesAndTryToSearch()
    {
        DateInput fromDate = managePointAwardsSearch.getFromDate();
        DateInput toDate = managePointAwardsSearch.getToDate();

        fromDate.clear();
        toDate.clear();

        buttonBar.clickSearch();

        verifyThat(fromDate, hasWarningTipTextWithWait(FIELD_IS_REQUIRED));
        verifyThat(toDate, hasWarningTipTextWithWait(FIELD_IS_REQUIRED));
    }

    @Test(priority = 15)
    public void tryToRemoveOneDateAndSearch()
    {
        DateInput fromDate = managePointAwardsSearch.getFromDate();
        DateInput toDate = managePointAwardsSearch.getToDate();

        fromDate.clear();
        buttonBar.clickSearch();
        verifyThat(fromDate, hasWarningTipTextWithWait(FIELD_IS_REQUIRED));

        buttonBar.clickClearCriteria();

        toDate.clear();
        buttonBar.clickSearch();
        verifyThat(toDate, hasWarningTipTextWithWait(FIELD_IS_REQUIRED));
    }

    @DataProvider(name = "searchFieldsProvider")
    public Object[][] searchFieldsDataProvider()
    {
        Select type = managePointAwardsSearch.getType();
        Select status = managePointAwardsSearch.getStatus();

        return new Object[][] { { type, AWARD_TYPE, "Welcome Amenity" }, { type, AWARD_TYPE, "Hotel Promotion" },
                { status, STATUS, "Initiated" }, { status, STATUS, "Pending" } };
    }

    @Test(priority = 20, dataProvider = "searchFieldsProvider")
    public void searchByStatusAndType(Select selectField, ManagePointAwardsGridRow.ManagePointAwardsGridCell cell,
            String selectName)
    {
        managePointAwardsSearch.fillDates(VALID_FROM_DATE, VALID_TO_DATE);
        selectField.select(selectName);

        buttonBar.clickSearch();
        verifyThat(grid, size(is(greaterThan(0))));

        for (ManagePointAwardsGridRow row : grid.getRowList())
        {
            verifyThat(row.getCell(cell), hasText(selectName));
        }
    }

    @DataProvider(name = "noResultFieldsProvider")
    public Object[][] noResultFieldsDataProvider()
    {
        return new Object[][] { { resultsFilter.getConfirmation(), "12345abc" }, //
                { resultsFilter.getRateCategory(), "IDDQD" }, //
                { resultsFilter.getCorporateId(), "0000012" }, //
                { resultsFilter.getIata(), "000000" } };
    }

    @Test(priority = 25, dataProvider = "noResultFieldsProvider")
    public void tryToSearchNotExistingEvents(Input inputField, String searchCriteria)
    {
        managePointAwardsSearch.fillDates(VALID_FROM_DATE, VALID_TO_DATE);

        inputField.type(searchCriteria);
        buttonBar.clickSearch();

        verifyThat(grid, size(0));
    }
}
