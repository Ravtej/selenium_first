package com.ihg.crm.automation.selenium.tests.hotel.stay.lpu.disqualified;

import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyNoError;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.hotel.pages.hoteloperations.stay.lpu.LoyaltyPendingUpdatesGridRow.LoyaltyPendingUpdatesGridCell.QUALIFYING_FLAG;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.pages.PaymentType;
import com.ihg.automation.selenium.common.pages.login.TopUserSection;
import com.ihg.automation.selenium.common.stay.Revenues;
import com.ihg.automation.selenium.common.stay.Stay;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.hotel.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.stay.lpu.LoyaltyPendingUpdatesGridRow;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.stay.lpu.LoyaltyPendingUpdatesPage;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.stay.lpu.LpuGridRowContainer;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.stay.lpu.missingstay.DisqualifyReasonPopUp;
import com.ihg.crm.automation.selenium.tests.hotel.LoginLogout;
import com.ihg.crm.automation.selenium.tests.hotel.stay.HotelDBUtils;

public class StayByIATATest extends LoginLogout
{
    public static final String NON_QUALIFYING_IATA_ALLIANCE_MESSAGE = "Non-Qualifying IATA Alliance Number";
    private Member member = new Member();
    private Stay stay = new Stay();

    @BeforeClass
    public void beforeClass()
    {
        member.addProgram(Program.RC);
        member.addEmail(MemberPopulateHelper.getRandomEmail());
        member.addPhone(MemberPopulateHelper.getRandomPhone());
        member.addAddress(MemberPopulateHelper.getUnitedStatesAddress());
        member.setPersonalInfo(MemberPopulateHelper.getSimplePersonalInfo());

        new HotelDBUtils(jdbcTemplate).getGnrHotel(stay);
        stay.setNights(5);
        stay.setCheckInDateByCheckout();
        stay.setRateCode("TEST");
        stay.setCorporateAccountNumber("100230372");
        stay.setConfirmationNumber("12345");
        stay.setRoomType("KING");
        stay.setRoomNumber("555");
        stay.setIataCode("10229");
        stay.setFolioNumber("98765");
        stay.setAvgRoomRate(100.00);
        stay.setIsQualifying(false);
        stay.setPaymentType(PaymentType.CASH);
        stay.setQualifyingNights(0);

        Revenues revenues = stay.getRevenues();
        revenues.getOverallTotal().setAmount(1300.00);
        revenues.getRoom().setAmount(500.00);
        revenues.getTotalQualifying().setAmount(500.00);
        revenues.getFood().setAmount(100.00);
        revenues.getBeverage().setAmount(100.00);
        revenues.getPhone().setAmount(100.00);
        revenues.getMeeting().setAmount(100.00);
        revenues.getMandatoryLoyalty().setAmount(100.00);
        revenues.getOtherLoyalty().setAmount(100.00);
        revenues.getNoLoyalty().setAmount(100.00);
        revenues.getTax().setAmount(100.00);
        revenues.getTotalNonQualifying().setAmount(1300.00);
        revenues.getTotalQualifying().setAmount(0.00);

        stay.getBookingDetails().setBookingSource("BOOK");

        login();

        member = new EnrollmentPage().enroll(member, helper, verifyNoError());

        new TopUserSection().switchHotel(stay.getHotelCode());
        new LoyaltyPendingUpdatesPage().goToByHotelOperations().createMissingStay(member, stay);
    }

    @Test
    public void verifyCreatedStayRow()
    {
        LoyaltyPendingUpdatesPage page = new LoyaltyPendingUpdatesPage();
        LoyaltyPendingUpdatesGridRow updatesGridRow = page.getLoyaltyPendingUpdatesGrid().getRow(1);

        updatesGridRow.verify(member, stay);
        updatesGridRow.getCell(QUALIFYING_FLAG).asLink().clickAndWait(verifyNoError());

        new DisqualifyReasonPopUp().verifyReasonAndClose(NON_QUALIFYING_IATA_ALLIANCE_MESSAGE);
    }

    @Test(dependsOnMethods = { "verifyCreatedStayRow" }, alwaysRun = true)
    public void verifyStayDetailsInView()
    {
        LpuGridRowContainer container = new LoyaltyPendingUpdatesPage().getLoyaltyPendingUpdatesGrid().getRow(1)
                .expand(LpuGridRowContainer.class);
        container.verify(stay, member);
        verifyThat(container.getAdjustRevenueDetails(), displayed(true));
    }
}
