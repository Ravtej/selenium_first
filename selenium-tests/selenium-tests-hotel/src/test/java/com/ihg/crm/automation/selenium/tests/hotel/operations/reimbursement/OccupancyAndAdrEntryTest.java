package com.ihg.crm.automation.selenium.tests.hotel.operations.reimbursement;

import static com.ihg.automation.common.DateUtils.DATE_FORMAT;
import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyNoError;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.hotel.pages.hoteloperations.reimbursement.occupancy.OccupancyAndADREntrySearchGridRow.OccupancyAndADREntrySearchGridCell.STATUS;
import static com.ihg.automation.selenium.hotel.pages.hoteloperations.reimbursement.occupancy.OccupancyAndADREntrySearchGridRow.OccupancyAndADREntrySearchGridCell.STAY_DATE;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasAnyText;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasTextAsDate;
import static com.ihg.automation.selenium.matchers.component.GridSize.size;
import static com.ihg.automation.selenium.matchers.component.HasDefaultText.hasDefault;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static com.ihg.automation.selenium.matchers.component.HasTipText.hasWarningTipTextWithWait;
import static com.ihg.automation.selenium.matchers.date.IsAfterOrEqualsDate.isAfterOrEquals;
import static com.ihg.automation.selenium.matchers.date.IsBeforeOrEqualsDate.isBeforeOrEquals;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNot.not;
import static org.joda.time.LocalDate.now;

import java.util.List;

import org.joda.time.LocalDate;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.gwt.components.input.DateInput;
import com.ihg.automation.selenium.hotel.pages.LeftPanel;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.HotelOperationsPage;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.reimbursement.occupancy.OccupancyAndADREntryPage;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.reimbursement.occupancy.OccupancyAndADREntrySearch;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.reimbursement.occupancy.OccupancyAndADREntrySearchGrid;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.reimbursement.occupancy.OccupancyAndADREntrySearchGridRow;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.reimbursement.occupancy.OccupancyGridRowContainer;
import com.ihg.crm.automation.selenium.tests.hotel.LoginLogout;

public class OccupancyAndAdrEntryTest extends LoginLogout
{
    private OccupancyAndADREntryPage occupancyAndADREntryPage;
    private OccupancyAndADREntrySearch occupancyAndADREntrySearch;
    private OccupancyAndADREntrySearchGrid grid;

    private static final LocalDate VALID_FROM_DATE = now().minusYears(5);
    private static final LocalDate VALID_TO_DATE = now().minusDays(1);
    private static final String ALL_STATUSES = "All Statuses";
    private static final String REWARD_NIGHT = "Reward Night";

    @BeforeClass
    public void before()
    {
        login();

        new LeftPanel().getHotelOperations().clickAndWait(verifyNoError());
        new HotelOperationsPage().getProcessHotelReimbForRewardFreeNights().clickAndWait(verifyNoError());
    }

    @Test(priority = 1)
    public void verifyPageComponents()
    {
        occupancyAndADREntryPage = new OccupancyAndADREntryPage();

        OccupancyAndADREntrySearch searchFields = occupancyAndADREntryPage.getSearchFields();
        verifyThat(searchFields.getHotelCode(), hasText(helper.getUser().getLocation()));
        verifyThat(searchFields.getStatus(), hasText(ALL_STATUSES));
        verifyThat(searchFields.getFromDate(), hasText(now().minusDays(31).toString(DATE_FORMAT)));
        verifyThat(searchFields.getToDate(), hasText(now().minusDays(1).toString(DATE_FORMAT)));
        verifyThat(searchFields.getType(), hasDefault());
    }

    @Test(priority = 5)
    public void tryToSearchWithEmptyDatesRange()
    {
        occupancyAndADREntrySearch = occupancyAndADREntryPage.getSearchFields();

        occupancyAndADREntrySearch.getFromDate().clear();

        DateInput toDate = occupancyAndADREntrySearch.getToDate();
        toDate.clear();

        occupancyAndADREntryPage.getButtonBar().clickSearch();

        verifyThat(toDate, hasWarningTipTextWithWait("This field is required"));
    }

    @Test(priority = 10)
    public void verifyDatesRangeInvalid()
    {
        DateInput fromDate = occupancyAndADREntrySearch.getFromDate();
        fromDate.type(now().plusDays(15).toString(DATE_FORMAT));

        occupancyAndADREntrySearch.getToDate().type(now().minusDays(14).toString(DATE_FORMAT));

        occupancyAndADREntryPage.getButtonBar().clickSearch();

        verifyThat(fromDate, hasWarningTipTextWithWait("Start date should be before end date"));
    }

    @Test(priority = 15)
    public void verifyValidDateRange()
    {
        occupancyAndADREntrySearch.getFromDate().type(VALID_FROM_DATE.toString(DATE_FORMAT));
        occupancyAndADREntrySearch.getToDate().type(VALID_TO_DATE.toString(DATE_FORMAT));
        occupancyAndADREntryPage.getButtonBar().clickSearch();

        grid = occupancyAndADREntryPage.getOccupancyAndADREntrySearchGrid();
        List<OccupancyAndADREntrySearchGridRow> gridRowList = grid.getRowList();
        for (OccupancyAndADREntrySearchGridRow row : gridRowList)
        {
            verifyThat(row.getCell(STAY_DATE), hasTextAsDate("ddMMMYY", isAfterOrEquals(VALID_FROM_DATE)));
            verifyThat(row.getCell(STAY_DATE), hasTextAsDate("ddMMMYY", isBeforeOrEquals(VALID_TO_DATE)));
        }
    }

    @Test(priority = 20, dataProvider = "statusProvider")
    public void verifySearchByStatus(String status)
    {
        occupancyAndADREntrySearch.getStatus().select(status);

        occupancyAndADREntryPage.getButtonBar().clickSearch();

        grid = occupancyAndADREntryPage.getOccupancyAndADREntrySearchGrid();
        verifyThat(grid, size(is(greaterThan(0))));

        List<OccupancyAndADREntrySearchGridRow> gridRowList = grid.getRowList();
        for (OccupancyAndADREntrySearchGridRow row : gridRowList)
        {
            verifyThat(row.getCell(STATUS), hasText(status));
        }
    }

    @Test(priority = 25)
    public void verifyRewardNightEvent()
    {
        occupancyAndADREntrySearch.getStatus().select(ALL_STATUSES);
        occupancyAndADREntrySearch.getType().select(REWARD_NIGHT);
        occupancyAndADREntryPage.getButtonBar().getSearch().clickAndWait(verifyNoError());

        grid = occupancyAndADREntryPage.getOccupancyAndADREntrySearchGrid();
        verifyThat(grid, size(not(0)));

        OccupancyGridRowContainer rowContainer = grid.getRow(1).expand(OccupancyGridRowContainer.class);
        verifyThat(rowContainer.getCertificateType(), hasText(REWARD_NIGHT));
        verifyThat(rowContainer.getConfirmationNumber(), hasAnyText());
        verifyThat(rowContainer.getMemberID(), hasAnyText());
        verifyThat(rowContainer.getLastName(), hasAnyText());
        verifyThat(rowContainer.getCertificateNumber(), hasAnyText());
        verifyThat(rowContainer.getSuffix(), hasAnyText());
        verifyThat(rowContainer.getStatus(), hasAnyText());
        verifyThat(rowContainer.getOfferName(), hasAnyText());
        verifyThat(rowContainer.getReimbursementAmount(), hasAnyText());
        verifyThat(rowContainer.getReimbursementMethod(), hasAnyText());
        verifyThat(rowContainer.getReimbursementDate(), hasAnyText());
    }

    @DataProvider(name = "statusProvider")
    public Object[][] statusDataProvider()
    {
        return new Object[][] { { "INITIATED" }, { "PENDING" }, { "ACCEPTED" }, { "PAID" } };
    }
}
