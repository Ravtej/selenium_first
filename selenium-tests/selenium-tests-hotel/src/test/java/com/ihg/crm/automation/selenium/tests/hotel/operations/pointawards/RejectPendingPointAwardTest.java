package com.ihg.crm.automation.selenium.tests.hotel.operations.pointawards;

import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyNoError;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.PointAwardsStatus.PENDING;
import static com.ihg.automation.selenium.common.PointAwardsStatus.REJECTED;
import static com.ihg.automation.selenium.common.types.program.RewardClubLevel.GOLD;
import static com.ihg.automation.selenium.hotel.pages.Role.HOTEL_FRONT_DESK_FEE_BASED;
import static com.ihg.automation.selenium.hotel.pages.Role.HOTEL_OPERATIONS_MANAGER;
import static com.ihg.automation.selenium.hotel.pages.hoteloperations.awards.manage.ManagePointAwardsGridRow.ManagePointAwardsGridCell.ACTION;
import static com.ihg.automation.selenium.hotel.pages.hoteloperations.awards.manage.ManagePointAwardsGridRow.ManagePointAwardsGridCell.STATUS;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasDefault;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasText;
import static org.hamcrest.Matchers.containsString;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.member.AmbassadorUtils;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.pages.login.TopUserSection;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.gwt.components.dialog.ConfirmDialog;
import com.ihg.automation.selenium.gwt.components.grid.GridCell;
import com.ihg.automation.selenium.hotel.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.awards.PointAward;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.awards.manage.ManagePointAward;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.awards.manage.ManagePointAwardsGridRow;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.awards.manage.ManagePointAwardsPage;
import com.ihg.automation.selenium.matchers.component.HasRowColor;
import com.ihg.crm.automation.selenium.tests.hotel.LoginLogout;

public class RejectPendingPointAwardTest extends LoginLogout
{
    private static final String HOTEL = "SHGHA";
    private String membershipId;
    private ManagePointAward managePointAward = new ManagePointAward();

    @BeforeClass
    public void before()
    {
        Member member = new Member();
        member.addProgram(Program.AMB);
        member.setPersonalInfo(MemberPopulateHelper.getSimplePersonalInfo());
        member.addAddress(MemberPopulateHelper.getUnitedStatesAddress());
        member.setAmbassadorAmount(AmbassadorUtils.PURCHASE_AMBASSADOR_HOTEL_$200);

        login(HOTEL);
        member = new EnrollmentPage().enroll(member, helper);
        membershipId = member.getAMBProgramId();

        managePointAward.setMemberNumber(membershipId);
        managePointAward.setTransaction(PointAward.HOTEL_PROMOTION);
        managePointAward.setMemberName(member);
        managePointAward.setPoints("2000");
        managePointAward.setMemberLevel(GOLD);
        managePointAward.setStatus(PENDING);
        managePointAward.setSource(helper, HOTEL);

        new TopUserSection().switchUserRole(HOTEL_FRONT_DESK_FEE_BASED);

        ManagePointAwardsPage managePointAwardsPage = new ManagePointAwardsPage();
        managePointAwardsPage.goToByOperation();
        managePointAwardsPage.createPointAward(managePointAward);

        new TopUserSection().switchUserRole(HOTEL_OPERATIONS_MANAGER, false);
    }

    @Test
    public void cancelRejectingPointAwardInPendingStatus()
    {
        ManagePointAwardsPage managePointAwardsPage = new ManagePointAwardsPage();
        managePointAwardsPage.goToByOperation();

        ManagePointAwardsGridRow pendingAwardRow = managePointAwardsPage.getManagePointAwardsGrid()
                .getRow(membershipId);
        GridCell rejectCell = pendingAwardRow.getCell(ACTION);
        verifyThat(rejectCell.asLink(), displayed(true));
        verifyThat(rejectCell, hasText("Reject"));
        rejectCell.asLink().click();

        ConfirmDialog warningPopUp = new ConfirmDialog();
        verifyThat(warningPopUp, displayed(true));

        verifyThat(warningPopUp, hasText(containsString(
                "If the point award request is rejected, it cannot be approved for posting later. Do you wish to continue with this transaction?")));

        warningPopUp.clickNo(verifyNoError());

        pendingAwardRow = managePointAwardsPage.getManagePointAwardsGrid().getRow(membershipId);
        pendingAwardRow.verify(managePointAward);
    }

    @Test(dependsOnMethods = { "cancelRejectingPointAwardInPendingStatus" }, alwaysRun = true)
    public void rejectPointAwardInPendingStatus()
    {
        ManagePointAwardsPage managePointAwardsPage = new ManagePointAwardsPage();
        ManagePointAwardsGridRow pendingAwardRow = managePointAwardsPage.getManagePointAwardsGrid()
                .getRow(membershipId);
        verifyThat(pendingAwardRow.getCell(STATUS), hasText(PENDING));

        pendingAwardRow.getCell(ACTION).asLink().click();
        new ConfirmDialog().clickYes();

        pendingAwardRow = managePointAwardsPage.getManagePointAwardsGrid().getRow(membershipId);
        verifyThat(pendingAwardRow, HasRowColor.isGreenColor());
        verifyThat(pendingAwardRow.getCell(STATUS), hasText(REJECTED));
        verifyThat(pendingAwardRow.getCell(ACTION), hasDefault());
    }
}
