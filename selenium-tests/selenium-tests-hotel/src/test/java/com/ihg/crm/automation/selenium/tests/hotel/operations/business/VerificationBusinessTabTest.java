package com.ihg.crm.automation.selenium.tests.hotel.operations.business;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.Constant.NOT_AVAILABLE;
import static com.ihg.automation.selenium.common.business.BusinessEventType.BUSINESS_EVENT_TYPE_LIST;
import static com.ihg.automation.selenium.common.business.BusinessStatus.BUSINESS_STATUS_LIST;
import static com.ihg.automation.selenium.common.business.BusinessStatus.POSTED;
import static com.ihg.automation.selenium.common.types.Currency.USD;
import static com.ihg.automation.selenium.hotel.pages.Role.SALES_MANAGER;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasDefault;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasText;
import static com.ihg.automation.selenium.matchers.component.HasSelectItems.hasSelectItems;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.business.AllPastStaysGrid;
import com.ihg.automation.selenium.common.business.BusinessEvent;
import com.ihg.automation.selenium.common.business.BusinessRewardsEventDetailsPopUp;
import com.ihg.automation.selenium.common.business.DiscretionaryPointsFields;
import com.ihg.automation.selenium.common.business.EventInformationFields;
import com.ihg.automation.selenium.common.business.EventStaysGrid;
import com.ihg.automation.selenium.common.business.EventSummaryTab;
import com.ihg.automation.selenium.common.business.GuestRoomsFields;
import com.ihg.automation.selenium.common.business.MeetingFields;
import com.ihg.automation.selenium.common.business.MeetingRevenueDetails;
import com.ihg.automation.selenium.common.business.OffersFieldsEdit;
import com.ihg.automation.selenium.common.business.RoomsTab;
import com.ihg.automation.selenium.common.business.StaySearch;
import com.ihg.automation.selenium.common.business.TotalEventAwardFields;
import com.ihg.automation.selenium.common.event.SearchButtonBar;
import com.ihg.automation.selenium.common.pages.login.TopUserSection;
import com.ihg.automation.selenium.gwt.components.input.Select;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.br.manage.ManageEventsGrid;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.br.manage.ManageEventsPage;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.br.manage.ManageEventsSearch;
import com.ihg.crm.automation.selenium.tests.hotel.LoginLogout;

public class VerificationBusinessTabTest extends LoginLogout
{
    private BusinessRewardsEventDetailsPopUp popUp;
    private EventSummaryTab eventSummaryTab;

    @BeforeClass
    public void before()
    {
        login();
        new TopUserSection().switchUserRole(SALES_MANAGER, false);
    }

    @Test
    public void verifyManagePointAwardsFields()
    {
        ManageEventsPage manageEventsPage = new ManageEventsPage();
        manageEventsPage.goToByOperation();
        verifyThat(manageEventsPage.getCreateEvent(), displayed(true));

        ManageEventsSearch searchFields = manageEventsPage.getSearchFields();
        verifyThat(searchFields.getHotelCode(), hasText(helper.getUser().getLocation()));

        Select eventStatus = searchFields.getEventStatus();
        verifyThat(eventStatus, hasDefault());

        BUSINESS_STATUS_LIST.remove(POSTED);
        verifyThat(eventStatus, hasSelectItems(BUSINESS_STATUS_LIST));

        verifyThat(searchFields.getFromDate(), hasDefault());
        verifyThat(searchFields.getToDate(), hasDefault());
        verifyThat(searchFields.getEventsWithLimitDays(), displayed(true));

        SearchButtonBar buttonBar = manageEventsPage.getButtonBar();
        verifyThat(buttonBar.getClearCriteria(), displayed(true));
        verifyThat(buttonBar.getSearch(), displayed(true));

        ManageEventsGrid manageEventsGrid = manageEventsPage.getManageEventsGrid();
        verifyThat(manageEventsGrid, displayed(true));

        manageEventsGrid.verifyHeader();
    }

    @Test(dependsOnMethods = { "verifyManagePointAwardsFields" }, alwaysRun = true)
    public void verifyEventInformationSectionOnCreateEvent()
    {
        popUp = new ManageEventsPage().openDetailsPopUp();
        eventSummaryTab = popUp.getEventSummaryTab();

        EventInformationFields eventInformationFields = eventSummaryTab.getEventInformation();

        verifyThat(eventInformationFields.getMemberNumber(), displayed(true));
        verifyThat(eventInformationFields.getHotel(), displayed(true));
        verifyThat(eventInformationFields.getHotelCurrency(), displayed(true));
        verifyThat(eventInformationFields.getEventContractDate(), displayed(true));
        verifyThat(eventInformationFields.getEventName(), displayed(true));
        verifyThat(eventInformationFields.getHotelContactEmail(), displayed(true));
        verifyThat(eventInformationFields.getHotelContactName(), displayed(true));
        verifyThat(eventInformationFields.getHotelContactPhone(), displayed(true));
        verifyThat(eventInformationFields.getEventType(), hasSelectItems(BUSINESS_EVENT_TYPE_LIST));
    }

    @Test(dependsOnMethods = { "verifyEventInformationSectionOnCreateEvent" }, priority = 0, alwaysRun = true)
    public void verifyMeetingSectionOnCreateEvent()
    {
        MeetingFields meetingFields = eventSummaryTab.getEventDetails().getMeetingFields();
        meetingFields.expand();

        verifyThat(meetingFields.getIncludeMeeting(), displayed(true));
        verifyThat(meetingFields.getStartDate(), displayed(true));
        verifyThat(meetingFields.getEndDate(), displayed(true));
        verifyThat(meetingFields.getNumberOfMeetingRooms(), displayed(true));
        verifyThat(meetingFields.getTotalDelegates(), displayed(true));

        MeetingRevenueDetails meetingRevenueDetail = meetingFields.getRevenueDetails();
        meetingRevenueDetail.getMeetingRoom().verifyDefault();
        meetingRevenueDetail.getFoodAndBeverage().verifyDefault();
        meetingRevenueDetail.getMiscellaneous().verifyDefault();
        meetingRevenueDetail.getTotalMeetingRevenue().verifyDefault();
    }

    @Test(dependsOnMethods = { "verifyEventInformationSectionOnCreateEvent" }, priority = 0, alwaysRun = true)
    public void verifyGuestRoomsSectionOnCreateEvent()
    {
        GuestRoomsFields guestRoomsFields = eventSummaryTab.getEventDetails().getGuestRoomsFields();
        guestRoomsFields.expand();
        guestRoomsFields.getEligibleRoomRevenue().verifyDefault();
        guestRoomsFields.getTotalRoom().verifyDefault();

        verifyThat(guestRoomsFields.getCorporateID(), displayed(true));
        verifyThat(guestRoomsFields.getIATANumber(), displayed(true));
        verifyThat(guestRoomsFields.getIncludeGuestRoomsRevenue(), displayed(true));
        verifyThat(guestRoomsFields.getTotalGuestRooms(), displayed(true));
        verifyThat(guestRoomsFields.getStayEndDate(), displayed(true));
        verifyThat(guestRoomsFields.getStayStartDate(), displayed(true));
        verifyThat(guestRoomsFields.getTotalGuests(), displayed(true));
        verifyThat(guestRoomsFields.getTotalRoomNights(), displayed(true));
    }

    @Test(dependsOnMethods = { "verifyEventInformationSectionOnCreateEvent" }, priority = 0, alwaysRun = true)
    public void verifyTotalEventRevenueSectionOnCreateEvent()
    {
        eventSummaryTab.getTotalEligibleRevenue().verifyDefault();
    }

    @Test(dependsOnMethods = { "verifyEventInformationSectionOnCreateEvent" }, priority = 0, alwaysRun = true)
    public void verifyDiscretionaryPointsSectionOnCreateEvent()
    {
        DiscretionaryPointsFields discretionaryPointsFields = eventSummaryTab.getDiscretionaryPoints();
        discretionaryPointsFields.expand();

        verifyThat(discretionaryPointsFields.getAdditionalPointsToAward(), displayed(true));
        verifyThat(discretionaryPointsFields.getComments(), displayed(true));
        verifyThat(discretionaryPointsFields.getIncludeDiscretionaryPoints(), displayed(true));
        verifyThat(discretionaryPointsFields.getReason(), displayed(true));
        verifyThat(discretionaryPointsFields.getEstimatedCost(), hasText(NOT_AVAILABLE));
    }

    @Test(dependsOnMethods = { "verifyEventInformationSectionOnCreateEvent" }, priority = 0, alwaysRun = true)
    public void verifyOfferFieldsOnCreateEvent()
    {
        OffersFieldsEdit offerFields = eventSummaryTab.getOffers();
        offerFields.expand();

        verifyThat(offerFields.getIncludeOffers(), displayed(true));
        verifyThat(offerFields.getOffer(), displayed(true));
        verifyThat(offerFields.getPointsToAward(), displayed(true));
        verifyThat(offerFields.getEstimatedCost(), hasText(NOT_AVAILABLE));
    }

    @Test(dependsOnMethods = { "verifyEventInformationSectionOnCreateEvent" }, priority = 0, alwaysRun = true)
    public void verifyTotalEventAwardSectionOnCreateEvent()
    {
        TotalEventAwardFields totalEventAwardFields = eventSummaryTab.getTotalEventAward();
        totalEventAwardFields.getBasePoints().verifyDefault();
        totalEventAwardFields.getDiscretionaryPoints().verifyDefault();
        totalEventAwardFields.getOfferPoints().verifyDefault();
        totalEventAwardFields.getTotalPoints().verifyDefault();
    }

    @Test(dependsOnMethods = { "verifyEventInformationSectionOnCreateEvent" }, priority = 1, alwaysRun = true)
    public void verifyRoomsTab()
    {
        BusinessEvent businessEvent = new BusinessEvent("NewEvent001", helper.getUser().getLocation(), USD);

        EventInformationFields eventInformationFields = eventSummaryTab.getEventInformation();

        eventInformationFields.populate(businessEvent);

        RoomsTab roomTab = popUp.getRoomsTab();
        roomTab.goTo();

        EventStaysGrid eventStaysGrid = roomTab.getEventStaysGrid();
        verifyThat(eventStaysGrid, displayed(true));
        eventStaysGrid.verifyHeader();

        AllPastStaysGrid allPastStaysGrid = roomTab.getAllPastStaysGrid();
        verifyThat(allPastStaysGrid, displayed(true));
        allPastStaysGrid.verifyHeader();

        StaySearch staySearch = roomTab.getSearchFields();

        verifyThat(staySearch.getHotel(), hasText(businessEvent.getHotel()));
        roomTab.getTotalEligibleRoomRevenue().verify(NOT_AVAILABLE, USD);

        verifyThat(staySearch.getGuestName(), displayed(true));
        verifyThat(staySearch.getConfirmationNumber(), displayed(true));
        verifyThat(staySearch.getToDate(), displayed(true));
        verifyThat(staySearch.getFromDate(), displayed(true));
    }

}
