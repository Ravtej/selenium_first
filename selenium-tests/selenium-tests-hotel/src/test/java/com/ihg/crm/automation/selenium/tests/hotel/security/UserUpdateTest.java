package com.ihg.crm.automation.selenium.tests.hotel.security;

import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyError;
import static com.ihg.automation.selenium.common.Constant.NOT_AVAILABLE;
import static com.ihg.automation.selenium.hotel.pages.Role.ALL_ROLES;
import static com.ihg.automation.selenium.hotel.pages.Role.HOTEL_SECURITY;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.hotel.pages.LeftPanel;
import com.ihg.automation.selenium.hotel.pages.Role;
import com.ihg.automation.selenium.hotel.pages.security.security.UserManagement;
import com.ihg.automation.selenium.hotel.pages.security.security.UserManagementDetailsPopUp;
import com.ihg.automation.selenium.hotel.pages.security.security.UserManagementGrid;
import com.ihg.automation.selenium.hotel.pages.security.security.UserManagementPage;
import com.ihg.crm.automation.selenium.tests.hotel.LoginLogout;

public class UserUpdateTest extends LoginLogout
{
    private UserManagementPage userManagementPage = new UserManagementPage();
    private UserManagementGrid userManagementGrid = userManagementPage.getUserManagementGrid();
    private UserManagement userManagement = new UserManagement();
    private UserManagementDetailsPopUp userManagementDetailsPopUp = new UserManagementDetailsPopUp();

    private List<String> activeUserRoles;

    private static final String CANNOT_SAVE_WITHOUT_ROLES = "[ROLES.NOT.SELECTED] At least 1 role must be selected.";
    private static final String HOTEL = "ATLCP";
    private static final String USER_NAME = "testID";
    private static final String USER_SURNAME = "Loy";
    private static final String USERNAME = "AMER\\testID loy";

    private static final String GET_USER_ROLES = "SELECT apl.ROLE_CD " //
            + "FROM LYTYUSERDATA.USR u " //
            + "JOIN LYTYUSERDATA.USR_APPL a ON u.USR_KEY = a.USR_KEY " //
            + "JOIN LYTYUSERDATA.USR_APPL_ROLE apl ON a.USR_APPL_KEY = apl.USR_APPL_KEY " //
            + "WHERE u.USR_ID = 'testid loy' " //
            + "AND a.APPL_CD = 'HOTEL'";

    @BeforeClass
    public void before()
    {
        activeUserRoles = new ArrayList<>(jdbcTemplate.queryForList(GET_USER_ROLES, String.class));

        userManagement.setHotel(HOTEL);
        userManagement.setFirstName(USER_NAME);
        userManagement.setLastName(USER_SURNAME);
        userManagement.setJobTitle("Unknown");
        userManagement.setRole(activeUserRoles);
        userManagement.setUsername(USERNAME);
        userManagement.setRcNumberForEmpRate(NOT_AVAILABLE);

        login(HOTEL, HOTEL_SECURITY);

        new LeftPanel().getHotelSecurity().click();
    }

    @Test(priority = 10)
    public void tryToRemoveAllRolesAndSave()
    {
        userManagementPage.searchMemberByAllRoles(userManagement);

        userManagementPage.getUserManagementGrid().getRowByUserName(userManagement).getUserManagementPopUp();
        userManagementDetailsPopUp.verifyUserData(userManagement);

        userManagementDetailsPopUp.getManagementRoleCheckBoxGroup().verifyUserRoles(activeUserRoles);

        userManagementDetailsPopUp.clickClearAll();
        userManagementDetailsPopUp.clickSaveChanges(verifyError(CANNOT_SAVE_WITHOUT_ROLES));
    }

    @Test(priority = 15)
    public void leaveOneRoleAndSave()
    {
        String testRole = Role.HOTEL_MANAGER;

        userManagement.setRole(Arrays.asList(testRole));

        userManagementDetailsPopUp.subscribeToOneRole(testRole);

        verifyAllUserData();

        userManagementDetailsPopUp.clickCancel();
    }

    @Test(priority = 20)
    public void addAllRolesAndSave()
    {
        activeUserRoles = new ArrayList<>(ALL_ROLES);
        userManagement.setRole(activeUserRoles);

        userManagementPage.searchMemberByAllRoles(userManagement);

        userManagementGrid.getRowByUserName(userManagement).getUserManagementPopUp().subscribeToAllRoles();

        verifyAllUserData();
    }

    private void verifyAllUserData()
    {
        userManagementPage.verifyAllMemberData(userManagement);
        userManagementPage.getUserManagementGrid().getRowByUserName(userManagement).getUserManagementPopUp();
        userManagementDetailsPopUp.verifyUserData(userManagement);
    }
}
