package com.ihg.crm.automation.selenium.tests.hotel.enrollment;

import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyNoError;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.assertThat;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.enroll.EmailConstant.EMAIL_WITH_10_DUPLICATES;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.automation.selenium.matchers.component.IsInvalid.isHighlightedAsInvalid;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.enroll.DuplicateEmailFoundPopUp;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.hotel.pages.enroll.EnrollmentPage;
import com.ihg.crm.automation.selenium.tests.hotel.LoginLogout;

public class DuplicateEnrollmentTest extends LoginLogout
{
    private Member member = new Member();
    private EnrollmentPage enrollmentPage;

    @BeforeClass
    public void before()
    {
        member.setPersonalInfo(MemberPopulateHelper.getSimplePersonalInfo());
        member.addProgram(Program.RC);
        member.addAddress(MemberPopulateHelper.getUnitedStatesAddress());
        member.addPhone(MemberPopulateHelper.getRandomPhone());

        login();

        enrollmentPage = new EnrollmentPage();
        enrollmentPage.goTo();
        enrollmentPage.populate(member, helper);
    }

    @Test
    public void tryToEnrollMemberWithDuplicateEmail()
    {
        enrollmentPage.getEmail().getEmail().type(EMAIL_WITH_10_DUPLICATES);
        enrollmentPage.clickSubmitAndSkipWarnings(verifyNoError());

        DuplicateEmailFoundPopUp emailFoundPopUp = new DuplicateEmailFoundPopUp();
        assertThat(emailFoundPopUp, displayed(true));
        verifyThat(emailFoundPopUp.getAbandonEnrollment(), displayed(true));
        verifyThat(emailFoundPopUp.getUpdateEmail(), displayed(true));
    }

    @Test(dependsOnMethods = { "tryToEnrollMemberWithDuplicateEmail" })
    public void clickUpdateEmail()
    {
        new DuplicateEmailFoundPopUp().clickUpdate();

        assertThat(enrollmentPage, displayed(true));
        verifyThat(enrollmentPage.getEmail().getEmail(), isHighlightedAsInvalid(true));
    }

    @Test(dependsOnMethods = { "clickUpdateEmail" })
    public void clickAbandonEnroll()
    {
        enrollmentPage.clickSubmitAndSkipWarnings(verifyNoError());
        new DuplicateEmailFoundPopUp().clickAbandonEnroll();

        verifyWelcome();
    }
}
