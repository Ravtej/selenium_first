package com.ihg.crm.automation.selenium.tests.hotel.operations.pointawards;

import static com.ihg.automation.common.DateUtils.getFormattedDate;
import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyNoError;
import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifySuccess;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.Constant.NOT_AVAILABLE;
import static com.ihg.automation.selenium.hotel.pages.Role.HOTEL_OPERATIONS_MANAGER;
import static com.ihg.automation.selenium.hotel.pages.hoteloperations.awards.manage.ManagePointAwardsSearch.DEFAULT_FROM_DATE_SHIFT;
import static com.ihg.automation.selenium.hotel.pages.hoteloperations.awards.posted.PostedPointAwardsPage.SUCCESS_MESSAGE;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasDefault;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasText;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.isHighlightedAsInvalid;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.isSelected;
import static com.ihg.automation.selenium.matchers.component.GridSize.size;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.event.SearchButtonBar;
import com.ihg.automation.selenium.common.member.AmbassadorUtils;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.pages.login.TopUserSection;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.gwt.components.dialog.ConfirmDialog;
import com.ihg.automation.selenium.hotel.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.awards.CreatePointAwardPopUp;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.awards.PointAward;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.awards.manage.ManagePointAwardsPage;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.awards.manage.ManagePointAwardsSearch;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.awards.posted.PostedPointAward;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.awards.posted.PostedPointAwardContainer;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.awards.posted.PostedPointAwardDetailsPopUp;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.awards.posted.PostedPointAwardsGridRow;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.awards.posted.PostedPointAwardsPage;
import com.ihg.automation.selenium.matchers.component.HasText;
import com.ihg.crm.automation.selenium.tests.hotel.LoginLogout;

public class CreatePointAwardsTest extends LoginLogout
{
    private static final String HOTEL = "SHGHA";
    private String membershipId;
    private PostedPointAwardsPage postedPointAwardsPage = new PostedPointAwardsPage();
    private PostedPointAward pointAwardWithoutReason = new PostedPointAward();
    private PostedPointAward pointAwardWithReason = new PostedPointAward();

    @BeforeClass
    public void before()
    {
        Member member = new Member();
        member.addProgram(Program.AMB);
        member.setPersonalInfo(MemberPopulateHelper.getSimplePersonalInfo());
        member.addAddress(MemberPopulateHelper.getUnitedStatesAddress());
        member.addEmail(MemberPopulateHelper.getRandomEmail());
        member.setAmbassadorAmount(AmbassadorUtils.PURCHASE_AMBASSADOR_HOTEL_$200);

        login(HOTEL);

        member = new EnrollmentPage().enroll(member, helper);
        membershipId = member.getAMBProgramId();

        pointAwardWithoutReason.setMemberNumber(membershipId);
        pointAwardWithoutReason.setTransaction(PointAward.HOTEL_PROMOTION);
        pointAwardWithoutReason.setMemberName(member);
        pointAwardWithoutReason.setHotel(HOTEL);
        pointAwardWithoutReason.setCheckInDate(NOT_AVAILABLE);
        pointAwardWithoutReason.setCheckOutDate(NOT_AVAILABLE);
        pointAwardWithoutReason.setPoints("250");
        pointAwardWithoutReason.setCostOfPoints("1.25");
        pointAwardWithoutReason.setSource(helper, HOTEL);

        pointAwardWithReason = pointAwardWithoutReason.clone();
        pointAwardWithReason.setTransaction(PointAward.SERVICE_RECOVERY);
        pointAwardWithReason.setReason("Room Location");
        pointAwardWithReason.setComments("test");

        new TopUserSection().switchUserRole(HOTEL_OPERATIONS_MANAGER);
    }

    @Test
    public void verifyManagePointAwardsFields()
    {
        ManagePointAwardsPage managePointAwardsPage = new ManagePointAwardsPage();
        managePointAwardsPage.goToByOperation();

        ManagePointAwardsSearch searchFields = managePointAwardsPage.getSearchFields();
        verifyThat(searchFields.getHotelCode(), hasText(helper.getUser().getLocation()));
        verifyThat(searchFields.getMemberNumber(), hasDefault());
        verifyThat(searchFields.getGuestName(), hasDefault());
        verifyThat(searchFields.getType(), hasText("All Types"));
        verifyThat(searchFields.getStatus(), hasText("All Statuses"));

        verifyThat(searchFields.getCheckIn(), isSelected(true));
        verifyThat(searchFields.getRequested(), isSelected(true));

        verifyThat(searchFields.getToDate(), hasText(getFormattedDate()));
        verifyThat(searchFields.getFromDate(), hasText(getFormattedDate(DEFAULT_FROM_DATE_SHIFT)));

        verifyThat(managePointAwardsPage.getManagePointAwardsGrid(), displayed(true));
    }

    @Test(dependsOnMethods = { "verifyManagePointAwardsFields" }, alwaysRun = true)
    public void verifyCreatePointAwardsFields()
    {
        ManagePointAwardsPage managePointAwardsPage = new ManagePointAwardsPage();
        managePointAwardsPage.getCreatePointAward().clickAndWait();

        CreatePointAwardPopUp createPointAwardPopUp = new CreatePointAwardPopUp();
        verifyThat(createPointAwardPopUp.getMembershipID(), hasDefault());
        verifyThat(createPointAwardPopUp.getHotel(), hasText(helper.getUser().getLocation()));
        verifyThat(createPointAwardPopUp.getPointAwardType(), hasDefault());
        verifyThat(createPointAwardPopUp.getAwardDescription(), hasDefault());

        verifyThat(createPointAwardPopUp.getClose(), displayed(true));
        verifyThat(createPointAwardPopUp.getSubmit(), displayed(true));
        verifyThat(createPointAwardPopUp.getClear(), displayed(true));
    }

    @Test(dependsOnMethods = { "verifyCreatePointAwardsFields" }, alwaysRun = true)
    public void closeCreatingPointAwards()
    {
        CreatePointAwardPopUp createPointAwardPopUp = new CreatePointAwardPopUp();
        createPointAwardPopUp.populate(pointAwardWithoutReason);
        createPointAwardPopUp.clickClose(verifyNoError());

        verifyThat(createPointAwardPopUp, displayed(false));

        verifyNoAwardWasCreated();
    }

    @Test(dependsOnMethods = { "closeCreatingPointAwards" }, alwaysRun = true)
    public void tryToCreatePointAwardsWithoutMandatoryFields()
    {
        postedPointAwardsPage.getCreatePointAward().clickAndWait();

        CreatePointAwardPopUp createPointAwardPopUp = new CreatePointAwardPopUp();
        createPointAwardPopUp.clickSubmit();
        verifyThat(createPointAwardPopUp.getMembershipID(), isHighlightedAsInvalid(true));
        verifyThat(createPointAwardPopUp.getPointAwardType(), isHighlightedAsInvalid(true));
        verifyThat(createPointAwardPopUp.getAwardDescription(), isHighlightedAsInvalid(true));

        createPointAwardPopUp.clickClose(verifyNoError());
    }

    @Test(dependsOnMethods = { "tryToCreatePointAwardsWithoutMandatoryFields" }, alwaysRun = true)
    public void clearCreatePointAwardFields()
    {
        postedPointAwardsPage.getCreatePointAward().clickAndWait();
        CreatePointAwardPopUp createPointAwardPopUp = new CreatePointAwardPopUp();

        createPointAwardPopUp.populate(pointAwardWithoutReason);

        createPointAwardPopUp.clickClear();
        verifyThat(createPointAwardPopUp.getMembershipID(), hasDefault());
        verifyThat(createPointAwardPopUp.getHotel(), hasDefault());
        verifyThat(createPointAwardPopUp.getPointAwardType(), hasDefault());
        verifyThat(createPointAwardPopUp.getAwardDescription(), hasDefault());

        createPointAwardPopUp.clickClose(verifyNoError());
    }

    @Test(dependsOnMethods = { "clearCreatePointAwardFields" }, alwaysRun = true)
    public void cancelCreatingPointAwards()
    {
        postedPointAwardsPage.getCreatePointAward().clickAndWait();

        CreatePointAwardPopUp createPointAwardPopUp = new CreatePointAwardPopUp();
        createPointAwardPopUp.populate(pointAwardWithoutReason);
        createPointAwardPopUp.clickSubmit(verifyNoError());

        ConfirmDialog warningPopUp = new ConfirmDialog();
        verifyThat(warningPopUp, displayed(true));
        verifyThat(warningPopUp, HasText.hasText(pointAwardWithoutReason.getCostOfPoints()
                + " USD will be billed to hotel SHGHA for this point award. Do you wish to continue with this transaction?"));
        warningPopUp.clickNo(verifyNoError());
        verifyThat(createPointAwardPopUp, displayed(false));

        verifyNoAwardWasCreated();
    }

    @Test(dependsOnMethods = { "cancelCreatingPointAwards" }, alwaysRun = true)
    public void createAndVerifyPointAwards()
    {
        postedPointAwardsPage.createPointAward(pointAwardWithoutReason,
                verifySuccess(SUCCESS_MESSAGE + pointAwardWithoutReason.getMemberNumber()));

        PostedPointAwardsGridRow row = postedPointAwardsPage.getPostedPointAwardsGrid().getRow(1);
        row.verify(pointAwardWithoutReason);

        PostedPointAwardContainer container = row.expand(PostedPointAwardContainer.class);

        container.verify(pointAwardWithoutReason);

        container.clickDetails();
        PostedPointAwardDetailsPopUp postedPointAwardDetailsPopUp = new PostedPointAwardDetailsPopUp();
        postedPointAwardDetailsPopUp.getPostedPointAwardDetailsTab().verify(pointAwardWithoutReason);
        postedPointAwardDetailsPopUp.clickCancel();
    }

    @Test(dependsOnMethods = { "createAndVerifyPointAwards" }, alwaysRun = true)
    public void createAndVerifyPointAwardsWithExtraFields()
    {
        postedPointAwardsPage.createPointAward(pointAwardWithReason);

        PostedPointAwardsGridRow row = postedPointAwardsPage.getPostedPointAwardsGrid().getRow(1);
        row.verify(pointAwardWithReason);

        PostedPointAwardContainer container = row.expand(PostedPointAwardContainer.class);
        container.verify(pointAwardWithReason);

        container.clickDetails();
        PostedPointAwardDetailsPopUp postedPointAwardDetailsPopUp = new PostedPointAwardDetailsPopUp();
        postedPointAwardDetailsPopUp.getPostedPointAwardDetailsTab().verify(pointAwardWithReason);
        postedPointAwardDetailsPopUp.clickCancel();
    }

    private void verifyNoAwardWasCreated()
    {
        postedPointAwardsPage.goTo();
        SearchButtonBar buttonBar = postedPointAwardsPage.getButtonBar();
        buttonBar.clickClearCriteria();
        postedPointAwardsPage.getSearchFields().getMemberNumber().typeAndWait(membershipId);
        buttonBar.clickSearch();

        verifyThat(postedPointAwardsPage.getPostedPointAwardsGrid(), size(0));
    }
}
