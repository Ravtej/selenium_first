package com.ihg.crm.automation.selenium.tests.hotel.guest;

import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyError;
import static com.ihg.automation.selenium.common.member.MemberPopulateHelper.getRandomEmail;
import static com.ihg.automation.selenium.common.member.MemberPopulateHelper.getRandomPhone;
import static com.ihg.automation.selenium.common.member.MemberPopulateHelper.getSimplePersonalInfo;
import static com.ihg.automation.selenium.common.member.MemberPopulateHelper.getUnitedStatesAddress;
import static com.ihg.automation.selenium.common.types.program.Program.RC;
import static com.ihg.automation.selenium.servicecenter.pages.Messages.CONCURRENT_CONTACT_EDIT_ERROR;

import java.util.HashMap;
import java.util.Map;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.personal.ContactListBase;
import com.ihg.automation.selenium.common.personal.EditableComponent;
import com.ihg.automation.selenium.hotel.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.hotel.pages.personal.HotelAddressContactList;
import com.ihg.automation.selenium.hotel.pages.personal.HotelEmailContactList;
import com.ihg.automation.selenium.hotel.pages.personal.HotelPhoneContactList;
import com.ihg.automation.selenium.hotel.pages.personal.HotelSmsContactList;
import com.ihg.automation.selenium.hotel.pages.personal.PersonalInfoPage;
import com.ihg.crm.automation.selenium.tests.hotel.LoginLogout;

public class ConcurrentUpdateTest extends LoginLogout
{
    private PersonalInfoPage personalInfoPage = new PersonalInfoPage();
    private Map<ContactListBase, Boolean> contactMap;
    private HotelPhoneContactList phoneList = personalInfoPage.getPhoneList();
    private HotelAddressContactList addressList = personalInfoPage.getAddressList();
    private HotelSmsContactList smsList = personalInfoPage.getSmsList();
    private HotelEmailContactList emailList = personalInfoPage.getEmailList();

    @BeforeClass
    public void beforeClass()
    {
        Member member = new Member();
        member.addProgram(RC);
        member.addEmail(getRandomEmail());
        member.addPhone(getRandomPhone());
        member.addAddress(getUnitedStatesAddress());
        member.setPersonalInfo(getSimplePersonalInfo());

        login();

        new EnrollmentPage().enroll(member, helper, false);
        smsList.add(getRandomPhone());

        contactMap = new HashMap<>();
        contactMap.put(phoneList, true);
        contactMap.put(addressList, true);
        contactMap.put(smsList, false);
        contactMap.put(emailList, false);
    }

    @DataProvider(name = "contactsProvider")
    protected Object[][] contactsProvider()
    {
        return new Object[][] { { phoneList }, { addressList }, { smsList }, { emailList } };
    }

    @Test(dataProvider = "contactsProvider")
    public void tryToEditAndAddContactWhileEditingAnother(ContactListBase contactList)
    {
        EditableComponent expandedContact = contactList.getExpandedContact();
        expandedContact.clickEdit();
        if (contactMap.get(contactList))
        {
            contactList.getAddButton().click(verifyError(CONCURRENT_CONTACT_EDIT_ERROR));
        }

        Map<ContactListBase, Boolean> contactMapWithoutActiveContact = new HashMap<>(contactMap);
        contactMapWithoutActiveContact.remove(contactList);
        for (Map.Entry<ContactListBase, Boolean> entry : contactMapWithoutActiveContact.entrySet())
        {
            ContactListBase key = entry.getKey();
            key.getExpandedContact().clickEdit(verifyError(CONCURRENT_CONTACT_EDIT_ERROR));
            if (entry.getValue())
            {
                key.getAddButton().click(verifyError(CONCURRENT_CONTACT_EDIT_ERROR));
            }
        }
        expandedContact.clickCancel();
    }
}
