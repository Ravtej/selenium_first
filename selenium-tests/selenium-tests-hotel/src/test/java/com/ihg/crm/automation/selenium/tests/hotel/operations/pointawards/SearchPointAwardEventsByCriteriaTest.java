package com.ihg.crm.automation.selenium.tests.hotel.operations.pointawards;

import static com.ihg.automation.common.MapUtils.queryResult;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.hotel.pages.Role.HOTEL_OPERATIONS_MANAGER;
import static com.ihg.automation.selenium.hotel.pages.hoteloperations.awards.manage.ManagePointAwardsGridRow.ManagePointAwardsGridCell.CHECK_IN;
import static com.ihg.automation.selenium.hotel.pages.hoteloperations.awards.manage.ManagePointAwardsGridRow.ManagePointAwardsGridCell.CHECK_OUT;
import static com.ihg.automation.selenium.hotel.pages.hoteloperations.awards.manage.ManagePointAwardsGridRow.ManagePointAwardsGridCell.MBR_NUMBER;
import static com.ihg.automation.selenium.hotel.pages.hoteloperations.awards.manage.ManagePointAwardsGridRow.ManagePointAwardsGridCell.MEMBER_NAME;
import static com.ihg.automation.selenium.matchers.component.GridSize.size;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static com.ihg.automation.selenium.matchers.text.ContainsIgnoreCase.containsIgnoreCase;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.core.Is.is;
import static org.joda.time.LocalDate.now;

import java.util.Map;

import org.joda.time.LocalDate;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.event.SearchButtonBar;
import com.ihg.automation.selenium.gwt.components.input.Input;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.awards.manage.ManagePointAwardsGrid;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.awards.manage.ManagePointAwardsGridRow;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.awards.manage.ManagePointAwardsPage;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.awards.manage.ManagePointAwardsSearch;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.awards.manage.ResultsFilter;
import com.ihg.crm.automation.selenium.tests.hotel.LoginLogout;

public class SearchPointAwardEventsByCriteriaTest extends LoginLogout
{

    private static final LocalDate VALID_TO_DATE = now();
    private static final LocalDate VALID_FROM_DATE = now().minusDays(30);

    private Map<String, String> managePointEventsMap;

    private static final String GET_POINT_AWARD_EVENTS = "SELECT MBRSHP_ID, LST_NM, FRST_NM, TO_CHAR(STY_CK_IN_DT, 'DDMonYY') STY_CK_IN_DT, TO_CHAR(STY_CK_OUT_DT, 'DDMonYY') STY_CK_OUT_DT, " //
            + "CONF_NBR, RATE_ITEM_RATE_CATGY_CD, CORP_NBR, HTL_CD " //
            + "FROM (SELECT MBRSHP_ID, LST_NM, FRST_NM, STY_CK_IN_DT, STY_CK_OUT_DT, CONF_NBR, RATE_ITEM_RATE_CATGY_CD, CORP_NBR, HTL_CD " //
            + ", COUNT(*) OVER (PARTITION BY LST_NM, CORP_NBR) co_count "
            + ", COUNT(*) OVER (PARTITION BY LST_NM, RATE_ITEM_RATE_CATGY_CD) rate_count " //
            + ", COUNT(*) OVER (PARTITION BY LST_NM, CONF_NBR) conf_count " //
            + "FROM RULE.PT_AWD_BKG_EXTR " //
            + "WHERE CONF_DT <= SYSDATE - 1 " //
            + "AND STY_CK_IN_DT >= SYSDATE - 30 " //
            + "AND STY_CK_OUT_DT >= SYSDATE " //
            + "AND CORP_NBR IS NOT NULL " //
            + "AND RATE_ITEM_RATE_CATGY_CD IS NOT NULL " //
            + "AND CONF_NBR IS NOT NULL) " //
            + "WHERE co_count = 1 AND rate_count = 1 AND conf_count = 1 " //
            + "AND ROWNUM < 2";

    private ManagePointAwardsPage managePointAwardsPage = new ManagePointAwardsPage();
    private ManagePointAwardsGrid grid = managePointAwardsPage.getManagePointAwardsGrid();
    private ResultsFilter resultsFilter = managePointAwardsPage.getResultsFilter();
    private ManagePointAwardsSearch managePointAwardsSearch = managePointAwardsPage.getSearchFields();
    private SearchButtonBar buttonBar;

    @BeforeClass
    public void before()
    {
        managePointEventsMap = queryResult(jdbcTemplate.queryForMap(GET_POINT_AWARD_EVENTS));

        login(managePointEventsMap.get("HTL_CD"), HOTEL_OPERATIONS_MANAGER);

        managePointAwardsPage.goToByOperation();

        resultsFilter.expand();

        buttonBar = managePointAwardsPage.getButtonBar();
    }

    @BeforeMethod
    public void beforeMethod()
    {
        buttonBar.clickClearCriteria();

        managePointAwardsSearch.fillDates(VALID_FROM_DATE, VALID_TO_DATE);

        managePointAwardsPage.getSearchFields().getGuestName().type(managePointEventsMap.get("LST_NM"));
    }

    @DataProvider(name = "resultFieldsProvider")
    public Object[][] resultFieldsDataProvider()
    {
        return new Object[][] { { resultsFilter.getConfirmation(), managePointEventsMap.get("CONF_NBR") }, //
                { resultsFilter.getRateCategory(), managePointEventsMap.get("RATE_ITEM_RATE_CATGY_CD") }, //
                { resultsFilter.getCorporateId(), managePointEventsMap.get("CORP_NBR") } };
    }

    @Test(dataProvider = "resultFieldsProvider")
    public void searchByNameAndSupplementaryCriteria(Input inputField, String searchCriteria)
    {
        inputField.type(searchCriteria);
        buttonBar.clickSearch();

        verifyThat(grid, size(is(greaterThan(0))));

        ManagePointAwardsGridRow row = grid.getRow(1);
        verifyThat(row.getCell(MEMBER_NAME), hasText(
                containsIgnoreCase(managePointEventsMap.get("LST_NM") + ", " + managePointEventsMap.get("FRST_NM"))));
        verifyThat(row.getCell(MBR_NUMBER), hasText(managePointEventsMap.get("MBRSHP_ID")));
        verifyThat(row.getCell(CHECK_IN), hasText(managePointEventsMap.get("STY_CK_IN_DT")));
        verifyThat(row.getCell(CHECK_OUT), hasText(managePointEventsMap.get("STY_CK_OUT_DT")));
    }
}
