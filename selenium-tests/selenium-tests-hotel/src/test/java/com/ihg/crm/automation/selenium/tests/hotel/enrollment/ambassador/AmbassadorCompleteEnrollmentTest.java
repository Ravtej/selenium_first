package com.ihg.crm.automation.selenium.tests.hotel.enrollment.ambassador;

import static com.ihg.automation.common.DateUtils.EXPIRATION_DATE_PATTERN;
import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyNoError;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.isDisplayedWithWait;
import static com.ihg.automation.selenium.matchers.component.HasDefaultText.hasDefault;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static org.hamcrest.core.StringEndsWith.endsWith;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.common.DateUtils;
import com.ihg.automation.selenium.common.RewardClubGridRow;
import com.ihg.automation.selenium.common.catalog.CatalogItem;
import com.ihg.automation.selenium.common.member.AmbassadorUtils;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.types.program.AmbassadorLevel;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.common.types.program.ProgramStatus;
import com.ihg.automation.selenium.common.types.program.RewardClubLevel;
import com.ihg.automation.selenium.gwt.components.panel.FieldSet;
import com.ihg.automation.selenium.hotel.pages.LeftPanel;
import com.ihg.automation.selenium.hotel.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.hotel.pages.programs.ProgramPageBase;
import com.ihg.automation.selenium.hotel.pages.programs.amb.AmbassadorPage;
import com.ihg.automation.selenium.hotel.pages.programs.amb.AmbassadorSummary;
import com.ihg.automation.selenium.hotel.pages.programs.amb.PurchaseDetailsPopUp;
import com.ihg.automation.selenium.hotel.pages.programs.amb.SuccessAmbEnrollmentPopUp;
import com.ihg.crm.automation.selenium.tests.hotel.LoginLogout;

public class AmbassadorCompleteEnrollmentTest extends LoginLogout
{
    private static final int MONTHS_FORWARD = 14;
    private static final String HOTEL = "SHGHA";
    private Member member = new Member();
    private static final CatalogItem ENROLMENT_AWARD = AmbassadorUtils.PURCHASE_AMBASSADOR_HOTEL_$200;
    private PurchaseDetailsPopUp purchaseDetailsPopUp = new PurchaseDetailsPopUp();
    private AmbassadorPage ambassadorPage = new AmbassadorPage();

    @BeforeClass
    public void beforeClass()
    {
        member.addProgram(Program.AMB);
        member.addEmail(MemberPopulateHelper.getRandomEmail());
        member.addPhone(MemberPopulateHelper.getRandomPhone());
        member.addAddress(MemberPopulateHelper.getUnitedStatesAddress());
        member.setPersonalInfo(MemberPopulateHelper.getSimplePersonalInfo());

        login(HOTEL);
    }

    @Test
    public void verifyPurchaseDetailsPopUp()
    {
        EnrollmentPage enrollmentPage = new EnrollmentPage();
        enrollmentPage.goTo();
        enrollmentPage.populate(member);
        enrollmentPage.getYourEmployeeID().type(helper.getUser().getEmployeeId());
        enrollmentPage.clickSubmitAndSkipWarnings(verifyNoError());
        verifyThat(purchaseDetailsPopUp, isDisplayedWithWait());
    }

    @Test(dependsOnMethods = { "verifyPurchaseDetailsPopUp" }, alwaysRun = true)
    public void verifySuccessEnrollmentPopUp()
    {
        purchaseDetailsPopUp.selectAmount(ENROLMENT_AWARD);
        purchaseDetailsPopUp.clickSubmit(verifyNoError());

        SuccessAmbEnrollmentPopUp popUp = new SuccessAmbEnrollmentPopUp();
        verifyThat(popUp, isDisplayedWithWait());
        member = popUp.putPrograms(member);
        verifyThat(popUp.getMemberId(Program.RC), hasText(popUp.getMemberId(Program.AMB).getText()));
        verifyThat(popUp.getPaymentMessage(), displayed(true));
        popUp.clickDone(verifyNoError());
    }

    @Test(dependsOnMethods = { "verifySuccessEnrollmentPopUp" }, alwaysRun = true)
    public void verifyOverview()
    {
        ambassadorPage = new AmbassadorPage();
        new LeftPanel().getProgramInfoPanel().getPrograms().getProgram(Program.AMB).goToProgramPage();
        verifyThat(ambassadorPage, displayed(true));

        FieldSet programOverview = ambassadorPage.getProgramOverview();
        programOverview.expand();
        verifyThat(programOverview, hasText(endsWith("Ambassador")));
    }

    @Test(dependsOnMethods = { "verifyOverview" }, alwaysRun = true)
    public void verifyProgramSummary()
    {
        AmbassadorSummary ambassadorSummary = ambassadorPage.getSummary();
        ambassadorSummary.verify(DateUtils.getMonthsForward(MONTHS_FORWARD, EXPIRATION_DATE_PATTERN),
                ProgramStatus.OPEN, AmbassadorLevel.AMB, member.getRCProgramId());
    }

    @Test(dependsOnMethods = { "verifyProgramSummary" }, alwaysRun = true)
    public void verifyEnrollmentDetails()
    {
        ProgramPageBase.EnrollmentDetails enrollmentDetails = ambassadorPage.getEnrollmentDetails();
        enrollmentDetails.verifyDetails(helper);
        verifyThat(enrollmentDetails.getReferringMember(), hasDefault());
    }

    @Test(dependsOnMethods = { "verifyEnrollmentDetails" }, alwaysRun = true)
    public void verifyPaymentDetails()
    {
        ProgramPageBase.EnrollmentDetails enrollmentDetails = ambassadorPage.getEnrollmentDetails();
        verifyThat(enrollmentDetails.getPaymentType(), hasDefault());
        verifyThat(enrollmentDetails.getPurchaseAmount(), hasDefault());
    }

    @Test(dependsOnMethods = { "verifyPaymentDetails" }, alwaysRun = true)
    public void verifyLeftPanel()
    {
        RewardClubGridRow leftPanel = new LeftPanel().getProgramInfoPanel().getPrograms().getRewardClubProgram();
        // click on RC program to reload status for member
        leftPanel.clickAndWait(verifyNoError());
        leftPanel.verify(RewardClubLevel.GOLD, "0");
    }
}
