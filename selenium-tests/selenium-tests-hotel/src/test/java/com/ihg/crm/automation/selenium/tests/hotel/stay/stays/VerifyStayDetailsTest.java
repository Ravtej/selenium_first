package com.ihg.crm.automation.selenium.tests.hotel.stay.stays;

import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyNoError;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.event.SearchButtonBar;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.pages.login.TopUserSection;
import com.ihg.automation.selenium.common.stay.Revenues;
import com.ihg.automation.selenium.common.stay.Stay;
import com.ihg.automation.selenium.common.types.Currency;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.hotel.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.stay.ResultsFilter;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.stay.lpu.LoyaltyPendingUpdatesPage;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.stay.stays.ReviewGuestStaysSearchPage;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.stay.stays.StayDetailsPopUp;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.stay.stays.StayGridRowContainer;
import com.ihg.crm.automation.selenium.tests.hotel.LoginLogout;
import com.ihg.crm.automation.selenium.tests.hotel.stay.HotelDBUtils;

public class VerifyStayDetailsTest extends LoginLogout
{
    private Member member = new Member();
    private Stay stay = new Stay();

    @BeforeClass
    public void beforeClass()
    {
        member.addProgram(Program.RC);
        member.addEmail(MemberPopulateHelper.getRandomEmail());
        member.addPhone(MemberPopulateHelper.getRandomPhone());
        member.addAddress(MemberPopulateHelper.getUnitedStatesAddress());
        member.setPersonalInfo(MemberPopulateHelper.getSimplePersonalInfo());

        new HotelDBUtils(jdbcTemplate).getGnrHotel(stay);
        stay.setNights(5);
        stay.setCheckInDateByCheckout();
        stay.setConfirmationNumber("1234567");
        stay.setFolioNumber("32165");
        stay.setRoomNumber("456");
        stay.setRoomType("PRES");
        stay.setCorporateAccountNumber("100230372");
        stay.setIataCode("9958309");
        stay.setRateCode("TEST");
        stay.setAvgRoomRate(100.00);
        stay.setIsQualifying(true);
        stay.getBookingDetails().setBookingSource("TEST");
        stay.getBookingDetails().setDataSource("GNR");

        Revenues revenues = stay.getRevenues();
        revenues.getOverallTotal().setAmount(1300.00);
        revenues.getRoom().setAmount(500.00);
        revenues.getFood().setAmount(100.00);
        revenues.getBeverage().setAmount(100.00);
        revenues.getPhone().setAmount(100.00);
        revenues.getMeeting().setAmount(100.00);
        revenues.getMandatoryLoyalty().setAmount(100.00);
        revenues.getOtherLoyalty().setAmount(100.00);
        revenues.getNoLoyalty().setAmount(100.00);
        revenues.getTax().setAmount(100.00);
        revenues.getTotalNonQualifying().setAmount(300.00);
        revenues.getTotalQualifying().setAmount(1000.00);

        login();

        member = new EnrollmentPage().enroll(member, helper, verifyNoError());

        new TopUserSection().switchHotel(stay.getHotelCode());
        new LoyaltyPendingUpdatesPage().goToByHotelOperations().createMissingStay(member, stay);
    }

    @Test
    public void searchAndExpandStay()
    {
        ReviewGuestStaysSearchPage reviewGuestStaysSearchPage = new ReviewGuestStaysSearchPage();
        reviewGuestStaysSearchPage.getTab().goTo();

        SearchButtonBar buttonBar = reviewGuestStaysSearchPage.getButtonBar();

        buttonBar.clickClearCriteria();

        ResultsFilter resultsFilter = reviewGuestStaysSearchPage.getResultsFilter();
        resultsFilter.expand();
        resultsFilter.getMemberNumber().type(member.getRCProgramId());
        reviewGuestStaysSearchPage.getSearchFields().getToDate().type(stay.getCheckOut());

        buttonBar.clickSearch();

        reviewGuestStaysSearchPage.getStayGrid().getRow(1).verify(member, stay);
    }

    @Test(dependsOnMethods = { "searchAndExpandStay" })
    public void verifyStayDetailsInView()
    {
        StayGridRowContainer stayGridRowContainer = new ReviewGuestStaysSearchPage().getStayGrid().getRow(1)
                .expand(StayGridRowContainer.class);
        stayGridRowContainer.verify(stay, member);
        verifyThat(stayGridRowContainer.getViewStayDetails(), displayed(true));
    }

    @Test(dependsOnMethods = { "verifyStayDetailsInView" })
    public void verifyStayAndRevenueDetails()
    {
        new ReviewGuestStaysSearchPage().getStayGrid().getRow(1).expand(StayGridRowContainer.class).getViewStayDetails()
                .clickAndWait(verifyNoError());

        new StayDetailsPopUp().getViewStayDetailsTab().getStayRevenueDetailsView().verifyRevenuesForNewStay(stay);
    }
}
