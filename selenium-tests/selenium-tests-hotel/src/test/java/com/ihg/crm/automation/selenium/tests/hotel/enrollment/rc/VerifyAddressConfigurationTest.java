package com.ihg.crm.automation.selenium.tests.hotel.enrollment.rc;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.automation.selenium.matchers.component.IsRequired.required;
import static org.hamcrest.CoreMatchers.allOf;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.components.Country;
import com.ihg.automation.selenium.common.contact.Address;
import com.ihg.automation.selenium.common.enroll.CustomerInfo;
import com.ihg.automation.selenium.common.types.address.AddressType;
import com.ihg.automation.selenium.common.types.address.Locality1Label;
import com.ihg.automation.selenium.common.types.address.RegionLabel;
import com.ihg.automation.selenium.common.types.address.ZipLabel;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.hotel.pages.enroll.EnrollmentPage;
import com.ihg.crm.automation.selenium.tests.hotel.LoginLogout;

public class VerifyAddressConfigurationTest extends LoginLogout
{
    private CustomerInfo customerInfo;
    private Address addressField;

    @BeforeClass
    public void beforeClass()
    {
        login();

        EnrollmentPage enrollPage = new EnrollmentPage();
        enrollPage.goTo();
        enrollPage.getPrograms().selectProgram(Program.RC);

        customerInfo = enrollPage.getCustomerInfo();
    }

    @Test
    public void verifyAddressConfigurationForUsBusiness()
    {
        customerInfo.getResidenceCountry().select(Country.US);

        EnrollmentPage enrollPage = new EnrollmentPage();
        addressField = enrollPage.getAddress();
        addressField.getType().selectByValue(AddressType.BUSINESS);

        verifyThat(addressField.getCompanyName(), allOf(displayed(true), required(false)));
        verifyThat(addressField.getAddress1(), allOf(displayed(true), required(true)));
        verifyThat(addressField.getAddress2(), allOf(displayed(true), required(false)));
        verifyThat(addressField.getLocality1(Locality1Label.CITY), allOf(displayed(true), required(true)));
        verifyThat(addressField.getRegion1(RegionLabel.STATE), allOf(displayed(true), required(true)));
        verifyThat(addressField.getZipCode(ZipLabel.ZIP), allOf(displayed(true), required(true)));
    }

    @Test(dependsOnMethods = { "verifyAddressConfigurationForUsBusiness" }, alwaysRun = true)
    public void verifyAddressConfigurationForCanadaBusiness()
    {
        customerInfo.getResidenceCountry().select(Country.CA);

        EnrollmentPage enrollPage = new EnrollmentPage();
        addressField = enrollPage.getAddress();
        addressField.getType().selectByValue(AddressType.BUSINESS);

        verifyThat(addressField.getCompanyName(), allOf(displayed(true), required(false)));
        verifyThat(addressField.getAddress1(), allOf(displayed(true), required(true)));
        verifyThat(addressField.getAddress2(), allOf(displayed(true), required(false)));
        verifyThat(addressField.getAddress3(), allOf(displayed(true), required(false)));
        verifyThat(addressField.getAddress4(), allOf(displayed(true), required(false)));
        verifyThat(addressField.getLocality1(Locality1Label.CITY), allOf(displayed(true), required(true)));
        verifyThat(addressField.getRegion1(RegionLabel.PROVINCE), allOf(displayed(true), required(true)));
        verifyThat(addressField.getZipCode(ZipLabel.POSTAL), allOf(displayed(true), required(true)));
    }

    @Test(dependsOnMethods = { "verifyAddressConfigurationForCanadaBusiness" }, alwaysRun = true)
    public void verifyAddressConfigurationForJapanBusiness()
    {
        customerInfo.getResidenceCountry().select(Country.JP);

        EnrollmentPage enrollPage = new EnrollmentPage();
        addressField = enrollPage.getAddress();
        addressField.getType().selectByValue(AddressType.BUSINESS);

        verifyThat(addressField.getCompanyName(), allOf(displayed(true), required(false)));
        verifyThat(addressField.getAddress1(), allOf(displayed(true), required(true)));
        verifyThat(addressField.getAddress2(), allOf(displayed(true), required(false)));
        verifyThat(addressField.getAddress3(), allOf(displayed(true), required(false)));
        verifyThat(addressField.getAddress4(), allOf(displayed(true), required(false)));
        verifyThat(addressField.getLocality1(Locality1Label.CITY_TOWN), allOf(displayed(true), required(true)));
        verifyThat(addressField.getRegion1(RegionLabel.PROVINCE), allOf(displayed(true), required(false)));
        verifyThat(addressField.getZipCode(ZipLabel.POSTAL), allOf(displayed(true), required(false)));
    }

    @Test(dependsOnMethods = { "verifyAddressConfigurationForJapanBusiness" }, alwaysRun = true)
    public void verifyAddressConfigurationForFranceBusiness()
    {
        customerInfo.getResidenceCountry().select(Country.FR);

        EnrollmentPage enrollPage = new EnrollmentPage();
        addressField = enrollPage.getAddress();
        addressField.getType().selectByValue(AddressType.BUSINESS);

        verifyThat(addressField.getCompanyName(), allOf(displayed(true), required(false)));
        verifyThat(addressField.getAddress1(), allOf(displayed(true), required(true)));
        verifyThat(addressField.getAddress2(), allOf(displayed(true), required(false)));
        verifyThat(addressField.getLocality1(Locality1Label.CITY_TOWN), allOf(displayed(true), required(true)));
        verifyThat(addressField.getZipCode(ZipLabel.POSTAL), allOf(displayed(true), required(false)));
    }
}
