package com.ihg.crm.automation.selenium.tests.hotel.enrollment.rc;

import static com.ihg.automation.common.RandomUtils.getRandomNumber;
import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyNoError;
import static com.ihg.automation.selenium.common.types.Alliance.JAL;
import static com.ihg.automation.selenium.common.types.program.RewardClubLevel.CLUB;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.enroll.SuccessEnrolmentPopUp;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberAlliance;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.hotel.pages.LeftPanel;
import com.ihg.automation.selenium.hotel.pages.enroll.EarningPreference;
import com.ihg.automation.selenium.hotel.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.hotel.pages.programs.rc.RewardClubPage;
import com.ihg.crm.automation.selenium.tests.hotel.LoginLogout;

public class EnrollWithMilesPreferencesTest extends LoginLogout
{
    private MemberAlliance memberAlliance = new MemberAlliance(JAL, getRandomNumber(9));
    EnrollmentPage enrollmentPage = new EnrollmentPage();

    @BeforeClass
    public void beforeClass()
    {
        Member member = new Member();
        member.addProgram(Program.RC);
        member.addEmail(MemberPopulateHelper.getRandomEmail());
        member.addPhone(MemberPopulateHelper.getRandomPhone());
        member.addAddress(MemberPopulateHelper.getUnitedStatesAddress());
        member.setPersonalInfo(MemberPopulateHelper.getSimplePersonalInfo());

        login();

        enrollmentPage.goTo();
        enrollmentPage.populate(member);
        enrollmentPage.getYourEmployeeID().type(helper.getUser().getEmployeeId());
    }

    @Test
    public void populateMilesPreferencesAndEnroll()
    {
        EarningPreference earningPreference = enrollmentPage.getEarningPreference();
        earningPreference.getEarningPreference().select("Miles");

        earningPreference.getAllianceDetails().populate(memberAlliance);

        enrollmentPage.clickSubmitAndSkipWarnings(verifyNoError());

        new SuccessEnrolmentPopUp().clickDone(verifyNoError());
    }

    @Test(dependsOnMethods = { "populateMilesPreferencesAndEnroll" })
    public void verifyEarningDetails()
    {
        RewardClubPage rewardClubPage = new RewardClubPage();
        rewardClubPage.goTo();

        rewardClubPage.getEarningDetails().verify(memberAlliance);
    }

    @Test(dependsOnMethods = { "verifyEarningDetails" })
    public void verifyLeftPanel()
    {
        new LeftPanel().getProgramInfoPanel().getPrograms().getRewardClubProgram().verify(CLUB, JAL, "0");
    }
}
