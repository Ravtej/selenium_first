package com.ihg.crm.automation.selenium.tests.hotel.operations;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.hotel.pages.Role.HOTEL_SECURITY;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.pages.login.TopUserSection;
import com.ihg.automation.selenium.hotel.pages.LeftPanel;
import com.ihg.crm.automation.selenium.tests.hotel.LoginLogout;

public class EmpRateEligibilityButtonTest extends LoginLogout
{
    @BeforeClass
    public void beforeClass()
    {
        login("ATLCP", HOTEL_SECURITY);
    }

    @Test(priority = 10)
    public void verifyButtonForFranchiseLocation()
    {
        verifyThat(new LeftPanel().getManageEmpRateEligibility(), displayed(true));
    }

    @Test(priority = 20)
    public void verifyButtonForNonFranchiseLocation()
    {
        new TopUserSection().switchHotel("LONCY", false);
        verifyThat(new LeftPanel().getManageEmpRateEligibility(), displayed(false));
    }
}
