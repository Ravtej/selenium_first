package com.ihg.crm.automation.selenium.tests.hotel.operations.orderpointvoucher;

import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyNoError;
import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifySuccess;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.order.DeliveryOption.PAPER;
import static com.ihg.automation.selenium.common.order.VoucherOrderStatus.PROCESSING;
import static com.ihg.automation.selenium.hotel.pages.Role.HOTEL_OPERATIONS_MANAGER;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.enabled;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasText;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.isDisplayedAndEnabled;
import static com.ihg.automation.selenium.matchers.component.GridSize.size;
import static com.ihg.automation.selenium.matchers.text.EntityMatcherFactory.isValue;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.core.StringContains.containsString;

import org.springframework.beans.factory.annotation.Value;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.components.Country;
import com.ihg.automation.selenium.common.order.VoucherOrder;
import com.ihg.automation.selenium.common.order.VoucherRule;
import com.ihg.automation.selenium.gwt.components.dialog.ConfirmDialog;
import com.ihg.automation.selenium.gwt.components.utils.WaitUtils;
import com.ihg.automation.selenium.hotel.pages.LeftPanel;
import com.ihg.automation.selenium.hotel.pages.Tabs;
import com.ihg.automation.selenium.hotel.pages.order.NewOrdersPage;
import com.ihg.automation.selenium.hotel.pages.order.OrderDetailsPopUp;
import com.ihg.automation.selenium.hotel.pages.order.OrderDetailsTab;
import com.ihg.automation.selenium.hotel.pages.order.OrderHistoryPage;
import com.ihg.automation.selenium.hotel.pages.order.OrdersGridRow;
import com.ihg.automation.selenium.hotel.pages.order.OrdersSearch;
import com.ihg.automation.selenium.hotel.pages.order.VoucherOrderGridRowContainer;
import com.ihg.automation.selenium.hotel.pages.order.VoucherOrderPopUp;
import com.ihg.automation.selenium.hotel.pages.order.VouchersGridRow;
import com.ihg.automation.selenium.hotel.pages.personal.CustomerInfoFields;
import com.ihg.automation.selenium.hotel.pages.personal.HotelAddressContactList;
import com.ihg.automation.selenium.hotel.pages.personal.PersonalInfoPage;
import com.ihg.crm.automation.selenium.tests.hotel.LoginLogout;

public class OrderPointVoucherTest extends LoginLogout
{
    private VoucherRule voucherRule;
    private VoucherOrder voucherOrder;

    @Value("${order.point.voucher.member}")
    private String htlMember;

    private static final String HOTEL = "BODHA";

    @BeforeClass
    public void beforeClass()
    {
        voucherRule = new VoucherRule("PVH10K", "Hotel Vouchers Queen's English 10000");
        voucherRule.setVoucherName("Hotel Vouchers Queen's English 10000");
        voucherRule.setCostAmount("50.00");
        voucherRule.setTotalCostAmount("1,250.00");
        voucherRule.setPointVoucherDenomination("10000");

        voucherOrder = new VoucherOrder(voucherRule);
        voucherOrder.setStatus(PROCESSING);
        voucherOrder.setNumberOfVouchers("25");
        voucherOrder.setDeliveryOption(PAPER);

        login(HOTEL, HOTEL_OPERATIONS_MANAGER);
        voucherOrder.setSource(helper);

        new LeftPanel().getOrderPointVoucher().clickAndWait(verifyNoError());
    }

    @Test(priority = 1)
    public void verifyPersonalInfoPage()
    {
        PersonalInfoPage personalInfoPage = new PersonalInfoPage();
        CustomerInfoFields customerInfo = personalInfoPage.getCustomerInfo();
        verifyThat(customerInfo.getFullName(), hasText("ICON " + HOTEL));
        verifyThat(customerInfo.getResidenceCountry(), hasText(isValue(Country.FR)));
        verifyThat(customerInfo.getEdit(), isDisplayedAndEnabled(false));

        HotelAddressContactList hotelAddressContactList = personalInfoPage.getAddressList();
        verifyThat(hotelAddressContactList.getContact().getBaseContact(),
                hasText(containsString("2-5 PL DE LA COMEDIE")));
        verifyThat(hotelAddressContactList.getAddButton(), enabled(false));

        verifyThat(personalInfoPage.getPhoneList().getAddButton(), enabled(false));
        verifyThat(personalInfoPage.getSmsList(), displayed(false));
        verifyThat(personalInfoPage.getEmailList().getAddButton(), enabled(false));
    }

    @Test(priority = 5)
    public void verifyOrderHistoryFields()
    {
        new Tabs().getEventsTab().clickAndWait(verifyNoError());

        OrderHistoryPage orderHistoryPage = new OrderHistoryPage();
        OrdersSearch searchFields = orderHistoryPage.getSearchFields();
        verifyThat(searchFields.getItemId(), isDisplayedAndEnabled(true));
        verifyThat(searchFields.getItemName(), isDisplayedAndEnabled(true));
        verifyThat(searchFields.getDateFrom(), isDisplayedAndEnabled(true));
        verifyThat(searchFields.getDateTo(), isDisplayedAndEnabled(true));
        verifyThat(searchFields.getStatus(), isDisplayedAndEnabled(true));

        verifyThat(orderHistoryPage.getOrdersGrid(), size(greaterThan(0)));
    }

    @Test(priority = 10)
    public void verifyNewOrdersTabFields()
    {
        new Tabs().getEventsTab().getNewOrders().clickAndWait(verifyNoError());
        verifyThat(new NewOrdersPage().getVouchersGrid(), size(greaterThan(0)));
    }

    @Test(priority = 15)
    public void verifyVoucherOrderPopUp()
    {
        VouchersGridRow vouchersGridRow = new NewOrdersPage().getVouchersGrid().getRow(voucherRule.getPromotionId());
        vouchersGridRow.verify(voucherRule);
        vouchersGridRow.clickById();

        VoucherOrderPopUp voucherOrderPopUp = new VoucherOrderPopUp();
        voucherOrderPopUp.verify(voucherRule);
        voucherOrderPopUp.verifyShippingInfoPaperFulfillment();
    }

    @Test(priority = 20)
    public void cancelPlacingOfOrder()
    {
        new VoucherOrderPopUp().clickPlaceOrder();

        ConfirmDialog warningPopUp = new ConfirmDialog();
        verifyThat(warningPopUp, displayed(true));
        verifyThat(warningPopUp, hasText(voucherRule.getTotalCostAmount() + " USD will be billed to hotel " + HOTEL
                + " for this voucher order. Do you wish to continue with this transaction?"));
        warningPopUp.clickNo(verifyNoError());
        verifyThat(new VoucherOrderPopUp(), displayed(true));
    }

    @Test(priority = 25)
    public void placeOrder()
    {
        new VoucherOrderPopUp().clickPlaceOrder();
        new ConfirmDialog().clickYes(verifyNoError(), verifySuccess("Order voucher have been successfully created."));
    }

    @Test(priority = 30)
    public void verifyCreatedOrder()
    {
        OrderHistoryPage orderHistoryPage = new OrderHistoryPage();
        orderHistoryPage.searchOrder(voucherOrder);

        OrdersGridRow gridRow = orderHistoryPage.getOrdersGrid().getRow(1);
        gridRow.verify(voucherOrder);

        VoucherOrderGridRowContainer rowContainer = gridRow.expand(VoucherOrderGridRowContainer.class);
        rowContainer.verify(voucherOrder);
        rowContainer.clickDetails();

        new OrderDetailsPopUp().getOrderDetailsTab().verify(voucherOrder);
    }

    @Test(priority = 35)
    public void rejectCancellingProcessingOrder()
    {
        OrderDetailsPopUp orderDetailsPopUp = new OrderDetailsPopUp();
        orderDetailsPopUp.clickCancelOrder();

        ConfirmDialog warningPopUp = new ConfirmDialog();
        verifyThat(warningPopUp, displayed(true));
        verifyThat(warningPopUp, hasText("Are you sure you want to cancel the voucher order?\n" + "\n" //
                + "Credit for cancelled orders may not appear on hotel invoice until next billing cycle.\n" + "\n" //
                + "Select Yes to continue with order cancellation."));
        warningPopUp.clickNo(verifyNoError());

        OrderDetailsTab orderDetailsTab = orderDetailsPopUp.getOrderDetailsTab();
        verifyThat(orderDetailsTab, displayed(true));
        verifyThat(orderDetailsTab.getOrderDetails().getDeliveryStatus(), hasText(PROCESSING.getName()));
    }

    @Test(priority = 40)
    public void confirmCancellingProcessingOrder()
    {
        OrderDetailsPopUp orderDetailsPopUp = new OrderDetailsPopUp();
        orderDetailsPopUp.clickCancelOrder();

        new ConfirmDialog().clickYes(verifyNoError(), verifySuccess("Voucher Order has been cancelled."));

        verifyThat(orderDetailsPopUp.getOrderDetailsTab(), displayed(false));

        OrderHistoryPage orderHistoryPage = new OrderHistoryPage();
        new WaitUtils().waitExecutingRequest();
        orderHistoryPage.searchOrder(voucherOrder);
        verifyThat(orderHistoryPage.getOrdersGrid(), size((0)));
    }
}
