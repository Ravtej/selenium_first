package com.ihg.crm.automation.selenium.tests.hotel.operations.reimbursement;

import static com.ihg.automation.common.DateUtils.DATE_FORMAT;
import static com.ihg.automation.common.MapUtils.queryResult;
import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyNoError;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.hotel.pages.hoteloperations.reimbursement.otheractivity.OtherActivityGridRow.OtherActivityGridCell.CHECK_IN_DATE;
import static com.ihg.automation.selenium.hotel.pages.hoteloperations.reimbursement.otheractivity.OtherActivityGridRow.OtherActivityGridCell.STATUS;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasAnyText;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasTextAsDate;
import static com.ihg.automation.selenium.matchers.component.GridSize.size;
import static com.ihg.automation.selenium.matchers.component.HasDefaultText.hasDefault;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static com.ihg.automation.selenium.matchers.component.IsSelected.isSelected;
import static com.ihg.automation.selenium.matchers.date.IsAfterOrEqualsDate.isAfterOrEquals;
import static com.ihg.automation.selenium.matchers.date.IsBeforeOrEqualsDate.isBeforeOrEquals;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNot.not;
import static org.hamcrest.core.StringContains.containsString;
import static org.joda.time.LocalDate.now;

import java.util.List;
import java.util.Map;

import org.joda.time.LocalDate;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.event.SearchButtonBar;
import com.ihg.automation.selenium.hotel.pages.LeftPanel;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.HotelOperationsPage;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.reimbursement.otheractivity.OtherActivityContainer;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.reimbursement.otheractivity.OtherActivityGrid;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.reimbursement.otheractivity.OtherActivityGridRow;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.reimbursement.otheractivity.OtherActivityPage;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.reimbursement.otheractivity.OtherActivitySearch;
import com.ihg.crm.automation.selenium.tests.hotel.LoginLogout;

public class OtherActivityTest extends LoginLogout
{
    private OtherActivityPage otherActivityPage;
    private OtherActivitySearch otherActivitySearch;
    private OtherActivityGrid otherActivityGrid;

    private Map<String, String> otherActivityEventMap;
    private static final LocalDate VALID_FROM_DATE = now().minusYears(2);
    private static final LocalDate VALID_TO_DATE = now();
    private static final String HOTEL = "ATLCP";
    private static final String CERT_STAT_CD = "PAID";

    private static final String GET_OTHER_ACTIVITY_EVENT = "SELECT TO_CHAR(CHECK_IN_DATE, 'DDMonYY') AS CHECK_IN_DATE, " //
            + "TO_CHAR(CHECK_OUT_DATE, 'DDMonYY') AS CHECK_OUT_DATE, " //
            + "TRIM(PMS_FOLIO_NBR), TRIM(BUNDLE_ID), TO_CHAR(REIMB_DATE, 'DDMonYY') AS REIMB_DATE, " //
            + "TRIM(GUEST_NAME), AGT_CERT_STAT_CD, REIMB_TOT_AMT, REIMB_CURR_CD, AGENT_ID, TRIM(AWARD_ID_NUMBER), " //
            + "CONFIRMATION_NBR, TRIM(AGENT_CERT_ID) " //
            + "FROM FMDS.TMAGTCRT " //
            + "WHERE FACILITY_ID IN " //
            + "(SELECT FAC_NBR " //
            + "FROM GROWTH_PRDHTL.HOTEL " //
            + "WHERE HLDX_CD = '%s') " //
            + "AND AGT_CERT_STAT_CD = '%s' " //
            + "AND CONFIRMATION_NBR <> 0 " //
            + "AND CONFIRMATION_NBR IS NOT NULL " //
            + "AND AGENT_CERT_ID IS NOT NULL " //
            + "AND PMS_FOLIO_NBR IS NOT NULL " //
            + "AND BUNDLE_ID IS NOT NULL " //
            + "AND REIMB_METHOD IS NOT NULL " //
            + "AND REIMB_DATE IS NOT NULL " //
            + "AND ROWNUM < 2";

    @BeforeClass
    public void before()
    {
        otherActivityEventMap = queryResult(
                jdbcTemplate.queryForMap(String.format(GET_OTHER_ACTIVITY_EVENT, HOTEL, CERT_STAT_CD)));

        login();

        new LeftPanel().getHotelOperations().clickAndWait(verifyNoError());
        new HotelOperationsPage().getOtherActivity().clickAndWait(verifyNoError());
    }

    @Test(priority = 1)
    public void verifyPageComponents()
    {
        otherActivityPage = new OtherActivityPage();

        OtherActivitySearch searchFields = otherActivityPage.getSearchFields();
        verifyThat(searchFields.getHotelCode(), hasText(HOTEL));
        verifyThat(searchFields.getItemID(), hasDefault());
        verifyThat(searchFields.getStatusOfRequest(), hasText("All Statuses"));
        verifyThat(searchFields.getCertificateNumber(), hasDefault());
        verifyThat(searchFields.getCheckIn(), isSelected(true));
        verifyThat(searchFields.getCheckOut(), isSelected(false));
        verifyThat(searchFields.getFromDate(), hasDefault());
        verifyThat(searchFields.getToDate(), hasDefault());
        verifyThat(searchFields.getConfirmationNumber(), hasDefault());
    }

    @Test(priority = 5, dataProvider = "statusProvider")
    public void verifySearchByStatus(String status)
    {
        otherActivitySearch = otherActivityPage.getSearchFields();

        otherActivitySearch.getStatusOfRequest().select(status);

        SearchButtonBar buttonBar = otherActivityPage.getButtonBar();
        buttonBar.clickSearch();

        otherActivityGrid = otherActivityPage.getOtherActivityGrid();
        verifyThat(otherActivityGrid, size(is(greaterThan(0))));

        List<OtherActivityGridRow> gridRowList = otherActivityGrid.getRowList();
        for (OtherActivityGridRow row : gridRowList)
        {
            verifyThat(row.getCell(STATUS), hasText(status));
        }

        buttonBar.clickClearCriteria();
    }

    @Test(priority = 10)
    public void verifyValidDateRange()
    {
        otherActivitySearch.getFromDate().type(VALID_FROM_DATE.toString(DATE_FORMAT));
        otherActivitySearch.getToDate().type(VALID_TO_DATE.toString(DATE_FORMAT));
        otherActivitySearch.getCheckOut().check();
        otherActivityPage.getButtonBar().clickSearch();

        List<OtherActivityGridRow> gridRowList = otherActivityGrid.getRowList();
        for (OtherActivityGridRow row : gridRowList)
        {
            verifyThat(row.getCell(CHECK_IN_DATE), hasTextAsDate("ddMMMYY", isAfterOrEquals(VALID_FROM_DATE)));
            verifyThat(row.getCell(CHECK_IN_DATE), hasTextAsDate("ddMMMYY", isBeforeOrEquals(VALID_TO_DATE)));
        }
        otherActivityPage.getButtonBar().clickClearCriteria();
    }

    @Test(priority = 15)
    public void verifyOtherActivityEventRow()
    {
        otherActivityGrid = otherActivityPage.getOtherActivityGrid();
        otherActivitySearch.getStatusOfRequest().select(CERT_STAT_CD);
        otherActivitySearch.getConfirmationNumber().type(otherActivityEventMap.get("CONFIRMATION_NBR"));
        otherActivityPage.getButtonBar().clickSearch();

        verifyThat(otherActivityGrid, size(not(0)));

        otherActivityGrid.getRow(1).verify(otherActivityEventMap.get("CHECK_IN_DATE"), //
                otherActivityEventMap.get("AGENT_CERT_ID"), //
                otherActivityEventMap.get("GUEST_NAME"), //
                otherActivityEventMap.get("AGT_CERT_STAT_CD"), //
                otherActivityEventMap.get("REIMB_TOT_AMT"), //
                otherActivityEventMap.get("REIMB_CURR_CD"));
    }

    @Test(priority = 20)
    public void verifyOtherActivityRowDetails()
    {
        OtherActivityContainer rowContainer = otherActivityGrid.getRow(1).expand(OtherActivityContainer.class);
        verifyThat(rowContainer.getBillingEntity(), hasText(otherActivityEventMap.get("AGENT_ID")));
        verifyThat(rowContainer.getConfirmationNumber(), hasText(otherActivityEventMap.get("CONFIRMATION_NBR")));
        verifyThat(rowContainer.getItemID(), hasText(otherActivityEventMap.get("AWARD_ID_NUMBER")));
        verifyThat(rowContainer.getDescriptionGuestName(), hasText(otherActivityEventMap.get("GUEST_NAME")));
        verifyThat(rowContainer.getCertificateNumber(), hasText(otherActivityEventMap.get("AGENT_CERT_ID")));
        verifyThat(rowContainer.getFolioNumber(), hasText(otherActivityEventMap.get("PMS_FOLIO_NBR")));
        verifyThat(rowContainer.getStatus(), hasText(otherActivityEventMap.get("AGT_CERT_STAT_CD")));
        verifyThat(rowContainer.getBundleNumber(), hasText(otherActivityEventMap.get("BUNDLE_ID")));
        verifyThat(rowContainer.getReimbursementAmount(),
                hasText(containsString(otherActivityEventMap.get("REIMB_TOT_AMT"))));
        verifyThat(rowContainer.getReimbursementMethod(), hasAnyText());
        verifyThat(rowContainer.getReimbursementDate(), hasText(otherActivityEventMap.get("REIMB_DATE")));
        verifyThat(rowContainer.getCheckInDate(), hasText(otherActivityEventMap.get("CHECK_IN_DATE")));
        verifyThat(rowContainer.getCheckOutDate(), hasText(otherActivityEventMap.get("CHECK_OUT_DATE")));
    }

    @DataProvider(name = "statusProvider")
    public Object[][] statusDataProvider()
    {
        return new Object[][] { { "ACCEPTED" }, { "CANCELED" }, { "INITIATED" }, { "PAID" } };
    }
}
