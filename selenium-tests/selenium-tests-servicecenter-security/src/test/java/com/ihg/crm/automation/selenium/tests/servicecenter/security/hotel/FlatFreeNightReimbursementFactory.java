package com.ihg.crm.automation.selenium.tests.servicecenter.security.hotel;

import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.HotelOperations.FREE_NIGHT_FLAT_V;

import java.util.HashMap;
import java.util.Map;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Factory;

import com.ihg.crm.automation.selenium.tests.servicecenter.security.SecurityMatrix;

public class FlatFreeNightReimbursementFactory
{
    @DataProvider(name = "flatFreeNightReimbursementAvailabilityProvider")
    public Object[][] flatFreeNightReimbursementAvailabilityProvider()
    {
        Map<String, Boolean> defaultPermissions = new HashMap<String, Boolean>();
        defaultPermissions.put(FREE_NIGHT_FLAT_V, false);

        SecurityMatrix permissions = new SecurityMatrix(defaultPermissions);

        permissions.getAgentTierLevel().put(FREE_NIGHT_FLAT_V, true);

        permissions.getHotelHelpDesk().put(FREE_NIGHT_FLAT_V, true);

        permissions.getAdminOperationsManager().put(FREE_NIGHT_FLAT_V, true);

        permissions.getAdminCentralProcessing().put(FREE_NIGHT_FLAT_V, true);

        permissions.getFraudTeam().put(FREE_NIGHT_FLAT_V, true);

        permissions.getSystemsAdministration().put(FREE_NIGHT_FLAT_V, true);

        permissions.getSystemsManager().put(FREE_NIGHT_FLAT_V, true);

        permissions.getSystemsCoordinator().put(FREE_NIGHT_FLAT_V, true);

        return permissions.getPermissionArray();
    }

    @Factory(dataProvider = "flatFreeNightReimbursementAvailabilityProvider")
    public Object[] createInstances(String role, Map<String, Boolean> permissions)
    {
        return new Object[] { new FlatFreeNightReimbursementFactoryTest(role, permissions) };
    }
}
