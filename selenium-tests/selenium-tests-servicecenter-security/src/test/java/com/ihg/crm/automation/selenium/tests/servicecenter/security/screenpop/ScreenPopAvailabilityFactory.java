package com.ihg.crm.automation.selenium.tests.servicecenter.security.screenpop;

import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Screenpop.ROLE_SCREENPOP_V;

import java.util.HashMap;
import java.util.Map;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Factory;

import com.ihg.crm.automation.selenium.tests.servicecenter.security.SecurityMatrix;

public class ScreenPopAvailabilityFactory
{
    @DataProvider(name = "screenPopAvailabilityProvider")
    public Object[][] screenPopAvailabilityProvider()
    {
        Map<String, Boolean> defaultPermissions = new HashMap<String, Boolean>();
        defaultPermissions.put(ROLE_SCREENPOP_V, false);

        SecurityMatrix permissions = new SecurityMatrix(defaultPermissions);

        permissions.getAgentPermissions().put(ROLE_SCREENPOP_V, true);

        permissions.getAgentTierLevel().put(ROLE_SCREENPOP_V, true);

        permissions.getHotelHelpDesk().put(ROLE_SCREENPOP_V, true);

        return permissions.getPermissionArray();
    }

    @Factory(dataProvider = "screenPopAvailabilityProvider")
    public Object[] createInstances(String role, Map<String, Boolean> permissions)
    {
        return new Object[] { new ScreenPopAvailabilityFactoryTest(role, permissions) };
    }
}
