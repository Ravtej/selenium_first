package com.ihg.crm.automation.selenium.tests.servicecenter.security.hotel;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.isDisplayedAndEnabled;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.HotelOperations.APPROVE_REWARD_NIGHTS;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.HotelOperations.REWARD_NIGHTS_OCCUPANCY_M;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.HotelOperations.REWARD_NIGHTS_OCCUPANCY_V;

import java.util.Map;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.servicecenter.pages.TopMenu;
import com.ihg.automation.selenium.servicecenter.pages.hotel.freenight.FreeNightReimbursementPage;
import com.ihg.automation.selenium.servicecenter.pages.hotel.freenight.FreeNightReimbursementSearchGridRow;
import com.ihg.automation.selenium.servicecenter.pages.hotel.freenight.FreeNightReimbursementSearchGridRow.FreeNightReimbursementCell;
import com.ihg.automation.selenium.servicecenter.pages.hotel.freenight.FreeNightReimbursementSearchGridRowAdjustPanel;
import com.ihg.crm.automation.selenium.tests.servicecenter.security.LoginLogout;

public class HotelRewardNightReimbursementFactoryTest extends LoginLogout
{
    private TopMenu topMenu = new TopMenu();
    private FreeNightReimbursementPage freeNightReimbursementPage = new FreeNightReimbursementPage();

    public HotelRewardNightReimbursementFactoryTest(String role, Map<String, Boolean> permissions)
    {
        super(role, permissions);
    }

    @BeforeClass
    public void beforeClass()
    {
        login();
    }

    @Test
    public void verifyHotelOperationsTab()
    {
        verifyThat(topMenu.getHotelOperations(), displayed(permissions.get(REWARD_NIGHTS_OCCUPANCY_V)));
    }

    @Test(dependsOnMethods = "verifyHotelOperationsTab")
    public void verifyViewRewardFreeNightReimbursement()
    {
        if (permissions.get(REWARD_NIGHTS_OCCUPANCY_V))
        {
            freeNightReimbursementPage.goTo();

            verifyThat(freeNightReimbursementPage, displayed(permissions.get(REWARD_NIGHTS_OCCUPANCY_V)));
            verifyThat(freeNightReimbursementPage.getButtonBar().getSearch(),
                    displayed(permissions.get(REWARD_NIGHTS_OCCUPANCY_V)));

        }
    }

    @Test(dependsOnMethods = "verifyHotelOperationsTab")
    public void verifyAdjustRewardFreeNightReimbursement()
    {
        if (permissions.get(REWARD_NIGHTS_OCCUPANCY_V))
        {
            freeNightReimbursementPage.goTo();
            freeNightReimbursementPage.getSearchFields().getHotelCode().type("ATLCP");
            freeNightReimbursementPage.getButtonBar().clickSearch();
            FreeNightReimbursementSearchGridRow row = freeNightReimbursementPage.getGrid().getRow(1);
            verifyThat(row.getCell(FreeNightReimbursementCell.ADJUST).asLink(),
                    displayed(permissions.get(REWARD_NIGHTS_OCCUPANCY_M)));

            if (permissions.get(REWARD_NIGHTS_OCCUPANCY_M))
            {
                FreeNightReimbursementSearchGridRowAdjustPanel adjustPanel = row.openAdjustPanel();

                verifyThat(adjustPanel.getSave(), isDisplayedAndEnabled(permissions.get(REWARD_NIGHTS_OCCUPANCY_M)));
                adjustPanel.getCancel().click();
            }
        }
    }

    @Test(dependsOnMethods = "verifyHotelOperationsTab")
    public void verifyApproveRewardFreeNight()
    {
        if (permissions.get(REWARD_NIGHTS_OCCUPANCY_V))
        {
            freeNightReimbursementPage.goTo();
            verifyThat(freeNightReimbursementPage.getButtonBar().getApproveSelected(),
                    isDisplayedAndEnabled(permissions.get(APPROVE_REWARD_NIGHTS)));
        }
    }
}
