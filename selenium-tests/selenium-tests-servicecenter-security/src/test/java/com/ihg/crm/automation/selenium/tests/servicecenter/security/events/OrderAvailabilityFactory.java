package com.ihg.crm.automation.selenium.tests.servicecenter.security.events;

import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Order.ORDER_HISTORY_V;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Order.ORDER_M;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Order.ORDER_V;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Order.RESEND;

import java.util.HashMap;
import java.util.Map;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Factory;

import com.ihg.crm.automation.selenium.tests.servicecenter.security.SecurityMatrix;

public class OrderAvailabilityFactory
{
    @DataProvider(name = "verifyOrderAvailabilityProvider")
    public Object[][] verifyOrderAvailabilityProvider()
    {
        Map<String, Boolean> defaultPermissions = setPermissionMap();

        SecurityMatrix permissions = new SecurityMatrix(defaultPermissions);

        permissions.getAgentPermissions().put(RESEND, false);

        permissions.getProgramSales().put(ORDER_M, false);
        permissions.getProgramSales().put(RESEND, false);

        permissions.getAgentTierLevel().put(RESEND, false);

        permissions.getHotelHelpDesk().put(RESEND, false);

        permissions.getAdminAwardSupport().put(RESEND, false);

        permissions.getAdminOperationsManager().put(RESEND, false);

        permissions.getAdminCentralProcessing().put(RESEND, false);

        permissions.getFraudTeam().put(RESEND, false);

        permissions.getAgentReadOnly().put(ORDER_V, false);
        permissions.getAgentReadOnly().put(ORDER_M, false);
        permissions.getAgentReadOnly().put(RESEND, false);
        permissions.getAgentReadOnly().put(ORDER_HISTORY_V, false);

        permissions.getSystemsAdministration().put(RESEND, false);

        permissions.getSystemsManager().put(RESEND, false);

        permissions.getSystemsCoordinator().put(RESEND, false);

        permissions.getManagerMarketing().put(ORDER_M, false);
        permissions.getManagerMarketing().put(RESEND, false);

        permissions.getReadOnlyMarketing().put(ORDER_M, false);
        permissions.getReadOnlyMarketing().put(RESEND, false);

        permissions.getReadOnlyVendor().put(ORDER_M, false);
        permissions.getReadOnlyVendor().put(RESEND, false);

        permissions.getPartnerVendor().put(ORDER_M, false);
        permissions.getPartnerVendor().put(RESEND, false);

        permissions.getHumanResources().put(ORDER_M, false);
        permissions.getHumanResources().put(RESEND, false);

        return permissions.getPermissionArray();
    }

    private Map<String, Boolean> setPermissionMap()
    {
        Map<String, Boolean> permissions = new HashMap<String, Boolean>();

        permissions.put(ORDER_V, true);
        permissions.put(ORDER_M, true);
        permissions.put(RESEND, true);
        permissions.put(ORDER_HISTORY_V, true);

        return permissions;
    }

    @Factory(dataProvider = "verifyOrderAvailabilityProvider")
    public Object[] createInstances(String role, Map<String, Boolean> permissions)
    {
        return new Object[] { new OrderAvailabilityFactoryTest(role, permissions) };
    }
}
