package com.ihg.crm.automation.selenium.tests.servicecenter.security.voucher;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.isSelected;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Voucher.DEPOSITS_V;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Voucher.VOUCHER_UNVOID_M;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Voucher.VOUCHER_V;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Voucher.VOUCHER_VOID_M;

import java.util.Map;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.servicecenter.pages.TopMenu;
import com.ihg.automation.selenium.servicecenter.pages.voucher.VoucherDepositTab;
import com.ihg.automation.selenium.servicecenter.pages.voucher.VoucherLookupTab;
import com.ihg.automation.selenium.servicecenter.pages.voucher.VoucherPopUp;
import com.ihg.crm.automation.selenium.tests.servicecenter.security.LoginLogout;

public class VoucherAvailabilityFactoryTest extends LoginLogout
{
    private TopMenu topMenu = new TopMenu();
    private VoucherPopUp voucherPopUp;
    private VoucherLookupTab tab;
    private String voucherNumber;
    private static final String AVAILABLE_VOUCHER = "SELECT * FROM"//
            + " (SELECT TO_CHAR(BON_VOUCHER_ID,'FM000000000')||TO_CHAR(FACILITY_ID,'FM00000')||TO_CHAR(BON_VOUCHER_PIN,'FM0000') VOUCHER_NUMBER"//
            + " FROM FMDS.TMBNVCH"//
            + " WHERE VOUCHER_SENT_IND = 'Y' AND VOUCHER_STAT_CD = ' '"//
            + " AND MEMBERSHIP_ID IS NULL"//
            + " AND PROMO_ID = 'CTUCGE'"//
            + " AND TRIM(LAST_UPDATE_USERID)!='user1'"//
            + " ORDER BY LAST_UPDATE_TMSTMP DESC)"//
            + " WHERE ROWNUM < 2";
    private static final String VOUCHER_VOID = "SELECT * FROM"//
            + " (SELECT TO_CHAR(BON_VOUCHER_ID,'FM000000000')||TO_CHAR(FACILITY_ID,'FM00000')||TO_CHAR(BON_VOUCHER_PIN,'FM0000') VOUCHER_NUMBER"//
            + " FROM FMDS.TMBNVCH"//
            + " WHERE VOUCHER_SENT_IND = 'N' AND VOUCHER_STAT_CD = 'V'"//
            + " AND TRIM(LAST_UPDATE_USERID)!='user1'"//
            + " ORDER BY LAST_UPDATE_TMSTMP DESC)"//
            + " WHERE ROWNUM < 2";

    public VoucherAvailabilityFactoryTest(String role, Map<String, Boolean> permissions)
    {
        super(role, permissions);
    }

    @BeforeClass
    private void beforeClass()
    {
        voucherNumber = getVoucherNumber(AVAILABLE_VOUCHER);
        login();
    }

    @Test
    public void verifyVoucherMenu()
    {
        verifyThat(topMenu.getVoucher(), displayed(permissions.get(VOUCHER_V)));
    }

    @Test(dependsOnMethods = { "verifyVoucherMenu" })
    public void verifyLookupVoucherMenu()
    {
        if (permissions.get(VOUCHER_V))
        {
            verifyThat(topMenu.getVoucher().getLookup(), displayed(permissions.get(VOUCHER_V)));
            openAndVerifyVoucherPopUp(voucherNumber);
            tab.clickClose();
        }
    }

    @Test(dependsOnMethods = { "verifyVoucherMenu" })
    public void verifyVoidButton()
    {
        if (permissions.get(VOUCHER_V))
        {
            tab = openAndVerifyVoucherPopUp(voucherNumber);
            verifyThat(tab.getVoid(), displayed(permissions.get(VOUCHER_VOID_M)));
            tab.clickClose();
        }
    }

    @Test(dependsOnMethods = { "verifyVoucherMenu" })
    public void verifyUnVoidButton()
    {
        if (permissions.get(VOUCHER_V))
        {
            tab = openAndVerifyVoucherPopUp(getVoucherNumber(VOUCHER_VOID));
            verifyThat(tab.getUnvoid(), displayed(permissions.get(VOUCHER_UNVOID_M)));
            tab.clickClose();
        }
    }

    @Test(dependsOnMethods = { "verifyVoucherMenu" })
    public void verifyDepositVoucher()
    {
        if (permissions.get(VOUCHER_V))
        {
            verifyThat(topMenu.getVoucher().getDeposit(), displayed(permissions.get(DEPOSITS_V)));

            VoucherDepositTab voucherDepositTab = new VoucherPopUp().getVoucherDepositTab();
            voucherDepositTab.goTo();
            verifyThat(voucherDepositTab, isSelected(true));
            voucherDepositTab.clickClose();
            verifyThat(voucherDepositTab, displayed(false));
        }
    }

    private VoucherLookupTab openAndVerifyVoucherPopUp(String voucherNumber)
    {
        voucherPopUp = new VoucherPopUp();
        tab = voucherPopUp.getVoucherLookupTab();
        tab.goTo();
        tab.getVoucherNumber().type(voucherNumber);
        tab.getLookup().clickAndWait();
        verifyThat(tab.getVoucherDetailsPanel(), displayed(permissions.get(VOUCHER_V)));
        return tab;
    }

    private String getVoucherNumber(String sql)
    {
        Map<String, Object> map = jdbcTemplate.queryForMap(String.format(sql));
        return map.get("VOUCHER_NUMBER").toString();
    }
}
