package com.ihg.crm.automation.selenium.tests.servicecenter.security.events;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.enabled;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Stay.STAY_A;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Stay.STAY_ADJUST_M;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Stay.STAY_HISTORY_V;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Stay.STAY_V;

import java.util.Map;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.servicecenter.pages.Tabs;
import com.ihg.automation.selenium.servicecenter.pages.event.stay.AdjustStayPopUp;
import com.ihg.automation.selenium.servicecenter.pages.event.stay.StayEventsPage;
import com.ihg.automation.selenium.servicecenter.pages.event.stay.StayGuestGridRowContainer;
import com.ihg.automation.selenium.servicecenter.pages.event.stay.StayInfoEdit;
import com.ihg.crm.automation.selenium.tests.servicecenter.security.LoginLogout;

public class StayAvailabilityFactoryTest extends LoginLogout
{
    private StayEventsPage stayPage = new StayEventsPage();
    private Tabs.EventsTab eventsTab = new Tabs().getEvents();

    public StayAvailabilityFactoryTest(String role, Map<String, Boolean> permissions)
    {
        super(role, permissions);
    }

    @BeforeClass
    public void beforeClass()
    {
        loginAndOpenMember(memberIdWithFreenightStay);
        eventsTab.goTo();
    }

    @Test
    public void verifyStayTab()
    {
        verifyThat(eventsTab.getStay(), displayed(permissions.get(STAY_V)));
    }

    @Test(dependsOnMethods = "verifyStayTab")
    public void verifyAdjustStay()
    {
        if (permissions.get(STAY_V))
        {
            StayGuestGridRowContainer rowContainer = stayPage.getGuestGrid().getRow(1)
                    .expand(StayGuestGridRowContainer.class);
            rowContainer.clickDetails();

            AdjustStayPopUp stayDetailsPopUp = new AdjustStayPopUp();
            StayInfoEdit stayInfoEdit = stayDetailsPopUp.getStayDetailsTab().getStayDetails().getStayInfo();

            verifyThat(stayDetailsPopUp, displayed(true));
            verifyThat(stayInfoEdit.getCheckIn(), displayed(permissions.get(STAY_ADJUST_M)));
            verifyThat(stayInfoEdit.getCheckOut(), displayed(permissions.get(STAY_ADJUST_M)));
            verifyThat(stayDetailsPopUp.getSaveChanges(), enabled(permissions.get(STAY_ADJUST_M)));

            stayDetailsPopUp.clickClose();
        }
    }

    @Test(dependsOnMethods = "verifyStayTab")
    public void verifyCreateStay()
    {
        if (permissions.get(STAY_V))
        {
            stayPage.goTo();
            verifyThat(stayPage.getButtonBar().getCreateStay(), displayed(permissions.get(STAY_A)));
        }
    }

    @Test(dependsOnMethods = "verifyStayTab")
    public void verifyHistoryStayTab()
    {
        if (permissions.get(STAY_V))
        {
            StayGuestGridRowContainer rowContainer = stayPage.getGuestGrid().getRow(1)
                    .expand(StayGuestGridRowContainer.class);
            rowContainer.clickDetails();

            AdjustStayPopUp stayDetailsPopUp = new AdjustStayPopUp();

            verifyThat(stayDetailsPopUp.getHistoryTab().getTab(), displayed(permissions.get(STAY_HISTORY_V)));
            stayDetailsPopUp.clickClose();
        }
    }
}
