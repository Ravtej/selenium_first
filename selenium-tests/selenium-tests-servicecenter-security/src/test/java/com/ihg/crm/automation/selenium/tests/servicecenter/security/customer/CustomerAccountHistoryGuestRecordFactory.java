package com.ihg.crm.automation.selenium.tests.servicecenter.security.customer;

import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Guest.ACCOUNT_REINSTATE_M;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Guest.ACCOUNT_REMOVE_M;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Guest.GUEST_ACCOUNT_HISTORY_V;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Guest.GUEST_FLAG_M;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Guest.GUEST_FLAG_V;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Guest.GUEST_NEW_ACCOUNT_V;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Guest.GUEST_STATUS_V;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Guest.GUEST_SYNTH_V;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Guest.PIN_M;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Guest.PIN_V;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Guest.SECURITY_M;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Guest.SECURITY_V;

import java.util.HashMap;
import java.util.Map;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Factory;

import com.ihg.crm.automation.selenium.tests.servicecenter.security.SecurityMatrix;

public class CustomerAccountHistoryGuestRecordFactory
{
    @DataProvider(name = "verifyCustomerAccountInfoProvider")
    public Object[][] verifyCustomerAccountInfoProvider()
    {
        Map<String, Boolean> defaultPermissions = setPermissionMap();

        SecurityMatrix permissions = new SecurityMatrix(defaultPermissions);
        permissions.getAgentPermissions().put(GUEST_SYNTH_V, false);
        permissions.getAgentPermissions().put(ACCOUNT_REMOVE_M, false);
        permissions.getAgentPermissions().put(ACCOUNT_REINSTATE_M, false);
        permissions.getAgentPermissions().put(PIN_M, false);

        permissions.getProgramSales().put(ACCOUNT_REMOVE_M, false);
        permissions.getProgramSales().put(ACCOUNT_REINSTATE_M, false);
        permissions.getProgramSales().put(SECURITY_V, false);
        permissions.getProgramSales().put(SECURITY_M, false);
        permissions.getProgramSales().put(PIN_V, false);
        permissions.getProgramSales().put(PIN_M, false);
        permissions.getProgramSales().put(GUEST_FLAG_M, false);

        permissions.getAgentTierLevel().put(GUEST_SYNTH_V, false);
        permissions.getAgentTierLevel().put(ACCOUNT_REMOVE_M, false);
        permissions.getAgentTierLevel().put(ACCOUNT_REINSTATE_M, false);
        permissions.getAgentTierLevel().put(PIN_M, false);

        permissions.getHotelHelpDesk().put(GUEST_SYNTH_V, false);
        permissions.getHotelHelpDesk().put(ACCOUNT_REMOVE_M, false);
        permissions.getHotelHelpDesk().put(ACCOUNT_REINSTATE_M, false);
        permissions.getHotelHelpDesk().put(PIN_V, false);

        permissions.getAdminAwardSupport().put(GUEST_SYNTH_V, false);
        permissions.getAdminAwardSupport().put(ACCOUNT_REMOVE_M, false);
        permissions.getAdminAwardSupport().put(ACCOUNT_REINSTATE_M, false);
        permissions.getAdminAwardSupport().put(PIN_V, false);

        permissions.getAdminOperationsManager().put(PIN_V, false);

        permissions.getAdminCentralProcessing().put(ACCOUNT_REMOVE_M, false);
        permissions.getAdminCentralProcessing().put(ACCOUNT_REINSTATE_M, false);
        permissions.getAdminCentralProcessing().put(PIN_V, false);

        permissions.getFraudTeam().put(PIN_V, false);

        permissions.getAgentReadOnly().put(GUEST_SYNTH_V, false);
        permissions.getAgentReadOnly().put(GUEST_NEW_ACCOUNT_V, false);
        permissions.getAgentReadOnly().put(ACCOUNT_REMOVE_M, false);
        permissions.getAgentReadOnly().put(ACCOUNT_REINSTATE_M, false);
        permissions.getAgentReadOnly().put(SECURITY_V, false);
        permissions.getAgentReadOnly().put(SECURITY_M, false);
        permissions.getAgentReadOnly().put(PIN_V, false);
        permissions.getAgentReadOnly().put(PIN_M, false);
        permissions.getAgentReadOnly().put(GUEST_FLAG_V, false);
        permissions.getAgentReadOnly().put(GUEST_FLAG_M, false);
        permissions.getAgentReadOnly().put(GUEST_ACCOUNT_HISTORY_V, false);
        permissions.getAgentReadOnly().put(GUEST_ACCOUNT_HISTORY_V, false);

        permissions.getSystemsAdministration().put(PIN_V, false);

        permissions.getSystemsManager().put(PIN_V, false);

        permissions.getSystemsCoordinator().put(PIN_V, false);

        permissions.getManagerMarketing().put(ACCOUNT_REMOVE_M, false);
        permissions.getManagerMarketing().put(ACCOUNT_REINSTATE_M, false);
        permissions.getManagerMarketing().put(SECURITY_V, false);
        permissions.getManagerMarketing().put(SECURITY_M, false);
        permissions.getManagerMarketing().put(PIN_V, false);
        permissions.getManagerMarketing().put(PIN_M, false);

        permissions.getReadOnlyMarketing().put(ACCOUNT_REMOVE_M, false);
        permissions.getReadOnlyMarketing().put(ACCOUNT_REINSTATE_M, false);
        permissions.getReadOnlyMarketing().put(SECURITY_V, false);
        permissions.getReadOnlyMarketing().put(SECURITY_M, false);
        permissions.getReadOnlyMarketing().put(PIN_V, false);
        permissions.getReadOnlyMarketing().put(PIN_M, false);
        permissions.getReadOnlyMarketing().put(GUEST_FLAG_M, false);

        permissions.getReadOnlyVendor().put(ACCOUNT_REMOVE_M, false);
        permissions.getReadOnlyVendor().put(ACCOUNT_REINSTATE_M, false);
        permissions.getReadOnlyVendor().put(SECURITY_V, false);
        permissions.getReadOnlyVendor().put(SECURITY_M, false);
        permissions.getReadOnlyVendor().put(PIN_V, false);
        permissions.getReadOnlyVendor().put(PIN_M, false);
        permissions.getReadOnlyVendor().put(GUEST_FLAG_M, false);

        permissions.getPartnerVendor().put(ACCOUNT_REMOVE_M, false);
        permissions.getPartnerVendor().put(ACCOUNT_REINSTATE_M, false);
        permissions.getPartnerVendor().put(SECURITY_V, false);
        permissions.getPartnerVendor().put(SECURITY_M, false);
        permissions.getPartnerVendor().put(PIN_V, false);
        permissions.getPartnerVendor().put(PIN_M, false);
        permissions.getPartnerVendor().put(GUEST_FLAG_M, false);

        permissions.getHumanResources().put(ACCOUNT_REMOVE_M, false);
        permissions.getHumanResources().put(ACCOUNT_REINSTATE_M, false);
        permissions.getHumanResources().put(SECURITY_V, false);
        permissions.getHumanResources().put(SECURITY_M, false);
        permissions.getHumanResources().put(GUEST_FLAG_M, false);

        return permissions.getPermissionArray();
    }

    private Map<String, Boolean> setPermissionMap()
    {
        Map<String, Boolean> permissions = new HashMap<String, Boolean>();

        permissions.put(GUEST_SYNTH_V, true);
        permissions.put(GUEST_NEW_ACCOUNT_V, true);
        permissions.put(GUEST_STATUS_V, true);
        permissions.put(ACCOUNT_REMOVE_M, true);
        permissions.put(ACCOUNT_REINSTATE_M, true);
        permissions.put(SECURITY_V, true);
        permissions.put(SECURITY_M, true);
        permissions.put(PIN_V, true);
        permissions.put(PIN_M, true);
        permissions.put(GUEST_FLAG_V, true);
        permissions.put(GUEST_FLAG_M, true);
        permissions.put(GUEST_ACCOUNT_HISTORY_V, true);

        return permissions;
    }

    @Factory(dataProvider = "verifyCustomerAccountInfoProvider")
    public Object[] createInstances(String role, Map<String, Boolean> permissions)
    {
        return new Object[] { new CustomerAccountHistoryGuestRecordFactoryTest(role, permissions) };
    }
}
