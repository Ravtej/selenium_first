package com.ihg.crm.automation.selenium.tests.servicecenter.security.screenpop;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Screenpop.ROLE_SCREENPOP_V;

import java.util.Map;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.servicecenter.pages.TopMenu;
import com.ihg.crm.automation.selenium.tests.servicecenter.security.LoginLogout;

public class ScreenPopAvailabilityFactoryTest extends LoginLogout
{
    protected TopMenu topMenu = new TopMenu();

    public ScreenPopAvailabilityFactoryTest(String role, Map<String, Boolean> permissions)
    {
        super(role, permissions);
    }

    @BeforeClass
    private void beforeClass()
    {
        login();
    }

    @Test
    public void verifyFlatFreeNightReimbursementAvailability()
    {
        verifyThat(topMenu.getPopInfoAvailable(), displayed(permissions.get(ROLE_SCREENPOP_V)));
    }
}
