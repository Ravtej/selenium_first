package com.ihg.crm.automation.selenium.tests.servicecenter.security.unit;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.UnitManagement.GOODWILL_UNITS;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.UnitManagement.PURCHASE_GIFT;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.UnitManagement.PURCHASE_TIER_LEVEL;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.UnitManagement.PURCHASE_UNITS;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.UnitManagement.TRANSFER_Q_NIGHTS;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.UnitManagement.TRANSFER_UNITS;

import java.util.Map;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.servicecenter.pages.TopMenu;
import com.ihg.crm.automation.selenium.tests.servicecenter.security.LoginLogout;

public class UnitOperationsAvailabilityFactoryTest extends LoginLogout
{
    private TopMenu topMenu = new TopMenu();

    public UnitOperationsAvailabilityFactoryTest(String role, Map<String, Boolean> permissions)
    {
        super(role, permissions);
    }

    @BeforeClass
    public void beforeClass()
    {
        loginAndOpenMember(programsMemberIdWithOfferAndOrder);
    }

    @Test
    public void verifyPurchaseUnitsOperationAvailability()
    {
        verifyThat(topMenu.getUnitManagement().getPurchaseUnits(), displayed(permissions.get(PURCHASE_UNITS)));
    }

    @Test
    public void verifyGiftOperationAvailability()
    {
        verifyThat(topMenu.getUnitManagement().getPurchaseGift(), displayed(permissions.get(PURCHASE_GIFT)));
    }

    @Test
    public void verifyPurchaseTierLevelOperationAvailability()
    {
        verifyThat(topMenu.getUnitManagement().getPurchaseTierLevel(), displayed(permissions.get(PURCHASE_TIER_LEVEL)));
    }

    @Test
    public void verifyGoodwillOperationAvailability()
    {
        verifyThat(topMenu.getUnitManagement().getGoodwillUnits(), displayed(permissions.get(GOODWILL_UNITS)));
    }

    @Test
    public void verifyTransferUnitsOperationAvailability()
    {
        verifyThat(topMenu.getUnitManagement().getTransferUnits(), displayed(permissions.get(TRANSFER_UNITS)));
    }

    @Test
    public void verifyTransferNightsOperationAvailability()
    {
        verifyThat(topMenu.getUnitManagement().getTransferQNights(), displayed(permissions.get(TRANSFER_Q_NIGHTS)));
    }

}
