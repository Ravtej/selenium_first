package com.ihg.crm.automation.selenium.tests.servicecenter.security.customer;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.isDisplayedAndEnabled;
import static com.ihg.automation.selenium.servicecenter.pages.personal.SocialIdGridRow.SocialIdCell.DELETE;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Guest.CONTACT_EMAIL_M;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Guest.GUEST_CONTACT_PHONE_ADDRESS_M;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Guest.GUEST_PERSONAL_M;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Guest.GUEST_PERSONAL_V;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Guest.GUEST_SMS_M;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Guest.GUEST_SMS_V;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Guest.GUEST_SOC_D;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Guest.GUEST_SOC_M;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Guest.GUEST_SOC_V;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Guest.PASSPORT_M;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Guest.PASSPORT_V;

import java.util.Map;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.personal.SmsContact;
import com.ihg.automation.selenium.servicecenter.pages.personal.PersonalInfoPage;
import com.ihg.crm.automation.selenium.tests.servicecenter.security.LoginLogout;

public class CustomerPersonalInfoAvailabilityFactoryTest extends LoginLogout
{
    private PersonalInfoPage personalInfoPage = new PersonalInfoPage();

    public CustomerPersonalInfoAvailabilityFactoryTest(String role, Map<String, Boolean> permissions)
    {
        super(role, permissions);
    }

    @BeforeClass
    private void beforeClass()
    {
        loginAndOpenMember(programsMemberIdWithOfferAndOrder);
        personalInfoPage.goTo();
    }

    @Test
    public void verifyViewPersonalInformation()
    {
        verifyThat(personalInfoPage, displayed(permissions.get(GUEST_PERSONAL_V)));
    }

    @Test(dependsOnMethods = { "verifyViewPersonalInformation" })
    public void verifyEditPersonalInformation()
    {
        verifyThat(personalInfoPage.getCustomerInfo().getEdit(),
                isDisplayedAndEnabled(permissions.get(GUEST_PERSONAL_M)));
    }

    @Test(dependsOnMethods = { "verifyViewPersonalInformation" })
    public void verifyAddAddress()
    {
        verifyThat(personalInfoPage.getAddressList().getAddButton(),
                isDisplayedAndEnabled(permissions.get(GUEST_CONTACT_PHONE_ADDRESS_M)));
    }

    @Test(dependsOnMethods = { "verifyViewPersonalInformation" })
    public void verifyEditAddress()
    {
        verifyThat(personalInfoPage.getAddressList().getContact().getEdit(),
                isDisplayedAndEnabled(permissions.get(GUEST_CONTACT_PHONE_ADDRESS_M)));
    }

    @Test(dependsOnMethods = { "verifyViewPersonalInformation" })
    public void verifyRemoveAddress()
    {
        verifyThat(personalInfoPage.getAddressList().getContact().getRemove(),
                isDisplayedAndEnabled(permissions.get(GUEST_CONTACT_PHONE_ADDRESS_M)));
    }

    @Test(dependsOnMethods = { "verifyViewPersonalInformation" })
    public void verifyAddPhone()
    {
        verifyThat(personalInfoPage.getPhoneList().getAddButton(),
                displayed(permissions.get(GUEST_CONTACT_PHONE_ADDRESS_M)));
    }

    @Test(dependsOnMethods = { "verifyViewPersonalInformation" })
    public void verifyEditPhone()
    {
        verifyThat(personalInfoPage.getPhoneList().getContact().getEdit(),
                displayed(permissions.get(GUEST_CONTACT_PHONE_ADDRESS_M)));
    }

    @Test(dependsOnMethods = { "verifyViewPersonalInformation" })
    public void verifyRemovePhone()
    {
        verifyThat(personalInfoPage.getPhoneList().getContact().getRemove(),
                isDisplayedAndEnabled(permissions.get(GUEST_CONTACT_PHONE_ADDRESS_M)));
    }

    @Test(dependsOnMethods = { "verifyViewPersonalInformation" })
    public void verifyPhoneSmsTypeView()
    {
        verifyThat(personalInfoPage.getSmsList().getContact(), displayed(permissions.get(GUEST_SMS_V)));
    }

    @Test(dependsOnMethods = { "verifyViewPersonalInformation" })
    public void verifyPhoneSmsTypeEdit()
    {
        if (permissions.get(GUEST_SMS_V) && permissions.get(GUEST_CONTACT_PHONE_ADDRESS_M))
        {
            SmsContact smsContact = personalInfoPage.getSmsList().getContact();
            verifyThat(smsContact.getEdit(), displayed(permissions.get(GUEST_SMS_M)));
        }
    }

    @Test(dependsOnMethods = { "verifyViewPersonalInformation" })
    public void verifyPhoneSmsTypeRemove()
    {
        if (permissions.get(GUEST_SMS_V))
        {
            verifyThat(personalInfoPage.getSmsList().getContact().getRemove(),
                    isDisplayedAndEnabled(permissions.get(GUEST_SMS_M)));
        }
    }

    @Test(dependsOnMethods = { "verifyViewPersonalInformation" })
    public void verifyAddEmail()
    {
        verifyThat(personalInfoPage.getEmailList().getAddButton(), displayed(permissions.get(CONTACT_EMAIL_M)));
    }

    @Test(dependsOnMethods = { "verifyViewPersonalInformation" })
    public void verifyEditEmail()
    {
        verifyThat(personalInfoPage.getEmailList().getContact().getEdit(),
                isDisplayedAndEnabled(permissions.get(CONTACT_EMAIL_M)));
    }

    @Test(dependsOnMethods = { "verifyViewPersonalInformation" })
    public void verifyRemoveEmail()
    {
        verifyThat(personalInfoPage.getEmailList().getContact().getRemove(),
                isDisplayedAndEnabled(permissions.get(CONTACT_EMAIL_M)));
    }

    @Test(dependsOnMethods = { "verifyViewPersonalInformation" })
    public void verifyViewPassport()
    {
        verifyThat(personalInfoPage.getPassport(), displayed(permissions.get(PASSPORT_V)));
    }

    @Test(dependsOnMethods = { "verifyViewPersonalInformation" })
    public void verifyEditPassport()
    {
        verifyThat(personalInfoPage.getPassport().getEdit(), isDisplayedAndEnabled(permissions.get(PASSPORT_M)));
    }

    @Test(groups = { "socialId" }, dependsOnMethods = { "verifyViewPersonalInformation" })
    public void verifyViewSocialId()
    {
        verifyThat(personalInfoPage.getSocialId(), displayed(permissions.get(GUEST_SOC_V)));
    }

    @Test(groups = { "socialId" }, dependsOnMethods = { "verifyViewPersonalInformation" })
    public void verifyAddSocialId()
    {
        verifyThat(personalInfoPage.getSocialId().getAddSocial(), displayed(permissions.get(GUEST_SOC_M)));
    }

    @Test(groups = { "socialId" }, dependsOnMethods = { "verifyViewPersonalInformation" })
    public void verifyDeleteSocialId()
    {
        if (permissions.get(GUEST_SOC_V))
        {
            verifyThat(personalInfoPage.getSocialId().getSocialIdGrid().getRow(1).getCell(DELETE).asLink(),
                    displayed(permissions.get(GUEST_SOC_D)));
        }
    }
}
