package com.ihg.crm.automation.selenium.tests.servicecenter.security;

public class PermissionConstant
{
    public static class Enroll
    {
        public static final String ENROLLMENT_ENROLL = "ENROLLMENT_ENROLL";
        public static final String ENROLL_PC = "ENROLL_PC";
        public static final String ENROLL_EMP = "ENROLL_EMP";
        public static final String ENROLL_AMB = "ENROLL_AMB";
        public static final String ENROLL_DR = "ENROLL_DR";
        public static final String ENROLL_MTGPL = "ENROLL_MTGPL";
        public static final String ENROLL_BR = "ENROLL_BR";
        public static final String ENROLL_IHG = "ENROLL_IHG";
        public static final String ENROLL_HTL = "ENROLL_HTL";
        public static final String ENROLL_KAR = "ENROLL_KAR";
    }

    public static class HotelOperations
    {
        public static final String REWARD_NIGHTS_OCCUPANCY_V = "REWARD_NIGHTS_OCCUPANCY_V";
        public static final String REWARD_NIGHTS_OCCUPANCY_M = "REWARD_NIGHTS_OCCUPANCY_M";
        public static final String APPROVE_REWARD_NIGHTS = "APPROVE_REWARD_NIGHTS";
        public static final String OTHER_ACTIVITY_V = "OTHER_ACTIVITY_V";
        public static final String OTHER_CERTIFICATE_A = "OTHER_CERTIFICATE_A";
        public static final String OTHER_CERTIFICATE_M = "OTHER_CERTIFICATE_M";
        public static final String FREE_NIGHT_FLAT_V = "FREE_NIGHT_FLAT_V";
        public static final String CATALOG_V = "CATALOG_V";
        public static final String SHOPPING_CART_V = "SHOPPING_CART_V";
        public static final String SHOPPING_CART_M = "SHOPPING_CART_M";
    }

    public static class Voucher
    {
        public static final String VOUCHER_V = "VOUCHER_V";
        public static final String VOUCHER_VOID_M = "VOUCHER_VOID_M";
        public static final String VOUCHER_UNVOID_M = "VOUCHER_UNVOID_M";
        public static final String DEPOSITS_V = "DEPOSITS_V";
    }

    public static class UnitManagement
    {
        public static final String GOODWILL_UNITS = "GOODWILL_UNITS";
        public static final String PURCHASE_UNITS = "PURCHASE_UNITS";
        public static final String PURCHASE_GIFT = "PURCHASE_GIFT";
        public static final String TRANSFER_UNITS = "TRANSFER_UNITS";
        public static final String PURCHASE_TIER_LEVEL = "PURCHASE_TIER_LEVEL";
        public static final String TRANSFER_Q_NIGHTS = "TRANSFER_Q_NIGHTS";
    }

    public static class Program
    {
        public static final String PROGRAM_MEMBER_POINT_BALANCE_V = "PROGRAM_MEMBER_POINT_BALANCE_V";
        public static final String PC_TAB_V = "PC_TAB_V";
        public static final String PC_SUMMARY_M = "PC_SUMMARY_M";
        public static final String PC_PURCHASE_LEVEL = "PC_PURCHASE_LEVEL";
        public static final String ALLIANCE_A = "ALLIANCE_A";
        public static final String ALLIANCE_M = "ALLIANCE_M";
        public static final String ALLIANCE_D = "ALLIANCE_D";
        public static final String TRAVEL_PROFILE_V = "TRAVEL_PROFILE_V";
        public static final String TRAVEL_PROFILE_M = "TRAVEL_PROFILE_M";
        public static final String IHG_TAB_V = "IHG_TAB_V";
        public static final String IHG_SUMMARY_M = "IHG_SUMMARY_M";
        public static final String AMB_TAB_V = "AMB_TAB_V";
        public static final String ENROLLMENT_RENEW = "ENROLLMENT_RENEW";
        public static final String EXTEND_MEMBERSHIP_AMB = "EXTEND_MEMBERSHIP_AMB";
        public static final String EXTEND_MEMBERSHIP_DR = "EXTEND_MEMBERSHIP_DR";
        public static final String DECLINE_TSCS = "DECLINE_TSCS";
        public static final String TIER_LEVEL_AMB_M = "TIER_LEVEL_AMB_M";
        public static final String TRAVEL_PROFILE_AMB_M = "TRAVEL_PROFILE_AMB_M";
        public static final String MTGPL_TAB_V = "MTGPL_TAB_V";
        public static final String EMP_TAB_V = "EMP_TAB_V";
        public static final String ANNUAL_ACTIVITY_V = "ANNUAL_ACTIVITY_V";
        public static final String HTL_TAB_V = "HTL_TAB_V";
        public static final String DR_TAB_V = "DR_TAB_V";
        public static final String DINING_V = "DINING_V";
        public static final String AMB_SUMMARY_M = "AMB_SUMMARY_M";
        public static final String DR_SUMMARY_M = "DR_SUMMARY_M";
        public static final String BR_SUMMARY_M = "BR_SUMMARY_M";
        public static final String EMP_SUMMARY_M = "EMP_SUMMARY_M";
        public static final String HTL_SUMMARY_M = "HTL_SUMMARY_M";
        public static final String RC_SUMMARY_M = "RC_SUMMARY_M";
        public static final String MTGPL_SUMMARY_M = "MTGPL_SUMMARY_M";
        public static final String EARNING_PREFS_M = "EARNING_PREFS_M";
        public static final String EMPLOYEE_RATE_ELIGIBILITY_M = "EMPLOYEE_RATE_ELIGIBILITY_M";
        public static final String TL_BENEFIT_M = "TL_BENEFIT_M";
        public static final String ALTERNATE_ID_M = "ALTERNATE_ID_M";
        public static final String ALTERNATE_ID_D = "ALTERNATE_ID_D";
        public static final String TIER_LEVEL_ACTV_AMB_V = "TIER_LEVEL_ACTV_AMB_V";

        public static final String KAR_TAB_V = "KAR_TAB_V";
        public static final String KAR_SUMMARY_M = "KAR_SUMMARY_M";
    }

    public static class Guest
    {
        public static final String GUEST_PERSONAL_V = "GUEST_PERSONAL_V";
        public static final String GUEST_PERSONAL_M = "GUEST_PERSONAL_M";
        public static final String GUEST_CONTACT_PHONE_ADDRESS_M = "GUEST_CONTACT_PHONE_ADDRESS_M";
        public static final String GUEST_SMS_V = "GUEST_SMS_V";
        public static final String GUEST_SMS_M = "GUEST_SMS_M";
        public static final String CONTACT_EMAIL_M = "CONTACT_EMAIL_M";
        public static final String PASSPORT_V = "PASSPORT_V";
        public static final String PASSPORT_M = "PASSPORT_M";

        public static final String GUEST_CONTACT_PREF_V = "GUEST_CONTACT_PREF_V";
        public static final String GUEST_LANG_M = "GUEST_LANG_M";
        public static final String GUEST_MARKET_PREF_M = "GUEST_MARKET_PREF_M";
        public static final String GUEST_SERVICE_PREF_V = "GUEST_SERVICE_PREF_V";

        public static final String GUEST_SERVICE_PREF_M = "GUEST_SERVICE_PREF_M";

        public static final String GUEST_SYNTH_V = "GUEST_SYNTH_V";

        public static final String GUEST_NEW_ACCOUNT_V = "GUEST_NEW_ACCOUNT_V";
        public static final String GUEST_STATUS_V = "GUEST_STATUS_V";
        public static final String ACCOUNT_REMOVE_M = "ACCOUNT_REMOVE_M";
        public static final String ACCOUNT_REINSTATE_M = "ACCOUNT_REINSTATE_M";
        public static final String SECURITY_V = "SECURITY_V";
        public static final String SECURITY_M = "SECURITY_M";
        public static final String PIN_V = "PIN_V";
        public static final String PIN_M = "PIN_M";
        public static final String GUEST_FLAG_V = "GUEST_FLAG_V";
        public static final String GUEST_FLAG_M = "GUEST_FLAG_M";
        public static final String GUEST_ACCOUNT_HISTORY_V = "GUEST_ACCOUNT_HISTORY_V";

        public static final String GUEST_SOC_V = "GUEST_SOC_V";
        public static final String GUEST_SOC_M = "GUEST_SOC_M";
        public static final String GUEST_SOC_D = "GUEST_SOC_D";
    }

    public static class Offer
    {
        public static final String OFFER_V = "OFFER_V";
        public static final String GUEST_OFFER_V = "GUEST_OFFER_V";
        public static final String OFFER_REGISTER_M = "OFFER_REGISTER_M";
        public static final String OFFER_UPDATE_M = "OFFER_UPDATE_M";
        public static final String OFFER_FORCE_REGISTER_M = "OFFER_FORCE_REGISTER_M";
        public static final String OFFER_RETRO_REGISTER_M = "OFFER_RETRO_REGISTER_M";
        public static final String OFFER_UPDATE_EVENTS_M = "OFFER_UPDATE_EVENTS_M";
        public static final String OFFER_UPDATE_ADJUST_M = "OFFER_UPDATE_ADJUST_M";
        public static final String OFFER_UPDATE_WIN_M = "OFFER_UPDATE_WIN_M";
        public static final String OFFER_UPDATE_ADJUST_DATE_M = "OFFER_UPDATE_ADJUST_DATE_M";
        public static final String OFFER_HISTORY_V = "OFFER_HISTORY_V";
    }

    public static class Communication
    {
        public static final String COMM_V = "COMM_V";
        public static final String COMM_RESEND_M = "COMM_RESEND_M";
        public static final String COMM_SEND_A = "COMM_SEND_A";
    }

    public static class Order
    {
        public static final String ORDER_V = "ORDER_V";
        public static final String ORDER_M = "ORDER_M";
        public static final String RESEND = "RESEND";
        public static final String ORDER_HISTORY_V = "ORDER_HISTORY_V";
    }

    public static class Stay
    {
        public static final String STAY_V = "STAY_V";
        public static final String STAY_ADJUST_M = "STAY_ADJUST_M";
        public static final String STAY_ASSIGN_M = "STAY_ASSIGN_M";
        public static final String STAY_A = "STAY_A";
        public static final String STAY_D = "STAY_D";
        public static final String STAY_HISTORY_V = "STAY_HISTORY_V";
    }

    public static class DataSteward
    {
        public static final String LOYALTY_RULES_V = "LOYALTY_RULES_V";
        public static final String LOYALTY_RULES_M = "LOYALTY_RULES_M";
    }

    public static class Events
    {
        public static final String ALL_EVENTS_TAB_V = "ALL_EVENTS_TAB_V";
    }

    public static class RewardNight
    {
        public static final String REWARD_NIGHT_V = "REWARD_NIGHT_V";
        public static final String REWARD_NIGHT_A = "REWARD_NIGHT_A";
        public static final String REWARD_NIGHT_M = "REWARD_NIGHT_M";
    }

    public static class FreeNight
    {
        public static final String FREE_NIGHT_V = "FREE_NIGHT_V";
        public static final String FREE_NIGHT_A = "FREE_NIGHT_A";
        public static final String FREE_NIGHT_M = "FREE_NIGHT_M";
        public static final String FREE_NIGHT_GOODWILL_A = "FREE_NIGHT_GOODWILL_A";
        public static final String FREE_NIGHT_SERVICE_A = "FREE_NIGHT_SERVICE_A";
        public static final String FREE_NIGHT_VOUCHER_CANCEL_M = "FREE_NIGHT_VOUCHER_CANCEL_M";
    }

    public static class CoPartner
    {
        public static final String COPARTNER_V = "COPARTNER_V";
        public static final String COPARTNER_A = "COPARTNER_A";
        public static final String COPARTNER_M = "COPARTNER_M";
        public static final String MEETING_M = "MEETING_M";
    }

    public static class Business
    {
        public static final String BUSINESS_V = "BUSINESS_V";
        public static final String BUSINESS_A = "BUSINESS_A";
    }

    public static class GuestProfile
    {
        public static final String GUEST_V = "GUEST_V";
        public static final String GUEST_M = "GUEST_M";
        public static final String GUEST_D = "GUEST_D";
        public static final String GUEST_OPEN_V = "GUEST_OPEN_V";
        public static final String GUEST_CLOSED_FRAUD_V = "GUEST_CLOSED_FRAUD_V";
        public static final String GUEST_CLOSED_IHG_V = "GUEST_CLOSED_IHG_V";
        public static final String GUEST_CLOSED_CUSTOMER_V = "GUEST_CLOSED_CUSTOMER_V";
        public static final String GUEST_ACCOUNT_V = "GUEST_ACCOUNT_V";
        public static final String GUEST_STATUS_M = "GUEST_STATUS_M";
        public static final String GUEST_STATUS_REASON_V = "GUEST_STATUS_REASON_V";
        public static final String GUEST_STATUS_REASON_M = "GUEST_STATUS_REASON_M";
    }

    public static class Screenpop
    {
        public static final String ROLE_SCREENPOP_V = "ROLE_SCREENPOP_V";
    }

    public static class TravelProfile
    {
        public static final String TRAVEL_PROFILE_V = "TRAVEL_PROFILE_V";
        public static final String TRAVEL_PROFILE_M = "TRAVEL_PROFILE_M";
    }

    public class StayPreferences
    {
        public static final String STAY_PREFERENCES_V = "STAY_PREFERENCES_V";
        public static final String STAY_PREFERENCES_M = "STAY_PREFERENCES_M";
    }
}
