package com.ihg.crm.automation.selenium.tests.servicecenter.security.events;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.enabled;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasText;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Events.ALL_EVENTS_TAB_V;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Offer.GUEST_OFFER_V;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Offer.OFFER_FORCE_REGISTER_M;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Offer.OFFER_HISTORY_V;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Offer.OFFER_REGISTER_M;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Offer.OFFER_UPDATE_ADJUST_M;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Offer.OFFER_V;

import java.util.Map;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.offer.OfferFactory;
import com.ihg.automation.selenium.servicecenter.pages.Tabs;
import com.ihg.automation.selenium.servicecenter.pages.event.offer.MemberOfferGridRow.MemberOfferCell;
import com.ihg.automation.selenium.servicecenter.pages.event.offer.OfferEventPage;
import com.ihg.automation.selenium.servicecenter.pages.event.offer.OfferPopUp;
import com.ihg.automation.selenium.servicecenter.pages.event.offer.OfferSearch;
import com.ihg.automation.selenium.servicecenter.pages.event.offer.OfferSearchButtonBar;
import com.ihg.automation.selenium.servicecenter.pages.event.offer.UniverseOfferGridRow.UniverseOfferCell;
import com.ihg.crm.automation.selenium.tests.servicecenter.security.LoginLogout;

public class OfferAndAllEventFactoryTest extends LoginLogout
{
    private static final String OFFER_TRACKING_NIGHTS = "8050504";
    private static final String OFFER_NOT_TRACKING_NIGHTS = OfferFactory.FREE_NIGHT_OFFER_CODE;
    private OfferEventPage offerPage = new OfferEventPage();
    private Tabs.EventsTab eventsTab = new Tabs().new EventsTab();

    public OfferAndAllEventFactoryTest(String role, Map<String, Boolean> permissions)
    {
        super(role, permissions);
    }

    @BeforeClass
    private void beforeClass()
    {
        loginAndOpenMember(programsMemberIdWithOfferAndOrder);
    }

    @Test
    public void verifyAllEventsTab()
    {
        eventsTab.goTo();
        verifyThat(eventsTab.getAll(), displayed(permissions.get(ALL_EVENTS_TAB_V)));
    }

    @Test
    public void verifyOffersTab()
    {
        eventsTab.goTo();
        verifyThat(eventsTab.getOffer(), displayed(permissions.get(OFFER_V)));
    }

    @Test(dependsOnMethods = "verifyOffersTab")
    public void verifyViewGuestOffers()
    {
        if (permissions.get(OFFER_V))
        {
            offerPage.goTo();
            verifyThat(offerPage.getMemberOffersPanel(), displayed(permissions.get(GUEST_OFFER_V)));
            offerPage.getMemberOffersGrid().getRow(1).getCell(MemberOfferCell.OFFER_NAME).clickAndWait();
            OfferPopUp offerPopUp = new OfferPopUp();
            verifyThat(offerPopUp.getOfferDetailsTab().getTab(), displayed(permissions.get(GUEST_OFFER_V)));
            offerPopUp.close();
        }
    }

    @Test(dependsOnMethods = "verifyOffersTab")
    public void verifyOfferRegistration()
    {
        if (permissions.get(OFFER_REGISTER_M))
        {
            offerPage.goTo();
            verifyThat(offerPage.getUniverseOffersGrid().getRow(1).getCell(UniverseOfferCell.ELIGIBLE),
                    hasText("Register"));
        }
    }

    @Test(dependsOnMethods = "verifyOffersTab", enabled = false)
    public void verifyOfferForceRegistration()
    {
        if (permissions.get(OFFER_V))
        {
            offerPage.goTo();
            OfferSearch offerSearch = offerPage.getSearchFields();
            OfferSearchButtonBar offerSearchButtonBar = offerPage.getButtonBar();
            offerSearch.getOfferCode().type("offerCodeForForceRegistration");
            offerSearch.getEligible().uncheck();
            offerSearchButtonBar.clickSearch();

            OfferPopUp offerPopUp = new OfferPopUp();
            verifyThat(offerPopUp, displayed(true));

            verifyThat(offerPopUp.getForceRegister(), enabled(permissions.get(OFFER_FORCE_REGISTER_M)));

            offerPopUp.close();
            offerSearchButtonBar.clickClearCriteria();
        }
    }

    @Test(dependsOnMethods = "verifyOffersTab")
    public void verifyAdjustOffer()
    {
        if (permissions.get(OFFER_V))
        {
            offerPage.goTo();
            offerPage.getMemberOffersGrid().getRowByOfferCode(OFFER_TRACKING_NIGHTS).getCell(MemberOfferCell.OFFER_NAME)
                    .clickAndWait();
            OfferPopUp offerPopUp = new OfferPopUp();
            verifyThat(offerPopUp.getAdjustOffer(), enabled(permissions.get(OFFER_UPDATE_ADJUST_M)));
            offerPopUp.close();

            offerPage.getMemberOffersGrid().getRowByOfferCode(OFFER_NOT_TRACKING_NIGHTS)
                    .getCell(MemberOfferCell.OFFER_NAME).clickAndWait();
            offerPopUp = new OfferPopUp();
            verifyThat(offerPopUp.getAdjustOffer(), enabled(false));
            offerPopUp.close();
        }
    }

    @Test(dependsOnMethods = "verifyOffersTab")
    public void verifyGuestOfferHistory()
    {
        if (permissions.get(OFFER_V))
        {
            offerPage.goTo();
            offerPage.getMemberOffersGrid().getRow(1).getCell(MemberOfferCell.OFFER_NAME).clickAndWait();
            OfferPopUp offerPopUp = new OfferPopUp();
            verifyThat(offerPopUp.getHistoryTab().getTab(), displayed(permissions.get(OFFER_HISTORY_V)));
            offerPopUp.close();
        }
    }
}
