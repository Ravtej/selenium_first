package com.ihg.crm.automation.selenium.tests.servicecenter.security.events;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Business.BUSINESS_A;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Business.BUSINESS_V;

import java.util.Map;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.servicecenter.pages.Tabs;
import com.ihg.automation.selenium.servicecenter.pages.event.business.BusinessEventsPage;
import com.ihg.crm.automation.selenium.tests.servicecenter.security.LoginLogout;

public class BusinessAvailabilityFactoryTest extends LoginLogout
{
    private Tabs.EventsTab eventsTab = new Tabs().new EventsTab();
    private BusinessEventsPage businessPage = new BusinessEventsPage();

    public BusinessAvailabilityFactoryTest(String role, Map<String, Boolean> permissions)
    {
        super(role, permissions);
    }

    @BeforeClass
    private void beforeClass()
    {
        loginAndOpenMember(businessMemberId);
    }

    @Test
    public void verifyBusinessTab()
    {
        eventsTab.goTo();
        verifyThat(eventsTab.getBusiness(), displayed(permissions.get(BUSINESS_V)));

        if (permissions.get(BUSINESS_V))
        {
            businessPage.goTo();
            verifyThat(businessPage.getButtonBar().getCreateEvent(), displayed(permissions.get(BUSINESS_A)));
        }
    }
}
