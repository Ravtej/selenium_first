package com.ihg.crm.automation.selenium.tests.servicecenter.security.rewardnight;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.isDisplayedAndEnabled;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.RewardNight.REWARD_NIGHT_A;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.RewardNight.REWARD_NIGHT_M;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.RewardNight.REWARD_NIGHT_V;

import java.util.Map;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.servicecenter.pages.Tabs;
import com.ihg.automation.selenium.servicecenter.pages.event.rewardnight.RewardNightDetails;
import com.ihg.automation.selenium.servicecenter.pages.event.rewardnight.RewardNightDetailsPopUp;
import com.ihg.automation.selenium.servicecenter.pages.event.rewardnight.RewardNightDetailsTab;
import com.ihg.automation.selenium.servicecenter.pages.event.rewardnight.RewardNightEventPage;
import com.ihg.automation.selenium.servicecenter.pages.event.rewardnight.RewardNightGridRowContainer;
import com.ihg.crm.automation.selenium.tests.servicecenter.security.LoginLogout;

public class RewardNightAvailabilityFactoryTest extends LoginLogout
{
    private RewardNightEventPage rewardNightPage = new RewardNightEventPage();
    private Tabs.EventsTab eventsTab = new Tabs().new EventsTab();
    private RewardNightDetailsPopUp rewardNightPopUp;

    public RewardNightAvailabilityFactoryTest(String role, Map<String, Boolean> permissions)
    {
        super(role, permissions);
    }

    @BeforeClass
    public void beforeClass()
    {
        loginAndOpenMember(memberIdWithRewardnight);
    }

    @Test
    public void verifyRewardNightTab()
    {
        eventsTab.goTo();
        verifyThat(eventsTab.getRewardNight(), displayed(permissions.get(REWARD_NIGHT_V)));
    }

    @Test(dependsOnMethods = "verifyRewardNightTab")
    public void verifyCreateRewardNight()
    {
        if (permissions.get(REWARD_NIGHT_V))
        {
            rewardNightPage.goTo();
            verifyThat(rewardNightPage.getCreateNewRewardNight(), displayed(permissions.get(REWARD_NIGHT_A)));
        }
    }

    @Test(dependsOnMethods = "verifyRewardNightTab")
    public void verifyModifyRewardNight()
    {
        if (permissions.get(REWARD_NIGHT_V))
        {
            rewardNightPage.goTo();
            RewardNightGridRowContainer rowContainer = rewardNightPage.getGrid().getRow(1)
                    .expand(RewardNightGridRowContainer.class);
            rowContainer.clickDetails();
            rewardNightPopUp = new RewardNightDetailsPopUp();
            RewardNightDetailsTab tab = rewardNightPopUp.getRewardNightDetailsTab();

            RewardNightDetails rewardNightDetails = tab.getDetails();
            verifyThat(rewardNightPopUp.getSaveChanges(), isDisplayedAndEnabled(permissions.get(REWARD_NIGHT_M)));
            verifyThat(rewardNightDetails.getCheckInDate(), displayed(permissions.get(REWARD_NIGHT_M)));
            verifyThat(rewardNightDetails.getCheckOutDate(), displayed(permissions.get(REWARD_NIGHT_M)));
            verifyThat(rewardNightDetails.getNumberOfRooms(), displayed(permissions.get(REWARD_NIGHT_M)));
            verifyThat(rewardNightDetails.getNights(), displayed(permissions.get(REWARD_NIGHT_M)));
        }
    }

}
