package com.ihg.crm.automation.selenium.tests.servicecenter.security.profile;

import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Guest.GUEST_NEW_ACCOUNT_V;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Guest.GUEST_STATUS_V;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.GuestProfile.GUEST_CLOSED_CUSTOMER_V;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.GuestProfile.GUEST_CLOSED_FRAUD_V;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.GuestProfile.GUEST_CLOSED_IHG_V;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.GuestProfile.GUEST_D;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.GuestProfile.GUEST_M;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.GuestProfile.GUEST_OPEN_V;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.GuestProfile.GUEST_STATUS_M;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.GuestProfile.GUEST_STATUS_REASON_M;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.GuestProfile.GUEST_STATUS_REASON_V;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.GuestProfile.GUEST_V;

import java.util.HashMap;
import java.util.Map;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Factory;

import com.ihg.crm.automation.selenium.tests.servicecenter.security.SecurityMatrix;

public class ProfileManagementFactory
{
    @DataProvider(name = "verifyProfileManagementProvider")
    public Object[][] verifyProfileManagementProvider()
    {
        Map<String, Boolean> defaultPermissions = setPermissionMap();
        SecurityMatrix permissions = new SecurityMatrix(defaultPermissions);

        permissions.getAgentPermissions().put(GUEST_D, false);
        permissions.getAgentPermissions().put(GUEST_STATUS_REASON_M, false);

        permissions.getProgramSales().put(GUEST_D, false);
        permissions.getProgramSales().put(GUEST_STATUS_REASON_M, false);

        permissions.getAgentTierLevel().put(GUEST_D, false);

        permissions.getHotelHelpDesk().put(GUEST_D, false);
        permissions.getHotelHelpDesk().put(GUEST_STATUS_REASON_M, false);

        permissions.getAdminAwardSupport().put(GUEST_D, false);
        permissions.getAdminAwardSupport().put(GUEST_STATUS_REASON_M, false);

        permissions.getAdminOperationsManager().put(GUEST_D, false);

        permissions.getAdminCentralProcessing().put(GUEST_D, false);

        permissions.getFraudTeam().put(GUEST_D, false);

        permissions.getAgentReadOnly().put(GUEST_M, false);
        permissions.getAgentReadOnly().put(GUEST_D, false);
        permissions.getAgentReadOnly().put(GUEST_CLOSED_FRAUD_V, false);
        permissions.getAgentReadOnly().put(GUEST_CLOSED_IHG_V, false);
        permissions.getAgentReadOnly().put(GUEST_CLOSED_CUSTOMER_V, false);
        permissions.getAgentReadOnly().put(GUEST_NEW_ACCOUNT_V, false);
        permissions.getAgentReadOnly().put(GUEST_STATUS_V, false);
        permissions.getAgentReadOnly().put(GUEST_STATUS_M, false);
        permissions.getAgentReadOnly().put(GUEST_STATUS_REASON_V, false);
        permissions.getAgentReadOnly().put(GUEST_STATUS_REASON_M, false);

        permissions.getManagerMarketing().put(GUEST_D, false);
        permissions.getManagerMarketing().put(GUEST_STATUS_M, false);
        permissions.getManagerMarketing().put(GUEST_STATUS_REASON_M, false);

        permissions.getReadOnlyMarketing().put(GUEST_D, false);
        permissions.getReadOnlyMarketing().put(GUEST_STATUS_M, false);
        permissions.getReadOnlyMarketing().put(GUEST_STATUS_REASON_M, false);

        permissions.getReadOnlyVendor().put(GUEST_D, false);
        permissions.getReadOnlyVendor().put(GUEST_STATUS_M, false);
        permissions.getReadOnlyVendor().put(GUEST_STATUS_REASON_M, false);

        permissions.getPartnerVendor().put(GUEST_D, false);
        permissions.getPartnerVendor().put(GUEST_STATUS_M, false);
        permissions.getPartnerVendor().put(GUEST_STATUS_REASON_M, false);

        permissions.getHumanResources().put(GUEST_D, false);
        permissions.getHumanResources().put(GUEST_STATUS_M, false);
        permissions.getHumanResources().put(GUEST_STATUS_REASON_M, false);

        return permissions.getPermissionArray();
    }

    private Map<String, Boolean> setPermissionMap()
    {
        Map<String, Boolean> permissions = new HashMap<String, Boolean>();

        permissions.put(GUEST_V, true);
        permissions.put(GUEST_M, true);
        permissions.put(GUEST_D, true);
        permissions.put(GUEST_OPEN_V, true);
        permissions.put(GUEST_CLOSED_FRAUD_V, true);
        permissions.put(GUEST_CLOSED_IHG_V, true);
        permissions.put(GUEST_CLOSED_CUSTOMER_V, true);
        permissions.put(GUEST_NEW_ACCOUNT_V, true);
        permissions.put(GUEST_STATUS_V, true);
        permissions.put(GUEST_STATUS_M, true);
        permissions.put(GUEST_STATUS_REASON_V, true);
        permissions.put(GUEST_STATUS_REASON_M, true);

        return permissions;
    }

    @Factory(dataProvider = "verifyProfileManagementProvider")
    public Object[] createInstances(String role, Map<String, Boolean> permissions)
    {
        return new Object[] { new ProfileManagementFactoryTest(role, permissions) };
    }
}
