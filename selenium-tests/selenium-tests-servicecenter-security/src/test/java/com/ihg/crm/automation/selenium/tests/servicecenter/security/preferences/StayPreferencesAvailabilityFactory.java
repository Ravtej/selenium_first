package com.ihg.crm.automation.selenium.tests.servicecenter.security.preferences;

import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.StayPreferences.STAY_PREFERENCES_M;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.StayPreferences.STAY_PREFERENCES_V;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.TravelProfile.TRAVEL_PROFILE_M;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.TravelProfile.TRAVEL_PROFILE_V;

import java.util.HashMap;
import java.util.Map;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Factory;

import com.ihg.crm.automation.selenium.tests.servicecenter.security.SecurityMatrix;

public class StayPreferencesAvailabilityFactory
{
    @DataProvider(name = "travelProfileAvailabilityProvider")
    public Object[][] travelProfileAvailabilityProvider()
    {
        Map<String, Boolean> defaultPermissions = setPermissionMap();
        SecurityMatrix permissions = new SecurityMatrix(defaultPermissions);

        permissions.getProgramSales().put(TRAVEL_PROFILE_M, false);

        permissions.getAgentReadOnly().put(TRAVEL_PROFILE_M, false);
        permissions.getAgentReadOnly().put(STAY_PREFERENCES_M, false);

        permissions.getManagerMarketing().put(TRAVEL_PROFILE_M, false);
        permissions.getManagerMarketing().put(STAY_PREFERENCES_M, false);

        permissions.getReadOnlyMarketing().put(TRAVEL_PROFILE_M, false);
        permissions.getReadOnlyMarketing().put(STAY_PREFERENCES_M, false);

        permissions.getHumanResources().put(TRAVEL_PROFILE_M, false);
        permissions.getHumanResources().put(STAY_PREFERENCES_M, false);

        permissions.getReadOnlyVendor().put(TRAVEL_PROFILE_M, false);
        permissions.getReadOnlyVendor().put(STAY_PREFERENCES_M, false);

        permissions.getPartnerVendor().put(TRAVEL_PROFILE_M, false);
        permissions.getPartnerVendor().put(STAY_PREFERENCES_M, false);

        return permissions.getPermissionArray();
    }

    private Map<String, Boolean> setPermissionMap()
    {
        Map<String, Boolean> permissions = new HashMap<String, Boolean>();

        permissions.put(TRAVEL_PROFILE_V, true);
        permissions.put(TRAVEL_PROFILE_M, true);
        permissions.put(STAY_PREFERENCES_V, true);
        permissions.put(STAY_PREFERENCES_M, true);
        return permissions;
    }

    @Factory(dataProvider = "travelProfileAvailabilityProvider")
    public Object[] createInstances(String role, Map<String, Boolean> permissions)
    {
        return new Object[] { new StayPreferencesAvailabilityFactoryTest(role, permissions) };
    }

}
