package com.ihg.crm.automation.selenium.tests.servicecenter.security.events;

import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Events.ALL_EVENTS_TAB_V;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Offer.GUEST_OFFER_V;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Offer.OFFER_FORCE_REGISTER_M;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Offer.OFFER_HISTORY_V;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Offer.OFFER_REGISTER_M;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Offer.OFFER_RETRO_REGISTER_M;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Offer.OFFER_UPDATE_ADJUST_DATE_M;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Offer.OFFER_UPDATE_ADJUST_M;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Offer.OFFER_UPDATE_EVENTS_M;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Offer.OFFER_UPDATE_M;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Offer.OFFER_UPDATE_WIN_M;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Offer.OFFER_V;

import java.util.HashMap;
import java.util.Map;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Factory;

import com.ihg.crm.automation.selenium.tests.servicecenter.security.SecurityMatrix;

public class OfferAndAllEventFactory
{
    @DataProvider(name = "verifyOfferAvailabilityProvider")
    public Object[][] verifyOfferAvailabilityProvider()
    {
        Map<String, Boolean> defaultPermissions = setPermissionMap();

        SecurityMatrix permissions = new SecurityMatrix(defaultPermissions);

        permissions.getProgramSales().put(OFFER_UPDATE_M, false);
        permissions.getProgramSales().put(OFFER_FORCE_REGISTER_M, false);
        permissions.getProgramSales().put(OFFER_RETRO_REGISTER_M, false);
        permissions.getProgramSales().put(OFFER_UPDATE_EVENTS_M, false);
        permissions.getProgramSales().put(OFFER_UPDATE_ADJUST_M, false);
        permissions.getProgramSales().put(OFFER_UPDATE_WIN_M, false);
        permissions.getProgramSales().put(OFFER_UPDATE_ADJUST_DATE_M, false);

        permissions.getAgentReadOnly().put(OFFER_V, false);
        permissions.getAgentReadOnly().put(GUEST_OFFER_V, false);
        permissions.getAgentReadOnly().put(OFFER_REGISTER_M, false);
        permissions.getAgentReadOnly().put(OFFER_UPDATE_M, false);
        permissions.getAgentReadOnly().put(OFFER_FORCE_REGISTER_M, false);
        permissions.getAgentReadOnly().put(OFFER_RETRO_REGISTER_M, false);
        permissions.getAgentReadOnly().put(OFFER_UPDATE_EVENTS_M, false);
        permissions.getAgentReadOnly().put(OFFER_UPDATE_ADJUST_M, false);
        permissions.getAgentReadOnly().put(OFFER_UPDATE_WIN_M, false);
        permissions.getAgentReadOnly().put(OFFER_UPDATE_ADJUST_DATE_M, false);
        permissions.getAgentReadOnly().put(OFFER_HISTORY_V, false);

        permissions.getManagerMarketing().put(OFFER_UPDATE_M, false);
        permissions.getManagerMarketing().put(OFFER_FORCE_REGISTER_M, false);
        permissions.getManagerMarketing().put(OFFER_RETRO_REGISTER_M, false);
        permissions.getManagerMarketing().put(OFFER_UPDATE_EVENTS_M, false);
        permissions.getManagerMarketing().put(OFFER_UPDATE_ADJUST_M, false);
        permissions.getManagerMarketing().put(OFFER_UPDATE_WIN_M, false);
        permissions.getManagerMarketing().put(OFFER_UPDATE_ADJUST_DATE_M, false);

        permissions.getReadOnlyMarketing().put(OFFER_REGISTER_M, false);
        permissions.getReadOnlyMarketing().put(OFFER_UPDATE_M, false);
        permissions.getReadOnlyMarketing().put(OFFER_FORCE_REGISTER_M, false);
        permissions.getReadOnlyMarketing().put(OFFER_RETRO_REGISTER_M, false);
        permissions.getReadOnlyMarketing().put(OFFER_UPDATE_EVENTS_M, false);
        permissions.getReadOnlyMarketing().put(OFFER_UPDATE_ADJUST_M, false);
        permissions.getReadOnlyMarketing().put(OFFER_UPDATE_WIN_M, false);
        permissions.getReadOnlyMarketing().put(OFFER_UPDATE_ADJUST_DATE_M, false);

        permissions.getReadOnlyVendor().put(OFFER_REGISTER_M, false);
        permissions.getReadOnlyVendor().put(OFFER_UPDATE_M, false);
        permissions.getReadOnlyVendor().put(OFFER_FORCE_REGISTER_M, false);
        permissions.getReadOnlyVendor().put(OFFER_RETRO_REGISTER_M, false);
        permissions.getReadOnlyVendor().put(OFFER_UPDATE_EVENTS_M, false);
        permissions.getReadOnlyVendor().put(OFFER_UPDATE_ADJUST_M, false);
        permissions.getReadOnlyVendor().put(OFFER_UPDATE_WIN_M, false);
        permissions.getReadOnlyVendor().put(OFFER_UPDATE_ADJUST_DATE_M, false);

        permissions.getPartnerVendor().put(OFFER_UPDATE_M, false);
        permissions.getPartnerVendor().put(OFFER_FORCE_REGISTER_M, false);
        permissions.getPartnerVendor().put(OFFER_RETRO_REGISTER_M, false);
        permissions.getPartnerVendor().put(OFFER_UPDATE_EVENTS_M, false);
        permissions.getPartnerVendor().put(OFFER_UPDATE_ADJUST_M, false);
        permissions.getPartnerVendor().put(OFFER_UPDATE_WIN_M, false);
        permissions.getPartnerVendor().put(OFFER_UPDATE_ADJUST_DATE_M, false);

        permissions.getHumanResources().put(ALL_EVENTS_TAB_V, false);
        permissions.getHumanResources().put(OFFER_REGISTER_M, false);
        permissions.getHumanResources().put(OFFER_UPDATE_M, false);
        permissions.getHumanResources().put(OFFER_FORCE_REGISTER_M, false);
        permissions.getHumanResources().put(OFFER_RETRO_REGISTER_M, false);
        permissions.getHumanResources().put(OFFER_UPDATE_EVENTS_M, false);
        permissions.getHumanResources().put(OFFER_UPDATE_ADJUST_M, false);
        permissions.getHumanResources().put(OFFER_UPDATE_WIN_M, false);
        permissions.getHumanResources().put(OFFER_UPDATE_ADJUST_DATE_M, false);

        return permissions.getPermissionArray();
    }

    private Map<String, Boolean> setPermissionMap()
    {
        Map<String, Boolean> permissions = new HashMap<String, Boolean>();

        permissions.put(ALL_EVENTS_TAB_V, true);
        permissions.put(OFFER_V, true);
        permissions.put(GUEST_OFFER_V, true);
        permissions.put(OFFER_REGISTER_M, true);
        permissions.put(OFFER_UPDATE_M, true);
        permissions.put(OFFER_FORCE_REGISTER_M, true);
        permissions.put(OFFER_RETRO_REGISTER_M, true);
        permissions.put(OFFER_UPDATE_EVENTS_M, true);
        permissions.put(OFFER_UPDATE_ADJUST_M, true);
        permissions.put(OFFER_UPDATE_WIN_M, true);
        permissions.put(OFFER_UPDATE_ADJUST_DATE_M, true);
        permissions.put(OFFER_HISTORY_V, true);

        return permissions;
    }

    @Factory(dataProvider = "verifyOfferAvailabilityProvider")
    public Object[] createInstances(String role, Map<String, Boolean> permissions)
    {
        return new Object[] { new OfferAndAllEventFactoryTest(role, permissions) };
    }
}
