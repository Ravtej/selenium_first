package com.ihg.crm.automation.selenium.tests.servicecenter.security.unit;

import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.UnitManagement.GOODWILL_UNITS;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.UnitManagement.PURCHASE_GIFT;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.UnitManagement.PURCHASE_TIER_LEVEL;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.UnitManagement.PURCHASE_UNITS;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.UnitManagement.TRANSFER_Q_NIGHTS;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.UnitManagement.TRANSFER_UNITS;

import java.util.HashMap;
import java.util.Map;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Factory;

import com.ihg.crm.automation.selenium.tests.servicecenter.security.SecurityMatrix;

public class UnitOperationsAvailabilityFactory
{
    @DataProvider(name = "verifyUnitAvailabilityProvider")
    public Object[][] verifyUnitAvailabilityProvider()
    {
        Map<String, Boolean> defaultPermissions = setPermissionMap();

        SecurityMatrix permissions = new SecurityMatrix(defaultPermissions);

        permissions.getAgentPermissions().put(PURCHASE_UNITS, false);
        permissions.getAgentPermissions().put(PURCHASE_GIFT, false);
        permissions.getAgentPermissions().put(PURCHASE_TIER_LEVEL, false);

        permissions.getProgramSales().put(GOODWILL_UNITS, false);
        permissions.getProgramSales().put(PURCHASE_UNITS, false);
        permissions.getProgramSales().put(PURCHASE_GIFT, false);
        permissions.getProgramSales().put(TRANSFER_UNITS, false);
        permissions.getProgramSales().put(PURCHASE_TIER_LEVEL, false);
        permissions.getProgramSales().put(TRANSFER_Q_NIGHTS, false);

        permissions.getHotelHelpDesk().put(PURCHASE_UNITS, false);
        permissions.getHotelHelpDesk().put(PURCHASE_GIFT, false);
        permissions.getHotelHelpDesk().put(PURCHASE_TIER_LEVEL, false);

        permissions.getAdminAwardSupport().put(PURCHASE_UNITS, false);
        permissions.getAdminAwardSupport().put(PURCHASE_GIFT, false);
        permissions.getAdminAwardSupport().put(PURCHASE_TIER_LEVEL, false);

        permissions.getAgentReadOnly().put(GOODWILL_UNITS, false);
        permissions.getAgentReadOnly().put(PURCHASE_UNITS, false);
        permissions.getAgentReadOnly().put(PURCHASE_GIFT, false);
        permissions.getAgentReadOnly().put(TRANSFER_UNITS, false);
        permissions.getAgentReadOnly().put(PURCHASE_TIER_LEVEL, false);
        permissions.getAgentReadOnly().put(TRANSFER_Q_NIGHTS, false);

        permissions.getManagerMarketing().put(GOODWILL_UNITS, false);
        permissions.getManagerMarketing().put(PURCHASE_UNITS, false);
        permissions.getManagerMarketing().put(PURCHASE_GIFT, false);
        permissions.getManagerMarketing().put(TRANSFER_UNITS, false);
        permissions.getManagerMarketing().put(PURCHASE_TIER_LEVEL, false);
        permissions.getManagerMarketing().put(TRANSFER_Q_NIGHTS, false);

        permissions.getReadOnlyMarketing().put(GOODWILL_UNITS, false);
        permissions.getReadOnlyMarketing().put(PURCHASE_UNITS, false);
        permissions.getReadOnlyMarketing().put(PURCHASE_GIFT, false);
        permissions.getReadOnlyMarketing().put(TRANSFER_UNITS, false);
        permissions.getReadOnlyMarketing().put(PURCHASE_TIER_LEVEL, false);
        permissions.getReadOnlyMarketing().put(TRANSFER_Q_NIGHTS, false);

        permissions.getReadOnlyVendor().put(GOODWILL_UNITS, false);
        permissions.getReadOnlyVendor().put(PURCHASE_UNITS, false);
        permissions.getReadOnlyVendor().put(PURCHASE_GIFT, false);
        permissions.getReadOnlyVendor().put(TRANSFER_UNITS, false);
        permissions.getReadOnlyVendor().put(PURCHASE_TIER_LEVEL, false);
        permissions.getReadOnlyVendor().put(TRANSFER_Q_NIGHTS, false);

        permissions.getPartnerVendor().put(GOODWILL_UNITS, false);
        permissions.getPartnerVendor().put(PURCHASE_UNITS, false);
        permissions.getPartnerVendor().put(PURCHASE_GIFT, false);
        permissions.getPartnerVendor().put(TRANSFER_UNITS, false);
        permissions.getPartnerVendor().put(PURCHASE_TIER_LEVEL, false);
        permissions.getPartnerVendor().put(TRANSFER_Q_NIGHTS, false);

        permissions.getHumanResources().put(GOODWILL_UNITS, false);
        permissions.getHumanResources().put(PURCHASE_UNITS, false);
        permissions.getHumanResources().put(PURCHASE_GIFT, false);
        permissions.getHumanResources().put(TRANSFER_UNITS, false);
        permissions.getHumanResources().put(PURCHASE_TIER_LEVEL, false);
        permissions.getHumanResources().put(TRANSFER_Q_NIGHTS, false);

        return permissions.getPermissionArray();
    }

    private Map<String, Boolean> setPermissionMap()
    {
        Map<String, Boolean> permissions = new HashMap<String, Boolean>();

        permissions.put(GOODWILL_UNITS, true);
        permissions.put(PURCHASE_UNITS, true);
        permissions.put(PURCHASE_GIFT, true);
        permissions.put(TRANSFER_UNITS, true);
        permissions.put(PURCHASE_TIER_LEVEL, true);
        permissions.put(TRANSFER_Q_NIGHTS, true);

        return permissions;
    }

    @Factory(dataProvider = "verifyUnitAvailabilityProvider")
    public Object[] createInstances(String role, Map<String, Boolean> permissions)
    {
        return new Object[] { new UnitOperationsAvailabilityFactoryTest(role, permissions) };
    }
}
