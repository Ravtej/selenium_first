package com.ihg.crm.automation.selenium.tests.servicecenter.security.program;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.isDisplayedAndEnabled;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Program.ALLIANCE_A;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Program.ALLIANCE_D;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Program.ALLIANCE_M;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Program.ALTERNATE_ID_D;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Program.ALTERNATE_ID_M;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Program.AMB_SUMMARY_M;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Program.AMB_TAB_V;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Program.ANNUAL_ACTIVITY_V;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Program.BR_SUMMARY_M;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Program.DECLINE_TSCS;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Program.DR_SUMMARY_M;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Program.DR_TAB_V;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Program.EARNING_PREFS_M;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Program.EMPLOYEE_RATE_ELIGIBILITY_M;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Program.EMP_SUMMARY_M;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Program.EMP_TAB_V;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Program.ENROLLMENT_RENEW;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Program.EXTEND_MEMBERSHIP_AMB;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Program.HTL_SUMMARY_M;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Program.HTL_TAB_V;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Program.IHG_SUMMARY_M;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Program.IHG_TAB_V;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Program.KAR_SUMMARY_M;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Program.KAR_TAB_V;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Program.PC_PURCHASE_LEVEL;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Program.PC_SUMMARY_M;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Program.PC_TAB_V;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Program.PROGRAM_MEMBER_POINT_BALANCE_V;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Program.TIER_LEVEL_ACTV_AMB_V;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Program.TIER_LEVEL_AMB_M;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Program.TL_BENEFIT_M;

import java.util.Map;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.pages.program.EmployeeRateEligibility;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.servicecenter.pages.Tabs;
import com.ihg.automation.selenium.servicecenter.pages.programs.EmployeePage;
import com.ihg.automation.selenium.servicecenter.pages.programs.GuestAccountPage;
import com.ihg.automation.selenium.servicecenter.pages.programs.HotelPage;
import com.ihg.automation.selenium.servicecenter.pages.programs.KarmaPage;
import com.ihg.automation.selenium.servicecenter.pages.programs.ProgramSummary;
import com.ihg.automation.selenium.servicecenter.pages.programs.RenewExtendProgramSummary;
import com.ihg.automation.selenium.servicecenter.pages.programs.amb.AmbassadorPage;
import com.ihg.automation.selenium.servicecenter.pages.programs.br.BusinessRewardsPage;
import com.ihg.automation.selenium.servicecenter.pages.programs.dr.DiningRewardsPage;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.AllianceGrid;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.AllianceGridRow.AllianceCell;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.AlternateLoyaltyIDGrid;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.AlternateLoyaltyIDRow.AlternateCell;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.EarningPreference;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.RewardClubPage;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.RewardClubSummary;
import com.ihg.crm.automation.selenium.tests.servicecenter.security.LoginLogout;

public class ProgramAvailabilityFactoryTest extends LoginLogout
{
    private Tabs.ProgramInfoTab programInfoTab = new Tabs().new ProgramInfoTab();
    private RewardClubPage rewardClubPage = new RewardClubPage();
    private AmbassadorPage ambassadorPage = new AmbassadorPage();
    private GuestAccountPage guestAccountPage = new GuestAccountPage();
    private DiningRewardsPage diningRewardsPage = new DiningRewardsPage();
    private BusinessRewardsPage businessRewardsPage = new BusinessRewardsPage();
    private HotelPage hotelPage = new HotelPage();
    private KarmaPage karmaPage = new KarmaPage();
    private EmployeePage employeePage = new EmployeePage();
    private RewardClubSummary rewardClubSummary;
    private ProgramSummary ihgProgramSummary;

    public ProgramAvailabilityFactoryTest(String role, Map<String, Boolean> permissions)
    {
        super(role, permissions);
    }

    @BeforeClass
    private void beforeClass()
    {
        loginAndOpenMember(programsMemberIdWithOfferAndOrder);
    }

    @Test
    public void verifyViewProgramInfo()
    {
        verifyThat(programInfoTab, displayed(permissions.get(PROGRAM_MEMBER_POINT_BALANCE_V)));
    }

    @Test(dependsOnMethods = { "verifyViewProgramInfo" }, alwaysRun = true)
    public void verifyPriorityClubRewardsTab()
    {
        if (permissions.get(PROGRAM_MEMBER_POINT_BALANCE_V))
        {
            programInfoTab.goTo();
            verifyThat(programInfoTab.getProgram(Program.RC), displayed(permissions.get(PC_TAB_V)));
        }
    }

    @Test(dependsOnMethods = { "verifyPriorityClubRewardsTab" }, alwaysRun = true)
    public void verifyPriorityClubProgramSummary()
    {
        if (permissions.get(PC_TAB_V))
        {
            rewardClubPage.goTo();
            rewardClubSummary = rewardClubPage.getSummary();
            verifyThat(rewardClubSummary.getEdit(), isDisplayedAndEnabled(permissions.get(PC_SUMMARY_M)));
        }
    }

    @Test(dependsOnMethods = { "verifyPriorityClubProgramSummary" }, alwaysRun = true)
    public void verifyPurchaseTierLevelPCR()
    {
        if (permissions.get(PC_TAB_V))
        {
            rewardClubPage.goTo();
            rewardClubSummary = rewardClubPage.getSummary();
            verifyThat(rewardClubSummary.getPurchase(), isDisplayedAndEnabled(permissions.get(PC_PURCHASE_LEVEL)));
        }
    }

    @Test(dependsOnMethods = { "verifyPurchaseTierLevelPCR" }, alwaysRun = true)
    public void verifyTierLevelBenefitsLink()
    {
        if (permissions.get(PC_TAB_V))
        {
            rewardClubPage.goTo();
            rewardClubSummary = rewardClubPage.getSummary();
            verifyThat(rewardClubSummary.getTierLevelBenefits(), isDisplayedAndEnabled(permissions.get(TL_BENEFIT_M)));
        }
    }

    @Test(dependsOnMethods = { "verifyTierLevelBenefitsLink" }, alwaysRun = true)
    public void verifyAddAlliance()
    {
        if (permissions.get(PC_TAB_V))
        {
            rewardClubPage.goTo();
            verifyThat(rewardClubPage.getAddAlliance(), isDisplayedAndEnabled(permissions.get(ALLIANCE_A)));
        }
    }

    @Test(dependsOnMethods = { "verifyAddAlliance" }, alwaysRun = true)
    public void verifyEditAlliance()
    {
        if (permissions.get(PC_TAB_V))
        {
            rewardClubPage.goTo();
            AllianceGrid alliances = rewardClubPage.getAlliances();
            verifyThat(alliances, displayed(true));
            verifyThat(alliances.getRow(1).getCell(AllianceCell.ALLIANCE).asLink(),
                    displayed(permissions.get(ALLIANCE_M)));
        }
    }

    @Test(dependsOnMethods = { "verifyEditAlliance" }, alwaysRun = true)
    public void verifyDeleteAlliance()
    {
        if (permissions.get(PC_TAB_V))
        {
            rewardClubPage.goTo();
            verifyThat(rewardClubPage.getAlliances().getRow(1).getCell(AllianceCell.DELETE),
                    displayed(permissions.get(ALLIANCE_D)));
        }
    }

    @Test(dependsOnMethods = { "verifyDeleteAlliance" }, alwaysRun = true)
    public void verifyAddAlternateLoyaltyID()
    {
        if (permissions.get(PC_TAB_V))
        {
            rewardClubPage.goTo();
            verifyThat(rewardClubPage.getAddAlternateID(), isDisplayedAndEnabled(permissions.get(ALTERNATE_ID_M)));
        }
    }

    @Test(dependsOnMethods = { "verifyAddAlternateLoyaltyID" }, alwaysRun = true)
    public void verifyEditAlternateLoyaltyID()
    {
        if (permissions.get(PC_TAB_V))
        {
            rewardClubPage.goTo();
            AlternateLoyaltyIDGrid alternateLoyaltyID = rewardClubPage.getAlternateLoyaltyID();
            verifyThat(alternateLoyaltyID, displayed(true));
            verifyThat(alternateLoyaltyID.getRow(1).getCell(AlternateCell.TYPE).asLink(),
                    displayed(permissions.get(ALTERNATE_ID_M)));
        }
    }

    @Test(dependsOnMethods = { "verifyEditAlternateLoyaltyID" }, alwaysRun = true)
    public void verifyDeleteAlternateLoyaltyID()
    {
        if (permissions.get(PC_TAB_V))
        {
            rewardClubPage.goTo();
            verifyThat(rewardClubPage.getAlternateLoyaltyID().getRow(1).getCell(AlternateCell.DELETE),
                    displayed(permissions.get(ALTERNATE_ID_D)));
        }
    }

    @Test(dependsOnMethods = { "verifyDeleteAlternateLoyaltyID" }, alwaysRun = true)
    public void verifyEarningPreferencesModify()
    {
        if (permissions.get(PC_TAB_V))
        {
            rewardClubPage.goTo();
            EarningPreference earningDetails = rewardClubPage.getEarningDetails();
            verifyThat(earningDetails, displayed(true));
            verifyThat(earningDetails.getEdit(), isDisplayedAndEnabled(permissions.get(EARNING_PREFS_M)));
        }
    }

    @Test(dependsOnMethods = { "verifyEarningPreferencesModify" }, alwaysRun = true)
    public void verifyIHGAccountTab()
    {
        if (permissions.get(PROGRAM_MEMBER_POINT_BALANCE_V))
        {
            programInfoTab.goTo();
            verifyThat(programInfoTab.getProgram(Program.IHG), displayed(permissions.get(IHG_TAB_V)));
        }
    }

    @Test(dependsOnMethods = { "verifyIHGAccountTab" }, alwaysRun = true)
    public void verifyIHGProgramSummary()
    {
        if (permissions.get(IHG_TAB_V))
        {
            guestAccountPage.goTo();
            ihgProgramSummary = guestAccountPage.getSummary();
            verifyThat(ihgProgramSummary.getEdit(), isDisplayedAndEnabled(permissions.get(IHG_SUMMARY_M)));
        }
    }

    @Test(dependsOnMethods = { "verifyIHGProgramSummary" }, alwaysRun = true)
    public void verifyAmbassadorAccountTab()
    {
        if (permissions.get(PROGRAM_MEMBER_POINT_BALANCE_V))
        {
            programInfoTab.goTo();
            verifyThat(programInfoTab.getProgram(Program.AMB), displayed(permissions.get(AMB_TAB_V)));
        }
    }

    @Test(dependsOnMethods = { "verifyAmbassadorAccountTab" }, alwaysRun = true)
    public void verifyAMBProgramSummary()
    {
        if (permissions.get(AMB_TAB_V))
        {
            ambassadorPage.goTo();
            verifyThat(ambassadorPage.getSummary().getEdit(), isDisplayedAndEnabled(permissions.get(AMB_SUMMARY_M)));
        }
    }

    @Test(dependsOnMethods = { "verifyAMBProgramSummary" }, alwaysRun = true)
    public void verifyExtendMembershipAMB()
    {
        if (permissions.get(AMB_TAB_V))
        {
            ambassadorPage.goTo();
            verifyThat(ambassadorPage.getSummary().getExtend(), displayed(permissions.get(EXTEND_MEMBERSHIP_AMB)));
        }
    }

    @Test(dependsOnMethods = { "verifyExtendMembershipAMB" }, alwaysRun = true)
    public void verifyTierLevelAMB()
    {
        if (permissions.get(AMB_SUMMARY_M))
        {
            ambassadorPage.goTo();
            RenewExtendProgramSummary summary = ambassadorPage.getSummary();
            summary.clickEdit();
            verifyThat(summary.getTierLevel(), displayed(permissions.get(TIER_LEVEL_AMB_M)));
            summary.clickCancel();
        }
    }

    @Test(dependsOnMethods = { "verifyTierLevelAMB" }, alwaysRun = true)
    public void verifyTierLevelActivityAMB()
    {
        if (permissions.get(AMB_TAB_V))
        {
            ambassadorPage.goTo();
            verifyThat(ambassadorPage.getAnnualActivities(), displayed(permissions.get(TIER_LEVEL_ACTV_AMB_V)));
        }
    }

    @Test(dependsOnMethods = { "verifyTierLevelActivityAMB" }, alwaysRun = true)
    public void verifyRenewMembershipAMB()
    {
        if (permissions.get(AMB_TAB_V))
        {
            ambassadorPage.goTo();
            verifyThat(ambassadorPage.getSummary().getRenew(), displayed(permissions.get(ENROLLMENT_RENEW)));
        }
    }

    @Test(dependsOnMethods = { "verifyRenewMembershipAMB" }, alwaysRun = true)
    public void verifyDRProgramSummary()
    {
        if (permissions.get(DR_TAB_V))
        {
            diningRewardsPage.goTo();
            verifyThat(diningRewardsPage.getSummary().getEdit(), isDisplayedAndEnabled(permissions.get(DR_SUMMARY_M)));
        }
    }

    @Test(dependsOnMethods = { "verifyDRProgramSummary" }, groups = { "brTests" })
    public void verifyDeclineTsAndCsBR()
    {
        if (permissions.get(PROGRAM_MEMBER_POINT_BALANCE_V))
        {
            businessRewardsPage.goTo();
            verifyThat(businessRewardsPage.getSummary().getDeclinedTsAndCs(),
                    isDisplayedAndEnabled(permissions.get(DECLINE_TSCS)));
        }
    }

    @Test(dependsOnMethods = { "verifyDRProgramSummary" }, groups = { "brTests" })
    public void verifyBRProgramSummary()
    {
        if (permissions.get(PROGRAM_MEMBER_POINT_BALANCE_V))
        {
            businessRewardsPage.goTo();
            verifyThat(businessRewardsPage.getSummary().getEdit(),
                    isDisplayedAndEnabled(permissions.get(BR_SUMMARY_M)));
        }
    }

    @Test(dependsOnMethods = { "verifyDRProgramSummary" }, alwaysRun = true)
    public void verifyEmployeeAccountTab()
    {
        if (permissions.get(PROGRAM_MEMBER_POINT_BALANCE_V))
        {
            programInfoTab.goTo();
            verifyThat(programInfoTab.getProgram(Program.EMP), displayed(permissions.get(EMP_TAB_V)));
        }
    }

    @Test(dependsOnMethods = { "verifyEmployeeAccountTab" }, alwaysRun = true)
    public void verifyEMPProgramSummary()
    {
        if (permissions.get(EMP_TAB_V))
        {
            employeePage.goTo();
            verifyThat(employeePage.getSummary().getEdit(), isDisplayedAndEnabled(permissions.get(EMP_SUMMARY_M)));
        }
    }

    @Test(dependsOnMethods = { "verifyEMPProgramSummary" }, alwaysRun = true)
    public void verifyAnnualActivity()
    {
        if (permissions.get(PROGRAM_MEMBER_POINT_BALANCE_V))
        {
            programInfoTab.goTo();
            verifyThat(programInfoTab.getAnnualActivity(), displayed(permissions.get(ANNUAL_ACTIVITY_V)));
        }
    }

    @Test(dependsOnMethods = { "verifyAnnualActivity" }, alwaysRun = true)
    public void verifyEmployeeRateEligibilityWithoutORM()
    {
        if (permissions.get(PC_TAB_V))
        {
            rewardClubPage.goTo();
            EmployeeRateEligibility employeeRateEligibility = rewardClubPage.getEmployeeRateEligibility();
            verifyThat(employeeRateEligibility, displayed(true));
            verifyThat(employeeRateEligibility.getEdit(),
                    isDisplayedAndEnabled(permissions.get(EMPLOYEE_RATE_ELIGIBILITY_M)));
        }
    }

    @Test(dependsOnMethods = { "verifyEmployeeRateEligibilityWithoutORM" }, alwaysRun = true)
    public void verifyEmployeeRateEligibilityWithORM()
    {
        searchNewMember(programsMemberIdWithORM);

        if (permissions.get(PC_TAB_V))
        {
            rewardClubPage.goTo();
            EmployeeRateEligibility employeeRateEligibility = rewardClubPage.getEmployeeRateEligibility();
            verifyThat(employeeRateEligibility, displayed(true));
            verifyThat(employeeRateEligibility.getEdit(),
                    isDisplayedAndEnabled(permissions.get(EMPLOYEE_RATE_ELIGIBILITY_M)));
        }
    }

    @Test(dependsOnMethods = { "verifyEmployeeRateEligibilityWithORM" }, alwaysRun = true)
    public void verifyHtlTab()
    {
        searchNewMember(hotelMemberId);

        if (permissions.get(PROGRAM_MEMBER_POINT_BALANCE_V))
        {
            programInfoTab.goTo();
            verifyThat(programInfoTab.getProgram(Program.HTL), displayed(permissions.get(HTL_TAB_V)));
        }
    }

    @Test(dependsOnMethods = { "verifyHtlTab" })
    public void verifyHTLProgramSummary()
    {
        if (permissions.get(HTL_TAB_V))
        {
            hotelPage.goTo();
            verifyThat(hotelPage.getSummary().getEdit(), isDisplayedAndEnabled(permissions.get(HTL_SUMMARY_M)));
        }
    }

    @Test(dependsOnMethods = { "verifyHTLProgramSummary" }, alwaysRun = true, groups = { "karma" })
    public void verifyKarmaAccountTab()
    {
        if (permissions.get(PROGRAM_MEMBER_POINT_BALANCE_V))
        {
            programInfoTab.goTo();
            verifyThat(programInfoTab.getProgram(Program.KAR), displayed(permissions.get(KAR_TAB_V)));
        }
    }

    @Test(dependsOnMethods = { "verifyKarmaAccountTab" }, groups = { "karma" })
    public void verifyKarmaProgramSummary()
    {
        if (permissions.get(KAR_TAB_V))
        {
            karmaPage.goTo();
            verifyThat(karmaPage.getSummary().getEdit(), isDisplayedAndEnabled(permissions.get(KAR_SUMMARY_M)));
        }
    }

}
