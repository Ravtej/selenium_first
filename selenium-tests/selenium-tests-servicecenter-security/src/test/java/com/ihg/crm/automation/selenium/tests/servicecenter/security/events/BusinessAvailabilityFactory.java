package com.ihg.crm.automation.selenium.tests.servicecenter.security.events;

import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Business.BUSINESS_A;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Business.BUSINESS_V;

import java.util.HashMap;
import java.util.Map;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Factory;

import com.ihg.crm.automation.selenium.tests.servicecenter.security.SecurityMatrix;

public class BusinessAvailabilityFactory
{
    @DataProvider(name = "verifyBusinessAvailabilityProvider")
    public Object[][] verifyBusinessAvailabilityProvider()
    {
        Map<String, Boolean> defaultPermissions = setPermissionMap();

        SecurityMatrix permissions = new SecurityMatrix(defaultPermissions);

        permissions.getAgentPermissions().put(BUSINESS_V, false);
        permissions.getAgentPermissions().put(BUSINESS_A, false);

        permissions.getReadOnlyMarketing().put(BUSINESS_V, false);
        permissions.getReadOnlyMarketing().put(BUSINESS_A, false);

        permissions.getHumanResources().put(BUSINESS_V, false);
        permissions.getHumanResources().put(BUSINESS_A, false);

        permissions.getReadOnlyVendor().put(BUSINESS_V, false);
        permissions.getReadOnlyVendor().put(BUSINESS_A, false);

        permissions.getPartnerVendor().put(BUSINESS_V, false);
        permissions.getPartnerVendor().put(BUSINESS_A, false);

        return permissions.getPermissionArray();
    }

    private Map<String, Boolean> setPermissionMap()
    {
        Map<String, Boolean> permissions = new HashMap<String, Boolean>();

        permissions.put(BUSINESS_V, true);
        permissions.put(BUSINESS_A, true);

        return permissions;
    }

    @Factory(dataProvider = "verifyBusinessAvailabilityProvider")
    public Object[] createInstances(String role, Map<String, Boolean> permissions)
    {
        return new Object[] { new BusinessAvailabilityFactoryTest(role, permissions) };
    }
}
