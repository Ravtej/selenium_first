package com.ihg.crm.automation.selenium.tests.servicecenter.security.copartner;

import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.CoPartner.COPARTNER_A;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.CoPartner.COPARTNER_M;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.CoPartner.COPARTNER_V;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.CoPartner.MEETING_M;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Factory;

import com.ihg.crm.automation.selenium.tests.servicecenter.security.SecurityMatrix;

public class CoPartnerAndMeetingFactory
{
    @DataProvider(name = "verifyCoPartnerAndMeetingProvider")
    public Object[][] verifyCoPartnerAndMeetingProvider() throws IOException
    {
        Map<String, Boolean> defaultPermissions = setPermissionMap();
        SecurityMatrix permissions = new SecurityMatrix(defaultPermissions);

        permissions.getAgentPermissions().put(COPARTNER_M, false);
        permissions.getAgentPermissions().put(MEETING_M, false);

        permissions.getProgramSales().put(COPARTNER_A, false);
        permissions.getProgramSales().put(COPARTNER_M, false);
        permissions.getProgramSales().put(MEETING_M, false);

        permissions.getFraudTeam().put(COPARTNER_A, false);
        permissions.getFraudTeam().put(COPARTNER_M, false);
        permissions.getFraudTeam().put(MEETING_M, false);

        permissions.getAgentReadOnly().put(COPARTNER_V, false);
        permissions.getAgentReadOnly().put(COPARTNER_A, false);
        permissions.getAgentReadOnly().put(COPARTNER_M, false);
        permissions.getAgentReadOnly().put(MEETING_M, false);

        permissions.getReadOnlyMarketing().put(COPARTNER_A, false);
        permissions.getReadOnlyMarketing().put(COPARTNER_M, false);

        permissions.getReadOnlyVendor().put(COPARTNER_A, false);
        permissions.getReadOnlyVendor().put(COPARTNER_M, false);
        permissions.getReadOnlyVendor().put(MEETING_M, false);

        permissions.getPartnerVendor().put(COPARTNER_A, false);
        permissions.getPartnerVendor().put(COPARTNER_M, false);
        permissions.getPartnerVendor().put(MEETING_M, false);

        permissions.getHumanResources().put(COPARTNER_V, false);
        permissions.getHumanResources().put(COPARTNER_A, false);
        permissions.getHumanResources().put(COPARTNER_M, false);
        permissions.getHumanResources().put(MEETING_M, false);

        return permissions.getPermissionArray();
    }

    private Map<String, Boolean> setPermissionMap()
    {
        Map<String, Boolean> permissions = new HashMap<String, Boolean>();

        permissions.put(COPARTNER_V, true);
        permissions.put(COPARTNER_A, true);
        permissions.put(COPARTNER_M, true);
        permissions.put(MEETING_M, true);

        return permissions;
    }

    @Factory(dataProvider = "verifyCoPartnerAndMeetingProvider")
    public Object[] createInstances(String role, Map<String, Boolean> permissions)
    {
        return new Object[] { new CoPartnerAndMeetingFactoryTest(role, permissions) };
    }
}
