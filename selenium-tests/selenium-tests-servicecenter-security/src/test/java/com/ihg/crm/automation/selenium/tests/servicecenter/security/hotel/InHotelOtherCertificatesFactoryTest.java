package com.ihg.crm.automation.selenium.tests.servicecenter.security.hotel;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.isDisplayedAndEnabled;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.HotelOperations.OTHER_ACTIVITY_V;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.HotelOperations.OTHER_CERTIFICATE_A;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.HotelOperations.OTHER_CERTIFICATE_M;

import java.util.Map;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.servicecenter.pages.TopMenu;
import com.ihg.automation.selenium.servicecenter.pages.hotel.certificate.EditHotelCertificatePopUp;
import com.ihg.automation.selenium.servicecenter.pages.hotel.certificate.HotelCertificatePage;
import com.ihg.automation.selenium.servicecenter.pages.hotel.certificate.HotelCertificateSearchRowContainer;
import com.ihg.crm.automation.selenium.tests.servicecenter.security.LoginLogout;

public class InHotelOtherCertificatesFactoryTest extends LoginLogout
{
    private TopMenu topMenu = new TopMenu();
    private HotelCertificatePage hotelCertificatePage = new HotelCertificatePage();

    public InHotelOtherCertificatesFactoryTest(String role, Map<String, Boolean> permissions)
    {
        super(role, permissions);
    }

    @BeforeClass
    public void beforeClass()
    {
        login();
    }

    @Test
    public void verifyHotelOperationsTab()
    {
        verifyThat(topMenu.getHotelOperations(), displayed(permissions.get(OTHER_ACTIVITY_V)));
    }

    @Test(dependsOnMethods = "verifyHotelOperationsTab")
    public void verifyViewInHotelOtherCertificates()
    {
        if (permissions.get(OTHER_ACTIVITY_V))
        {
            hotelCertificatePage.goTo();

            verifyThat(hotelCertificatePage, displayed(permissions.get(OTHER_ACTIVITY_V)));
            verifyThat(hotelCertificatePage.getButtonBar().getSearch(), displayed(permissions.get(OTHER_ACTIVITY_V)));

            hotelCertificatePage.searchByHotelCode("ATLCP");
            HotelCertificateSearchRowContainer hotelCertificateSearchRowContainer = hotelCertificatePage.getGrid()
                    .getRow(1).expand(HotelCertificateSearchRowContainer.class);
            verifyThat(hotelCertificateSearchRowContainer.getDetails(), displayed(permissions.get(OTHER_ACTIVITY_V)));
        }
    }

    @Test(dependsOnMethods = "verifyHotelOperationsTab")
    public void verifyEditInHotelOtherCertificates()
    {
        if (permissions.get(OTHER_ACTIVITY_V))
        {
            hotelCertificatePage.goTo();
            verifyThat(hotelCertificatePage.getButtonBar().getEnterCertificate(),
                    displayed(permissions.get(OTHER_CERTIFICATE_A)));

            if (permissions.get(OTHER_CERTIFICATE_A))
            {
                hotelCertificatePage.getButtonBar().clickEnterCertificate();
                EditHotelCertificatePopUp editHotelCertificatePopUp = new EditHotelCertificatePopUp();

                verifyThat(editHotelCertificatePopUp.getSaveApprove(),
                        isDisplayedAndEnabled(permissions.get(OTHER_CERTIFICATE_A)));
                editHotelCertificatePopUp.clickClose();
            }
        }
    }

    @Test(dependsOnMethods = "verifyHotelOperationsTab")
    public void verifyCancelInHotelOtherCertificate()
    {
        if (permissions.get(OTHER_CERTIFICATE_A))
        {
            hotelCertificatePage.goTo();
            verifyThat(hotelCertificatePage.getButtonBar().getCancelCertificate(),
                    isDisplayedAndEnabled(permissions.get(OTHER_CERTIFICATE_M)));
        }
    }

}
