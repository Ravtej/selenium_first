package com.ihg.crm.automation.selenium.tests.servicecenter.security.enroll;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Enroll.ENROLLMENT_ENROLL;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Enroll.ENROLL_AMB;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Enroll.ENROLL_BR;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Enroll.ENROLL_EMP;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Enroll.ENROLL_HTL;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Enroll.ENROLL_IHG;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Enroll.ENROLL_KAR;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Enroll.ENROLL_PC;

import java.util.Map;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.servicecenter.pages.TopMenu;
import com.ihg.automation.selenium.servicecenter.pages.enroll.EnrollmentPage;
import com.ihg.crm.automation.selenium.tests.servicecenter.security.LoginLogout;

public class TopMenuEnrollItemAvailabilityFactoryTest extends LoginLogout
{
    private TopMenu topMenu = new TopMenu();
    private EnrollmentPage enrollmentPage = new EnrollmentPage();

    public TopMenuEnrollItemAvailabilityFactoryTest(String role, Map<String, Boolean> permissions)
    {
        super(role, permissions);
    }

    @BeforeClass
    private void beforeClass()
    {
        login();
    }

    @Test
    public void verifyTopMenuEnrollButtonAvailability()
    {
        verifyThat(topMenu.getEnrollment(), displayed(permissions.get(ENROLLMENT_ENROLL)));
    }

    @Test(dependsOnMethods = { "verifyTopMenuEnrollButtonAvailability" })
    public void verifyCanEnrollPCRMember()
    {
        verifyCanEnrollMember(Program.RC, permissions.get(ENROLL_PC));
    }

    @Test(dependsOnMethods = { "verifyTopMenuEnrollButtonAvailability" })
    public void verifyCanEnrollEMPMember()
    {
        verifyCanEnrollMember(Program.EMP, permissions.get(ENROLL_EMP));
    }

    @Test(dependsOnMethods = { "verifyTopMenuEnrollButtonAvailability" })
    public void verifyCanEnrollAMBMember()
    {
        verifyCanEnrollMember(Program.AMB, permissions.get(ENROLL_AMB));
    }

    @Test(dependsOnMethods = { "verifyTopMenuEnrollButtonAvailability" })
    public void verifyCanEnrollDRMember()
    {
        verifyCanEnrollMember(Program.DR, false);
    }

    @Test(dependsOnMethods = { "verifyTopMenuEnrollButtonAvailability" }, groups = { "brTests" })
    public void verifyCanEnrollBRMember()
    {
        verifyCanEnrollMember(Program.BR, permissions.get(ENROLL_BR));
    }

    @Test(dependsOnMethods = { "verifyTopMenuEnrollButtonAvailability" })
    public void verifyCanEnrollIHGMember()
    {
        verifyCanEnrollMember(Program.IHG, permissions.get(ENROLL_IHG));
    }

    @Test(dependsOnMethods = { "verifyTopMenuEnrollButtonAvailability" })
    public void verifyCanEnrollHTLMember()
    {
        verifyCanEnrollMember(Program.HTL, permissions.get(ENROLL_HTL));
    }

    @Test(dependsOnMethods = { "verifyTopMenuEnrollButtonAvailability" }, groups = { "karma" })
    public void verifyCanEnrollKARMember()
    {
        verifyCanEnrollMember(Program.KAR, permissions.get(ENROLL_KAR));
    }

    private void verifyCanEnrollMember(Program program, boolean displayed)
    {
        if (permissions.get(ENROLLMENT_ENROLL))
        {
            enrollmentPage.goTo();
            verifyThat(enrollmentPage.getPrograms().getProgramCheckbox(program), displayed(displayed));
        }
    }

}
