package com.ihg.crm.automation.selenium.tests.servicecenter.security.customer;

import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Guest.CONTACT_EMAIL_M;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Guest.GUEST_CONTACT_PHONE_ADDRESS_M;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Guest.GUEST_PERSONAL_M;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Guest.GUEST_PERSONAL_V;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Guest.GUEST_SMS_M;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Guest.GUEST_SMS_V;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Guest.GUEST_SOC_D;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Guest.GUEST_SOC_M;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Guest.GUEST_SOC_V;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Guest.PASSPORT_M;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Guest.PASSPORT_V;

import java.util.HashMap;
import java.util.Map;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Factory;

import com.ihg.crm.automation.selenium.tests.servicecenter.security.SecurityMatrix;

public class CustomerPersonalInfoAvailabilityFactory
{
    @DataProvider(name = "verifyCustomerAvailabilityProvider")
    public Object[][] verifyCustomerAvailabilityProvider()
    {
        Map<String, Boolean> defaultPermissions = setPermissionMap();
        SecurityMatrix permissions = new SecurityMatrix(defaultPermissions);

        permissions.getProgramSales().put(GUEST_PERSONAL_M, false);
        permissions.getProgramSales().put(GUEST_CONTACT_PHONE_ADDRESS_M, false);
        permissions.getProgramSales().put(GUEST_SMS_M, false);
        permissions.getProgramSales().put(PASSPORT_V, false);
        permissions.getProgramSales().put(PASSPORT_M, false);

        permissions.getFraudTeam().put(GUEST_SMS_M, false);

        permissions.getAgentReadOnly().put(GUEST_PERSONAL_M, false);
        permissions.getAgentReadOnly().put(GUEST_CONTACT_PHONE_ADDRESS_M, false);
        permissions.getAgentReadOnly().put(GUEST_SMS_V, false);
        permissions.getAgentReadOnly().put(GUEST_SMS_M, false);
        permissions.getAgentReadOnly().put(CONTACT_EMAIL_M, false);
        permissions.getAgentReadOnly().put(PASSPORT_V, false);
        permissions.getAgentReadOnly().put(PASSPORT_M, false);
        permissions.getAgentReadOnly().put(GUEST_SOC_V, false);
        permissions.getAgentReadOnly().put(GUEST_SOC_M, false);
        permissions.getAgentReadOnly().put(GUEST_SOC_D, false);

        permissions.getManagerMarketing().put(PASSPORT_V, false);
        permissions.getManagerMarketing().put(PASSPORT_M, false);

        permissions.getReadOnlyMarketing().put(GUEST_PERSONAL_M, false);
        permissions.getReadOnlyMarketing().put(GUEST_CONTACT_PHONE_ADDRESS_M, false);
        permissions.getReadOnlyMarketing().put(GUEST_SMS_M, false);
        permissions.getReadOnlyMarketing().put(CONTACT_EMAIL_M, false);
        permissions.getReadOnlyMarketing().put(PASSPORT_V, false);
        permissions.getReadOnlyMarketing().put(PASSPORT_M, false);
        permissions.getReadOnlyMarketing().put(GUEST_SOC_V, false);
        permissions.getReadOnlyMarketing().put(GUEST_SOC_M, false);
        permissions.getReadOnlyMarketing().put(GUEST_SOC_D, false);

        permissions.getReadOnlyVendor().put(GUEST_PERSONAL_M, false);
        permissions.getReadOnlyVendor().put(GUEST_CONTACT_PHONE_ADDRESS_M, false);
        permissions.getReadOnlyVendor().put(GUEST_SMS_M, false);
        permissions.getReadOnlyVendor().put(CONTACT_EMAIL_M, false);
        permissions.getReadOnlyVendor().put(PASSPORT_V, false);
        permissions.getReadOnlyVendor().put(PASSPORT_M, false);
        permissions.getReadOnlyVendor().put(GUEST_SOC_V, false);
        permissions.getReadOnlyVendor().put(GUEST_SOC_M, false);
        permissions.getReadOnlyVendor().put(GUEST_SOC_D, false);

        permissions.getPartnerVendor().put(GUEST_PERSONAL_M, false);
        permissions.getPartnerVendor().put(GUEST_CONTACT_PHONE_ADDRESS_M, false);
        permissions.getPartnerVendor().put(GUEST_SMS_M, false);
        permissions.getPartnerVendor().put(PASSPORT_V, false);
        permissions.getPartnerVendor().put(PASSPORT_M, false);
        permissions.getPartnerVendor().put(GUEST_SOC_V, false);
        permissions.getPartnerVendor().put(GUEST_SOC_M, false);
        permissions.getPartnerVendor().put(GUEST_SOC_D, false);

        permissions.getHumanResources().put(PASSPORT_V, false);
        permissions.getHumanResources().put(PASSPORT_M, false);
        permissions.getHumanResources().put(GUEST_SOC_V, false);
        permissions.getHumanResources().put(GUEST_SOC_M, false);
        permissions.getHumanResources().put(GUEST_SOC_D, false);

        return permissions.getPermissionArray();
    }

    private Map<String, Boolean> setPermissionMap()
    {
        Map<String, Boolean> permissions = new HashMap<String, Boolean>();

        permissions.put(GUEST_PERSONAL_V, true);
        permissions.put(GUEST_PERSONAL_M, true);
        permissions.put(GUEST_CONTACT_PHONE_ADDRESS_M, true);
        permissions.put(GUEST_SMS_V, true);
        permissions.put(GUEST_SMS_M, true);
        permissions.put(CONTACT_EMAIL_M, true);
        permissions.put(PASSPORT_V, true);
        permissions.put(PASSPORT_M, true);
        permissions.put(GUEST_SOC_V, true);
        permissions.put(GUEST_SOC_M, true);
        permissions.put(GUEST_SOC_D, true);

        return permissions;
    }

    @Factory(dataProvider = "verifyCustomerAvailabilityProvider")
    public Object[] createInstances(String role, Map<String, Boolean> permissions)
    {
        return new Object[] { new CustomerPersonalInfoAvailabilityFactoryTest(role, permissions) };
    }
}
