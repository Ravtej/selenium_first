package com.ihg.crm.automation.selenium.tests.servicecenter.security.hotel;

import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.HotelOperations.OTHER_ACTIVITY_V;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.HotelOperations.OTHER_CERTIFICATE_A;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.HotelOperations.OTHER_CERTIFICATE_M;

import java.util.HashMap;
import java.util.Map;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Factory;

import com.ihg.crm.automation.selenium.tests.servicecenter.security.SecurityMatrix;

public class InHotelOtherCertificatesFactory
{
    @DataProvider(name = "verifyInHotelOtherCertificatesProvider", parallel = true)
    public Object[][] verifyInHotelOtherCertificatesProvider()
    {
        Map<String, Boolean> defaultPermissions = setPermissionMap();
        SecurityMatrix permissions = new SecurityMatrix(defaultPermissions);

        permissions.getAgentTierLevel().put(OTHER_ACTIVITY_V, true);

        permissions.getHotelHelpDesk().put(OTHER_ACTIVITY_V, true);

        permissions.getAdminOperationsManager().put(OTHER_ACTIVITY_V, true);
        permissions.getAdminOperationsManager().put(OTHER_CERTIFICATE_A, true);
        permissions.getAdminOperationsManager().put(OTHER_CERTIFICATE_M, true);

        permissions.getAdminCentralProcessing().put(OTHER_ACTIVITY_V, true);
        permissions.getAdminCentralProcessing().put(OTHER_CERTIFICATE_A, true);
        permissions.getAdminCentralProcessing().put(OTHER_CERTIFICATE_M, true);

        permissions.getFraudTeam().put(OTHER_ACTIVITY_V, true);

        permissions.getSystemsAdministration().put(OTHER_ACTIVITY_V, true);
        permissions.getSystemsAdministration().put(OTHER_CERTIFICATE_A, true);
        permissions.getSystemsAdministration().put(OTHER_CERTIFICATE_M, true);

        permissions.getSystemsManager().put(OTHER_ACTIVITY_V, true);
        permissions.getSystemsManager().put(OTHER_CERTIFICATE_A, true);
        permissions.getSystemsManager().put(OTHER_CERTIFICATE_M, true);

        permissions.getSystemsCoordinator().put(OTHER_ACTIVITY_V, true);
        permissions.getSystemsCoordinator().put(OTHER_CERTIFICATE_A, true);
        permissions.getSystemsCoordinator().put(OTHER_CERTIFICATE_M, true);

        return permissions.getPermissionArray();
    }

    private Map<String, Boolean> setPermissionMap()
    {
        Map<String, Boolean> permissions = new HashMap<String, Boolean>();

        permissions.put(OTHER_ACTIVITY_V, false);
        permissions.put(OTHER_CERTIFICATE_A, false);
        permissions.put(OTHER_CERTIFICATE_M, false);

        return permissions;
    }

    @Factory(dataProvider = "verifyInHotelOtherCertificatesProvider")
    public Object[] createInstances(String role, Map<String, Boolean> permissions)
    {
        return new Object[] { new InHotelOtherCertificatesFactoryTest(role, permissions) };
    }
}
