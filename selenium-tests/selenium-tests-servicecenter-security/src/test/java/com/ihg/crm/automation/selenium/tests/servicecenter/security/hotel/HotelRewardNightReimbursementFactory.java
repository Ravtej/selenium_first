package com.ihg.crm.automation.selenium.tests.servicecenter.security.hotel;

import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.HotelOperations.APPROVE_REWARD_NIGHTS;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.HotelOperations.REWARD_NIGHTS_OCCUPANCY_M;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.HotelOperations.REWARD_NIGHTS_OCCUPANCY_V;

import java.util.HashMap;
import java.util.Map;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Factory;

import com.ihg.crm.automation.selenium.tests.servicecenter.security.SecurityMatrix;

public class HotelRewardNightReimbursementFactory
{
    @DataProvider(name = "verifyRewardNightReimbursementProvider", parallel = true)
    public Object[][] verifyRewardNightReimbursementProvider()
    {
        Map<String, Boolean> defaultPermissions = setPermissionMap();
        SecurityMatrix permissions = new SecurityMatrix(defaultPermissions);

        permissions.getAgentTierLevel().put(REWARD_NIGHTS_OCCUPANCY_V, true);

        permissions.getHotelHelpDesk().put(REWARD_NIGHTS_OCCUPANCY_V, true);

        permissions.getAdminOperationsManager().put(REWARD_NIGHTS_OCCUPANCY_V, true);
        permissions.getAdminOperationsManager().put(REWARD_NIGHTS_OCCUPANCY_M, true);
        permissions.getAdminOperationsManager().put(APPROVE_REWARD_NIGHTS, true);

        permissions.getAdminCentralProcessing().put(REWARD_NIGHTS_OCCUPANCY_V, true);
        permissions.getAdminCentralProcessing().put(REWARD_NIGHTS_OCCUPANCY_M, true);
        permissions.getAdminCentralProcessing().put(APPROVE_REWARD_NIGHTS, true);

        permissions.getFraudTeam().put(REWARD_NIGHTS_OCCUPANCY_V, true);

        permissions.getSystemsAdministration().put(REWARD_NIGHTS_OCCUPANCY_V, true);
        permissions.getSystemsAdministration().put(REWARD_NIGHTS_OCCUPANCY_M, true);
        permissions.getSystemsAdministration().put(APPROVE_REWARD_NIGHTS, true);

        permissions.getSystemsManager().put(REWARD_NIGHTS_OCCUPANCY_V, true);
        permissions.getSystemsManager().put(REWARD_NIGHTS_OCCUPANCY_M, true);
        permissions.getSystemsManager().put(APPROVE_REWARD_NIGHTS, true);

        permissions.getSystemsCoordinator().put(REWARD_NIGHTS_OCCUPANCY_V, true);
        permissions.getSystemsCoordinator().put(REWARD_NIGHTS_OCCUPANCY_M, true);
        permissions.getSystemsCoordinator().put(APPROVE_REWARD_NIGHTS, true);

        return permissions.getPermissionArray();
    }

    private Map<String, Boolean> setPermissionMap()
    {
        Map<String, Boolean> permissions = new HashMap<String, Boolean>();

        permissions.put(REWARD_NIGHTS_OCCUPANCY_V, false);
        permissions.put(REWARD_NIGHTS_OCCUPANCY_M, false);
        permissions.put(APPROVE_REWARD_NIGHTS, false);

        return permissions;
    }

    @Factory(dataProvider = "verifyRewardNightReimbursementProvider")
    public Object[] createInstances(String role, Map<String, Boolean> permissions)
    {
        return new Object[] { new HotelRewardNightReimbursementFactoryTest(role, permissions) };
    }
}
