package com.ihg.crm.automation.selenium.tests.servicecenter.security;

import static com.ihg.automation.selenium.servicecenter.pages.Role.ADMINISTRATION_AWARD_SUPPORT;
import static com.ihg.automation.selenium.servicecenter.pages.Role.ADMINISTRATION_CENTRAL_PROCESSING;
import static com.ihg.automation.selenium.servicecenter.pages.Role.ADMINISTRATION_OPERATIONS_MANAGER;
import static com.ihg.automation.selenium.servicecenter.pages.Role.AGENT;
import static com.ihg.automation.selenium.servicecenter.pages.Role.AGENT_READ_ONLY_LOOKUP;
import static com.ihg.automation.selenium.servicecenter.pages.Role.AGENT_SPECIALTY_PROGRAM_SALES;
import static com.ihg.automation.selenium.servicecenter.pages.Role.AGENT_TIER2;
import static com.ihg.automation.selenium.servicecenter.pages.Role.CORPORATE_OFFICE_HUMAN_RESOURCES;
import static com.ihg.automation.selenium.servicecenter.pages.Role.FRAUD_TEAM;
import static com.ihg.automation.selenium.servicecenter.pages.Role.HOTEL_HELP_DESK;
import static com.ihg.automation.selenium.servicecenter.pages.Role.MANAGER_MARKETING;
import static com.ihg.automation.selenium.servicecenter.pages.Role.PARTNER_VENDOR;
import static com.ihg.automation.selenium.servicecenter.pages.Role.READ_ONLY_MARKETING;
import static com.ihg.automation.selenium.servicecenter.pages.Role.READ_ONLY_VENDOR;
import static com.ihg.automation.selenium.servicecenter.pages.Role.SYSTEMS_ADMINISTRATION;
import static com.ihg.automation.selenium.servicecenter.pages.Role.SYSTEMS_COORDINATOR;
import static com.ihg.automation.selenium.servicecenter.pages.Role.SYSTEMS_MANAGER;

import java.util.HashMap;
import java.util.Map;

import com.ihg.automation.security.SecurityMatrixBase;

public class SecurityMatrix extends SecurityMatrixBase
{
    public SecurityMatrix(Map<String, Boolean> defaultPermissions)
    {
        securityMatrix.put(AGENT, new HashMap<>(defaultPermissions));
        securityMatrix.put(AGENT_SPECIALTY_PROGRAM_SALES, new HashMap<>(defaultPermissions));
        securityMatrix.put(AGENT_TIER2, new HashMap<>(defaultPermissions));
        securityMatrix.put(HOTEL_HELP_DESK, new HashMap<>(defaultPermissions));
        securityMatrix.put(ADMINISTRATION_AWARD_SUPPORT, new HashMap<>(defaultPermissions));
        securityMatrix.put(ADMINISTRATION_OPERATIONS_MANAGER, new HashMap<>(defaultPermissions));
        securityMatrix.put(ADMINISTRATION_CENTRAL_PROCESSING, new HashMap<>(defaultPermissions));
        securityMatrix.put(FRAUD_TEAM, new HashMap<>(defaultPermissions));
        securityMatrix.put(AGENT_READ_ONLY_LOOKUP, new HashMap<>(defaultPermissions));
        securityMatrix.put(SYSTEMS_ADMINISTRATION, new HashMap<>(defaultPermissions));
        securityMatrix.put(SYSTEMS_MANAGER, new HashMap<>(defaultPermissions));
        securityMatrix.put(SYSTEMS_COORDINATOR, new HashMap<>(defaultPermissions));
        securityMatrix.put(MANAGER_MARKETING, new HashMap<>(defaultPermissions));
        securityMatrix.put(READ_ONLY_MARKETING, new HashMap<>(defaultPermissions));
        securityMatrix.put(READ_ONLY_VENDOR, new HashMap<>(defaultPermissions));
        securityMatrix.put(PARTNER_VENDOR, new HashMap<>(defaultPermissions));
        securityMatrix.put(CORPORATE_OFFICE_HUMAN_RESOURCES, new HashMap<>(defaultPermissions));
    }

    public Map<String, Boolean> getAgentPermissions()
    {
        return securityMatrix.get(AGENT);
    }

    public Map<String, Boolean> getProgramSales()
    {
        return securityMatrix.get(AGENT_SPECIALTY_PROGRAM_SALES);
    }

    public Map<String, Boolean> getAgentTierLevel()
    {
        return securityMatrix.get(AGENT_TIER2);
    }

    public Map<String, Boolean> getHotelHelpDesk()
    {
        return securityMatrix.get(HOTEL_HELP_DESK);
    }

    public Map<String, Boolean> getAdminAwardSupport()
    {
        return securityMatrix.get(ADMINISTRATION_AWARD_SUPPORT);
    }

    public Map<String, Boolean> getAdminOperationsManager()
    {
        return securityMatrix.get(ADMINISTRATION_OPERATIONS_MANAGER);
    }

    public Map<String, Boolean> getAdminCentralProcessing()
    {
        return securityMatrix.get(ADMINISTRATION_CENTRAL_PROCESSING);
    }

    public Map<String, Boolean> getFraudTeam()
    {
        return securityMatrix.get(FRAUD_TEAM);
    }

    public Map<String, Boolean> getAgentReadOnly()
    {
        return securityMatrix.get(AGENT_READ_ONLY_LOOKUP);
    }

    public Map<String, Boolean> getSystemsAdministration()
    {
        return securityMatrix.get(SYSTEMS_ADMINISTRATION);
    }

    public Map<String, Boolean> getSystemsManager()
    {
        return securityMatrix.get(SYSTEMS_MANAGER);
    }

    public Map<String, Boolean> getSystemsCoordinator()
    {
        return securityMatrix.get(SYSTEMS_COORDINATOR);
    }

    public Map<String, Boolean> getManagerMarketing()
    {
        return securityMatrix.get(MANAGER_MARKETING);
    }

    public Map<String, Boolean> getReadOnlyMarketing()
    {
        return securityMatrix.get(READ_ONLY_MARKETING);
    }

    public Map<String, Boolean> getReadOnlyVendor()
    {
        return securityMatrix.get(READ_ONLY_VENDOR);
    }

    public Map<String, Boolean> getPartnerVendor()
    {
        return securityMatrix.get(PARTNER_VENDOR);
    }

    public Map<String, Boolean> getHumanResources()
    {
        return securityMatrix.get(CORPORATE_OFFICE_HUMAN_RESOURCES);
    }

}
