package com.ihg.crm.automation.selenium.tests.servicecenter.security.events;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Order.ORDER_HISTORY_V;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Order.ORDER_M;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Order.ORDER_V;

import java.util.Map;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.servicecenter.pages.Tabs;
import com.ihg.automation.selenium.servicecenter.pages.event.order.OrderDetailsPopUp;
import com.ihg.automation.selenium.servicecenter.pages.event.order.OrderEventsPage;
import com.ihg.automation.selenium.servicecenter.pages.event.order.OrderSystemDetailsGridRowContainer;
import com.ihg.crm.automation.selenium.tests.servicecenter.security.LoginLogout;

public class OrderAvailabilityFactoryTest extends LoginLogout
{
    private OrderEventsPage orderPage = new OrderEventsPage();
    private Tabs.EventsTab eventsTab = new Tabs().new EventsTab();

    public OrderAvailabilityFactoryTest(String role, Map<String, Boolean> permissions)
    {
        super(role, permissions);
    }

    @BeforeClass
    private void beforeClass()
    {
        loginAndOpenMember(programsMemberIdWithOfferAndOrder);
        eventsTab.goTo();
    }

    @Test
    public void verifyOrderTab()
    {
        verifyThat(eventsTab.getOrder(), displayed(permissions.get(ORDER_V)));
    }

    @Test(dependsOnMethods = "verifyOrderTab")
    public void verifyModifyGuestOrders()
    {
        if (permissions.get(ORDER_V))
        {
            orderPage.goTo();
            OrderSystemDetailsGridRowContainer rowContainer = orderPage.getOrdersGrid().getRow(1)
                    .expand(OrderSystemDetailsGridRowContainer.class);
            verifyThat(rowContainer.getDetails(), displayed(permissions.get(ORDER_M)));
        }
    }

    @Test(dependsOnMethods = "verifyOrderTab")
    public void verifyOrderHistory()
    {
        if (permissions.get(ORDER_V))
        {
            orderPage.goTo();
            OrderSystemDetailsGridRowContainer rowContainer = orderPage.getOrdersGrid().getRow(1)
                    .expand(OrderSystemDetailsGridRowContainer.class);
            if (permissions.get(ORDER_M))
            {
                rowContainer.clickDetails();
                OrderDetailsPopUp orderDetailsPopUp = new OrderDetailsPopUp();
                verifyThat(orderDetailsPopUp.getHistoryTab().getTab(), displayed(permissions.get(ORDER_HISTORY_V)));
                orderDetailsPopUp.clickClose();
            }
        }
    }
}
