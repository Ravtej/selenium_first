package com.ihg.crm.automation.selenium.tests.servicecenter.security.customer;

import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Guest.GUEST_CONTACT_PREF_V;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Guest.GUEST_LANG_M;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Guest.GUEST_MARKET_PREF_M;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Guest.GUEST_SERVICE_PREF_M;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Guest.GUEST_SERVICE_PREF_V;

import java.util.HashMap;
import java.util.Map;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Factory;

import com.ihg.crm.automation.selenium.tests.servicecenter.security.SecurityMatrix;

public class CustomerCommunicationPreferencesFactory
{

    @DataProvider(name = "verifyCommunicationPreferencesProvider")
    public Object[][] verifyCommunicationPreferencesProvider()
    {
        Map<String, Boolean> defaultPermissions = setPermissionMap();

        SecurityMatrix permissions = new SecurityMatrix(defaultPermissions);
        permissions.getAgentReadOnly().put(GUEST_CONTACT_PREF_V, false);
        permissions.getAgentReadOnly().put(GUEST_LANG_M, false);
        permissions.getAgentReadOnly().put(GUEST_MARKET_PREF_M, false);
        permissions.getAgentReadOnly().put(GUEST_SERVICE_PREF_V, false);
        permissions.getAgentReadOnly().put(GUEST_SERVICE_PREF_M, false);

        permissions.getReadOnlyMarketing().put(GUEST_LANG_M, false);
        permissions.getReadOnlyMarketing().put(GUEST_MARKET_PREF_M, false);
        permissions.getReadOnlyMarketing().put(GUEST_SERVICE_PREF_M, false);

        permissions.getReadOnlyVendor().put(GUEST_LANG_M, false);
        permissions.getReadOnlyVendor().put(GUEST_MARKET_PREF_M, false);
        permissions.getReadOnlyVendor().put(GUEST_SERVICE_PREF_M, false);

        permissions.getPartnerVendor().put(GUEST_MARKET_PREF_M, false);
        permissions.getPartnerVendor().put(GUEST_LANG_M, false);
        permissions.getPartnerVendor().put(GUEST_SERVICE_PREF_M, false);

        permissions.getHumanResources().put(GUEST_SERVICE_PREF_M, false);

        return permissions.getPermissionArray();
    }

    private Map<String, Boolean> setPermissionMap()
    {
        Map<String, Boolean> permissions = new HashMap<String, Boolean>();

        permissions.put(GUEST_CONTACT_PREF_V, true);
        permissions.put(GUEST_LANG_M, true);
        permissions.put(GUEST_MARKET_PREF_M, true);
        permissions.put(GUEST_SERVICE_PREF_V, true);
        permissions.put(GUEST_SERVICE_PREF_M, true);

        return permissions;
    }

    @Factory(dataProvider = "verifyCommunicationPreferencesProvider")
    public Object[] createInstances(String role, Map<String, Boolean> permissions)
    {
        return new Object[] { new CustomerCommunicationPreferencesFactoryTest(role, permissions) };
    }
}
