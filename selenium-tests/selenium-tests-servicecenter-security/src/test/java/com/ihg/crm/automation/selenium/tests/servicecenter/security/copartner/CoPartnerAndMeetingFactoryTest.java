package com.ihg.crm.automation.selenium.tests.servicecenter.security.copartner;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.enabled;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.isDisplayedAndEnabled;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.CoPartner.COPARTNER_A;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.CoPartner.COPARTNER_V;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.CoPartner.MEETING_M;

import java.util.Map;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.gwt.components.Button;
import com.ihg.automation.selenium.servicecenter.pages.Tabs;
import com.ihg.automation.selenium.servicecenter.pages.event.copartner.CoPartnerEventsPage;
import com.ihg.automation.selenium.servicecenter.pages.event.meeting.MeetingEventPage;
import com.ihg.crm.automation.selenium.tests.servicecenter.security.LoginLogout;

public class CoPartnerAndMeetingFactoryTest extends LoginLogout
{
    private CoPartnerEventsPage coPartnerEventsPage = new CoPartnerEventsPage();
    private Tabs.EventsTab eventsTab = new Tabs().getEvents();
    private MeetingEventPage meetingEventPage = new MeetingEventPage();

    public CoPartnerAndMeetingFactoryTest(String role, Map<String, Boolean> permissions)
    {
        super(role, permissions);
    }

    @BeforeClass
    public void beforeClass()
    {
        loginAndOpenMember(memberIdWithFreenightStay);
        eventsTab.goTo();
    }

    @Test
    public void verifyCoPartnerTab()
    {
        verifyThat(eventsTab.getCoPartner(), displayed(permissions.get(COPARTNER_V)));
    }

    @Test
    public void verifyMeetingTab()
    {
        eventsTab.goTo();
        verifyThat(eventsTab.getMeeting(), displayed(permissions.get(COPARTNER_V)));
    }

    @Test(dependsOnMethods = "verifyCoPartnerTab")
    public void verifyCreateCoPartner()
    {
        if (permissions.get(COPARTNER_V))
        {
            coPartnerEventsPage.goTo();
            verifyThat(coPartnerEventsPage.getSearchFields().getCreateCoPartner(),
                    isDisplayedAndEnabled(permissions.get(COPARTNER_A)));
        }
    }

    @Test(dependsOnMethods = "verifyMeetingTab")
    public void verifyCreateMeetingBonus()
    {
        if (permissions.get(COPARTNER_V))
        {
            meetingEventPage.goTo();

            Button createMeetingBonus = meetingEventPage.getCreateMeetingBonus();
            verifyThat(createMeetingBonus, displayed(permissions.get(COPARTNER_A)));

            if (permissions.get(COPARTNER_A))
            {
                verifyThat(createMeetingBonus, enabled(false));
            }
        }
    }

    @Test(dependsOnMethods = "verifyMeetingTab")
    public void verifyCreateMeetingEvent()
    {
        if (permissions.get(COPARTNER_V))
        {
            meetingEventPage.goTo();

            Button createMeeting = meetingEventPage.getCreateMeeting();
            verifyThat(createMeeting, displayed(permissions.get(MEETING_M)));

            if (permissions.get(MEETING_M))
            {
                verifyThat(createMeeting, enabled(false));
            }
        }
    }
}
