package com.ihg.crm.automation.selenium.tests.servicecenter.security.catalog;

import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.HotelOperations.CATALOG_V;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.HotelOperations.SHOPPING_CART_M;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.HotelOperations.SHOPPING_CART_V;

import java.util.HashMap;
import java.util.Map;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Factory;

import com.ihg.crm.automation.selenium.tests.servicecenter.security.SecurityMatrix;

public class CatalogViewAvailabilityFactory
{
    @DataProvider(name = "verifyCatalogItemDetailsAvailabilityProvider")
    public Object[][] verifyCatalogItemDetailsAvailabilityProvider()
    {
        Map<String, Boolean> defaultPermissions = setPermissionMap();

        SecurityMatrix permissions = new SecurityMatrix(defaultPermissions);
        permissions.getProgramSales().put(SHOPPING_CART_V, false);
        permissions.getProgramSales().put(SHOPPING_CART_M, false);

        permissions.getAgentReadOnly().put(CATALOG_V, false);
        permissions.getAgentReadOnly().put(SHOPPING_CART_V, false);
        permissions.getAgentReadOnly().put(SHOPPING_CART_M, false);

        permissions.getManagerMarketing().put(SHOPPING_CART_V, false);
        permissions.getManagerMarketing().put(SHOPPING_CART_M, false);

        permissions.getReadOnlyMarketing().put(SHOPPING_CART_V, false);
        permissions.getReadOnlyMarketing().put(SHOPPING_CART_M, false);

        permissions.getReadOnlyVendor().put(SHOPPING_CART_V, false);
        permissions.getReadOnlyVendor().put(SHOPPING_CART_M, false);

        permissions.getPartnerVendor().put(SHOPPING_CART_V, false);
        permissions.getPartnerVendor().put(SHOPPING_CART_M, false);

        permissions.getHumanResources().put(SHOPPING_CART_V, false);
        permissions.getHumanResources().put(SHOPPING_CART_M, false);

        return permissions.getPermissionArray();
    }

    private Map<String, Boolean> setPermissionMap()
    {
        Map<String, Boolean> permissions = new HashMap<String, Boolean>();

        permissions.put(CATALOG_V, true);
        permissions.put(SHOPPING_CART_V, true);
        permissions.put(SHOPPING_CART_M, true);

        return permissions;
    }

    @Factory(dataProvider = "verifyCatalogItemDetailsAvailabilityProvider")
    public Object[] createInstances(String role, Map<String, Boolean> permissions)
    {
        return new Object[] { new CatalogAvailabilityFactoryTest(role, permissions) };
    }

}
