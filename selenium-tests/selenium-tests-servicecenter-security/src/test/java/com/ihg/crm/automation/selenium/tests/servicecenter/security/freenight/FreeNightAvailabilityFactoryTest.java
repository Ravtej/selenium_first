package com.ihg.crm.automation.selenium.tests.servicecenter.security.freenight;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.isDisplayedAndEnabled;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.FreeNight.FREE_NIGHT_GOODWILL_A;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.FreeNight.FREE_NIGHT_M;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.FreeNight.FREE_NIGHT_V;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.FreeNight.FREE_NIGHT_VOUCHER_CANCEL_M;

import java.util.Map;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.servicecenter.pages.Tabs;
import com.ihg.automation.selenium.servicecenter.pages.event.freenight.CancelFreeNightPopUp;
import com.ihg.automation.selenium.servicecenter.pages.event.freenight.FreeNightDetailsPopUp;
import com.ihg.automation.selenium.servicecenter.pages.event.freenight.FreeNightDetailsTab;
import com.ihg.automation.selenium.servicecenter.pages.event.freenight.FreeNightEventPage;
import com.ihg.automation.selenium.servicecenter.pages.event.freenight.FreeNightVoucherGridRow;
import com.ihg.automation.selenium.servicecenter.pages.event.freenight.FreeNightVoucherGridRow.FreeNightVoucherCell;
import com.ihg.crm.automation.selenium.tests.servicecenter.security.LoginLogout;

public class FreeNightAvailabilityFactoryTest extends LoginLogout
{
    private static final String VOUCHER_CODE = "6100025";
    private FreeNightEventPage freeNightPage = new FreeNightEventPage();
    private Tabs.EventsTab eventsTab = new Tabs().getEvents();
    private FreeNightDetailsPopUp freeNightPopUp;
    private final static String OFFER_CODE = "8310441";
    private FreeNightVoucherGridRow voucherRow;
    private CancelFreeNightPopUp cancelFreeNightPopUp;

    public FreeNightAvailabilityFactoryTest(String role, Map<String, Boolean> permissions)
    {
        super(role, permissions);
    }

    @BeforeClass
    public void beforeClass()
    {
        loginAndOpenMember(memberIdWithFreenightStay);
        eventsTab.goTo();
    }

    @Test
    public void verifyFreeNightTab()
    {
        verifyThat(eventsTab.getFreeNight(), displayed(permissions.get(FREE_NIGHT_V)));
    }

    @Test(dependsOnMethods = "verifyFreeNightTab")
    public void verifyModifyFreeNight()
    {
        if (permissions.get(FREE_NIGHT_V))
        {
            freeNightPage.goTo();
            freeNightPopUp = freeNightPage.getFreeNightGrid().getFreeNightDetailsPopUp(OFFER_CODE);
            FreeNightDetailsTab tab = freeNightPopUp.getFreeNightDetailsTab();
            tab.goTo();
            voucherRow = tab.getFreeNightVoucherGrid().getRowByVoucherCode(VOUCHER_CODE);

            verifyThat(voucherRow.getCell(FreeNightVoucherCell.CONFIRMATION).asLink(),
                    displayed(permissions.get(FREE_NIGHT_M)));
            freeNightPopUp.close();
        }
    }

    @Test(dependsOnMethods = "verifyFreeNightTab")
    public void verifyGoodwillFreeNight()
    {
        if (permissions.get(FREE_NIGHT_V))
        {
            freeNightPage.goTo();
            freeNightPopUp = freeNightPage.getFreeNightGrid().getFreeNightDetailsPopUp(OFFER_CODE);

            verifyThat(freeNightPopUp.getGoodwillFreeNightButton(), displayed(permissions.get(FREE_NIGHT_GOODWILL_A)));
            freeNightPopUp.close();
        }
    }

    @Test(dependsOnMethods = "verifyFreeNightTab")
    public void verifyCancelFreeNightVoucher()
    {
        if (permissions.get(FREE_NIGHT_M))
        {
            freeNightPage.goTo();
            freeNightPopUp = freeNightPage.getFreeNightGrid().getFreeNightDetailsPopUp(OFFER_CODE);
            FreeNightDetailsTab tab = freeNightPopUp.getFreeNightDetailsTab();
            tab.goTo();
            voucherRow = tab.getFreeNightVoucherGrid().getRowByVoucherCode(VOUCHER_CODE);
            voucherRow.getCell(FreeNightVoucherCell.CONFIRMATION).clickByLabel();

            cancelFreeNightPopUp = new CancelFreeNightPopUp();

            verifyThat(cancelFreeNightPopUp.getCancelFreeNightButton(),
                    isDisplayedAndEnabled(permissions.get(FREE_NIGHT_VOUCHER_CANCEL_M)));
            cancelFreeNightPopUp.clickClose();
            freeNightPopUp.close();
        }
    }

}
