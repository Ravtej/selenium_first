package com.ihg.crm.automation.selenium.tests.servicecenter.security.enroll;

import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Enroll.ENROLLMENT_ENROLL;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Enroll.ENROLL_AMB;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Enroll.ENROLL_BR;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Enroll.ENROLL_DR;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Enroll.ENROLL_EMP;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Enroll.ENROLL_HTL;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Enroll.ENROLL_IHG;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Enroll.ENROLL_KAR;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Enroll.ENROLL_MTGPL;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Enroll.ENROLL_PC;

import java.util.HashMap;
import java.util.Map;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Factory;

import com.ihg.crm.automation.selenium.tests.servicecenter.security.SecurityMatrix;

public class TopMenuEnrollItemAvailabilityFactory
{
    @DataProvider(name = "verifyCatalogItemDetailsAvailabilityProvider")
    public Object[][] verifyCatalogItemDetailsAvailabilityProvider()
    {
        Map<String, Boolean> defaultPermissions = setPermissionMap();

        SecurityMatrix permissions = new SecurityMatrix(defaultPermissions);

        permissions.getAgentPermissions().put(ENROLL_AMB, false);
        permissions.getAgentPermissions().put(ENROLL_DR, false);
        permissions.getAgentPermissions().put(ENROLL_MTGPL, false);
        permissions.getAgentPermissions().put(ENROLL_BR, false);
        permissions.getAgentPermissions().put(ENROLL_HTL, false);

        permissions.getProgramSales().put(ENROLL_EMP, false);
        permissions.getProgramSales().put(ENROLL_MTGPL, false);
        permissions.getProgramSales().put(ENROLL_BR, false);
        permissions.getProgramSales().put(ENROLL_IHG, false);
        permissions.getProgramSales().put(ENROLL_HTL, false);

        permissions.getHotelHelpDesk().put(ENROLL_AMB, false);
        permissions.getHotelHelpDesk().put(ENROLL_DR, false);

        permissions.getAdminAwardSupport().put(ENROLL_AMB, false);
        permissions.getAdminAwardSupport().put(ENROLL_DR, false);
        permissions.getAdminAwardSupport().put(ENROLL_MTGPL, false);
        permissions.getAdminAwardSupport().put(ENROLL_BR, false);

        permissions.getFraudTeam().put(ENROLL_EMP, false);
        permissions.getFraudTeam().put(ENROLL_AMB, false);
        permissions.getFraudTeam().put(ENROLL_DR, false);
        permissions.getFraudTeam().put(ENROLL_MTGPL, false);
        permissions.getFraudTeam().put(ENROLL_BR, false);
        permissions.getFraudTeam().put(ENROLL_IHG, false);
        permissions.getFraudTeam().put(ENROLL_HTL, false);
        permissions.getFraudTeam().put(ENROLL_KAR, false);

        permissions.getAgentReadOnly().put(ENROLLMENT_ENROLL, false);
        permissions.getAgentReadOnly().put(ENROLL_PC, false);
        permissions.getAgentReadOnly().put(ENROLL_EMP, false);
        permissions.getAgentReadOnly().put(ENROLL_AMB, false);
        permissions.getAgentReadOnly().put(ENROLL_DR, false);
        permissions.getAgentReadOnly().put(ENROLL_MTGPL, false);
        permissions.getAgentReadOnly().put(ENROLL_BR, false);
        permissions.getAgentReadOnly().put(ENROLL_IHG, false);
        permissions.getAgentReadOnly().put(ENROLL_HTL, false);
        permissions.getAgentReadOnly().put(ENROLL_KAR, false);

        permissions.getReadOnlyMarketing().put(ENROLLMENT_ENROLL, false);
        permissions.getReadOnlyMarketing().put(ENROLL_PC, false);
        permissions.getReadOnlyMarketing().put(ENROLL_EMP, false);
        permissions.getReadOnlyMarketing().put(ENROLL_AMB, false);
        permissions.getReadOnlyMarketing().put(ENROLL_DR, false);
        permissions.getReadOnlyMarketing().put(ENROLL_MTGPL, false);
        permissions.getReadOnlyMarketing().put(ENROLL_BR, false);
        permissions.getReadOnlyMarketing().put(ENROLL_IHG, false);
        permissions.getReadOnlyMarketing().put(ENROLL_HTL, false);
        permissions.getReadOnlyMarketing().put(ENROLL_KAR, false);

        permissions.getReadOnlyVendor().put(ENROLLMENT_ENROLL, false);
        permissions.getReadOnlyVendor().put(ENROLL_PC, false);
        permissions.getReadOnlyVendor().put(ENROLL_EMP, false);
        permissions.getReadOnlyVendor().put(ENROLL_AMB, false);
        permissions.getReadOnlyVendor().put(ENROLL_DR, false);
        permissions.getReadOnlyVendor().put(ENROLL_MTGPL, false);
        permissions.getReadOnlyVendor().put(ENROLL_BR, false);
        permissions.getReadOnlyVendor().put(ENROLL_IHG, false);
        permissions.getReadOnlyVendor().put(ENROLL_HTL, false);
        permissions.getReadOnlyVendor().put(ENROLL_KAR, false);

        permissions.getPartnerVendor().put(ENROLL_EMP, false);
        permissions.getPartnerVendor().put(ENROLL_AMB, false);
        permissions.getPartnerVendor().put(ENROLL_DR, false);
        permissions.getPartnerVendor().put(ENROLL_MTGPL, false);
        permissions.getPartnerVendor().put(ENROLL_BR, false);
        permissions.getPartnerVendor().put(ENROLL_IHG, false);
        permissions.getPartnerVendor().put(ENROLL_HTL, false);
        permissions.getPartnerVendor().put(ENROLL_KAR, false);

        permissions.getHumanResources().put(ENROLL_EMP, false);
        permissions.getHumanResources().put(ENROLL_AMB, false);
        permissions.getHumanResources().put(ENROLL_DR, false);
        permissions.getHumanResources().put(ENROLL_MTGPL, false);
        permissions.getHumanResources().put(ENROLL_BR, false);
        permissions.getHumanResources().put(ENROLL_IHG, false);
        permissions.getHumanResources().put(ENROLL_HTL, false);
        permissions.getHumanResources().put(ENROLL_KAR, false);

        return permissions.getPermissionArray();
    }

    private Map<String, Boolean> setPermissionMap()
    {
        Map<String, Boolean> permissions = new HashMap<String, Boolean>();

        permissions.put(ENROLLMENT_ENROLL, true);
        permissions.put(ENROLL_PC, true);
        permissions.put(ENROLL_EMP, true);
        permissions.put(ENROLL_AMB, true);
        permissions.put(ENROLL_DR, true);
        permissions.put(ENROLL_MTGPL, true);
        permissions.put(ENROLL_BR, true);
        permissions.put(ENROLL_IHG, true);
        permissions.put(ENROLL_HTL, true);
        permissions.put(ENROLL_KAR, true);

        return permissions;
    }

    @Factory(dataProvider = "verifyCatalogItemDetailsAvailabilityProvider")
    public Object[] createInstances(String role, Map<String, Boolean> permissions)
    {
        return new Object[] { new TopMenuEnrollItemAvailabilityFactoryTest(role, permissions) };
    }

}
