package com.ihg.crm.automation.selenium.tests.servicecenter.security.customer;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.isDisplayedAndEnabled;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Guest.GUEST_CONTACT_PREF_V;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Guest.GUEST_LANG_M;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Guest.GUEST_MARKET_PREF_M;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Guest.GUEST_SERVICE_PREF_M;

import java.util.Map;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.communication.ContactPermissionItems;
import com.ihg.automation.selenium.common.communication.MarketingPreferences;
import com.ihg.automation.selenium.common.communication.SmsMobileTextingMarketingPreferences;
import com.ihg.automation.selenium.gwt.components.Button;
import com.ihg.automation.selenium.gwt.components.label.Label;
import com.ihg.automation.selenium.servicecenter.pages.Tabs;
import com.ihg.automation.selenium.servicecenter.pages.communication.CommunicationPreferences;
import com.ihg.automation.selenium.servicecenter.pages.communication.CommunicationPreferencesPage;
import com.ihg.automation.selenium.servicecenter.pages.communication.CustomerService;
import com.ihg.crm.automation.selenium.tests.servicecenter.security.LoginLogout;

public class CustomerCommunicationPreferencesFactoryTest extends LoginLogout
{
    private CommunicationPreferencesPage communicationPreferencesPage = new CommunicationPreferencesPage();
    private Tabs.CustomerInfoTab customerInfoTab = new Tabs().getCustomerInfo();
    private boolean isEdit;

    public CustomerCommunicationPreferencesFactoryTest(String role, Map<String, Boolean> permissions)
    {
        super(role, permissions);
    }

    @BeforeClass
    private void beforeClass()
    {
        loginAndOpenMember(programsMemberIdWithOfferAndOrder);
        customerInfoTab.goTo();

        isEdit = permissions.get(GUEST_LANG_M) || permissions.get(GUEST_MARKET_PREF_M)
                || permissions.get(GUEST_SERVICE_PREF_M);
    }

    @Test
    public void verifyCommunicationPreferencesTab()
    {
        verifyThat(customerInfoTab.getCommunicationPref(), displayed(permissions.get(GUEST_CONTACT_PREF_V)));
    }

    @Test(dependsOnMethods = { "verifyCommunicationPreferencesTab" })
    public void verifyCustomerServiceSection()
    {
        if (permissions.get(GUEST_CONTACT_PREF_V))
        {
            communicationPreferencesPage.goTo();

            CommunicationPreferences communicationPreferences = communicationPreferencesPage
                    .getCommunicationPreferences();
            verifyThat(communicationPreferences.getCustomerService(), displayed(true));
            verifyThat(communicationPreferences.getMarketingPreferences(), displayed(true));
        }
    }

    @Test(dependsOnMethods = { "verifyCommunicationPreferencesTab" })
    public void verifyEditButton()
    {
        if (permissions.get(GUEST_CONTACT_PREF_V))
        {
            communicationPreferencesPage.goTo();

            verifyThat(communicationPreferencesPage.getCommunicationPreferences().getEdit(),
                    isDisplayedAndEnabled(isEdit));
        }
    }

    @Test(dependsOnMethods = { "verifyEditButton" })
    public void verifyEditLanguagePreferences()
    {
        if (openEditMode())
        {
            CommunicationPreferences communicationPreferences = communicationPreferencesPage
                    .getCommunicationPreferences();

            verifyThat(communicationPreferences.getPreferredLanguage(), displayed(permissions.get(GUEST_LANG_M)));
        }
    }

    @Test(dependsOnMethods = { "verifyEditButton" })
    public void verifyEditMarketingPreferences()
    {
        if (openEditMode())
        {
            MarketingPreferences marketingPreferences = communicationPreferencesPage.getCommunicationPreferences()
                    .getMarketingPreferences();

            verifyThat(marketingPreferences.getEmailAddress(), displayed(permissions.get(GUEST_MARKET_PREF_M)));

            ContactPermissionItems marketingPermissionItems = marketingPreferences.getContactPermissionItems();
            for (int index = 1; index <= marketingPermissionItems.getContactPermissionItemsCount(); index++)
            {
                verifyThat(marketingPermissionItems.getContactPermissionItem(index).getSubscribe(),
                        displayed(permissions.get(GUEST_MARKET_PREF_M)));
            }
        }
    }

    @Test(dependsOnMethods = { "verifyEditButton" })
    public void verifySmsMarketingPreferences()
    {
        SmsMobileTextingMarketingPreferences smsMobileTextingMarketingPreferences = communicationPreferencesPage
                .getCommunicationPreferences().getSmsMobileTextingMarketingPreferences();
        Label smsNumberConfirmationStatus = smsMobileTextingMarketingPreferences.getSMSNumberConfirmationStatus();
        Button resendButton = smsMobileTextingMarketingPreferences.getResend();

        verifyThat(smsNumberConfirmationStatus, displayed(permissions.get(GUEST_MARKET_PREF_M)));
        verifyThat(resendButton, isDisplayedAndEnabled(permissions.get(GUEST_MARKET_PREF_M)));

        if (openEditMode())
        {
            verifyThat(smsMobileTextingMarketingPreferences.getSmsNumber(),
                    displayed(permissions.get(GUEST_MARKET_PREF_M)));

            verifyThat(resendButton, isDisplayedAndEnabled(permissions.get(GUEST_MARKET_PREF_M)));
            verifyThat(smsNumberConfirmationStatus, displayed(permissions.get(GUEST_MARKET_PREF_M)));
        }
    }

    @Test(dependsOnMethods = { "verifyEditButton" })
    public void verifyResentButton()
    {
        verifyThat(communicationPreferencesPage.getCommunicationPreferences().getSmsMobileTextingMarketingPreferences()
                .getResend(), isDisplayedAndEnabled(permissions.get(GUEST_MARKET_PREF_M)));
    }

    @Test(dependsOnMethods = { "verifyEditButton" })
    public void verifyEditCustomerService()
    {
        if (openEditMode())
        {
            CommunicationPreferences communicationPreferences = communicationPreferencesPage
                    .getCommunicationPreferences();

            CustomerService customerService = communicationPreferences.getCustomerService();
            verifyThat(customerService.getPhone().getPhoneCheckBox(),
                    isDisplayedAndEnabled(permissions.get(GUEST_SERVICE_PREF_M)));
            verifyThat(customerService.getEmail().getEmailCheckBox(),
                    isDisplayedAndEnabled(permissions.get(GUEST_SERVICE_PREF_M)));
            verifyThat(communicationPreferences.getComments(),
                    isDisplayedAndEnabled(permissions.get(GUEST_SERVICE_PREF_M)));

        }
    }

    private boolean openEditMode()
    {
        boolean isOpen = isEdit;

        if (isEdit)
        {
            communicationPreferencesPage.goTo();
            CommunicationPreferences communicationPreferences = communicationPreferencesPage
                    .getCommunicationPreferences();
            if (communicationPreferences.getEdit().isDisplayed(false))
            {
                communicationPreferences.clickEdit();
            }
        }

        return isOpen;
    }
}
