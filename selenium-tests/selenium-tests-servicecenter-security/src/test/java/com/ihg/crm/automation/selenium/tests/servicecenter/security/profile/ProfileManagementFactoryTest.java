package com.ihg.crm.automation.selenium.tests.servicecenter.security.profile;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Guest.GUEST_NEW_ACCOUNT_V;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Guest.GUEST_STATUS_V;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.GuestProfile.GUEST_CLOSED_CUSTOMER_V;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.GuestProfile.GUEST_CLOSED_FRAUD_V;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.GuestProfile.GUEST_CLOSED_IHG_V;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.GuestProfile.GUEST_OPEN_V;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.GuestProfile.GUEST_STATUS_REASON_V;

import java.util.Map;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.member.MemberJdbcUtils;
import com.ihg.automation.selenium.servicecenter.pages.account.AccountInfoPage;
import com.ihg.automation.selenium.servicecenter.pages.personal.PersonalInfoPage;
import com.ihg.crm.automation.selenium.tests.servicecenter.security.LoginLogout;

public class ProfileManagementFactoryTest extends LoginLogout
{
    private PersonalInfoPage personalInfoPage = new PersonalInfoPage();
    private AccountInfoPage accountInfoPage = new AccountInfoPage();
    private static final String MEMBER_ID_CLOSED = "SELECT m1.MBRSHP_ID FROM DGST.MBRSHP m1"//
            + " WHERE LYTY_PGM_CD = 'PC'"//
            + " AND CREAT_TS < SYSDATE AND MBRSHP_STAT_CD = 'C'"//
            + " AND MBRSHP_STAT_RSN_CD = '%s' AND NOT EXISTS"//
            + " (SELECT 1 FROM DGST.MBRSHP m2"//
            + " WHERE m1.DGST_MSTR_KEY = m2.DGST_MSTR_KEY"//
            + " AND m1.MBRSHP_KEY <> m2.MBRSHP_KEY)"//
            + " AND ROWNUM < 2";

    public ProfileManagementFactoryTest(String role, Map<String, Boolean> permissions)
    {
        super(role, permissions);
    }

    @BeforeClass
    public void beforeClass()
    {
        login();
    }

    @Test
    public void verifyAccessToOpenGuests()
    {
        searchNewMember(programsMemberIdWithOfferAndOrder);
        verifyThat(personalInfoPage, displayed(permissions.get(GUEST_OPEN_V)));
    }

    @Test
    public void verifyAccessToGuestClosedForFraud()
    {
        searchNewMember(MemberJdbcUtils.getMemberId(jdbcTemplate, String.format(MEMBER_ID_CLOSED, "F")));
        verifyThat(personalInfoPage, displayed(permissions.get(GUEST_CLOSED_FRAUD_V)));
    }

    @Test
    public void verifyAccessToGuestClosedByIHGRequest()
    {
        searchNewMember(MemberJdbcUtils.getMemberId(jdbcTemplate, String.format(MEMBER_ID_CLOSED, "I")));
        verifyThat(personalInfoPage, displayed(permissions.get(GUEST_CLOSED_IHG_V)));
    }

    @Test
    public void verifyAccessToGuestClosedByGuestRequest()
    {
        searchNewMember(MemberJdbcUtils.getMemberId(jdbcTemplate, String.format(MEMBER_ID_CLOSED, "H")));
        verifyThat(personalInfoPage, displayed(permissions.get(GUEST_CLOSED_CUSTOMER_V)));
    }

    @Test(dependsOnMethods = "verifyAccessToGuestClosedByGuestRequest")
    public void verifyStatusAndReasonView()
    {
        if (permissions.get(GUEST_NEW_ACCOUNT_V))
        {
            accountInfoPage.goTo();
            verifyThat(accountInfoPage.getAccountStatus(), displayed(permissions.get(GUEST_STATUS_V)));
            verifyThat(accountInfoPage.getStatusReason(), displayed(permissions.get(GUEST_STATUS_REASON_V)));
        }
    }
}
