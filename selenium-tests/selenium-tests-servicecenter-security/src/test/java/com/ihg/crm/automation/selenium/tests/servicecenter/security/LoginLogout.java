package com.ihg.crm.automation.selenium.tests.servicecenter.security;

import java.util.Map;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ContextConfiguration;

import com.ihg.automation.security.SecurityTestNGBase;
import com.ihg.automation.selenium.common.SiteHelperBase;
import com.ihg.automation.selenium.common.User;
import com.ihg.automation.selenium.servicecenter.pages.GuestSearch;
import com.ihg.automation.selenium.servicecenter.pages.LeftPanel;
import com.ihg.automation.selenium.servicecenter.pages.ServiceCenterHelper;

@ContextConfiguration(locations = { "classpath:app-context-security.xml" })
public class LoginLogout extends SecurityTestNGBase
{
    @Value("${loginUrl}")
    protected String loginUrl;

    @Resource
    protected Map<String, User> users;

    @Value("${programsMemberIdWithOfferAndOrder}")
    protected String programsMemberIdWithOfferAndOrder;

    @Value("${programsMemberIdWithORM}")
    protected String programsMemberIdWithORM;

    @Value("${hotelMemberId}")
    protected String hotelMemberId;

    @Value("${offerCodeForForceRegistration}")
    protected String offerCodeForForceRegistration;

    @Value("${memberIdWithFreenightStay}")
    protected String memberIdWithFreenightStay;

    @Value("${memberIdWithRewardnight}")
    protected String memberIdWithRewardnight;

    @Value("${businessMemberId}")
    protected String businessMemberId;

    @Resource
    protected JdbcTemplate jdbcTemplate;

    protected LoginLogout(String role, Map<String, Boolean> permissions)
    {
        super(role, permissions);
    }

    @Override
    protected SiteHelperBase getHelper()
    {
        return new ServiceCenterHelper(loginUrl);
    }

    @Override
    protected Map<String, User> getUsers()
    {
        return users;
    }

    protected void loginAndOpenMember(String memberNumber)
    {
        login();
        openMemberContext(memberNumber);
    }

    private void openMemberContext(String memberNumber)
    {
        new GuestSearch().byMemberNumber(memberNumber);
    }

    protected void searchNewMember(String memberNumber)
    {
        LeftPanel leftPanel = new LeftPanel();
        leftPanel.goToNewSearch();
        openMemberContext(memberNumber);
    }

}
