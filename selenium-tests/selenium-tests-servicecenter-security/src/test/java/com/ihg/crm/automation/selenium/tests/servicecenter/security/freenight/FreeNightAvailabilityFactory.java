package com.ihg.crm.automation.selenium.tests.servicecenter.security.freenight;

import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.FreeNight.FREE_NIGHT_A;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.FreeNight.FREE_NIGHT_GOODWILL_A;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.FreeNight.FREE_NIGHT_M;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.FreeNight.FREE_NIGHT_SERVICE_A;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.FreeNight.FREE_NIGHT_V;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.FreeNight.FREE_NIGHT_VOUCHER_CANCEL_M;

import java.util.HashMap;
import java.util.Map;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Factory;

import com.ihg.crm.automation.selenium.tests.servicecenter.security.SecurityMatrix;

public class FreeNightAvailabilityFactory
{
    @DataProvider(name = "verifyFreeNightAvailabilityProvider")
    public Object[][] verifyFreeNightAvailabilityProvider()
    {
        Map<String, Boolean> defaultPermissions = setPermissionMap();
        SecurityMatrix permissions = new SecurityMatrix(defaultPermissions);

        permissions.getAgentPermissions().put(FREE_NIGHT_A, false);
        permissions.getAgentPermissions().put(FREE_NIGHT_M, false);
        permissions.getAgentPermissions().put(FREE_NIGHT_GOODWILL_A, false);
        permissions.getAgentPermissions().put(FREE_NIGHT_SERVICE_A, false);
        permissions.getAgentPermissions().put(FREE_NIGHT_VOUCHER_CANCEL_M, false);

        permissions.getProgramSales().put(FREE_NIGHT_V, false);
        permissions.getProgramSales().put(FREE_NIGHT_A, false);
        permissions.getProgramSales().put(FREE_NIGHT_M, false);
        permissions.getProgramSales().put(FREE_NIGHT_GOODWILL_A, false);
        permissions.getProgramSales().put(FREE_NIGHT_SERVICE_A, false);
        permissions.getProgramSales().put(FREE_NIGHT_VOUCHER_CANCEL_M, false);

        permissions.getAgentTierLevel().put(FREE_NIGHT_SERVICE_A, false);

        permissions.getFraudTeam().put(FREE_NIGHT_V, false);
        permissions.getFraudTeam().put(FREE_NIGHT_A, false);
        permissions.getFraudTeam().put(FREE_NIGHT_M, false);
        permissions.getFraudTeam().put(FREE_NIGHT_GOODWILL_A, false);
        permissions.getFraudTeam().put(FREE_NIGHT_SERVICE_A, false);
        permissions.getFraudTeam().put(FREE_NIGHT_VOUCHER_CANCEL_M, false);

        permissions.getAgentReadOnly().put(FREE_NIGHT_V, false);
        permissions.getAgentReadOnly().put(FREE_NIGHT_A, false);
        permissions.getAgentReadOnly().put(FREE_NIGHT_M, false);
        permissions.getAgentReadOnly().put(FREE_NIGHT_GOODWILL_A, false);
        permissions.getAgentReadOnly().put(FREE_NIGHT_SERVICE_A, false);
        permissions.getAgentReadOnly().put(FREE_NIGHT_VOUCHER_CANCEL_M, false);

        permissions.getManagerMarketing().put(FREE_NIGHT_V, false);
        permissions.getManagerMarketing().put(FREE_NIGHT_A, false);
        permissions.getManagerMarketing().put(FREE_NIGHT_M, false);
        permissions.getManagerMarketing().put(FREE_NIGHT_GOODWILL_A, false);
        permissions.getManagerMarketing().put(FREE_NIGHT_SERVICE_A, false);
        permissions.getManagerMarketing().put(FREE_NIGHT_VOUCHER_CANCEL_M, false);

        permissions.getReadOnlyMarketing().put(FREE_NIGHT_V, false);
        permissions.getReadOnlyMarketing().put(FREE_NIGHT_A, false);
        permissions.getReadOnlyMarketing().put(FREE_NIGHT_M, false);
        permissions.getReadOnlyMarketing().put(FREE_NIGHT_GOODWILL_A, false);
        permissions.getReadOnlyMarketing().put(FREE_NIGHT_SERVICE_A, false);
        permissions.getReadOnlyMarketing().put(FREE_NIGHT_VOUCHER_CANCEL_M, false);

        permissions.getReadOnlyVendor().put(FREE_NIGHT_V, false);
        permissions.getReadOnlyVendor().put(FREE_NIGHT_A, false);
        permissions.getReadOnlyVendor().put(FREE_NIGHT_M, false);
        permissions.getReadOnlyVendor().put(FREE_NIGHT_GOODWILL_A, false);
        permissions.getReadOnlyVendor().put(FREE_NIGHT_SERVICE_A, false);
        permissions.getReadOnlyVendor().put(FREE_NIGHT_VOUCHER_CANCEL_M, false);

        permissions.getPartnerVendor().put(FREE_NIGHT_V, false);
        permissions.getPartnerVendor().put(FREE_NIGHT_A, false);
        permissions.getPartnerVendor().put(FREE_NIGHT_M, false);
        permissions.getPartnerVendor().put(FREE_NIGHT_GOODWILL_A, false);
        permissions.getPartnerVendor().put(FREE_NIGHT_SERVICE_A, false);
        permissions.getPartnerVendor().put(FREE_NIGHT_VOUCHER_CANCEL_M, false);

        permissions.getHumanResources().put(FREE_NIGHT_V, false);
        permissions.getHumanResources().put(FREE_NIGHT_A, false);
        permissions.getHumanResources().put(FREE_NIGHT_M, false);
        permissions.getHumanResources().put(FREE_NIGHT_GOODWILL_A, false);
        permissions.getHumanResources().put(FREE_NIGHT_SERVICE_A, false);
        permissions.getHumanResources().put(FREE_NIGHT_VOUCHER_CANCEL_M, false);

        return permissions.getPermissionArray();
    }

    private Map<String, Boolean> setPermissionMap()
    {
        Map<String, Boolean> permissions = new HashMap<String, Boolean>();

        permissions.put(FREE_NIGHT_V, true);
        permissions.put(FREE_NIGHT_A, true);
        permissions.put(FREE_NIGHT_M, true);
        permissions.put(FREE_NIGHT_GOODWILL_A, true);
        permissions.put(FREE_NIGHT_SERVICE_A, true);
        permissions.put(FREE_NIGHT_VOUCHER_CANCEL_M, true);

        return permissions;
    }

    @Factory(dataProvider = "verifyFreeNightAvailabilityProvider")
    public Object[] createInstances(String role, Map<String, Boolean> permissions)
    {
        return new Object[] { new FreeNightAvailabilityFactoryTest(role, permissions) };
    }
}
