package com.ihg.crm.automation.selenium.tests.servicecenter.security.voucher;

import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Voucher.DEPOSITS_V;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Voucher.VOUCHER_UNVOID_M;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Voucher.VOUCHER_V;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Voucher.VOUCHER_VOID_M;

import java.util.HashMap;
import java.util.Map;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Factory;

import com.ihg.crm.automation.selenium.tests.servicecenter.security.SecurityMatrix;

public class VoucherAvailabilityFactory
{
    @DataProvider(name = "verifyVoucherAvailabilityProvider")
    public Object[][] verifyVoucherAvailabilityProvider()
    {
        Map<String, Boolean> defaultPermissions = setPermissionMap();

        SecurityMatrix permissions = new SecurityMatrix(defaultPermissions);

        permissions.getAgentPermissions().put(VOUCHER_V, true);
        permissions.getAgentPermissions().put(DEPOSITS_V, true);

        permissions.getAgentTierLevel().put(VOUCHER_V, true);
        permissions.getAgentTierLevel().put(DEPOSITS_V, true);

        permissions.getHotelHelpDesk().put(VOUCHER_V, true);
        permissions.getHotelHelpDesk().put(DEPOSITS_V, true);

        permissions.getAdminAwardSupport().put(VOUCHER_V, true);
        permissions.getAdminAwardSupport().put(DEPOSITS_V, true);

        permissions.getAdminOperationsManager().put(VOUCHER_V, true);
        permissions.getAdminOperationsManager().put(VOUCHER_VOID_M, true);
        permissions.getAdminOperationsManager().put(VOUCHER_UNVOID_M, true);
        permissions.getAdminOperationsManager().put(DEPOSITS_V, true);

        permissions.getAdminCentralProcessing().put(VOUCHER_V, true);
        permissions.getAdminCentralProcessing().put(VOUCHER_VOID_M, true);
        permissions.getAdminCentralProcessing().put(VOUCHER_UNVOID_M, true);
        permissions.getAdminCentralProcessing().put(DEPOSITS_V, true);

        permissions.getFraudTeam().put(VOUCHER_V, true);
        permissions.getFraudTeam().put(VOUCHER_VOID_M, true);
        permissions.getFraudTeam().put(VOUCHER_UNVOID_M, true);
        permissions.getFraudTeam().put(DEPOSITS_V, true);

        permissions.getSystemsAdministration().put(VOUCHER_V, true);
        permissions.getSystemsAdministration().put(VOUCHER_VOID_M, true);
        permissions.getSystemsAdministration().put(VOUCHER_UNVOID_M, true);
        permissions.getSystemsAdministration().put(DEPOSITS_V, true);

        permissions.getSystemsManager().put(VOUCHER_V, true);
        permissions.getSystemsManager().put(VOUCHER_VOID_M, true);
        permissions.getSystemsManager().put(VOUCHER_UNVOID_M, true);
        permissions.getSystemsManager().put(DEPOSITS_V, true);

        permissions.getSystemsCoordinator().put(VOUCHER_V, true);
        permissions.getSystemsCoordinator().put(VOUCHER_VOID_M, true);
        permissions.getSystemsCoordinator().put(VOUCHER_UNVOID_M, true);
        permissions.getSystemsCoordinator().put(DEPOSITS_V, true);

        return permissions.getPermissionArray();
    }

    private Map<String, Boolean> setPermissionMap()
    {
        Map<String, Boolean> permissions = new HashMap<String, Boolean>();

        permissions.put(VOUCHER_V, false);
        permissions.put(VOUCHER_VOID_M, false);
        permissions.put(VOUCHER_UNVOID_M, false);
        permissions.put(DEPOSITS_V, false);

        return permissions;
    }

    @Factory(dataProvider = "verifyVoucherAvailabilityProvider")
    public Object[] createInstances(String role, Map<String, Boolean> permissions)
    {
        return new Object[] { new VoucherAvailabilityFactoryTest(role, permissions) };
    }

}
