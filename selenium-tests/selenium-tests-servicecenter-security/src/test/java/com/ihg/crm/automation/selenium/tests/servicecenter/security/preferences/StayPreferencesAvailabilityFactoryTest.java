package com.ihg.crm.automation.selenium.tests.servicecenter.security.preferences;

import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyNoError;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.assertThat;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.isDisplayedAndEnabled;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.StayPreferences.STAY_PREFERENCES_M;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.StayPreferences.STAY_PREFERENCES_V;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.TravelProfile.TRAVEL_PROFILE_M;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.TravelProfile.TRAVEL_PROFILE_V;

import java.util.Map;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.servicecenter.pages.Tabs;
import com.ihg.automation.selenium.servicecenter.pages.preferences.AddTravelProfilePopUp;
import com.ihg.automation.selenium.servicecenter.pages.preferences.EditTravelProfilePopUp;
import com.ihg.automation.selenium.servicecenter.pages.preferences.StayPreferencesPage;
import com.ihg.automation.selenium.servicecenter.pages.preferences.TravelProfileGridRow;
import com.ihg.crm.automation.selenium.tests.servicecenter.security.LoginLogout;

public class StayPreferencesAvailabilityFactoryTest extends LoginLogout
{
    private StayPreferencesPage stayPreferencesPage = new StayPreferencesPage();
    private Tabs.CustomerInfoTab customerInfoTab = new Tabs().getCustomerInfo();

    public StayPreferencesAvailabilityFactoryTest(String role, Map<String, Boolean> permissions)
    {
        super(role, permissions);
    }

    @BeforeClass
    public void beforeClass()
    {
        loginAndOpenMember(programsMemberIdWithOfferAndOrder);
    }

    @Test
    public void verifyStayPreferencesPage()
    {
        verifyThat(customerInfoTab.getStayPreferences(), displayed(permissions.get(STAY_PREFERENCES_V)));
    }

    @Test(dependsOnMethods = { "verifyStayPreferencesPage" }, alwaysRun = true)
    public void verifyStayPreferencesEditMode()
    {
        if (permissions.get(STAY_PREFERENCES_V))
        {
            stayPreferencesPage.goTo();
            assertThat(stayPreferencesPage.getEdit(), displayed(permissions.get(STAY_PREFERENCES_M)));

            if (permissions.get(STAY_PREFERENCES_M))
            {
                stayPreferencesPage.getEdit().clickAndWait(verifyNoError());
                verifyThat(stayPreferencesPage.getReset(), displayed(true));
                verifyThat(stayPreferencesPage.getCancel(), displayed(true));
                verifyThat(stayPreferencesPage.getSave(), displayed(true));
                stayPreferencesPage.getCancel().clickAndWait(verifyNoError());
            }
        }
    }

    @Test(dependsOnMethods = { "verifyStayPreferencesEditMode" }, alwaysRun = true)
    public void verifyTravelProfile()
    {
        if (permissions.get(STAY_PREFERENCES_V))
        {
            stayPreferencesPage.goTo();
            verifyThat(stayPreferencesPage.getTravelProfiles(), displayed(permissions.get(TRAVEL_PROFILE_V)));
        }
    }

    @Test(dependsOnMethods = { "verifyTravelProfile" }, alwaysRun = true)
    public void verifyAddTravelProfile()
    {
        if (permissions.get(TRAVEL_PROFILE_V))
        {
            stayPreferencesPage.goTo();
            assertThat(stayPreferencesPage.getAddTravelProfile(),
                    isDisplayedAndEnabled(permissions.get(TRAVEL_PROFILE_M)));
            if (permissions.get(TRAVEL_PROFILE_M))
            {
                stayPreferencesPage.getAddTravelProfile().clickAndWait();
                AddTravelProfilePopUp addTravelProfilePopUp = new AddTravelProfilePopUp();
                verifyThat(addTravelProfilePopUp.getSave(), isDisplayedAndEnabled(true));
                addTravelProfilePopUp.clickClose(verifyNoError());
            }
        }
    }

    @Test(dependsOnMethods = { "verifyAddTravelProfile" }, alwaysRun = true)
    public void verifyEditTravelProfile()
    {
        if (permissions.get(TRAVEL_PROFILE_M))
        {
            stayPreferencesPage.goTo();
            TravelProfileGridRow row = stayPreferencesPage.getTravelProfilesGrid().getRow(1);
            verifyThat(row.getCell(TravelProfileGridRow.TravelProfileCell.PROFILE_NAME).asLink(), displayed(true));
            EditTravelProfilePopUp editTravelProfilePopUp = row.openTravelProfile();
            verifyThat(editTravelProfilePopUp.getSave(), isDisplayedAndEnabled(true));
            editTravelProfilePopUp.clickClose(verifyNoError());
        }
    }
}
