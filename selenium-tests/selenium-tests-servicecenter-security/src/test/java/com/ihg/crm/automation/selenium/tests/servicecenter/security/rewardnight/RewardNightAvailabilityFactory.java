package com.ihg.crm.automation.selenium.tests.servicecenter.security.rewardnight;

import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.RewardNight.REWARD_NIGHT_A;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.RewardNight.REWARD_NIGHT_M;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.RewardNight.REWARD_NIGHT_V;

import java.util.HashMap;
import java.util.Map;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Factory;

import com.ihg.crm.automation.selenium.tests.servicecenter.security.SecurityMatrix;

public class RewardNightAvailabilityFactory
{
    @DataProvider(name = "verifyRewardNightAvailabilityProvider")
    public Object[][] verifyRewardNightAvailabilityProvider()
    {
        Map<String, Boolean> defaultPermissions = setPermissionMap();
        SecurityMatrix permissions = new SecurityMatrix(defaultPermissions);

        permissions.getAgentPermissions().put(REWARD_NIGHT_A, false);
        permissions.getAgentPermissions().put(REWARD_NIGHT_M, false);

        permissions.getProgramSales().put(REWARD_NIGHT_V, false);
        permissions.getProgramSales().put(REWARD_NIGHT_A, false);
        permissions.getProgramSales().put(REWARD_NIGHT_M, false);

        permissions.getFraudTeam().put(REWARD_NIGHT_A, false);
        permissions.getFraudTeam().put(REWARD_NIGHT_M, false);

        permissions.getAgentReadOnly().put(REWARD_NIGHT_V, false);
        permissions.getAgentReadOnly().put(REWARD_NIGHT_A, false);
        permissions.getAgentReadOnly().put(REWARD_NIGHT_M, false);

        permissions.getManagerMarketing().put(REWARD_NIGHT_V, false);
        permissions.getManagerMarketing().put(REWARD_NIGHT_A, false);
        permissions.getManagerMarketing().put(REWARD_NIGHT_M, false);

        permissions.getReadOnlyMarketing().put(REWARD_NIGHT_V, false);
        permissions.getReadOnlyMarketing().put(REWARD_NIGHT_A, false);
        permissions.getReadOnlyMarketing().put(REWARD_NIGHT_M, false);

        permissions.getReadOnlyVendor().put(REWARD_NIGHT_V, false);
        permissions.getReadOnlyVendor().put(REWARD_NIGHT_A, false);
        permissions.getReadOnlyVendor().put(REWARD_NIGHT_M, false);

        permissions.getPartnerVendor().put(REWARD_NIGHT_V, false);
        permissions.getPartnerVendor().put(REWARD_NIGHT_A, false);
        permissions.getPartnerVendor().put(REWARD_NIGHT_M, false);

        permissions.getHumanResources().put(REWARD_NIGHT_V, false);
        permissions.getHumanResources().put(REWARD_NIGHT_A, false);
        permissions.getHumanResources().put(REWARD_NIGHT_M, false);

        return permissions.getPermissionArray();
    }

    private Map<String, Boolean> setPermissionMap()
    {
        Map<String, Boolean> permissions = new HashMap<String, Boolean>();

        permissions.put(REWARD_NIGHT_V, true);
        permissions.put(REWARD_NIGHT_A, true);
        permissions.put(REWARD_NIGHT_M, true);

        return permissions;
    }

    @Factory(dataProvider = "verifyRewardNightAvailabilityProvider")
    public Object[] createInstances(String role, Map<String, Boolean> permissions)
    {
        return new Object[] { new RewardNightAvailabilityFactoryTest(role, permissions) };
    }
}
