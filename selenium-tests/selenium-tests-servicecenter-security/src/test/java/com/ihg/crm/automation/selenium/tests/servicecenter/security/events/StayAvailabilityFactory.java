package com.ihg.crm.automation.selenium.tests.servicecenter.security.events;

import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Stay.STAY_A;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Stay.STAY_ADJUST_M;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Stay.STAY_ASSIGN_M;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Stay.STAY_D;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Stay.STAY_HISTORY_V;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Stay.STAY_V;

import java.util.HashMap;
import java.util.Map;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Factory;

import com.ihg.crm.automation.selenium.tests.servicecenter.security.SecurityMatrix;

public class StayAvailabilityFactory
{
    @DataProvider(name = "verifyStayAvailabilityProvider")
    public Object[][] verifyStayAvailabilityProvider()
    {
        Map<String, Boolean> defaultPermissions = setPermissionMap();
        SecurityMatrix permissions = new SecurityMatrix(defaultPermissions);

        permissions.getProgramSales().put(STAY_ADJUST_M, false);
        permissions.getProgramSales().put(STAY_ASSIGN_M, false);
        permissions.getProgramSales().put(STAY_A, false);
        permissions.getProgramSales().put(STAY_D, false);

        permissions.getAgentReadOnly().put(STAY_V, false);
        permissions.getAgentReadOnly().put(STAY_ADJUST_M, false);
        permissions.getAgentReadOnly().put(STAY_ASSIGN_M, false);
        permissions.getAgentReadOnly().put(STAY_A, false);
        permissions.getAgentReadOnly().put(STAY_D, false);
        permissions.getAgentReadOnly().put(STAY_HISTORY_V, false);

        permissions.getManagerMarketing().put(STAY_ADJUST_M, false);
        permissions.getManagerMarketing().put(STAY_ASSIGN_M, false);
        permissions.getManagerMarketing().put(STAY_A, false);
        permissions.getManagerMarketing().put(STAY_D, false);

        permissions.getReadOnlyMarketing().put(STAY_ADJUST_M, false);
        permissions.getReadOnlyMarketing().put(STAY_ASSIGN_M, false);
        permissions.getReadOnlyMarketing().put(STAY_A, false);
        permissions.getReadOnlyMarketing().put(STAY_D, false);

        permissions.getReadOnlyVendor().put(STAY_ADJUST_M, false);
        permissions.getReadOnlyVendor().put(STAY_ASSIGN_M, false);
        permissions.getReadOnlyVendor().put(STAY_A, false);
        permissions.getReadOnlyVendor().put(STAY_D, false);

        permissions.getPartnerVendor().put(STAY_ADJUST_M, false);
        permissions.getPartnerVendor().put(STAY_ASSIGN_M, false);
        permissions.getPartnerVendor().put(STAY_A, false);
        permissions.getPartnerVendor().put(STAY_D, false);

        permissions.getHumanResources().put(STAY_ADJUST_M, false);
        permissions.getHumanResources().put(STAY_ASSIGN_M, false);
        permissions.getHumanResources().put(STAY_A, false);
        permissions.getHumanResources().put(STAY_D, false);

        return permissions.getPermissionArray();
    }

    private Map<String, Boolean> setPermissionMap()
    {
        Map<String, Boolean> permissions = new HashMap<String, Boolean>();

        permissions.put(STAY_V, true);
        permissions.put(STAY_ADJUST_M, true);
        permissions.put(STAY_ASSIGN_M, true);
        permissions.put(STAY_A, true);
        permissions.put(STAY_D, true);
        permissions.put(STAY_HISTORY_V, true);

        return permissions;
    }

    @Factory(dataProvider = "verifyStayAvailabilityProvider")
    public Object[] createInstances(String role, Map<String, Boolean> permissions)
    {
        return new Object[] { new StayAvailabilityFactoryTest(role, permissions) };
    }
}
