package com.ihg.crm.automation.selenium.tests.servicecenter.security.catalog;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.HotelOperations.CATALOG_V;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.HotelOperations.SHOPPING_CART_M;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.HotelOperations.SHOPPING_CART_V;

import java.util.Map;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.contact.Address;
import com.ihg.automation.selenium.gwt.components.Menu;
import com.ihg.automation.selenium.servicecenter.pages.TopMenu;
import com.ihg.automation.selenium.servicecenter.pages.catalog.CatalogItemDetailsPopUp;
import com.ihg.automation.selenium.servicecenter.pages.catalog.CatalogPage;
import com.ihg.automation.selenium.servicecenter.pages.order.ItemRow;
import com.ihg.automation.selenium.servicecenter.pages.order.OrderSummaryPopUp;
import com.ihg.crm.automation.selenium.tests.servicecenter.security.LoginLogout;

public class CatalogAvailabilityFactoryTest extends LoginLogout
{
    private TopMenu topMenu = new TopMenu();

    private CatalogPage catalogPage = new CatalogPage();
    private final static String ITEM_ID = "GF300";
    private CatalogItemDetailsPopUp catalogItemDetailsPopUp;
    private Menu catalog;

    public CatalogAvailabilityFactoryTest(String role, Map<String, Boolean> permissions)
    {
        super(role, permissions);
    }

    @BeforeClass
    public void beforeClass()
    {
        loginAndOpenMember(programsMemberIdWithOfferAndOrder);
    }

    @Test
    public void verifyMemberContextCatalogItemDetailsAvailability()
    {
        catalog = topMenu.getCatalog();
        verifyThat(catalog, displayed(permissions.get(CATALOG_V)));

        if (permissions.get(CATALOG_V))
        {
            catalogPage.goTo();
            catalogPage.getButtonBar().clickSearch();

            catalogItemDetailsPopUp = catalogPage.openCatalogItemDetailsPopUp();
            catalogItemDetailsPopUp.close();
        }
    }

    @Test
    public void verifyViewShoppingCart()
    {
        if (permissions.get(CATALOG_V))
        {
            catalogPage.goTo();
            catalogPage.getButtonBar().clickSearch();

            catalogItemDetailsPopUp = catalogPage.openCatalogItemDetailsPopUp();
            verifyThat(catalogItemDetailsPopUp.getAddToCart(), displayed(permissions.get(SHOPPING_CART_V)));
            if (permissions.get(SHOPPING_CART_V))
            {
                OrderSummaryPopUp orderSummaryPopUp = catalogItemDetailsPopUp.openOrderSummaryPopUp();
                orderSummaryPopUp.clickContinueShopping();
            }
        }
    }

    @Test
    public void verifyManageCatalogItemInCart()
    {
        if (permissions.get(CATALOG_V))
        {
            catalogPage.goTo();
            catalogPage.searchByItemId(ITEM_ID);

            catalogItemDetailsPopUp = catalogPage.openCatalogItemDetailsPopUp();
            if (permissions.get(SHOPPING_CART_V))
            {
                OrderSummaryPopUp orderSummaryPopUp = catalogItemDetailsPopUp.openOrderSummaryPopUp();

                ItemRow itemRow = orderSummaryPopUp.getRow(1);
                verifyThat(itemRow.getQuantity(), displayed(permissions.get(SHOPPING_CART_M)));
                verifyThat(itemRow.getItemDescription().getColor(), displayed(permissions.get(SHOPPING_CART_M)));
                verifyThat(orderSummaryPopUp.getName().getGivenName(), displayed(permissions.get(SHOPPING_CART_M)));

                Address address = orderSummaryPopUp.getAddress();
                verifyThat(address.getCountry(), displayed(permissions.get(SHOPPING_CART_M)));
                verifyThat(address.getType(), displayed(permissions.get(SHOPPING_CART_M)));

                verifyThat(orderSummaryPopUp.getRemove(), displayed(permissions.get(SHOPPING_CART_M)));
                verifyThat(orderSummaryPopUp.getCheckout(), displayed(permissions.get(SHOPPING_CART_M)));

                orderSummaryPopUp.clickContinueShopping();
            }
        }
    }
}
