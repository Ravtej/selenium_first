package com.ihg.crm.automation.selenium.tests.servicecenter.security.customer;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.isDisplayedAndEnabled;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Guest.ACCOUNT_REMOVE_M;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Guest.GUEST_ACCOUNT_HISTORY_V;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Guest.GUEST_FLAG_M;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Guest.GUEST_FLAG_V;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Guest.GUEST_NEW_ACCOUNT_V;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Guest.GUEST_STATUS_V;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Guest.PIN_M;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Guest.PIN_V;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Guest.SECURITY_M;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Guest.SECURITY_V;

import java.util.Map;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.servicecenter.pages.Tabs;
import com.ihg.automation.selenium.servicecenter.pages.account.AccountInfoPage;
import com.ihg.automation.selenium.servicecenter.pages.account.Security;
import com.ihg.crm.automation.selenium.tests.servicecenter.security.LoginLogout;

public class CustomerAccountHistoryGuestRecordFactoryTest extends LoginLogout
{
    private Tabs.CustomerInfoTab customerInfoTab = new Tabs().getCustomerInfo();
    private AccountInfoPage accountInfoPage = new AccountInfoPage();

    public CustomerAccountHistoryGuestRecordFactoryTest(String role, Map<String, Boolean> permissions)
    {
        super(role, permissions);
    }

    @BeforeClass
    private void beforeClass()
    {
        loginAndOpenMember(programsMemberIdWithOfferAndOrder);
        customerInfoTab.goTo();
    }

    @Test
    public void verifyViewAccountInformation()
    {
        verifyThat(customerInfoTab.getAccountInfo(), displayed(permissions.get(GUEST_NEW_ACCOUNT_V)));
    }

    @Test(dependsOnMethods = { "verifyViewAccountInformation" })
    public void verifyAccountDetails()
    {
        if (permissions.get(GUEST_NEW_ACCOUNT_V))
        {
            accountInfoPage.goTo();
            verifyThat(accountInfoPage.getAccountStatus(), displayed(permissions.get(GUEST_STATUS_V)));
        }
    }

    @Test(dependsOnMethods = { "verifyViewAccountInformation" })
    public void verifyRemoveAccount()
    {
        if (permissions.get(GUEST_NEW_ACCOUNT_V))
        {
            accountInfoPage.goTo();
            verifyThat(accountInfoPage.getRemoveAccount(), isDisplayedAndEnabled(permissions.get(ACCOUNT_REMOVE_M)));
        }
    }

    @Test(dependsOnMethods = { "verifyViewAccountInformation" })
    public void verifySecurity()
    {
        if (permissions.get(GUEST_NEW_ACCOUNT_V))
        {
            accountInfoPage.goTo();
            verifyThat(accountInfoPage.getSecurity(), displayed(permissions.get(SECURITY_V)));
        }
    }

    @Test(dependsOnMethods = { "verifyViewAccountInformation" })
    public void verifyEditSecurity()
    {
        if (permissions.get(GUEST_NEW_ACCOUNT_V))
        {
            accountInfoPage.goTo();
            verifyThat(accountInfoPage.getSecurity().getEdit(), isDisplayedAndEnabled(permissions.get(SECURITY_M)));
        }
    }

    @Test(dependsOnMethods = { "verifyViewAccountInformation" })
    public void verifyPinRemoveButton()
    {
        if (permissions.get(SECURITY_M))
        {
            accountInfoPage.goTo();
            Security security = accountInfoPage.getSecurity();
            security.clickEdit();
            verifyThat(security.getRemovePin(), displayed(permissions.get(PIN_V)));
            security.clickCancel();
        }
    }

    @Test(dependsOnMethods = { "verifyViewAccountInformation" })
    public void verifyPinClearLink()
    {
        if (permissions.get(SECURITY_M))
        {
            accountInfoPage.goTo();
            Security security = accountInfoPage.getSecurity();
            security.clickEdit();
            verifyThat(security.getClear(), displayed(permissions.get(PIN_M)));
            security.clickCancel();
        }
    }

    @Test(dependsOnMethods = { "verifyViewAccountInformation" })
    public void verifyProfileFlags()
    {
        if (permissions.get(GUEST_NEW_ACCOUNT_V))
        {
            accountInfoPage.goTo();
            verifyThat(accountInfoPage.getFlags(), displayed(permissions.get(GUEST_FLAG_V)));
        }
    }

    @Test(dependsOnMethods = { "verifyViewAccountInformation" })
    public void verifyEditProfileFlags()
    {
        if (permissions.get(GUEST_NEW_ACCOUNT_V))
        {
            accountInfoPage.goTo();
            verifyThat(accountInfoPage.getFlags().getEdit(), isDisplayedAndEnabled(permissions.get(GUEST_FLAG_M)));
        }
    }

    @Test(dependsOnMethods = { "verifyViewAccountInformation" })
    public void verifyGuestRecordDataTab()
    {
        if (permissions.get(GUEST_NEW_ACCOUNT_V))
        {
            customerInfoTab.goTo();
            verifyThat(customerInfoTab.getGuestRecordData(), displayed(false));
        }
    }

    @Test(dependsOnMethods = { "verifyViewAccountInformation" })
    public void verifyHistoryTab()
    {
        if (permissions.get(GUEST_NEW_ACCOUNT_V))
        {
            customerInfoTab.goTo();
            verifyThat(customerInfoTab.getProfileHistory(), displayed(permissions.get(GUEST_ACCOUNT_HISTORY_V)));
        }
    }
}
