package com.ihg.crm.automation.selenium.tests.servicecenter.security.program;

import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Program.ALLIANCE_A;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Program.ALLIANCE_D;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Program.ALLIANCE_M;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Program.ALTERNATE_ID_D;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Program.ALTERNATE_ID_M;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Program.AMB_SUMMARY_M;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Program.AMB_TAB_V;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Program.ANNUAL_ACTIVITY_V;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Program.BR_SUMMARY_M;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Program.DECLINE_TSCS;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Program.DINING_V;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Program.DR_SUMMARY_M;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Program.DR_TAB_V;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Program.EARNING_PREFS_M;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Program.EMPLOYEE_RATE_ELIGIBILITY_M;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Program.EMP_SUMMARY_M;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Program.EMP_TAB_V;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Program.ENROLLMENT_RENEW;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Program.EXTEND_MEMBERSHIP_AMB;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Program.EXTEND_MEMBERSHIP_DR;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Program.HTL_SUMMARY_M;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Program.HTL_TAB_V;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Program.IHG_SUMMARY_M;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Program.IHG_TAB_V;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Program.KAR_SUMMARY_M;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Program.KAR_TAB_V;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Program.MTGPL_SUMMARY_M;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Program.MTGPL_TAB_V;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Program.PC_PURCHASE_LEVEL;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Program.PC_SUMMARY_M;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Program.PC_TAB_V;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Program.PROGRAM_MEMBER_POINT_BALANCE_V;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Program.RC_SUMMARY_M;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Program.TIER_LEVEL_ACTV_AMB_V;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Program.TIER_LEVEL_AMB_M;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Program.TL_BENEFIT_M;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Program.TRAVEL_PROFILE_AMB_M;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Program.TRAVEL_PROFILE_M;
import static com.ihg.crm.automation.selenium.tests.servicecenter.security.PermissionConstant.Program.TRAVEL_PROFILE_V;

import java.util.HashMap;
import java.util.Map;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Factory;

import com.ihg.crm.automation.selenium.tests.servicecenter.security.SecurityMatrix;

public class ProgramAvailabilityFactory
{
    @DataProvider(name = "verifyProgramAvailabilityProvider")
    public Object[][] verifyProgramAvailabilityProvider()
    {
        Map<String, Boolean> defaultPermissions = setPermissionMap();
        SecurityMatrix permissions = new SecurityMatrix(defaultPermissions);

        permissions.getAgentPermissions().put(PC_PURCHASE_LEVEL, false);
        permissions.getAgentPermissions().put(ENROLLMENT_RENEW, false);
        permissions.getAgentPermissions().put(EXTEND_MEMBERSHIP_AMB, false);
        permissions.getAgentPermissions().put(EXTEND_MEMBERSHIP_DR, false);
        permissions.getAgentPermissions().put(DECLINE_TSCS, false);
        permissions.getAgentPermissions().put(TIER_LEVEL_AMB_M, false);
        permissions.getAgentPermissions().put(RC_SUMMARY_M, false);

        permissions.getProgramSales().put(PC_SUMMARY_M, false);
        permissions.getProgramSales().put(PC_PURCHASE_LEVEL, false);
        permissions.getProgramSales().put(ALLIANCE_M, false);
        permissions.getProgramSales().put(ALLIANCE_D, false);
        permissions.getProgramSales().put(TRAVEL_PROFILE_M, false);
        permissions.getProgramSales().put(IHG_SUMMARY_M, false);
        permissions.getProgramSales().put(EXTEND_MEMBERSHIP_AMB, false);
        permissions.getProgramSales().put(EXTEND_MEMBERSHIP_DR, false);
        permissions.getProgramSales().put(DECLINE_TSCS, false);
        permissions.getProgramSales().put(TIER_LEVEL_AMB_M, false);
        permissions.getProgramSales().put(TRAVEL_PROFILE_AMB_M, false);
        permissions.getProgramSales().put(EMP_TAB_V, false);
        permissions.getProgramSales().put(AMB_SUMMARY_M, false);
        permissions.getProgramSales().put(DR_SUMMARY_M, false);
        permissions.getProgramSales().put(BR_SUMMARY_M, false);
        permissions.getProgramSales().put(EMP_SUMMARY_M, false);
        permissions.getProgramSales().put(HTL_SUMMARY_M, false);
        permissions.getProgramSales().put(RC_SUMMARY_M, false);
        permissions.getProgramSales().put(MTGPL_SUMMARY_M, false);
        permissions.getProgramSales().put(KAR_SUMMARY_M, false);

        permissions.getAgentTierLevel().put(RC_SUMMARY_M, false);
        permissions.getAgentTierLevel().put(PC_PURCHASE_LEVEL, false);

        permissions.getHotelHelpDesk().put(ENROLLMENT_RENEW, false);
        permissions.getHotelHelpDesk().put(EXTEND_MEMBERSHIP_AMB, false);
        permissions.getHotelHelpDesk().put(EXTEND_MEMBERSHIP_DR, false);
        permissions.getHotelHelpDesk().put(DECLINE_TSCS, false);
        permissions.getHotelHelpDesk().put(TIER_LEVEL_AMB_M, false);
        permissions.getHotelHelpDesk().put(TRAVEL_PROFILE_AMB_M, false);
        permissions.getHotelHelpDesk().put(RC_SUMMARY_M, false);

        permissions.getAdminAwardSupport().put(ENROLLMENT_RENEW, false);
        permissions.getAdminAwardSupport().put(EXTEND_MEMBERSHIP_AMB, false);
        permissions.getAdminAwardSupport().put(EXTEND_MEMBERSHIP_DR, false);
        permissions.getAdminAwardSupport().put(DECLINE_TSCS, false);
        permissions.getAdminAwardSupport().put(TIER_LEVEL_AMB_M, false);
        permissions.getAdminAwardSupport().put(TRAVEL_PROFILE_AMB_M, false);
        permissions.getAdminAwardSupport().put(RC_SUMMARY_M, false);
        permissions.getAdminAwardSupport().put(TIER_LEVEL_ACTV_AMB_V, true);

        permissions.getAdminOperationsManager().put(RC_SUMMARY_M, false);

        permissions.getAdminCentralProcessing().put(ENROLLMENT_RENEW, false);
        permissions.getAdminCentralProcessing().put(EXTEND_MEMBERSHIP_AMB, false);
        permissions.getAdminCentralProcessing().put(EXTEND_MEMBERSHIP_DR, false);
        permissions.getAdminCentralProcessing().put(DECLINE_TSCS, false);
        permissions.getAdminCentralProcessing().put(TIER_LEVEL_AMB_M, false);
        permissions.getAdminCentralProcessing().put(TRAVEL_PROFILE_AMB_M, false);
        permissions.getAdminCentralProcessing().put(RC_SUMMARY_M, false);

        permissions.getFraudTeam().put(PC_PURCHASE_LEVEL, false);
        permissions.getFraudTeam().put(ALLIANCE_A, false);
        permissions.getFraudTeam().put(ALLIANCE_M, false);
        permissions.getFraudTeam().put(ALLIANCE_D, false);
        permissions.getFraudTeam().put(IHG_SUMMARY_M, false);
        permissions.getFraudTeam().put(ENROLLMENT_RENEW, false);
        permissions.getFraudTeam().put(EXTEND_MEMBERSHIP_AMB, false);
        permissions.getFraudTeam().put(EXTEND_MEMBERSHIP_DR, false);
        permissions.getFraudTeam().put(DECLINE_TSCS, false);
        permissions.getFraudTeam().put(TIER_LEVEL_AMB_M, false);
        permissions.getFraudTeam().put(TRAVEL_PROFILE_AMB_M, false);
        permissions.getFraudTeam().put(RC_SUMMARY_M, false);

        permissions.getAgentReadOnly().put(PC_SUMMARY_M, false);
        permissions.getAgentReadOnly().put(PC_PURCHASE_LEVEL, false);
        permissions.getAgentReadOnly().put(ALLIANCE_A, false);
        permissions.getAgentReadOnly().put(ALLIANCE_M, false);
        permissions.getAgentReadOnly().put(ALLIANCE_D, false);
        permissions.getAgentReadOnly().put(TRAVEL_PROFILE_M, false);
        permissions.getAgentReadOnly().put(IHG_SUMMARY_M, false);
        permissions.getAgentReadOnly().put(ENROLLMENT_RENEW, false);
        permissions.getAgentReadOnly().put(EXTEND_MEMBERSHIP_AMB, false);
        permissions.getAgentReadOnly().put(EXTEND_MEMBERSHIP_DR, false);
        permissions.getAgentReadOnly().put(DECLINE_TSCS, false);
        permissions.getAgentReadOnly().put(TIER_LEVEL_AMB_M, false);
        permissions.getAgentReadOnly().put(TRAVEL_PROFILE_AMB_M, false);
        permissions.getAgentReadOnly().put(EMP_TAB_V, false);
        permissions.getAgentReadOnly().put(DINING_V, false);
        permissions.getAgentReadOnly().put(AMB_SUMMARY_M, false);
        permissions.getAgentReadOnly().put(DR_SUMMARY_M, false);
        permissions.getAgentReadOnly().put(BR_SUMMARY_M, false);
        permissions.getAgentReadOnly().put(EMP_SUMMARY_M, false);
        permissions.getAgentReadOnly().put(HTL_SUMMARY_M, false);
        permissions.getAgentReadOnly().put(RC_SUMMARY_M, false);
        permissions.getAgentReadOnly().put(MTGPL_SUMMARY_M, false);
        permissions.getAgentReadOnly().put(EARNING_PREFS_M, false);
        permissions.getAgentReadOnly().put(TL_BENEFIT_M, false);
        permissions.getAgentReadOnly().put(ALTERNATE_ID_M, false);
        permissions.getAgentReadOnly().put(ALTERNATE_ID_D, false);

        permissions.getAgentReadOnly().put(KAR_SUMMARY_M, false);

        permissions.getSystemsAdministration().put(RC_SUMMARY_M, false);
        permissions.getSystemsAdministration().put(TIER_LEVEL_ACTV_AMB_V, true);

        permissions.getSystemsManager().put(RC_SUMMARY_M, false);
        permissions.getSystemsManager().put(TIER_LEVEL_ACTV_AMB_V, true);

        permissions.getSystemsCoordinator().put(EXTEND_MEMBERSHIP_AMB, false);
        permissions.getSystemsCoordinator().put(EXTEND_MEMBERSHIP_DR, false);
        permissions.getSystemsCoordinator().put(DECLINE_TSCS, false);
        permissions.getSystemsCoordinator().put(TIER_LEVEL_AMB_M, false);
        permissions.getSystemsCoordinator().put(RC_SUMMARY_M, false);
        permissions.getSystemsCoordinator().put(TIER_LEVEL_ACTV_AMB_V, true);

        permissions.getManagerMarketing().put(PC_SUMMARY_M, false);
        permissions.getManagerMarketing().put(PC_PURCHASE_LEVEL, false);
        permissions.getManagerMarketing().put(ALLIANCE_M, false);
        permissions.getManagerMarketing().put(ALLIANCE_D, false);
        permissions.getManagerMarketing().put(TRAVEL_PROFILE_M, false);
        permissions.getManagerMarketing().put(IHG_SUMMARY_M, false);
        permissions.getManagerMarketing().put(ENROLLMENT_RENEW, false);
        permissions.getManagerMarketing().put(EXTEND_MEMBERSHIP_AMB, false);
        permissions.getManagerMarketing().put(EXTEND_MEMBERSHIP_DR, false);
        permissions.getManagerMarketing().put(DECLINE_TSCS, false);
        permissions.getManagerMarketing().put(TIER_LEVEL_AMB_M, false);
        permissions.getManagerMarketing().put(TRAVEL_PROFILE_AMB_M, false);
        permissions.getManagerMarketing().put(EMP_TAB_V, false);
        permissions.getManagerMarketing().put(EMP_SUMMARY_M, false);
        permissions.getManagerMarketing().put(RC_SUMMARY_M, false);
        permissions.getManagerMarketing().put(KAR_SUMMARY_M, false);

        permissions.getReadOnlyMarketing().put(PC_SUMMARY_M, false);
        permissions.getReadOnlyMarketing().put(PC_PURCHASE_LEVEL, false);
        permissions.getReadOnlyMarketing().put(ALLIANCE_A, false);
        permissions.getReadOnlyMarketing().put(ALLIANCE_M, false);
        permissions.getReadOnlyMarketing().put(ALLIANCE_D, false);
        permissions.getReadOnlyMarketing().put(TRAVEL_PROFILE_M, false);
        permissions.getReadOnlyMarketing().put(IHG_SUMMARY_M, false);
        permissions.getReadOnlyMarketing().put(ENROLLMENT_RENEW, false);
        permissions.getReadOnlyMarketing().put(EXTEND_MEMBERSHIP_AMB, false);
        permissions.getReadOnlyMarketing().put(EXTEND_MEMBERSHIP_DR, false);
        permissions.getReadOnlyMarketing().put(DECLINE_TSCS, false);
        permissions.getReadOnlyMarketing().put(TIER_LEVEL_AMB_M, false);
        permissions.getReadOnlyMarketing().put(TRAVEL_PROFILE_AMB_M, false);
        permissions.getReadOnlyMarketing().put(EMP_TAB_V, false);
        permissions.getReadOnlyMarketing().put(AMB_SUMMARY_M, false);
        permissions.getReadOnlyMarketing().put(DR_SUMMARY_M, false);
        permissions.getReadOnlyMarketing().put(BR_SUMMARY_M, false);
        permissions.getReadOnlyMarketing().put(EMP_SUMMARY_M, false);
        permissions.getReadOnlyMarketing().put(HTL_SUMMARY_M, false);
        permissions.getReadOnlyMarketing().put(RC_SUMMARY_M, false);
        permissions.getReadOnlyMarketing().put(MTGPL_SUMMARY_M, false);
        permissions.getReadOnlyMarketing().put(EARNING_PREFS_M, false);
        permissions.getReadOnlyMarketing().put(TL_BENEFIT_M, false);
        permissions.getReadOnlyMarketing().put(ALTERNATE_ID_M, false);
        permissions.getReadOnlyMarketing().put(ALTERNATE_ID_D, false);

        permissions.getReadOnlyMarketing().put(KAR_SUMMARY_M, false);

        permissions.getReadOnlyVendor().put(PC_SUMMARY_M, false);
        permissions.getReadOnlyVendor().put(PC_PURCHASE_LEVEL, false);
        permissions.getReadOnlyVendor().put(ALLIANCE_A, false);
        permissions.getReadOnlyVendor().put(ALLIANCE_M, false);
        permissions.getReadOnlyVendor().put(ALLIANCE_D, false);
        permissions.getReadOnlyVendor().put(TRAVEL_PROFILE_M, false);
        permissions.getReadOnlyVendor().put(IHG_SUMMARY_M, false);
        permissions.getReadOnlyVendor().put(ENROLLMENT_RENEW, false);
        permissions.getReadOnlyVendor().put(EXTEND_MEMBERSHIP_AMB, false);
        permissions.getReadOnlyVendor().put(EXTEND_MEMBERSHIP_DR, false);
        permissions.getReadOnlyVendor().put(DECLINE_TSCS, false);
        permissions.getReadOnlyVendor().put(TIER_LEVEL_AMB_M, false);
        permissions.getReadOnlyVendor().put(TRAVEL_PROFILE_AMB_M, false);
        permissions.getReadOnlyVendor().put(EMP_TAB_V, false);
        permissions.getReadOnlyVendor().put(AMB_SUMMARY_M, false);
        permissions.getReadOnlyVendor().put(DR_SUMMARY_M, false);
        permissions.getReadOnlyVendor().put(BR_SUMMARY_M, false);
        permissions.getReadOnlyVendor().put(EMP_SUMMARY_M, false);
        permissions.getReadOnlyVendor().put(HTL_SUMMARY_M, false);
        permissions.getReadOnlyVendor().put(RC_SUMMARY_M, false);
        permissions.getReadOnlyVendor().put(MTGPL_SUMMARY_M, false);
        permissions.getReadOnlyVendor().put(EARNING_PREFS_M, false);
        permissions.getReadOnlyVendor().put(TL_BENEFIT_M, false);
        permissions.getReadOnlyVendor().put(ALTERNATE_ID_M, false);
        permissions.getReadOnlyVendor().put(ALTERNATE_ID_D, false);

        permissions.getReadOnlyVendor().put(KAR_SUMMARY_M, false);

        permissions.getPartnerVendor().put(PC_SUMMARY_M, false);
        permissions.getPartnerVendor().put(PC_PURCHASE_LEVEL, false);
        permissions.getPartnerVendor().put(ALLIANCE_M, false);
        permissions.getPartnerVendor().put(ALLIANCE_D, false);
        permissions.getPartnerVendor().put(TRAVEL_PROFILE_M, false);
        permissions.getPartnerVendor().put(IHG_SUMMARY_M, false);
        permissions.getPartnerVendor().put(ENROLLMENT_RENEW, false);
        permissions.getPartnerVendor().put(EXTEND_MEMBERSHIP_AMB, false);
        permissions.getPartnerVendor().put(EXTEND_MEMBERSHIP_DR, false);
        permissions.getPartnerVendor().put(DECLINE_TSCS, false);
        permissions.getPartnerVendor().put(TIER_LEVEL_AMB_M, false);
        permissions.getPartnerVendor().put(TRAVEL_PROFILE_AMB_M, false);
        permissions.getPartnerVendor().put(EMP_TAB_V, false);
        permissions.getPartnerVendor().put(AMB_SUMMARY_M, false);
        permissions.getPartnerVendor().put(DR_SUMMARY_M, false);
        permissions.getPartnerVendor().put(BR_SUMMARY_M, false);
        permissions.getPartnerVendor().put(EMP_SUMMARY_M, false);
        permissions.getPartnerVendor().put(HTL_SUMMARY_M, false);
        permissions.getPartnerVendor().put(RC_SUMMARY_M, false);
        permissions.getPartnerVendor().put(MTGPL_SUMMARY_M, false);
        permissions.getPartnerVendor().put(TL_BENEFIT_M, false);
        permissions.getPartnerVendor().put(ALTERNATE_ID_D, false);
        permissions.getPartnerVendor().put(ALTERNATE_ID_M, false);

        permissions.getPartnerVendor().put(KAR_SUMMARY_M, false);

        permissions.getHumanResources().put(PC_SUMMARY_M, false);
        permissions.getHumanResources().put(PC_PURCHASE_LEVEL, false);
        permissions.getHumanResources().put(ALLIANCE_A, false);
        permissions.getHumanResources().put(ALLIANCE_M, false);
        permissions.getHumanResources().put(ALLIANCE_D, false);
        permissions.getHumanResources().put(TRAVEL_PROFILE_M, false);
        permissions.getHumanResources().put(IHG_SUMMARY_M, false);
        permissions.getHumanResources().put(ENROLLMENT_RENEW, false);
        permissions.getHumanResources().put(EXTEND_MEMBERSHIP_AMB, false);
        permissions.getHumanResources().put(EXTEND_MEMBERSHIP_DR, false);
        permissions.getHumanResources().put(TIER_LEVEL_AMB_M, false);
        permissions.getHumanResources().put(TRAVEL_PROFILE_AMB_M, false);
        permissions.getHumanResources().put(EMP_TAB_V, false);
        permissions.getHumanResources().put(AMB_SUMMARY_M, false);
        permissions.getHumanResources().put(DR_SUMMARY_M, false);
        permissions.getHumanResources().put(BR_SUMMARY_M, false);
        permissions.getHumanResources().put(EMP_SUMMARY_M, false);
        permissions.getHumanResources().put(HTL_SUMMARY_M, false);
        permissions.getHumanResources().put(RC_SUMMARY_M, false);
        permissions.getHumanResources().put(MTGPL_SUMMARY_M, false);
        permissions.getHumanResources().put(EARNING_PREFS_M, false);
        permissions.getHumanResources().put(EMPLOYEE_RATE_ELIGIBILITY_M, true);
        permissions.getHumanResources().put(DR_TAB_V, false);
        permissions.getHumanResources().put(TL_BENEFIT_M, false);
        permissions.getHumanResources().put(ALTERNATE_ID_M, false);
        permissions.getHumanResources().put(ALTERNATE_ID_D, false);

        permissions.getHumanResources().put(KAR_SUMMARY_M, false);

        return permissions.getPermissionArray();
    }

    private Map<String, Boolean> setPermissionMap()
    {
        Map<String, Boolean> permissions = new HashMap<String, Boolean>();

        permissions.put(PROGRAM_MEMBER_POINT_BALANCE_V, true);
        permissions.put(PC_TAB_V, true);
        permissions.put(PC_SUMMARY_M, true);
        permissions.put(PC_PURCHASE_LEVEL, true);
        permissions.put(ALLIANCE_A, true);
        permissions.put(ALLIANCE_M, true);
        permissions.put(ALLIANCE_D, true);
        permissions.put(TRAVEL_PROFILE_V, true);
        permissions.put(TRAVEL_PROFILE_M, true);
        permissions.put(IHG_TAB_V, true);
        permissions.put(IHG_SUMMARY_M, true);
        permissions.put(AMB_TAB_V, true);
        permissions.put(ENROLLMENT_RENEW, true);
        permissions.put(EXTEND_MEMBERSHIP_AMB, true);
        permissions.put(EXTEND_MEMBERSHIP_DR, true);
        permissions.put(DECLINE_TSCS, true);
        permissions.put(TIER_LEVEL_AMB_M, true);
        permissions.put(TRAVEL_PROFILE_AMB_M, true);
        permissions.put(MTGPL_TAB_V, true);
        permissions.put(EMP_TAB_V, true);
        permissions.put(ANNUAL_ACTIVITY_V, true);
        permissions.put(HTL_TAB_V, true);
        permissions.put(DR_TAB_V, true);
        permissions.put(DINING_V, true);
        permissions.put(AMB_SUMMARY_M, true);
        permissions.put(DR_SUMMARY_M, true);
        permissions.put(BR_SUMMARY_M, true);
        permissions.put(EMP_SUMMARY_M, true);
        permissions.put(HTL_SUMMARY_M, true);
        permissions.put(RC_SUMMARY_M, true);
        permissions.put(MTGPL_SUMMARY_M, true);
        permissions.put(EARNING_PREFS_M, true);
        permissions.put(EMPLOYEE_RATE_ELIGIBILITY_M, false);
        permissions.put(TL_BENEFIT_M, true);
        permissions.put(ALTERNATE_ID_M, true);
        permissions.put(ALTERNATE_ID_D, true);

        permissions.put(TIER_LEVEL_ACTV_AMB_V, false);

        permissions.put(KAR_TAB_V, true);
        permissions.put(KAR_SUMMARY_M, true);

        return permissions;
    }

    @Factory(dataProvider = "verifyProgramAvailabilityProvider")
    public Object[] createInstances(String role, Map<String, Boolean> permissions)
    {
        return new Object[] { new ProgramAvailabilityFactoryTest(role, permissions) };
    }
}
