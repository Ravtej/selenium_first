package com.ihg.crm.automation.selenium.tests.sanitycheck.servicecenter.sanitycheck;

import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyNoError;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyNoErrors;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.Constant.NOT_AVAILABLE;
import static com.ihg.automation.selenium.common.types.program.ProgramStatus.CLOSED;
import static com.ihg.automation.selenium.common.types.program.ProgramStatus.OPEN;
import static com.ihg.automation.selenium.common.types.program.RewardClubLevel.SPRE;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasAnyText;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasDefault;
import static com.ihg.automation.selenium.matchers.component.GridSize.size;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static com.ihg.automation.selenium.matchers.component.ViewableMatcher.hasTextInView;
import static org.hamcrest.core.IsNot.not;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.Constant;
import com.ihg.automation.selenium.common.pages.program.EmployeeRateEligibility;
import com.ihg.automation.selenium.common.types.program.AmbassadorLevel;
import com.ihg.automation.selenium.common.types.program.RewardClubLevelReason;
import com.ihg.automation.selenium.servicecenter.pages.GuestSearch;
import com.ihg.automation.selenium.servicecenter.pages.annual.AnnualActivityPage;
import com.ihg.automation.selenium.servicecenter.pages.programs.ProgramPageBase;
import com.ihg.automation.selenium.servicecenter.pages.programs.RenewExtendProgramSummary;
import com.ihg.automation.selenium.servicecenter.pages.programs.amb.AmbassadorPage;
import com.ihg.automation.selenium.servicecenter.pages.programs.br.BusinessRewardsPage;
import com.ihg.automation.selenium.servicecenter.pages.programs.br.BusinessRewardsSummary;
import com.ihg.automation.selenium.servicecenter.pages.programs.dr.AlternateLoyaltyID;
import com.ihg.automation.selenium.servicecenter.pages.programs.dr.DiningRewardsPage;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.PartnerBenefitDetailsPopUp;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.RewardClubPage;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.RewardClubSummary;
import com.ihg.crm.automation.selenium.tests.sanitycheck.servicecenter.LoginLogout;

public class ProgramInfoTest extends LoginLogout
{
    @BeforeClass
    public void before()
    {
        login();
        new GuestSearch().byMemberNumber(memberId);
    }

    @Test
    public void validateRewardClubPage()
    {
        RewardClubPage rewardClubPage = new RewardClubPage();
        rewardClubPage.goTo();
        verifyNoErrors();

        RewardClubSummary summary = rewardClubPage.getSummary();
        verifyThat(summary.getMemberId(), hasText(memberId));
        verifyThat(summary.getBalance(), hasAnyText());
        verifyThat(summary.getBalanceExpirationDate(), hasAnyText());
        verifyThat(summary.getExpiredLoyaltyUnitBalance(), hasAnyText());
        verifyThat(summary.getLastActivityDate(), hasAnyText());
        verifyThat(summary.getLastAdjusted(), hasAnyText());

        summary.verify(OPEN, SPRE, RewardClubLevelReason.POINTS);

        EmployeeRateEligibility employeeRateEligibility = rewardClubPage.getEmployeeRateEligibility();
        verifyThat(employeeRateEligibility, displayed(true));
        employeeRateEligibility.verifyInViewMode("Yes", "Does not expire", NOT_AVAILABLE, "Merlin");

        verifyThat(rewardClubPage.getCurrentYearAnnualActivities(), displayed(true));
        verifyThat(rewardClubPage.getEarningDetails().getEarningPreference(), hasTextInView(Constant.RC_POINTS));

        ProgramPageBase.EnrollmentDetails enrollmentDetails = rewardClubPage.getEnrollmentDetails();
        verifyThat(enrollmentDetails.getEnrollDate(), hasAnyText());
        verifyThat(enrollmentDetails.getOfferCode(), hasText("SCCAL"));
        verifyThat(enrollmentDetails.getPaymentDetails().getMethod(), hasDefault());
        verifyThat(enrollmentDetails.getReferringMember(), hasText(NOT_AVAILABLE));
        verifyThat(enrollmentDetails.getRenewalDate(), hasDefault());

        summary.getPartnerBenefits().clickAndWait(verifyNoError());
        PartnerBenefitDetailsPopUp partnerBenefitDetailsPopUp = new PartnerBenefitDetailsPopUp();
        verifyThat(partnerBenefitDetailsPopUp, displayed(true));
        partnerBenefitDetailsPopUp.close();
    }

    @Test
    public void validateAmbassadorPage()
    {
        AmbassadorPage ambassadorPage = new AmbassadorPage();
        ambassadorPage.goTo();
        verifyNoErrors();

        RenewExtendProgramSummary summary = ambassadorPage.getSummary();
        verifyThat(summary.getMemberId(), hasText(memberId));
        verifyThat(summary.getExpirationDate(), hasAnyText());
        summary.verify(OPEN, AmbassadorLevel.AMB);

        verifyThat(ambassadorPage.getAnnualActivities(), displayed(true));

        ProgramPageBase.EnrollmentDetails details = ambassadorPage.getEnrollmentDetails();
        verifyThat(details.getEnrollDate(), hasAnyText());
        verifyThat(details.getOfferCode(), hasText("SCBRS"));
        verifyThat(details.getReferringMember(), hasText(NOT_AVAILABLE));
    }

    @Test(groups = { "drGroup" })
    public void validateDiningRewardsPage()
    {
        DiningRewardsPage diningRewardsPage = new DiningRewardsPage();
        diningRewardsPage.goTo();
        verifyNoErrors();

        RenewExtendProgramSummary summary = diningRewardsPage.getSummary();
        verifyThat(summary.getMemberId(), hasText(memberId));
        verifyThat(summary.getExpirationDate(), hasAnyText());
        verifyThat(summary.getStatus(), hasTextInView(CLOSED));

        AlternateLoyaltyID alternateLoyaltyID = diningRewardsPage.getAlternateLoyaltyID();
        verifyThat(alternateLoyaltyID.getNumber(), hasText(NOT_AVAILABLE));
        verifyThat(alternateLoyaltyID.getType(), hasText(NOT_AVAILABLE));

        ProgramPageBase.EnrollmentDetails enrollDetails = diningRewardsPage.getEnrollmentDetails();

        verifyThat(enrollDetails.getEnrollDate(), hasAnyText());
        verifyThat(enrollDetails.getReferringMember(), hasText(Constant.NOT_AVAILABLE));
        verifyThat(enrollDetails.getOfferCode(), hasText(Constant.NOT_AVAILABLE));
        verifyThat(enrollDetails.getRenewalDate(), hasAnyText());
    }

    @Test
    public void validateBusinessRewardsPage()
    {
        BusinessRewardsPage businessRewardsPage = new BusinessRewardsPage();
        businessRewardsPage.goTo();
        verifyNoErrors();

        BusinessRewardsSummary summary = businessRewardsPage.getSummary();
        verifyThat(summary.getMemberId(), hasText(memberId));
        verifyThat(summary.getStatus(), hasTextInView(OPEN));

        ProgramPageBase.EnrollmentDetails enrollDetails = businessRewardsPage.getEnrollmentDetails();

        verifyThat(enrollDetails.getPaymentDetails().getMethod(), hasDefault());
        verifyThat(enrollDetails.getReferringMember(), hasText(NOT_AVAILABLE));
        verifyThat(enrollDetails.getRenewalDate(), hasDefault());
        verifyThat(enrollDetails.getEnrollDate(), hasAnyText());
    }

    @Test
    public void validateAnnualActivity()
    {
        AnnualActivityPage annualActivityPage = new AnnualActivityPage();
        annualActivityPage.goTo();
        verifyNoErrors();

        verifyThat(annualActivityPage.getBaseUnits().getAmount(), hasAnyText());
        verifyThat(annualActivityPage.getBonusUnits().getAmount(), hasAnyText());
        verifyThat(annualActivityPage.getTotalUnits().getAmount(), hasAnyText());
        verifyThat(annualActivityPage.getCurrentBalance().getAmount(), hasAnyText());

        verifyThat(annualActivityPage.getAnnualActivities(), size(not(0)));
    }
}
