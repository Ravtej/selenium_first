package com.ihg.crm.automation.selenium.tests.sanitycheck.servicecenter.sanitycheck;

import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyNoError;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasTextWithWait;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;

import org.springframework.beans.factory.annotation.Value;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.order.VoucherOrder;
import com.ihg.automation.selenium.common.pages.SubmitPopUpBase;
import com.ihg.automation.selenium.gwt.components.Menu;
import com.ihg.automation.selenium.hotel.pages.hoteloperations.reimbursement.freenight.FreeNightFlatReimbursementPage;
import com.ihg.automation.selenium.servicecenter.pages.GuestSearch;
import com.ihg.automation.selenium.servicecenter.pages.TopMenu;
import com.ihg.automation.selenium.servicecenter.pages.hotel.certificate.HotelCertificatePage;
import com.ihg.automation.selenium.servicecenter.pages.hotel.freenight.FreeNightReimbursementPage;
import com.ihg.automation.selenium.servicecenter.pages.offer.OfferRegistrationPopUp;
import com.ihg.automation.selenium.servicecenter.pages.unit.GiftUnitsPopUp;
import com.ihg.automation.selenium.servicecenter.pages.unit.GoodwillPopUp;
import com.ihg.automation.selenium.servicecenter.pages.unit.PurchaseTierLevelPopUp;
import com.ihg.automation.selenium.servicecenter.pages.unit.PurchaseUnitsPopUp;
import com.ihg.automation.selenium.servicecenter.pages.unit.TransferNightsPopUp;
import com.ihg.automation.selenium.servicecenter.pages.unit.TransferUnitsPopUp;
import com.ihg.automation.selenium.servicecenter.pages.voucher.OrderDetails;
import com.ihg.automation.selenium.servicecenter.pages.voucher.VoucherDetailsPanel;
import com.ihg.automation.selenium.servicecenter.pages.voucher.VoucherLookupTab;
import com.ihg.automation.selenium.servicecenter.pages.voucher.VoucherPopUp;
import com.ihg.crm.automation.selenium.tests.sanitycheck.servicecenter.LoginLogout;

public class TopMenuTest extends LoginLogout
{
    @Value("${voucher.number}")
    protected String voucherNumber;

    @Value("${voucher.name}")
    protected String voucherName;

    @Value("${voucher.id}")
    protected String voucherId;

    @Value("${item.name}")
    protected String itemName;

    @BeforeClass
    public void before()
    {
        login();
    }

    @DataProvider
    private Object[][] unitManagementSubMenuItems()
    {
        TopMenu.UnitManagementMenu unitManagement = new TopMenu().getUnitManagement();
        return new Object[][] { { unitManagement.getGoodwillUnits(), new GoodwillPopUp() },
                { unitManagement.getPurchaseUnits(), new PurchaseUnitsPopUp(), },
                { unitManagement.getPurchaseGift(), new GiftUnitsPopUp() },
                { unitManagement.getPurchaseTierLevel(), new PurchaseTierLevelPopUp() },
                { unitManagement.getTransferUnits(), new TransferUnitsPopUp() },
                { unitManagement.getTransferQNights(), new TransferNightsPopUp() } };
    }

    @DataProvider
    private Object[][] voucherSubMenuItems()
    {
        TopMenu.VoucherMenu voucherMenu = new TopMenu().getVoucher();
        return new Object[][] { { voucherMenu.getLookup(), new VoucherPopUp().getVoucherLookupTab() },

                { voucherMenu.getDeposit(), new VoucherPopUp().getVoucherDepositTab() } };
    }

    @Test(priority = 3)
    public void openReimbursementSubMenu()
    {
        TopMenu.HotelOperationsMenu hotelOperations = new TopMenu().getHotelOperations();
        hotelOperations.getFreeNightFlatReimb().clickAndWait(verifyNoError());
        verifyThat(new FreeNightFlatReimbursementPage(), displayed(true));

        hotelOperations.getHotelAndOtherCertificates().clickAndWait(verifyNoError());
        verifyThat(new HotelCertificatePage(), displayed(true));

        hotelOperations.getHotelStartupFees().clickAndWait(verifyNoError());

        hotelOperations.getRewardAndFreeNightReimb().clickAndWait(verifyNoError());
        verifyThat(new FreeNightReimbursementPage(), displayed(true));
    }

    @Test(priority = 5)
    public void openOfferSubMenu()
    {
        TopMenu.OfferMenu offer = new TopMenu().getOffer();

        offer.getRegistration().clickAndWait(verifyNoError());
        OfferRegistrationPopUp popUp = new OfferRegistrationPopUp();
        verifyThat(popUp, displayed(true));
        popUp.close();

        offer.getSearch().clickAndWait(verifyNoError());
    }

    @Test(priority = 6)
    public void openMemberAndOfferSubMenu()
    {
        new GuestSearch().byMemberNumber(memberId);

        new TopMenu().getOffer().getRegistration().clickAndWait(verifyNoError());
        OfferRegistrationPopUp offerRegistrationPopUp = new OfferRegistrationPopUp();
        verifyThat(offerRegistrationPopUp, displayed(true));
        offerRegistrationPopUp.close();
    }

    @Test(dataProvider = "unitManagementSubMenuItems", priority = 10)
    public void openUnitManagementSubMenu(Menu.Item item, SubmitPopUpBase popUp)
    {
        item.clickAndWait(verifyNoError());
        verifyThat(popUp, displayed(true));
        verifyThat(popUp.getSubmit(), displayed(true));
        popUp.close();
    }

    @Test(priority = 10)
    public void openVoucherSubMenu()
    {
        VoucherOrder voucherOrder = new VoucherOrder(voucherId, voucherName);

        new TopMenu().getVoucher().getLookup().clickAndWait(verifyNoError());
        VoucherLookupTab voucherLookupTab = new VoucherPopUp().getVoucherLookupTab();
        verifyThat(voucherLookupTab, displayed(true));
        voucherLookupTab.getVoucherNumber().type(voucherNumber);
        voucherLookupTab.clickLookup();

        VoucherDetailsPanel voucherDetailsPanel = voucherLookupTab.getVoucherDetailsPanel();
        verifyThat(voucherDetailsPanel.getStatus(), hasTextWithWait("Deposited"));
        verifyThat(voucherDetailsPanel.getVoucherNumber(), hasText(voucherNumber));
        verifyThat(voucherDetailsPanel.getItemName(), hasText(itemName));

        OrderDetails orderDetails = voucherLookupTab.getOrderDetailsPanel().getOrderDetails();
        verifyThat(orderDetails.getItemID(), hasText(voucherOrder.getPromotionId()));
        verifyThat(orderDetails.getItemName(), hasText(voucherOrder.getVoucherName()));

        verifyThat(voucherLookupTab.getStatusGrid(), displayed(true));

        voucherLookupTab.clickClose();
    }

}
