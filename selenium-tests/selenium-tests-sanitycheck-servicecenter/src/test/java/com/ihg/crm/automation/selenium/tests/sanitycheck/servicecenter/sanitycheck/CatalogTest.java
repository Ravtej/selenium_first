package com.ihg.crm.automation.selenium.tests.sanitycheck.servicecenter.sanitycheck;

import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyError;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyNoErrors;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasEmptyText;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasTextAsInt;
import static com.ihg.automation.selenium.matchers.component.GridSize.size;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static com.ihg.automation.selenium.matchers.text.ContainsIgnoreCase.containsIgnoreCase;
import static com.ihg.automation.selenium.servicecenter.pages.catalog.CatalogItemsGridRow.CatalogItemsGridCell.ITEM_NAME;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.core.AllOf.allOf;
import static org.hamcrest.number.OrderingComparison.greaterThanOrEqualTo;
import static org.hamcrest.number.OrderingComparison.lessThanOrEqualTo;

import org.springframework.beans.factory.annotation.Value;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.google.common.collect.ImmutableList;
import com.ihg.automation.selenium.common.catalog.CatalogItem;
import com.ihg.automation.selenium.servicecenter.pages.GuestSearch;
import com.ihg.automation.selenium.servicecenter.pages.TopMenu;
import com.ihg.automation.selenium.servicecenter.pages.catalog.CatalogItemDetailsPopUp;
import com.ihg.automation.selenium.servicecenter.pages.catalog.CatalogItemsGrid;
import com.ihg.automation.selenium.servicecenter.pages.catalog.CatalogItemsGridRow;
import com.ihg.automation.selenium.servicecenter.pages.catalog.CatalogPage;
import com.ihg.automation.selenium.servicecenter.pages.catalog.CatalogSearch;
import com.ihg.automation.selenium.servicecenter.pages.order.OrderSummaryPopUp;
import com.ihg.crm.automation.selenium.tests.sanitycheck.servicecenter.LoginLogout;

public class CatalogTest extends LoginLogout
{
    @Value("${catalogMemberId}")
    protected String catalogMemberId;

    protected final static String UNIT_COST_MIN = "1000";
    protected final static String UNIT_COST_MAX = "5000";
    protected final static String ITEM_ID = "IHG04";
    private final static CatalogItem CATALOG_ITEM = CatalogItem.builder(ITEM_ID, "IHG Rewards Club - GLDE - 2014")
            .vendor("FISERV").startDate("07May13").endDate("31Dec99").status("Active").orderLimit("1")
            .loyaltyUnitCost("0").regions(ImmutableList.of("UNITED STATES", "CANADA", "MSAC", "EMEA", "ASIA PACIFIC"))
            .countries(ImmutableList.of("All")).tierLevels(ImmutableList.of("All")).build();
    private CatalogPage catalogPage = new CatalogPage();
    private TopMenu menu = new TopMenu();
    private OrderSummaryPopUp orderSummaryPopUp = new OrderSummaryPopUp();
    private CatalogItemsGrid catalogGrid;

    @BeforeClass
    public void before()
    {
        login();
        new GuestSearch().byMemberNumber(catalogMemberId);
    }

    @Test
    public void verifyCatalogFields()
    {
        catalogPage.goTo();
        verifyNoErrors();

        CatalogSearch catalogSearch = catalogPage.getSearchFields();

        verifyThat(catalogSearch.getCatalogID(), hasEmptyText());
        verifyThat(catalogSearch.getItemID(), hasEmptyText());
        verifyThat(catalogSearch.getItemName(), hasEmptyText());
        verifyThat(catalogSearch.getLoyaltyUnitCost().getLoyaltyUnitCostMin(), hasEmptyText());

        catalogPage.getButtonBar().clickClearCriteria();
        verifyThat(catalogSearch.getLoyaltyUnitCost().getLoyaltyUnitCostMax(), hasEmptyText());
    }

    @Test(dependsOnMethods = { "verifyCatalogFields" }, alwaysRun = true)
    public void searchByItemId()
    {
        catalogPage.searchByItemId(ITEM_ID);

        catalogGrid = catalogPage.getCatalogGrid();
        verifyThat(catalogGrid, size(1));

        verifyThat(catalogGrid.getRow(1).getCell(CatalogItemsGridRow.CatalogItemsGridCell.ITEM_ID), hasText(ITEM_ID));
    }

    @Test(dependsOnMethods = { "searchByItemId" }, alwaysRun = true)
    public void searchByItemName()
    {
        String itemName = "ipod";

        catalogPage.search(null, null, itemName, null, null);

        catalogGrid = catalogPage.getCatalogGrid();
        catalogGrid.verify(ITEM_NAME, hasText(containsIgnoreCase(itemName)));

    }

    @Test(dependsOnMethods = { "searchByItemName" }, alwaysRun = true)
    public void searchByLoyaltyUnitCostMin()
    {
        catalogPage.search(null, null, null, UNIT_COST_MIN, null);

        catalogGrid = catalogPage.getCatalogGrid();
        verifyThat(catalogGrid, size(10));
        catalogGrid.verifyUnitCost(hasTextAsInt(greaterThanOrEqualTo(Integer.parseInt(UNIT_COST_MIN))));
    }

    @Test(dependsOnMethods = { "searchByLoyaltyUnitCostMin" }, alwaysRun = true)
    public void searchByLoyaltyUnitCostMax()
    {
        catalogPage.search(null, null, null, null, UNIT_COST_MAX);

        catalogGrid = catalogPage.getCatalogGrid();
        verifyThat(catalogGrid, size(10));
        catalogGrid.verifyUnitCost(hasTextAsInt(lessThanOrEqualTo(Integer.parseInt(UNIT_COST_MAX))));
    }

    @Test(dependsOnMethods = { "searchByLoyaltyUnitCostMax" }, alwaysRun = true)
    public void searchByLoyaltyUnitCost()
    {
        catalogPage.search(null, null, null, UNIT_COST_MIN, UNIT_COST_MAX);

        catalogGrid = catalogPage.getCatalogGrid();
        verifyThat(catalogGrid, size(10));
        catalogGrid.verifyUnitCost(hasTextAsInt(allOf(greaterThanOrEqualTo(Integer.parseInt(UNIT_COST_MIN)),
                lessThanOrEqualTo(Integer.parseInt(UNIT_COST_MAX)))));
    }

    @Test(dependsOnMethods = { "searchByLoyaltyUnitCost" }, alwaysRun = true)
    public void verifyItemDetails()
    {
        catalogPage.searchByItemId(CATALOG_ITEM);

        CatalogItemDetailsPopUp catalogItemDetailsPopUp = catalogPage.openCatalogItemDetailsPopUp();
        catalogItemDetailsPopUp.verify(CATALOG_ITEM);

        verifyThat(catalogItemDetailsPopUp.getQuantity(), hasText("1"));

        catalogItemDetailsPopUp.openOrderSummaryPopUp();
    }

    @Test(dependsOnMethods = { "verifyItemDetails" }, alwaysRun = true)
    public void continueShopping()
    {
        orderSummaryPopUp = new OrderSummaryPopUp();
        orderSummaryPopUp.clickContinueShopping();
        verifyThat(orderSummaryPopUp, displayed(false));
        verifyThat(catalogPage, displayed(true));
        menu.getShoppingCart().clickAndWait();

        orderSummaryPopUp.flush();
        verifyThat(orderSummaryPopUp, displayed(true));
    }

    @Test(dependsOnMethods = { "continueShopping" }, alwaysRun = true)
    public void removeItemFromShoppingCart()
    {
        orderSummaryPopUp = new OrderSummaryPopUp();

        verifyThat(orderSummaryPopUp.getRow(1).getItemDescription().getItemID(), hasText(ITEM_ID));

        orderSummaryPopUp.clickRemove();
        verifyThat(orderSummaryPopUp, hasText(containsString("Shopping Cart is empty.")));
        orderSummaryPopUp.clickCheckout(verifyError(hasText(containsString("Shopping Cart is empty."))));
        orderSummaryPopUp.clickContinueShopping();
        verifyThat(orderSummaryPopUp, displayed(false));
        verifyThat(menu.getShoppingCart(), displayed(false));
    }
}
