package com.ihg.crm.automation.selenium.tests.sanitycheck.servicecenter.sanitycheck;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.isDisplayedWithWait;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.pages.login.SingleSignOn;
import com.ihg.automation.selenium.common.pages.login.TopUserSection;
import com.ihg.automation.selenium.gwt.components.dialog.ConfirmDialog;
import com.ihg.automation.selenium.matchers.component.HasText;
import com.ihg.crm.automation.selenium.tests.sanitycheck.servicecenter.LoginLogout;

public class SignOffTest extends LoginLogout
{
    @BeforeClass
    public void beforeClass()
    {
        login();
    }

    @Test
    public void simpleSignOff()
    {
        TopUserSection userSection = new TopUserSection();
        verifyThat(userSection.getSignOff(), isDisplayedWithWait());
        ConfirmDialog dialog = userSection.clickSignOff();
        verifyThat(dialog, isDisplayedWithWait());
        verifyThat(dialog, HasText.hasText("Are you sure you want to sign off?"));
        dialog.close(true);
        verifyThat(new SingleSignOn().getPassword(), displayed(true, true));
    }
}
