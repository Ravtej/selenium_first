package com.ihg.crm.automation.selenium.tests.sanitycheck.servicecenter.sanitycheck;

import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyNoError;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasText;
import static com.ihg.automation.selenium.matchers.component.GridSize.size;
import static org.hamcrest.CoreMatchers.containsString;

import org.springframework.beans.factory.annotation.Value;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.CustomerInfoPanel;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.matchers.component.ComponentMatcher;
import com.ihg.automation.selenium.servicecenter.pages.GuestSearch;
import com.ihg.automation.selenium.servicecenter.pages.LeftPanel;
import com.ihg.automation.selenium.servicecenter.pages.search.StaySearchGrid;
import com.ihg.automation.selenium.servicecenter.pages.search.StaySearchGridCell;
import com.ihg.automation.selenium.servicecenter.pages.search.StaySearchGridRow;
import com.ihg.crm.automation.selenium.tests.sanitycheck.servicecenter.LoginLogout;

public class SearchTest extends LoginLogout
{
    @Value("${stay.firstName}")
    protected String firstName;

    @Value("${stay.secondName}")
    protected String secondName;

    private GuestSearch guestSearch = new GuestSearch();
    private GuestSearch.SearchByStay searchByStay = guestSearch.getSearchByStay();

    @BeforeClass
    public void beforeClass()
    {
        login();
    }

    @Test(priority = 10)
    public void defaultSearchCriteria()
    {
        getWelcome().click();
        new GuestSearch().getCustomerSearch().verifyDefault();
    }

    @Test(priority = 20)
    public void searchByMemberId()
    {
        guestSearch.byMemberNumber(memberId, verifyNoError());
        verifyThat(new CustomerInfoPanel().getMemberNumber(Program.RC), ComponentMatcher.hasText(memberId));

        new LeftPanel().goToNewSearch();
        verifyNoError();
    }

    @Test(priority = 20)
    public void searchByHotelCheckoutFolio()
    {
        guestSearch.clickClear();

        searchByStay.expand();
        searchByStay.getHotel().type(stayHotel);
        searchByStay.getStayDate().type(stayDate);
        searchByStay.getFolioNumber().type(folioNumber);
        guestSearch.clickSearch(verifyNoError());

        verifySearchResults();
    }

    @Test(priority = 20)
    public void searchByHotelCheckoutConfirmation()
    {
        guestSearch.clickClear(verifyNoError());

        searchByStay.expand();
        searchByStay.getHotel().type(stayHotel);
        searchByStay.getStayDate().type(stayDate);
        searchByStay.getConfirmationNumber().type(confirmationNumber);
        guestSearch.clickSearch(verifyNoError());

        verifySearchResults();
    }

    private void verifySearchResults()
    {
        StaySearchGrid staySearchGrid = new StaySearchGrid();
        verifyThat(staySearchGrid, size(1));

        StaySearchGridRow customerSearchGridRow = staySearchGrid.getRow(1);
        verifyThat(customerSearchGridRow.getCell(StaySearchGridCell.MEMBER_NUMBER), hasText(memberId));
        verifyThat(customerSearchGridRow.getCell(StaySearchGridCell.GUEST_NAME), hasText(containsString(firstName)));
        verifyThat(customerSearchGridRow.getCell(StaySearchGridCell.GUEST_NAME), hasText(containsString(secondName)));

        verifyThat(customerSearchGridRow.getCell(StaySearchGridCell.HOTEL), hasText(stayHotel));
        verifyThat(customerSearchGridRow.getCell(StaySearchGridCell.CHECK_OUT), hasText(stayDate));
        verifyThat(customerSearchGridRow.getCell(StaySearchGridCell.ROOM_NUMBER), hasText(roomNumber));
        verifyThat(customerSearchGridRow.getCell(StaySearchGridCell.CONFIRMATION_NUMBER), hasText(confirmationNumber));
    }
}
