package com.ihg.crm.automation.selenium.tests.sanitycheck.servicecenter;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.test.context.ContextConfiguration;

import com.ihg.automation.selenium.common.SiteHelperBase;
import com.ihg.automation.selenium.common.User;
import com.ihg.automation.selenium.gwt.components.Component;
import com.ihg.automation.selenium.gwt.components.utils.FindStepUtils;
import com.ihg.automation.selenium.servicecenter.pages.Role;
import com.ihg.automation.selenium.servicecenter.pages.ServiceCenterHelper;
import com.ihg.automation.selenium.tests.common.TestNGBase;

@ContextConfiguration(locations = { "classpath:app-context-sanity-servicecenter.xml" })
public class LoginLogout extends TestNGBase
{
    @Value("${loginUrl}")
    private String loginUrl;

    @Resource(name = "prodUser")
    protected User prodUser;

    @Value("${memberId}")
    protected String memberId;

    @Value("${stay.date}")
    protected String stayDate;

    @Value("${stay.roomNumber}")
    protected String roomNumber;

    @Value("${stay.folioNumber}")
    protected String folioNumber;

    @Value("${stay.confirmationNumber}")
    protected String confirmationNumber;

    @Value("${stay.hotel}")
    protected String stayHotel;

    @Override
    protected SiteHelperBase getHelper()
    {
        return new ServiceCenterHelper(loginUrl);
    }

    public void login()
    {
        helper.login(prodUser, Role.SYSTEMS_MANAGER, true);
    }

    public Component getWelcome()
    {
        return new Component(null, "Welcome screen", "Welcome screen",
                FindStepUtils.byXpathWithWait(".//div[@class='welcome-panel']"));
    }

}
