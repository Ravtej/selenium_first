package com.ihg.crm.automation.selenium.tests.sanitycheck.servicecenter.sanitycheck;

import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.assertNoError;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyNoErrors;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.Constant.NOT_AVAILABLE;
import static com.ihg.automation.selenium.common.Constant.RC_POINTS;
import static com.ihg.automation.selenium.common.components.RangeSelect.Range.ZERO_MONTH;
import static com.ihg.automation.selenium.common.event.EventTransactionType.Membership.MEMBERSHIP_RENEWAL;
import static com.ihg.automation.selenium.common.event.EventTransactionType.Order.REDEEM;
import static com.ihg.automation.selenium.common.event.EventTransactionType.RewardNight.REWARD_NIGHT;
import static com.ihg.automation.selenium.common.offer.Offer.builder;
import static com.ihg.automation.selenium.common.payment.PaymentFactory.COMPLIMENTARY;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasAnyText;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasEmptyText;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.isDisplayedAndEnabled;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.isDisplayedWithWait;
import static com.ihg.automation.selenium.matchers.component.GridSize.size;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.core.IsNot.not;
import static org.hamcrest.core.StringContains.containsString;

import org.springframework.beans.factory.annotation.Value;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.billing.EventBillingAmount;
import com.ihg.automation.selenium.common.billing.EventBillingDetailsTab;
import com.ihg.automation.selenium.common.components.Country;
import com.ihg.automation.selenium.common.components.IndicatorSelect;
import com.ihg.automation.selenium.common.earning.EventEarningAmount;
import com.ihg.automation.selenium.common.earning.EventEarningDetailsTab;
import com.ihg.automation.selenium.common.earning.EventEarningGridRow;
import com.ihg.automation.selenium.common.member.GuestAddress;
import com.ihg.automation.selenium.common.offer.Offer;
import com.ihg.automation.selenium.common.order.Order;
import com.ihg.automation.selenium.common.order.OrderItem;
import com.ihg.automation.selenium.common.reward.RewardNight;
import com.ihg.automation.selenium.common.types.Currency;
import com.ihg.automation.selenium.common.types.address.AddressType;
import com.ihg.automation.selenium.common.types.address.Region;
import com.ihg.automation.selenium.servicecenter.pages.GuestSearch;
import com.ihg.automation.selenium.servicecenter.pages.deposit.DepositDetailsPopUp;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsGridRow;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsPage;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsSearch;
import com.ihg.automation.selenium.servicecenter.pages.event.copartner.CoPartnerEventGrid;
import com.ihg.automation.selenium.servicecenter.pages.event.copartner.CoPartnerEventGridRow;
import com.ihg.automation.selenium.servicecenter.pages.event.copartner.CoPartnerEventsPage;
import com.ihg.automation.selenium.servicecenter.pages.event.deposit.DepositDetails;
import com.ihg.automation.selenium.servicecenter.pages.event.deposit.DepositGridRowContainer;
import com.ihg.automation.selenium.servicecenter.pages.event.enroll.MembershipDetailsPopUp;
import com.ihg.automation.selenium.servicecenter.pages.event.enroll.MembershipDetailsTab;
import com.ihg.automation.selenium.servicecenter.pages.event.enroll.MembershipGridRowContainer;
import com.ihg.automation.selenium.servicecenter.pages.event.freenight.FreeNightDetailsPopUp;
import com.ihg.automation.selenium.servicecenter.pages.event.freenight.FreeNightDetailsTab;
import com.ihg.automation.selenium.servicecenter.pages.event.freenight.FreeNightEventPage;
import com.ihg.automation.selenium.servicecenter.pages.event.freenight.FreeNightGridRow;
import com.ihg.automation.selenium.servicecenter.pages.event.freenight.OfferDetailsFreeNightTab;
import com.ihg.automation.selenium.servicecenter.pages.event.offer.OfferDetailsTabExtended;
import com.ihg.automation.selenium.servicecenter.pages.event.offer.OfferDetailsViewExtended;
import com.ihg.automation.selenium.servicecenter.pages.event.offer.OfferEventPage;
import com.ihg.automation.selenium.servicecenter.pages.event.offer.OfferPopUp;
import com.ihg.automation.selenium.servicecenter.pages.event.order.OrderDetailsPopUp;
import com.ihg.automation.selenium.servicecenter.pages.event.order.OrderDetailsTab;
import com.ihg.automation.selenium.servicecenter.pages.event.order.OrderEventsPage;
import com.ihg.automation.selenium.servicecenter.pages.event.order.OrderGridRow;
import com.ihg.automation.selenium.servicecenter.pages.event.order.OrderRedeemGridRowContainer;
import com.ihg.automation.selenium.servicecenter.pages.event.rewardnight.RewardNightDetailsPopUp;
import com.ihg.automation.selenium.servicecenter.pages.event.rewardnight.RewardNightEventGridRow;
import com.ihg.automation.selenium.servicecenter.pages.event.rewardnight.RewardNightEventPage;
import com.ihg.automation.selenium.servicecenter.pages.event.rewardnight.RewardNightGridRowContainer;
import com.ihg.automation.selenium.servicecenter.pages.event.rewardnight.RewardNightSearchButtonBar;
import com.ihg.automation.selenium.servicecenter.pages.event.stay.AdjustStayPopUp;
import com.ihg.automation.selenium.servicecenter.pages.event.stay.RevenueDetailsEdit;
import com.ihg.automation.selenium.servicecenter.pages.event.stay.RevenueDetailsView;
import com.ihg.automation.selenium.servicecenter.pages.event.stay.StayBookingInfoBase;
import com.ihg.automation.selenium.servicecenter.pages.event.stay.StayDetailsAdjust;
import com.ihg.automation.selenium.servicecenter.pages.event.stay.StayEventsPage;
import com.ihg.automation.selenium.servicecenter.pages.event.stay.StayGuestGrid;
import com.ihg.automation.selenium.servicecenter.pages.event.stay.StayGuestGridRow;
import com.ihg.automation.selenium.servicecenter.pages.event.stay.StayGuestGridRowContainer;
import com.ihg.automation.selenium.servicecenter.pages.event.stay.StayInfoBase;
import com.ihg.automation.selenium.servicecenter.pages.event.stay.StayInfoEdit;
import com.ihg.automation.selenium.servicecenter.pages.event.stay.StayInfoView;
import com.ihg.automation.selenium.servicecenter.pages.event.stay.StaySearch;
import com.ihg.crm.automation.selenium.tests.sanitycheck.servicecenter.LoginLogout;

public class EventsTabsTest extends LoginLogout
{
    @Value("${order.date}")
    protected String orderDate;

    @Value("${order.transactionDate}")
    protected String orderTransDate;

    @Value("${order.itemName}")
    protected String orderItemName;

    @Value("${order.itemId}")
    protected String itemId;

    @Value("${rewardnight.certificateNumber}")
    protected String certNumber;

    @Value("${rewardnight.bookingSource}")
    protected String bookingSource;

    @Value("${rewardnight.transactionDate}")
    protected String transactionDate;

    private static final String OFFER_CODE = "8300440";
    private static final String OFFER_NAME = "Chase Free Night";
    private static final String CO_PARTNER_ID = "SLCTBA";

    @BeforeClass
    public void before()
    {
        login();
        new GuestSearch().byMemberNumber(memberId);
    }

    @Test
    public void allEvents()
    {
        AllEventsPage allEventsPage = new AllEventsPage();
        allEventsPage.goTo();
        verifyNoErrors();

        verifyThat(allEventsPage.getGrid(), size(not(0)));

        AllEventsSearch searchFields = allEventsPage.getSearchFields();
        searchFields.getEventType().select("Membership");
        searchFields.getTransactionType().select(MEMBERSHIP_RENEWAL);
        allEventsPage.getButtonBar().clickSearch();
        verifyThat(allEventsPage.getGrid(), size(not(0)));

        AllEventsGridRow row = allEventsPage.getGrid().getRow(1);
        MembershipGridRowContainer container = new MembershipGridRowContainer(row.expand());
        container.getMembershipDetails().getPaymentDetails().verifyCommonDetails(COMPLIMENTARY);

        container.clickDetails();
        MembershipDetailsPopUp membershipDetailsPopUp = new MembershipDetailsPopUp();
        verifyThat(membershipDetailsPopUp, isDisplayedWithWait());

        MembershipDetailsTab membershipDetailsTab = membershipDetailsPopUp.getMembershipDetailsTab();
        membershipDetailsTab.goTo();
        membershipDetailsTab.getMembershipDetails().getPaymentDetails().verifyCommonDetails(COMPLIMENTARY);

        EventEarningDetailsTab earningDetailsTab = membershipDetailsPopUp.getEarningDetailsTab();
        earningDetailsTab.goTo();
        earningDetailsTab.verifyBasicDetailsNoPointsAndMiles();

        verifyThat(earningDetailsTab.getGrid(), size(not(0)));

        EventBillingDetailsTab billingDetailsTab = membershipDetailsPopUp.getBillingDetailsTab();
        billingDetailsTab.goTo();
        verifyNoErrors();
        verifyThat(billingDetailsTab, hasText(containsString(MEMBERSHIP_RENEWAL)));

        membershipDetailsPopUp.clickClose(assertNoError());
    }

    @Test
    public void verifyStayDetails()
    {
        StayEventsPage stayPage = new StayEventsPage();
        stayPage.goTo();
        verifyNoErrors();

        StaySearch searchFields = stayPage.getSearchFields();
        searchFields.getGuestName().clear();
        searchFields.getConfirmationNumber().typeAndWait(confirmationNumber);
        searchFields.getStayDate().populate(stayDate, ZERO_MONTH);
        searchFields.getHotel().typeAndWait(stayHotel);
        stayPage.getButtonBar().clickSearch();

        StayGuestGrid guestGrid = stayPage.getGuestGrid();
        verifyThat(guestGrid, size(1));

        StayGuestGridRow row = guestGrid.getRow(1);
        StayGuestGridRowContainer rowContainer = row.getDetails();
        verifyStayDetails(rowContainer);

        row.getDetails().clickDetails();

        AdjustStayPopUp adjustStayPopUp = new AdjustStayPopUp();
        StayDetailsAdjust stayDetails = adjustStayPopUp.getStayDetailsTab().getStayDetails();

        StayInfoEdit stayInfo = stayDetails.getStayInfo();
        verifyThat(stayInfo.getHotel(), hasText(stayHotel));
        verifyThat(stayInfo.getEarningPreference(), hasText(RC_POINTS));

        verifyStayInfo(stayInfo);
        verifyThat(stayInfo.getCorpAcctNumber(), hasEmptyText());
        verifyThat(stayInfo.getIATANumber(), hasEmptyText());

        verifyBookingInfo(stayDetails.getBookingInfo());

        RevenueDetailsEdit revenueDetails = stayDetails.getRevenueDetails();
        verifyThat(revenueDetails.getTotalRoom().getLocalAmount(), hasAnyText());
        verifyThat(revenueDetails.getFood().getLocalAmount(), hasAnyText());
        verifyThat(revenueDetails.getBeverage().getLocalAmount(), hasAnyText());
        verifyThat(revenueDetails.getPhone().getLocalAmount(), hasAnyText());
        verifyThat(revenueDetails.getMeeting().getLocalAmount(), hasAnyText());
        verifyThat(revenueDetails.getMandatoryRevenue().getLocalAmount(), hasAnyText());
        verifyThat(revenueDetails.getOtherRevenue().getLocalAmount(), hasAnyText());
        verifyThat(revenueDetails.getNoRevenue().getLocalAmount(), hasAnyText());
        verifyThat(revenueDetails.getRoomTax().getLocalAmount(), hasAnyText());
        verifyThat(revenueDetails.getTotalRevenue().getLocalAmount(), hasAnyText());
        verifyThat(revenueDetails.getTotalNonQualifyingRevenue().getLocalAmount(), hasAnyText());
        verifyThat(revenueDetails.getTotalQualifyingRevenue().getLocalAmount(), hasAnyText());

        EventEarningDetailsTab earningDetailsTab = adjustStayPopUp.getEarningDetailsTab();
        earningDetailsTab.goTo();
        verifyNoErrors();

        verifyThat(earningDetailsTab.getEarningPreference(), hasText(RC_POINTS));

        EventEarningAmount totalIHGUnits = earningDetailsTab.getTotalIHGUnits();
        verifyThat(totalIHGUnits.getAmount(), allOf(hasAnyText(), not(hasText(NOT_AVAILABLE))));
        verifyThat(totalIHGUnits.getUnitType(), hasText(RC_POINTS));

        EventEarningAmount totalQualifyingUnits = earningDetailsTab.getTotalQualifyingUnits();
        verifyThat(totalQualifyingUnits.getAmount(), allOf(hasAnyText(), not(hasText(NOT_AVAILABLE))));
        verifyThat(totalQualifyingUnits.getUnitType(), hasText(RC_POINTS));

        verifyThat(earningDetailsTab.getTotalAllianceUnits().getAmount(), isDisplayedAndEnabled(false));
        verifyThat(earningDetailsTab.getTotalAllianceUnits().getUnitType(), hasText(NOT_AVAILABLE));

        verifyThat(earningDetailsTab.getGrid(), size(2));

        EventBillingDetailsTab billingDetailsTab = adjustStayPopUp.getBillingDetailsTab();
        billingDetailsTab.goTo();
        verifyNoErrors();

        EventBillingAmount amount = billingDetailsTab.getEventBillingDetail(1).getBaseLoyaltyUnits();

        verifyThat(amount.getAmount(), allOf(hasAnyText(), not(hasText(NOT_AVAILABLE))));
        verifyThat(amount.getCurrency(), hasText(Currency.USD.getCode()));
        verifyThat(amount.getBillingTo(), hasText(stayHotel));

        adjustStayPopUp.clickClose(assertNoError());
    }

    @Test
    public void verifyOrderSystem()
    {
        OrderEventsPage orderEventsPage = new OrderEventsPage();
        orderEventsPage.goTo();
        verifyNoErrors();

        orderEventsPage.getSearchFields().getItemId().typeAndWait(itemId);
        orderEventsPage.getButtonBar().clickSearch();

        verifyThat(orderEventsPage.getOrdersGrid(), size(1));

        OrderGridRow row = orderEventsPage.getOrdersGrid().getRow(1);

        GuestAddress address = new GuestAddress();
        address.setCountryCode(Country.US);
        address.setType(AddressType.RESIDENCE);
        address.setLine1("6358 VERNON WOODS DR");
        address.setLocality1("ATLANTA");
        address.setRegion1(Region.GE);
        address.setPostalCode("30328-3331");
        Order order = new Order();
        order.setDeliveryAddress(address);
        OrderItem orderItem = new OrderItem(itemId);
        orderItem.setItemName(orderItemName);
        orderItem.setDeliveryStatus("AT VENDOR");
        orderItem.setQuantity("1");
        order.getOrderItems().add(orderItem);
        order.setTransactionType(REDEEM);
        order.setOrderedDate(orderDate);
        order.setTransactionDate(orderTransDate);
        OrderRedeemGridRowContainer orderSystemDetailsGridRowContainer = new OrderRedeemGridRowContainer(row.expand());
        orderSystemDetailsGridRowContainer.getOrderDetails().verify(order.getOrderItems().get(0));
        orderSystemDetailsGridRowContainer.getShippingInfo().verify(order);
        orderSystemDetailsGridRowContainer.clickDetails();

        OrderDetailsPopUp orderDetailsPopUp = new OrderDetailsPopUp();
        verifyThat(orderDetailsPopUp, displayed(true));

        OrderDetailsTab orderDetailsTab = orderDetailsPopUp.getOrderDetailsTab();
        orderDetailsTab.goTo();
        verifyNoErrors();
        verifyThat(orderDetailsTab.getOrderDetails().getItemId(), hasText(itemId));

        EventEarningDetailsTab earningDetailsTab = orderDetailsPopUp.getEarningDetailsTab();
        earningDetailsTab.goTo();
        earningDetailsTab.verifyBasicDetailsNoPointsAndMiles();
        verifyThat(earningDetailsTab.getGrid().getRow(1).getCell(EventEarningGridRow.EventEarningCell.AWARD).asLink(),
                displayed(true));

        EventBillingDetailsTab billingDetailsTab = orderDetailsPopUp.getBillingDetailsTab();
        billingDetailsTab.goTo();
        verifyNoErrors();
        verifyThat(billingDetailsTab, hasText(containsString(REDEEM)));

        orderDetailsPopUp.clickClose(assertNoError());
    }

    @Test
    public void verifyOfferRegistration()
    {
        Offer CHASE_FREE_NIGHT = builder(OFFER_CODE, OFFER_NAME).description("Chase Anniversary Free Night")
                .freeNight("Yes").rateCategoryCode("IVCFN").maximumWinsAllowed("Unlimited").maxDaysToPlay("Unlimited")
                .status("Open").build();

        OfferEventPage offerEventPage = new OfferEventPage();
        offerEventPage.goTo();
        verifyNoErrors();

        offerEventPage.getSearchFields().getOfferCode().typeAndWait(OFFER_CODE);
        offerEventPage.getButtonBar().clickSearch();

        OfferPopUp offerPopUp = new OfferPopUp();
        verifyThat(offerPopUp, displayed(true));

        OfferDetailsTabExtended offerDetailsTab = offerPopUp.getOfferDetailsTab();
        offerDetailsTab.goTo();
        verifyNoErrors();

        OfferDetailsViewExtended offerDetails = offerDetailsTab.getOfferDetails();

        offerDetails.getOfferDetails().verify(CHASE_FREE_NIGHT);

        offerPopUp.clickClose(assertNoError());
    }

    @Test
    public void verifyRewardNight()
    {
        RewardNight rewardNight = new RewardNight();

        rewardNight.setCheckIn("30Mar15");
        rewardNight.setCheckOut("31Mar15");
        rewardNight.setConfirmationNumber("66877534");
        rewardNight.setCertificateNumber(certNumber);
        rewardNight.setHotel("ATLPR");
        rewardNight.setNights("1");
        rewardNight.setNumberOfRooms("1");
        rewardNight.setBookingSource(bookingSource);
        rewardNight.setTransactionDescription(NOT_AVAILABLE);
        rewardNight.setLoyaltyUnits("-15000");
        rewardNight.setTransactionDate(transactionDate);
        rewardNight.setTransactionType(REWARD_NIGHT);

        RewardNightEventPage rwrdNightEventPage = new RewardNightEventPage();
        rwrdNightEventPage.goTo();
        verifyNoErrors();

        rwrdNightEventPage.getSearchFields().verifyDefaults();

        RewardNightSearchButtonBar buttonBar = rwrdNightEventPage.getButtonBar();
        verifyThat(buttonBar.getClearCriteria(), isDisplayedAndEnabled(true));
        verifyThat(buttonBar.getSearch(), isDisplayedAndEnabled(true));
        verifyThat(buttonBar.getCreateNewRewardNight(), isDisplayedAndEnabled(true));

        RewardNightEventGridRow rewardNightEventGridRow = rwrdNightEventPage.getGrid().getRow(REWARD_NIGHT,
                rewardNight.getCertificateNumber());
        RewardNightGridRowContainer rewardNightGridRowContainer = new RewardNightGridRowContainer(
                rewardNightEventGridRow.expand());
        rewardNightGridRowContainer.verify(rewardNight.getConfirmationNumber(), "", REWARD_NIGHT,
                rewardNight.getCertificateNumber(), rewardNight.getNumberOfRooms());
        rewardNightGridRowContainer.getDetails().clickAndWait();
        RewardNightDetailsPopUp rewardNightDetailsPopUp = new RewardNightDetailsPopUp();
        rewardNightDetailsPopUp.getRewardNightDetailsTab().getDetails().verify(rewardNight);

        EventEarningDetailsTab earningTab = rewardNightDetailsPopUp.getEarningDetailsTab();
        earningTab.goTo();
        earningTab.verifyBasicDetailsForPoints("-15,000", "0");
        verifyThat(earningTab.getGrid(), size(not(0)));

        EventBillingDetailsTab billingTab = rewardNightDetailsPopUp.getBillingDetailsTab();
        billingTab.goTo();
        verifyNoErrors();
        verifyThat(billingTab, hasText(containsString(REWARD_NIGHT)));

        rewardNightDetailsPopUp.clickClose(assertNoError());
    }

    @Test
    public void verifyFreeNight()
    {
        FreeNightEventPage freeNightPage = new FreeNightEventPage();
        freeNightPage.goTo();
        verifyNoErrors();

        FreeNightGridRow offerRow = freeNightPage.getFreeNightGrid().getRowByOfferCode(OFFER_CODE);
        verifyThat(offerRow, displayed(true));
        verifyThat(offerRow.getCell(FreeNightGridRow.FreeNightCell.OFFER_NAME).asLink(), displayed(true));

        FreeNightDetailsPopUp freeNightPopUp = freeNightPage.getFreeNightGrid().getFreeNightDetailsPopUp(OFFER_CODE);
        verifyThat(freeNightPopUp, isDisplayedWithWait());

        FreeNightDetailsTab tab = freeNightPopUp.getFreeNightDetailsTab();
        tab.goTo();
        verifyThat(tab.getOfferCode(), hasText(OFFER_CODE));
        verifyThat(tab.getNameOfItem(), hasText(OFFER_NAME));

        verifyThat(tab.getFreeNightVoucherGrid(), size(not(0)));

        OfferDetailsFreeNightTab offerDetailsTab = freeNightPopUp.getOfferDetailsTab();
        offerDetailsTab.goTo();
        verifyThat(offerDetailsTab.getOfferDetails().getOfferDetails().getOfferCode(), hasText(OFFER_CODE));

        freeNightPopUp.clickClose(assertNoError());
    }

    @Test
    public void verifyCoPartner()
    {
        CoPartnerEventsPage coPartnerEventsPage = new CoPartnerEventsPage();
        coPartnerEventsPage.goTo();
        verifyNoErrors();

        coPartnerEventsPage.getSearchFields().getTransactionId().typeAndWait(CO_PARTNER_ID);
        coPartnerEventsPage.getButtonBar().clickSearch();

        CoPartnerEventGrid grid = coPartnerEventsPage.getGrid();
        verifyThat(grid, size(not(0)));
        CoPartnerEventGridRow row = grid.getRow(1);

        DepositGridRowContainer depositGridRowContainer = new DepositGridRowContainer(row.expand());
        DepositDetails depositDetails = depositGridRowContainer.getDepositDetails();
        verifyThat(depositDetails.getTransactionId(), hasText(CO_PARTNER_ID));
        verifyThat(depositDetails.getTransactionName(), hasAnyText());

        depositGridRowContainer.clickDetails();
        DepositDetailsPopUp depositDetailsPopUp = new DepositDetailsPopUp();

        DepositDetails depositPopUpDetails = depositDetailsPopUp.getDepositDetailsTab().getDepositDetails();
        verifyThat(depositPopUpDetails.getTransactionId(), hasText(CO_PARTNER_ID));
        verifyThat(depositPopUpDetails.getTransactionName(), hasAnyText());

        EventEarningDetailsTab eventEarningDetailsTab = depositDetailsPopUp.getEarningDetailsTab();
        eventEarningDetailsTab.goTo();
        eventEarningDetailsTab.verifyBasicDetailsForPoints("20", "20");
        verifyThat(eventEarningDetailsTab.getGrid(), size(not(0)));

        EventBillingDetailsTab eventBillingDetailsTab = depositDetailsPopUp.getBillingDetailsTab();
        eventBillingDetailsTab.goTo();
        verifyNoErrors();

        depositDetailsPopUp.close();
    }

    private void verifyStayDetails(StayGuestGridRowContainer rowContainer)
    {
        StayInfoView stayInfo = rowContainer.getStayInfo();
        verifyStayInfo(stayInfo);

        verifyThat(stayInfo.getCorpAcctNumber(), hasText(NOT_AVAILABLE));
        verifyThat(stayInfo.getIATANumber(), hasText(NOT_AVAILABLE));
        verifyThat(stayInfo.getHotelCurrency(), hasAnyText());

        verifyBookingInfo(rowContainer.getBookingInfo());

        RevenueDetailsView revenueDetails = rowContainer.getRevenueDetails();
        verifyThat(revenueDetails.getAverageRoomRate(), hasAnyText());
        verifyThat(revenueDetails.getTotalRoom(), hasAnyText());
        verifyThat(revenueDetails.getFood(), hasAnyText());
        verifyThat(revenueDetails.getBeverage(), hasAnyText());
        verifyThat(revenueDetails.getPhone(), hasAnyText());
        verifyThat(revenueDetails.getMeeting(), hasAnyText());
        verifyThat(revenueDetails.getMandatoryRevenue(), hasAnyText());
        verifyThat(revenueDetails.getOtherRevenue(), hasAnyText());
        verifyThat(revenueDetails.getNoRevenue(), hasAnyText());
        verifyThat(revenueDetails.getRoomTax(), hasAnyText());
        verifyThat(revenueDetails.getTotalRevenue(), hasAnyText());
        verifyThat(revenueDetails.getTotalNonQualifyingRevenue(), hasAnyText());
        verifyThat(revenueDetails.getTotalQualifyingRevenue(), hasAnyText());
    }

    private void verifyStayInfo(StayInfoBase stayInfo)
    {
        verifyThat(stayInfo.getCheckOut(), hasText(stayDate));
        verifyThat(stayInfo.getConfirmationNumber(), hasText(confirmationNumber));
        verifyThat(stayInfo.getFolioNumber(), hasText(folioNumber));
        verifyThat(stayInfo.getRoomNumber(), hasText(roomNumber));
        verifyThat(stayInfo.getOverlappingStay(), hasAnyText());
        verifyThat(stayInfo.getEnrollingStay(), hasText(IndicatorSelect.NO));
    }

    private void verifyBookingInfo(StayBookingInfoBase bookingInfo)
    {
        verifyThat(bookingInfo.getBookingDate(), hasAnyText());
        verifyThat(bookingInfo.getBookingSource(), hasAnyText());
        verifyThat(bookingInfo.getDataSource(), hasText("DCO"));
    }
}
