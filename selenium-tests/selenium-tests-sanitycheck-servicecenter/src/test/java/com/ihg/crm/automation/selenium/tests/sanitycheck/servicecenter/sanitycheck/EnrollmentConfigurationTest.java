package com.ihg.crm.automation.selenium.tests.sanitycheck.servicecenter.sanitycheck;

import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyNoError;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyNoErrors;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.types.program.Program.AMB;
import static com.ihg.automation.selenium.common.types.program.Program.BR;
import static com.ihg.automation.selenium.common.types.program.Program.EMP;
import static com.ihg.automation.selenium.common.types.program.Program.HTL;
import static com.ihg.automation.selenium.common.types.program.Program.IHG;
import static com.ihg.automation.selenium.common.types.program.Program.RC;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.components.Country;
import com.ihg.automation.selenium.common.contact.Address;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.servicecenter.pages.enroll.EarningPreference;
import com.ihg.automation.selenium.servicecenter.pages.enroll.EnrollmentPage;
import com.ihg.crm.automation.selenium.tests.sanitycheck.servicecenter.LoginLogout;

public class EnrollmentConfigurationTest extends LoginLogout
{
    private EnrollmentPage enrollmentPage = new EnrollmentPage();

    @BeforeClass
    public void before()
    {
        login();
    }

    @Test(priority = 1)
    public void autoPopulateCityState()
    {
        EnrollmentPage enrollmentPage = new EnrollmentPage();
        enrollmentPage.goTo();
        enrollmentPage.getPrograms().selectProgram(Program.RC);

        Address address = enrollmentPage.getAddress();
        address.setAddressConfiguration(Country.US);
        address.getCountry().select(Country.US);
        address.getZipCode().typeAndWait("10010");
        address.getAddress1().type("1 MADISON AVE");

        verifyThat(address.getLocality1(), hasText("NEW YORK"));
        verifyThat(address.getRegion1(), hasText("New York"));
    }

    @DataProvider(name = "programProvider")
    protected Object[][] programProvider()
    {
        return new Object[][] { { HTL, true, false, false, false, false }, { IHG, false, false, false, false, true },
                { RC, true, true, true, false, false }, { AMB, true, true, true, true, false },
                { BR, true, true, true, false, false }, { EMP, true, false, false, false, false } };
    }

    @Test(priority = 20, dataProvider = "programProvider")
    public void programConfigRC(Program program, boolean isAdditionalInfo, boolean isEarningPreference,
            boolean isRCOfferCode, boolean isAMBOfferCode, boolean isIHGOfferCode)
    {
        enrollmentPage.goTo();
        verifyNoErrors();
        enrollmentPage.getPrograms().selectProgram(program);
        enrollmentPage.verifyDefault();

        verifyThat(enrollmentPage.getAdditionalInformation(), displayed(isAdditionalInfo));

        EarningPreference earningPreferenceSeparator = enrollmentPage.getEarningPreferenceSeparator();
        verifyThat(earningPreferenceSeparator, displayed(isEarningPreference));
        verifyThat(earningPreferenceSeparator.getEarningPreference(), displayed(isEarningPreference));

        verifyThat(enrollmentPage.getRCOfferCode(), displayed(isRCOfferCode));
        verifyThat(enrollmentPage.getReferredMember().getRCReferredBy(), displayed(isRCOfferCode));
        verifyThat(enrollmentPage.getAMBOfferCode(), displayed(isAMBOfferCode));
        verifyThat(enrollmentPage.getIHGOfferCode(), displayed(isIHGOfferCode));
    }

    @AfterMethod
    public void clearProgram()
    {
        new EnrollmentPage().clickCancel(verifyNoError());
    }
}
