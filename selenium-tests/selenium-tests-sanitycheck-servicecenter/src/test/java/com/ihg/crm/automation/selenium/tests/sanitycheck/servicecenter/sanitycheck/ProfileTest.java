package com.ihg.crm.automation.selenium.tests.sanitycheck.servicecenter.sanitycheck;

import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyNoError;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyNoErrors;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.Constant.NOT_AVAILABLE;
import static com.ihg.automation.selenium.common.types.WrittenLanguage.ENGLISH;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasAnyText;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasAnyTextInView;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.isDisplayedWithWait;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static com.ihg.automation.selenium.matchers.component.IsDisplayed.displayed;
import static com.ihg.automation.selenium.matchers.component.ViewableMatcher.hasTextInView;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNot.not;
import static org.hamcrest.core.StringStartsWith.startsWith;

import org.springframework.beans.factory.annotation.Value;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.communication.ContactPermissionItems;
import com.ihg.automation.selenium.common.communication.MarketingPreferences;
import com.ihg.automation.selenium.common.communication.SmsMobileTextingMarketingPreferences;
import com.ihg.automation.selenium.common.components.Country;
import com.ihg.automation.selenium.common.contact.Address;
import com.ihg.automation.selenium.common.name.PersonNameFields;
import com.ihg.automation.selenium.common.preferences.TravelProfileType;
import com.ihg.automation.selenium.common.types.Gender;
import com.ihg.automation.selenium.common.types.address.AddressType;
import com.ihg.automation.selenium.common.types.program.ProgramStatus;
import com.ihg.automation.selenium.servicecenter.pages.GuestSearch;
import com.ihg.automation.selenium.servicecenter.pages.account.AccountInfoPage;
import com.ihg.automation.selenium.servicecenter.pages.account.Security;
import com.ihg.automation.selenium.servicecenter.pages.communication.CommunicationPreferences;
import com.ihg.automation.selenium.servicecenter.pages.communication.CommunicationPreferencesPage;
import com.ihg.automation.selenium.servicecenter.pages.personal.AddressContact;
import com.ihg.automation.selenium.servicecenter.pages.personal.CustomerInfoFields;
import com.ihg.automation.selenium.servicecenter.pages.personal.Passport;
import com.ihg.automation.selenium.servicecenter.pages.personal.PersonalInfoPage;
import com.ihg.automation.selenium.servicecenter.pages.preferences.EditTravelProfilePopUp;
import com.ihg.automation.selenium.servicecenter.pages.preferences.StayPreferencesPage;
import com.ihg.automation.selenium.servicecenter.pages.preferences.TravelProfile;
import com.ihg.automation.selenium.servicecenter.pages.preferences.TravelProfileGridRow;
import com.ihg.crm.automation.selenium.tests.sanitycheck.servicecenter.LoginLogout;

public class ProfileTest extends LoginLogout
{
    @Value("${fullName}")
    protected String fullName;

    @Value("${nameOnCard}")
    protected String nameOnCard;

    @Value("${email}")
    protected String email;

    @BeforeClass
    public void before()
    {
        login();
        new GuestSearch().byMemberNumber(memberId);
    }

    @Test
    public void validatePersonalInformation()
    {
        PersonalInfoPage personalInfoPage = new PersonalInfoPage();
        personalInfoPage.goTo();
        verifyNoErrors();

        CustomerInfoFields customerInfo = personalInfoPage.getCustomerInfo();
        verifyThat(customerInfo.getResidenceCountry(), hasText(Country.US.getValue()));
        PersonNameFields name = customerInfo.getName();
        name.setConfiguration(Country.US);
        verifyThat(name.getFullName(), hasText(fullName));
        verifyThat(customerInfo.getNameOnCard(), hasTextInView(nameOnCard));
        verifyThat(customerInfo.getGender(), hasTextInView(Gender.MALE.getValue()));
        verifyThat(customerInfo.getBirthDate().getFullDate(), hasAnyText());

        AddressContact addressContact = personalInfoPage.getAddressList().getExpandedContact();
        Address address = addressContact.getBaseContact();
        verifyThat(address.getType(), hasTextInView(AddressType.RESIDENCE.getValue()));
        address.setAddressConfiguration(Country.US);

        verifyThat(address.getCountry(), hasTextInView(Country.US.getValue()));
        verifyThat(address.getAddress1(), hasTextInView("6358 VERNON WOODS DR"));
        verifyThat(address.getRegion1(), hasTextInView("Georgia"));
        verifyThat(address.getLocality1(), hasTextInView("ATLANTA"));
        verifyThat(address.getZipCode(), hasTextInView("30328-3331"));

        verifyThat(personalInfoPage.getPhoneList().getExpandedContact().getBaseContact().getFullPhone(),
                hasText("(678) 488 8695"));
        verifyThat(personalInfoPage.getEmailList().getExpandedContact().getBaseContact().getEmail(),
                hasTextInView(email));

        Passport passport = personalInfoPage.getPassport();
        verifyThat(passport.getBirthCity(), hasTextInView(NOT_AVAILABLE));
        verifyThat(passport.getBirthCountry(), hasTextInView(NOT_AVAILABLE));
        verifyThat(passport.getIssuanceCountry(), hasTextInView(NOT_AVAILABLE));
        verifyThat(passport.getNumber(), hasTextInView(NOT_AVAILABLE));
    }

    @Test
    public void verifyCommunicationPreferences()
    {
        CommunicationPreferencesPage commPrefPage = new CommunicationPreferencesPage();
        commPrefPage.goTo();
        verifyNoErrors();
        CommunicationPreferences commPrefs = commPrefPage.getCommunicationPreferences();
        commPrefs.verify(ENGLISH);

        verifyThat(commPrefs.getThirdPartyShare(), hasAnyTextInView());
        verifyThat(commPrefs.getDoNotContactLabel(), hasText("No"));
        verifyThat(commPrefs.getDoNotCallLabel(), hasText("No"));

        MarketingPreferences marketingPrefs = commPrefs.getMarketingPreferences();
        verifyThat(marketingPrefs.getEmailAddress(), hasTextInView(email));
        verifyThat(commPrefs.getMarketingPreferences().getInvalidEmailImage(), displayed(false));

        ContactPermissionItems marketingPermissionItems = marketingPrefs.getContactPermissionItems();
        int marketingItemsCount = marketingPermissionItems.getContactPermissionItemsCount();
        verifyThat(marketingPrefs, marketingItemsCount, not(is(0)), "There are items in marketing preferences");

        SmsMobileTextingMarketingPreferences smsMobileTextingMarketingPreferences = commPrefs
                .getSmsMobileTextingMarketingPreferences();
        verifyThat(smsMobileTextingMarketingPreferences.getSmsNumber(), hasTextInView("17706565439"));
        verifyThat(smsMobileTextingMarketingPreferences.getResend(), displayed(true));
        verifyThat(smsMobileTextingMarketingPreferences.getSMSNumberConfirmationStatus(), hasAnyText());

        ContactPermissionItems smsPermissionItems = smsMobileTextingMarketingPreferences.getContactPermissionItems();
        int smsItemsCount = smsPermissionItems.getContactPermissionItemsCount();
        verifyThat(smsPermissionItems, smsItemsCount, not(is(0)), "There are items in sms preferences");

        verifyThat(commPrefs.getCustomerService(), displayed(true));

        verifyThat(commPrefs.getComments(), hasTextInView(NOT_AVAILABLE));
    }

    @Test
    public void validateAccountInformation()
    {
        AccountInfoPage accountInfoPage = new AccountInfoPage();
        accountInfoPage.goTo();
        verifyNoErrors();
        accountInfoPage.verify(ProgramStatus.OPEN);

        Security secur = accountInfoPage.getSecurity();
        verifyThat(secur.getLockout(), hasTextInView("Unlocked"));
        verifyThat(secur.getPin(), hasAnyTextInView());
        verifyThat(secur.getUserName(), hasText(email));
        verifyThat(accountInfoPage.getFlags(), hasText(startsWith("No flags selected")));
    }

    @Test
    public void verifyStayPreferencesTabMember()
    {
        TravelProfile travelProfile = TravelProfile.getDefaultTravelProfile();
        StayPreferencesPage stayPreferencesPage = new StayPreferencesPage();
        stayPreferencesPage.goTo();
        verifyNoErrors();
        TravelProfileGridRow row = stayPreferencesPage.getTravelProfilesGrid()
                .getRowByName(TravelProfileType.LEISURE.getValue());
        row.verify(travelProfile);
        EditTravelProfilePopUp editTravelProfilePopUp = row.openTravelProfile();
        verifyThat(editTravelProfilePopUp.getDefaultIndicator(), isDisplayedWithWait());
        editTravelProfilePopUp.clickClose(verifyNoError());
    }
}
