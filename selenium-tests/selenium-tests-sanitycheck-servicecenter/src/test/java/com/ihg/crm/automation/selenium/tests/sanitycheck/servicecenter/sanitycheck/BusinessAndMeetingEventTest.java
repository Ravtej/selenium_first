package com.ihg.crm.automation.selenium.tests.sanitycheck.servicecenter.sanitycheck;

import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyNoError;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyNoErrors;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.Constant.NOT_AVAILABLE;
import static com.ihg.automation.selenium.common.event.EventTransactionType.Meeting.MEETING;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.isDisplayedWithWait;
import static com.ihg.automation.selenium.matchers.component.GridSize.size;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static com.ihg.automation.selenium.matchers.component.IsDisplayed.displayed;
import static com.ihg.automation.selenium.matchers.component.IsEnabled.enabled;

import org.springframework.beans.factory.annotation.Value;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.Constant;
import com.ihg.automation.selenium.common.MeetingDetails;
import com.ihg.automation.selenium.common.billing.EventBillingDetail;
import com.ihg.automation.selenium.common.billing.EventBillingDetailsTab;
import com.ihg.automation.selenium.common.business.BusinessRewardsEventDetailsPopUp;
import com.ihg.automation.selenium.common.business.DiscretionaryPointsFields;
import com.ihg.automation.selenium.common.business.EventInformationFields;
import com.ihg.automation.selenium.common.business.EventStaysGrid;
import com.ihg.automation.selenium.common.business.GuestRoomsFields;
import com.ihg.automation.selenium.common.business.MeetingFields;
import com.ihg.automation.selenium.common.business.MeetingRevenueDetails;
import com.ihg.automation.selenium.common.business.RoomsTab;
import com.ihg.automation.selenium.common.business.StaySearch;
import com.ihg.automation.selenium.common.earning.EventEarningDetailsTab;
import com.ihg.automation.selenium.common.earning.EventEarningGrid;
import com.ihg.automation.selenium.common.event.SearchButtonBar;
import com.ihg.automation.selenium.servicecenter.pages.GuestSearch;
import com.ihg.automation.selenium.servicecenter.pages.event.business.BusinessEventsGrid;
import com.ihg.automation.selenium.servicecenter.pages.event.business.BusinessEventsPage;
import com.ihg.automation.selenium.servicecenter.pages.event.meeting.MeetingEventDetailsPopUp;
import com.ihg.automation.selenium.servicecenter.pages.event.meeting.MeetingEventDetailsTab;
import com.ihg.automation.selenium.servicecenter.pages.event.meeting.MeetingEventGrid;
import com.ihg.automation.selenium.servicecenter.pages.event.meeting.MeetingEventGridRow;
import com.ihg.automation.selenium.servicecenter.pages.event.meeting.MeetingEventGridRowContainer;
import com.ihg.automation.selenium.servicecenter.pages.event.meeting.MeetingEventPage;
import com.ihg.automation.selenium.servicecenter.pages.event.meeting.MeetingEventRow;
import com.ihg.automation.selenium.servicecenter.pages.event.meeting.MeetingSearch;
import com.ihg.crm.automation.selenium.tests.sanitycheck.servicecenter.LoginLogout;

public class BusinessAndMeetingEventTest extends LoginLogout
{
    @Value("${memberWithBusinessEvent}")
    protected String memberWithBusinessEvent;

    @Value("${businessEventId}")
    protected String businessEventId;

    private BusinessEventsPage businessEventsPage;
    private BusinessRewardsEventDetailsPopUp popUp;
    private EventInformationFields eventInformationFields;

    @BeforeClass
    public void before()
    {
        login();
        new GuestSearch().byMemberNumber(memberWithBusinessEvent);
    }

    @Test(priority = 10)
    public void verifySearchEvent()
    {
        businessEventsPage = new BusinessEventsPage();
        businessEventsPage.goTo();
        verifyNoErrors();

        businessEventsPage.getSearchFields().getEventID().typeAndWait(businessEventId);
        businessEventsPage.getButtonBar().clickSearch();

        BusinessEventsGrid grid = businessEventsPage.getGrid();
        verifyThat(grid, size(1));

        verifyThat(businessEventsPage.getGrid().getRowById(businessEventId), displayed(true));

    }

    @Test(priority = 20)
    public void verifyEventInformationSectionOnCreateEvent()
    {
        businessEventsPage = new BusinessEventsPage();
        popUp = businessEventsPage.createEvent();

        eventInformationFields = popUp.getEventSummaryTab().getEventInformation();

        verifyThat(eventInformationFields.getHotel(), displayed(true));
        verifyThat(eventInformationFields.getHotelCurrency(), displayed(true));
        verifyThat(eventInformationFields.getEventContractDate(), displayed(true));
        verifyThat(eventInformationFields.getEventName(), displayed(true));
        verifyThat(eventInformationFields.getHotelContactEmail(), displayed(true));
        verifyThat(eventInformationFields.getHotelContactName(), displayed(true));
        verifyThat(eventInformationFields.getHotelContactPhone(), displayed(true));
    }

    @Test(priority = 30)
    public void verifyMeetingSectionOnCreateEvent()
    {
        MeetingFields meetingFields = popUp.getEventSummaryTab().getEventDetails().getMeetingFields();
        meetingFields.expand();

        verifyThat(meetingFields.getIncludeMeeting(), displayed(true));
        verifyThat(meetingFields.getStartDate(), displayed(true));
        verifyThat(meetingFields.getEndDate(), displayed(true));
        verifyThat(meetingFields.getNumberOfMeetingRooms(), displayed(true));
        verifyThat(meetingFields.getTotalDelegates(), displayed(true));

        MeetingRevenueDetails meetingRevenueDetail = meetingFields.getRevenueDetails();
        meetingRevenueDetail.getMeetingRoom().verifyDefault();
        meetingRevenueDetail.getFoodAndBeverage().verifyDefault();
        meetingRevenueDetail.getMiscellaneous().verifyDefault();
        meetingRevenueDetail.getTotalMeetingRevenue().verifyDefault();
    }

    @Test(priority = 30)
    public void verifyGuestRoomsSectionOnCreateEvent()
    {
        GuestRoomsFields guestRoomsFields = popUp.getEventSummaryTab().getEventDetails().getGuestRoomsFields();
        guestRoomsFields.expand();
        guestRoomsFields.getEligibleRoomRevenue().verifyDefault();
        guestRoomsFields.getTotalRoom().verifyDefault();

        verifyThat(guestRoomsFields.getCorporateID(), displayed(true));
        verifyThat(guestRoomsFields.getIATANumber(), displayed(true));
        verifyThat(guestRoomsFields.getIncludeGuestRoomsRevenue(), displayed(true));
        verifyThat(guestRoomsFields.getTotalGuestRooms(), displayed(true));
        verifyThat(guestRoomsFields.getStayEndDate(), displayed(true));
        verifyThat(guestRoomsFields.getStayStartDate(), displayed(true));
        verifyThat(guestRoomsFields.getTotalGuests(), displayed(true));
        verifyThat(guestRoomsFields.getTotalRoomNights(), displayed(true));
    }

    @Test(priority = 30)
    public void verifyTotalEventRevenueSectionOnCreateEvent()
    {
        popUp.getEventSummaryTab().getTotalEligibleRevenue().verifyDefault();
    }

    @Test(priority = 30)
    public void verifyDiscretionaryPointsSectionOnCreateEvent()
    {
        DiscretionaryPointsFields discretionaryPointsFields = popUp.getEventSummaryTab().getDiscretionaryPoints();
        discretionaryPointsFields.expand();

        verifyThat(discretionaryPointsFields.getAdditionalPointsToAward(), displayed(true));
        verifyThat(discretionaryPointsFields.getComments(), displayed(true));
        verifyThat(discretionaryPointsFields.getIncludeDiscretionaryPoints(), displayed(true));
        verifyThat(discretionaryPointsFields.getReason(), displayed(true));
        verifyThat(discretionaryPointsFields.getEstimatedCost(), hasText(NOT_AVAILABLE));
    }

    @Test(priority = 40)
    public void verifyRoomsTab()
    {
        eventInformationFields = popUp.getEventSummaryTab().getEventInformation();

        RoomsTab roomTab = popUp.getRoomsTab();
        roomTab.goTo();

        EventStaysGrid eventStaysGrid = roomTab.getEventStaysGrid();
        verifyThat(eventStaysGrid, displayed(true));
        eventStaysGrid.verifyHeader();

        StaySearch staySearch = roomTab.getSearchFields();

        verifyThat(staySearch.getGuestName(), displayed(true));
        verifyThat(staySearch.getConfirmationNumber(), displayed(true));
        verifyThat(staySearch.getToDate(), displayed(true));
        verifyThat(staySearch.getFromDate(), displayed(true));

        popUp.clickCancel(verifyNoError());
    }

    @Test(priority = 50, groups = { "meetingGroup" })
    public void verifyMeetingEvent()
    {
        MeetingDetails meetingDetails = new MeetingDetails("ATLCP", "06May13", "09May13", "55539", Constant.RC_POINTS,
                "100");
        meetingDetails.setMeetingEventName("Selective Insurance Marshberry Sales Pro Classic");
        meetingDetails.setTotalNumberOfRoomNights("108");
        meetingDetails.setTotalQualifiedNonRoomsRevenue("23,526.20 USD");
        meetingDetails.setNegotiatedRoomRate("125.00 USD");
        meetingDetails.setTotalRoomsRevenue("13,500.00 USD");

        MeetingEventRow meetingEventRow = new MeetingEventRow(meetingDetails);
        meetingEventRow.setTransDate("17May13");

        MeetingEventPage meetingEventsPage = new MeetingEventPage();

        meetingEventsPage.goTo();

        verifyThat(meetingEventsPage, isDisplayedWithWait());
        MeetingSearch searchFields = meetingEventsPage.getSearchFields();
        searchFields.verifyDefaults();
        verifyThat(meetingEventsPage.getCreateMeeting(), enabled(false));
        verifyThat(meetingEventsPage.getCreateMeetingBonus(), enabled(false));

        SearchButtonBar buttonBar = meetingEventsPage.getButtonBar();
        verifyThat(buttonBar.getClearCriteria(), displayed(true));
        verifyThat(buttonBar.getSearch(), displayed(true));

        searchFields.getMeetingEventName().typeAndWait(meetingDetails.getMeetingEventName());
        buttonBar.clickSearch();

        MeetingEventGrid grid = meetingEventsPage.getGrid();
        verifyThat(grid, size(1));

        MeetingEventGridRow row = grid.getRow(meetingEventRow);
        row.verify(meetingEventRow);

        MeetingEventGridRowContainer meetingEventContainer = new MeetingEventGridRowContainer(row.expand());

        meetingEventContainer.getMeetingDetails().verifyWithoutSalesManagerDetails(meetingDetails);

        meetingEventContainer.getDetails().clickAndWait();

        MeetingEventDetailsPopUp meetingEventDetailsPopUp = new MeetingEventDetailsPopUp();
        verifyThat(meetingEventDetailsPopUp, isDisplayedWithWait());

        MeetingEventDetailsTab tab = meetingEventDetailsPopUp.getMeetingDetailsTab();
        tab.goTo();
        tab.getDetails().verifyWithoutSalesManagerDetails(meetingDetails);

        EventEarningDetailsTab eventEarningDetailsTab = meetingEventDetailsPopUp.getEarningDetailsTab();
        eventEarningDetailsTab.goTo();

        eventEarningDetailsTab.verifyBasicDetailsForPoints("55,539", "0");

        EventEarningGrid earningGrid = eventEarningDetailsTab.getGrid();
        verifyThat(earningGrid, size(1));

        EventBillingDetailsTab billingTab = meetingEventDetailsPopUp.getBillingDetailsTab();
        billingTab.goTo();
        EventBillingDetail eventBillingDetail = billingTab.getEventBillingDetail(1);
        verifyThat(eventBillingDetail.getBillingDate(), hasText("May 17, 2013"));
        verifyThat(eventBillingDetail.getEventType(), hasText(MEETING));
        eventBillingDetail.getBaseLoyaltyUnits().verifyUSDAmount("222.15", "ATLCP");
    }
}
