package com.ihg.crm.automation.selenium.tests.servicecenter.programinfo;

import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifySuccess;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.Constant.NOT_AVAILABLE;
import static com.ihg.automation.selenium.common.event.EventTransactionType.TierLevel.DOWNGRADE_MANUAL;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.isDisplayedWithWait;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static com.ihg.automation.selenium.servicecenter.pages.personal.PersonalInfoPage.SUCCESS_MESSAGE;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.common.DateUtils;
import com.ihg.automation.selenium.common.event.AllEventsRow;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.common.types.program.ProgramStatus;
import com.ihg.automation.selenium.common.types.program.RewardClubLevel;
import com.ihg.automation.selenium.common.types.program.RewardClubLevelReason;
import com.ihg.automation.selenium.gwt.components.input.Select;
import com.ihg.automation.selenium.servicecenter.pages.LeftPanel;
import com.ihg.automation.selenium.servicecenter.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsGridRow;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsPage;
import com.ihg.automation.selenium.servicecenter.pages.event.level.ManualTierLevelChangeGridRowContainer;
import com.ihg.automation.selenium.servicecenter.pages.programs.ProgramSummary;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.RewardClubPage;
import com.ihg.crm.automation.selenium.tests.servicecenter.LoginLogout;

public class DowngradeRCTierLevelFromGoldTest extends LoginLogout
{
    private Member member = new Member();
    private RewardClubPage rewardClubPage = new RewardClubPage();
    private AllEventsPage allEventsPage = new AllEventsPage();
    private AllEventsRow downgradePlatinumEventRow;

    @BeforeClass
    public void beforeClass()
    {
        member.setPersonalInfo(MemberPopulateHelper.getSimplePersonalInfo());
        member.addAddress(MemberPopulateHelper.getUnitedStatesAddress());
        member.addProgram(Program.RC);

        login();

        member = new EnrollmentPage().enroll(member);
        rewardClubPage.goTo();
        rewardClubPage.getSummary().setTierLevelWithExpiration(RewardClubLevel.GOLD, RewardClubLevelReason.BASE,
                verifySuccess(SUCCESS_MESSAGE));

        downgradePlatinumEventRow = new AllEventsRow(DateUtils.getFormattedDate(), DOWNGRADE_MANUAL, "RC - CLUB", "",
                "");
    }

    @Test
    public void downgradeTierLevelToCLUB()
    {
        rewardClubPage = new RewardClubPage();
        ProgramSummary summary = rewardClubPage.getSummary();
        summary.clickEdit();
        summary.getTierLevel().selectByCode(RewardClubLevel.CLUB);

        Select tierLevelReason = summary.getTierLevelReason();
        verifyThat(tierLevelReason, isDisplayedWithWait());
        verifyThat(tierLevelReason, hasText("Select"));
        summary.clickSave();
        verifyThat(summary.getTierLevelReason(), displayed(false));
    }

    @Test(dependsOnMethods = { "downgradeTierLevelToCLUB" }, alwaysRun = true)
    public void verifyResultingTierLevel()
    {
        rewardClubPage = new RewardClubPage();
        rewardClubPage.getSummary().verify(ProgramStatus.OPEN, RewardClubLevel.CLUB);
        verifyThat(new LeftPanel().getProgramInfoPanel().getPrograms().getRewardClubProgram().getLevelCode(),
                hasText(RewardClubLevel.CLUB.getCode()));
    }

    @Test(dependsOnMethods = { "verifyResultingTierLevel" }, alwaysRun = true)
    public void verifyAllEventsGridRow()
    {
        allEventsPage.goTo();
        AllEventsGridRow row = allEventsPage.getGrid().getRow(downgradePlatinumEventRow.getTransType(),
                downgradePlatinumEventRow.getDetail());
        row.verify(downgradePlatinumEventRow);
    }

    @Test(dependsOnMethods = { "verifyAllEventsGridRow" }, alwaysRun = true)
    public void verifyAllEventsGridRowDetails()
    {
        allEventsPage.goTo();
        AllEventsGridRow row = allEventsPage.getGrid().getRow(downgradePlatinumEventRow.getTransType(),
                downgradePlatinumEventRow.getDetail());
        ManualTierLevelChangeGridRowContainer container = row.expand(ManualTierLevelChangeGridRowContainer.class);
        container.verify(NOT_AVAILABLE, NOT_AVAILABLE, NOT_AVAILABLE, helper);
    }

}
