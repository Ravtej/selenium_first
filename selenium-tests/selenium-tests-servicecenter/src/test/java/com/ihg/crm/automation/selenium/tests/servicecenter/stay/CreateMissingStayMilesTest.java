package com.ihg.crm.automation.selenium.tests.servicecenter.stay;

import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyError;
import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyNoError;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.assertThat;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.Constant.BASE_UNITS;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.automation.selenium.matchers.component.GridSize.size;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static com.ihg.automation.selenium.matchers.component.ViewableMatcher.hasTextInView;
import static com.ihg.automation.selenium.matchers.text.AsFormattedDouble.asFormattedDouble;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.ProgramInfoPanel;
import com.ihg.automation.selenium.common.billing.EventBillingDetailsTab;
import com.ihg.automation.selenium.common.earning.EventEarningDetailsTab;
import com.ihg.automation.selenium.common.event.EarningEvent;
import com.ihg.automation.selenium.common.member.AlliancePopulateHelper;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberAlliance;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.stay.Revenues;
import com.ihg.automation.selenium.common.stay.Stay;
import com.ihg.automation.selenium.common.types.Alliance;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.servicecenter.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.servicecenter.pages.event.stay.AdjustStayPopUp;
import com.ihg.automation.selenium.servicecenter.pages.event.stay.CreateStayPopUp;
import com.ihg.automation.selenium.servicecenter.pages.event.stay.StayDetailsAdjust;
import com.ihg.automation.selenium.servicecenter.pages.event.stay.StayDetailsCreate;
import com.ihg.automation.selenium.servicecenter.pages.event.stay.StayEventsPage;
import com.ihg.automation.selenium.servicecenter.pages.event.stay.StayGuestGrid;
import com.ihg.automation.selenium.servicecenter.pages.event.stay.StayInfoEdit;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.RewardClubPage;
import com.ihg.crm.automation.selenium.tests.servicecenter.LoginLogout;

public class CreateMissingStayMilesTest extends LoginLogout
{
    private StayEventsPage stayEventsPage = new StayEventsPage();
    private AdjustStayPopUp adjustPopUp = new AdjustStayPopUp();

    private Member member = new Member();
    private Stay stay = new Stay();

    private static final String NIGHTS = "2";
    private static final int NIGHTS_INT = Integer.valueOf(NIGHTS);
    private Alliance alliance = Alliance.DJA;
    private String allianceNumber = AlliancePopulateHelper.getRandomDJA().getNumber();
    private String MILES_FOR_STAY = "600";

    private void initMember()
    {
        member.addProgram(Program.RC);
        member.setPersonalInfo(MemberPopulateHelper.getSimplePersonalInfo());
        member.addAddress(MemberPopulateHelper.getUnitedStatesAddress());
        member.setMilesEarningPreference(new MemberAlliance(alliance, allianceNumber, member.getName()));
    }

    private void initStay()
    {
        stay.setHotelCode("CEQHA");
        stay.setCheckInOutByNights(NIGHTS_INT);
        stay.setRateCode("TEST");
        stay.setAvgRoomRate(100.0);
        stay.setEarningPreference(alliance.getValue());
        stay.setTierLevelNights(NIGHTS);
        stay.setQualifyingNights(NIGHTS_INT);
        stay.setAllianceUnits(MILES_FOR_STAY);
        Revenues revenues = stay.getRevenues();
        revenues.getFood().setAmount(50.0);
    }

    @BeforeClass
    public void before()
    {
        initMember();
        initStay();

        login();

        new EnrollmentPage().enroll(member);

        assertThat(new ProgramInfoPanel().getPrograms().getRewardClubProgram().getEarningPreference(),
                hasText(alliance.getCode()));
    }

    @AfterClass
    public void removeAlliance()
    {
        new RewardClubPage().removeAlliance(alliance);
    }

    @Test
    public void createMissingStayForMiles()
    {
        stayEventsPage.goTo();

        stayEventsPage.getButtonBar().clickCreateStay(verifyNoError());

        CreateStayPopUp popUp = new CreateStayPopUp();
        StayDetailsCreate stayDetails = popUp.getStayDetails();

        verifyThat(stayDetails.getStayInfo().getEarningPreference(), hasText(alliance.getValue()));

        popUp.getStayDetails().populate(stay);
        popUp.clickCreateStay(verifyNoError());
    }

    @Test(dependsOnMethods = { "createMissingStayForMiles" }, alwaysRun = true)
    public void expandStayEvent()
    {
        stayEventsPage.waitUntilFirstStayProcess();
        StayGuestGrid guestGrid = stayEventsPage.getGuestGrid();
        verifyThat(guestGrid, size(1));

        guestGrid.getRow(1).getDetails()
                .clickDetails(verifyError("Stay has Alliance Miles Earning. Stay Adjustment is not allowed."));

        StayDetailsAdjust stayDetails = adjustPopUp.getStayDetailsTab().getStayDetails();

        StayInfoEdit stayInfo = stayDetails.getStayInfo();
        verifyThat(stayInfo.getHotel(), hasText(stay.getHotelCode()));
        verifyThat(stayInfo.getCheckIn(), hasTextInView(stay.getCheckIn()));
        verifyThat(stayInfo.getCheckOut(), hasTextInView(stay.getCheckOut()));
        verifyThat(stayInfo.getAvgRoomRate(), hasTextInView(asFormattedDouble(stay.getAvgRoomRate())));
        verifyThat(stayInfo.getEarningPreference(), hasTextInView(alliance.getValue()));

        verifyThat(adjustPopUp.getSaveChanges(), displayed(false));
    }

    @Test(dependsOnMethods = { "expandStayEvent" }, alwaysRun = true)
    public void verifyEarningDetails()
    {
        EventEarningDetailsTab earningDetailsTab = adjustPopUp.getEarningDetailsTab();
        earningDetailsTab.goTo();

        earningDetailsTab.verifyBasicDetailsForMiles(alliance, MILES_FOR_STAY);

        EarningEvent event = new EarningEvent(BASE_UNITS, MILES_FOR_STAY, "0", alliance.getValue());
        event.setStatus("Pending");
        earningDetailsTab.getGrid().getRowByType(BASE_UNITS).verify(event);
    }

    @Test(dependsOnMethods = { "verifyEarningDetails" }, alwaysRun = true)
    public void verifyBillingDetails()
    {
        EventBillingDetailsTab billingDetailsTab = adjustPopUp.getBillingDetailsTab();
        billingDetailsTab.goTo();

        billingDetailsTab.getEventBillingDetail(1).getBaseLoyaltyUnits().verifyUSDAmount("11.87", stay.getHotelCode());
    }

    @Test(dependsOnMethods = { "verifyBillingDetails" }, alwaysRun = true)
    public void verifyEventGrid()
    {
        adjustPopUp.clickClose();

        stayEventsPage.getGuestGrid().getRow(1).verify(stay);
    }
}
