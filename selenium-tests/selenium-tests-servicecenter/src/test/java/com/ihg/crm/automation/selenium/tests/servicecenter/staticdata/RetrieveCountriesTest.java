package com.ihg.crm.automation.selenium.tests.servicecenter.staticdata;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.HasSelectItems.hasSelectItems;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.components.Country;
import com.ihg.automation.selenium.common.contact.Address;
import com.ihg.automation.selenium.common.enroll.CustomerInfo;
import com.ihg.automation.selenium.common.types.address.RegionLabel;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.servicecenter.pages.GuestSearch;
import com.ihg.automation.selenium.servicecenter.pages.enroll.EnrollmentPage;
import com.ihg.crm.automation.selenium.tests.servicecenter.LoginLogout;

public class RetrieveCountriesTest extends LoginLogout
{
    private ArrayList<String> expectedCountryList;
    private ArrayList<String> expectedArgentinaSubList;
    private ArrayList<String> expectedUkrainSubList;
    private EnrollmentPage enrollPage = new EnrollmentPage();
    private CustomerInfo customerInfo;
    private GuestSearch.CustomerSearch customerSearch = new GuestSearch().getCustomerSearch();
    private final static String COUNTRY_SQL = "SELECT countries.CTRY_CD ||' - '|| countries.CTRY_NM AS RESULT"//
            + " FROM REFDATA.CTRY  countries"//
            + " WHERE countries.ACTV_IND='Y'";
    private final static String SUBDIVISION_COUNTRY_SQL = "SELECT SUBDIV_CD ||' - '|| SUBDIV_1_NM AS RESULT"//
            + " FROM REFDATA.CTRY_SUBDIV"//
            + " WHERE CTRY_CD = '%s'";

    @BeforeClass
    public void beforeClass()
    {
        expectedCountryList = getCountryList();
        expectedArgentinaSubList = getCountrySubList(Country.AR);
        expectedUkrainSubList = getCountrySubList(Country.UA);

        login();
    }

    @DataProvider(name = "countryProvider")
    protected Object[][] countryProvider()
    {
        return new Object[][] { { Country.AR, expectedArgentinaSubList }, { Country.UA, expectedUkrainSubList } };
    }

    @Test
    public void verifyCountryDropDownOnCustomerSearch()
    {
        verifyThat(customerSearch.getCountry(), hasSelectItems(expectedCountryList));
    }

    @Test(dependsOnMethods = { "verifyCountryDropDownOnCustomerSearch" })
    public void verifyCountryDropDownOnEnrollmentPage()
    {
        enrollPage.goTo();

        customerInfo = enrollPage.getCustomerInfo();
        verifyThat(customerInfo.getResidenceCountry(), hasSelectItems(expectedCountryList));
    }

    @Test(dependsOnMethods = { "verifyCountryDropDownOnEnrollmentPage" }, dataProvider = "countryProvider")
    public void verifyArgentinaSubdivisionDropDownForUS(Country country, ArrayList<String> expectedList)
    {
        enrollPage.goTo();
        enrollPage.getPrograms().selectProgram(Program.RC);
        customerInfo = enrollPage.getCustomerInfo();
        customerInfo.getResidenceCountry().select(country);

        Address address = enrollPage.getAddress();
        verifyThat(address.getRegion1(RegionLabel.PROVINCE), hasSelectItems(expectedList));
    }

    private ArrayList<String> getCountryList()
    {
        List<Map<String, Object>> listMap = jdbcTemplate.queryForList(COUNTRY_SQL);

        ArrayList<String> list = new ArrayList<String>();

        for (int i = 0; i < listMap.size(); i++)
        {
            list.add(listMap.get(i).get("RESULT").toString());
        }

        return list;
    }

    private ArrayList<String> getCountrySubList(Country country)
    {
        List<Map<String, Object>> listMap = jdbcTemplate
                .queryForList(String.format(SUBDIVISION_COUNTRY_SQL, country.getCode()));

        ArrayList<String> list = new ArrayList<String>();

        for (int i = 0; i < listMap.size(); i++)
        {
            list.add(listMap.get(i).get("RESULT").toString());
        }

        return list;
    }
}
