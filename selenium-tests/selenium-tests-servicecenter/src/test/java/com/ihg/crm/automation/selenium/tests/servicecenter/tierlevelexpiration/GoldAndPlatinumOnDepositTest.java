package com.ihg.crm.automation.selenium.tests.servicecenter.tierlevelexpiration;

import static com.ihg.automation.selenium.common.types.program.ProgramStatus.OPEN;
import static com.ihg.automation.selenium.common.types.program.RewardClubLevel.CLUB;
import static com.ihg.automation.selenium.common.types.program.RewardClubLevel.GOLD;
import static com.ihg.automation.selenium.common.types.program.RewardClubLevel.PLTN;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.deposit.Deposit;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.servicecenter.pages.LeftPanel;
import com.ihg.automation.selenium.servicecenter.pages.deposit.CreateDepositPopUp;
import com.ihg.automation.selenium.servicecenter.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.RewardClubPage;
import com.ihg.crm.automation.selenium.tests.servicecenter.LoginLogout;

public class GoldAndPlatinumOnDepositTest extends LoginLogout
{
    private RewardClubPage rewardClubPage;
    private Deposit goldCurrentYearDeposit = new Deposit();

    @BeforeClass
    public void beforeClass()
    {
        Member member = new Member();
        member.setPersonalInfo(MemberPopulateHelper.getSimplePersonalInfo());
        member.addAddress(MemberPopulateHelper.getUnitedStatesAddress());
        member.addProgram(Program.RC);

        goldCurrentYearDeposit.setTransactionId("PTSEUG");
        goldCurrentYearDeposit.setHotel("ATLCP");

        login();
        new EnrollmentPage().enroll(member);
        new LeftPanel().verifyRCLevel(CLUB);
    }

    @Test
    public void verifyGoldCurrentYearTierLevel()
    {
        new CreateDepositPopUp().createDeposit(goldCurrentYearDeposit, false);

        rewardClubPage = new RewardClubPage();
        rewardClubPage.goTo();
        rewardClubPage.getSummary().verify(OPEN, GOLD, 0);
    }

    @Test(dependsOnMethods = { "verifyGoldCurrentYearTierLevel" }, alwaysRun = true)
    public void verifyGoldLifetimeTierLevel()
    {
        Deposit goldLifetimeDeposit = new Deposit();
        goldLifetimeDeposit.setTransactionId("VGLDRC");
        goldLifetimeDeposit.setHotel("ATLCP");

        new CreateDepositPopUp().createDeposit(goldLifetimeDeposit, false);

        rewardClubPage = new RewardClubPage();
        rewardClubPage.goTo();
        rewardClubPage.getSummary().verifyLifetimeTierLevel(OPEN, GOLD);
    }

    @Test(dependsOnMethods = { "verifyGoldLifetimeTierLevel" }, alwaysRun = true)
    public void verifyPlatinumFollowingYearTierLevel()
    {
        Deposit platinumFollowingYearDeposit = new Deposit();
        platinumFollowingYearDeposit.setTransactionId("IHGPLT");
        platinumFollowingYearDeposit.setHotel("ATLCP");

        new CreateDepositPopUp().createDeposit(platinumFollowingYearDeposit, false);

        rewardClubPage = new RewardClubPage();
        rewardClubPage.goTo();
        rewardClubPage.getSummary().verify(OPEN, PLTN, 1);
    }

    @Test(dependsOnMethods = { "verifyPlatinumFollowingYearTierLevel" }, alwaysRun = true)
    public void verifyPlatinumLifetimeTierLevel()
    {
        Deposit platinumLifetimeDeposit = new Deposit();
        platinumLifetimeDeposit.setTransactionId("IHGVIP");
        platinumLifetimeDeposit.setHotel("ATLCP");

        new CreateDepositPopUp().createDeposit(platinumLifetimeDeposit, false);

        rewardClubPage = new RewardClubPage();
        rewardClubPage.goTo();
        rewardClubPage.getSummary().verifyLifetimeTierLevel(OPEN, PLTN);
    }
}
