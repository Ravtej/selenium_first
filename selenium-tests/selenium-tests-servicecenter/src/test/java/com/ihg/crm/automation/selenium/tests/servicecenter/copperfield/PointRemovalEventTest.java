package com.ihg.crm.automation.selenium.tests.servicecenter.copperfield;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.Constant.NOT_AVAILABLE;
import static com.ihg.automation.selenium.common.event.EventTransactionType.PointExpiration.EXPIRED_POINTS_REMOVAL;
import static com.ihg.automation.selenium.matchers.component.GridSize.size;
import static org.apache.commons.lang3.StringUtils.EMPTY;

import java.util.Map;

import org.joda.time.LocalDate;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.common.DateUtils;
import com.ihg.automation.selenium.common.Constant;
import com.ihg.automation.selenium.common.billing.EventBillingDetailsTab;
import com.ihg.automation.selenium.common.earning.EventEarningDetailsTab;
import com.ihg.automation.selenium.common.event.AllEventsRow;
import com.ihg.automation.selenium.common.event.EarningEvent;
import com.ihg.automation.selenium.common.event.EventTransactionType;
import com.ihg.automation.selenium.common.event.Source;
import com.ihg.automation.selenium.servicecenter.pages.GuestSearch;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsGrid;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsGridRow;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsPage;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsSearch;
import com.ihg.automation.selenium.servicecenter.pages.event.point.ExpiredPointsRemovalDetailsPopUp;
import com.ihg.automation.selenium.servicecenter.pages.event.point.PointExpirationRowContainer;
import com.ihg.crm.automation.selenium.tests.servicecenter.LoginLogout;

@Test(groups = { "copperfield" })
public class PointRemovalEventTest extends LoginLogout
{
    private AllEventsRow eventsRow;
    private PointExpirationRowContainer rowContainer;

    private LocalDate eventDate;

    private final static Source BATCH_SOURCE = new Source("BATCH", NOT_AVAILABLE, NOT_AVAILABLE, NOT_AVAILABLE,
            "MBATCH");

    private final static String SELECT_MEMBER_SQL = "SELECT /*+ INDEX_DESC(a MBRSHP_ACTV_LST_UPDT_TS_IDX)*/ "//
            + " a.MBRSHP_ID, TO_CHAR(a.MPE_XACT_DT, 'DDMonYY') AS EVENT_DATE, e.MEMBER_EVENT_PNTS"//
            + " FROM DGST.MBRSHP_ACTV a"//
            + " JOIN FMDS.MBR_POINT_EVENT e ON "//
            + " a.MPE_MBRSHP_ID = e.MEMBERSHIP_ID "//
            + " AND a.MPE_XACT_DT = e.TRANSACTION_DATE "//
            + " AND a.MPE_XACT_TM = e.TRANSACTION_TIME"//
            + " AND a.MPE_PT_EVN_TYP = e.POINT_EVENT_TYPE "//
            + " AND a.MPE_MBR_PE_SEQ_NBR = e.MBR_PE_SEQ_NBR"//
            + " WHERE MPE_PT_EVN_TYP='EXPPR'"//
            + " AND (LST_UPDT_TS BETWEEN ADD_MONTHS(SYSDATE, - 12) AND SYSDATE)"//
            + " AND NOT EXISTS("//
            + "  SELECT 1 FROM DGST.MBRSHP_ACTV b"//
            + "  WHERE a.DGST_MSTR_KEY = b.DGST_MSTR_KEY"//
            + "  AND a.MPE_PT_EVN_TYP = b.MPE_PT_EVN_TYP"//
            + "  AND a.MBRSHP_ACTV_KEY <> b.MBRSHP_ACTV_KEY)"//
            + " AND ROWNUM < 2";

    @BeforeClass
    public void beforeClass()
    {
        Map<String, Object> map = jdbcTemplate.queryForMap(SELECT_MEMBER_SQL);

        String memberId = map.get("MBRSHP_ID").toString();
        eventDate = DateUtils.getStringToDate(map.get("EVENT_DATE").toString());
        String pointAmount = map.get("MEMBER_EVENT_PNTS").toString();

        eventsRow = new AllEventsRow(eventDate.toString(DateUtils.DATE_FORMAT), EXPIRED_POINTS_REMOVAL,
                "Expired Points Removal", pointAmount, EMPTY);

        login();

        new GuestSearch().byMemberNumber(memberId);
    }

    @Test
    public void verifyExpiredPointsRemovalEventRow()
    {
        AllEventsPage allEventsPage = new AllEventsPage();
        allEventsPage.goTo();
        AllEventsSearch searchFields = allEventsPage.getSearchFields();
        searchFields.getEventType().select(EventTransactionType.PointExpiration.EVENT_TYPE);
        searchFields.getTransactionType().select(EXPIRED_POINTS_REMOVAL);
        allEventsPage.getButtonBar().clickSearch();

        AllEventsGrid grid = allEventsPage.getGrid();
        verifyThat(grid, size(1));

        AllEventsGridRow row = grid.getRow(1);
        row.verify(eventsRow);

        rowContainer = row.expand(PointExpirationRowContainer.class);
        rowContainer.getSource().verify(BATCH_SOURCE, NOT_AVAILABLE);
    }

    @Test(dependsOnMethods = { "verifyExpiredPointsRemovalEventRow" })
    public void verifyExpiredPointsRemovalEventDetails()
    {
        rowContainer.clickDetails();

        ExpiredPointsRemovalDetailsPopUp removalDetailsPopUp = new ExpiredPointsRemovalDetailsPopUp();
        removalDetailsPopUp.getPointExpirationDetailsTab().verify(eventsRow, BATCH_SOURCE);

        EventEarningDetailsTab earningDetailsTab = removalDetailsPopUp.getEarningDetailsTab();
        earningDetailsTab.goTo();
        earningDetailsTab.verifyBasicDetailsForPoints(eventsRow.getIhgUnits(), "0");
        EarningEvent baseUnits = new EarningEvent(Constant.BASE_UNITS, eventsRow.getIhgUnits(), "0",
                Constant.RC_POINTS);
        baseUnits.setDateEarned(eventDate);
        earningDetailsTab.getGrid().getRow(1).verify(baseUnits);

        EventBillingDetailsTab billingDetailsTab = removalDetailsPopUp.getBillingDetailsTab();
        billingDetailsTab.goTo();
        billingDetailsTab.verifyTabIsEmpty(eventDate, EXPIRED_POINTS_REMOVAL);
    }

}
