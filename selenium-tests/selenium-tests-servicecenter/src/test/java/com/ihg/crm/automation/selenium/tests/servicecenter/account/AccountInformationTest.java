package com.ihg.crm.automation.selenium.tests.servicecenter.account;

import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyError;
import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyNoError;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.compareLists;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.types.program.ProgramStatus.OPEN;
import static com.ihg.automation.selenium.common.types.program.ProgramStatus.REMOVED;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.isDisplayedWithWait;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static com.ihg.automation.selenium.matchers.component.ViewableMatcher.hasTextInView;
import static org.hamcrest.CoreMatchers.containsString;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.CustomerInfoFlags;
import com.ihg.automation.selenium.common.CustomerInfoPanel;
import com.ihg.automation.selenium.common.ProgramInfoPanel;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.common.types.program.TierLevelFlag;
import com.ihg.automation.selenium.gwt.components.Component;
import com.ihg.automation.selenium.gwt.components.dialog.ConfirmDialog;
import com.ihg.automation.selenium.servicecenter.pages.GuestSearch;
import com.ihg.automation.selenium.servicecenter.pages.LeftPanel;
import com.ihg.automation.selenium.servicecenter.pages.TopMenu;
import com.ihg.automation.selenium.servicecenter.pages.account.AccountInfoPage;
import com.ihg.automation.selenium.servicecenter.pages.account.Flags;
import com.ihg.automation.selenium.servicecenter.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.servicecenter.pages.history.ProfileHistoryGridRow;
import com.ihg.automation.selenium.servicecenter.pages.history.ProfileHistoryPage;
import com.ihg.automation.selenium.servicecenter.pages.programs.EmployeePage;
import com.ihg.automation.selenium.servicecenter.pages.programs.GuestAccountPage;
import com.ihg.automation.selenium.servicecenter.pages.programs.ProgramPageBase;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.RewardClubPage;
import com.ihg.automation.selenium.servicecenter.pages.unit.PurchaseUnitsPopUp;
import com.ihg.crm.automation.selenium.tests.servicecenter.LoginLogout;

public class AccountInformationTest extends LoginLogout
{
    private static final String ACCOUNT_INFORMATION = "Account Information";
    private static final String ACCOUNT_STATUS = "Account Status";
    private String testMemberId;
    private EmployeePage emplPage = new EmployeePage();
    private GuestAccountPage gstAccPage = new GuestAccountPage();
    private RewardClubPage rwdPage = new RewardClubPage();
    private ProgramInfoPanel prInfoPanel = new ProgramInfoPanel();

    private static final String expectedBalance = "0";
    private static final String PURCHASE_AMOUNT = "21000";
    private static final Set<String> editFlags = new HashSet<String>(
            Arrays.asList(TierLevelFlag.POSSIBLE_FRAUD.getValue(), TierLevelFlag.HAND_RAISER_FOR_ENROLLMENT.getValue(),
                    TierLevelFlag.CISCO.getValue(), TierLevelFlag.GENERAL_VIP.getValue()));

    @DataProvider(name = "accountProvider")
    protected Object[][] accountProvider()
    {
        return new Object[][] { { emplPage }, { gstAccPage }, { rwdPage } };
    }

    @BeforeClass
    public void beforeClass()
    {
        login();

        Member member = new Member();
        member.addProgram(Program.RC, Program.IHG, Program.EMP);
        member.setPersonalInfo(MemberPopulateHelper.getSimplePersonalInfo());
        member.addAddress(MemberPopulateHelper.getUnitedStatesAddress());

        new EnrollmentPage().enroll(member);
        testMemberId = new LeftPanel().getCustomerInfoPanel().getMemberNumber(Program.RC).getText();
        new PurchaseUnitsPopUp().purchaseComplimentary(PURCHASE_AMOUNT);
    }

    @Test
    public void addProfileFlags()
    {
        AccountInfoPage accountInfo = new AccountInfoPage();
        accountInfo.goTo();

        Flags flags = accountInfo.getFlags();
        flags.clickEdit();
        flags.getFlags().select(editFlags);
        flags.clickSave();

        for (String flag : editFlags)
        {
            verifyThat(accountInfo, hasText(containsString(flag)));
        }
    }

    @Test(dependsOnMethods = { "addProfileFlags" })
    public void verifyProfileHistoryAfterAddingFlags()
    {
        ProfileHistoryPage historyPage = new ProfileHistoryPage();
        historyPage.goTo();
        verifyNoError();

        ProfileHistoryGridRow accountInfoRow = historyPage.getProfileHistoryGrid().getRow(1)
                .getSubRowByName(ACCOUNT_INFORMATION);

        for (String flag : editFlags)
        {
            accountInfoRow.getSubRowByName(flag).verify("Not set", "Set");
        }
    }

    @Test(dependsOnMethods = { "verifyProfileHistoryAfterAddingFlags" })
    public void verifyCustomerInfoPanelFlags()
    {
        AccountInfoPage accountInfo = new AccountInfoPage();
        accountInfo.goTo();

        CustomerInfoFlags infoFlags = new CustomerInfoPanel().getFlags();
        compareLists(infoFlags, editFlags, infoFlags.getFlags());
    }

    @Test(dependsOnMethods = { "verifyCustomerInfoPanelFlags" })
    public void removeAccount()
    {
        AccountInfoPage accountInfo = new AccountInfoPage();
        accountInfo.goTo();
        accountInfo.clickRemoveAccount();

        ConfirmDialog dialog = new ConfirmDialog();
        verifyThat(dialog, hasText("Customer Loyalty Unit Balance will be reduced to 0. Do you want to proceed?"));
        dialog.clickYes();

        verifyThat(accountInfo.getAccountStatus(), hasText(REMOVED));
        verifyThat(accountInfo.getReinstateAccount(), displayed(true));
    }

    @Test(dependsOnMethods = { "removeAccount" }, groups = { "verifyRemoved" })
    public void verifyHistoryAfterRemovingAccount()
    {
        ProfileHistoryPage historyPage = new ProfileHistoryPage();
        historyPage.goTo();
        verifyNoError();

        historyPage.getProfileHistoryGrid().getRow(1).getSubRowByName(ACCOUNT_INFORMATION)
                .getSubRowByName(ACCOUNT_STATUS).verify(OPEN, REMOVED);
    }

    @Test(dependsOnMethods = { "verifyHistoryAfterRemovingAccount" })
    public void selectGoodwillUnitsOnRemovedAccount()
    {
        TopMenu menu = new TopMenu();
        menu.getUnitManagement().getGoodwillUnits()
                .clickAndWait(verifyError("RC Membership Status is Removed. This Transaction is not allowed."));
    }

    @Test(dependsOnMethods = { "removeAccount" })
    public void searchRemovedAccount()
    {
        LeftPanel panel = new LeftPanel();
        panel.clickNewSearch();

        ConfirmDialog dialog = new ConfirmDialog();

        verifyThat(dialog, isDisplayedWithWait());
        verifyThat(dialog, hasText("Are you done assisting this guest?"));
        dialog.clickYes();

        new GuestSearch().byMemberNumber(testMemberId);

        Component memNumber = new LeftPanel().getCustomerInfoPanel().getMemberNumber(Program.RC);
        verifyThat(memNumber, isDisplayedWithWait());
        verifyThat(memNumber, hasText(testMemberId));
    }

    @Test(dependsOnMethods = { "searchRemovedAccount" }, dependsOnGroups = { "verifyRemoved" }, alwaysRun = true)
    public void reinstateAccount()
    {
        AccountInfoPage accountInfo = new AccountInfoPage();
        accountInfo.goTo();
        accountInfo.clickReinstateAccount();

        verifyThat(accountInfo.getAccountStatus(), hasText(OPEN));
        verifyThat(accountInfo.getRemoveAccount(), displayed(true));
    }

    @Test(dependsOnMethods = { "reinstateAccount" })
    public void verifyHistoryAfterReinstateAccount()
    {
        ProfileHistoryPage historyPage = new ProfileHistoryPage();
        historyPage.goTo();
        verifyNoError();

        historyPage.getProfileHistoryGrid().getRow(1).getSubRowByName(ACCOUNT_INFORMATION)
                .getSubRowByName(ACCOUNT_STATUS).verify(REMOVED, OPEN);
    }

    @Test(dataProvider = "accountProvider", dependsOnMethods = { "removeAccount" }, groups = { "verifyRemoved" })
    public <T extends ProgramPageBase> void verifyRemovedAccountStatuses(T page)
    {
        verifyAccountStatus(page, REMOVED);
    }

    @Test(dataProvider = "accountProvider", dependsOnMethods = { "reinstateAccount" })
    public <T extends ProgramPageBase> void verifyReinstatedAccountStatuses(T page)
    {
        verifyAccountStatus(page, OPEN);
    }

    @Test(dependsOnMethods = { "removeAccount" }, groups = { "verifyRemoved" })
    public void verifyRemovedAccountBalance()
    {
        verifyAccountBalance();
    }

    @Test(dependsOnMethods = { "reinstateAccount" })
    public void verifyReinstatedAccountBalance()
    {
        verifyAccountBalance();
    }

    private <T extends ProgramPageBase> void verifyAccountStatus(T page, String status)
    {
        page.goTo();
        verifyThat(page.getSummary().getStatus(), hasTextInView(status));
    }

    public void verifyAccountBalance()
    {
        rwdPage.goTo();
        verifyThat(prInfoPanel.getPrograms().getRewardClubProgram().getPointsBalance(), hasText(expectedBalance));
        verifyThat(rwdPage.getSummary().getBalance(), hasText(expectedBalance));
    }
}
