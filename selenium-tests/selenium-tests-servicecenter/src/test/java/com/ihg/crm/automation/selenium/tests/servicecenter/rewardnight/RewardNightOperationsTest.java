package com.ihg.crm.automation.selenium.tests.servicecenter.rewardnight;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.Constant.NOT_AVAILABLE;
import static com.ihg.automation.selenium.common.event.EarningEventFactory.REDEEM;
import static com.ihg.automation.selenium.common.event.EventTransactionType.RewardNight.REWARD_NIGHT;
import static com.ihg.automation.selenium.common.event.EventTransactionType.RewardNight.REWARD_NIGHT_CANCELED;
import static com.ihg.automation.selenium.common.history.EventAuditConstant.CREATE;
import static com.ihg.automation.selenium.common.history.EventAuditConstant.RewardNight.CHECKIN_DATE;
import static com.ihg.automation.selenium.common.history.EventAuditConstant.RewardNight.CHECKOUT_DATE;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.enabled;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasEmptyText;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.isDisplayedWithWait;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.isHighlightedAsInvalid;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static org.hamcrest.core.Is.is;

import org.hamcrest.core.AnyOf;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.common.DateUtils;
import com.ihg.automation.common.RandomUtils;
import com.ihg.automation.selenium.common.Constant;
import com.ihg.automation.selenium.common.billing.EventBillingDetailsTab;
import com.ihg.automation.selenium.common.earning.EventEarningDetailsTab;
import com.ihg.automation.selenium.common.event.AllEventsRow;
import com.ihg.automation.selenium.common.event.EarningEvent;
import com.ihg.automation.selenium.common.event.EarningEventFactory;
import com.ihg.automation.selenium.common.event.Source;
import com.ihg.automation.selenium.common.history.EventAuditConstant;
import com.ihg.automation.selenium.common.history.EventHistoryGridRow;
import com.ihg.automation.selenium.common.history.EventHistoryRecords;
import com.ihg.automation.selenium.common.history.EventHistoryTab;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.reward.RewardNight;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.servicecenter.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsGridRow;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsPage;
import com.ihg.automation.selenium.servicecenter.pages.event.rewardnight.CreateRewardNightPopUp;
import com.ihg.automation.selenium.servicecenter.pages.event.rewardnight.ReservationCancellationPopUp;
import com.ihg.automation.selenium.servicecenter.pages.event.rewardnight.RewardNightDetails;
import com.ihg.automation.selenium.servicecenter.pages.event.rewardnight.RewardNightDetailsPopUp;
import com.ihg.automation.selenium.servicecenter.pages.event.rewardnight.RewardNightDetailsPopUpGridRow;
import com.ihg.automation.selenium.servicecenter.pages.event.rewardnight.RewardNightDetailsTab;
import com.ihg.automation.selenium.servicecenter.pages.event.rewardnight.RewardNightEventGrid.RewardNightCell;
import com.ihg.automation.selenium.servicecenter.pages.event.rewardnight.RewardNightEventGridRow;
import com.ihg.automation.selenium.servicecenter.pages.event.rewardnight.RewardNightEventPage;
import com.ihg.automation.selenium.servicecenter.pages.event.rewardnight.RewardNightEventRow;
import com.ihg.automation.selenium.servicecenter.pages.event.rewardnight.RewardNightGridRowContainer;
import com.ihg.automation.selenium.servicecenter.pages.unit.GoodwillPopUp;
import com.ihg.crm.automation.selenium.tests.servicecenter.LoginLogout;

public class RewardNightOperationsTest extends LoginLogout
{
    private String CERTIFICATE_NUMBER = "";
    private final static String CURRENT_DATE = DateUtils.getFormattedDate();
    private Member member = new Member();
    private RewardNight rewardNight = new RewardNight();
    private EarningEvent rewardNightEarningEvent = new EarningEvent(REDEEM, "-30,000", "0", Constant.RC_POINTS);
    private EarningEvent earningEvent;
    private AllEventsRow allEventsRewardNightRow = new AllEventsRow(DateUtils.getFormattedDate(), REWARD_NIGHT, null);
    private EventEarningDetailsTab eventEarningDetailsTab;
    private RewardNightEventPage rwrdNightEventPage = new RewardNightEventPage();
    private RewardNightEventGridRow rewardNightEventGridRow;
    private RewardNightGridRowContainer rewardNightGridRowContainer;
    private CreateRewardNightPopUp createRwrdNightPopUp = new CreateRewardNightPopUp();
    private RewardNightDetailsPopUp rewardNightDetailsPopUp = new RewardNightDetailsPopUp();

    private RewardNightEventRow rewardNightEventRow = new RewardNightEventRow();
    private AllEventsPage allEvntsPage = new AllEventsPage();
    private Source unitSource;

    @BeforeClass
    public void beforeClass()
    {
        rewardNight.setCheckIn(DateUtils.getDateBackward(2));
        rewardNight.setCheckOut(DateUtils.getDateBackward(1));
        String confNumber = RandomUtils.getRandomNumber(8, false);
        rewardNight.setConfirmationNumber(confNumber);
        rewardNight.setHotel("ATLFX");
        rewardNight.setNights("1");
        rewardNight.setNumberOfRooms("1");
        rewardNight.setRoomType("BBL");

        rewardNightEventRow.setTransactionType(REWARD_NIGHT);
        rewardNightEventRow.setCheckInDate(rewardNight.getCheckIn());
        rewardNightEventRow.setCheckOutDate(rewardNight.getCheckOut());
        rewardNightEventRow.setHotel(rewardNight.getHotel());
        rewardNightEventRow.setIhgUnits("-30,000");
        rewardNightEventRow.setNights("1");

        rewardNightEarningEvent.setAward("HQ300");

        earningEvent = EarningEventFactory.getBaseUnitsEvent("30,000", "0");
        earningEvent.setAward("HQ300");
        earningEvent.setOrderNumber(confNumber);

        allEventsRewardNightRow.setDetail("ATLFX " + rewardNight.getCheckIn() + " - " + rewardNight.getCheckOut() + " "
                + rewardNight.getNights() + " Night");
        allEventsRewardNightRow.setIhgUnits("-30000");

        member.setPersonalInfo(MemberPopulateHelper.getSimplePersonalInfo());
        member.addAddress(MemberPopulateHelper.getUnitedStatesAddress());
        member.addProgram(Program.RC);

        login();
        new EnrollmentPage().enroll(member);
        new GoodwillPopUp().goodwillPoints("50000");
    }

    @Test
    public void openCreateRewardNightsDialog()
    {
        rwrdNightEventPage.openRewardNightsDialog();
    }

    @Test(dependsOnMethods = { "openCreateRewardNightsDialog" })
    public void verifyCreateRewardNightsDialogControls()
    {
        verifyThat(createRwrdNightPopUp.getCheckInDate(), enabled(true));
        verifyThat(createRwrdNightPopUp.getCheckOutDate(), enabled(true));
        verifyThat(createRwrdNightPopUp.getConfirmationNumber(), enabled(true));
        verifyThat(createRwrdNightPopUp.getHotel(), enabled(true));
        verifyThat(createRwrdNightPopUp.getNumberOfRooms(), enabled(true));
        verifyThat(createRwrdNightPopUp.getRoomType(), enabled(true));
        verifyThat(createRwrdNightPopUp.getNights(), enabled(false));
        verifyThat(createRwrdNightPopUp.getHotel(), hasText("Hotel"));
        verifyThat(createRwrdNightPopUp.getCheckInDate(), hasText("ddMMMyy"));
        verifyThat(createRwrdNightPopUp.getCheckOutDate(), hasText("ddMMMyy"));
        verifyThat(createRwrdNightPopUp.getConfirmationNumber(), hasEmptyText());
        verifyThat(createRwrdNightPopUp.getNumberOfRooms(), hasEmptyText());
        verifyThat(createRwrdNightPopUp.getRoomType(), hasEmptyText());
        verifyThat(createRwrdNightPopUp.getNights(), hasEmptyText());
    }

    @Test(dependsOnMethods = { "verifyCreateRewardNightsDialogControls" }, alwaysRun = true)
    public void verifyEmptyFieldsRewardNightCreation()
    {
        createRwrdNightPopUp.clickCreateRewardNight();

        verifyThat(createRwrdNightPopUp.getCheckInDate(), isHighlightedAsInvalid(true));
        verifyThat(createRwrdNightPopUp.getCheckOutDate(), isHighlightedAsInvalid(true));
        verifyThat(createRwrdNightPopUp.getConfirmationNumber(), isHighlightedAsInvalid(true));
        verifyThat(createRwrdNightPopUp.getHotel(), isHighlightedAsInvalid(true));
        verifyThat(createRwrdNightPopUp.getNumberOfRooms(), isHighlightedAsInvalid(true));
        verifyThat(createRwrdNightPopUp.getRoomType(), isHighlightedAsInvalid(false));
        verifyThat(createRwrdNightPopUp.getNights(), isHighlightedAsInvalid(false));
    }

    @Test(dependsOnMethods = { "verifyEmptyFieldsRewardNightCreation" }, alwaysRun = true)
    public void closeCreateRewardNightsPopUp()
    {
        createRwrdNightPopUp.close();
    }

    @Test(dependsOnMethods = { "closeCreateRewardNightsPopUp" }, alwaysRun = true)
    public void reopenCreateRewardClubPopUp()
    {
        rwrdNightEventPage.openRewardNightsDialog();
    }

    @Test(dependsOnMethods = { "reopenCreateRewardClubPopUp" })
    public void createRewardNight()
    {
        unitSource = new Source(NOT_AVAILABLE, NOT_AVAILABLE, NOT_AVAILABLE, NOT_AVAILABLE,
                helper.getUser().getUserId());

        createRwrdNightPopUp = new CreateRewardNightPopUp();
        createRwrdNightPopUp.create(rewardNight);
        verifyThat(rwrdNightEventPage, isDisplayedWithWait());
    }

    @Test(dependsOnMethods = { "createRewardNight" }, alwaysRun = true)
    public void getCertificateNumber()
    {
        rwrdNightEventPage.goTo();
        rewardNightEventGridRow = rwrdNightEventPage.getGrid().getRow(REWARD_NIGHT);
        CERTIFICATE_NUMBER = rewardNightEventGridRow.getCell(RewardNightCell.CERTIFICATE_NUMBER).getText();
    }

    @Test(dependsOnMethods = { "getCertificateNumber" }, alwaysRun = true)
    public void verifyRewardNightEventRow()
    {
        rewardNightEventGridRow = rwrdNightEventPage.getGrid().getRow(REWARD_NIGHT);
        rewardNightEventGridRow.verify(rewardNightEventRow);
    }

    @Test(dependsOnMethods = { "verifyRewardNightEventRow" }, alwaysRun = true)
    public void verifyRewardNightEventRowDetails()
    {
        rewardNightEventGridRow = rwrdNightEventPage.getGrid().getRow(REWARD_NIGHT);
        rewardNightGridRowContainer = rewardNightEventGridRow.expand(RewardNightGridRowContainer.class);
        rewardNightGridRowContainer.verify(rewardNight.getConfirmationNumber(), rewardNight.getRoomType(), REWARD_NIGHT,
                CERTIFICATE_NUMBER, rewardNight.getNights(), unitSource);
    }

    @Test(dependsOnMethods = { "verifyRewardNightEventRowDetails" }, alwaysRun = true)
    public void openRewardNightEventDetailsPopUp()
    {
        rewardNightGridRowContainer.clickDetails();
        verifyThat(rewardNightDetailsPopUp, isDisplayedWithWait());
    }

    @Test(dependsOnMethods = { "openRewardNightEventDetailsPopUp" })
    public void verifyRewardNightEventDetailsTab()
    {
        RewardNightDetailsTab tab = rewardNightDetailsPopUp.getRewardNightDetailsTab();
        tab.goTo();

        rewardNight.setTransactionDate(CURRENT_DATE);
        rewardNight.setTransactionType(REWARD_NIGHT);
        rewardNight.setCertificateNumber(CERTIFICATE_NUMBER);
        rewardNight.setBookingSource("SCUI");
        rewardNight.setLoyaltyUnits("-30000");
        rewardNight.setTransactionDescription(NOT_AVAILABLE);

        tab.getDetails().verify(rewardNight);

        RewardNightDetailsPopUpGridRow row = tab.getGrid().getRow(1);
        row.verify(AnyOf.anyOf(is("INITIATED"), is("APPROVED")), "1", "-30000");
    }

    @Test(dependsOnMethods = { "openRewardNightEventDetailsPopUp" })
    public void verifyRewardNightEventEarningDetailsTab()
    {
        EventEarningDetailsTab tab = rewardNightDetailsPopUp.getEarningDetailsTab();
        tab.goTo();
        tab.verifyBasicDetailsForPoints("-30,000", "0");
        tab.getGrid().verify(rewardNightEarningEvent);
    }

    @Test(dependsOnMethods = { "openRewardNightEventDetailsPopUp" })
    public void verifyRewardNightEventBillingDetailsTab()
    {
        EventBillingDetailsTab tab = rewardNightDetailsPopUp.getBillingDetailsTab();
        tab.goTo();
        tab.verifyTabIsEmpty(REWARD_NIGHT);
    }

    @Test(dependsOnMethods = { "openRewardNightEventDetailsPopUp" })
    public void verifyRewardNightEventHistoryTab()
    {
        EventHistoryTab historyTab = rewardNightDetailsPopUp.getHistoryTab();
        historyTab.goTo();

        EventHistoryGridRow historyRow = historyTab.getGrid().getRow(1);
        historyRow.verify(CREATE, helper.getUser().getUserId());

        EventHistoryRecords stayRecords = historyRow.getAllRecords();
        stayRecords.getRecord(EventAuditConstant.RewardNight.CERTIFICATE_NUMBER).verifyAdd(CERTIFICATE_NUMBER);
        stayRecords.getRecord(CHECKIN_DATE).verifyAdd(rewardNightEventRow.getCheckInDate());
        stayRecords.getRecord(CHECKOUT_DATE).verifyAdd(rewardNightEventRow.getCheckOutDate());
    }

    @Test(dependsOnMethods = { "verifyRewardNightEventBillingDetailsTab", "verifyRewardNightEventEarningDetailsTab",
            "verifyRewardNightEventDetailsTab", "verifyRewardNightEventHistoryTab" }, alwaysRun = true)
    public void closeRewardNightEventDetailsPopUp()
    {
        rewardNightDetailsPopUp.close();
    }

    @Test(dependsOnMethods = { "closeRewardNightEventDetailsPopUp" }, alwaysRun = true)
    public void verifyAllEventsTabRewardNightEvent()
    {
        allEvntsPage.goTo();
        AllEventsGridRow row = allEvntsPage.getGrid().getRow(REWARD_NIGHT, allEventsRewardNightRow.getDetail());
        row.verify(allEventsRewardNightRow);
    }

    @Test(dependsOnMethods = { "verifyAllEventsTabRewardNightEvent" }, alwaysRun = true)
    public void cancelReservation()
    {
        rwrdNightEventPage = new RewardNightEventPage();
        rwrdNightEventPage.goTo();
        rewardNightEventGridRow = rwrdNightEventPage.getGrid().getRow(REWARD_NIGHT, CERTIFICATE_NUMBER);
        rewardNightGridRowContainer = rewardNightEventGridRow.expand(RewardNightGridRowContainer.class);
        rewardNightGridRowContainer.getDetails().clickAndWait();
        rewardNightDetailsPopUp = new RewardNightDetailsPopUp();
        verifyThat(rewardNightDetailsPopUp, isDisplayedWithWait());
        ReservationCancellationPopUp popUp = rewardNightDetailsPopUp.clickCancelRewardNight();
        verifyThat(popUp, isDisplayedWithWait());
        popUp.getCancellationNumber().typeAndWait(rewardNight.getConfirmationNumber());
        popUp.clickSubmitNoError();
        verifyThat(popUp, displayed(false));
    }

    @Test(dependsOnMethods = { "cancelReservation" })
    public void verifyCanceledRewardNightEventRow()
    {
        rewardNightEventRow.setTransactionType(REWARD_NIGHT_CANCELED);
        rewardNightEventRow.setIhgUnits("0");

        rwrdNightEventPage = new RewardNightEventPage();
        rewardNightEventGridRow = rwrdNightEventPage.getGrid().getRow(REWARD_NIGHT_CANCELED);
        rewardNightEventGridRow.verify(rewardNightEventRow);
    }

    @Test(dependsOnMethods = { "verifyCanceledRewardNightEventRow" }, alwaysRun = true)
    public void verifyCanceledRewardNightEventRowDetails()
    {
        rewardNightGridRowContainer = rewardNightEventGridRow.expand(RewardNightGridRowContainer.class);
        rewardNightGridRowContainer.verify(rewardNight.getConfirmationNumber(), rewardNight.getRoomType(),
                REWARD_NIGHT_CANCELED, CERTIFICATE_NUMBER, rewardNight.getNights(), unitSource);
    }

    @Test(dependsOnMethods = { "verifyCanceledRewardNightEventRowDetails" }, alwaysRun = true)
    public void verifyRewardNightEventDetailsPopUpControls()
    {
        rewardNightDetailsPopUp = new RewardNightDetailsPopUp();
        rewardNightGridRowContainer.clickDetails();
        verifyThat(rewardNightDetailsPopUp, isDisplayedWithWait());
        RewardNightDetails details = rewardNightDetailsPopUp.getRewardNightDetailsTab().getDetails();
        verifyThat(details.getCheckInDate(), enabled(false));
        verifyThat(details.getCheckOutDate(), enabled(false));
        verifyThat(details.getNights(), enabled(false));
        verifyThat(details.getNumberOfRooms(), enabled(false));
    }

    @Test(dependsOnMethods = { "verifyRewardNightEventDetailsPopUpControls" }, alwaysRun = true)
    public void verifyRewardNightEventEarningDetails()
    {
        eventEarningDetailsTab = rewardNightDetailsPopUp.getEarningDetailsTab();

        eventEarningDetailsTab.goTo();
        eventEarningDetailsTab.verifyBasicDetailsForPoints("0", "0");
        eventEarningDetailsTab.getGrid().verify(earningEvent);
    }

    @Test(dependsOnMethods = { "verifyRewardNightEventEarningDetails" }, alwaysRun = true)
    public void verifyRewardNightEventBillingDetails()
    {
        EventBillingDetailsTab tab = rewardNightDetailsPopUp.getBillingDetailsTab();
        tab.goTo();
        tab.getEventBillingDetail(1).verifyBillingIsEmpty(REWARD_NIGHT_CANCELED);
        tab.getEventBillingDetail(2).verifyBillingIsEmpty(REWARD_NIGHT);

        rewardNightDetailsPopUp.close();
    }

    @Test(dependsOnMethods = { "verifyRewardNightEventBillingDetails" }, alwaysRun = true)
    public void verifyAllEventsTabCanceledRewardNightEvent()
    {
        allEvntsPage = new AllEventsPage();
        allEvntsPage.goTo();

        allEventsRewardNightRow.setTransType(REWARD_NIGHT_CANCELED);
        allEventsRewardNightRow.setIhgUnits("0");

        AllEventsGridRow row = allEvntsPage.getGrid().getRow(REWARD_NIGHT_CANCELED,
                allEventsRewardNightRow.getDetail());
        row.verify(allEventsRewardNightRow);
    }

}
