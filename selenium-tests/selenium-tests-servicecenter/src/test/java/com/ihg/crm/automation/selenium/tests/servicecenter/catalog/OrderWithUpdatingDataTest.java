package com.ihg.crm.automation.selenium.tests.servicecenter.catalog;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.StringContains.containsString;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.member.GuestAddress;
import com.ihg.automation.selenium.common.member.GuestEmail;
import com.ihg.automation.selenium.common.member.GuestName;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.order.Order;
import com.ihg.automation.selenium.common.personal.EmailContactList;
import com.ihg.automation.selenium.common.types.EmailFormat;
import com.ihg.automation.selenium.common.types.EmailType;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.gwt.components.MessageBox;
import com.ihg.automation.selenium.gwt.components.MessageBoxType;
import com.ihg.automation.selenium.gwt.components.Mode;
import com.ihg.automation.selenium.servicecenter.pages.catalog.CatalogItemDetailsPopUp;
import com.ihg.automation.selenium.servicecenter.pages.catalog.CatalogPage;
import com.ihg.automation.selenium.servicecenter.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.servicecenter.pages.event.order.OrderEventsPage;
import com.ihg.automation.selenium.servicecenter.pages.order.OrderSummaryPopUp;
import com.ihg.automation.selenium.servicecenter.pages.personal.AddressContactList;
import com.ihg.automation.selenium.servicecenter.pages.personal.PersonalInfoPage;

public class OrderWithUpdatingDataTest extends CatalogCommon
{
    private Member member = new Member();
    private PersonalInfoPage personalInfoPage = new PersonalInfoPage();
    private CatalogPage catalogPage = new CatalogPage();
    private GuestEmail email = new GuestEmail();
    private GuestEmail updatedEmail = new GuestEmail();
    private Order order = new Order();
    private GuestName updatedName = new GuestName();
    private GuestAddress address;
    private GuestAddress updatedAddress;
    private static String itemId;

    @BeforeClass
    public void beforeClass()
    {
        updatedEmail.setType(EmailType.PERSONAL);
        updatedEmail.setEmail("test_updated@email.com");
        updatedEmail.setFormat(EmailFormat.TEXT);

        updatedName = MemberPopulateHelper.getRandomName();

        address = MemberPopulateHelper.getUnitedStatesAddress();
        email = MemberPopulateHelper.getRandomEmail();

        member.setPersonalInfo(MemberPopulateHelper.getSimplePersonalInfo());
        member.addAddress(address);
        member.addEmail(email);
        member.addProgram(Program.RC);

        updatedAddress = MemberPopulateHelper.getUnitedStatesSecondAddress();

        order.setDeliveryAddress(updatedAddress);
        order.setDeliveryEmail(updatedEmail.getEmail());
        order.setName(updatedName.getNameOnPanel());

        itemId = getItemId();

        login();

        new EnrollmentPage().enroll(member);
    }

    @Test
    public void makingChangesInOrderWithoutSaving()
    {
        makingChanges(false);
    }

    @Test(dependsOnMethods = { "makingChangesInOrderWithoutSaving" }, alwaysRun = true)
    public void checkNewOrderDetailsWithoutSavingChanges()
    {
        checkShippingDetails(order);
    }

    @Test(dependsOnMethods = { "checkNewOrderDetailsWithoutSavingChanges" }, alwaysRun = true)
    public void checkChangesWithoutSavingOnPersonalPage()
    {
        personalInfoPage.goTo();

        AddressContactList addressList = personalInfoPage.getAddressList();
        verifyThat(addressList, addressList.getContactsCount(), is(1), "One address on personal info page");
        addressList.getExpandedContact().verify(address, Mode.VIEW);

        EmailContactList emailList = personalInfoPage.getEmailList();
        verifyThat(emailList, emailList.getContactsCount(), is(1), "One email on personal info page");
        emailList.getExpandedContact().verify(email, Mode.VIEW);

        verifyThat(personalInfoPage.getCustomerInfo().getName().getFullName(),
                hasText(is(member.getPersonalInfo().getName().getFullName())));
    }

    @Test(dependsOnMethods = { "checkChangesWithoutSavingOnPersonalPage" }, alwaysRun = true)
    public void makingChangesInOrderWithSave()
    {
        makingChanges(true);
    }

    @Test(dependsOnMethods = { "makingChangesInOrderWithSave" }, alwaysRun = true)
    public void checkNewOrderDetailsWithSavedChanges()
    {
        checkShippingDetails(order);
    }

    @Test(dependsOnMethods = { "checkNewOrderDetailsWithSavedChanges" }, alwaysRun = true)
    public void checkChangesWithSavingOnPersonalPage()
    {
        personalInfoPage.goTo();

        AddressContactList addressList = personalInfoPage.getAddressList();
        verifyThat(addressList, addressList.getContactsCount(), is(1), "One address on personal info page");
        addressList.getExpandedContact().verify(updatedAddress, Mode.VIEW);

        verifyThat(personalInfoPage.getCustomerInfo().getName().getFullName(),
                hasText(member.getPersonalInfo().getName().getFullName()));
    }

    private void makingChanges(Boolean isSaved)
    {
        catalogPage.goTo();
        catalogPage.getButtonBar().clickClearCriteria();
        catalogPage.searchByItemId(itemId);
        CatalogItemDetailsPopUp catalogItemDetailsPopUp = catalogPage.openCatalogItemDetailsPopUp();
        OrderSummaryPopUp orderSummaryPopUp = catalogItemDetailsPopUp.openOrderSummaryPopUp();

        orderSummaryPopUp.getName().populate(updatedName);

        orderSummaryPopUp.getAddress().populate(updatedAddress);

        orderSummaryPopUp.getSelectEmail().type(updatedEmail.getEmail());

        if (isSaved)
        {
            orderSummaryPopUp.getSaveAddress().check();
        }
        orderSummaryPopUp.clickCheckout();

        // verifyText(new MessageBox(MessageBoxType.SUCCESS),
        // containsString("Contact information was successfully updated"));

        verifyThat(new MessageBox(MessageBoxType.SUCCESS), hasText(containsString("Order was successfully submitted")));

        verifyThat(orderSummaryPopUp, displayed(false));
        OrderEventsPage orderPage = new OrderEventsPage();
        verifyThat(orderPage, displayed(true));
    }
}
