package com.ihg.crm.automation.selenium.tests.servicecenter.commpref;

import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyNoError;
import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifySuccess;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.communication.CommunicationPreferencesConstants.SUCCESS_MESSAGE_SC;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static com.ihg.automation.selenium.matchers.component.IsEnabled.enabled;
import static com.ihg.automation.selenium.servicecenter.pages.history.AuditConstant.CommunicationPreferences.COMMUNICATION_PREFERENCES;
import static com.ihg.automation.selenium.servicecenter.pages.history.AuditConstant.CommunicationPreferences.EMAIL;
import static com.ihg.automation.selenium.servicecenter.pages.history.AuditConstant.CommunicationPreferences.EMAIL_FOR_CUSTOMER_SERVICE;
import static org.hamcrest.Matchers.containsString;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.member.GuestEmail;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.personal.EmailContact;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.gwt.components.MessageBox;
import com.ihg.automation.selenium.gwt.components.MessageBoxType;
import com.ihg.automation.selenium.gwt.components.input.Select;
import com.ihg.automation.selenium.servicecenter.pages.LeftPanel;
import com.ihg.automation.selenium.servicecenter.pages.Messages;
import com.ihg.automation.selenium.servicecenter.pages.communication.CommunicationPreferences;
import com.ihg.automation.selenium.servicecenter.pages.communication.CommunicationPreferencesPage;
import com.ihg.automation.selenium.servicecenter.pages.communication.CustomerServiceEmail;
import com.ihg.automation.selenium.servicecenter.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.servicecenter.pages.history.ProfileHistoryGridRow;
import com.ihg.automation.selenium.servicecenter.pages.history.ProfileHistoryPage;
import com.ihg.automation.selenium.servicecenter.pages.personal.PersonalInfoPage;
import com.ihg.crm.automation.selenium.tests.servicecenter.LoginLogout;

public class CustomerServiceEmailTest extends LoginLogout
{
    private Member member = new Member();
    private EnrollmentPage enrollmentPage = new EnrollmentPage();
    private GuestEmail updateEmail = new GuestEmail();
    private CustomerServiceEmail email;
    private ProfileHistoryPage profileHistoryPage = new ProfileHistoryPage();

    @BeforeClass
    public void beforeClass()
    {
        GuestEmail guestEmail = MemberPopulateHelper.getRandomEmail();

        updateEmail.setEmail("update_" + guestEmail.getEmail());

        member.addProgram(Program.RC);
        member.setPersonalInfo(MemberPopulateHelper.getSimplePersonalInfo());
        member.addAddress(MemberPopulateHelper.getUnitedStatesAddress());
        member.addEmail(guestEmail);
        member.addPhone(MemberPopulateHelper.getRandomPhone());

        login();
        enrollmentPage.enroll(member);
    }

    @Test
    public void verifyEmailEnabled()
    {
        CommunicationPreferencesPage commPrefPage = new CommunicationPreferencesPage();
        commPrefPage.goTo();
        CommunicationPreferences commPrefs = commPrefPage.getCommunicationPreferences();
        commPrefs.clickEdit(verifyNoError());

        email = commPrefs.getCustomerService().getEmail();
        email.getEmailCheckBox().check();

        Select emailAddress = email.getEmail();
        verifyThat(emailAddress, enabled(true));
        verifyThat(emailAddress, hasText(member.getPreferredEmail().getEmail()));

        commPrefs.clickSave(verifySuccess(SUCCESS_MESSAGE_SC));

        verifyThat(emailAddress.getView(), hasText(member.getPreferredEmail().getEmail()));
    }

    @Test(dependsOnMethods = { "verifyEmailEnabled" }, alwaysRun = true)
    public void verifyHistoryForEmailEnabled()
    {
        profileHistoryPage.goTo();
        verifyNoError();

        ProfileHistoryGridRow emailRow = getEmailHistoryGridRow();

        emailRow.verifyAddAction(member.getPreferredEmail().getEmail());
    }

    @Test(dependsOnMethods = { "verifyHistoryForEmailEnabled" }, alwaysRun = true)
    public void verifyUpdatingEmail()
    {
        PersonalInfoPage personalInfoPage = new PersonalInfoPage();
        personalInfoPage.goTo();
        EmailContact emailContact = personalInfoPage.getEmailList().getContact();
        emailContact.update(updateEmail);

        verifyThat(new MessageBox(MessageBoxType.SUCCESS),
                hasText(containsString(Messages.getUpdatedMessage(true, true))));

        CommunicationPreferencesPage commPrefPage = new CommunicationPreferencesPage();
        commPrefPage.goTo();
        email = commPrefPage.getCommunicationPreferences().getCustomerService().getEmail();
        verifyThat(email.getEmail().getView(), hasText(updateEmail.getEmail()));
    }

    @Test(dependsOnMethods = { "verifyUpdatingEmail" }, alwaysRun = true)
    public void verifyHistoryWithoutChangesAfterUpdateEmail()
    {
        profileHistoryPage.goTo();
        verifyNoError();

        ProfileHistoryGridRow emailRow = getEmailHistoryGridRow();

        emailRow.verifyAddAction(member.getPreferredEmail().getEmail());
    }

    @Test(dependsOnMethods = { "verifyHistoryWithoutChangesAfterUpdateEmail" }, alwaysRun = true)
    public void removeEmail()
    {
        new LeftPanel().reOpenMemberProfile(member);

        CommunicationPreferencesPage commPrefPage = new CommunicationPreferencesPage();
        commPrefPage.goTo();
        CommunicationPreferences commPrefs = commPrefPage.getCommunicationPreferences();
        commPrefs.clickEdit(verifyNoError());

        email = commPrefs.getCustomerService().getEmail();
        email.getEmailCheckBox().uncheck();

        Select emailAddress = email.getEmail();
        verifyThat(emailAddress, enabled(false));

        commPrefs.clickSave(verifySuccess(SUCCESS_MESSAGE_SC));

        verifyThat(emailAddress.getView(), displayed(false));
    }

    @Test(dependsOnMethods = { "removeEmail" }, alwaysRun = true)
    public void verifyHistoryAfterRemoveEmail()
    {
        profileHistoryPage.goTo();
        verifyNoError();

        ProfileHistoryGridRow emailRow = getEmailHistoryGridRow();

        emailRow.verifyDeleteAction(updateEmail.getEmail());
    }

    private ProfileHistoryGridRow getEmailHistoryGridRow()
    {
        return profileHistoryPage.getProfileHistoryGrid().getRow(1).getSubRowByName(COMMUNICATION_PREFERENCES)
                .getSubRowByName(EMAIL_FOR_CUSTOMER_SERVICE).getSubRowByName(EMAIL);
    }
}
