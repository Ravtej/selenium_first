package com.ihg.crm.automation.selenium.tests.servicecenter.offer;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.GridSize.size;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static com.ihg.automation.selenium.matchers.component.IsDisplayed.displayed;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.common.DateUtils;
import com.ihg.automation.selenium.common.earning.EventEarningDetailsTab;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.stay.Stay;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.servicecenter.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsPage;
import com.ihg.automation.selenium.servicecenter.pages.event.all.OfferGridRowContainer;
import com.ihg.automation.selenium.servicecenter.pages.event.offer.MemberOfferGridRow;
import com.ihg.automation.selenium.servicecenter.pages.event.offer.OfferEventPage;
import com.ihg.automation.selenium.servicecenter.pages.event.offer.OfferPopUp;
import com.ihg.automation.selenium.servicecenter.pages.event.offer.UniverseOfferGridRow;
import com.ihg.automation.selenium.servicecenter.pages.event.offer.UniverseOffersGrid;
import com.ihg.automation.selenium.servicecenter.pages.event.stay.AdjustStayPopUp;
import com.ihg.automation.selenium.servicecenter.pages.event.stay.StayEventsPage;
import com.ihg.automation.selenium.servicecenter.pages.offer.OfferRegistrationPopUp;

@Test(groups = { "targetOfferTest" })
public class RegisterToTargetOfferWithMemberInContextTest extends TargetingOfferCommon
{
    private static String regEndDate;
    private OfferEventPage offerPage = new OfferEventPage();
    private UniverseOfferGridRow row;
    private UniverseOffersGrid universeOffersGrid;

    @BeforeClass
    public void beforeClass()
    {
        Member member = new Member();
        member.setPersonalInfo(MemberPopulateHelper.getSimplePersonalInfo());
        member.addAddress(MemberPopulateHelper.getIndonesiaAddress());
        member.addProgram(Program.RC);

        login();

        new EnrollmentPage().enroll(member);

        regEndDate = DateUtils.getFormattedDate(1);

        insert(TARGET_OFFER_ID, member, "(SYSDATE+1)");
    }

    @Test
    public void searchWithEligible()
    {
        offerPage.goTo();
        offerPage.searchByOfferId(TARGET_OFFER_ID, true);

        new OfferPopUp().waitAndClose();

        universeOffersGrid = offerPage.getUniverseOffersGrid();
        row = universeOffersGrid.getRow(1);

        verifyThat(universeOffersGrid, size(1));

        row.verify(TARGET_OFFER_ID, "Register");

        verifyThat(row.getRowContainer().getOfferDetails().getOfferEligibility().getTargetExpiry(),
                hasText(regEndDate));

    }

    @Test
    public void searchWithoutEligible()
    {
        offerPage.goTo();
        offerPage.searchByOfferId(TARGET_OFFER_ID, false);

        new OfferPopUp().waitAndClose();

        universeOffersGrid = offerPage.getUniverseOffersGrid();
        row = universeOffersGrid.getRow(1);

        verifyThat(universeOffersGrid, size(1));

        row.verify(TARGET_OFFER_ID, "Register");

        verifyThat(row.getRowContainer().getOfferDetails().getOfferEligibility().getTargetExpiry(),
                hasText(regEndDate));
    }

    @Test(dependsOnMethods = { "searchWithEligible", "searchWithoutEligible" }, alwaysRun = true)
    public void registerToOffer()
    {
        OfferRegistrationPopUp offerRegistrationPopUp = new OfferRegistrationPopUp();
        offerRegistrationPopUp.registerToTargetingOffer(null, TARGET_OFFER_ID, regEndDate);
    }

    @Test(dependsOnMethods = { "registerToOffer" })
    public void verifyOffer()
    {
        verifyTargetExpiry(regEndDate);
        MemberOfferGridRow offerGridRow = offerPage.getMemberOffersGrid().getRowByOfferCode(TARGET_OFFER_ID);
        verifyThat(offerGridRow, displayed(true));
        verifyThat(offerGridRow.getCell(MemberOfferGridRow.MemberOfferCell.WINS), hasText("0"));
    }

    @Test(dependsOnMethods = { "verifyOffer" })
    public void verifyOfferOnAllEventsPage()
    {
        AllEventsPage allEventsPage = new AllEventsPage();
        allEventsPage.goTo();
        OfferGridRowContainer offerGridRowContainer = allEventsPage.getGrid().getRow(1)
                .expand(OfferGridRowContainer.class);
        verifyThat(offerGridRowContainer.getOfferDetails().getOfferCode(), hasText(TARGET_OFFER_ID));
    }

    @Test(dependsOnMethods = { "verifyOfferOnAllEventsPage" }, alwaysRun = true)
    public void winTheOffer()
    {
        Stay firstStay = new Stay();
        firstStay.setHotelCode("BEYHA");
        firstStay.setCheckInOutByNights(1);
        firstStay.setAvgRoomRate(100.00);
        firstStay.setRateCode("Test");

        Stay secondStay = firstStay.clone();
        secondStay.setCheckInOutByNights(1, firstStay.getNights());

        StayEventsPage stayEventsPage = new StayEventsPage();
        stayEventsPage.createStay(firstStay);
        stayEventsPage.createStay(secondStay);

        stayEventsPage.getGuestGrid().getRow(1).getDetails().clickDetails();
        AdjustStayPopUp adjustStayPopUp = new AdjustStayPopUp();
        EventEarningDetailsTab earningDetailsTab = adjustStayPopUp.getEarningDetailsTab();
        earningDetailsTab.goTo();
        verifyThat(earningDetailsTab.getGrid().getRowByType(TARGET_OFFER_ID), displayed(true));
        adjustStayPopUp.close();

        OfferEventPage offerPage = new OfferEventPage();
        offerPage.goTo();
        verifyThat(offerPage.getMemberOffersGrid().getRowByOfferCode(TARGET_OFFER_ID)
                .getCell(MemberOfferGridRow.MemberOfferCell.WINS), hasText("1"));
    }
}
