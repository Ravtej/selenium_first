package com.ihg.crm.automation.selenium.tests.servicecenter.programinfo.dr;

import static com.ihg.automation.common.DateUtils.getStringToDate;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.event.EarningEventFactory.getProgramEventWithoutStatus;
import static com.ihg.automation.selenium.common.event.EventTransactionType.Membership.MEMBERSHIP_RENEWAL;
import static com.ihg.automation.selenium.common.event.EventTransactionType.Order.REDEEM;
import static com.ihg.automation.selenium.common.member.DiningRewardAward.GOLD_TO_PLAT_RENEWAL_NON_HOTEL;
import static com.ihg.automation.selenium.common.member.MemberJdbcUtils.UPDATE_EXP;
import static com.ihg.automation.selenium.common.offer.OfferFactory.FREE_NIGHT_OFFER_CODE;
import static com.ihg.automation.selenium.common.types.program.Program.DR;
import static com.ihg.automation.selenium.common.types.program.ProgramStatus.OPEN;
import static com.ihg.automation.selenium.common.types.program.RewardClubLevel.PLTN;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.isDisplayedWithWait;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static org.hamcrest.core.StringContains.containsString;

import org.joda.time.LocalDate;
import org.springframework.beans.factory.annotation.Value;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.common.DateUtils;
import com.ihg.automation.selenium.common.Constant;
import com.ihg.automation.selenium.common.billing.EventBillingDetailsTab;
import com.ihg.automation.selenium.common.catalog.CatalogItem;
import com.ihg.automation.selenium.common.earning.EventEarningDetailsTab;
import com.ihg.automation.selenium.common.event.AllEventsRow;
import com.ihg.automation.selenium.common.event.AllEventsRowConverter;
import com.ihg.automation.selenium.common.event.EarningEvent;
import com.ihg.automation.selenium.common.event.EarningEventFactory;
import com.ihg.automation.selenium.common.member.DiningRewardAward;
import com.ihg.automation.selenium.common.member.MemberJdbcUtils;
import com.ihg.automation.selenium.common.order.Order;
import com.ihg.automation.selenium.common.order.OrderFactory;
import com.ihg.automation.selenium.common.payment.Payment;
import com.ihg.automation.selenium.common.payment.PaymentFactory;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.common.types.program.RewardClubLevel;
import com.ihg.automation.selenium.servicecenter.pages.GuestSearch;
import com.ihg.automation.selenium.servicecenter.pages.LeftPanel;
import com.ihg.automation.selenium.servicecenter.pages.Role;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsGridRow;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsPage;
import com.ihg.automation.selenium.servicecenter.pages.event.enroll.MembershipDetails;
import com.ihg.automation.selenium.servicecenter.pages.event.enroll.MembershipDetailsPopUp;
import com.ihg.automation.selenium.servicecenter.pages.event.enroll.MembershipDetailsTab;
import com.ihg.automation.selenium.servicecenter.pages.event.enroll.MembershipGridRowContainer;
import com.ihg.automation.selenium.servicecenter.pages.event.order.OrderDetailsPopUp;
import com.ihg.automation.selenium.servicecenter.pages.event.order.OrderEventsPage;
import com.ihg.automation.selenium.servicecenter.pages.event.order.OrderGridRow;
import com.ihg.automation.selenium.servicecenter.pages.event.order.OrderGridRow.OrderCell;
import com.ihg.automation.selenium.servicecenter.pages.event.order.OrderRedeemGridRowContainer;
import com.ihg.automation.selenium.servicecenter.pages.event.order.OrderSystemDetailsGridRowContainer;
import com.ihg.automation.selenium.servicecenter.pages.programs.ProgramPageBase.EnrollmentDetails;
import com.ihg.automation.selenium.servicecenter.pages.programs.dr.DiningRewardsPage;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.RewardClubPage;

public class RenewByPointsTest extends DRCommon
{
    @Value("${memberRenewByPoints}")
    protected String memberRenewByPoints;

    @Value("${memberRenewByPoints.date}")
    protected String date;

    private LocalDate enrollDate;
    private String enrollDateToString;
    private static final CatalogItem RENEW_AWARD = DiningRewardAward.RENEW_90000_POINTS_SC;
    private AllEventsPage allEventsPage = new AllEventsPage();
    private OrderDetailsPopUp orderDetailsPopUp;
    private OrderRedeemGridRowContainer orderRedeemContainer;
    private EarningEvent redeemEvent = EarningEventFactory.getRedeemEvent(RENEW_AWARD);
    private Payment payment = PaymentFactory.getPointsPayment(RENEW_AWARD);

    @BeforeClass
    public void beforeClass()
    {
        MemberJdbcUtils.update(jdbcTemplateUpdate, UPDATE_EXP, memberRenewByPoints, DR, 13);

        enrollDate = getStringToDate(date);
        enrollDateToString = enrollDate.toString(DateUtils.DATE_FORMAT);

        login(user15U, Role.AGENT_TIER2);

        new GuestSearch().byMemberNumber(memberRenewByPoints);
    }

    @Test
    public void verifyTierLevel()
    {
        RewardClubPage rewardClubPage = new RewardClubPage();
        rewardClubPage.goTo();
        rewardClubPage.getSummary().verify(OPEN, PLTN);
    }

    @Test(dependsOnMethods = "verifyTierLevel", alwaysRun = true)
    public void verifyPointsRemained()
    {
        new LeftPanel().verifyBalance("65,000");
    }

    @Test(dependsOnMethods = "verifyPointsRemained", alwaysRun = true)
    public void verifyRCProgramInformation()
    {
        verifyRCProgramInformationDetails(RewardClubLevel.PLTN);
    }

    @Test(dependsOnMethods = "verifyRCProgramInformation", alwaysRun = true)
    public void verifyProgramSummaryDetails()
    {
        verifyProgramSummary(memberRenewByPoints, 13);
    }

    @Test(dependsOnMethods = "verifyProgramSummaryDetails", alwaysRun = true)
    public void verifyEnrollmentDetails()
    {
        DiningRewardsPage diningRewardsPage = new DiningRewardsPage();
        EnrollmentDetails enrollmentDetails = diningRewardsPage.getEnrollmentDetails();
        enrollmentDetails.getPaymentDetails().verifyCommonDetails(payment);
        enrollmentDetails.verifyScSource(helper);
    }

    @Test(dependsOnMethods = "verifyEnrollmentDetails", alwaysRun = true)
    public void verifyOrderSystemEventDetails()
    {
        OrderEventsPage orderPage = new OrderEventsPage();
        orderPage.goTo();
        OrderGridRow itemRow = orderPage.getOrdersGrid().getRow(1);
        redeemEvent.setOrderNumber(itemRow.getCell(OrderCell.ORDER_NUMBER).getText());

        allEventsPage.goTo();

        Order systemOrder = OrderFactory.getSystemOrder(GOLD_TO_PLAT_RENEWAL_NON_HOTEL, null);
        systemOrder.setTransactionDate(enrollDateToString);
        systemOrder.getOrderItems().get(0).setDeliveryStatus(null);
        AllEventsRow systemOrderRow = AllEventsRowConverter.convert(systemOrder);

        AllEventsGridRow orderSystemRow = allEventsPage.getGrid().getRow(systemOrderRow.getTransType(),
                systemOrderRow.getDetail());
        orderSystemRow.verify(systemOrderRow);
        orderSystemRow.expand(OrderSystemDetailsGridRowContainer.class).verifyWithShippingAsOnLeftPanel(systemOrder);
    }

    @Test(dependsOnMethods = "verifyOrderSystemEventDetails", alwaysRun = true)
    public void verifyMembershipRenewEventInContainer()
    {
        AllEventsRow membershipRenewalRowData = new AllEventsRow(enrollDateToString, MEMBERSHIP_RENEWAL,
                Program.DR.getCode(), "5,000", "");

        AllEventsGridRow membershipRenewalRow = allEventsPage.getGrid().getRow(MEMBERSHIP_RENEWAL);
        membershipRenewalRow.verify(membershipRenewalRowData);

        MembershipGridRowContainer container = membershipRenewalRow.expand(MembershipGridRowContainer.class);
        MembershipDetails membershipDetails = container.getMembershipDetails();
        membershipDetails.getPaymentDetails().verifyCommonDetails(payment);
        membershipDetails.verifyScSource(helper);
    }

    @Test(dependsOnMethods = "verifyMembershipRenewEventInContainer", alwaysRun = true)
    public void verifyMembershipRenewEventDetails()
    {
        AllEventsGridRow membershipRenewalRow = allEventsPage.getGrid().getRow(MEMBERSHIP_RENEWAL);
        MembershipGridRowContainer container = membershipRenewalRow.expand(MembershipGridRowContainer.class);
        container.clickDetails();
        MembershipDetailsPopUp membershipDetailsPopUp = new MembershipDetailsPopUp();
        verifyThat(membershipDetailsPopUp, isDisplayedWithWait());

        MembershipDetailsTab membershipDetailsTab = membershipDetailsPopUp.getMembershipDetailsTab();
        membershipDetailsTab.goTo();
        MembershipDetails membershipDetails = membershipDetailsTab.getMembershipDetails();
        membershipDetails.getPaymentDetails().verifyCommonDetails(payment);
        membershipDetails.verifyScSource(helper);
    }

    @Test(dependsOnMethods = "verifyMembershipRenewEventDetails", alwaysRun = true)
    public void validateMembershipRenewEventEarningDetails()
    {
        verifyMembershipRenewEarningDetails();
    }

    @Test(dependsOnMethods = "validateMembershipRenewEventEarningDetails", alwaysRun = true)
    public void validateMembershipRenewEventEarningDetailsGrid()
    {
        EarningEvent rcPointEvent = new EarningEvent(FREE_NIGHT_OFFER_CODE, "0", "0", Constant.RC_POINTS, enrollDate);

        new MembershipDetailsPopUp().getEarningDetailsTab().getGrid().verifyRowsFoundByTypeAndAward(
                EarningEventFactory.getAwardEvent(RENEW_AWARD, enrollDate),
                EarningEventFactory.getRenewDREvent(enrollDate), rcPointEvent, rcPointEvent,
                getProgramEventWithoutStatus(DR, GOLD_TO_PLAT_RENEWAL_NON_HOTEL, enrollDate));

    }

    @Test(dependsOnMethods = "validateMembershipRenewEventEarningDetailsGrid", alwaysRun = true)
    public void verifyMembershipRenewEventBillingDetails()
    {
        validateMembershipRenewBillingDetails(RENEW_AWARD, enrollDate);
    }

    @Test(dependsOnMethods = { "verifyMembershipRenewEventBillingDetails" }, alwaysRun = true)
    public void validateRedeemEventDetails()
    {
        Order redeemOrder = OrderFactory.getRedeemOrder(RENEW_AWARD, helper);
        redeemOrder.setTransactionDate(enrollDateToString);
        AllEventsRow redeemRowData = AllEventsRowConverter.getRedeemEvent(redeemOrder);

        allEventsPage = new AllEventsPage();
        allEventsPage.goTo();
        AllEventsGridRow redeemRow = allEventsPage.getGrid().getRow(REDEEM);
        redeemRow.verify(redeemRowData, hasText(containsString(redeemRowData.getDetail())));

        orderRedeemContainer = redeemRow.expand(OrderRedeemGridRowContainer.class);
        orderRedeemContainer.getOrderDetails().verify(redeemOrder.getOrderItems().get(0), "RC Units");
        orderRedeemContainer.getShippingInfo().verifyAsOnCustomerInformation(true);
        orderRedeemContainer.getSource().verify(redeemOrder.getSource(), Constant.NOT_AVAILABLE);
    }

    @Test(dependsOnMethods = "validateRedeemEventDetails", alwaysRun = true)
    public void verifyRedeemEarningDetails()
    {
        redeemEvent.setDateEarned(enrollDate.toString(DateUtils.dd_MMM_YYYY));

        orderRedeemContainer.clickDetails();
        orderDetailsPopUp = new OrderDetailsPopUp();
        verifyThat(orderDetailsPopUp, isDisplayedWithWait());

        EventEarningDetailsTab earningDetailsTab = orderDetailsPopUp.getEarningDetailsTab();
        earningDetailsTab.goTo();
        earningDetailsTab.verifyBasicDetailsForPoints("-90,000", "0");
        earningDetailsTab.getGrid().verify(redeemEvent);
    }

    @Test(dependsOnMethods = "verifyRedeemEarningDetails", alwaysRun = true)
    public void verifyRedeemBillingDetails()
    {
        EventBillingDetailsTab redeemBillingDetailsTab = orderDetailsPopUp.getBillingDetailsTab();
        redeemBillingDetailsTab.goTo();
        redeemBillingDetailsTab.verifyTabIsEmpty(enrollDate, REDEEM);
        orderDetailsPopUp.clickClose();
    }

    @Test(dependsOnMethods = { "verifyRedeemBillingDetails" }, alwaysRun = true)
    public void verifyOfferRows()
    {
        verifyOfferRow();
    }

    @Test(dependsOnMethods = { "verifyOfferRows" }, alwaysRun = true)
    public void verifyFreeNightTab()
    {
        verifyFreeNightHasTwoWinsAfterRenew();
    }

}
