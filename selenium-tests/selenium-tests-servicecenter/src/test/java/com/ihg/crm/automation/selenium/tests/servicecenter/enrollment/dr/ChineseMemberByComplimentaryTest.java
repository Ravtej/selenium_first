package com.ihg.crm.automation.selenium.tests.servicecenter.enrollment.dr;

import static com.ihg.automation.common.DateUtils.DATE_FORMAT;
import static com.ihg.automation.common.DateUtils.dd_MMM_YYYY;
import static com.ihg.automation.common.DateUtils.getStringToDate;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.event.EarningEventFactory.getAwardEvent;
import static com.ihg.automation.selenium.common.event.EarningEventFactory.getProgramEventWithoutStatus;
import static com.ihg.automation.selenium.common.event.EventTransactionType.Membership.ENROLL;
import static com.ihg.automation.selenium.common.event.EventTransactionType.Offer.OFFER_REGISTRATION;
import static com.ihg.automation.selenium.common.member.DiningRewardAward.FUL_WELCOME_TO_VIP_GOLD;
import static com.ihg.automation.selenium.common.member.MemberJdbcUtils.UPDATE_EXP;
import static com.ihg.automation.selenium.common.offer.OfferFactory.DELAYED_FULFILMENT_GLOBAL;
import static com.ihg.automation.selenium.common.offer.OfferFactory.DR_FREE_NIGHT;
import static com.ihg.automation.selenium.common.payment.PaymentFactory.COMPLIMENTARY;
import static com.ihg.automation.selenium.common.types.program.Program.DR;
import static com.ihg.automation.selenium.matchers.component.GridSize.size;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static com.ihg.automation.selenium.matchers.component.IsDisplayed.isDisplayedWithWait;
import static com.ihg.automation.selenium.matchers.component.ViewableMatcher.hasTextInView;
import static org.apache.commons.lang.StringUtils.EMPTY;
import static org.hamcrest.Matchers.startsWith;

import java.util.Arrays;

import org.joda.time.LocalDate;
import org.springframework.beans.factory.annotation.Value;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.ihg.automation.common.DateUtils;
import com.ihg.automation.selenium.common.Constant;
import com.ihg.automation.selenium.common.billing.EventBillingDetailsTab;
import com.ihg.automation.selenium.common.catalog.CatalogItem;
import com.ihg.automation.selenium.common.earning.EventEarningDetailsTab;
import com.ihg.automation.selenium.common.event.AllEventsRowFactory;
import com.ihg.automation.selenium.common.event.EarningEvent;
import com.ihg.automation.selenium.common.freenight.FreeNight;
import com.ihg.automation.selenium.common.freenight.VoucherRedemption;
import com.ihg.automation.selenium.common.member.DiningRewardAward;
import com.ihg.automation.selenium.common.member.MemberJdbcUtils;
import com.ihg.automation.selenium.common.offer.GuestOffer;
import com.ihg.automation.selenium.common.offer.Offer;
import com.ihg.automation.selenium.common.order.Order;
import com.ihg.automation.selenium.common.order.OrderFactory;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.common.types.program.ProgramStatus;
import com.ihg.automation.selenium.common.types.program.RewardClubLevel;
import com.ihg.automation.selenium.servicecenter.pages.GuestSearch;
import com.ihg.automation.selenium.servicecenter.pages.Role;
import com.ihg.automation.selenium.servicecenter.pages.account.AccountInfoPage;
import com.ihg.automation.selenium.servicecenter.pages.account.FailedSelfServiceLoginAttempts;
import com.ihg.automation.selenium.servicecenter.pages.account.Security;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsGrid;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsGridRow;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsPage;
import com.ihg.automation.selenium.servicecenter.pages.event.all.OfferGridRowContainer;
import com.ihg.automation.selenium.servicecenter.pages.event.enroll.MembershipDetailsPopUp;
import com.ihg.automation.selenium.servicecenter.pages.event.enroll.MembershipDetailsTab;
import com.ihg.automation.selenium.servicecenter.pages.event.enroll.MembershipGridRowContainer;
import com.ihg.automation.selenium.servicecenter.pages.event.freenight.FreeNightDetailsPopUp;
import com.ihg.automation.selenium.servicecenter.pages.event.freenight.FreeNightEventPage;
import com.ihg.automation.selenium.servicecenter.pages.event.freenight.FreeNightGridRow;
import com.ihg.automation.selenium.servicecenter.pages.event.offer.MemberOfferGridRow;
import com.ihg.automation.selenium.servicecenter.pages.event.offer.OfferEventPage;
import com.ihg.automation.selenium.servicecenter.pages.event.order.OrderSystemDetailsGridRowContainer;
import com.ihg.automation.selenium.servicecenter.pages.programs.dr.DiningRewardsPage;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.RewardClubPage;

public class ChineseMemberByComplimentaryTest extends EnrollToDrCommon
{
    @Value("${memberByComplimentaryCN}")
    protected String memberByComplimentaryCN;

    @Value("${memberByComplimentaryCN.date}")
    protected String date;

    private LocalDate enrollDate;
    private String enrollDateToString;
    private static final CatalogItem ENROLMENT_AWARD = DiningRewardAward.ENROLL_COMPLIMENTARY;
    private AllEventsPage allEventsPage = new AllEventsPage();
    private MembershipDetailsPopUp membershipDetailsPopUp = new MembershipDetailsPopUp();
    private EarningEvent rcEarningEvent = new EarningEvent("PC", "0", "0", Constant.RC_POINTS, "");
    private VoucherRedemption voucherRedemption = new VoucherRedemption();
    private FreeNight freeNight = new FreeNight();
    private GuestOffer guestOffer;

    @BeforeClass
    private void beforeClass()
    {
        MemberJdbcUtils.update(jdbcTemplateUpdate, UPDATE_EXP, memberByComplimentaryCN, DR, 13);

        enrollDate = getStringToDate(date);
        enrollDateToString = enrollDate.toString(DATE_FORMAT);
        guestOffer = new GuestOffer(DR_FREE_NIGHT);
        guestOffer.setRegistrationDate(enrollDateToString);
        guestOffer.setReplaysCount("1");

        freeNight.setGuestOffer(guestOffer);
        freeNight.setAvailable(EMPTY);
        freeNight.setExpired("1");

        voucherRedemption.setStartBookDates(enrollDateToString);
        voucherRedemption.setEarningDate(enrollDateToString);
        voucherRedemption.setEndBookDates(enrollDate.plusMonths(13).toString(DateUtils.EXPIRATION_DATE_PATTERN));

        rcEarningEvent.setDateEarned(enrollDate.toString(dd_MMM_YYYY));

        login(user15U, Role.AGENT_TIER2);

        new GuestSearch().byMemberNumber(memberByComplimentaryCN);
    }

    @Test
    public void verifyAccountInformation()
    {
        AccountInfoPage accountInfoPage = new AccountInfoPage();
        accountInfoPage.goTo();
        accountInfoPage.verify(ProgramStatus.OPEN);

        Security secur = accountInfoPage.getSecurity();
        verifyThat(secur.getLockout(), hasTextInView(Constant.NOT_AVAILABLE));
        verifyThat(accountInfoPage.getFlags(), hasText(startsWith("No flags selected")));
        FailedSelfServiceLoginAttempts attempts = accountInfoPage.getFailedSelfServiceLoginAttempts();
        attempts.expand();
        verifyThat(attempts.getGrid(), size(0));
    }

    @Test(dependsOnMethods = { "verifyAccountInformation" }, alwaysRun = true)
    public void verifyRewardsClubPage()
    {
        RewardClubPage rewardClubPage = new RewardClubPage();
        rewardClubPage.goTo();
        rewardClubPage.getSummary().verify(ProgramStatus.OPEN, RewardClubLevel.GOLD);
    }

    @Test(dependsOnMethods = { "verifyRewardsClubPage" }, alwaysRun = true)
    public void verifyDiningRewardClubPage()
    {
        validateDiningRewardClubPage(memberByComplimentaryCN, EXP_DATE, enrollDate);
        new DiningRewardsPage().getEnrollmentDetails().getPaymentDetails().verifyCommonDetails(COMPLIMENTARY);
    }

    @Test(dependsOnMethods = { "verifyDiningRewardClubPage" }, alwaysRun = true)
    public void verifyEvents()
    {
        validateEvents(Arrays.asList(AllEventsRowFactory.getEnrollEvent(enrollDate, Program.DR), enrollRC,
                AllEventsRowFactory.getOfferRegistrationEvent(enrollDate, DR_FREE_NIGHT), delayedFulfilmentOffer,
                orderSystem), 5);
    }

    @Test(dependsOnMethods = { "verifyEvents" }, alwaysRun = true)
    public void openEnrollRCEventDetails()
    {
        allEventsPage.goTo();
        AllEventsGrid allEventsGrid = allEventsPage.getGrid();
        AllEventsGridRow allEventsGridRow = allEventsGrid.getEnrollRow(Program.RC);
        allEventsGridRow.expand(MembershipGridRowContainer.class).clickDetails();
        verifyThat(membershipDetailsPopUp, isDisplayedWithWait());
    }

    @Test(dependsOnMethods = { "openEnrollRCEventDetails" })
    public void verifyEnrollRCEventBillingDetails()
    {
        EventBillingDetailsTab tab = membershipDetailsPopUp.getBillingDetailsTab();
        tab.goTo();
        tab.verifyTabIsEmpty(enrollDate, ENROLL);
    }

    @Test(dependsOnMethods = { "openEnrollRCEventDetails" })
    public void verifyEnrollRCEventMemberDetails()
    {
        MembershipDetailsTab tab = membershipDetailsPopUp.getMembershipDetailsTab();
        tab.goTo();
        tab.getMembershipDetails().verifyScSource(helper);
    }

    @Test(dependsOnMethods = { "openEnrollRCEventDetails" })
    public void verifyEnrollRCEventEarningDetails()
    {
        EventEarningDetailsTab tab = membershipDetailsPopUp.getEarningDetailsTab();
        tab.goTo();
        tab.verifyBasicDetailsNoPointsAndMiles();
        tab.getGrid().verify(rcEarningEvent);
    }

    @Test(dependsOnMethods = { "openEnrollRCEventDetails", "verifyEnrollRCEventBillingDetails",
            "verifyEnrollRCEventMemberDetails", "verifyEnrollRCEventEarningDetails" }, alwaysRun = true)
    public void closeEnrollRCEventDetails()
    {
        membershipDetailsPopUp.close();
    }

    @Test(dependsOnMethods = { "closeEnrollRCEventDetails" }, alwaysRun = true)
    public void openEnrollDREventDetails()
    {
        allEventsPage = new AllEventsPage();
        allEventsPage.goTo();
        AllEventsGrid allEventsGrid = allEventsPage.getGrid();
        AllEventsGridRow allEventsGridRow = allEventsGrid.getEnrollRow(DR);
        allEventsGridRow.expand(MembershipGridRowContainer.class).clickDetails();
        membershipDetailsPopUp = new MembershipDetailsPopUp();
        verifyThat(membershipDetailsPopUp, isDisplayedWithWait());
    }

    @Test(dependsOnMethods = { "openEnrollDREventDetails" })
    public void verifyEnrollDREventBillingDetails()
    {
        EventBillingDetailsTab tab = membershipDetailsPopUp.getBillingDetailsTab();
        tab.goTo();
        tab.verifyTabIsEmpty(enrollDate, ENROLL);
    }

    @Test(dependsOnMethods = { "openEnrollDREventDetails" })
    public void verifyEnrollDREventEarningDetails()
    {
        EventEarningDetailsTab tab = membershipDetailsPopUp.getEarningDetailsTab();
        tab.goTo();

        tab.verifyBasicDetailsNoPointsAndMiles();

        EarningEvent rcEvent = new EarningEvent(DR_FREE_NIGHT.getCode(), "0", "0", Constant.RC_POINTS, enrollDate);
        tab.getGrid().verifyRowsFoundByType(getAwardEvent(ENROLMENT_AWARD, enrollDate),
                getProgramEventWithoutStatus(DR, FUL_WELCOME_TO_VIP_GOLD, enrollDate), rcEvent, rcEvent);
    }

    @Test(dependsOnMethods = { "openEnrollDREventDetails" })
    public void verifyEnrollDREventMemberDetails()
    {
        MembershipDetailsTab tab = membershipDetailsPopUp.getMembershipDetailsTab();
        tab.goTo();
        tab.getMembershipDetails().verify(COMPLIMENTARY, helper);
    }

    @Test(dependsOnMethods = { "openEnrollDREventDetails", "verifyEnrollDREventBillingDetails",
            "verifyEnrollDREventMemberDetails", "verifyEnrollDREventEarningDetails" }, alwaysRun = true)
    public void closeEnrollDREventDetails()
    {
        membershipDetailsPopUp.close();
    }

    @Test(dependsOnMethods = { "closeEnrollDREventDetails" }, alwaysRun = true)
    public void verifyAllEventsSystemOrderEvent()
    {
        allEventsPage.goTo();
        AllEventsGrid grid = allEventsPage.getGrid();

        Order order = OrderFactory.getSystemOrder(FUL_WELCOME_TO_VIP_GOLD, null);
        order.setTransactionDate(enrollDateToString);
        order.getOrderItems().get(0).setDeliveryStatus(null);

        grid.getRow(order.getTransactionType()).expand(OrderSystemDetailsGridRowContainer.class)
                .verifyWithShippingAsOnLeftPanel(order);
    }

    @DataProvider(name = "verifyAllEventsOfferRegistrationEventProvider")
    public Object[][] verifyAllEventsOfferRegistrationEventProvider()
    {
        return new Object[][] { { DELAYED_FULFILMENT_GLOBAL }, { DR_FREE_NIGHT } };
    }

    @Test(dataProvider = "verifyAllEventsOfferRegistrationEventProvider", dependsOnMethods = {
            "verifyAllEventsSystemOrderEvent" }, alwaysRun = true)
    public void verifyAllEventsOfferRegistrationEvent(Offer offer)
    {
        allEventsPage.goTo();
        AllEventsGrid grid = allEventsPage.getGrid();
        AllEventsGridRow row = grid.getRow(OFFER_REGISTRATION, offer.getName());
        row.expand(OfferGridRowContainer.class).getOfferDetails().verify(offer, helper);
    }

    @Test(dependsOnMethods = { "verifyAllEventsOfferRegistrationEvent" }, alwaysRun = true)
    public void verifyOfferWon()
    {
        OfferEventPage offerEventPage = new OfferEventPage();
        offerEventPage.goTo();
        MemberOfferGridRow offerRow = offerEventPage.getMemberOffersGrid().getRowByOfferCode(DR_FREE_NIGHT);

        verifyThat(offerRow, isDisplayedWithWait());
        offerRow.verify(guestOffer);
    }

    @Test(dependsOnMethods = { "verifyOfferWon" }, alwaysRun = true)
    public void verifyFreeNightVoucher()
    {
        FreeNightEventPage freeNightEventPage = new FreeNightEventPage();
        freeNightEventPage.goTo();
        FreeNightGridRow row = freeNightEventPage.getFreeNightGrid().getRowByOfferCode(DR_FREE_NIGHT);
        row.verify(freeNight);

        FreeNightDetailsPopUp freeNightDetailsPopUp = row.getFreeNightDetails();
        freeNightDetailsPopUp.getFreeNightDetailsTab().getFreeNightVoucherGrid().getRow(1)
                .verifyBookingDates(voucherRedemption);
        freeNightDetailsPopUp.close();
    }
}
