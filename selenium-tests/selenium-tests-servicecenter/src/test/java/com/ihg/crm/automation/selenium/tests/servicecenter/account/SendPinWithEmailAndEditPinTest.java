package com.ihg.crm.automation.selenium.tests.servicecenter.account;

import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyNoError;
import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifySuccess;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.types.program.Program.RC;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.isHighlightedAsInvalid;
import static com.ihg.automation.selenium.matchers.component.HasDefaultText.hasDefault;
import static com.ihg.automation.selenium.matchers.component.ViewableMatcher.hasTextInView;
import static org.hamcrest.core.Is.is;

import java.util.Map;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.Constant;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.gwt.components.MessageBox;
import com.ihg.automation.selenium.servicecenter.pages.account.AccountInfoPage;
import com.ihg.automation.selenium.servicecenter.pages.account.Security;
import com.ihg.automation.selenium.servicecenter.pages.enroll.EnrollmentPage;
import com.ihg.crm.automation.selenium.tests.servicecenter.LoginLogout;

public class SendPinWithEmailAndEditPinTest extends LoginLogout
{
    private Member member = new Member();
    private AccountInfoPage accountInfo;
    private Security security;
    private static final String PIN = "SELECT a.PIN_NBR"//
            + " FROM DGST.GST_CREDENTIAL a"//
            + " WHERE a.DGST_MSTR_KEY IN"//
            + " (SELECT m.DGST_MSTR_KEY"//
            + "  FROM DGST.MBRSHP m"//
            + " WHERE m.MBRSHP_ID='%s')";

    @BeforeClass
    public void beforeClass()
    {
        member.addProgram(RC);
        member.setPersonalInfo(MemberPopulateHelper.getSimplePersonalInfo());
        member.addEmail(MemberPopulateHelper.getRandomEmail());
        member.addAddress(MemberPopulateHelper.getUnitedStatesAddress());

        login();

        new EnrollmentPage().enroll(member);
    }

    @Test
    public void tryToAddThreeDigitsPIN()
    {
        accountInfo = new AccountInfoPage();
        accountInfo.goTo();
        security = accountInfo.getSecurity();
        security.addPIN("111");
        verifyThat(security.getPin(), isHighlightedAsInvalid(true));
        verifyThat(security.getPin(), displayed(true));
        security.clickCancel(verifyNoError());
    }

    @Test(dependsOnMethods = { "tryToAddThreeDigitsPIN" }, alwaysRun = true)
    public void addFiveDigitsPIN()
    {
        security.addPIN("81731");
        verifyThat(security.getPin(), hasTextInView("••••"));
        verifyThat(security.getPin(), getMemberId(), is("8173"), "Verification PIN in DB");
    }

    @Test(dependsOnMethods = { "addFiveDigitsPIN" }, alwaysRun = true)
    public void sendPINWithEmail()
    {
        MessageBox.closeAll();
        security.clickEdit();
        security.getSendPin().clickAndWait(verifySuccess("PIN has been successfully sent to email address on file."));
    }

    @Test(dependsOnMethods = { "sendPINWithEmail" }, alwaysRun = true)
    public void editPIN()
    {
        security = accountInfo.getSecurity();
        verifyThat(security.getPin(), hasTextInView("••••"));

        security.addPIN("8346");
        verifyThat(security.getPin(), hasTextInView("••••"));
        verifyThat(security.getPin(), getMemberId(), is("8346"), "Verification PIN in DB");
    }

    @Test(dependsOnMethods = { "editPIN" }, alwaysRun = true)
    public void removePIN()
    {
        verifyThat(security.getPin(), hasTextInView("••••"));

        security.clickEdit();
        security.clickClear();
        verifyThat(security.getPin(), hasDefault());
        security.clickSave();
        verifyThat(security.getPin(), hasTextInView(Constant.NOT_AVAILABLE));
    }

    private String getMemberId()
    {
        Map<String, Object> map = jdbcTemplate.queryForMap(String.format(PIN, member.getRCProgramId()));
        return map.get("PIN_NBR").toString();
    }
}
