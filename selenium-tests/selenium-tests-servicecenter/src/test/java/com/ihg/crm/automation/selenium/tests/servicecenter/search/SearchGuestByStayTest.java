package com.ihg.crm.automation.selenium.tests.servicecenter.search;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.GridSize.size;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static java.text.MessageFormat.format;
import static org.hamcrest.CoreMatchers.containsString;

import java.util.Map;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.servicecenter.pages.GuestSearch;
import com.ihg.automation.selenium.servicecenter.pages.GuestSearch.SearchByStay;
import com.ihg.automation.selenium.servicecenter.pages.search.StaySearchGrid;
import com.ihg.automation.selenium.servicecenter.pages.search.StaySearchGridCell;
import com.ihg.automation.selenium.servicecenter.pages.search.StaySearchGridRow;
import com.ihg.crm.automation.selenium.tests.servicecenter.LoginLogout;

public class SearchGuestByStayTest extends LoginLogout
{
    private final static String BY_UNIQUE_NBR = "SELECT HTL_CD, "//
            + "TO_CHAR(CK_OUT_DT, ''DDMonYY'') AS CK_OUT_DT, FOL_NBR, RM_NBR_TXT, CONF_NBR, FRST_NM, LST_NM, MBR.MBRSHP_ID " //
            + "FROM STY.STY_FOL FOL JOIN DGST.MBRSHP MBR ON FOL.DGST_MSTR_KEY = MBR.DGST_MSTR_KEY "//
            + "WHERE NOT EXISTS (SELECT 1 FROM STY.STY_FOL FOL2 WHERE FOL2.STY_FOL_KEY <> FOL.STY_FOL_KEY " //
            + "AND FOL2.{0} = FOL.{0} " //
            + "AND FOL2.HTL_CD = FOL.HTL_CD " //
            + "AND FOL.CK_OUT_DT BETWEEN FOL2.CK_IN_TS-1 AND FOL2.CK_OUT_DT)" //
            + "AND EXISTS (SELECT 1 FROM DGST.MBRSHP MBR "//
            + "WHERE MBR.DGST_MSTR_KEY = FOL.DGST_MSTR_KEY "//
            + "GROUP BY DGST_MSTR_KEY HAVING COUNT(LYTY_PGM_CD) = 1) "//
            + "AND MBR.DGST_MSTR_KEY = FOL.DGST_MSTR_KEY " + "AND MBR.LYTY_PGM_CD =''PC'' "//
            + "AND EVN_TYP IS NOT NULL "//
            + "AND FOL_NBR IS NOT NULL "//
            + "AND RM_NBR_TXT IS NOT NULL "//
            + "AND CONF_NBR IN (SELECT DISTINCT CONF_NBR FROM STY.STY_FOL WHERE CONF_NBR IS NOT NULL AND ROWNUM < 5) "//
            + "AND FRST_NM IS NOT NULL "//
            + "AND LST_NM IS NOT NULL " //
            + "AND ENTERPRISE_ID IN "//
            + "(SELECT M.ENTERPRISE_ID FROM DGST.GST_MSTR M WHERE M.ENTERPRISE_ID <> -1) " //
            + "AND ROWNUM < 2";

    private GuestSearch guestSearch = new GuestSearch();
    private SearchByStay searchByStay = guestSearch.getSearchByStay();

    private Map<String, Object> map;
    private String hotelCode;
    private String folioNumber;
    private String roomNumber;
    private String confirmationNumber;
    private String membershipId;
    private String firstName;
    private String lastName;
    private String stayDate;

    private void findStay(String query)
    {
        map = jdbcTemplate.queryForMap(query);
        hotelCode = map.get("HTL_CD").toString();
        folioNumber = map.get("FOL_NBR").toString();
        roomNumber = map.get("RM_NBR_TXT").toString();
        confirmationNumber = map.get("CONF_NBR").toString();
        membershipId = map.get("MBRSHP_ID").toString();
        firstName = map.get("FRST_NM").toString().trim();
        lastName = map.get("LST_NM").toString().trim();
        stayDate = map.get("CK_OUT_DT").toString().trim();
    }

    private void verifySearchResults()
    {
        StaySearchGrid staySearchGrid = new StaySearchGrid();
        verifyThat(staySearchGrid, size(1));

        StaySearchGridRow customerSearchGridRow = staySearchGrid.getRow(1);
        verifyThat(customerSearchGridRow.getCell(StaySearchGridCell.MEMBER_NUMBER), hasText(membershipId));
        verifyThat(customerSearchGridRow.getCell(StaySearchGridCell.GUEST_NAME), hasText(containsString(firstName)));
        verifyThat(customerSearchGridRow.getCell(StaySearchGridCell.GUEST_NAME), hasText(containsString(lastName)));
        verifyThat(customerSearchGridRow.getCell(StaySearchGridCell.HOTEL), hasText(hotelCode));
        verifyThat(customerSearchGridRow.getCell(StaySearchGridCell.CHECK_OUT), hasText(stayDate));
        verifyThat(customerSearchGridRow.getCell(StaySearchGridCell.ROOM_NUMBER), hasText(roomNumber));
        verifyThat(customerSearchGridRow.getCell(StaySearchGridCell.CONFIRMATION_NUMBER), hasText(confirmationNumber));
    }

    @BeforeClass
    public void before()
    {
        login();
    }

    @Test
    public void searchByHotelCheckoutFolio()
    {
        findStay(format(BY_UNIQUE_NBR, "FOL_NBR"));
        guestSearch.clickClear();

        searchByStay.expand();
        searchByStay.getHotel().type(hotelCode);
        searchByStay.getStayDate().type(stayDate);
        searchByStay.getFolioNumber().type(folioNumber);
        guestSearch.clickSearch();

        verifySearchResults();
    }

    @Test
    public void searchByHotelCheckoutRoom()
    {
        findStay(format(BY_UNIQUE_NBR, "RM_NBR_TXT"));
        guestSearch.clickClear();

        searchByStay.expand();
        searchByStay.getHotel().type(hotelCode);
        searchByStay.getStayDate().type(stayDate);
        searchByStay.getRoomNumber().type(roomNumber);
        guestSearch.clickSearch();

        verifySearchResults();
    }

    @Test
    public void searchByHotelCheckoutConfirmation()
    {
        findStay(format(BY_UNIQUE_NBR, "CONF_NBR"));
        guestSearch.clickClear();

        searchByStay.expand();
        searchByStay.getHotel().type(hotelCode);
        searchByStay.getStayDate().type(stayDate);
        searchByStay.getConfirmationNumber().type(confirmationNumber);
        guestSearch.clickSearch();

        verifySearchResults();
    }
}
