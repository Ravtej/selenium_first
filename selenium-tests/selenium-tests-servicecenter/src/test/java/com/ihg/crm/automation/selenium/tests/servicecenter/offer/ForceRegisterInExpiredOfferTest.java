package com.ihg.crm.automation.selenium.tests.servicecenter.offer;

import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyError;
import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyNoError;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static org.hamcrest.core.StringContains.containsString;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.offer.GuestOffer;
import com.ihg.automation.selenium.common.offer.Offer;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.gwt.components.input.Input;
import com.ihg.automation.selenium.gwt.components.utils.WaitUtils;
import com.ihg.automation.selenium.matchers.component.HasTipText;
import com.ihg.automation.selenium.servicecenter.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.servicecenter.pages.event.offer.OfferEventPage;
import com.ihg.automation.selenium.servicecenter.pages.event.offer.OfferPopUp;
import com.ihg.automation.selenium.servicecenter.pages.offer.ForceRegistrationPopUp;
import com.ihg.automation.selenium.servicecenter.pages.offer.OfferRegistrationPopUp;
import com.ihg.crm.automation.selenium.tests.servicecenter.LoginLogout;

public class ForceRegisterInExpiredOfferTest extends LoginLogout
{
    private static final String EXPIRED_OFFER_CODE = "8051922";
    private static final String NOT_ELIGIBLE_OFFER_FOR_CHINA = "8530441";
    private static final String INVALID_OFFER = "test";
    private Offer offer;
    private Member member = new Member();
    private final static String OFFER_NAME = "SELECT LYTY_OFFER_NM FROM OFFER.LYTY_OFFER" //
            + " WHERE LYTY_OFFER_KEY=%s";

    @BeforeClass
    public void beforeClass()
    {
        member.setPersonalInfo(MemberPopulateHelper.getSimplePersonalInfo());
        member.addAddress(MemberPopulateHelper.getUnitedStatesAddress());
        member.addProgram(Program.RC);

        String offerName = jdbcTemplate.queryForMap(String.format(OFFER_NAME, EXPIRED_OFFER_CODE)).get("LYTY_OFFER_NM")
                .toString();
        offer = Offer.builder(EXPIRED_OFFER_CODE, offerName).status("Expired").build();

        login();

        new EnrollmentPage().enroll(member);
    }

    @Test
    public void tryToRegisterInNotEligibleOffer()
    {
        OfferRegistrationPopUp offerRegistrationPopUp = new OfferRegistrationPopUp();
        offerRegistrationPopUp.goTo();
        verifyThat(offerRegistrationPopUp.getMembershipId(), hasText(member.getRCProgramId()));
        offerRegistrationPopUp.getRegistrationCode().typeAndWait(NOT_ELIGIBLE_OFFER_FOR_CHINA);
        offerRegistrationPopUp.clickRegister(verifyError(
                "The Guest is not eligible for the offer.\nThe Guest's country is not TAIWAN, PROVINCE OF CHINA,MACAO,MONGOLIA,HONG KONG or CHINA."));
        offerRegistrationPopUp.clickClose();

        OfferEventPage offerPage = new OfferEventPage();
        offerPage.goTo();
        verifyThat(offerPage.getMemberOffersGrid().getRowByOfferCode(NOT_ELIGIBLE_OFFER_FOR_CHINA), displayed(false));
    }

    @Test(dependsOnMethods = { "tryToRegisterInNotEligibleOffer" }, alwaysRun = true)
    public void tryToRegisterInInvalidOffer()
    {
        OfferRegistrationPopUp offerRegistrationPopUp = new OfferRegistrationPopUp();
        offerRegistrationPopUp.goTo();

        Input registrationCode = offerRegistrationPopUp.getRegistrationCode();
        registrationCode.typeAndWait(INVALID_OFFER);
        verifyThat(registrationCode,
                HasTipText.hasWarningTipTextWithWait("Invalid Offer Registration Code / Invalid Loyalty Offer Code."));
        offerRegistrationPopUp.clickClose();

        OfferEventPage offerPage = new OfferEventPage();
        offerPage.goTo();
        verifyThat(offerPage.getMemberOffersGrid().getRowByOfferCode(INVALID_OFFER), displayed(false));
    }

    @Test(dependsOnMethods = { "tryToRegisterInInvalidOffer" }, alwaysRun = true)
    public void cancelForceRegisterInOffer()
    {
        OfferRegistrationPopUp offerRegistrationPopUp = new OfferRegistrationPopUp();
        offerRegistrationPopUp.registerInOffer(offer.getCode());

        ForceRegistrationPopUp forceRegistrationPopUp = new ForceRegistrationPopUp();
        verifyThat(forceRegistrationPopUp, displayed(true));
        verifyThat(forceRegistrationPopUp, hasText(containsString("Offer is expired: force register or cancel.")));

        forceRegistrationPopUp.getCancel().click();
        verifyThat(forceRegistrationPopUp, displayed(false));

        offerRegistrationPopUp.clickClose(verifyNoError());

        OfferEventPage offerPage = new OfferEventPage();
        offerPage.goTo();
        verifyThat(offerPage.getMemberOffersGrid().getRowByOfferCode(offer.getCode()), displayed(false));
    }

    @Test(dependsOnMethods = { "cancelForceRegisterInOffer" }, alwaysRun = true)
    public void forceRegisterInOffer()
    {
        new OfferRegistrationPopUp().registerInOffer(offer.getCode());

        ForceRegistrationPopUp forceRegistrationPopUp = new ForceRegistrationPopUp();
        forceRegistrationPopUp.forceRegisterToOffer("Agent Error");

        OfferPopUp offerPopUp = new OfferPopUp();
        new WaitUtils().waitExecutingRequest();
        verifyThat(offerPopUp, displayed(true));

        offerPopUp.getOfferDetailsTab().getOfferDetails().verify(offer);
        offerPopUp.close();

        new OfferEventPage().getMemberOffersGrid().getRowByOfferCode(offer.getCode()).verify(new GuestOffer(offer));
    }

}
