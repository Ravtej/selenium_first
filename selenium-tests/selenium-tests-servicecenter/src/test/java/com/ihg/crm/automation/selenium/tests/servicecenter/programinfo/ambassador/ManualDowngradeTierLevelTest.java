package com.ihg.crm.automation.selenium.tests.servicecenter.programinfo.ambassador;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.Constant.NOT_AVAILABLE;
import static com.ihg.automation.selenium.common.event.EventTransactionType.TierLevel.DOWNGRADE_MANUAL;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static com.ihg.automation.selenium.matchers.component.ViewableMatcher.hasTextInView;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.common.DateUtils;
import com.ihg.automation.selenium.common.ProgramsGrid;
import com.ihg.automation.selenium.common.event.AllEventsRow;
import com.ihg.automation.selenium.common.member.AmbassadorOfferCode;
import com.ihg.automation.selenium.common.member.AmbassadorUtils;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.types.program.AmbassadorLevel;
import com.ihg.automation.selenium.common.types.program.AmbassadorLevelReason;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.servicecenter.pages.LeftPanel;
import com.ihg.automation.selenium.servicecenter.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsGridRow;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsPage;
import com.ihg.automation.selenium.servicecenter.pages.event.level.ManualTierLevelChangeGridRowContainer;
import com.ihg.automation.selenium.servicecenter.pages.programs.RenewExtendProgramSummary;
import com.ihg.automation.selenium.servicecenter.pages.programs.amb.AmbassadorPage;
import com.ihg.crm.automation.selenium.tests.servicecenter.LoginLogout;

public class ManualDowngradeTierLevelTest extends LoginLogout
{

    private Member member = new Member();
    private AmbassadorPage ambassadorPage = new AmbassadorPage();
    private RenewExtendProgramSummary ambSummary;
    private AllEventsPage allEventsPage = new AllEventsPage();
    private AllEventsRow downgradeRAMEventRow = new AllEventsRow(DateUtils.getFormattedDate(), DOWNGRADE_MANUAL,
            "AMB - AMBASSADOR", "", "");

    @BeforeClass
    public void beforeClass()
    {
        member.setPersonalInfo(MemberPopulateHelper.getSimplePersonalInfo());
        member.addAddress(MemberPopulateHelper.getUnitedStatesAddress());
        member.addProgram(Program.AMB);
        member.setAmbassadorOfferCode(AmbassadorOfferCode.CHSRO); // CHSRO -
        // CHARLESTON
        // RESERVATIONS
        member.setAmbassadorAmount(AmbassadorUtils.AMBASSADOR_COMP_PROMO_$0);

        login();

        member = new EnrollmentPage().enroll(member);

        ambassadorPage.goTo();
        ambSummary = ambassadorPage.getSummary();
        ambSummary.setTierLevelWithoutExpiration(AmbassadorLevel.RAM, AmbassadorLevelReason.SYSTEM_ERROR);
    }

    @Test
    public void downgradeTierLevel()
    {
        ambassadorPage = new AmbassadorPage();
        ambSummary = ambassadorPage.getSummary();
        ambSummary.setTierLevelWithoutExpiration(AmbassadorLevel.AMB, null);
    }

    @Test(dependsOnMethods = { "downgradeTierLevel" })
    public void verifyDowngradeResult()
    {
        ambassadorPage = new AmbassadorPage();
        ambSummary = ambassadorPage.getSummary();
        verifyThat(ambSummary.getTierLevel(), hasTextInView(AmbassadorLevel.AMB.getValue()));
        verifyThat(ambSummary.getTierLevelReason(), displayed(false));
    }

    @Test(dependsOnMethods = { "verifyDowngradeResult" }, alwaysRun = true)
    public void verifyLeftPanelAMBLevel()
    {
        ProgramsGrid grid = new LeftPanel().getProgramInfoPanel().getPrograms();
        verifyThat(grid.getProgram(Program.AMB).getLevelCode(), hasText(AmbassadorLevel.AMB.getCode()));
    }

    @Test(dependsOnMethods = { "verifyLeftPanelAMBLevel" }, alwaysRun = true)
    public void verifyAMBDowngradeEventGridRow()
    {
        allEventsPage.goTo();
        AllEventsGridRow row = allEventsPage.getGrid().getRow(downgradeRAMEventRow.getTransType(),
                downgradeRAMEventRow.getDetail());
        row.verify(downgradeRAMEventRow);
    }

    @Test(dependsOnMethods = { "verifyAMBDowngradeEventGridRow" }, alwaysRun = true)
    public void verifyAMBDowngradeEventGridRowDetails()
    {
        allEventsPage.goTo();
        AllEventsGridRow row = allEventsPage.getGrid().getRow(downgradeRAMEventRow.getTransType(),
                downgradeRAMEventRow.getDetail());
        ManualTierLevelChangeGridRowContainer container = row.expand(ManualTierLevelChangeGridRowContainer.class);
        container.verify(NOT_AVAILABLE, NOT_AVAILABLE, NOT_AVAILABLE, helper);
    }
}
