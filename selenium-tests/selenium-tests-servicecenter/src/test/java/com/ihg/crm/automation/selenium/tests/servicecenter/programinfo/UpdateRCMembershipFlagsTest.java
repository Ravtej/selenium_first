package com.ihg.crm.automation.selenium.tests.servicecenter.programinfo;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static org.hamcrest.CoreMatchers.containsString;

import java.util.ArrayList;
import java.util.Arrays;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.CustomerInfoFlags;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.common.types.program.TierLevelFlag;
import com.ihg.automation.selenium.matchers.component.ContainsDualListItems;
import com.ihg.automation.selenium.servicecenter.pages.LeftPanel;
import com.ihg.automation.selenium.servicecenter.pages.account.AccountInfoPage;
import com.ihg.automation.selenium.servicecenter.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.servicecenter.pages.programs.ProgramSummary;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.RewardClubPage;
import com.ihg.crm.automation.selenium.tests.servicecenter.LoginLogout;

public class UpdateRCMembershipFlagsTest extends LoginLogout
{

    private Member member = new Member();
    private RewardClubPage rewardClubPage = new RewardClubPage();
    private ProgramSummary summary;
    private ArrayList<String> expectedListContent = new ArrayList<String>(TierLevelFlag.getRcValueList());

    private final static String FLAG_TO_SELECT = TierLevelFlag.AMERICA_AGENCY_CONTACTS.getValue();

    @BeforeClass
    public void beforeClass()
    {
        member.setPersonalInfo(MemberPopulateHelper.getSimplePersonalInfo());
        member.addAddress(MemberPopulateHelper.getUnitedStatesAddress());
        member.addProgram(Program.RC);

        login();

        member = new EnrollmentPage().enroll(member);
    }

    @Test
    public void navigateRCProgramSummary()
    {
        rewardClubPage.goTo();
        summary = rewardClubPage.getSummary();
        summary.clickEdit();
    }

    @Test(dependsOnMethods = { "navigateRCProgramSummary" })
    public void verifyAvailableSummaryFlags()
    {
        verifyThat(summary.getFlags().getLeftList(), ContainsDualListItems.containsItems(expectedListContent));
    }

    @Test(dependsOnMethods = { "verifyAvailableSummaryFlags", "navigateRCProgramSummary" }, alwaysRun = true)
    public void selectTheFlag()
    {
        summary.getFlags().select(FLAG_TO_SELECT);
        verifyThat(summary.getFlags().getRightList(),
                ContainsDualListItems.containsItems(Arrays.asList(FLAG_TO_SELECT)));
        expectedListContent.remove(FLAG_TO_SELECT);
        verifyThat(summary.getFlags().getLeftList(), ContainsDualListItems.containsItems(expectedListContent));
        summary.clickSave();
    }

    @Test(dependsOnMethods = { "selectTheFlag" }, alwaysRun = true)
    public void verifyProgramInformationFlags()
    {
        rewardClubPage = new RewardClubPage();
        verifyThat(rewardClubPage.getSummary(), hasText(containsString(FLAG_TO_SELECT)));
    }

    @Test(dependsOnMethods = { "verifyProgramInformationFlags" }, alwaysRun = true)
    public void verifyLeftPanelFlags()
    {
        CustomerInfoFlags flags = new LeftPanel().getCustomerInfoPanel().getFlags();
        verifyThat(flags, hasText(containsString(FLAG_TO_SELECT)));
    }

    @Test(dependsOnMethods = { "verifyLeftPanelFlags" }, alwaysRun = true)
    public void verifyAccountInformationFlags()
    {
        new LeftPanel().getCustomerInfoPanel().getFlags().getFlag(FLAG_TO_SELECT).clickAndWait();

        verifyThat(new AccountInfoPage().getFlags(), hasText(containsString(FLAG_TO_SELECT)));
    }
}
