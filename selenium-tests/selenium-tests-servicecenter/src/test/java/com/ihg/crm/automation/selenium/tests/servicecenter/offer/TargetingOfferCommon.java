package com.ihg.crm.automation.selenium.tests.servicecenter.offer;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static com.ihg.automation.selenium.matchers.component.IsDisplayed.displayed;

import java.util.Map;

import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.gwt.components.utils.WaitUtils;
import com.ihg.automation.selenium.servicecenter.pages.event.offer.OfferPopUp;
import com.ihg.crm.automation.selenium.tests.servicecenter.LoginLogout;

public class TargetingOfferCommon extends LoginLogout
{
    protected static final String TARGET_OFFER_ID = "8870471";
    private final static String OFFER_ID = "SELECT LYTY_OFFER_KEY"// +
            + " FROM OFFER.LYTY_OFFER "//
            + " WHERE TGT_REQD_IND = 'Y'"//
            + " AND LST_EFF_TS > SYSDATE"//
            + " AND ROWNUM < 2";

    private final static String INSERT = "INSERT INTO OFFER.GST_TGT_OFFER_ELIG"//
            + " (GST_OFFER_TGT_ELIG_KEY,LYTY_OFFER_KEY,"//
            + " DGST_MSTR_KEY,MBRSHP_ID,REG_END_DT,CREAT_USR_ID,"//
            + " CREAT_TS,LST_UPDT_USR_ID,LST_UPDT_TS)"//
            + " VALUES (OFFER.GST_OFFER_TGT_ELIG_KEY_SEQ.NEXTVAL,'%1$s',"//
            + " (SELECT DGST_MSTR_KEY FROM DGST.MBRSHP WHERE MBRSHP_ID = '%2$s'),"//
            + " '%2$s', %3$s,'LYTYUIAUTO',SYSDATE,'LYTYUIAUTO',SYSDATE)";

    protected String getOfferId()
    {
        Map<String, Object> map = jdbcTemplate.queryForMap(OFFER_ID);

        return map.get("LYTY_OFFER_KEY").toString();
    }

    protected void insert(String offerId, Member member, String regEndDate)
    {
        jdbcTemplateUpdate.update(String.format(INSERT, offerId, member.getRCProgramId(), regEndDate));
    }

    protected void verifyTargetExpiry(String regEndDate)
    {
        OfferPopUp offerPopUp = new OfferPopUp();
        new WaitUtils().waitExecutingRequest();
        verifyThat(offerPopUp, displayed(true));
        verifyThat(offerPopUp.getOfferDetailsTab().getOfferDetails().getOfferEligibility().getTargetExpiry(),
                hasText(regEndDate));
        offerPopUp.close();
    }
}
