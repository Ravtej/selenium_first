package com.ihg.crm.automation.selenium.tests.servicecenter.tierlevelexpiration;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.types.program.ProgramStatus.OPEN;
import static com.ihg.automation.selenium.common.types.program.RewardClubLevel.CLUB;
import static com.ihg.automation.selenium.common.types.program.RewardClubLevel.PLTN;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static com.ihg.automation.selenium.servicecenter.pages.event.offer.MemberOfferGridRow.MemberOfferCell.OFFER_CODE;
import static com.ihg.automation.selenium.servicecenter.pages.event.offer.MemberOfferGridRow.MemberOfferCell.WINS;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.Constant;
import com.ihg.automation.selenium.common.deposit.Deposit;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.servicecenter.pages.LeftPanel;
import com.ihg.automation.selenium.servicecenter.pages.deposit.CreateDepositPopUp;
import com.ihg.automation.selenium.servicecenter.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.servicecenter.pages.event.offer.MemberOfferGridRow;
import com.ihg.automation.selenium.servicecenter.pages.event.offer.OfferEventPage;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.RewardClubPage;
import com.ihg.crm.automation.selenium.tests.servicecenter.LoginLogout;

public class AwardTierLevelByOfferTest extends LoginLogout
{
    private final static String OFFER_AWARDING_TIER_LEVEL = "8730444";
    private RewardClubPage rewardClubPage;
    private Deposit deposit = new Deposit();

    @BeforeClass
    public void beforeClass()
    {
        Member member = new Member();
        member.setPersonalInfo(MemberPopulateHelper.getSimplePersonalInfo());
        member.addAddress(MemberPopulateHelper.getUnitedStatesAddress());
        member.addProgram(Program.RC);

        deposit.setTransactionId("HCVTXF");
        deposit.setHotel("ATLCP");
        deposit.setLoyaltyUnitsAmount("200");
        deposit.setLoyaltyUnitsType(Constant.RC_POINTS);

        login();
        new EnrollmentPage().enroll(member);
        new LeftPanel().verifyRCLevel(CLUB);
    }

    @Test
    public void createDepositAndWinToOffer()
    {
        new CreateDepositPopUp().createDeposit(deposit, false);
        OfferEventPage offerPage = new OfferEventPage();
        offerPage.goTo();
        MemberOfferGridRow offerRow = offerPage.getMemberOffersGrid().getRow(1);
        verifyThat(offerRow.getCell(OFFER_CODE), hasText(OFFER_AWARDING_TIER_LEVEL));
        verifyThat(offerRow.getCell(WINS), hasText("1"));
    }

    @Test(dependsOnMethods = "createDepositAndWinToOffer", alwaysRun = true)
    public void verifyPlatinumNextYearTierLevel()
    {
        rewardClubPage = new RewardClubPage();
        rewardClubPage.goTo();
        rewardClubPage.getSummary().verify(OPEN, PLTN, 1);
    }
}
