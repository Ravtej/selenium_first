package com.ihg.crm.automation.selenium.tests.servicecenter.stay;

import static com.ihg.automation.common.DateUtils.currentYear;
import static com.ihg.automation.common.DateUtils.now;
import static com.ihg.automation.selenium.assertions.Assume.assumeThat;
import static com.ihg.automation.selenium.common.event.EarningEventFactory.getBaseUnitsEvent;
import static com.ihg.automation.selenium.matchers.date.IsAfterDate.isAfter;
import static com.ihg.automation.selenium.servicecenter.pages.event.stay.StayEventsPage.STAY_SUCCESS_CREATED;

import org.joda.time.LocalDate;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.billing.EventBillingDetailsTab;
import com.ihg.automation.selenium.common.earning.EventEarningDetailsTab;
import com.ihg.automation.selenium.common.event.AllEventsRow;
import com.ihg.automation.selenium.common.event.AllEventsRowConverter;
import com.ihg.automation.selenium.common.member.AnnualActivity;
import com.ihg.automation.selenium.common.member.LifetimeActivity;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.stay.Revenue;
import com.ihg.automation.selenium.common.stay.Stay;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.common.types.program.RewardClubLevel;
import com.ihg.automation.selenium.formatter.Converter;
import com.ihg.automation.selenium.gwt.components.Mode;
import com.ihg.automation.selenium.servicecenter.pages.LeftPanel;
import com.ihg.automation.selenium.servicecenter.pages.annual.AnnualActivityPage;
import com.ihg.automation.selenium.servicecenter.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsGridRow;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsPage;
import com.ihg.automation.selenium.servicecenter.pages.event.stay.AdjustStayPopUp;
import com.ihg.automation.selenium.servicecenter.pages.event.stay.StayEventsPage;
import com.ihg.automation.selenium.servicecenter.pages.event.stay.StayGuestGridRow;
import com.ihg.automation.selenium.servicecenter.pages.event.stay.StayGuestGridRowContainer;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.RewardClubPage;
import com.ihg.crm.automation.selenium.tests.servicecenter.LoginLogout;

public class CreateMissingStayBillingCommonTest extends LoginLogout
{
    private Member member = new Member();
    private Stay stay = new Stay();
    private StayEventsPage stayEventsPage = new StayEventsPage();
    private AdjustStayPopUp adjustStayPopUp = new AdjustStayPopUp();
    private StayGuestGridRow stayGuestGridRow;

    @BeforeClass
    public void beforeClass()
    {
        assumeThat(now(), isAfter(new LocalDate(currentYear(), 1, 4)), "Checkout date should be in current year");

        stay.setHotelCode("SGNHB");
        stay.setBrandCode("ICON");
        stay.setNights(2);
        stay.setCheckInOutByNights(2);
        stay.setQualifyingNights(2);
        stay.setTierLevelNights("2");
        stay.setIhgUnits("1250");
        stay.setAvgRoomRate(250.00);
        stay.setRateCode("1");

        Revenue room = stay.getRevenues().getRoom();
        room.setAmount(500.00);
        room.setUsdAmount(500.00);
        room.setUsdAmount(500.00);
        room.setAmount(500.00);
        room.setAllowOverride(true);

        member.setPersonalInfo(MemberPopulateHelper.getSimplePersonalInfo());
        member.addAddress(MemberPopulateHelper.getUnitedStatesAddress());
        member.addProgram(Program.RC);

        login();

        member = new AnnualActivityPage().captureCounters(new EnrollmentPage().enroll(member), jdbcTemplate);
    }

    @Test(priority = 10)
    public void createStay()
    {
        stayEventsPage.createStay(stay, STAY_SUCCESS_CREATED);
    }

    @Test(priority = 20)
    public void verifyStayCreationEvent()
    {
        stayGuestGridRow = stayEventsPage.getGuestGrid().getRow(1);
        stayGuestGridRow.verify(stay);
        stayGuestGridRow.expand(StayGuestGridRowContainer.class).verifyRoomRevenueDetails(stay.getRevenues().getRoom(),
                Mode.EDIT);
    }

    @Test(priority = 40)
    public void verifyEarningDetailsInPopUp()
    {
        stayGuestGridRow.getDetails().clickDetails();

        EventEarningDetailsTab earningDetailsTab = adjustStayPopUp.getEarningDetailsTab();
        earningDetailsTab.goTo();

        String pointsForStay = stay.getIhgUnits();

        earningDetailsTab.verifyBasicDetailsForPoints(pointsForStay, pointsForStay);

        earningDetailsTab.getGrid().verifyRowsFoundByType(getBaseUnitsEvent(pointsForStay, pointsForStay));
    }

    @Test(priority = 50)
    public void verifyBillingDetailsInPopUp()
    {
        EventBillingDetailsTab billingDetailsTab = adjustStayPopUp.getBillingDetailsTab();
        billingDetailsTab.goTo();
        billingDetailsTab.getEventBillingDetail(1).getBaseLoyaltyUnits().verifyUSDAmount("5.93", stay.getHotelCode());

        adjustStayPopUp.clickClose();
    }

    @Test(priority = 60)
    public void verifyStayCreateEvent()
    {
        AllEventsRow stayEvent = AllEventsRowConverter.convert(stay);

        AllEventsPage allEventsPage = new AllEventsPage();
        allEventsPage.goTo();

        AllEventsGridRow allEventsGridRow = allEventsPage.getGrid().getRow(stayEvent.getTransType());
        allEventsGridRow.verify(stayEvent);
        allEventsGridRow.expand(StayGuestGridRowContainer.class).verifyRoomRevenueDetails(stay.getRevenues().getRoom(),
                Mode.VIEW);
    }

    @Test(priority = 80)
    public void verifyLeftPanelRCPointsBalance()
    {
        new LeftPanel().getProgramInfoPanel().getPrograms().getRewardClubProgram().verify(RewardClubLevel.CLUB,
                "1,250");
    }

    @Test(priority = 90)
    public void verifyActivityCounters()
    {
        AnnualActivity annualActivity = member.getAnnualActivity();
        annualActivity.setQualifiedNights(stay.getNights());
        annualActivity.setTierLevelNightsFromString(stay.getTierLevelNights());

        int ihgUnits = Converter.stringToInteger(stay.getIhgUnits());
        annualActivity.setTotalQualifiedUnits(ihgUnits);
        annualActivity.setTotalUnits(ihgUnits);

        LifetimeActivity lifetimeActivity = member.getLifetimeActivity();
        lifetimeActivity.setBaseUnits(ihgUnits);
        lifetimeActivity.setTotalUnits(ihgUnits);
        lifetimeActivity.setCurrentBalance(ihgUnits);

        new AnnualActivityPage().verifyCounters(member);
    }

    @Test(priority = 100)
    public void verifyRewardNightTierLevelCounters()
    {
        RewardClubPage rewardClubPage = new RewardClubPage();
        rewardClubPage.goTo();
        rewardClubPage.getCurrentYearAnnualActivities().getPointsRow().verifyCurrent(stay.getIhgUnits());
    }
}
