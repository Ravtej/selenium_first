package com.ihg.crm.automation.selenium.tests.servicecenter.catalog;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.event.EventTransactionType.Order.REDEEM;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasTextAsInt;
import static com.ihg.automation.selenium.matchers.component.GridSize.size;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static com.ihg.automation.selenium.matchers.text.ContainsIgnoreCase.containsIgnoreCase;
import static com.ihg.automation.selenium.servicecenter.pages.catalog.CatalogItemsGridRow.CatalogItemsGridCell.CATALOG_ID;
import static com.ihg.automation.selenium.servicecenter.pages.catalog.CatalogItemsGridRow.CatalogItemsGridCell.ITEM_NAME;
import static org.hamcrest.core.AllOf.allOf;
import static org.hamcrest.number.OrderingComparison.greaterThanOrEqualTo;
import static org.hamcrest.number.OrderingComparison.lessThanOrEqualTo;

import java.util.Map;

import com.ihg.automation.selenium.common.catalog.CatalogItem;
import com.ihg.automation.selenium.common.event.AllEventsRow;
import com.ihg.automation.selenium.common.order.Order;
import com.ihg.automation.selenium.servicecenter.pages.catalog.CatalogItemDetailsPopUp;
import com.ihg.automation.selenium.servicecenter.pages.catalog.CatalogItemsGrid;
import com.ihg.automation.selenium.servicecenter.pages.catalog.CatalogItemsGridRow.CatalogItemsGridCell;
import com.ihg.automation.selenium.servicecenter.pages.catalog.CatalogPage;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsGridRow;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsPage;
import com.ihg.automation.selenium.servicecenter.pages.event.order.OrderEventsPage;
import com.ihg.automation.selenium.servicecenter.pages.event.order.OrderGridRow;
import com.ihg.automation.selenium.servicecenter.pages.event.order.OrderRedeemGridRowContainer;
import com.ihg.crm.automation.selenium.tests.servicecenter.LoginLogout;

public class CatalogCommon extends LoginLogout
{
    private CatalogPage catalogPage;
    private CatalogItemsGrid catalogGrid;
    private Map<String, Object> map;
    protected final static String ITEM_ID = "IHG04";
    protected static final String MEMBER_ID = "SELECT M.MBRSHP_ID , M.CURRENT_BAL_UNIT_AMT POINTS"//
            + " FROM DGST.MBRSHP M"//
            + " INNER JOIN DGST.GST_CONTACT C ON C.DGST_MSTR_KEY = M.DGST_MSTR_KEY"//
            + " INNER JOIN DGST.GST_ADDR A ON C.GST_CONTACT_KEY=A.GST_CONTACT_KEY"//
            + " WHERE M.LYTY_PGM_CD = 'PC'"//
            + " AND A.CTRY_CD ='US'"//
            + " AND C.DFLT_PRF_IND ='Y'"//
            + " AND M.MBRSHP_STAT_CD = 'O'"//
            + " AND M.CURRENT_BAL_UNIT_AMT >='50000' AND M.CURRENT_BAL_UNIT_AMT <=200000"//
            + " AND ROWNUM < 2";
    protected static final String ITEM_ID_ORDER = "SELECT DISTINCT I.AWD_ID"//
            + " FROM CATLG.AWD I"//
            + " INNER JOIN CATLG.CATLG_AWD AW ON I.AWD_KEY = AW.AWD_KEY"//
            + " INNER JOIN CATLG.CATLG CTL ON AW.CATLG_KEY = CTL.CATLG_KEY"//
            + " INNER JOIN CATLG.CATLG_AUTH AU ON AW.CATLG_KEY = AU.CATLG_KEY"//
            + " LEFT OUTER JOIN REFDATA.RGN R ON AU.MKT_RGN_CD = R.RGN_CD"//
            + " WHERE I.STRT_DT < SYSDATE"//
            + " AND I.END_DT > SYSDATE"//
            + " AND  PGM_ID = 'PC'"//
            + " AND MKT_RGN_CD IN ('05388', '%')"//
            + " AND CTRY_CD IN ('US', '%')"//
            + " AND MBR_TYP IN ('%', 'CLUB')"//
            + " AND CATLG_TYP_CD  IN ('F', 'R')"//
            + " AND (CURRENT_DATE BETWEEN CTL.STRT_DT AND CTL.END_DT)"//
            + " AND I.BUYER_PT_QTY = 0 "//
            + " AND ROWNUM < 2";

    protected void clearSearchCriteria()
    {
        catalogPage = new CatalogPage();
        catalogPage.getButtonBar().clickClearCriteria();
    }

    protected void verifySearchByItemName(String itemName)
    {
        catalogPage = new CatalogPage();
        catalogPage.search(null, null, itemName, null, null);

        catalogGrid = catalogPage.getCatalogGrid();

        if (catalogGrid.getRowCount() != 0)
        {
            catalogGrid.verify(ITEM_NAME, hasText(containsIgnoreCase(itemName)));
        }
    }

    protected void verifySearchByCatalogId(String catalogId)
    {
        catalogPage = new CatalogPage();
        catalogPage.search(catalogId, null, null, null, null);

        catalogGrid = catalogPage.getCatalogGrid();

        if (catalogGrid.getRowCount() != 0)
        {
            catalogGrid.verify(CATALOG_ID, hasText(catalogId));
        }
    }

    protected void verifySearchByItemId(String itemId)
    {
        catalogPage = new CatalogPage();
        catalogPage.searchByItemId(itemId);

        catalogGrid = catalogPage.getCatalogGrid();
        verifyThat(catalogGrid, size(1));

        verifyThat(catalogGrid.getRow(1).getCell(CatalogItemsGridCell.ITEM_ID), hasText(itemId));
    }

    protected void verifySearchByLoyaltyUnitCostMin(String loyaltyUnitCostMin)
    {
        catalogPage.search(null, null, null, loyaltyUnitCostMin, null);

        catalogGrid = catalogPage.getCatalogGrid();
        verifyThat(catalogGrid, size(10));
        catalogGrid.verifyUnitCost(hasTextAsInt(greaterThanOrEqualTo(Integer.parseInt(loyaltyUnitCostMin))));
    }

    protected void verifySearchByLoyaltyUnitCostMax(String loyaltyUnitCostMax)
    {
        catalogPage = new CatalogPage();
        catalogPage.search(null, null, null, null, loyaltyUnitCostMax);

        catalogGrid = catalogPage.getCatalogGrid();
        verifyThat(catalogGrid, size(10));

        catalogGrid.verifyUnitCost(hasTextAsInt(lessThanOrEqualTo(Integer.parseInt(loyaltyUnitCostMax))));
    }

    protected void verifySearchByLoyaltyUnitCost(String loyaltyUnitCostMin, String loyaltyUnitCostMax)
    {
        catalogPage = new CatalogPage();
        catalogPage.search(null, null, null, loyaltyUnitCostMin, loyaltyUnitCostMax);

        catalogGrid = catalogPage.getCatalogGrid();
        verifyThat(catalogGrid, size(10));
        catalogGrid.verifyUnitCost(hasTextAsInt(allOf(greaterThanOrEqualTo(Integer.parseInt(loyaltyUnitCostMin)),
                lessThanOrEqualTo(Integer.parseInt(loyaltyUnitCostMax)))));
    }

    protected void verifyCatalogItemDetailsPopUp(CatalogItem catalogItem)
    {
        catalogPage = new CatalogPage();
        CatalogItemDetailsPopUp catalogItemDetailsPopUp = catalogPage.openCatalogItemDetailsPopUp();
        catalogItemDetailsPopUp.verify(catalogItem);
    }

    public String getItemId()
    {
        map = jdbcTemplate.queryForMap(ITEM_ID_ORDER);
        return map.get("AWD_ID").toString();
    }

    public void checkShippingDetails(Order order)
    {
        OrderEventsPage orderPage = new OrderEventsPage();

        OrderGridRow itemRow = orderPage.getOrdersGrid().getRow(1);

        OrderRedeemGridRowContainer orderRedeemGridRowContainer = itemRow.expand(OrderRedeemGridRowContainer.class);

        orderRedeemGridRowContainer.getShippingInfo().verify(order, true);
    }

    public void verifyRedeemEventOnAllEventsPage(AllEventsRow rowData)
    {
        AllEventsPage allEventsPage = new AllEventsPage();
        allEventsPage.goTo();
        AllEventsGridRow allEventsRow = allEventsPage.getGrid().getRow(REDEEM);
        allEventsRow.verify(rowData);
    }
}
