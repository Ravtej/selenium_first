package com.ihg.crm.automation.selenium.tests.servicecenter.deposit;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.event.EventTransactionType.Deposit.CO_PARTNER;
import static com.ihg.automation.selenium.common.event.EventTransactionType.Deposit.INCENTIVE;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.automation.selenium.matchers.component.GridSize.size;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static com.ihg.automation.selenium.matchers.component.IsInvalid.isHighlightedAsInvalid;
import static com.ihg.automation.selenium.servicecenter.pages.deposit.CreateDepositPopUp.CO_PARTNER_CREATION_IHG_UNITS_LIMIT;
import static com.ihg.automation.selenium.servicecenter.pages.deposit.CreateDepositPopUp.NOTE_TRANSACTION_IS_NOT_CO_PARTNER;
import static org.hamcrest.core.IsNot.not;
import static org.hamcrest.core.StringContains.containsString;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.common.DateUtils;
import com.ihg.automation.common.RandomUtils;
import com.ihg.automation.selenium.common.Constant;
import com.ihg.automation.selenium.common.billing.EventBillingDetailsTab;
import com.ihg.automation.selenium.common.deposit.Deposit;
import com.ihg.automation.selenium.common.earning.EventEarningDetailsTab;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.servicecenter.pages.deposit.CreateDepositPopUp;
import com.ihg.automation.selenium.servicecenter.pages.deposit.DepositDetailsPopUp;
import com.ihg.automation.selenium.servicecenter.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsPage;
import com.ihg.automation.selenium.servicecenter.pages.event.copartner.CoPartnerEventGridRow;
import com.ihg.automation.selenium.servicecenter.pages.event.copartner.CoPartnerEventsPage;
import com.ihg.automation.selenium.servicecenter.pages.event.copartner.CoPartnerEventsPage.CoPartnerEventsButtonBar;
import com.ihg.automation.selenium.servicecenter.pages.event.deposit.DepositGridRowContainer;
import com.ihg.automation.selenium.common.hotel.HotelDetailsPopUp;
import com.ihg.crm.automation.selenium.tests.servicecenter.LoginLogout;

public class TransactionTypeCoPartnerTest extends LoginLogout
{
    private CoPartnerEventsButtonBar searchFields;
    private CoPartnerEventsPage coPartnerEventsPage = new CoPartnerEventsPage();
    private CreateDepositPopUp deposPopUp = new CreateDepositPopUp();
    private DepositGridRowContainer depositGridRowContainer;

    private Member member = new Member();
    private Deposit depositCoPartner = new Deposit();
    private Deposit depositNotCoPartner = new Deposit();

    @BeforeClass
    public void beforeClass()
    {
        member.addProgram(Program.RC);
        member.setPersonalInfo(MemberPopulateHelper.getSimplePersonalInfo());
        member.addAddress(MemberPopulateHelper.getUnitedStatesAddress());

        depositCoPartner.setTransactionId("DBWOLF");
        depositCoPartner.setTransactionType(CO_PARTNER);
        depositCoPartner.setTransactionName("PC MALL- WOLF CAMERA");
        depositCoPartner.setTransactionDescription("PC MALL- WOLF CAMERA");
        depositCoPartner.setHotel("ATLCP");
        depositCoPartner.setCheckInDate(DateUtils.getDateBackward(5));
        depositCoPartner.setCheckOutDate(DateUtils.getDateBackward(2));
        depositCoPartner.setPartnerTransactionDate(DateUtils.getDateBackward(1));
        depositCoPartner.setPartnerComment("SC UI test automation" + DateUtils.now());
        depositCoPartner.setOrderTrackingNumber(RandomUtils.getRandomNumber(8));
        depositCoPartner.setLoyaltyUnitsAmount(String.valueOf(CO_PARTNER_CREATION_IHG_UNITS_LIMIT));
        depositCoPartner.setLoyaltyUnitsType(Constant.RC_POINTS);

        depositNotCoPartner.setTransactionId("CNFPTS");
        depositNotCoPartner.setTransactionType(INCENTIVE);
        depositNotCoPartner.setTransactionName("IHG RWDS CLUB BONUS");
        depositNotCoPartner.setTransactionDescription("REWARDS CLUB BONUS");
        depositNotCoPartner.setLoyaltyUnitsType(Constant.RC_POINTS);
        depositNotCoPartner.setLoyaltyUnitsAmount("20000");
        depositNotCoPartner.setHotel("ATLCP");

        login();

        new EnrollmentPage().enroll(member);
    }

    @Test
    public void verifySearchFieldsPresent()
    {
        coPartnerEventsPage.goTo();
        searchFields = coPartnerEventsPage.getSearchFields();
        verifyThat(searchFields.getPartnerName(), displayed(true));
        verifyThat(searchFields.geTransactionDate(), displayed(true));
        verifyThat(searchFields.getTransactionDatePeriod(), displayed(true));
        verifyThat(searchFields.getTransactionId(), displayed(true));
        verifyThat(searchFields.getKeywords(), displayed(true));

        verifyThat(searchFields.getSearch(), displayed(true));
        verifyThat(searchFields.getClearCriteria(), displayed(true));
        verifyThat(searchFields.getCreateCoPartner(), displayed(true));
    }

    @Test(dependsOnMethods = { "verifySearchFieldsPresent" }, alwaysRun = true)
    public void verifyCreateCoPartnerPopUpControls()
    {
        searchFields.clickCreateCoPartner();
        verifyThat(deposPopUp, displayed(true));

        deposPopUp.verifyControls();
    }

    @Test(dependsOnMethods = { "verifyCreateCoPartnerPopUpControls" }, alwaysRun = true)
    public void createNotCoPartnerDepositFromCoPartnerPage()
    {
        deposPopUp.getTransactionId().typeAndWait(depositNotCoPartner.getTransactionId());
        verifyThat(deposPopUp, hasText(containsString(NOTE_TRANSACTION_IS_NOT_CO_PARTNER)));

        deposPopUp.createDepositWithoutGoTo(depositNotCoPartner);

        AllEventsPage allEventsPage = new AllEventsPage();
        verifyThat(allEventsPage, displayed(true));
        verifyThat(allEventsPage.getGrid().getRow(depositNotCoPartner.getTransactionType()), displayed(true));

        coPartnerEventsPage.goTo();
        verifyThat(coPartnerEventsPage.getGrid(), size(0));
    }

    @Test(dependsOnMethods = { "createNotCoPartnerDepositFromCoPartnerPage" }, alwaysRun = true)
    public void tryToCreateCoPartnerWithAmountExceedMaximumAllowed()
    {
        coPartnerEventsPage.goTo();
        coPartnerEventsPage.getSearchFields().clickCreateCoPartner();

        deposPopUp.getTransactionId().typeAndWait(depositCoPartner.getTransactionId());
        verifyThat(deposPopUp, not(hasText(containsString(NOTE_TRANSACTION_IS_NOT_CO_PARTNER))));

        verifyThat(deposPopUp.getTransactionName(), hasText(depositCoPartner.getTransactionName()));
        verifyThat(deposPopUp.getTransactionType(), hasText(depositCoPartner.getTransactionType()));

        deposPopUp.getLoyaltyUnitsInput().typeAndWait(CO_PARTNER_CREATION_IHG_UNITS_LIMIT + 1);
        deposPopUp.getLoyaltyUnitsSelect().select(depositCoPartner.getLoyaltyUnitsType());
        deposPopUp.getCreateDeposit().clickAndWait();

        verifyThat(deposPopUp, displayed(true));
        // Tip: Number exceeds maximum allowed of 50000
        verifyThat(deposPopUp.getLoyaltyUnitsInput(), isHighlightedAsInvalid(true));
    }

    @Test(dependsOnMethods = { "tryToCreateCoPartnerWithAmountExceedMaximumAllowed" }, alwaysRun = true)
    public void createCoPartner()
    {
        deposPopUp.createDepositWithoutGoTo(depositCoPartner);
        verifyThat(coPartnerEventsPage, displayed(true));
    }

    @Test(dependsOnMethods = { "createCoPartner" }, alwaysRun = true)
    public void verifyCoPartnersEventGridRowContainer()
    {
        CoPartnerEventGridRow row = coPartnerEventsPage.getGrid().getRow(1);
        row.verify(depositCoPartner);
        depositGridRowContainer = row.expand(DepositGridRowContainer.class);
        depositGridRowContainer.verify(depositCoPartner, helper);
    }

    @Test(dependsOnMethods = { "verifyCoPartnersEventGridRowContainer" }, alwaysRun = true)
    public void verifyHotelPopUp()
    {
        depositGridRowContainer.getDepositDetails().getHotel().clickAndWait();

        HotelDetailsPopUp hotelDetailsPopUp = new HotelDetailsPopUp();
        verifyThat(hotelDetailsPopUp, displayed(true));
        verifyThat(hotelDetailsPopUp.getHotelDetails().getHotel(), hasText(depositCoPartner.getHotel()));
        hotelDetailsPopUp.close();
        verifyThat(hotelDetailsPopUp, displayed(false));
    }

    @Test(dependsOnMethods = { "verifyHotelPopUp" }, alwaysRun = true)
    public void verifyCoPartnersDepositDetailsPopUp()
    {
        depositGridRowContainer.clickDetails();
        DepositDetailsPopUp depositDetailsPopUp = new DepositDetailsPopUp();

        depositDetailsPopUp.getDepositDetailsTab().getDepositDetails().verify(depositCoPartner);

        EventEarningDetailsTab eventEarningDetailsTab = depositDetailsPopUp.getEarningDetailsTab();
        eventEarningDetailsTab.goTo();

        EventBillingDetailsTab eventBillingDetailsTab = depositDetailsPopUp.getBillingDetailsTab();
        eventBillingDetailsTab.goTo();

        depositDetailsPopUp.close();
    }

    @Test(dependsOnMethods = { "verifyCoPartnersDepositDetailsPopUp" }, alwaysRun = true)
    public void verifyAllEventsCoPartnersEvent()
    {
        AllEventsPage allEventsPage = new AllEventsPage();
        allEventsPage.goTo();
        verifyThat(allEventsPage.getGrid().getRow(depositCoPartner.getTransactionType()), displayed(true));
    }
}
