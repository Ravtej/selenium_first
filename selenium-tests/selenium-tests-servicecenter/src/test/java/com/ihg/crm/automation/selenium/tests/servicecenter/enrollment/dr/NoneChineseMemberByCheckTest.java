package com.ihg.crm.automation.selenium.tests.servicecenter.enrollment.dr;

import static com.ihg.automation.selenium.common.event.EventTransactionType.Order.ORDER_SYSTEM;
import static com.ihg.automation.selenium.common.member.MemberJdbcUtils.UPDATE_EXP;
import static com.ihg.automation.selenium.common.offer.OfferFactory.DR_FREE_NIGHT;
import static com.ihg.automation.selenium.common.types.program.Program.DR;

import java.util.Arrays;

import org.joda.time.LocalDate;
import org.springframework.beans.factory.annotation.Value;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.common.DateUtils;
import com.ihg.automation.selenium.common.Constant;
import com.ihg.automation.selenium.common.catalog.CatalogItem;
import com.ihg.automation.selenium.common.event.AllEventsRow;
import com.ihg.automation.selenium.common.event.AllEventsRowFactory;
import com.ihg.automation.selenium.common.member.DiningRewardAward;
import com.ihg.automation.selenium.common.member.MemberJdbcUtils;
import com.ihg.automation.selenium.common.payment.CheckPayment;
import com.ihg.automation.selenium.common.payment.PaymentDetails;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.common.types.program.ProgramStatus;
import com.ihg.automation.selenium.common.types.program.RewardClubLevel;
import com.ihg.automation.selenium.common.types.program.RewardClubLevelReason;
import com.ihg.automation.selenium.gwt.components.Mode;
import com.ihg.automation.selenium.servicecenter.pages.GuestSearch;
import com.ihg.automation.selenium.servicecenter.pages.Role;
import com.ihg.automation.selenium.servicecenter.pages.programs.dr.DiningRewardsPage;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.RewardClubPage;

public class NoneChineseMemberByCheckTest extends EnrollToDrCommon
{
    @Value("${memberByCheckNoneCN}")
    protected String memberByCheckNoneCN;

    @Value("${memberByCheckNoneCN.date}")
    protected String date;

    private LocalDate enrollDate;
    protected static final CatalogItem ENROLMENT_AWARD = DiningRewardAward.ENROLL_GLOBAL_PRICE_385USD_SC_AND_WEB;
    private AllEventsRow orderSystem = new AllEventsRow(ORDER_SYSTEM, "Welcome to Platinum IDR - Full Kit");
    private final String CHECK_NUMBER = "12345678";
    private CheckPayment checkPayment = new CheckPayment(ENROLMENT_AWARD, CHECK_NUMBER, Constant.NOT_AVAILABLE,
            Constant.NOT_AVAILABLE, Constant.NOT_AVAILABLE, Constant.NOT_AVAILABLE);

    @BeforeClass
    private void beforeClass()
    {
        MemberJdbcUtils.update(jdbcTemplateUpdate, UPDATE_EXP, memberByCheckNoneCN, DR, 13);

        enrollDate = DateUtils.getStringToDate(date);

        login(user15U, Role.AGENT_TIER2);

        new GuestSearch().byMemberNumber(memberByCheckNoneCN);
    }

    @Test
    public void verifyAccountInformation()
    {
        validateAccountInformation();
    }

    @Test(dependsOnMethods = { "verifyAccountInformation" }, alwaysRun = true)
    public void verifyRewardsClubPage()
    {
        RewardClubPage rewardClubPage = new RewardClubPage();
        rewardClubPage.goTo();
        rewardClubPage.getSummary().verify(ProgramStatus.OPEN, RewardClubLevel.PLTN,
                RewardClubLevelReason.IHG_DINING_REWARDS);
    }

    @Test(dependsOnMethods = { "verifyRewardsClubPage" }, alwaysRun = true)
    public void verifyDiningRewardClubPage()
    {
        validateDiningRewardClubPage(memberByCheckNoneCN, EXP_DATE, enrollDate);
        PaymentDetails details = new DiningRewardsPage().getEnrollmentDetails().getPaymentDetails();
        details.verify(checkPayment, Mode.VIEW);
    }

    @Test(dependsOnMethods = { "verifyDiningRewardClubPage" }, alwaysRun = true)
    public void verifyEvents()
    {
        validateEvents(Arrays.asList(AllEventsRowFactory.getEnrollEvent(enrollDate, Program.DR),
                AllEventsRowFactory.getOfferRegistrationEvent(enrollDate, DR_FREE_NIGHT), orderSystem), 7);
    }

}
