package com.ihg.crm.automation.selenium.tests.servicecenter.stay;

import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyNoError;
import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifySuccess;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.event.EventTransactionType.Stay.STAY_CREATED;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.isDisplayedWithWait;
import static com.ihg.automation.selenium.matchers.component.GridSize.size;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static org.hamcrest.core.StringContains.containsString;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.member.AnnualActivity;
import com.ihg.automation.selenium.common.member.LifetimeActivity;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.pages.QualifyingWarningDialog;
import com.ihg.automation.selenium.common.stay.Revenues;
import com.ihg.automation.selenium.common.stay.Stay;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.common.types.program.RewardClubLevel;
import com.ihg.automation.selenium.servicecenter.pages.LeftPanel;
import com.ihg.automation.selenium.servicecenter.pages.annual.AnnualActivityPage;
import com.ihg.automation.selenium.servicecenter.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.servicecenter.pages.event.stay.AdjustStayPopUp;
import com.ihg.automation.selenium.servicecenter.pages.event.stay.CreateStayPopUp;
import com.ihg.automation.selenium.servicecenter.pages.event.stay.DisqualifyReasonPopUp;
import com.ihg.automation.selenium.servicecenter.pages.event.stay.StayEventsPage;
import com.ihg.automation.selenium.servicecenter.pages.event.stay.StayGuestGrid;
import com.ihg.automation.selenium.servicecenter.pages.event.stay.StayGuestGridRow;
import com.ihg.automation.selenium.servicecenter.pages.event.stay.StayGuestGridRow.StayCell;
import com.ihg.crm.automation.selenium.tests.servicecenter.LoginLogout;

public class CreateMissingStayRewardNightTest extends LoginLogout
{
    private static final String NON_QUALIFYING_REASON = "Non-Qualifying Rate Category";
    private static final int STAY_POINTS = 500;
    private StayEventsPage stayEventsPage = new StayEventsPage();
    private AdjustStayPopUp adjustPopUp = new AdjustStayPopUp();

    private Member member = new Member();
    private Stay stay = new Stay();

    private static final String NIGHTS = "15";
    private static final int NIGHTS_INT = Integer.valueOf(NIGHTS);

    private void initMember()
    {
        member.addProgram(Program.RC);
        member.setPersonalInfo(MemberPopulateHelper.getSimplePersonalInfo());
        member.addAddress(MemberPopulateHelper.getUnitedStatesAddress());
    }

    private void initStay()
    {
        stay.setHotelCode("ATLCP");
        stay.setCheckInOutByNights(NIGHTS_INT);
        stay.setRateCode("IVANI");
        stay.setAvgRoomRate(100.0);
        stay.setQualifyingNights(0);
        stay.setIhgUnits("500");
        stay.setTierLevelNights(NIGHTS);
        Revenues revenues = stay.getRevenues();
        revenues.getFood().setAmount(50.0);
    }

    @BeforeClass
    public void before()
    {
        initMember();
        initStay();

        login();

        new EnrollmentPage().enroll(member);
        new AnnualActivityPage().captureCounters(member, jdbcTemplate);
    }

    @Test
    public void createRewardNightStay()
    {
        stayEventsPage.goTo();
        stayEventsPage.getButtonBar().clickCreateStay(verifyNoError());
        CreateStayPopUp popUp = new CreateStayPopUp();
        popUp.getStayDetails().populate(stay);
        popUp.clickCreateStay(verifyNoError());
        verifyThat(popUp, isDisplayedWithWait());

        QualifyingWarningDialog warningDialog = new QualifyingWarningDialog();
        verifyThat(warningDialog,
                hasText(containsString("Are you sure you want to post the stay as 'non-qualifying'?")));
        verifyThat(warningDialog, hasText(containsString(NON_QUALIFYING_REASON)));
        warningDialog.clickSubmitNoError(verifySuccess(hasText(containsString(String.format(
                "Stay Qualifying indicator was set to NO by System Rules. Reason: %s.", NON_QUALIFYING_REASON)))));

        stayEventsPage.waitUntilFirstStayProcess();
    }

    @Test(dependsOnMethods = { "createRewardNightStay" }, alwaysRun = true)
    public void verifyRowDetails()
    {
        StayGuestGrid guestGrid = stayEventsPage.getGuestGrid();
        verifyThat(guestGrid, size(1));

        StayGuestGridRow row = guestGrid.getRow(1);
        row.verify(stay);
        row.getCell(StayCell.QUALIFYING_NIGHTS).asLink().clickAndWait();

        new DisqualifyReasonPopUp().verifyReasonAndClose(NON_QUALIFYING_REASON);
    }

    @Test(dependsOnMethods = { "verifyRowDetails" }, alwaysRun = true)
    public void verifyStayDetailsInPopUp()
    {
        stayEventsPage.getGuestGrid().getRow(1).getDetails().clickDetails();

        adjustPopUp.getStayDetailsTab().getStayDetails().getStayInfo().verify(stay);
    }

    @Test(dependsOnMethods = { "verifyStayDetailsInPopUp" }, alwaysRun = true)
    public void verifyEarningDetails()
    {
        adjustPopUp.getEarningDetailsTab().verifyForBaseUnitsEvent(STAY_POINTS, STAY_POINTS);
    }

    @Test(dependsOnMethods = { "verifyEarningDetails" }, alwaysRun = true)
    public void verifyBillingDetails()
    {
        adjustPopUp.getBillingDetailsTab().verifyBaseUSDBilling(STAY_CREATED, "2.37", stay.getHotelCode());
    }

    @Test(dependsOnMethods = { "verifyBillingDetails" }, alwaysRun = true)
    public void closeAdjustPopUp()
    {
        adjustPopUp.clickClose(verifyNoError());
    }

    @Test(dependsOnMethods = { "closeAdjustPopUp" }, alwaysRun = true)
    public void verifyAnnualActivityPage()
    {
        LifetimeActivity lifetimeActivity = member.getLifetimeActivity();
        lifetimeActivity.setBaseUnits(STAY_POINTS);
        lifetimeActivity.setTotalUnits(STAY_POINTS);
        lifetimeActivity.setCurrentBalance(STAY_POINTS);

        AnnualActivity annualActivity = member.getAnnualActivity();
        annualActivity.setTierLevelNights(NIGHTS_INT);
        annualActivity.setRewardNights(NIGHTS_INT);
        annualActivity.setNonQualifiedNights(NIGHTS_INT);
        annualActivity.setQualifiedChains(0);
        annualActivity.setTotalQualifiedUnits(STAY_POINTS);
        annualActivity.setTotalUnits(STAY_POINTS);

        new AnnualActivityPage().verifyCounters(member);

        new LeftPanel().verifyRCLevel(RewardClubLevel.GOLD);
    }
}
