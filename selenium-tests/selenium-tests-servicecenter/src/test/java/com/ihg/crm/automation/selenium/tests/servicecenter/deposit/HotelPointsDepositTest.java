package com.ihg.crm.automation.selenium.tests.servicecenter.deposit;

import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.assertNoError;
import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyError;
import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifySuccess;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.event.EventTransactionType.Deposit.HOTEL_POINT_AWARD;
import static com.ihg.automation.selenium.common.types.program.RewardClubLevel.GOLD;
import static com.ihg.automation.selenium.common.types.program.RewardClubLevelReason.BASE;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.isHighlightedAsInvalid;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.Constant;
import com.ihg.automation.selenium.common.deposit.Deposit;
import com.ihg.automation.selenium.common.event.AllEventsRowConverter;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.servicecenter.pages.LeftPanel;
import com.ihg.automation.selenium.servicecenter.pages.deposit.CreateDepositPopUp;
import com.ihg.automation.selenium.servicecenter.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsGridRow;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsPage;
import com.ihg.automation.selenium.servicecenter.pages.event.deposit.DepositGridRowContainer;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.RewardClubPage;
import com.ihg.crm.automation.selenium.tests.servicecenter.LoginLogout;

public class HotelPointsDepositTest extends LoginLogout
{
    private Deposit depositWithReasonAndComment = new Deposit();

    @BeforeClass
    public void beforeClass()
    {
        Member member = new Member();
        member.setPersonalInfo(MemberPopulateHelper.getSimplePersonalInfo());
        member.addAddress(MemberPopulateHelper.getUnitedStatesAddress());
        member.addProgram(Program.RC);

        depositWithReasonAndComment.setTransactionId("SVC500");
        depositWithReasonAndComment.setTransactionType(HOTEL_POINT_AWARD);
        depositWithReasonAndComment.setTransactionName("Hotel Awarded Points");
        depositWithReasonAndComment.setTransactionDescription("Service Recovery 500 Points");
        depositWithReasonAndComment.setReason("Room Location");
        depositWithReasonAndComment.setHotel("ATLCP");
        depositWithReasonAndComment.setLoyaltyUnitsAmount("500");
        depositWithReasonAndComment.setLoyaltyUnitsType(Constant.RC_POINTS);
        depositWithReasonAndComment.setCustomerServiceComment("test");

        login();

        new EnrollmentPage().enroll(member);

        RewardClubPage rewardClubPage = new RewardClubPage();
        rewardClubPage.goTo();
        rewardClubPage.getSummary().setTierLevelWithExpiration(GOLD, BASE);
        new LeftPanel().verifyRCLevel(GOLD);
    }

    @Test
    public void tryToCreateDepositWithoutMandatoryReason()
    {
        CreateDepositPopUp createDepositPopUp = new CreateDepositPopUp();
        createDepositPopUp.goTo();

        verifyThat(createDepositPopUp.getReason(), displayed(false));
        verifyThat(createDepositPopUp.getCustomerServiceComment(), displayed(false));
        createDepositPopUp.getTransactionId().typeAndWait(depositWithReasonAndComment.getTransactionId());
        createDepositPopUp.getHotel().typeAndWait(depositWithReasonAndComment.getHotel());
        createDepositPopUp.getLoyaltyUnitsSelect().select(depositWithReasonAndComment.getLoyaltyUnitsType());
        createDepositPopUp.getLoyaltyUnitsInput().typeAndWait(depositWithReasonAndComment.getLoyaltyUnitsAmount());

        verifyThat(createDepositPopUp.getReason(), displayed(true));
        verifyThat(createDepositPopUp.getCustomerServiceComment(), displayed(true));

        createDepositPopUp
                .clickCreateDeposit(verifyError("Deposit can't be saved, because not all fields passed validation"));

        verifyThat(createDepositPopUp.getReason(), isHighlightedAsInvalid(true));
    }

    @Test(dependsOnMethods = { "tryToCreateDepositWithoutMandatoryReason" })
    public void createDepositWithReasonAndComment()
    {
        new CreateDepositPopUp().createDeposit(depositWithReasonAndComment);

        verifyAllEventPage(depositWithReasonAndComment);
    }

    @Test(dependsOnMethods = { "createDepositWithReasonAndComment" })
    public void createDepositWithoutOptionalReasonAndComment()
    {
        Deposit depositWithoutReasonAndComment = new Deposit();
        depositWithoutReasonAndComment.setTransactionId("WLP500");
        depositWithoutReasonAndComment.setTransactionType(HOTEL_POINT_AWARD);
        depositWithoutReasonAndComment.setTransactionName("Welcome Bonus Points");
        depositWithoutReasonAndComment.setTransactionDescription("Welcome Platinum 500 Points");
        depositWithoutReasonAndComment.setHotel("ATLCP");
        depositWithoutReasonAndComment.setLoyaltyUnitsAmount("500");
        depositWithoutReasonAndComment.setLoyaltyUnitsType(Constant.RC_POINTS);

        CreateDepositPopUp createDepositPopUp = new CreateDepositPopUp();
        createDepositPopUp.goTo();

        createDepositPopUp.getTransactionId().typeAndWait(depositWithoutReasonAndComment.getTransactionId());

        verifyThat(createDepositPopUp.getReason(), displayed(false));
        verifyThat(createDepositPopUp.getCustomerServiceComment(), displayed(false));

        createDepositPopUp.getHotel().typeAndWait(depositWithoutReasonAndComment.getHotel());

        createDepositPopUp.getLoyaltyUnitsSelect().select(depositWithoutReasonAndComment.getLoyaltyUnitsType());
        createDepositPopUp.getLoyaltyUnitsInput().typeAndWait(depositWithoutReasonAndComment.getLoyaltyUnitsAmount());

        createDepositPopUp.getCreateDeposit().clickAndWait(assertNoError(),
                verifySuccess(CreateDepositPopUp.SUCCESS_MESSAGE));

        verifyAllEventPage(depositWithoutReasonAndComment);

    }

    private void verifyAllEventPage(Deposit testDeposit)
    {
        AllEventsPage allEventsPage = new AllEventsPage();
        verifyThat(allEventsPage, displayed(true));
        AllEventsGridRow row = allEventsPage.getGrid().getRow(testDeposit.getTransactionType());
        row.verify(AllEventsRowConverter.convert(testDeposit));
        row.expand(DepositGridRowContainer.class).verify(testDeposit, helper);
    }
}
