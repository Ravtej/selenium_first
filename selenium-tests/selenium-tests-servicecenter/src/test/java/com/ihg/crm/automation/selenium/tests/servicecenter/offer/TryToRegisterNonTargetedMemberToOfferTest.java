package com.ihg.crm.automation.selenium.tests.servicecenter.offer;

import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyError;
import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyStaticError;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.GridSize.size;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static com.ihg.automation.selenium.matchers.component.IsDisplayed.displayed;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.Constant;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.servicecenter.pages.LeftPanel;
import com.ihg.automation.selenium.servicecenter.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.servicecenter.pages.event.offer.OfferEventPage;
import com.ihg.automation.selenium.servicecenter.pages.event.offer.OfferPopUp;
import com.ihg.automation.selenium.servicecenter.pages.event.offer.UniverseOffersGrid;
import com.ihg.automation.selenium.servicecenter.pages.offer.OfferRegistrationPopUp;

@Test(groups = { "targetOfferTest" })
public class TryToRegisterNonTargetedMemberToOfferTest extends TargetingOfferCommon
{
    private Member member = new Member();
    private OfferEventPage offerPage = new OfferEventPage();

    @BeforeClass
    public void beforeClass()
    {
        member.setPersonalInfo(MemberPopulateHelper.getSimplePersonalInfo());
        member.addAddress(MemberPopulateHelper.getIndonesiaAddress());
        member.addProgram(Program.RC);

        login();

        new EnrollmentPage().enroll(member);
    }

    @Test
    public void searchWithEligible()
    {
        offerPage.goTo();
        offerPage.searchByOfferId(TARGET_OFFER_ID, true);
        verifyThat(offerPage.getUniverseOffersGrid(), size(0));
    }

    @Test
    public void searchWithoutEligible()
    {
        offerPage.goTo();
        offerPage.searchByOfferId(TARGET_OFFER_ID, false);

        new OfferPopUp().waitAndClose();

        UniverseOffersGrid universeOffersGrid = offerPage.getUniverseOffersGrid();

        verifyThat(universeOffersGrid, size(1));

        universeOffersGrid.getRow(1).verify(TARGET_OFFER_ID, "Not Targeted");
    }

    @Test(dependsOnMethods = { "searchWithoutEligible" })
    public void verifyOffersContainer()
    {
        verifyThat(offerPage.getUniverseOffersGrid().getRow(1).getRowContainer().getOfferDetails().getOfferEligibility()
                .getTargetExpiry(), hasText(Constant.NOT_AVAILABLE));
    }

    @Test
    public void tryRegisterToTargetingOfferWithMemberInContext()
    {
        OfferRegistrationPopUp offerRegistrationPopUp = new OfferRegistrationPopUp();
        offerRegistrationPopUp.registerToTargetingOffer(member, TARGET_OFFER_ID, Constant.NOT_AVAILABLE,
                verifyError(hasText("The Guest is not Targeted for the offer.")));
        offerRegistrationPopUp.close();

        offerPage.goTo();
        verifyThat(offerPage.getMemberOffersGrid().getRowByOfferCode(TARGET_OFFER_ID), displayed(false));
    }

    @Test
    public void tryQuickRegister()
    {
        offerPage.goTo();
        offerPage.getSearchFields().getOfferCode().typeAndWait(TARGET_OFFER_ID);
        offerPage.getButtonBar().clickQuickRegister(verifyStaticError("The Guest is not Targeted for the offer."));

        OfferPopUp offerPopUp = new OfferPopUp();
        verifyThat(offerPopUp, displayed(true));

        offerPopUp.close();
        verifyThat(offerPage.getMemberOffersGrid().getRowByOfferCode(TARGET_OFFER_ID), displayed(false));
    }

    @Test(dependsOnMethods = { "searchWithEligible", "verifyOffersContainer",
            "tryRegisterToTargetingOfferWithMemberInContext", "tryQuickRegister" }, alwaysRun = true)
    public void tryRegisterToTargetingOfferWithoutMemberInContext()
    {
        new LeftPanel().goToNewSearch();
        OfferRegistrationPopUp offerRegistrationPopUp = new OfferRegistrationPopUp();
        offerRegistrationPopUp.registerToTargetingOffer(null, TARGET_OFFER_ID, Constant.NOT_AVAILABLE,
                verifyError(hasText("Guest must be in Context to continue.")));
        offerRegistrationPopUp.close();
    }
}
