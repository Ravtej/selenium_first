package com.ihg.crm.automation.selenium.tests.servicecenter.commpref;

import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyNoError;
import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifySuccess;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.communication.CommunicationPreferencesConstants.DO_NOT_SELL;
import static com.ihg.automation.selenium.common.communication.CommunicationPreferencesConstants.OK_TO_SELL;
import static com.ihg.automation.selenium.common.communication.CommunicationPreferencesConstants.SUCCESS_MESSAGE_SC;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static com.ihg.automation.selenium.matchers.component.IsDisplayed.displayed;
import static com.ihg.automation.selenium.matchers.component.IsEnabled.enabled;
import static com.ihg.automation.selenium.servicecenter.pages.history.AuditConstant.NOT_SET;
import static com.ihg.automation.selenium.servicecenter.pages.history.AuditConstant.CommunicationPreferences.COMMUNICATION_PREFERENCES;
import static com.ihg.automation.selenium.servicecenter.pages.history.AuditConstant.CommunicationPreferences.THIRD_PARTY_SHARE;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNot.not;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.communication.MarketingPreferences;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.servicecenter.pages.LeftPanel;
import com.ihg.automation.selenium.servicecenter.pages.communication.CommunicationPreferences;
import com.ihg.automation.selenium.servicecenter.pages.communication.CommunicationPreferencesPage;
import com.ihg.automation.selenium.servicecenter.pages.communication.CustomerService;
import com.ihg.automation.selenium.servicecenter.pages.communication.CustomerServiceEmail;
import com.ihg.automation.selenium.servicecenter.pages.communication.CustomerServicePhone;
import com.ihg.automation.selenium.servicecenter.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.servicecenter.pages.history.ProfileHistoryGridRow;
import com.ihg.automation.selenium.servicecenter.pages.history.ProfileHistoryPage;
import com.ihg.crm.automation.selenium.tests.servicecenter.LoginLogout;

public class RCMemberWithoutEmailAndPhoneTest extends LoginLogout
{
    private MarketingPreferences marketingPrefs;
    private Member member = new Member();
    private EnrollmentPage enrollmentPage = new EnrollmentPage();

    @BeforeClass
    public void beforeClass()
    {
        member.addProgram(Program.RC);
        member.setPersonalInfo(MemberPopulateHelper.getSimplePersonalInfo());
        member.addAddress(MemberPopulateHelper.getUnitedStatesAddress());

        login();
        enrollmentPage.enroll(member);
    }

    @Test
    public void verifyMarketingPreferences()
    {
        CommunicationPreferencesPage commPrefPage = new CommunicationPreferencesPage();
        commPrefPage.goTo();

        CommunicationPreferences commPrefs = commPrefPage.getCommunicationPreferences();

        marketingPrefs = commPrefs.getMarketingPreferences();

        commPrefs.clickEdit();

        verifyThat(marketingPrefs, marketingPrefs.getContactPermissionItems().getContactPermissionItemsCount(),
                not(is(0)), "No items on communication preferences page");
    }

    @Test(dependsOnMethods = { "verifyMarketingPreferences" }, alwaysRun = true)
    public void verifyPhoneAndEmailNotAvailable()
    {
        CommunicationPreferencesPage commPrefPage = new CommunicationPreferencesPage();
        CustomerService customerService = commPrefPage.getCommunicationPreferences().getCustomerService();

        CustomerServicePhone phone = customerService.getPhone();
        phone.getPhoneCheckBox().check();
        verifyThat(phone.getPhone(), enabled(false));

        CustomerServiceEmail email = customerService.getEmail();
        email.getEmailCheckBox().check();
        verifyThat(email.getEmail(), enabled(false));

        CommunicationPreferences commPrefs = commPrefPage.getCommunicationPreferences();
        commPrefs.clickCancel();
    }

    @Test(dependsOnMethods = { "verifyPhoneAndEmailNotAvailable" }, alwaysRun = true)
    public void verifyNoSubscribeLinks()
    {
        CommunicationPreferencesPage commPrefPage = new CommunicationPreferencesPage();
        commPrefPage.goTo();
        CommunicationPreferences commPrefs = commPrefPage.getCommunicationPreferences();
        commPrefs.clickEdit(verifyNoError());

        marketingPrefs = commPrefs.getMarketingPreferences();

        verifyThat(marketingPrefs.getSubscribeAllLink(), displayed(false));
        verifyThat(marketingPrefs.getUnSubscribeAllLink(), displayed(false));
    }

    @Test(dependsOnMethods = { "verifyNoSubscribeLinks" }, alwaysRun = true)
    public void selectThirdPartyShareDoNotSell()
    {
        selectThirdParty(DO_NOT_SELL);
    }

    @Test(dependsOnMethods = { "selectThirdPartyShareDoNotSell" }, alwaysRun = true)
    public void verifyHistoryAfterSelectThirdPartyDoNotSell()
    {
        ProfileHistoryPage historyPage = new ProfileHistoryPage();
        historyPage.goTo();
        verifyNoError();

        ProfileHistoryGridRow travelProfileRow = historyPage.getProfileHistoryGrid().getRow(1)
                .getSubRowByName(COMMUNICATION_PREFERENCES);

        travelProfileRow.getSubRowByName(THIRD_PARTY_SHARE).verify(NOT_SET, DO_NOT_SELL);
    }

    @Test(dependsOnMethods = { "verifyHistoryAfterSelectThirdPartyDoNotSell" }, alwaysRun = true)
    public void selectThirdPartyShareOkToSellAll()
    {
        new LeftPanel().reOpenMemberProfile(member);

        CommunicationPreferencesPage commPrefPage = new CommunicationPreferencesPage();
        commPrefPage.goTo();
        commPrefPage.getCommunicationPreferences().clickEdit(verifyNoError());

        selectThirdParty(OK_TO_SELL);
    }

    @Test(dependsOnMethods = { "selectThirdPartyShareOkToSellAll" }, alwaysRun = true)
    public void verifyHistoryAfterSelectThirdPartyOkToSellAll()
    {
        ProfileHistoryPage historyPage = new ProfileHistoryPage();
        historyPage.goTo();
        verifyNoError();

        ProfileHistoryGridRow travelProfileRow = historyPage.getProfileHistoryGrid().getRow(1)
                .getSubRowByName(COMMUNICATION_PREFERENCES);

        travelProfileRow.getSubRowByName(THIRD_PARTY_SHARE).verify(DO_NOT_SELL, OK_TO_SELL);
    }

    private void selectThirdParty(String thirdParty)
    {
        CommunicationPreferencesPage commPrefPage = new CommunicationPreferencesPage();
        CommunicationPreferences commPrefs = commPrefPage.getCommunicationPreferences();
        commPrefs.getThirdPartyShare().select(thirdParty);
        commPrefs.clickSave(verifySuccess(SUCCESS_MESSAGE_SC));

        verifyThat(commPrefs.getThirdPartyShare().getView(), hasText(thirdParty));
    }
}
