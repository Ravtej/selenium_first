package com.ihg.crm.automation.selenium.tests.servicecenter.order;

import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyError;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.CustomerInfoPanel;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.servicecenter.pages.GuestSearch;
import com.ihg.automation.selenium.servicecenter.pages.TopMenu;
import com.ihg.automation.selenium.servicecenter.pages.voucher.VoucherDepositTab;
import com.ihg.automation.selenium.servicecenter.pages.voucher.VoucherPage;
import com.ihg.automation.selenium.servicecenter.pages.voucher.VoucherPopUp;

public class VoucherOrderNegativeTest extends VoucherOrderCommon
{
    private TopMenu menu = new TopMenu();
    private VoucherDepositTab voucherDepositTab = new VoucherPopUp().getVoucherDepositTab();

    private static final String VOUCHER_NOT_SEND = "SELECT * FROM"//
            + " (SELECT TO_CHAR(BON_VOUCHER_ID,'FM000000000')||TO_CHAR(FACILITY_ID,'FM00000')||TO_CHAR(BON_VOUCHER_PIN,'FM0000') VOUCHER_NUMBER"//
            + " FROM FMDS.TMBNVCH"//
            + " WHERE VOUCHER_SENT_IND = 'N' AND VOUCHER_STAT_CD != 'R'"//
            + " ORDER BY LAST_UPDATE_TMSTMP"//
            + " ) WHERE ROWNUM < 2";

    private static final String VOUCHER_DEPOSITED = "SELECT * FROM"//
            + " (SELECT TO_CHAR(BON_VOUCHER_ID,'FM000000000')||TO_CHAR(FACILITY_ID,'FM00000')||TO_CHAR(BON_VOUCHER_PIN,'FM0000') VOUCHER_NUMBER"//
            + " FROM FMDS.TMBNVCH"//
            + " WHERE (TRIM(VOUCHER_STAT_CD) IS NOT NULL) "//
            + "  AND (MEMBERSHIP_ID IS NOT NULL) "//
            + "  AND (VOUCHER_SENT_IND = 'Y') "//
            + " ORDER BY LAST_UPDATE_TMSTMP"//
            + " ) WHERE ROWNUM < 2";

    private static final String VOUCHER_VOID = "SELECT * FROM"//
            + " (SELECT TO_CHAR(BON_VOUCHER_ID,'FM000000000')||TO_CHAR(FACILITY_ID,'FM00000')||TO_CHAR(BON_VOUCHER_PIN,'FM0000') VOUCHER_NUMBER"//
            + " FROM FMDS.TMBNVCH"//
            + " WHERE VOUCHER_SENT_IND = 'N' AND VOUCHER_STAT_CD != 'R'"//
            + " ORDER BY LAST_UPDATE_TMSTMP"//
            + " ) WHERE ROWNUM < 2";

    @BeforeClass
    public void beforeClass()
    {
        login();
        new GuestSearch().byMemberNumber(memberID);
        verifyThat(new CustomerInfoPanel().getMemberNumber(Program.RC), hasText(memberID));
    }

    @Test
    public void tryToOrderVoucherByRcMember()
    {
        menu.getVoucher().clickAndWait();
        menu.getVoucher().getOrder()
                .clickAndWait(verifyError("Customer not in Program which allows this Transaction."));
        verifyThat(new VoucherPage(), displayed(false));
    }

    @DataProvider(name = "invalidVouchers")
    public Object[][] invalidVouchers()
    {
        final String DEPOSIT_NOT_ALLOWED_MSG = "Voucher is not yet processed to vendor, deposit not allowed";

        return new Object[][] { { "123456789012345678", "Voucher Number is invalid" },
                { getVoucherNumber(VOUCHER_NOT_SEND), DEPOSIT_NOT_ALLOWED_MSG },
                { getVoucherNumber(VOUCHER_VOID), DEPOSIT_NOT_ALLOWED_MSG },
                { getVoucherNumber(VOUCHER_DEPOSITED), "The voucher is already deposited" } };
    }

    @Test(dataProvider = "invalidVouchers", dependsOnMethods = { "tryToOrderVoucherByRcMember" }, alwaysRun = true)
    public void tryToDepositVoucher(String voucherNumber, String errorMessage)
    {
        voucherDepositTab.goTo();

        voucherDepositTab.getVoucherNumber().type(voucherNumber);
        voucherDepositTab.clickDeposit(verifyError(errorMessage));

        verifyThat(voucherDepositTab, displayed(true));
    }
}
