package com.ihg.crm.automation.selenium.tests.servicecenter.preferences;

import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyNoError;
import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifySuccess;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.preferences.TravelProfileType.LEISURE;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.isDisplayedWithWait;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.isHighlightedAsInvalid;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static com.ihg.automation.selenium.matchers.text.EntityMatcherFactory.isValue;
import static com.ihg.automation.selenium.servicecenter.pages.history.AuditConstant.TravelProfile.DEFAULT;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.preferences.TravelProfileType;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.gwt.components.dialog.ConfirmDialog;
import com.ihg.automation.selenium.gwt.components.input.Input;
import com.ihg.automation.selenium.servicecenter.pages.LeftPanel;
import com.ihg.automation.selenium.servicecenter.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.servicecenter.pages.history.ProfileHistoryGridRow;
import com.ihg.automation.selenium.servicecenter.pages.history.ProfileHistoryPage;
import com.ihg.automation.selenium.servicecenter.pages.preferences.EditTravelProfilePopUp;
import com.ihg.automation.selenium.servicecenter.pages.preferences.StayPreferencesPage;
import com.ihg.automation.selenium.servicecenter.pages.preferences.TravelProfile;
import com.ihg.automation.selenium.servicecenter.pages.preferences.TravelProfileGridRow;
import com.ihg.automation.selenium.servicecenter.pages.preferences.TravelProfilePopUpBase;
import com.ihg.crm.automation.selenium.tests.servicecenter.LoginLogout;

public class VerificationStayPreferencesTabTest extends LoginLogout
{
    private static final String INVALID_NAME = "%&^%^&%*(&(";
    private static final String VALID_NAME = "Test10";
    private static final String FOR_REMOVE = "Profile1";
    private TravelProfileGridRow row;
    private StayPreferencesPage stayPreferencesPage = new StayPreferencesPage();
    private EditTravelProfilePopUp editTravelProfilePopUp;
    private Member member = new Member();
    private TravelProfile travelProfile;

    @BeforeClass
    public void beforeClass()
    {

        member.addProgram(Program.RC);
        member.addAddress(MemberPopulateHelper.getUnitedStatesAddress());
        member.setPersonalInfo(MemberPopulateHelper.getSimplePersonalInfo());

        login();
        new EnrollmentPage().enroll(member);
    }

    @Test
    public void verifyHistoryForDefaultTravelProfile()
    {
        ProfileHistoryPage historyPage = new ProfileHistoryPage();
        historyPage.goTo();
        verifyNoError();

        ProfileHistoryGridRow travelProfileRow = historyPage.getProfileHistoryGrid().getRow(1).getSubRowByName(
                com.ihg.automation.selenium.servicecenter.pages.history.AuditConstant.TravelProfile.TRAVEL_PROFILE);

        travelProfileRow.getSubRowByName(DEFAULT).verifyAddAction("Yes");
        travelProfileRow
                .getSubRowByName(
                        com.ihg.automation.selenium.servicecenter.pages.history.AuditConstant.TravelProfile.TYPE)
                .verifyAddAction(hasText(isValue(LEISURE)));
        travelProfileRow
                .getSubRowByName(
                        com.ihg.automation.selenium.servicecenter.pages.history.AuditConstant.TravelProfile.NAME)
                .verifyAddAction(hasText(isValue(LEISURE)));

    }

    @Test(dependsOnMethods = { "verifyHistoryForDefaultTravelProfile" }, alwaysRun = true)
    public void verifyStayPreferencesTabMember()
    {
        TravelProfile travelProfile = TravelProfile.getDefaultTravelProfile();

        stayPreferencesPage.goTo();
        row = stayPreferencesPage.getTravelProfilesGrid().getRowByName(LEISURE.getValue());
        row.verify(travelProfile);
        editTravelProfilePopUp = row.openTravelProfile();
        verifyThat(editTravelProfilePopUp.getDefaultIndicator(), isDisplayedWithWait());
        editTravelProfilePopUp.clickClose(verifyNoError());
    }

    @Test(dependsOnMethods = { "verifyStayPreferencesTabMember" }, alwaysRun = true)
    public void verifyGridAfterUpdateToInvalid()
    {
        new LeftPanel().reOpenMemberProfile(member);
        stayPreferencesPage.goTo();

        row = stayPreferencesPage.getTravelProfilesGrid().getRowByName(LEISURE.getValue());

        row.openTravelProfile();
        Input travelProfileName = editTravelProfilePopUp.getTravelProfileName();
        travelProfileName.type(INVALID_NAME);
        editTravelProfilePopUp.getSave().clickAndWait();
        verifyThat(travelProfileName, isHighlightedAsInvalid(true));
        verifyThat(editTravelProfilePopUp, isDisplayedWithWait());
    }

    @Test(dependsOnMethods = { "verifyGridAfterUpdateToInvalid" }, alwaysRun = true)
    public void verifyGridAfterUpdateToValid()
    {
        travelProfile = new TravelProfile(VALID_NAME, TravelProfileType.BUSINESS_GROUP);

        editTravelProfilePopUp.getTravelProfileName().type(travelProfile.getProfileName());
        editTravelProfilePopUp.getType().select(travelProfile.getProfileType().getCodeWithValue());
        editTravelProfilePopUp.clickSave(verifyNoError(), verifySuccess(TravelProfilePopUpBase.SUCCESS_MESSAGE_UPDATE));

        row = stayPreferencesPage.getTravelProfilesGrid().getRowByName(VALID_NAME);
        row.verify(travelProfile);
    }

    @Test(dependsOnMethods = { "verifyGridAfterUpdateToValid" }, alwaysRun = true)
    public void verifyHistoryAfterUpdatingTravelProfile()
    {
        ProfileHistoryPage historyPage = new ProfileHistoryPage();
        historyPage.goTo();
        verifyNoError();

        ProfileHistoryGridRow travelProfileRow = historyPage.getProfileHistoryGrid().getRow(1).getSubRowByName(
                com.ihg.automation.selenium.servicecenter.pages.history.AuditConstant.TravelProfile.TRAVEL_PROFILE);

        travelProfileRow
                .getSubRowByName(
                        com.ihg.automation.selenium.servicecenter.pages.history.AuditConstant.TravelProfile.TYPE)
                .verify(hasText(isValue(LEISURE)), hasText(isValue(travelProfile.getProfileType())));
        travelProfileRow
                .getSubRowByName(
                        com.ihg.automation.selenium.servicecenter.pages.history.AuditConstant.TravelProfile.NAME)
                .verify(hasText(isValue(LEISURE)), hasText(travelProfile.getProfileName()));
    }

    @Test(dependsOnMethods = { "verifyHistoryAfterUpdatingTravelProfile" }, alwaysRun = true)
    public void addSuppTravelProfileForRemoval()
    {
        StayPreferencesPage stayPreferencesPage = new StayPreferencesPage();
        stayPreferencesPage.goTo();
        stayPreferencesPage.addMaxNumberOfTravelProfiles();
    }

    @Test(dependsOnMethods = { "addSuppTravelProfileForRemoval" }, alwaysRun = true)
    public void removeTravelProfileDeny()
    {
        new LeftPanel().reOpenMemberProfile(member);
        stayPreferencesPage.goTo();

        row = stayPreferencesPage.getTravelProfilesGrid().getRowByName(FOR_REMOVE);

        row.clickDelete(verifyNoError());
        ConfirmDialog dialog = new ConfirmDialog();
        verifyThat(dialog, isDisplayedWithWait());
        verifyThat(dialog, hasText("Are you sure you want to delete the travel profile?"));
        dialog.clickNo(verifyNoError());
        verifyThat(row, displayed(true));
    }

    @Test(dependsOnMethods = { "removeTravelProfileDeny" }, alwaysRun = true)
    public void removeTravelProfileAccept()
    {
        row = stayPreferencesPage.getTravelProfilesGrid().getRowByName(FOR_REMOVE);

        row.clickDelete(verifyNoError());
        ConfirmDialog confirmDialog = new ConfirmDialog();
        verifyThat(confirmDialog, isDisplayedWithWait());
        confirmDialog.clickYes(verifyNoError());
        verifyThat(row, displayed(false));
    }

    @Test(dependsOnMethods = { "removeTravelProfileAccept" }, alwaysRun = true)
    public void verifyHistoryAfterRemovingTravelProfile()
    {
        ProfileHistoryPage historyPage = new ProfileHistoryPage();
        historyPage.goTo();
        verifyNoError();

        ProfileHistoryGridRow travelProfileRow = historyPage.getProfileHistoryGrid().getRow(1).getSubRowByName(
                com.ihg.automation.selenium.servicecenter.pages.history.AuditConstant.TravelProfile.TRAVEL_PROFILE);

        travelProfileRow.getSubRowByName(DEFAULT).verifyDeleteAction("No");
        travelProfileRow
                .getSubRowByName(
                        com.ihg.automation.selenium.servicecenter.pages.history.AuditConstant.TravelProfile.TYPE)
                .verifyDeleteAction(hasText(isValue(LEISURE)));
        travelProfileRow
                .getSubRowByName(
                        com.ihg.automation.selenium.servicecenter.pages.history.AuditConstant.TravelProfile.NAME)
                .verifyDeleteAction(FOR_REMOVE);

    }
}
