package com.ihg.crm.automation.selenium.tests.servicecenter.staticdata;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.automation.selenium.matchers.component.IsRequired.required;
import static org.hamcrest.CoreMatchers.allOf;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.components.Country;
import com.ihg.automation.selenium.common.contact.Address;
import com.ihg.automation.selenium.common.member.GuestAddress;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.name.PersonNameFields;
import com.ihg.automation.selenium.common.types.address.AddressType;
import com.ihg.automation.selenium.common.types.address.Locality1Label;
import com.ihg.automation.selenium.common.types.address.Locality2Label;
import com.ihg.automation.selenium.common.types.address.RegionLabel;
import com.ihg.automation.selenium.common.types.address.ZipLabel;
import com.ihg.automation.selenium.common.types.name.DegreeLabel;
import com.ihg.automation.selenium.common.types.name.GivenLabel;
import com.ihg.automation.selenium.common.types.name.MiddleLabel;
import com.ihg.automation.selenium.common.types.name.SalutationLabel;
import com.ihg.automation.selenium.common.types.name.SurnameLabel;
import com.ihg.automation.selenium.common.types.name.TitleLabel;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.servicecenter.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.servicecenter.pages.personal.AddressContact;
import com.ihg.automation.selenium.servicecenter.pages.personal.CustomerInfoFields;
import com.ihg.automation.selenium.servicecenter.pages.personal.PersonalInfoPage;
import com.ihg.crm.automation.selenium.tests.servicecenter.LoginLogout;

public class FieldsConfigurationOnPersonalInfoPageTest extends LoginLogout
{
    private EnrollmentPage enrollPage = new EnrollmentPage();
    private Address addressField;
    private PersonNameFields nameField;
    private Member member = new Member();
    private PersonalInfoPage personalInfoPage = new PersonalInfoPage();
    private AddressContact address;
    private CustomerInfoFields customerInfoFields;
    private GuestAddress canadaAddress = new GuestAddress();

    @BeforeClass
    public void beforeClass()
    {
        member.addProgram(Program.RC);
        member.setPersonalInfo(MemberPopulateHelper.getSimplePersonalInfo());
        member.addAddress(MemberPopulateHelper.getIndonesiaAddress());

        canadaAddress = MemberPopulateHelper.getCanadaAddress();

        login();
        enrollPage.enroll(member);
    }

    @Test()
    public void verifyNameConfigurationForIndonesia()
    {
        personalInfoPage.goTo();

        customerInfoFields = personalInfoPage.getCustomerInfo();
        customerInfoFields.clickEdit();

        nameField = customerInfoFields.getName();

        verifyThat(nameField.getSalutation(SalutationLabel.PREFIX), allOf(displayed(true), required(false)));
        verifyThat(nameField.getSurname(SurnameLabel.FAMILY), allOf(displayed(true), required(true)));
        verifyThat(nameField.getGivenName(GivenLabel.GIVEN), allOf(displayed(true), required(true)));
        verifyThat(nameField.getMiddleName(MiddleLabel.MIDDLE), allOf(displayed(true), required(false)));
        verifyThat(nameField.getNickName(), allOf(displayed(true), required(false)));

        verifyThat(nameField.getSuffix(), displayed(false));
        verifyThat(nameField.getTitle(TitleLabel.SURNAME_2ND), displayed(false));
        verifyThat(nameField.getTitle(TitleLabel.TITLE), displayed(false));
        verifyThat(nameField.getDegree(DegreeLabel.DEGREE), displayed(false));
        verifyThat(nameField.getDegree(DegreeLabel.HONORIFIC), displayed(false));

        customerInfoFields.clickCancel();
    }

    @Test(dependsOnMethods = { "verifyNameConfigurationForIndonesia" }, alwaysRun = true)
    public void verifyAddressConfigurationForIndonesiaBusiness()
    {
        address = personalInfoPage.getAddressList().getContact();
        address.clickEdit();
        addressField = address.getBaseContact();
        addressField.getType().selectByValue(AddressType.BUSINESS);

        verifyThat(addressField.getAddress1(), allOf(displayed(true), required(true)));
        verifyThat(addressField.getRegion1(RegionLabel.PROVINCE), allOf(displayed(true), required(false)));
        verifyThat(addressField.getLocality1(Locality1Label.CITY_TOWN), allOf(displayed(true), required(true)));
        verifyThat(addressField.getZipCode(ZipLabel.POSTAL), allOf(displayed(true), required(false)));
        verifyThat(addressField.getCompanyName(), allOf(displayed(true), required(false)));

        verifyThat(addressField.getAddress2(), displayed(false));
        verifyThat(addressField.getAddress3(), displayed(false));
        verifyThat(addressField.getAddress4(), displayed(false));
        verifyThat(addressField.getAddress5(), displayed(false));
        verifyThat(addressField.getLocality2(Locality2Label.NEIGHBORHOOD), displayed(false));
    }

    @Test(dependsOnMethods = { "verifyAddressConfigurationForIndonesiaBusiness" }, alwaysRun = true)
    public void verifyAddressConfigurationForIndonesiaResidence()
    {
        addressField.getType().selectByValue(AddressType.RESIDENCE);

        verifyThat(addressField.getAddress1(), allOf(displayed(true), required(true)));
        verifyThat(addressField.getRegion1(RegionLabel.PROVINCE), allOf(displayed(true), required(false)));
        verifyThat(addressField.getLocality1(Locality1Label.CITY_TOWN), allOf(displayed(true), required(true)));
        verifyThat(addressField.getZipCode(ZipLabel.POSTAL), allOf(displayed(true), required(false)));

        verifyThat(addressField.getCompanyName(), displayed(false));

    }

    @Test(dependsOnMethods = { "verifyAddressConfigurationForIndonesiaResidence" }, alwaysRun = true)
    public void verifyAddressConfigurationForCanadaBusiness()
    {
        addressField.getCountry().select(Country.CA);
        addressField.getType().selectByValue(AddressType.BUSINESS);
        addressField = address.getBaseContact();

        verifyThat(addressField.getAddress1(), allOf(displayed(true), required(true)));
        verifyThat(addressField.getAddress2(), allOf(displayed(true), required(false)));
        verifyThat(addressField.getAddress3(), allOf(displayed(true), required(false)));
        verifyThat(addressField.getAddress4(), allOf(displayed(true), required(false)));
        verifyThat(addressField.getRegion1(RegionLabel.PROVINCE), allOf(displayed(true), required(true)));
        verifyThat(addressField.getLocality1(Locality1Label.CITY), allOf(displayed(true), required(true)));
        verifyThat(addressField.getZipCode(ZipLabel.POSTAL), allOf(displayed(true), required(true)));
        verifyThat(addressField.getCompanyName(), allOf(displayed(true), required(false)));

        verifyThat(addressField.getAddress5(), displayed(false));
        verifyThat(addressField.getLocality2(Locality2Label.NEIGHBORHOOD), displayed(false));
    }

    @Test(dependsOnMethods = { "verifyAddressConfigurationForCanadaBusiness" }, alwaysRun = true)
    public void populateCanadaAddress()
    {
        address.populate(canadaAddress);
        address.clickSave();
    }

    @Test(dependsOnMethods = { "populateCanadaAddress" }, alwaysRun = true)
    public void verifyNameConfigurationForCanada()
    {
        customerInfoFields = personalInfoPage.getCustomerInfo();
        customerInfoFields.clickEdit();

        nameField = customerInfoFields.getName();

        verifyThat(nameField.getSalutation(SalutationLabel.SALUTATION), allOf(displayed(true), required(false)));
        verifyThat(nameField.getSurname(SurnameLabel.LAST), allOf(displayed(true), required(true)));
        verifyThat(nameField.getGivenName(GivenLabel.FIRST), allOf(displayed(true), required(true)));
        verifyThat(nameField.getMiddleName(MiddleLabel.MIDDLE), allOf(displayed(true), required(false)));
        verifyThat(nameField.getDegree(DegreeLabel.DEGREE), allOf(displayed(true), required(false)));
        verifyThat(nameField.getTitle(TitleLabel.TITLE), allOf(displayed(true), required(false)));
        verifyThat(nameField.getSuffix(), allOf(displayed(true), required(false)));

        verifyThat(nameField.getNickName(), displayed(false));
    }
}
