package com.ihg.crm.automation.selenium.tests.servicecenter.programinfo.karma;

import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyNoError;
import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifySuccess;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.Constant.NOT_AVAILABLE;
import static com.ihg.automation.selenium.common.member.MemberJdbcUtils.MEMBER_ID;
import static com.ihg.automation.selenium.common.types.program.KarmaLevel.KAR;
import static com.ihg.automation.selenium.common.types.program.KarmaLevel.KIC;
import static com.ihg.automation.selenium.common.types.program.KarmaLevelReason.BASE;
import static com.ihg.automation.selenium.common.types.program.ProgramStatus.OPEN;
import static com.ihg.automation.selenium.common.types.program.TierLevelExpiration.EXPIRE_CURRENT_YEAR;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.isHighlightedAsInvalid;
import static com.ihg.automation.selenium.matchers.component.HasTipText.hasWarningTipText;
import static com.ihg.automation.selenium.matchers.component.ViewableMatcher.hasTextInView;
import static com.ihg.automation.selenium.servicecenter.pages.personal.PersonalInfoPage.SUCCESS_MESSAGE;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberJdbcUtils;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.gwt.components.input.Input;
import com.ihg.automation.selenium.servicecenter.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.servicecenter.pages.programs.KarmaPage;
import com.ihg.automation.selenium.servicecenter.pages.programs.ProgramSummary;
import com.ihg.crm.automation.selenium.tests.servicecenter.LoginLogout;

@Test(groups = { "karma" })
public class KarmaTierLevelWithReferringMemberTest extends LoginLogout
{
    public static final String MEMBERSHIP_IS_NOT_ACTIVE_MESSAGE = "Membership is not active";
    public static final String INVALID_MEMBER_MESSAGE = "Not valid referring member";
    private String openedReferringMemberId;
    private String closedReferringMemberId;
    private String removedReferringMemberId;
    private ProgramSummary summary;

    @BeforeClass
    public void beforeClass()
    {
        Member member = new Member();
        member.setPersonalInfo(MemberPopulateHelper.getSimplePersonalInfo());
        member.addAddress(MemberPopulateHelper.getUnitedStatesAddress());
        member.addProgram(Program.KAR);

        login();

        openedReferringMemberId = MemberJdbcUtils.getMemberId(jdbcTemplate,
                String.format(MEMBER_ID, "O", Program.KAR.getCode()));
        closedReferringMemberId = MemberJdbcUtils.getMemberId(jdbcTemplate,
                String.format(MEMBER_ID, "C", Program.KAR.getCode()));
        removedReferringMemberId = MemberJdbcUtils.getMemberId(jdbcTemplate,
                String.format(MEMBER_ID, "R", Program.KAR.getCode()));

        new EnrollmentPage().enroll(member);

        KarmaPage karmaPage = new KarmaPage();
        karmaPage.goTo();
        summary = karmaPage.getSummary();
        summary.clickEdit();
    }

    @DataProvider(name = "reasonProvider")
    protected Object[][] invalidReferringMember()
    {
        return new Object[][] { { closedReferringMemberId, MEMBERSHIP_IS_NOT_ACTIVE_MESSAGE },
                { removedReferringMemberId, MEMBERSHIP_IS_NOT_ACTIVE_MESSAGE }, { "invalid", INVALID_MEMBER_MESSAGE } };
    }

    @Test(dataProvider = "invalidReferringMember")
    public void tryToSetTierLevelWithInvalidReferringMember(String invalidMemberId, String message)
    {
        summary.populateTierLevel(KIC, BASE, EXPIRE_CURRENT_YEAR);

        Input referringMember = summary.getReferringMember();
        referringMember.typeAndWait(invalidMemberId);
        summary.clickSave();

        verifyThat(referringMember, isHighlightedAsInvalid(true));
        verifyThat(referringMember, hasWarningTipText(message));
    }

    @Test(dependsOnMethods = { "tryToSetTierLevelWithInvalidReferringMember" })
    public void cancelSettingTierLevel()
    {
        summary.populateTierLevel(KIC, BASE, EXPIRE_CURRENT_YEAR);

        Input referringMember = summary.getReferringMember();
        referringMember.typeAndWait(openedReferringMemberId);
        verifyThat(referringMember, isHighlightedAsInvalid(false));
        summary.clickCancel(verifyNoError());

        summary.verify(OPEN, KAR);
    }

    @Test(dependsOnMethods = { "cancelSettingTierLevel" })
    public void setKicTierLevelWithReferringMember()
    {
        summary.clickEdit();
        summary.populateTierLevel(KIC, BASE, EXPIRE_CURRENT_YEAR);

        Input referringMember = summary.getReferringMember();
        referringMember.typeAndWait(openedReferringMemberId);
        summary.clickSave(verifySuccess(SUCCESS_MESSAGE));

        summary.verify(OPEN, KIC, 0, BASE);

        verifyThat(referringMember, hasTextInView(openedReferringMemberId));
    }

    @Test(dependsOnMethods = { "setKicTierLevelWithReferringMember" })
    public void setKarWithReferringMemberWithReason()
    {
        summary.clickEdit();
        summary.populateTierLevel(KAR, BASE);

        Input referringMember = summary.getReferringMember();
        referringMember.typeAndWait(openedReferringMemberId);
        summary.clickSave(verifySuccess(SUCCESS_MESSAGE));

        summary.verify(OPEN, KAR, BASE);
        verifyThat(summary.getTierLevelExpiration(), hasTextInView(NOT_AVAILABLE));
        verifyThat(referringMember, hasTextInView(openedReferringMemberId));
    }
}
