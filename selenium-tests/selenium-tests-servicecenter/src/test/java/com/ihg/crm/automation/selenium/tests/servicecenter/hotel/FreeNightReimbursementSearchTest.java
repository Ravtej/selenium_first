package com.ihg.crm.automation.selenium.tests.servicecenter.hotel;

import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyError;
import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyNoError;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasTextAsDate;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.isDisplayedWithWait;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.isHighlightedAsInvalid;
import static com.ihg.automation.selenium.matchers.component.GridSize.size;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static com.ihg.automation.selenium.matchers.date.IsAfterOrEqualsDate.isAfterOrEquals;
import static com.ihg.automation.selenium.matchers.date.IsBeforeOrEqualsDate.isBeforeOrEquals;
import static com.ihg.automation.selenium.servicecenter.pages.Messages.HIGHLIGHTED_FIELDS_DID_NOT_PASS_VALIDATION;
import static com.ihg.automation.selenium.servicecenter.pages.hotel.freenight.FreeNightReimbursementSearchGridRow.FreeNightReimbursementCell.ADR;
import static com.ihg.automation.selenium.servicecenter.pages.hotel.freenight.FreeNightReimbursementSearchGridRow.FreeNightReimbursementCell.DAYS_LEFT;
import static com.ihg.automation.selenium.servicecenter.pages.hotel.freenight.FreeNightReimbursementSearchGridRow.FreeNightReimbursementCell.FREE_NIGHTS_COUNT;
import static com.ihg.automation.selenium.servicecenter.pages.hotel.freenight.FreeNightReimbursementSearchGridRow.FreeNightReimbursementCell.HOTEL_CURRENCY;
import static com.ihg.automation.selenium.servicecenter.pages.hotel.freenight.FreeNightReimbursementSearchGridRow.FreeNightReimbursementCell.OCC;
import static com.ihg.automation.selenium.servicecenter.pages.hotel.freenight.FreeNightReimbursementSearchGridRow.FreeNightReimbursementCell.OVERRIDE_ADR;
import static com.ihg.automation.selenium.servicecenter.pages.hotel.freenight.FreeNightReimbursementSearchGridRow.FreeNightReimbursementCell.OVERRIDE_OCC;
import static com.ihg.automation.selenium.servicecenter.pages.hotel.freenight.FreeNightReimbursementSearchGridRow.FreeNightReimbursementCell.STATUS;
import static com.ihg.automation.selenium.servicecenter.pages.hotel.freenight.FreeNightReimbursementSearchGridRow.FreeNightReimbursementCell.STAY_DATE;
import static com.ihg.automation.selenium.servicecenter.pages.hotel.freenight.FreeNightReimbursementSearchGridRow.FreeNightReimbursementCell.TOTAL_REIMBURSEMENT;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNot.not;
import static org.joda.time.LocalDate.now;

import java.util.List;

import org.hamcrest.core.AllOf;
import org.joda.time.LocalDate;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.ihg.automation.common.DateUtils;
import com.ihg.automation.selenium.gwt.components.grid.GridHeader;
import com.ihg.automation.selenium.servicecenter.pages.hotel.freenight.FreeNightReimbursementPage;
import com.ihg.automation.selenium.servicecenter.pages.hotel.freenight.FreeNightReimbursementSearch;
import com.ihg.automation.selenium.servicecenter.pages.hotel.freenight.FreeNightReimbursementSearchGridRow;
import com.ihg.automation.selenium.servicecenter.pages.hotel.freenight.FreeNightReimbursementSearchGridRow.FreeNightReimbursementCell;
import com.ihg.crm.automation.selenium.tests.servicecenter.LoginLogout;

public class FreeNightReimbursementSearchTest extends LoginLogout
{
    public static final String HOTEL = "ATLCP";
    private FreeNightReimbursementPage reimbursementPage = new FreeNightReimbursementPage();

    public static final String HOTEL_IS_INVALID = "Hotel mnemonic is invalid";

    private static final LocalDate VALID_FROM_DATE = now().minusYears(2);
    private static final LocalDate VALID_TO_DATE = now();

    @BeforeClass
    public void beforeClass()
    {
        login();

        reimbursementPage.goTo();
        verifyThat(reimbursementPage, isDisplayedWithWait());
    }

    @Test(priority = 1)
    public void validateSearchControls()
    {
        FreeNightReimbursementSearch reimbursementSearch = reimbursementPage.getSearchFields();
        verifyThat(reimbursementSearch.getFrom(), hasText(DateUtils.getDateBackward(31)));
        verifyThat(reimbursementSearch.getTo(), hasText(DateUtils.getDateBackward(1)));
        verifyThat(reimbursementSearch.getHotelCode(), hasText("Hotel"));
        verifyThat(reimbursementSearch.getStatus(), hasText("All Statuses"));
        verifyThat(reimbursementSearch.getType(), hasText("Select"));

        FreeNightReimbursementPage.FreeNightReimbursementSearchButtonBar buttonBar = reimbursementPage.getButtonBar();
        verifyThat(buttonBar.getSearch(), displayed(true));
        verifyThat(buttonBar.getClearCriteria(), displayed(true));
        verifyThat(buttonBar.getApproveSelected(), displayed(true));
    }

    @Test(priority = 1)
    public void verifyGridHeader()
    {
        GridHeader<FreeNightReimbursementCell> header = reimbursementPage.getGrid().getHeader();

        verifyThat(header.getCell(ADR), hasText(ADR.getValue()));
        verifyThat(header.getCell(DAYS_LEFT), hasText(DAYS_LEFT.getValue()));
        verifyThat(header.getCell(FREE_NIGHTS_COUNT), hasText(FREE_NIGHTS_COUNT.getValue()));
        verifyThat(header.getCell(HOTEL_CURRENCY), hasText(HOTEL_CURRENCY.getValue()));
        verifyThat(header.getCell(OCC), hasText(OCC.getValue()));
        verifyThat(header.getCell(OVERRIDE_OCC), hasText(OVERRIDE_OCC.getValue()));
        verifyThat(header.getCell(OVERRIDE_ADR), hasText(OVERRIDE_ADR.getValue()));
        verifyThat(header.getCell(STATUS), hasText(STATUS.getValue()));
        verifyThat(header.getCell(STAY_DATE), hasText(STAY_DATE.getValue()));
        verifyThat(header.getCell(TOTAL_REIMBURSEMENT), hasText(TOTAL_REIMBURSEMENT.getValue()));

    }

    @DataProvider(name = "invalidHotelCodeProvider")
    public Object[][] verifyInvalidSearchProvider()
    {
        return new Object[][] { { "afsas", HOTEL_IS_INVALID }, { null, HIGHLIGHTED_FIELDS_DID_NOT_PASS_VALIDATION } };
    }

    @Test(dataProvider = "verifyInvalidSearchProvider", priority = 2)
    public void searchByInvalidHotelCode(String hotel, String message)
    {
        reimbursementPage.searchByHotel(hotel, verifyError(message));

        verifyThat(reimbursementPage.getSearchFields().getHotelCode(), isHighlightedAsInvalid(true));
        verifyThat(reimbursementPage.getGrid(), size(0));
    }

    @Test(priority = 2)
    public void searchByValidHotelCode()
    {
        reimbursementPage.searchByHotel(HOTEL, verifyNoError());

        verifyThat(reimbursementPage.getGrid(), size(not(is(0))));
    }

    @Test(priority = 5)
    public void searchByDate()
    {
        reimbursementPage.search(HOTEL, "All Statuses", VALID_FROM_DATE.toString(), VALID_TO_DATE.toString(),
                "All Types");

        List<FreeNightReimbursementSearchGridRow> gridRowList = reimbursementPage.getGrid().getRowList();
        for (FreeNightReimbursementSearchGridRow row : gridRowList)
        {
            verifyThat(row.getCell(STAY_DATE), AllOf.allOf(hasTextAsDate("ddMMMYY", isAfterOrEquals(VALID_FROM_DATE)),
                    hasTextAsDate("ddMMMYY", isBeforeOrEquals(VALID_TO_DATE))));
        }
    }

    @Test(priority = 10, dataProvider = "statusProvider")
    public void verifySearchByStatus(String status)
    {
        reimbursementPage.search(HOTEL, status, VALID_FROM_DATE.toString(), VALID_TO_DATE.toString(), "All Types");

        List<FreeNightReimbursementSearchGridRow> gridRowList = reimbursementPage.getGrid().getRowList();
        for (FreeNightReimbursementSearchGridRow row : gridRowList)
        {
            verifyThat(row.getCell(STATUS), hasText(status));
        }
    }

    @AfterMethod
    public void after()
    {
        reimbursementPage.getButtonBar().clickClearCriteria();
    }

    // skip ACCEPTED, since no data in both environments
    @DataProvider(name = "statusProvider")
    public Object[][] statusDataProvider()
    {
        return new Object[][] { { "PENDING" }, { "INITIATED" }, { "PAID" } };
    }
}
