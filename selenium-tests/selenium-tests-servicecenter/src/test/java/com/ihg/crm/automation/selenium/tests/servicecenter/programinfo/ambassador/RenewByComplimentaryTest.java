package com.ihg.crm.automation.selenium.tests.servicecenter.programinfo.ambassador;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.event.EventTransactionType.Membership.MEMBERSHIP_RENEWAL;
import static com.ihg.automation.selenium.common.member.AmbassadorUtils.AR210_AMBASSADOR_RENEWAL_PROMO_$0;
import static com.ihg.automation.selenium.common.payment.PaymentFactory.COMPLIMENTARY;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.isDisplayedWithWait;
import static com.ihg.automation.selenium.matchers.component.HasSelectItems.hasSelectItems;

import java.util.ArrayList;
import java.util.Arrays;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.catalog.CatalogItem;
import com.ihg.automation.selenium.common.member.AmbassadorJdbcUtils;
import com.ihg.automation.selenium.common.member.AmbassadorUtils;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.pages.PaymentMethod;
import com.ihg.automation.selenium.common.payment.PaymentDetailsPopUp;
import com.ihg.automation.selenium.servicecenter.pages.LeftPanel;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsGridRow;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsPage;
import com.ihg.automation.selenium.servicecenter.pages.event.enroll.MembershipDetailsPopUp;
import com.ihg.automation.selenium.servicecenter.pages.event.enroll.MembershipDetailsTab;
import com.ihg.automation.selenium.servicecenter.pages.event.enroll.MembershipGridRowContainer;
import com.ihg.automation.selenium.servicecenter.pages.programs.amb.AmbassadorPage;

public class RenewByComplimentaryTest extends AmbassadorCommon
{
    public static final CatalogItem RENEW_AWARD = AmbassadorUtils.AMBASSADOR_RENEWAL_PROMO_$0;

    private AmbassadorPage ambassadorPage = new AmbassadorPage();
    private ArrayList<String> EXPECTED_TOTAL_AMOUNT_RENEW = new ArrayList<String>(
            Arrays.asList("AMBASSADOR RENEWAL $200", "AMBASSADOR RENEWAL 24000 POINTS", "AMBASSADOR RENEWAL $150",
                    "ROYAL AMBASSADOR RENEWAL $100", "AMBASSADOR RENEWAL PROMO $0", "AMBASSADOR RENEWAL COMP $0",
                    "AMBASSADOR RENEWAL CORP DISC $100", "ROYAL AMB RENEWAL CORP $75", "ROYAL AMB RENEWAL COMP $0",
                    "AMBASSADOR RENEWAL CROSS CHARGE $100", "ROYAL AMB RENEWAL PROMO $0"));
    private MembershipDetailsPopUp membershipDetailsPopUp = new MembershipDetailsPopUp();
    private AllEventsPage allEventsPage = new AllEventsPage();
    private MembershipGridRowContainer container;
    private int daysShift;

    @BeforeClass
    public void beforeClass()
    {
        login();

        Member member = enrollAMBmember();
        daysShift = new AmbassadorJdbcUtils(jdbcTemplateUpdate, lastUpdateUserId).updateForRenew(member);
        new LeftPanel().reOpenMemberProfile(member);

        ambassadorPage.goTo();
    }

    @Test
    public void verifyRenewButtonAndExpirationDate()
    {
        ambassadorPage.getSummary().verifyRenewAndExtendButtons(true, true);
    }

    @Test(dependsOnMethods = "verifyRenewButtonAndExpirationDate", alwaysRun = true)
    public void verifyPaymentDetailsPopUp()
    {
        ambassadorPage.getSummary().clickRenew();

        PaymentDetailsPopUp paymentDetailsPopUp = new PaymentDetailsPopUp();
        verifyThat(paymentDetailsPopUp, isDisplayedWithWait());
        verifyThat(paymentDetailsPopUp.getTotalAmount(), hasSelectItems(EXPECTED_TOTAL_AMOUNT_RENEW));
    }

    @Test(dependsOnMethods = "verifyPaymentDetailsPopUp", alwaysRun = true)
    public void populatePaymentDetailsPopUp()
    {
        PaymentDetailsPopUp paymentDetailsPopUp = new PaymentDetailsPopUp();
        paymentDetailsPopUp.selectAmount(RENEW_AWARD);
        verifyThat(paymentDetailsPopUp.getPaymentTypeButton(PaymentMethod.COMPLIMENTARY), displayed(true));
        paymentDetailsPopUp.clickSubmitNoError();
    }

    @Test(dependsOnMethods = "populatePaymentDetailsPopUp", alwaysRun = true)
    public void verifyExpiredDate()
    {
        ambassadorPage.getSummary().verifyExpDateAfterRenew(daysShift);
    }

    @Test(dependsOnMethods = "verifyExpiredDate", alwaysRun = true)
    public void verifyAllEventsContainer()
    {
        allEventsPage.goTo();
        AllEventsGridRow row = allEventsPage.getGrid().getRow(MEMBERSHIP_RENEWAL);
        container = row.expand(MembershipGridRowContainer.class);
        container.getMembershipDetails().verify(COMPLIMENTARY, helper);
    }

    @Test(dependsOnMethods = "verifyAllEventsContainer", alwaysRun = true)
    public void openMembershipDetailsPopUpVerify()
    {
        container.clickDetails();
        membershipDetailsPopUp = new MembershipDetailsPopUp();
        verifyThat(membershipDetailsPopUp, isDisplayedWithWait());
    }

    @Test(dependsOnMethods = "openMembershipDetailsPopUpVerify", alwaysRun = true)
    public void verifyAllEventsMembershipDetailsTab()
    {
        MembershipDetailsTab membershipDetailsTab = membershipDetailsPopUp.getMembershipDetailsTab();
        membershipDetailsTab.goTo();
        membershipDetailsTab.getMembershipDetails().verify(COMPLIMENTARY, helper);
    }

    @Test(dependsOnMethods = "verifyAllEventsMembershipDetailsTab")
    public void verifyAllEventsEarningDetailsTab()
    {
        verifyEarningDetailsTab(RENEW_AWARD, AR210_AMBASSADOR_RENEWAL_PROMO_$0);
    }

    @Test(dependsOnMethods = { "verifyAllEventsEarningDetailsTab" }, alwaysRun = true)
    public void validateBillingDetailsTab()
    {
        validateBillingDetails(RENEW_AWARD);
    }
}
