package com.ihg.crm.automation.selenium.tests.servicecenter.stay.disqualified;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.event.AllEventsRow;
import com.ihg.automation.selenium.common.event.AllEventsRowConverter;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.stay.Revenue;
import com.ihg.automation.selenium.common.stay.Stay;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.common.types.program.RewardClubLevel;
import com.ihg.automation.selenium.gwt.components.Mode;
import com.ihg.automation.selenium.servicecenter.pages.LeftPanel;
import com.ihg.automation.selenium.servicecenter.pages.annual.AnnualActivityPage;
import com.ihg.automation.selenium.servicecenter.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsGridRow;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsPage;
import com.ihg.automation.selenium.servicecenter.pages.event.stay.StayEventsPage;
import com.ihg.automation.selenium.servicecenter.pages.event.stay.StayGuestGridRow;
import com.ihg.automation.selenium.servicecenter.pages.event.stay.StayGuestGridRowContainer;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.RewardClubPage;
import com.ihg.crm.automation.selenium.tests.servicecenter.LoginLogout;

public class StayByIATATest extends LoginLogout
{
    private static final String DISQUALIFY_REASON = "Non-Qualifying IATA Alliance Number";

    private Member member = new Member();
    private Stay stay = new Stay();
    private StayEventsPage stayEventsPage = new StayEventsPage();
    private StayGuestGridRow stayGuestGridRow;
    private AllEventsGridRow allEventsGridRow;
    private Revenue room = new Revenue();

    @BeforeClass
    public void beforeClass()
    {
        stay.setHotelCode("ATLCP");
        stay.setBrandCode("HICP");
        stay.setCheckInOutByNights(2);
        stay.setQualifyingNights(0);
        stay.setTierLevelNights("0");
        stay.setAvgRoomRate(50.00);
        stay.setRateCode("1");
        stay.setIataCode("10229");
        stay.getRevenues().getFood().setAmount(100.00);
        stay.setIsQualifying(false);

        room.setAllowOverride(true);
        room.setQualifying(false);
        room.setBillHotel(false);

        member.setPersonalInfo(MemberPopulateHelper.getSimplePersonalInfo());
        member.addAddress(MemberPopulateHelper.getUnitedStatesAddress());
        member.addProgram(Program.RC);

        login();

        member = new AnnualActivityPage().captureCounters(new EnrollmentPage().enroll(member), jdbcTemplate);
    }

    @Test
    public void createMissingStay()
    {
        stayEventsPage.createStayAndVerifySuccessMessage(stay, DISQUALIFY_REASON);
    }

    @Test(dependsOnMethods = { "createMissingStay" }, alwaysRun = true)
    public void verifyStayCreationEvent()
    {
        stayGuestGridRow = stayEventsPage.getGuestGrid().getRow(1);
        stayGuestGridRow.verify(stay);
    }

    @Test(dependsOnMethods = { "verifyStayCreationEvent" }, alwaysRun = true)
    public void verifyDisqualifyReasonPopUp()
    {
        stayGuestGridRow.verifyDisqualifyPopUp(DISQUALIFY_REASON);
    }

    @Test(dependsOnMethods = { "verifyDisqualifyReasonPopUp" }, alwaysRun = true)
    public void verifyRevenueDetails()
    {
        stayGuestGridRow.expand(StayGuestGridRowContainer.class).verifyRoomRevenueDetails(room, Mode.EDIT);
    }

    @Test(dependsOnMethods = { "verifyRevenueDetails" }, alwaysRun = true)
    public void verifyAllEventsGridRow()
    {
        AllEventsRow stayEvent = AllEventsRowConverter.convert(stay);

        AllEventsPage allEventsPage = new AllEventsPage();
        allEventsPage.goTo();
        allEventsGridRow = allEventsPage.getGrid().getRow(stayEvent.getTransType());
        allEventsGridRow.verify(stayEvent);
    }

    @Test(dependsOnMethods = { "verifyAllEventsGridRow" }, alwaysRun = true)
    public void verifyAllEventsGridRowDetails()
    {
        allEventsGridRow.expand(StayGuestGridRowContainer.class).verifyRoomRevenueDetails(room, Mode.VIEW);
    }

    @Test(dependsOnMethods = { "verifyAllEventsGridRowDetails" }, alwaysRun = true)
    public void verifyLeftPanelRCPointsBalance()
    {
        new LeftPanel().getProgramInfoPanel().getPrograms().getRewardClubProgram().verify(RewardClubLevel.CLUB, "0");
    }

    @Test(dependsOnMethods = { "verifyLeftPanelRCPointsBalance" }, alwaysRun = true)
    public void verifyActivityCounters()
    {
        member.getLifetimeActivity().setBaseUnitsFromString("0");
        member.getLifetimeActivity().setTotalUnitsFromString("0");
        member.getLifetimeActivity().setCurrentBalanceFromString("0");
        member.getAnnualActivity().setNonQualifiedNightsFromString("2");
        member.getAnnualActivity().setTotalUnitsFromString("0");
        member.getAnnualActivity().setTotalQualifiedUnitsFromString("0");

        new AnnualActivityPage().verifyCounters(member);
    }

    @Test(dependsOnMethods = { "verifyActivityCounters" }, alwaysRun = true)
    public void verifyRewardNightTierLevelCounters()
    {
        RewardClubPage rewardClubPage = new RewardClubPage();
        rewardClubPage.goTo();
        rewardClubPage.getCurrentYearAnnualActivities().getPointsRow().verify("0", achieveGoldPoints);
    }
}
