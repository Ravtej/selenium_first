package com.ihg.crm.automation.selenium.tests.servicecenter.copperfield;

import static com.ihg.automation.common.DateUtils.DATE_FORMAT;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.Constant.NOT_AVAILABLE;
import static com.ihg.automation.selenium.common.types.program.RewardClubLevel.CLUB;
import static com.ihg.automation.selenium.common.types.program.RewardClubLevel.GOLD;
import static com.ihg.automation.selenium.common.types.program.RewardClubLevel.PLTN;
import static com.ihg.automation.selenium.common.types.program.RewardClubLevel.SPRE;
import static com.ihg.automation.selenium.common.types.program.RewardClubLevelReason.BASE;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.enabled;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasText;

import org.hamcrest.core.AllOf;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.ihg.automation.common.DateUtils;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.common.types.program.ProgramLevel;
import com.ihg.automation.selenium.hotel.pages.personal.PersonalInfoPage;
import com.ihg.automation.selenium.servicecenter.pages.LeftPanel;
import com.ihg.automation.selenium.servicecenter.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.RewardClubPage;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.RewardClubSummary;
import com.ihg.automation.selenium.servicecenter.pages.unit.GoodwillPopUp;
import com.ihg.crm.automation.selenium.tests.servicecenter.LoginLogout;

public class BalanceExpirationDateTest extends LoginLogout
{
    private static final int GOODWILL_AMOUNT = 5000;
    private final static String UPDATE_EXPIRATION_DATE = "UPDATE DGST.MBRSHP SET" //
            + " LST_ACTV_DT = TRUNC(SYSDATE-1),"//
            + " LST_UPDT_USR_ID='%2$s',"//
            + " LST_UPDT_TS = SYSDATE"//
            + " WHERE MBRSHP_ID='%1$s' AND LYTY_PGM_CD='PC'";
    private static final String DATE_NEXT_YEAR = DateUtils.now().plusYears(1).toString(DATE_FORMAT);

    private RewardClubPage rewardClubPage = new RewardClubPage();
    private RewardClubSummary summary = rewardClubPage.getSummary();
    private Member member = new Member();

    @BeforeClass
    public void before()
    {
        member.setPersonalInfo(MemberPopulateHelper.getSimplePersonalInfo());
        member.addAddress(MemberPopulateHelper.getUnitedStatesAddress());
        member.addProgram(Program.RC);

        login();

        new EnrollmentPage().enroll(member);

        new GoodwillPopUp().goodwillPoints(GOODWILL_AMOUNT);

        rewardClubPage.goTo();

        verifyDates(DateUtils.getFormattedDate(), DATE_NEXT_YEAR);

        verifyThat(new LeftPanel().getProgramInfoPanel().getPrograms().getRewardClubProgram().getExpirationDate(),
                hasText(DATE_NEXT_YEAR));
    }

    @Test(dataProvider = "tierLevelProvider", priority = 10, groups = { "copperfield" })
    public void upgradeTierLevelAndVerifyExtendFeature(ProgramLevel level)
    {
        summary.setTierLevelWithExpiration(level, BASE);

        verifyDates(DateUtils.getFormattedDate(), NOT_AVAILABLE);

        verifyThat(new LeftPanel().getProgramInfoPanel().getPrograms().getRewardClubProgram().getExpirationDate(),
                displayed(false));
    }

    @Test(dataProvider = "tierLevelProvider", priority = 20, groups = { "copperfield" })
    public void downgradeToClub(ProgramLevel level)
    {
        summary.setTierLevelWithExpiration(level, BASE);

        jdbcTemplateUpdate.update(String.format(UPDATE_EXPIRATION_DATE, member.getRCProgramId(), lastUpdateUserId));

        // switch between Tabs for new LST_ACTV_DT to appear
        new PersonalInfoPage().goTo();
        rewardClubPage.goTo();

        verifyDates(DateUtils.now().minusDays(1).toString(DATE_FORMAT), NOT_AVAILABLE);

        summary.setTierLevelWithoutExpiration(CLUB, BASE);

        verifyDates(DateUtils.getFormattedDate(), DATE_NEXT_YEAR);

        verifyThat(new LeftPanel().getProgramInfoPanel().getPrograms().getRewardClubProgram().getExpirationDate(),
                hasText(DATE_NEXT_YEAR));
    }

    private void verifyDates(String lstActivityDate, String balanceExpDate)
    {
        verifyThat(summary.getLastActivityDate(), hasText(lstActivityDate));
        verifyThat(summary.getBalanceExpirationDate(), hasText(balanceExpDate));
        verifyThat(summary.getExtend(), AllOf.allOf(displayed(true), enabled(false)));
    }

    @DataProvider(name = "tierLevelProvider")
    public Object[][] tierLevelDataProvider()
    {
        return new Object[][] { { GOLD }, { PLTN }, {SPRE} };
    }
}
