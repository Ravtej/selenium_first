package com.ihg.crm.automation.selenium.tests.servicecenter.enrollment;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static com.ihg.automation.selenium.matchers.component.ViewableMatcher.hasTextInView;

import org.joda.time.LocalDate;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.member.PersonalInfo;
import com.ihg.automation.selenium.common.types.Gender;
import com.ihg.automation.selenium.common.types.name.Degree;
import com.ihg.automation.selenium.common.types.name.NameSuffix;
import com.ihg.automation.selenium.common.types.name.Salutation;
import com.ihg.automation.selenium.common.types.name.Title;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.gwt.components.Mode;
import com.ihg.automation.selenium.servicecenter.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.servicecenter.pages.personal.CustomerInfoFields;
import com.ihg.automation.selenium.servicecenter.pages.personal.PersonalInfoPage;
import com.ihg.crm.automation.selenium.tests.servicecenter.LoginLogout;

public class CustomerInformationTest extends LoginLogout
{
    private EnrollmentPage enrollmentPage = new EnrollmentPage();
    private Member member = new Member();

    // private WrittenLanguage writtenLanguage = WrittenLanguage.SPANISH;

    @BeforeClass
    public void before()
    {
        PersonalInfo personalInfoPage = MemberPopulateHelper.getSimplePersonalInfo();
        personalInfoPage.getName().setSalutation(Salutation.DR);
        personalInfoPage.getName().setSuffix(NameSuffix.ESQ);
        personalInfoPage.getName().setTitle(Title.AMB);
        personalInfoPage.getName().setDegree(Degree.DDS);
        personalInfoPage.setGenderCode(Gender.MALE);
        personalInfoPage.setBirthDate(new LocalDate());

        member.addProgram(Program.RC);
        member.setPersonalInfo(personalInfoPage);
        member.addAddress(MemberPopulateHelper.getUnitedStatesAddress());

        login();
    }

    @Test
    public void customerInformation()
    {
        enrollmentPage.enroll(member);

        // customerInfo.getWrittenLanguage().selectByValue(writtenLanguage);

        PersonalInfoPage personalInfoPage = new PersonalInfoPage();
        CustomerInfoFields customerInfoViewMode = personalInfoPage.getCustomerInfo();

        verifyThat(customerInfoViewMode.getGender(),
                hasTextInView(member.getPersonalInfo().getGenderCode().getValue()));
        customerInfoViewMode.getBirthDate().verify(member.getPersonalInfo().getBirthDate(), Mode.VIEW);
        verifyThat(customerInfoViewMode.getResidenceCountry(),
                hasText(member.getPersonalInfo().getResidenceCountry().getValue()));
        verifyThat(customerInfoViewMode.getName().getFullName(),
                hasText(member.getPersonalInfo().getName().getFullName()));
    }
}
