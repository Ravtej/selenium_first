package com.ihg.crm.automation.selenium.tests.servicecenter.offer;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.GridSize.size;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static com.ihg.automation.selenium.matchers.component.IsDisplayed.displayed;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.common.DateUtils;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.servicecenter.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.servicecenter.pages.event.offer.OfferEventPage;
import com.ihg.automation.selenium.servicecenter.pages.event.offer.OfferPopUp;
import com.ihg.automation.selenium.servicecenter.pages.event.offer.UniverseOfferGridRow;
import com.ihg.automation.selenium.servicecenter.pages.event.offer.UniverseOffersGrid;
import com.ihg.automation.selenium.servicecenter.pages.offer.ForceRegistrationPopUp;
import com.ihg.automation.selenium.servicecenter.pages.offer.OfferRegistrationPopUp;

@Test(groups = { "targetOfferTest" })
public class ForceRegisterToTargetOfferWithMemberInContextTest extends TargetingOfferCommon
{
    private OfferEventPage offerPage = new OfferEventPage();
    private static String regEndDate;

    @BeforeClass
    public void beforeClass()
    {
        Member member = new Member();
        member.setPersonalInfo(MemberPopulateHelper.getSimplePersonalInfo());
        member.addAddress(MemberPopulateHelper.getIndonesiaAddress());
        member.addProgram(Program.RC);

        login();

        new EnrollmentPage().enroll(member);

        regEndDate = DateUtils.getFormattedDate(-1);

        insert(TARGET_OFFER_ID, member, "(SYSDATE-1)");

    }

    @Test
    public void searchWithEligible()
    {
        offerPage.goTo();
        offerPage.searchByOfferId(TARGET_OFFER_ID, true);

        verifyThat(offerPage.getUniverseOffersGrid(), size(0));
    }

    @Test
    public void searchWithoutEligible()
    {
        offerPage.goTo();
        offerPage.searchByOfferId(TARGET_OFFER_ID, false);

        new OfferPopUp().waitAndClose();

        UniverseOffersGrid universeOffersGrid = offerPage.getUniverseOffersGrid();

        verifyThat(universeOffersGrid, size(1));

        UniverseOfferGridRow row = universeOffersGrid.getRow(1);

        row.verify(TARGET_OFFER_ID, "Force Register");

        verifyThat(row.getRowContainer().getOfferDetails().getOfferEligibility().getTargetExpiry(),
                hasText(regEndDate));
        row.getExpander().collapse();
    }

    @Test(dependsOnMethods = { "searchWithEligible", "searchWithoutEligible" }, alwaysRun = true)
    public void forceRegisterTargetingOffer()
    {
        OfferRegistrationPopUp offerRegistrationPopUp = new OfferRegistrationPopUp();
        offerRegistrationPopUp.registerToTargetingOffer(null, TARGET_OFFER_ID, regEndDate);

        ForceRegistrationPopUp forceRegistrationPopUp = new ForceRegistrationPopUp();
        verifyThat(forceRegistrationPopUp, displayed(true));
        forceRegistrationPopUp.forceRegisterToOffer("Agent Error");
    }

    @Test(dependsOnMethods = { "forceRegisterTargetingOffer" })
    public void verifyOffer()
    {
        verifyTargetExpiry(regEndDate);
        verifyThat(offerPage.getMemberOffersGrid().getRowByOfferCode(TARGET_OFFER_ID), displayed(true));
    }
}
