package com.ihg.crm.automation.selenium.tests.servicecenter.enrollment.br;

import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifySuccess;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.Constant.NOT_AVAILABLE;
import static com.ihg.automation.selenium.common.event.EventTransactionType.Membership.MEMBERSHIP_OPEN;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasDefault;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static com.ihg.automation.selenium.matchers.component.ViewableMatcher.hasTextInView;
import static org.hamcrest.core.StringContains.containsString;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.common.DateUtils;
import com.ihg.automation.selenium.common.BusinessRewardsGridRow;
import com.ihg.automation.selenium.common.ProgramInfoPanel;
import com.ihg.automation.selenium.common.event.AllEventsRow;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.common.types.program.ProgramStatus;
import com.ihg.automation.selenium.common.types.program.ProgramStatusReason;
import com.ihg.automation.selenium.matchers.component.ComponentMatcher;
import com.ihg.automation.selenium.matchers.component.HasBackgroundColor;
import com.ihg.automation.selenium.servicecenter.pages.LeftPanel;
import com.ihg.automation.selenium.servicecenter.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsGrid;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsGridRow;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsPage;
import com.ihg.automation.selenium.servicecenter.pages.event.enroll.MembershipGridRowContainer;
import com.ihg.automation.selenium.servicecenter.pages.programs.ProgramPageBase;
import com.ihg.automation.selenium.servicecenter.pages.programs.br.BusinessRewardsPage;
import com.ihg.automation.selenium.servicecenter.pages.programs.br.BusinessRewardsSummary;
import com.ihg.crm.automation.selenium.tests.servicecenter.LoginLogout;

@Test(groups = { "brTest" })
public class ReactivateDeclinedMemberTest extends LoginLogout
{
    private Member member = new Member();
    private BusinessRewardsPage businessRewardsPage = new BusinessRewardsPage();
    private EnrollmentPage enrollmentPage = new EnrollmentPage();
    private AllEventsPage allEventsPage = new AllEventsPage();
    protected AllEventsRow membershipOpen = new AllEventsRow(MEMBERSHIP_OPEN, Program.BR.getCode());

    @BeforeClass
    public void beforeClass()
    {
        member.addProgram(Program.BR);
        member.addAddress(MemberPopulateHelper.getUnitedStatesAddress());
        member.setPersonalInfo(MemberPopulateHelper.getSimplePersonalInfo());

        login();
        enrollmentPage.enroll(member);

        businessRewardsPage.goTo();

        businessRewardsPage.getSummary().getDeclinedTsAndCs()
                .click(verifySuccess("IHG Business Rewards Membership Ts&Cs have been Declined"));
    }

    @Test
    public void checkBRProgram()
    {
        enrollmentPage.enrollToAnotherProgram(member, Program.BR);
    }

    @Test(dependsOnMethods = { "checkBRProgram" }, alwaysRun = true)
    public void verifyBusinessRewardsPage()
    {
        businessRewardsPage.goTo();

        BusinessRewardsSummary summary = businessRewardsPage.getSummary();
        verifyThat(summary.getStatus(), hasTextInView(ProgramStatus.OPEN));
        verifyThat(summary.getStatusReason(), hasTextInView(ProgramStatusReason.PENDING));
        verifyThat(summary.getMemberId(), hasText(member.getBRProgramId()));
        verifyThat(summary.getDeclinedTsAndCs(), displayed(true));
        verifyThat(summary, hasText(containsString("No flags selected")));

        ProgramPageBase.EnrollmentDetails enrollDetails = businessRewardsPage.getEnrollmentDetails();

        enrollDetails.verifyScSource(helper);
        verifyThat(enrollDetails.getPaymentDetails().getMethod(), hasDefault());
        verifyThat(enrollDetails.getReferringMember(), hasText(NOT_AVAILABLE));
        verifyThat(enrollDetails.getRenewalDate(), hasDefault());
        verifyThat(enrollDetails.getEnrollDate(), hasText(DateUtils.getFormattedDate()));
    }

    @Test(dependsOnMethods = { "verifyBusinessRewardsPage" }, alwaysRun = true)
    public void openAllEventsEnrollBREventDetails()
    {
        allEventsPage.goTo();

        AllEventsGrid allEventsGrid = allEventsPage.getGrid();

        AllEventsGridRow allEventsGridRow = allEventsGrid.getRow(MEMBERSHIP_OPEN, Program.BR.getCode());
        allEventsGridRow.verify(membershipOpen);

        MembershipGridRowContainer membershipGridRowContainer = allEventsGridRow
                .expand(MembershipGridRowContainer.class);

        verifyThat(membershipGridRowContainer.getReason(), hasText(ProgramStatusReason.PENDING));
        membershipGridRowContainer.getMembershipDetails().verifyScSource(helper);
    }

    @Test(dependsOnMethods = { "openAllEventsEnrollBREventDetails" })
    public void verifyProgramInformationPanelLevel()
    {
        BusinessRewardsGridRow rowBR = new ProgramInfoPanel().getPrograms().getBusinessRewardsProgram();
        verifyThat(rowBR, HasBackgroundColor.hasBackgroundColor(LeftPanel.YELLOW_COLOR));

        verifyThat(rowBR.getStatus(), ComponentMatcher.hasText(ProgramStatusReason.PENDING));
    }
}
