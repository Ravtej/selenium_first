package com.ihg.crm.automation.selenium.tests.servicecenter.copperfield;

import static com.ihg.automation.common.DateUtils.DATE_FORMAT;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.pages.PaymentMethod.COMPLIMENTARY;
import static com.ihg.automation.selenium.common.types.ExtendReason.AGENT_ERROR;
import static com.ihg.automation.selenium.common.types.program.Program.AMB;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasDefault;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasText;

import org.joda.time.LocalDate;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.ihg.automation.common.DateUtils;
import com.ihg.automation.selenium.common.deposit.Deposit;
import com.ihg.automation.selenium.common.member.AmbassadorJdbcUtils;
import com.ihg.automation.selenium.common.member.AmbassadorOfferCode;
import com.ihg.automation.selenium.common.member.AmbassadorUtils;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.payment.PaymentDetailsPopUp;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.common.types.program.RewardClubLevel;
import com.ihg.automation.selenium.common.types.program.RewardClubLevelReason;
import com.ihg.automation.selenium.servicecenter.pages.LeftPanel;
import com.ihg.automation.selenium.servicecenter.pages.deposit.CreateDepositPopUp;
import com.ihg.automation.selenium.servicecenter.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.servicecenter.pages.personal.PersonalInfoPage;
import com.ihg.automation.selenium.servicecenter.pages.programs.amb.AmbassadorPage;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.RewardClubPage;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.RewardClubSummary;
import com.ihg.automation.selenium.servicecenter.pages.unit.PurchaseTierLevelPopUp;
import com.ihg.crm.automation.selenium.tests.servicecenter.LoginLogout;

@Test(groups = { "copperfield" })
public class TierLevelMaintainEventsTest extends LoginLogout
{

    private Member member = new Member();
    private LocalDate lastActivityDate;

    private AmbassadorJdbcUtils ambassadorJdbcUtils;

    private final static String UPDATE_EXPIRATION_DATE = "UPDATE DGST.MBRSHP SET" //
            + " LST_ACTV_DT = TRUNC(SYSDATE-1),"//
            + " LST_UPDT_USR_ID='%2$s',"//
            + " LST_UPDT_TS = SYSDATE,"//
            + " TIER_LVL_EXPR_DT = SYSDATE + 1"//
            + " WHERE MBRSHP_ID='%1$s' AND LYTY_PGM_CD='PC'";

    @BeforeClass
    public void beforeClass()
    {
        member.setPersonalInfo(MemberPopulateHelper.getSimplePersonalInfo());
        member.addAddress(MemberPopulateHelper.getUnitedStatesAddress());
        member.addProgram(Program.RC);

        login();

        new EnrollmentPage().enroll(member);
        RewardClubPage rewardClubPage = new RewardClubPage();
        rewardClubPage.goTo();
        rewardClubPage.getSummary().setTierLevelWithExpiration(RewardClubLevel.GOLD, RewardClubLevelReason.BASE);

        ambassadorJdbcUtils = new AmbassadorJdbcUtils(jdbcTemplateUpdate, lastUpdateUserId);
    }

    @BeforeMethod
    public void upgradeDatesInDb()
    {
        jdbcTemplateUpdate.update(String.format(UPDATE_EXPIRATION_DATE, member.getRCProgramId(), lastUpdateUserId));
        lastActivityDate = DateUtils.now().minusDays(1);
    }

    @Test
    public void purchaseGold()
    {
        PurchaseTierLevelPopUp tierLevelPopUp = new PurchaseTierLevelPopUp();
        tierLevelPopUp.goTo();
        tierLevelPopUp.setLevel(Program.RC, RewardClubLevel.GOLD.getValue());

        PaymentDetailsPopUp paymentDetailsPopUp = new PaymentDetailsPopUp();
        paymentDetailsPopUp.selectPaymentType(COMPLIMENTARY);
        paymentDetailsPopUp.clickSubmitNoError();

        verifyDates();
    }

    @Test
    public void goldByDeposit()
    {
        Deposit deposit = new Deposit();
        deposit.setTransactionId("JCBGLD");
        new CreateDepositPopUp().createDeposit(deposit, false);

        verifyDates();
    }

    @Test
    public void enrollToAMB()
    {
        member.setAmbassadorOfferCode(AmbassadorOfferCode.CHSRO);
        new EnrollmentPage().enrollToDROrAMBProgramWithoutProfileChange(member, AMB,
                AmbassadorUtils.AMBASSADOR_COMP_PROMO_$0);

        verifyDates();
    }

    @Test(dependsOnMethods = { "enrollToAMB" }, alwaysRun = true)
    public void goldByExtendAMB()
    {
        AmbassadorPage ambassadorPage = new AmbassadorPage();
        ambassadorPage.goTo();
        ambassadorPage.getSummary().extend(1, AGENT_ERROR);

        verifyDates();
    }

    @Test(dependsOnMethods = { "enrollToAMB" }, alwaysRun = true)
    public void goldByRenewAMB()
    {
        ambassadorJdbcUtils.updateForRenew(member);
        new PersonalInfoPage().goTo();// for re-read ambassador details

        AmbassadorPage ambassadorPage = new AmbassadorPage();
        ambassadorPage.goTo();
        ambassadorPage.getSummary().clickRenew();

        PaymentDetailsPopUp paymentDetailsPopUp = new PaymentDetailsPopUp();
        paymentDetailsPopUp.selectAmount(AmbassadorUtils.AMBASSADOR_RENEWAL_PROMO_$0);
        paymentDetailsPopUp.clickSubmitNoError();

        verifyDates();
    }

    @Test(dependsOnMethods = { "enrollToAMB" }, alwaysRun = true)
    public void goldByReactivateAMB()
    {
        ambassadorJdbcUtils.updateForReactivate(member);
        new PersonalInfoPage().goTo();// for re-read ambassador details
        new AmbassadorPage().goTo();

        new EnrollmentPage().enrollToDROrAMBProgramWithoutProfileChange(member, AMB,
                AmbassadorUtils.AMBASSADOR_COMP_PROMO_$0);

        verifyDates();
    }

    private void verifyDates()
    {
        new LeftPanel().verifyRCLevel(RewardClubLevel.GOLD);

        RewardClubPage rewardClubPage = new RewardClubPage();
        rewardClubPage.goTo();
        RewardClubSummary summary = rewardClubPage.getSummary();
        verifyThat(summary.getBalanceExpirationDate(), hasDefault());
        verifyThat(summary.getLastActivityDate(), hasText(lastActivityDate.toString(DATE_FORMAT)));
        verifyThat(summary.getLastAdjusted(), hasDefault());
    }
}
