package com.ihg.crm.automation.selenium.tests.servicecenter.enrollment;

import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyError;
import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyNoError;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.Constant.NOT_AVAILABLE;
import static com.ihg.automation.selenium.common.event.EventTransactionType.Membership.ENROLL;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasDefault;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasEmptyText;
import static com.ihg.automation.selenium.matchers.component.GridSize.size;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static com.ihg.automation.selenium.matchers.component.ViewableMatcher.hasTextInView;
import static com.ihg.automation.selenium.matchers.text.RegexMatcher.matches;
import static com.ihg.automation.selenium.servicecenter.pages.account.FailedLoginGridRow.FailedLoginCell.DATE_OF_LAST_ATTEMPT;
import static com.ihg.automation.selenium.servicecenter.pages.account.FailedLoginGridRow.FailedLoginCell.NUMBER_OF_FAILED_ATTEMPTS;
import static com.ihg.automation.selenium.servicecenter.pages.history.AuditConstant.AccountInformation.ACCOUNT_INFORMATION;
import static com.ihg.automation.selenium.servicecenter.pages.history.AuditConstant.AccountInformation.PIN;
import static com.ihg.automation.selenium.servicecenter.pages.history.AuditConstant.AccountInformation.SELF_SERVICE_LOCKOUT_INDICATOR;
import static org.hamcrest.core.StringContains.containsString;

import java.util.ArrayList;

import org.hamcrest.Matcher;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.common.DateUtils;
import com.ihg.automation.selenium.common.ProgramsGrid;
import com.ihg.automation.selenium.common.billing.EventBillingDetailsTab;
import com.ihg.automation.selenium.common.earning.EventEarningDetailsTab;
import com.ihg.automation.selenium.common.enroll.EnrollProgramContainer;
import com.ihg.automation.selenium.common.event.AllEventsRowFactory;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.common.types.program.ProgramStatus;
import com.ihg.automation.selenium.gwt.components.Componentable;
import com.ihg.automation.selenium.gwt.components.Mode;
import com.ihg.automation.selenium.servicecenter.pages.LeftPanel;
import com.ihg.automation.selenium.servicecenter.pages.account.AccountInfoPage;
import com.ihg.automation.selenium.servicecenter.pages.account.FailedLoginGrid;
import com.ihg.automation.selenium.servicecenter.pages.account.FailedLoginGridRow;
import com.ihg.automation.selenium.servicecenter.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsGridRow;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsPage;
import com.ihg.automation.selenium.servicecenter.pages.event.enroll.MembershipDetailsPopUp;
import com.ihg.automation.selenium.servicecenter.pages.event.enroll.MembershipGridRowContainer;
import com.ihg.automation.selenium.servicecenter.pages.history.ProfileHistoryGridRow;
import com.ihg.automation.selenium.servicecenter.pages.history.ProfileHistoryPage;
import com.ihg.automation.selenium.servicecenter.pages.personal.PersonalInfoPage;
import com.ihg.automation.selenium.servicecenter.pages.programs.HotelPage;
import com.ihg.automation.selenium.servicecenter.pages.programs.ProgramPageBase.EnrollmentDetails;
import com.ihg.automation.selenium.servicecenter.pages.programs.ProgramSummary;
import com.ihg.crm.automation.selenium.tests.servicecenter.LoginLogout;

public class HtlEnrollmentTest extends LoginLogout
{
    private Member member = new Member();
    private EnrollmentPage enrollmentPage = new EnrollmentPage();
    private HotelPage hotelPage = new HotelPage();
    private AllEventsPage allEvents;
    private AllEventsGridRow row;
    private MembershipGridRowContainer rowCont;
    private MembershipDetailsPopUp popUp;

    @BeforeClass
    public void beforeClass()
    {
        member.setPersonalInfo(MemberPopulateHelper.getSimplePersonalInfo());
        member.addProgram(Program.HTL);
        member.addPhone(MemberPopulateHelper.getRandomPhone());
        member.addEmail(MemberPopulateHelper.getRandomEmail());
        member.addAddress(MemberPopulateHelper.getUnitedStatesAddress());

        login();

        enrollmentPage.enroll(member);
    }

    @Test
    public void verifyHistoryAccountInfoAfterEnrollment()
    {
        ProfileHistoryPage historyPage = new ProfileHistoryPage();
        historyPage.goTo();
        verifyNoError();

        ProfileHistoryGridRow travelProfileRow = historyPage.getProfileHistoryGrid().getRow(1)
                .getSubRowByName(ACCOUNT_INFORMATION);

        travelProfileRow.getSubRowByName(SELF_SERVICE_LOCKOUT_INDICATOR).verifyAddAction("Unlocked");
        travelProfileRow.getSubRowByName(PIN).verifyAddAction("*******");
    }

    @Test(dependsOnMethods = { "verifyHistoryAccountInfoAfterEnrollment" })
    public void validateLeftPanelProgramInfoNavigation()
    {
        ProgramsGrid grid = new LeftPanel().getProgramInfoPanel().getPrograms();
        grid.getProgram(Program.HTL).goToProgramPage();
        verifyThat(hotelPage, displayed(true));
    }

    @Test(dependsOnMethods = { "validateLeftPanelProgramInfoNavigation" })
    public void validatePersonalInformation()
    {
        PersonalInfoPage persInfo = new PersonalInfoPage();
        persInfo.goTo();
        persInfo.getCustomerInfo().verify(member, Mode.VIEW);
        persInfo.getAddressList().getExpandedContact().verify(member.getPreferredAddress(), Mode.VIEW);
    }

    @Test(dependsOnMethods = { "validatePersonalInformation" })
    public void validateAccountInformation()
    {
        AccountInfoPage accountPage = new AccountInfoPage();
        accountPage.goTo();

        verifyThat(accountPage.getSecurity().getLockout(), hasTextInView("Unlocked"));
        accountPage.getFailedSelfServiceLoginAttempts().expand();

        FailedLoginGrid grid = accountPage.getFailedSelfServiceLoginAttempts().getGrid();
        verifyThat(grid, size(1));

        FailedLoginGridRow row = grid.getRow(1);
        verifyThat(row.getCell(DATE_OF_LAST_ATTEMPT), hasEmptyText());
        verifyThat(row.getCell(NUMBER_OF_FAILED_ATTEMPTS), hasText("0"));

        accountPage.verify(ProgramStatus.OPEN);
        verifyThat(accountPage.getFlags(), hasText(containsString("No flags selected")));
    }

    @Test(dependsOnMethods = { "validateAccountInformation" })
    public void validateProgramInfoSummaryDetails()
    {
        hotelPage = new HotelPage();
        hotelPage.goTo();
        ProgramSummary hotelSummary = hotelPage.getSummary();

        verifyThat(hotelSummary.getStatus(), hasTextInView(ProgramStatus.OPEN));

        verifyThat(hotelSummary.getMemberId(), hasText(member.getRCProgramId()));
        verifyThat(hotelSummary, hasText(containsString("No flags selected")));
    }

    @Test(dependsOnMethods = { "validateProgramInfoSummaryDetails" })
    public void validateProgramInfoEnrollmentDetails()
    {
        hotelPage = new HotelPage();
        hotelPage.goTo();
        EnrollmentDetails enrlDetails = hotelPage.getEnrollmentDetails();
        verifyThat(enrlDetails.getOfferCode(), hasText("SCCAL"));
        verifyThat(enrlDetails.getPaymentDetails().getMethod(), hasDefault());
        verifyThat(enrlDetails.getReferringMember(), hasText(NOT_AVAILABLE));
        verifyThat(enrlDetails.getRenewalDate(), hasDefault());
        verifyThat(enrlDetails.getEnrollDate(), hasText(DateUtils.getFormattedDate()));
        enrlDetails.verifyScSource(helper);
    }

    @Test(dependsOnMethods = { "validateProgramInfoEnrollmentDetails" })
    public void verifyEnrollmentAllEventsGrid()
    {
        allEvents = new AllEventsPage();
        allEvents.goTo();
        allEvents.getGrid().getRow(1).verify(AllEventsRowFactory.getEnrollEvent(Program.HTL));
    }

    @Test(dependsOnMethods = { "verifyEnrollmentAllEventsGrid" })
    public void verifyEnrollmentDetails()
    {
        allEvents = new AllEventsPage();
        allEvents.goTo();
        row = allEvents.getGrid().getEnrollRow(Program.HTL);
        rowCont = row.expand(MembershipGridRowContainer.class);

        rowCont.getMembershipDetails().verifyScSource(helper);
    }

    @Test(dependsOnMethods = { "verifyEnrollmentDetails" })
    public void verifyMembershipDetailsTab()
    {
        rowCont.clickDetails();
        popUp = new MembershipDetailsPopUp();
        popUp.getMembershipDetailsTab().getMembershipDetails().verifyScSource(helper);
    }

    @Test(dependsOnMethods = { "verifyMembershipDetailsTab" })
    public void verifyEarningDetailsTab()
    {
        popUp = new MembershipDetailsPopUp();
        EventEarningDetailsTab earningDetailsTab = popUp.getEarningDetailsTab();
        earningDetailsTab.goTo();
        earningDetailsTab.verifyBasicDetailsNoPointsAndMiles();
    }

    @Test(dependsOnMethods = { "verifyEarningDetailsTab" })
    public void verifyBillingDetailsTab()
    {
        popUp = new MembershipDetailsPopUp();
        EventBillingDetailsTab billingDetailsTab = popUp.getBillingDetailsTab();
        billingDetailsTab.goTo();
        billingDetailsTab.verifyTabIsEmpty(ENROLL);
        popUp.clickClose();
    }

    @Test(dependsOnMethods = { "verifyBillingDetailsTab" })
    public void verifyMutuallyExclusivePrograms()
    {
        enrollmentPage.goTo();

        ArrayList<Program> programs = Program.getProgramList();
        programs.remove(Program.HTL);
        EnrollProgramContainer programsCheckboxes = enrollmentPage.getPrograms();
        verifyThat(programsCheckboxes.getProgramCheckbox(Program.DR), displayed(false));
        for (Program pr : programs)
        {
            programsCheckboxes.getProgramCheckbox(pr).click(verifyError(getMutuallyExclusiveMatcher(pr)));
        }
    }

    @Test(dependsOnMethods = { "verifyMutuallyExclusivePrograms" }, groups = { "karma" })
    public void verifyMutuallyExclusiveKarProgram()
    {
        enrollmentPage.getPrograms().getProgramCheckbox(Program.KAR)
                .click(verifyError(getMutuallyExclusiveMatcher(Program.KAR)));
    }

    private Matcher<Componentable> getMutuallyExclusiveMatcher(Program program)
    {
        String programCode = program.getCode();

        return hasText(matches("(Program HTL is mutually exclusive with program " + programCode + ")|(" + programCode
                + " is mutually exclusive with program HTL)"));
    }
}
