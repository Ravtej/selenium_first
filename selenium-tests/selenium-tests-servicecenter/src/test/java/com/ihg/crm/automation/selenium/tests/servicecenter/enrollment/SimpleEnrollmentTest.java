package com.ihg.crm.automation.selenium.tests.servicecenter.enrollment;

import static com.ihg.automation.common.DateUtils.dd_MMM_YYYY;
import static com.ihg.automation.common.DateUtils.getFormattedDate;
import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyNoError;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static com.ihg.automation.selenium.servicecenter.pages.history.AuditConstant.ProgramInformation.PROGRAM_INFORMATION;
import static com.ihg.automation.selenium.servicecenter.pages.history.AuditConstant.ProgramInformation.RC_MEMBER_SINCE_DATE;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.ProgramsGrid;
import com.ihg.automation.selenium.common.member.GuestName;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.gwt.components.label.Label;
import com.ihg.automation.selenium.servicecenter.pages.LeftPanel;
import com.ihg.automation.selenium.servicecenter.pages.annual.AnnualActivityPage;
import com.ihg.automation.selenium.servicecenter.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.servicecenter.pages.history.ProfileHistoryGridRow;
import com.ihg.automation.selenium.servicecenter.pages.history.ProfileHistoryPage;
import com.ihg.automation.selenium.servicecenter.pages.personal.PersonalInfoPage;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.RewardClubPage;
import com.ihg.crm.automation.selenium.tests.servicecenter.LoginLogout;

public class SimpleEnrollmentTest extends LoginLogout
{
    private EnrollmentPage enrollmentPage = new EnrollmentPage();
    private Member member = new Member();
    private GuestName guestName;
    private RewardClubPage rewardClubPage = new RewardClubPage();
    private AnnualActivityPage annualActivityPage = new AnnualActivityPage();

    @BeforeClass
    public void before()
    {
        member.addProgram(Program.RC);
        member.setPersonalInfo(MemberPopulateHelper.getSimplePersonalInfo());
        member.addAddress(MemberPopulateHelper.getUnitedStatesAddress());
        guestName = member.getPersonalInfo().getName();

        login();
    }

    @Test
    public void simpleEnrollment()
    {
        enrollmentPage.enroll(member);

        Label fullName = new PersonalInfoPage().getCustomerInfo().getName().getFullName();
        verifyThat(fullName, hasText(guestName.getGiven() + " " + guestName.getSurname()));
    }

    @Test(dependsOnMethods = { "simpleEnrollment" })
    public void verifyHistoryProgramInfoAfterEnrollment()
    {
        ProfileHistoryPage historyPage = new ProfileHistoryPage();
        historyPage.goTo();
        verifyNoError();

        ProfileHistoryGridRow travelProfileRow = historyPage.getProfileHistoryGrid().getRow(1)
                .getSubRowByName(PROGRAM_INFORMATION);

        travelProfileRow.getSubRowByName(RC_MEMBER_SINCE_DATE).verifyAddAction(getFormattedDate(dd_MMM_YYYY));
    }

    @Test(dependsOnMethods = { "verifyHistoryProgramInfoAfterEnrollment" })
    public void validateLeftPanelProgramInfoNavigation()
    {
        annualActivityPage.goTo();
        ProgramsGrid grid = new LeftPanel().getProgramInfoPanel().getPrograms();
        grid.getRewardClubProgram().goToProgramPage();
        verifyThat(rewardClubPage, displayed(true));
    }

    @Test(dependsOnMethods = { "validateLeftPanelProgramInfoNavigation" })
    public void validateLeftPanelEarningPreferenceNavigation()
    {
        annualActivityPage.goTo();
        ProgramsGrid grid = new LeftPanel().getProgramInfoPanel().getPrograms();
        grid.getRewardClubProgram().getEarningPreference().clickAndWait(verifyNoError());
        verifyThat(rewardClubPage, displayed(true));
    }

    @Test(dependsOnMethods = { "validateLeftPanelEarningPreferenceNavigation" })
    public void validateLeftPanelPointsBalanceNavigation()
    {
        annualActivityPage.goTo();
        ProgramsGrid grid = new LeftPanel().getProgramInfoPanel().getPrograms();
        grid.getRewardClubProgram().getPointsBalance().clickAndWait(verifyNoError());
        verifyThat(rewardClubPage, displayed(true));
    }
}
