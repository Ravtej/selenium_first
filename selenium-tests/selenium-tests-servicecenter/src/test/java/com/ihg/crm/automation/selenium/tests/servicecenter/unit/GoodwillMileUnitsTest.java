package com.ihg.crm.automation.selenium.tests.servicecenter.unit;

import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifySuccess;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.assertNoErrors;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.event.EventTransactionType.Goodwill.GOODWILL;
import static com.ihg.automation.selenium.common.event.EventTransactionType.Goodwill.GOODWILL_CODE;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasTextAsInt;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.isDisplayedWithWait;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.isHighlightedAsInvalid;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.common.DateUtils;
import com.ihg.automation.common.RandomUtils;
import com.ihg.automation.selenium.common.Constant;
import com.ihg.automation.selenium.common.billing.EventBillingDetailsTab;
import com.ihg.automation.selenium.common.earning.EventEarningDetailsTab;
import com.ihg.automation.selenium.common.event.AllEventsRow;
import com.ihg.automation.selenium.common.event.EarningEvent;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberAlliance;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.types.Alliance;
import com.ihg.automation.selenium.common.types.GoodwillReason;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.common.types.program.ProgramStatus;
import com.ihg.automation.selenium.common.types.program.RewardClubLevel;
import com.ihg.automation.selenium.gwt.components.Component;
import com.ihg.automation.selenium.servicecenter.pages.LeftPanel;
import com.ihg.automation.selenium.servicecenter.pages.annual.AnnualActivityPage;
import com.ihg.automation.selenium.servicecenter.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsGridRow;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsPage;
import com.ihg.automation.selenium.servicecenter.pages.event.goodwill.GoodwillDetailsPopUp;
import com.ihg.automation.selenium.servicecenter.pages.event.goodwill.GoodwillDetailsTab;
import com.ihg.automation.selenium.servicecenter.pages.event.goodwill.GoodwillGridRowContainer;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.RewardClubPage;
import com.ihg.automation.selenium.servicecenter.pages.unit.GoodwillPopUp;

public class GoodwillMileUnitsTest extends UnitsCommon
{
    private AllEventsPage allEvntsPage = new AllEventsPage();
    private GoodwillGridRowContainer eventContainer;
    private GoodwillDetailsPopUp gdwillDetailsPopUp = new GoodwillDetailsPopUp();

    private Member recipient = new Member();

    private static final GoodwillReason GOODWILL_REASON = GoodwillReason.AGENT_ERROR;
    private static final Alliance ALLIANCE = Alliance.DJA;
    private static final String GOODWILL_MILES = "10000";

    private static final EarningEvent testEvent = new EarningEvent(Constant.BASE_UNITS, GOODWILL_MILES, "0",
            ALLIANCE.getValue(), "Pending");
    private AllEventsRow goodWillEventRow = new AllEventsRow(DateUtils.getFormattedDate(), GOODWILL, ALLIANCE.getCode(),
            null, GOODWILL_MILES);
    private static final String ALLIANCE_NUMBER = RandomUtils.getRandomNumber(10);

    @BeforeClass
    public void beforeClass()
    {
        login();

        recipient.setPersonalInfo(MemberPopulateHelper.getSimplePersonalInfo());
        recipient.addAddress(MemberPopulateHelper.getUnitedStatesAddress());
        MemberAlliance alliance = new MemberAlliance(ALLIANCE, ALLIANCE_NUMBER, recipient.getName());
        recipient.setMilesEarningPreference(alliance);
        recipient.addProgram(Program.RC);

        new EnrollmentPage().enroll(recipient);
        recipient = new AnnualActivityPage().captureCounters(recipient, jdbcTemplate);
    }

    @Test
    public void goodwillMiles()
    {
        GoodwillPopUp goodwillPopUp = new GoodwillPopUp();

        goodwillPopUp.goTo();
        assertNoErrors();

        goodwillPopUp.getAmount().type(GOODWILL_MILES);
        goodwillPopUp.getUnitType().select(ALLIANCE.getCode());
        goodwillPopUp.getReason().selectByCode(GOODWILL_REASON);
        goodwillPopUp.clickSubmitNoError(verifySuccess(GoodwillPopUp.GOODWILL_SUCCESS));
    }

    @Test(dependsOnMethods = { "goodwillMiles" }, priority = 10)
    public void verifyActivitiesPoints()
    {
        new AnnualActivityPage().verifyCounters(recipient);
    }

    @Test(dependsOnMethods = { "goodwillMiles" }, priority = 10)
    public void verifyRCAccountBalance()
    {
        Component cmpnt = new LeftPanel().getProgramInfoPanel().getPrograms().getRewardClubProgram().getPointsBalance();
        verifyThat(cmpnt, hasTextAsInt(0));
    }

    @Test(dependsOnMethods = { "goodwillMiles" }, priority = 10)
    public void verifyTierLevel()
    {
        RewardClubPage rewardClubPage = new RewardClubPage();
        rewardClubPage.goTo();
        rewardClubPage.getSummary().verify(ProgramStatus.OPEN, RewardClubLevel.CLUB);
    }

    @Test(dependsOnMethods = { "goodwillMiles" }, priority = 20)
    public void verifyGoodwillEvent()
    {
        allEvntsPage = new AllEventsPage();
        allEvntsPage.goTo();

        AllEventsGridRow row = allEvntsPage.getGrid().getRow(1);
        row.verify(goodWillEventRow);

        eventContainer = row.expand(GoodwillGridRowContainer.class);
        eventContainer.getGoodwillDetails().verify(GOODWILL_REASON, helper);
    }

    @Test(dependsOnMethods = { "goodwillMiles" }, priority = 30)
    public void openGoodwillEventDetailsPopUp()
    {
        eventContainer.clickDetails();
        verifyThat(gdwillDetailsPopUp, isDisplayedWithWait());
    }

    @Test(dependsOnMethods = { "openGoodwillEventDetailsPopUp" })
    public void verifyGoodwillEventDetailsTab()
    {
        GoodwillDetailsTab tab = gdwillDetailsPopUp.getGoodwillDetailsTab();
        tab.goTo();
        tab.getGoodwillDetails().verify(GOODWILL_REASON, helper);
    }

    @Test(dependsOnMethods = { "openGoodwillEventDetailsPopUp" })
    public void verifyEventEarningDetailsTab()
    {
        EventEarningDetailsTab tab = gdwillDetailsPopUp.getEarningDetailsTab();
        tab.goTo();
        tab.verifyBasicDetailsForMiles(ALLIANCE, GOODWILL_MILES);
        tab.getGrid().verify(testEvent);
    }

    @Test(dependsOnMethods = { "openGoodwillEventDetailsPopUp" })
    public void verifyEventBillingDetailsTab()
    {
        EventBillingDetailsTab tab = gdwillDetailsPopUp.getBillingDetailsTab();
        tab.goTo();
        tab.verifyTabIsEmpty(GOODWILL_CODE);
    }

    @Test(dependsOnMethods = { "verifyGoodwillEventDetailsTab", "verifyEventEarningDetailsTab",
            "verifyEventBillingDetailsTab" }, alwaysRun = true)
    public void closeEventDetails()
    {
        gdwillDetailsPopUp.clickClose();
    }

    @Test(dependsOnMethods = { "closeEventDetails" })
    public void tryToGoodwillMilesNegativeAmount()
    {
        GoodwillPopUp goodwillPopUp = new GoodwillPopUp();

        goodwillPopUp.goTo();
        assertNoErrors();
        goodwillPopUp.populate("-1000", ALLIANCE.getCode(), GOODWILL_REASON);
        goodwillPopUp.clickSubmit();

        verifyThat(goodwillPopUp, displayed(true));
        verifyThat(goodwillPopUp.getAmount(), isHighlightedAsInvalid(true));
    }
}
