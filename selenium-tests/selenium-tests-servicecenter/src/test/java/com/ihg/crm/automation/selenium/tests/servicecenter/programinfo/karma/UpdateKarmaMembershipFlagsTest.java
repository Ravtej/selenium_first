package com.ihg.crm.automation.selenium.tests.servicecenter.programinfo.karma;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static org.hamcrest.CoreMatchers.containsString;

import java.util.ArrayList;
import java.util.Arrays;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.CustomerInfoFlags;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.common.types.program.TierLevelFlag;
import com.ihg.automation.selenium.gwt.components.DualList;
import com.ihg.automation.selenium.matchers.component.ContainsDualListItems;
import com.ihg.automation.selenium.servicecenter.pages.LeftPanel;
import com.ihg.automation.selenium.servicecenter.pages.account.AccountInfoPage;
import com.ihg.automation.selenium.servicecenter.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.servicecenter.pages.programs.KarmaPage;
import com.ihg.automation.selenium.servicecenter.pages.programs.ProgramSummary;
import com.ihg.crm.automation.selenium.tests.servicecenter.LoginLogout;

@Test(groups = { "karma" })
public class UpdateKarmaMembershipFlagsTest extends LoginLogout
{
    private KarmaPage karmaPage = new KarmaPage();
    private ProgramSummary summary;
    private DualList flags;
    private CustomerInfoFlags leftPanelFlags;
    private ArrayList<String> expectedListContent = new ArrayList<String>(TierLevelFlag.getKarValueList());

    private final static String FLAG_TO_SELECT = TierLevelFlag.BIG_CHEESE.getValue();

    @BeforeClass
    public void beforeClass()
    {
        Member member = new Member();
        member.setPersonalInfo(MemberPopulateHelper.getSimplePersonalInfo());
        member.addAddress(MemberPopulateHelper.getUnitedStatesAddress());
        member.addProgram(Program.KAR);

        login();

        new EnrollmentPage().enroll(member);
    }

    @Test
    public void navigateKarmaProgramSummary()
    {
        karmaPage.goTo();
        summary = karmaPage.getSummary();
        summary.clickEdit();
    }

    @Test(dependsOnMethods = { "navigateKarmaProgramSummary" })
    public void verifyAvailableSummaryFlags()
    {
        flags = summary.getFlags();
        verifyThat(flags.getLeftList(), ContainsDualListItems.containsItems(expectedListContent));
    }

    @Test(dependsOnMethods = { "verifyAvailableSummaryFlags", "navigateKarmaProgramSummary" }, alwaysRun = true)
    public void cancelSelectTheFlag()
    {
        flags.select(FLAG_TO_SELECT);
        verifyThat(flags.getRightList(), ContainsDualListItems.containsItems(Arrays.asList(FLAG_TO_SELECT)));
        summary.clickCancel();
        verifyThat(summary, hasText(containsString("No flags selected")));
    }

    @Test(dependsOnMethods = { "verifyAvailableSummaryFlags", "navigateKarmaProgramSummary",
            "cancelSelectTheFlag" }, alwaysRun = true)
    public void selectTheFlag()
    {
        summary.clickEdit();
        flags.select(FLAG_TO_SELECT);
        verifyThat(flags.getRightList(), ContainsDualListItems.containsItems(Arrays.asList(FLAG_TO_SELECT)));
        expectedListContent.remove(FLAG_TO_SELECT);
        verifyThat(flags.getLeftList(), ContainsDualListItems.containsItems(expectedListContent));
        summary.clickSave();
    }

    @Test(dependsOnMethods = { "selectTheFlag" }, alwaysRun = true)
    public void verifyProgramInformationFlags()
    {
        verifyThat(summary, hasText(containsString(FLAG_TO_SELECT)));
    }

    @Test(dependsOnMethods = { "verifyProgramInformationFlags" }, alwaysRun = true)
    public void verifyLeftPanelFlags()
    {
        leftPanelFlags = new LeftPanel().getCustomerInfoPanel().getFlags();
        verifyThat(leftPanelFlags, hasText(containsString(FLAG_TO_SELECT)));
    }

    @Test(dependsOnMethods = { "verifyLeftPanelFlags" }, alwaysRun = true)
    public void verifyAccountInformationFlags()
    {
        leftPanelFlags.getFlag(FLAG_TO_SELECT).clickAndWait();

        AccountInfoPage accountInfoPage = new AccountInfoPage();
        verifyThat(accountInfoPage, displayed(true));
        verifyThat(accountInfoPage.getFlags(), hasText(containsString(FLAG_TO_SELECT)));
    }
}
