package com.ihg.crm.automation.selenium.tests.servicecenter.programinfo.ambassador;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.event.EventTransactionType.Membership.MEMBERSHIP_RENEWAL;
import static com.ihg.automation.selenium.common.member.AmbassadorUtils.RENEW_GOLD_AMBASSADOR_KIT_200;
import static com.ihg.automation.selenium.common.types.program.ProgramStatus.OPEN;
import static com.ihg.automation.selenium.common.types.program.RewardClubLevel.GOLD;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.isDisplayedWithWait;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.common.DateUtils;
import com.ihg.automation.selenium.common.catalog.CatalogItem;
import com.ihg.automation.selenium.common.event.EarningEvent;
import com.ihg.automation.selenium.common.member.AmbassadorJdbcUtils;
import com.ihg.automation.selenium.common.member.AmbassadorUtils;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.order.Order;
import com.ihg.automation.selenium.common.order.OrderFactory;
import com.ihg.automation.selenium.common.pages.PaymentMethod;
import com.ihg.automation.selenium.common.payment.CashPayment;
import com.ihg.automation.selenium.common.payment.CashPaymentContainer;
import com.ihg.automation.selenium.common.payment.PaymentDetailsPopUp;
import com.ihg.automation.selenium.gwt.components.Mode;
import com.ihg.automation.selenium.servicecenter.pages.LeftPanel;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsGridRow;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsPage;
import com.ihg.automation.selenium.servicecenter.pages.event.enroll.MembershipDetails;
import com.ihg.automation.selenium.servicecenter.pages.event.enroll.MembershipDetailsPopUp;
import com.ihg.automation.selenium.servicecenter.pages.event.enroll.MembershipDetailsTab;
import com.ihg.automation.selenium.servicecenter.pages.event.enroll.MembershipGridRowContainer;
import com.ihg.automation.selenium.servicecenter.pages.programs.amb.AmbassadorPage;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.RewardClubPage;

public class RenewByCashTest extends AmbassadorCommon
{
    public static final CatalogItem RENEW_AWARD = AmbassadorUtils.AMBASSADOR_RENEWAL_$200;

    private AmbassadorPage ambassadorPage = new AmbassadorPage();
    private EarningEvent event = new EarningEvent("Base Awards", "1", "", "", "Created");
    private MembershipDetailsPopUp membershipDetailsPopUp = new MembershipDetailsPopUp();
    private AllEventsPage allEventsPage = new AllEventsPage();
    private MembershipGridRowContainer container;
    private MembershipDetails membershipDetails;
    private final static String CASH_DEPOSIT_DATE = DateUtils.getFormattedDate();
    private final static String CASH_DEPOSIT_NUMBER = "12345";
    private final static String CASH_NAME = "test name";
    private final static String CASH_LOCATION = "test location";
    private CashPayment cashPaymentItem = new CashPayment(RENEW_AWARD, CASH_NAME, CASH_LOCATION, CASH_DEPOSIT_DATE,
            CASH_DEPOSIT_NUMBER);
    private PaymentDetailsPopUp paymentDetailsPopUp = new PaymentDetailsPopUp();
    private CashPaymentContainer cashPayment;
    private Member member;
    private int daysShift;

    @BeforeClass
    public void beforeClass()
    {
        login();

        member = enrollAMBmember();
        daysShift = new AmbassadorJdbcUtils(jdbcTemplateUpdate, lastUpdateUserId).updateForRenew(member);
        new LeftPanel().reOpenMemberProfile(member);
        ambassadorPage.goTo();
    }

    @Test
    public void verifyRenewButtonAndExpirationDate()
    {
        ambassadorPage.getSummary().verifyRenewAndExtendButtons(true, true);
    }

    @Test(dependsOnMethods = "verifyRenewButtonAndExpirationDate", alwaysRun = true)
    public void verifyPaymentMethod()
    {
        ambassadorPage.getSummary().clickRenew();
        paymentDetailsPopUp = new PaymentDetailsPopUp();
        paymentDetailsPopUp.selectAmount(RENEW_AWARD);
        verifyThat(paymentDetailsPopUp.getPaymentTypeButton(PaymentMethod.CASH), displayed(true));
        verifyThat(paymentDetailsPopUp.getPaymentTypeButton(PaymentMethod.CHECK), displayed(true));
        verifyThat(paymentDetailsPopUp.getPaymentTypeButton(PaymentMethod.CREDIT_CARD), displayed(true));
    }

    @Test(dependsOnMethods = "verifyPaymentMethod", alwaysRun = true)
    public void verifyCashPaymentDetailsPopUp()
    {
        paymentDetailsPopUp.getPaymentTypeButton(PaymentMethod.CASH).select();
        cashPayment = paymentDetailsPopUp.getCashPayment();
        verifyThat(cashPayment.getName(), displayed(true));
        verifyThat(cashPayment.getLocation(), displayed(true));
        verifyThat(cashPayment.getDepositDate(), displayed(true));
        verifyThat(cashPayment.getDepositNumber(), displayed(true));
    }

    @Test(dependsOnMethods = "verifyCashPaymentDetailsPopUp", alwaysRun = true)
    public void populateCashPaymentDetailsPopUp()
    {
        cashPayment.populate(cashPaymentItem);
        paymentDetailsPopUp.clickSubmitNoError();
    }

    @Test(dependsOnMethods = "populateCashPaymentDetailsPopUp", alwaysRun = true)
    public void verifyExpiredDate()
    {
        ambassadorPage.getSummary().verifyExpDateAfterRenew(daysShift);
    }

    @Test(dependsOnMethods = "verifyExpiredDate", alwaysRun = true)
    public void verifyTierLevel()
    {
        RewardClubPage rewardClubPage = new RewardClubPage();
        rewardClubPage.goTo();
        rewardClubPage.getSummary().verify(OPEN, GOLD, 1, daysShift);
    }

    @Test(dependsOnMethods = "verifyTierLevel", alwaysRun = true)
    public void verifyAllEventsContainer()
    {
        allEventsPage.goTo();
        AllEventsGridRow membershipRenewalRow = allEventsPage.getGrid().getRow(MEMBERSHIP_RENEWAL);

        container = membershipRenewalRow.expand(MembershipGridRowContainer.class);
        membershipDetails = container.getMembershipDetails();
        membershipDetails.getPaymentDetails().verify(cashPaymentItem, Mode.VIEW);
        membershipDetails.verifyScSource(helper);
    }

    @Test(dependsOnMethods = "verifyAllEventsContainer", alwaysRun = true)
    public void openMembershipDetailsPopUpVerify()
    {
        container.clickDetails();
        membershipDetailsPopUp = new MembershipDetailsPopUp();
        verifyThat(membershipDetailsPopUp, isDisplayedWithWait());
    }

    @Test(dependsOnMethods = "openMembershipDetailsPopUpVerify", alwaysRun = true)
    public void verifyAllEventsMembershipDetailsTab()
    {
        MembershipDetailsTab membershipDetailsTab = membershipDetailsPopUp.getMembershipDetailsTab();
        membershipDetailsTab.goTo();
        membershipDetails = membershipDetailsTab.getMembershipDetails();
        membershipDetails.getPaymentDetails().verify(cashPaymentItem, Mode.VIEW);
        membershipDetails.verifyScSource(helper);
    }

    @Test(dependsOnMethods = "verifyAllEventsMembershipDetailsTab", alwaysRun = true)
    public void verifyAllEventsEarningDetailsTab()
    {
        verifyEarningDetailsTab(RENEW_AWARD, RENEW_GOLD_AMBASSADOR_KIT_200);
    }

    @Test(dependsOnMethods = { "verifyAllEventsEarningDetailsTab" }, alwaysRun = true)
    public void validateBillingDetailsTab()
    {
        validateBillingDetails(RENEW_AWARD);
    }

    @Test(dependsOnMethods = { "validateBillingDetailsTab" }, alwaysRun = true)
    public void verifyOrderSystemContainer()
    {
        Order order = OrderFactory.getSystemOrder(RENEW_GOLD_AMBASSADOR_KIT_200, member, helper);
        verifyOrderSystemDetails(order);
    }
}
