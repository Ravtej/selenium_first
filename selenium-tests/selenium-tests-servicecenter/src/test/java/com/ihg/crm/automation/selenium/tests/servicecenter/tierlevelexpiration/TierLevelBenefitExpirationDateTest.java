package com.ihg.crm.automation.selenium.tests.servicecenter.tierlevelexpiration;

import static com.ihg.automation.selenium.common.types.program.ProgramStatus.OPEN;
import static com.ihg.automation.selenium.common.types.program.RewardClubLevel.PLTN;
import static com.ihg.automation.selenium.common.types.program.RewardClubLevel.SPRE;
import static com.ihg.automation.selenium.common.types.program.RewardClubLevelReason.BASE;
import static com.ihg.automation.selenium.common.types.program.TierLevelExpiration.EXPIRE_CURRENT_YEAR;
import static com.ihg.automation.selenium.common.types.program.TierLevelExpiration.EXPIRE_NEXT_YEAR;
import static com.ihg.automation.selenium.common.types.program.TierLevelExpiration.LIFETIME;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.common.types.program.RewardClubLevel;
import com.ihg.automation.selenium.servicecenter.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.BenefitsGrid;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.RewardClubPage;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.RewardClubSummary;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.TierLevelBenefitsDetailsPopUp;
import com.ihg.crm.automation.selenium.tests.servicecenter.LoginLogout;

public class TierLevelBenefitExpirationDateTest extends LoginLogout
{
    @BeforeClass
    public void beforeClass()
    {
        Member member = new Member();
        member.setPersonalInfo(MemberPopulateHelper.getSimplePersonalInfo());
        member.addAddress(MemberPopulateHelper.getUnitedStatesAddress());
        member.addProgram(Program.RC);

        login();
        new EnrollmentPage().enroll(member);
    }

    @DataProvider(name = "expirationDate")
    public Object[][] expirationDate()
    {
        return new Object[][] { { EXPIRE_CURRENT_YEAR, 0 }, { EXPIRE_NEXT_YEAR, 1 }, { LIFETIME, 1 } };
    }

    @Test(dataProvider = "expirationDate", alwaysRun = true)
    public void verifyTierLevelBenefitExpirationDate(String expDate, int yearShift)
    {
        setSpireTierLevel(SPRE, expDate);

        RewardClubSummary summary = new RewardClubPage().getSummary();

        if (LIFETIME.equals(expDate))
        {
            summary.verifyLifetimeTierLevel(OPEN, SPRE);
        }
        else
        {
            summary.verify(OPEN, SPRE, yearShift);
        }

        summary.getTierLevelBenefits().click();
        TierLevelBenefitsDetailsPopUp tierLevelBenefitsDetailsPopUp = new TierLevelBenefitsDetailsPopUp();
        BenefitsGrid benefitsGrid = tierLevelBenefitsDetailsPopUp.getBenefitsGrid();

        benefitsGrid.getRow(1).verifyExpirationDate(yearShift);
        benefitsGrid.getRow(2).verifyExpirationDate(yearShift);

        tierLevelBenefitsDetailsPopUp.clickClose();
    }

    @AfterMethod
    public void downgradeLevel()
    {
        setSpireTierLevel(PLTN, EXPIRE_CURRENT_YEAR);
    }

    private void setSpireTierLevel(RewardClubLevel rewardClubLevel, String expDate)
    {
        RewardClubPage rewardClubPage = new RewardClubPage();
        rewardClubPage.goTo();
        rewardClubPage.getSummary().setTierLevelWithExpiration(rewardClubLevel, BASE, expDate);
    }
}
