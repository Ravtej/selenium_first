package com.ihg.crm.automation.selenium.tests.servicecenter.refdata;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.components.Country.BY;
import static com.ihg.automation.selenium.common.components.Country.CN;
import static com.ihg.automation.selenium.common.components.Country.US;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasText;
import static com.ihg.automation.selenium.matchers.text.EntityMatcherFactory.isValue;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.components.CountrySelect;
import com.ihg.automation.selenium.common.contact.Address;
import com.ihg.automation.selenium.common.hotel.AdvancedHotelSearchPopUp;
import com.ihg.automation.selenium.common.hotel.HotelAddress;
import com.ihg.automation.selenium.common.types.Currency;
import com.ihg.automation.selenium.common.types.address.RegionLabel;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.gwt.components.input.SearchInput;
import com.ihg.automation.selenium.gwt.components.input.Select;
import com.ihg.automation.selenium.servicecenter.pages.GuestSearch;
import com.ihg.automation.selenium.servicecenter.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.servicecenter.pages.event.stay.CreateStayPopUp;
import com.ihg.automation.selenium.servicecenter.pages.event.stay.StayEventsPage;
import com.ihg.automation.selenium.servicecenter.pages.event.stay.StayInfoCreate;
import com.ihg.crm.automation.selenium.tests.servicecenter.LoginLogout;

public class VerifyCountryAndCurrencyTest extends LoginLogout
{
    @BeforeClass
    public void before()
    {
        login();
    }

    @Test(priority = 10)
    public void verifyCountryOnEnrollmentPage() throws InterruptedException
    {
        EnrollmentPage enrollmentPage = new EnrollmentPage();
        enrollmentPage.goTo();
        enrollmentPage.getPrograms().getProgramCheckbox(Program.RC).check();

        CountrySelect residenceCountry = enrollmentPage.getCustomerInfo().getResidenceCountry();
        residenceCountry.select(CN);
        verifyThat(residenceCountry, hasText(isValue(CN)));

        Address address = enrollmentPage.getAddress();
        address.getCountry().select(US);
        verifyThat(address.getCountry(), hasText(isValue(US)));

        address.getRegion1(RegionLabel.STATE).select("TX - Texas");

        enrollmentPage.clickCancel();
    }

    @Test(priority = 20)
    public void verifyCountryOnCustomerSearchPanel()
    {
        CountrySelect country = new GuestSearch().getCustomerSearch().getCountry();
        country.select(BY);
        verifyThat(country, hasText(isValue(BY)));
    }

    @Test(priority = 30)
    public void verifyStateOnHotelAdvancedSearch() throws InterruptedException
    {
        GuestSearch.SearchByStay searchByStay = new GuestSearch().getSearchByStay();
        searchByStay.expand();
        searchByStay.getHotel().clickButton();

        AdvancedHotelSearchPopUp hotelSearchPopUp = new AdvancedHotelSearchPopUp();

        HotelAddress hotelAddress = hotelSearchPopUp.getSearchFields().getHotelAddress();
        hotelAddress.populateCountryAndState(US, "GA - Georgia");

        verifyThat(hotelAddress.getCountry(), hasText(isValue(US)));
        verifyThat(hotelAddress.getState(), hasText("Georgia"));

        hotelSearchPopUp.getCancel().clickAndWait();
    }

    @Test(priority = 40)
    public void verifyCurrencyOnStay()
    {
        new GuestSearch().byMemberNumber(memberID);

        StayEventsPage stayPage = new StayEventsPage();
        stayPage.goTo();

        stayPage.getButtonBar().clickCreateStay();

        StayInfoCreate stayInfo = new CreateStayPopUp().getStayDetails().getStayInfo();
        SearchInput hotel = stayInfo.getHotel();
        hotel.typeAndWait("LONHB");

        Select hotelCurrency = stayInfo.getHotelCurrency();
        verifyThat(hotelCurrency, hasText(Currency.GBP.getCodeWithValue()));

        hotel.typeAndWait("ATLCP");
        verifyThat(hotelCurrency, hasText((Currency.USD.getCodeWithValue())));
    }
}
