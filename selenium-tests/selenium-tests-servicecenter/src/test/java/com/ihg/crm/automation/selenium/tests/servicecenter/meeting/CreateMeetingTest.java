package com.ihg.crm.automation.selenium.tests.servicecenter.meeting;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.event.EventTransactionType.Meeting.MEETING;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.isDisplayedWithWait;
import static com.ihg.automation.selenium.matchers.component.GridSize.size;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static com.ihg.automation.selenium.matchers.component.IsEnabled.enabled;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.Constant;
import com.ihg.automation.selenium.common.MeetingDetails;
import com.ihg.automation.selenium.common.billing.EventBillingDetail;
import com.ihg.automation.selenium.common.billing.EventBillingDetailsTab;
import com.ihg.automation.selenium.common.earning.EventEarningDetailsTab;
import com.ihg.automation.selenium.common.earning.EventEarningGrid;
import com.ihg.automation.selenium.common.event.AllEventsRow;
import com.ihg.automation.selenium.common.event.EarningEvent;
import com.ihg.automation.selenium.common.event.SearchButtonBar;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.pages.PaymentType;
import com.ihg.automation.selenium.common.types.program.ProgramStatus;
import com.ihg.automation.selenium.common.types.program.RewardClubLevel;
import com.ihg.automation.selenium.servicecenter.pages.GuestSearch;
import com.ihg.automation.selenium.servicecenter.pages.LeftPanel;
import com.ihg.automation.selenium.servicecenter.pages.annual.AnnualActivityPage;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsGridRow;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsPage;
import com.ihg.automation.selenium.servicecenter.pages.event.meeting.MeetingEventDetailsPopUp;
import com.ihg.automation.selenium.servicecenter.pages.event.meeting.MeetingEventDetailsTab;
import com.ihg.automation.selenium.servicecenter.pages.event.meeting.MeetingEventGridRow;
import com.ihg.automation.selenium.servicecenter.pages.event.meeting.MeetingEventGridRowContainer;
import com.ihg.automation.selenium.servicecenter.pages.event.meeting.MeetingEventPage;
import com.ihg.automation.selenium.servicecenter.pages.event.meeting.MeetingEventRow;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.RewardClubPage;
import com.ihg.crm.automation.selenium.tests.servicecenter.LoginLogout;

public class CreateMeetingTest extends LoginLogout
{
    private AnnualActivityPage annualPage = new AnnualActivityPage();
    private MeetingEventPage meetingEventsPage = new MeetingEventPage();
    private MeetingEventGridRowContainer meetingEventContainer;
    private MeetingEventDetailsPopUp meetingEventDetailsPopUp = new MeetingEventDetailsPopUp();
    private RewardClubPage rwrdsPage = new RewardClubPage();
    private AllEventsPage allEventsPage = new AllEventsPage();

    private String checkInDate = "19Jun15";
    private String checkOutDate = "22Jun15";
    private Member member = new Member();
    private MeetingDetails meetingDetails = new MeetingDetails("ATLCP", checkInDate, checkOutDate, "4000",
            Constant.RC_POINTS, "100");

    private MeetingEventRow meetingEventRow;
    private AllEventsRow meetingEvent;
    private AllEventsGridRow allEventsGridRow;
    private String nameOnPanel;

    @BeforeClass
    public void beforeClass()
    {
        meetingDetails.setMeetingEventName("SC UI test automation 24Jun15");
        meetingDetails.setMeetingEventType("Social");
        meetingDetails.setTotalNumberOfRoomNights("3");
        meetingDetails.setTotalQualifiedNonRoomsRevenue("100");
        meetingDetails.setPaymentType(PaymentType.CASH);

        meetingEventRow = new MeetingEventRow(meetingDetails);
        meetingEventRow.setTransDate("24Jun15");
        meetingEvent = new AllEventsRow(
                "24Jun15", MEETING, "ATLCP " + meetingDetails.getCheckInDate() + " - "
                        + meetingDetails.getCheckOutDate() + " " + meetingDetails.getTotalNumberOfRoomNights(),
                "4000", "");

        login();

        new GuestSearch().byMemberNumber(memberIDWithMeetingEvent);

        nameOnPanel = new LeftPanel().getCustomerInfoPanel().getFullName().getText();

        annualPage.goTo();
        member.setLifetimeActivity(annualPage.getLifetimeActivityCounters());
        member.setAnnualActivity(annualPage.getAnnualActivityCounters(15));
    }

    @Test
    public void verifyTierLevel()
    {
        rwrdsPage.goTo();
        rwrdsPage.getSummary().verify(ProgramStatus.OPEN, RewardClubLevel.CLUB);
    }

    @Test(dependsOnMethods = { "verifyTierLevel" }, alwaysRun = true)
    public void verifyMeetingSearchPage()
    {
        meetingEventsPage.goTo();

        verifyThat(meetingEventsPage, isDisplayedWithWait());
        meetingEventsPage.getSearchFields().verifyDefaults();
        meetingEventsPage.verifyGridPanelText(nameOnPanel);
        verifyThat(meetingEventsPage.getCreateMeeting(), enabled(false));
        verifyThat(meetingEventsPage.getCreateMeetingBonus(), enabled(false));

        SearchButtonBar buttonBar = meetingEventsPage.getButtonBar();
        verifyThat(buttonBar.getClearCriteria(), displayed(true));
        verifyThat(buttonBar.getSearch(), displayed(true));
    }

    @Test(dependsOnMethods = { "verifyMeetingSearchPage" })
    public void verifyMeetingEventsEventRow()
    {
        MeetingEventGridRow row = meetingEventsPage.getGrid().getRow(meetingEventRow);
        row.verify(meetingEventRow);
    }

    @Test(dependsOnMethods = { "verifyMeetingEventsEventRow" }, alwaysRun = true)
    public void verifyMeetingEventsEventRowContainerDetails()
    {
        meetingDetails.setNegotiatedRoomRate("100.00 USD");
        meetingDetails.setTotalRoomsRevenue("300.00 USD");
        meetingDetails.setTotalQualifiedNonRoomsRevenue("300.00 USD");

        MeetingEventGridRow row = meetingEventsPage.getGrid().getRow(meetingEventRow);
        meetingEventContainer = row.expand(MeetingEventGridRowContainer.class);
        meetingEventContainer.getMeetingDetails().verify(meetingDetails, helper);
    }

    @Test(dependsOnMethods = { "verifyMeetingEventsEventRowContainerDetails" }, alwaysRun = true)
    public void openMeetingEventsEventDetailsPopUp()
    {
        meetingEventContainer.getDetails().clickAndWait();
        verifyThat(meetingEventDetailsPopUp, isDisplayedWithWait());
    }

    @Test(dependsOnMethods = { "openMeetingEventsEventDetailsPopUp" })
    public void verifyMeetingEventsEventDetailsPopUpMeetingDetails()
    {
        MeetingEventDetailsTab tab = meetingEventDetailsPopUp.getMeetingDetailsTab();
        tab.goTo();
        tab.getDetails().verify(meetingDetails, helper);
    }

    @Test(dependsOnMethods = { "openMeetingEventsEventDetailsPopUp" })
    public void verifyMeetingEventsEventDetailsPopUpEarningDetails()
    {
        verifyForBaseUnitsEvent();
    }

    @Test(dependsOnMethods = { "openMeetingEventsEventDetailsPopUp" })
    public void verifyMeetingEventsEventDetailsPopUpBillingDetails()
    {
        verifyBaseUSDBilling();
    }

    @Test(dependsOnMethods = { "verifyMeetingEventsEventDetailsPopUpBillingDetails",
            "verifyMeetingEventsEventDetailsPopUpEarningDetails",
            "verifyMeetingEventsEventDetailsPopUpMeetingDetails" }, alwaysRun = true)
    public void closeMeetingEventsEventDetailsPopUp()
    {
        meetingEventDetailsPopUp.clickClose();
        verifyThat(meetingEventDetailsPopUp, displayed(false));
    }

    @Test(dependsOnMethods = { "closeMeetingEventsEventDetailsPopUp" }, alwaysRun = true)
    public void verifyAllEventsMeetingEventRow()
    {
        allEventsPage.goTo();
        allEventsGridRow = allEventsPage.getGrid().getRow(meetingEvent.getTransType(), meetingEvent.getDetail());
        allEventsGridRow.verify(meetingEvent);
    }

    @Test(dependsOnMethods = { "verifyAllEventsMeetingEventRow" }, alwaysRun = true)
    public void verifyAllEventsMeetingEventRowDetails()
    {
        meetingEventContainer = allEventsGridRow.expand(MeetingEventGridRowContainer.class);
        meetingEventContainer.getMeetingDetails().verify(meetingDetails, helper);
    }

    @Test(dependsOnMethods = { "verifyAllEventsMeetingEventRowDetails" }, alwaysRun = true)
    public void openAllEventsMeetingEventsEventDetailsPopUp()
    {
        meetingEventContainer.getDetails().clickAndWait();
        meetingEventDetailsPopUp = new MeetingEventDetailsPopUp();
        verifyThat(meetingEventDetailsPopUp, isDisplayedWithWait());
    }

    @Test(dependsOnMethods = { "openAllEventsMeetingEventsEventDetailsPopUp" })
    public void verifyAllEventsMeetingEventsEventDetailsPopUpMeetingDetails()
    {
        MeetingEventDetailsTab tab = meetingEventDetailsPopUp.getMeetingDetailsTab();
        tab.goTo();
        tab.getDetails().verify(meetingDetails, helper);
    }

    @Test(dependsOnMethods = { "openAllEventsMeetingEventsEventDetailsPopUp" })
    public void verifyAllEventsMeetingEventsEventDetailsPopUpEarningDetails()
    {
        verifyForBaseUnitsEvent();
    }

    @Test(dependsOnMethods = { "openAllEventsMeetingEventsEventDetailsPopUp" })
    public void verifyAllEventsMeetingEventsEventDetailsPopUpBillingDetails()
    {
        verifyBaseUSDBilling();
    }

    @Test(dependsOnMethods = { "verifyAllEventsMeetingEventsEventDetailsPopUpBillingDetails",
            "verifyAllEventsMeetingEventsEventDetailsPopUpEarningDetails",
            "verifyAllEventsMeetingEventsEventDetailsPopUpMeetingDetails" }, alwaysRun = true)
    public void closeAllEventsMeetingEventsEventDetailsPopUp()
    {
        meetingEventDetailsPopUp.clickClose();
        verifyThat(meetingEventDetailsPopUp, displayed(false));
    }

    @Test(dependsOnMethods = { "closeAllEventsMeetingEventsEventDetailsPopUp" }, alwaysRun = true)
    public void verifyLeftPanelProgramInformation()
    {
        new LeftPanel().getProgramInfoPanel().getPrograms().getRewardClubProgram().verify(RewardClubLevel.CLUB, "0");
    }

    @Test(dependsOnMethods = { "verifyLeftPanelProgramInformation" }, alwaysRun = true)
    public void verifyActivityCounters()
    {
        member.getLifetimeActivity().setTotalUnitsFromString("4000");
        member.getLifetimeActivity().setBonusUnitsFromString("4000");
        member.getLifetimeActivity().setCurrentBalanceFromString("0");
        member.getAnnualActivity().setTotalQualifiedUnitsFromString("4000");
        member.getAnnualActivity().setTotalUnitsFromString("4000");

        annualPage = new AnnualActivityPage();
        annualPage.goTo();
        annualPage.verifyLifetimeActivities(member.getLifetimeActivity());
        annualPage.getAnnualActivities().getRowByYear(15).verify(member.getAnnualActivity());
    }

    @Test(dependsOnMethods = { "verifyActivityCounters" }, alwaysRun = true)
    public void verifyTierLevelActivityCounters()
    {
        rwrdsPage.goTo();
        rwrdsPage.getAnnualActivities(2015).getPointsRow().verifyCurrent("4000");
    }

    private void verifyBaseUSDBilling()
    {
        EventBillingDetailsTab tab = meetingEventDetailsPopUp.getBillingDetailsTab();
        tab.goTo();
        EventBillingDetail eventBillingDetail = tab.getEventBillingDetail(1);
        verifyThat(eventBillingDetail.getBillingDate(), hasText("June 24, 2015"));
        verifyThat(eventBillingDetail.getEventType(), hasText(MEETING));
        eventBillingDetail.getBaseLoyaltyUnits().verifyUSDAmount("16", "ATLCP");
    }

    private void verifyForBaseUnitsEvent()
    {
        EventEarningDetailsTab eventEarningDetailsTab = meetingEventDetailsPopUp.getEarningDetailsTab();
        eventEarningDetailsTab.goTo();

        eventEarningDetailsTab.verifyBasicDetailsForPoints("4000", "4000");

        EarningEvent baseUnits = new EarningEvent(Constant.BASE_UNITS, "4000", "4000", Constant.RC_POINTS);
        baseUnits.setDateEarned("24Jun2015");

        EventEarningGrid grid = eventEarningDetailsTab.getGrid();
        verifyThat(grid, size(1));
        grid.getRow(1).verify(baseUnits);
    }
}
