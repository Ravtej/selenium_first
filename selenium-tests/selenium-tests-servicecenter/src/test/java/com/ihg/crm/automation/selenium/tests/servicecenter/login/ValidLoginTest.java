package com.ihg.crm.automation.selenium.tests.servicecenter.login;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;

import org.testng.annotations.Test;

import com.ihg.crm.automation.selenium.tests.servicecenter.LoginLogout;

public class ValidLoginTest extends LoginLogout
{
    @Test
    public void submitValidCredentials()
    {
        login();

        verifyThat(getWelcome(), displayed(true));
    }
}
