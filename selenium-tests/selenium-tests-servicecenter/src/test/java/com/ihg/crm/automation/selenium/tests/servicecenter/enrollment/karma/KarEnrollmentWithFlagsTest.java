package com.ihg.crm.automation.selenium.tests.servicecenter.enrollment.karma;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.compareLists;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static org.hamcrest.CoreMatchers.containsString;

import java.util.Arrays;
import java.util.List;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.CustomerInfoFlags;
import com.ihg.automation.selenium.common.CustomerInfoPanel;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.common.types.program.TierLevelFlag;
import com.ihg.automation.selenium.servicecenter.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.servicecenter.pages.programs.KarmaPage;
import com.ihg.automation.selenium.servicecenter.pages.programs.ProgramSummary;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.RewardClubPage;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.RewardClubSummary;
import com.ihg.crm.automation.selenium.tests.servicecenter.LoginLogout;

@Test(groups = { "karma" })
public class KarEnrollmentWithFlagsTest extends LoginLogout
{
    private EnrollmentPage enrollmentPage = new EnrollmentPage();
    private List<String> flags = Arrays.asList(TierLevelFlag.BIG_CHEESE.getValue(),
            TierLevelFlag.SOCIAL_INFLUENCER.getValue(), TierLevelFlag.POSSIBLE_FRAUD.getValue());

    @BeforeClass
    public void before()
    {
        Member member = new Member();
        member.addProgram(Program.KAR);
        member.setPersonalInfo(MemberPopulateHelper.getSimplePersonalInfo());
        member.addAddress(MemberPopulateHelper.getUnitedStatesAddress());
        member.addFlags(flags);

        login();

        enrollmentPage.enroll(member);
    }

    @Test
    public void customerInfoPanelFlags()
    {
        CustomerInfoFlags customerInfoFlags = new CustomerInfoPanel().getFlags();
        compareLists(customerInfoFlags, flags, customerInfoFlags.getFlags());
    }

    @Test
    public void karmaProgramInfoFlags()
    {
        KarmaPage karmaPage = new KarmaPage();
        karmaPage.goTo();
        ProgramSummary karmaSummary = karmaPage.getSummary();
        for (String flag : flags)
        {
            verifyThat(karmaSummary, hasText(containsString(flag)));
        }
    }

    @Test
    public void rcProgramInfoFlags()
    {
        RewardClubPage rewardClubPage = new RewardClubPage();
        rewardClubPage.goTo();
        RewardClubSummary rcSummary = rewardClubPage.getSummary();
        verifyThat(rcSummary, hasText(containsString(TierLevelFlag.POSSIBLE_FRAUD.getValue())));
    }
}
