package com.ihg.crm.automation.selenium.tests.servicecenter.enrollment;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.Constant.NOT_AVAILABLE;
import static com.ihg.automation.selenium.common.event.EventTransactionType.Membership.ENROLL;
import static com.ihg.automation.selenium.common.event.EventTransactionType.Offer.OFFER_REGISTRATION;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasDefault;
import static com.ihg.automation.selenium.matchers.component.GridSize.size;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static com.ihg.automation.selenium.matchers.component.ViewableMatcher.hasTextInView;
import static org.hamcrest.core.StringContains.containsString;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.common.DateUtils;
import com.ihg.automation.selenium.common.Constant;
import com.ihg.automation.selenium.common.CustomerInfoPanel;
import com.ihg.automation.selenium.common.member.GuestAddress;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.offer.OfferFactory;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.common.types.program.ProgramStatus;
import com.ihg.automation.selenium.common.types.program.RewardClubLevel;
import com.ihg.automation.selenium.gwt.components.Component;
import com.ihg.automation.selenium.gwt.components.Mode;
import com.ihg.automation.selenium.servicecenter.pages.account.AccountInfoPage;
import com.ihg.automation.selenium.servicecenter.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsGrid;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsPage;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsSearch;
import com.ihg.automation.selenium.servicecenter.pages.event.all.OfferDetails;
import com.ihg.automation.selenium.servicecenter.pages.event.all.OfferGridRowContainer;
import com.ihg.automation.selenium.servicecenter.pages.event.enroll.MembershipGridRowContainer;
import com.ihg.automation.selenium.servicecenter.pages.personal.PersonalInfoPage;
import com.ihg.automation.selenium.servicecenter.pages.programs.ProgramPageBase.EnrollmentDetails;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.RewardClubPage;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.RewardClubSummary;
import com.ihg.crm.automation.selenium.tests.servicecenter.LoginLogout;

public class CompleteEnrollmentTest extends LoginLogout
{
    private EnrollmentPage enrollmentPage = new EnrollmentPage();
    private Member member = new Member();

    private RewardClubPage rewardClubPage;

    private AllEventsPage allEventsPage;
    private AllEventsSearch allEventsSearch;
    private AllEventsGrid allEventsGrid;

    @BeforeClass
    public void before()
    {
        member.addProgram(Program.RC);
        member.addAddress(MemberPopulateHelper.getUnitedStatesAddress());
        member.setPersonalInfo(MemberPopulateHelper.getSimplePersonalInfo());

        login();

        enrollmentPage.enroll(member);
    }

    @Test
    public void validatePersonalInformation()
    {
        PersonalInfoPage persInfo = new PersonalInfoPage();
        persInfo.goTo();
        persInfo.getCustomerInfo().verify(member, Mode.VIEW);
        persInfo.getAddressList().getExpandedContact().verify(member.getPreferredAddress(), Mode.VIEW);
    }

    @Test(dependsOnMethods = { "validatePersonalInformation" })
    public void validateAccountInformation()
    {
        AccountInfoPage accountPage = new AccountInfoPage();
        accountPage.goTo();
        verifyThat(accountPage.getSecurity().getLockout(), hasTextInView(NOT_AVAILABLE));
        accountPage.getFailedSelfServiceLoginAttempts().expand();
        verifyThat(accountPage.getFailedSelfServiceLoginAttempts().getGrid(), size(0));
        accountPage.verify(ProgramStatus.OPEN);
        verifyThat(accountPage.getFlags(), hasText(containsString("No flags selected")));
    }

    @Test(dependsOnMethods = { "validateAccountInformation" })
    public void validateProgramInfoSummaryDetails()
    {
        rewardClubPage = new RewardClubPage();
        rewardClubPage.goTo();
        RewardClubSummary rewardClubsummary = rewardClubPage.getSummary();
        rewardClubsummary.verify(ProgramStatus.OPEN, RewardClubLevel.CLUB);
        verifyThat(rewardClubsummary.getMemberId(), hasText(member.getRCProgramId()));
        verifyThat(rewardClubsummary.getUnitsBalance(), hasText("0"));
        verifyThat(rewardClubsummary, hasText(containsString("No flags selected")));
    }

    @Test(dependsOnMethods = { "validateProgramInfoSummaryDetails" })
    public void validateProgramInfoEnrollmentDetails()
    {
        rewardClubPage = new RewardClubPage();
        rewardClubPage.goTo();
        EnrollmentDetails enrlDetails = rewardClubPage.getEnrollmentDetails();
        verifyThat(enrlDetails.getOfferCode(), hasText("SCCAL"));
        verifyThat(enrlDetails.getPaymentDetails().getMethod(), hasDefault());
        verifyThat(enrlDetails.getReferringMember(), hasText(NOT_AVAILABLE));
        verifyThat(enrlDetails.getRenewalDate(), hasDefault());
        verifyThat(enrlDetails.getEnrollDate(), hasText(DateUtils.getFormattedDate()));
        enrlDetails.verifyScSource(helper);
    }

    @Test(dependsOnMethods = { "validateProgramInfoEnrollmentDetails" })
    public void validateEarningDetails()
    {
        rewardClubPage = new RewardClubPage();
        rewardClubPage.goTo();
        verifyThat(rewardClubPage.getEarningDetails().getEarningPreference(), hasTextInView(Constant.RC_POINTS));
    }

    @Test(dependsOnMethods = { "validateEarningDetails" })
    public void validateAllianceGrid()
    {
        verifyThat(rewardClubPage.getAlliances(), size(0));
    }

    @Test(dependsOnMethods = { "validateAllianceGrid" })
    public void allEventsEnrollment()
    {
        allEventsPage = new AllEventsPage();
        allEventsPage.goTo();

        allEventsGrid = allEventsPage.getGrid();
        verifyThat(allEventsGrid, size(2));

        allEventsSearch = allEventsPage.getSearchFields();
        allEventsSearch.getEventType().select("Membership");
        allEventsSearch.getTransactionType().select(ENROLL);
        allEventsPage.getButtonBar().clickSearch();

        allEventsGrid = allEventsPage.getGrid();
        verifyThat(allEventsGrid, size(1));

        allEventsGrid.getRow(1).expand(MembershipGridRowContainer.class).getMembershipDetails().verifyScSource(helper);
    }

    @Test(dependsOnMethods = { "allEventsEnrollment" })
    public void allEventsOffer()
    {
        allEventsPage.getButtonBar().clickClearCriteria();
        allEventsSearch.getEventType().select("Offer");
        allEventsSearch.getTransactionType().select(OFFER_REGISTRATION);
        allEventsPage.getButtonBar().clickSearch();

        allEventsGrid = allEventsPage.getGrid();
        verifyThat(allEventsGrid, size(1));

        OfferDetails offerDetails = allEventsGrid.getRow(1).expand(OfferGridRowContainer.class).getOfferDetails();
        offerDetails.verify(OfferFactory.DELAYED_FULFILMENT_GLOBAL, helper);
    }

    @Test(dependsOnMethods = { "allEventsOffer" })
    public void verifyCustomerInformationPanel()
    {
        CustomerInfoPanel custPanel = new CustomerInfoPanel();
        verifyThat(custPanel.getFullName(), hasText(member.getPersonalInfo().getName().getNameOnPanel()));

        GuestAddress guestAddr = member.getPreferredAddress();
        Component panelAddr = custPanel.getAddress();

        verifyThat(panelAddr, hasText(guestAddr.getFormattedAddress()));
    }
}
