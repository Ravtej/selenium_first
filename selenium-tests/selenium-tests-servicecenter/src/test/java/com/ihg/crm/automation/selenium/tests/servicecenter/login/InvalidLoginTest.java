package com.ihg.crm.automation.selenium.tests.servicecenter.login;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.core.AnyOf.anyOf;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.pages.login.LoginErrorBox;
import com.ihg.automation.selenium.common.pages.login.SingleSignOn;
import com.ihg.crm.automation.selenium.tests.servicecenter.LoginLogout;

public class InvalidLoginTest extends LoginLogout
{
    private SingleSignOn login = new SingleSignOn();

    @Test
    public void submitEmptyFields()
    {
        openLoginPage();
        login.clickSubmit();
        LoginErrorBox errors = new LoginErrorBox();
        verifyThat(errors, hasText(LoginErrorBox.USER_ERROR_MESSAGE));
    }

    @DataProvider(name = "invalidCredentials")
    public Object[][] invalidCredentials()
    {
        return new Object[][] { { "username", user10T2.getPassword(), user10T2.getDomain(), "Invalid user" },
                { user10T2.getUserId(), "password", user10T2.getDomain(), "Invalid password" },
                { user10T2.getUserId(), user10T2.getPassword(), "AMER", "Invalid domain" } };
    }

    @Test(dataProvider = "invalidCredentials")
    public void submitInvalidCredentials(String username, String password, String domain, String description)
    {
        openLoginPage();
        login.submitLoginDetails(username, password, domain);
        verifyThat(new LoginErrorBox(), hasText(anyOf(is(LoginErrorBox.CREDENTIALS_MISMATCH_ERROR_MESSAGE),
                is(LoginErrorBox.CREDENTIALS_MISMATCH_NEW_ERROR_MESSAGE))));
    }
}
