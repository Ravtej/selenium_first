package com.ihg.crm.automation.selenium.tests.servicecenter.hotel;

import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyError;
import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyNoError;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasTextWithWaitAndFlush;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.isDisplayedWithWait;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.isHighlightedAsInvalid;
import static com.ihg.automation.selenium.matchers.component.GridSize.size;
import static com.ihg.automation.selenium.servicecenter.pages.Messages.HIGHLIGHTED_FIELDS_DID_NOT_PASS_VALIDATION;
import static org.hamcrest.core.AllOf.allOf;

import java.util.Map;

import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.common.DateUtils;
import com.ihg.automation.common.RandomUtils;
import com.ihg.automation.selenium.common.hotel.HotelNight;
import com.ihg.automation.selenium.common.hotel.HotelNightDetails;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.reward.RewardNight;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.servicecenter.pages.LeftPanel;
import com.ihg.automation.selenium.servicecenter.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.servicecenter.pages.event.rewardnight.RewardNightEventPage;
import com.ihg.automation.selenium.servicecenter.pages.hotel.freenight.FreeNightReimbursementPage;
import com.ihg.automation.selenium.servicecenter.pages.hotel.freenight.FreeNightReimbursementSearchGridRow;
import com.ihg.automation.selenium.servicecenter.pages.hotel.freenight.FreeNightReimbursementSearchGridRow.FreeNightReimbursementCell;
import com.ihg.automation.selenium.servicecenter.pages.hotel.freenight.FreeNightReimbursementSearchGridRowAdjustPanel;
import com.ihg.automation.selenium.servicecenter.pages.hotel.freenight.FreeNightReimbursementSearchRowContainer;
import com.ihg.automation.selenium.servicecenter.pages.unit.GoodwillPopUp;
import com.ihg.crm.automation.selenium.tests.servicecenter.LoginLogout;

public class FreeNightReimbursementOperationsTest extends LoginLogout
{
    private Member member = new Member();
    private FreeNightReimbursementPage reimbursementPage = new FreeNightReimbursementPage();
    private RewardNight rewardNight, rewardNight2, rewardNight3;
    private HotelNight hotelNight = new HotelNight();
    private HotelNightDetails hotelNightDetails = new HotelNightDetails();
    private FreeNightReimbursementSearchGridRow searchGridRow;
    private FreeNightReimbursementSearchGridRowAdjustPanel rowAdjustPanel;

    private static final DateTimeFormatter DB_TIME_FORMAT = DateTimeFormat.forPattern("YYYY-MM-dd");
    private static final String AVERAGE_DAILY_RATE = "99.99";

    private final static String HOTEL_BY_STATUS = "SELECT HLDX_CD FROM GROWTH_PRDHTL.HOTEL "
            + "WHERE FAC_NBR=(SELECT st.FACILITY_ID FROM FMDS.TMFSTAY st "
            + "LEFT JOIN FMDS.TMANCRT crt ON st.FACILITY_ID = crt.FACILITY_ID AND st.STAY_DATE = crt.STAY_DATE "
            + "WHERE st.PC_ACCTG_STAT_CD='%s' AND crt.AN_CERT_STAT_CD='%s' "
            + "AND (st.STAY_DATE > SYSDATE-30) AND ROWNUM < 2)";

    private final static String FREE_DATE_REQUEST = "SELECT" + " TO_CHAR(FREE_DATE, 'YYYY-MM-DD') AS FREE_DATE, "
            + " TO_CHAR(FREE_DATE - 1, 'YYYY-MM-DD') AS BOOK_START_DATE,"
            + " TO_CHAR(SYSDATE + 1, 'YYYY-MM-DD') AS BOOK_END_DATE,"
            + " htl.HTL_ID, htl.HLDX_CD, htl.FAC_NBR, htl.USED_CURR_CD, st.STAY_DATE "
            + "FROM (SELECT TRUNC(SYSDATE-1 - level, 'DD') AS FREE_DATE FROM dual CONNECT BY level <= 10) FREE_DATES"
            + " LEFT JOIN GROWTH_PRDHTL.HOTEL htl ON 1=1"
            + " LEFT JOIN FMDS.TMFSTAY st ON TRUNC(FREE_DATE, 'DD') = TRUNC(st.STAY_DATE, 'DD') AND htl.FAC_NBR = st.FACILITY_ID"
            + " LEFT JOIN CATLG.RN_PRCE_HTL_EXCP htl_excp ON htl.HTL_ID = htl_excp.HTL_ID WHERE 1=1"
            + " AND htl.HTL_ID IN (SELECT inn_htl.HTL_ID FROM GROWTH_PRDHTL.HOTEL inn_htl WHERE 1=1"
            + " AND inn_htl.HTL_STAT_ID = (SELECT HTL_STAT_ID FROM GROWTH_PRDHTL.HTL_STAT WHERE STAT_CD ='OPEN')"
            + " AND inn_htl.USED_CURR_CD IS NOT NULL AND inn_htl.USED_CURR_CD NOT IN ('USD') AND ROWNUM < 10)"
            + " AND st.STAY_DATE IS NULL AND htl_excp.AWD_KEY IS NOT NULL AND ROWNUM < 2";

    private final static String MIN_OCC = "SELECT * FROM (SELECT r.HIGH_OCC_PCT, r.EFF_STY_DT"
            + " FROM CATLG.RN_REIMB_HTL_EXCP_RULE r"
            + " WHERE r.HTL_ID = (SELECT h.HTL_ID FROM GROWTH_PRDHTL.HOTEL h WHERE h.HLDX_CD = '%s')"
            + " AND r.EFF_STY_DT <= TO_DATE('%s', 'DDMONYYYY') ORDER BY r.EFF_STY_DT DESC) WHERE ROWNUM < 2";

    private String getPaidHotel()
    {
        return jdbcTemplate.queryForMap(String.format(HOTEL_BY_STATUS, "R", "PAID")).get("HLDX_CD").toString();
    }

    private String getMinOCC(String hotel, String date)
    {
        String occ = String.format(MIN_OCC, hotel, DateUtils.convertDateTimeFormat(date, "ddMMMYY", "ddMMMYYYY"));
        Map<String, Object> map = jdbcTemplate.queryForMap(occ);

        return map.get("HIGH_OCC_PCT").toString();
    }

    private RewardNight createRewardNightForFreeDate()
    {
        RewardNight rewardNight = new RewardNight();

        rewardNight.setConfirmationNumber(RandomUtils.getRandomNumber(8, false));
        rewardNight.setNights("1");
        rewardNight.setNumberOfRooms("1");
        rewardNight.setRoomType("BBL");

        Map<String, Object> resultMap = jdbcTemplate.queryForMap(FREE_DATE_REQUEST);
        rewardNight.setHotel(resultMap.get("HLDX_CD").toString());
        rewardNight.setHotelCurrency(resultMap.get("USED_CURR_CD").toString());

        String freeDate = DB_TIME_FORMAT.parseLocalDate(resultMap.get("FREE_DATE").toString()).toString("ddMMMyy");
        rewardNight.setCheckIn(freeDate);
        rewardNight.setCheckOut(freeDate);

        new RewardNightEventPage().createRewardNight(rewardNight);

        return rewardNight;
    }

    @BeforeClass
    public void beforeClass()
    {

        member.setPersonalInfo(MemberPopulateHelper.getSimplePersonalInfo());
        member.addAddress(MemberPopulateHelper.getUnitedStatesAddress());
        member.addProgram(Program.RC);

        login();

        new EnrollmentPage().enroll(member);
        new GoodwillPopUp().goodwillPoints("50000");
        new GoodwillPopUp().goodwillPoints("50000");
        new GoodwillPopUp().goodwillPoints("50000");

        rewardNight = createRewardNightForFreeDate();

        hotelNight.setCurrencyCode(rewardNight.getHotelCurrency());
        hotelNight.setStayDate(rewardNight.getCheckIn());

        hotelNightDetails.setConfirmationNumber(rewardNight.getConfirmationNumber());
        hotelNightDetails.setMemberID(member.getRCProgramId());
        hotelNightDetails.setLastName(member.getName().getSurname());
        hotelNightDetails.setCertificateType("Reward Night");
        hotelNightDetails.setSuffix("1");
        hotelNightDetails.setStatus("INITIATED");
        hotelNightDetails.setCheckInDate(rewardNight.getCheckIn());

        rewardNight2 = createRewardNightForFreeDate();
        rewardNight3 = createRewardNightForFreeDate();

        new LeftPanel().goToNewSearch();

        reimbursementPage.goTo();
        verifyThat(reimbursementPage, isDisplayedWithWait());
    }

    @Test
    public void tryApprovePaidRequest()
    {
        reimbursementPage.search(getPaidHotel(), "PAID", null, null, null);
        tryApproveFirstRequest();
    }

    @Test(groups = { "firstNightProcessing" }, dependsOnMethods = { "tryApprovePaidRequest" })
    public void searchCreatedRewardNight()
    {
        reimbursementPage.searchByHotelAndDate(rewardNight.getHotel(), rewardNight.getCheckIn(), verifyNoError());
        verifyThat(reimbursementPage.getGrid(), size(1));
    }

    @Test(groups = { "firstNightProcessing" }, dependsOnMethods = { "searchCreatedRewardNight" }, priority = 11)
    public void verifyReimbursementRowWithDetails()
    {
        FreeNightReimbursementSearchGridRow row = reimbursementPage.getGrid().getRow(1);
        row.verify(hotelNight);

        FreeNightReimbursementSearchRowContainer container = row.expand(FreeNightReimbursementSearchRowContainer.class);
        container.getDetail(rewardNight.getConfirmationNumber()).verify(hotelNightDetails);
    }

    @Test(groups = { "firstNightProcessing" }, dependsOnMethods = { "searchCreatedRewardNight" }, priority = 11)
    public void tryApproveInitiatedRequest()
    {
        tryApproveFirstRequest();
    }

    @Test(groups = { "firstNightProcessing" }, dependsOnMethods = { "searchCreatedRewardNight" }, priority = 12)
    public void tryAdjustReimbursementRequestWithoutADR()
    {
        searchGridRow = reimbursementPage.getGrid().getRow(1);
        String minOcc = getMinOCC(rewardNight.getHotel(), rewardNight.getCheckIn());

        rowAdjustPanel = searchGridRow.openAdjustPanel();
        rowAdjustPanel.getActualOcc().type(minOcc);
        rowAdjustPanel.getSave().click(verifyError(HIGHLIGHTED_FIELDS_DID_NOT_PASS_VALIDATION));

        verifyThat(rowAdjustPanel.getActualAdr(), allOf(isDisplayedWithWait(), isHighlightedAsInvalid(true)));

        rowAdjustPanel.getCancel().click();
    }

    @Test(groups = { "firstNightProcessing" }, dependsOnMethods = { "searchCreatedRewardNight" }, priority = 13)
    public void adjustReimbursementRequestCancel()
    {
        searchGridRow = reimbursementPage.getGrid().getRow(1);

        rowAdjustPanel = searchGridRow.openAdjustPanel();
        rowAdjustPanel.populate("90.00", null);
        rowAdjustPanel.getCancel().click(verifyNoError());
        verifyThat(rowAdjustPanel, displayed(false));

        reimbursementPage.getGrid().getRow(1).verify(hotelNight);
    }

    @Test(groups = { "firstNightProcessing" }, dependsOnMethods = { "searchCreatedRewardNight" }, priority = 14)
    public void adjustReimbursementRequestSave()
    {
        searchGridRow = reimbursementPage.getGrid().getRow(1);
        rowAdjustPanel = searchGridRow.openAdjustPanel();
        rowAdjustPanel.populate("90.00", null);
        rowAdjustPanel.getSave().clickAndWait();
        verifyThat(rowAdjustPanel, displayed(false));

        searchGridRow.verifyAdjusted("ACCEPTED", "90.00", null);
    }

    @Test(groups = { "firstNightProcessing" }, dependsOnMethods = { "adjustReimbursementRequestSave" }, priority = 14)
    public void tryApproveAcceptedRequest()
    {
        tryApproveFirstRequest();
    }

    @Test(groups = { "secondNightProcessing" }, dependsOnGroups = { "firstNightProcessing" }, alwaysRun = true)
    public void adjustReimbursementRequestSetADRPending()
    {
        String minOcc = getMinOCC(rewardNight2.getHotel(), rewardNight2.getCheckIn()) + ".00";

        reimbursementPage.searchByHotelAndDate(rewardNight2.getHotel(), rewardNight2.getCheckIn());
        searchGridRow = reimbursementPage.getGrid().getRow(1);

        rowAdjustPanel = searchGridRow.openAdjustPanel();
        rowAdjustPanel.populate(minOcc, AVERAGE_DAILY_RATE);
        rowAdjustPanel.getSave().clickAndWait();

        searchGridRow.verifyAdjusted("PENDING", minOcc, AVERAGE_DAILY_RATE);
    }

    @Test(groups = { "secondNightProcessing" }, dependsOnMethods = { "adjustReimbursementRequestSetADRPending" })
    public void approvePendingRequest()
    {
        searchGridRow = reimbursementPage.getGrid().getRow(1);
        searchGridRow.check();
        reimbursementPage.getButtonBar().getApproveSelected().click();

        verifyThat(searchGridRow.getCell(FreeNightReimbursementCell.STATUS), hasTextWithWaitAndFlush("ACCEPTED"));
    }

    @Test(dependsOnGroups = { "firstNightProcessing", "secondNightProcessing" }, alwaysRun = true)
    public void adjustReimbursementRequestSetADRAccepted()
    {
        String minOcc = getMinOCC(rewardNight3.getHotel(), rewardNight3.getCheckIn()) + ".00";

        reimbursementPage.searchByHotelAndDate(rewardNight3.getHotel(), rewardNight3.getCheckIn());
        searchGridRow = reimbursementPage.getGrid().getRow(1);

        rowAdjustPanel = searchGridRow.openAdjustPanel();
        rowAdjustPanel.populate(minOcc, AVERAGE_DAILY_RATE);
        rowAdjustPanel.getSaveAndApprove().clickAndWait();

        searchGridRow.verifyAdjusted("ACCEPTED", minOcc, AVERAGE_DAILY_RATE);
    }

    private void tryApproveFirstRequest()
    {
        searchGridRow = reimbursementPage.getGrid().getRow(1);
        searchGridRow.check();
        reimbursementPage.getButtonBar().getApproveSelected()
                .click(verifyError("There are no selected records that can be approved"));
    }

}
