package com.ihg.crm.automation.selenium.tests.servicecenter.programinfo;

import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifySuccess;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.Constant.NOT_AVAILABLE;
import static com.ihg.automation.selenium.common.pages.program.EmployeeRateEligibility.EMPLOYEE_RATE_ELIGIBILITY_REMOVED;
import static com.ihg.automation.selenium.common.pages.program.EmployeeRateEligibility.EMPLOYEE_RATE_ELIGIBILITY_SAVED;
import static com.ihg.automation.selenium.matchers.component.HasSelectItems.hasSelectItems;

import java.util.ArrayList;
import java.util.Arrays;

import javax.annotation.Resource;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.common.DateUtils;
import com.ihg.automation.selenium.common.UI;
import com.ihg.automation.selenium.common.User;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.pages.program.EmployeeRateEligibility;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.servicecenter.pages.LeftPanel;
import com.ihg.automation.selenium.servicecenter.pages.Role;
import com.ihg.automation.selenium.servicecenter.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.RewardClubPage;
import com.ihg.crm.automation.selenium.tests.servicecenter.LoginLogout;

public class EmployeeRateEligibilityTest extends LoginLogout
{
    @Resource(name = "user2HT2")
    protected User user;

    private Member member;
    private RewardClubPage rewardClubPage = new RewardClubPage();
    private EmployeeRateEligibility employeeRateEligibility;

    private String RATE_LABEL_YES = "Yes";
    private String RATE_LABEL_NO = "No";
    private static final String DOES_NOT_EXPIRE = "Does not expire";
    private static final String EXPIRES = DateUtils.getFormattedDate();
    private static final String CHANNEL = UI.SC.getChannel();

    private static String RETIREE = "RETIREE";
    public static final String UKCRO = "UKCRO";
    public static final String HUMAN_RESOURCES = "HUMAN RESOURCES";
    private ArrayList<String> expectedListContent = new ArrayList<String>(
            Arrays.asList(RETIREE, UKCRO, HUMAN_RESOURCES, "ASIA PARTNER RATE", ""));

    @BeforeClass
    public void beforeClass()
    {
        member = new Member();
        member.addProgram(Program.RC);
        member.setPersonalInfo(MemberPopulateHelper.getSimplePersonalInfo());
        member.addAddress(MemberPopulateHelper.getUnitedStatesAddress());

        login(user, Role.CORPORATE_OFFICE_HUMAN_RESOURCES);

        new EnrollmentPage().enroll(member);
    }

    @Test()
    public void verifyEmployeeRateEligibilityInViewMode()
    {
        rewardClubPage.goTo();
        employeeRateEligibility = rewardClubPage.getEmployeeRateEligibility();

        employeeRateEligibility.verifyInViewMode(RATE_LABEL_NO, NOT_AVAILABLE, NOT_AVAILABLE, NOT_AVAILABLE);
    }

    @Test(dependsOnMethods = { "verifyEmployeeRateEligibilityInViewMode" }, alwaysRun = true)
    public void verifyEmployeeRateEligibilityInEditMode()
    {
        employeeRateEligibility.clickEdit();

        employeeRateEligibility.verifyFieldsDisplayed(false);
        verifyThat(employeeRateEligibility.getLocation(), hasSelectItems(expectedListContent));
    }

    @Test(dependsOnMethods = { "verifyEmployeeRateEligibilityInEditMode" }, alwaysRun = true)
    public void populateEmployeeRateEligibility()
    {
        employeeRateEligibility.getEligibleForEmployeeRate().check();
        employeeRateEligibility.verifyFieldsDisplayed(true);

        employeeRateEligibility.getExpires().type(EXPIRES);
        employeeRateEligibility.getLocation().select(RETIREE);
        employeeRateEligibility.clickSave(verifySuccess(EMPLOYEE_RATE_ELIGIBILITY_SAVED));
    }

    @Test(dependsOnMethods = { "populateEmployeeRateEligibility" }, alwaysRun = true)
    public void verifyEmployeeRateEligibility()
    {
        employeeRateEligibility.verifyInViewMode(RATE_LABEL_YES, EXPIRES, RETIREE, CHANNEL);
    }

    @Test(dependsOnMethods = { "verifyEmployeeRateEligibility" }, alwaysRun = true)
    public void reopenMember()
    {
        new LeftPanel().reOpenMemberProfile(member);
    }

    @Test(dependsOnMethods = { "reopenMember" }, alwaysRun = true)
    public void verifySavedEmployeeEligibilityDetails()
    {
        rewardClubPage.goTo();
        employeeRateEligibility = rewardClubPage.getEmployeeRateEligibility();
        employeeRateEligibility.verifyInViewMode(RATE_LABEL_YES, EXPIRES, RETIREE, CHANNEL);
    }

    @Test(dependsOnMethods = { "verifySavedEmployeeEligibilityDetails" }, alwaysRun = true)
    public void uncheckRate()
    {
        employeeRateEligibility.clickEdit();
        employeeRateEligibility.getEligibleForEmployeeRate().uncheck();
        employeeRateEligibility.verifyFieldsDisplayed(false);
        employeeRateEligibility.clickSave(verifySuccess(EMPLOYEE_RATE_ELIGIBILITY_REMOVED));
    }

    @Test(dependsOnMethods = { "uncheckRate" }, alwaysRun = true)
    public void verifySavedEmployeeEligibilityDetailsAfterUncheck()
    {
        employeeRateEligibility.verifyInViewMode(RATE_LABEL_NO, NOT_AVAILABLE, NOT_AVAILABLE, NOT_AVAILABLE);
    }

    @Test(dependsOnMethods = { "verifySavedEmployeeEligibilityDetailsAfterUncheck" }, alwaysRun = true)
    public void populateEligibilityWithoutExpires()
    {
        employeeRateEligibility.clickEdit();
        employeeRateEligibility.getEligibleForEmployeeRate().check();
        employeeRateEligibility.getLocation().select(UKCRO);
        employeeRateEligibility.clickSave(verifySuccess(EMPLOYEE_RATE_ELIGIBILITY_SAVED));
    }

    @Test(dependsOnMethods = { "populateEligibilityWithoutExpires" }, alwaysRun = true)
    public void verifyEligibilityWithoutExpires()
    {
        employeeRateEligibility.verifyInViewMode(RATE_LABEL_YES, DOES_NOT_EXPIRE, UKCRO, CHANNEL);
    }
}
