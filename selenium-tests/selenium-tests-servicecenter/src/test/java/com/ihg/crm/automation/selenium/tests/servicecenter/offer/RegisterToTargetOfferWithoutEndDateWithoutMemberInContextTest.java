package com.ihg.crm.automation.selenium.tests.servicecenter.offer;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.Constant.NOT_AVAILABLE;
import static com.ihg.automation.selenium.matchers.component.IsDisplayed.displayed;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.servicecenter.pages.LeftPanel;
import com.ihg.automation.selenium.servicecenter.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.servicecenter.pages.event.offer.OfferEventPage;
import com.ihg.automation.selenium.servicecenter.pages.offer.OfferRegistrationPopUp;

@Test(groups = { "targetOfferTest" })
public class RegisterToTargetOfferWithoutEndDateWithoutMemberInContextTest extends TargetingOfferCommon
{
    private Member member = new Member();

    @BeforeClass
    public void beforeClass()
    {
        member.setPersonalInfo(MemberPopulateHelper.getSimplePersonalInfo());
        member.addAddress(MemberPopulateHelper.getIndonesiaAddress());
        member.addProgram(Program.RC);

        login();

        new EnrollmentPage().enroll(member);
        new LeftPanel().goToNewSearch();

        insert(TARGET_OFFER_ID, member, null);
    }

    @Test
    public void registerToOffer()
    {
        OfferRegistrationPopUp offerRegistrationPopUp = new OfferRegistrationPopUp();
        offerRegistrationPopUp.registerToTargetingOffer(member, TARGET_OFFER_ID, NOT_AVAILABLE);
    }

    @Test(dependsOnMethods = { "registerToOffer" })
    public void verifyOffer()
    {
        verifyTargetExpiry(NOT_AVAILABLE);
        OfferEventPage offerPage = new OfferEventPage();
        offerPage.getButtonBar().clickSearch();
        verifyThat(offerPage.getMemberOffersGrid().getRowByOfferCode(TARGET_OFFER_ID), displayed(true));
    }
}
