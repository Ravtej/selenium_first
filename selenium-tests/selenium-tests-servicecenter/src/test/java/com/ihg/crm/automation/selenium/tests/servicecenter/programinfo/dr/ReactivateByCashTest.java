package com.ihg.crm.automation.selenium.tests.servicecenter.programinfo.dr;

import static com.ihg.automation.common.DateUtils.getStringToDate;
import static com.ihg.automation.selenium.common.event.EventTransactionType.Membership.MEMBERSHIP_OPEN;
import static com.ihg.automation.selenium.common.event.EventTransactionType.Order.ORDER_SYSTEM;
import static com.ihg.automation.selenium.common.member.DiningRewardAward.FUL_WELCOME_TO_PLATINUM;
import static com.ihg.automation.selenium.common.member.MemberJdbcUtils.UPDATE_EXP;
import static com.ihg.automation.selenium.common.types.program.Program.DR;

import org.springframework.beans.factory.annotation.Value;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.common.DateUtils;
import com.ihg.automation.selenium.common.catalog.CatalogItem;
import com.ihg.automation.selenium.common.event.AllEventsRow;
import com.ihg.automation.selenium.common.event.AllEventsRowConverter;
import com.ihg.automation.selenium.common.member.DiningRewardAward;
import com.ihg.automation.selenium.common.member.MemberJdbcUtils;
import com.ihg.automation.selenium.common.order.Order;
import com.ihg.automation.selenium.common.order.OrderFactory;
import com.ihg.automation.selenium.common.payment.CashPayment;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.common.types.program.RewardClubLevel;
import com.ihg.automation.selenium.gwt.components.Mode;
import com.ihg.automation.selenium.servicecenter.pages.GuestSearch;
import com.ihg.automation.selenium.servicecenter.pages.Role;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsGridRow;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsPage;
import com.ihg.automation.selenium.servicecenter.pages.event.enroll.MembershipDetails;
import com.ihg.automation.selenium.servicecenter.pages.event.enroll.MembershipGridRowContainer;
import com.ihg.automation.selenium.servicecenter.pages.event.order.OrderSystemDetailsGridRowContainer;
import com.ihg.automation.selenium.servicecenter.pages.programs.ProgramPageBase.EnrollmentDetails;
import com.ihg.automation.selenium.servicecenter.pages.programs.dr.DiningRewardsPage;

public class ReactivateByCashTest extends DRCommon
{
    @Value("${memberReactivateByCash}")
    protected String memberReactivateByCash;

    @Value("${memberReactivateByCash.date}")
    protected String date;

    protected static final CatalogItem ENROLLMENT_AWARD = DiningRewardAward.ENROLL_RMB2388;
    private String enrollDate;
    private AllEventsPage allEventsPage = new AllEventsPage();
    private CashPayment cashPaymentItem;

    @BeforeClass
    public void beforeClass()
    {
        MemberJdbcUtils.update(jdbcTemplateUpdate, UPDATE_EXP, memberReactivateByCash, DR, 13);

        enrollDate = getStringToDate(date).toString(DateUtils.DATE_FORMAT);
        cashPaymentItem = new CashPayment(ENROLLMENT_AWARD, "test name", "test location", enrollDate, "12345");

        login(user15U, Role.AGENT_TIER2);

        new GuestSearch().byMemberNumber(memberReactivateByCash);
    }

    @Test
    public void verifyRCProgramInformation()
    {
        verifyRCProgramInformationDetails(RewardClubLevel.PLTN);
    }

    @Test(dependsOnMethods = "verifyRCProgramInformation", alwaysRun = true)
    public void verifyProgramSummaryDetails()
    {
        verifyProgramSummary(memberReactivateByCash, 13);
    }

    @Test(dependsOnMethods = "verifyProgramSummaryDetails", alwaysRun = true)
    public void verifyEnrollmentDetails()
    {
        DiningRewardsPage diningRewardsPage = new DiningRewardsPage();
        EnrollmentDetails enrollmentDetails = diningRewardsPage.getEnrollmentDetails();
        enrollmentDetails.getPaymentDetails().verifyCommonDetails(cashPaymentItem);
        enrollmentDetails.verifyScSource(helper);
    }

    @Test(dependsOnMethods = "verifyEnrollmentDetails", alwaysRun = true)
    public void verifyOrderSystemEventDetails()
    {
        Order systemOrder = OrderFactory.getSystemOrder(FUL_WELCOME_TO_PLATINUM, null);
        systemOrder.setTransactionDate(enrollDate);
        systemOrder.getOrderItems().get(0).setDeliveryStatus(null);

        allEventsPage = new AllEventsPage();
        allEventsPage.goTo();
        AllEventsGridRow orderSystemRow = allEventsPage.getGrid().getRow(ORDER_SYSTEM);

        orderSystemRow.verify(AllEventsRowConverter.convert(systemOrder));
        orderSystemRow.expand(OrderSystemDetailsGridRowContainer.class).verifyWithShippingAsOnLeftPanel(systemOrder);
    }

    @Test(dependsOnMethods = "verifyOrderSystemEventDetails", alwaysRun = true)
    public void verifyMembershipRenewalEventDetails()
    {
        AllEventsRow membershipRowData = new AllEventsRow(enrollDate, MEMBERSHIP_OPEN, Program.DR.getCode(), "", "");

        AllEventsGridRow membershipRenewalRow = allEventsPage.getGrid().getRow(MEMBERSHIP_OPEN);
        membershipRenewalRow.verify(membershipRowData);

        MembershipGridRowContainer container = membershipRenewalRow.expand(MembershipGridRowContainer.class);
        MembershipDetails membershipDetails = container.getMembershipDetails();
        membershipDetails.getPaymentDetails().verify(cashPaymentItem, Mode.VIEW);
        membershipDetails.verifyScSource(helper);
    }
}
