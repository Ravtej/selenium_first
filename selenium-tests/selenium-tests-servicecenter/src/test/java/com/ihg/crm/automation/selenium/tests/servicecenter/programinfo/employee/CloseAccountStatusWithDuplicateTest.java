package com.ihg.crm.automation.selenium.tests.servicecenter.programinfo.employee;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.event.EventTransactionType.Membership.MEMBERSHIP_CLOSE;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.isRedColor;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static com.ihg.automation.selenium.matchers.component.ModeMatcher.inView;
import static com.ihg.automation.selenium.matchers.component.ViewableMatcher.hasTextInView;
import static org.apache.commons.lang.StringUtils.EMPTY;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.common.DateUtils;
import com.ihg.automation.selenium.common.event.AllEventsRow;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.common.types.program.ProgramStatus;
import com.ihg.automation.selenium.common.types.program.ProgramStatusReason;
import com.ihg.automation.selenium.servicecenter.pages.LeftPanel;
import com.ihg.automation.selenium.servicecenter.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsGridRow;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsPage;
import com.ihg.automation.selenium.servicecenter.pages.event.status.ProfileStatusGridRowContainer;
import com.ihg.automation.selenium.servicecenter.pages.programs.EmployeePage;
import com.ihg.automation.selenium.servicecenter.pages.programs.ProgramSummary;
import com.ihg.crm.automation.selenium.tests.servicecenter.LoginLogout;

public class CloseAccountStatusWithDuplicateTest extends LoginLogout
{
    private Member member = new Member();
    private EmployeePage employeePage = new EmployeePage();
    private AllEventsPage allEventsPage = new AllEventsPage();
    private ProgramSummary summary;
    private AllEventsRow membershipCloseRow = new AllEventsRow(DateUtils.getFormattedDate(), MEMBERSHIP_CLOSE,
            Program.EMP.getCode(), EMPTY, EMPTY);

    @BeforeClass
    public void beforeClass()
    {
        member.setPersonalInfo(MemberPopulateHelper.getSimplePersonalInfo());
        member.addAddress(MemberPopulateHelper.getUnitedStatesAddress());
        member.addProgram(Program.EMP);

        login();

        member = new EnrollmentPage().enroll(member);
    }

    @Test
    public void verifyInitialStatus()
    {
        employeePage.goTo();
        summary = employeePage.getSummary();
        verifyThat(summary.getStatus(), inView(hasText(ProgramStatus.OPEN)));
    }

    @Test(dependsOnMethods = { "verifyInitialStatus" }, alwaysRun = true)
    public void setStatusToClosedWithDuplicate()
    {
        employeePage.goTo();
        summary.setClosedStatus(ProgramStatusReason.DUPLICATE);
    }

    @Test(dependsOnMethods = { "setStatusToClosedWithDuplicate" }, alwaysRun = true)
    public void verifyLeftPanel()
    {
        LeftPanel panel = new LeftPanel();
        panel.getCustomerInfoPanel().verifyStatus(ProgramStatus.CLOSED);

        verifyThat(panel.getProgramInfoPanel().getPrograms().getProgram(Program.EMP), isRedColor());
    }

    @Test(dependsOnMethods = { "verifyLeftPanel" }, alwaysRun = true)
    public void verifyClosedStatusOnEmployeeProgramPage()
    {
        employeePage.goTo();
        verifyThat(summary.getStatus(), hasTextInView(ProgramStatus.CLOSED));
        verifyThat(summary.getStatusReason(), hasTextInView(ProgramStatusReason.DUPLICATE));
    }

    @Test(dependsOnMethods = { "verifyClosedStatusOnEmployeeProgramPage" }, alwaysRun = true)
    public void verifyAllEventsMembershipGridRow()
    {
        allEventsPage.goTo();
        AllEventsGridRow membershipRow = allEventsPage.getGrid().getRow(membershipCloseRow.getTransType(),
                membershipCloseRow.getDetail());
        membershipRow.verify(membershipCloseRow);
    }

    @Test(dependsOnMethods = { "verifyAllEventsMembershipGridRow" }, alwaysRun = true)
    public void verifyAllEventsMembershipGridRowDetails()
    {
        allEventsPage.goTo();
        AllEventsGridRow row = allEventsPage.getGrid().getRow(membershipCloseRow.getTransType(),
                membershipCloseRow.getDetail());
        row.expand(ProfileStatusGridRowContainer.class).verify(helper, ProgramStatusReason.DUPLICATE);
    }
}
