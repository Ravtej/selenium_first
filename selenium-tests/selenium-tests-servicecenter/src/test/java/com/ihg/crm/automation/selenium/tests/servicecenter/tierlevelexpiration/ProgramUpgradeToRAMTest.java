package com.ihg.crm.automation.selenium.tests.servicecenter.tierlevelexpiration;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.types.program.AmbassadorLevel.AMB;
import static com.ihg.automation.selenium.common.types.program.AmbassadorLevel.RAM;
import static com.ihg.automation.selenium.common.types.program.ProgramStatus.OPEN;
import static com.ihg.automation.selenium.common.types.program.RewardClubLevel.GOLD;
import static com.ihg.automation.selenium.common.types.program.RewardClubLevel.SPRE;
import static com.ihg.automation.selenium.matchers.component.GridSize.size;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static org.hamcrest.Matchers.not;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.member.AmbassadorOfferCode;
import com.ihg.automation.selenium.common.member.AmbassadorUtils;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.stay.Stay;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.servicecenter.pages.LeftPanel;
import com.ihg.automation.selenium.servicecenter.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.servicecenter.pages.event.stay.StayEventsPage;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.RewardClubPage;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.RewardClubSummary;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.TierLevelBenefitsDetailsPopUp;
import com.ihg.crm.automation.selenium.tests.servicecenter.LoginLogout;

public class ProgramUpgradeToRAMTest extends LoginLogout
{
    @BeforeClass
    public void beforeClass()
    {
        Member member = new Member();
        member.setPersonalInfo(MemberPopulateHelper.getSimplePersonalInfo());
        member.addAddress(MemberPopulateHelper.getUnitedStatesAddress());
        member.addProgram(Program.AMB);
        member.setAmbassadorOfferCode(AmbassadorOfferCode.CHSRO);
        member.setAmbassadorAmount(AmbassadorUtils.AMBASSADOR_COMP_PROMO_$0);

        Stay firstStay = new Stay();
        firstStay.setHotelCode("AHBHA");
        firstStay.setCheckInOutByNights(10);
        firstStay.setAvgRoomRate(100.00);
        firstStay.setRateCode("Test");

        Stay secondStay = new Stay();
        secondStay = firstStay.clone();
        secondStay.setHotelCode("ALAHA");
        secondStay.setCheckInOutByNights(5, 10);

        Stay thirdStay = new Stay();
        thirdStay = firstStay.clone();
        thirdStay.setHotelCode("AMMHA");
        thirdStay.setCheckInOutByNights(5, 15);

        Stay fourthStay = new Stay();
        fourthStay = firstStay.clone();
        fourthStay.setHotelCode("ATLCP");
        fourthStay.setCheckInOutByNights(40, 20);

        login();
        new EnrollmentPage().enroll(member);

        LeftPanel leftPanel = new LeftPanel();
        verifyThat(leftPanel.getProgramInfoPanel().getPrograms().getProgram(Program.AMB).getLevelCode(),
                hasText(AMB.getCode()));

        leftPanel.verifyRCLevel(GOLD);

        StayEventsPage stayEventsPage = new StayEventsPage();
        stayEventsPage.createStay(firstStay);
        stayEventsPage.createStay(secondStay);
        stayEventsPage.createStay(thirdStay);
        stayEventsPage.createStay(fourthStay);
    }

    @Test
    public void verifyRAMlevel()
    {
        verifyThat(new LeftPanel().getProgramInfoPanel().getPrograms().getProgram(Program.AMB).getLevelCode(),
                hasText(RAM.getCode()));
    }

    @Test
    public void verifyTierLevel()
    {
        RewardClubPage rewardClubPage = new RewardClubPage();
        rewardClubPage.goTo();

        RewardClubSummary summary = rewardClubPage.getSummary();
        summary.verify(OPEN, SPRE, 2);

        TierLevelBenefitsDetailsPopUp tierLevelBenefitsDetailsPopUp = summary.openTierLevelBenefitsDetailsPopUp();
        verifyThat(tierLevelBenefitsDetailsPopUp.getBenefitsGrid(), size(not(0)));
        tierLevelBenefitsDetailsPopUp.close();
    }
}
