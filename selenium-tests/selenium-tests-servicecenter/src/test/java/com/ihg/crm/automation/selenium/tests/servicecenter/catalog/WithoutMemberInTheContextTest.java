package com.ihg.crm.automation.selenium.tests.servicecenter.catalog;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasDefault;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasEmptyText;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasTextAsDate;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.isHighlightedAsInvalid;
import static com.ihg.automation.selenium.matchers.component.GridSize.size;
import static com.ihg.automation.selenium.matchers.date.IsBeforeDate.isBeforeToday;
import static org.hamcrest.number.OrderingComparison.greaterThanOrEqualTo;

import org.openqa.selenium.support.Color;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.google.common.collect.ImmutableList;
import com.ihg.automation.selenium.common.catalog.CatalogItem;
import com.ihg.automation.selenium.gwt.components.grid.GridCell;
import com.ihg.automation.selenium.servicecenter.pages.catalog.CatalogItemsGrid;
import com.ihg.automation.selenium.servicecenter.pages.catalog.CatalogItemsGridRow.CatalogItemsGridCell;
import com.ihg.automation.selenium.servicecenter.pages.catalog.CatalogPage;
import com.ihg.automation.selenium.servicecenter.pages.catalog.CatalogSearch;

public class WithoutMemberInTheContextTest extends CatalogCommon
{
    private CatalogPage catalogPage = new CatalogPage();
    private CatalogSearch catalogSearch;
    private CatalogItemsGrid catalogGrid;
    private final static CatalogItem CATALOG_ITEM = CatalogItem.builder("F1101", "DELAYED FULFILLMENT CLUB KIT")
            .vendor("SOURCE ONE").startDate("01Jan00").endDate("31Dec99").status("Active").orderLimit("1")
            .loyaltyUnitCost("0").regions(ImmutableList.of("UNITED STATES", "CANADA", "MSAC", "EMEA", "ASIA PACIFIC"))
            .countries(ImmutableList.of("All")).tierLevels(ImmutableList.of("All")).build();
    private int expiredAmount;
    private int amount;
    private final static String GREY_COLOR = "#909090";

    @BeforeClass
    public void beforeClass()
    {
        login();
    }

    @Test
    public void openCatalog()
    {
        catalogPage.goTo();
        catalogSearch = catalogPage.getSearchFields();

        verifyThat(catalogSearch.getCatalogID(), displayed(true));
        verifyThat(catalogSearch.getItemID(), displayed(true));
        verifyThat(catalogSearch.getItemName(), displayed(true));
        verifyThat(catalogSearch.getLoyaltyUnitCost().getLoyaltyUnitCostMin(), displayed(true));
        verifyThat(catalogSearch.getLoyaltyUnitCost().getLoyaltyUnitCostMax(), displayed(true));
    }

    @Test(dependsOnMethods = { "openCatalog" }, alwaysRun = true)
    public void tryEmptySearch()
    {
        catalogPage.getButtonBar().clickSearch();

        verifyThat(catalogSearch.getCatalogID(), isHighlightedAsInvalid(true));
        verifyThat(catalogSearch.getItemID(), isHighlightedAsInvalid(true));
        verifyThat(catalogSearch.getItemName(), isHighlightedAsInvalid(true));
        verifyThat(catalogSearch.getLoyaltyUnitCost().getLoyaltyUnitCostMin(), isHighlightedAsInvalid(true));
        verifyThat(catalogSearch.getLoyaltyUnitCost().getLoyaltyUnitCostMax(), isHighlightedAsInvalid(true));
    }

    @Test(dependsOnMethods = { "tryEmptySearch" }, alwaysRun = true)
    public void searchByCatalogId()
    {
        verifySearchByCatalogId("FHTEL");
    }

    @Test(dependsOnMethods = { "searchByCatalogId" }, alwaysRun = true)
    public void verifyClearSearchCriteria()
    {
        clearSearchCriteria();

        catalogGrid = catalogPage.getCatalogGrid();
        verifyThat(catalogGrid, size(0));

        verifyThat(catalogSearch.getLoyaltyUnitCost().getLoyaltyUnitCostMax(), hasEmptyText());
        verifyThat(catalogSearch.getCatalogID(), hasDefault());
        verifyThat(catalogSearch.getItemID(), hasEmptyText());
        verifyThat(catalogSearch.getItemName(), hasEmptyText());
        verifyThat(catalogSearch.getLoyaltyUnitCost().getLoyaltyUnitCostMin(), hasEmptyText());
    }

    @Test(dependsOnMethods = { "verifyClearSearchCriteria" }, alwaysRun = true)
    public void searchByItemId()
    {
        verifySearchByItemId("PS208");
    }

    @Test(dependsOnMethods = { "searchByItemId" }, alwaysRun = true)
    public void searchByItemName()
    {
        verifySearchByItemName("ipod");
    }

    @Test(dependsOnMethods = { "searchByItemName" }, alwaysRun = true)
    public void searchByLoyaltyUnitCostMin()
    {
        verifySearchByLoyaltyUnitCostMin("1000");
    }

    @Test(dependsOnMethods = { "searchByLoyaltyUnitCostMin" }, alwaysRun = true)
    public void searchByLoyaltyUnitCostMax()
    {
        verifySearchByLoyaltyUnitCostMax("5000");
    }

    @Test(dependsOnMethods = { "searchByLoyaltyUnitCostMax" }, alwaysRun = true)
    public void searchByLoyaltyUnitCost()
    {
        catalogPage.goTo();
        verifySearchByLoyaltyUnitCost("1000", "5000");
    }

    @Test(dependsOnMethods = { "searchByLoyaltyUnitCost" }, alwaysRun = true)
    public void verifyExpiredItems()
    {
        catalogPage.search(null, null, null, "600", "1000");

        catalogGrid = catalogPage.getCatalogGrid();

        catalogSearch.getDisplayExpiredItems().check();
        catalogPage.getButtonBar().clickSearch();

        for (int i = 1; i <= catalogGrid.getRowCount(); i++)
        {
            GridCell catalogCell = catalogGrid.getRow(i).getCell(CatalogItemsGridCell.END_DATE);
            Color color = Color.fromString(catalogCell.getCssValue("color"));

            if (color.asHex().equals(GREY_COLOR) && catalogCell.getCssValue("font-style").equals("italic"))
            {
                verifyThat(catalogCell, hasTextAsDate("ddMMMYY", isBeforeToday()));

                expiredAmount++;
            }

            else
            {
                amount++;
            }
        }
        verifyThat(catalogGrid, expiredAmount, greaterThanOrEqualTo(1),
                "Assert that amount of expired dates is greater than 0");
        verifyThat(catalogGrid, amount, greaterThanOrEqualTo(1),
                "Assert that amount of not expired dates is greater than 0");
    }

    @Test(dependsOnMethods = { "verifyExpiredItems" }, alwaysRun = true)
    public void verifyItemDetailsGrid()
    {
        catalogPage.searchByItemId(CATALOG_ITEM);
        catalogGrid.getRow(1).verify(CATALOG_ITEM);
    }

    @Test(dependsOnMethods = { "verifyItemDetailsGrid" }, alwaysRun = true)
    public void verifyItemDetailsPopUp()
    {
        verifyCatalogItemDetailsPopUp(CATALOG_ITEM);
    }
}
