package com.ihg.crm.automation.selenium.tests.servicecenter.commpref;

import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyError;
import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyNoError;
import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifySuccess;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.communication.CommunicationPreferencesConstants.CANNOT_SAVE_PREFERENCES;
import static com.ihg.automation.selenium.common.communication.CommunicationPreferencesConstants.SUCCESS_MESSAGE_SC;
import static com.ihg.automation.selenium.matchers.component.IsDisplayed.displayed;
import static com.ihg.automation.selenium.matchers.component.IsInvalid.isHighlightedAsInvalid;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.member.GuestEmail;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.personal.EmailContact;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.gwt.components.dialog.WarningDialog;
import com.ihg.automation.selenium.servicecenter.pages.communication.CommunicationPreferences;
import com.ihg.automation.selenium.servicecenter.pages.communication.CommunicationPreferencesPage;
import com.ihg.automation.selenium.servicecenter.pages.communication.CustomerServiceEmail;
import com.ihg.automation.selenium.servicecenter.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.servicecenter.pages.personal.PersonalInfoPage;
import com.ihg.crm.automation.selenium.tests.servicecenter.LoginLogout;

public class MarkEmailAsInvalidTest extends LoginLogout
{
    private Member member = new Member();
    private EnrollmentPage enrollmentPage = new EnrollmentPage();

    @BeforeClass
    public void beforeClass()
    {
        GuestEmail guestEmail = MemberPopulateHelper.getRandomEmail();

        member.addProgram(Program.RC);
        member.setPersonalInfo(MemberPopulateHelper.getSimplePersonalInfo());
        member.addAddress(MemberPopulateHelper.getUnitedStatesAddress());
        member.addEmail(guestEmail);
        member.addPhone(MemberPopulateHelper.getRandomPhone());

        login();
        enrollmentPage.enroll(member);
    }

    @Test
    public void selectEmailAsContact()
    {
        CommunicationPreferencesPage commPrefPage = new CommunicationPreferencesPage();
        commPrefPage.goTo();
        CommunicationPreferences commPrefs = commPrefPage.getCommunicationPreferences();
        commPrefs.clickEdit(verifyNoError());

        CustomerServiceEmail email = commPrefs.getCustomerService().getEmail();
        email.getEmailCheckBox().check();

        email.getEmail().select(member.getPreferredEmail().getEmail());

        commPrefs.getMarketingPreferences().getSubscribeAllLink().click();
        commPrefs.clickSave(verifySuccess(SUCCESS_MESSAGE_SC));
        verifyThat(commPrefs.getMarketingPreferences().getInvalidEmailImage(), displayed(false));
    }

    @Test(dependsOnMethods = { "selectEmailAsContact" }, alwaysRun = true)
    public void markEmailAsInvalid()
    {
        PersonalInfoPage personalInfoPage = new PersonalInfoPage();
        personalInfoPage.goTo();
        EmailContact emailContact = personalInfoPage.getEmailList().getContact();
        emailContact.clickEdit();
        emailContact.getInvalid().check();
        emailContact.clickSave();

        String INVALID_CONTACT_SUCCESS_SAVE = "Contact info has been successfully saved. "
                + "The Contact flagged as Invalid is used in Marketing Preferences and Customer Service Preferences.\n"
                + "Please update Marketing Preferences and Customer Service Preferences at Communication Preferences tab.";

        new WarningDialog().clickOk(verifyNoError(), verifySuccess(INVALID_CONTACT_SUCCESS_SAVE));
    }

    @Test(dependsOnMethods = { "markEmailAsInvalid" }, alwaysRun = true)
    public void verifyCommPrefWithInvalidEmail()
    {
        CommunicationPreferencesPage commPrefPage = new CommunicationPreferencesPage();
        commPrefPage.goTo();
        CommunicationPreferences commPrefs = commPrefPage.getCommunicationPreferences();
        commPrefs.clickEdit(verifyNoError());

        verifyThat(commPrefs.getMarketingPreferences().getInvalidEmailImage(), displayed(true));

        verifyThat(commPrefs.getCustomerService().getEmail().getEmail(), isHighlightedAsInvalid(true));

        commPrefs.clickSave(verifyError(CANNOT_SAVE_PREFERENCES));
    }
}
