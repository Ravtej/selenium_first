package com.ihg.crm.automation.selenium.tests.servicecenter.account;

import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyNoError;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.types.program.ProgramStatusReason.FROZEN;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasDefault;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasEmptyText;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.common.RandomUtils;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.servicecenter.pages.LeftPanel;
import com.ihg.automation.selenium.servicecenter.pages.account.AccountInfoPage;
import com.ihg.automation.selenium.servicecenter.pages.account.Comments;
import com.ihg.automation.selenium.servicecenter.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.servicecenter.pages.programs.KarmaPage;
import com.ihg.crm.automation.selenium.tests.servicecenter.LoginLogout;

@Test(groups = { "karma" })
public class KarmaCommentsTest extends LoginLogout
{
    public static final String COMMENTS_TEXT_255_CHARACTERS = String.format("%s, %s - %s",
            RandomUtils.getRandomLetters(100), RandomUtils.getRandomNumber(50), RandomUtils.getRandomLetters(100));
    public static final String EDIT_COMMENTS_TEXT = "Edit - Test comments.";
    private Comments commentsSection;
    private Member member = new Member();
    private AccountInfoPage accountInfoPage = new AccountInfoPage();

    @BeforeClass
    public void beforeClass()
    {

        member.setPersonalInfo(MemberPopulateHelper.getSimplePersonalInfo());
        member.addAddress(MemberPopulateHelper.getUnitedStatesAddress());
        member.addProgram(Program.KAR);

        login();

        new EnrollmentPage().enroll(member);
    }

    @Test(priority = 10)
    public void verifyCommentSectionInViewMode()
    {
        defaultCommentsVerification(true);
    }

    @Test(priority = 20)
    public void verifyCommentSectionInEditMode()
    {
        commentsSection.clickEdit(verifyNoError());

        verifyThat(commentsSection, displayed(true));
        verifyThat(commentsSection.getComments(), hasEmptyText());
        verifyThat(commentsSection.getCancel(), displayed(true));
        verifyThat(commentsSection.getSave(), displayed(true));
    }

    @Test(priority = 30)
    public void cancelAddComments()
    {
        commentsSection.getComments().typeAndWait(COMMENTS_TEXT_255_CHARACTERS);
        commentsSection.clickCancel(verifyNoError());
        verifyThat(commentsSection.getCommentsLabel(), hasDefault());
    }

    @Test(priority = 40)
    public void saveEmptyComments()
    {
        commentsSection.clickEdit(verifyNoError());

        verifyThat(commentsSection.getComments(), hasEmptyText());
        commentsSection.clickSave(verifyNoError());
        verifyThat(commentsSection.getCommentsLabel(), hasDefault());
    }

    @Test(priority = 50)
    public void addComments()
    {
        commentsSection.clickEdit(verifyNoError());

        commentsSection.getComments().typeAndWait(COMMENTS_TEXT_255_CHARACTERS);
        commentsSection.clickSave(verifyNoError());
        verifyThat(commentsSection.getCommentsLabel(), hasText(COMMENTS_TEXT_255_CHARACTERS));
    }

    @Test(priority = 60)
    public void reopenMemberAndVerifyAddedComments()
    {
        new LeftPanel().reOpenMemberProfile(member);

        accountInfoPage.goTo();
        verifyThat(commentsSection.getCommentsLabel(), hasText(COMMENTS_TEXT_255_CHARACTERS));
    }

    @Test(priority = 65)
    public void tryToAddMoreThan255CharactersComments()
    {
        commentsSection.clickEdit(verifyNoError());

        commentsSection.getComments().typeAndWait(COMMENTS_TEXT_255_CHARACTERS + "a");
        commentsSection.clickSave(verifyNoError());
        verifyThat(commentsSection.getCommentsLabel(), hasText(COMMENTS_TEXT_255_CHARACTERS));
    }

    @Test(priority = 70)
    public void cancelEditComments()
    {
        commentsSection.clickEdit(verifyNoError());

        commentsSection.getComments().typeAndWait(EDIT_COMMENTS_TEXT);
        commentsSection.clickCancel(verifyNoError());
        verifyThat(commentsSection.getCommentsLabel(), hasText(COMMENTS_TEXT_255_CHARACTERS));
    }

    @Test(priority = 80)
    public void editComments()
    {
        commentsSection.clickEdit(verifyNoError());

        commentsSection.getComments().typeAndWait(EDIT_COMMENTS_TEXT);
        commentsSection.clickSave(verifyNoError());
        verifyThat(commentsSection.getCommentsLabel(), hasText(EDIT_COMMENTS_TEXT));
    }

    @Test(priority = 90)
    public void removeComments()
    {
        commentsSection.clickEdit(verifyNoError());

        commentsSection.getComments().clear();
        commentsSection.clickSave(verifyNoError());
        verifyThat(commentsSection.getCommentsLabel(), hasDefault());
    }

    @Test(priority = 95)
    public void reopenMemberAndVerifyRemovedComments()
    {
        new LeftPanel().reOpenMemberProfile(member);

        accountInfoPage.goTo();
        verifyThat(commentsSection.getCommentsLabel(), hasDefault());
    }

    @Test(priority = 100)
    public void closeMemberAndVerifyCommentSection()
    {
        KarmaPage karmaPage = new KarmaPage();
        karmaPage.goTo();
        karmaPage.getSummary().setClosedStatus(FROZEN);

        defaultCommentsVerification(true);
    }

    @Test(priority = 110)
    public void removeMemberAndVerifyCommentSection()
    {
        accountInfoPage.removedAccount(false);

        defaultCommentsVerification(false);
    }

    private void defaultCommentsVerification(boolean isEditDisplayed)
    {
        accountInfoPage.goTo();

        commentsSection = accountInfoPage.getComments();
        verifyThat(commentsSection, displayed(true));
        verifyThat(commentsSection.getCommentsLabel(), hasDefault());
        verifyThat(commentsSection.getEdit(), displayed(isEditDisplayed));
    }
}
