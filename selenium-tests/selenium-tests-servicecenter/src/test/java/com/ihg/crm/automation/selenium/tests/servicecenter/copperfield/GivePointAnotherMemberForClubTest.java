package com.ihg.crm.automation.selenium.tests.servicecenter.copperfield;

import static com.ihg.crm.automation.selenium.tests.servicecenter.copperfield.VerificationDatesUtils.verifyDatesWithShiftedExpiration;

import org.joda.time.LocalDate;

public class GivePointAnotherMemberForClubTest extends GivePointAnotherMemberBase
{
    @Override
    protected void postEnrollActionsForOriginator()
    {
        // Nothing to do
    }

    @Override
    protected void verifyDatesForRecipient(LocalDate localDate)
    {
        verifyDatesWithShiftedExpiration(localDate);
    }

    @Override
    protected void verifyDatesForOriginator(LocalDate localDate)
    {
        verifyDatesWithShiftedExpiration(localDate);
    }

}
