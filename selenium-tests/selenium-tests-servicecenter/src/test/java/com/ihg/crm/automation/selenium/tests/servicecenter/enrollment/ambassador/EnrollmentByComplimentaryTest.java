package com.ihg.crm.automation.selenium.tests.servicecenter.enrollment.ambassador;

import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyNoError;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.event.EventTransactionType.Membership.ENROLL;
import static com.ihg.automation.selenium.common.event.EventTransactionType.Offer.OFFER_REGISTRATION;
import static com.ihg.automation.selenium.common.member.AmbassadorUtils.AMBASSADOR_COMP_PROMO_$0;
import static com.ihg.automation.selenium.common.member.AmbassadorUtils.NEW_GOLD_AMBASSADOR_KIT;
import static com.ihg.automation.selenium.common.payment.PaymentFactory.COMPLIMENTARY;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.enabled;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.isDisplayedWithWait;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.isSelected;
import static com.ihg.automation.selenium.matchers.component.HasSelectItems.hasSelectItems;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static org.hamcrest.core.StringContains.containsString;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.common.DateUtils;
import com.ihg.automation.selenium.common.Constant;
import com.ihg.automation.selenium.common.CustomerInfoPanel;
import com.ihg.automation.selenium.common.billing.EventBillingDetailsTab;
import com.ihg.automation.selenium.common.catalog.CatalogItem;
import com.ihg.automation.selenium.common.earning.EventEarningDetailsTab;
import com.ihg.automation.selenium.common.enroll.SuccessEnrolmentPopUp;
import com.ihg.automation.selenium.common.event.AllEventsRow;
import com.ihg.automation.selenium.common.event.AllEventsRowConverter;
import com.ihg.automation.selenium.common.event.AllEventsRowFactory;
import com.ihg.automation.selenium.common.event.EarningEvent;
import com.ihg.automation.selenium.common.member.AmbassadorOfferCode;
import com.ihg.automation.selenium.common.member.AmbassadorUtils;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.offer.Offer;
import com.ihg.automation.selenium.common.offer.OfferFactory;
import com.ihg.automation.selenium.common.order.Order;
import com.ihg.automation.selenium.common.order.OrderFactory;
import com.ihg.automation.selenium.common.pages.PaymentMethod;
import com.ihg.automation.selenium.common.payment.PaymentDetailsPopUp;
import com.ihg.automation.selenium.common.types.program.AmbassadorLevel;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.common.types.program.ProgramStatus;
import com.ihg.automation.selenium.common.types.program.RewardClubLevel;
import com.ihg.automation.selenium.gwt.components.checkbox.CheckBox;
import com.ihg.automation.selenium.servicecenter.pages.LeftPanel;
import com.ihg.automation.selenium.servicecenter.pages.account.AccountInfoPage;
import com.ihg.automation.selenium.servicecenter.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsGrid;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsGridRow;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsPage;
import com.ihg.automation.selenium.servicecenter.pages.event.all.OfferGridRowContainer;
import com.ihg.automation.selenium.servicecenter.pages.event.enroll.MembershipDetailsPopUp;
import com.ihg.automation.selenium.servicecenter.pages.event.enroll.MembershipDetailsTab;
import com.ihg.automation.selenium.servicecenter.pages.event.enroll.MembershipGridRowContainer;
import com.ihg.automation.selenium.servicecenter.pages.event.order.OrderSystemDetailsGridRowContainer;
import com.ihg.automation.selenium.servicecenter.pages.personal.PersonalInfoPage;
import com.ihg.automation.selenium.servicecenter.pages.programs.ProgramPageBase.EnrollmentDetails;
import com.ihg.automation.selenium.servicecenter.pages.programs.RenewExtendProgramSummary;
import com.ihg.automation.selenium.servicecenter.pages.programs.amb.AmbassadorPage;
import com.ihg.automation.selenium.servicecenter.pages.programs.amb.AmbassadorTierLevelActivityGrid;
import com.ihg.automation.selenium.servicecenter.pages.programs.amb.AmbassadorTierLevelActivityGrid.AmbassadorTierLevelAccumulator;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.RewardClubPage;

public class EnrollmentByComplimentaryTest extends AMBEnrollmentCommon
{
    private static final CatalogItem ENROLMENT_AWARD = AMBASSADOR_COMP_PROMO_$0;

    private Member member = new Member();
    private EnrollmentPage enrollmentPage = new EnrollmentPage();
    private PaymentDetailsPopUp ambEnrollPaymentDetailsPopUp = new PaymentDetailsPopUp();
    private PersonalInfoPage personalInfoPage = new PersonalInfoPage();
    private AccountInfoPage accountInfoPage = new AccountInfoPage();
    private RewardClubPage rewardClubPage = new RewardClubPage();
    private AmbassadorPage ambassadorPage = new AmbassadorPage();

    private AllEventsPage allEventsPage = new AllEventsPage();
    private MembershipGridRowContainer rcMembershipGridRowContainer;
    private MembershipDetailsPopUp membershipDetailsPopUp = new MembershipDetailsPopUp();
    private EarningEvent rcEarningEvent;

    @BeforeClass
    public void beforeClass()
    {
        member.addProgram(Program.AMB);
        member.addAddress(MemberPopulateHelper.getUnitedStatesAddress());
        member.setPersonalInfo(MemberPopulateHelper.getSimplePersonalInfo());
        member.setAmbassadorOfferCode(AmbassadorOfferCode.CHSRO);

        rcEarningEvent = new EarningEvent("PC", "0", "0", Constant.RC_POINTS, "");
        rcEarningEvent.setAward("");
        rcEarningEvent.setAllianceTransactionCode("");
        rcEarningEvent.setOrderNumber("");
        rcEarningEvent.setAllianceSentDate("");

        login();
    }

    @Test
    public void populateAMBMember()
    {
        enrollmentPage = new EnrollmentPage();
        enrollmentPage.goTo();
        enrollmentPage.populate(member);
    }

    @Test(dependsOnMethods = { "populateAMBMember" })
    public void verifyRCProgramCheckBox()
    {
        CheckBox rcPropgram = enrollmentPage.getPrograms().getProgramCheckbox(Program.RC);
        verifyThat(rcPropgram, isSelected(true));
    }

    @Test(dependsOnMethods = { "verifyRCProgramCheckBox" }, alwaysRun = true)
    public void enrollAMBMember()
    {
        enrollmentPage.clickSubmit(verifyNoError());
    }

    @Test(dependsOnMethods = { "enrollAMBMember" })
    public void verifyAMBPaymentPopUp()
    {
        verifyThat(ambEnrollPaymentDetailsPopUp, isDisplayedWithWait());
        verifyThat(ambEnrollPaymentDetailsPopUp.getTotalAmount(),
                hasSelectItems(AmbassadorUtils.ENROLL_AWARD_NAME_LIST));
    }

    @Test(dependsOnMethods = { "verifyAMBPaymentPopUp" }, alwaysRun = true)
    public void verifyPaymentDetails()
    {
        ambEnrollPaymentDetailsPopUp.selectAmount(ENROLMENT_AWARD);
        verifyThat(ambEnrollPaymentDetailsPopUp, hasText(containsString(PaymentMethod.COMPLIMENTARY.getLabel())));
    }

    @Test(dependsOnMethods = { "verifyPaymentDetails" }, alwaysRun = true)
    public void submitPaymentDetails()
    {
        ambEnrollPaymentDetailsPopUp.clickSubmitNoError();

        SuccessEnrolmentPopUp popUp = new SuccessEnrolmentPopUp();
        verifyThat(popUp, isDisplayedWithWait());
        member = popUp.putPrograms(member);
        verifyThat(popUp.getMemberId(Program.RC), hasText(popUp.getMemberId(Program.AMB).getText()));
        popUp.clickDone(verifyNoError());
    }

    @Test(dependsOnMethods = { "submitPaymentDetails" }, alwaysRun = true)
    public void verifyAccountInformation()
    {
        verifyThat(personalInfoPage, isDisplayedWithWait());
        accountInfoPage.goTo();
        verifyThat(accountInfoPage.getAccountStatus(), hasText(ProgramStatus.OPEN));
        verifyThat(accountInfoPage.getSecurity().getLockout().getView(), hasText(Constant.NOT_AVAILABLE));
        verifyThat(accountInfoPage.getFlags(), hasText(containsString("No flags selected")));
    }

    @Test(dependsOnMethods = { "verifyAccountInformation" }, alwaysRun = true)
    public void verifyRCProgramInfo()
    {
        rewardClubPage.goTo();
        rewardClubPage.getSummary().verify(ProgramStatus.OPEN, RewardClubLevel.GOLD);
    }

    @Test(dependsOnMethods = { "verifyRCProgramInfo" }, alwaysRun = true)
    public void verifyAMBProgramInfoSummary()
    {
        ambassadorPage.goTo();
        RenewExtendProgramSummary summ = ambassadorPage.getSummary();
        verifyThat(summ.getMemberId(), hasText(member.getAMBProgramId()));
        summ.verify(ProgramStatus.OPEN, AmbassadorLevel.AMB);
        verifyThat(summ.getExpirationDate(), hasText(AmbassadorUtils.getExpirationDate()));
        verifyThat(summ, hasText(containsString("No flags selected")));
        verifyThat(summ.getRenew(), enabled(false));
        verifyThat(summ.getExtend(), displayed(true));
    }

    @Test(dependsOnMethods = { "verifyAMBProgramInfoSummary" }, alwaysRun = true)
    public void verifyAMBProgramInfoAnnualActivity()
    {
        ambassadorPage.goTo();
        AmbassadorTierLevelActivityGrid grid = ambassadorPage.getAnnualActivities();
        grid.getRow(AmbassadorTierLevelAccumulator.IC_HOTELS).verify("0", Integer.toString(achieveRamIcHotels));
        grid.getRow(AmbassadorTierLevelAccumulator.QUALIFYING_SPEND).verify("0", Integer.toString(achieveRamIQualifyingSpend));
    }

    @Test(dependsOnMethods = { "verifyAMBProgramInfoAnnualActivity" }, alwaysRun = true)
    public void verifyAMBProgramInfoEnrollmentDetails()
    {
        ambassadorPage.goTo();
        EnrollmentDetails details = ambassadorPage.getEnrollmentDetails();
        details.verifyScSource(helper);
        verifyThat(details.getEnrollDate(), hasText(DateUtils.getFormattedDate()));
        verifyThat(details.getRenewalDate(), hasText(Constant.NOT_AVAILABLE));
        verifyThat(details.getOfferCode(), hasText(member.getAmbassadorOfferCode().getCode()));
        verifyThat(details.getReferringMember(), hasText(Constant.NOT_AVAILABLE));
        details.getPaymentDetails().verifyCommonDetails(COMPLIMENTARY);
    }

    @Test(dependsOnMethods = "verifyAMBProgramInfoEnrollmentDetails", alwaysRun = true)
    public void verifyAllEventsEnrollRCEvent()
    {
        allEventsPage.goTo();
        AllEventsGrid grid = allEventsPage.getGrid();
        AllEventsGridRow row = grid.getEnrollRow(Program.RC);
        row.verify(AllEventsRowFactory.getEnrollEvent(Program.RC));
        rcMembershipGridRowContainer = row.expand(MembershipGridRowContainer.class);
        rcMembershipGridRowContainer.getMembershipDetails().verifyScSource(helper);
    }

    @Test(dependsOnMethods = { "verifyAllEventsEnrollRCEvent" }, alwaysRun = true)
    public void openEnrollRCEventMembershipDetailsPopUp()
    {
        AllEventsGrid grid = allEventsPage.getGrid();
        AllEventsGridRow row = grid.getEnrollRow(Program.RC);
        rcMembershipGridRowContainer = row.expand(MembershipGridRowContainer.class);
        rcMembershipGridRowContainer.getDetails().click();
        verifyThat(membershipDetailsPopUp, isDisplayedWithWait());
    }

    @Test(dependsOnMethods = { "openEnrollRCEventMembershipDetailsPopUp" })
    public void verifyAllEventsEnrollRCEventMembershipDetails()
    {
        MembershipDetailsTab tab = membershipDetailsPopUp.getMembershipDetailsTab();
        tab.goTo();
        tab.getMembershipDetails().verifyScSource(helper);
    }

    @Test(dependsOnMethods = { "openEnrollRCEventMembershipDetailsPopUp" })
    public void verifyAllEventsEnrollRCEventBillingDetails()
    {
        EventBillingDetailsTab tab = membershipDetailsPopUp.getBillingDetailsTab();
        tab.goTo();
        tab.verifyTabIsEmpty(ENROLL);
    }

    @Test(dependsOnMethods = { "openEnrollRCEventMembershipDetailsPopUp" })
    public void verifyAllEventsEnrollRCEventEarningDetails()
    {
        EventEarningDetailsTab tab = membershipDetailsPopUp.getEarningDetailsTab();
        tab.goTo();
        tab.verifyBasicDetailsNoPointsAndMiles();
        tab.getGrid().verify(rcEarningEvent);
    }

    @Test(dependsOnMethods = { "verifyAllEventsEnrollRCEventEarningDetails",
            "verifyAllEventsEnrollRCEventBillingDetails", "verifyAllEventsEnrollRCEventMembershipDetails",
            "openEnrollRCEventMembershipDetailsPopUp" }, alwaysRun = true)
    public void closeEnrollRCEventMembershipDetailsPopUp()
    {
        membershipDetailsPopUp.close();
    }

    @Test(dependsOnMethods = "closeEnrollRCEventMembershipDetailsPopUp", alwaysRun = true)
    public void verifyAllEventsEnrollAMBEvent()
    {
        AllEventsGridRow row = allEventsPage.getGrid().getEnrollRow(Program.AMB);
        row.verify(AllEventsRowFactory.getEnrollEvent(Program.AMB));
        row.expand(MembershipGridRowContainer.class).getMembershipDetails().verify(COMPLIMENTARY, helper);
    }

    @Test(dependsOnMethods = { "verifyAllEventsEnrollAMBEvent" })
    public void openEnrollAMBEventMembershipDetailsPopUp()
    {
        openAMBEventDetailsPopUp();
    }

    @Test(dependsOnMethods = { "openEnrollAMBEventMembershipDetailsPopUp" })
    public void verifyAllEventsEnrollAMBEventMembershipDetails()
    {
        MembershipDetailsTab tab = membershipDetailsPopUp.getMembershipDetailsTab();
        tab.goTo();
        tab.getMembershipDetails().verify(COMPLIMENTARY, helper);
    }

    @Test(dependsOnMethods = { "openEnrollAMBEventMembershipDetailsPopUp" })
    public void verifyAllEventsEnrollAMBEventBillingDetails()
    {
        verifyAMBEventBillingDetails(ENROLMENT_AWARD);
    }

    @Test(dependsOnMethods = { "openEnrollAMBEventMembershipDetailsPopUp" })
    public void verifyAllEventsEnrollAMBEventEarningDetails()
    {
        verifyAMBEventEarningDetails(ENROLMENT_AWARD);
    }

    @Test(dependsOnMethods = { "verifyAllEventsEnrollAMBEventEarningDetails",
            "verifyAllEventsEnrollAMBEventBillingDetails", "verifyAllEventsEnrollAMBEventMembershipDetails",
            "openEnrollAMBEventMembershipDetailsPopUp" }, alwaysRun = true)
    public void closeEnrollAMBEventMembershipDetailsPopUp()
    {
        membershipDetailsPopUp.close();
    }

    @Test(dependsOnMethods = { "closeEnrollAMBEventMembershipDetailsPopUp" }, alwaysRun = true)
    public void verifyAllEventsSystemOrderEvent()
    {
        Order order = OrderFactory.getSystemOrder(NEW_GOLD_AMBASSADOR_KIT, member, helper);
        AllEventsRow orderEventRow = AllEventsRowConverter.convert(order);

        allEventsPage.goTo();
        AllEventsGrid grid = allEventsPage.getGrid();
        AllEventsGridRow row = grid.getRow(orderEventRow.getTransType());
        row.verify(orderEventRow);
        row.expand(OrderSystemDetailsGridRowContainer.class).verify(order);
    }

    @Test(dependsOnMethods = { "verifyAllEventsSystemOrderEvent" }, alwaysRun = true)
    public void verifyAllEventsOfferRegistrationEvent()
    {
        Offer offer = OfferFactory.DELAYED_FULFILMENT_GLOBAL;

        allEventsPage.goTo();
        AllEventsGrid grid = allEventsPage.getGrid();
        AllEventsGridRow row = grid.getRow(OFFER_REGISTRATION);

        row.verify(AllEventsRowFactory.getOfferRegistrationEvent(offer));

        OfferGridRowContainer container = row.expand(OfferGridRowContainer.class);
        container.getOfferDetails().verify(offer, helper);
    }

    @Test(dependsOnMethods = { "verifyAllEventsOfferRegistrationEvent" }, alwaysRun = true)
    public void verifyLeftPanelCustomerInformation()
    {
        CustomerInfoPanel panel = new LeftPanel().getCustomerInfoPanel();
        verifyThat(panel.getFullName(), hasText(member.getPersonalInfo().getName().getNameOnPanel()));
        verifyThat(panel.getMemberNumber(Program.RC), hasText(member.getRCProgramId()));
        verifyThat(panel.getPhone(), hasText(Constant.NOT_AVAILABLE));
        verifyThat(panel.getEmail(), hasText(Constant.NOT_AVAILABLE));
    }
}
