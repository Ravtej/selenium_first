package com.ihg.crm.automation.selenium.tests.servicecenter.hotel;

import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyNoError;
import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifySuccess;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasDefault;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.isDisplayedWithWait;
import static com.ihg.automation.selenium.matchers.component.GridSize.size;
import static com.ihg.automation.selenium.servicecenter.pages.hotel.certificate.HotelCertificatePage.CERTIFICATES_SUCCESSFULLY_CANCELED;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.common.DateUtils;
import com.ihg.automation.common.RandomUtils;
import com.ihg.automation.selenium.common.hotel.HotelCertificate;
import com.ihg.automation.selenium.common.hotel.HotelCertificateStatus;
import com.ihg.automation.selenium.common.types.Currency;
import com.ihg.automation.selenium.servicecenter.pages.hotel.certificate.CreateHotelCertificatePopUp;
import com.ihg.automation.selenium.servicecenter.pages.hotel.certificate.EditHotelCertificatePopUp;
import com.ihg.automation.selenium.servicecenter.pages.hotel.certificate.HotelCertificateDetailsEdit;
import com.ihg.automation.selenium.servicecenter.pages.hotel.certificate.HotelCertificatePage;
import com.ihg.automation.selenium.servicecenter.pages.hotel.certificate.HotelCertificateSearchGridRow;
import com.ihg.automation.selenium.servicecenter.pages.hotel.certificate.HotelCertificateSearchRowContainer;
import com.ihg.automation.selenium.servicecenter.pages.hotel.certificate.ReimbursementDetails;
import com.ihg.automation.selenium.servicecenter.pages.hotel.certificate.ViewHotelCertificatePopUp;
import com.ihg.crm.automation.selenium.tests.servicecenter.LoginLogout;

public class HotelCertificateUpdateAcceptCancelTest extends LoginLogout
{
    private HotelCertificatePage hotelCertificatePage = new HotelCertificatePage();
    private EditHotelCertificatePopUp editHotelCertificatePopUp;
    private HotelCertificate createCertificate, updateCertificate;

    @BeforeClass
    public void beforeClass()
    {
        createCertificate = new HotelCertificate();
        createCertificate.setHotelCode("ATLCP");
        createCertificate.setCheckInDate(DateUtils.getDateBackward(1));
        createCertificate.setCheckOutDate(DateUtils.getFormattedDate());
        createCertificate.setItemId("HQ250");
        createCertificate.setCertificateNumber(RandomUtils.getRandomNumber(8));
        createCertificate.setDescriptionGuestName("auto test certificate " + createCertificate.getCertificateNumber());
        createCertificate.setAmount("100.00");
        createCertificate.setReimbursementAmount("100.00");
        createCertificate.setReimbursementMethod(HotelCertificate.INVOICE);
        createCertificate.setBillingEntity("CORPC");
        createCertificate.setCurrency(Currency.USD);
        createCertificate.setItemName("Reward Night 25,000");
        createCertificate.setStatus(HotelCertificateStatus.INITIATED);

        updateCertificate = createCertificate.clone();
        updateCertificate.setHotelCode("ATLBH");
        updateCertificate.setCheckInDate(DateUtils.getDateBackward(2));
        updateCertificate.setCheckOutDate(DateUtils.getDateBackward(1));
        updateCertificate.setDescriptionGuestName(createCertificate.getDescriptionGuestName() + " (update)");
        updateCertificate.setAmount("100.01");
        updateCertificate.setReimbursementAmount("100.01");
        updateCertificate.setFolioNumber(RandomUtils.getRandomNumber(7));
        updateCertificate.setBundleNumber(RandomUtils.getRandomNumber(7));

        login();
        hotelCertificatePage.goTo();
    }

    @Test
    public void createNewCertificateClose()
    {
        CreateHotelCertificatePopUp createHotelCertificatePopUp = hotelCertificatePage.getSearchFields()
                .clickEnterCertificate();

        createHotelCertificatePopUp.populate(createCertificate);
        createHotelCertificatePopUp.getClose().clickAndWait();
        verifyThat(createHotelCertificatePopUp, displayed(false));

        hotelCertificatePage.search(createCertificate);
        verifyThat(hotelCertificatePage.getGrid(), size(0));
    }

    @Test(dependsOnMethods = { "createNewCertificateClose" }, alwaysRun = true)
    public void createNewCertificateSave()
    {
        hotelCertificatePage.getSearchFields().clickEnterCertificate().createCertificate(createCertificate);
    }

    @Test(dependsOnMethods = { "createNewCertificateSave" }, priority = 10)
    public void searchCreatedCertificate()
    {
        hotelCertificatePage.search(createCertificate);
        verifyThat(hotelCertificatePage.getGrid(), size(1));
    }

    @Test(dependsOnMethods = { "createNewCertificateSave" }, priority = 10)
    public void verifyCreatedCertificate()
    {
        HotelCertificateSearchGridRow row = hotelCertificatePage.getGrid().getRow(1);
        row.verify(createCertificate);
        row.getRowContainer().verify(createCertificate);
    }

    @Test(dependsOnMethods = { "createNewCertificateSave" }, priority = 20)
    public void openEditCertificateDetailsPopUp()
    {
        hotelCertificatePage.getGrid().getRow(1).getRowContainer().clickDetails();
        editHotelCertificatePopUp = new EditHotelCertificatePopUp();
        verifyThat(editHotelCertificatePopUp, isDisplayedWithWait());
    }

    @Test(dependsOnMethods = { "openEditCertificateDetailsPopUp" }, priority = 30)
    public void verifyFieldsVisibilityOnCertificateDetailsPopUp()
    {
        HotelCertificateDetailsEdit details = editHotelCertificatePopUp.getDetails();
        verifyThat(details.getBillingEntity(), displayed(true));
        verifyThat(details.getBundle(), displayed(true));
        verifyThat(details.getCertificateNumber(), displayed(true));
        verifyThat(details.getCheckIn(), displayed(true));
        verifyThat(details.getCheckOut(), displayed(true));
        verifyThat(details.getConfirmationNumber(), displayed(true));
        verifyThat(details.getCurrency(), displayed(true));
        verifyThat(details.getDescriptionGuestName(), displayed(true));
        verifyThat(details.getFolioNumber(), displayed(true));
        verifyThat(details.getHotel(), displayed(true));
        verifyThat(details.getItemId(), displayed(true));
        verifyThat(details.getItemName(), displayed(true));
        verifyThat(details.getReimbursementAmount(), displayed(true));
        verifyThat(details.getStatus(), displayed(true));
    }

    @Test(dependsOnMethods = { "openEditCertificateDetailsPopUp" }, priority = 30)
    public void verifyCreatedCertificateDetailsOnPopUp()
    {
        editHotelCertificatePopUp.getDetails().verify(createCertificate);
    }

    @Test(dependsOnMethods = "openEditCertificateDetailsPopUp", priority = 40)
    public void editHotel()
    {
        new EditHotelCertificatePopUp().editCertificate(updateCertificate, verifyNoError());
    }

    @Test(dependsOnMethods = { "editHotel" }, priority = 50)
    public void searchByObsoleteHotelCertificateDetails()
    {
        hotelCertificatePage.search(createCertificate);
        verifyThat(hotelCertificatePage.getGrid(), size(0));
    }

    @Test(dependsOnMethods = { "editHotel" }, priority = 51)
    public void searchByUpdatedHotelCertificateDetails()
    {
        hotelCertificatePage.search(updateCertificate);
        verifyThat(hotelCertificatePage.getGrid(), size(1));
        HotelCertificateSearchGridRow row = hotelCertificatePage.getGrid().getRow(1);
        row.verify(updateCertificate);
        row.getRowContainer().verify(updateCertificate);
    }

    @Test(dependsOnMethods = { "editHotel" }, priority = 60)
    public void approveInitiatedHotelCertificate()
    {
        hotelCertificatePage.getGrid().getRow(1).check();
        hotelCertificatePage.getButtonBar().clickAccept(verifyNoError(),
                verifySuccess(HotelCertificatePage.CERTIFICATES_SUCCESSFULLY_ACCEPTED));
        updateCertificate.setStatus(HotelCertificateStatus.ACCEPTED);
    }

    @Test(dependsOnMethods = { "approveInitiatedHotelCertificate" }, priority = 61)
    public void verifyUpdatedAndAcceptedCertificateDetailsOnPopUp()
    {
        HotelCertificateSearchGridRow row = hotelCertificatePage.getGrid().getRow(1);
        row.verify(updateCertificate);
        row.getRowContainer().clickDetails();

        EditHotelCertificatePopUp editHotelCertificatePopUp = new EditHotelCertificatePopUp();
        editHotelCertificatePopUp.getDetails().verify(updateCertificate);
        verifyThat(editHotelCertificatePopUp.getSaveChanges(), displayed(true));
        verifyThat(editHotelCertificatePopUp.getSaveApprove(), displayed(false));
        editHotelCertificatePopUp.clickClose(verifyNoError());
    }

    @Test(dependsOnMethods = { "approveInitiatedHotelCertificate" }, priority = 65)
    public void cancelCertificate()
    {
        hotelCertificatePage.getGrid().getRow(1).check();
        hotelCertificatePage.getButtonBar().clickCancelCertificate(verifyNoError(),
                verifySuccess(CERTIFICATES_SUCCESSFULLY_CANCELED));
        updateCertificate.setStatus(HotelCertificateStatus.CANCELED);
    }

    @Test(dependsOnMethods = { "cancelCertificate" })
    public void searchAndVerifyCancelledCertificateDetails()
    {
        hotelCertificatePage.search(updateCertificate);
        verifyThat(hotelCertificatePage.getGrid(), size(1));

        HotelCertificateSearchGridRow row = hotelCertificatePage.getGrid().getRow(1);

        String reimbursementAmount = updateCertificate.getReimbursementAmount();
        updateCertificate.setReimbursementAmount(null);
        updateCertificate.setReimbursementMethod(null);
        row.verify(updateCertificate);
        HotelCertificateSearchRowContainer rowContainer = row.getRowContainer();
        rowContainer.verify(updateCertificate);
        rowContainer.clickDetails();
        updateCertificate.setReimbursementAmount(reimbursementAmount);

        ViewHotelCertificatePopUp popUp = new ViewHotelCertificatePopUp();
        popUp.getDetails().verify(updateCertificate);

        ReimbursementDetails reimbursementDetails = popUp.getReimbursementDetails();
        verifyThat(reimbursementDetails.getReimbursementMethod(), hasDefault());
        verifyThat(reimbursementDetails.getReimbursementDate(), hasDefault());
        verifyThat(reimbursementDetails.getReimbursementAmount(), hasDefault());
        verifyThat(reimbursementDetails.getCurrency(), hasDefault());

        verifyThat(popUp.getSaveChanges(), displayed(false));
        verifyThat(popUp.getSaveApprove(), displayed(false));

        popUp.clickClose(verifyNoError());
    }

}
