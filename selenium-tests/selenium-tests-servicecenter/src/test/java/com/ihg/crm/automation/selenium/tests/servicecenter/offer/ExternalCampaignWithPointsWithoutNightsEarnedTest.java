package com.ihg.crm.automation.selenium.tests.servicecenter.offer;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static com.ihg.automation.selenium.matchers.component.IsDisplayed.displayed;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.servicecenter.pages.GuestSearch;
import com.ihg.automation.selenium.servicecenter.pages.event.offer.CampaignDashboardTab;
import com.ihg.automation.selenium.servicecenter.pages.event.offer.OfferEventPage;
import com.ihg.automation.selenium.servicecenter.pages.event.offer.OfferPopUp;
import com.ihg.automation.selenium.servicecenter.pages.event.offer.SubOffersGrid;
import com.ihg.automation.selenium.servicecenter.pages.event.offer.SubOffersGridRow;
import com.ihg.crm.automation.selenium.tests.servicecenter.LoginLogout;

public class ExternalCampaignWithPointsWithoutNightsEarnedTest extends LoginLogout
{
    private static final String OFFER_CODE_WITH_EXTERNAL_CAMPAIGN = "8710487";

    @BeforeClass
    public void beforeClass()
    {
        login();
        new GuestSearch().byMemberNumber(memberWithExternalCampaignWithPointsWithoutNights);
    }

    @Test
    public void verifyExternalTrackingTab()
    {
        OfferEventPage offerEventPage = new OfferEventPage();
        offerEventPage.goTo();

        OfferPopUp popUp = offerEventPage.getMemberOffersGrid().getOfferPopUp(OFFER_CODE_WITH_EXTERNAL_CAMPAIGN);

        CampaignDashboardTab tab = popUp.getCampaignDashboardTab();

        verifyThat(tab, displayed(true));

        SubOffersGrid grid = tab.getMemberOffersGrid();

        verifyThat(grid.getRow(1).getCell(SubOffersGridRow.SubOfferCell.REWARD), hasText("5,000 Points"));
        verifyThat(grid.getRow(2).getCell(SubOffersGridRow.SubOfferCell.REWARD), hasText("2,000 Points"));

        verifyThat(tab.getExternalTrackingDetails().getCampaignSummary().getEarned(), hasText("0 of 7,000 points"));
    }
}
