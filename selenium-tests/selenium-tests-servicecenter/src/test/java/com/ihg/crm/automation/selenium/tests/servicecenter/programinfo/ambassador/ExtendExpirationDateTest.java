package com.ihg.crm.automation.selenium.tests.servicecenter.programinfo.ambassador;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.types.ExtendReason.AGENT_ERROR;
import static com.ihg.automation.selenium.common.types.ExtendReason.COBRAND_CARDHOLDER;
import static com.ihg.automation.selenium.common.types.ExtendReason.COMMUNICATION_ERROR;
import static com.ihg.automation.selenium.common.types.ExtendReason.CUSTOMER_RETENTION;
import static com.ihg.automation.selenium.common.types.ExtendReason.SYSTEM_ERROR_ISSUES;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.isDisplayedWithWait;
import static com.ihg.automation.selenium.matchers.component.HasSelectItems.hasSelectItems;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;

import java.util.ArrayList;
import java.util.Arrays;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.common.DateUtils;
import com.ihg.automation.selenium.common.member.AmbassadorOfferCode;
import com.ihg.automation.selenium.common.member.AmbassadorUtils;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.servicecenter.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.servicecenter.pages.programs.RenewExtendProgramSummary;
import com.ihg.automation.selenium.servicecenter.pages.programs.amb.AmbassadorPage;
import com.ihg.automation.selenium.servicecenter.pages.programs.amb.ExtendMembershipPopUp;
import com.ihg.crm.automation.selenium.tests.servicecenter.LoginLogout;

public class ExtendExpirationDateTest extends LoginLogout
{
    private String expirationDate;
    private RenewExtendProgramSummary ambSummary;
    private ExtendMembershipPopUp extendPopUp;
    private ArrayList<String> expectedExtendByMonth = new ArrayList<String>(
            Arrays.asList("1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11"));
    private ArrayList<String> expectedReasonCode = new ArrayList<String>(Arrays.asList(AGENT_ERROR, COMMUNICATION_ERROR,
            CUSTOMER_RETENTION, SYSTEM_ERROR_ISSUES, COBRAND_CARDHOLDER));

    @BeforeClass
    public void beforeClass()
    {
        Member member = new Member();
        member.setPersonalInfo(MemberPopulateHelper.getSimplePersonalInfo());
        member.addAddress(MemberPopulateHelper.getUnitedStatesAddress());
        member.addProgram(Program.AMB);
        member.setAmbassadorOfferCode(AmbassadorOfferCode.CHSRO);
        member.setAmbassadorAmount(AmbassadorUtils.AMBASSADOR_COMP_PROMO_$0);

        login();
        new EnrollmentPage().enroll(member);

        AmbassadorPage ambassadorPage = new AmbassadorPage();
        ambassadorPage.goTo();
        ambSummary = ambassadorPage.getSummary();
        expirationDate = ambSummary.getExpirationDate().getText();
    }

    @Test
    public void verifyExtendMembershipPopUp()
    {
        ambSummary.clickExtend();
        extendPopUp = new ExtendMembershipPopUp();
        verifyThat(extendPopUp, isDisplayedWithWait());
        verifyThat(extendPopUp.getReasonCode(), hasSelectItems(expectedReasonCode));
        verifyThat(extendPopUp.getExtendByMonth(), hasSelectItems(expectedExtendByMonth));
    }

    @Test(dependsOnMethods = { "verifyExtendMembershipPopUp" })
    public void verifyNoChanges()
    {
        extendPopUp.getExtendByMonth().select("4");
        extendPopUp.getReasonCode().select(COMMUNICATION_ERROR);
        extendPopUp.close();
        verifyThat(ambSummary.getExpirationDate(), hasText(expirationDate));
    }

    @Test(dependsOnMethods = { "verifyNoChanges" })
    public void populateExtendMembershipPopUp()
    {
        ambSummary.extend(6, AGENT_ERROR);
        String newExpirationDate = DateUtils.getStringToDate(expirationDate).plusMonths(6).toString("ddMMMYY");
        verifyThat(ambSummary.getExpirationDate(), hasText(newExpirationDate));
    }
}
