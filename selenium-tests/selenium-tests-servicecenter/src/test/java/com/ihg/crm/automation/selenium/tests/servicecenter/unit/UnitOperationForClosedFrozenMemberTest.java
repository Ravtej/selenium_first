package com.ihg.crm.automation.selenium.tests.servicecenter.unit;

import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyError;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasText;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.pages.SubmitPopUpBase;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.common.types.program.ProgramStatusReason;
import com.ihg.automation.selenium.condition.SearchContextTimeoutException;
import com.ihg.automation.selenium.gwt.components.Menu;
import com.ihg.automation.selenium.servicecenter.pages.GuestSearch;
import com.ihg.automation.selenium.servicecenter.pages.LeftPanel;
import com.ihg.automation.selenium.servicecenter.pages.TopMenu;
import com.ihg.automation.selenium.servicecenter.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.RewardClubPage;
import com.ihg.automation.selenium.servicecenter.pages.unit.GiftUnitsPopUp;
import com.ihg.automation.selenium.servicecenter.pages.unit.GoodwillPopUp;
import com.ihg.automation.selenium.servicecenter.pages.unit.PurchaseTierLevelPopUp;
import com.ihg.automation.selenium.servicecenter.pages.unit.PurchaseUnitsPopUp;
import com.ihg.automation.selenium.servicecenter.pages.unit.TransferNightsPopUp;
import com.ihg.automation.selenium.servicecenter.pages.unit.TransferUnitsPopUp;
import com.ihg.crm.automation.selenium.tests.servicecenter.LoginLogout;

public class UnitOperationForClosedFrozenMemberTest extends LoginLogout
{
    private static final String MEMBERSHIP_IS_CLOSED_ERROR = "RC Membership Status is Closed. This Transaction is not allowed.";
    private static final String SELECT_MEMBER = "SELECT MBRSHP_ID FROM DGST.MBRSHP WHERE"//
            + " MBRSHP_STAT_CD = 'C'"//
            + " AND MBRSHP_STAT_RSN_CD = 'F'"//
            + " AND LYTY_PGM_CD = 'PC'"//
            + " AND LST_UPDT_TS BETWEEN (SYSDATE - 365) AND (SYSDATE - 5)"//
            + " AND ROWNUM < 2";

    @BeforeClass
    public void beforeClass()
    {
        login();

        List<String> list = jdbcTemplate.queryForList(SELECT_MEMBER, String.class);

        if (CollectionUtils.isNotEmpty(list))
        {
            String memberNumber = list.get(0);
            new GuestSearch().byMemberNumber(memberNumber);
            verifyThat(new LeftPanel().getCustomerInfoPanel().getMemberNumber(Program.RC), hasText(memberNumber));
        }
        else
        {
            Member member = new Member();
            member.setPersonalInfo(MemberPopulateHelper.getSimplePersonalInfo());
            member.addAddress(MemberPopulateHelper.getUnitedStatesAddress());
            member.addProgram(Program.RC);

            new EnrollmentPage().enroll(member);

            RewardClubPage rewardClubPage = new RewardClubPage();
            rewardClubPage.goTo();
            rewardClubPage.getSummary().setClosedStatus(ProgramStatusReason.FROZEN);
        }

    }

    @DataProvider
    private Object[][] unitManagementSubMenuItems()
    {
        TopMenu.UnitManagementMenu unitManagement = new TopMenu().getUnitManagement();
        return new Object[][] { { unitManagement.getGoodwillUnits(), new GoodwillPopUp() },

                { unitManagement.getPurchaseUnits(), new PurchaseUnitsPopUp(), },
                { unitManagement.getPurchaseGift(), new GiftUnitsPopUp() },
                { unitManagement.getPurchaseTierLevel(), new PurchaseTierLevelPopUp() },
                { unitManagement.getTransferUnits(), new TransferUnitsPopUp() },
                { unitManagement.getTransferQNights(), new TransferNightsPopUp() } };
    }

    @Test(dataProvider = "unitManagementSubMenuItems")
    public void tryToOpenUnitManagementSubMenu(Menu.Item item, SubmitPopUpBase popUp)
    {
        try
        {
            item.clickAndWait(verifyError(MEMBERSHIP_IS_CLOSED_ERROR));
        }
        catch (SearchContextTimeoutException timeout)
        {
            throw timeout;
        }
        finally
        {
            if (!verifyThat(popUp, displayed(false)))
            {
                popUp.clickClose();
            }
        }
    }

}
