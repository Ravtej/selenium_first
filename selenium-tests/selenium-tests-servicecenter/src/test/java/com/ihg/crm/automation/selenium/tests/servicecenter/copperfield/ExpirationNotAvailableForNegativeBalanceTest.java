package com.ihg.crm.automation.selenium.tests.servicecenter.copperfield;

import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyNoError;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.Constant.NOT_AVAILABLE;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasDefault;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasText;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasTextAsInt;
import static org.hamcrest.Matchers.lessThan;
import static org.hamcrest.Matchers.not;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.gwt.components.dialog.ConfirmDialog;
import com.ihg.automation.selenium.servicecenter.pages.LeftPanel;
import com.ihg.automation.selenium.servicecenter.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.RewardClubPage;
import com.ihg.automation.selenium.servicecenter.pages.unit.GoodwillPopUp;
import com.ihg.crm.automation.selenium.tests.servicecenter.LoginLogout;

@Test(groups = { "copperfield" })
public class ExpirationNotAvailableForNegativeBalanceTest extends LoginLogout
{
    public static final int GOODWILL_AMOUNT = 1000;

    @BeforeClass
    public void before()
    {
        Member member = new Member();
        member.setPersonalInfo(MemberPopulateHelper.getSimplePersonalInfo());
        member.addAddress(MemberPopulateHelper.getUnitedStatesAddress());
        member.addProgram(Program.RC);

        login();

        new EnrollmentPage().enroll(member);
    }

    @BeforeMethod
    public void addPositivePointsByGoodwill()
    {
        new GoodwillPopUp().goodwillPoints(GOODWILL_AMOUNT);

        RewardClubPage rewardClubPage = new RewardClubPage();
        rewardClubPage.goTo();
        verifyThat(rewardClubPage.getSummary().getBalanceExpirationDate(), hasText(not(NOT_AVAILABLE)));
    }

    @Test(priority = 5)
    public void expirationDateCleanedIfBalanceIsZero()
    {
        new GoodwillPopUp().goodwillPoints(-GOODWILL_AMOUNT);

        new LeftPanel().verifyBalance("0");

        RewardClubPage rewardClubPage = new RewardClubPage();
        rewardClubPage.goTo();
        verifyThat(rewardClubPage.getSummary().getBalanceExpirationDate(), hasDefault());
    }

    @Test(priority = 10)
    public void expirationDateCleanedIfBalanceIsNegative()
    {
        new GoodwillPopUp().goodwillPoints(-(GOODWILL_AMOUNT + 1));
        new ConfirmDialog().clickYes(verifyNoError());

        new LeftPanel().verifyBalance(hasTextAsInt(lessThan(0)));

        RewardClubPage rewardClubPage = new RewardClubPage();
        rewardClubPage.goTo();
        verifyThat(rewardClubPage.getSummary().getBalanceExpirationDate(), hasDefault());
    }
}
