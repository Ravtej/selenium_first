package com.ihg.crm.automation.selenium.tests.servicecenter.programinfo.ambassador;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static org.hamcrest.CoreMatchers.containsString;

import java.util.ArrayList;
import java.util.Arrays;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.member.AmbassadorOfferCode;
import com.ihg.automation.selenium.common.member.AmbassadorUtils;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.matchers.component.ContainsDualListItems;
import com.ihg.automation.selenium.servicecenter.pages.LeftPanel;
import com.ihg.automation.selenium.servicecenter.pages.account.AccountInfoPage;
import com.ihg.automation.selenium.servicecenter.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.servicecenter.pages.programs.RenewExtendProgramSummary;
import com.ihg.automation.selenium.servicecenter.pages.programs.amb.AmbassadorPage;
import com.ihg.crm.automation.selenium.tests.servicecenter.LoginLogout;

public class UpdateFlagsTest extends LoginLogout
{
    private Member member = new Member();
    private AccountInfoPage accountInfoPage = new AccountInfoPage();
    private AmbassadorPage ambassadorPage = new AmbassadorPage();
    private RenewExtendProgramSummary ambSummary;
    private ArrayList<String> expectedListContent = new ArrayList<String>(Arrays.asList("AMEX Centurion", "Auto-Renew",
            "Chase Affluent Rewards", "Citic Ambassador", "Corporate", "Customer Retention", "Do Not Upgrade",
            "General VIP", "Global Sales", "Inactive for 6 months", "JCB Ambassador", "Life time Member",
            "New Hotel/Acquired", "Partner", "Possible Fraud", "Retiree"));

    private final static String FLAG_TO_SELECT = "AMEX Centurion";

    @BeforeClass
    public void beforeClass()
    {
        member.setPersonalInfo(MemberPopulateHelper.getSimplePersonalInfo());
        member.addAddress(MemberPopulateHelper.getUnitedStatesAddress());
        member.addProgram(Program.AMB);
        member.setAmbassadorOfferCode(AmbassadorOfferCode.CHSRO); // CHSRO -
        // CHARLESTON
        // RESERVATIONS
        member.setAmbassadorAmount(AmbassadorUtils.AMBASSADOR_COMP_PROMO_$0);

        login();

        member = new EnrollmentPage().enroll(member);
    }

    @Test
    public void navigateAMBProgramSummary()
    {
        ambassadorPage.goTo();
        ambSummary = ambassadorPage.getSummary();
        ambSummary.clickEdit();
    }

    @Test(dependsOnMethods = { "navigateAMBProgramSummary" })
    public void verifyAvailableSummaryFlags()
    {
        verifyThat(ambSummary.getFlags().getLeftList(), ContainsDualListItems.containsItems(expectedListContent));
    }

    @Test(dependsOnMethods = { "verifyAvailableSummaryFlags", "navigateAMBProgramSummary" }, alwaysRun = true)
    public void selectTheFlag()
    {
        ambSummary.getFlags().select(FLAG_TO_SELECT);
        verifyThat(ambSummary.getFlags().getRightList(),
                ContainsDualListItems.containsItems(Arrays.asList(FLAG_TO_SELECT)));
        expectedListContent.remove(FLAG_TO_SELECT);
        verifyThat(ambSummary.getFlags().getLeftList(), ContainsDualListItems.containsItems(expectedListContent));
        ambSummary.clickSave();
    }

    @Test(dependsOnMethods = { "selectTheFlag" }, alwaysRun = true)
    public void verifyLeftPanelFlags()
    {
        verifyThat(new LeftPanel().getCustomerInfoPanel().getFlags(), hasText(containsString(FLAG_TO_SELECT)));
    }

    @Test(dependsOnMethods = { "verifyLeftPanelFlags" }, alwaysRun = true)
    public void verifyAccountInformationFlags()
    {
        accountInfoPage.goTo();
        verifyThat(accountInfoPage.getFlags(), hasText(containsString(FLAG_TO_SELECT)));
    }

}
