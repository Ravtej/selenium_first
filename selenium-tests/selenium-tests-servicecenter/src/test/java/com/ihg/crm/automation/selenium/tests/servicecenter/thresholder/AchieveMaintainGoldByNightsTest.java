package com.ihg.crm.automation.selenium.tests.servicecenter.thresholder;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.event.EventTransactionType.TierLevel.UPGRADE_ACHIEVE;
import static com.ihg.automation.selenium.common.event.EventTransactionType.TierLevel.UPGRADE_MAINTAIN;
import static com.ihg.automation.selenium.common.types.program.ProgramStatus.OPEN;
import static com.ihg.automation.selenium.common.types.program.RewardClubLevel.CLUB;
import static com.ihg.automation.selenium.common.types.program.RewardClubLevel.GOLD;
import static com.ihg.automation.selenium.common.types.program.RewardClubLevel.PLTN;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.stay.Stay;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.servicecenter.pages.LeftPanel;
import com.ihg.automation.selenium.servicecenter.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsPage;
import com.ihg.automation.selenium.servicecenter.pages.event.stay.StayEventsPage;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.RewardClubPage;
import com.ihg.automation.selenium.tests.common.RulesDBUtils;
import com.ihg.crm.automation.selenium.tests.servicecenter.LoginLogout;

public class AchieveMaintainGoldByNightsTest extends LoginLogout
{
    private Stay stayInPreviousYear = new Stay();
    private Stay stayInCurrentYear;
    private static int nightsToMaintainGold;
    private static int nightsToAchievePlatinum;
    private static int nightsToAchieveGoldPrevious;
    private static int nightsToAchievePlatinumPrevious;
    private static int nightsToAchieveGoldCurrent;

    @BeforeClass
    public void before()
    {
        RulesDBUtils rulesDBUtils = new RulesDBUtils(jdbcTemplate);
        nightsToMaintainGold = rulesDBUtils.getCurrentYearRuleNights(GOLD, GOLD);
        nightsToAchievePlatinum = rulesDBUtils.getCurrentYearRuleNights(GOLD, PLTN);
        nightsToAchieveGoldPrevious = rulesDBUtils.getPreviousYearRuleNights(CLUB, GOLD);
        nightsToAchievePlatinumPrevious = rulesDBUtils.getPreviousYearRuleNights(GOLD, PLTN);
        nightsToAchieveGoldCurrent = rulesDBUtils.getCurrentYearRuleNights(CLUB, GOLD);

        stayInPreviousYear.setHotelCode("ATLCP");
        stayInPreviousYear.setCheckInOutByNightsInPreviousYear(nightsToAchieveGoldPrevious);
        stayInPreviousYear.setAvgRoomRate(15.00);
        stayInPreviousYear.setRateCode("Test");

        stayInCurrentYear = stayInPreviousYear.clone();
        stayInCurrentYear.setCheckInOutByNights(nightsToMaintainGold);

        Member member = new Member();
        member.addProgram(Program.RC);
        member.setPersonalInfo(MemberPopulateHelper.getSimplePersonalInfo());
        member.addAddress(MemberPopulateHelper.getUnitedStatesAddress());

        login();

        new EnrollmentPage().enroll(member);

        new LeftPanel().verifyRCLevel(CLUB);
    }

    @Test
    public void verifyAnnualActivitiesForClub()
    {
        RewardClubPage rewardClubPage = new RewardClubPage();
        rewardClubPage.goTo();
        rewardClubPage.getCurrentYearAnnualActivities().getNightsRow().verify(0, nightsToAchieveGoldCurrent);
    }

    @Test(dependsOnMethods = { "verifyAnnualActivitiesForClub" }, alwaysRun = true)
    public void achieveGoldTierLevel()
    {
        new StayEventsPage().createStay(stayInPreviousYear);
        new LeftPanel().verifyRCLevel(GOLD);

        RewardClubPage rewardClubPage = new RewardClubPage();
        rewardClubPage.goTo();

        rewardClubPage.getPreviousYearAnnualActivities().getNightsRow().verify(nightsToAchieveGoldPrevious,
                nightsToAchievePlatinumPrevious);

        rewardClubPage.getCurrentYearAnnualActivities().getNightsRow().verify(0, nightsToAchievePlatinum,
                nightsToMaintainGold);

        AllEventsPage allEventsPage = new AllEventsPage();
        allEventsPage.goTo();

        verifyThat(allEventsPage.getGrid().getRow(UPGRADE_ACHIEVE, "RC - GOLD"), displayed(true));
    }

    @Test(dependsOnMethods = { "achieveGoldTierLevel" }, alwaysRun = true)
    public void maintainGoldTierLevel()
    {
        new StayEventsPage().createStay(stayInCurrentYear);

        RewardClubPage rewardClubPage = new RewardClubPage();
        rewardClubPage.goTo();

        rewardClubPage.getCurrentYearAnnualActivities().getNightsRow().verifyMaintainIsAchieved(nightsToMaintainGold,
                nightsToAchievePlatinum);

        AllEventsPage allEventsPage = new AllEventsPage();
        allEventsPage.goTo();
        verifyThat(allEventsPage.getGrid().getRow(UPGRADE_MAINTAIN, "RC - GOLD"), displayed(true));
    }

    @Test(dependsOnMethods = { "maintainGoldTierLevel" }, alwaysRun = true)
    public void verifyTierLevelExpiration()
    {
        RewardClubPage rewardClubPage = new RewardClubPage();
        rewardClubPage.goTo();
        rewardClubPage.getSummary().verify(OPEN, GOLD, 1);
    }
}
