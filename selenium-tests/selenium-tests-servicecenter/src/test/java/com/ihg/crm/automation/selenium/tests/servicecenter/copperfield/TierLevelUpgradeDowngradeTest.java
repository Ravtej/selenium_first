package com.ihg.crm.automation.selenium.tests.servicecenter.copperfield;

import static com.ihg.automation.common.DateUtils.DATE_FORMAT;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.enabled;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasDefault;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasText;
import static com.ihg.automation.selenium.servicecenter.pages.programs.rc.RewardClubSummary.EXPIRATION_MONTH_SHIFT;
import static org.hamcrest.Matchers.allOf;

import org.joda.time.LocalDate;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.ihg.automation.common.DateUtils;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.common.types.program.RewardClubLevel;
import com.ihg.automation.selenium.common.types.program.RewardClubLevelReason;
import com.ihg.automation.selenium.servicecenter.pages.LeftPanel;
import com.ihg.automation.selenium.servicecenter.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.servicecenter.pages.personal.PersonalInfoPage;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.RewardClubPage;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.RewardClubSummary;
import com.ihg.automation.selenium.servicecenter.pages.unit.GoodwillPopUp;
import com.ihg.crm.automation.selenium.tests.servicecenter.LoginLogout;

@Test(groups = { "copperfield" })
public class TierLevelUpgradeDowngradeTest extends LoginLogout
{
    private RewardClubSummary summary;

    private Member member = new Member();

    private final static String UPDATE_EXPIRATION_DATE = "UPDATE DGST.MBRSHP SET" //
            + " LST_ACTV_DT = TRUNC(SYSDATE-1),"//
            + " LST_UPDT_USR_ID='%2$s',"//
            + " LST_UPDT_TS = SYSDATE"//
            + " WHERE MBRSHP_ID='%1$s' AND LYTY_PGM_CD='PC'";

    @BeforeClass
    public void beforeClass()
    {
        member.setPersonalInfo(MemberPopulateHelper.getSimplePersonalInfo());
        member.addAddress(MemberPopulateHelper.getUnitedStatesAddress());
        member.addProgram(Program.RC);

        login();

        member = new EnrollmentPage().enroll(member);
        new GoodwillPopUp().goodwillPoints("1000");

    }

    @BeforeMethod
    public void upgradeDatesInDb()
    {
        jdbcTemplateUpdate.update(String.format(UPDATE_EXPIRATION_DATE, member.getRCProgramId(), lastUpdateUserId));

        new PersonalInfoPage().goTo(); // for re-read summary details

        RewardClubPage rewardClubPage = new RewardClubPage();
        rewardClubPage.goTo();
        summary = rewardClubPage.getSummary();
        verifyThat(summary.getLastActivityDate(), hasText(DateUtils.now().minusDays(1).toString(DATE_FORMAT)));
    }

    @Test
    public void upgradeToGold()
    {
        summary.setTierLevelWithExpiration(RewardClubLevel.GOLD, RewardClubLevelReason.BASE);
        verifyDates();
    }

    @Test(priority = 5)
    public void upgradeToPlatinum()
    {
        summary.setTierLevelWithExpiration(RewardClubLevel.PLTN, RewardClubLevelReason.BASE);
        verifyDates();
    }

    @Test(priority = 10)
    public void upgradeToSpire()
    {
        summary.setTierLevelWithExpiration(RewardClubLevel.SPRE, RewardClubLevelReason.BASE);
        verifyDates();
    }

    @Test(priority = 15)
    public void downgradeToPlatinum()
    {
        summary.setTierLevelWithExpiration(RewardClubLevel.PLTN, RewardClubLevelReason.BASE);
        verifyDates();
    }

    @Test(priority = 20)
    public void downgradeToGold()
    {
        summary.setTierLevelWithExpiration(RewardClubLevel.GOLD, RewardClubLevelReason.BASE);
        verifyDates();
    }

    @Test(priority = 25)
    public void downgradeToClub()
    {
        summary.setTierLevelWithoutExpiration(RewardClubLevel.CLUB, RewardClubLevelReason.BASE);

        LocalDate now = DateUtils.now();
        String expirationDate = now.plusMonths(EXPIRATION_MONTH_SHIFT).toString(DATE_FORMAT);
        String lastActivityDate = now.toString(DATE_FORMAT);

        verifyThat(summary.getBalanceExpirationDate(), hasText(expirationDate));
        verifyThat(summary.getLastActivityDate(), hasText(lastActivityDate));
        verifyThat(summary.getLastAdjusted(), hasDefault());

        verifyThat(summary.getExtend(), allOf(displayed(true), enabled(false)));
        verifyThat(new LeftPanel().getProgramInfoPanel().getPrograms().getRewardClubProgram().getExpirationDate(),
                allOf(displayed(true), hasText(expirationDate)));
    }

    private void verifyDates()
    {
        verifyThat(summary.getBalanceExpirationDate(), hasDefault());
        verifyThat(summary.getLastActivityDate(), hasText(DateUtils.now().toString(DATE_FORMAT)));
        verifyThat(summary.getLastAdjusted(), hasDefault());

        verifyThat(summary.getExtend(), allOf(displayed(true), enabled(false)));
        verifyThat(new LeftPanel().getProgramInfoPanel().getPrograms().getRewardClubProgram().getExpirationDate(),
                displayed(false));
    }

}
