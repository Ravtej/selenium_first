package com.ihg.crm.automation.selenium.tests.servicecenter.programinfo.ambassador;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.event.EventTransactionType.TierLevel.UPGRADE_MANUAL;
import static com.ihg.automation.selenium.common.member.AmbassadorUtils.AMBASSADOR_COMP_PROMO_$0;
import static com.ihg.automation.selenium.common.types.program.AmbassadorLevelReason.ROYAL_AMBASSADOR_CERTIFICATE;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasTextWithWait;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.isHighlightedAsInvalid;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static com.ihg.automation.selenium.matchers.component.ViewableMatcher.hasTextInView;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.ihg.automation.common.DateUtils;
import com.ihg.automation.selenium.common.Constant;
import com.ihg.automation.selenium.common.ProgramsGrid;
import com.ihg.automation.selenium.common.event.AllEventsRow;
import com.ihg.automation.selenium.common.event.AllEventsRowConverter;
import com.ihg.automation.selenium.common.member.AmbassadorOfferCode;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.order.Order;
import com.ihg.automation.selenium.common.order.OrderItem;
import com.ihg.automation.selenium.common.types.program.AmbassadorLevel;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.common.types.program.ProgramStatus;
import com.ihg.automation.selenium.common.types.program.ProgramStatusReason;
import com.ihg.automation.selenium.common.types.program.RewardClubLevel;
import com.ihg.automation.selenium.servicecenter.pages.LeftPanel;
import com.ihg.automation.selenium.servicecenter.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsGridRow;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsPage;
import com.ihg.automation.selenium.servicecenter.pages.event.level.ManualTierLevelChangeGridRowContainer;
import com.ihg.automation.selenium.servicecenter.pages.event.order.OrderSystemDetailsGridRowContainer;
import com.ihg.automation.selenium.servicecenter.pages.programs.RenewExtendProgramSummary;
import com.ihg.automation.selenium.servicecenter.pages.programs.amb.AmbassadorPage;
import com.ihg.crm.automation.selenium.tests.servicecenter.LoginLogout;

public class ManualUpgradeTierLevelRequiredReferringMemberTest extends LoginLogout
{
    private Member member = new Member();
    private Member referringMember = new Member();
    private Member closedMember = new Member();
    private AmbassadorPage ambassadorPage = new AmbassadorPage();
    private RenewExtendProgramSummary ambSummary;
    private AllEventsPage allEventsPage = new AllEventsPage();
    private AllEventsRow upgradeEventRow = new AllEventsRow(DateUtils.getFormattedDate(), UPGRADE_MANUAL,
            "AMB - ROYAL AMBASSADOR", "", "");
    private AllEventsRow orderEventRow;
    private Order order = new Order();

    @BeforeClass
    public void beforeClass()
    {
        closedMember.setPersonalInfo(MemberPopulateHelper.getSimplePersonalInfo());
        closedMember.addAddress(MemberPopulateHelper.getUnitedStatesAddress());
        closedMember.addProgram(Program.AMB);
        closedMember.setAmbassadorOfferCode(AmbassadorOfferCode.CHSRO);
        closedMember.setAmbassadorAmount(AMBASSADOR_COMP_PROMO_$0);
        member.setPersonalInfo(MemberPopulateHelper.getSimplePersonalInfo());
        member.addAddress(MemberPopulateHelper.getUnitedStatesAddress());
        member.addProgram(Program.AMB);
        member.setAmbassadorOfferCode(AmbassadorOfferCode.CHSRO);
        member.setAmbassadorAmount(AMBASSADOR_COMP_PROMO_$0);
        referringMember.setPersonalInfo(MemberPopulateHelper.getSimplePersonalInfo());
        referringMember.addAddress(MemberPopulateHelper.getUnitedStatesAddress());
        referringMember.addProgram(Program.AMB);
        referringMember.setAmbassadorAmount(AMBASSADOR_COMP_PROMO_$0);
        referringMember.setAmbassadorOfferCode(AmbassadorOfferCode.CHSRO);

        order.setShippingInfo(member);
        order.setDeliveryEmail(Constant.NOT_AVAILABLE);
        OrderItem orderItem = new OrderItem("FRA00", "NEW ROYAL AMBASSADOR KIT");
        orderItem.setDeliveryStatus("PROCESSING");
        order.getOrderItems().add(orderItem);

        orderEventRow = AllEventsRowConverter.convert(order);

        login();

        EnrollmentPage enrollPage = new EnrollmentPage();
        enrollPage.enroll(closedMember);
        new LeftPanel().goToNewSearch();
        enrollPage.enroll(referringMember);
        new LeftPanel().goToNewSearch();
        enrollPage.enroll(member);
    }

    @Test
    public void closeMember()
    {
        new LeftPanel().reOpenMemberProfile(closedMember);

        ambassadorPage.goTo();
        ambSummary = ambassadorPage.getSummary();
        ambSummary.setClosedStatus(ProgramStatusReason.CUSTOMER_REQUEST);
        ambSummary.verify(ProgramStatus.CLOSED, AmbassadorLevel.AMB);
    }

    @Test(dependsOnMethods = { "closeMember" }, alwaysRun = true)
    public void setUpgradeMemberInformation()
    {
        new LeftPanel().reOpenMemberProfile(member);

        ambassadorPage = new AmbassadorPage();
        ambassadorPage.goTo();
        ambSummary = ambassadorPage.getSummary();
        ambSummary.clickEdit();
        ambSummary.populateTierLevel(AmbassadorLevel.RAM, ROYAL_AMBASSADOR_CERTIFICATE);
    }

    @Test(dependsOnMethods = { "setUpgradeMemberInformation" })
    public void verifyNoReferringMember()
    {
        ambSummary.getReferringMember().clear();
        ambSummary.clickSave();
        verifyThat(ambSummary.getReferringMember(), isHighlightedAsInvalid(true));
    }

    @Test(dependsOnMethods = { "setUpgradeMemberInformation" })
    public void verifySelfReferringMember()
    {
        ambSummary.getReferringMember().clear();
        ambSummary.getReferringMember().typeAndWait(member.getRCProgramId());
        ambSummary.clickSave();
        verifyThat(ambSummary.getReferringMember(), isHighlightedAsInvalid(true));
        verifyThat(ambSummary.getReferringMemberName(), hasTextWithWait(member.getName().getNameOnPanel()));
    }

    @Test(dependsOnMethods = { "setUpgradeMemberInformation" })
    public void verifyClosedReferringMember()
    {
        ambSummary.getReferringMember().clear();
        ambSummary.getReferringMember().typeAndWait(closedMember.getRCProgramId());
        ambSummary.clickSave();
        verifyThat(ambSummary.getReferringMember(), isHighlightedAsInvalid(true));
        verifyThat(ambSummary.getReferringMemberName(), hasTextWithWait(closedMember.getName().getNameOnPanel()));
    }

    @Test(dependsOnMethods = { "setUpgradeMemberInformation", "verifyClosedReferringMember",
            "verifySelfReferringMember", "verifyNoReferringMember" }, alwaysRun = true)
    public void upgradeMemberLevel()
    {
        ambSummary.getReferringMember().clear();
        ambSummary.getReferringMember().typeAndWait(referringMember.getRCProgramId());
        verifyThat(ambSummary.getReferringMemberName(), hasTextWithWait(referringMember.getName().getNameOnPanel()));
        ambSummary.clickSave();
    }

    @Test(dependsOnMethods = { "upgradeMemberLevel" })
    public void verifyMemberLevelUpgrade()
    {
        ambassadorPage = new AmbassadorPage();
        ambSummary = ambassadorPage.getSummary();
        verifyThat(ambSummary.getTierLevel(), hasTextInView(AmbassadorLevel.RAM.getValue()));
        verifyThat(ambSummary.getTierLevelReason(), hasTextInView(ROYAL_AMBASSADOR_CERTIFICATE.getValue()));
        verifyThat(ambSummary.getReferringMember(), hasTextInView(referringMember.getRCProgramId()));
    }

    @Test(dependsOnMethods = { "verifyMemberLevelUpgrade" }, alwaysRun = true)
    public void verifyLeftPanelLevels()
    {
        ProgramsGrid grid = new LeftPanel().getProgramInfoPanel().getPrograms();
        verifyThat(grid.getProgram(Program.AMB).getLevelCode(), hasText(AmbassadorLevel.RAM.getCode()));
        verifyThat(grid.getRewardClubProgram().getLevelCode(), hasText(RewardClubLevel.SPRE.getCode()));
    }

    @DataProvider(name = "verifyAllEventsGridRowProvider")
    public Object[][] verifyAllEventsGridRowProvider()
    {
        return new Object[][] { { orderEventRow }, { upgradeEventRow } };
    }

    @Test(dataProvider = "verifyAllEventsGridRowProvider", dependsOnMethods = {
            "verifyLeftPanelLevels" }, alwaysRun = true)
    public void verifyAllEventsGridRow(AllEventsRow eventRow)
    {
        allEventsPage.goTo();
        AllEventsGridRow row = allEventsPage.getGrid().getRow(eventRow.getTransType(), eventRow.getDetail());
        verifyThat(row, displayed(true));
        row.verify(eventRow);
    }

    @Test(dependsOnMethods = { "verifyAllEventsGridRow" }, alwaysRun = true)
    public void verifyAMBUpgradeEventDetails()
    {
        allEventsPage.goTo();
        AllEventsGridRow row = allEventsPage.getGrid().getRow(upgradeEventRow.getTransType(),
                upgradeEventRow.getDetail());
        ManualTierLevelChangeGridRowContainer container = row.expand(ManualTierLevelChangeGridRowContainer.class);
        container.verify(referringMember.getRCProgramId(), ROYAL_AMBASSADOR_CERTIFICATE.getValue(), "FRA00", helper);
    }

    @Test(dependsOnMethods = { "verifyAMBUpgradeEventDetails" }, alwaysRun = true)
    public void verifyAMBOrderEventDetails()
    {
        allEventsPage.goTo();
        AllEventsGridRow row = allEventsPage.getGrid().getRow(orderEventRow.getTransType(), orderEventRow.getDetail());

        OrderSystemDetailsGridRowContainer container = row.expand(OrderSystemDetailsGridRowContainer.class);
        verifyThat(container.getDetails(), displayed(true));
        container.verify(order);
    }

}
