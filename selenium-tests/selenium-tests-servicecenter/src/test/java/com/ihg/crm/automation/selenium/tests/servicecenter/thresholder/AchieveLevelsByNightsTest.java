package com.ihg.crm.automation.selenium.tests.servicecenter.thresholder;

import static com.ihg.automation.common.DateUtils.currentYear;
import static com.ihg.automation.common.DateUtils.now;
import static com.ihg.automation.selenium.assertions.Assume.assumeThat;
import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyNoError;
import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifySuccess;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.event.EventTransactionType.TierLevel.UPGRADE_ACHIEVE;
import static com.ihg.automation.selenium.common.stay.Stay.MIN_DAYS_SHIFT;
import static com.ihg.automation.selenium.common.stay.Stay.QUALIFYING_NIGHTS_MAX_AMOUNT_FOR_STAY;
import static com.ihg.automation.selenium.common.types.program.ProgramStatus.OPEN;
import static com.ihg.automation.selenium.common.types.program.RewardClubLevel.CLUB;
import static com.ihg.automation.selenium.common.types.program.RewardClubLevel.GOLD;
import static com.ihg.automation.selenium.common.types.program.RewardClubLevel.PLTN;
import static com.ihg.automation.selenium.common.types.program.RewardClubLevel.SPRE;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasTextAsInt;
import static com.ihg.automation.selenium.matchers.component.GridSize.size;
import static com.ihg.automation.selenium.matchers.date.IsAfterDate.isAfter;
import static org.hamcrest.Matchers.not;

import org.joda.time.LocalDate;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.common.DateUtils;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.stay.Stay;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.servicecenter.pages.LeftPanel;
import com.ihg.automation.selenium.servicecenter.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsPage;
import com.ihg.automation.selenium.servicecenter.pages.event.stay.AdjustStayPopUp;
import com.ihg.automation.selenium.servicecenter.pages.event.stay.StayEventsPage;
import com.ihg.automation.selenium.servicecenter.pages.event.stay.StayInfoEdit;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.RewardClubPage;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.RewardClubSummary;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.TierLevelBenefitsDetailsPopUp;
import com.ihg.automation.selenium.tests.common.RulesDBUtils;
import com.ihg.crm.automation.selenium.tests.servicecenter.LoginLogout;

public class AchieveLevelsByNightsTest extends LoginLogout
{
    private Stay stay = new Stay();
    private static int nightsToAchieveGold;
    private static int nightsToAchievePlatinum;
    private static int nightsToAchieveSpire;
    private static int nightsFromGoldToPlatinum;
    private static int nightsFromPlatinumToSpire;

    @BeforeClass
    public void before()
    {
        assumeThat(now(), isAfter(new LocalDate(currentYear(), 1, 3)), "Checkout date should be in current year");

        RulesDBUtils rulesDBUtils = new RulesDBUtils(jdbcTemplate);

        nightsToAchieveGold = rulesDBUtils.getCurrentYearRuleNights(CLUB, GOLD);
        nightsToAchievePlatinum = rulesDBUtils.getCurrentYearRuleNights(CLUB, PLTN);
        nightsToAchieveSpire = rulesDBUtils.getCurrentYearRuleNights(CLUB, SPRE);

        stay.setHotelCode("ATLCP");
        stay.setCheckInOutByNights(nightsToAchieveGold);
        stay.setAvgRoomRate(15.00);
        stay.setRateCode("Test");

        Member member = new Member();
        member.addProgram(Program.RC);
        member.setPersonalInfo(MemberPopulateHelper.getSimplePersonalInfo());
        member.addAddress(MemberPopulateHelper.getUnitedStatesAddress());

        login();

        new EnrollmentPage().enroll(member);

        new LeftPanel().verifyRCLevel(CLUB);
    }

    @Test
    public void verifyAnnualActivitiesForClub()
    {
        RewardClubPage rewardClubPage = new RewardClubPage();
        rewardClubPage.goTo();
        rewardClubPage.getCurrentYearAnnualActivities().getNightsRow().verify(0, nightsToAchieveGold);
    }

    @Test(dependsOnMethods = { "verifyAnnualActivitiesForClub" }, alwaysRun = true)
    public void achieveGoldTierLevel()
    {
        new StayEventsPage().createStay(stay);

        new LeftPanel().verifyRCLevel(GOLD);

        RewardClubPage rewardClubPage = new RewardClubPage();
        rewardClubPage.goTo();

        rewardClubPage.getCurrentYearAnnualActivities().getNightsRow().verify(nightsToAchieveGold,
                nightsToAchievePlatinum);

        AllEventsPage allEventsPage = new AllEventsPage();
        allEventsPage.goTo();

        verifyThat(allEventsPage.getGrid().getRow(UPGRADE_ACHIEVE, "RC - GOLD"), displayed(true));
    }

    @Test(dependsOnMethods = { "achieveGoldTierLevel" }, alwaysRun = true)
    public void verifyTierLevelExpiration()
    {
        RewardClubPage rewardClubPage = new RewardClubPage();
        rewardClubPage.goTo();
        rewardClubPage.getSummary().verify(OPEN, GOLD, 1);
    }

    @Test(dependsOnMethods = { "verifyTierLevelExpiration" }, alwaysRun = true)
    public void achievePlatinumTierLevel()
    {
        adjustStayToAchieveNextLevel(nightsToAchievePlatinum);

        new LeftPanel().verifyRCLevel(PLTN);

        RewardClubPage rewardClubPage = new RewardClubPage();
        rewardClubPage.goTo();

        rewardClubPage.getCurrentYearAnnualActivities().getNightsRow().verify(nightsToAchievePlatinum,
                nightsToAchieveSpire);

        AllEventsPage allEventsPage = new AllEventsPage();
        allEventsPage.goTo();

        verifyThat(allEventsPage.getGrid().getRow(UPGRADE_ACHIEVE, "RC - PLATINUM"), displayed(true));
    }

    @Test(dependsOnMethods = { "achievePlatinumTierLevel" }, alwaysRun = true)
    public void achieveSpireTierLevel()
    {
        assumeThat(now(),
                isAfter(new LocalDate(currentYear(), 1,
                        nightsToAchieveSpire - QUALIFYING_NIGHTS_MAX_AMOUNT_FOR_STAY - MIN_DAYS_SHIFT)),
                "Checkout date should be in current year");

        adjustStayToAchieveNextLevel(nightsToAchieveSpire - QUALIFYING_NIGHTS_MAX_AMOUNT_FOR_STAY);

        Stay stayForMaxAmountNights = stay.clone();
        stayForMaxAmountNights.setCheckInOutByNights(QUALIFYING_NIGHTS_MAX_AMOUNT_FOR_STAY,
                nightsToAchieveSpire - QUALIFYING_NIGHTS_MAX_AMOUNT_FOR_STAY);

        new StayEventsPage().createStay(stayForMaxAmountNights);

        new LeftPanel().verifyRCLevel(SPRE);

        RewardClubPage rewardClubPage = new RewardClubPage();
        rewardClubPage.goTo();

        RewardClubSummary summary = rewardClubPage.getSummary();
        summary.verify(OPEN, SPRE, 1);

        TierLevelBenefitsDetailsPopUp tierLevelBenefitsDetailsPopUp = summary.openTierLevelBenefitsDetailsPopUp();
        verifyThat(tierLevelBenefitsDetailsPopUp.getBenefitsGrid(), size(not(0)));
        tierLevelBenefitsDetailsPopUp.close();

        rewardClubPage.getCurrentYearAnnualActivities().getNightsRow().verify(hasTextAsInt(nightsToAchieveSpire),
                displayed(false));

        AllEventsPage allEventsPage = new AllEventsPage();
        allEventsPage.goTo();

        verifyThat(allEventsPage.getGrid().getRow(UPGRADE_ACHIEVE, "RC - SPIRE"), displayed(true));
    }

    private void adjustStayToAchieveNextLevel(int nights)
    {
        StayEventsPage stayEventsPage = new StayEventsPage();
        stayEventsPage.goTo();
        AdjustStayPopUp adjustStayPopUp = stayEventsPage.getGuestGrid().getRow(1).openAdjustPopUp();

        StayInfoEdit stayInfo = adjustStayPopUp.getStayDetailsTab().getStayDetails().getStayInfo();
        stayInfo.getCheckIn().typeAndWait(DateUtils.getDateBackward(nights - MIN_DAYS_SHIFT));
        adjustStayPopUp.clickSaveChanges(verifyNoError(), verifySuccess(StayEventsPage.STAY_SUCCESS_ADJUSTED));
        stayEventsPage.waitUntilFirstStayProcess();
    }
}
