package com.ihg.crm.automation.selenium.tests.servicecenter.deposit;

import static com.ihg.automation.selenium.common.event.EventTransactionType.Deposit.CO_PARTNER;
import static com.ihg.automation.selenium.common.event.EventTransactionType.Order.ORDER_SYSTEM;
import static com.ihg.automation.selenium.common.event.EventTransactionType.TierLevel.UPGRADE_ACHIEVE;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.ihg.automation.common.DateUtils;
import com.ihg.automation.common.RandomUtils;
import com.ihg.automation.selenium.common.Constant;
import com.ihg.automation.selenium.common.deposit.Deposit;
import com.ihg.automation.selenium.common.event.AllEventsRow;
import com.ihg.automation.selenium.common.event.AllEventsRowConverter;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.types.Partner;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.common.types.program.ProgramStatus;
import com.ihg.automation.selenium.common.types.program.RewardClubLevel;
import com.ihg.automation.selenium.common.types.program.RewardClubLevelReason;
import com.ihg.automation.selenium.servicecenter.pages.LeftPanel;
import com.ihg.automation.selenium.servicecenter.pages.annual.AnnualActivityPage;
import com.ihg.automation.selenium.servicecenter.pages.deposit.CreateDepositPopUp;
import com.ihg.automation.selenium.servicecenter.pages.deposit.DepositDetailsPopUp;
import com.ihg.automation.selenium.servicecenter.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsGrid;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsGridRow;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsPage;
import com.ihg.automation.selenium.servicecenter.pages.event.copartner.CoPartnerEventGridRow;
import com.ihg.automation.selenium.servicecenter.pages.event.copartner.CoPartnerEventsPage;
import com.ihg.automation.selenium.servicecenter.pages.event.deposit.DepositGridRowContainer;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.RewardClubPage;
import com.ihg.crm.automation.selenium.tests.servicecenter.LoginLogout;

public class DepositQualifyingPointsTest extends LoginLogout
{
    private Member member = new Member();
    private Deposit testDeposit = new Deposit();

    private CreateDepositPopUp createDepositPopUp = new CreateDepositPopUp();
    private AllEventsGrid allEventsGrid;
    private DepositGridRowContainer depositGridRowContainer;

    private AllEventsRow orderSystemEvent = new AllEventsRow(DateUtils.getFormattedDate(), ORDER_SYSTEM,
            "16 GOLD ACHIEVED KIT (POINTS)", "", "");
    private AllEventsRow upgradeAchieveEvent = new AllEventsRow(DateUtils.getFormattedDate(), UPGRADE_ACHIEVE,
            "RC - GOLD", "", "");

    @BeforeClass
    public void beforeClass()
    {
        member.addProgram(Program.RC);
        member.setPersonalInfo(MemberPopulateHelper.getSimplePersonalInfo());
        member.addAddress(MemberPopulateHelper.getUnitedStatesAddress());

        testDeposit.setTransactionId("DBWOLF");
        testDeposit.setTransactionType(CO_PARTNER);
        testDeposit.setTransactionName("PC MALL- WOLF CAMERA");
        testDeposit.setTransactionDescription("PC MALL- WOLF CAMERA");
        testDeposit.setHotel("ATLCP");
        testDeposit.setCheckInDate(DateUtils.getDateBackward(5));
        testDeposit.setCheckOutDate(DateUtils.getDateBackward(2));
        testDeposit.setPartnerTransactionDate(DateUtils.getDateBackward(1));
        testDeposit.setPartnerComment("SC UI test automation" + DateUtils.now());
        testDeposit.setOrderTrackingNumber(RandomUtils.getRandomNumber(8));
        testDeposit.setLoyaltyUnitsAmount("20000");
        testDeposit.setLoyaltyUnitsType(Constant.RC_POINTS);
        testDeposit.setCoPartner(Partner.DBG);
        testDeposit.setBillingTo(Partner.DBG);

        login();
        member = new AnnualActivityPage().captureCounters(new EnrollmentPage().enroll(member), jdbcTemplate);
        new LeftPanel().verifyRCLevel(RewardClubLevel.CLUB);
    }

    @Test
    public void openCreateDepositPopUp()
    {
        createDepositPopUp.goTo();
    }

    @Test(dependsOnMethods = { "openCreateDepositPopUp" })
    public void verifyCreateDepositPopUpControls()
    {
        createDepositPopUp.verifyControls();
    }

    @Test(dependsOnMethods = { "verifyCreateDepositPopUpControls" }, alwaysRun = true)
    public void createDeposit()
    {
        createDepositPopUp.createDeposit(testDeposit);
    }

    @Test(dependsOnMethods = { "createDeposit" })
    public void verifyCoPartnerEventOnCoPartnerPage()
    {
        CoPartnerEventGridRow row = new CoPartnerEventsPage().getGrid().getRow(1);
        row.verify(testDeposit);
        depositGridRowContainer = row.expand(DepositGridRowContainer.class);
        depositGridRowContainer.verify(testDeposit, helper);
    }

    @Test(dependsOnMethods = { "verifyCoPartnerEventOnCoPartnerPage" })
    public void verifyDepositDetailsFromCoPartnerPage()
    {
        depositGridRowContainer.clickDetails();

        verifyDepositDetails();
    }

    @Test(dependsOnMethods = { "verifyDepositDetailsFromCoPartnerPage" }, alwaysRun = true)
    public void closeCoPartnersDepositDetailsPopUp()
    {
        new DepositDetailsPopUp().close();
    }

    @Test(dependsOnMethods = { "closeCoPartnersDepositDetailsPopUp" }, alwaysRun = true)
    public void goToAllEventPage()
    {
        AllEventsPage allEventsPage = new AllEventsPage();
        allEventsPage.goTo();
        allEventsGrid = allEventsPage.getGrid();
    }

    @DataProvider(name = "depositSubEvents")
    private Object[][] depositSubEvents()
    {
        return new Object[][] { { orderSystemEvent }, { upgradeAchieveEvent } };
    }

    @Test(dataProvider = "depositSubEvents", dependsOnMethods = { "goToAllEventPage" })
    public void verifyCoPartnerSubEvent(AllEventsRow eventRow)
    {
        allEventsGrid.getRow(eventRow.getTransType()).verify(eventRow);
    }

    @Test(dependsOnMethods = { "goToAllEventPage" }, alwaysRun = true)
    public void verifyCoPartnerEventWithDetailsInGrid()
    {
        AllEventsGridRow row = allEventsGrid.getRow(testDeposit.getTransactionType());
        row.verify(AllEventsRowConverter.convert(testDeposit));
        depositGridRowContainer = row.expand(DepositGridRowContainer.class);
        depositGridRowContainer.verify(testDeposit, helper);
    }

    @Test(dependsOnMethods = { "verifyCoPartnerEventWithDetailsInGrid" })
    public void verifyDepositDetailsFromAllEventsPage()
    {
        depositGridRowContainer.clickDetails();
        verifyDepositDetails();
    }

    @Test(dependsOnMethods = { "verifyDepositDetailsFromAllEventsPage" }, alwaysRun = true)
    public void closeAllEventsCoPartnersDepositDetailsPopUp()
    {
        new DepositDetailsPopUp().close();
    }

    @Test(dependsOnMethods = { "closeAllEventsCoPartnersDepositDetailsPopUp" }, alwaysRun = true)
    public void verifyResultingTierLevelAndQualifyingPoints()
    {
        RewardClubPage rewardClubPage = new RewardClubPage();
        rewardClubPage.goTo();
        rewardClubPage.getSummary().verify(ProgramStatus.OPEN, RewardClubLevel.GOLD, RewardClubLevelReason.POINTS);
        rewardClubPage.getCurrentYearAnnualActivities().getPointsRow().verifyMaintainIsAchieved(20000,
                achievePlatinumPoints);
    }

    @Test(dependsOnMethods = { "verifyResultingTierLevelAndQualifyingPoints" }, alwaysRun = true)
    public void verifyResultingLeftPanelProgramInformation()
    {
        new LeftPanel().getProgramInfoPanel().getPrograms().getRewardClubProgram().verify(RewardClubLevel.GOLD,
                "20,000");
    }

    @Test(dependsOnMethods = { "verifyResultingLeftPanelProgramInformation" }, alwaysRun = true)
    public void verifyResultingLifetimeActivities()
    {
        member.getLifetimeActivity().setBonusUnits(20000);
        member.getLifetimeActivity().setTotalUnits(20000);
        member.getLifetimeActivity().setCurrentBalance(20000);
        member.getAnnualActivity().setTotalUnits(20000);
        member.getAnnualActivity().setTotalQualifiedUnits(20000);

        new AnnualActivityPage().verifyCounters(member);
    }

    private void verifyDepositDetails()
    {
        DepositDetailsPopUp depositDetailsPopUp = new DepositDetailsPopUp();
        depositDetailsPopUp.getDepositDetailsTab().getDepositDetails().verify(testDeposit);
        depositDetailsPopUp.getEarningDetailsTab().verifyForBaseUnitsEvent("20,000", "20,000");
        depositDetailsPopUp.getBillingDetailsTab().verifyBaseUSDBilling(CO_PARTNER, "200", testDeposit.getBillingTo());
    }
}
