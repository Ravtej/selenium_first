package com.ihg.crm.automation.selenium.tests.servicecenter.programinfo.ambassador;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.event.EventTransactionType.Membership.MEMBERSHIP_RENEWAL;
import static com.ihg.automation.selenium.common.member.AmbassadorUtils.RENEW_GOLD_AMBASSADOR_KIT_200;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.isDisplayedWithWait;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.common.DateUtils;
import com.ihg.automation.selenium.common.catalog.CatalogItem;
import com.ihg.automation.selenium.common.member.AmbassadorJdbcUtils;
import com.ihg.automation.selenium.common.member.AmbassadorUtils;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.pages.PaymentMethod;
import com.ihg.automation.selenium.common.payment.CheckPayment;
import com.ihg.automation.selenium.common.payment.CheckPaymentContainer;
import com.ihg.automation.selenium.common.payment.PaymentDetailsPopUp;
import com.ihg.automation.selenium.gwt.components.Mode;
import com.ihg.automation.selenium.servicecenter.pages.LeftPanel;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsGridRow;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsPage;
import com.ihg.automation.selenium.servicecenter.pages.event.enroll.MembershipDetails;
import com.ihg.automation.selenium.servicecenter.pages.event.enroll.MembershipDetailsPopUp;
import com.ihg.automation.selenium.servicecenter.pages.event.enroll.MembershipDetailsTab;
import com.ihg.automation.selenium.servicecenter.pages.event.enroll.MembershipGridRowContainer;
import com.ihg.automation.selenium.servicecenter.pages.programs.amb.AmbassadorPage;

public class RenewByCheckTest extends AmbassadorCommon
{
    public static final CatalogItem RENEW_AWARD = AmbassadorUtils.AMBASSADOR_RENEWAL_$200;

    private AmbassadorPage ambassadorPage = new AmbassadorPage();
    private MembershipDetailsPopUp membershipDetailsPopUp;
    private AllEventsPage allEventsPage = new AllEventsPage();
    private MembershipGridRowContainer container;
    private MembershipDetails membershipDetails;
    private final static String DEPOSIT_DATE = DateUtils.getFormattedDate();
    private final static String DEPOSIT_NUMBER = "1234567";
    private final static String ISSUING_BANK = "Issuing bank";
    private final static String NAME_ON_ACCOUNT = "Name on account";
    private final static String CHECK_NUMBER = "7788999000";
    private CheckPayment checkPaymentItem = new CheckPayment(RENEW_AWARD, CHECK_NUMBER, NAME_ON_ACCOUNT, ISSUING_BANK,
            DEPOSIT_DATE, DEPOSIT_NUMBER);
    private PaymentDetailsPopUp paymentDetailsPopUp = new PaymentDetailsPopUp();
    private CheckPaymentContainer checkPayment;
    private int daysShift;

    @BeforeClass
    public void beforeClass()
    {
        login();

        Member member = enrollAMBmember();
        daysShift = new AmbassadorJdbcUtils(jdbcTemplateUpdate, lastUpdateUserId).updateForRenew(member);
        new LeftPanel().reOpenMemberProfile(member);
        ambassadorPage.goTo();
    }

    @Test
    public void verifyRenewButtonAndExpirationDate()
    {
        ambassadorPage.getSummary().verifyRenewAndExtendButtons(true, true);
    }

    @Test(dependsOnMethods = "verifyRenewButtonAndExpirationDate", alwaysRun = true)
    public void verifyCheckPaymentDetailsPopUp()
    {
        ambassadorPage.getSummary().clickRenew();
        paymentDetailsPopUp.selectAmountAndPaymentMethod(RENEW_AWARD, PaymentMethod.CHECK);
        checkPayment = paymentDetailsPopUp.getCheckPayment();
        verifyThat(checkPayment.getCheckNumber(), displayed(true));
        verifyThat(checkPayment.getNameOnAccount(), displayed(true));
        verifyThat(checkPayment.getIssuingBank(), displayed(true));
        verifyThat(checkPayment.getDepositDate(), displayed(true));
        verifyThat(checkPayment.getDepositNumber(), displayed(true));
    }

    @Test(dependsOnMethods = "verifyCheckPaymentDetailsPopUp", alwaysRun = true)
    public void populateCheckPaymentDetailsPopUp()
    {
        checkPayment = paymentDetailsPopUp.getCheckPayment();
        checkPayment.populate(checkPaymentItem);
        paymentDetailsPopUp.clickSubmitNoError();
    }

    @Test(dependsOnMethods = "populateCheckPaymentDetailsPopUp", alwaysRun = true)
    public void verifyExpiredDate()
    {
        ambassadorPage.getSummary().verifyExpDateAfterRenew(daysShift);
    }

    @Test(dependsOnMethods = "verifyExpiredDate", alwaysRun = true)
    public void verifyAllEventsContainer()
    {
        allEventsPage.goTo();
        AllEventsGridRow row = allEventsPage.getGrid().getRow(MEMBERSHIP_RENEWAL);

        container = row.expand(MembershipGridRowContainer.class);
        membershipDetails = container.getMembershipDetails();
        membershipDetails.getPaymentDetails().verify(checkPaymentItem, Mode.VIEW);
        membershipDetails.verifyScSource(helper);
    }

    @Test(dependsOnMethods = "verifyAllEventsContainer", alwaysRun = true)
    public void openMembershipDetailsPopUpVerify()
    {
        container.clickDetails();
        membershipDetailsPopUp = new MembershipDetailsPopUp();
        verifyThat(membershipDetailsPopUp, isDisplayedWithWait());
    }

    @Test(dependsOnMethods = "openMembershipDetailsPopUpVerify", alwaysRun = true)
    public void verifyAllEventsMembershipDetailsTab()
    {
        MembershipDetailsTab membershipDetailsTab = membershipDetailsPopUp.getMembershipDetailsTab();
        membershipDetailsTab.goTo();
        membershipDetails = membershipDetailsTab.getMembershipDetails();
        membershipDetails.getPaymentDetails().verify(checkPaymentItem, Mode.VIEW);
        membershipDetails.verifyScSource(helper);
    }

    @Test(dependsOnMethods = "verifyAllEventsMembershipDetailsTab", alwaysRun = true)
    public void verifyAllEventsEarningDetailsTab()
    {
        verifyEarningDetailsTab(RENEW_AWARD, RENEW_GOLD_AMBASSADOR_KIT_200);
    }

    @Test(dependsOnMethods = { "verifyAllEventsEarningDetailsTab" }, alwaysRun = true)
    public void validateBillingDetailsTab()
    {
        validateBillingDetails(RENEW_AWARD);
    }
}
