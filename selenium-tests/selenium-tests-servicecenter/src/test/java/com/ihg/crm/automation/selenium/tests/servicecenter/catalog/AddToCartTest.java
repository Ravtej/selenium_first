package com.ihg.crm.automation.selenium.tests.servicecenter.catalog;

import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyError;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasEmptyText;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasTextAsInt;
import static com.ihg.automation.selenium.matchers.component.GridSize.size;
import static com.ihg.automation.selenium.matchers.component.HasSelectItems.hasSelectItems;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.number.OrderingComparison.lessThanOrEqualTo;

import java.util.List;
import java.util.Map;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.google.common.collect.ImmutableList;
import com.ihg.automation.selenium.common.catalog.CatalogItem;
import com.ihg.automation.selenium.servicecenter.pages.GuestSearch;
import com.ihg.automation.selenium.servicecenter.pages.TopMenu;
import com.ihg.automation.selenium.servicecenter.pages.catalog.CatalogItemDetailsPopUp;
import com.ihg.automation.selenium.servicecenter.pages.catalog.CatalogItemsGrid;
import com.ihg.automation.selenium.servicecenter.pages.catalog.CatalogPage;
import com.ihg.automation.selenium.servicecenter.pages.catalog.CatalogSearch;
import com.ihg.automation.selenium.servicecenter.pages.order.OrderSummaryPopUp;

public class AddToCartTest extends CatalogCommon
{
    private CatalogPage catalogPage = new CatalogPage();
    private CatalogItemDetailsPopUp catalogItemDetailsPopUp = new CatalogItemDetailsPopUp();
    private OrderSummaryPopUp orderSummaryPopUp;
    private TopMenu menu = new TopMenu();
    private GuestSearch guestSearch = new GuestSearch();
    private List<Map<String, Object>> listMap;
    private static String COLOR_SIZE_ITEM_ID = "GF300";
    private static List<String> colorList;
    private static List<String> sizeList;
    private static String points;
    private CatalogSearch catalogSearch;
    private final static String COLOR = "SELECT DISTINCT COLOR_NM, I.AWD_ID"//
            + " FROM CATLG.COLOR_TYP_XLAT X "//
            + " INNER JOIN CATLG.AWD_COLOR CA ON CA.COLOR_TYP_CD = X.COLOR_TYP_CD"//
            + " INNER JOIN CATLG.AWD I ON CA.AWD_KEY = I.AWD_KEY"//
            + " WHERE AWD_ID = 'GF300'"//
            + " AND LOCALE_CD = 'en-US'";

    private final static String SIZE = "SELECT DISTINCT SIZE_NM, X.SIZE_TYP_CD, I.AWD_ID"//
            + " FROM CATLG.SIZE_TYP_XLAT X"//
            + " INNER JOIN CATLG.AWD_SIZE CA ON CA.SIZE_TYP_CD = X.SIZE_TYP_CD"//
            + " INNER JOIN CATLG.AWD I ON CA.AWD_KEY = I.AWD_KEY"//
            + " WHERE AWD_ID = 'GF300'"//
            + " AND LOCALE_CD = 'en-US'";

    @BeforeClass
    public void beforeClass()
    {
        CatalogItem catalogItemWithColor = getCatalogItemWithColor();
        colorList = catalogItemWithColor.getColorList();

        CatalogItem catalogItemWithSize = getCatalogItemWithSize();
        sizeList = catalogItemWithSize.getSizeList();

        Map<String, Object> map = jdbcTemplate.queryForMap(MEMBER_ID);

        points = map.get("POINTS").toString();
        String memberId = map.get("MBRSHP_ID").toString();

        login();

        guestSearch.byMemberNumber(memberId);
    }

    @Test
    public void openCatalog()
    {
        catalogPage.goTo();

        verifyCatalogFields();

        clearSearchCriteria();
        verifyThat(catalogSearch.getLoyaltyUnitCost().getLoyaltyUnitCostMax(), hasEmptyText());
    }

    @Test(dependsOnMethods = { "openCatalog" }, alwaysRun = true)
    public void closeCatalog()
    {
        catalogPage.clickClose();
        verifyThat(catalogPage, displayed(false));

        catalogPage.goTo();
        verifyThat(catalogPage, displayed(true));
        verifyCatalogFields();
    }

    @Test(dependsOnMethods = { "closeCatalog" }, alwaysRun = true)
    public void searchWithPrePopulatedFields()
    {
        catalogPage.getButtonBar().clickSearch();

        CatalogItemsGrid catalogGrid = catalogPage.getCatalogGrid();
        verifyThat(catalogGrid, size(10));
        catalogGrid.verifyUnitCost(hasTextAsInt(lessThanOrEqualTo(Integer.parseInt(points))));
    }

    @Test(dependsOnMethods = { "searchWithPrePopulatedFields" }, alwaysRun = true)
    public void verifyOpenOrderSummaryPopUp()
    {
        catalogPage.searchByItemId(ITEM_ID);
        catalogItemDetailsPopUp = catalogPage.openCatalogItemDetailsPopUp();

        catalogItemDetailsPopUp.getQuantity().select(1);

        catalogItemDetailsPopUp.openOrderSummaryPopUp();
    }

    @Test(dependsOnMethods = { "verifyOpenOrderSummaryPopUp" }, alwaysRun = true)
    public void continueShopping()
    {
        orderSummaryPopUp = new OrderSummaryPopUp();
        orderSummaryPopUp.clickContinueShopping();
        verifyThat(orderSummaryPopUp, displayed(false));
        verifyThat(catalogPage, displayed(true));
        menu.getShoppingCart().clickAndWait();

        orderSummaryPopUp.flush();
        verifyThat(orderSummaryPopUp, displayed(true));
    }

    @Test(dependsOnMethods = { "continueShopping" }, alwaysRun = true)
    public void removeItemFromShoppingCart()
    {
        orderSummaryPopUp = new OrderSummaryPopUp();

        verifyThat(orderSummaryPopUp.getRow(1).getItemDescription().getItemID(), hasText(ITEM_ID));

        orderSummaryPopUp.clickRemove();
        verifyThat(orderSummaryPopUp, hasText(containsString("Shopping Cart is empty.")));
        orderSummaryPopUp.clickCheckout(verifyError(hasText(containsString("Shopping Cart is empty."))));
        orderSummaryPopUp.clickContinueShopping();
        verifyThat(orderSummaryPopUp, displayed(false));
        verifyThat(menu.getShoppingCart(), displayed(false));
    }

    @Test(dependsOnMethods = { "removeItemFromShoppingCart" }, alwaysRun = true)
    public void notEnoughLoyaltyUnits()
    {
        catalogPage.search(null, "PS251", null, null, null);

        catalogItemDetailsPopUp = catalogPage.openCatalogItemDetailsPopUp();

        catalogItemDetailsPopUp.clickAddToCart(
                verifyError(hasText(containsString("Customer not Eligible for Item, not enough Loyalty Units"))));
        catalogItemDetailsPopUp.close();
        verifyThat(catalogItemDetailsPopUp, displayed(false));
    }

    @Test(dependsOnMethods = { "notEnoughLoyaltyUnits" }, alwaysRun = true)
    public void verifyColor()
    {
        catalogPage.searchByItemId(COLOR_SIZE_ITEM_ID);

        catalogItemDetailsPopUp = catalogPage.openCatalogItemDetailsPopUp();

        verifyThat(catalogItemDetailsPopUp.getColor(), hasSelectItems(colorList));

        orderSummaryPopUp = catalogItemDetailsPopUp.openOrderSummaryPopUp();

        verifyThat(orderSummaryPopUp.getRow(1).getItemDescription().getColor(), hasSelectItems(colorList));

        orderSummaryPopUp.clickRemove();
        orderSummaryPopUp.clickContinueShopping();
    }

    @Test(dependsOnMethods = { "verifyColor" }, alwaysRun = true)
    public void verifySize()
    {
        catalogPage.searchByItemId(COLOR_SIZE_ITEM_ID);

        catalogItemDetailsPopUp = catalogPage.openCatalogItemDetailsPopUp();

        verifyThat(catalogItemDetailsPopUp.getSize(), hasSelectItems(sizeList));

        orderSummaryPopUp = catalogItemDetailsPopUp.openOrderSummaryPopUp();

        verifyThat(orderSummaryPopUp.getRow(1).getItemDescription().getSize(), hasSelectItems(sizeList));

    }

    private CatalogItem getCatalogItemWithColor()
    {
        listMap = jdbcTemplate.queryForList(COLOR);

        CatalogItem.Builder builder = CatalogItem.builder();
        ImmutableList.Builder<String> listBuilder = ImmutableList.builder();

        for (Map<String, Object> aListMap : listMap)
        {
            listBuilder.add(aListMap.get("COLOR_NM").toString());
        }
        builder.colorList(listBuilder.build());
        builder.itemId(listMap.get(0).get("AWD_ID").toString());

        return builder.build();
    }

    private CatalogItem getCatalogItemWithSize()
    {
        listMap = jdbcTemplate.queryForList(SIZE);
        CatalogItem.Builder builder = CatalogItem.builder();
        ImmutableList.Builder<String> listBuilder = ImmutableList.builder();

        for (Map<String, Object> aListMap : listMap)
        {
            listBuilder.add(aListMap.get("SIZE_NM").toString());
        }
        builder.sizeList(listBuilder.build());
        builder.itemId(listMap.get(0).get("AWD_ID").toString());

        return builder.build();
    }

    private void verifyCatalogFields()
    {
        catalogSearch = catalogPage.getSearchFields();

        verifyThat(catalogSearch.getLoyaltyUnitCost().getLoyaltyUnitCostMax(), hasText(points));

        verifyThat(catalogSearch.getCatalogID(), hasEmptyText());
        verifyThat(catalogSearch.getItemID(), hasEmptyText());
        verifyThat(catalogSearch.getItemName(), hasEmptyText());
        verifyThat(catalogSearch.getLoyaltyUnitCost().getLoyaltyUnitCostMin(), hasEmptyText());
    }
}
