package com.ihg.crm.automation.selenium.tests.servicecenter.account;

import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyNoError;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.types.program.TierLevelFlag.CUSTOMER_RETENTION;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static org.hamcrest.Matchers.containsString;

import java.util.ArrayList;
import java.util.List;

import org.hamcrest.Matcher;
import org.hamcrest.core.AllOf;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.CustomerInfoPanel;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.gwt.components.DualList;
import com.ihg.automation.selenium.matchers.component.ContainsDualListItems;
import com.ihg.automation.selenium.servicecenter.pages.LeftPanel;
import com.ihg.automation.selenium.servicecenter.pages.account.AccountInfoPage;
import com.ihg.automation.selenium.servicecenter.pages.account.Flags;
import com.ihg.automation.selenium.servicecenter.pages.enroll.EnrollmentPage;
import com.ihg.crm.automation.selenium.tests.servicecenter.LoginLogout;

public class VerifyFlagOnAccountInfoFactoryTest extends LoginLogout
{
    private Member member;

    private AccountInfoPage accountInfoPage = new AccountInfoPage();
    private CustomerInfoPanel customerInfoPanel = new LeftPanel().getCustomerInfoPanel();

    public VerifyFlagOnAccountInfoFactoryTest(Member member)
    {
        this.member = member;
    }

    @BeforeClass
    public void before()
    {
        member.setPersonalInfo(MemberPopulateHelper.getSimplePersonalInfo());
        member.addAddress(MemberPopulateHelper.getUnitedStatesAddress());

        login();
    }

    @Test(priority = 10)
    public void enrollWithFlag()
    {
        new EnrollmentPage().enroll(member);
    }

    @Test(dependsOnMethods = "enrollWithFlag", priority = 15)
    public void verifyFlagAfterEnrollment()
    {
        accountInfoPage.goTo();

        verifyFlags(accountInfoPage.getFlags());
    }

    @Test(dependsOnMethods = "enrollWithFlag", priority = 20)
    public void addFlagOnAccountPage()
    {
        Flags flags = accountInfoPage.getFlags();
        flags.clickEdit();

        String addOnEditFlag = CUSTOMER_RETENTION.getValue();
        member.addFlag(addOnEditFlag);

        DualList dualListFlags = flags.getFlags();
        dualListFlags.select(addOnEditFlag);

        verifyThat(dualListFlags.getRightList(), ContainsDualListItems.containsItems(member.getFlags()));

        flags.clickSave(verifyNoError());

        verifyFlags(flags);
    }

    @Test(dependsOnMethods = "enrollWithFlag", priority = 25)
    public void removeAllFlags()
    {
        Flags flags = accountInfoPage.getFlags();
        flags.clickEdit();
        flags.getFlags().removeAll();
        flags.clickSave();

        verifyThat(flags, hasText(containsString("No flags selected")));

        verifyThat(customerInfoPanel.getFlags(), displayed(false));
    }

    private void verifyFlags(Flags flags)
    {
        List<Matcher<String>> flagsList = new ArrayList<>();
        for (String flag : member.getFlags())
        {
            flagsList.add(containsString(flag));
        }

        Matcher<String>[] matcherArray = new Matcher[flagsList.size()];
        matcherArray = flagsList.toArray(matcherArray);

        verifyThat(flags, hasText(AllOf.allOf(matcherArray)));

        verifyThat(customerInfoPanel.getFlags(), hasText(AllOf.allOf(matcherArray)));
    }

    @Override
    public String toString()
    {
        return "[" + "program=" + member.getPrograms().keySet() + ", flags=" + member.getFlags() + "]";
    }
}
