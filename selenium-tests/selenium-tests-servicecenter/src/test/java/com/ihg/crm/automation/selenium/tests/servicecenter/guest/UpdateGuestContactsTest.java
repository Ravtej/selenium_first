package com.ihg.crm.automation.selenium.tests.servicecenter.guest;

import static com.ihg.automation.common.RandomUtils.getTimeStamp;
import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyNoError;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.BirthDate.DAY_MONTH;
import static com.ihg.automation.selenium.common.member.MemberPopulateHelper.getPhone;
import static com.ihg.automation.selenium.common.member.MemberPopulateHelper.getRandomName;
import static com.ihg.automation.selenium.common.member.MemberPopulateHelper.getRandomPhone;
import static com.ihg.automation.selenium.common.member.MemberPopulateHelper.getUnitedStatesAddress;
import static com.ihg.automation.selenium.common.types.WrittenLanguage.ENGLISH;
import static com.ihg.automation.selenium.gwt.components.utils.ByStringText.contains;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.enabled;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.isDisplayedAndEnabled;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.isSelected;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static com.ihg.automation.selenium.matchers.text.EntityMatcherFactory.isValue;
import static com.ihg.automation.selenium.servicecenter.pages.communication.DaysMenu.ANY_AUDIT;
import static com.ihg.automation.selenium.servicecenter.pages.history.AuditConstant.FALSE;
import static com.ihg.automation.selenium.servicecenter.pages.history.AuditConstant.NOT_SET;
import static com.ihg.automation.selenium.servicecenter.pages.history.AuditConstant.OPT_IN;
import static com.ihg.automation.selenium.servicecenter.pages.history.AuditConstant.TRUE;
import static com.ihg.automation.selenium.servicecenter.pages.history.AuditConstant.UNLOCKED;
import static com.ihg.automation.selenium.servicecenter.pages.history.AuditConstant.AccountInformation.ACCOUNT_INFORMATION;
import static com.ihg.automation.selenium.servicecenter.pages.history.AuditConstant.AccountInformation.PIN;
import static com.ihg.automation.selenium.servicecenter.pages.history.AuditConstant.AccountInformation.SELF_SERVICE_LOCKOUT_INDICATOR;
import static com.ihg.automation.selenium.servicecenter.pages.history.AuditConstant.CommunicationPreferences.COMMUNICATIONS;
import static com.ihg.automation.selenium.servicecenter.pages.history.AuditConstant.CommunicationPreferences.COMMUNICATION_PREFERENCES;
import static com.ihg.automation.selenium.servicecenter.pages.history.AuditConstant.CommunicationPreferences.DAYS_OF_THE_WEEK;
import static com.ihg.automation.selenium.servicecenter.pages.history.AuditConstant.CommunicationPreferences.PHONE;
import static com.ihg.automation.selenium.servicecenter.pages.history.AuditConstant.CommunicationPreferences.PHONE_FOR_CUSTOMER_SERVICE;
import static com.ihg.automation.selenium.servicecenter.pages.history.AuditConstant.CommunicationPreferences.SPOKEN_LANGUAGE;
import static com.ihg.automation.selenium.servicecenter.pages.history.AuditConstant.CommunicationPreferences.STATUS;
import static com.ihg.automation.selenium.servicecenter.pages.history.AuditConstant.CommunicationPreferences.WRITTEN_LANGUAGE;
import static com.ihg.automation.selenium.servicecenter.pages.history.AuditConstant.ContactInformation.Address;
import static com.ihg.automation.selenium.servicecenter.pages.history.AuditConstant.ContactInformation.CONTACT_INFORMATION;
import static com.ihg.automation.selenium.servicecenter.pages.history.AuditConstant.ContactInformation.Email;
import static com.ihg.automation.selenium.servicecenter.pages.history.AuditConstant.ContactInformation.MARK_INVALID;
import static com.ihg.automation.selenium.servicecenter.pages.history.AuditConstant.ContactInformation.NON_PREFERRED;
import static com.ihg.automation.selenium.servicecenter.pages.history.AuditConstant.ContactInformation.PREFERRED;
import static com.ihg.automation.selenium.servicecenter.pages.history.AuditConstant.ContactInformation.TYPE;
import static com.ihg.automation.selenium.servicecenter.pages.history.AuditConstant.ContactInformation.Address.ADDRESS_1;
import static com.ihg.automation.selenium.servicecenter.pages.history.AuditConstant.ContactInformation.Address.ADDRESS_ADDED;
import static com.ihg.automation.selenium.servicecenter.pages.history.AuditConstant.ContactInformation.Address.ADDRESS_CHANGED;
import static com.ihg.automation.selenium.servicecenter.pages.history.AuditConstant.ContactInformation.Address.ADDRESS_DELETED;
import static com.ihg.automation.selenium.servicecenter.pages.history.AuditConstant.ContactInformation.Address.CITY;
import static com.ihg.automation.selenium.servicecenter.pages.history.AuditConstant.ContactInformation.Address.DO_NOT_CLEAN;
import static com.ihg.automation.selenium.servicecenter.pages.history.AuditConstant.ContactInformation.Address.STATE;
import static com.ihg.automation.selenium.servicecenter.pages.history.AuditConstant.ContactInformation.Address.ZIP_CODE;
import static com.ihg.automation.selenium.servicecenter.pages.history.AuditConstant.ContactInformation.Email.EMAIL_ADDED;
import static com.ihg.automation.selenium.servicecenter.pages.history.AuditConstant.ContactInformation.Email.EMAIL_CHANGED;
import static com.ihg.automation.selenium.servicecenter.pages.history.AuditConstant.ContactInformation.Email.EMAIL_DELETED;
import static com.ihg.automation.selenium.servicecenter.pages.history.AuditConstant.ContactInformation.Email.FORMAT;
import static com.ihg.automation.selenium.servicecenter.pages.history.AuditConstant.ContactInformation.Phone.FULL_NUMBER;
import static com.ihg.automation.selenium.servicecenter.pages.history.AuditConstant.ContactInformation.Phone.PHONE_ADDED;
import static com.ihg.automation.selenium.servicecenter.pages.history.AuditConstant.ContactInformation.Phone.PHONE_CHANGED;
import static com.ihg.automation.selenium.servicecenter.pages.history.AuditConstant.ContactInformation.Phone.PHONE_DELETED;
import static com.ihg.automation.selenium.servicecenter.pages.history.AuditConstant.ContactInformation.Phone.SMS_ADDED;
import static com.ihg.automation.selenium.servicecenter.pages.history.AuditConstant.PersonalInformation.COUNTRY;
import static com.ihg.automation.selenium.servicecenter.pages.history.AuditConstant.PersonalInformation.DATE_OF_BIRTH;
import static com.ihg.automation.selenium.servicecenter.pages.history.AuditConstant.PersonalInformation.GENDER;
import static com.ihg.automation.selenium.servicecenter.pages.history.AuditConstant.PersonalInformation.NAME;
import static com.ihg.automation.selenium.servicecenter.pages.history.AuditConstant.PersonalInformation.PERSONAL_INFORMATION;
import static org.apache.commons.lang3.StringUtils.EMPTY;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNot.not;
import static org.hamcrest.core.StringContains.containsString;

import java.util.Map;

import org.joda.time.LocalDate;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.ihg.atp.schema.crm.guest.guest.servicetypes.v2.RetrieveGuestRequestType;
import com.ihg.atp.schema.crm.guest.guest.servicetypes.v2.RetrieveGuestResponseType;
import com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory;
import com.ihg.automation.selenium.common.Constant;
import com.ihg.automation.selenium.common.CustomerInfoPanel;
import com.ihg.automation.selenium.common.member.GuestAddress;
import com.ihg.automation.selenium.common.member.GuestEmail;
import com.ihg.automation.selenium.common.member.GuestName;
import com.ihg.automation.selenium.common.member.GuestPhone;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.PersonalInfo;
import com.ihg.automation.selenium.common.personal.EmailContactList;
import com.ihg.automation.selenium.common.personal.SmsContactList;
import com.ihg.automation.selenium.common.types.EmailFormat;
import com.ihg.automation.selenium.common.types.EmailType;
import com.ihg.automation.selenium.common.types.Gender;
import com.ihg.automation.selenium.common.types.name.Salutation;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.gwt.components.Component;
import com.ihg.automation.selenium.gwt.components.Mode;
import com.ihg.automation.selenium.gwt.components.Tab;
import com.ihg.automation.selenium.servicecenter.pages.Messages;
import com.ihg.automation.selenium.servicecenter.pages.Tabs;
import com.ihg.automation.selenium.servicecenter.pages.Tabs.CustomerInfoTab;
import com.ihg.automation.selenium.servicecenter.pages.communication.CommunicationPreferences;
import com.ihg.automation.selenium.servicecenter.pages.communication.CommunicationPreferencesPage;
import com.ihg.automation.selenium.servicecenter.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.servicecenter.pages.history.AuditConstant;
import com.ihg.automation.selenium.servicecenter.pages.history.ProfileHistoryGridRow;
import com.ihg.automation.selenium.servicecenter.pages.history.ProfileHistoryPage;
import com.ihg.automation.selenium.servicecenter.pages.personal.CustomerInfoFields;
import com.ihg.automation.selenium.servicecenter.pages.personal.PersonalInfoPage;
import com.ihg.automation.utils.DiffHelper;
import com.ihg.crm.automation.selenium.tests.servicecenter.LoginLogout;
import com.ihg.crm.automation.service.guest.RetrieveGuestRequestHelper;

public class UpdateGuestContactsTest extends LoginLogout
{
    private Tabs tabs = new Tabs();
    private EnrollmentPage enrollmentPage = new EnrollmentPage();
    private PersonalInfoPage personalPage = new PersonalInfoPage();
    private CustomerInfoPanel customerInfoPanel = new CustomerInfoPanel();
    private Member memberCreate, memberUpdate;
    private GuestName nameCreate, nameUpdate;
    private GuestAddress addressCreate, addressUpdate, address2;
    private GuestPhone phoneCreate, phoneUpdate, phone2, smsPhone;
    private GuestEmail emailCreate, emailUpdate;
    private PersonalInfo personalInfoCreate, personalInfoUpdate;
    private RetrieveGuestRequestType retrieveGuestRequest;
    private DiffHelper diffHelper = new DiffHelper();

    private void initAddresses()
    {
        addressCreate = getUnitedStatesAddress();
        addressCreate.setDoNotChangeLock(true);

        addressUpdate = addressCreate.clone();
        addressUpdate.setLine1(addressCreate.getLine1() + "(upd)");

        address2 = addressCreate.clone();
        address2.setLine1("address 2");
    }

    private void initPhones()
    {
        phoneCreate = getPhone();
        phoneCreate.setNumber("1234567");

        phoneUpdate = getPhone();
        phoneUpdate.setNumber("7654321");

        phone2 = getPhone();
        phone2.setNumber("1122334");
    }

    private void initEmails()
    {
        final String format = "email-%s-%s@yahoo.com";

        emailCreate = new GuestEmail(EmailType.BUSINESS, EmailFormat.HTML,
                String.format(format, getTimeStamp(), "create"));

        emailUpdate = new GuestEmail(EmailType.PERSONAL, EmailFormat.TEXT,
                String.format(format, getTimeStamp(), "update"));
    }

    private void initPersoanlInfo()
    {
        personalInfoCreate = new PersonalInfo();
        nameCreate = getRandomName();
        personalInfoCreate.setName(nameCreate);
        personalInfoCreate.setResidenceCountry(addressCreate.getCountry());
        personalInfoCreate.setBirthDate(new LocalDate());

        personalInfoUpdate = new PersonalInfo();
        personalInfoUpdate.setNameOnCard("Name On Card");
        personalInfoUpdate.setResidenceCountry(addressCreate.getCountry());
        personalInfoUpdate.setBirthDate(new LocalDate().plusMonths(1).plusDays(1));
        personalInfoUpdate.setGenderCode(Gender.FEMALE);
        nameUpdate = getRandomName();
        nameUpdate.setGiven("Given Update");
        nameUpdate.setSalutation(Salutation.MS);
        personalInfoUpdate.setName(nameUpdate);
    }

    private String getGuestId()
    {
        final String SQL = "SELECT n.DGST_MSTR_KEY FROM dgst.GST_NM n"
                + " WHERE n.GIVEN_NM ='%s' AND n.SURNAME_NM = '%s' AND n.LST_UPDT_TS > (SYSDATE-1)";

        Map<String, Object> map = jdbcTemplate
                .queryForMap(String.format(SQL, nameCreate.getGiven(), nameCreate.getSurname()));
        return map.get("DGST_MSTR_KEY").toString();
    }

    @BeforeClass
    public void before()
    {
        initAddresses();
        initPhones();
        initEmails();
        initPersoanlInfo();

        memberCreate = new Member();
        memberCreate.addProgram(Program.IHG);
        memberCreate.setPersonalInfo(personalInfoCreate);
        memberCreate.addAddress(addressCreate);
        memberCreate.addPhone(phoneCreate);
        memberCreate.addEmail(emailCreate);

        memberUpdate = new Member();
        memberUpdate.setPersonalInfo(personalInfoUpdate);
    }

    @Test
    public void enrollGuestToIHG()
    {
        login();
        enrollmentPage.enroll(memberCreate);
        retrieveGuestRequest = RetrieveGuestRequestHelper.byGuestId(getGuestId());
    }

    @DataProvider(name = "customerInformationTabs")
    private Object[][] customerInformationTabs()
    {
        CustomerInfoTab tab = tabs.getCustomerInfo();

        return new Object[][] { { tab.getPersonalInfo(), true }, { tab.getCommunicationPref(), false },
                { tab.getAccountInfo(), false }, { tab.getProfileHistory(), false } };
    }

    @Test(dataProvider = "customerInformationTabs", dependsOnMethods = { "enrollGuestToIHG" })
    public void customerInformationTabsAvailability(Tab tab, boolean isActive)
    {
        verifyThat(tab, isDisplayedAndEnabled(true));
        verifyThat(tab.getContainer(), isDisplayedAndEnabled(isActive));
        verifyThat(tab, isSelected(isActive));
    }

    @Test(dependsOnMethods = { "customerInformationTabsAvailability" })
    public void verifyHistoryAfterEnrollment()
    {
        ProfileHistoryPage historyPage = new ProfileHistoryPage();
        historyPage.goTo();
        verifyNoError();

        ProfileHistoryGridRow historyGridRow = historyPage.getProfileHistoryGrid().getRow(1);
        ProfileHistoryGridRow personalInfoRow = historyGridRow.getSubRowByName(PERSONAL_INFORMATION);

        personalInfoRow.getSubRowByName(COUNTRY).verifyAddAction(hasText(isValue(addressCreate.getCountry())));
        personalInfoRow.getSubRowByName(DATE_OF_BIRTH)
                .verifyAddAction(personalInfoCreate.getBirthDate().toString(DAY_MONTH));
        personalInfoRow.getSubRowByName(NAME).verifyAddAction(nameCreate.getNameOnPanel());

        ProfileHistoryGridRow commPrefRow = historyGridRow.getSubRowByName(COMMUNICATION_PREFERENCES)
                .getSubRowByName(COMMUNICATIONS);

        ProfileHistoryGridRow heartbeatCommPrefRow = commPrefRow.getSubRowByName("Heartbeat");
        heartbeatCommPrefRow.getSubRowByName(STATUS).verify(NOT_SET, OPT_IN);
        heartbeatCommPrefRow.getSubRowByName(AuditConstant.CommunicationPreferences.EMAIL)
                .verifyAddAction(emailCreate.getEmail());

        ProfileHistoryGridRow specialOffersCommPrefRow = commPrefRow
                .getSubRowByName("IHG Special Offers and Promotions");
        specialOffersCommPrefRow.getSubRowByName(STATUS).verify(NOT_SET, OPT_IN);
        specialOffersCommPrefRow.getSubRowByName(AuditConstant.CommunicationPreferences.EMAIL)
                .verifyAddAction(emailCreate.getEmail());

        commPrefRow.getSubRowByName(WRITTEN_LANGUAGE).verifyAddAction(hasText(isValue(ENGLISH)));
        commPrefRow.getSubRowByName(SPOKEN_LANGUAGE).verifyAddAction(hasText(isValue(ENGLISH)));

        ProfileHistoryGridRow contactInfoSubRow = historyGridRow.getSubRowByName(CONTACT_INFORMATION);
        ProfileHistoryGridRow addressRow = contactInfoSubRow.getSubRowByName(contains(ADDRESS_ADDED));
        addressRow.getSubRowByName(STATE).verifyAddAction(hasText(isValue(addressCreate.getRegion1())));
        addressRow.getSubRowByName(CITY).verifyAddAction(addressCreate.getLocality1());
        addressRow.getSubRowByName(DO_NOT_CLEAN).verifyAddAction(FALSE);
        addressRow.getSubRowByName(MARK_INVALID).verifyAddAction(NOT_SET);
        addressRow.getSubRowByName(Address.COUNTRY).verifyAddAction(hasText(isValue(addressCreate.getCountry())));
        addressRow.getSubRowByName(TYPE).verifyAddAction(hasText(isValue(addressCreate.getType())));
        addressRow.getSubRowByName(ZIP_CODE).verifyAddAction(addressCreate.getPostalCode());
        addressRow.getSubRowByName(ADDRESS_1).verifyAddAction(addressCreate.getLine1());
        addressRow.getSubRowByName(PREFERRED).verifyAddAction(PREFERRED);

        ProfileHistoryGridRow emailRow = contactInfoSubRow.getSubRowByName(contains(EMAIL_ADDED));
        emailRow.getSubRowByName(MARK_INVALID).verifyAddAction(NOT_SET);
        emailRow.getSubRowByName(Email.EMAIL).verifyAddAction(emailCreate.getEmail());
        emailRow.getSubRowByName(TYPE).verifyAddAction(hasText(isValue(emailCreate.getType())));
        emailRow.getSubRowByName(FORMAT).verifyAddAction(hasText(isValue(emailCreate.getFormat())));
        emailRow.getSubRowByName(PREFERRED).verifyAddAction(PREFERRED);

        ProfileHistoryGridRow phoneRow = contactInfoSubRow.getSubRowByName(contains(PHONE_ADDED));
        phoneRow.getSubRowByName(MARK_INVALID).verifyAddAction(NOT_SET);
        phoneRow.getSubRowByName(FULL_NUMBER).verifyAddAction(phoneCreate.getNumber());
        phoneRow.getSubRowByName(TYPE).verifyAddAction(phoneCreate.getAuditPhoneType());
        phoneRow.getSubRowByName(PREFERRED).verifyAddAction(PREFERRED);

        ProfileHistoryGridRow accountInfoSubRow = historyGridRow.getSubRowByName(ACCOUNT_INFORMATION);
        accountInfoSubRow.getSubRowByName(SELF_SERVICE_LOCKOUT_INDICATOR).verifyAddAction(UNLOCKED);
        accountInfoSubRow.getSubRowByName(PIN).verifyAddAction("*******");
    }

    @Test(dependsOnMethods = "verifyHistoryAfterEnrollment")
    public void cancelUpdateCustomerInformation()
    {
        RetrieveGuestResponseType retrieveGuestBefore = guestClient.retrieveGuest(retrieveGuestRequest);
        PersonalInfoPage personalInfoPage = new PersonalInfoPage();
        personalInfoPage.goTo();

        CustomerInfoFields customerInfo = personalInfoPage.getCustomerInfo();
        customerInfo.clickEdit();
        customerInfo.populate(memberUpdate);
        customerInfo.clickCancel(verifyNoError());
        customerInfo.verify(memberCreate, Mode.VIEW);

        RetrieveGuestResponseType retrieveGuestAfter = guestClient.retrieveGuest(retrieveGuestRequest);
        verifyThat(null, retrieveGuestAfter.getProfile().getLastModifyDateTime(),
                is(retrieveGuestBefore.getProfile().getLastModifyDateTime()),
                "Profile must have the same lastModifyDateTime");
    }

    @Test(dependsOnMethods = "cancelUpdateCustomerInformation")
    public void updateCustomerInformation()
    {
        RetrieveGuestResponseType retrieveGuestBefore = guestClient.retrieveGuest(retrieveGuestRequest);

        CustomerInfoFields customerInfo = new PersonalInfoPage().getCustomerInfo();

        customerInfo.clickEdit();
        customerInfo.populate(memberUpdate);
        customerInfo.clickSave(verifyNoError());

        customerInfo.verify(memberUpdate, Mode.VIEW);

        Component nameOnPanel = new CustomerInfoPanel().getFullName();
        verifyThat(nameOnPanel, hasText(not(is(nameCreate.getNameOnPanel()))));
        verifyThat(nameOnPanel, hasText(nameUpdate.getNameOnPanel()));

        RetrieveGuestResponseType retrieveGuestAfter = guestClient.retrieveGuest(retrieveGuestRequest);
        diffHelper.verifyEntitiesSizeAfterUpdate(retrieveGuestBefore, retrieveGuestAfter, 0, 0, 0);
    }

    @Test(dependsOnMethods = { "updateCustomerInformation" })
    public void verifyHistoryAfterUpdatingCustomerInfo()
    {
        ProfileHistoryPage historyPage = new ProfileHistoryPage();
        historyPage.goTo();
        verifyNoError();

        ProfileHistoryGridRow personalInfoRow = historyPage.getProfileHistoryGrid().getRow(1)
                .getSubRowByName(PERSONAL_INFORMATION);

        personalInfoRow.getSubRowByName(GENDER).verifyAddAction(hasText(isValue(personalInfoUpdate.getGenderCode())));
        personalInfoRow.getSubRowByName(DATE_OF_BIRTH).verify(personalInfoCreate.getBirthDate().toString(DAY_MONTH),
                personalInfoUpdate.getBirthDate().toString(DAY_MONTH));
        personalInfoRow.getSubRowByName(NAME).verify(nameCreate.getNameOnPanel(), nameUpdate.getNameOnPanel());

    }

    @Test(dependsOnMethods = { "verifyHistoryAfterUpdatingCustomerInfo" }, alwaysRun = true)
    public void addCommunicationPreferences()
    {
        CommunicationPreferencesPage page = new CommunicationPreferencesPage();
        page.goTo();
        CommunicationPreferences preferences = page.getCommunicationPreferences();
        preferences.clickEdit();

        preferences.getMarketingPreferences().getEmailAddress().select(emailCreate.getEmail());

        preferences.getCustomerService().getPhone().populate(phoneCreate);

        preferences.clickSave(verifyNoError());
    }

    @Test(dependsOnMethods = { "addCommunicationPreferences" })
    public void verifyHistoryAfterUpdatingCommPref()
    {
        ProfileHistoryPage historyPage = new ProfileHistoryPage();
        historyPage.goTo();
        verifyNoError();

        ProfileHistoryGridRow commPrefRow = historyPage.getProfileHistoryGrid().getRow(1)
                .getSubRowByName(COMMUNICATION_PREFERENCES).getSubRowByName(PHONE_FOR_CUSTOMER_SERVICE);

        commPrefRow.getSubRowByName(DAYS_OF_THE_WEEK).verifyAddAction(ANY_AUDIT);
        commPrefRow.getSubRowByName(PHONE).verifyAddAction(phoneCreate.getNumber());

    }

    @Test(dependsOnMethods = { "verifyHistoryAfterUpdatingCommPref" }, alwaysRun = true)
    public void updateAddress()
    {
        personalPage.goTo();

        RetrieveGuestResponseType retrieveGuestBefore = guestClient.retrieveGuest(retrieveGuestRequest);

        personalPage.getAddressList().updateFirstContact(addressUpdate, Messages.getSimpleSuccess());
        verifyThat(customerInfoPanel.getAddress(), hasText(containsString(addressUpdate.getLine1())));

        RetrieveGuestResponseType retrieveGuestAfter = guestClient.retrieveGuest(retrieveGuestRequest);
        diffHelper.verifyEntitiesSizeAfterUpdate(retrieveGuestBefore, retrieveGuestAfter, 0, 0, 0);
    }

    @Test(dependsOnMethods = { "updateAddress" })
    public void verifyHistoryAfterUpdatingAddress()
    {
        ProfileHistoryPage historyPage = new ProfileHistoryPage();
        historyPage.goTo();
        verifyNoError();

        ProfileHistoryGridRow addressRow = historyPage.getProfileHistoryGrid().getRow(1)
                .getSubRowByName(CONTACT_INFORMATION).getSubRowByName(contains(ADDRESS_CHANGED));

        addressRow.getSubRowByName(ADDRESS_1).verify(addressCreate.getLine1(), addressUpdate.getLine1());
        addressRow.getSubRowByName(DO_NOT_CLEAN).verify(FALSE, TRUE);

    }

    @Test(dependsOnMethods = { "verifyHistoryAfterUpdatingAddress" }, alwaysRun = true)
    public void addSecondAddressAsPreferred()
    {
        RetrieveGuestResponseType retrieveGuestBefore = guestClient.retrieveGuest(retrieveGuestRequest);

        personalPage.goTo();
        personalPage.getAddressList().addSecondContactAsPreferred(addressUpdate, address2, Messages.getSimpleSuccess());
        verifyThat(customerInfoPanel.getAddress(), hasText(containsString(address2.getLine1())));

        RetrieveGuestResponseType retrieveGuestAfter = guestClient.retrieveGuest(retrieveGuestRequest);
        diffHelper.verifyEntitiesSizeAfterUpdate(retrieveGuestBefore, retrieveGuestAfter, 1, 0, 0);
    }

    @Test(dependsOnMethods = { "addSecondAddressAsPreferred" })
    public void verifyHistoryAfterAddingSecondaryPreferredAddress()
    {
        ProfileHistoryPage historyPage = new ProfileHistoryPage();
        historyPage.goTo();
        verifyNoError();

        ProfileHistoryGridRow firstAddressRow = historyPage.getProfileHistoryGrid().getRow(1)
                .getSubRowByName(CONTACT_INFORMATION).getSubRowByName(contains(ADDRESS_CHANGED));

        firstAddressRow.getSubRowByName(PREFERRED).verify(PREFERRED, NON_PREFERRED);

        ProfileHistoryGridRow secondAddressRow = historyPage.getProfileHistoryGrid().getRow(1)
                .getSubRowByName(CONTACT_INFORMATION).getSubRowByName(contains(ADDRESS_ADDED));

        secondAddressRow.getSubRowByName(STATE).verifyAddAction(hasText(isValue(address2.getRegion1())));
        secondAddressRow.getSubRowByName(CITY).verifyAddAction(address2.getLocality1());
        secondAddressRow.getSubRowByName(DO_NOT_CLEAN).verifyAddAction(TRUE);
        secondAddressRow.getSubRowByName(MARK_INVALID).verifyAddAction(NOT_SET);
        secondAddressRow.getSubRowByName(Address.COUNTRY).verifyAddAction(hasText(isValue(address2.getCountry())));
        secondAddressRow.getSubRowByName(TYPE).verifyAddAction(hasText(isValue(address2.getType())));
        secondAddressRow.getSubRowByName(ZIP_CODE).verifyAddAction(address2.getPostalCode());
        secondAddressRow.getSubRowByName(ADDRESS_1).verifyAddAction(address2.getLine1());
        secondAddressRow.getSubRowByName(PREFERRED).verifyAddAction(PREFERRED);
    }

    @Test(dependsOnMethods = { "verifyHistoryAfterAddingSecondaryPreferredAddress" }, alwaysRun = true)
    public void removePreferredAddress()
    {
        RetrieveGuestResponseType retrieveGuestBefore = guestClient.retrieveGuest(retrieveGuestRequest);

        personalPage.goTo();
        personalPage.getAddressList().removePreferredContact(addressUpdate, Messages.getSimpleSuccess());
        verifyThat(customerInfoPanel.getAddress(), hasText(containsString(addressUpdate.getLine1())));

        RetrieveGuestResponseType retrieveGuestAfter = guestClient.retrieveGuest(retrieveGuestRequest);
        diffHelper.verifyEntitiesSizeAfterUpdate(retrieveGuestBefore, retrieveGuestAfter, -1, 0, 0);
    }

    @Test(dependsOnMethods = { "removePreferredAddress" })
    public void verifyHistoryAfterRemovingPreferredAddress()
    {
        ProfileHistoryPage historyPage = new ProfileHistoryPage();
        historyPage.goTo();
        verifyNoError();

        ProfileHistoryGridRow firstAddressRow = historyPage.getProfileHistoryGrid().getRow(1)
                .getSubRowByName(CONTACT_INFORMATION).getSubRowByName(contains(ADDRESS_CHANGED));

        firstAddressRow.getSubRowByName(PREFERRED).verify(NON_PREFERRED, PREFERRED);

        ProfileHistoryGridRow secondAddressRow = historyPage.getProfileHistoryGrid().getRow(1)
                .getSubRowByName(CONTACT_INFORMATION).getSubRowByName(contains(ADDRESS_DELETED));

        secondAddressRow.getSubRowByName(STATE).verifyDeleteAction(hasText(isValue(address2.getRegion1())));
        secondAddressRow.getSubRowByName(CITY).verifyDeleteAction(address2.getLocality1());
        secondAddressRow.getSubRowByName(DO_NOT_CLEAN).verifyDeleteAction(TRUE);
        secondAddressRow.getSubRowByName(MARK_INVALID).verifyDeleteAction(NOT_SET);
        secondAddressRow.getSubRowByName(Address.COUNTRY).verifyDeleteAction(hasText(isValue(address2.getCountry())));
        secondAddressRow.getSubRowByName(TYPE).verifyDeleteAction(hasText(isValue(address2.getType())));
        secondAddressRow.getSubRowByName(ZIP_CODE).verifyDeleteAction(address2.getPostalCode());
        secondAddressRow.getSubRowByName(ADDRESS_1).verifyDeleteAction(address2.getLine1());
        secondAddressRow.getSubRowByName(PREFERRED).verifyDeleteAction(PREFERRED);
    }

    @Test(dependsOnMethods = { "verifyHistoryAfterRemovingPreferredAddress" }, alwaysRun = true)
    public void addMaxAddressAmount()
    {
        personalPage.goTo();
        personalPage.getAddressList().addMaxContactAmount(5);
    }

    @Test(dependsOnMethods = { "addMaxAddressAmount" }, alwaysRun = true)
    public void updatePhone()
    {
        RetrieveGuestResponseType retrieveGuestBefore = guestClient.retrieveGuest(retrieveGuestRequest);

        personalPage.getPhoneList().updateFirstContact(phoneUpdate, Messages.getUpdatedMessage(false, true));
        verifyThat(customerInfoPanel.getPhone(), hasText(containsString(phoneUpdate.getFullPhone())));

        RetrieveGuestResponseType retrieveGuestAfter = guestClient.retrieveGuest(retrieveGuestRequest);
        diffHelper.verifyEntitiesSizeAfterUpdate(retrieveGuestBefore, retrieveGuestAfter, 0, 0, 0);
    }

    @Test(dependsOnMethods = { "updatePhone" })
    public void verifyHistoryAfterUpdatingPhone()
    {
        ProfileHistoryPage historyPage = new ProfileHistoryPage();
        historyPage.goTo();
        verifyNoError();

        ProfileHistoryGridRow phoneRow = historyPage.getProfileHistoryGrid().getRow(1)
                .getSubRowByName(CONTACT_INFORMATION).getSubRowByName(contains(PHONE_CHANGED));

        phoneRow.getSubRowByName(FULL_NUMBER).verify(phoneCreate.getFullPhone(), phoneUpdate.getFullPhone());
    }

    @Test(dependsOnMethods = { "verifyHistoryAfterUpdatingPhone" }, alwaysRun = true)
    public void addSecondPhoneAsPreferred()
    {
        RetrieveGuestResponseType retrieveGuestBefore = guestClient.retrieveGuest(retrieveGuestRequest);

        personalPage.goTo();
        personalPage.getPhoneList().addSecondContactAsPreferred(phoneUpdate, phone2,
                Messages.getPreferredMessage(false, true));
        verifyThat(customerInfoPanel.getPhone(), hasText(phone2.getFullPhone()));

        RetrieveGuestResponseType retrieveGuestAfter = guestClient.retrieveGuest(retrieveGuestRequest);
        diffHelper.verifyEntitiesSizeAfterUpdate(retrieveGuestBefore, retrieveGuestAfter, 0, 1, 0);
    }

    @Test(dependsOnMethods = { "addSecondPhoneAsPreferred" })
    public void verifyHistoryAfterAddingPhone()
    {
        ProfileHistoryPage historyPage = new ProfileHistoryPage();
        historyPage.goTo();
        verifyNoError();

        ProfileHistoryGridRow phoneRow = historyPage.getProfileHistoryGrid().getRow(1)
                .getSubRowByName(CONTACT_INFORMATION).getSubRowByName(contains(PHONE_ADDED));

        phoneRow.getSubRowByName(MARK_INVALID).verifyAddAction(NOT_SET);
        phoneRow.getSubRowByName(FULL_NUMBER).verifyAddAction(phone2.getNumber());
        phoneRow.getSubRowByName(TYPE).verifyAddAction(phoneCreate.getAuditPhoneType());
        phoneRow.getSubRowByName(PREFERRED).verifyAddAction(PREFERRED);
    }

    @Test(dependsOnMethods = { "addSecondPhoneAsPreferred" }, alwaysRun = true)
    public void removePreferredPhone()
    {
        RetrieveGuestResponseType retrieveGuestBefore = guestClient.retrieveGuest(retrieveGuestRequest);

        personalPage.goTo();
        personalPage.getPhoneList().removePreferredContact(phoneUpdate, Messages.getSimpleSuccess());
        verifyThat(customerInfoPanel.getPhone(), hasText(phoneUpdate.getFullPhone()));

        RetrieveGuestResponseType retrieveGuestAfter = guestClient.retrieveGuest(retrieveGuestRequest);
        diffHelper.verifyEntitiesSizeAfterUpdate(retrieveGuestBefore, retrieveGuestAfter, 0, -1, 0);
    }

    @Test(dependsOnMethods = { "removePreferredPhone" })
    public void verifyHistoryAfterRemovingPreferredPhone()
    {
        ProfileHistoryPage historyPage = new ProfileHistoryPage();
        historyPage.goTo();
        verifyNoError();

        ProfileHistoryGridRow deletedPhoneRow = historyPage.getProfileHistoryGrid().getRow(1)
                .getSubRowByName(CONTACT_INFORMATION).getSubRowByName(contains(PHONE_DELETED));

        deletedPhoneRow.getSubRowByName(MARK_INVALID).verifyDeleteAction(NOT_SET);
        deletedPhoneRow.getSubRowByName(FULL_NUMBER).verifyDeleteAction(phone2.getNumber());
        deletedPhoneRow.getSubRowByName(TYPE).verifyDeleteAction(phoneCreate.getAuditPhoneType());
        deletedPhoneRow.getSubRowByName(PREFERRED).verifyDeleteAction(PREFERRED);

        ProfileHistoryGridRow changedPhoneRow = historyPage.getProfileHistoryGrid().getRow(1)
                .getSubRowByName(CONTACT_INFORMATION).getSubRowByName(contains(PHONE_CHANGED));
        changedPhoneRow.getSubRowByName(PREFERRED).verify(NON_PREFERRED, PREFERRED);

    }

    @Test(dependsOnMethods = { "verifyHistoryAfterRemovingPreferredPhone" }, alwaysRun = true)
    public void addMaxPhonesAmount()
    {
        personalPage.goTo();
        personalPage.getPhoneList().addMaxContactAmount(3);
    }

    @Test(dependsOnMethods = { "addMaxPhonesAmount" }, alwaysRun = true)
    public void addMaxSmsAmount()
    {
        SmsContactList smsList = personalPage.getSmsList();
        smsPhone = getRandomPhone();
        smsList.add(smsPhone);

        smsList.getExpandedContact().verify(smsPhone, Mode.VIEW);
        verifyThat(smsList.getAddButton(), enabled(false));
    }

    @Test(dependsOnMethods = { "addMaxSmsAmount" })
    public void verifyHistoryAfterAddingSms()
    {
        ProfileHistoryPage historyPage = new ProfileHistoryPage();
        historyPage.goTo();
        verifyNoError();

        ProfileHistoryGridRow smsRow = historyPage.getProfileHistoryGrid().getRow(1)
                .getSubRowByName(CONTACT_INFORMATION).getSubRowByName(contains(SMS_ADDED));

        smsRow.getSubRowByName(MARK_INVALID).verifyAddAction(NOT_SET);
        smsRow.getSubRowByName(FULL_NUMBER).verifyAddAction(smsPhone.getNumber());
    }

    @Test(dependsOnMethods = { "verifyHistoryAfterAddingSms" }, alwaysRun = true)
    public void updateEmail()
    {
        RetrieveGuestResponseType retrieveGuestBefore = guestClient.retrieveGuest(retrieveGuestRequest);

        personalPage.goTo();
        personalPage.getEmailList().updateFirstContact(emailUpdate, Messages.getUpdatedMessage(true, false));
        verifyThat(customerInfoPanel.getEmail(), hasText(emailUpdate.getEmail()));

        RetrieveGuestResponseType retrieveGuestAfter = guestClient.retrieveGuest(retrieveGuestRequest);
        diffHelper.verifyEntitiesSizeAfterUpdate(retrieveGuestBefore, retrieveGuestAfter, 0, 0, 0);
    }

    @Test(dependsOnMethods = { "updateEmail" }, alwaysRun = true)
    public void verifyAddEmailButtonDisabled()
    {
        verifyThat(personalPage.getEmailList().getAddButton(), enabled(false));
    }

    @Test(dependsOnMethods = { "verifyAddEmailButtonDisabled" })
    public void verifyHistoryAfterUpdatingEmail()
    {
        ProfileHistoryPage historyPage = new ProfileHistoryPage();
        historyPage.goTo();
        verifyNoError();

        ProfileHistoryGridRow emailRow = historyPage.getProfileHistoryGrid().getRow(1)
                .getSubRowByName(CONTACT_INFORMATION).getSubRowByName(contains(EMAIL_CHANGED));

        emailRow.getSubRowByName(Email.EMAIL).verify(emailCreate.getEmail(), emailUpdate.getEmail());
    }

    @Test(dependsOnMethods = { "verifyHistoryAfterUpdatingEmail" }, alwaysRun = true)
    public void removeEmail()
    {
        RetrieveGuestResponseType retrieveGuestBefore = guestClient.retrieveGuest(retrieveGuestRequest);

        personalPage.goTo();
        EmailContactList emailContactList = personalPage.getEmailList();

        verifyThat(emailContactList, emailContactList.getContactsCount(), is(1),
                "Invalid email amount (before remove)");
        emailContactList.getContact()
                .remove(MessageBoxVerifierFactory.verifySuccess(Messages.getDeletedMessage(true, false)));
        verifyThat(emailContactList, emailContactList.getContactsCount(), is(0), "Invalid email amount (after remove)");

        verifyThat(customerInfoPanel.getEmail(), hasText(Constant.NOT_AVAILABLE));

        RetrieveGuestResponseType retrieveGuestAfter = guestClient.retrieveGuest(retrieveGuestRequest);
        diffHelper.verifyEntitiesSizeAfterUpdate(retrieveGuestBefore, retrieveGuestAfter, 0, 0, -1);
    }

    @Test(dependsOnMethods = { "removeEmail" }, alwaysRun = true)
    public void verifyAddEmailButtonEnabled()
    {
        verifyThat(personalPage.getEmailList().getAddButton(), enabled(true));
    }

    @Test(dependsOnMethods = { "verifyAddEmailButtonEnabled" })
    public void verifyHistoryAfterRemovingEmail()
    {
        ProfileHistoryPage historyPage = new ProfileHistoryPage();
        historyPage.goTo();
        verifyNoError();

        ProfileHistoryGridRow emailRow = historyPage.getProfileHistoryGrid().getRow(1)
                .getSubRowByName(CONTACT_INFORMATION).getSubRowByName(contains(EMAIL_DELETED));

        emailRow.getSubRowByName(MARK_INVALID).verifyDeleteAction(NOT_SET);
        emailRow.getSubRowByName(Email.EMAIL).verifyDeleteAction(emailUpdate.getEmail());
        emailRow.getSubRowByName(TYPE).verifyDeleteAction(hasText(isValue(emailUpdate.getType())));
        emailRow.getSubRowByName(FORMAT).verifyDeleteAction(hasText(isValue(emailUpdate.getFormat())));
        emailRow.getSubRowByName(PREFERRED).verifyDeleteAction(PREFERRED);
    }

    @Test(dependsOnMethods = { "verifyHistoryAfterRemovingEmail" }, alwaysRun = true)
    public void verifyRemoveBirthday()
    {
        PersonalInfoPage personalInfoPage = new PersonalInfoPage();
        personalInfoPage.goTo();
        CustomerInfoFields customerInfo = personalInfoPage.getCustomerInfo();
        customerInfo.clickEdit();
        customerInfo.getBirthDate().getMonth().select(EMPTY);
        customerInfo.clickSave();

        verifyThat(customerInfo.getBirthDate().getFullDate(), hasText(Constant.NOT_AVAILABLE));
    }

    @Test(dependsOnMethods = { "verifyRemoveBirthday" })
    public void verifyHistoryAfterRemovingBirthday()
    {
        ProfileHistoryPage historyPage = new ProfileHistoryPage();
        historyPage.goTo();
        verifyNoError();

        ProfileHistoryGridRow emailRow = historyPage.getProfileHistoryGrid().getRow(1)
                .getSubRowByName(PERSONAL_INFORMATION);

        emailRow.getSubRowByName(DATE_OF_BIRTH)
                .verifyDeleteAction(personalInfoUpdate.getBirthDate().toString(DAY_MONTH));
    }
}
