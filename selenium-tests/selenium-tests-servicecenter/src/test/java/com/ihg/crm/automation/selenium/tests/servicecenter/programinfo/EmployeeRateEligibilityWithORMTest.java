package com.ihg.crm.automation.selenium.tests.servicecenter.programinfo;

import static com.ihg.atp.schema.crm.guest.membership.datatypes.v1.SourceType.SC;
import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifySuccess;
import static com.ihg.automation.selenium.common.Constant.NOT_AVAILABLE;
import static com.ihg.automation.selenium.common.pages.program.EmployeeRateEligibility.EMPLOYEE_RATE_ELIGIBILITY_SAVED;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.joda.time.LocalDate;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.atp.schema.crm.common.contact.datatypes.v2.AddressContactType;
import com.ihg.atp.schema.crm.common.contact.datatypes.v2.UsageTypeType;
import com.ihg.atp.schema.crm.common.location.address.datatypes.v2.AddressType;
import com.ihg.atp.schema.crm.guest.guest.datatypes.v2.Addresses;
import com.ihg.atp.schema.crm.guest.guest.datatypes.v2.ExtendedPersonNameType;
import com.ihg.atp.schema.crm.guest.guest.datatypes.v2.GuestIdentityType;
import com.ihg.atp.schema.crm.guest.guest.datatypes.v2.Names;
import com.ihg.atp.schema.crm.guest.guest.datatypes.v2.ProfileType;
import com.ihg.atp.schema.crm.guest.guest.member.v2.ProgramMembershipKey;
import com.ihg.atp.schema.crm.guest.guest.servicetypes.v2.UpdateEmployeeRateEligibilityRequestType;
import com.ihg.atp.schema.crm.guest.membership.datatypes.v1.ActivitySourceType;
import com.ihg.atp.schema.crm.guest.membership.datatypes.v1.EnrollmentRequestType;
import com.ihg.atp.schema.crm.guest.membership.datatypes.v1.EnrollmentRequests;
import com.ihg.atp.schema.crm.guest.membership.datatypes.v1.MembershipInfoType;
import com.ihg.atp.schema.crm.guest.membership.servicetypes.v1.EnrollGuestRequestType;
import com.ihg.atp.schema.crm.guest.membership.servicetypes.v1.EnrollGuestResponseType;
import com.ihg.automation.common.DateUtils;
import com.ihg.automation.common.GregorianCalendarUtils;
import com.ihg.automation.selenium.common.Channel;
import com.ihg.automation.selenium.common.UI;
import com.ihg.automation.selenium.common.User;
import com.ihg.automation.selenium.common.member.GuestAddress;
import com.ihg.automation.selenium.common.member.GuestName;
import com.ihg.automation.selenium.common.member.MemberJdbcUtils;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.pages.program.EmployeeRateEligibility;
import com.ihg.automation.selenium.servicecenter.pages.GuestSearch;
import com.ihg.automation.selenium.servicecenter.pages.Role;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.RewardClubPage;
import com.ihg.crm.automation.selenium.tests.servicecenter.LoginLogout;

public class EmployeeRateEligibilityWithORMTest extends LoginLogout
{
    @Resource(name = "user2HT2")
    protected User user;

    private RewardClubPage rewardClubPage = new RewardClubPage();
    private EmployeeRateEligibility employeeRateEligibility;
    private static final String CHANNEL = UI.SC.getChannel();
    public static final LocalDate EXPIRES_DATE = DateUtils.getMonthsForward(13);
    public static final String EXPIRES = EXPIRES_DATE.toString(DateUtils.DATE_FORMAT);
    private String RATE_LABEL_YES = "Yes";
    public static final String MERLIN = "Merlin";
    public static final String HUMAN_RESOURCES = "HUMAN RESOURCES";

    @BeforeClass
    public void beforeClass()
    {
        EnrollGuestResponseType enrollGuestResponseType = membershipClient.enrollGuest(createRequest());

        String memberId = enrollGuestResponseType.getMemberships().getMembership().get(0).getMemberId();
        MemberJdbcUtils.waitAnnualActivitiesCounter(jdbcTemplate, memberId);

        guestClient.updateEmployeeRateEligibility(getUpdateEmployeeRateEligibilityRequestType(memberId));

        login(user, Role.CORPORATE_OFFICE_HUMAN_RESOURCES);

        new GuestSearch().byMemberNumber(memberId);
    }

    @Test
    public void verifyEligibilityDetails()
    {
        rewardClubPage.goTo();
        employeeRateEligibility = rewardClubPage.getEmployeeRateEligibility();

        employeeRateEligibility.verifyInViewMode(RATE_LABEL_YES, EXPIRES, NOT_AVAILABLE, MERLIN);
    }

    @Test(dependsOnMethods = { "verifyEligibilityDetails" }, alwaysRun = true)
    public void populateLocationToMemberWithORMandVerifyDetails()
    {
        employeeRateEligibility.clickEdit();

        employeeRateEligibility.verifyFieldsDisplayed(true);

        employeeRateEligibility.getLocation().select(HUMAN_RESOURCES);
        employeeRateEligibility.clickSave(verifySuccess(EMPLOYEE_RATE_ELIGIBILITY_SAVED));

        employeeRateEligibility.verifyInViewMode(RATE_LABEL_YES, EXPIRES, HUMAN_RESOURCES, CHANNEL);
    }

    private EnrollGuestRequestType createRequest()
    {
        EnrollGuestRequestType request = new EnrollGuestRequestType();

        ProfileType profileType = new ProfileType();
        profileType.setAddresses(getAddresses());
        profileType.setNames(getName());

        request.setEnrollmentRequests(getEnrollRequests());
        request.setProfile(profileType);

        return request;
    }

    private Addresses getAddresses()
    {
        GuestAddress guestAddress = MemberPopulateHelper.getUnitedStatesAddress();
        AddressType addressType = new AddressType();

        addressType.setCountryCode(guestAddress.getCountry().getCode());
        addressType.setLine1(guestAddress.getLine1());
        addressType.setLocality1(guestAddress.getLocality1());
        addressType.setRegion1(guestAddress.getRegion1().getCode());
        addressType.setPostalCode(guestAddress.getPostalCode());

        AddressContactType addressContactType = new AddressContactType();
        addressContactType.setAddress(addressType);
        addressContactType.setUsageType(UsageTypeType.HOME);

        List<AddressContactType> addressContactTypes = new ArrayList<AddressContactType>();
        addressContactTypes.add(addressContactType);

        Addresses addresses = new Addresses();
        addresses.setAddress(addressContactTypes);

        return addresses;
    }

    private Names getName()
    {
        GuestName guestName = MemberPopulateHelper.getRandomName();
        ExtendedPersonNameType personNameType = new ExtendedPersonNameType();

        personNameType.setGiven(guestName.getGiven());
        personNameType.setSurname(guestName.getSurname());

        List<ExtendedPersonNameType> namesList = new ArrayList<ExtendedPersonNameType>();
        namesList.add(personNameType);

        Names name = new Names();
        name.setName(namesList);

        return name;
    }

    private EnrollmentRequests getEnrollRequests()
    {

        MembershipInfoType membershipInfoType = new MembershipInfoType();
        membershipInfoType.setProgramCode("PC");

        MembershipInfoType employeeMembership = new MembershipInfoType();
        employeeMembership.setMemberId(user10T2.getEmployeeId());

        ActivitySourceType activitySourceType = new ActivitySourceType();
        activitySourceType.setChannel(UI.SC.getChannel());
        activitySourceType.setCode(SC);
        activitySourceType.setLocationCode("ATLCP");

        EnrollmentRequestType enrollmentRequestType = new EnrollmentRequestType();
        enrollmentRequestType.setMembership(membershipInfoType);
        enrollmentRequestType.setEmployeeMembership(employeeMembership);
        enrollmentRequestType.setSource(activitySourceType);
        enrollmentRequestType.setOfferTreatmentId("SCCAL");

        List<EnrollmentRequestType> enrollmentRequestList = new ArrayList<EnrollmentRequestType>();
        enrollmentRequestList.add(enrollmentRequestType);

        EnrollmentRequests enrollmentRequests = new EnrollmentRequests();
        enrollmentRequests.setEnrollmentRequest(enrollmentRequestList);

        return enrollmentRequests;
    }

    private UpdateEmployeeRateEligibilityRequestType getUpdateEmployeeRateEligibilityRequestType(String memberId)
    {
        com.ihg.atp.schema.crm.common.loyalty.loyalty.datatypes.v2.ActivitySourceType activitySourceType = new com.ihg.atp.schema.crm.common.loyalty.loyalty.datatypes.v2.ActivitySourceType();
        activitySourceType.setChannel(Channel.ORM);
        activitySourceType.setCode(com.ihg.atp.schema.crm.common.loyalty.loyalty.datatypes.v2.SourceType.CORPORATE);

        UpdateEmployeeRateEligibilityRequestType updateEmployeeRateEligibilityRequestType = new UpdateEmployeeRateEligibilityRequestType();
        updateEmployeeRateEligibilityRequestType.setSource(activitySourceType);

        GuestIdentityType guestIdentityType = new GuestIdentityType();

        ProgramMembershipKey programMembershipKey = new ProgramMembershipKey();
        programMembershipKey.setMemberId(memberId);
        programMembershipKey.setCode("PC");
        guestIdentityType.setProgramMembershipKey(programMembershipKey);

        updateEmployeeRateEligibilityRequestType.setGuestIdentity(guestIdentityType);

        UpdateEmployeeRateEligibilityRequestType.Expiration expiration = new UpdateEmployeeRateEligibilityRequestType.Expiration();
        expiration.setExpires(true);

        expiration.setExpirationDate(GregorianCalendarUtils.convert(EXPIRES_DATE));

        updateEmployeeRateEligibilityRequestType.setExpiration(expiration);
        updateEmployeeRateEligibilityRequestType.setEligible(true);

        return updateEmployeeRateEligibilityRequestType;
    }
}
