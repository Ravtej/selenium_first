package com.ihg.crm.automation.selenium.tests.servicecenter.programinfo.ambassador;

import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyNoError;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.event.EventTransactionType.Membership.MEMBERSHIP_OPEN;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.automation.selenium.matchers.component.HasSelectItems.hasSelectItems;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.catalog.CatalogItem;
import com.ihg.automation.selenium.common.member.AmbassadorJdbcUtils;
import com.ihg.automation.selenium.common.member.AmbassadorOfferCode;
import com.ihg.automation.selenium.common.member.AmbassadorUtils;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.pages.CreditCardType;
import com.ihg.automation.selenium.common.pages.PaymentMethod;
import com.ihg.automation.selenium.common.payment.CreditCardPayment;
import com.ihg.automation.selenium.common.payment.CreditCardPaymentContainer;
import com.ihg.automation.selenium.common.payment.PaymentDetailsPopUp;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.common.types.program.ProgramStatus;
import com.ihg.automation.selenium.gwt.components.Mode;
import com.ihg.automation.selenium.servicecenter.pages.LeftPanel;
import com.ihg.automation.selenium.servicecenter.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsGridRow;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsPage;
import com.ihg.automation.selenium.servicecenter.pages.event.enroll.MembershipDetails;
import com.ihg.automation.selenium.servicecenter.pages.event.enroll.MembershipGridRowContainer;
import com.ihg.automation.selenium.servicecenter.pages.programs.RenewExtendProgramSummary;
import com.ihg.automation.selenium.servicecenter.pages.programs.amb.AmbassadorPage;

public class ReactivateByCreditCardTest extends AmbassadorCommon
{
    private static final CatalogItem ENROLMENT_AWARD = AmbassadorUtils.AMBASSADOR_CORP_DISC_$150;

    private AmbassadorPage ambassadorPage = new AmbassadorPage();
    private AllEventsPage allEventsPage = new AllEventsPage();
    private CreditCardPayment creditCardItem = new CreditCardPayment(ENROLMENT_AWARD, "342507263407463",
            CreditCardType.AMERICAN_EXPRESS);
    private Member member;
    private EnrollmentPage enrollmentPage = new EnrollmentPage();

    @BeforeClass
    public void beforeClass()
    {
        login();

        member = enrollAMBmember();
        new AmbassadorJdbcUtils(jdbcTemplateUpdate, lastUpdateUserId).updateForReactivate(member);
        new LeftPanel().reOpenMemberProfile(member);

        enrollmentPage.goTo();
        enrollmentPage.getPrograms().selectProgram(Program.AMB);
    }

    @Test()
    public void populateCreditCardPaymentFields()
    {
        enrollmentPage.getAMBOfferCode().selectByCode(AmbassadorOfferCode.CHNRO);
        enrollmentPage.clickSubmit(verifyNoError());

        PaymentDetailsPopUp paymentDetailsPopUp = new PaymentDetailsPopUp();
        paymentDetailsPopUp.selectAmountAndPaymentMethod(ENROLMENT_AWARD, PaymentMethod.CREDIT_CARD);
        CreditCardPaymentContainer creditCardPayment = paymentDetailsPopUp.getCreditCardPayment();
        creditCardPayment.verifyPaymentFieldsDisplayed();
        verifyThat(creditCardPayment.getType(), hasSelectItems(CreditCardType.getDataList()));
        creditCardPayment.populate(creditCardItem);
        paymentDetailsPopUp.clickSubmitNoError();
    }

    @Test(dependsOnMethods = { "populateCreditCardPaymentFields" }, alwaysRun = true)
    public void verifySuccessEnrollmentPopUp()
    {
        validateSuccessEnrollmentPopUp(Program.AMB, member);
    }

    @Test(dependsOnMethods = { "verifySuccessEnrollmentPopUp" }, alwaysRun = true)
    public void verifySummary()
    {
        ambassadorPage.goTo();
        RenewExtendProgramSummary ambSummary = ambassadorPage.getSummary();
        verifyThat(ambSummary.getExpirationDate(), hasText(AmbassadorUtils.getExpirationDate()));

        ambSummary.verifyRenewAndExtendButtons(false, true);
        ambSummary.verifyStatus(ProgramStatus.OPEN);
    }

    @Test(dependsOnMethods = "verifySummary", alwaysRun = true)
    public void verifyAllEventsContainer()
    {
        allEventsPage.goTo();
        AllEventsGridRow row = allEventsPage.getGrid().getRow(MEMBERSHIP_OPEN);
        verifyThat(row, displayed(true));

        MembershipGridRowContainer container = row.expand(MembershipGridRowContainer.class);
        MembershipDetails membershipDetails = container.getMembershipDetails();
        membershipDetails.getPaymentDetails().verify(creditCardItem, Mode.VIEW);
        membershipDetails.verifyScSource(helper);
    }

    @Test(dependsOnMethods = "verifyAllEventsContainer", alwaysRun = true)
    public void verifyOrderSystemContainer()
    {
        verifyOrderSystemDetailsAfterReactivate(member);
    }
}
