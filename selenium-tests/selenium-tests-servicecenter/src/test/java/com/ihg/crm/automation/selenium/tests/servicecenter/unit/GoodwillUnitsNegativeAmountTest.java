package com.ihg.crm.automation.selenium.tests.servicecenter.unit;

import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyNoError;
import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifySuccess;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.assertNoErrors;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.Constant.RC_POINTS;
import static com.ihg.automation.selenium.common.event.EventTransactionType.Goodwill.GOODWILL;
import static com.ihg.automation.selenium.common.event.EventTransactionType.Goodwill.GOODWILL_CODE;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.isDisplayedWithWait;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;

import org.apache.commons.lang3.StringUtils;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.common.DateUtils;
import com.ihg.automation.selenium.common.billing.EventBillingDetailsTab;
import com.ihg.automation.selenium.common.event.AllEventsRow;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.types.GoodwillReason;
import com.ihg.automation.selenium.gwt.components.dialog.ConfirmDialog;
import com.ihg.automation.selenium.servicecenter.pages.annual.AnnualActivityPage;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsGridRow;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsPage;
import com.ihg.automation.selenium.servicecenter.pages.event.goodwill.GoodwillDetailsPopUp;
import com.ihg.automation.selenium.servicecenter.pages.event.goodwill.GoodwillDetailsTab;
import com.ihg.automation.selenium.servicecenter.pages.event.goodwill.GoodwillGridRowContainer;
import com.ihg.automation.selenium.servicecenter.pages.unit.GoodwillPopUp;

public class GoodwillUnitsNegativeAmountTest extends UnitsCommon
{
    private Member recipient;
    private GoodwillGridRowContainer goodwillGridRowContainer;

    private static final int GOODWILL_POINTS = -2000;
    private static final String GOODWILL_POINTS_STRING = Integer.toString(GOODWILL_POINTS);
    private static final GoodwillReason GOODWILL_REASON = GoodwillReason.AGENT_ERROR;

    private AllEventsRow goodWillEventRow = new AllEventsRow(DateUtils.getFormattedDate(), GOODWILL, RC_POINTS,
            GOODWILL_POINTS_STRING, StringUtils.EMPTY);

    @BeforeClass
    public void beforeClass()
    {
        login();
        recipient = new AnnualActivityPage().captureCounters(enrollMember(), jdbcTemplate);
    }

    @Test
    public void goodwillNegativePoints()
    {
        GoodwillPopUp goodwillPopUp = new GoodwillPopUp();

        goodwillPopUp.goTo();
        assertNoErrors();

        goodwillPopUp.populate(GOODWILL_POINTS_STRING, RC_POINTS, GOODWILL_REASON);
        goodwillPopUp.clickSubmitNoError();

        ConfirmDialog dialog = new ConfirmDialog();
        verifyThat(dialog, isDisplayedWithWait());
        verifyThat(dialog, hasText(
                "The amount entered will result in a negative point balance, are you sure you want to continue?"));
        dialog.clickYes(verifyNoError(), verifySuccess(GoodwillPopUp.GOODWILL_SUCCESS));

        verifyThat(dialog, displayed(false));
        verifyThat(goodwillPopUp, displayed(false));
    }

    @Test(dependsOnMethods = { "goodwillNegativePoints" }, priority = 10)
    public void verifyActivityCounters()
    {
        recipient.getLifetimeActivity().setAdjustedUnits(GOODWILL_POINTS);
        recipient.getLifetimeActivity().setTotalUnits(GOODWILL_POINTS);
        recipient.getLifetimeActivity().setCurrentBalance(GOODWILL_POINTS);
        recipient.getAnnualActivity().setTotalUnits(GOODWILL_POINTS);

        new AnnualActivityPage().verifyCounters(recipient);
    }

    @Test(dependsOnMethods = { "goodwillNegativePoints" }, priority = 20)
    public void verifyGoodwillEventRow()
    {
        AllEventsPage allEventsPage = new AllEventsPage();
        allEventsPage.goTo();

        AllEventsGridRow row = allEventsPage.getGrid().getRow(1);
        row.verify(goodWillEventRow);

        goodwillGridRowContainer = row.expand(GoodwillGridRowContainer.class);
        goodwillGridRowContainer.getGoodwillDetails().verify(GOODWILL_REASON, helper);
    }

    @Test(dependsOnMethods = { "goodwillNegativePoints" }, priority = 30)
    public void verifyGoodwillEventDetailsPopUp()
    {
        goodwillGridRowContainer.clickDetails();

        GoodwillDetailsPopUp goodwillDetailsPopUp = new GoodwillDetailsPopUp();

        GoodwillDetailsTab goodwillDetailsTab = goodwillDetailsPopUp.getGoodwillDetailsTab();
        goodwillDetailsTab.goTo();
        goodwillDetailsTab.getGoodwillDetails().verify(GOODWILL_REASON, helper);

        goodwillDetailsPopUp.getEarningDetailsTab().verifyForBaseUnitsEvent(GOODWILL_POINTS, 0);

        EventBillingDetailsTab billingDetailsTab = goodwillDetailsPopUp.getBillingDetailsTab();
        billingDetailsTab.goTo();
        billingDetailsTab.verifyTabIsEmpty(GOODWILL_CODE);

        goodwillDetailsPopUp.clickClose();
    }

    @Test(dependsOnMethods = { "goodwillNegativePoints" }, priority = 40)
    public void goodwillNegativePointsOneMore()
    {
        GoodwillPopUp goodwillPopUp = new GoodwillPopUp();

        goodwillPopUp.goTo();
        assertNoErrors();

        goodwillPopUp.populate(GOODWILL_POINTS_STRING, RC_POINTS, GOODWILL_REASON);
        goodwillPopUp.clickSubmit(verifyNoError());

        verifyThat(new ConfirmDialog(), displayed(false), "Due to the balance has already negative");
        verifyThat(goodwillPopUp, displayed(false));
    }

}
