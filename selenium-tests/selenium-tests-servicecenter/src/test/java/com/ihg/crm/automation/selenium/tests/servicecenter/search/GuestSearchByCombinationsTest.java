package com.ihg.crm.automation.selenium.tests.servicecenter.search;

import static com.ihg.automation.common.RandomUtils.getRandomNumber;
import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyNoError;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.gwt.components.Mode.VIEW;
import static com.ihg.automation.selenium.hotel.pages.search.GuestSearchResultsGridRow.GuestSearchCell.GUEST_NAME;
import static com.ihg.automation.selenium.matchers.component.GridSize.size;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.greaterThan;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.components.Country;
import com.ihg.automation.selenium.common.member.GuestAddress;
import com.ihg.automation.selenium.common.member.GuestName;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.hotel.pages.search.GuestSearchResultsGrid;
import com.ihg.automation.selenium.servicecenter.pages.GuestSearch;
import com.ihg.automation.selenium.servicecenter.pages.LeftPanel;
import com.ihg.automation.selenium.servicecenter.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.servicecenter.pages.personal.PersonalInfoPage;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.RewardClubPage;
import com.ihg.crm.automation.selenium.tests.servicecenter.LoginLogout;

public class GuestSearchByCombinationsTest extends LoginLogout
{
    public static final String ALTERNATE_LOYALTY_NUMBER = getRandomNumber(9);
    private static final Logger logger = LoggerFactory.getLogger(GuestSearchByCombinationsTest.class);
    public static final String ADDRESS_HAS_BEEN_CLEANSED_MESSAGE = "Address has been cleansed";
    private Member member = new Member();
    private GuestSearch guestSearch = new GuestSearch();
    private RewardClubPage rewardClubPage = new RewardClubPage();

    @BeforeClass
    public void before()
    {
        member.addProgram(Program.RC);
        member.setPersonalInfo(MemberPopulateHelper.getSimplePersonalInfo());
        member.addAddress(MemberPopulateHelper.getUnitedStatesAddress());
        member.addPhone(MemberPopulateHelper.getRandomPhone());
        member.addEmail(MemberPopulateHelper.getRandomEmail());

        login();

        member = new EnrollmentPage().enroll(member);
        rewardClubPage.addAlternateLoyaltyId(ALTERNATE_LOYALTY_NUMBER);

        new LeftPanel().goToNewSearch();
    }

    @AfterMethod
    public void afterMethod()
    {
        new LeftPanel().goToNewSearch();
    }

    @Test
    public void findMemberByAlternateId()
    {
        guestSearch.getCustomerSearch().getNumber().type(ALTERNATE_LOYALTY_NUMBER);

        guestSearch.clickSearch(verifyNoError());

        verifyPersonalData();

        rewardClubPage.goTo();
        rewardClubPage.getAlternateLoyaltyID().getRow(1).verifyHertz(ALTERNATE_LOYALTY_NUMBER);
    }

    @Test
    public void findMemberByEmail()
    {
        guestSearch.getCustomerSearch().getEmail().type(member.getPreferredEmail().getEmail());

        guestSearch.clickSearch(verifyNoError());

        verifyPersonalData();
    }

    @Test
    public void findMemberByPhoneAndName()
    {
        guestSearch.getCustomerSearch().getPhone().type(member.getPreferredPhone().getNumber());

        fillInNameAndSurname();

        guestSearch.clickSearch(verifyNoError());

        verifyPersonalData();
    }

    @Test
    public void findMemberByNameAndSurname()
    {
        fillInNameAndSurname();

        guestSearch.clickSearch(verifyNoError());

        verifyPersonalData();
    }

    @Test
    public void findMemberByAddressAndName()
    {

        fillInNameAndSurname();

        fillInAddress();

        guestSearch.clickSearch(verifyNoError());

        verifyPersonalData();
    }

    @Test
    public void findMemberByAddressAndPhone()
    {
        guestSearch.getCustomerSearch().getPhone().type(member.getPreferredPhone().getNumber());

        fillInAddress();

        guestSearch.clickSearch(verifyNoError());

        verifyPersonalData();
    }

    @Test
    public void findMemberByAddressAndEmail()
    {
        guestSearch.getCustomerSearch().getEmail().type(member.getPreferredEmail().getEmail());

        fillInAddress();

        guestSearch.clickSearch(verifyNoError());

        verifyPersonalData();
    }

    @Test
    public void findMultipleGuests()
    {
        GuestSearch.CustomerSearch customerSearch = guestSearch.getCustomerSearch();

        customerSearch.getFirstName().type("cyarges");
        customerSearch.getLastName().type("smoke");

        guestSearch.clickSearch(verifyNoError());

        verifyThat(new GuestSearchResultsGrid(), size(greaterThan(1)));
    }

    @Test
    public void trilliumCleansingVerification()
    {
        GuestSearch.CustomerSearch customerSearch = guestSearch.getCustomerSearch();

        customerSearch.getFirstName().type("Smith");
        customerSearch.getPhone().type("123456");

        customerSearch.getStreetAddress().type("22 orsel");
        customerSearch.getCity().type("Paris");
        customerSearch.getCountry().select(Country.FR);
        customerSearch.getZip().type("123456");

        guestSearch.clickSearch(verifyNoError());

        verifyThat(customerSearch, hasText(containsString(ADDRESS_HAS_BEEN_CLEANSED_MESSAGE)));
    }

    private void fillInNameAndSurname()
    {
        GuestName name = member.getName();

        GuestSearch.CustomerSearch customerSearch = guestSearch.getCustomerSearch();
        customerSearch.getLastName().type(name.getSurname());
        customerSearch.getFirstName().type(name.getGiven());
    }

    private void fillInAddress()
    {
        GuestAddress guestAddress = member.getPreferredAddress();

        GuestSearch.CustomerSearch customerSearch = guestSearch.getCustomerSearch();
        customerSearch.getStreetAddress().type(guestAddress.getLine1());
        customerSearch.getCity().type(guestAddress.getRegion1().toString());
        customerSearch.getZip().type(guestAddress.getPostalCode());
    }

    private void verifyPersonalData()
    {
        PersonalInfoPage personalInfoPage = new PersonalInfoPage();

        if (!personalInfoPage.isDisplayed())
        {
            logger.debug("Multiple results were found, select guest by memberId");
            new GuestSearchResultsGrid().getRow(member).getCell(GUEST_NAME).clickAndWait();
        }
        personalInfoPage.getCustomerInfo().verify(member, VIEW);
        personalInfoPage.getAddressList().getExpandedContact().verify(member.getPreferredAddress(), VIEW);
        personalInfoPage.getPhoneList().getExpandedContact().verify(member.getPreferredPhone(), VIEW);
        personalInfoPage.getEmailList().getExpandedContact().verify(member.getPreferredEmail(), VIEW);
    }
}
