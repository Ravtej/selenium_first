package com.ihg.crm.automation.selenium.tests.servicecenter.stay.disqualified;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.event.AllEventsRow;
import com.ihg.automation.selenium.common.event.AllEventsRowConverter;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.stay.Revenue;
import com.ihg.automation.selenium.common.stay.Stay;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.common.types.program.RewardClubLevel;
import com.ihg.automation.selenium.gwt.components.Mode;
import com.ihg.automation.selenium.servicecenter.pages.LeftPanel;
import com.ihg.automation.selenium.servicecenter.pages.annual.AnnualActivityPage;
import com.ihg.automation.selenium.servicecenter.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsGridRow;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsPage;
import com.ihg.automation.selenium.servicecenter.pages.event.stay.StayEventsPage;
import com.ihg.automation.selenium.servicecenter.pages.event.stay.StayGuestGridRow;
import com.ihg.automation.selenium.servicecenter.pages.event.stay.StayGuestGridRowContainer;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.RewardClubPage;
import com.ihg.crm.automation.selenium.tests.servicecenter.LoginLogout;

public class StayNotEnoughQualifyingRevenueTest extends LoginLogout
{
    private static final String NOT_ENOUGH_ROOM_REVENUE = "Not Enough Room Revenue";
    private static final String NOT_ENOUGH_TOTAL_REVENUE = "Not Enough Total Revenue";

    private Member member = new Member();
    private Stay stay = new Stay();
    private StayEventsPage stayEventsPage = new StayEventsPage();
    private StayGuestGridRow stayGuestGridRow;
    private AllEventsGridRow allEventsGridRow;
    private Revenue room = new Revenue();

    @BeforeClass
    public void beforeClass()
    {
        stay.setHotelCode("ATLCP");
        stay.setBrandCode("HICP");
        stay.setCheckInOutByNights(2);
        stay.setAvgRoomRate(0.00);
        stay.setQualifyingNights(0);
        stay.setTierLevelNights("0");
        stay.setRateCode("1");
        stay.setIsQualifying(false);

        room.setAllowOverride(true);
        room.setQualifying(false);
        room.setBillHotel(false);

        member.setPersonalInfo(MemberPopulateHelper.getSimplePersonalInfo());
        member.addAddress(MemberPopulateHelper.getUnitedStatesAddress());
        member.addProgram(Program.RC);

        login();

        member = new AnnualActivityPage().captureCounters(new EnrollmentPage().enroll(member), jdbcTemplate);
    }

    @Test
    public void createNotEnoughQualifyingRevenueStay()
    {
        stayEventsPage.createStayAndVerifySuccessMessage(stay, NOT_ENOUGH_ROOM_REVENUE, NOT_ENOUGH_TOTAL_REVENUE);
    }

    @Test(dependsOnMethods = { "createNotEnoughQualifyingRevenueStay" }, alwaysRun = true)
    public void verifyStayCreationEvent()
    {
        stayGuestGridRow = stayEventsPage.getGuestGrid().getRow(1);
        stayGuestGridRow.verify(stay);
    }

    @Test(dependsOnMethods = { "verifyStayCreationEvent" }, alwaysRun = true)
    public void verifyDisqualifyReasonPopUp()
    {
        stayGuestGridRow.verifyDisqualifyPopUp(NOT_ENOUGH_ROOM_REVENUE, NOT_ENOUGH_TOTAL_REVENUE);
    }

    @Test(dependsOnMethods = { "verifyDisqualifyReasonPopUp" }, alwaysRun = true)
    public void verifyRevenueDetails()
    {
        stayGuestGridRow.expand(StayGuestGridRowContainer.class).verifyRoomRevenueDetails(room, Mode.EDIT);
    }

    @Test(dependsOnMethods = { "verifyRevenueDetails" }, alwaysRun = true)
    public void verifyAllEventsGridRow()
    {
        AllEventsRow stayEvent = AllEventsRowConverter.convert(stay);

        AllEventsPage allEventsPage = new AllEventsPage();
        allEventsPage.goTo();
        allEventsGridRow = allEventsPage.getGrid().getRow(stayEvent.getTransType());
        allEventsGridRow.verify(stayEvent);
    }

    @Test(dependsOnMethods = { "verifyAllEventsGridRow" }, alwaysRun = true)
    public void verifyAllEventsGridRowDetails()
    {
        allEventsGridRow.expand(StayGuestGridRowContainer.class).verifyRoomRevenueDetails(room, Mode.VIEW);
    }

    @Test(dependsOnMethods = { "verifyAllEventsGridRowDetails" }, alwaysRun = true)
    public void verifyLeftPanelRCPointsBalance()
    {
        new LeftPanel().getProgramInfoPanel().getPrograms().getRewardClubProgram().verify(RewardClubLevel.CLUB, "0");
    }

    @Test(dependsOnMethods = { "verifyLeftPanelRCPointsBalance" }, alwaysRun = true)
    public void verifyActivityCounters()
    {
        member.getAnnualActivity().setNonQualifiedNights(2);

        new AnnualActivityPage().verifyCounters(member);
    }

    @Test(dependsOnMethods = { "verifyActivityCounters" }, alwaysRun = true)
    public void verifyRewardNightTierLevelCounters()
    {
        RewardClubPage rewardClubPage = new RewardClubPage();
        rewardClubPage.goTo();
        rewardClubPage.getCurrentYearAnnualActivities().getPointsRow().verify("0", achieveGoldPoints);
    }
}
