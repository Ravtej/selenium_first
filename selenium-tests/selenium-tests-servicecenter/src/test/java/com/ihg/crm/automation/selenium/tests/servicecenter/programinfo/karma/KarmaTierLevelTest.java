package com.ihg.crm.automation.selenium.tests.servicecenter.programinfo.karma;

import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyError;
import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyNoError;
import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifySuccess;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.assertThat;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.types.program.KarmaLevel.KAR;
import static com.ihg.automation.selenium.common.types.program.KarmaLevel.KIC;
import static com.ihg.automation.selenium.common.types.program.RewardClubLevel.CLUB;
import static com.ihg.automation.selenium.common.types.program.RewardClubLevel.SPRE;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.isDisplayedWithWait;
import static com.ihg.automation.selenium.matchers.component.HasSelectItems.hasSelectItems;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static com.ihg.automation.selenium.matchers.component.ViewableMatcher.hasTextInView;
import static com.ihg.automation.selenium.matchers.text.EntityMatcherFactory.isValue;
import static com.ihg.automation.selenium.servicecenter.pages.personal.PersonalInfoPage.SUCCESS_MESSAGE;
import static org.hamcrest.core.StringEndsWith.endsWith;

import java.util.Arrays;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.ProgramsGrid;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.types.program.KarmaLevelReason;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.gwt.components.input.Select;
import com.ihg.automation.selenium.gwt.components.panel.FieldSet;
import com.ihg.automation.selenium.servicecenter.pages.LeftPanel;
import com.ihg.automation.selenium.servicecenter.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.servicecenter.pages.programs.KarmaPage;
import com.ihg.automation.selenium.servicecenter.pages.programs.ProgramSummary;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.RewardClubPage;
import com.ihg.crm.automation.selenium.tests.servicecenter.LoginLogout;

@Test(groups = { "karma" })
public class KarmaTierLevelTest extends LoginLogout
{
    private KarmaPage karmaPage = new KarmaPage();
    private ProgramSummary summary;

    @BeforeClass
    public void beforeClass()
    {
        Member member = new Member();
        member.setPersonalInfo(MemberPopulateHelper.getSimplePersonalInfo());
        member.addAddress(MemberPopulateHelper.getUnitedStatesAddress());
        member.addProgram(Program.KAR);

        login();

        new EnrollmentPage().enroll(member);
        verifyThat(new LeftPanel().getProgramInfoPanel().getPrograms().getRewardClubProgram().getLevelCode(),
                hasText(CLUB.getCode()));
    }

    @Test
    public void verifySummaryPanelContent()
    {
        karmaPage.goTo();
        summary = karmaPage.getSummary();
        summary.clickEdit();

        Select tierLevel = summary.getTierLevel();
        assertThat(tierLevel, isDisplayedWithWait());
        verifyThat(tierLevel, hasText(isValue(KAR)));
        verifyThat(tierLevel, hasSelectItems(Arrays.asList(KAR.getValue(), KIC.getValue())));
    }

    @Test(dependsOnMethods = { "verifySummaryPanelContent" }, alwaysRun = true)
    public void verifyTierLevelReasonContent()
    {
        summary.getTierLevel().selectByValue(KIC);

        Select tierLevelReason = summary.getTierLevelReason();
        assertThat(tierLevelReason, isDisplayedWithWait());
        verifyThat(tierLevelReason, hasText("Select"));
        verifyThat(tierLevelReason, hasSelectItems(KarmaLevelReason.getValuesList()));

        verifyThat(summary.getReferringMember(), isDisplayedWithWait());
    }

    @Test(dependsOnMethods = { "verifyTierLevelReasonContent" }, alwaysRun = true)
    public void verifyErrorMessageWithNotSavedTierLevel()
    {
        summary.getTierLevel().selectByValue(KIC);
        new RewardClubPage().getTab().click(verifyError(RewardClubPage.WARNING_MESSAGE));
        summary.clickCancel();
        verifyThat(summary.getTierLevel(), hasTextInView(isValue(KAR)));
    }

    @DataProvider(name = "reasonProvider")
    protected Object[][] reasonProvider()
    {
        return new Object[][] { { KarmaLevelReason.BASE }, { KarmaLevelReason.EXECUTIVE }, { KarmaLevelReason.GRACE },
                { KarmaLevelReason.POINTS }, { KarmaLevelReason.VIP } };
    }

    @Test(dependsOnMethods = {
            "verifyErrorMessageWithNotSavedTierLevel" }, dataProvider = "reasonProvider", alwaysRun = true)
    public void setTierLevel(KarmaLevelReason reason)
    {
        summary.setTierLevelWithExpiration(KIC, reason, verifyNoError(), verifySuccess(SUCCESS_MESSAGE));

        FieldSet programOverview = karmaPage.getProgramOverview();
        programOverview.expand();
        verifyThat(programOverview, hasText(endsWith(KIC.getValue())));
        ProgramsGrid programs = new LeftPanel().getProgramInfoPanel().getPrograms();
        verifyThat(programs.getProgram(Program.KAR).getLevelCode(), hasText(KIC.getCode()));
        verifyThat(programs.getRewardClubProgram().getLevelCode(), hasText(SPRE.getCode()));

        summary.setTierLevelWithoutExpiration(KAR, reason, verifyNoError(), verifySuccess(SUCCESS_MESSAGE));

        programOverview.expand();
        verifyThat(programOverview, hasText(endsWith(KAR.getValue())));
        programs = new LeftPanel().getProgramInfoPanel().getPrograms();
        verifyThat(programs.getProgram(Program.KAR).getLevelCode(), hasText(KAR.getCode()));
        verifyThat(programs.getRewardClubProgram().getLevelCode(), hasText(SPRE.getCode()));
    }
}
