package com.ihg.crm.automation.selenium.tests.servicecenter.preferences;

import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyNoError;
import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifySuccess;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.assertThat;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasText;
import static com.ihg.automation.selenium.matchers.component.GridSize.size;
import static com.ihg.automation.selenium.matchers.text.EntityMatcherFactory.isValue;
import static com.ihg.automation.selenium.servicecenter.pages.history.AuditConstant.TravelProfile.CORPORATE_ACCOUNT_NUMBER;
import static com.ihg.automation.selenium.servicecenter.pages.history.AuditConstant.TravelProfile.DEFAULT;
import static org.hamcrest.number.OrderingComparison.greaterThanOrEqualTo;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.Constant;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.member.OccupationInfo;
import com.ihg.automation.selenium.common.preferences.TravelProfileType;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.servicecenter.pages.LeftPanel;
import com.ihg.automation.selenium.servicecenter.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.servicecenter.pages.history.ProfileHistoryGridRow;
import com.ihg.automation.selenium.servicecenter.pages.history.ProfileHistoryPage;
import com.ihg.automation.selenium.servicecenter.pages.occupation.AdvancedCorporateAccountSearchPopUp;
import com.ihg.automation.selenium.servicecenter.pages.occupation.OccupationInfoFields;
import com.ihg.automation.selenium.servicecenter.pages.preferences.AddTravelProfilePopUp;
import com.ihg.automation.selenium.servicecenter.pages.preferences.StayPreferencesPage;
import com.ihg.automation.selenium.servicecenter.pages.preferences.TravelProfile;
import com.ihg.automation.selenium.servicecenter.pages.preferences.TravelProfileGridRow;
import com.ihg.automation.selenium.servicecenter.pages.preferences.TravelProfilePopUpBase;
import com.ihg.crm.automation.selenium.tests.servicecenter.LoginLogout;

public class CorporateAccountTest extends LoginLogout
{
    private static final String SEC_TP_NAME = "Test11";
    private OccupationInfo primaryOccupationInfo;
    private OccupationInfo newOccupationInfo;
    private TravelProfile travelProfile;
    private OccupationInfo updatedOccupationInfo;
    private Member member = new Member();

    @BeforeClass
    public void beforeClass()
    {
        member.addProgram(Program.RC);
        member.addAddress(MemberPopulateHelper.getUnitedStatesAddress());
        member.setPersonalInfo(MemberPopulateHelper.getSimplePersonalInfo());

        login();
        new EnrollmentPage().enroll(member);
    }

    @Test()
    public void addCorporateAccountToExistingTravelProfile()
    {
        TravelProfile travelProfile = TravelProfile.getDefaultTravelProfile();

        primaryOccupationInfo = new OccupationInfo("951103512", "BOEING CO");
        travelProfile.setCorporateId(primaryOccupationInfo);
        StayPreferencesPage stayPreferencesPage = new StayPreferencesPage();

        stayPreferencesPage.goTo();
        TravelProfileGridRow row = stayPreferencesPage.getTravelProfilesGrid()
                .getRowByName(TravelProfileType.LEISURE.getValue());
        TravelProfilePopUpBase travelProfilePopUpEdit = row.openTravelProfile();

        OccupationInfoFields occupationInfoFields = travelProfilePopUpEdit.getOccupationInfoFields();
        occupationInfoFields.getCorporateNumber().clickButton();

        AdvancedCorporateAccountSearchPopUp popUp = new AdvancedCorporateAccountSearchPopUp();
        popUp.getSearchFields().getCorporateAccountName().typeAndWait(primaryOccupationInfo.getCorporateName());
        popUp.getButtonBar().clickSearch();

        verifyThat(popUp.getAdvancedCorporateAccountGrid(), size(greaterThanOrEqualTo(1)));
        popUp.getButtonBar().clickSelectCorporateAccount();
        assertThat(popUp, displayed(false));

        verifyThat(occupationInfoFields.getCorporateNumber(), displayed(true));
        verifyThat(occupationInfoFields.getCorporateName(), hasText(primaryOccupationInfo.getCorporateName()));
        travelProfilePopUpEdit.clickSave(verifyNoError(), verifySuccess(TravelProfilePopUpBase.SUCCESS_MESSAGE_UPDATE));

        row = stayPreferencesPage.getTravelProfilesGrid().getRowByName(TravelProfileType.LEISURE.getValue());

        row.verify(travelProfile);

        row.openTravelProfile();
        verifyThat(occupationInfoFields.getCorporateNumber(), hasText(primaryOccupationInfo.getCorporateNumber()));
        verifyThat(occupationInfoFields.getCorporateName(), hasText(primaryOccupationInfo.getCorporateName()));

        travelProfilePopUpEdit.clickClose(verifyNoError());
    }

    @Test(dependsOnMethods = { "addCorporateAccountToExistingTravelProfile" }, alwaysRun = true)
    public void verifyHistoryAfterAddCorporateAccountToExistingTravelProfile()
    {
        ProfileHistoryPage historyPage = new ProfileHistoryPage();
        historyPage.goTo();
        verifyNoError();

        ProfileHistoryGridRow travelProfileRow = historyPage.getProfileHistoryGrid().getRow(1).getSubRowByName(
                com.ihg.automation.selenium.servicecenter.pages.history.AuditConstant.TravelProfile.TRAVEL_PROFILE);

        travelProfileRow.getSubRowByName(CORPORATE_ACCOUNT_NUMBER)
                .verifyAddAction(primaryOccupationInfo.getCorporateNumber());
    }

    @Test(dependsOnMethods = { "verifyHistoryAfterAddCorporateAccountToExistingTravelProfile" }, alwaysRun = true)
    public void addCorporateAccountToNewTravelProfile()
    {
        new LeftPanel().reOpenMemberProfile(member);

        travelProfile = new TravelProfile(SEC_TP_NAME, TravelProfileType.LEISURE, true);
        StayPreferencesPage stayPreferencesPage = new StayPreferencesPage();
        stayPreferencesPage.goTo();
        newOccupationInfo = new OccupationInfo("100230372", "EPAM SYSTEMS");
        travelProfile.setCorporateId(newOccupationInfo);

        stayPreferencesPage.getAddTravelProfile().clickAndWait(verifyNoError());
        TravelProfilePopUpBase travelProfilePopUpAdd = new AddTravelProfilePopUp();
        travelProfilePopUpAdd.getTravelProfileName().type(SEC_TP_NAME);
        travelProfilePopUpAdd.getType().select(TravelProfileType.LEISURE.getCodeWithValue());
        OccupationInfoFields occupationInfoFields = travelProfilePopUpAdd.getOccupationInfoFields();
        occupationInfoFields.getCorporateNumber().clickButton();

        AdvancedCorporateAccountSearchPopUp popUp = new AdvancedCorporateAccountSearchPopUp();
        popUp.getSearchFields().getCorporateAccountName().typeAndWait(newOccupationInfo.getCorporateName());
        popUp.getButtonBar().clickSearch();

        popUp.getButtonBar().clickSelectCorporateAccount();

        travelProfilePopUpAdd.clickSave(verifyNoError(), verifySuccess(TravelProfilePopUpBase.SUCCESS_MESSAGE_CREATE));

        TravelProfileGridRow row = stayPreferencesPage.getTravelProfilesGrid().getRowByName(SEC_TP_NAME);

        row.verify(travelProfile);

        TravelProfilePopUpBase travelProfilePopUpEdit = row.openTravelProfile();
        OccupationInfoFields occupationInfoFieldsUpdate = travelProfilePopUpEdit.getOccupationInfoFields();
        verifyThat(occupationInfoFieldsUpdate.getCorporateNumber(), hasText(newOccupationInfo.getCorporateNumber()));
        verifyThat(occupationInfoFieldsUpdate.getCorporateName(), hasText(newOccupationInfo.getCorporateName()));
        travelProfilePopUpEdit.clickClose(verifyNoError());
    }

    @Test(dependsOnMethods = { "addCorporateAccountToNewTravelProfile" }, alwaysRun = true)
    public void verifyHistoryAfterAddCorporateAccountToNewTravelProfile()
    {
        ProfileHistoryPage historyPage = new ProfileHistoryPage();
        historyPage.goTo();
        verifyNoError();

        ProfileHistoryGridRow travelProfileRow = historyPage.getProfileHistoryGrid().getRow(1).getSubRowByName(
                com.ihg.automation.selenium.servicecenter.pages.history.AuditConstant.TravelProfile.TRAVEL_PROFILE);

        travelProfileRow.getSubRowByName(CORPORATE_ACCOUNT_NUMBER)
                .verifyAddAction(newOccupationInfo.getCorporateNumber());

        travelProfileRow.getSubRowByName(DEFAULT).verifyAddAction("No");
        travelProfileRow
                .getSubRowByName(
                        com.ihg.automation.selenium.servicecenter.pages.history.AuditConstant.TravelProfile.TYPE)
                .verifyAddAction(hasText(isValue(travelProfile.getProfileType())));
        travelProfileRow
                .getSubRowByName(
                        com.ihg.automation.selenium.servicecenter.pages.history.AuditConstant.TravelProfile.NAME)
                .verifyAddAction(travelProfile.getProfileName());
    }

    @Test(dependsOnMethods = { "verifyHistoryAfterAddCorporateAccountToNewTravelProfile" }, alwaysRun = true)
    public void updateCorporateAccountNumberForExistingTravelProfile()
    {
        new LeftPanel().reOpenMemberProfile(member);

        TravelProfile travelProfile = new TravelProfile(SEC_TP_NAME, TravelProfileType.LEISURE, true);
        StayPreferencesPage stayPreferencesPage = new StayPreferencesPage();
        stayPreferencesPage.goTo();
        updatedOccupationInfo = new OccupationInfo("100179926", "ORACLE CORP");
        travelProfile.setCorporateId(updatedOccupationInfo);

        TravelProfileGridRow row = stayPreferencesPage.getTravelProfilesGrid().getRowByName(SEC_TP_NAME);
        TravelProfilePopUpBase travelProfilePopUpEdit = row.openTravelProfile();
        OccupationInfoFields occupationInfoFields = travelProfilePopUpEdit.getOccupationInfoFields();
        occupationInfoFields.getCorporateNumber().clickButton();

        AdvancedCorporateAccountSearchPopUp popUp = new AdvancedCorporateAccountSearchPopUp();
        popUp.getSearchFields().getCorporateAccountName().typeAndWait(updatedOccupationInfo.getCorporateName());
        popUp.getButtonBar().clickSearch();

        popUp.getButtonBar().clickSelectCorporateAccount();

        travelProfilePopUpEdit.clickSave(verifyNoError(), verifySuccess(TravelProfilePopUpBase.SUCCESS_MESSAGE_UPDATE));

        TravelProfileGridRow rowUpdated = stayPreferencesPage.getTravelProfilesGrid().getRowByName(SEC_TP_NAME);

        rowUpdated.verify(travelProfile);

        rowUpdated.openTravelProfile();
        verifyThat(occupationInfoFields.getCorporateNumber(), hasText(updatedOccupationInfo.getCorporateNumber()));
        verifyThat(occupationInfoFields.getCorporateName(), hasText(updatedOccupationInfo.getCorporateName()));
        travelProfilePopUpEdit.clickClose(verifyNoError());
    }

    @Test(dependsOnMethods = {
            "updateCorporateAccountNumberForExistingTravelProfile" }, description = "DE5847 - Incorrect History record "
                    + "for updated Corporate Account Number", alwaysRun = true)
    public void verifyHistoryAfterUpdateCorporateAccountToNewTravelProfile()
    {
        ProfileHistoryPage historyPage = new ProfileHistoryPage();
        historyPage.goTo();
        verifyNoError();

        ProfileHistoryGridRow travelProfileRow = historyPage.getProfileHistoryGrid().getRow(1).getSubRowByName(
                com.ihg.automation.selenium.servicecenter.pages.history.AuditConstant.TravelProfile.TRAVEL_PROFILE);

        travelProfileRow.getSubRowByName(CORPORATE_ACCOUNT_NUMBER).verify(newOccupationInfo.getCorporateNumber(),
                updatedOccupationInfo.getCorporateNumber());
    }

    @Test(dependsOnMethods = { "verifyHistoryAfterUpdateCorporateAccountToNewTravelProfile" }, alwaysRun = true)
    public void deleteCorporateAccountNumber()
    {
        new LeftPanel().reOpenMemberProfile(member);

        TravelProfile travelProfile = new TravelProfile(SEC_TP_NAME, TravelProfileType.LEISURE, true);

        StayPreferencesPage stayPreferencesPage = new StayPreferencesPage();
        stayPreferencesPage.goTo();
        TravelProfileGridRow row = stayPreferencesPage.getTravelProfilesGrid().getRowByName(SEC_TP_NAME);
        TravelProfilePopUpBase travelProfilePopUpEdit = row.openTravelProfile();

        OccupationInfoFields occupationInfoFields = travelProfilePopUpEdit.getOccupationInfoFields();
        occupationInfoFields.getCorporateNumber().clear();
        travelProfilePopUpEdit.clickSave(verifySuccess(TravelProfilePopUpBase.SUCCESS_MESSAGE_UPDATE));

        row = stayPreferencesPage.getTravelProfilesGrid().getRowByName(SEC_TP_NAME);

        row.verify(travelProfile);

        row.openTravelProfile();
        verifyThat(occupationInfoFields.getCorporateNumber(), hasText(""));
        verifyThat(occupationInfoFields.getCorporateName(), hasText(Constant.NOT_AVAILABLE));
        travelProfilePopUpEdit.clickClose(verifyNoError());
    }

    @Test(dependsOnMethods = { "deleteCorporateAccountNumber" }, alwaysRun = true)
    public void verifyHistoryAfterDeleteCorporateAccountToNewTravelProfile()
    {
        ProfileHistoryPage historyPage = new ProfileHistoryPage();
        historyPage.goTo();
        verifyNoError();

        ProfileHistoryGridRow travelProfileRow = historyPage.getProfileHistoryGrid().getRow(1).getSubRowByName(
                com.ihg.automation.selenium.servicecenter.pages.history.AuditConstant.TravelProfile.TRAVEL_PROFILE);

        travelProfileRow.getSubRowByName(CORPORATE_ACCOUNT_NUMBER)
                .verifyDeleteAction(updatedOccupationInfo.getCorporateNumber());
    }
}
