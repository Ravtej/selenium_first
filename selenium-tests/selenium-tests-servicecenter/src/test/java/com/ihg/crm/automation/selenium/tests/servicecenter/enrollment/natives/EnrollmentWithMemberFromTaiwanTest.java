package com.ihg.crm.automation.selenium.tests.servicecenter.enrollment.natives;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.components.NativeLanguage.SIMPLIFIED_CHINESE;
import static com.ihg.automation.selenium.common.components.NativeLanguage.TRADITIONAL_CHINESE;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static org.hamcrest.core.StringContains.containsString;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.components.Country;
import com.ihg.automation.selenium.common.contact.Address;
import com.ihg.automation.selenium.common.member.GuestAddress;
import com.ihg.automation.selenium.common.member.GuestName;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.name.PersonNameFields;
import com.ihg.automation.selenium.common.types.address.AddressType;
import com.ihg.automation.selenium.common.types.address.RegionTaiwanNative;
import com.ihg.automation.selenium.common.types.address.RegionTaiwanRoman;
import com.ihg.automation.selenium.common.types.name.NameConfiguration;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.gwt.components.Mode;
import com.ihg.automation.selenium.servicecenter.pages.LeftPanel;
import com.ihg.automation.selenium.servicecenter.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.servicecenter.pages.personal.AddressContact;
import com.ihg.automation.selenium.servicecenter.pages.personal.AddressContactList;
import com.ihg.automation.selenium.servicecenter.pages.personal.CustomerInfoFields;
import com.ihg.automation.selenium.servicecenter.pages.personal.PersonalInfoPage;
import com.ihg.crm.automation.selenium.tests.servicecenter.LoginLogout;

public class EnrollmentWithMemberFromTaiwanTest extends LoginLogout
{
    private EnrollmentPage enrollmentPage = new EnrollmentPage();
    private GuestName name = new GuestName();
    private GuestName nativeName = new GuestName();
    private GuestName editedNativeName = new GuestName();
    private GuestAddress address = new GuestAddress();
    private GuestAddress nativeAddress = new GuestAddress();
    private GuestAddress editedNativeAddress = new GuestAddress();
    private PersonalInfoPage persInfo = new PersonalInfoPage();
    private CustomerInfoFields customerInfoFields;
    private PersonNameFields personNameFields;
    private Address addressEditFields;
    private AddressContactList addressContactList;
    private Member member = new Member();

    @BeforeClass
    public void before()
    {
        nativeName.setGiven("名字");
        nativeName.setSurname("姓");
        nativeName.setNativeLanguage(SIMPLIFIED_CHINESE);

        name.setGiven("ming zi");
        name.setSurname("xing");
        name.setNativeName(nativeName);

        nativeAddress.setLine1("东直门北中街4");
        nativeAddress.setLocality1("北京市");
        nativeAddress.setRegion1(RegionTaiwanNative.TPE);

        address.setCountryCode(Country.TW);
        address.setType(AddressType.RESIDENCE);
        address.setLocality1("bei jing shi");
        address.setLine1("dong zhi men bei zhong jie 4");
        address.setRegion1(RegionTaiwanRoman.TAI_BEI_SHI);

        address.setNativeAddress(nativeAddress);

        login();
    }

    @Test
    public void populateNameNativeFields()
    {
        enrollmentPage.goTo();
        enrollmentPage.getPrograms().selectProgram(Program.RC);
        enrollmentPage.getCustomerInfo().getResidenceCountry().selectByCode(Country.TW);
        PersonNameFields nameFields = enrollmentPage.getCustomerInfo().getName();
        nameFields.setConfiguration(Country.TW);
        nameFields.populateNativeFields(name);
        nameFields.verify(name, Mode.EDIT);
    }

    @Test(dependsOnMethods = { "populateNameNativeFields" })
    public void populateAddressNativeFields()
    {
        Address addressFields = enrollmentPage.getAddress();
        verifyThat(addressFields, hasText(containsString(TRADITIONAL_CHINESE)));
        addressFields.populateNativeFields(address);
        addressFields.verify(address, Mode.EDIT);

        addressFields.getClearNativeAddress().clickAndWait();
    }

    @Test(dependsOnMethods = { "populateAddressNativeFields" })
    public void completeEnrollment()
    {
        member = enrollmentPage.completeEnrollWithDuplicatePopUp(member);
    }

    @Test(dependsOnMethods = { "completeEnrollment" })
    public void verifyCustomerInfoDetailsInView()
    {
        persInfo.goTo();
        customerInfoFields = persInfo.getCustomerInfo();
        personNameFields = customerInfoFields.getName();
        verifyThat(personNameFields.getFullName(),
                hasText(name.getFullName(NameConfiguration.getEasternAsiaConfiguration())));
        verifyThat(personNameFields.getFullNativeName(),
                hasText(name.getNativeName().getFullName(NameConfiguration.getEasternAsiaConfiguration())));
        verifyThat(customerInfoFields.getNativeLanguage(), hasText(SIMPLIFIED_CHINESE));
        verifyThat(personNameFields.getTransliterate(), displayed(false));
    }

    @Test(dependsOnMethods = { "verifyCustomerInfoDetailsInView" })
    public void verifyCustomerInfoDetailsInEditMode()
    {
        customerInfoFields.clickEdit();
        personNameFields = customerInfoFields.getName();
        personNameFields.setConfiguration(Country.TW);
        verifyThat(personNameFields.getNativeGivenName(), displayed(true));
        verifyThat(personNameFields.getNativeMiddleName(), displayed(true));
        verifyThat(personNameFields.getNativeSurname(), displayed(true));
        verifyThat(personNameFields.getTransliterate(), displayed(true));
    }

    @Test(dependsOnMethods = { "verifyCustomerInfoDetailsInEditMode" })
    public void editCustomerInfoDetails()
    {
        editedNativeName.setGiven("大");
        editedNativeName.setSurname("罗斯");

        name.setGiven("da");
        name.setSurname("luo si");
        name.setNativeName(editedNativeName);

        customerInfoFields = persInfo.getCustomerInfo();
        personNameFields.setConfiguration(Country.TW);
        personNameFields.populateNativeFields(name);
        personNameFields.verify(name, Mode.EDIT);

        customerInfoFields.clickSave();
        verifyThat(personNameFields.getFullName(),
                hasText(name.getFullName(NameConfiguration.getEasternAsiaConfiguration())));

    }

    @Test(dependsOnMethods = { "editCustomerInfoDetails" })
    public void verifyAddressDetailsExpanded()
    {
        addressContactList = persInfo.getAddressList();
        addressContactList.getContact().expand();
        Address addressExpanded = addressContactList.getExpandedContact().getBaseContact();
        verifyThat(addressExpanded.getTransliterate(), displayed(false));
        verifyThat(addressExpanded.getClearNativeAddress(), displayed(false));
    }

    @Test(dependsOnMethods = { "verifyAddressDetailsExpanded" })
    public void verifyAddressDetailsInEditMode()
    {
        AddressContact contact = addressContactList.getContact();
        contact.clickEdit();
        contact.getBaseContact().verifyNativeFieldsBase(TRADITIONAL_CHINESE);
    }

    @Test(dependsOnMethods = { "verifyAddressDetailsInEditMode" })
    public void editAddressDetails()
    {
        editedNativeAddress.setLine1("中山区中山北路104");
        editedNativeAddress.setLocality1("台北");
        editedNativeAddress.setRegion1(RegionTaiwanNative.TAI_BEI);

        address.setCountryCode(Country.TW);
        address.setLocality1("tai bei");
        address.setRegion1(RegionTaiwanRoman.TAI_BEI);
        address.setLine1("zhong shan qu zhong shan bei lu 104");
        address.setNativeAddress(editedNativeAddress);

        addressEditFields = addressContactList.getContact().getBaseContact();
        addressEditFields.populateNativeFields(address);
        addressEditFields.verify(address, Mode.EDIT);

        addressContactList.getContact().clickSave();

        verifyThat(addressContactList.getContact(), hasText(containsString(address.getCollapseAddress())));
    }

    @Test(dependsOnMethods = { "editAddressDetails" })
    public void newSearch()
    {
        new LeftPanel().reOpenMemberProfile(member);

        verifyThat(personNameFields.getFullName(),
                hasText(name.getFullName(NameConfiguration.getEasternAsiaConfiguration())));
        verifyThat(addressContactList.getContact(), hasText(containsString(address.getCollapseAddress())));

    }
}
