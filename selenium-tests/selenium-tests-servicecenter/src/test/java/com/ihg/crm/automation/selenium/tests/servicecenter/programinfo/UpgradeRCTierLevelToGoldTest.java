package com.ihg.crm.automation.selenium.tests.servicecenter.programinfo;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.Constant.NOT_AVAILABLE;
import static com.ihg.automation.selenium.common.event.EventTransactionType.TierLevel.UPGRADE_MANUAL;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.isDisplayedWithWait;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.isHighlightedAsInvalid;
import static com.ihg.automation.selenium.matchers.component.HasSelectItems.hasSelectItems;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;

import java.util.ArrayList;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.common.DateUtils;
import com.ihg.automation.selenium.common.event.AllEventsRow;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.common.types.program.ProgramStatus;
import com.ihg.automation.selenium.common.types.program.RewardClubLevel;
import com.ihg.automation.selenium.common.types.program.RewardClubLevelReason;
import com.ihg.automation.selenium.common.types.program.TierLevelExpiration;
import com.ihg.automation.selenium.gwt.components.input.Select;
import com.ihg.automation.selenium.servicecenter.pages.LeftPanel;
import com.ihg.automation.selenium.servicecenter.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsGridRow;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsPage;
import com.ihg.automation.selenium.servicecenter.pages.event.level.ManualTierLevelChangeGridRowContainer;
import com.ihg.automation.selenium.servicecenter.pages.programs.ProgramSummary;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.RewardClubPage;
import com.ihg.crm.automation.selenium.tests.servicecenter.LoginLogout;

public class UpgradeRCTierLevelToGoldTest extends LoginLogout
{
    Member member = new Member();

    private RewardClubPage rewardClubPage = new RewardClubPage();
    private AllEventsPage allEventsPage = new AllEventsPage();
    private ProgramSummary summary;
    private AllEventsRow upgradeGoldEventRow;
    private ArrayList<String> rewardClubLevelReasons;

    @BeforeClass
    public void beforeClass()
    {
        member.setPersonalInfo(MemberPopulateHelper.getSimplePersonalInfo());
        member.addAddress(MemberPopulateHelper.getUnitedStatesAddress());
        member.addProgram(Program.RC);

        rewardClubLevelReasons = RewardClubLevel.getValuesList();

        login();

        member = new EnrollmentPage().enroll(member);
        upgradeGoldEventRow = new AllEventsRow(DateUtils.getFormattedDate(), UPGRADE_MANUAL, "RC - GOLD", "", "");
    }

    @Test
    public void navigateSummaryPage()
    {
        rewardClubPage.goTo();
        summary = rewardClubPage.getSummary();
        summary.clickEdit();
    }

    @Test(dependsOnMethods = { "navigateSummaryPage" })
    public void verifyTierLevelDDLContent()
    {
        verifyThat(summary.getTierLevel(), isDisplayedWithWait());
        verifyThat(summary.getTierLevel(), hasText(RewardClubLevel.CLUB.getCode()));

        verifyThat(summary.getTierLevel(), hasSelectItems(rewardClubLevelReasons));
    }

    @Test(dependsOnMethods = { "navigateSummaryPage", "verifyTierLevelDDLContent" }, alwaysRun = true)
    public void verifyTierLevelReasonContent()
    {
        summary.getTierLevel().select(RewardClubLevel.GOLD.getCode());

        Select tierLevelReason = summary.getTierLevelReason();
        verifyThat(tierLevelReason, isDisplayedWithWait());
        verifyThat(tierLevelReason, hasText("Select"));
        verifyThat(tierLevelReason, hasSelectItems(RewardClubLevelReason.getValuesList()));
    }

    @Test(dependsOnMethods = { "verifyTierLevelReasonContent" }, alwaysRun = true)
    public void tryToSaveWithNoReasonSet()
    {
        summary.getTierLevel().select(RewardClubLevel.GOLD.getCode());
        summary.clickSave();
        verifyThat(summary.getTierLevelReason(), isHighlightedAsInvalid(true));
    }

    @Test(dependsOnMethods = { "tryToSaveWithNoReasonSet" }, alwaysRun = true)
    public void setTierLevelToGOLD()
    {
        summary.populateTierLevel(RewardClubLevel.GOLD, RewardClubLevelReason.BASE,
                TierLevelExpiration.EXPIRE_NEXT_YEAR);
        summary.clickSave();
    }

    @Test(dependsOnMethods = { "setTierLevelToGOLD" }, alwaysRun = true)
    public void verifyResultingStatus()
    {
        rewardClubPage = new RewardClubPage();
        summary = rewardClubPage.getSummary();
        summary.verify(ProgramStatus.OPEN, RewardClubLevel.GOLD);
        verifyThat(new LeftPanel().getProgramInfoPanel().getPrograms().getRewardClubProgram().getLevelCode(),
                hasText(RewardClubLevel.GOLD.getCode()));
    }

    @Test(dependsOnMethods = { "verifyResultingStatus" }, alwaysRun = true)
    public void verifyAllEventsGridRow()
    {
        allEventsPage.goTo();
        AllEventsGridRow row = allEventsPage.getGrid().getRow(upgradeGoldEventRow.getTransType(),
                upgradeGoldEventRow.getDetail());
        row.verify(upgradeGoldEventRow);
    }

    @Test(dependsOnMethods = { "verifyAllEventsGridRow" }, alwaysRun = true)
    public void verifyAllEventsGridRowDetails()
    {
        allEventsPage.goTo();
        AllEventsGridRow row = allEventsPage.getGrid().getRow(upgradeGoldEventRow.getTransType(),
                upgradeGoldEventRow.getDetail());
        ManualTierLevelChangeGridRowContainer container = row.expand(ManualTierLevelChangeGridRowContainer.class);
        container.verify(NOT_AVAILABLE, RewardClubLevelReason.BASE.getValue(), NOT_AVAILABLE, helper);
    }

}
