package com.ihg.crm.automation.selenium.tests.servicecenter.tierlevelexpiration;

import static com.ihg.automation.selenium.common.member.MemberJdbcUtils.UPDATE_TL_EXP;
import static com.ihg.automation.selenium.common.types.program.Program.RC;
import static com.ihg.automation.selenium.common.types.program.ProgramStatus.OPEN;
import static com.ihg.automation.selenium.common.types.program.RewardClubLevel.CLUB;
import static com.ihg.automation.selenium.common.types.program.RewardClubLevel.GOLD;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.deposit.Deposit;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberJdbcUtils;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.stay.Stay;
import com.ihg.automation.selenium.servicecenter.pages.LeftPanel;
import com.ihg.automation.selenium.servicecenter.pages.deposit.CreateDepositPopUp;
import com.ihg.automation.selenium.servicecenter.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.servicecenter.pages.event.stay.StayEventsPage;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.RewardClubPage;
import com.ihg.automation.selenium.tests.common.RulesDBUtils;
import com.ihg.crm.automation.selenium.tests.servicecenter.LoginLogout;

public class ExpDateMoreThanRuleAndDepositHasTest extends LoginLogout
{
    private static final int MONTHS_FOR_FOUR_YEARS = 60;
    private static int nightsToAchieveGoldPreviousYear;
    private static int nightsToAchieveGoldCurrentYear;

    private RewardClubPage rewardClubPage;
    private Stay stayPrevious = new Stay();
    private Stay stayCurrent = new Stay();

    @BeforeClass
    public void beforeClass()
    {
        RulesDBUtils rulesDBUtils = new RulesDBUtils(jdbcTemplate);
        nightsToAchieveGoldPreviousYear = rulesDBUtils.getPreviousYearRuleNights(CLUB, GOLD);
        nightsToAchieveGoldCurrentYear = rulesDBUtils.getCurrentYearRuleNights(CLUB, GOLD);

        Member member = new Member();
        member.setPersonalInfo(MemberPopulateHelper.getSimplePersonalInfo());
        member.addAddress(MemberPopulateHelper.getUnitedStatesAddress());
        member.addProgram(RC);

        stayPrevious.setHotelCode("ATLCP");
        stayPrevious.setCheckInOutByNightsInPreviousYear(nightsToAchieveGoldPreviousYear);
        stayPrevious.setAvgRoomRate(100.00);
        stayPrevious.setRateCode("Test");

        stayCurrent = stayPrevious.clone();
        stayCurrent.setCheckInOutByNights(nightsToAchieveGoldCurrentYear);

        login();
        member = new EnrollmentPage().enroll(member);

        new StayEventsPage().createStay(stayPrevious);

        MemberJdbcUtils.update(jdbcTemplateUpdate, UPDATE_TL_EXP, member, RC, MONTHS_FOR_FOUR_YEARS);

        new LeftPanel().reOpenMemberProfile(member);

        rewardClubPage = new RewardClubPage();
        rewardClubPage.goTo();
        rewardClubPage.getSummary().verify(OPEN, GOLD, 4);
    }

    @Test
    public void createDepositAndVerifyTierLevelNotChanged()
    {
        Deposit testDeposit = new Deposit();
        testDeposit.setTransactionId("CBCGU");
        testDeposit.setHotel("ATLCP");

        new CreateDepositPopUp().createDeposit(testDeposit, false);
        rewardClubPage.goTo();
        rewardClubPage.getSummary().verify(OPEN, GOLD, 4);
    }

    @Test(dependsOnMethods = { "createDepositAndVerifyTierLevelNotChanged" }, alwaysRun = true)
    public void maintainSpireAndVerifyTierLevelNotChanged()
    {
        new StayEventsPage().createStay(stayCurrent);

        rewardClubPage.goTo();
        rewardClubPage.getSummary().verify(OPEN, GOLD, 4);
    }
}
