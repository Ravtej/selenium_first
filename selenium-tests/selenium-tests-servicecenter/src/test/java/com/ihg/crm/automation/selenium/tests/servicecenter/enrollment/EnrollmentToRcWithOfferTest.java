package com.ihg.crm.automation.selenium.tests.servicecenter.enrollment;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.Constant.NOT_AVAILABLE;
import static com.ihg.automation.selenium.common.event.EventTransactionType.Offer.OFFER_REGISTRATION;
import static com.ihg.automation.selenium.common.types.program.Program.RC;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasDefault;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;

import java.util.Map;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.common.DateUtils;
import com.ihg.automation.selenium.common.earning.EventEarningDetailsTab;
import com.ihg.automation.selenium.common.event.AllEventsRowFactory;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.offer.GuestOffer;
import com.ihg.automation.selenium.common.offer.Offer;
import com.ihg.automation.selenium.servicecenter.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsGrid;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsGridRow;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsPage;
import com.ihg.automation.selenium.servicecenter.pages.event.all.OfferGridRowContainer;
import com.ihg.automation.selenium.servicecenter.pages.event.enroll.MembershipDetailsPopUp;
import com.ihg.automation.selenium.servicecenter.pages.event.enroll.MembershipGridRowContainer;
import com.ihg.automation.selenium.servicecenter.pages.event.offer.MemberOfferGridRow;
import com.ihg.automation.selenium.servicecenter.pages.event.offer.OfferDetailsTabExtended;
import com.ihg.automation.selenium.servicecenter.pages.event.offer.OfferEventPage;
import com.ihg.automation.selenium.servicecenter.pages.event.offer.OfferPopUp;
import com.ihg.automation.selenium.servicecenter.pages.programs.ProgramPageBase;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.RewardClubPage;
import com.ihg.crm.automation.selenium.tests.servicecenter.LoginLogout;

public class EnrollmentToRcWithOfferTest extends LoginLogout
{
    private final static String OFFER_RULE = "SELECT * FROM RULE.RULE r"//
            + " JOIN RULE.RULE_ATTR ra ON r.RULE_KEY = ra.RULE_KEY"//
            + " JOIN OFFER.LYTY_OFFER lo ON r.LYTY_OFFER_KEY= lo.LYTY_OFFER_KEY"//
            + " WHERE ra.RULE_ATTR_TYP_CD = 'Enrollment.Offer.TreatmentId'"//
            + " AND ra.RULE_ATTR_VAL IN (SELECT OFFER_TRMT_ID FROM LYTYREFDATA.OFFER_TRMT_TYP)"//
            + " AND r.RULE_TYP_CD = 'ENROLLMENT'"//
            + " AND r.LST_EFF_TS > SYSDATE"//
            + " AND lo.LST_EFF_TS > SYSDATE"//
            + " AND ROWNUM <2";
    private Member member = new Member();
    private AllEventsPage allEventsPage;
    private Offer offer;

    @BeforeClass
    public void before()
    {
        Map<String, Object> map = jdbcTemplate.queryForMap(OFFER_RULE);

        offer = Offer.builder(map.get("OFFER_CD").toString(), map.get("LYTY_OFFER_NM").toString())
                .description(map.get("LYTY_OFFER_DESC").toString()).build();

        member.addProgram(RC);
        member.setPersonalInfo(MemberPopulateHelper.getSimplePersonalInfo());
        member.addAddress(MemberPopulateHelper.getUnitedStatesAddress());
        member.setRewardClubOfferCode(map.get("RULE_ATTR_VAL").toString());

        login();
    }

    @Test
    public void enrollment()
    {
        new EnrollmentPage().enroll(member);
    }

    @Test(dependsOnMethods = { "enrollment" }, alwaysRun = true)
    public void verifyRewardClubPage()
    {
        RewardClubPage rewardClubPage = new RewardClubPage();
        rewardClubPage.goTo();

        ProgramPageBase.EnrollmentDetails enrlDetails = rewardClubPage.getEnrollmentDetails();
        verifyThat(enrlDetails.getOfferCode(), hasText(member.getRewardClubOfferCode()));
        verifyThat(enrlDetails.getPaymentDetails().getMethod(), hasDefault());
        verifyThat(enrlDetails.getReferringMember(), hasText(NOT_AVAILABLE));
        verifyThat(enrlDetails.getRenewalDate(), hasDefault());
        verifyThat(enrlDetails.getEnrollDate(), hasText(DateUtils.getFormattedDate()));

        enrlDetails.verifyScSource(helper);
    }

    @Test(dependsOnMethods = { "verifyRewardClubPage" }, alwaysRun = true)
    public void verifyRegisteredOffer()
    {
        OfferEventPage offerEventPage = new OfferEventPage();
        offerEventPage.goTo();
        MemberOfferGridRow rowByOfferCode = offerEventPage.getMemberOffersGrid().getRow(1);
        rowByOfferCode.verify(new GuestOffer(offer));

        OfferPopUp offerPopUp = rowByOfferCode.getOfferDetails();
        OfferDetailsTabExtended offerDetailsTab = offerPopUp.getOfferDetailsTab();
        offerDetailsTab.goTo();
        offerDetailsTab.getOfferDetails().verify(offer);

        offerPopUp.close();
    }

    @Test(dependsOnMethods = { "verifyRegisteredOffer" }, alwaysRun = true)
    public void verifyOfferOnAllEventsPage()
    {
        allEventsPage = new AllEventsPage();
        allEventsPage.goTo();
        AllEventsGrid grid = allEventsPage.getGrid();
        AllEventsGridRow row = grid.getRow(OFFER_REGISTRATION);
        row.verify(AllEventsRowFactory.getOfferRegistrationEvent(offer));

        row.expand(OfferGridRowContainer.class).getOfferDetails().verify(offer, helper);
    }

    @Test(dependsOnMethods = { "verifyOfferOnAllEventsPage" }, alwaysRun = true)
    public void verifyOfferInEnrollEarningDetails()
    {
        allEventsPage.getGrid().getEnrollRow(RC).expand(MembershipGridRowContainer.class).getDetails().clickAndWait();

        EventEarningDetailsTab earningDetailsTab = new MembershipDetailsPopUp().getEarningDetailsTab();
        earningDetailsTab.goTo();
        earningDetailsTab.verifyBasicDetailsNoPointsAndMiles();

        verifyThat(earningDetailsTab.getGrid().getRowByType(offer.getCode()), displayed(true));
    }
}
