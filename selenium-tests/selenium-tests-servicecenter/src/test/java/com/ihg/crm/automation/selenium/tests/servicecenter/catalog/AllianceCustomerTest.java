package com.ihg.crm.automation.selenium.tests.servicecenter.catalog;

import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyError;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.event.EventTransactionType.Order.REDEEM;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasTextAsInt;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static org.hamcrest.core.StringContains.containsString;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.common.DateUtils;
import com.ihg.automation.common.RandomUtils;
import com.ihg.automation.selenium.common.ProgramInfoPanel;
import com.ihg.automation.selenium.common.catalog.CatalogItem;
import com.ihg.automation.selenium.common.event.AllEventsRowConverter;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.order.Order;
import com.ihg.automation.selenium.common.order.OrderItem;
import com.ihg.automation.selenium.common.types.Alliance;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.servicecenter.pages.catalog.CatalogItemDetailsPopUp;
import com.ihg.automation.selenium.servicecenter.pages.catalog.CatalogPage;
import com.ihg.automation.selenium.servicecenter.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.servicecenter.pages.event.order.OrderEventsPage;
import com.ihg.automation.selenium.servicecenter.pages.event.order.OrderGridRow;
import com.ihg.automation.selenium.servicecenter.pages.event.order.OrderGridRow.OrderCell;
import com.ihg.automation.selenium.servicecenter.pages.order.OrderSummaryPopUp;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.AllianceAddPopUp;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.AllianceDetailsAdd;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.RewardClubPage;
import com.ihg.automation.selenium.servicecenter.pages.unit.GoodwillPopUp;

public class AllianceCustomerTest extends CatalogCommon
{
    private Member member = new Member();
    private CatalogItemDetailsPopUp catalogItemDetailsPopUp = new CatalogItemDetailsPopUp();
    private CatalogPage catalogPage = new CatalogPage();
    private final static CatalogItem CATALOG_ITEM = CatalogItem.builder("YAJAL", "Japan Airlines - 2,000 miles")
            .loyaltyUnitCost("10000").build();
    private RewardClubPage rewardClubPage = new RewardClubPage();
    private OrderEventsPage orderPage = new OrderEventsPage();
    private Alliance alliance = Alliance.JAL;
    private String allianceNumber = RandomUtils.getRandomNumber(9);
    private Order order = new Order();

    @BeforeClass
    public void beforeClass()
    {
        OrderItem orderItem = new OrderItem(CATALOG_ITEM.getItemId(), CATALOG_ITEM.getItemName());
        orderItem.setQuantity("1");
        orderItem.setLoyaltyUnits(CATALOG_ITEM.getLoyaltyUnitCost());
        orderItem.setItemName(CATALOG_ITEM.getItemName());

        order.getOrderItems().add(orderItem);
        order.setTotalLoyaltyUnits(orderItem.getLoyaltyUnits());
        order.setTransactionType(REDEEM);
        order.setTransactionDate(DateUtils.getFormattedDate());
        order.setTotalAllianceUnits("2000");

        member.addProgram(Program.RC);
        member.setPersonalInfo(MemberPopulateHelper.getSimplePersonalInfo());
        member.addAddress(MemberPopulateHelper.getUnitedStatesAddress());

        login();

        new EnrollmentPage().enroll(member);

        new GoodwillPopUp().goodwillPoints("10000");
    }

    @Test
    public void withoutAlliance()
    {
        catalogPage.goTo();

        catalogPage.searchByItemId(CATALOG_ITEM);
        catalogItemDetailsPopUp = catalogPage.openCatalogItemDetailsPopUp();
        catalogItemDetailsPopUp.clickAddToCart(verifyError(
                hasText(containsString("Customer not Eligible for Item. JAL not available on Membership."))));

        catalogItemDetailsPopUp.clickClose();
        verifyThat(catalogItemDetailsPopUp, displayed(false));

        catalogPage.clickClose();
        verifyThat(catalogPage, displayed(false));
    }

    @Test(dependsOnMethods = "withoutAlliance", alwaysRun = true)
    public void withAlliance()
    {
        rewardClubPage.goTo();

        rewardClubPage.clickAddAlliance();
        AllianceAddPopUp allianceAddPopUp = new AllianceAddPopUp();
        AllianceDetailsAdd allianceDetails = allianceAddPopUp.getAllianceDetails();
        allianceDetails.getAlliance().selectByCode(alliance);
        allianceDetails.getAllianceNumber().type(allianceNumber);
        allianceAddPopUp.clickSave();

        catalogPage.goTo();
        catalogPage.searchByItemId(CATALOG_ITEM);
        catalogItemDetailsPopUp = catalogPage.openCatalogItemDetailsPopUp();
        OrderSummaryPopUp orderSummaryPopUp = catalogItemDetailsPopUp.openOrderSummaryPopUp();
        orderSummaryPopUp.clickCheckout();

        verifyThat(new ProgramInfoPanel().getPrograms().getRewardClubProgram().getPointsBalance(), hasTextAsInt(0));

        verifyThat(orderPage, displayed(true));

        OrderGridRow itemRow = orderPage.getOrdersGrid().getRow(1);

        itemRow.verify(order);

        itemRow.verifyRowGreenColor();

        order.setOrderNumber(itemRow.getCell(OrderCell.ORDER_NUMBER).getText());
    }

    @Test(dependsOnMethods = { "withAlliance" })
    public void verifyRedeemEventOnAllEvents()
    {
        verifyRedeemEventOnAllEventsPage(AllEventsRowConverter.getRedeemEvent(order));
    }
}
