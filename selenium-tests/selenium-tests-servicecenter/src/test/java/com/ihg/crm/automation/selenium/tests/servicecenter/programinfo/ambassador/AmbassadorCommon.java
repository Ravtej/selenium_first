package com.ihg.crm.automation.selenium.tests.servicecenter.programinfo.ambassador;

import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyNoError;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.Constant.SC_LOCATION;
import static com.ihg.automation.selenium.common.event.EarningEventFactory.getAwardEvent;
import static com.ihg.automation.selenium.common.event.EventTransactionType.Membership.MEMBERSHIP_RENEWAL;
import static com.ihg.automation.selenium.common.event.EventTransactionType.Order.ORDER_SYSTEM;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.isDisplayedWithWait;
import static com.ihg.automation.selenium.matchers.component.GridSize.size;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;

import com.ihg.automation.selenium.common.catalog.CatalogItem;
import com.ihg.automation.selenium.common.earning.EventEarningDetailsTab;
import com.ihg.automation.selenium.common.earning.EventEarningGrid;
import com.ihg.automation.selenium.common.enroll.SuccessEnrolmentPopUp;
import com.ihg.automation.selenium.common.event.AllEventsRowConverter;
import com.ihg.automation.selenium.common.member.AmbassadorOfferCode;
import com.ihg.automation.selenium.common.member.AmbassadorUtils;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.order.Order;
import com.ihg.automation.selenium.common.order.OrderFactory;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.servicecenter.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsGrid;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsGridRow;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsPage;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsSearch;
import com.ihg.automation.selenium.servicecenter.pages.event.enroll.MembershipDetailsPopUp;
import com.ihg.automation.selenium.servicecenter.pages.event.order.OrderSystemDetailsGridRowContainer;
import com.ihg.automation.selenium.servicecenter.pages.personal.PersonalInfoPage;
import com.ihg.crm.automation.selenium.tests.servicecenter.LoginLogout;

public class AmbassadorCommon extends LoginLogout
{
    private MembershipDetailsPopUp membershipDetailsPopUp;

    private EventEarningDetailsTab earningDetailsTab;
    private AllEventsPage allEventsPage = new AllEventsPage();
    private PersonalInfoPage persInfo = new PersonalInfoPage();

    protected Member enrollAMBmember()
    {
        Member member = new Member();
        member.setPersonalInfo(MemberPopulateHelper.getSimplePersonalInfo());
        member.addAddress(MemberPopulateHelper.getUnitedStatesAddress());
        member.addProgram(Program.AMB);
        member.setAmbassadorOfferCode(AmbassadorOfferCode.CHSRO);
        member.setAmbassadorAmount(AmbassadorUtils.AMBASSADOR_COMP_PROMO_$0);

        member = new EnrollmentPage().enroll(member);
        return member;
    }

    protected void validateBillingDetails(CatalogItem award)
    {
        membershipDetailsPopUp = new MembershipDetailsPopUp();
        membershipDetailsPopUp.getBillingDetailsTab().verifyBaseAwardUSDBilling(MEMBERSHIP_RENEWAL, award, SC_LOCATION);
        membershipDetailsPopUp.close();
    }

    protected void validateSuccessEnrollmentPopUp(Program program, Member member)
    {
        SuccessEnrolmentPopUp popUp = new SuccessEnrolmentPopUp();
        verifyThat(popUp, isDisplayedWithWait());
        verifyThat(popUp.getMemberId(program), hasText(member.getAMBProgramId()));
        popUp.clickDone(verifyNoError());
        verifyThat(persInfo, isDisplayedWithWait());
    }

    protected void verifyOrderSystemDetails(Order order)
    {
        AllEventsGrid allEventsGrid = allEventsPage.getGrid();
        AllEventsGridRow orderSystemRow = allEventsGrid.getRow(ORDER_SYSTEM);
        verifyThat(orderSystemRow, displayed(true));
        OrderSystemDetailsGridRowContainer orderSystemContainer = orderSystemRow
                .expand(OrderSystemDetailsGridRowContainer.class);
        orderSystemContainer.verify(order);
    }

    protected void verifyOrderSystemDetailsAfterReactivate(Member member)
    {
        AllEventsSearch searchFields = allEventsPage.getSearchFields();
        searchFields.getEventType().select("Order");
        searchFields.getTransactionType().select(ORDER_SYSTEM);
        allEventsPage.getButtonBar().getSearch().clickAndWait();

        AllEventsGrid grid = allEventsPage.getGrid();
        verifyThat(grid, size(2));

        AllEventsGridRow row = grid.getRow(1);
        Order order = OrderFactory.getSystemOrder(AmbassadorUtils.NEW_GOLD_AMBASSADOR_KIT, member, helper);

        row.verify(AllEventsRowConverter.convert(order));
        row.expand(OrderSystemDetailsGridRowContainer.class).verify(order);
    }

    protected void verifyEarningDetailsTab(CatalogItem renewAward, CatalogItem fulfillmentAward)
    {
        membershipDetailsPopUp = new MembershipDetailsPopUp();
        earningDetailsTab = membershipDetailsPopUp.getEarningDetailsTab();
        earningDetailsTab.goTo();
        earningDetailsTab.verifyBasicDetailsNoPointsAndMiles();

        EventEarningGrid eventEarningGrid = earningDetailsTab.getGrid();
        eventEarningGrid.getRowByAward(renewAward.getItemId()).verify(getAwardEvent(renewAward));
        eventEarningGrid.getRowByAward(fulfillmentAward.getItemId()).verify(getAwardEvent(fulfillmentAward));
    }

}
