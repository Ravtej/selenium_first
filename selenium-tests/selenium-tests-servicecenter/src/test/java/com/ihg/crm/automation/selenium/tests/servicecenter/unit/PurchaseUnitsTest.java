package com.ihg.crm.automation.selenium.tests.servicecenter.unit;

import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.assertNoError;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.assertNoErrors;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.event.EventTransactionType.Loyalty.UNIT_PURCHASE;
import static com.ihg.automation.selenium.matchers.component.HasSelectItems.hasSelectItems;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.ProgramInfoPanel;
import com.ihg.automation.selenium.common.event.AllEventsRow;
import com.ihg.automation.selenium.common.event.AllEventsRowFactory;
import com.ihg.automation.selenium.common.member.LifetimeActivity;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.pages.CreditCardType;
import com.ihg.automation.selenium.common.pages.PaymentMethod;
import com.ihg.automation.selenium.common.payment.CreditCardPayment;
import com.ihg.automation.selenium.common.payment.CreditCardPaymentContainer;
import com.ihg.automation.selenium.common.payment.PaymentDetailsPopUp;
import com.ihg.automation.selenium.common.types.Currency;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.common.types.program.RewardClubLevel;
import com.ihg.automation.selenium.gwt.components.Mode;
import com.ihg.automation.selenium.servicecenter.pages.annual.AnnualActivityPage;
import com.ihg.automation.selenium.servicecenter.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsGridRow;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsPage;
import com.ihg.automation.selenium.servicecenter.pages.event.unit.UnitDetails;
import com.ihg.automation.selenium.servicecenter.pages.event.unit.UnitDetailsPopUp;
import com.ihg.automation.selenium.servicecenter.pages.event.unit.UnitGridRowContainer;
import com.ihg.automation.selenium.servicecenter.pages.unit.PurchaseUnitsPopUp;
import com.ihg.crm.automation.selenium.tests.servicecenter.LoginLogout;

public class PurchaseUnitsTest extends LoginLogout
{
    private Member member = new Member();

    private static final int PURCHASE_AMOUNT = 21000;
    private static final String PURCHASE_AMOUNT_STR = "21,000";
    private static final String USD_AMOUNT = "262.5";

    private CreditCardPayment creditCard = new CreditCardPayment(USD_AMOUNT, Currency.USD.getCode(), "4966648225694538",
            CreditCardType.VISA);

    private UnitGridRowContainer rowContainer;

    @BeforeClass
    public void before()
    {
        member.addProgram(Program.RC);
        member.setPersonalInfo(MemberPopulateHelper.getSimplePersonalInfo());
        member.addAddress(MemberPopulateHelper.getUnitedStatesAddress());

        login();

        member = new EnrollmentPage().enroll(member);
        member = new AnnualActivityPage().captureCounters(member, jdbcTemplate);

        new ProgramInfoPanel().getPrograms().getRewardClubProgram().verify(RewardClubLevel.CLUB, "0");
    }

    @Test
    public void purchaseLoyaltyUnits()
    {
        PurchaseUnitsPopUp purchase = new PurchaseUnitsPopUp();

        purchase.goTo();
        assertNoErrors();
        purchase.getAmount().select(PURCHASE_AMOUNT);
        purchase.clickSubmitNoError();

        PaymentDetailsPopUp payment = new PaymentDetailsPopUp();
        payment.selectPaymentType(PaymentMethod.CREDIT_CARD);
        CreditCardPaymentContainer creditCardPayment = payment.getCreditCardPayment();
        verifyThat(creditCardPayment.getType(), hasSelectItems(CreditCardType.getDataList()));

        creditCardPayment.populate(creditCard);

        payment.clickSubmitAndVerifySuccessMsg();
    }

    @Test(dependsOnMethods = { "purchaseLoyaltyUnits" })
    public void verifyCountersAndLevel()
    {
        AnnualActivityPage annualActivityPage = new AnnualActivityPage();
        annualActivityPage.goTo();
        assertNoErrors();

        LifetimeActivity lifetimeActivity = member.getLifetimeActivity();
        lifetimeActivity.setBonusUnits(PURCHASE_AMOUNT);
        lifetimeActivity.setTotalUnits(PURCHASE_AMOUNT);
        lifetimeActivity.setCurrentBalance(PURCHASE_AMOUNT);

        member.getAnnualActivity().setTotalUnits(PURCHASE_AMOUNT);

        annualActivityPage.verifyCounters(member);

        // Program Info
        new ProgramInfoPanel().getPrograms().getRewardClubProgram().verify(RewardClubLevel.CLUB, PURCHASE_AMOUNT_STR);
    }

    @Test(dependsOnMethods = { "purchaseLoyaltyUnits" })
    public void verifyEventDetailsInExpander()
    {
        AllEventsPage allEvents = new AllEventsPage();

        allEvents.goTo();
        assertNoErrors();

        AllEventsGridRow row = allEvents.getGrid().getRow(1);

        AllEventsRow unitPurchaseRow = AllEventsRowFactory.getPointEvent(UNIT_PURCHASE, PURCHASE_AMOUNT);
        row.verify(unitPurchaseRow);

        rowContainer = row.expand(UnitGridRowContainer.class);

        UnitDetails unitDetails = rowContainer.getUnitDetails();
        unitDetails.getPaymentDetails().getCreditCardPayment().verify(creditCard, Mode.VIEW);
        unitDetails.verifySource(helper);
    }

    @Test(dependsOnMethods = { "purchaseLoyaltyUnits" })
    public void verifyEventDetailsPopUp()
    {
        rowContainer.clickDetails(assertNoError());

        UnitDetailsPopUp popUp = new UnitDetailsPopUp();

        // Earning Details Tab
        popUp.getEarningDetailsTab().verifyForBaseUnitsEvent(PURCHASE_AMOUNT, 0);

        // Billing Details Tab
        popUp.getBillingDetailsTab().verifyBaseUSDBilling(UNIT_PURCHASE, USD_AMOUNT, "CPMPP");
    }
}
