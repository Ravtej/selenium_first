package com.ihg.crm.automation.selenium.tests.servicecenter.unit;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.Constant.RC_POINTS;
import static com.ihg.automation.selenium.common.event.EventTransactionType.Loyalty.GIFT;
import static com.ihg.automation.selenium.common.event.EventTransactionType.Loyalty.GIFT_PURCHASE;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasDefault;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasTextWithWait;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.isDisplayedWithWait;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.isHighlightedAsInvalid;
import static com.ihg.automation.selenium.matchers.component.HasSelectItems.hasSelectItems;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static com.ihg.automation.selenium.matchers.component.ViewableMatcher.hasTextInView;
import static org.apache.commons.lang3.StringUtils.EMPTY;

import java.util.Arrays;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.billing.EventBillingDetailsTab;
import com.ihg.automation.selenium.common.event.AllEventsRow;
import com.ihg.automation.selenium.common.event.AllEventsRowFactory;
import com.ihg.automation.selenium.common.member.LifetimeActivity;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.pages.CreditCardType;
import com.ihg.automation.selenium.common.pages.PaymentMethod;
import com.ihg.automation.selenium.common.payment.CreditCardPayment;
import com.ihg.automation.selenium.common.payment.CreditCardPaymentContainer;
import com.ihg.automation.selenium.common.payment.PaymentDetailsPopUp;
import com.ihg.automation.selenium.common.types.Currency;
import com.ihg.automation.selenium.common.types.program.ProgramStatus;
import com.ihg.automation.selenium.common.types.program.RewardClubLevel;
import com.ihg.automation.selenium.gwt.components.Mode;
import com.ihg.automation.selenium.gwt.components.input.Input;
import com.ihg.automation.selenium.gwt.components.input.Select;
import com.ihg.automation.selenium.servicecenter.pages.LeftPanel;
import com.ihg.automation.selenium.servicecenter.pages.annual.AnnualActivityPage;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsGridRow;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsPage;
import com.ihg.automation.selenium.servicecenter.pages.event.unit.UnitDetails;
import com.ihg.automation.selenium.servicecenter.pages.event.unit.UnitDetailsPopUp;
import com.ihg.automation.selenium.servicecenter.pages.event.unit.UnitGridRowContainer;
import com.ihg.automation.selenium.servicecenter.pages.personal.PersonalInfoPage;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.RewardClubPage;
import com.ihg.automation.selenium.servicecenter.pages.unit.GiftUnitsPopUp;

public class PurchaseGiftTest extends UnitsCommon
{
    private Member member1, member2;

    private PaymentDetailsPopUp paymDetailsPopUp = new PaymentDetailsPopUp();

    private GiftUnitsPopUp giftPopUp = new GiftUnitsPopUp();
    private static final String POINTS_AMOUNT_STRING = "25000";
    private static final int POINTS_AMOUNT_INT = Integer.parseInt(POINTS_AMOUNT_STRING);
    private static final String EXPECTED_AMOUNT = "312.50";
    private static final String EXPECTED_CURRENCY = Currency.USD.getCode();
    private static final String BILLING_TO = "CPMGP";
    private CreditCardPayment creditCard = new CreditCardPayment(EXPECTED_AMOUNT, EXPECTED_CURRENCY, "4966648225694538",
            CreditCardType.VISA);

    AllEventsRow member2AllEventsRow = AllEventsRowFactory.getPointEvent(GIFT_PURCHASE, EMPTY);
    AllEventsRow member1AllEventsRow = AllEventsRowFactory.getPointEvent(GIFT, POINTS_AMOUNT_STRING);

    @BeforeClass()
    public void beforeClass()
    {
        login();

        member1 = new AnnualActivityPage().captureCounters(enrollMember(), jdbcTemplate);

        new LeftPanel().goToNewSearch();

        member2 = new AnnualActivityPage().captureCounters(enrollMember(), jdbcTemplate);
    }

    @Test
    public void verifyPurchaseGiftPopUpControls()
    {
        giftPopUp.goTo();
        verifyThat(giftPopUp.getMemberName(), hasDefault());
        verifyThat(giftPopUp.getMembershipId(), hasDefault());

        Select amount = giftPopUp.getAmount();
        verifyThat(amount, hasText("Select Amount"));
        verifyThat(amount, hasSelectItems(getExpectedAmounts(1000, 50000, 1000)));

        Select unitType = giftPopUp.getUnitType();
        verifyThat(unitType, hasText(RC_POINTS));
        verifyThat(unitType, hasSelectItems(Arrays.asList(RC_POINTS)));
        // TODO verify mandatory
    }

    @Test(dependsOnMethods = { "verifyPurchaseGiftPopUpControls" }, alwaysRun = true)
    public void submitGiftPopUp()
    {
        giftPopUp.getMembershipId().typeAndSubmit(member1.getRCProgramId());
        giftPopUp.getAmount().select(POINTS_AMOUNT_STRING);
        verifyThat(giftPopUp.getMemberName(), hasTextWithWait(member1.getName().getNameOnPanel()));
        giftPopUp.clickSubmitNoError();
    }

    @Test(dependsOnMethods = { "submitGiftPopUp" })
    public void verifyPaymentDetailsPopUp()
    {
        verifyThat(paymDetailsPopUp, isDisplayedWithWait());
        verifyThat(paymDetailsPopUp.getMembershipId(), hasText(member1.getRCProgramId()));
        verifyThat(paymDetailsPopUp.getTotalAmount(), hasTextInView(EXPECTED_AMOUNT + " " + EXPECTED_CURRENCY));
        verifyThat(paymDetailsPopUp.getMemberName(), hasText(member1.getName().getNameOnPanel()));
    }

    @Test(dependsOnMethods = { "verifyPaymentDetailsPopUp" }, alwaysRun = true)
    public void submitPayment()
    {
        paymDetailsPopUp.selectPaymentType(PaymentMethod.CREDIT_CARD);
        CreditCardPaymentContainer creditCardPayment = paymDetailsPopUp.getCreditCardPayment();

        verifyThat(creditCardPayment.getType(), hasSelectItems(CreditCardType.getDataList()));

        creditCardPayment.populate(creditCard);

        paymDetailsPopUp.clickSubmitAndVerifySuccessMsg();
    }

    @Test(dependsOnMethods = { "submitPayment" })
    public void verifyGiftPurchaseEvent()
    {
        AllEventsPage allEvents = new AllEventsPage();
        verifyThat(allEvents, displayed(true));

        AllEventsGridRow row = allEvents.getGrid().getRow(member2AllEventsRow.getTransType());
        row.verify(member2AllEventsRow);

        UnitDetails details = row.expand(UnitGridRowContainer.class).getUnitDetails();

        details.verifyRelatedMemberID(member1);
        details.getPaymentDetails().verify(creditCard, Mode.VIEW);
        details.verifySource(helper);
    }

    @Test(dependsOnMethods = { "verifyGiftPurchaseEvent" }, alwaysRun = true)
    public void navigateMember2DetailsPopUp()
    {
        openDetailsPopUp(member2AllEventsRow.getTransType());
    }

    @Test(dependsOnMethods = { "navigateMember2DetailsPopUp" })
    public void verifyMember2LoyaltyUnitsDetailsTab()
    {
        UnitDetailsPopUp unitPopUp = new UnitDetailsPopUp();
        unitPopUp.getUnitDetailsTab().goTo();
        UnitDetails details = unitPopUp.getUnitDetailsTab().getUnitDetails();

        details.verifyRelatedMemberID(member1);
        details.getPaymentDetails().verify(creditCard, Mode.VIEW);
        details.verifySource(helper);
    }

    @Test(dependsOnMethods = { "verifyMember2LoyaltyUnitsDetailsTab" }, alwaysRun = true)
    public void verifyMember2EarningDetailsTab()
    {
        new UnitDetailsPopUp().getEarningDetailsTab().verifyForEmptyBaseUnitsEvent();
    }

    @Test(dependsOnMethods = { "verifyMember2EarningDetailsTab" }, alwaysRun = true)
    public void verifyMember2BillingDetailsTab()
    {
        EventBillingDetailsTab billingDetailsTab = new UnitDetailsPopUp().getBillingDetailsTab();
        billingDetailsTab.goTo();
        billingDetailsTab.verifyTabIsEmpty(GIFT_PURCHASE);
    }

    @Test(dependsOnMethods = { "verifyMember2BillingDetailsTab" }, alwaysRun = true)
    public void closeMember2UnitDetailsPopUp()
    {
        new UnitDetailsPopUp().close();
    }

    @Test(dependsOnMethods = { "closeMember2UnitDetailsPopUp" })
    public void verifyMember2TierLevel()
    {
        RewardClubPage rwrds = new RewardClubPage();
        rwrds.goTo();
        rwrds.getSummary().verify(ProgramStatus.OPEN, RewardClubLevel.CLUB);
    }

    @Test(dependsOnMethods = { "verifyMember2TierLevel" }, alwaysRun = true)
    public void verifyMember2AnnualActivityCounters()
    {
        new AnnualActivityPage().verifyCounters(member2);
    }

    @Test(dependsOnMethods = { "verifyMember2AnnualActivityCounters" }, alwaysRun = true)
    public void openMemberProfileLinkFromGiftPurchase()
    {
        openRecipientProfileByLink(member1, 1);
    }

    @Test(dependsOnMethods = { "openMemberProfileLinkFromGiftPurchase" })
    public void verifyTransitionToMember()
    {
        new PersonalInfoPage().getCustomerInfo().verify(member1, Mode.VIEW);
    }

    @Test(dependsOnMethods = { "verifyTransitionToMember" }, alwaysRun = true)
    public void verifyMember1GiftEvent()
    {
        AllEventsPage allEvents = new AllEventsPage();
        allEvents.goTo();

        AllEventsGridRow row = allEvents.getGrid().getRow(member1AllEventsRow.getTransType());
        row.verify(member1AllEventsRow);

        UnitDetails details = row.expand(UnitGridRowContainer.class).getUnitDetails();
        details.verifyRelatedMemberID(member2);
        details.verifySource(helper);
    }

    @Test(dependsOnMethods = { "verifyMember1GiftEvent" }, alwaysRun = true)
    public void navigateMember1DetailsPopUp()
    {
        openDetailsPopUp(member1AllEventsRow.getTransType());
    }

    @Test(dependsOnMethods = { "navigateMember1DetailsPopUp" })
    public void verifyMember1LoyaltyUnitsDetailsTab()
    {
        UnitDetailsPopUp unitPopUp = new UnitDetailsPopUp();
        unitPopUp.getUnitDetailsTab().goTo();

        UnitDetails details = unitPopUp.getUnitDetailsTab().getUnitDetails();
        details.verifyRelatedMemberID(member2);
        details.verifySource(helper);
    }

    @Test(dependsOnMethods = { "verifyMember1LoyaltyUnitsDetailsTab" }, alwaysRun = true)
    public void verifyMember1EarningDetailsTab()
    {
        new UnitDetailsPopUp().getEarningDetailsTab().verifyForBaseUnitsEvent(POINTS_AMOUNT_STRING, "0");
    }

    @Test(dependsOnMethods = { "verifyMember1EarningDetailsTab" }, alwaysRun = true)
    public void verifyMember1BillingDetailsTab()
    {
        new UnitDetailsPopUp().getBillingDetailsTab().verifyBaseUSDBilling(GIFT, "312.5", BILLING_TO);
    }

    @Test(dependsOnMethods = { "verifyMember1BillingDetailsTab" }, alwaysRun = true)
    public void closeMember1UnitDetailsPopUp()
    {
        new UnitDetailsPopUp().close();
    }

    @Test(dependsOnMethods = { "closeMember1UnitDetailsPopUp" })
    public void verifyMember1TierLevel()
    {
        RewardClubPage rwrds = new RewardClubPage();
        rwrds.goTo();
        rwrds.getSummary().verify(ProgramStatus.OPEN, RewardClubLevel.CLUB);
    }

    @Test(dependsOnMethods = { "verifyMember1TierLevel" }, alwaysRun = true)
    public void verifyMember1AnnualActivityCounters()
    {
        member1.getAnnualActivity().setTotalUnits(POINTS_AMOUNT_INT);

        LifetimeActivity lifetimeActivity = member1.getLifetimeActivity();
        lifetimeActivity.setBonusUnits(POINTS_AMOUNT_INT);
        lifetimeActivity.setTotalUnits(POINTS_AMOUNT_INT);
        lifetimeActivity.setCurrentBalance(POINTS_AMOUNT_INT);

        new AnnualActivityPage().verifyCounters(member1);
    }

    @Test(dependsOnMethods = { "verifyMember1AnnualActivityCounters" }, alwaysRun = true)
    public void tryToPurchaseGiftOneself()
    {
        GiftUnitsPopUp giftPopUp = new GiftUnitsPopUp();
        giftPopUp.goTo();

        Input membershipId = giftPopUp.getMembershipId();
        membershipId.typeAndSubmit(member1.getRCProgramId());
        verifyThat(membershipId, isHighlightedAsInvalid(true));

        Select amount = giftPopUp.getAmount();

        giftPopUp.clickSubmitNoError();
        verifyThat(amount, isHighlightedAsInvalid(true));

        amount.select("1000");
        giftPopUp.clickSubmitNoError();
        verifyThat(giftPopUp, displayed(true));
        verifyThat(membershipId, isHighlightedAsInvalid(true));
        verifyThat(amount, isHighlightedAsInvalid(false));

        giftPopUp.close();

        new AnnualActivityPage().verifyCounters(member1);
    }

}
