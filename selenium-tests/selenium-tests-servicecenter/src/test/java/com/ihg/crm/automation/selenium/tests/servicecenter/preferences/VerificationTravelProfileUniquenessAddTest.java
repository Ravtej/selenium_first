package com.ihg.crm.automation.selenium.tests.servicecenter.preferences;

import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyNoError;
import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifySuccess;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.isHighlightedAsInvalid;
import static com.ihg.automation.selenium.matchers.component.GridSize.size;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.preferences.TravelProfileType;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.gwt.components.dialog.ConfirmDialog;
import com.ihg.automation.selenium.gwt.components.input.Input;
import com.ihg.automation.selenium.gwt.components.input.Select;
import com.ihg.automation.selenium.servicecenter.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.servicecenter.pages.preferences.AddTravelProfilePopUp;
import com.ihg.automation.selenium.servicecenter.pages.preferences.EditTravelProfilePopUp;
import com.ihg.automation.selenium.servicecenter.pages.preferences.TravelProfile;
import com.ihg.automation.selenium.servicecenter.pages.preferences.StayPreferencesPage;
import com.ihg.automation.selenium.servicecenter.pages.preferences.TravelProfileGrid;
import com.ihg.automation.selenium.servicecenter.pages.preferences.TravelProfileGridRow;
import com.ihg.automation.selenium.servicecenter.pages.preferences.TravelProfilePopUpBase;
import com.ihg.crm.automation.selenium.tests.servicecenter.LoginLogout;

public class VerificationTravelProfileUniquenessAddTest extends LoginLogout
{
    private static final String TP_NAME_UPDATED = "Test11";
    private static final String TP_NAME_FINAL = "Test12";
    private StayPreferencesPage stayPreferencesPage = new StayPreferencesPage();
    private TravelProfilePopUpBase travelProfilePopUpAdd = new AddTravelProfilePopUp();

    @BeforeClass
    public void beforeClass()
    {
        Member member = new Member();
        member.addProgram(Program.RC);
        member.addAddress(MemberPopulateHelper.getUnitedStatesAddress());
        member.setPersonalInfo(MemberPopulateHelper.getSimplePersonalInfo());

        login();
        new EnrollmentPage().enroll(member);
        stayPreferencesPage.goTo();
    }

    @Test
    public void verifyUniquenessOnCreate()
    {
        stayPreferencesPage.getAddTravelProfile().clickAndWait();
        Input travelProfileName = travelProfilePopUpAdd.getTravelProfileName();
        travelProfileName.type(TravelProfileType.LEISURE.getValue());
        travelProfilePopUpAdd.clickSave();
        verifyThat(travelProfileName, isHighlightedAsInvalid(true));
        travelProfilePopUpAdd.clickClose();

        ConfirmDialog dialog = new ConfirmDialog();
        dialog.clickYes(verifyNoError());

        TravelProfileGrid grid = new StayPreferencesPage().getTravelProfilesGrid();
        verifyThat(grid, size(1));
    }

    @Test(dependsOnMethods = { "verifyUniquenessOnCreate" }, alwaysRun = true)
    public void verifyRowAfterUpdate()
    {
        TravelProfile travelProfile = new TravelProfile(TP_NAME_UPDATED, TravelProfileType.LEISURE, true);

        stayPreferencesPage.getAddTravelProfile().clickAndWait();
        travelProfilePopUpAdd.getTravelProfileName().type(TP_NAME_UPDATED);
        Select type = travelProfilePopUpAdd.getType();
        type.select(TravelProfileType.LEISURE.getCodeWithValue());
        travelProfilePopUpAdd.clickSave(verifyNoError(), verifySuccess(TravelProfilePopUpBase.SUCCESS_MESSAGE_CREATE));
        TravelProfileGridRow row = stayPreferencesPage.getTravelProfilesGrid().getRowByName(TP_NAME_UPDATED);
        row.verify(travelProfile);
    }

    @Test(dependsOnMethods = { "verifyUniquenessOnCreate" }, alwaysRun = true)
    public void verifyUniquenessOnUpdate()
    {
        TravelProfilePopUpBase travelProfilePopUpEdit = new EditTravelProfilePopUp();
        TravelProfileGridRow row = stayPreferencesPage.getTravelProfilesGrid().getRowByNameAndType(TP_NAME_UPDATED,
                TravelProfileType.LEISURE.getValue());

        row.openTravelProfile();
        travelProfilePopUpEdit.getTravelProfileName().type(TravelProfileType.LEISURE.getValue());
        travelProfilePopUpEdit.clickSave();
        verifyThat(travelProfilePopUpEdit.getTravelProfileName(), isHighlightedAsInvalid(true));
    }

    @Test(dependsOnMethods = { "verifyUniquenessOnUpdate" }, alwaysRun = true)
    public void verifyUniquenessOnUpdateAndSave()
    {
        TravelProfilePopUpBase editTravelProfile = new EditTravelProfilePopUp();
        TravelProfile travelProfile = new TravelProfile(TP_NAME_FINAL, TravelProfileType.LEISURE, true);

        editTravelProfile.getTravelProfileName().type(TP_NAME_FINAL);
        editTravelProfile.clickSave(verifyNoError(), verifySuccess(TravelProfilePopUpBase.SUCCESS_MESSAGE_UPDATE));

        TravelProfileGridRow row = stayPreferencesPage.getTravelProfilesGrid().getRowByNameAndType(TP_NAME_FINAL,
                TravelProfileType.LEISURE.getValue());
        row.verify(travelProfile);
    }
}
