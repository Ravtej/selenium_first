package com.ihg.crm.automation.selenium.tests.servicecenter.programinfo;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.event.EventTransactionType.TierLevel.TIER_LEVEL_PURCHASE;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.isDisplayedWithWait;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.isHighlightedAsInvalid;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.common.types.program.ProgramStatus;
import com.ihg.automation.selenium.common.types.program.RewardClubLevel;
import com.ihg.automation.selenium.common.types.program.RewardClubLevelReason;
import com.ihg.automation.selenium.common.types.program.TierLevelExpiration;
import com.ihg.automation.selenium.common.types.program.TierLevelFlag;
import com.ihg.automation.selenium.servicecenter.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsPage;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.RewardClubPage;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.RewardClubSummary;
import com.ihg.automation.selenium.servicecenter.pages.unit.PurchaseTierLevelPopUp;
import com.ihg.crm.automation.selenium.tests.servicecenter.LoginLogout;

public class PurchaseTierLevelDowngradeToGoldTest extends LoginLogout
{
    private Member member = new Member();
    private RewardClubPage rewardClubPage = new RewardClubPage();
    private RewardClubSummary summary;
    private PurchaseTierLevelPopUp purchaseTierLevelPopUp = new PurchaseTierLevelPopUp();
    private AllEventsPage allEventsPage = new AllEventsPage();

    @BeforeClass
    public void beforeClass()
    {
        member.setPersonalInfo(MemberPopulateHelper.getSimplePersonalInfo());
        member.addAddress(MemberPopulateHelper.getUnitedStatesAddress());
        member.addProgram(Program.RC);

        login();

        member = new EnrollmentPage().enroll(member);
        rewardClubPage.goTo();
        summary = rewardClubPage.getSummary();
        summary.clickEdit();
        summary.populateTierLevel(RewardClubLevel.PLTN, RewardClubLevelReason.VIP,
                TierLevelExpiration.EXPIRE_NEXT_YEAR);
        summary.getFlags().select(TierLevelFlag.GENERAL_VIP.getValue());
        summary.clickSave();
    }

    @Test
    public void verifyInitialTierLevel()
    {
        rewardClubPage = new RewardClubPage();
        rewardClubPage.getSummary().verify(ProgramStatus.OPEN, RewardClubLevel.PLTN);
    }

    @Test(dependsOnMethods = { "verifyInitialTierLevel" }, alwaysRun = true)
    public void tryToDowngrade()
    {
        summary = rewardClubPage.getSummary();
        summary.clickPurchase();
        verifyThat(purchaseTierLevelPopUp, isDisplayedWithWait());
        purchaseTierLevelPopUp.getTierLevel().select(RewardClubLevel.GOLD.getCode());
        purchaseTierLevelPopUp.clickSubmit();
        verifyThat(purchaseTierLevelPopUp.getTierLevel(), isHighlightedAsInvalid(true));
        verifyThat(purchaseTierLevelPopUp, displayed(true));
        purchaseTierLevelPopUp.clickClose();
        verifyThat(purchaseTierLevelPopUp, displayed(false));
    }

    @Test(dependsOnMethods = { "tryToDowngrade" }, alwaysRun = true)
    public void verifyPurchaseEvent()
    {
        allEventsPage.goTo();
        verifyThat(allEventsPage.getGrid().getRow(TIER_LEVEL_PURCHASE), displayed(false));
    }
}
