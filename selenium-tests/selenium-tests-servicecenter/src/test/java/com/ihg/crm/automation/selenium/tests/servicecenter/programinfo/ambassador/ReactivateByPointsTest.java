package com.ihg.crm.automation.selenium.tests.servicecenter.programinfo.ambassador;

import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyNoError;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.Constant.RC_POINTS;
import static com.ihg.automation.selenium.common.event.EventTransactionType.Goodwill.GOODWILL;
import static com.ihg.automation.selenium.common.event.EventTransactionType.Membership.MEMBERSHIP_OPEN;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.isDisplayedWithWait;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.common.DateUtils;
import com.ihg.automation.selenium.common.catalog.CatalogItem;
import com.ihg.automation.selenium.common.event.AllEventsRow;
import com.ihg.automation.selenium.common.member.AmbassadorJdbcUtils;
import com.ihg.automation.selenium.common.member.AmbassadorOfferCode;
import com.ihg.automation.selenium.common.member.AmbassadorUtils;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.pages.PaymentMethod;
import com.ihg.automation.selenium.common.payment.Payment;
import com.ihg.automation.selenium.common.payment.PaymentDetailsPopUp;
import com.ihg.automation.selenium.common.payment.PaymentFactory;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.common.types.program.ProgramStatus;
import com.ihg.automation.selenium.servicecenter.pages.LeftPanel;
import com.ihg.automation.selenium.servicecenter.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsGrid;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsGridRow;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsPage;
import com.ihg.automation.selenium.servicecenter.pages.event.enroll.MembershipGridRowContainer;
import com.ihg.automation.selenium.servicecenter.pages.programs.RenewExtendProgramSummary;
import com.ihg.automation.selenium.servicecenter.pages.programs.amb.AmbassadorPage;
import com.ihg.automation.selenium.servicecenter.pages.unit.GoodwillPopUp;

public class ReactivateByPointsTest extends AmbassadorCommon
{
    private static final CatalogItem ENROLMENT_AWARD = AmbassadorUtils.AMBASSADOR_32000_POINTS;

    private AmbassadorPage ambassadorPage = new AmbassadorPage();
    private AllEventsPage allEventsPage = new AllEventsPage();
    private GoodwillPopUp goodwillPopUp = new GoodwillPopUp();
    private EnrollmentPage enrollmentPage = new EnrollmentPage();
    private AllEventsRow rowData = new AllEventsRow(DateUtils.getFormattedDate(), GOODWILL, RC_POINTS, "50000", "");
    private Payment payment = PaymentFactory.getPointsPayment(ENROLMENT_AWARD);
    private Member member;

    @BeforeClass
    public void beforeClass()
    {
        login();

        member = enrollAMBmember();
        new AmbassadorJdbcUtils(jdbcTemplateUpdate, lastUpdateUserId).updateForReactivate(member);
        new LeftPanel().reOpenMemberProfile(member);
    }

    @Test
    public void openGoodwillLoyaltyUnitsPopUp()
    {
        goodwillPopUp.goTo();
    }

    @Test(dependsOnMethods = { "openGoodwillLoyaltyUnitsPopUp" }, alwaysRun = true)
    public void populateGoodwillLoyaltyUnitsPopUp()
    {
        goodwillPopUp.goodwillPoints("50000");
        verifyThat(allEventsPage, isDisplayedWithWait());
        AllEventsGridRow goodwillRow = allEventsPage.getGrid().getRow(GOODWILL);
        verifyThat(goodwillRow, displayed(true));
        goodwillRow.verify(rowData);
    }

    @Test(dependsOnMethods = { "populateGoodwillLoyaltyUnitsPopUp" }, alwaysRun = true)
    public void populateEnrollDetails()
    {
        enrollmentPage.goTo();
        enrollmentPage.getPrograms().selectProgram(Program.AMB);
        enrollmentPage.getAMBOfferCode().selectByCode(AmbassadorOfferCode.CHNRO);
        enrollmentPage.clickSubmit(verifyNoError());
        PaymentDetailsPopUp paymentDetailsPopUp = new PaymentDetailsPopUp();
        paymentDetailsPopUp.selectAmount(ENROLMENT_AWARD);
        verifyThat(paymentDetailsPopUp.getPaymentTypeButton(PaymentMethod.LOYALTY_UNITS), displayed(true));
        paymentDetailsPopUp.clickSubmitNoError();
    }

    @Test(dependsOnMethods = { "populateEnrollDetails" }, alwaysRun = true)
    public void verifySuccessEnrollmentPopUp()
    {
        validateSuccessEnrollmentPopUp(Program.AMB, member);
    }

    @Test(dependsOnMethods = { "verifySuccessEnrollmentPopUp" }, alwaysRun = true)
    public void verifySummary()
    {
        ambassadorPage.goTo();
        RenewExtendProgramSummary ambSummary = ambassadorPage.getSummary();
        verifyThat(ambSummary.getExpirationDate(), hasText(AmbassadorUtils.getExpirationDate()));

        ambSummary.verifyRenewAndExtendButtons(false, true);
        ambSummary.verifyStatus(ProgramStatus.OPEN);
    }

    @Test(dependsOnMethods = "verifySummary", alwaysRun = true)
    public void verifyAllEventsContainer()
    {
        allEventsPage.goTo();
        AllEventsGrid allEventsGrid = allEventsPage.getGrid();
        AllEventsGridRow row = allEventsGrid.getRow(MEMBERSHIP_OPEN);
        verifyThat(row, displayed(true));
        row.expand(MembershipGridRowContainer.class).getMembershipDetails().verify(payment, helper);
    }

    @Test(dependsOnMethods = "verifyAllEventsContainer", alwaysRun = true)
    public void verifyOrderSystemContainer()
    {
        verifyOrderSystemDetailsAfterReactivate(member);
    }
}
