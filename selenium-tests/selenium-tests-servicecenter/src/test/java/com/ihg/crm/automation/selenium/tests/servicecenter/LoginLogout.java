package com.ihg.crm.automation.selenium.tests.servicecenter;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ContextConfiguration;

import com.ihg.atp.wsdl.crm.guest.guest.serviceinterface.v2.GuestServiceDomainInterface;
import com.ihg.atp.wsdl.crm.guest.membership.serviceinterface.v1.MembershipServiceDomainInterface;
import com.ihg.automation.selenium.common.SiteHelperBase;
import com.ihg.automation.selenium.common.User;
import com.ihg.automation.selenium.gwt.components.Component;
import com.ihg.automation.selenium.gwt.components.utils.FindStepUtils;
import com.ihg.automation.selenium.servicecenter.pages.Role;
import com.ihg.automation.selenium.servicecenter.pages.ServiceCenterHelper;
import com.ihg.automation.selenium.tests.common.TestNGBase;
import com.ihg.crm.refdata.ws.v1_0.wsdl.RefdataService;

@ContextConfiguration(locations = { "classpath:app-context.xml" })
public class LoginLogout extends TestNGBase
{
    @Value("${loginUrl}")
    private String loginUrl;

    @Resource(name = "user10T2")
    @Lazy
    protected User user10T2;

    @Value("${memberID:}")
    protected String memberID;

    @Value("${memberIDWithMeetingEvent:}")
    protected String memberIDWithMeetingEvent;

    @Value("${memberWithExternalCampaign:}")
    protected String memberWithExternalCampaign;

    @Value("${memberWithExternalCampaignWithPointsEarned:}")
    protected String memberWithExternalCampaignWithPointsEarned;

    @Value("${memberWithExternalCampaignWithNightsEarned:}")
    protected String memberWithExternalCampaignWithNightsEarned;

    @Value("${memberWithExternalCampaignWithPointsWithoutNights:}")
    protected String memberWithExternalCampaignWithPointsWithoutNights;

    @Value("${lastUpdateUserId:}")
    protected String lastUpdateUserId;

    @Value("${achieveGoldPoints:}")
    protected Integer achieveGoldPoints;

    @Value("${achievePlatinumPoints:}")
    protected Integer achievePlatinumPoints;

    @Value("${achieveGoldNights:}")
    protected Integer achieveGoldNights;

    @Value("${achieveRamIQualifyingSpend:}")
    protected Integer achieveRamIQualifyingSpend;

    @Value("${achieveRamIcHotels:}")
    protected Integer achieveRamIcHotels;

    @Resource
    @Lazy
    protected JdbcTemplate jdbcTemplate;

    @Resource
    @Lazy
    protected JdbcTemplate jdbcTemplateUpdate;

    @Autowired
    @Lazy
    protected GuestServiceDomainInterface guestClient;

    @Autowired
    @Lazy
    protected RefdataService refdataClient;

    @Autowired
    @Lazy
    protected MembershipServiceDomainInterface membershipClient;

    @Override
    protected SiteHelperBase getHelper()
    {
        return new ServiceCenterHelper(loginUrl);
    }

    public void login()
    {
        helper.login(user10T2, Role.SYSTEMS_MANAGER, true);
    }

    public Component getWelcome()
    {
        return new Component(null, "Welcome screen", "Welcome screen",
                FindStepUtils.byXpathWithWait(".//div[@class='welcome-panel']"));
    }

}
