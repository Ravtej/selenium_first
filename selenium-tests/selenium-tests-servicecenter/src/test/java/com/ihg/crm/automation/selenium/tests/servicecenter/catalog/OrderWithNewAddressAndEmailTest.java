package com.ihg.crm.automation.selenium.tests.servicecenter.catalog;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static org.hamcrest.core.Is.is;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.member.GuestAddress;
import com.ihg.automation.selenium.common.member.GuestEmail;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.order.Order;
import com.ihg.automation.selenium.common.personal.EmailContactList;
import com.ihg.automation.selenium.common.types.EmailFormat;
import com.ihg.automation.selenium.common.types.EmailType;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.gwt.components.Mode;
import com.ihg.automation.selenium.servicecenter.pages.catalog.CatalogItemDetailsPopUp;
import com.ihg.automation.selenium.servicecenter.pages.catalog.CatalogPage;
import com.ihg.automation.selenium.servicecenter.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.servicecenter.pages.event.order.OrderEventsPage;
import com.ihg.automation.selenium.servicecenter.pages.order.OrderSummaryPopUp;
import com.ihg.automation.selenium.servicecenter.pages.personal.AddressContactList;
import com.ihg.automation.selenium.servicecenter.pages.personal.PersonalInfoPage;

public class OrderWithNewAddressAndEmailTest extends CatalogCommon
{
    private Member member = new Member();
    private CatalogPage catalogPage = new CatalogPage();
    private GuestEmail newEmail = new GuestEmail();
    private Order order = new Order();
    private GuestAddress address;
    private GuestAddress newAddress;
    private static String itemId;

    @BeforeClass
    public void beforeClass()
    {
        newEmail = MemberPopulateHelper.getRandomEmail();
        newEmail.setType(EmailType.BUSINESS);
        newEmail.setFormat(EmailFormat.HTML);

        address = MemberPopulateHelper.getUnitedStatesAddress();

        member.setPersonalInfo(MemberPopulateHelper.getSimplePersonalInfo());
        member.addAddress(address);
        member.addProgram(Program.RC);

        newAddress = MemberPopulateHelper.getUnitedStatesSecondAddress();
        newAddress.setPreferred(false);

        order.setDeliveryEmail(newEmail.getEmail());
        order.setName(member.getPersonalInfo().getName().getNameOnPanel());

        itemId = getItemId();

        login();

        new EnrollmentPage().enroll(member);
    }

    @Test
    public void typeNewAddressAndEmailWithoutSaving()
    {
        typeNewData(false);
    }

    @Test(dependsOnMethods = { "typeNewAddressAndEmailWithoutSaving" }, alwaysRun = true)
    public void checkNewAddressAndEmailWithoutSavingInOrder()
    {
        order.setDeliveryAddress(newAddress);

        checkShippingDetails(order);
    }

    @Test(dependsOnMethods = { "checkNewAddressAndEmailWithoutSavingInOrder" }, alwaysRun = true)
    public void checkNewAddressAndEmailWithoutSavingOnPersonalPage()
    {
        PersonalInfoPage personalInfoPage = new PersonalInfoPage();
        personalInfoPage.goTo();

        AddressContactList addressList = personalInfoPage.getAddressList();
        verifyThat(addressList, addressList.getContactsCount(), is(1), "One address on personal info page");
        addressList.getExpandedContact().verify(address, Mode.VIEW);

        EmailContactList emailList = personalInfoPage.getEmailList();
        verifyThat(emailList, emailList.getContactsCount(), is(0), "No emails on personal info page");
    }

    @Test(dependsOnMethods = { "checkNewAddressAndEmailWithoutSavingOnPersonalPage" }, alwaysRun = true)
    public void typeNewAddressAndEmailWithSave()
    {
        typeNewData(true);
    }

    @Test(dependsOnMethods = { "typeNewAddressAndEmailWithSave" }, alwaysRun = true)
    public void checkNewAddressAndEmailWithSaveInOrder()
    {
        order.setDeliveryAddress(newAddress);

        checkShippingDetails(order);
    }

    @Test(dependsOnMethods = { "checkNewAddressAndEmailWithSaveInOrder" }, alwaysRun = true)
    public void checkNewAddressAndEmailWithSaveOnPersonalPage()
    {
        PersonalInfoPage personalInfoPage = new PersonalInfoPage();
        personalInfoPage.goTo();

        AddressContactList addressList = personalInfoPage.getAddressList();
        verifyThat(addressList, addressList.getContactsCount(), is(2), "Two addresses on personal info page");
        addressList.getExpandedContact().verify(address, Mode.VIEW);
        addressList.getExpandedContact(2).verify(newAddress, Mode.VIEW);

        personalInfoPage.getEmailList().getExpandedContact().verify(newEmail, Mode.VIEW);
    }

    private void typeNewData(Boolean isSaved)
    {
        catalogPage.goTo();
        catalogPage.getButtonBar().clickClearCriteria();
        catalogPage.searchByItemId(itemId);
        CatalogItemDetailsPopUp catalogItemDetailsPopUp = catalogPage.openCatalogItemDetailsPopUp();
        OrderSummaryPopUp orderSummaryPopUp = catalogItemDetailsPopUp.openOrderSummaryPopUp();

        orderSummaryPopUp.getUseExisting().select("New Address");
        orderSummaryPopUp.getAddress().populate(newAddress);

        if (isSaved)
        {
            orderSummaryPopUp.getSaveAddress().check();

            orderSummaryPopUp.getSaveEmail().check();
            orderSummaryPopUp.getEmail().populate(newEmail);
        }
        else
        {
            orderSummaryPopUp.getSelectEmail().type(newEmail.getEmail());
        }
        orderSummaryPopUp.clickCheckout();
        verifyThat(orderSummaryPopUp, displayed(false));
        OrderEventsPage orderPage = new OrderEventsPage();
        verifyThat(orderPage, displayed(true));
    }

}
