package com.ihg.crm.automation.selenium.tests.servicecenter.enrollment.karma;

import static com.ihg.automation.common.DateUtils.dd_MMM_YYYY;
import static com.ihg.automation.common.DateUtils.getFormattedDate;
import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyError;
import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyNoError;
import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifySuccess;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.assertThat;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.Constant.NOT_AVAILABLE;
import static com.ihg.automation.selenium.common.LeftPanelBase.GREY_COLOR;
import static com.ihg.automation.selenium.common.event.EventTransactionType.Membership.ENROLL;
import static com.ihg.automation.selenium.common.types.program.Program.KAR;
import static com.ihg.automation.selenium.common.types.program.Program.RC;
import static com.ihg.automation.selenium.common.types.program.ProgramStatus.OPEN;
import static com.ihg.automation.selenium.gwt.components.Mode.VIEW;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasDefault;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.isDisplayedWithWait;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.isHighlightedAsInvalid;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.isSelected;
import static com.ihg.automation.selenium.matchers.component.GridSize.size;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static com.ihg.automation.selenium.matchers.component.HasTipText.hasWarningTipText;
import static com.ihg.automation.selenium.matchers.component.ViewableMatcher.hasTextInView;
import static com.ihg.automation.selenium.servicecenter.pages.Messages.HIGHLIGHTED_FIELDS_DID_NOT_PASS_VALIDATION;
import static com.ihg.automation.selenium.servicecenter.pages.history.AuditConstant.ProgramInformation.KARMA_MEMBER_SINCE_DATE;
import static com.ihg.automation.selenium.servicecenter.pages.history.AuditConstant.ProgramInformation.PROGRAM_INFORMATION;
import static com.ihg.automation.selenium.servicecenter.pages.history.AuditConstant.ProgramInformation.RC_MEMBER_SINCE_DATE;
import static com.ihg.automation.selenium.servicecenter.pages.personal.PersonalInfoPage.SUCCESS_MESSAGE;
import static org.hamcrest.core.StringContains.containsString;
import static org.hamcrest.core.StringEndsWith.endsWith;

import java.util.ArrayList;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.common.DateUtils;
import com.ihg.automation.selenium.common.AddressNotMailableDialog;
import com.ihg.automation.selenium.common.ProgramGridRow;
import com.ihg.automation.selenium.common.billing.EventBillingDetailsTab;
import com.ihg.automation.selenium.common.earning.EventEarningDetailsTab;
import com.ihg.automation.selenium.common.enroll.EnrollProgramContainer;
import com.ihg.automation.selenium.common.enroll.SuccessEnrolmentPopUp;
import com.ihg.automation.selenium.common.event.AllEventsRowFactory;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.types.program.KarmaLevel;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.common.types.program.TierLevelFlag;
import com.ihg.automation.selenium.gwt.components.DualList;
import com.ihg.automation.selenium.gwt.components.input.DateInput;
import com.ihg.automation.selenium.gwt.components.panel.FieldSet;
import com.ihg.automation.selenium.matchers.component.ContainsDualListItems;
import com.ihg.automation.selenium.matchers.component.HasBackgroundColor;
import com.ihg.automation.selenium.servicecenter.pages.LeftPanel;
import com.ihg.automation.selenium.servicecenter.pages.account.AccountInfoPage;
import com.ihg.automation.selenium.servicecenter.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsGrid;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsGridRow;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsPage;
import com.ihg.automation.selenium.servicecenter.pages.event.enroll.MembershipDetailsPopUp;
import com.ihg.automation.selenium.servicecenter.pages.event.enroll.MembershipGridRowContainer;
import com.ihg.automation.selenium.servicecenter.pages.history.ProfileHistoryGridRow;
import com.ihg.automation.selenium.servicecenter.pages.history.ProfileHistoryPage;
import com.ihg.automation.selenium.servicecenter.pages.personal.PersonalInfoPage;
import com.ihg.automation.selenium.servicecenter.pages.programs.KarmaPage;
import com.ihg.automation.selenium.servicecenter.pages.programs.ProgramPageBase;
import com.ihg.automation.selenium.servicecenter.pages.programs.ProgramSummary;
import com.ihg.crm.automation.selenium.tests.servicecenter.LoginLogout;

@Test(groups = { "karma" })
public class KAREnrollmentTest extends LoginLogout
{
    private Member member = new Member();
    private KarmaPage karmaPage = new KarmaPage();
    private MembershipDetailsPopUp membershipDetailsPopUp;
    private MembershipGridRowContainer membershipGridRowContainer;
    private EnrollmentPage enrollmentPage;

    @BeforeClass
    public void beforeClass()
    {
        member.setPersonalInfo(MemberPopulateHelper.getSimplePersonalInfo());
        member.addProgram(KAR);
        member.addPhone(MemberPopulateHelper.getRandomPhone());
        member.addEmail(MemberPopulateHelper.getRandomEmail());
        member.addAddress(MemberPopulateHelper.getUnitedStatesAddress());

        login();
    }

    @Test(priority = 10)
    public void verifyEnrollmentFieldsForKarma()
    {
        ArrayList<String> expectedFlagList = new ArrayList<String>(TierLevelFlag.getKarValueList());
        expectedFlagList.addAll(TierLevelFlag.getRcValueList());

        enrollmentPage = new EnrollmentPage();
        enrollmentPage.goTo();
        EnrollProgramContainer programs = enrollmentPage.getPrograms();
        programs.selectProgram(Program.KAR);
        verifyThat(programs.getProgramCheckbox(Program.RC), isSelected(true));

        verifyThat(enrollmentPage.getCustomerInfo(), displayed(true));
        verifyThat(enrollmentPage.getAddress(), displayed(true));
        verifyThat(enrollmentPage.getPhone(), displayed(true));
        verifyThat(enrollmentPage.getEmail(), displayed(true));
        verifyThat(enrollmentPage.getAdditionalInformation(), displayed(true));
        verifyThat(enrollmentPage.getEarningPreferenceSeparator(), displayed(true));
        verifyThat(enrollmentPage.getRCOfferCode(), displayed(true));
        verifyThat(enrollmentPage.getReferredMember().getRCReferredBy(), displayed(true));

        DualList flags = enrollmentPage.getFlags();
        verifyThat(flags, displayed(true));
        verifyThat(flags.getLeftList(), ContainsDualListItems.containsItems(expectedFlagList));
    }

    @Test(priority = 20)
    public void tryToEnrollToKarWithoutMandatoryFields()
    {

        enrollmentPage.clickSubmit(verifyError(HIGHLIGHTED_FIELDS_DID_NOT_PASS_VALIDATION));
    }

    @Test(priority = 30)
    public void tryToEnrollToKarWithInvalidAddress()
    {
        enrollmentPage.populate(member);
        enrollmentPage.getAddress().getAddress1().type("invalid address");

        enrollmentPage.clickSubmit();

        AddressNotMailableDialog addressNotMailableDialog = new AddressNotMailableDialog();
        verifyThat(addressNotMailableDialog, displayed(true));
        addressNotMailableDialog.clickContinue();
    }

    @Test(priority = 40)
    public void cancelEnrollment()
    {
        enrollmentPage.clickCancel();
        assertThat(getWelcome(), displayed(true));
    }

    @Test(priority = 50)
    public void createEnrollmentToKAR()
    {
        enrollmentPage.submitEnrollment(member);
        SuccessEnrolmentPopUp popUp = new SuccessEnrolmentPopUp();

        verifyThat(popUp, isDisplayedWithWait());
        verifyThat(popUp.getMemberId(KAR), hasText(popUp.getMemberId(RC).getText()));

        popUp.clickDone(verifyNoError());
    }

    @Test(priority = 60)
    public void validatePersonalInformation()
    {
        PersonalInfoPage persInfo = new PersonalInfoPage();
        persInfo.getCustomerInfo().verify(member, VIEW);
        persInfo.getAddressList().getExpandedContact().verify(member.getPreferredAddress(), VIEW);
        persInfo.getPhoneList().getExpandedContact().verify(member.getPreferredPhone(), VIEW);
        persInfo.getEmailList().getExpandedContact().verify(member.getPreferredEmail(), VIEW);
    }

    @Test(priority = 70)
    public void validateAccountInformation()
    {
        AccountInfoPage accountPage = new AccountInfoPage();
        accountPage.goTo();
        verifyThat(accountPage.getSecurity().getLockout(), hasTextInView("Unlocked"));
        accountPage.getFailedSelfServiceLoginAttempts().expand();
        verifyThat(accountPage.getFailedSelfServiceLoginAttempts().getGrid(), size(1));
        accountPage.verify(OPEN);
        verifyThat(accountPage.getFlags(), hasText(containsString("No flags selected")));
    }

    @Test(priority = 80)
    public void validateLeftPanelProgramInfoNavigation()
    {
        ProgramGridRow programGridRow = new LeftPanel().getProgramInfoPanel().getPrograms().getProgram(KAR);
        verifyThat(programGridRow, HasBackgroundColor.hasBackgroundColor(GREY_COLOR));

        verifyThat(karmaPage, displayed(false));
        programGridRow.goToProgramPage();
        verifyThat(karmaPage, displayed(true));
    }

    @Test(priority = 90)
    public void verifyKarmaOverviewSection()
    {
        FieldSet programOverview = karmaPage.getProgramOverview();
        programOverview.expand();
        verifyThat(programOverview, hasText(endsWith(KarmaLevel.KAR.getValue())));
    }

    @Test(priority = 100)
    public void validateProgramInfoSummaryDetails()
    {
        ProgramSummary karmaSummary = karmaPage.getSummary();

        karmaSummary.verify(OPEN, KarmaLevel.KAR);
        verifyThat(karmaSummary.getMemberId(), hasText(member.getKARProgramId()));
        verifyThat(karmaSummary, hasText(containsString("No flags selected")));
    }

    @Test(priority = 110)
    public void validateProgramInfoEnrollmentDetails()
    {
        ProgramPageBase.EnrollmentDetails enrollmentDetails = karmaPage.getEnrollmentDetails();
        verifyThat(enrollmentDetails.getOfferCode(), hasDefault());
        verifyThat(enrollmentDetails.getPaymentDetails().getMethod(), hasDefault());
        verifyThat(enrollmentDetails.getReferringMember(), hasText(NOT_AVAILABLE));
        verifyThat(enrollmentDetails.getRenewalDate(), hasDefault());

        String currentDate = DateUtils.getFormattedDate();
        verifyThat(enrollmentDetails.getEnrollDate(), hasText(currentDate));
        verifyThat(enrollmentDetails.getMemberSince(), hasTextInView(currentDate));

        enrollmentDetails.verifyScSource(helper);
    }

    @Test(priority = 115)
    public void verifyHistoryProgramInfoAfterEnrollment()
    {
        ProfileHistoryPage historyPage = new ProfileHistoryPage();
        historyPage.goTo();
        verifyNoError();

        ProfileHistoryGridRow programInfoRow = historyPage.getProfileHistoryGrid().getRow(1)
                .getSubRowByName(PROGRAM_INFORMATION);

        String enrollDate = getFormattedDate(dd_MMM_YYYY);
        programInfoRow.getSubRowByName(RC_MEMBER_SINCE_DATE).verifyAddAction(enrollDate);
        programInfoRow.getSubRowByName(KARMA_MEMBER_SINCE_DATE).verifyAddAction(enrollDate);
    }

    @Test(priority = 116)
    public void cancelEditMemberSinceDate()
    {
        karmaPage.goTo();
        ProgramPageBase.EnrollmentDetails enrollmentDetails = karmaPage.getEnrollmentDetails();
        enrollmentDetails.clickEdit(verifyNoError());

        DateInput memberSince = enrollmentDetails.getMemberSince();
        memberSince.type(getFormattedDate(-1));
        enrollmentDetails.clickCancel(verifyNoError());

        verifyThat(memberSince, hasTextInView(getFormattedDate()));
    }

    @Test(priority = 117)
    public void tryToEditMemberSinceDateWithFutureDate()
    {
        ProgramPageBase.EnrollmentDetails enrollmentDetails = karmaPage.getEnrollmentDetails();
        enrollmentDetails.clickEdit(verifyNoError());

        DateInput memberSince = enrollmentDetails.getMemberSince();
        memberSince.type(getFormattedDate(1));
        enrollmentDetails.clickSave(verifyNoError());

        verifyThat(memberSince, isHighlightedAsInvalid(true));
        verifyThat(memberSince, hasWarningTipText("Member Since Date may not be a future date"));

        enrollmentDetails.clickCancel(verifyNoError());
        verifyThat(memberSince, hasTextInView(getFormattedDate()));
    }

    @Test(priority = 118)
    public void editMemberSinceDate()
    {
        ProgramPageBase.EnrollmentDetails enrollmentDetails = karmaPage.getEnrollmentDetails();
        enrollmentDetails.clickEdit(verifyNoError());

        String yesterday = getFormattedDate(-1);
        enrollmentDetails.getMemberSince().type(yesterday);
        enrollmentDetails.clickSave(verifySuccess(SUCCESS_MESSAGE));

        verifyThat(enrollmentDetails.getMemberSince(), hasTextInView(yesterday));
    }

    @Test(priority = 120)
    public void openAllEventsEnrollKarmaEventDetails()
    {
        AllEventsPage allEventsPage = new AllEventsPage();
        allEventsPage.goTo();
        AllEventsGrid allEventsGrid = allEventsPage.getGrid();

        AllEventsGridRow allEventsGridRow = allEventsGrid.getEnrollRow(KAR);
        allEventsGridRow.verify(AllEventsRowFactory.getEnrollEvent(KAR));

        membershipGridRowContainer = allEventsGridRow.expand(MembershipGridRowContainer.class);
        membershipGridRowContainer.getMembershipDetails().verifyScSource(helper);
    }

    @Test(priority = 130)
    public void verifyEnrollmentAllEventsMembershipDetailsTab()
    {
        membershipGridRowContainer.getDetails().clickAndWait();
        membershipDetailsPopUp = new MembershipDetailsPopUp();
        membershipDetailsPopUp.getMembershipDetailsTab().getMembershipDetails().verifyScSource(helper);
    }

    @Test(priority = 140)
    public void verifyAllEventsEarningDetails()
    {
        EventEarningDetailsTab tab = membershipDetailsPopUp.getEarningDetailsTab();
        tab.goTo();

        tab.verifyBasicDetailsNoPointsAndMiles();
    }

    @Test(priority = 150)
    public void verifyAllEventsBillingDetails()
    {
        EventBillingDetailsTab tab = membershipDetailsPopUp.getBillingDetailsTab();
        tab.goTo();
        tab.verifyTabIsEmpty(ENROLL);
        membershipDetailsPopUp.close();
    }

    @Test(priority = 160)
    public void openAllEventsEnrollRCEventDetails()
    {
        AllEventsPage allEventsPage = new AllEventsPage();
        allEventsPage.goTo();

        AllEventsGridRow allEventsGridRow = allEventsPage.getGrid().getEnrollRow(RC);
        allEventsGridRow.verify(AllEventsRowFactory.getEnrollEvent(RC));

        membershipGridRowContainer.getMembershipDetails().verifyScSource(helper);
    }

    @Test(priority = 170)
    public void tryToEnrollToKarAgain()
    {
        enrollmentPage.goTo();

        EnrollProgramContainer programs = enrollmentPage.getPrograms();
        verifyThat(programs.getProgramCheckbox(Program.KAR), displayed(false));
        verifyThat(programs.getProgramChecked(Program.KAR), displayed(true));

        enrollmentPage.clickCancel();
    }

}
