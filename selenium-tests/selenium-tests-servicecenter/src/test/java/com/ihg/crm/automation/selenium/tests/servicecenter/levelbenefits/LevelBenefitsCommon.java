package com.ihg.crm.automation.selenium.tests.servicecenter.levelbenefits;

import static com.ihg.automation.common.DateUtils.DATE_FORMAT;

import org.joda.time.DateTime;

import com.google.common.collect.ImmutableList;
import com.ihg.automation.selenium.common.catalog.CatalogItem;
import com.ihg.automation.selenium.common.types.program.RewardClubLevel;
import com.ihg.crm.automation.selenium.tests.servicecenter.LoginLogout;

public class LevelBenefitsCommon extends LoginLogout
{
    private static final String END_DATE = DateTime.now().monthOfYear().withMaximumValue().dayOfMonth()
            .withMaximumValue().plusYears(1).toString(DATE_FORMAT);
    public static final String SUCCESS_BENEFIT_REDEEM_MESSAGE = "Tier-Level Benefit was successfully redeemed";

    public CatalogItem benefitGiftPlatinumItem = CatalogItem
            .builder("BTLRC20", "Spire Elite - Choice Benefit - Gift Platinum")
            .tierLevels(ImmutableList.of(RewardClubLevel.SPRE.getValue()))
            .itemDescription("Give a friend Platinum Elite as a Choice Benefit for achieving IHGRC Spire Elite!")
            .endDate(END_DATE).build();;

    public CatalogItem benefitPointsItem = CatalogItem.builder("BPRC20", "Spire Elite - Choice Benefit - 25,000 Points")
            .tierLevels(ImmutableList.of(RewardClubLevel.SPRE.getValue())).loyaltyUnitCost("25,000")
            .itemDescription("Enjoy 25,000 Points as a Choice Benefit for achieving IHGRC Spire Elite!")
            .endDate(END_DATE).build();
}
