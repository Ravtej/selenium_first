package com.ihg.crm.automation.selenium.tests.servicecenter.copperfield;

import static com.ihg.automation.common.DateUtils.DATE_FORMAT;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasDefault;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasText;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.ihg.automation.common.DateUtils;
import com.ihg.automation.common.RandomUtils;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberAlliance;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.stay.Stay;
import com.ihg.automation.selenium.common.types.Alliance;
import com.ihg.automation.selenium.common.types.GoodwillReason;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.servicecenter.pages.LeftPanel;
import com.ihg.automation.selenium.servicecenter.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.servicecenter.pages.event.stay.StayEventsPage;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.RewardClubPage;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.RewardClubSummary;
import com.ihg.automation.selenium.servicecenter.pages.unit.GoodwillPopUp;
import com.ihg.crm.automation.selenium.tests.servicecenter.LoginLogout;

@Test(groups = { "copperfield" })
public class MileEarningTest extends LoginLogout
{
    private Member member;
    private Alliance alliance;

    private final static String UPDATE_EXPIRATION_DATE = "UPDATE DGST.MBRSHP SET" //
            + " LST_ACTV_DT = TRUNC(SYSDATE -1),"//
            + " LST_UPDT_USR_ID='%2$s',"//
            + " LST_UPDT_TS = SYSDATE"//
            + " WHERE MBRSHP_ID='%1$s' AND LYTY_PGM_CD='PC'";

    @BeforeClass
    public void beforeClass()
    {
        member = new Member();
        member.setPersonalInfo(MemberPopulateHelper.getSimplePersonalInfo());
        member.addAddress(MemberPopulateHelper.getUnitedStatesAddress());
        member.addProgram(Program.RC);
        MemberAlliance memberAlliance = new MemberAlliance(Alliance.DJA, RandomUtils.getRandomNumber(10),
                member.getName());
        alliance = memberAlliance.getAlliance();
        member.setMilesEarningPreference(memberAlliance);

        login();

        member = new EnrollmentPage().enroll(member);

    }

    @BeforeMethod
    public void upgradeDatesInDb()
    {
        jdbcTemplateUpdate.update(String.format(UPDATE_EXPIRATION_DATE, member.getRCProgramId(), lastUpdateUserId));
    }

    @Test
    public void goodwillMiles()
    {
        new GoodwillPopUp().goodwillUnits("100", alliance.getCode(), GoodwillReason.AGENT_ERROR);
        verifyDates();
    }

    @Test
    public void createMissingStay()
    {
        Stay stay = new Stay();
        stay.setHotelCode("ATLCP");
        stay.setCheckInOutByNights(1);
        stay.setRateCode("TEST");
        stay.setAvgRoomRate(10.0);
        stay.setEarningPreference(alliance.getValue());
        new StayEventsPage().createStay(stay);

        verifyDates();
    }

    private void verifyDates()
    {
        RewardClubPage rewardClubPage = new RewardClubPage();
        rewardClubPage.goTo();
        RewardClubSummary summary = rewardClubPage.getSummary();

        verifyThat(summary.getBalanceExpirationDate(), hasDefault());
        verifyThat(summary.getLastActivityDate(), hasText(DateUtils.now().toString(DATE_FORMAT)));
        verifyThat(summary.getLastAdjusted(), hasDefault());

        verifyThat(new LeftPanel().getProgramInfoPanel().getPrograms().getRewardClubProgram().getExpirationDate(),
                displayed(false));
    }

}
