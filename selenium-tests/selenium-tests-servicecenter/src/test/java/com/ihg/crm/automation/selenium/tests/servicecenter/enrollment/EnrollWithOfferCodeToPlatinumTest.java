package com.ihg.crm.automation.selenium.tests.servicecenter.enrollment;

import static com.ihg.automation.selenium.assertions.Assume.assumeThat;
import static com.ihg.automation.selenium.common.types.program.Program.RC;
import static com.ihg.automation.selenium.common.types.program.ProgramStatus.OPEN;
import static com.ihg.automation.selenium.common.types.program.RewardClubLevel.PLTN;
import static com.ihg.automation.selenium.common.types.program.RewardClubLevelReason.COMPETITOR;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNot.not;

import java.util.HashMap;
import java.util.Map;

import org.springframework.dao.EmptyResultDataAccessException;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.servicecenter.pages.LeftPanel;
import com.ihg.automation.selenium.servicecenter.pages.annual.AnnualActivityPage;
import com.ihg.automation.selenium.servicecenter.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.RewardClubPage;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.RewardClubSummary;
import com.ihg.crm.automation.selenium.tests.servicecenter.LoginLogout;

public class EnrollWithOfferCodeToPlatinumTest extends LoginLogout
{
    private Member member = new Member();

    private final static String OFFER_CODE = "SELECT RULE_ATTR_VAL "//
            + "FROM RULE.RULE_ATTR "//
            + "WHERE RULE_KEY = 50009 "//
            + "AND RULE_ATTR_TYP_CD = 'Enrollment.Offer.TreatmentId' "//
            + "AND ROWNUM < 2";

    @BeforeClass
    public void before()
    {
        Map offerCode = getOfferCode();

        assumeThat(offerCode.size(), not(is(0)), "Skipped since no RULE_ATTR_VAL in database found.");

        member.addProgram(RC);
        member.setPersonalInfo(MemberPopulateHelper.getSimplePersonalInfo());
        member.addAddress(MemberPopulateHelper.getUnitedStatesAddress());
        member.setRewardClubOfferCode(offerCode.get("RULE_ATTR_VAL").toString());

        login();
    }

    @Test(priority = 10)
    public void enrollMember()
    {
        // add counter waiter for rules process the level before verification
        new AnnualActivityPage().captureCounters(new EnrollmentPage().enroll(member), jdbcTemplate);
    }

    @Test(dependsOnMethods = { "enrollMember" }, priority = 20)
    public void verifyMemberTierLevelWithReason()
    {
        RewardClubPage rewardClubPage = new RewardClubPage();
        rewardClubPage.goTo();

        RewardClubSummary rcSummary = rewardClubPage.getSummary();
        rcSummary.verify(OPEN, PLTN, 1, COMPETITOR);

        new LeftPanel().getProgramInfoPanel().getPrograms().getRewardClubProgram().verify(PLTN, "0");
    }

    private Map getOfferCode()
    {
        try
        {
            return jdbcTemplate.queryForMap(OFFER_CODE);
        }
        catch (EmptyResultDataAccessException e)
        {
            return new HashMap();
        }
    }
}
