package com.ihg.crm.automation.selenium.tests.servicecenter.enrollment;

import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyNoError;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.Constant.NOT_AVAILABLE;
import static com.ihg.automation.selenium.common.event.EventTransactionType.Membership.ENROLL;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasDefault;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.isDisplayedWithWait;
import static com.ihg.automation.selenium.matchers.component.GridSize.size;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static com.ihg.automation.selenium.matchers.component.ViewableMatcher.hasTextInView;
import static org.hamcrest.core.StringContains.containsString;

import java.util.ArrayList;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.common.DateUtils;
import com.ihg.automation.selenium.common.CustomerInfoPanel;
import com.ihg.automation.selenium.common.ProgramsGrid;
import com.ihg.automation.selenium.common.billing.EventBillingDetailsTab;
import com.ihg.automation.selenium.common.earning.EventEarningDetailsTab;
import com.ihg.automation.selenium.common.enroll.EnrollProgramContainer;
import com.ihg.automation.selenium.common.enroll.SuccessEnrolmentPopUp;
import com.ihg.automation.selenium.common.event.AllEventsRowFactory;
import com.ihg.automation.selenium.common.member.GuestAddress;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.common.types.program.ProgramStatus;
import com.ihg.automation.selenium.gwt.components.Component;
import com.ihg.automation.selenium.gwt.components.Mode;
import com.ihg.automation.selenium.servicecenter.pages.LeftPanel;
import com.ihg.automation.selenium.servicecenter.pages.account.AccountInfoPage;
import com.ihg.automation.selenium.servicecenter.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsGridRow;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsPage;
import com.ihg.automation.selenium.servicecenter.pages.event.enroll.MembershipDetails;
import com.ihg.automation.selenium.servicecenter.pages.event.enroll.MembershipDetailsPopUp;
import com.ihg.automation.selenium.servicecenter.pages.event.enroll.MembershipGridRowContainer;
import com.ihg.automation.selenium.servicecenter.pages.personal.PersonalInfoPage;
import com.ihg.automation.selenium.servicecenter.pages.programs.EmployeePage;
import com.ihg.automation.selenium.servicecenter.pages.programs.ProgramPageBase.EnrollmentDetails;
import com.ihg.crm.automation.selenium.tests.servicecenter.LoginLogout;

public class EnrollmentToEMPTest extends LoginLogout
{
    private Member member = new Member();
    private EnrollmentPage enrollmentPage = new EnrollmentPage();
    private EmployeePage employeePage = new EmployeePage();

    @BeforeClass
    public void beforeClass()
    {
        member.setPersonalInfo(MemberPopulateHelper.getSimplePersonalInfo());
        member.addProgram(Program.EMP);
        member.addPhone(MemberPopulateHelper.getRandomPhone());
        member.addEmail(MemberPopulateHelper.getRandomEmail());
        member.addAddress(MemberPopulateHelper.getUnitedStatesAddress());

        login();
    }

    @Test
    public void additionalInfoCheckBoxVerify()
    {
        enrollmentPage.goTo();
        enrollmentPage.getPrograms().selectProgram(Program.EMP);
        verifyThat(enrollmentPage.getAdditionalInformation().getEMPMemberID(), displayed(true));
    }

    @Test(dependsOnMethods = { "additionalInfoCheckBoxVerify" }, alwaysRun = true)
    public void createEnrollmentToEMP()
    {
        enrollmentPage.submitEnrollment(member);
        SuccessEnrolmentPopUp popUp = new SuccessEnrolmentPopUp();
        verifyThat(popUp, isDisplayedWithWait());
        verifyThat(popUp.getMemberId(Program.EMP), displayed(true));
        popUp.clickDone(verifyNoError());
    }

    @Test(dependsOnMethods = { "createEnrollmentToEMP" }, priority = 0)
    public void validatePersonalInformation()
    {
        PersonalInfoPage persInfo = new PersonalInfoPage();
        persInfo.getCustomerInfo().verify(member, Mode.VIEW);
        persInfo.getAddressList().getExpandedContact().verify(member.getPreferredAddress(), Mode.VIEW);
        persInfo.getPhoneList().getExpandedContact().verify(member.getPreferredPhone(), Mode.VIEW);
        persInfo.getEmailList().getExpandedContact().verify(member.getPreferredEmail(), Mode.VIEW);
    }

    @Test(dependsOnMethods = { "createEnrollmentToEMP" }, priority = 1)
    public void validateAccountInformation()
    {
        AccountInfoPage accountPage = new AccountInfoPage();
        accountPage.goTo();
        verifyThat(accountPage.getSecurity().getLockout(), hasTextInView("Unlocked"));
        accountPage.getFailedSelfServiceLoginAttempts().expand();
        verifyThat(accountPage.getFailedSelfServiceLoginAttempts().getGrid(), size(1));
        accountPage.verify(ProgramStatus.OPEN);
        verifyThat(accountPage.getFlags(), hasText(containsString("No flags selected")));
    }

    @Test(dependsOnMethods = { "createEnrollmentToEMP" }, priority = 2)
    public void validateLeftPanelProgramInfoNavigation()
    {
        verifyThat(employeePage, displayed(false));
        ProgramsGrid grid = new LeftPanel().getProgramInfoPanel().getPrograms();
        grid.getProgram(Program.EMP).goToProgramPage();
        verifyThat(employeePage, displayed(true));
    }

    @Test(dependsOnMethods = { "createEnrollmentToEMP" }, priority = 3)
    public void validateEmployeeProgramInformation()
    {
        employeePage.goTo();
        verifyThat(employeePage.getSummary().getStatus(), hasTextInView(ProgramStatus.OPEN));
        verifyThat(employeePage.getSummary().getMemberId(), hasText(member.getEMPProgramId()));
        EnrollmentDetails enrlDetails = employeePage.getEnrollmentDetails();
        verifyThat(enrlDetails.getOfferCode(), hasText("SCCAL"));
        verifyThat(enrlDetails.getPaymentDetails().getMethod(), hasDefault());
        verifyThat(enrlDetails.getReferringMember(), hasText(NOT_AVAILABLE));
        verifyThat(enrlDetails.getRenewalDate(), hasDefault());
        verifyThat(enrlDetails.getEnrollDate(), hasText(DateUtils.getFormattedDate()));
        enrlDetails.getSource().verify(helper.getSourceFactory().getNoEmployeeIdSource());
    }

    @Test(dependsOnMethods = { "createEnrollmentToEMP" }, priority = 4)
    public void verifyEnrollmentAllEventsGrid()
    {
        AllEventsPage allEvents = new AllEventsPage();
        allEvents.goTo();
        allEvents.getGrid().getRow(1).verify(AllEventsRowFactory.getEnrollEvent(Program.EMP));
    }

    @Test(dependsOnMethods = { "createEnrollmentToEMP", "verifyEnrollmentAllEventsGrid" })
    public void verifyEnrollmentAllEventsMembershipDetailsTab()
    {
        AllEventsPage allEvents = new AllEventsPage();
        allEvents.goTo();
        AllEventsGridRow row = allEvents.getGrid().getRow(1);
        MembershipGridRowContainer rowCont = row.expand(MembershipGridRowContainer.class);
        rowCont.getDetails().clickAndWait();

        MembershipDetailsPopUp popUp = new MembershipDetailsPopUp();
        MembershipDetails details = popUp.getMembershipDetailsTab().getMembershipDetails();
        verifyThat(details, displayed(true));
        details.getSource().verify(helper.getSourceFactory().getNoEmployeeIdSource());
    }

    @Test(dependsOnMethods = { "verifyEnrollmentAllEventsMembershipDetailsTab" })
    public void verifyEnrollmentAllEventsEarningDetailsTab()
    {
        MembershipDetailsPopUp popUp = new MembershipDetailsPopUp();
        EventEarningDetailsTab earningDetailsTab = popUp.getEarningDetailsTab();
        earningDetailsTab.goTo();
        verifyThat(earningDetailsTab.getGrid(), size(0));
        earningDetailsTab.verifyBasicDetailsNoPointsAndMiles();
    }

    @Test(dependsOnMethods = { "verifyEnrollmentAllEventsEarningDetailsTab" })
    public void verifyEnrollmentAllEventsBillingDetailsTab()
    {
        MembershipDetailsPopUp popUp = new MembershipDetailsPopUp();
        EventBillingDetailsTab billingDetailsTab = popUp.getBillingDetailsTab();
        billingDetailsTab.goTo();
        billingDetailsTab.verifyTabIsEmpty(ENROLL);
        popUp.clickClose();
    }

    @Test(dependsOnMethods = { "verifyEnrollmentAllEventsBillingDetailsTab" })
    public void verifyCustomerInformationPanel()
    {
        CustomerInfoPanel custPanel = new CustomerInfoPanel();
        custPanel.verify(member);
        GuestAddress guestAddr = member.getPreferredAddress();
        Component panelAddr = custPanel.getAddress();

        custPanel.verify(member);
        verifyThat(panelAddr, hasText(guestAddr.getFormattedAddress()));
    }

    @Test(dependsOnMethods = { "verifyCustomerInformationPanel" })
    public void verifyImpossibleToEnroll()
    {
        enrollmentPage.goTo();
        EnrollProgramContainer progsSeparator = enrollmentPage.getPrograms();

        ArrayList<Program> programs = Program.getProgramList();
        programs.remove(Program.EMP);
        for (Program pr : programs)
        {
            verifyThat(progsSeparator.getProgramCheckbox(pr), displayed(false));
        }
    }

    @Test(dependsOnMethods = { "verifyImpossibleToEnroll" }, groups = { "karma" })
    public void verifyImpossibleToKarmaEnroll()
    {
        verifyThat(enrollmentPage.getPrograms().getProgramCheckbox(Program.KAR), displayed(false));
    }
}
