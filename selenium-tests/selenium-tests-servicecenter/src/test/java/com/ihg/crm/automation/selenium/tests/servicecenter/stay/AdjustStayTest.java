package com.ihg.crm.automation.selenium.tests.servicecenter.stay;

import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyNoError;
import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifySuccess;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.history.EventAuditConstant.CREATED;
import static com.ihg.automation.selenium.common.history.EventAuditConstant.UPDATE;
import static com.ihg.automation.selenium.common.history.EventAuditConstant.ZERO_VALUE;
import static com.ihg.automation.selenium.common.history.EventAuditConstant.Stay.AVERAGE_ROOM_RATE;
import static com.ihg.automation.selenium.common.history.EventAuditConstant.Stay.CHECK_IN;
import static com.ihg.automation.selenium.common.history.EventAuditConstant.Stay.CHECK_OUT;
import static com.ihg.automation.selenium.common.history.EventAuditConstant.Stay.EVENT_TYPE;
import static com.ihg.automation.selenium.common.history.EventAuditConstant.Stay.NIGHTS;
import static com.ihg.automation.selenium.common.history.EventAuditConstant.Stay.QUALIFYING_NIGHTS;
import static com.ihg.automation.selenium.common.history.EventAuditConstant.Stay.RATE_CODE;
import static com.ihg.automation.selenium.common.history.EventAuditConstant.Stay.ROOM_TYPE;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasTextAsDouble;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasTextAsInt;
import static com.ihg.automation.selenium.matchers.component.GridSize.size;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static com.ihg.automation.selenium.matchers.text.ContainsIgnoreCase.containsIgnoreCase;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.history.EventAuditConstant;
import com.ihg.automation.selenium.common.history.EventHistoryGrid;
import com.ihg.automation.selenium.common.history.EventHistoryGridRow;
import com.ihg.automation.selenium.common.history.EventHistoryRecords;
import com.ihg.automation.selenium.common.history.EventHistoryTab;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.pages.PaymentType;
import com.ihg.automation.selenium.common.stay.Revenue;
import com.ihg.automation.selenium.common.stay.Stay;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.servicecenter.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.servicecenter.pages.event.stay.AdjustStayPopUp;
import com.ihg.automation.selenium.servicecenter.pages.event.stay.StayDetailsAdjust;
import com.ihg.automation.selenium.servicecenter.pages.event.stay.StayEventsPage;
import com.ihg.automation.selenium.servicecenter.pages.event.stay.StayGuestGridRow;
import com.ihg.crm.automation.selenium.tests.servicecenter.LoginLogout;

public class AdjustStayTest extends LoginLogout
{
    private Stay adjustStay;
    private Member member = new Member();
    private StayEventsPage stayPage = new StayEventsPage();
    private Stay createStay = new Stay();
    private Revenue meeting;
    private String stayEventType;

    @BeforeClass
    public void beforeClass()
    {
        member.setPersonalInfo(MemberPopulateHelper.getSimplePersonalInfo());
        member.addAddress(MemberPopulateHelper.getUnitedStatesAddress());
        member.addProgram(Program.RC);

        createStay.setHotelCode("ABQMB");
        createStay.setCheckInOutByNights(2);
        createStay.setAvgRoomRate(100.00);
        createStay.setRateCode("Test");

        adjustStay = createStay.clone();
        adjustStay.setStayEventType("ADJUSTED STAY");
        adjustStay.setCheckInOutByNights(1, createStay.getNights());
        adjustStay.setQualifyingNights(1);
        adjustStay.setAvgRoomRate(110.00);
        adjustStay.setRateCode("TEST2");
        adjustStay.setRoomType("SNGL");
        adjustStay.setPaymentType(PaymentType.CASH);
        adjustStay.getBookingDetails().setBookingSource("TEST");
        adjustStay.setIhgUnits(null); // skip units verification

        meeting = adjustStay.getRevenues().getMeeting();
        meeting.setAmount(100.00);
        meeting.setUsdAmount(100.00);
        meeting.setQualifying(false);
        meeting.setReasonCode("Adjust Revenue");

        Revenue nonQualifying = adjustStay.getRevenues().getTotalNonQualifying();
        nonQualifying.setAmount(100.00);
        nonQualifying.setUsdAmount(100.00);

        login();

        new EnrollmentPage().enroll(member);

        stayPage.goTo();
        stayPage.createStay(createStay);
    }

    @Test
    public void adjustStay()
    {
        AdjustStayPopUp adjustStayPopUp = stayPage.getGuestGrid().getRow(1).openAdjustPopUp();

        StayDetailsAdjust stayDetails = adjustStayPopUp.getStayDetailsTab().getStayDetails();
        stayDetails.getStayInfo().populate(adjustStay);
        stayDetails.getBookingInfo().populate(adjustStay);
        stayDetails.getRevenueDetails().getMeeting().populate(adjustStay.getRevenues().getMeeting());

        adjustStayPopUp.clickSaveChanges(verifyNoError(), verifySuccess(StayEventsPage.STAY_SUCCESS_ADJUSTED));
        stayPage.waitUntilFirstStayProcess();
    }

    @Test(dependsOnMethods = { "adjustStay" })
    public void verifyAdjustStay()
    {
        StayGuestGridRow row = stayPage.getGuestGrid().getRow(1);
        row.verify(adjustStay);
        row.getDetails().verify(adjustStay, helper);
    }

    @Test(dependsOnMethods = { "verifyAdjustStay" }, alwaysRun = true)
    public void verifyStayHistoryAfterAdjustStay()
    {
        stayPage.getGuestGrid().getRow(1).getDetails().clickDetails();

        AdjustStayPopUp adjustStayPopUp = new AdjustStayPopUp();
        EventHistoryTab historyTab = adjustStayPopUp.getHistoryTab();
        historyTab.goTo();

        EventHistoryGrid grid = historyTab.getGrid();
        verifyThat(grid, size(2));
        EventHistoryGridRow historyRow = grid.getRow(1);
        historyRow.verify(UPDATE, helper.getUser().getNameFull());

        EventHistoryRecords stayRecords = historyRow.getAllRecords();
        stayRecords.getRecord(AVERAGE_ROOM_RATE).verify(hasTextAsDouble(createStay.getAvgRoomRate()),
                hasTextAsDouble(adjustStay.getAvgRoomRate()));
        stayRecords.getRecord(EventAuditConstant.Stay.Booking.SOURCE)
                .verifyAdd(adjustStay.getBookingDetails().getBookingSource());
        stayRecords.getRecord(CHECK_IN).verify(createStay.getCheckIn(), adjustStay.getCheckIn());
        stayRecords.getRecord(CHECK_OUT).verify(createStay.getCheckOut(), adjustStay.getCheckOut());
        stayRecords.getRecord(EVENT_TYPE).verify(CREATED, stayEventType);
        stayRecords.getStayAmountRecord(EventAuditConstant.Stay.Revenue.MEETING).verify(hasTextAsDouble(ZERO_VALUE),
                hasTextAsDouble(meeting.getAmount()));
        stayRecords.getRecord(NIGHTS).verify(hasTextAsInt(createStay.getNights()),
                hasTextAsInt(adjustStay.getNights()));
        stayRecords.getRecord(EventAuditConstant.Stay.Booking.PAYMENT_TYPE_CODE).verifyAdd(PaymentType.CASH.getCode());
        stayRecords.getRecord(QUALIFYING_NIGHTS).verify(hasTextAsInt(2),
                hasTextAsInt(adjustStay.getQualifyingNights()));
        stayRecords.getRecord(RATE_CODE).verify(hasText(containsIgnoreCase(createStay.getRateCode())),
                hasText(containsIgnoreCase(adjustStay.getRateCode())));
        stayRecords.getRecord(ROOM_TYPE).verifyAdd(adjustStay.getRoomType());
        stayRecords.getStayAmountRecord(EventAuditConstant.Stay.Revenue.TOTAL_ROOM).verify(
                hasTextAsDouble(createStay.getTotalRoomAmount()), hasTextAsDouble(adjustStay.getTotalRoomAmount()));
    }
}
