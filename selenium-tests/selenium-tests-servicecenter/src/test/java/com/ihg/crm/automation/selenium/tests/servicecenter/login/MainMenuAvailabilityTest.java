package com.ihg.crm.automation.selenium.tests.servicecenter.login;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.isDisplayedAndEnabled;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.servicecenter.pages.TopMenu;
import com.ihg.crm.automation.selenium.tests.servicecenter.LoginLogout;

public class MainMenuAvailabilityTest extends LoginLogout
{
    @BeforeClass
    public void before()
    {
        login();
    }

    @Test
    public void mainMenuAvailable()
    {
        TopMenu topMenu = new TopMenu();

        verifyThat(topMenu.getHotelOperations(), isDisplayedAndEnabled(true));
        verifyThat(topMenu.getCatalog(), isDisplayedAndEnabled(true));
        verifyThat(topMenu.getEnrollment(), isDisplayedAndEnabled(true));
        verifyThat(topMenu.getVoucher(), isDisplayedAndEnabled(true));
        verifyThat(topMenu.getOffer(), isDisplayedAndEnabled(true));
    }
}
