package com.ihg.crm.automation.selenium.tests.servicecenter.preferences;

import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyNoError;
import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifySuccess;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.preferences.TravelProfileType.BUSINESS_REWARDS;
import static com.ihg.automation.selenium.common.preferences.TravelProfileType.LEISURE;
import static com.ihg.automation.selenium.common.types.program.Program.BR;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.enabled;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasDefault;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasText;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.isDisplayedWithWait;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.isHighlightedAsInvalid;
import static com.ihg.automation.selenium.matchers.text.EntityMatcherFactory.isValue;
import static com.ihg.automation.selenium.servicecenter.pages.history.AuditConstant.TravelProfile.DEFAULT;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.gwt.components.dialog.ConfirmDialog;
import com.ihg.automation.selenium.gwt.components.input.Input;
import com.ihg.automation.selenium.matchers.component.HasText;
import com.ihg.automation.selenium.servicecenter.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.servicecenter.pages.history.ProfileHistoryGridRow;
import com.ihg.automation.selenium.servicecenter.pages.history.ProfileHistoryPage;
import com.ihg.automation.selenium.servicecenter.pages.preferences.EditTravelProfilePopUp;
import com.ihg.automation.selenium.servicecenter.pages.preferences.StayPreferencesPage;
import com.ihg.automation.selenium.servicecenter.pages.preferences.TravelProfile;
import com.ihg.automation.selenium.servicecenter.pages.preferences.TravelProfileGridRow;
import com.ihg.automation.selenium.servicecenter.pages.preferences.TravelProfilePopUpBase;
import com.ihg.crm.automation.selenium.tests.servicecenter.LoginLogout;

public class VerificationBRTravelProfileTest extends LoginLogout
{
    private StayPreferencesPage stayPreferencesPage = new StayPreferencesPage();
    private EditTravelProfilePopUp editTravelProfilePopUp = new EditTravelProfilePopUp();
    private static final String VALID_IATA = "10229";
    private static final String INVALID_IATA = "12345";

    @BeforeClass
    public void beforeClass()
    {
        Member member = new Member();
        member.addProgram(Program.RC);
        member.addAddress(MemberPopulateHelper.getUnitedStatesAddress());
        member.setPersonalInfo(MemberPopulateHelper.getSimplePersonalInfo());

        login();
        EnrollmentPage enrollmentPage = new EnrollmentPage();
        enrollmentPage.enroll(member);

        // add separate enroll to BR for simplifying of the history verification
        enrollmentPage.enrollToAnotherProgram(member, BR);
    }

    @Test
    public void verifyHistoryForBRAndDefaultTravelProfile()
    {
        ProfileHistoryPage historyPage = new ProfileHistoryPage();
        historyPage.goTo();
        verifyNoError();

        ProfileHistoryGridRow defaultTravelProfileRow = historyPage.getProfileHistoryGrid().getRow(2)
                .getSubRowByName(com.ihg.automation.selenium.servicecenter.pages.history.AuditConstant.TravelProfile.TRAVEL_PROFILE);

        defaultTravelProfileRow.getSubRowByName(DEFAULT).verifyAddAction("Yes");
        defaultTravelProfileRow.getSubRowByName(com.ihg.automation.selenium.servicecenter.pages.history.AuditConstant.TravelProfile.TYPE).verifyAddAction(hasText(isValue(LEISURE)));
        defaultTravelProfileRow.getSubRowByName(com.ihg.automation.selenium.servicecenter.pages.history.AuditConstant.TravelProfile.NAME).verifyAddAction(hasText(isValue(LEISURE)));

        ProfileHistoryGridRow brTravelProfileRow = historyPage.getProfileHistoryGrid().getRow(1)
                .getSubRowByName(com.ihg.automation.selenium.servicecenter.pages.history.AuditConstant.TravelProfile.TRAVEL_PROFILE);

        brTravelProfileRow.getSubRowByName(DEFAULT).verifyAddAction("No");
        brTravelProfileRow.getSubRowByName(com.ihg.automation.selenium.servicecenter.pages.history.AuditConstant.TravelProfile.TYPE).verifyAddAction(hasText(isValue(BUSINESS_REWARDS)));
        brTravelProfileRow.getSubRowByName(com.ihg.automation.selenium.servicecenter.pages.history.AuditConstant.TravelProfile.NAME).verifyAddAction(hasText(isValue(BUSINESS_REWARDS)));
    }

    @Test(dependsOnMethods = { "verifyHistoryForBRAndDefaultTravelProfile" }, alwaysRun = true)
    public void verifyStayPreferencesTabForBusinessRewardMember()
    {
        TravelProfile travelProfile = new TravelProfile(BUSINESS_REWARDS.getValue(), BUSINESS_REWARDS);

        stayPreferencesPage.goTo();
        TravelProfileGridRow row = stayPreferencesPage.getTravelProfilesGrid()
                .getRowByName(BUSINESS_REWARDS.getValue());
        row.verify(travelProfile);
    }

    @Test(dependsOnMethods = { "verifyStayPreferencesTabForBusinessRewardMember" }, alwaysRun = true)
    public void verifyBusinessRewardTravelProfile()
    {
        TravelProfileGridRow row = stayPreferencesPage.getTravelProfilesGrid()
                .getRowByName(BUSINESS_REWARDS.getValue());
        editTravelProfilePopUp = row.openTravelProfile();

        verifyThat(editTravelProfilePopUp.getTravelProfileName(), enabled(false));
        verifyThat(editTravelProfilePopUp.getType(), enabled(false));
        verifyThat(editTravelProfilePopUp.getDefaultCheckbox(), enabled(false));
        editTravelProfilePopUp.clickClose(verifyNoError());
    }

    @Test(dependsOnMethods = { "verifyBusinessRewardTravelProfile" }, alwaysRun = true)
    public void addValidIataNumber()
    {
        TravelProfileGridRow row = stayPreferencesPage.getTravelProfilesGrid()
                .getRowByName(BUSINESS_REWARDS.getValue());
        editTravelProfilePopUp = row.openTravelProfile();
        row = stayPreferencesPage.getTravelProfilesGrid().getRowByName(BUSINESS_REWARDS.getValue());

        Input iataNumber = editTravelProfilePopUp.getIataNumber();
        iataNumber.type(VALID_IATA);
        editTravelProfilePopUp.clickSave(verifyNoError(), verifySuccess(TravelProfilePopUpBase.SUCCESS_MESSAGE_UPDATE));

        editTravelProfilePopUp = row.openTravelProfile();
        verifyThat(iataNumber, hasText(VALID_IATA));
        editTravelProfilePopUp.close();
    }

    @Test(dependsOnMethods = { "addValidIataNumber" }, alwaysRun = true)
    public void verifyHistoryAfterAddingIataNumber()
    {
        ProfileHistoryPage historyPage = new ProfileHistoryPage();
        historyPage.goTo();
        verifyNoError();

        ProfileHistoryGridRow brTravelProfileRow = historyPage.getProfileHistoryGrid().getRow(1)
                .getSubRowByName(com.ihg.automation.selenium.servicecenter.pages.history.AuditConstant.TravelProfile.TRAVEL_PROFILE);

        brTravelProfileRow.getSubRowByName(com.ihg.automation.selenium.servicecenter.pages.history.AuditConstant.TravelProfile.IATA_NUMBER).verifyAddAction(VALID_IATA);
    }

    @Test(dependsOnMethods = { "verifyHistoryAfterAddingIataNumber" }, alwaysRun = true)
    public void removeIata()
    {
        TravelProfileGridRow row = stayPreferencesPage.getTravelProfilesGrid()
                .getRowByName(BUSINESS_REWARDS.getValue());
        stayPreferencesPage.goTo();
        editTravelProfilePopUp = row.openTravelProfile();
        row = stayPreferencesPage.getTravelProfilesGrid().getRowByName(BUSINESS_REWARDS.getValue());

        Input iataNumber = editTravelProfilePopUp.getIataNumber();
        verifyThat(iataNumber, hasText(VALID_IATA));
        iataNumber.clear();
        editTravelProfilePopUp.clickSave(verifyNoError(), verifySuccess(TravelProfilePopUpBase.SUCCESS_MESSAGE_UPDATE));

        editTravelProfilePopUp = row.openTravelProfile();
        verifyThat(iataNumber, hasDefault());
        editTravelProfilePopUp.close();
    }

    @Test(dependsOnMethods = { "removeIata" }, alwaysRun = true)
    public void verifyHistoryAfterRemovingIataNumber()
    {
        ProfileHistoryPage historyPage = new ProfileHistoryPage();
        historyPage.goTo();
        verifyNoError();

        ProfileHistoryGridRow brTravelProfileRow = historyPage.getProfileHistoryGrid().getRow(1)
                .getSubRowByName(com.ihg.automation.selenium.servicecenter.pages.history.AuditConstant.TravelProfile.TRAVEL_PROFILE);

        brTravelProfileRow.getSubRowByName(com.ihg.automation.selenium.servicecenter.pages.history.AuditConstant.TravelProfile.IATA_NUMBER).verifyDeleteAction(VALID_IATA);
    }

    @Test(dependsOnMethods = { "verifyHistoryAfterRemovingIataNumber" }, alwaysRun = true)
    public void tryToAddInvalidIata()
    {
        TravelProfileGridRow row = stayPreferencesPage.getTravelProfilesGrid()
                .getRowByName(BUSINESS_REWARDS.getValue());

        stayPreferencesPage.goTo();
        editTravelProfilePopUp = row.openTravelProfile();
        Input iataNumber = editTravelProfilePopUp.getIataNumber();
        iataNumber.type(INVALID_IATA);
        editTravelProfilePopUp.clickSave();
        verifyThat(iataNumber, isHighlightedAsInvalid(true));
        verifyThat(editTravelProfilePopUp, displayed(true));
    }

    @Test(dependsOnMethods = { "tryToAddInvalidIata" }, alwaysRun = true)
    public void verifyConfirmDialogClickNo()
    {
        editTravelProfilePopUp.clickClose(verifyNoError());
        ConfirmDialog dialog = new ConfirmDialog();
        verifyThat(dialog, isDisplayedWithWait());
        verifyThat(dialog, HasText.hasText(TravelProfilePopUpBase.ABANDON_MESSAGE));
        dialog.clickNo(verifyNoError());
        verifyThat(dialog, displayed(false));
        verifyThat(editTravelProfilePopUp, displayed(true));
    }

    @Test(dependsOnMethods = { "verifyConfirmDialogClickNo" })
    public void verifyConfirmDialogClickYes()
    {
        editTravelProfilePopUp.clickClose(verifyNoError());
        ConfirmDialog dialogNext = new ConfirmDialog();
        verifyThat(dialogNext, isDisplayedWithWait());
        verifyThat(dialogNext, HasText.hasText(TravelProfilePopUpBase.ABANDON_MESSAGE));
        dialogNext.clickYes(verifyNoError());
        verifyThat(dialogNext, displayed(false));
        verifyThat(editTravelProfilePopUp, displayed(false));
    }
}
