package com.ihg.crm.automation.selenium.tests.servicecenter.copperfield;

import static com.ihg.crm.automation.selenium.tests.servicecenter.copperfield.VerificationDatesUtils.verifyDatesWithShiftedExpiration;

import org.joda.time.LocalDate;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.ihg.automation.common.DateUtils;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.servicecenter.pages.LeftPanel;
import com.ihg.automation.selenium.servicecenter.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.servicecenter.pages.unit.GiftUnitsPopUp;
import com.ihg.automation.selenium.servicecenter.pages.unit.GoodwillPopUp;
import com.ihg.automation.selenium.servicecenter.pages.unit.TransferUnitsPopUp;
import com.ihg.crm.automation.selenium.tests.servicecenter.LoginLogout;

@Test(groups = { "copperfield" })
public abstract class GivePointAnotherMemberBase extends LoginLogout
{
    private LocalDate originatorLastActivityDate, recipientLastActivityDate;

    private Member originator, recipient;

    private final static String UPDATE_EXPIRATION_DATE = "UPDATE DGST.MBRSHP SET" //
            + " LST_ACTV_DT = TRUNC(SYSDATE -1),"//
            + " PT_BAL_EXPR_DT = TRUNC(ADD_MONTHS(SYSDATE,12)-1),"//
            + " LST_UPDT_USR_ID='%2$s',"//
            + " LST_UPDT_TS = SYSDATE"//
            + " WHERE MBRSHP_ID='%1$s' AND LYTY_PGM_CD='PC'";

    @BeforeClass
    public void beforeClass()
    {
        originator = new Member();
        originator.setPersonalInfo(MemberPopulateHelper.getSimplePersonalInfo());
        originator.addAddress(MemberPopulateHelper.getUnitedStatesAddress());
        originator.addProgram(Program.RC);

        recipient = new Member();
        recipient.setPersonalInfo(MemberPopulateHelper.getSimplePersonalInfo());
        recipient.addAddress(MemberPopulateHelper.getUnitedStatesAddress());
        recipient.addProgram(Program.RC);

        login();

        new EnrollmentPage().enroll(originator);
        // add initial points balance
        new GoodwillPopUp().goodwillPoints("2000");
        postEnrollActionsForOriginator();

        new LeftPanel().goToNewSearch();
        new EnrollmentPage().enroll(recipient);

    }

    protected abstract void postEnrollActionsForOriginator();

    protected abstract void verifyDatesForRecipient(LocalDate localDate);

    protected abstract void verifyDatesForOriginator(LocalDate localDate);

    @BeforeMethod
    public void upgradeDatesInDb()
    {
        jdbcTemplateUpdate.update(String.format(UPDATE_EXPIRATION_DATE, recipient.getRCProgramId(), lastUpdateUserId));
        jdbcTemplateUpdate.update(String.format(UPDATE_EXPIRATION_DATE, originator.getRCProgramId(), lastUpdateUserId));

        originatorLastActivityDate = DateUtils.now().minusDays(1);
        recipientLastActivityDate = DateUtils.now().minusDays(1);

        new LeftPanel().reOpenMemberProfile(originator);
    }

    @Test(description = "After purchasing gift originator's balance isn't change,"
            + " hence balance expiration data remains the same")
    public void purchaseGift()
    {
        new GiftUnitsPopUp().giftPoints(recipient, "1000");

        recipientLastActivityDate = DateUtils.now();

        verifyDatesWithShiftedExpiration(originatorLastActivityDate);
        new LeftPanel().reOpenMemberProfile(recipient);

        verifyDatesWithShiftedExpiration(recipientLastActivityDate);
    }

    @Test
    public void transferUnits()
    {
        new TransferUnitsPopUp().transferPoints(recipient, "1000");

        originatorLastActivityDate = DateUtils.now();
        recipientLastActivityDate = DateUtils.now();

        verifyDatesForOriginator(originatorLastActivityDate);

        new LeftPanel().reOpenMemberProfile(recipient);

        verifyDatesForRecipient(recipientLastActivityDate);
    }

}
