package com.ihg.crm.automation.selenium.tests.servicecenter.preferences;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.enabled;
import static com.ihg.automation.selenium.matchers.component.GridSize.size;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.preferences.TravelProfileType;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.servicecenter.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.servicecenter.pages.preferences.StayPreferencesPage;
import com.ihg.automation.selenium.servicecenter.pages.preferences.TravelProfileGrid;
import com.ihg.automation.selenium.servicecenter.pages.preferences.TravelProfileGridRow;
import com.ihg.crm.automation.selenium.tests.servicecenter.LoginLogout;

public class AddBRTravelProfileOnMaxTravelProfilesTest extends LoginLogout
{
    private Member member = new Member();
    private StayPreferencesPage stayPreferencesPage = new StayPreferencesPage();

    @BeforeClass
    public void beforeClass()
    {
        member.addProgram(Program.RC);
        member.addAddress(MemberPopulateHelper.getUnitedStatesAddress());
        member.setPersonalInfo(MemberPopulateHelper.getSimplePersonalInfo());

        login();
        new EnrollmentPage().enroll(member);
    }

    @Test
    public void addMaximumNumberOfTravelProfiles()
    {
        stayPreferencesPage.goTo();
        stayPreferencesPage.addMaxNumberOfTravelProfiles();
    }

    @Test(dependsOnMethods = { "addMaximumNumberOfTravelProfiles" }, alwaysRun = true)
    public void enrollToBusinessRewardsProgram()
    {
        new EnrollmentPage().enrollToAnotherProgram(member, Program.BR);

        stayPreferencesPage.goTo();
        TravelProfileGrid grid = new StayPreferencesPage().getTravelProfilesGrid();
        TravelProfileGridRow row = stayPreferencesPage.getTravelProfilesGrid()
                .getRowByName(TravelProfileType.BUSINESS_REWARDS.getValue());
        verifyThat(row, displayed(true));
        verifyThat(grid, size(6));
        verifyThat(stayPreferencesPage.getAddTravelProfile(), enabled(false));
    }
}
