package com.ihg.crm.automation.selenium.tests.servicecenter.enrollment;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.components.Country;
import com.ihg.automation.selenium.common.contact.Address;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.servicecenter.pages.enroll.EnrollmentPage;
import com.ihg.crm.automation.selenium.tests.servicecenter.LoginLogout;

public class AutoPopulateCityStateTest extends LoginLogout
{

    @BeforeClass
    public void before()
    {
        login();
    }

    @Test
    public void autoPopulateCityState()
    {
        EnrollmentPage enrollmentPage = new EnrollmentPage();
        enrollmentPage.goTo();
        enrollmentPage.getPrograms().selectProgram(Program.RC);

        Address address = enrollmentPage.getAddress();
        address.setAddressConfiguration(Country.US);
        address.getCountry().select(Country.US);
        address.getZipCode().typeAndWait("10010");
        address.getAddress1().type("1 MADISON AVE");

        verifyThat(address.getLocality1(), hasText("NEW YORK"));
        verifyThat(address.getRegion1(), hasText("New York"));
    }
}
