package com.ihg.crm.automation.selenium.tests.servicecenter.enrollment;

import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyNoError;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyNoErrors;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.Constant.NOT_AVAILABLE;
import static com.ihg.automation.selenium.common.Constant.RC_POINTS;
import static com.ihg.automation.selenium.common.event.EventTransactionType.Membership.ENROLL;
import static com.ihg.automation.selenium.matchers.component.GridSize.size;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static com.ihg.automation.selenium.matchers.component.ViewableMatcher.hasTextInView;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.ProgramInfoPanel;
import com.ihg.automation.selenium.common.billing.EventBillingDetailsTab;
import com.ihg.automation.selenium.common.earning.EventEarningDetailsTab;
import com.ihg.automation.selenium.common.event.EarningEvent;
import com.ihg.automation.selenium.common.member.AlliancePopulateHelper;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberAlliance;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.types.Alliance;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.servicecenter.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsGrid;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsGridRow;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsPage;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsSearch;
import com.ihg.automation.selenium.servicecenter.pages.event.enroll.MembershipDetailsPopUp;
import com.ihg.automation.selenium.servicecenter.pages.event.enroll.MembershipGridRowContainer;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.AllianceGrid;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.RewardClubPage;
import com.ihg.crm.automation.selenium.tests.servicecenter.LoginLogout;

public class EarningPreferenceIsMilesTest extends LoginLogout
{
    private EnrollmentPage enrollmentPage = new EnrollmentPage();
    private RewardClubPage rewardClubPage = new RewardClubPage();
    private AllEventsPage allEventsPage = new AllEventsPage();
    private AllEventsGrid allEventsGrid;
    private MembershipDetailsPopUp membershipDetails = new MembershipDetailsPopUp();

    private Member member = new Member();

    private Alliance alliance = Alliance.DJA;
    private String allianceNumber = AlliancePopulateHelper.getRandomDJA().getNumber();
    private MemberAlliance memberAlliance;
    private EarningEvent event = new EarningEvent("PC", "0", "0", RC_POINTS);

    @BeforeClass
    public void before()
    {
        member.addProgram(Program.RC);
        member.setPersonalInfo(MemberPopulateHelper.getSimplePersonalInfo());
        member.addAddress(MemberPopulateHelper.getUnitedStatesAddress());
        memberAlliance = new MemberAlliance(alliance, allianceNumber, member.getName());
        member.setMilesEarningPreference(memberAlliance);

        login();
    }

    @Test
    public void enrollWithEarningPreference()
    {
        enrollmentPage.enroll(member);

        verifyThat(new ProgramInfoPanel().getPrograms().getRewardClubProgram().getEarningPreference(),
                hasText(alliance.getCode()));
    }

    @Test(dependsOnMethods = { "enrollWithEarningPreference" }, alwaysRun = true)
    public void verifyEnrollEvent()
    {
        allEventsPage.goTo();
        verifyNoErrors();

        AllEventsSearch allEventsSearch = allEventsPage.getSearchFields();
        allEventsSearch.getEventType().select("Membership");
        allEventsSearch.getTransactionType().select(ENROLL);
        allEventsPage.getButtonBar().clickSearch();

        allEventsGrid = allEventsPage.getGrid();
        verifyThat(allEventsGrid, size(1));

        AllEventsGridRow row = allEventsGrid.getRow(1);

        row.expand(MembershipGridRowContainer.class).getMembershipDetails().verifyScSource(helper);
    }

    @Test(dependsOnMethods = { "verifyEnrollEvent" }, alwaysRun = true)
    public void verifyMembershipDetailsTab()
    {
        AllEventsGridRow row = allEventsPage.getGrid().getRow(1);
        MembershipGridRowContainer rowCont = row.expand(MembershipGridRowContainer.class);
        rowCont.getDetails().clickAndWait(verifyNoError());

        membershipDetails.getMembershipDetailsTab().getMembershipDetails().verifyScSource(helper);
    }

    @Test(dependsOnMethods = { "verifyMembershipDetailsTab" }, alwaysRun = true)
    public void verifyEarningDetailsTab()
    {
        EventEarningDetailsTab earningDetails = membershipDetails.getEarningDetailsTab();
        earningDetails.goTo();

        earningDetails.verifyBasicDetailsForMiles(alliance, NOT_AVAILABLE);
        earningDetails.getGrid().verify(event);
    }

    @Test(dependsOnMethods = { "verifyEarningDetailsTab" }, alwaysRun = true)
    public void verifyBillingDetailsTab()
    {
        EventBillingDetailsTab billing = membershipDetails.getBillingDetailsTab();
        billing.goTo();
        billing.verifyTabIsEmpty(ENROLL);

        membershipDetails.clickClose();
    }

    @Test(dependsOnMethods = { "verifyBillingDetailsTab" }, alwaysRun = true)
    public void verifyAllianceDetailsInProgramInformation()
    {
        rewardClubPage.goTo();

        AllianceGrid allianceGrid = rewardClubPage.getAlliances();
        verifyThat(allEventsGrid, size(1));
        allianceGrid.getRow(1).verify(memberAlliance);

        verifyThat(rewardClubPage.getEarningDetails().getEarningPreference(), hasTextInView(alliance.getValue()));
    }

    @Test(dependsOnMethods = { "verifyAllianceDetailsInProgramInformation" }, alwaysRun = true)
    public void removeAlliance()
    {
        rewardClubPage.removeAlliance(alliance);
    }
}
