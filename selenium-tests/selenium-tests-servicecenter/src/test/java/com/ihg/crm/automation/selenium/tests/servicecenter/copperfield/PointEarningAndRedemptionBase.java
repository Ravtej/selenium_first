package com.ihg.crm.automation.selenium.tests.servicecenter.copperfield;

import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyNoError;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.Constant;
import com.ihg.automation.selenium.common.deposit.Deposit;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.stay.Stay;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.servicecenter.pages.catalog.CatalogPage;
import com.ihg.automation.selenium.servicecenter.pages.deposit.CreateDepositPopUp;
import com.ihg.automation.selenium.servicecenter.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.servicecenter.pages.event.stay.AdjustStayPopUp;
import com.ihg.automation.selenium.servicecenter.pages.event.stay.StayEventsPage;
import com.ihg.automation.selenium.servicecenter.pages.event.stay.StayInfoEdit;
import com.ihg.automation.selenium.servicecenter.pages.unit.GoodwillPopUp;
import com.ihg.automation.selenium.servicecenter.pages.unit.PurchaseUnitsPopUp;
import com.ihg.crm.automation.selenium.tests.servicecenter.LoginLogout;

@Test(groups = { "copperfield" })
public abstract class PointEarningAndRedemptionBase extends LoginLogout
{

    private Member member = new Member();;

    private final static String UPDATE_EXPIRATION_DATE = "UPDATE DGST.MBRSHP SET" //
            + " CURRENT_BAL_UNIT_AMT = 10000,"// (if negative goodwill goes
                                              // first + for catalog item)
            + " LST_ACTV_DT = TRUNC(SYSDATE -1),"//
            + " PT_BAL_EXPR_DT = TRUNC(ADD_MONTHS(SYSDATE,12)-1),"//
            + " LST_UPDT_USR_ID='%2$s',"//
            + " LST_UPDT_TS = SYSDATE"//
            + " WHERE MBRSHP_ID='%1$s' AND LYTY_PGM_CD='PC'";

    @BeforeClass
    public void beforeClass()
    {
        member.setPersonalInfo(MemberPopulateHelper.getSimplePersonalInfo());
        member.addAddress(MemberPopulateHelper.getUnitedStatesAddress());
        member.addProgram(Program.RC);

        login();

        member = new EnrollmentPage().enroll(member);

        postEnrollActions();
    }

    protected abstract void postEnrollActions();

    protected abstract void verifyAfterPointEvent();

    @BeforeMethod
    public void upgradeDatesInDb()
    {
        jdbcTemplateUpdate.update(String.format(UPDATE_EXPIRATION_DATE, member.getRCProgramId(), lastUpdateUserId));
    }

    @Test
    public void goodwillPoints()
    {
        new GoodwillPopUp().goodwillPoints("100");

        verifyAfterPointEvent();
    }

    @Test
    public void goodwillNegativePoints()
    {
        new GoodwillPopUp().goodwillPoints("-100");

        verifyAfterPointEvent();
    }

    @Test
    public void purchasePoints()
    {
        new PurchaseUnitsPopUp().purchaseComplimentary("1000");

        verifyAfterPointEvent();
    }

    @Test
    public void createMissingStay()
    {
        Stay stay = new Stay();
        stay.setHotelCode("ATLCP");
        stay.setCheckInOutByNights(1);
        stay.setRateCode("TEST");
        stay.setAvgRoomRate(10.0);
        new StayEventsPage().createStay(stay);

        verifyAfterPointEvent();
    }

    @Test(dependsOnMethods = { "createMissingStay" })
    public void adjustMissingStay()
    {
        Stay stay = new Stay();
        stay.setAvgRoomRate(5.0);

        StayEventsPage stayEventsPage = new StayEventsPage();
        stayEventsPage.goTo();
        AdjustStayPopUp adjustStayPopUp = stayEventsPage.getGuestGrid().getRow(1).openAdjustPopUp();
        StayInfoEdit stayInfoEdit = adjustStayPopUp.getStayDetailsTab().getStayDetails().getStayInfo();
        stayInfoEdit.getAvgRoomRate().type(5.0);
        adjustStayPopUp.getSaveChanges().clickAndWait(verifyNoError());
        stayEventsPage.waitUntilFirstStayProcess();

        verifyAfterPointEvent();
    }

    @Test
    public void createDeposit()
    {
        Deposit deposit = new Deposit();
        deposit.setTransactionId("PTSPR1");
        deposit.setLoyaltyUnitsAmount("100");
        deposit.setLoyaltyUnitsType(Constant.RC_POINTS);
        new CreateDepositPopUp().createDeposit(deposit, false);

        verifyAfterPointEvent();
    }

    @Test
    public void redeemPointsByCatalogAward()
    {
        CatalogPage catalogPage = new CatalogPage();
        catalogPage.goTo();
        catalogPage.searchByItemId("CAR05");
        catalogPage.openCatalogItemDetailsPopUp().openOrderSummaryPopUp().getCheckout().clickAndWait(verifyNoError());

        verifyAfterPointEvent();
    }
}
