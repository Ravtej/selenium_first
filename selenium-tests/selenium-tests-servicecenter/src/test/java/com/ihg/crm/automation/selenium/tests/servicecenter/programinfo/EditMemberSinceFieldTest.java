package com.ihg.crm.automation.selenium.tests.servicecenter.programinfo;

import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyError;

import org.joda.time.LocalDate;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.common.DateUtils;
import com.ihg.automation.selenium.assertions.SeleniumAssertionUtils;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.gwt.components.Mode;
import com.ihg.automation.selenium.gwt.components.input.DateInput;
import com.ihg.automation.selenium.servicecenter.pages.annual.AnnualActivityPage;
import com.ihg.automation.selenium.servicecenter.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.servicecenter.pages.programs.ProgramPageBase.EnrollmentDetails;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.RewardClubPage;
import com.ihg.crm.automation.selenium.tests.servicecenter.LoginLogout;

public class EditMemberSinceFieldTest extends LoginLogout
{
    private Member member = new Member();
    private RewardClubPage rewardClubPage = new RewardClubPage();
    private EnrollmentDetails enrollmentDetails;
    private AnnualActivityPage annualActivityPage = new AnnualActivityPage();
    private DateInput memberSince;
    private final LocalDate INPUT_DATE = DateUtils.now().minusMonths(1);

    @BeforeClass
    public void enrollRCMember()
    {
        member.setPersonalInfo(MemberPopulateHelper.getSimplePersonalInfo());
        member.addAddress(MemberPopulateHelper.getUnitedStatesAddress());
        member.addProgram(Program.RC);

        login();

        member = new EnrollmentPage().enroll(member);
        rewardClubPage.goTo();
    }

    @Test
    public void verifyWarningMessageWithNotSavedDate()
    {
        enrollmentDetails = rewardClubPage.getEnrollmentDetails();
        enrollmentDetails.clickEdit();
        memberSince = enrollmentDetails.getMemberSince();
        memberSince.type(INPUT_DATE);
        annualActivityPage.getTab().click(verifyError(RewardClubPage.WARNING_MESSAGE));
        SeleniumAssertionUtils.verifyDate(memberSince, INPUT_DATE, Mode.EDIT);
        enrollmentDetails.clickCancel();
        SeleniumAssertionUtils.verifyDateInView(memberSince, DateUtils.now());
    }

    @Test(dependsOnMethods = { "verifyWarningMessageWithNotSavedDate" }, alwaysRun = true)
    public void editMemberSinceField()
    {
        enrollmentDetails.clickEdit();
        memberSince = enrollmentDetails.getMemberSince();
        memberSince.type(INPUT_DATE);
        enrollmentDetails.clickSave();
        SeleniumAssertionUtils.verifyDateInView(memberSince, INPUT_DATE);
        annualActivityPage.goTo();
    }
}
