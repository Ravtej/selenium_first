package com.ihg.crm.automation.selenium.tests.servicecenter.programinfo.dr;

import static com.ihg.automation.common.DateUtils.getStringToDate;
import static com.ihg.automation.selenium.common.event.EventTransactionType.Membership.MEMBERSHIP_OPEN;
import static com.ihg.automation.selenium.common.event.EventTransactionType.Order.ORDER_SYSTEM;
import static com.ihg.automation.selenium.common.member.DiningRewardAward.FUL_WELCOME_TO_GOLD;
import static com.ihg.automation.selenium.common.member.MemberJdbcUtils.UPDATE_EXP;
import static com.ihg.automation.selenium.common.types.program.Program.DR;
import static com.ihg.automation.selenium.common.types.program.ProgramStatus.OPEN;
import static com.ihg.automation.selenium.common.types.program.RewardClubLevel.GOLD;

import org.springframework.beans.factory.annotation.Value;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.common.DateUtils;
import com.ihg.automation.selenium.common.catalog.CatalogItem;
import com.ihg.automation.selenium.common.event.AllEventsRow;
import com.ihg.automation.selenium.common.event.AllEventsRowConverter;
import com.ihg.automation.selenium.common.member.DiningRewardAward;
import com.ihg.automation.selenium.common.member.MemberJdbcUtils;
import com.ihg.automation.selenium.common.order.Order;
import com.ihg.automation.selenium.common.order.OrderFactory;
import com.ihg.automation.selenium.common.pages.CreditCardType;
import com.ihg.automation.selenium.common.payment.CreditCardPayment;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.gwt.components.Mode;
import com.ihg.automation.selenium.servicecenter.pages.GuestSearch;
import com.ihg.automation.selenium.servicecenter.pages.Role;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsGrid;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsGridRow;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsPage;
import com.ihg.automation.selenium.servicecenter.pages.event.enroll.MembershipDetails;
import com.ihg.automation.selenium.servicecenter.pages.event.enroll.MembershipGridRowContainer;
import com.ihg.automation.selenium.servicecenter.pages.event.order.OrderSystemDetailsGridRowContainer;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.RewardClubPage;

public class ReactivateByCreditCardTest extends DRCommon
{
    @Value("${memberReactivateByCreditCard}")
    protected String memberReactivateByCreditCard;

    @Value("${memberReactivateByCreditCard.date}")
    protected String date;

    private String enrollDate;
    private static final CatalogItem ENROLLMENT_AWARD = DiningRewardAward.ENROLL_10_RMB2188_SC_2_EVOUCHER;
    private AllEventsPage allEventsPage = new AllEventsPage();
    private AllEventsGrid allEventsGrid;
    private CreditCardPayment creditCardItem = new CreditCardPayment(ENROLLMENT_AWARD, "6240008631401148",
            CreditCardType.CHINA_UNION_PAY);

    @BeforeClass
    public void beforeClass()
    {
        MemberJdbcUtils.update(jdbcTemplateUpdate, UPDATE_EXP, memberReactivateByCreditCard, DR, 13);

        enrollDate = getStringToDate(date).toString(DateUtils.DATE_FORMAT);

        login(user15U, Role.AGENT_TIER2);

        new GuestSearch().byMemberNumber(memberReactivateByCreditCard);
    }

    @Test
    public void verifyProgramSummaryDetails()
    {
        verifyProgramSummary(memberReactivateByCreditCard, 13);
    }

    @Test(dependsOnMethods = "verifyProgramSummaryDetails", alwaysRun = true)
    public void verifyRCProgramInformation()
    {
        RewardClubPage rewardClubPage = new RewardClubPage();
        rewardClubPage.goTo();
        rewardClubPage.getSummary().verify(OPEN, GOLD);
    }

    @Test(dependsOnMethods = "verifyRCProgramInformation", alwaysRun = true)
    public void verifyOrderSystemEventDetails()
    {
        allEventsPage = new AllEventsPage();
        allEventsPage.goTo();
        allEventsGrid = allEventsPage.getGrid();
        AllEventsGridRow orderSystemRow = allEventsGrid.getRow(ORDER_SYSTEM);

        Order order = OrderFactory.getSystemOrder(FUL_WELCOME_TO_GOLD, null);
        order.setTransactionDate(enrollDate);
        order.getOrderItems().get(0).setDeliveryStatus(null);
        AllEventsRow orderRowData = AllEventsRowConverter.convert(order);

        orderSystemRow.verify(orderRowData);
        orderSystemRow.expand(OrderSystemDetailsGridRowContainer.class).verifyWithShippingAsOnLeftPanel(order);
    }

    @Test(dependsOnMethods = "verifyOrderSystemEventDetails", alwaysRun = true)
    public void verifyMembershipOpenEventDetails()
    {
        AllEventsRow membershipRowData = new AllEventsRow(enrollDate, MEMBERSHIP_OPEN, Program.DR.getCode(), "", "");

        allEventsGrid = allEventsPage.getGrid();
        AllEventsGridRow membershipRenewalRow = allEventsGrid.getRow(MEMBERSHIP_OPEN);
        membershipRenewalRow.verify(membershipRowData);

        MembershipGridRowContainer container = membershipRenewalRow.expand(MembershipGridRowContainer.class);
        MembershipDetails membershipDetails = container.getMembershipDetails();
        membershipDetails.getPaymentDetails().verify(creditCardItem, Mode.VIEW);
        membershipDetails.verifyScSource(helper);
    }
}
