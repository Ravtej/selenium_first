package com.ihg.crm.automation.selenium.tests.servicecenter.offer;

import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifySuccess;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.IsDisplayed.displayed;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.offer.GuestOffer;
import com.ihg.automation.selenium.common.offer.Offer;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.gwt.components.utils.WaitUtils;
import com.ihg.automation.selenium.servicecenter.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.servicecenter.pages.event.offer.MemberOffersGrid;
import com.ihg.automation.selenium.servicecenter.pages.event.offer.OfferEventPage;
import com.ihg.automation.selenium.servicecenter.pages.event.offer.OfferPopUp;
import com.ihg.automation.selenium.servicecenter.pages.offer.OfferRegistrationPopUp;
import com.ihg.crm.automation.selenium.tests.servicecenter.LoginLogout;

public class RegisterInSecondaryOfferTest extends LoginLogout
{
    private static final String OFFER_CODE = "8530441";
    private static final String SECONDARY_OFFER_CODE = "8560440";
    private Offer offer;
    private Offer secondaryOffer;
    private Member member = new Member();
    private final static String OFFER_NAME = "SELECT LYTY_OFFER_NM FROM OFFER.LYTY_OFFER" //
            + " WHERE LYTY_OFFER_KEY=%s";

    @BeforeClass
    public void beforeClass()
    {
        member.setPersonalInfo(MemberPopulateHelper.getSimplePersonalInfo());
        member.addAddress(MemberPopulateHelper.getChinaAddress());
        member.addProgram(Program.RC);

        String offerName = jdbcTemplate.queryForMap(String.format(OFFER_NAME, OFFER_CODE)).get("LYTY_OFFER_NM")
                .toString();
        offer = Offer.builder(OFFER_CODE, offerName).build();
        secondaryOffer = Offer.builder(SECONDARY_OFFER_CODE, offerName).build();

        login();

        new EnrollmentPage().enroll(member);
    }

    @Test
    public void registerInOfferWithSecondaryOffer()
    {
        String offerName = offer.getName();
        new OfferRegistrationPopUp().registerInOffer(offer.getCode(),
                verifySuccess("Congratulations guest has been successfully registered in\n'" + offerName
                        + "'. Additional Offers were Registered '" + offerName + "'"));

        OfferPopUp offerPopUp = new OfferPopUp();
        new WaitUtils().waitExecutingRequest();
        verifyThat(offerPopUp, displayed(true));

        offerPopUp.getOfferDetailsTab().getOfferDetails().verify(offer);
        offerPopUp.close();

        MemberOffersGrid memberOffersGrid = new OfferEventPage().getMemberOffersGrid();
        memberOffersGrid.getRowByOfferCode(offer.getCode()).verify(new GuestOffer(offer));
        memberOffersGrid.getRowByOfferCode(secondaryOffer.getCode()).verify(new GuestOffer(secondaryOffer));
    }
}
