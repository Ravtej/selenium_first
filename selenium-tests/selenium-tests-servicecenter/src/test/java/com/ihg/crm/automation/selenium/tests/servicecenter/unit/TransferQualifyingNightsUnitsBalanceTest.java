package com.ihg.crm.automation.selenium.tests.servicecenter.unit;

import static com.ihg.automation.common.DateUtils.currentYear;
import static com.ihg.automation.common.DateUtils.now;
import static com.ihg.automation.selenium.assertions.Assume.assumeThat;
import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyNoError;
import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifySuccess;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.Constant.RC_POINTS;
import static com.ihg.automation.selenium.common.event.EventTransactionType.Loyalty.Q_NIGHT_TRANSFER;
import static com.ihg.automation.selenium.common.event.EventTransactionType.Loyalty.TRANSFER;
import static com.ihg.automation.selenium.common.event.EventTransactionType.Loyalty.TRANSFER_PURCHASE;
import static com.ihg.automation.selenium.common.types.Currency.USD;
import static com.ihg.automation.selenium.common.types.program.ProgramStatus.OPEN;
import static com.ihg.automation.selenium.common.types.program.RewardClubLevel.CLUB;
import static com.ihg.automation.selenium.common.types.program.RewardClubLevel.GOLD;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasTextAsInt;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasTextWithWait;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.isDisplayedWithWait;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static com.ihg.automation.selenium.matchers.date.IsAfterDate.isAfter;
import static com.ihg.automation.selenium.servicecenter.pages.annual.AnnualActivityRow.AnnualActivityCell.NON_QUALIFIED_ROOM;
import static com.ihg.automation.selenium.servicecenter.pages.annual.AnnualActivityRow.AnnualActivityCell.QUALIFIED_ROOM;
import static com.ihg.automation.selenium.servicecenter.pages.annual.AnnualActivityRow.AnnualActivityCell.REWARD_NIGHT;
import static com.ihg.automation.selenium.servicecenter.pages.annual.AnnualActivityRow.AnnualActivityCell.TIER_LEVEL_NIGHT;
import static org.hamcrest.number.OrderingComparison.greaterThan;
import static org.hamcrest.number.OrderingComparison.greaterThanOrEqualTo;

import org.joda.time.LocalDate;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.common.DateUtils;
import com.ihg.automation.selenium.common.event.AllEventsRow;
import com.ihg.automation.selenium.common.event.AllEventsRowFactory;
import com.ihg.automation.selenium.common.member.AnnualActivity;
import com.ihg.automation.selenium.common.member.LifetimeActivity;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.pages.CreditCardType;
import com.ihg.automation.selenium.common.pages.PaymentMethod;
import com.ihg.automation.selenium.common.pages.program.RewardClubTierLevelActivityGrid;
import com.ihg.automation.selenium.common.payment.CreditCardPayment;
import com.ihg.automation.selenium.common.payment.PaymentDetailsPopUp;
import com.ihg.automation.selenium.common.stay.Stay;
import com.ihg.automation.selenium.common.types.program.ProgramStatus;
import com.ihg.automation.selenium.common.types.program.RewardClubLevel;
import com.ihg.automation.selenium.gwt.components.Mode;
import com.ihg.automation.selenium.gwt.components.dialog.ConfirmDialog;
import com.ihg.automation.selenium.servicecenter.pages.LeftPanel;
import com.ihg.automation.selenium.servicecenter.pages.annual.AnnualActivityPage;
import com.ihg.automation.selenium.servicecenter.pages.annual.AnnualActivityRow;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsGridRow;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsPage;
import com.ihg.automation.selenium.servicecenter.pages.event.stay.StayEventsPage;
import com.ihg.automation.selenium.servicecenter.pages.event.unit.UnitDetails;
import com.ihg.automation.selenium.servicecenter.pages.event.unit.UnitGridRowContainer;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.RewardClubPage;
import com.ihg.automation.selenium.servicecenter.pages.unit.GoodwillPopUp;
import com.ihg.automation.selenium.servicecenter.pages.unit.TransferNightsPopUp;
import com.ihg.automation.selenium.servicecenter.pages.unit.TransferUnitsPopUp;
import com.ihg.automation.selenium.tests.common.RulesDBUtils;

public class TransferQualifyingNightsUnitsBalanceTest extends UnitsCommon
{
    private static int nightsToAchieveGold;
    private static String nights_amount_string;
    private static final int UNITS_TO_SEND_INT = 10000;
    private static final String HOTEL = "ATLCP";
    private static final double INIT_AVRG_ROOM_RATE = 100;
    private static final String GOODWILL_UNITS = "15000";
    private static final String CURRENT_YEAR = Integer.toString((DateUtils.currentYear()));
    private static final String EXPECTED_AMOUNT = "50.00";
    private static final String EXPECTED_CURRENCY = USD.getCode();

    private CreditCardPayment creditCard = new CreditCardPayment(EXPECTED_AMOUNT, EXPECTED_CURRENCY, "4966648225694538",
            CreditCardType.VISA);

    private Member originator, recipient;
    private Stay missingStay = new Stay();
    private Stay missingStay1n = new Stay();

    private AllEventsPage allEventsPage = new AllEventsPage();

    @BeforeClass
    public void beforeClass()
    {
        RulesDBUtils rulesDBUtils = new RulesDBUtils(jdbcTemplate);
        nightsToAchieveGold = rulesDBUtils.getCurrentYearRuleNights(CLUB, GOLD);
        nights_amount_string = Integer.toString(nightsToAchieveGold);

        assumeThat(now(), isAfter(new LocalDate(currentYear(), 1, 4)), "Checkout date should be in current year");

        missingStay1n.setHotelCode(HOTEL);
        missingStay1n.setAvgRoomRate(INIT_AVRG_ROOM_RATE);
        missingStay1n.setRateCode("IVANI");
        missingStay1n.setHotelCurrency(USD);
        missingStay1n.setCheckInOutByNights(1, nightsToAchieveGold + 1);
        missingStay1n.setIsQualifying(false);

        missingStay.setHotelCode(HOTEL);
        missingStay.setAvgRoomRate(INIT_AVRG_ROOM_RATE);
        missingStay.setRateCode("TEST");
        missingStay.setHotelCurrency(USD);
        missingStay.setCheckInOutByNights(nightsToAchieveGold);

        login();

        recipient = new AnnualActivityPage().captureCounters(enrollMember(), jdbcTemplate);

        new LeftPanel().goToNewSearch();

        originator = enrollMember();

        StayEventsPage eventsPage = new StayEventsPage();
        /** Qualified Units and non-Qualified Nights **/
        eventsPage.createStay(missingStay1n);
        /** Qualified Units and Nights **/
        eventsPage.createStay(missingStay);
        /** Total Units **/
        new GoodwillPopUp().goodwillPoints(GOODWILL_UNITS);

        AnnualActivityPage annualActivityPage = new AnnualActivityPage();
        annualActivityPage.captureCounters(originator, jdbcTemplate);

        // Do basic amount verification
        verifyThat(annualActivityPage.getCurrentBalance().getAmount(),
                hasTextAsInt(greaterThanOrEqualTo(UNITS_TO_SEND_INT)));

        AnnualActivityRow activityRow = annualActivityPage.getAnnualActivities().getCurrentYearRow();
        verifyThat(activityRow.getCell(NON_QUALIFIED_ROOM), hasTextAsInt(greaterThan(0)));
        verifyThat(activityRow.getCell(REWARD_NIGHT), hasTextAsInt(greaterThan(0)));
        verifyThat(activityRow.getCell(TIER_LEVEL_NIGHT), hasTextAsInt(greaterThan(nightsToAchieveGold)));
        verifyThat(activityRow.getCell(QUALIFIED_ROOM), hasText(nights_amount_string));

        RewardClubPage rewardClubPage = new RewardClubPage();
        rewardClubPage.goTo();

        rewardClubPage.getSummary().verify(OPEN, GOLD);
    }

    @Test
    public void transferAllQualifiedNights()
    {
        TransferNightsPopUp nightsPopUp = new TransferNightsPopUp();
        nightsPopUp.goTo();
        nightsPopUp.verify(CURRENT_YEAR, nights_amount_string);
        nightsPopUp.getMembershipId().typeAndWait(recipient.getRCProgramId());
        nightsPopUp.clickSubmitNoError(verifySuccess("Qualifying Nights was successfully transferred"));

        ConfirmDialog dialog = new ConfirmDialog();
        verifyThat(dialog, isDisplayedWithWait());
        verifyThat(dialog, hasText("Would you also like to transfer Units Balance?"));
        dialog.clickYes(verifyNoError());
        verifyThat(dialog, displayed(false));
    }

    @Test(dependsOnMethods = { "transferAllQualifiedNights" })
    public void transferUnits()
    {
        TransferUnitsPopUp transUnitsPopUp = new TransferUnitsPopUp();
        verifyThat(transUnitsPopUp, isDisplayedWithWait());
        transUnitsPopUp.getMembershipId().typeAndWait(recipient.getRCProgramId());
        transUnitsPopUp.getAmount().select(UNITS_TO_SEND_INT);
        transUnitsPopUp.getUnitType().select(RC_POINTS);

        verifyThat(transUnitsPopUp.getMemberName(), hasTextWithWait(recipient.getName().getNameOnPanel()));
        transUnitsPopUp.getSubmit().clickAndWait(verifyNoError());
        verifyThat(transUnitsPopUp, displayed(false));
    }

    @Test(dependsOnMethods = { "transferUnits" })
    public void submitPayment()
    {
        PaymentDetailsPopUp paymentPopUp = new PaymentDetailsPopUp();
        verifyThat(paymentPopUp, isDisplayedWithWait());
        verifyThat(paymentPopUp.getMemberName(), hasTextWithWait(recipient.getName().getNameOnPanel()));
        paymentPopUp.selectPaymentType(PaymentMethod.CREDIT_CARD);
        paymentPopUp.getCreditCardPayment().populate(creditCard);

        paymentPopUp.clickSubmitAndVerifySuccessMsg();
    }

    @Test(dependsOnMethods = { "submitPayment" }, alwaysRun = true)
    public void verifyOriginatorTransferNightsEvent()
    {
        AllEventsRow allEventsRow = AllEventsRowFactory.getNightTransfer(-nightsToAchieveGold);

        allEventsPage = new AllEventsPage();
        AllEventsGridRow row = allEventsPage.getGrid().getRow(Q_NIGHT_TRANSFER);
        row.verify(allEventsRow);

        UnitDetails unitDetails = row.expand(UnitGridRowContainer.class).getUnitDetails();
        unitDetails.verifyRelatedMemberID(recipient);
        unitDetails.verifySource(helper);
    }

    @Test(dependsOnMethods = { "verifyOriginatorTransferNightsEvent" }, alwaysRun = true)
    public void verifyOriginatorTransferPurchaseEvent()
    {
        allEventsPage = new AllEventsPage();
        AllEventsGridRow row = allEventsPage.getGrid().getRow(TRANSFER_PURCHASE);
        row.verify(AllEventsRowFactory.getPointEvent(TRANSFER_PURCHASE, -UNITS_TO_SEND_INT));

        UnitDetails details = row.expand(UnitGridRowContainer.class).getUnitDetails();
        details.verifyRelatedMemberID(recipient);
        details.getPaymentDetails().verify(creditCard, Mode.VIEW);
        details.verifySource(helper);
    }

    @Test(dependsOnMethods = { "verifyOriginatorTransferPurchaseEvent" }, alwaysRun = true)
    public void verifyOriginatorAnnualActivityPage()
    {

        LifetimeActivity lifetimeActivity = originator.getLifetimeActivity();
        lifetimeActivity.setAdjustedUnits(lifetimeActivity.getAdjustedUnits() - UNITS_TO_SEND_INT);
        lifetimeActivity.setCurrentBalance(lifetimeActivity.getCurrentBalance() - UNITS_TO_SEND_INT);

        AnnualActivity annualActivity = originator.getAnnualActivity();
        annualActivity.setTierLevelNights(annualActivity.getTierLevelNights() - nightsToAchieveGold);
        annualActivity.setQualifiedNights(0);
        annualActivity.setTotalUnits(annualActivity.getTotalUnits() - UNITS_TO_SEND_INT);

        new AnnualActivityPage().verifyCounters(originator);
    }

    @Test(dependsOnMethods = { "verifyOriginatorAnnualActivityPage" }, alwaysRun = true)
    public void verifyOriginatorRCTierLevel()
    {
        RewardClubPage rewardClubPage = new RewardClubPage();
        rewardClubPage.goTo();
        rewardClubPage.getSummary().verify(ProgramStatus.OPEN, RewardClubLevel.GOLD);
    }

    // Recipient member verification

    @Test(dependsOnMethods = { "verifyOriginatorAnnualActivityPage" }, alwaysRun = true)
    public void navigateRecipientProfile() throws InterruptedException
    {
        openRecipientProfileByLink(recipient, 2);
    }

    @Test(dependsOnMethods = { "navigateRecipientProfile" })
    public void verifyRecipientTransferNightsEvent()
    {
        AllEventsRow allEventsRow = AllEventsRowFactory.getNightTransfer(nightsToAchieveGold);

        allEventsPage = new AllEventsPage();
        allEventsPage.goTo();
        AllEventsGridRow row = allEventsPage.getGrid().getRow(Q_NIGHT_TRANSFER);
        row.verify(allEventsRow);

        UnitDetails unitDetails = row.expand(UnitGridRowContainer.class).getUnitDetails();
        unitDetails.verifyRelatedMemberID(originator);
        unitDetails.verifySource(helper);
    }

    @Test(dependsOnMethods = { "verifyRecipientTransferNightsEvent" })
    public void verifyRecipientTransferEvent()
    {
        allEventsPage = new AllEventsPage();
        AllEventsGridRow row = allEventsPage.getGrid().getRow(TRANSFER);
        row.verify(AllEventsRowFactory.getPointEvent(TRANSFER, UNITS_TO_SEND_INT));

        row.expand(UnitGridRowContainer.class).getUnitDetails().verifySource(helper);
    }

    @Test(dependsOnMethods = { "verifyRecipientTransferEvent" })
    public void verifyRecipientAnnualActivityPage()
    {
        LifetimeActivity lifetimeActivity = recipient.getLifetimeActivity();
        lifetimeActivity.setAdjustedUnitsFromString(String.valueOf(UNITS_TO_SEND_INT));
        lifetimeActivity.setTotalUnitsFromString(String.valueOf(UNITS_TO_SEND_INT));
        lifetimeActivity.setCurrentBalanceFromString(String.valueOf(UNITS_TO_SEND_INT));

        AnnualActivity annualActivity = recipient.getAnnualActivity();
        annualActivity.setTierLevelNights(nightsToAchieveGold);
        annualActivity.setQualifiedNights(nightsToAchieveGold);
        annualActivity.setTotalUnitsFromString(String.valueOf(UNITS_TO_SEND_INT));

        new AnnualActivityPage().verifyCounters(recipient);
    }

    @Test(dependsOnMethods = { "verifyRecipientAnnualActivityPage" }, alwaysRun = true)
    public void verifyRecipientRewardsClubPage()
    {
        RewardClubPage rewardClubPage = new RewardClubPage();
        rewardClubPage.goTo();

        if (achieveGoldNights > nightsToAchieveGold)
        {
            rewardClubPage.getSummary().verify(ProgramStatus.OPEN, RewardClubLevel.CLUB);
        }
        else
        {
            rewardClubPage.getSummary().verify(ProgramStatus.OPEN, RewardClubLevel.GOLD);
        }

        RewardClubTierLevelActivityGrid currentYearAnnualActivities = rewardClubPage.getCurrentYearAnnualActivities();
        currentYearAnnualActivities.getPointsRow().verifyCurrent("0");
        currentYearAnnualActivities.getNightsRow().verifyCurrent(nights_amount_string);
    }

}
