package com.ihg.crm.automation.selenium.tests.servicecenter.programinfo.dr;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.member.MemberJdbcUtils.UPDATE_EXP;
import static com.ihg.automation.selenium.common.types.program.Program.DR;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static org.hamcrest.CoreMatchers.containsString;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Value;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.member.MemberJdbcUtils;
import com.ihg.automation.selenium.common.types.program.TierLevelFlag;
import com.ihg.automation.selenium.matchers.component.ContainsDualListItems;
import com.ihg.automation.selenium.servicecenter.pages.GuestSearch;
import com.ihg.automation.selenium.servicecenter.pages.LeftPanel;
import com.ihg.automation.selenium.servicecenter.pages.Role;
import com.ihg.automation.selenium.servicecenter.pages.account.AccountInfoPage;
import com.ihg.automation.selenium.servicecenter.pages.programs.RenewExtendProgramSummary;
import com.ihg.automation.selenium.servicecenter.pages.programs.dr.DiningRewardsPage;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.RewardClubPage;

public class UpdateDRProgramFlagsTest extends DRCommon
{
    @Value("${memberFlagDR}")
    protected String memberFlagDR;

    @Value("${memberFlagDR.date}")
    protected String date;

    private DiningRewardsPage diningRewardsPage = new DiningRewardsPage();
    private RenewExtendProgramSummary diningSummary;
    private ArrayList<String> expectedFlagList = new ArrayList<String>(TierLevelFlag.getDrValueList());
    private final static String FLAG_TO_SELECT = TierLevelFlag.CUSTOMER_RETENTION.getValue();
    private AccountInfoPage accountInfoPage = new AccountInfoPage();
    private RewardClubPage rewardClubPage = new RewardClubPage();

    @BeforeClass
    public void beforeClass()
    {
        MemberJdbcUtils.update(jdbcTemplateUpdate, UPDATE_EXP, memberFlagDR, DR, 13);

        login(user15U, Role.AGENT_TIER2);
        new GuestSearch().byMemberNumber(memberFlagDR);
    }

    @Test
    public void verifyAvailableFlags()
    {
        diningRewardsPage.goTo();
        diningSummary = diningRewardsPage.getSummary();
        diningSummary.clickEdit();
        verifyThat(diningSummary.getFlags().getLeftList(), ContainsDualListItems.containsItems(expectedFlagList));
    }

    @Test(dependsOnMethods = { "verifyAvailableFlags" }, alwaysRun = true)
    public void selectFlag()
    {
        diningSummary = diningRewardsPage.getSummary();
        diningSummary.getFlags().select(FLAG_TO_SELECT);

        diningSummary.clickSave();
        verifyThat(diningSummary, hasText(containsString(FLAG_TO_SELECT)));
    }

    @Test(dependsOnMethods = { "selectFlag" }, alwaysRun = true)
    public void verifyLeftPanelFlags()
    {
        verifyThat(new LeftPanel().getCustomerInfoPanel().getFlags(), hasText(containsString(FLAG_TO_SELECT)));
    }

    @Test(dependsOnMethods = { "verifyLeftPanelFlags" }, alwaysRun = true)
    public void verifyAccountInformationFlags()
    {
        accountInfoPage.goTo();
        verifyThat(accountInfoPage.getFlags(), hasText(containsString(FLAG_TO_SELECT)));
    }

    @Test(dependsOnMethods = {
            "verifyAccountInformationFlags" }, enabled = false, description = "disabled due to no DR flags in dgst.gst_flag_typ for QA")
    public void verifyRCSummeryFlag()
    {
        rewardClubPage.goTo();
        verifyThat(rewardClubPage.getSummary(), hasText(containsString("No flags selected")));
    }

    @Test(dependsOnMethods = { "verifyAccountInformationFlags" }, alwaysRun = true)
    public void removeFlag()
    {
        diningRewardsPage.goTo();
        diningSummary = diningRewardsPage.getSummary();
        diningSummary.clickEdit();
        diningSummary.getFlags().removeAll();

        diningSummary.clickSave();
        verifyThat(diningSummary, hasText(containsString("No flags selected")));
    }
}
