package com.ihg.crm.automation.selenium.tests.servicecenter.catalog;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasEmptyText;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasTextAsInt;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static org.hamcrest.number.OrderingComparison.lessThanOrEqualTo;

import java.util.Map;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.google.common.collect.ImmutableList;
import com.ihg.automation.selenium.common.catalog.CatalogItem;
import com.ihg.automation.selenium.servicecenter.pages.GuestSearch;
import com.ihg.automation.selenium.servicecenter.pages.catalog.CatalogItemDetailsPopUp;
import com.ihg.automation.selenium.servicecenter.pages.catalog.CatalogItemsGrid;
import com.ihg.automation.selenium.servicecenter.pages.catalog.CatalogPage;
import com.ihg.automation.selenium.servicecenter.pages.catalog.CatalogSearch;

public class WithMemberInContextTest extends CatalogCommon
{
    private CatalogPage catalogPage = new CatalogPage();
    private CatalogSearch catalogSearch;
    private CatalogItemsGrid catalogGrid;
    private final static CatalogItem CATALOG_ITEM = CatalogItem.builder(ITEM_ID, "IHG Rewards Club - GLDE - 2014")
            .vendor("FISERV").startDate("07May13").endDate("31Dec99").status("Active").orderLimit("1")
            .loyaltyUnitCost("0").regions(ImmutableList.of("UNITED STATES", "CANADA", "MSAC", "EMEA", "ASIA PACIFIC"))
            .countries(ImmutableList.of("All")).tierLevels(ImmutableList.of("All")).build();
    private CatalogItemDetailsPopUp catalogItemDetailsPopUp = new CatalogItemDetailsPopUp();
    private GuestSearch guestSearch = new GuestSearch();
    private static String points;
    private Map<String, Object> map;

    @BeforeClass
    public void beforeClass()
    {
        map = jdbcTemplate.queryForMap(MEMBER_ID);

        points = map.get("POINTS").toString();
        String memberId = map.get("MBRSHP_ID").toString();

        login();
        guestSearch.byMemberNumber(memberId);
    }

    @Test
    public void openCatalog()
    {
        catalogPage.goTo();

        verifyCatalogFields();

        clearSearchCriteria();
        verifyThat(catalogSearch.getLoyaltyUnitCost().getLoyaltyUnitCostMax(), hasEmptyText());
    }

    @Test(dependsOnMethods = { "openCatalog" }, alwaysRun = true)
    public void closeCatalog()
    {
        catalogPage.clickClose();
        verifyThat(catalogPage, displayed(false));

        catalogPage.goTo();

        verifyCatalogFields();
    }

    @Test(dependsOnMethods = { "closeCatalog" }, alwaysRun = true)
    public void searchWithPrePopulatedFields()
    {
        catalogPage.getButtonBar().clickSearch();

        catalogGrid = catalogPage.getCatalogGrid();
        catalogGrid.verifyUnitCost(hasTextAsInt(lessThanOrEqualTo(Integer.parseInt(points))));
    }

    @Test(dependsOnMethods = { "searchWithPrePopulatedFields" }, alwaysRun = true)
    public void searchByItemId()
    {
        verifySearchByItemId(ITEM_ID);
    }

    @Test(dependsOnMethods = { "searchByItemId" }, alwaysRun = true)
    public void searchByItemName()
    {
        verifySearchByItemName("ipod");
    }

    @Test(dependsOnMethods = { "searchByItemName" }, alwaysRun = true)
    public void searchByLoyaltyUnitCostMin()
    {

        verifySearchByLoyaltyUnitCostMin("1000");
    }

    @Test(dependsOnMethods = { "searchByLoyaltyUnitCostMin" }, alwaysRun = true)
    public void searchByLoyaltyUnitCostMax()
    {
        verifySearchByLoyaltyUnitCostMax("5000");
    }

    @Test(dependsOnMethods = { "searchByLoyaltyUnitCostMax" }, alwaysRun = true)
    public void searchByLoyaltyUnitCost()
    {
        verifySearchByLoyaltyUnitCost("1000", "5000");
    }

    @Test(dependsOnMethods = { "openCatalog" }, alwaysRun = true)
    public void verifyItemDetails()
    {
        catalogPage.searchByItemId(CATALOG_ITEM);

        verifyCatalogItemDetailsPopUp(CATALOG_ITEM);

        verifyThat(catalogItemDetailsPopUp.getQuantity(), hasText("1"));

        catalogItemDetailsPopUp.close();
        verifyThat(catalogItemDetailsPopUp, displayed(false));

        catalogGrid = catalogPage.getCatalogGrid();
        catalogGrid.getRow(1).verify(CATALOG_ITEM);
    }

    private void verifyCatalogFields()
    {
        catalogSearch = catalogPage.getSearchFields();

        verifyThat(catalogSearch.getLoyaltyUnitCost().getLoyaltyUnitCostMax(), hasText(points));

        verifyThat(catalogSearch.getCatalogID(), hasEmptyText());
        verifyThat(catalogSearch.getItemID(), hasEmptyText());
        verifyThat(catalogSearch.getItemName(), hasEmptyText());
        verifyThat(catalogSearch.getLoyaltyUnitCost().getLoyaltyUnitCostMin(), hasEmptyText());
    }
}
