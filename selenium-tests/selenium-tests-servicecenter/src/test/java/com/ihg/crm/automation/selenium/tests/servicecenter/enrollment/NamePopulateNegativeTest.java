package com.ihg.crm.automation.selenium.tests.servicecenter.enrollment;

import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyError;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.isHighlightedAsInvalid;
import static com.ihg.automation.selenium.servicecenter.pages.Messages.HIGHLIGHTED_FIELDS_DID_NOT_PASS_VALIDATION;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.components.Country;
import com.ihg.automation.selenium.common.enroll.CustomerInfo;
import com.ihg.automation.selenium.common.member.GuestAddress;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.name.PersonNameFields;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.gwt.components.input.InputBase;
import com.ihg.automation.selenium.servicecenter.pages.enroll.EnrollmentPage;
import com.ihg.crm.automation.selenium.tests.servicecenter.LoginLogout;

public class NamePopulateNegativeTest extends LoginLogout
{
    private EnrollmentPage enrollmentPage = new EnrollmentPage();
    private PersonNameFields personNameEdit;

    @BeforeClass
    public void before()
    {
        GuestAddress guestAddress = MemberPopulateHelper.getUnitedStatesAddress();

        login();

        enrollmentPage.goTo();
        enrollmentPage.getPrograms().selectProgram(Program.RC);

        CustomerInfo customerInfo = enrollmentPage.getCustomerInfo();
        customerInfo.getResidenceCountry().select(Country.US);

        personNameEdit = customerInfo.getName();
        personNameEdit.setConfiguration(Country.US);

        enrollmentPage.getAddress().populate(guestAddress);

    }

    @BeforeMethod
    public void beforeMethod()
    {
        personNameEdit.getSalutation().clear();
        personNameEdit.getGivenName().type("Test");
        personNameEdit.getMiddleName().clear();
        personNameEdit.getSurname().type("Test");
        personNameEdit.getSuffix().clear();
        personNameEdit.getTitle().clear();
        personNameEdit.getDegree().clear();
    }

    @Test(dataProviderClass = NegativeNamesDataProvider.class, dataProvider = "negativeNames")
    public void verifySalutationWithInvalidValue(String salutation)
    {
        verifyFieldWithInvalidValue(personNameEdit.getSalutation(), salutation);
    }

    @Test(dataProviderClass = NegativeNamesDataProvider.class, dataProvider = "negativeNames")
    public void verifyFirstNameWithInvalidValue(String firstName)
    {
        verifyFieldWithInvalidValue(personNameEdit.getGivenName(), firstName);
    }

    @Test(dataProviderClass = NegativeNamesDataProvider.class, dataProvider = "negativeNames")
    public void verifyMiddleNameWithInvalidValue(String middleName)
    {
        verifyFieldWithInvalidValue(personNameEdit.getMiddleName(), middleName);
    }

    @Test(dataProviderClass = NegativeNamesDataProvider.class, dataProvider = "negativeNames")
    public void verifyLastNameWithInvalidValue(String lastName)
    {
        verifyFieldWithInvalidValue(personNameEdit.getSurname(), lastName);
    }

    @Test(dataProviderClass = NegativeNamesDataProvider.class, dataProvider = "negativeNames")
    public void verifySuffixWithInvalidValue(String suffix)
    {
        verifyFieldWithInvalidValue(personNameEdit.getSuffix(), suffix);
    }

    @Test(dataProviderClass = NegativeNamesDataProvider.class, dataProvider = "negativeNames")
    public void verifyTitleWithInvalidValue(String title)
    {
        verifyFieldWithInvalidValue(personNameEdit.getTitle(), title);
    }

    @Test(dataProviderClass = NegativeNamesDataProvider.class, dataProvider = "negativeNames")
    public void verifyDegreeWithInvalidValue(String degree)
    {
        verifyFieldWithInvalidValue(personNameEdit.getDegree(), degree);
    }

    private void verifyFieldWithInvalidValue(InputBase inputField, String inputValue)
    {
        inputField.clickAndWait();
        inputField.type(inputValue);
        enrollmentPage.clickSubmit(verifyError(HIGHLIGHTED_FIELDS_DID_NOT_PASS_VALIDATION));
        verifyThat(inputField, isHighlightedAsInvalid(true));
    }

}
