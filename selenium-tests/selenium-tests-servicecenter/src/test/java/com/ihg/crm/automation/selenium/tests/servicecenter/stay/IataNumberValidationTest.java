package com.ihg.crm.automation.selenium.tests.servicecenter.stay;

import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyError;
import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyNoError;
import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifySuccess;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.stay.Stay;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.servicecenter.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.servicecenter.pages.event.stay.AdjustStayPopUp;
import com.ihg.automation.selenium.servicecenter.pages.event.stay.CreateStayPopUp;
import com.ihg.automation.selenium.servicecenter.pages.event.stay.StayDetailsAdjust;
import com.ihg.automation.selenium.servicecenter.pages.event.stay.StayDetailsCreate;
import com.ihg.automation.selenium.servicecenter.pages.event.stay.StayEventsPage;
import com.ihg.automation.selenium.servicecenter.pages.event.stay.StayInfoCreate;
import com.ihg.automation.selenium.servicecenter.pages.event.stay.StayInfoEdit;
import com.ihg.crm.automation.selenium.tests.servicecenter.LoginLogout;

public class IataNumberValidationTest extends LoginLogout
{
    private static final String IATA_NUMBER_WITHOUT_CHECK_DIGIT = "9683725";
    private static final String IATA_NUMBER_WITH_CORRECT_CHECK_DIGIT = IATA_NUMBER_WITHOUT_CHECK_DIGIT + "2";
    private static final String IATA_NUMBER_WITH_NOT_CORRECT_CHECK_DIGIT = IATA_NUMBER_WITHOUT_CHECK_DIGIT + "9";
    private Stay stay = new Stay();
    private StayEventsPage stayPage = new StayEventsPage();
    private CreateStayPopUp createStayPopUp;
    private StayDetailsCreate details;
    private StayInfoCreate stayInfoCreate;
    private StayInfoEdit stayInfoEdit;
    private AdjustStayPopUp adjustStayPopUp;
    private StayDetailsAdjust stayDetailsAdjust;

    @BeforeClass
    public void beforeClass()
    {
        Member member = new Member();
        member.setPersonalInfo(MemberPopulateHelper.getSimplePersonalInfo());
        member.addAddress(MemberPopulateHelper.getUnitedStatesAddress());
        member.addProgram(Program.RC);

        stay.setHotelCode("ABQMB");
        stay.setCheckInOutByNights(2);
        stay.setAvgRoomRate(100.00);
        stay.setRateCode("Test");

        login();

        new EnrollmentPage().enroll(member);

        stayPage.goTo();
        stayPage.getButtonBar().clickCreateStay(verifyNoError());

        createStayPopUp = new CreateStayPopUp();
        details = createStayPopUp.getStayDetails();
        details.populate(stay);
    }

    @Test(priority = 0)
    public void fillIATANumberWithNineDigits()
    {
        stayInfoCreate = details.getStayInfo();
        stayInfoCreate.getIATANumber().type("123456789");
        verifyThat(stayInfoCreate.getIATANumber(), hasText("12345678"));
    }

    @Test(priority = 0)
    public void typeNotCorrectIATANumber()
    {
        stayInfoCreate = details.getStayInfo();
        stayInfoCreate.getIATANumber().type("12345678");
        createStayPopUp.clickCreateStay(
                verifyError("IATA number is not found or invalid. Do not use check-digit if one was entered."));
    }

    @Test(priority = 1)
    public void fillIATANumberWithFiveDigitsWithoutCheckDigit()
    {
        stayInfoCreate = details.getStayInfo();
        stayInfoCreate.getIATANumber().type("48120");
        createStayPopUp.clickCreateStay(verifySuccess(StayEventsPage.STAY_SUCCESS_CREATED));
        stayPage.waitUntilFirstStayProcess();

        verifyThat(stayPage.getGuestGrid().getRow(1).getDetails().getStayInfo().getIATANumber(), hasText("48120"));
    }

    @Test(priority = 2)
    public void fillIATANumberWithSevenDigitsWithoutCheckDigit()
    {
        createStayWithIATANumber(IATA_NUMBER_WITHOUT_CHECK_DIGIT);
    }

    @Test(priority = 2)
    public void fillIATANumberWithSevenDigitsWithCorrectCheckDigit()
    {
        createStayWithIATANumber(IATA_NUMBER_WITH_CORRECT_CHECK_DIGIT);

    }

    @Test(priority = 2)
    public void fillIATANumberWithSevenDigitsWithNotCorrectCheckDigit()
    {
        createStayWithIATANumber(IATA_NUMBER_WITH_NOT_CORRECT_CHECK_DIGIT);
    }

    @Test(priority = 3)
    public void adjustIATANumberWithNineDigits()
    {
        adjustStayPopUp = stayPage.getGuestGrid().getRow(1).openAdjustPopUp();

        stayDetailsAdjust = adjustStayPopUp.getStayDetailsTab().getStayDetails();
        stayInfoEdit = stayDetailsAdjust.getStayInfo();
        stayInfoEdit.getIATANumber().typeAndWait("123456789");
        verifyThat(stayInfoEdit.getIATANumber(), hasText("12345678"));
    }

    @Test(priority = 4)
    public void adjustTypeNotCorrectIATANumber()
    {
        stayInfoEdit = stayDetailsAdjust.getStayInfo();
        stayInfoEdit.getIATANumber().type("12345678");
        adjustStayPopUp.clickSaveChanges(
                verifyError("IATA number is not found or invalid. Do not use check-digit if one was entered."));
    }

    @Test(priority = 5)
    public void adjustIATANumberWithFiveDigitsWithoutCheckDigit()
    {
        stayInfoEdit.getIATANumber().type("48120");
        adjustStayPopUp.clickSaveChanges(verifySuccess(StayEventsPage.STAY_SUCCESS_ADJUSTED));
        stayPage.waitUntilFirstStayProcess();

        verifyThat(stayPage.getGuestGrid().getRow(1).getDetails().getStayInfo().getIATANumber(), hasText("48120"));
    }

    @Test(priority = 6)
    public void adjustIATANumberWithSevenDigitsWithoutCheckDigit()
    {
        editIATANumber(IATA_NUMBER_WITHOUT_CHECK_DIGIT);
    }

    @Test(priority = 6)
    public void adjustIATANumberWithSevenDigitsWithCorrectCheckDigit()
    {
        editIATANumber(IATA_NUMBER_WITH_CORRECT_CHECK_DIGIT);
    }

    @Test(priority = 6)
    public void adjustIATANumberWithSevenDigitsWithNotCorrectCheckDigit()
    {
        editIATANumber(IATA_NUMBER_WITH_NOT_CORRECT_CHECK_DIGIT);
    }

    private void editIATANumber(String number)
    {
        adjustStayPopUp = stayPage.getGuestGrid().getRow(1).openAdjustPopUp();
        stayDetailsAdjust = adjustStayPopUp.getStayDetailsTab().getStayDetails();
        stayInfoEdit = stayDetailsAdjust.getStayInfo();
        stayInfoEdit.getIATANumber().type(number);
        adjustStayPopUp.clickSaveChanges(verifySuccess(StayEventsPage.STAY_SUCCESS_ADJUSTED));
        stayPage.waitUntilFirstStayProcess();

        verifyThat(stayPage.getGuestGrid().getRow(1).getDetails().getStayInfo().getIATANumber(), hasText(number));
    }

    private void createStayWithIATANumber(String number)
    {
        stay.setIataCode(number);
        stayPage.createStay(stay);

        verifyThat(stayPage.getGuestGrid().getRow(1).getDetails().getStayInfo().getIATANumber(),
                hasText(stay.getIataCode()));

    }
}
