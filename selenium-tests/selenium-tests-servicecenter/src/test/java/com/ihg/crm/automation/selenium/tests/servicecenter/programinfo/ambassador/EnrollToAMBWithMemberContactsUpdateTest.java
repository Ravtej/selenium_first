package com.ihg.crm.automation.selenium.tests.servicecenter.programinfo.ambassador;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static org.hamcrest.core.Is.is;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.member.AmbassadorOfferCode;
import com.ihg.automation.selenium.common.member.AmbassadorUtils;
import com.ihg.automation.selenium.common.member.GuestAddress;
import com.ihg.automation.selenium.common.member.GuestEmail;
import com.ihg.automation.selenium.common.member.GuestPhone;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.personal.PhoneContactList;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.gwt.components.Mode;
import com.ihg.automation.selenium.servicecenter.pages.LeftPanel;
import com.ihg.automation.selenium.servicecenter.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.servicecenter.pages.personal.AddressContactList;
import com.ihg.automation.selenium.servicecenter.pages.personal.CustomerInfoFields;
import com.ihg.automation.selenium.servicecenter.pages.personal.PersonalInfoPage;
import com.ihg.crm.automation.selenium.tests.servicecenter.LoginLogout;

public class EnrollToAMBWithMemberContactsUpdateTest extends LoginLogout
{
    private Member member = new Member();
    private Member memberAMB = new Member();
    private GuestEmail updatedEmail;
    private PersonalInfoPage personalInfoPage = new PersonalInfoPage();
    private GuestPhone updatedPhone = new GuestPhone();
    private GuestPhone secondPhone = new GuestPhone();
    private GuestAddress updatedAddress = new GuestAddress();
    private GuestAddress secondAddress = new GuestAddress();
    private CustomerInfoFields customerInfo;

    @BeforeClass
    public void enrollRC()
    {
        secondPhone = MemberPopulateHelper.getRandomPhone();
        secondPhone.setPreferred(false);

        secondAddress = MemberPopulateHelper.getUnitedStatesSecondAddress();
        secondAddress.setPreferred(false);

        member.setPersonalInfo(MemberPopulateHelper.getSimplePersonalInfo());
        member.addAddress(MemberPopulateHelper.getUnitedStatesAddress());
        member.addPhone(MemberPopulateHelper.getRandomPhone());
        member.addEmail(MemberPopulateHelper.getRandomEmail());
        member.addProgram(Program.RC);

        updatedAddress = member.getPreferredAddress().clone();
        updatedAddress.setLine1("2 1ST AVE");
        updatedAddress.setPostalCode("10009-8147");
        updatedEmail = MemberPopulateHelper.getRandomEmail();
        updatedPhone = MemberPopulateHelper.getRandomPhone();

        memberAMB.setAmbassadorOfferCode(AmbassadorOfferCode.CHSRO);
        memberAMB.setAmbassadorAmount(AmbassadorUtils.AMBASSADOR_COMP_PROMO_$0);
        memberAMB.addProgram(Program.AMB);
        memberAMB.addPhone(updatedPhone);
        memberAMB.addAddress(updatedAddress);
        memberAMB.addEmail(updatedEmail);

        login();
        member = new EnrollmentPage().enroll(member);

        personalInfoPage.getPhoneList().add(secondPhone);
        personalInfoPage.getAddressList().add(secondAddress);
    }

    @Test
    public void enrollAMBandReopenProfile()
    {
        memberAMB = new EnrollmentPage().enroll(memberAMB);

        new LeftPanel().reOpenMemberProfile(member);
    }

    @Test(dependsOnMethods = { "enrollAMBandReopenProfile" }, alwaysRun = true, description = "DE14694")
    public void verifyPersonalInformation()
    {
        AddressContactList addressContactList = personalInfoPage.getAddressList();

        verifyThat(addressContactList, addressContactList.getContactsCount(), is(2),
                "Two addresses on personal info page");

        addressContactList.getExpandedContact().verify(updatedAddress, Mode.VIEW);
        addressContactList.getExpandedContact(2).verify(secondAddress, Mode.VIEW);

        PhoneContactList phoneContactList = personalInfoPage.getPhoneList();
        verifyThat(phoneContactList, phoneContactList.getContactsCount(), is(2), "Two phones on personal info page");

        phoneContactList.getExpandedContact().verify(updatedPhone, Mode.VIEW);
        phoneContactList.getExpandedContact(2).verify(secondPhone, Mode.VIEW);

        personalInfoPage.getEmailList().getExpandedContact().verify(updatedEmail, Mode.VIEW);
    }
}
