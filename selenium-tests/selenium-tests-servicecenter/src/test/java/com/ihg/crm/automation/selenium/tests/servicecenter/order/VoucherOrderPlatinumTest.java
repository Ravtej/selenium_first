package com.ihg.crm.automation.selenium.tests.servicecenter.order;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.event.EventTransactionType.Order.ORDER_SYSTEM;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static org.apache.commons.lang3.StringUtils.EMPTY;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.Constant;
import com.ihg.automation.selenium.common.billing.EventBillingDetailsTab;
import com.ihg.automation.selenium.common.earning.EventEarningDetailsTab;
import com.ihg.automation.selenium.common.event.EarningEvent;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.order.Order;
import com.ihg.automation.selenium.common.order.OrderItem;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.common.types.program.RewardClubLevel;
import com.ihg.automation.selenium.gwt.components.MessageBox;
import com.ihg.automation.selenium.gwt.components.MessageBoxType;
import com.ihg.automation.selenium.servicecenter.pages.LeftPanel;
import com.ihg.automation.selenium.servicecenter.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsGridRow;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsPage;
import com.ihg.automation.selenium.servicecenter.pages.event.order.OrderDetailsPopUp;
import com.ihg.automation.selenium.servicecenter.pages.event.order.OrderSystemDetailsGridRowContainer;
import com.ihg.automation.selenium.servicecenter.pages.event.voucher.VoucherDetailsPopUp;
import com.ihg.automation.selenium.servicecenter.pages.event.voucher.VoucherGridRowContainer;
import com.ihg.automation.selenium.servicecenter.pages.voucher.VoucherPopUp;

public class VoucherOrderPlatinumTest extends VoucherOrderCommon
{
    private String voucherNumber;
    private EarningEvent baseAwardEvent, baseUnitsEvent;
    private Order order = new Order();
    private AllEventsPage allEventsPage = new AllEventsPage();
    private static final String VOUCHER_DEPOSIT = "VOUCHER DEPOSIT";

    private static final String AVAILABLE_VOUCHER_UPGRADE_TO_PLTN = "SELECT TO_CHAR(BON_VOUCHER_ID,'FM000000000') || "
                                                                  + "TO_CHAR(FACILITY_ID,'FM00000') || TO_CHAR(BON_VOUCHER_PIN,'FM0000') VOUCHER_NUMBER "
                                                                  + "FROM FMDS.TMBNVCH "
                                                                  + "WHERE TRIM(VOUCHER_STAT_CD) IS NULL "
                                                                  + "AND MEMBERSHIP_ID IS NULL "
                                                                  + "AND VOUCHER_SENT_IND='Y' "
                                                                  + "AND PROMO_ID='CTUCP ' "
                                                                  + "AND ROWNUM < 2";

    @BeforeClass
    public void beforeClass()
    {
        Member member = new Member();
        member.setPersonalInfo(MemberPopulateHelper.getSimplePersonalInfo());
        member.addAddress(MemberPopulateHelper.getUnitedStatesAddress());
        member.addProgram(Program.RC);

        voucherNumber = getVoucherNumber(String.format(AVAILABLE_VOUCHER_UPGRADE_TO_PLTN));
        OrderItem orderItem = new OrderItem("F0010", "ELITE UPGRADE CERTIFICATE KIT");
        orderItem.setDeliveryStatus("PROCESSING");
        order.setShippingInfo(member);
        order.setTransactionType(ORDER_SYSTEM);
        order.getOrderItems().add(orderItem);

        baseAwardEvent = new EarningEvent(Constant.BASE_AWARDS, "1", EMPTY, EMPTY, "Created");
        baseAwardEvent.setAward(orderItem.getItemID());

        baseUnitsEvent = new EarningEvent(Constant.BASE_UNITS, "0", "0", Constant.RC_POINTS, EMPTY);

        login();
        new EnrollmentPage().enroll(member);
    }

    @Test(priority = 10)
    public void addDepositVoucherToMember()
    {
        new VoucherPopUp().getVoucherDepositTab().deposit(voucherNumber);
        verifyThat(new MessageBox(MessageBoxType.SUCCESS), hasText("Voucher has been successfully deposited."));
    }

    @Test(priority = 20)
    public void verifyVoucherDepositEvent()
    {
        allEventsPage.goTo();

        AllEventsGridRow row = allEventsPage.getGrid().getRow(VOUCHER_DEPOSIT);
        verifyThat(row, displayed(true));

        VoucherGridRowContainer rowContainer = row.expand(VoucherGridRowContainer.class);

        rowContainer.getVoucherDetails().verify(voucherNumber, helper);

        rowContainer.clickDetails();
        VoucherDetailsPopUp voucherDetailsPopUp = new VoucherDetailsPopUp();

        // Voucher Details tab
        voucherDetailsPopUp.getVoucherDetailsTab().getVoucherDetails().verify(voucherNumber, helper);

        // Earning Details tab
        EventEarningDetailsTab earningDetailsTab = voucherDetailsPopUp.getEarningDetailsTab();
        earningDetailsTab.goTo();
        earningDetailsTab.verifyBasicDetailsNoPointsAndMiles();
        earningDetailsTab.getGrid().verifyRowsFoundByType(baseAwardEvent, baseUnitsEvent);

        // Billing Details tab
        EventBillingDetailsTab billingDetailsTab = voucherDetailsPopUp.getBillingDetailsTab();
        billingDetailsTab.goTo();
        billingDetailsTab.verifyTabIsEmpty("VOUCHER");

        voucherDetailsPopUp.clickClose();
    }

    @Test(priority = 30)
    public void verifySystemOrderEvent()
    {
        allEventsPage.goTo();

        AllEventsGridRow row = allEventsPage.getGrid().getRow(ORDER_SYSTEM);
        verifyThat(row, displayed(true));

        OrderSystemDetailsGridRowContainer rowContainer = row.expand(OrderSystemDetailsGridRowContainer.class);

        rowContainer.verify(order);

        rowContainer.clickDetails();
        OrderDetailsPopUp orderDetailsPopUp = new OrderDetailsPopUp();

        // Voucher Details tab
        orderDetailsPopUp.getOrderDetailsTab().verify(order);

        // Earning Details tab
        EventEarningDetailsTab earningDetailsTab = orderDetailsPopUp.getEarningDetailsTab();
        earningDetailsTab.goTo();
        earningDetailsTab.verifyBasicDetailsNoPointsAndMiles();
        earningDetailsTab.getGrid().verify(baseAwardEvent);

        // Billing Details tab
        EventBillingDetailsTab billingDetailsTab = orderDetailsPopUp.getBillingDetailsTab();
        billingDetailsTab.goTo();
        billingDetailsTab.verifyTabIsEmpty(ORDER_SYSTEM);

        orderDetailsPopUp.clickClose();
    }

    @Test(priority = 40)
    public void verifyTierLevel()
    {
        new LeftPanel().verifyRCLevel(RewardClubLevel.PLTN);
    }
}
