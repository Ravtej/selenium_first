package com.ihg.crm.automation.selenium.tests.servicecenter.offer;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.Constant.NOT_AVAILABLE;
import static com.ihg.automation.selenium.matchers.component.GridSize.size;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static com.ihg.automation.selenium.matchers.component.IsDisplayed.displayed;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.servicecenter.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.servicecenter.pages.event.offer.OfferEventPage;
import com.ihg.automation.selenium.servicecenter.pages.event.offer.OfferPopUp;
import com.ihg.automation.selenium.servicecenter.pages.event.offer.UniverseOfferGridRow;
import com.ihg.automation.selenium.servicecenter.pages.event.offer.UniverseOffersGrid;
import com.ihg.automation.selenium.servicecenter.pages.offer.OfferRegistrationPopUp;

@Test(groups = { "targetOfferTest" })
public class RegisterToTargetOfferWithoutEndDateWithMemberInContextTest extends TargetingOfferCommon
{
    private OfferEventPage offerPage = new OfferEventPage();

    @BeforeClass
    public void beforeClass()
    {
        Member member = new Member();
        member.setPersonalInfo(MemberPopulateHelper.getSimplePersonalInfo());
        member.addAddress(MemberPopulateHelper.getIndonesiaAddress());
        member.addProgram(Program.RC);

        login();

        new EnrollmentPage().enroll(member);

        insert(TARGET_OFFER_ID, member, null);

    }

    @Test
    public void searchWithEligible()
    {
        offerPage.goTo();
        offerPage.searchByOfferId(TARGET_OFFER_ID, true);

        new OfferPopUp().waitAndClose();

        UniverseOffersGrid universeOffersGrid = offerPage.getUniverseOffersGrid();
        UniverseOfferGridRow row = universeOffersGrid.getRow(1);

        verifyThat(universeOffersGrid, size(1));

        row.verify(TARGET_OFFER_ID, "Register");

        verifyThat(row.getRowContainer().getOfferDetails().getOfferEligibility().getTargetExpiry(),
                hasText(NOT_AVAILABLE));
    }

    @Test(dependsOnMethods = { "searchWithEligible" }, alwaysRun = true)
    public void registerToOffer()
    {
        OfferRegistrationPopUp offerRegistrationPopUp = new OfferRegistrationPopUp();
        offerRegistrationPopUp.registerToTargetingOffer(null, TARGET_OFFER_ID, NOT_AVAILABLE);
    }

    @Test(dependsOnMethods = { "registerToOffer" })
    public void verifyOffer()
    {
        verifyTargetExpiry(NOT_AVAILABLE);
        verifyThat(offerPage.getMemberOffersGrid().getRowByOfferCode(TARGET_OFFER_ID), displayed(true));
    }
}
