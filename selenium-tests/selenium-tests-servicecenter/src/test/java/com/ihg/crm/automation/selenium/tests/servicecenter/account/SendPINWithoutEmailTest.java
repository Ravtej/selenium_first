package com.ihg.crm.automation.selenium.tests.servicecenter.account;

import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyNoError;
import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifySuccess;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.types.program.Program.RC;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static com.ihg.automation.selenium.matchers.component.ViewableMatcher.hasTextInView;
import static org.hamcrest.CoreMatchers.containsString;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.servicecenter.pages.account.AccountInfoPage;
import com.ihg.automation.selenium.servicecenter.pages.account.EmailPopUp;
import com.ihg.automation.selenium.servicecenter.pages.account.Security;
import com.ihg.automation.selenium.servicecenter.pages.enroll.EnrollmentPage;
import com.ihg.crm.automation.selenium.tests.servicecenter.LoginLogout;

public class SendPINWithoutEmailTest extends LoginLogout
{
    private Member member = new Member();

    @BeforeClass
    public void beforeClass()
    {
        member.addProgram(RC);
        member.setPersonalInfo(MemberPopulateHelper.getSimplePersonalInfo());
        member.addAddress(MemberPopulateHelper.getUnitedStatesAddress());

        login();

        new EnrollmentPage().enroll(member);
    }

    @Test
    public void AddPINUsingSendPINWithoutEmail()
    {
        AccountInfoPage accountInfo = new AccountInfoPage();
        accountInfo.goTo();
        Security security = accountInfo.getSecurity();
        security.clickEdit();
        security.getSendPin().click();
        EmailPopUp popUp = new EmailPopUp();
        verifyThat(popUp, displayed(true));
        verifyThat(popUp, hasText(containsString(
                "The email address on the account is invalid. Please add a valid email address to the account to send a PIN.")));
        popUp.getEmail().type(MemberPopulateHelper.getRandomEmail().getEmail());
        popUp.clickSave(verifyNoError(), verifySuccess("PIN has been successfully sent to email address on file."));
        verifyThat(security.getPin(), hasTextInView("••••"));
    }
}
