package com.ihg.crm.automation.selenium.tests.servicecenter.demo;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.types.program.ProgramStatus.CLOSED;
import static com.ihg.automation.selenium.common.types.program.RewardClubLevel.CLUB;
import static com.ihg.automation.selenium.matchers.component.GridSize.size;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static com.ihg.automation.selenium.matchers.component.ViewableMatcher.hasTextInView;
import static org.hamcrest.core.StringContains.containsString;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.member.GuestName;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.common.types.program.ProgramStatus;
import com.ihg.automation.selenium.gwt.components.Mode;
import com.ihg.automation.selenium.gwt.components.label.Label;
import com.ihg.automation.selenium.servicecenter.pages.account.AccountInfoPage;
import com.ihg.automation.selenium.servicecenter.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.servicecenter.pages.personal.PersonalInfoPage;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.RewardClubPage;
import com.ihg.crm.automation.selenium.tests.servicecenter.LoginLogout;

public class DemoTest extends LoginLogout
{
    private static final String WRONG_STATUS = CLOSED;
    private EnrollmentPage enrollmentPage = new EnrollmentPage();
    private Member member = new Member();
    private GuestName guestName;

    @BeforeClass
    public void before()
    {
        member.setPersonalInfo(MemberPopulateHelper.getSimplePersonalInfo());
        member.addProgram(Program.RC);
        member.addPhone(MemberPopulateHelper.getRandomPhone());
        member.addEmail(MemberPopulateHelper.getRandomEmail());
        member.addAddress(MemberPopulateHelper.getUnitedStatesAddress());
        guestName = member.getPersonalInfo().getName();

        login();
    }

    @Test
    public void simpleEnrollment()
    {
        enrollmentPage.enroll(member);

        Label fullName = new PersonalInfoPage().getCustomerInfo().getName().getFullName();
        verifyThat(fullName, hasText(guestName.getGiven() + " " + guestName.getSurname()));
    }

    @Test(dependsOnMethods = { "simpleEnrollment" }, alwaysRun = true)
    public void validatePersonalInformation()
    {
        PersonalInfoPage persInfo = new PersonalInfoPage();
        persInfo.getCustomerInfo().verify(member, Mode.VIEW);
        persInfo.getAddressList().getExpandedContact().verify(member.getPreferredAddress(), Mode.VIEW);
        persInfo.getPhoneList().getExpandedContact().verify(member.getPreferredPhone(), Mode.VIEW);
        persInfo.getEmailList().getExpandedContact().verify(member.getPreferredEmail(), Mode.VIEW);
    }

    @Test(dependsOnMethods = { "validatePersonalInformation" }, alwaysRun = true)
    public void validateAccountInformation()
    {
        AccountInfoPage accountPage = new AccountInfoPage();
        accountPage.goTo();
        verifyThat(accountPage.getSecurity().getLockout(), hasTextInView("Unlocked"));
        accountPage.getFailedSelfServiceLoginAttempts().expand();
        verifyThat(accountPage.getFailedSelfServiceLoginAttempts().getGrid(), size(1));
        accountPage.verify(ProgramStatus.OPEN);
        verifyThat(accountPage.getFlags(), hasText(containsString("No flags selected")));
    }

    @Test(dependsOnMethods = { "validateAccountInformation" }, alwaysRun = true)
    public void validateRewardClubSummary()
    {
        RewardClubPage rewardClubPage = new RewardClubPage();
        rewardClubPage.goTo();
        rewardClubPage.getSummary().verify(WRONG_STATUS, CLUB);
    }
}
