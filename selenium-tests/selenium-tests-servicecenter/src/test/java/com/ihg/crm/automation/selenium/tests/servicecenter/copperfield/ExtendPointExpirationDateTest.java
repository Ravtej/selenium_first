package com.ihg.crm.automation.selenium.tests.servicecenter.copperfield;

import static com.ihg.automation.common.DateUtils.DATE_FORMAT;
import static com.ihg.automation.common.DateUtils.now;
import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyNoError;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.Constant.NOT_AVAILABLE;
import static com.ihg.automation.selenium.common.event.EventTransactionType.PointExpiration.EXTEND_POINT_EXPIRATION;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.enabled;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasDefault;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasText;
import static com.ihg.automation.selenium.servicecenter.pages.programs.rc.RewardClubSummary.EXPIRATION_MONTH_SHIFT;
import static org.apache.commons.lang3.StringUtils.EMPTY;
import static org.hamcrest.Matchers.allOf;

import org.joda.time.LocalDate;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.event.AllEventsRow;
import com.ihg.automation.selenium.common.event.EventTransactionType;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.common.types.program.ProgramStatusReason;
import com.ihg.automation.selenium.gwt.components.dialog.ConfirmDialog;
import com.ihg.automation.selenium.matchers.component.GridSize;
import com.ihg.automation.selenium.servicecenter.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsGrid;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsGridRow;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsPage;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsSearch;
import com.ihg.automation.selenium.servicecenter.pages.event.point.PointExpirationRowContainer;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.RewardClubPage;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.RewardClubSummary;
import com.ihg.automation.selenium.servicecenter.pages.unit.GoodwillPopUp;
import com.ihg.crm.automation.selenium.tests.servicecenter.LoginLogout;

@Test(groups = { "copperfield" })
public class ExtendPointExpirationDateTest extends LoginLogout
{
    public static final String QUESTION = "Are you sure you want to extend the Loyalty Unit balance expiration date? "
            + "New expiration date will be %s.";
    private final static String UPDATE_EXPIRATION_DATE = "UPDATE DGST.MBRSHP SET" //
            + " LST_ACTV_DT = TRUNC(SYSDATE -1),"//
            + " PT_BAL_EXPR_DT = TO_DATE('%3$s', 'ddMONYY'),"//
            + " LST_UPDT_USR_ID='%2$s',"//
            + " LST_UPDT_TS = SYSDATE"//
            + " WHERE MBRSHP_ID='%1$s' AND LYTY_PGM_CD='PC'";
    private Member member = new Member();
    private RewardClubPage rewardClubPage = new RewardClubPage();
    private RewardClubSummary summary;
    private AllEventsPage allEventsPage;
    private String expirationDate, lastActivityDate;

    @BeforeClass
    public void beforeClass()
    {
        member.setPersonalInfo(MemberPopulateHelper.getSimplePersonalInfo());
        member.addAddress(MemberPopulateHelper.getUnitedStatesAddress());
        member.addProgram(Program.RC);

        login();

        member = new EnrollmentPage().enroll(member);
        new GoodwillPopUp().goodwillPoints("1000");

        expirationDate = getUpdatedExpirationDate(-1);
        lastActivityDate = now().plusDays(-1).toString(DATE_FORMAT);

        rewardClubPage.goTo();
        summary = rewardClubPage.getSummary();

        verifySummaryFieldsAfterUpdate();
    }

    @Test
    public void extendBalanceClickNo()
    {
        summary.getExtend().clickAndWait(verifyNoError());
        ConfirmDialog questionPopUp = new ConfirmDialog();
        verifyThat(questionPopUp,
                hasText(String.format(QUESTION, now().plusMonths(EXPIRATION_MONTH_SHIFT).toString(DATE_FORMAT))));
        questionPopUp.clickNo(verifyNoError());

        verifySummaryFieldsAfterUpdate();
    }

    @Test(dependsOnMethods = { "extendBalanceClickNo" }, alwaysRun = true)
    public void extendBalanceExpirationDate()
    {
        summary.getExtend().clickAndWait(verifyNoError());
        new ConfirmDialog().clickYes(verifyNoError());

        LocalDate now = now();
        expirationDate = now.plusMonths(EXPIRATION_MONTH_SHIFT).toString(DATE_FORMAT);

        verifyThat(summary.getExtend(), allOf(displayed(true), enabled(false)));

        verifyThat(summary.getBalanceExpirationDate(), hasText(expirationDate));
        verifyThat(summary.getLastActivityDate(), hasText(lastActivityDate));
        verifyThat(summary.getLastAdjusted(), hasText(now.toString(DATE_FORMAT)));
    }

    @Test(dependsOnMethods = { "extendBalanceExpirationDate" })
    public void verifyExtendPointExpirationEvent()
    {
        allEventsPage = new AllEventsPage();
        allEventsPage.goTo();
        AllEventsRow extendPointExpirationEvent = new AllEventsRow(EXTEND_POINT_EXPIRATION,
                String.format("Extend Point Expiration Date - %s", expirationDate));
        extendPointExpirationEvent.setIhgUnits(EMPTY);
        extendPointExpirationEvent.setAllianceUnits(EMPTY);

        AllEventsGridRow row = allEventsPage.getGrid().getRow(1);
        row.verify(extendPointExpirationEvent);
        PointExpirationRowContainer rowContainer = row.expand(PointExpirationRowContainer.class);
        rowContainer.getSource().verify(helper.getSourceFactory().getNoEmployeeIdSource(), NOT_AVAILABLE);
        verifyThat(rowContainer.getDetails(), displayed(false));
    }

    @Test(dependsOnMethods = { "verifyExtendPointExpirationEvent" })
    public void verifyExtendPointExpirationEventFiltering()
    {
        AllEventsSearch searchFields = allEventsPage.getSearchFields();
        searchFields.getEventType().select(EventTransactionType.PointExpiration.EVENT_TYPE);
        searchFields.getTransactionType().select(EXTEND_POINT_EXPIRATION);
        allEventsPage.getButtonBar().clickSearch();

        AllEventsGrid grid = allEventsPage.getGrid();
        verifyThat(grid, GridSize.size(1));
        verifyThat(grid.getRow(1).getCell(AllEventsGridRow.AllEventsCell.TRANS_TYPE), hasText(EXTEND_POINT_EXPIRATION));
    }

    @Test(dependsOnMethods = { "verifyExtendPointExpirationEventFiltering" }, alwaysRun = true)
    public void verifyExtendButtonAvailability()
    {
        expirationDate = getUpdatedExpirationDate(1);

        rewardClubPage.goTo();
        verifyThat(summary.getBalanceExpirationDate(), hasText(expirationDate));
        verifyThat(summary.getExtend(), allOf(displayed(true), enabled(false)));
    }

    @Test(dependsOnMethods = { "verifyExtendButtonAvailability" }, alwaysRun = true)
    public void verifyExtendButtonAvailabilityForClosedMember()
    {
        expirationDate = getUpdatedExpirationDate(-1);

        summary.setClosedStatus(ProgramStatusReason.FROZEN);
        verifyThat(summary.getBalanceExpirationDate(), hasText(expirationDate));
        verifyThat(summary.getExtend(), allOf(displayed(true), enabled(false)));
    }

    private String getUpdatedExpirationDate(int shift)
    {
        expirationDate = now().plusDays(shift).plusMonths(EXPIRATION_MONTH_SHIFT).toString(DATE_FORMAT);
        jdbcTemplateUpdate.update(
                String.format(UPDATE_EXPIRATION_DATE, member.getRCProgramId(), lastUpdateUserId, expirationDate));
        return expirationDate;
    }

    private void verifySummaryFieldsAfterUpdate()
    {
        verifyThat(summary.getBalanceExpirationDate(), hasText(expirationDate));
        verifyThat(summary.getLastActivityDate(), hasText(lastActivityDate));
        verifyThat(summary.getLastAdjusted(), hasDefault());

        verifyThat(summary.getExtend(), allOf(displayed(true), enabled(true)));
    }
}
