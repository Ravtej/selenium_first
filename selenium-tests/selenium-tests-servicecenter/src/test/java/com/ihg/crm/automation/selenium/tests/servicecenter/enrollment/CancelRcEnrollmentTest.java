package com.ihg.crm.automation.selenium.tests.servicecenter.enrollment;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.assertThat;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.member.PersonalInfo;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.servicecenter.pages.enroll.EnrollmentPage;
import com.ihg.crm.automation.selenium.tests.servicecenter.LoginLogout;

public class CancelRcEnrollmentTest extends LoginLogout
{
    private EnrollmentPage enrollmentPage = new EnrollmentPage();
    private Member member = new Member();

    @BeforeClass
    public void before()
    {
        member.addProgram(Program.RC);
        PersonalInfo personalInfo = new PersonalInfo();
        personalInfo.setName(MemberPopulateHelper.getRandomName());
        member.setPersonalInfo(personalInfo);
        member.addAddress(MemberPopulateHelper.getUnitedStatesAddress());

        login();
        enrollmentPage.goTo();
    }

    @Test
    public void enrollmentIsCancelled()
    {
        enrollmentPage.populate(member);

        enrollmentPage.clickCancel();
        assertThat(getWelcome(), displayed(true));
    }
}
