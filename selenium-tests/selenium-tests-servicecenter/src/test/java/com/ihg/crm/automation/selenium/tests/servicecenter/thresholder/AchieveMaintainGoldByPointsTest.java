package com.ihg.crm.automation.selenium.tests.servicecenter.thresholder;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.event.EventTransactionType.TierLevel.UPGRADE_ACHIEVE;
import static com.ihg.automation.selenium.common.event.EventTransactionType.TierLevel.UPGRADE_MAINTAIN;
import static com.ihg.automation.selenium.common.types.program.RewardClubLevel.CLUB;
import static com.ihg.automation.selenium.common.types.program.RewardClubLevel.GOLD;
import static com.ihg.automation.selenium.common.types.program.RewardClubLevel.PLTN;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.stay.Stay;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.servicecenter.pages.LeftPanel;
import com.ihg.automation.selenium.servicecenter.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsPage;
import com.ihg.automation.selenium.servicecenter.pages.event.stay.StayEventsPage;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.RewardClubPage;
import com.ihg.automation.selenium.tests.common.RulesDBUtils;
import com.ihg.crm.automation.selenium.tests.servicecenter.LoginLogout;

public class AchieveMaintainGoldByPointsTest extends LoginLogout
{
    private Stay stayInPreviousYear = new Stay();
    private Stay stayInCurrentYear = new Stay();
    private static int pointsToMaintainGold;
    private static int pointsToAchievePlatinum;
    private static int pointsToAchievePlatinumPrevious;
    private static int pointsToAchieveGoldPrevious;
    private static int pointsToAchieveGoldCurrent;

    @BeforeClass
    public void before()
    {
        RulesDBUtils rulesDBUtils = new RulesDBUtils(jdbcTemplate);

        pointsToMaintainGold = rulesDBUtils.getCurrentYearRulePoints(GOLD, GOLD);
        pointsToAchievePlatinum = rulesDBUtils.getCurrentYearRulePoints(GOLD, PLTN);
        pointsToAchievePlatinumPrevious = rulesDBUtils.getPreviousYearRulePoints(GOLD, PLTN);
        pointsToAchieveGoldPrevious = rulesDBUtils.getPreviousYearRulePoints(CLUB, GOLD);
        pointsToAchieveGoldCurrent = rulesDBUtils.getCurrentYearRulePoints(CLUB, GOLD);

        stayInPreviousYear.setHotelCode("CEQHA");
        stayInPreviousYear.setCheckInOutByNightsInPreviousYear(1);
        stayInPreviousYear.setUsdAvgRoomRateByPoints(pointsToAchieveGoldPrevious);
        stayInPreviousYear.setRateCode("Test");

        stayInCurrentYear.setHotelCode("CEQHA");
        stayInCurrentYear.setCheckInOutByNights(1);
        stayInCurrentYear.setUsdAvgRoomRateByPoints(pointsToMaintainGold);
        stayInCurrentYear.setRateCode("Test");

        Member member = new Member();
        member.addProgram(Program.RC);
        member.setPersonalInfo(MemberPopulateHelper.getSimplePersonalInfo());
        member.addAddress(MemberPopulateHelper.getUnitedStatesAddress());

        login();

        new EnrollmentPage().enroll(member);

        new LeftPanel().verifyRCLevel(CLUB);
    }

    @Test
    public void verifyAnnualActivitiesForClub()
    {
        RewardClubPage rewardClubPage = new RewardClubPage();
        rewardClubPage.goTo();
        rewardClubPage.getCurrentYearAnnualActivities().getPointsRow().verify(0, pointsToAchieveGoldCurrent);
    }

    @Test(dependsOnMethods = { "verifyAnnualActivitiesForClub" }, alwaysRun = true)
    public void achieveGoldTierLevel()
    {
        new StayEventsPage().createStay(stayInPreviousYear);
        new LeftPanel().verifyRCLevel(GOLD);

        RewardClubPage rewardClubPage = new RewardClubPage();
        rewardClubPage.goTo();

        rewardClubPage.getPreviousYearAnnualActivities().getPointsRow().verify(pointsToAchieveGoldPrevious,
                pointsToAchievePlatinumPrevious);

        rewardClubPage.getCurrentYearAnnualActivities().getPointsRow().verify(0, pointsToAchievePlatinum,
                pointsToMaintainGold);

        AllEventsPage allEventsPage = new AllEventsPage();
        allEventsPage.goTo();

        verifyThat(allEventsPage.getGrid().getRow(UPGRADE_ACHIEVE, "RC - GOLD"), displayed(true));
    }

    @Test(dependsOnMethods = { "achieveGoldTierLevel" }, alwaysRun = true)
    public void maintainGoldTierLevel()
    {
        new StayEventsPage().createStay(stayInCurrentYear);

        RewardClubPage rewardClubPage = new RewardClubPage();
        rewardClubPage.goTo();

        rewardClubPage.getCurrentYearAnnualActivities().getPointsRow().verifyMaintainIsAchieved(pointsToMaintainGold,
                pointsToAchievePlatinum);

        AllEventsPage allEventsPage = new AllEventsPage();
        allEventsPage.goTo();
        verifyThat(allEventsPage.getGrid().getRow(UPGRADE_MAINTAIN, "RC - GOLD"), displayed(true));
    }
}
