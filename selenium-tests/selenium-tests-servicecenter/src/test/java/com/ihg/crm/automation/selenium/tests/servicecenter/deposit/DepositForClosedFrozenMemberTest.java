package com.ihg.crm.automation.selenium.tests.servicecenter.deposit;

import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyError;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.event.EventTransactionType.Deposit.CO_PARTNER;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.Constant;
import com.ihg.automation.selenium.common.deposit.Deposit;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.common.types.program.ProgramStatus;
import com.ihg.automation.selenium.common.types.program.ProgramStatusReason;
import com.ihg.automation.selenium.common.types.program.RewardClubLevel;
import com.ihg.automation.selenium.servicecenter.pages.LeftPanel;
import com.ihg.automation.selenium.servicecenter.pages.annual.AnnualActivityPage;
import com.ihg.automation.selenium.servicecenter.pages.deposit.CreateDepositPopUp;
import com.ihg.automation.selenium.servicecenter.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsPage;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.RewardClubPage;
import com.ihg.crm.automation.selenium.tests.servicecenter.LoginLogout;

public class DepositForClosedFrozenMemberTest extends LoginLogout
{
    private Member member = new Member();
    private Deposit testDeposit = new Deposit();
    private CreateDepositPopUp createDepositPopUp = new CreateDepositPopUp();

    @BeforeClass
    public void beforeClass()
    {
        member.setPersonalInfo(MemberPopulateHelper.getSimplePersonalInfo());
        member.addAddress(MemberPopulateHelper.getUnitedStatesAddress());
        member.addProgram(Program.RC);

        testDeposit.setTransactionId("DBWOLF");
        testDeposit.setTransactionType(CO_PARTNER);
        testDeposit.setTransactionName("PC MALL- WOLF CAMERA");
        testDeposit.setTransactionDescription("PC MALL- WOLF CAMERA");
        testDeposit.setLoyaltyUnitsType(Constant.RC_POINTS);
        testDeposit.setLoyaltyUnitsAmount("20000");

        login();
        member = new AnnualActivityPage().captureCounters(new EnrollmentPage().enroll(member), jdbcTemplate);
    }

    @Test
    public void setMemberStatus()
    {
        RewardClubPage rewardClubPage = new RewardClubPage();
        rewardClubPage.goTo();
        rewardClubPage.getSummary().setClosedStatus(ProgramStatusReason.FROZEN);
    }

    @Test(dependsOnMethods = { "setMemberStatus" })
    public void createDeposit()
    {
        createDepositPopUp.goTo();
        createDepositPopUp.setDepositData(testDeposit);
        createDepositPopUp.verifyTransactionInfo(testDeposit);
        createDepositPopUp
                .clickCreateDeposit(verifyError("RC Membership Status is Closed. This Transaction is not allowed."));
    }

    @Test(dependsOnMethods = { "createDeposit" }, alwaysRun = true)
    public void closeDepositPopUp()
    {
        createDepositPopUp.close();
    }

    @Test(dependsOnMethods = { "closeDepositPopUp" }, alwaysRun = true)
    public void verifyAccountData()
    {
        RewardClubPage rewardClubPage = new RewardClubPage();
        rewardClubPage.goTo();
        rewardClubPage.getSummary().verify(ProgramStatus.CLOSED, RewardClubLevel.CLUB);
        new LeftPanel().verifyBalance("0");
    }

    @Test(dependsOnMethods = { "verifyAccountData" }, alwaysRun = true)
    public void verifyCounters()
    {
        new AnnualActivityPage().verifyCounters(member);
    }

    @Test(dependsOnMethods = { "verifyCounters" }, alwaysRun = true)
    public void verifyCoPartnerEvent()
    {
        AllEventsPage allEventsPage = new AllEventsPage();
        allEventsPage.goTo();
        verifyThat(allEventsPage.getGrid().getRow(CO_PARTNER), displayed(false));
    }

}
