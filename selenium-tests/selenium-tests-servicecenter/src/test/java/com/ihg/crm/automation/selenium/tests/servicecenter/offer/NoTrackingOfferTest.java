package com.ihg.crm.automation.selenium.tests.servicecenter.offer;

import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyNoError;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.automation.selenium.matchers.component.HasTipText.hasInfoTipTextWithWait;

import java.util.Map;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.billing.EventBillingDetailsTab;
import com.ihg.automation.selenium.common.earning.EventEarningDetailsTab;
import com.ihg.automation.selenium.common.earning.EventEarningGridRow;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.stay.Stay;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.servicecenter.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.servicecenter.pages.event.offer.OfferEventPage;
import com.ihg.automation.selenium.servicecenter.pages.event.stay.AdjustStayPopUp;
import com.ihg.automation.selenium.servicecenter.pages.event.stay.StayEventsPage;
import com.ihg.crm.automation.selenium.tests.servicecenter.LoginLogout;

public class NoTrackingOfferTest extends LoginLogout
{
    private final static String OFFER_NO_TRACKING = "8051184";
    private final static String RULE_ID = "101213";
    private String baseLoyaltyUnits;
    private String ruleName;
    private StayEventsPage stayEventsPage;
    private Stay stay = new Stay();
    private AdjustStayPopUp adjustPopUp = new AdjustStayPopUp();
    private final static String BASE_LOYALTY_UNITS = " SELECT r.RULE_NM, a.RULE_ATTR_VAL FROM RULE.RULE r" //
            + " JOIN RULE.RULE_ATTR a ON r.RULE_KEY = a.RULE_KEY"//
            + " WHERE a.RULE_KEY=%s" //
            + " AND a.RULE_ATTR_TYP_CD='Billing.Amount.FlatFee'";

    @BeforeClass
    public void beforeClass()
    {
        Map<String, Object> map = jdbcTemplate.queryForMap(String.format(BASE_LOYALTY_UNITS, RULE_ID));
        baseLoyaltyUnits = map.get("RULE_ATTR_VAL").toString();
        ruleName = map.get("RULE_NM").toString();

        Member member = new Member();
        member.setPersonalInfo(MemberPopulateHelper.getSimplePersonalInfo());
        member.addAddress(MemberPopulateHelper.getUnitedStatesAddress());
        member.addProgram(Program.RC);

        stay.setHotelCode("LASVN");
        stay.setCheckInOutByNights(1);
        stay.setRateCode("IKDB2");
        stay.setAvgRoomRate(100.0);

        login();

        new EnrollmentPage().enroll(member);
        stayEventsPage = new StayEventsPage();
        stayEventsPage.createStay(stay);
    }

    @Test
    public void verifyOfferInEarningDetails()
    {
        stayEventsPage.getGuestGrid().getRow(1).getDetails().clickDetails();

        EventEarningDetailsTab earningDetailsTab = adjustPopUp.getEarningDetailsTab();
        earningDetailsTab.goTo();
        verifyThat(
                earningDetailsTab.getGrid().getRowByType(OFFER_NO_TRACKING)
                        .getCell(EventEarningGridRow.EventEarningCell.TYPE),
                hasInfoTipTextWithWait("Rule: " + RULE_ID + " - " + ruleName));
    }

    @Test(dependsOnMethods = "verifyOfferInEarningDetails")
    public void verifyOfferInBillingDetails()
    {
        EventBillingDetailsTab billingDetailsTab = adjustPopUp.getBillingDetailsTab();
        billingDetailsTab.goTo();

        billingDetailsTab.getEventBillingDetail(1).getBaseLoyaltyUnits().verifyUSDAmount(baseLoyaltyUnits,
                stay.getHotelCode());

        adjustPopUp.clickClose(verifyNoError());
    }

    @Test(dependsOnMethods = "verifyOfferInBillingDetails")
    public void verifyOfferNoTracking()
    {
        OfferEventPage offerPage = new OfferEventPage();
        offerPage.goTo();
        verifyThat(offerPage.getMemberOffersGrid().getRowByOfferCode(OFFER_NO_TRACKING), displayed(false));
    }
}
