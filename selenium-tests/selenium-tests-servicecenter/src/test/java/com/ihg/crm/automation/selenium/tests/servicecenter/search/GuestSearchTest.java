package com.ihg.crm.automation.selenium.tests.servicecenter.search;

import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyError;
import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyNoError;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasText;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.CustomerInfoPanel;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.servicecenter.pages.GuestSearch;
import com.ihg.automation.selenium.servicecenter.pages.LeftPanel;
import com.ihg.crm.automation.selenium.tests.servicecenter.LoginLogout;

public class GuestSearchTest extends LoginLogout
{
    private static final Logger logger = LoggerFactory.getLogger(GuestSearchTest.class);

    @Value("${memberID}")
    private String VALID_MEMBERSHIP_ID;

    private static final String INVALID_MEMBERSHIP_ID = "123456789";

    private static final String ERROR_INVALID_MEMBERID = "Membership Number invalid";
    private static final String ERROR_MORE_INFO = "Sorry, but the search you are performing requires that you "
            + "enter more information. Please review the form, add additional information, and then try again.";

    private GuestSearch guestSearch = new GuestSearch();

    @BeforeClass
    public void before()
    {
        login();
    }

    @Test
    public void defaultSearchCriteria()
    {
        getWelcome().click();

        guestSearch.getCustomerSearch().verifyDefault();
    }

    @Test
    public void searchByInvalidMembershipId()
    {
        guestSearch.byMemberNumber(INVALID_MEMBERSHIP_ID, verifyError(ERROR_INVALID_MEMBERID));
    }

    @DataProvider(name = "getName")
    public Object[][] getName()
    {
        return new Object[][] { { "Test", "Test" },
                { "1229baa50748245db216a6892507f268e4576286", "7da6f81dfd40caf7de01a52f" } };
    }

    @Test(dataProvider = "getName")
    public void searchByInvalidName(String lastName, String firstName)
    {
        guestSearch.byName(lastName, firstName, verifyError(ERROR_MORE_INFO));
    }

    @Test
    public void searchByValidMemberIdWithMemberNumber()
    {
        guestSearch.byMemberNumber(VALID_MEMBERSHIP_ID, verifyNoError());
        verifyMemberAndGoBack();
    }

    @Test
    public void searchByValidMemberIdWithNumber()
    {
        guestSearch.byNumber(VALID_MEMBERSHIP_ID, verifyNoError());
        verifyMemberAndGoBack();
    }

    private void verifyMemberAndGoBack()
    {
        logger.info("verify guest by membership id");
        verifyThat(new CustomerInfoPanel().getMemberNumber(Program.RC), hasText(VALID_MEMBERSHIP_ID));

        logger.info("back to welcome screen");
        new LeftPanel().clickNewSearch();
    }
}
