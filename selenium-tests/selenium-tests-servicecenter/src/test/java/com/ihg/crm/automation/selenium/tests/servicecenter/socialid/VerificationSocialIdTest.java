package com.ihg.crm.automation.selenium.tests.servicecenter.socialid;

import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyError;
import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyNoError;
import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifySuccess;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.personal.SocialIdType.INSTAGRAM;
import static com.ihg.automation.selenium.common.personal.SocialIdType.PINTEREST;
import static com.ihg.automation.selenium.common.personal.SocialIdType.TWITTER;
import static com.ihg.automation.selenium.common.types.program.ProgramStatus.OPEN;
import static com.ihg.automation.selenium.common.types.program.ProgramStatus.REMOVED;
import static com.ihg.automation.selenium.common.types.program.ProgramStatusReason.FROZEN;
import static com.ihg.automation.selenium.gwt.components.utils.ByStringText.contains;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasDefault;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasEmptyText;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasText;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.isHighlightedAsInvalid;
import static com.ihg.automation.selenium.matchers.component.GridSize.size;
import static com.ihg.automation.selenium.matchers.component.HasSelectItems.hasSelectItems;
import static com.ihg.automation.selenium.matchers.component.ViewableMatcher.hasTextInView;
import static com.ihg.automation.selenium.servicecenter.pages.history.AuditConstant.ContactInformation.CONTACT_INFORMATION;
import static com.ihg.automation.selenium.servicecenter.pages.history.AuditConstant.ContactInformation.USERNAME_HANDLE;
import static com.ihg.automation.selenium.servicecenter.pages.personal.PersonalInfoPage.SUCCESS_MESSAGE;
import static com.ihg.automation.selenium.servicecenter.pages.personal.SocialIdGridRow.SocialIdCell.DELETE;
import static com.ihg.automation.selenium.servicecenter.pages.personal.SocialIdGridRow.SocialIdCell.TYPE;
import static org.hamcrest.CoreMatchers.allOf;
import static org.hamcrest.core.IsNot.not;

import java.util.ArrayList;
import java.util.Arrays;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.gwt.components.dialog.ConfirmDialog;
import com.ihg.automation.selenium.gwt.components.input.Input;
import com.ihg.automation.selenium.matchers.component.HasText;
import com.ihg.automation.selenium.servicecenter.pages.account.AccountInfoPage;
import com.ihg.automation.selenium.servicecenter.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.servicecenter.pages.history.AuditConstant;
import com.ihg.automation.selenium.servicecenter.pages.history.ProfileHistoryGridRow;
import com.ihg.automation.selenium.servicecenter.pages.history.ProfileHistoryPage;
import com.ihg.automation.selenium.servicecenter.pages.personal.AddSocialIdPopUp;
import com.ihg.automation.selenium.servicecenter.pages.personal.EditSocialIdPopUp;
import com.ihg.automation.selenium.servicecenter.pages.personal.PersonalInfoPage;
import com.ihg.automation.selenium.servicecenter.pages.personal.SocialId;
import com.ihg.automation.selenium.servicecenter.pages.personal.SocialIdGrid;
import com.ihg.automation.selenium.servicecenter.pages.personal.SocialIdGridRow;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.RewardClubPage;
import com.ihg.crm.automation.selenium.tests.servicecenter.LoginLogout;

@Test(groups = { "socialId" })
public class VerificationSocialIdTest extends LoginLogout
{
    private static final String TEST_NAME = "TestName";
    private static final String NEW_TEST_NAME = "NewTestName";
    private PersonalInfoPage persInfo = new PersonalInfoPage();
    private ArrayList<String> expectedSocialIdTypes = new ArrayList<>(Arrays.asList(INSTAGRAM, PINTEREST, TWITTER));

    @BeforeClass
    public void beforeClass()
    {
        Member member = new Member();
        member.addProgram(Program.RC);
        member.setPersonalInfo(MemberPopulateHelper.getSimplePersonalInfo());
        member.addAddress(MemberPopulateHelper.getUnitedStatesAddress());

        login();

        new EnrollmentPage().enroll(member);
    }

    @DataProvider(name = "socialIdProvider")
    protected Object[][] socialIdProvider()
    {
        return new Object[][] { { INSTAGRAM }, { PINTEREST }, { TWITTER } };
    }

    @Test
    public void validateSocialIdFields()
    {
        SocialId socialId = persInfo.getSocialId();
        verifyThat(socialId.getAddSocial(), displayed(true));

        SocialIdGrid socialIdGrid = socialId.getSocialIdGrid();
        verifyThat(socialIdGrid, size(0));
        socialIdGrid.verifyHeader();
    }

    @Test(dependsOnMethods = { "validateSocialIdFields" }, alwaysRun = true)
    public void validateSocialIdPopUp()
    {
        persInfo.getSocialId().getAddSocial().clickAndWait(verifyNoError());

        AddSocialIdPopUp addSocialIdPopUp = new AddSocialIdPopUp();
        verifyThat(addSocialIdPopUp, displayed(true));

        verifyThat(addSocialIdPopUp.getUsername(), hasDefault());
        verifyThat(addSocialIdPopUp.getType(), allOf(hasEmptyText(), hasSelectItems(expectedSocialIdTypes)));

        addSocialIdPopUp.clickClose(verifyNoError());
        verifyThat(addSocialIdPopUp, displayed(false));
    }

    @Test(dependsOnMethods = { "validateSocialIdPopUp" }, alwaysRun = true)
    public void tryToAddSocialIdWithoutMandatoryFields()
    {
        persInfo.getSocialId().getAddSocial().clickAndWait(verifyNoError());

        AddSocialIdPopUp addSocialIdPopUp = new AddSocialIdPopUp();
        addSocialIdPopUp.clickSave(verifyNoError());

        verifyThat(addSocialIdPopUp, displayed(true));
        verifyThat(addSocialIdPopUp.getUsername(), isHighlightedAsInvalid(true));
        verifyThat(addSocialIdPopUp.getType(), isHighlightedAsInvalid(true));

        addSocialIdPopUp.clickClose(verifyNoError());
    }

    @Test(dataProvider = "socialIdProvider", dependsOnMethods = {
            "tryToAddSocialIdWithoutMandatoryFields" }, alwaysRun = true)
    public void addSocialId(String type)
    {
        SocialId socialId = persInfo.getSocialId();
        socialId.getAddSocial().clickAndWait(verifyNoError());

        AddSocialIdPopUp addSocialIdPopUp = new AddSocialIdPopUp();
        addSocialIdPopUp.getType().select(type);
        addSocialIdPopUp.getUsername().type(TEST_NAME);
        addSocialIdPopUp.clickSave(verifySuccess(SUCCESS_MESSAGE));

        SocialIdGridRow socialIdRow = socialId.getSocialIdGrid().getRow(type);
        socialIdRow.verify(type, TEST_NAME);

        EditSocialIdPopUp editSocialIdPopUp = socialIdRow.getEditSocialIdPopUp();
        verifyThat(editSocialIdPopUp.getUsername(), hasText(TEST_NAME));
        verifyThat(editSocialIdPopUp.getType(), hasTextInView(type));
        editSocialIdPopUp.clickClose(verifyNoError());
    }

    @Test(dataProvider = "socialIdProvider", dependsOnMethods = { "addSocialId" }, alwaysRun = true)
    public void verifyHistoryAfterAddSocialId(String type)
    {
        ProfileHistoryPage historyPage = new ProfileHistoryPage();
        historyPage.goTo();
        verifyNoError();

        ProfileHistoryGridRow socialIdRow = historyPage.getProfileHistoryGrid().getRow(1)
                .getSubRowByName(CONTACT_INFORMATION).getSubRowByName(contains(String.format("Social %s Added", type)));
        socialIdRow.getSubRowByName(AuditConstant.ContactInformation.TYPE).verifyAddAction(type);
        socialIdRow.getSubRowByName(USERNAME_HANDLE).verifyAddAction(TEST_NAME);
    }

    @Test(dataProvider = "socialIdProvider", dependsOnMethods = { "verifyHistoryAfterAddSocialId" }, alwaysRun = true)
    public void tryToAddDuplicateSocialId(String type)
    {
        persInfo.goTo();
        SocialId socialId = persInfo.getSocialId();
        socialId.getAddSocial().clickAndWait(verifyNoError());

        AddSocialIdPopUp addSocialIdPopUp = new AddSocialIdPopUp();
        addSocialIdPopUp.getType().select(type);
        addSocialIdPopUp.getUsername().type(NEW_TEST_NAME);
        addSocialIdPopUp.clickSave(verifyError(String.format("Member already has a Social ID of %s type.", type)));

        addSocialIdPopUp.clickClose(verifyNoError());
    }

    @Test(dependsOnMethods = { "tryToAddDuplicateSocialId" }, alwaysRun = true)
    public void updateSocialIdWithoutChanges()
    {
        SocialId socialId = persInfo.getSocialId();
        EditSocialIdPopUp editSocialIdPopUp = socialId.getSocialIdGrid().getRow(INSTAGRAM).getEditSocialIdPopUp();
        editSocialIdPopUp.clickSave(verifyNoError(), verifySuccess(SUCCESS_MESSAGE));
        socialId.getSocialIdGrid().getRow(INSTAGRAM).verify(INSTAGRAM, TEST_NAME);
    }

    @Test(dependsOnMethods = { "updateSocialIdWithoutChanges" }, alwaysRun = true)
    public void tryToUpdateSocialIdWithoutMandatoryField()
    {
        EditSocialIdPopUp editSocialIdPopUp = persInfo.getSocialId().getSocialIdGrid().getRow(1).getEditSocialIdPopUp();

        Input username = editSocialIdPopUp.getUsername();
        username.clear();
        editSocialIdPopUp.clickSave();

        verifyThat(editSocialIdPopUp, displayed(true));
        verifyThat(username, isHighlightedAsInvalid(true));

        editSocialIdPopUp.clickClose(verifyNoError());
    }

    @Test(dependsOnMethods = { "tryToUpdateSocialIdWithoutMandatoryField" }, alwaysRun = true)
    public void updateSocialId()
    {
        SocialId socialId = persInfo.getSocialId();
        EditSocialIdPopUp editSocialIdPopUp = socialId.getSocialIdGrid().getRow(INSTAGRAM).getEditSocialIdPopUp();
        editSocialIdPopUp.getUsername().type(NEW_TEST_NAME);
        editSocialIdPopUp.clickSave(verifyNoError(), verifySuccess(SUCCESS_MESSAGE));
        socialId.getSocialIdGrid().getRow(INSTAGRAM).verify(INSTAGRAM, NEW_TEST_NAME);
    }

    @Test(dependsOnMethods = { "updateSocialId" }, alwaysRun = true)
    public void verifyHistoryAfterUpdateSocialId()
    {
        ProfileHistoryPage historyPage = new ProfileHistoryPage();
        historyPage.goTo();
        verifyNoError();

        ProfileHistoryGridRow socialIdRow = historyPage.getProfileHistoryGrid().getRow(1)
                .getSubRowByName(CONTACT_INFORMATION)
                .getSubRowByName(contains(String.format("Social %s Changed", INSTAGRAM)));
        socialIdRow.getSubRowByName(USERNAME_HANDLE).verify(TEST_NAME, NEW_TEST_NAME);
    }

    @Test(dependsOnMethods = { "verifyHistoryAfterUpdateSocialId" }, alwaysRun = true)
    public void verifySocialIdForRemovedAccount()
    {
        AccountInfoPage accountInfo = new AccountInfoPage();
        accountInfo.goTo();
        accountInfo.clickRemoveAccount();
        verifyThat(accountInfo.getAccountStatus(), HasText.hasText(REMOVED));

        persInfo.goTo();
        SocialId socialId = persInfo.getSocialId();
        verifyThat(socialId, displayed(true));
        verifyThat(socialId.getAddSocial(), displayed(false));
        verifyThat(socialId.getSocialIdGrid(), size(not(0)));
        SocialIdGridRow row = socialId.getSocialIdGrid().getRow(1);
        verifyThat(row.getCell(TYPE).asLink(), displayed(false));
        verifyThat(row.getCell(DELETE), displayed(false));

        accountInfo.goTo();
        accountInfo.clickReinstateAccount();
        verifyThat(accountInfo.getAccountStatus(), HasText.hasText(OPEN));
    }

    @Test(dependsOnMethods = { "verifySocialIdForRemovedAccount" }, alwaysRun = true)
    public void verifySocialIdForClosedMember()
    {
        RewardClubPage rwdPage = new RewardClubPage();
        rwdPage.goTo();
        rwdPage.getSummary().setClosedStatus(FROZEN);

        persInfo.goTo();
        SocialId socialId = persInfo.getSocialId();
        verifyThat(socialId, displayed(true));
        verifyThat(socialId.getAddSocial(), displayed(true));
        SocialIdGrid socialIdGrid = socialId.getSocialIdGrid();
        verifyThat(socialIdGrid, size(not(0)));
        SocialIdGridRow row = socialIdGrid.getRow(1);
        verifyThat(row.getCell(TYPE).asLink(), displayed(true));
        verifyThat(row.getCell(DELETE).asLink(), displayed(true));

        rwdPage.goTo();
        rwdPage.getSummary().setOpenStatus();
    }

    @Test(dependsOnMethods = { "verifySocialIdForClosedMember" }, alwaysRun = true)
    public void cancelDeletingSocialId()
    {
        persInfo.goTo();
        SocialId socialId = persInfo.getSocialId();
        socialId.getSocialIdGrid().getRow(INSTAGRAM).clickDelete();

        ConfirmDialog confirmDialog = new ConfirmDialog();
        verifyThat(confirmDialog, allOf(displayed(true), hasText("Are you sure you want to delete the Social ID?")));
        confirmDialog.clickNo(verifyNoError());
        verifyThat(confirmDialog, displayed(false));

        verifyThat(socialId.getSocialIdGrid().getRow(INSTAGRAM), displayed(true));
    }

    @Test(dependsOnMethods = { "cancelDeletingSocialId" }, alwaysRun = true)
    public void deleteSocialId()
    {
        SocialId socialId = persInfo.getSocialId();
        socialId.getSocialIdGrid().getRow(INSTAGRAM).clickDelete();

        ConfirmDialog confirmDialog = new ConfirmDialog();
        confirmDialog.clickYes(verifyNoError(), verifySuccess(SUCCESS_MESSAGE));
        verifyThat(confirmDialog, displayed(false));

        verifyThat(socialId.getSocialIdGrid().getRow(INSTAGRAM), displayed(false));
    }

    @Test(dependsOnMethods = { "deleteSocialId" }, alwaysRun = true)
    public void verifyHistoryAfterDeleteSocialId()
    {
        ProfileHistoryPage historyPage = new ProfileHistoryPage();
        historyPage.goTo();
        verifyNoError();

        ProfileHistoryGridRow socialIdRow = historyPage.getProfileHistoryGrid().getRow(1)
                .getSubRowByName(CONTACT_INFORMATION)
                .getSubRowByName(contains(String.format("Social %s Deleted", INSTAGRAM)));
        socialIdRow.getSubRowByName(AuditConstant.ContactInformation.TYPE).verifyDeleteAction(INSTAGRAM);
        socialIdRow.getSubRowByName(USERNAME_HANDLE).verifyDeleteAction(NEW_TEST_NAME);
    }

}
