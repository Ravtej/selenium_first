package com.ihg.crm.automation.selenium.tests.servicecenter.enrollment.dr;

import static com.ihg.automation.common.DateUtils.DATE_FORMAT;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.event.EarningEventFactory.getAwardEvent;
import static com.ihg.automation.selenium.common.event.EarningEventFactory.getProgramEventWithoutStatus;
import static com.ihg.automation.selenium.common.event.EventTransactionType.Membership.ENROLL;
import static com.ihg.automation.selenium.common.event.EventTransactionType.Offer.OFFER_REGISTRATION;
import static com.ihg.automation.selenium.common.event.EventTransactionType.Order.ORDER_SYSTEM;
import static com.ihg.automation.selenium.common.member.DiningRewardAward.FUL_WELCOME_TO_PLATINUM;
import static com.ihg.automation.selenium.common.member.MemberJdbcUtils.UPDATE_EXP;
import static com.ihg.automation.selenium.common.offer.OfferFactory.DR_FREE_NIGHT;
import static com.ihg.automation.selenium.common.types.program.Program.DR;
import static com.ihg.automation.selenium.matchers.component.IsDisplayed.isDisplayedWithWait;
import static org.apache.commons.lang.StringUtils.EMPTY;

import org.joda.time.LocalDate;
import org.springframework.beans.factory.annotation.Value;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.common.DateUtils;
import com.ihg.automation.selenium.common.Constant;
import com.ihg.automation.selenium.common.catalog.CatalogItem;
import com.ihg.automation.selenium.common.earning.EventEarningDetailsTab;
import com.ihg.automation.selenium.common.event.AllEventsRowConverter;
import com.ihg.automation.selenium.common.event.AllEventsRowFactory;
import com.ihg.automation.selenium.common.event.EarningEvent;
import com.ihg.automation.selenium.common.freenight.FreeNight;
import com.ihg.automation.selenium.common.freenight.VoucherRedemption;
import com.ihg.automation.selenium.common.member.DiningRewardAward;
import com.ihg.automation.selenium.common.member.MemberJdbcUtils;
import com.ihg.automation.selenium.common.offer.GuestOffer;
import com.ihg.automation.selenium.common.order.Order;
import com.ihg.automation.selenium.common.order.OrderFactory;
import com.ihg.automation.selenium.common.pages.CreditCardType;
import com.ihg.automation.selenium.common.payment.CreditCardPayment;
import com.ihg.automation.selenium.common.payment.PaymentDetails;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.common.types.program.ProgramStatus;
import com.ihg.automation.selenium.common.types.program.RewardClubLevel;
import com.ihg.automation.selenium.gwt.components.Mode;
import com.ihg.automation.selenium.servicecenter.pages.GuestSearch;
import com.ihg.automation.selenium.servicecenter.pages.Role;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsGrid;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsGridRow;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsPage;
import com.ihg.automation.selenium.servicecenter.pages.event.all.OfferGridRowContainer;
import com.ihg.automation.selenium.servicecenter.pages.event.enroll.MembershipDetailsPopUp;
import com.ihg.automation.selenium.servicecenter.pages.event.enroll.MembershipGridRowContainer;
import com.ihg.automation.selenium.servicecenter.pages.event.freenight.FreeNightDetailsPopUp;
import com.ihg.automation.selenium.servicecenter.pages.event.freenight.FreeNightEventPage;
import com.ihg.automation.selenium.servicecenter.pages.event.freenight.FreeNightGridRow;
import com.ihg.automation.selenium.servicecenter.pages.event.offer.MemberOfferGridRow;
import com.ihg.automation.selenium.servicecenter.pages.event.offer.OfferEventPage;
import com.ihg.automation.selenium.servicecenter.pages.event.order.OrderSystemDetailsGridRowContainer;
import com.ihg.automation.selenium.servicecenter.pages.programs.dr.DiningRewardsPage;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.RewardClubPage;

public class EnrollmentByCreditCardTest extends EnrollToDrCommon
{
    @Value("${memberByCreditCardCN}")
    protected String memberByCreditCardCN;

    @Value("${memberByCreditCardCN.date}")
    protected String date;

    private LocalDate enrollDate;
    private String enrollDateToString;
    private GuestOffer guestOffer;
    protected static final CatalogItem ENROLMENT_AWARD = DiningRewardAward.ENROLL_20_RMB1988_BIG_SALE;
    private AllEventsPage allEventsPage = new AllEventsPage();
    private MembershipDetailsPopUp membershipDetailsPopUp = new MembershipDetailsPopUp();
    private FreeNight freeNight = new FreeNight();
    private VoucherRedemption voucherRedemption = new VoucherRedemption();
    private CreditCardPayment creditCardItem = new CreditCardPayment(ENROLMENT_AWARD, "6240008631401148",
            CreditCardType.CHINA_UNION_PAY);

    @BeforeClass
    private void beforeClass()
    {
        MemberJdbcUtils.update(jdbcTemplateUpdate, UPDATE_EXP, memberByCreditCardCN, DR, 13);

        enrollDate = DateUtils.getStringToDate(date);
        enrollDateToString = enrollDate.toString(DATE_FORMAT);

        guestOffer = new GuestOffer(DR_FREE_NIGHT);
        guestOffer.setRegistrationDate(enrollDateToString);
        guestOffer.setReplaysCount("1");

        freeNight.setGuestOffer(guestOffer);
        freeNight.setAvailable(EMPTY);
        freeNight.setExpired("1");

        voucherRedemption.setStartBookDates(enrollDateToString);
        voucherRedemption.setEarningDate(enrollDateToString);
        voucherRedemption.setEndBookDates(enrollDate.plusMonths(13).toString(DateUtils.EXPIRATION_DATE_PATTERN));

        login(user15U, Role.AGENT_TIER2);

        new GuestSearch().byMemberNumber(memberByCreditCardCN);
    }

    @Test
    public void verifyAccountInformation()
    {
        validateAccountInformation();
    }

    @Test(dependsOnMethods = { "verifyAccountInformation" }, alwaysRun = true)
    public void verifyRewardsClubPage()
    {
        RewardClubPage rewardClubPage = new RewardClubPage();
        rewardClubPage.goTo();
        rewardClubPage.getSummary().verify(ProgramStatus.OPEN, RewardClubLevel.PLTN);
    }

    @Test(dependsOnMethods = { "verifyRewardsClubPage" }, alwaysRun = true)
    public void verifyDiningRewardClubPage()
    {
        validateDiningRewardClubPage(memberByCreditCardCN, EXP_DATE, enrollDate);
        PaymentDetails details = new DiningRewardsPage().getEnrollmentDetails().getPaymentDetails();
        details.verify(creditCardItem, Mode.VIEW);
    }

    @Test(dependsOnMethods = { "verifyDiningRewardClubPage" }, alwaysRun = true)
    public void openEnrollDREventDetails()
    {
        allEventsPage = new AllEventsPage();
        allEventsPage.goTo();
        AllEventsGridRow allEventsGridRow = allEventsPage.getGrid().getEnrollRow(DR);
        allEventsGridRow.verify(AllEventsRowFactory.getEnrollEvent(enrollDate, Program.DR));
        MembershipGridRowContainer membershipGridRowContainer = allEventsGridRow
                .expand(MembershipGridRowContainer.class);
        membershipGridRowContainer.clickDetails();
        membershipDetailsPopUp = new MembershipDetailsPopUp();
        verifyThat(membershipDetailsPopUp, isDisplayedWithWait());
    }

    @Test(dependsOnMethods = { "openEnrollDREventDetails" })
    public void verifyEnrollDREventBillingDetails()
    {
        membershipDetailsPopUp.getBillingDetailsTab().verifyBaseAwardUSDBilling(ENROLL, ENROLMENT_AWARD, user15U,
                enrollDate);
    }

    @Test(dependsOnMethods = { "verifyEnrollDREventBillingDetails" })
    public void verifyEnrollDREventEarningDetails()
    {
        EventEarningDetailsTab tab = membershipDetailsPopUp.getEarningDetailsTab();
        tab.goTo();

        tab.verifyBasicDetailsNoPointsAndMiles();

        EarningEvent rcEvent = new EarningEvent(DR_FREE_NIGHT.getCode(), "0", "0", Constant.RC_POINTS, enrollDate);
        tab.getGrid().verifyRowsFoundByType(getAwardEvent(ENROLMENT_AWARD, enrollDate),
                getProgramEventWithoutStatus(DR, FUL_WELCOME_TO_PLATINUM, enrollDate), rcEvent, rcEvent);

        membershipDetailsPopUp.close();
    }

    @Test(dependsOnMethods = { "verifyEnrollDREventEarningDetails" }, alwaysRun = true)
    public void verifySystemOrderEvent()
    {
        allEventsPage.goTo();
        AllEventsGrid grid = allEventsPage.getGrid();
        AllEventsGridRow row = grid.getRow(ORDER_SYSTEM);

        Order order = OrderFactory.getSystemOrder(FUL_WELCOME_TO_PLATINUM, null);
        order.setTransactionDate(enrollDateToString);
        order.getOrderItems().get(0).setDeliveryStatus(null);
        row.verify(AllEventsRowConverter.convert(order));
        row.expand(OrderSystemDetailsGridRowContainer.class).verifyWithShippingAsOnLeftPanel(order);
    }

    @Test(dependsOnMethods = { "verifySystemOrderEvent" }, alwaysRun = true)
    public void verifyOfferRegistrationEvent()
    {
        allEventsPage.goTo();
        AllEventsGrid grid = allEventsPage.getGrid();
        AllEventsGridRow row = grid.getRow(OFFER_REGISTRATION);
        row.verify(AllEventsRowFactory.getOfferRegistrationEvent(enrollDate, DR_FREE_NIGHT));

        row.expand(OfferGridRowContainer.class).getOfferDetails().verify(DR_FREE_NIGHT, helper);
    }

    @Test(dependsOnMethods = { "verifyOfferRegistrationEvent" }, alwaysRun = true)
    public void verifyOfferWon()
    {
        OfferEventPage offerEventPage = new OfferEventPage();
        offerEventPage.goTo();
        MemberOfferGridRow offerRow = offerEventPage.getMemberOffersGrid().getRowByOfferCode(DR_FREE_NIGHT);

        verifyThat(offerRow, isDisplayedWithWait());
        offerRow.verify(guestOffer);
    }

    @Test(dependsOnMethods = { "verifyOfferWon" }, alwaysRun = true)
    public void verifyFreeNightVoucher()
    {
        FreeNightEventPage freeNightEventPage = new FreeNightEventPage();
        freeNightEventPage.goTo();
        FreeNightGridRow row = freeNightEventPage.getFreeNightGrid().getRowByOfferCode(DR_FREE_NIGHT);
        row.verify(freeNight);

        FreeNightDetailsPopUp freeNightDetailsPopUp = row.getFreeNightDetails();
        freeNightDetailsPopUp.getFreeNightDetailsTab().getFreeNightVoucherGrid().getRow(1)
                .verifyBookingDates(voucherRedemption);
        freeNightDetailsPopUp.close();
    }

}
