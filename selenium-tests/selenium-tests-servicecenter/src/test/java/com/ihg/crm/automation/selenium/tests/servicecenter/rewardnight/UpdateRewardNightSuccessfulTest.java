package com.ihg.crm.automation.selenium.tests.servicecenter.rewardnight;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.event.EventTransactionType.RewardNight.REWARD_NIGHT;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.enabled;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasText;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.isDisplayedWithWait;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.common.DateUtils;
import com.ihg.automation.common.RandomUtils;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.reward.RewardNight;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.servicecenter.pages.LeftPanel;
import com.ihg.automation.selenium.servicecenter.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.servicecenter.pages.event.rewardnight.CreateRewardNightPopUp;
import com.ihg.automation.selenium.servicecenter.pages.event.rewardnight.RewardNightDetails;
import com.ihg.automation.selenium.servicecenter.pages.event.rewardnight.RewardNightDetailsPopUp;
import com.ihg.automation.selenium.servicecenter.pages.event.rewardnight.RewardNightEventGridRow;
import com.ihg.automation.selenium.servicecenter.pages.event.rewardnight.RewardNightEventPage;
import com.ihg.automation.selenium.servicecenter.pages.event.rewardnight.RewardNightEventRow;
import com.ihg.automation.selenium.servicecenter.pages.event.rewardnight.RewardNightGridRowContainer;
import com.ihg.automation.selenium.servicecenter.pages.unit.GoodwillPopUp;
import com.ihg.crm.automation.selenium.tests.servicecenter.LoginLogout;

public class UpdateRewardNightSuccessfulTest extends LoginLogout
{
    private Member member = new Member();
    private RewardNight rewardNight = new RewardNight();
    private RewardNightEventGridRow rewardNightEventGridRow;
    private RewardNightDetails details;
    private RewardNightEventPage rwrdNightEventPage = new RewardNightEventPage();
    private RewardNightGridRowContainer rewardNightGridRowContainer;
    private CreateRewardNightPopUp createRwrdNightPopUp = new CreateRewardNightPopUp();
    private RewardNightDetailsPopUp rewardNightDetailsPopUp = new RewardNightDetailsPopUp();
    private RewardNightEventRow rewardNightEventRow = new RewardNightEventRow();

    @BeforeClass
    public void beforeClass()
    {
        rewardNight.setCheckIn(DateUtils.getDateBackward(3));
        rewardNight.setCheckOut(DateUtils.getFormattedDate());
        String confNumber = RandomUtils.getRandomNumber(8, false);
        rewardNight.setConfirmationNumber(confNumber);
        rewardNight.setHotel("JEDMM");
        rewardNight.setNights("3");
        rewardNight.setNumberOfRooms("1");
        rewardNight.setRoomType("BBL");

        rewardNightEventRow.setTransactionType(REWARD_NIGHT);
        rewardNightEventRow.setCheckInDate(rewardNight.getCheckIn());
        rewardNightEventRow.setCheckOutDate(rewardNight.getCheckOut());
        rewardNightEventRow.setHotel(rewardNight.getHotel());
        rewardNightEventRow.setIhgUnits("-75,000");
        rewardNightEventRow.setNights("3");

        member.setPersonalInfo(MemberPopulateHelper.getSimplePersonalInfo());
        member.addAddress(MemberPopulateHelper.getUnitedStatesAddress());
        member.addProgram(Program.RC);

        login();
        new EnrollmentPage().enroll(member);
        new GoodwillPopUp().goodwillPoints("50000");
        new GoodwillPopUp().goodwillPoints("50000");
    }

    @Test
    public void createRewardNight()
    {
        rwrdNightEventPage.createRewardNight(rewardNight);
    }

    @Test(dependsOnMethods = { "createRewardNight" }, alwaysRun = true)
    public void verifyPoints()
    {
        new LeftPanel().verifyBalance("25,000");
    }

    @Test(dependsOnMethods = { "verifyPoints" }, alwaysRun = true)
    public void verifyRewardNightEventRow()
    {
        rwrdNightEventPage.goTo();
        rewardNightEventGridRow = rwrdNightEventPage.getGrid().getRow(REWARD_NIGHT);
        verifyThat(rewardNightEventGridRow, displayed(true));
    }

    @Test(dependsOnMethods = { "verifyRewardNightEventRow" }, alwaysRun = true)
    public void verifyRewardNightEventDetailsPopUpControls()
    {
        rewardNightGridRowContainer = rewardNightEventGridRow.expand(RewardNightGridRowContainer.class);

        rewardNightGridRowContainer.clickDetails();
        rewardNightDetailsPopUp = new RewardNightDetailsPopUp();
        verifyThat(rewardNightDetailsPopUp, isDisplayedWithWait());
        details = rewardNightDetailsPopUp.getRewardNightDetailsTab().getDetails();
        verifyThat(details.getCheckInDate(), enabled(true));
        verifyThat(details.getCheckOutDate(), enabled(true));
    }

    @Test(dependsOnMethods = { "verifyRewardNightEventDetailsPopUpControls" }, alwaysRun = true)
    public void updatingCheckInDateSettingTwoNights()
    {
        rewardNightDetailsPopUp = new RewardNightDetailsPopUp();
        rewardNightDetailsPopUp.getRewardNightDetailsTab().getDetails().updateCheckInDate(DateUtils.getDateBackward(2),
                rewardNightEventRow.getNights(), "2");
        rewardNightDetailsPopUp.getSaveChanges().clickAndWait();
        verifyThat(rewardNightDetailsPopUp, displayed(false));

        new LeftPanel().verifyBalance("50,000");
        verifyRewardNightDetails("2", "-50000");
    }

    @Test(dependsOnMethods = { "updatingCheckInDateSettingTwoNights" }, alwaysRun = true)
    public void updateCheckInDateSettingOneNight()
    {
        rewardNightEventGridRow = rwrdNightEventPage.getGrid().getRow(REWARD_NIGHT);
        rewardNightGridRowContainer = rewardNightEventGridRow.expand(RewardNightGridRowContainer.class);
        rewardNightGridRowContainer.clickDetails();

        rewardNightDetailsPopUp = new RewardNightDetailsPopUp();
        rewardNightDetailsPopUp.getRewardNightDetailsTab().getDetails().updateCheckInDate(DateUtils.getDateBackward(1),
                "2", "1");
        rewardNightDetailsPopUp.getSaveChanges().clickAndWait();
        verifyThat(rewardNightDetailsPopUp, displayed(false));

        new LeftPanel().verifyBalance("75,000");
        verifyRewardNightDetails("1", "-25000");
    }

    private void verifyRewardNightDetails(String nightsAmount, String loyaltyUnits)
    {
        rewardNightEventGridRow = rwrdNightEventPage.getGrid().getRow(REWARD_NIGHT);
        rewardNightGridRowContainer = rewardNightEventGridRow.expand(RewardNightGridRowContainer.class);
        rewardNightGridRowContainer.clickDetails();
        rewardNightDetailsPopUp = new RewardNightDetailsPopUp();
        details = rewardNightDetailsPopUp.getRewardNightDetailsTab().getDetails();
        verifyThat(details.getNights(), hasText(nightsAmount));
        verifyThat(details.getLoyaltyUnits(), hasText(loyaltyUnits));
        rewardNightDetailsPopUp.close();
    }

}
