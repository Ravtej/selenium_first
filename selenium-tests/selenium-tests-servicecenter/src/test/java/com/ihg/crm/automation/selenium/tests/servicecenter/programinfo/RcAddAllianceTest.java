package com.ihg.crm.automation.selenium.tests.servicecenter.programinfo;

import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyError;
import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifySuccess;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.Constant.RC_POINTS;
import static com.ihg.automation.selenium.matchers.component.GridSize.size;
import static com.ihg.automation.selenium.matchers.component.HasSelectItems.hasSelectItems;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static com.ihg.automation.selenium.matchers.component.ViewableMatcher.hasTextInView;
import static org.hamcrest.CoreMatchers.containsString;

import java.util.Arrays;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.common.RandomUtils;
import com.ihg.automation.selenium.common.ProgramInfoPanel;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberAlliance;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.types.Alliance;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.gwt.components.dialog.ConfirmDialog;
import com.ihg.automation.selenium.gwt.components.input.Select;
import com.ihg.automation.selenium.servicecenter.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.AllianceAddPopUp;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.AllianceDetailsAdd;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.EarningPreference;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.RewardClubPage;
import com.ihg.crm.automation.selenium.tests.servicecenter.LoginLogout;

public class RcAddAllianceTest extends LoginLogout
{
    private Member member;
    private RewardClubPage rewardClubPage;

    private Alliance alliance = Alliance.JAL;
    private String allianceNumber = RandomUtils.getRandomNumber(9);

    @BeforeClass
    public void beforeClass()
    {
        member = new Member();
        member.addProgram(Program.RC);
        member.setPersonalInfo(MemberPopulateHelper.getSimplePersonalInfo());
        member.addAddress(MemberPopulateHelper.getUnitedStatesAddress());

        login();

        new EnrollmentPage().enroll(member);
    }

    @Test
    public void programInfoEarningPreferenceIsPoints()
    {
        rewardClubPage = new RewardClubPage();
        rewardClubPage.goTo();
        verifyThat(rewardClubPage.getEarningDetails().getEarningPreference(), hasTextInView(RC_POINTS));
    }

    @Test(dependsOnMethods = "programInfoEarningPreferenceIsPoints")
    public void addAlliance()
    {
        rewardClubPage.clickAddAlliance();
        AllianceAddPopUp allianceAddPopUp = new AllianceAddPopUp();
        AllianceDetailsAdd allianceDetails = allianceAddPopUp.getAllianceDetails();
        allianceDetails.getAlliance().selectByCode(alliance);
        allianceDetails.getAllianceNumber().type(allianceNumber);
        allianceAddPopUp.clickSave();
        rewardClubPage.getAlliances().getRow(1).verify(new MemberAlliance(alliance, allianceNumber, member.getName()));
    }

    @Test(dependsOnMethods = "addAlliance")
    public void leftPanelEarningPreferenceIsPoints()
    {
        verifyThat(new ProgramInfoPanel().getPrograms().getRewardClubProgram().getEarningPreference(),
                hasText("Points"));
    }

    @Test(dependsOnMethods = "addAlliance")
    public void earningPreferenceMilesAvailable()
    {
        EarningPreference earningDetails = rewardClubPage.getEarningDetails();
        earningDetails.clickEdit();
        verifyThat(earningDetails.getEarningPreference(),
                hasSelectItems(Arrays.asList(RC_POINTS, alliance.getValue())));
    }

    @Test(dependsOnMethods = "earningPreferenceMilesAvailable")
    public void updateEarningPreferenceToMiles()
    {
        EarningPreference earningDetails = rewardClubPage.getEarningDetails();
        Select earningPreference = earningDetails.getEarningPreference();
        earningPreference.selectByValue(alliance);
        earningDetails.clickSave();
        verifyThat(earningPreference, hasTextInView(alliance.getValue()));
        verifyThat(new ProgramInfoPanel().getPrograms().getRewardClubProgram().getEarningPreference(),
                hasText(alliance.getCode()));
    }

    @Test(dependsOnMethods = "updateEarningPreferenceToMiles")
    public void unableToDeleteAllianceForEarningPreference()
    {
        String errorMsg = "The Partner Miles are selected as default earning units. "
                + "Please update earning preferences in order to delete the selected Partner from the guest's profile.";
        rewardClubPage.getAlliances().getRow(alliance).delete(verifyError(errorMsg));
    }

    @Test(dependsOnMethods = "unableToDeleteAllianceForEarningPreference")
    public void updateEarningPreferenceToRcPoints()
    {
        EarningPreference earningDetails = rewardClubPage.getEarningDetails();
        earningDetails.clickEdit();
        Select earningPreference = earningDetails.getEarningPreference();
        earningPreference.select(RC_POINTS);
        earningDetails.clickSave();
        verifyThat(rewardClubPage.getEarningDetails().getEarningPreference().getView(), hasText(RC_POINTS));
        verifyThat(new ProgramInfoPanel().getPrograms().getRewardClubProgram().getEarningPreference(),
                hasText("Points"));
    }

    @Test(dependsOnMethods = "updateEarningPreferenceToRcPoints")
    public void deleteAllianceClickNo()
    {
        rewardClubPage.getAlliances().getRow(alliance).delete();
        ConfirmDialog dialog = new ConfirmDialog();
        verifyThat(dialog, hasText("Are you sure you want to delete the JAL alliance?"));
        dialog.clickNo();
        verifyThat(rewardClubPage.getAlliances(), hasText(containsString(alliance.getCodeWithValue())));
    }

    @Test(dependsOnMethods = "deleteAllianceClickNo")
    public void deleteAllianceClickYes()
    {
        rewardClubPage.getAlliances().getRow(alliance).delete();
        new ConfirmDialog().clickYes(verifySuccess("Program info has been successfully saved."));
        verifyThat(rewardClubPage.getAlliances(), size(0));
    }

    @Test(dependsOnMethods = "deleteAllianceClickYes")
    public void earningPreferenceMilesNotAvailable()
    {
        EarningPreference earningDetails = rewardClubPage.getEarningDetails();
        earningDetails.clickEdit();
        verifyThat(earningDetails.getEarningPreference(), hasSelectItems(Arrays.asList(RC_POINTS)));
    }
}
