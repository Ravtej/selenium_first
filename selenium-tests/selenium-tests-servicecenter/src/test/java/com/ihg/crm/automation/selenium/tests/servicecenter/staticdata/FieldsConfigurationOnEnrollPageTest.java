package com.ihg.crm.automation.selenium.tests.servicecenter.staticdata;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.automation.selenium.matchers.component.IsRequired.required;
import static org.hamcrest.CoreMatchers.allOf;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.components.Country;
import com.ihg.automation.selenium.common.contact.Address;
import com.ihg.automation.selenium.common.enroll.CustomerInfo;
import com.ihg.automation.selenium.common.name.PersonNameFields;
import com.ihg.automation.selenium.common.types.address.AddressType;
import com.ihg.automation.selenium.common.types.address.Locality1Label;
import com.ihg.automation.selenium.common.types.address.Locality2Label;
import com.ihg.automation.selenium.common.types.address.RegionLabel;
import com.ihg.automation.selenium.common.types.address.ZipLabel;
import com.ihg.automation.selenium.common.types.name.DegreeLabel;
import com.ihg.automation.selenium.common.types.name.GivenLabel;
import com.ihg.automation.selenium.common.types.name.MiddleLabel;
import com.ihg.automation.selenium.common.types.name.SalutationLabel;
import com.ihg.automation.selenium.common.types.name.SurnameLabel;
import com.ihg.automation.selenium.common.types.name.TitleLabel;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.servicecenter.pages.enroll.EnrollmentPage;
import com.ihg.crm.automation.selenium.tests.servicecenter.LoginLogout;

public class FieldsConfigurationOnEnrollPageTest extends LoginLogout
{
    private EnrollmentPage enrollPage = new EnrollmentPage();
    private CustomerInfo customerInfo;
    private Address addressField;
    private PersonNameFields nameField;

    @BeforeClass
    public void beforeClass()
    {
        login();

        enrollPage.goTo();
        enrollPage.getPrograms().selectProgram(Program.RC);
    }

    @Test()
    public void verifyNameConfigurationForBrazil()
    {
        customerInfo = enrollPage.getCustomerInfo();
        customerInfo.getResidenceCountry().select(Country.BR);

        nameField = customerInfo.getName();
        verifyThat(nameField.getSalutation(SalutationLabel.SALUTATION), allOf(displayed(true), required(false)));
        verifyThat(nameField.getSurname(SurnameLabel.FAMILY), allOf(displayed(true), required(true)));
        verifyThat(nameField.getGivenName(GivenLabel.FIRST), allOf(displayed(true), required(true)));
        verifyThat(nameField.getMiddleName(MiddleLabel.MIDDLE), allOf(displayed(true), required(false)));
        verifyThat(nameField.getDegree(DegreeLabel.DEGREE), allOf(displayed(true), required(false)));
        verifyThat(nameField.getSuffix(), allOf(displayed(true), required(false)));

        verifyThat(nameField.getTitle(TitleLabel.SURNAME_2ND), displayed(false));
        verifyThat(nameField.getTitle(TitleLabel.TITLE), displayed(false));
        verifyThat(nameField.getNickName(), displayed(false));
    }

    @Test(dependsOnMethods = { "verifyNameConfigurationForBrazil" }, alwaysRun = true)
    public void verifyAddressConfigurationForBrazilBusiness()
    {
        addressField = enrollPage.getAddress();
        addressField.getType().selectByValue(AddressType.BUSINESS);

        verifyThat(addressField.getAddress1(), allOf(displayed(true), required(true)));
        verifyThat(addressField.getCompanyName(), allOf(displayed(true), required(false)));
        verifyThat(addressField.getRegion1(RegionLabel.DISTRICT), allOf(displayed(true), required(false)));
        verifyThat(addressField.getRegion1(RegionLabel.STATE), allOf(displayed(true), required(false)));
        verifyThat(addressField.getLocality1(Locality1Label.CITY), allOf(displayed(true), required(true)));
        verifyThat(addressField.getZipCode(ZipLabel.ZIP), allOf(displayed(true), required(false)));

        verifyThat(addressField.getAddress2(), displayed(false));
        verifyThat(addressField.getAddress3(), displayed(false));
        verifyThat(addressField.getAddress4(), displayed(false));
        verifyThat(addressField.getAddress5(), displayed(false));
        verifyThat(addressField.getLocality2(Locality2Label.NEIGHBORHOOD), displayed(false));
    }

    @Test(dependsOnMethods = { "verifyAddressConfigurationForBrazilBusiness" }, alwaysRun = true)
    public void verifyAddressConfigurationForBrazilResidence()
    {
        addressField.getType().selectByValue(AddressType.RESIDENCE);

        verifyThat(addressField.getAddress1(), allOf(displayed(true), required(true)));
        verifyThat(addressField.getRegion1(RegionLabel.DISTRICT), allOf(displayed(true), required(false)));
        verifyThat(addressField.getRegion1(RegionLabel.STATE), allOf(displayed(true), required(false)));
        verifyThat(addressField.getLocality1(Locality1Label.CITY), allOf(displayed(true), required(true)));
        verifyThat(addressField.getZipCode(ZipLabel.ZIP), allOf(displayed(true), required(false)));

        verifyThat(addressField.getCompanyName(), displayed(false));
        verifyThat(addressField.getAddress2(), displayed(false));
        verifyThat(addressField.getAddress3(), displayed(false));
        verifyThat(addressField.getAddress4(), displayed(false));
        verifyThat(addressField.getAddress5(), displayed(false));
        verifyThat(addressField.getLocality2(Locality2Label.NEIGHBORHOOD), displayed(false));
    }

    @Test(dependsOnMethods = { "verifyAddressConfigurationForBrazilResidence" }, alwaysRun = true)
    public void verifyNameConfigurationForEritrea()
    {
        customerInfo.getResidenceCountry().select(Country.ER);

        nameField = customerInfo.getName();
        verifyThat(nameField.getSalutation(SalutationLabel.SALUTATION), allOf(displayed(true), required(false)));
        verifyThat(nameField.getSurname(SurnameLabel.LAST), allOf(displayed(true), required(true)));
        verifyThat(nameField.getGivenName(GivenLabel.FIRST), allOf(displayed(true), required(true)));
        verifyThat(nameField.getMiddleName(MiddleLabel.MIDDLE), allOf(displayed(true), required(false)));

        verifyThat(nameField.getSuffix(), displayed(false));
        verifyThat(nameField.getTitle(TitleLabel.SURNAME_2ND), displayed(false));
        verifyThat(nameField.getTitle(TitleLabel.TITLE), displayed(false));
        verifyThat(nameField.getDegree(DegreeLabel.DEGREE), displayed(false));
        verifyThat(nameField.getDegree(DegreeLabel.HONORIFIC), displayed(false));
        verifyThat(nameField.getNickName(), displayed(false));
    }

    @Test(dependsOnMethods = { "verifyNameConfigurationForEritrea" }, alwaysRun = true)
    public void verifyAddressConfigurationForEritreaResidence()
    {
        addressField.getType().selectByValue(AddressType.RESIDENCE);

        verifyThat(addressField.getAddress1(), allOf(displayed(true), required(true)));
        verifyThat(addressField.getRegion1(RegionLabel.REGION), allOf(displayed(true), required(false)));
        verifyThat(addressField.getLocality1(Locality1Label.CITY_TOWN), allOf(displayed(true), required(true)));

        verifyThat(addressField.getCompanyName(), displayed(false));
        verifyThat(addressField.getAddress2(), displayed(false));
        verifyThat(addressField.getAddress3(), displayed(false));
        verifyThat(addressField.getAddress4(), displayed(false));
        verifyThat(addressField.getAddress5(), displayed(false));
        verifyThat(addressField.getLocality2(Locality2Label.NEIGHBORHOOD), displayed(false));
        verifyThat(addressField.getZipCode(ZipLabel.POSTAL), displayed(false));
        verifyThat(addressField.getZipCode(ZipLabel.POST), displayed(false));
        verifyThat(addressField.getZipCode(ZipLabel.ZIP), displayed(false));
    }
}
