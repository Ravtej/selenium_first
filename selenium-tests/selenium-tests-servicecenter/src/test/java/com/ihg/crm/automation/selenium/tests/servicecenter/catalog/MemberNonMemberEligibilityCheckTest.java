package com.ihg.crm.automation.selenium.tests.servicecenter.catalog;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.GridSize.size;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.member.MemberJdbcUtils;
import com.ihg.automation.selenium.servicecenter.pages.GuestSearch;
import com.ihg.automation.selenium.servicecenter.pages.catalog.CatalogItemsGrid;
import com.ihg.automation.selenium.servicecenter.pages.catalog.CatalogPage;

public class MemberNonMemberEligibilityCheckTest extends CatalogCommon
{
    private CatalogPage catalogPage = new CatalogPage();
    private GuestSearch guestSearch = new GuestSearch();
    private static String memberId;
    private final static String ITEM_ID_NON_MEMBER_ELIGIBLE = "B2UCG";

    @BeforeClass
    public void beforeClass()
    {
        login();

        memberId = MemberJdbcUtils.getMemberId(jdbcTemplate, MEMBER_ID);

        catalogPage.goTo();
    }

    @Test
    public void checkWithoutMember()
    {
        verifySearchByItemId(ITEM_ID_NON_MEMBER_ELIGIBLE);
    }

    @Test(dependsOnMethods = { "checkWithoutMember" }, alwaysRun = true)
    public void checkWithMember()
    {
        guestSearch.byMemberNumber(memberId);

        catalogPage.goTo();
        catalogPage.searchByItemId(ITEM_ID_NON_MEMBER_ELIGIBLE);

        CatalogItemsGrid catalogGrid = catalogPage.getCatalogGrid();
        verifyThat(catalogGrid, size(0));
    }
}
