package com.ihg.crm.automation.selenium.tests.servicecenter.freenight;

import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyNoError;
import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifySuccess;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.isDisplayedWithWait;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static org.hamcrest.core.StringContains.containsString;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.common.DateUtils;
import com.ihg.automation.selenium.common.deposit.Deposit;
import com.ihg.automation.selenium.common.freenight.CancelReason;
import com.ihg.automation.selenium.common.freenight.FreeNight;
import com.ihg.automation.selenium.common.freenight.Redemption;
import com.ihg.automation.selenium.common.freenight.Status;
import com.ihg.automation.selenium.common.freenight.VoucherCancelReason;
import com.ihg.automation.selenium.common.freenight.VoucherRedemption;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.offer.GuestOffer;
import com.ihg.automation.selenium.common.stay.Stay;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.gwt.components.MessageBox;
import com.ihg.automation.selenium.gwt.components.MessageBoxType;
import com.ihg.automation.selenium.servicecenter.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.servicecenter.pages.event.freenight.BookFreeNightDetailsPopUp;
import com.ihg.automation.selenium.servicecenter.pages.event.freenight.CancelFreeNightBookingPopUp;
import com.ihg.automation.selenium.servicecenter.pages.event.freenight.CancelFreeNightPopUp;
import com.ihg.automation.selenium.servicecenter.pages.event.freenight.CancelFreeNightVoucherPopUp;
import com.ihg.automation.selenium.servicecenter.pages.event.freenight.FreeNightDetailsCancelTab;
import com.ihg.automation.selenium.servicecenter.pages.event.freenight.FreeNightDetailsPopUp;
import com.ihg.automation.selenium.servicecenter.pages.event.freenight.FreeNightDetailsTab;
import com.ihg.automation.selenium.servicecenter.pages.event.freenight.FreeNightEventPage;
import com.ihg.automation.selenium.servicecenter.pages.event.freenight.FreeNightGridRow;
import com.ihg.automation.selenium.servicecenter.pages.event.freenight.FreeNightGridRow.FreeNightCell;
import com.ihg.automation.selenium.servicecenter.pages.event.freenight.FreeNightVoucherGridRow;
import com.ihg.automation.selenium.servicecenter.pages.event.freenight.FreeNightVoucherGridRow.FreeNightVoucherCell;
import com.ihg.automation.selenium.servicecenter.pages.event.freenight.OfferDetailsFreeNightTab;
import com.ihg.automation.selenium.servicecenter.pages.event.offer.OfferDetailsViewBase;
import com.ihg.automation.selenium.servicecenter.pages.event.offer.OfferEventPage;
import com.ihg.automation.selenium.servicecenter.pages.event.offer.OfferPopUp;
import com.ihg.automation.selenium.servicecenter.pages.event.offer.OfferRegistrationPopUp;
import com.ihg.automation.selenium.servicecenter.pages.event.stay.StayEventsPage;

public class FreeNightsByOfferRegistrationTest extends FreeNightCommon
{
    private Member member = new Member();
    private Stay stay = new Stay();
    private OfferEventPage offerPage = new OfferEventPage();
    private OfferRegistrationPopUp offerRegisterPopUp;
    private OfferPopUp offerPopUp;
    private FreeNightEventPage freeNightPage = new FreeNightEventPage();
    private StayEventsPage stayPage = new StayEventsPage();
    private FreeNightGridRow offerRow;
    private FreeNightDetailsPopUp freeNightPopUp;
    private FreeNightVoucherGridRow voucherRow;
    private OfferDetailsFreeNightTab offerDetailsTab;
    private BookFreeNightDetailsPopUp bookFreeNightPopUp;
    private CancelFreeNightPopUp cancelFreeNightPopUp;
    private CancelFreeNightBookingPopUp cancelBookingPopUp;
    private Redemption redemption = new Redemption();
    private Deposit deposit = new Deposit();
    private OfferDetailsViewBase offerDetails;
    private FreeNight freeNight = new FreeNight();
    private VoucherRedemption voucherRedemptionAvailable = new VoucherRedemption();
    private VoucherRedemption voucherRedemptionRedeemed = new VoucherRedemption();
    private CancelFreeNightVoucherPopUp cancelFreeNightVoucherPopUp = new CancelFreeNightVoucherPopUp();

    private static final String OFFER_CODE = "8310441";
    private static final String STAY_HOTEL = "ABQMB";
    private static final String REDEMPTION_HOTEL = "ATLCP";
    private static final int NIGHTS = 2;
    private static final int DAYS_SHIFT = -7;
    private static final String CHECK_IN = DateUtils.getFormattedDate(DAYS_SHIFT);
    private static final String CHECK_OUT = DateUtils.getFormattedDate(DAYS_SHIFT + NIGHTS);
    private static final String RATE_CODE = "Test";
    private static final double AVG_AMOUNT = 100.00;
    private static final String TRANSACTION_ID = "BRCLYFR";
    private static final String NUMBER_OF_ROOMS = "1";
    private static final String CONFIRMATION_NUMBER = "12345678";

    @BeforeClass
    public void beforeClass()
    {
        member.setPersonalInfo(MemberPopulateHelper.getSimplePersonalInfo());
        member.addAddress(MemberPopulateHelper.getUnitedStatesAddress());
        member.addProgram(Program.RC);

        initStay();

        deposit.setTransactionId(TRANSACTION_ID);

        login();

        new EnrollmentPage().enroll(member);

        offerPage.goTo();
        offerPage.getSearchFields().getOfferCode().type(OFFER_CODE);
        offerPage.getButtonBar().clickSearch();
        offerRegisterPopUp = new OfferRegistrationPopUp();
        verifyThat(offerRegisterPopUp, isDisplayedWithWait());
        offerRegisterPopUp.clickRegister(
                verifySuccess(hasText(containsString("Congratulations guest has been successfully registered"))));

        offerPopUp = new OfferPopUp();
        verifyThat(offerPopUp, isDisplayedWithWait());

        offerDetails = offerPopUp.getOfferDetailsTab().getOfferDetails();
        freeNight.setGuestOffer(new GuestOffer(offerDetails.capture()));

        offerPopUp.close();

        initRedemption();

        voucherRedemptionAvailable.setStatus(Status.AVAILABLE);

        initVoucherRedemptionRedeemed();
    }

    @Test
    public void verifyOfferWinAmountAfterRegistration()
    {
        verifyOfferWinAmount(OFFER_CODE, "0");
    }

    @Test(dependsOnMethods = { "verifyOfferWinAmountAfterRegistration" }, alwaysRun = true)
    public void stayTwoNightsTest()
    {
        stayPage.createStay(stay);
        verifyOfferWinAmount(OFFER_CODE, "1");
    }

    @Test(dependsOnMethods = { "stayTwoNightsTest" }, alwaysRun = true)
    public void verifyFreeNightTabControls()
    {
        freeNightPage.goTo();
        offerRow = freeNightPage.getFreeNightGrid().getRowByOfferCode(OFFER_CODE);
        verifyThat(offerRow, displayed(true));
        offerRow.verify(freeNight);
        verifyThat(offerRow.getCell(FreeNightCell.OFFER_NAME).asLink(), displayed(true));
    }

    @Test(dependsOnMethods = { "verifyFreeNightTabControls" }, alwaysRun = true)
    public void openFreeNightPopUp()
    {
        freeNightPopUp = freeNightPage.getFreeNightGrid().getFreeNightDetailsPopUp(OFFER_CODE);
        verifyThat(freeNightPopUp, isDisplayedWithWait());
    }

    @Test(dependsOnMethods = { "openFreeNightPopUp" })
    public void verifyFreeNightVoucherTab()
    {
        FreeNightDetailsTab tab = freeNightPopUp.getFreeNightDetailsTab();
        tab.goTo();
        tab.verify(freeNight);
        voucherRow = tab.getFreeNightVoucherGrid().getRow(1);
        voucherRow.verify(voucherRedemptionAvailable);
    }

    @Test(dependsOnMethods = { "openFreeNightPopUp" })
    public void verifyOfferDetailsTab()
    {
        offerDetailsTab = freeNightPopUp.getOfferDetailsTab();
        offerDetailsTab.goTo();
        verifyThat(offerDetailsTab.getOfferDetails().getOfferDetails().getOfferCode(), hasText(OFFER_CODE));
    }

    @Test(dependsOnMethods = { "openFreeNightPopUp", "verifyOfferDetailsTab",
            "verifyFreeNightVoucherTab" }, alwaysRun = true)
    public void closeFreeNightPopUp()
    {
        freeNightPopUp.close();
    }

    @Test(dependsOnMethods = { "closeFreeNightPopUp" }, alwaysRun = true)
    public void verifyRowNotChanged()
    {
        offerRow = freeNightPage.getFreeNightGrid().getRowByOfferCode(OFFER_CODE);
        offerRow.verify(freeNight);
    }

    @Test(dependsOnMethods = { "verifyRowNotChanged" }, alwaysRun = true)
    public void bookFreeNightPopupChecking()
    {
        freeNightPopUp = freeNightPage.getFreeNightGrid().getFreeNightDetailsPopUp(OFFER_CODE);
        freeNightPopUp.clickBookFreeNightButton();

        bookFreeNightPopUp = new BookFreeNightDetailsPopUp();
        verifyThat(bookFreeNightPopUp, isDisplayedWithWait());
        bookFreeNightPopUp.verifyEnabledFields();
        bookFreeNightPopUp.close();
        verifyThat(freeNightPopUp, isDisplayedWithWait());
        freeNightPopUp.clickClose(verifyNoError());
    }

    @Test(dependsOnMethods = { "bookFreeNightPopupChecking" }, alwaysRun = true)
    public void bookFreeNightVoucher()
    {
        bookFreeNights(OFFER_CODE, redemption);
        verifyThat(freeNightPopUp, isDisplayedWithWait());
    }

    @Test(dependsOnMethods = { "bookFreeNightVoucher" }, alwaysRun = true)
    public void freeNightVoucherGridChecking()
    {
        voucherRow = freeNightPopUp.getFreeNightDetailsTab().getFreeNightVoucherGrid().getRow(1);

        voucherRow.verify(voucherRedemptionRedeemed);
        freeNightPopUp.close();
    }

    @Test(dependsOnMethods = { "freeNightVoucherGridChecking" }, alwaysRun = true)
    public void cancelFreeNight()
    {
        freeNightPopUp = freeNightPage.getFreeNightGrid().getFreeNightDetailsPopUp(OFFER_CODE);

        freeNightPopUp.getFreeNightDetailsTab().goTo();
        voucherRow = freeNightPopUp.getFreeNightDetailsTab().getFreeNightVoucherGrid().getRow(1);
        voucherRow.getCell(FreeNightVoucherCell.CONFIRMATION).clickByLabel();

        cancelFreeNightPopUp = new CancelFreeNightPopUp();
        verifyThat(cancelFreeNightPopUp, isDisplayedWithWait());
        FreeNightDetailsCancelTab tab = cancelFreeNightPopUp.getFreeNightDetailsCancelTab();
        tab.goTo();
        String certificateNumber = tab.getCertificateNumber().getText();
        cancelFreeNightPopUp.clickCancelFreeNightButton();

        cancelBookingPopUp = new CancelFreeNightBookingPopUp();
        verifyThat(cancelBookingPopUp, isDisplayedWithWait());
        verifyThat(cancelBookingPopUp.getBookingNumber(), hasText(certificateNumber));
        cancelBookingPopUp.cancelFreeNight(CancelReason.CANCEL);
        cancelFreeNightPopUp.close();

        voucherRow = freeNightPopUp.getFreeNightDetailsTab().getFreeNightVoucherGrid().getRow(1);
        voucherRow.verify(voucherRedemptionAvailable);
        freeNightPopUp.close();
    }

    @Test(dependsOnMethods = { "cancelFreeNight" }, alwaysRun = true)
    public void cancelFreeNightVoucher()
    {
        freeNightPage.goTo();
        freeNightPopUp = freeNightPage.getFreeNightGrid().getFreeNightDetailsPopUp(OFFER_CODE);

        FreeNightDetailsTab tab = freeNightPopUp.getFreeNightDetailsTab();
        tab.goTo();
        voucherRow = tab.getFreeNightVoucherGrid().getRow(1);
        voucherRow.getCell(FreeNightVoucherCell.STATUS).clickByLabel();

        verifyThat(cancelFreeNightVoucherPopUp, isDisplayedWithWait());
        cancelFreeNightVoucherPopUp.cancelFreeNightVoucher(VoucherCancelReason.EXCEPTION);
        verifyThat(new MessageBox(MessageBoxType.SUCCESS), hasText("Voucher has been successfully canceled."));

        voucherRow = tab.getFreeNightVoucherGrid().getRow(1);
        voucherRedemptionAvailable.setStatus(Status.CANCELED);
        voucherRow.verify(voucherRedemptionAvailable);

        freeNightPopUp.close();
    }

    private void initRedemption()
    {
        redemption.setConfirmationNumber(CONFIRMATION_NUMBER);
        redemption.setNumberOfRooms(NUMBER_OF_ROOMS);
        redemption.setHotel(REDEMPTION_HOTEL);
        redemption.setCheckIn(DateUtils.getFormattedDate(1));
        redemption.setCheckOut(DateUtils.getFormattedDate(2));
    }

    private void initStay()
    {
        stay.setHotelCode(STAY_HOTEL);
        stay.setCheckIn(CHECK_IN);
        stay.setCheckOut(CHECK_OUT);
        stay.setAvgRoomRate(AVG_AMOUNT);
        stay.setRateCode(RATE_CODE);
    }

    private void initVoucherRedemptionRedeemed()
    {
        voucherRedemptionRedeemed.setConfirmation(CONFIRMATION_NUMBER);
        voucherRedemptionRedeemed.setHotel(REDEMPTION_HOTEL);
        voucherRedemptionRedeemed.setStatus(Status.REDEEMED);
        voucherRedemptionRedeemed.setRedeemDate(DateUtils.getFormattedDate());
    }
}
