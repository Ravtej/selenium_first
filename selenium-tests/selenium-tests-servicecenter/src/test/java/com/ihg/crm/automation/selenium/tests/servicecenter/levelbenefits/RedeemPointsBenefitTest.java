package com.ihg.crm.automation.selenium.tests.servicecenter.levelbenefits;

import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyError;
import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifySuccess;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.event.EarningEventFactory.getAwardEvent;
import static com.ihg.automation.selenium.common.event.EarningEventFactory.getUnitEvent;
import static com.ihg.automation.selenium.common.event.EventTransactionType.TierLevel.BENEFIT_REDEEM;
import static com.ihg.automation.selenium.matchers.component.GridSize.size;
import static com.ihg.automation.selenium.matchers.component.IsDisplayed.displayed;
import static com.ihg.automation.selenium.matchers.component.ViewableMatcher.hasTextInView;
import static com.ihg.automation.selenium.servicecenter.pages.programs.rc.RewardClubSummary.NO_TIER_LEVEL_BENEFITS_MESSAGE;
import static org.hamcrest.Matchers.startsWith;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.common.DateUtils;
import com.ihg.automation.selenium.common.Constant;
import com.ihg.automation.selenium.common.billing.EventBillingDetailsTab;
import com.ihg.automation.selenium.common.earning.EventEarningDetailsTab;
import com.ihg.automation.selenium.common.event.AllEventsRow;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.common.types.program.RewardClubLevel;
import com.ihg.automation.selenium.common.types.program.RewardClubLevelReason;
import com.ihg.automation.selenium.gwt.components.dialog.ConfirmDialog;
import com.ihg.automation.selenium.servicecenter.pages.LeftPanel;
import com.ihg.automation.selenium.servicecenter.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsGridRow;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsPage;
import com.ihg.automation.selenium.servicecenter.pages.event.benefit.BenefitContainer;
import com.ihg.automation.selenium.servicecenter.pages.event.benefit.BenefitRedeemDetailsPopUp;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.BenefitsGrid;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.BenefitsGridRow;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.BenefitsGridRowContainer;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.RewardClubPage;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.RewardClubSummary;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.TierLevelBenefitsDetailsPopUp;

public class RedeemPointsBenefitTest extends LevelBenefitsCommon
{
    private BenefitsGridRow benefitPointsItemRow;
    private RewardClubSummary rewardClubSummary;
    private BenefitRedeemDetailsPopUp benefitRedeemDetailsPopUp;
    private BenefitContainer benefitRedeemContainer;

    @BeforeClass
    public void before()
    {
        Member member = new Member();
        member.addProgram(Program.RC);
        member.setPersonalInfo(MemberPopulateHelper.getSimplePersonalInfo());
        member.addAddress(MemberPopulateHelper.getUnitedStatesAddress());

        login();
        new EnrollmentPage().enroll(member);
    }

    @Test
    public void verifyNoLevelBenefitsForClub()
    {
        RewardClubPage rewardClubPage = new RewardClubPage();
        rewardClubPage.goTo();
        rewardClubSummary = rewardClubPage.getSummary();
        verifyThat(rewardClubSummary.getTierLevel(), hasTextInView(startsWith(RewardClubLevel.CLUB.getValue())));
        rewardClubSummary.getTierLevelBenefits().clickAndWait(verifyError(NO_TIER_LEVEL_BENEFITS_MESSAGE));
    }

    @Test(dependsOnMethods = { "verifyNoLevelBenefitsForClub" }, alwaysRun = true)
    public void verifyNoLevelBenefitsForGold()
    {
        rewardClubSummary.setTierLevelWithExpiration(RewardClubLevel.GOLD, RewardClubLevelReason.POINTS);
        verifyThat(rewardClubSummary.getTierLevel(), hasTextInView(startsWith(RewardClubLevel.GOLD.getValue())));
        rewardClubSummary.getTierLevelBenefits().clickAndWait(verifyError(NO_TIER_LEVEL_BENEFITS_MESSAGE));
    }

    @Test(dependsOnMethods = { "verifyNoLevelBenefitsForGold" }, alwaysRun = true)
    public void verifyNoLevelBenefitsForPlatinum()
    {
        rewardClubSummary.setTierLevelWithExpiration(RewardClubLevel.PLTN, RewardClubLevelReason.POINTS);
        verifyThat(rewardClubSummary.getTierLevel(), hasTextInView(startsWith(RewardClubLevel.PLTN.getValue())));
        rewardClubSummary.getTierLevelBenefits().clickAndWait(verifyError(NO_TIER_LEVEL_BENEFITS_MESSAGE));
    }

    @Test(dependsOnMethods = { "verifyNoLevelBenefitsForPlatinum" }, alwaysRun = true)
    public void verifyLevelBenefitsForSpire()
    {
        rewardClubSummary.setTierLevelWithExpiration(RewardClubLevel.SPRE, RewardClubLevelReason.POINTS);
        verifyThat(rewardClubSummary.getTierLevel(), hasTextInView(startsWith(RewardClubLevel.SPRE.getValue())));

        TierLevelBenefitsDetailsPopUp tierLevelBenefitsDetailsPopUp = rewardClubSummary
                .openTierLevelBenefitsDetailsPopUp();
        BenefitsGrid benefitsGrid = tierLevelBenefitsDetailsPopUp.getBenefitsGrid();
        verifyThat(benefitsGrid, size(2));

        BenefitsGridRow benefitGiftPlatinumItemRow = benefitsGrid.getRow(benefitGiftPlatinumItem);
        benefitGiftPlatinumItemRow.verifyBeforeRedeem(benefitGiftPlatinumItem);
        benefitGiftPlatinumItemRow.expand(BenefitsGridRowContainer.class)
                .verifyForPointsBenefit(benefitGiftPlatinumItem, Constant.NOT_AVAILABLE);

        benefitPointsItemRow = benefitsGrid.getRow(benefitPointsItem);
        benefitPointsItemRow.verifyBeforeRedeem(benefitPointsItem);
        benefitPointsItemRow.expand(BenefitsGridRowContainer.class).verifyForPointsBenefit(benefitPointsItem,
                Constant.NOT_AVAILABLE);
    }

    @Test(dependsOnMethods = { "verifyLevelBenefitsForSpire" }, alwaysRun = true)
    public void cancelRedeemPointBenefit()
    {
        benefitPointsItemRow.clickRedeem();

        ConfirmDialog dialog = new ConfirmDialog();
        verifyThat(dialog, displayed(true));
        dialog.clickNo();
        verifyThat(dialog, displayed(false));
    }

    @Test(dependsOnMethods = { "cancelRedeemPointBenefit" }, alwaysRun = true)
    public void redeemOnlyPointBenefit()
    {
        benefitPointsItemRow.clickRedeem();

        new ConfirmDialog().clickYes(verifySuccess(SUCCESS_BENEFIT_REDEEM_MESSAGE));

        TierLevelBenefitsDetailsPopUp tierLevelBenefitsDetailsPopUp = rewardClubSummary
                .openTierLevelBenefitsDetailsPopUp();
        BenefitsGrid benefitsGrid = tierLevelBenefitsDetailsPopUp.getBenefitsGrid();

        benefitPointsItemRow = benefitsGrid.getRow(benefitPointsItem);
        benefitPointsItemRow.verifyAfterRedeemForRedeemedBenefit(benefitPointsItem, DateUtils.getFormattedDate());
        benefitPointsItemRow.expand(BenefitsGridRowContainer.class).verifyForPointsBenefit(benefitPointsItem,
                DateUtils.getFormattedDate());

        BenefitsGridRow benefitGiftPlatinumItemRow = benefitsGrid.getRow(benefitGiftPlatinumItem);
        benefitGiftPlatinumItemRow.verifyAfterRedeemForNotRedeemedBenefit(benefitGiftPlatinumItem);
        benefitGiftPlatinumItemRow.expand(BenefitsGridRowContainer.class)
                .verifyForPointsBenefit(benefitGiftPlatinumItem, Constant.NOT_AVAILABLE);

        tierLevelBenefitsDetailsPopUp.close();

        new LeftPanel().verifyBalance(benefitPointsItem.getLoyaltyUnitCost());
    }

    @Test(dependsOnMethods = { "redeemOnlyPointBenefit" }, alwaysRun = true)
    public void verifyAllEventsBenefitRedeemEvent()
    {
        AllEventsPage allEventsPage = new AllEventsPage();
        allEventsPage.goTo();

        AllEventsRow benefitRedeemRowData = new AllEventsRow(DateUtils.getFormattedDate(), BENEFIT_REDEEM,
                benefitPointsItem.getItemName(), benefitPointsItem.getLoyaltyUnitCost(), "");
        AllEventsGridRow benefitRedeemRow = allEventsPage.getGrid().getRow(BENEFIT_REDEEM);
        benefitRedeemRow.verify(benefitRedeemRowData);

        benefitRedeemContainer = benefitRedeemRow.expand(BenefitContainer.class);
        benefitRedeemContainer.getBenefitRedeemDetails().verifyForPointsBenefit(benefitPointsItem, helper);
    }

    @Test(dependsOnMethods = { "verifyAllEventsBenefitRedeemEvent" }, alwaysRun = true)
    public void verifyBenefitRedeemEventDetails()
    {
        benefitRedeemContainer.clickDetails();

        benefitRedeemDetailsPopUp = new BenefitRedeemDetailsPopUp();
        verifyThat(benefitRedeemDetailsPopUp, displayed(true));
        benefitRedeemDetailsPopUp.getBenefitRedeemDetailsTab().getBenefitRedeemDetails()
                .verifyForPointsBenefit(benefitPointsItem, helper);

    }

    @Test(dependsOnMethods = { "verifyBenefitRedeemEventDetails" }, alwaysRun = true)
    public void verifyEarningDetails()
    {
        EventEarningDetailsTab eventEarningDetailsTab = benefitRedeemDetailsPopUp.getEarningDetailsTab();
        eventEarningDetailsTab.goTo();

        eventEarningDetailsTab.verifyBasicDetailsForPoints(benefitPointsItem.getLoyaltyUnitCost(), "0");
        eventEarningDetailsTab.getGrid().verifyRowsFoundByType(getAwardEvent(benefitPointsItem),
                getUnitEvent(benefitPointsItem));
    }

    @Test(dependsOnMethods = { "verifyEarningDetails" }, alwaysRun = true)
    public void verifyBillingDetails()
    {
        EventBillingDetailsTab billingDetailsTab = benefitRedeemDetailsPopUp.getBillingDetailsTab();
        billingDetailsTab.goTo();
        billingDetailsTab.verifyTabIsEmpty(BENEFIT_REDEEM);
    }
}
