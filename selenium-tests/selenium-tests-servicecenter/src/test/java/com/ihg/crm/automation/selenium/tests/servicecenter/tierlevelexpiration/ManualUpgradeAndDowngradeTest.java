package com.ihg.crm.automation.selenium.tests.servicecenter.tierlevelexpiration;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.Constant.NOT_AVAILABLE;
import static com.ihg.automation.selenium.common.types.program.ProgramStatus.OPEN;
import static com.ihg.automation.selenium.common.types.program.RewardClubLevel.CLUB;
import static com.ihg.automation.selenium.common.types.program.RewardClubLevel.GOLD;
import static com.ihg.automation.selenium.common.types.program.RewardClubLevel.SPRE;
import static com.ihg.automation.selenium.common.types.program.RewardClubLevelReason.BASE;
import static com.ihg.automation.selenium.common.types.program.TierLevelExpiration.EXPIRE_CURRENT_YEAR;
import static com.ihg.automation.selenium.matchers.component.ViewableMatcher.hasTextInView;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.servicecenter.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.RewardClubPage;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.RewardClubSummary;
import com.ihg.crm.automation.selenium.tests.servicecenter.LoginLogout;

public class ManualUpgradeAndDowngradeTest extends LoginLogout
{
    private RewardClubPage rewardClubPage;
    private RewardClubSummary summary;

    @BeforeClass
    public void beforeClass()
    {
        Member member = new Member();
        member.setPersonalInfo(MemberPopulateHelper.getSimplePersonalInfo());
        member.addAddress(MemberPopulateHelper.getUnitedStatesAddress());
        member.addProgram(Program.RC);

        login();
        new EnrollmentPage().enroll(member);

        rewardClubPage = new RewardClubPage();
        rewardClubPage.goTo();
        summary = rewardClubPage.getSummary();
        summary.setTierLevelWithExpiration(GOLD, BASE);

        summary.verify(OPEN, GOLD, 1);
    }

    @Test
    public void upgradeTierLevelToSpire()
    {
        summary.setTierLevelWithExpiration(SPRE, BASE, EXPIRE_CURRENT_YEAR);
        summary.verify(OPEN, SPRE, 0);
    }

    @Test(dependsOnMethods = { "upgradeTierLevelToSpire" }, alwaysRun = true)
    public void downgradeTierLevelToGold()
    {
        summary.setTierLevelWithExpiration(GOLD, BASE, EXPIRE_CURRENT_YEAR);
        summary.verify(OPEN, GOLD, 0);
    }

    @Test(dependsOnMethods = { "downgradeTierLevelToGold" }, alwaysRun = true)
    public void downgradeTierLevelToClub()
    {
        summary.setTierLevelWithoutExpiration(CLUB, BASE);

        summary.verify(OPEN, CLUB);
        verifyThat(summary.getTierLevelExpiration(), hasTextInView(NOT_AVAILABLE));
    }
}
