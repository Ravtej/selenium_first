package com.ihg.crm.automation.selenium.tests.servicecenter.enrollment;

import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyNoError;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.Constant.RC_POINTS;
import static com.ihg.automation.selenium.common.event.AllEventsRowFactory.getEnrollEvent;
import static com.ihg.automation.selenium.common.event.AllEventsRowFactory.getOfferRegistrationEvent;
import static com.ihg.automation.selenium.common.event.EventTransactionType.Membership.ENROLL;
import static com.ihg.automation.selenium.common.event.EventTransactionType.Offer.OFFER_REGISTRATION;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;

import java.util.Map;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.CustomerInfoPanel;
import com.ihg.automation.selenium.common.billing.EventBillingDetailsTab;
import com.ihg.automation.selenium.common.earning.EventEarningDetailsTab;
import com.ihg.automation.selenium.common.enroll.SuccessEnrolmentPopUp;
import com.ihg.automation.selenium.common.event.EarningEvent;
import com.ihg.automation.selenium.common.member.GuestName;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.offer.OfferFactory;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.gwt.components.Mode;
import com.ihg.automation.selenium.servicecenter.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsGrid;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsGridRow;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsPage;
import com.ihg.automation.selenium.servicecenter.pages.event.all.OfferGridRowContainer;
import com.ihg.automation.selenium.servicecenter.pages.event.enroll.MembershipDetailsPopUp;
import com.ihg.automation.selenium.servicecenter.pages.event.enroll.MembershipGridRowContainer;
import com.ihg.automation.selenium.servicecenter.pages.personal.PersonalInfoPage;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.RewardClubPage;
import com.ihg.crm.automation.selenium.tests.servicecenter.LoginLogout;

public class EnrollmentWithReferredByTest extends LoginLogout
{
    private EnrollmentPage enrollmentPage = new EnrollmentPage();
    private EarningEvent event = new EarningEvent("PC", "0", "0", RC_POINTS);
    private Member member = new Member();
    private static String referringMemberId;
    private AllEventsGridRow enrollRow;
    private AllEventsGridRow offerRow;
    private MembershipGridRowContainer rowCont;
    private MembershipDetailsPopUp popUp;
    private AllEventsPage allEvents;
    private RewardClubPage rewardClubPage;
    private GuestName guestName = new GuestName();
    private static final String REFERRING_MEMBER = "SELECT"//
            + " M.MBRSHP_ID,"//
            + " N.SALUTATION_TXT,"//
            + " N.GIVEN_NM,"//
            + " N.SURNAME_NM"//
            + " FROM  DGST.MBRSHP M"//
            + " JOIN DGST.GST_NM N"//
            + " ON N.DGST_MSTR_KEY = M.DGST_MSTR_KEY"//
            + " WHERE M.MBRSHP_STAT_CD = 'O'"//
            + " AND M.LYTY_PGM_CD = 'PC'"//
            + " AND M.CREAT_TS < SYSDATE"//
            + " AND N.DFLT_PRF_IND = 'Y'"//
            + " AND ROWNUM = 1";

    @BeforeClass
    public void beforeClass()
    {
        member.addProgram(Program.RC);
        member.addAddress(MemberPopulateHelper.getUnitedStatesAddress());
        member.setPersonalInfo(MemberPopulateHelper.getSimplePersonalInfo());

        Map<String, Object> map = jdbcTemplate.queryForMap(String.format(REFERRING_MEMBER));

        guestName.setSalutation((String) map.get("SALUTATION_TXT"));
        guestName.setGiven(map.get("GIVEN_NM").toString());
        guestName.setSurname(map.get("SURNAME_NM").toString());

        referringMemberId = map.get("MBRSHP_ID").toString();

        member.setRewardClubReferredBy(referringMemberId);

        login();
    }

    @Test
    public void createEnrollmentToRC()
    {
        enrollmentPage.goTo();
        enrollmentPage.populate(member);

        enrollmentPage.getReferredMember().verify(guestName.getNameOnPanel());

        enrollmentPage.clickSubmit(verifyNoError());

        SuccessEnrolmentPopUp popUp = new SuccessEnrolmentPopUp();
        member = popUp.putPrograms(member);
        popUp.clickDone(verifyNoError());
    }

    @Test(dependsOnMethods = { "createEnrollmentToRC" })
    public void validatePersonalInformation()
    {
        PersonalInfoPage persInfo = new PersonalInfoPage();
        persInfo.goTo();
        persInfo.getCustomerInfo().verify(member, Mode.VIEW);
        persInfo.getAddressList().getExpandedContact().verify(member.getPreferredAddress(), Mode.VIEW);
    }

    @Test(dependsOnMethods = { "validatePersonalInformation" })
    public void verifyEnrollmentAllEventsGrid()
    {
        allEvents = new AllEventsPage();
        allEvents.goTo();

        AllEventsGrid grid = allEvents.getGrid();
        grid.getEnrollRow(Program.RC).verify(getEnrollEvent(Program.RC));
        grid.getRow(OFFER_REGISTRATION).verify(getOfferRegistrationEvent(OfferFactory.DELAYED_FULFILMENT_GLOBAL));
    }

    @Test(dependsOnMethods = { "createEnrollmentToRC", "verifyEnrollmentAllEventsGrid" })
    public void verifyEnrollmentDetails()
    {
        allEvents = new AllEventsPage();
        allEvents.goTo();
        enrollRow = allEvents.getGrid().getEnrollRow(Program.RC);
        rowCont = enrollRow.expand(MembershipGridRowContainer.class);
        rowCont.getMembershipDetails().verifyScSource(helper);
    }

    @Test(dependsOnMethods = { "verifyEnrollmentDetails" })
    public void verifyMembershipDetailsTab()
    {
        rowCont.clickDetails();
        popUp = new MembershipDetailsPopUp();
        popUp.getMembershipDetailsTab().getMembershipDetails().verifyScSource(helper);
    }

    @Test(dependsOnMethods = { "verifyMembershipDetailsTab" })
    public void verifyEarningDetailsTab()
    {
        popUp = new MembershipDetailsPopUp();
        EventEarningDetailsTab earningDetailsTab = popUp.getEarningDetailsTab();
        earningDetailsTab.goTo();
        earningDetailsTab.verifyBasicDetailsNoPointsAndMiles();
        earningDetailsTab.getGrid().verify(event);
    }

    @Test(dependsOnMethods = { "verifyEarningDetailsTab" })
    public void verifyBillingDetailsTab()
    {
        popUp = new MembershipDetailsPopUp();
        EventBillingDetailsTab billingDetailsTab = popUp.getBillingDetailsTab();
        billingDetailsTab.goTo();
        billingDetailsTab.verifyTabIsEmpty(ENROLL);
        popUp.clickClose();
    }

    @Test(dependsOnMethods = { "verifyBillingDetailsTab" })
    public void verifyOfferGridDetails()
    {
        offerRow = allEvents.getGrid().getRow(OFFER_REGISTRATION);
        OfferGridRowContainer offerContainer = offerRow.expand(OfferGridRowContainer.class);
        offerContainer.getOfferDetails().verify(OfferFactory.DELAYED_FULFILMENT_GLOBAL, helper);
    }

    @Test(dependsOnMethods = { "verifyOfferGridDetails" })
    public void verifyReferredMember()
    {
        rewardClubPage = new RewardClubPage();
        rewardClubPage.goTo();
        verifyThat(rewardClubPage.getEnrollmentDetails().getReferringMember(), hasText(referringMemberId));
    }

    @Test(dependsOnMethods = { "verifyReferredMember" })
    public void clickReferredMember()
    {
        rewardClubPage.getEnrollmentDetails().getReferringMember().clickAndWait();

        CustomerInfoPanel custPanel = new CustomerInfoPanel();
        verifyThat(custPanel.getMemberNumber(Program.RC), hasText(referringMemberId));
    }
}
