package com.ihg.crm.automation.selenium.tests.servicecenter.catalog;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.GridSize.size;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.member.MemberJdbcUtils;
import com.ihg.automation.selenium.servicecenter.pages.GuestSearch;
import com.ihg.automation.selenium.servicecenter.pages.LeftPanel;
import com.ihg.automation.selenium.servicecenter.pages.catalog.CatalogItemsGrid;
import com.ihg.automation.selenium.servicecenter.pages.catalog.CatalogPage;

public class TierLevelEligibilityCheckTest extends CatalogCommon
{
    private CatalogPage catalogPage = new CatalogPage();
    private CatalogItemsGrid catalogGrid;
    private GuestSearch guestSearch = new GuestSearch();
    private LeftPanel leftPanel = new LeftPanel();
    private static String memberId;
    private final static String US_GOLD_ITEM_ID_ELIGIBLE = "V015";

    private static final String MEMBER_ID = "SELECT M.MBRSHP_ID "//
            + " FROM DGST.MBRSHP M"//
            + " INNER JOIN DGST.GST_CONTACT C ON C.DGST_MSTR_KEY = M.DGST_MSTR_KEY"//
            + " INNER JOIN DGST.GST_ADDR A ON C.GST_CONTACT_KEY=A.GST_CONTACT_KEY"//
            + " WHERE M.LYTY_PGM_CD = 'PC'"//
            + " AND A.CTRY_CD ='%s'"//
            + " AND C.DFLT_PRF_IND ='Y'"//
            + " AND M.MBRSHP_STAT_CD = 'O'"//
            + "  AND M.MBRSHP_LVL_CD = '%s'"//
            + " AND ROWNUM < 2";

    @BeforeClass
    public void beforeClass()
    {
        login();
    }

    @Test
    public void searchNotEligibleItemWithCanadaMember()
    {
        memberId = MemberJdbcUtils.getMemberId(jdbcTemplate, String.format(MEMBER_ID, "CA", "CLUB"));

        guestSearch.byMemberNumber(memberId);

        catalogPage.goTo();
        catalogPage.searchByItemId(US_GOLD_ITEM_ID_ELIGIBLE);

        catalogGrid = catalogPage.getCatalogGrid();
        verifyThat(catalogGrid, size(0));
    }

    @Test
    public void searchNotEligibleItemWithUSMemberClubLevel()
    {
        memberId = MemberJdbcUtils.getMemberId(jdbcTemplate, String.format(MEMBER_ID, "US", "CLUB"));

        guestSearch.byMemberNumber(memberId);

        catalogPage.goTo();
        catalogPage.searchByItemId(US_GOLD_ITEM_ID_ELIGIBLE);

        catalogGrid = catalogPage.getCatalogGrid();
        verifyThat(catalogGrid, size(0));
    }

    @Test
    public void searchEligibleItemWithUSMemberGoldLevel()
    {
        memberId = MemberJdbcUtils.getMemberId(jdbcTemplate, String.format(MEMBER_ID, "US", "GOLD"));

        guestSearch.byMemberNumber(memberId);

        catalogPage.goTo();
        verifySearchByItemId(US_GOLD_ITEM_ID_ELIGIBLE);
    }

    @AfterMethod
    public void afterTest()
    {
        leftPanel.clickNewSearch();
    }
}
