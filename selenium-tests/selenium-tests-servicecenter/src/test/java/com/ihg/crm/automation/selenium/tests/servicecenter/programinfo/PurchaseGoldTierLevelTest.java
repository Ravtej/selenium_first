package com.ihg.crm.automation.selenium.tests.servicecenter.programinfo;

import static com.ihg.automation.common.DateUtils.now;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.event.EventTransactionType.TierLevel.TIER_LEVEL_PURCHASE;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.isDisplayedWithWait;
import static com.ihg.automation.selenium.matchers.component.HasSelectItems.hasSelectItems;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static com.ihg.automation.selenium.matchers.component.ViewableMatcher.hasTextInView;
import static com.ihg.automation.selenium.matchers.text.EntityMatcherFactory.isValue;
import static org.hamcrest.CoreMatchers.containsString;

import java.util.Arrays;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.common.DateUtils;
import com.ihg.automation.selenium.common.catalog.CatalogItem;
import com.ihg.automation.selenium.common.event.AllEventsRow;
import com.ihg.automation.selenium.common.event.AllEventsRowConverter;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.order.Order;
import com.ihg.automation.selenium.common.order.OrderFactory;
import com.ihg.automation.selenium.common.pages.PaymentMethod;
import com.ihg.automation.selenium.common.payment.CheckPayment;
import com.ihg.automation.selenium.common.payment.CheckPaymentContainer;
import com.ihg.automation.selenium.common.payment.PaymentDetailsPopUp;
import com.ihg.automation.selenium.common.types.Currency;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.common.types.program.ProgramStatus;
import com.ihg.automation.selenium.common.types.program.RewardClubLevel;
import com.ihg.automation.selenium.gwt.components.input.Select;
import com.ihg.automation.selenium.servicecenter.pages.LeftPanel;
import com.ihg.automation.selenium.servicecenter.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsGridRow;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsPage;
import com.ihg.automation.selenium.servicecenter.pages.event.level.TierLevelGridRowContainer;
import com.ihg.automation.selenium.servicecenter.pages.event.order.OrderSystemDetailsGridRowContainer;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.RewardClubPage;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.RewardClubSummary;
import com.ihg.automation.selenium.servicecenter.pages.unit.PurchaseTierLevelPopUp;
import com.ihg.crm.automation.selenium.tests.servicecenter.LoginLogout;

public class PurchaseGoldTierLevelTest extends LoginLogout
{
    private Member member = new Member();
    private RewardClubPage rewardClubPage = new RewardClubPage();
    private AllEventsPage allEventsPage = new AllEventsPage();
    private PurchaseTierLevelPopUp purchaseTierLevelPopUp = new PurchaseTierLevelPopUp();
    private PaymentDetailsPopUp paymentDetailsPopUp = new PaymentDetailsPopUp();
    private AllEventsRow purchTierLevelRow = new AllEventsRow(DateUtils.getFormattedDate(), TIER_LEVEL_PURCHASE,
            "RC - GOLD", "", "");
    private CheckPayment checkPayment;

    private static final CatalogItem upgradeAward = CatalogItem.builder("F0010", "ELITE UPGRADE CERTIFICATE KIT")
            .build();

    @BeforeClass
    public void beforeClass()
    {
        member.setPersonalInfo(MemberPopulateHelper.getSimplePersonalInfo());
        member.addAddress(MemberPopulateHelper.getUnitedStatesAddress());
        member.addEmail(MemberPopulateHelper.getRandomEmail());
        member.addProgram(Program.RC);

        login();
        member = new EnrollmentPage().enroll(member);

        checkPayment = new CheckPayment("50.00", Currency.USD.getCode(), "1234567", member.getName().getGiven(),
                "test bank", DateUtils.getFormattedDate(), "7654321");
    }

    @Test
    public void openPurchaseTierLevelPopUp()
    {
        rewardClubPage.goTo();
        RewardClubSummary summary = rewardClubPage.getSummary();
        summary.clickPurchase();
    }

    @Test(dependsOnMethods = { "openPurchaseTierLevelPopUp" })
    public void verifyPurchaseTierLevelPopUp()
    {
        verifyThat(purchaseTierLevelPopUp, isDisplayedWithWait());

        Select program = purchaseTierLevelPopUp.getProgram();
        Select tierLevel = purchaseTierLevelPopUp.getTierLevel();

        verifyThat(program, hasText(isValue(Program.RC)));
        verifyThat(tierLevel, hasText("Select Tier Level"));

        verifyThat(program, hasSelectItems(Arrays.asList(Program.RC.getValue())));
        program.click(); // For closing opened List
        verifyThat(tierLevel, hasSelectItems(Arrays.asList(RewardClubLevel.GOLD.getValue())));
    }

    @Test(dependsOnMethods = { "verifyPurchaseTierLevelPopUp" }, alwaysRun = true)
    public void purchaseTierLevel()
    {
        purchaseTierLevelPopUp.setLevel(Program.RC, RewardClubLevel.GOLD.getCode());
        verifyThat(purchaseTierLevelPopUp, displayed(false));
    }

    @Test(dependsOnMethods = { "purchaseTierLevel" }, alwaysRun = true)
    public void verifyPaymentPopUp()
    {
        verifyThat(paymentDetailsPopUp, isDisplayedWithWait());

        verifyThat(paymentDetailsPopUp.getTotalAmount(), hasTextInView("50.00 USD"));
        verifyThat(paymentDetailsPopUp.getMemberName(), hasText(member.getName().getNameOnPanel()));
        verifyThat(paymentDetailsPopUp.getMembershipId(), hasText(member.getRCProgramId()));
        verifyThat(paymentDetailsPopUp, hasText(containsString(Program.RC.getValue())));
        verifyThat(paymentDetailsPopUp, hasText(containsString(RewardClubLevel.GOLD.getValue())));
    }

    @Test(dependsOnMethods = { "verifyPaymentPopUp" }, alwaysRun = true)
    public void submitCheckPayment()
    {
        paymentDetailsPopUp.selectPaymentType(PaymentMethod.CHECK);
        CheckPaymentContainer paym = paymentDetailsPopUp.getCheckPayment();

        paym.populate(checkPayment);

        paymentDetailsPopUp.clickSubmitNoError();
        verifyThat(paymentDetailsPopUp, displayed(false));
    }

    @Test(dependsOnMethods = { "submitCheckPayment" }, alwaysRun = true)
    public void verifyAllEventsPurchaseLevelRowDetails()
    {
        AllEventsGridRow row = allEventsPage.getGrid().getRow(purchTierLevelRow.getTransType(),
                purchTierLevelRow.getDetail());

        row.verify(purchTierLevelRow);
        row.expand(TierLevelGridRowContainer.class).getTierLevelDetails().verify(upgradeAward, checkPayment, helper);
    }

    @Test(dependsOnMethods = { "submitCheckPayment" }, alwaysRun = true)
    public void verifyAllEventsOrderSystemRowDetails()
    {
        Order order = OrderFactory.getSystemOrder(upgradeAward, member, helper);
        AllEventsRow orderSystemRow = AllEventsRowConverter.convert(order);

        AllEventsGridRow row = allEventsPage.getGrid().getRow(orderSystemRow.getTransType(),
                orderSystemRow.getDetail());
        row.verify(orderSystemRow);
        row.expand(OrderSystemDetailsGridRowContainer.class).verify(order);
    }

    @Test(dependsOnMethods = { "verifyAllEventsOrderSystemRowDetails",
            "verifyAllEventsPurchaseLevelRowDetails" }, alwaysRun = true)
    public void verifyResultingTierLevel()
    {
        int year;
        if (now().isAfter(now().withMonthOfYear(10).withDayOfMonth(30)))
        {
            year = 1;
        }
        else
        {
            year = 0;
        }

        rewardClubPage = new RewardClubPage();
        rewardClubPage.goTo();
        rewardClubPage.getSummary().verify(ProgramStatus.OPEN, RewardClubLevel.GOLD, year);
        verifyThat(new LeftPanel().getProgramInfoPanel().getPrograms().getRewardClubProgram().getLevelCode(),
                hasText(RewardClubLevel.GOLD.getCode()));
    }
}
