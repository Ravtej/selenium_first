package com.ihg.crm.automation.selenium.tests.servicecenter.enrollment;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasDefault;

import org.joda.time.LocalDate;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.contact.Address;
import com.ihg.automation.selenium.common.enroll.CustomerInfo;
import com.ihg.automation.selenium.common.member.GuestAddress;
import com.ihg.automation.selenium.common.member.GuestEmail;
import com.ihg.automation.selenium.common.member.GuestPhone;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.member.PersonalInfo;
import com.ihg.automation.selenium.common.name.PersonNameFields;
import com.ihg.automation.selenium.common.types.Gender;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.servicecenter.pages.enroll.EnrollmentPage;
import com.ihg.crm.automation.selenium.tests.servicecenter.LoginLogout;

public class CancelIhgEnrollmentTest extends LoginLogout
{
    private EnrollmentPage enrollmentPage = new EnrollmentPage();
    private Member member = new Member();

    @BeforeClass
    public void before()
    {
        PersonalInfo personalInfo = new PersonalInfo();
        personalInfo.setName(MemberPopulateHelper.getRandomName());
        personalInfo.setGenderCode(Gender.MALE);
        personalInfo.setBirthDate(new LocalDate());

        GuestAddress guestAddress = MemberPopulateHelper.getUnitedStatesAddress();
        GuestPhone guestPhone = MemberPopulateHelper.getPhone();
        GuestEmail guestEmail = MemberPopulateHelper.getRandomEmail();

        member.addProgram(Program.IHG);
        member.setPersonalInfo(personalInfo);
        member.addAddress(guestAddress);
        member.addPhone(guestPhone);
        member.addEmail(guestEmail);

        login();
        enrollmentPage.goTo();
    }

    @Test
    public void cancelIhgEnrollment()
    {
        enrollmentPage.populate(member);
        enrollmentPage.clickCancel();

        enrollmentPage.goTo();
        enrollmentPage.getPrograms().selectProgram(Program.IHG);
        CustomerInfo customerInfo = enrollmentPage.getCustomerInfo();
        verifyThat(customerInfo.getResidenceCountry(), hasDefault());

        PersonNameFields name = customerInfo.getName();
        verifyThat(name.getSalutation(), hasDefault());
        verifyThat(name.getGivenName(), hasDefault());
        verifyThat(name.getMiddleName(), hasDefault());
        verifyThat(name.getSurname(), hasDefault());
        verifyThat(name.getSuffix(), hasDefault());
        verifyThat(name.getTitle(), hasDefault());
        verifyThat(name.getDegree(), hasDefault());

        verifyThat(customerInfo.getGender(), hasDefault());
        verifyThat(customerInfo.getBirthDate().getMonth(), hasDefault());
        verifyThat(customerInfo.getBirthDate().getDay(), hasDefault());

        Address address = enrollmentPage.getAddress();
        verifyThat(address.getCountry(), hasDefault());
        verifyThat(address.getAddress1(), hasDefault());
        verifyThat(address.getAddress2(), hasDefault());
        verifyThat(address.getAddress3(), hasDefault());
        verifyThat(address.getAddress4(), hasDefault());
        verifyThat(address.getAddress5(), hasDefault());
        verifyThat(address.getLocality1(), hasDefault());
        verifyThat(address.getRegion1(), hasDefault());
        verifyThat(address.getZipCode(), hasDefault());
    }
}
