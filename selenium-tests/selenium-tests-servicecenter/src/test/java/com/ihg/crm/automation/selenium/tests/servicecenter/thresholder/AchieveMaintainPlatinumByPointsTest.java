package com.ihg.crm.automation.selenium.tests.servicecenter.thresholder;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.event.EventTransactionType.TierLevel.UPGRADE_ACHIEVE;
import static com.ihg.automation.selenium.common.event.EventTransactionType.TierLevel.UPGRADE_MAINTAIN;
import static com.ihg.automation.selenium.common.types.program.RewardClubLevel.CLUB;
import static com.ihg.automation.selenium.common.types.program.RewardClubLevel.PLTN;
import static com.ihg.automation.selenium.common.types.program.RewardClubLevel.SPRE;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.stay.Stay;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.servicecenter.pages.LeftPanel;
import com.ihg.automation.selenium.servicecenter.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsPage;
import com.ihg.automation.selenium.servicecenter.pages.event.stay.StayEventsPage;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.RewardClubPage;
import com.ihg.automation.selenium.tests.common.RulesDBUtils;
import com.ihg.crm.automation.selenium.tests.servicecenter.LoginLogout;

public class AchieveMaintainPlatinumByPointsTest extends LoginLogout
{
    private Stay stayInPreviousYear = new Stay();
    private Stay stayInCurrentYear = new Stay();
    private static int pointsToMaintainPlatinum;
    private static int pointsToAchieveSpire;
    private static int pointsToAchieveSpirePrevious;
    private static int pointsToAchievePlatinumPrevious;

    @BeforeClass
    public void before()
    {
        RulesDBUtils rulesDBUtils = new RulesDBUtils(jdbcTemplate);
        pointsToMaintainPlatinum = rulesDBUtils.getCurrentYearRulePoints(PLTN, PLTN);
        pointsToAchieveSpirePrevious = rulesDBUtils.getPreviousYearRulePoints(CLUB, SPRE);
        pointsToAchieveSpire = rulesDBUtils.getCurrentYearRulePoints(PLTN, SPRE);
        pointsToAchievePlatinumPrevious = rulesDBUtils.getPreviousYearRulePoints(CLUB, PLTN);

        stayInPreviousYear.setHotelCode("CEQHA");
        stayInPreviousYear.setCheckInOutByNightsInPreviousYear(1);
        stayInPreviousYear.setUsdAvgRoomRateByPoints(pointsToAchievePlatinumPrevious);
        stayInPreviousYear.setRateCode("Test");

        stayInCurrentYear.setHotelCode("CEQHA");
        stayInCurrentYear.setCheckInOutByNights(1);
        stayInCurrentYear.setUsdAvgRoomRateByPoints(pointsToMaintainPlatinum);
        stayInCurrentYear.setRateCode("Test");

        Member member = new Member();
        member.addProgram(Program.RC);
        member.setPersonalInfo(MemberPopulateHelper.getSimplePersonalInfo());
        member.addAddress(MemberPopulateHelper.getUnitedStatesAddress());

        login();

        new EnrollmentPage().enroll(member);

        new LeftPanel().verifyRCLevel(CLUB);
    }

    @Test
    public void achievePlatinumTierLevel()
    {
        new StayEventsPage().createStay(stayInPreviousYear);
        new LeftPanel().verifyRCLevel(PLTN);

        RewardClubPage rewardClubPage = new RewardClubPage();
        rewardClubPage.goTo();

        rewardClubPage.getPreviousYearAnnualActivities().getPointsRow().verify(pointsToAchievePlatinumPrevious,
                pointsToAchieveSpirePrevious);

        rewardClubPage.getCurrentYearAnnualActivities().getPointsRow().verify(0, pointsToAchieveSpire,
                pointsToMaintainPlatinum);

        AllEventsPage allEventsPage = new AllEventsPage();
        allEventsPage.goTo();

        verifyThat(allEventsPage.getGrid().getRow(UPGRADE_ACHIEVE, "RC - PLATINUM"), displayed(true));
    }

    @Test(dependsOnMethods = { "achievePlatinumTierLevel" }, alwaysRun = true)
    public void maintainPlatinumTierLevel()
    {
        new StayEventsPage().createStay(stayInCurrentYear);
        RewardClubPage rewardClubPage = new RewardClubPage();
        rewardClubPage.goTo();

        rewardClubPage.getCurrentYearAnnualActivities().getPointsRow()
                .verifyMaintainIsAchieved(pointsToMaintainPlatinum, pointsToAchieveSpire);

        AllEventsPage allEventsPage = new AllEventsPage();
        allEventsPage.goTo();
        verifyThat(allEventsPage.getGrid().getRow(UPGRADE_MAINTAIN, "RC - PLATINUM"), displayed(true));
    }
}
