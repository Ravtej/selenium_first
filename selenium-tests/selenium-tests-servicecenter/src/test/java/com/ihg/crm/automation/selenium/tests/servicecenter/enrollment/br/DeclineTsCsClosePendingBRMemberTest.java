package com.ihg.crm.automation.selenium.tests.servicecenter.enrollment.br;

import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifySuccess;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.event.EventTransactionType.Membership.MEMBERSHIP_CLOSE;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.enabled;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.isRedColor;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static com.ihg.automation.selenium.matchers.component.ViewableMatcher.hasTextInView;
import static org.hamcrest.MatcherAssert.assertThat;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.common.DateUtils;
import com.ihg.automation.selenium.common.BusinessRewardsGridRow;
import com.ihg.automation.selenium.common.ProgramInfoPanel;
import com.ihg.automation.selenium.common.event.AllEventsRow;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.common.types.program.ProgramStatus;
import com.ihg.automation.selenium.common.types.program.ProgramStatusReason;
import com.ihg.automation.selenium.servicecenter.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsGridRow;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsPage;
import com.ihg.automation.selenium.servicecenter.pages.event.status.ProfileStatusGridRowContainer;
import com.ihg.automation.selenium.servicecenter.pages.programs.br.BusinessRewardsPage;
import com.ihg.automation.selenium.servicecenter.pages.programs.br.BusinessRewardsSummary;
import com.ihg.crm.automation.selenium.tests.servicecenter.LoginLogout;

@Test(groups = { "brTest" })
public class DeclineTsCsClosePendingBRMemberTest extends LoginLogout
{
    private Member member = new Member();
    private BusinessRewardsPage businessRewardsPage;
    private BusinessRewardsSummary summary;
    private AllEventsPage allEventsPage = new AllEventsPage();
    private AllEventsRow allEventsRow = new AllEventsRow(DateUtils.getFormattedDate(), MEMBERSHIP_CLOSE,
            Program.BR.getCode(), "", "");

    @BeforeClass
    public void beforeClass()
    {
        member.addProgram(Program.BR);
        member.addAddress(MemberPopulateHelper.getUnitedStatesAddress());
        member.setPersonalInfo(MemberPopulateHelper.getSimplePersonalInfo());

        login();
        new EnrollmentPage().enroll(member);
    }

    @Test()
    public void verifyStatus()
    {
        businessRewardsPage = new BusinessRewardsPage();
        businessRewardsPage.goTo();

        summary = businessRewardsPage.getSummary();
        verifyThat(summary.getStatus(), hasTextInView(ProgramStatus.OPEN));
    }

    @Test(dependsOnMethods = { "verifyStatus" }, alwaysRun = true)
    public void clickDeclinedTsAndCs()
    {
        assertThat(summary.getDeclinedTsAndCs(), displayed(true));
        summary.getDeclinedTsAndCs().click(verifySuccess("IHG Business Rewards Membership Ts&Cs have been Declined"));
    }

    @Test(dependsOnMethods = { "clickDeclinedTsAndCs" }, alwaysRun = true)
    public void verifyBusinessRewardsPage()
    {
        businessRewardsPage = new BusinessRewardsPage();
        businessRewardsPage.goTo();

        BusinessRewardsSummary summary = businessRewardsPage.getSummary();

        verifyThat(summary.getStatus(), hasTextInView(ProgramStatus.CLOSED));
        verifyThat(summary.getStatusReason(), hasTextInView(ProgramStatusReason.DECLINED));
        verifyThat(summary.getMemberId(), hasText(member.getBRProgramId()));
        verifyThat(summary.getTermsAndConditions(), hasText("Declined"));
        verifyThat(summary.getSource(), hasText("Service Center"));
        verifyThat(summary.getDateOfResponse(), hasText(DateUtils.getFormattedDate()));
        verifyThat(summary.getDeclinedTsAndCs(), displayed(false));
    }

    @Test(dependsOnMethods = { "verifyBusinessRewardsPage" }, alwaysRun = true)
    public void verifyAllEventsGridRow()
    {
        allEventsPage.goTo();
        AllEventsGridRow row = allEventsPage.getGrid().getRow(allEventsRow.getTransType(), allEventsRow.getDetail());
        row.verify(allEventsRow);
    }

    @Test(dependsOnMethods = { "verifyAllEventsGridRow" }, alwaysRun = true)
    public void verifyAllEventsGridRowDetails()
    {
        allEventsPage.goTo();
        AllEventsGridRow row = allEventsPage.getGrid().getRow(allEventsRow.getTransType(), allEventsRow.getDetail());
        row.expand(ProfileStatusGridRowContainer.class).verify(helper, ProgramStatusReason.DECLINED);
    }

    @Test(dependsOnMethods = { "verifyAllEventsGridRowDetails" }, alwaysRun = true)
    public void verifyProgramInformationPanelLevel()
    {
        BusinessRewardsGridRow rowBR = new ProgramInfoPanel().getPrograms().getBusinessRewardsProgram();
        verifyThat(rowBR, isRedColor());

        verifyThat(rowBR.getStatus(), displayed(false));
    }

    @Test(dependsOnMethods = { "verifyProgramInformationPanelLevel" })
    public void tryToReopen()
    {
        businessRewardsPage.goTo();
        summary = businessRewardsPage.getSummary();
        summary.clickEdit();

        verifyThat(summary.getStatus(), enabled(false));
        verifyThat(summary.getStatusReason(), enabled(false));
    }
}
