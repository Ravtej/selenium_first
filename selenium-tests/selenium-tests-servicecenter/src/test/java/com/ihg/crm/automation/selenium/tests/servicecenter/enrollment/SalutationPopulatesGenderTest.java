package com.ihg.crm.automation.selenium.tests.servicecenter.enrollment;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.components.Country;
import com.ihg.automation.selenium.common.enroll.CustomerInfo;
import com.ihg.automation.selenium.common.name.PersonNameFields;
import com.ihg.automation.selenium.common.types.Gender;
import com.ihg.automation.selenium.common.types.name.Salutation;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.servicecenter.pages.enroll.EnrollmentPage;
import com.ihg.crm.automation.selenium.tests.servicecenter.LoginLogout;

public class SalutationPopulatesGenderTest extends LoginLogout
{
    private EnrollmentPage enrollmentPage = new EnrollmentPage();
    private CustomerInfo customerInfo;
    private PersonNameFields personNameEdit;

    @BeforeClass
    public void before()
    {
        login();
        enrollmentPage.goTo();
        enrollmentPage.getPrograms().selectProgram(Program.RC);
        customerInfo = enrollmentPage.getCustomerInfo();
    }

    @DataProvider(name = "salutationRules")
    public Object[][] salutationRules()
    {
        return new Object[][] { { Country.US, Salutation.MRS, Gender.FEMALE },
                { Country.US, Salutation.SR, Gender.MALE } };
    }

    @DataProvider(name = "genderRules")
    public Object[][] genderRules()
    {
        return new Object[][] { { Country.ES, Gender.FEMALE, Salutation.SRA },
                { Country.ES, Gender.MALE, Salutation.SR } };
    }

    @Test(dataProvider = "salutationRules")
    public void salutationPopulatesGender(Country country, String salutation, Gender gender)
    {
        customerInfo.getResidenceCountry().select(country);
        personNameEdit = customerInfo.getName();
        personNameEdit.setConfiguration(country);
        personNameEdit.getSalutation().select(salutation);
        verifyThat(customerInfo.getGender(), hasText(gender.getValue()));
    }

    @Test(dataProvider = "genderRules")
    public void genderPopulatesSalutation(Country country, Gender gender, String salutation)
    {
        customerInfo.getResidenceCountry().select(country);
        personNameEdit = customerInfo.getName();
        personNameEdit.setConfiguration(country);
        customerInfo.getGender().selectByValue(gender);
        verifyThat(personNameEdit.getSalutation(), hasText(salutation));
    }
}
