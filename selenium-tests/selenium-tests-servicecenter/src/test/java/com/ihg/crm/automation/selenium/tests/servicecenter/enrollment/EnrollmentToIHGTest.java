package com.ihg.crm.automation.selenium.tests.servicecenter.enrollment;

import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyNoError;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.Constant.NOT_AVAILABLE;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasDefault;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasEmptyText;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.isDisplayedWithWait;
import static com.ihg.automation.selenium.matchers.component.GridSize.size;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static com.ihg.automation.selenium.matchers.component.ViewableMatcher.hasTextInView;
import static org.hamcrest.core.StringContains.containsString;

import org.joda.time.LocalDate;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.common.DateUtils;
import com.ihg.automation.selenium.common.CustomerInfoPanel;
import com.ihg.automation.selenium.common.ProgramInfoPanel;
import com.ihg.automation.selenium.common.ProgramsGrid;
import com.ihg.automation.selenium.common.components.Country;
import com.ihg.automation.selenium.common.contact.Address;
import com.ihg.automation.selenium.common.enroll.SuccessEnrolmentPopUp;
import com.ihg.automation.selenium.common.event.AllEventsRowFactory;
import com.ihg.automation.selenium.common.member.GuestAddress;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.member.PersonalInfo;
import com.ihg.automation.selenium.common.personal.EmailContact;
import com.ihg.automation.selenium.common.types.Gender;
import com.ihg.automation.selenium.common.types.WrittenLanguage;
import com.ihg.automation.selenium.common.types.name.Salutation;
import com.ihg.automation.selenium.common.types.program.GuestAccountLevel;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.common.types.program.ProgramStatus;
import com.ihg.automation.selenium.gwt.components.Component;
import com.ihg.automation.selenium.gwt.components.Mode;
import com.ihg.automation.selenium.servicecenter.pages.LeftPanel;
import com.ihg.automation.selenium.servicecenter.pages.account.AccountInfoPage;
import com.ihg.automation.selenium.servicecenter.pages.communication.CommunicationPreferencesPage;
import com.ihg.automation.selenium.servicecenter.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsGridRow;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsPage;
import com.ihg.automation.selenium.servicecenter.pages.event.enroll.MembershipDetailsPopUp;
import com.ihg.automation.selenium.servicecenter.pages.event.enroll.MembershipGridRowContainer;
import com.ihg.automation.selenium.servicecenter.pages.personal.AddressContact;
import com.ihg.automation.selenium.servicecenter.pages.personal.PersonalInfoPage;
import com.ihg.automation.selenium.servicecenter.pages.programs.GuestAccountPage;
import com.ihg.automation.selenium.servicecenter.pages.programs.ProgramPageBase.EnrollmentDetails;
import com.ihg.crm.automation.selenium.tests.servicecenter.LoginLogout;

public class EnrollmentToIHGTest extends LoginLogout
{
    private Member member = new Member();
    private GuestAddress unitedStatesAddress = MemberPopulateHelper.getUnitedStatesAddress();
    private GuestAccountPage gaPage = new GuestAccountPage();

    @BeforeClass
    public void beforeClass()
    {
        PersonalInfo personalInfo = MemberPopulateHelper.getSimplePersonalInfo();
        personalInfo.setBirthCityName("BirthCity");
        personalInfo.setBirthCountry(Country.US);
        personalInfo.setBirthDate(new LocalDate());
        personalInfo.setGenderCode(Gender.MALE);
        personalInfo.setResidenceCountry(Country.US);
        personalInfo.getName().setSalutation(Salutation.MR);

        member.setPersonalInfo(personalInfo);
        member.addProgram(Program.IHG);
        member.addPhone(MemberPopulateHelper.getPhone());
        member.addEmail(MemberPopulateHelper.getRandomEmail());
        member.addAddress(unitedStatesAddress);

        login();
    }

    @Test
    public void createEnrollmentToIHG()
    {
        EnrollmentPage enrollmentPage = new EnrollmentPage();
        enrollmentPage.submitEnrollment(member);
        SuccessEnrolmentPopUp popUp = new SuccessEnrolmentPopUp();

        verifyThat(popUp, isDisplayedWithWait());
        verifyThat(popUp.getMemberId(Program.IHG), hasEmptyText());
        verifyThat(popUp, hasText(Program.IHG.getCode()));

        popUp.clickDone(verifyNoError());
    }

    @Test(dependsOnMethods = { "createEnrollmentToIHG" })
    public void validateLeftPanelProgramInfoNavigation()
    {
        verifyThat(gaPage, displayed(false));
        ProgramsGrid grid = new LeftPanel().getProgramInfoPanel().getPrograms();
        grid.getProgram(Program.IHG).goToProgramPage();
        verifyThat(gaPage, isDisplayedWithWait());
    }

    @Test(dependsOnMethods = { "validateLeftPanelProgramInfoNavigation" })
    public void verifyIHGGuestAccount()
    {
        gaPage.goTo();

        verifyThat(gaPage.getSummary().getStatus(), hasTextInView(ProgramStatus.OPEN));

        verifyThat(gaPage.getSummary().getMemberId(), hasDefault());

        EnrollmentDetails enrlDetails = gaPage.getEnrollmentDetails();
        verifyThat(enrlDetails.getOfferCode(), hasDefault());
        verifyThat(enrlDetails.getPaymentDetails().getMethod(), hasDefault());
        verifyThat(enrlDetails.getReferringMember(), hasText(NOT_AVAILABLE));
        verifyThat(enrlDetails.getRenewalDate(), hasDefault());
        verifyThat(enrlDetails.getEnrollDate(), hasText(DateUtils.getFormattedDate()));

        enrlDetails.verifyScSource(helper);
    }

    @Test(dependsOnMethods = { "createEnrollmentToIHG" })
    public void verifyEnrollmentAllEventsGrid()
    {
        AllEventsPage allEvents = new AllEventsPage();
        allEvents.goTo();
        allEvents.getGrid().getRow(1).verify(AllEventsRowFactory.getEnrollEvent(Program.IHG));
    }

    @Test(dependsOnMethods = { "createEnrollmentToIHG", "verifyEnrollmentAllEventsGrid" })
    public void verifyEnrollmentAllEventsRowMembershipDetails()
    {
        AllEventsPage allEvents = new AllEventsPage();
        allEvents.goTo();
        AllEventsGridRow row = allEvents.getGrid().getRow(1);

        row.expand(MembershipGridRowContainer.class).getMembershipDetails().verifyScSource(helper);
    }

    @Test(dependsOnMethods = { "createEnrollmentToIHG", "verifyEnrollmentAllEventsGrid",
            "verifyEnrollmentAllEventsRowMembershipDetails" })
    public void verifyEnrollmentAllEventsMembershipDetailsTab()
    {
        AllEventsPage allEvents = new AllEventsPage();
        allEvents.goTo();
        AllEventsGridRow row = allEvents.getGrid().getRow(1);

        MembershipGridRowContainer rowCont = row.expand(MembershipGridRowContainer.class);
        rowCont.getDetails().clickAndWait();
        MembershipDetailsPopUp popUp = new MembershipDetailsPopUp();
        popUp.getMembershipDetailsTab().getMembershipDetails().verifyScSource(helper);
    }

    @Test(dependsOnMethods = { "verifyEnrollmentAllEventsMembershipDetailsTab" })
    public void verifyEnrollmentAllEventsEarningDetailsTab()
    {
        MembershipDetailsPopUp popUp = new MembershipDetailsPopUp();
        popUp.getEarningDetailsTab().goTo();

        verifyThat(popUp.getEarningDetailsTab().getGrid(), size(0));
    }

    @Test(dependsOnMethods = { "verifyEnrollmentAllEventsEarningDetailsTab" })
    public void verifyEnrollmentAllEventsBillingDetailsTab()
    {
        MembershipDetailsPopUp popUp = new MembershipDetailsPopUp();
        popUp.getBillingDetailsTab().goTo();
        // TODO add verification

        popUp.clickClose();
    }

    @Test(dependsOnMethods = { "createEnrollmentToIHG" })
    public void verifyPersonalInformation()
    {
        PersonalInfoPage persInfo = new PersonalInfoPage();
        persInfo.goTo();

        persInfo.getCustomerInfo().verify(member, Mode.VIEW);

        AddressContact addressContact = persInfo.getAddressList().getExpandedContact();
        Address address = addressContact.getBaseContact();
        address.verify(member.getPreferredAddress(), Mode.VIEW);

        verifyThat(persInfo.getPhoneList().getContact(),
                hasText(containsString(member.getPreferredPhone().getFullPhone())));
        EmailContact emailContact = persInfo.getEmailList().getExpandedContact();
        verifyThat(emailContact.getBaseContact().getEmail(), hasTextInView(member.getPreferredEmail().getEmail()));
    }

    @Test(dependsOnMethods = { "createEnrollmentToIHG" })
    public void verifyCustomerInformationPanel()
    {
        CustomerInfoPanel custPanel = new CustomerInfoPanel();
        custPanel.verify(member);

        GuestAddress guestAddr = member.getPreferredAddress();
        Component panelAddr = custPanel.getAddress();

        verifyThat(panelAddr, hasText(containsString(guestAddr.getLine1())));
        verifyThat(panelAddr, hasText(containsString(guestAddr.getLocality1())));
        verifyThat(panelAddr, hasText(containsString(guestAddr.getPostalCode())));
        verifyThat(panelAddr, hasText(containsString(guestAddr.getRegion1().getValue())));
        verifyThat(panelAddr, hasText(containsString(guestAddr.getCountry().getValue())));
    }

    @Test(dependsOnMethods = { "createEnrollmentToIHG" })
    public void verifyPersonalInformationPanel()
    {
        ProgramInfoPanel infoPanel = new ProgramInfoPanel();
        ProgramsGrid grid = infoPanel.getPrograms();
        verifyThat(grid.getProgram(Program.IHG).getLevelCode(), hasText(GuestAccountLevel.IHG.getCode()));
        verifyThat(grid, size(1));
    }

    @Test(dependsOnMethods = { "createEnrollmentToIHG" })
    public void verifyCommunicationPreferences()
    {
        CommunicationPreferencesPage page = new CommunicationPreferencesPage();
        page.goTo();
        page.getCommunicationPreferences().verify(WrittenLanguage.ENGLISH);
    }

    @Test(dependsOnMethods = { "createEnrollmentToIHG" })
    public void verifyAccountStatus()
    {
        AccountInfoPage page = new AccountInfoPage();
        page.goTo();
        page.verify(ProgramStatus.OPEN);
    }
}
