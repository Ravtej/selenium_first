package com.ihg.crm.automation.selenium.tests.servicecenter.copperfield;

import static com.ihg.automation.selenium.common.types.program.RewardClubLevel.GOLD;
import static com.ihg.crm.automation.selenium.tests.servicecenter.copperfield.VerificationDatesUtils.verifyDatesWithDefaultExpiration;
import static com.ihg.crm.automation.selenium.tests.servicecenter.copperfield.VerificationDatesUtils.verifyDatesWithShiftedExpiration;

import org.joda.time.LocalDate;

import com.ihg.automation.selenium.common.types.program.RewardClubLevelReason;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.RewardClubPage;

public class GivePointAnotherMemberForGoldTest extends GivePointAnotherMemberBase
{
    @Override
    protected void postEnrollActionsForOriginator()
    {
        RewardClubPage rewardClubPage = new RewardClubPage();
        rewardClubPage.goTo();
        rewardClubPage.getSummary().setTierLevelWithExpiration(GOLD, RewardClubLevelReason.BASE);
    }

    @Override
    protected void verifyDatesForRecipient(LocalDate localDate)
    {
        verifyDatesWithShiftedExpiration(localDate);
    }

    @Override
    protected void verifyDatesForOriginator(LocalDate localDate)
    {
        verifyDatesWithDefaultExpiration(localDate);
    }
}
