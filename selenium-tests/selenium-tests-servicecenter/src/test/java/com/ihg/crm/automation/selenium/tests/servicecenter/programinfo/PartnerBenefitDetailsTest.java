package com.ihg.crm.automation.selenium.tests.servicecenter.programinfo;

import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyNoError;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.types.program.Program.RC;
import static com.ihg.automation.selenium.common.types.program.RewardClubLevel.CLUB;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.isDisplayedAndEnabled;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.isDisplayedWithWait;
import static com.ihg.automation.selenium.matchers.component.GridSize.size;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static org.hamcrest.core.Is.is;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.common.MapUtils;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberJdbcUtils;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.types.program.RewardClubLevel;
import com.ihg.automation.selenium.gwt.components.Link;
import com.ihg.automation.selenium.servicecenter.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.AmazonBenefitsGrid;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.AmazonBenefitsGridRow;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.AmazonBenefitsPopUp;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.BenefitFrequency;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.BenefitRow;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.PartnerBenefitDetailsPopUp;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.PartnerBenefitGridGrowContainer;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.PartnerBenefitRedeemedPopUp;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.PartnerBenefitsGrid;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.PartnerBenefitsGridRow;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.PartnerBenefitsRedeemedGrid;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.RedeemedPartnerBenefitPopUpBase;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.RewardClubPage;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.YearToDateRedeemedBenefitPopUp;
import com.ihg.crm.automation.selenium.tests.servicecenter.LoginLogout;

public class PartnerBenefitDetailsTest extends LoginLogout
{
    private static final String BENEFIT_NAME = "AMAZON KINDLE";
    private static final String BENEFIT_DESCRIPTION = "Kindle Downloads";
    private PartnerBenefitDetailsPopUp partnerBenefitDetailsPopUp;
    private PartnerBenefitsGridRow amazonRow;
    private RewardClubPage rwdPage = new RewardClubPage();
    private Member member = new Member();

    private final static String ACTUAL_US_BENEFITS = "SELECT TIER_LVL_CD, BENEFIT_QTY, BENEFIT_PER_NM"//
            + " FROM LYTYBENEFIT.ACTUAL_BENEFIT_RULE"//
            + " WHERE CTRY_CD='US' AND IS_ACTV_IND='Y'";
    private final static String UPDATE_BENEFIT = "UPDATE LYTYBENEFIT.MBRSHP_PROMO_STAT"//
            + " SET PROMO_STAT_CD = 'R'"//
            + " WHERE MBRSHP_ID = '%s'";

    @BeforeClass
    public void beforeClass()
    {
        member.addProgram(RC);
        member.setPersonalInfo(MemberPopulateHelper.getSimplePersonalInfo());
        member.addAddress(MemberPopulateHelper.getUnitedStatesAddress());

        login();
        new EnrollmentPage().enroll(member);
    }

    @Test(priority = 1)
    public void verifyPartnerBenefitsDBEmpty()
    {
        verifyThat(null,
                jdbcTemplate.queryForObject(String.format(MemberJdbcUtils.BENEFIT_COUNTERS, member.getRCProgramId()),
                        Integer.class),
                is(0), "No partner benefits in DB for just enrolled members before opening benefit popup");
    }

    @Test(priority = 15)
    public void verifyPartnerBenefitsPopUp()
    {
        rwdPage.goTo();
        Link partnerBenefits = rwdPage.getSummary().getPartnerBenefits();
        verifyThat(partnerBenefits, isDisplayedAndEnabled(true));
        partnerBenefits.clickAndWait(verifyNoError());

        partnerBenefitDetailsPopUp = new PartnerBenefitDetailsPopUp();
        verifyThat(partnerBenefitDetailsPopUp, isDisplayedWithWait());

        PartnerBenefitsGrid currentYearPartnerBenefitsGrid = partnerBenefitDetailsPopUp
                .getCurrentYearPartnerBenefitsGrid();
        verifyThat(currentYearPartnerBenefitsGrid, displayed(true));
        currentYearPartnerBenefitsGrid.verifyHeader();

        // Partner benefits are setting up in DB right after clicking on the
        // Partner
        // Benefit link
        MemberJdbcUtils.waitPartnerBenefitCounter(jdbcTemplate, member);
    }

    @Test(priority = 20)
    public void verifyPartnerBenefitsGrid()
    {
        amazonRow = partnerBenefitDetailsPopUp.getCurrentYearPartnerBenefitsGrid().getRow(1);
        amazonRow.verify(CLUB, BENEFIT_NAME, 0, 0);
    }

    @Test(priority = 25)
    public void verifyPartnerBenefitsGridRowContainer()
    {
        verifyThat(amazonRow.expand(PartnerBenefitGridGrowContainer.class).getItemDescription(),
                hasText(BENEFIT_DESCRIPTION));
    }

    @Test(priority = 30)
    public void verifyAmazonBenefitsPopUp()
    {
        AmazonBenefitsPopUp amazonBenefitsPopUp = amazonRow.openAmazonBenefitsPopUp();
        AmazonBenefitsGrid amazonBenefitsGrid = amazonBenefitsPopUp.getAmazonBenefitsGrid();
        amazonBenefitsGrid.verifyHeader();

        List<BenefitRow> benefitRows = getBenefitRows();
        verifyThat(amazonBenefitsGrid, size(benefitRows.size()));

        for (BenefitRow benefit : benefitRows)
        {
            AmazonBenefitsGridRow row = amazonBenefitsGrid.getRow(benefit.getTierLevel());
            row.verify(benefit);
        }
        amazonBenefitsPopUp.close();
    }

    @Test(priority = 40)
    public void verifyRedeemedPopUpWithNoRedeemedBenefits()
    {
        PartnerBenefitRedeemedPopUp redeemedPopUp = amazonRow.openRedeemedBenefitPopUp();
        redeemedPopUp.getPartnerBenefitsGrid().verifyHeader();
        verifyRedeemedPopUp(redeemedPopUp, 0);

        YearToDateRedeemedBenefitPopUp ytdRedeemedPopUp = amazonRow.openYTDRedeemedBenefitPopUp();
        ytdRedeemedPopUp.getPartnerBenefitsGrid().verifyHeader();
        verifyRedeemedPopUp(ytdRedeemedPopUp, 0);

        partnerBenefitDetailsPopUp.clickClose(verifyNoError());
    }

    @Test(priority = 50)
    public void updateDBAndVerifyRedeemedBenefit()
    {
        jdbcTemplate.update(String.format(UPDATE_BENEFIT, member.getRCProgramId()));

        rwdPage.getSummary().getPartnerBenefits().clickAndWait(verifyNoError());
        amazonRow = new PartnerBenefitDetailsPopUp().getCurrentYearPartnerBenefitsGrid().getRow(1);
        amazonRow.verify(CLUB, BENEFIT_NAME, 1, 1);

        verifyRedeemedPopUp(amazonRow.openRedeemedBenefitPopUp(), 1);
        verifyRedeemedPopUp(amazonRow.openYTDRedeemedBenefitPopUp(), 1);
    }

    private List<BenefitRow> getBenefitRows()
    {
        List<Map<String, Object>> listMap = jdbcTemplate.queryForList(ACTUAL_US_BENEFITS);
        List<BenefitRow> rows = new ArrayList<>();

        for (Map<String, Object> map : listMap)
        {
            Map<String, String> resultMap = MapUtils.queryResult(map);
            BenefitRow benefitRow = new BenefitRow(RewardClubLevel.valueOf(resultMap.get("TIER_LVL_CD")),
                    resultMap.get("BENEFIT_QTY"), BenefitFrequency.valueOf(resultMap.get("BENEFIT_PER_NM")));

            rows.add(benefitRow);
        }

        return rows;
    }

    private void verifyRedeemedPopUp(RedeemedPartnerBenefitPopUpBase redeemedPopUp, int size)
    {
        PartnerBenefitsRedeemedGrid redeemedGrid = redeemedPopUp.getPartnerBenefitsGrid();
        verifyThat(redeemedGrid, size(size));

        if (size > 0)
        {
            redeemedGrid.getRow(1).verify(BENEFIT_NAME);
        }
        redeemedPopUp.close();
    }
}
