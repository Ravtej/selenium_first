package com.ihg.crm.automation.selenium.tests.servicecenter.stay.disqualified;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.event.AllEventsRowConverter;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.pages.program.RewardClubTierLevelActivityGrid;
import com.ihg.automation.selenium.common.stay.Revenue;
import com.ihg.automation.selenium.common.stay.Stay;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.common.types.program.RewardClubLevel;
import com.ihg.automation.selenium.gwt.components.Mode;
import com.ihg.automation.selenium.servicecenter.pages.LeftPanel;
import com.ihg.automation.selenium.servicecenter.pages.annual.AnnualActivityPage;
import com.ihg.automation.selenium.servicecenter.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsPage;
import com.ihg.automation.selenium.servicecenter.pages.event.stay.StayEventsPage;
import com.ihg.automation.selenium.servicecenter.pages.event.stay.StayGuestGridRow;
import com.ihg.automation.selenium.servicecenter.pages.event.stay.StayGuestGridRowContainer;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.RewardClubPage;
import com.ihg.crm.automation.selenium.tests.servicecenter.LoginLogout;

public class PartiallyOverlappingStayTest extends LoginLogout
{

    private Member member = new Member();
    private Stay stay1 = new Stay();
    private Stay stay2 = new Stay();
    private StayEventsPage stayEventsPage = new StayEventsPage();
    private StayGuestGridRow stayGuestGridRow;
    private Revenue room = new Revenue();

    @BeforeClass
    public void beforeClass()
    {
        member.setPersonalInfo(MemberPopulateHelper.getSimplePersonalInfo());
        member.addAddress(MemberPopulateHelper.getUnitedStatesAddress());
        member.addProgram(Program.RC);

        stay1.setHotelCode("CEQHA");
        stay1.setBrandCode("ICON");
        stay1.setCheckInOutByNights(2);
        stay1.setQualifyingNights(2);
        stay1.setTierLevelNights("2");
        stay1.setIhgUnits("2000");
        stay1.setRateCode("1");
        stay1.setAvgRoomRate((double) 100);

        stay2.setHotelCode("ATLFX");
        stay2.setBrandCode("INDG");
        stay2.setCheckInOutByNights(5);
        stay2.setQualifyingNights(3);
        stay2.setTierLevelNights("3");
        stay2.setIhgUnits("5000");
        stay2.setRateCode("1");
        stay2.setAvgRoomRate((double) 100);

        room.setAllowOverride(true);
        room.setQualifying(true);
        room.setBillHotel(true);

        login();

        member = new EnrollmentPage().enroll(member);

        stayEventsPage.createStayAndVerifySuccessMessage(stay1);
        stayEventsPage.getGuestGrid().getRow(1).verify(stay1);
        new AnnualActivityPage().captureCounters(member, jdbcTemplate);
    }

    @Test
    public void createOverlappingStay()
    {
        stayEventsPage.createStayAndVerifySuccessMessage(stay2);
    }

    @Test(dependsOnMethods = { "createOverlappingStay" }, alwaysRun = true)
    public void verifyOverlappingStayCreationEvent()
    {
        stayGuestGridRow = stayEventsPage.getGuestGrid().getRow(1);
        stayGuestGridRow.verify(stay2);
    }

    @Test(dependsOnMethods = { "verifyOverlappingStayCreationEvent" }, alwaysRun = true)
    public void verifyOverlappingStayEventDetails()
    {
        stayGuestGridRow.expand(StayGuestGridRowContainer.class).verifyRoomRevenueDetails(room, Mode.EDIT);
    }

    @Test(dependsOnMethods = { "verifyOverlappingStayEventDetails" }, alwaysRun = true)
    public void verifyOverlappingAllEventsGridRow()
    {
        AllEventsPage allEventsPage = new AllEventsPage();
        allEventsPage.goTo();
        allEventsPage.getGrid().getRow(1).verify(AllEventsRowConverter.convert(stay2));
    }

    @Test(dependsOnMethods = { "verifyOverlappingAllEventsGridRow" }, alwaysRun = true)
    public void verifyOverlappingLeftPanelRCPointsBalance()
    {
        new LeftPanel().getProgramInfoPanel().getPrograms().getRewardClubProgram().verify(RewardClubLevel.CLUB,
                "7,000");
    }

    @Test(dependsOnMethods = { "verifyOverlappingLeftPanelRCPointsBalance" }, alwaysRun = true)
    public void verifyOverlappingActivityCounters()
    {
        member.getAnnualActivity().setTierLevelNights(5);
        member.getAnnualActivity().setQualifiedNights(5);
        member.getAnnualActivity().setNonQualifiedNights(2);
        member.getAnnualActivity().setTotalUnits(7000);
        member.getAnnualActivity().setTotalQualifiedUnits(7000);

        member.getLifetimeActivity().setBaseUnits(7000);
        member.getLifetimeActivity().setTotalUnits(7000);
        member.getLifetimeActivity().setCurrentBalance(7000);

        new AnnualActivityPage().verifyCounters(member);
    }

    @Test(dependsOnMethods = { "verifyOverlappingActivityCounters" }, alwaysRun = true)
    public void verifyOverlappingRewardNightTierLevelCounters()
    {
        RewardClubPage rewardClubPage = new RewardClubPage();
        rewardClubPage.goTo();
        RewardClubTierLevelActivityGrid activities = rewardClubPage.getCurrentYearAnnualActivities();
        activities.getPointsRow().verify("7000", achieveGoldPoints);
        activities.getNightsRow().verify("5", achieveGoldNights);
    }

}
