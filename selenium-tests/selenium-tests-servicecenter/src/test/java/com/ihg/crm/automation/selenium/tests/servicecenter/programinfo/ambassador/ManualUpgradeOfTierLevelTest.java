package com.ihg.crm.automation.selenium.tests.servicecenter.programinfo.ambassador;

import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.assertNoError;
import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyError;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.assertThat;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.event.EventTransactionType.Order.ORDER_SYSTEM;
import static com.ihg.automation.selenium.common.event.EventTransactionType.TierLevel.UPGRADE_MANUAL;
import static com.ihg.automation.selenium.common.history.EventAuditConstant.CREATE;
import static com.ihg.automation.selenium.common.history.EventAuditConstant.CREATED;
import static com.ihg.automation.selenium.common.history.EventAuditConstant.Order.STATUS;
import static com.ihg.automation.selenium.common.types.program.AmbassadorLevelReason.SYSTEM_ERROR;
import static com.ihg.automation.selenium.common.types.program.ProgramStatus.OPEN;
import static com.ihg.automation.selenium.common.types.program.RewardClubLevel.SPRE;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.isDisplayedWithWait;
import static com.ihg.automation.selenium.matchers.component.GridSize.size;
import static com.ihg.automation.selenium.matchers.component.HasSelectItems.hasSelectItems;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static com.ihg.automation.selenium.matchers.component.ViewableMatcher.hasTextInView;
import static org.hamcrest.Matchers.not;

import java.util.Arrays;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.ihg.automation.common.DateUtils;
import com.ihg.automation.selenium.common.Constant;
import com.ihg.automation.selenium.common.ProgramsGrid;
import com.ihg.automation.selenium.common.billing.EventBillingDetailsTab;
import com.ihg.automation.selenium.common.earning.EventEarningDetailsTab;
import com.ihg.automation.selenium.common.event.AllEventsRow;
import com.ihg.automation.selenium.common.event.AllEventsRowConverter;
import com.ihg.automation.selenium.common.event.EarningEvent;
import com.ihg.automation.selenium.common.history.EventHistoryGridRow;
import com.ihg.automation.selenium.common.history.EventHistoryRecords;
import com.ihg.automation.selenium.common.history.EventHistoryTab;
import com.ihg.automation.selenium.common.member.AmbassadorOfferCode;
import com.ihg.automation.selenium.common.member.AmbassadorUtils;
import com.ihg.automation.selenium.common.member.GuestAddress;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.order.Order;
import com.ihg.automation.selenium.common.order.OrderItem;
import com.ihg.automation.selenium.common.types.program.AmbassadorLevel;
import com.ihg.automation.selenium.common.types.program.AmbassadorLevelReason;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.common.types.program.RewardClubLevel;
import com.ihg.automation.selenium.gwt.components.input.Select;
import com.ihg.automation.selenium.servicecenter.pages.LeftPanel;
import com.ihg.automation.selenium.servicecenter.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsGridRow;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsPage;
import com.ihg.automation.selenium.servicecenter.pages.event.level.ManualTierLevelChangeGridRowContainer;
import com.ihg.automation.selenium.servicecenter.pages.event.order.OrderDetailsPopUp;
import com.ihg.automation.selenium.servicecenter.pages.event.order.OrderDetailsTab;
import com.ihg.automation.selenium.servicecenter.pages.event.order.OrderEventsPage;
import com.ihg.automation.selenium.servicecenter.pages.event.order.OrderGridRow;
import com.ihg.automation.selenium.servicecenter.pages.event.order.OrderSystemDetailsGridRowContainer;
import com.ihg.automation.selenium.servicecenter.pages.programs.RenewExtendProgramSummary;
import com.ihg.automation.selenium.servicecenter.pages.programs.amb.AmbassadorPage;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.RewardClubPage;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.RewardClubSummary;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.TierLevelBenefitsDetailsPopUp;
import com.ihg.crm.automation.selenium.tests.servicecenter.LoginLogout;

public class ManualUpgradeOfTierLevelTest extends LoginLogout
{
    private Member member = new Member();
    private AmbassadorPage ambassadorPage = new AmbassadorPage();
    private RenewExtendProgramSummary ambSummary;
    private AllEventsPage allEventsPage = new AllEventsPage();
    private AllEventsRow upgradeEventRow = new AllEventsRow(DateUtils.getFormattedDate(), UPGRADE_MANUAL,
            "AMB - ROYAL AMBASSADOR", "", "");
    private AllEventsRow orderEventRow;
    private GuestAddress deliveryAddress = new GuestAddress();
    private Order order = new Order();
    private OrderEventsPage orderEventsPage = new OrderEventsPage();
    private OrderGridRow orderGridRow;
    private OrderSystemDetailsGridRowContainer orderSystemDetailsGridRowContainer;
    private OrderDetailsPopUp orderDetailsPopUp = new OrderDetailsPopUp();
    private EarningEvent earningEvent;
    private RewardClubPage rewardClubPage = new RewardClubPage();
    private ProgramsGrid grid;

    @BeforeClass
    public void beforeClass()
    {
        member.setPersonalInfo(MemberPopulateHelper.getSimplePersonalInfo());
        member.addAddress(MemberPopulateHelper.getUnitedStatesAddress());
        member.addProgram(Program.AMB);
        member.setAmbassadorOfferCode(AmbassadorOfferCode.CHSRO); // CHSRO -
        // CHARLESTON
        // RESERVATIONS
        member.setAmbassadorAmount(AmbassadorUtils.AMBASSADOR_COMP_PROMO_$0);
        GuestAddress address = member.getPreferredAddress();
        deliveryAddress.setLine1(address.getLine1());
        deliveryAddress.setLocality1(address.getLocality1());
        deliveryAddress.setPostalCode(address.getPostalCode());
        deliveryAddress.setRegion1(address.getRegion1());
        deliveryAddress.setCountryCode(address.getCountry());

        order.setShippingInfo(member);
        order.setDeliveryEmail(Constant.NOT_AVAILABLE);
        OrderItem orderItem = new OrderItem("FRA00", "NEW ROYAL AMBASSADOR KIT");
        orderItem.setDeliveryStatus("PROCESSING");
        order.getOrderItems().add(orderItem);

        orderEventRow = AllEventsRowConverter.convert(order);

        login();

        member = new EnrollmentPage().enroll(member);
        earningEvent = new EarningEvent("Base Awards", "1", "", "", "Created");
        earningEvent.setAward("FRA00");
    }

    @Test
    public void navigateSummaryPage()
    {
        ambassadorPage.goTo();
        ambSummary = ambassadorPage.getSummary();
        ambSummary.clickEdit();
    }

    @Test(dependsOnMethods = { "navigateSummaryPage" })
    public void verifySummaryPanelContent()
    {
        verifyThat(ambSummary.getMemberId(), hasText(member.getRCProgramId()));

        Select tierLevel = ambSummary.getTierLevel();
        assertThat(tierLevel, isDisplayedWithWait());
        verifyThat(tierLevel, hasText(AmbassadorLevel.AMB.getValue()));
        verifyThat(tierLevel, hasSelectItems(Arrays.asList("AMBASSADOR", "ROYAL AMBASSADOR")));
    }

    @Test(dependsOnMethods = { "navigateSummaryPage", "verifySummaryPanelContent" }, alwaysRun = true)
    public void verifyTierLevelReasonContent()
    {
        ambSummary.getTierLevel().select(AmbassadorLevel.RAM.getValue());

        Select tierLevelReason = ambSummary.getTierLevelReason();
        assertThat(tierLevelReason, isDisplayedWithWait());
        verifyThat(tierLevelReason, hasText("Select"));
        verifyThat(tierLevelReason, hasSelectItems(AmbassadorLevelReason.getValuesList()));

        verifyThat(ambSummary.getReferringMember(), isDisplayedWithWait());
    }

    @Test(dependsOnMethods = { "verifyTierLevelReasonContent" }, alwaysRun = true)
    public void verifyErrorMessageWithNotSavedTierLevel()
    {
        ambSummary.getTierLevel().select(AmbassadorLevel.RAM.getValue());
        rewardClubPage.getTab().click(verifyError(RewardClubPage.WARNING_MESSAGE));
        ambSummary.clickCancel();
        verifyThat(ambSummary.getTierLevel(), hasTextInView(AmbassadorLevel.AMB.getValue()));
    }

    @Test(dependsOnMethods = { "verifyErrorMessageWithNotSavedTierLevel" }, alwaysRun = true)
    public void setTierLevelToRAM()
    {
        ambSummary.clickEdit();
        ambSummary.getTierLevel().select(AmbassadorLevel.RAM.getValue());
        ambSummary.getTierLevelReason().selectByValue(SYSTEM_ERROR);
        ambSummary.clickSave(assertNoError());
    }

    @Test(dependsOnMethods = { "setTierLevelToRAM" })
    public void verifyTierLevelExpDate()
    {
        rewardClubPage = new RewardClubPage();
        rewardClubPage.goTo();
        RewardClubSummary summary = rewardClubPage.getSummary();
        summary.verify(OPEN, SPRE, 2);

        TierLevelBenefitsDetailsPopUp tierLevelBenefitsDetailsPopUp = summary.openTierLevelBenefitsDetailsPopUp();
        verifyThat(tierLevelBenefitsDetailsPopUp.getBenefitsGrid(), size(not(0)));
        tierLevelBenefitsDetailsPopUp.close();
    }

    @Test(dependsOnMethods = { "verifyTierLevelExpDate" })
    public void verifyLeftPanelProgramInformation()
    {
        grid = new LeftPanel().getProgramInfoPanel().getPrograms();
        verifyThat(grid.getRewardClubProgram().getLevelCode(), hasText(RewardClubLevel.SPRE.getCode()));
        verifyThat(grid.getProgram(Program.AMB).getLevelCode(), hasText(AmbassadorLevel.RAM.getCode()));
    }

    @Test(dependsOnMethods = { "verifyLeftPanelProgramInformation" })
    public void verifyLeftPanelProgramInfoNavigation()
    {
        verifyThat(ambassadorPage, displayed(false));
        grid.getProgram(Program.AMB).goToProgramPage();
        verifyThat(ambassadorPage, displayed(true));
    }

    @DataProvider(name = "verifyLeftPanelProgramInfoNavigation")
    public Object[][] verifyAllEventsGridRowProvider()
    {
        return new Object[][] { { orderEventRow }, { upgradeEventRow } };
    }

    @Test(dataProvider = "verifyAllEventsGridRowProvider", dependsOnMethods = {
            "verifyLeftPanelProgramInformation" }, alwaysRun = true)
    public void verifyAllEventsGridRow(AllEventsRow eventRow)
    {
        allEventsPage.goTo();
        AllEventsGridRow row = allEventsPage.getGrid().getRow(eventRow.getTransType(), eventRow.getDetail());
        verifyThat(row, displayed(true));
        row.verify(eventRow);
    }

    @Test(dependsOnMethods = { "verifyAllEventsGridRow" }, alwaysRun = true)
    public void verifyAMBUpgradeEventDetails()
    {
        allEventsPage.goTo();
        AllEventsGridRow row = allEventsPage.getGrid().getRow(upgradeEventRow.getTransType(),
                upgradeEventRow.getDetail());
        ManualTierLevelChangeGridRowContainer container = row.expand(ManualTierLevelChangeGridRowContainer.class);
        container.verify(Constant.NOT_AVAILABLE, SYSTEM_ERROR.getValue(), "FRA00", helper);
    }

    @Test(dependsOnMethods = { "verifyAMBUpgradeEventDetails" }, alwaysRun = true)
    public void verifyAMBOrderEventDetails()
    {
        allEventsPage.goTo();
        AllEventsGridRow row = allEventsPage.getGrid().getRow(orderEventRow.getTransType(), orderEventRow.getDetail());

        OrderSystemDetailsGridRowContainer container = row.expand(OrderSystemDetailsGridRowContainer.class);
        container.verify(order);
    }

    @Test(dependsOnMethods = { "verifyAMBOrderEventDetails" }, alwaysRun = true)
    public void navigateOrderEventTab()
    {
        orderEventsPage.goTo();
        verifyThat(orderEventsPage, isDisplayedWithWait());
    }

    @Test(dependsOnMethods = { "navigateOrderEventTab" }, alwaysRun = true)
    public void verifyOrderEventsTabRoyalEventDetails()
    {
        orderGridRow = orderEventsPage.getOrdersGrid().getRow(ORDER_SYSTEM, "NEW ROYAL AMBASSADOR KIT");
        orderSystemDetailsGridRowContainer = orderGridRow.expand(OrderSystemDetailsGridRowContainer.class);
        orderSystemDetailsGridRowContainer.verify(order);
    }

    @Test(dependsOnMethods = { "verifyOrderEventsTabRoyalEventDetails" }, alwaysRun = true)
    public void openOrderEventsTabRoyalEvent()
    {
        orderSystemDetailsGridRowContainer = orderGridRow.expand(OrderSystemDetailsGridRowContainer.class);
        orderSystemDetailsGridRowContainer.clickDetails();
        verifyThat(orderDetailsPopUp, isDisplayedWithWait());
    }

    @Test(dependsOnMethods = { "openOrderEventsTabRoyalEvent" })
    public void verifyOrderEventsTabRoyalEventEditDetails()
    {
        OrderDetailsTab tab = orderDetailsPopUp.getOrderDetailsTab();
        tab.goTo();
        order.setTransactionType(ORDER_SYSTEM);
        tab.verify(order);
    }

    @Test(dependsOnMethods = { "openOrderEventsTabRoyalEvent" })
    public void verifyOrderEventsTabRoyalEventEarningDetails()
    {
        EventEarningDetailsTab tab = orderDetailsPopUp.getEarningDetailsTab();
        tab.goTo();
        tab.verifyBasicDetailsNoPointsAndMiles();
        tab.getGrid().verify(earningEvent);
    }

    @Test(dependsOnMethods = { "openOrderEventsTabRoyalEvent" })
    public void verifyOrderEventsTabRoyalEventBillingDetails()
    {
        EventBillingDetailsTab tab = orderDetailsPopUp.getBillingDetailsTab();
        tab.goTo();
        tab.verifyTabIsEmpty(ORDER_SYSTEM);
    }

    @Test(dependsOnMethods = { "openOrderEventsTabRoyalEvent" })
    public void verifyOrderEventsHistory()
    {
        EventHistoryTab historyTab = orderDetailsPopUp.getHistoryTab();
        historyTab.goTo();

        EventHistoryGridRow historyRow = historyTab.getGrid().getRow(1);
        historyRow.verify(CREATE, helper.getUser().getUserId());

        EventHistoryRecords stayRecords = historyRow.getAllRecords();
        stayRecords.getRecord(STATUS).verifyAdd(CREATED);
    }

    @Test(dependsOnMethods = { "verifyOrderEventsTabRoyalEventBillingDetails",
            "verifyOrderEventsTabRoyalEventEarningDetails", "verifyOrderEventsTabRoyalEventEditDetails",
            "openOrderEventsTabRoyalEvent", "verifyOrderEventsHistory" }, alwaysRun = true)
    public void closeOrderEventsTabRoyalEvent()
    {
        orderDetailsPopUp.close();
    }
}
