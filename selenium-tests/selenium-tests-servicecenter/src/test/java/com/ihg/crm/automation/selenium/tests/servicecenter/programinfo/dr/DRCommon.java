package com.ihg.crm.automation.selenium.tests.servicecenter.programinfo.dr;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.event.EventTransactionType.Membership.MEMBERSHIP_RENEWAL;
import static com.ihg.automation.selenium.common.offer.OfferFactory.FREE_NIGHT_OFFER_CODE;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.isDisplayedWithWait;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static com.ihg.automation.selenium.matchers.component.ViewableMatcher.hasTextInView;
import static org.apache.commons.lang.StringUtils.EMPTY;
import static org.hamcrest.CoreMatchers.containsString;

import javax.annotation.Resource;

import org.joda.time.LocalDate;

import com.ihg.automation.common.DateUtils;
import com.ihg.automation.selenium.common.User;
import com.ihg.automation.selenium.common.catalog.CatalogItem;
import com.ihg.automation.selenium.common.earning.EventEarningDetailsTab;
import com.ihg.automation.selenium.common.types.program.ProgramStatus;
import com.ihg.automation.selenium.common.types.program.RewardClubLevel;
import com.ihg.automation.selenium.common.types.program.RewardClubLevelReason;
import com.ihg.automation.selenium.servicecenter.pages.event.enroll.MembershipDetailsPopUp;
import com.ihg.automation.selenium.servicecenter.pages.event.freenight.FreeNightEventPage;
import com.ihg.automation.selenium.servicecenter.pages.event.freenight.FreeNightGridRow;
import com.ihg.automation.selenium.servicecenter.pages.event.freenight.FreeNightGridRow.FreeNightCell;
import com.ihg.automation.selenium.servicecenter.pages.event.offer.MemberOfferGridRow;
import com.ihg.automation.selenium.servicecenter.pages.event.offer.MemberOfferGridRow.MemberOfferCell;
import com.ihg.automation.selenium.servicecenter.pages.event.offer.OfferEventPage;
import com.ihg.automation.selenium.servicecenter.pages.programs.RenewExtendProgramSummary;
import com.ihg.automation.selenium.servicecenter.pages.programs.dr.DiningRewardsPage;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.RewardClubPage;
import com.ihg.crm.automation.selenium.tests.servicecenter.LoginLogout;

public class DRCommon extends LoginLogout
{
    @Resource(name = "user15U")
    protected User user15U;

    private MembershipDetailsPopUp membershipDetailsPopUp = new MembershipDetailsPopUp();
    private EventEarningDetailsTab earningDetailsTab;
    private FreeNightEventPage freeNightPage = new FreeNightEventPage();
    private final static String WINS_AMOUNT = "2";

    protected void verifyMembershipRenewEarningDetails()
    {
        membershipDetailsPopUp = new MembershipDetailsPopUp();
        earningDetailsTab = membershipDetailsPopUp.getEarningDetailsTab();
        earningDetailsTab.goTo();
        earningDetailsTab.verifyBasicDetailsForPoints("5,000", "0");
    }

    protected void validateMembershipRenewBillingDetails(CatalogItem amount, LocalDate date)
    {
        membershipDetailsPopUp = new MembershipDetailsPopUp();
        membershipDetailsPopUp.getBillingDetailsTab().verifyBaseAwardUSDBilling(MEMBERSHIP_RENEWAL, amount, user15U,
                date);
        membershipDetailsPopUp.close();
    }

    protected void verifyOfferRow()
    {
        OfferEventPage offerPage = new OfferEventPage();
        offerPage.goTo();
        MemberOfferGridRow offerRow = offerPage.getMemberOffersGrid().getRowByOfferCode(FREE_NIGHT_OFFER_CODE);
        verifyThat(offerRow, isDisplayedWithWait());
        verifyThat(offerRow.getCell(MemberOfferCell.WINS), hasText(WINS_AMOUNT));
    }

    protected void verifyFreeNightHasTwoWinsAfterRenew()
    {
        freeNightPage.goTo();
        FreeNightGridRow offerRow = freeNightPage.getFreeNightGrid().getRowByOfferCode(FREE_NIGHT_OFFER_CODE);
        verifyThat(offerRow, displayed(true));
        verifyThat(offerRow.getCell(FreeNightCell.AVAILABLE), hasText(EMPTY));
        verifyThat(offerRow.getCell(FreeNightCell.EXPIRED), hasText(WINS_AMOUNT));
    }

    protected void verifyProgramSummary(String memberId, int expiredMonths)
    {
        DiningRewardsPage diningRewardsPage = new DiningRewardsPage();
        diningRewardsPage.goTo();
        RenewExtendProgramSummary diningSummary = diningRewardsPage.getSummary();
        verifyThat(diningSummary.getMemberId(), hasText(memberId));
        verifyThat(diningSummary.getStatus(), hasTextInView(ProgramStatus.OPEN));
        verifyThat(diningSummary.getExpirationDate(),
                hasText(DateUtils.getMonthsForward(expiredMonths, DateUtils.EXPIRATION_DATE_PATTERN)));
        verifyThat(diningSummary, hasText(containsString("No flags selected")));

        verifyThat(diningSummary.getRenew(), displayed(false));
    }

    protected void verifyRCProgramInformationDetails(RewardClubLevel level)
    {
        RewardClubPage rewardClubPage = new RewardClubPage();
        rewardClubPage.goTo();
        rewardClubPage.getSummary().verify(ProgramStatus.OPEN, level, RewardClubLevelReason.IHG_DINING_REWARDS);
    }
}
