package com.ihg.crm.automation.selenium.tests.servicecenter.catalog;

import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyError;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static org.hamcrest.core.StringContains.containsString;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.member.MemberJdbcUtils;
import com.ihg.automation.selenium.servicecenter.pages.GuestSearch;
import com.ihg.automation.selenium.servicecenter.pages.catalog.CatalogItemDetailsPopUp;
import com.ihg.automation.selenium.servicecenter.pages.catalog.CatalogPage;

public class WithClosedMemberInContextTest extends CatalogCommon
{
    private CatalogPage catalogPage = new CatalogPage();
    private GuestSearch guestSearch = new GuestSearch();
    private static final String MEMBER_ID_CLOSED = "SELECT MBRSHP_ID"//
            + " FROM DGST.MBRSHP"//
            + " WHERE DGST.MBRSHP.MBRSHP_STAT_CD = 'C'"//
            + " AND DGST.MBRSHP.LYTY_PGM_CD = 'PC'"//
            + "  AND ROWNUM < 2";

    @BeforeClass
    public void beforeClass()
    {
        String memberId = MemberJdbcUtils.getMemberId(jdbcTemplate, MEMBER_ID_CLOSED);

        login();
        guestSearch.byMemberNumber(memberId);
        catalogPage.goTo();
    }

    @Test
    public void tryToOpenCatalog()
    {
        catalogPage.getButtonBar().clickSearch();

        CatalogItemDetailsPopUp catalogItemDetailsPopUp = catalogPage.openCatalogItemDetailsPopUp();
        catalogItemDetailsPopUp.clickAddToCart(
                verifyError(hasText(containsString("Customer Status is Closed. This Transaction is not allowed."))));
    }
}
