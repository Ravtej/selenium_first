package com.ihg.crm.automation.selenium.tests.servicecenter.regiontransfer;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.Constant.NOT_AVAILABLE;
import static com.ihg.automation.selenium.common.event.EventTransactionType.Profile.REGION_TRANSFER;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static com.ihg.automation.selenium.matchers.component.IsDisplayed.displayed;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.common.DateUtils;
import com.ihg.automation.selenium.common.event.AllEventsRow;
import com.ihg.automation.selenium.common.event.Source;
import com.ihg.automation.selenium.common.member.GuestAddress;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.servicecenter.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsGrid;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsGridRow;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsPage;
import com.ihg.automation.selenium.servicecenter.pages.event.all.RegionTransferContainer;
import com.ihg.automation.selenium.servicecenter.pages.personal.PersonalInfoPage;
import com.ihg.crm.automation.selenium.tests.servicecenter.LoginLogout;

public class RegionTransferTest extends LoginLogout
{
    private Member member = new Member();
    private AllEventsPage allEventsPage;
    private GuestAddress brazilAddress;
    private AllEventsGridRow row;
    private PersonalInfoPage persInfo;
    private AllEventsGrid grid;
    protected AllEventsRow rowData = new AllEventsRow(DateUtils.getFormattedDate(), REGION_TRANSFER,
            "Region Updated - CANADA", "", "");
    private Source source;

    @BeforeClass
    public void before()
    {
        source = new Source(NOT_AVAILABLE, NOT_AVAILABLE, NOT_AVAILABLE, NOT_AVAILABLE, user10T2.getUserId());

        member.addProgram(Program.RC);
        member.addAddress(MemberPopulateHelper.getBelarusAddress());
        member.setPersonalInfo(MemberPopulateHelper.getSimplePersonalInfo());

        brazilAddress = MemberPopulateHelper.getBrazilAddress();

        login();

        new EnrollmentPage().enroll(member);
    }

    @Test
    public void changeAddressWithoutChangingCountry()
    {
        persInfo = new PersonalInfoPage();
        persInfo.goTo();

        persInfo.getAddressList().getExpandedContact().update(MemberPopulateHelper.getBelarusSecondAddress());
    }

    @Test(dependsOnMethods = "changeAddressWithoutChangingCountry", alwaysRun = true)
    public void verifyRegionTransferRowNotCreated()
    {
        allEventsPage = new AllEventsPage();
        allEventsPage.goTo();
        verifyThat(allEventsPage.getGrid().getRegionTransferRow(), displayed(false));
    }

    @Test(dependsOnMethods = "verifyRegionTransferRowNotCreated", alwaysRun = true)
    public void changeCountryWithoutChangingRegion()
    {
        persInfo = new PersonalInfoPage();
        persInfo.goTo();

        persInfo.getAddressList().getExpandedContact().update(MemberPopulateHelper.getFrenchAddress());
    }

    @Test(dependsOnMethods = "changeCountryWithoutChangingRegion", alwaysRun = true)
    public void regionTransferEventNotCreatedWithoutChangingRegion()
    {
        allEventsPage = new AllEventsPage();
        allEventsPage.goTo();
        verifyThat(allEventsPage.getGrid().getRegionTransferRow(), displayed(false));
    }

    @Test(dependsOnMethods = "regionTransferEventNotCreatedWithoutChangingRegion", alwaysRun = true)
    public void changeCountryWithChangingRegion()
    {
        persInfo = new PersonalInfoPage();
        persInfo.goTo();

        persInfo.getAddressList().getExpandedContact().update(MemberPopulateHelper.getCanadaAddress());
    }

    @Test(dependsOnMethods = "changeCountryWithChangingRegion", alwaysRun = true)
    public void verifyRegionTransferRow()
    {
        allEventsPage = new AllEventsPage();
        allEventsPage.goTo();
        grid = allEventsPage.getGrid();
        row = grid.getRegionTransferRow("Region Updated - CANADA");
        row.verify(rowData);
    }

    @Test(dependsOnMethods = "verifyRegionTransferRow", alwaysRun = true)
    public void verifyRegionTransferDetails()
    {
        RegionTransferContainer regionTransferContainer = row.expand(RegionTransferContainer.class);
        regionTransferContainer.getSource().verify(source);
    }

    @Test(dependsOnMethods = "verifyRegionTransferDetails", alwaysRun = true)
    public void changePreferredCountry()
    {
        persInfo = new PersonalInfoPage();
        persInfo.goTo();

        brazilAddress.setPreferred(true);
        persInfo.getAddressList().add(brazilAddress);
    }

    @Test(dependsOnMethods = "changePreferredCountry", alwaysRun = true)
    public void verifyRegionTransferEventCreated()
    {
        allEventsPage = new AllEventsPage();
        allEventsPage.goTo();

        rowData.setDetail("Region Updated - MSAC");
        grid = allEventsPage.getGrid();
        grid.getRow(1).verify(rowData);

        verifyThat(grid.getRow(2).getCell(AllEventsGridRow.AllEventsCell.TRANS_TYPE), hasText(REGION_TRANSFER));
    }

    @Test(dependsOnMethods = "verifyRegionTransferEventCreated", alwaysRun = true)
    public void removePreferredCountry()
    {
        persInfo = new PersonalInfoPage();
        persInfo.goTo();

        brazilAddress.setPreferred(true);
        persInfo.getAddressList().getExpandedContact().remove();
    }

    @Test(dependsOnMethods = "removePreferredCountry", alwaysRun = true)
    public void verifyEventAfterRemoving()
    {
        allEventsPage = new AllEventsPage();
        allEventsPage.goTo();
        rowData.setDetail("Region Updated - CANADA");
        row = grid.getRow(1);
        row.verify(rowData);

        verifyThat(grid.getRow(2).getCell(AllEventsGridRow.AllEventsCell.TRANS_TYPE), hasText(REGION_TRANSFER));
        verifyThat(grid.getRow(3).getCell(AllEventsGridRow.AllEventsCell.TRANS_TYPE), hasText(REGION_TRANSFER));
    }

    @Test(dependsOnMethods = "verifyEventAfterRemoving", alwaysRun = true)
    public void verifyRegionTransferDetailsAfterRemoving()
    {
        RegionTransferContainer regionTransferContainer = row.expand(RegionTransferContainer.class);
        regionTransferContainer.getSource().verify(source);
    }
}
