package com.ihg.crm.automation.selenium.tests.servicecenter.unit;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.assertThat;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.Constant.RC_POINTS;
import static com.ihg.automation.selenium.common.event.EventTransactionType.Loyalty.TRANSFER;
import static com.ihg.automation.selenium.common.event.EventTransactionType.Loyalty.TRANSFER_PURCHASE;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasDefault;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasTextAsInt;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasTextWithWait;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.isDisplayedWithWait;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.isHighlightedAsInvalid;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.isSelected;
import static com.ihg.automation.selenium.matchers.component.HasSelectItems.hasSelectItems;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static com.ihg.automation.selenium.matchers.component.ViewableMatcher.hasTextInView;
import static com.ihg.automation.selenium.servicecenter.pages.annual.AnnualActivityRow.AnnualActivityCell.QUALIFIED_ROOM;
import static com.ihg.automation.selenium.servicecenter.pages.annual.AnnualActivityRow.AnnualActivityCell.TIER_LEVEL_NIGHT;
import static org.hamcrest.number.OrderingComparison.greaterThan;
import static org.hamcrest.number.OrderingComparison.lessThan;

import java.util.Arrays;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.billing.EventBillingDetailsTab;
import com.ihg.automation.selenium.common.event.AllEventsRow;
import com.ihg.automation.selenium.common.event.AllEventsRowFactory;
import com.ihg.automation.selenium.common.member.LifetimeActivity;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.pages.CreditCardType;
import com.ihg.automation.selenium.common.pages.PaymentMethod;
import com.ihg.automation.selenium.common.payment.CreditCardPayment;
import com.ihg.automation.selenium.common.payment.CreditCardPaymentContainer;
import com.ihg.automation.selenium.common.payment.PaymentDetailsPopUp;
import com.ihg.automation.selenium.common.stay.Stay;
import com.ihg.automation.selenium.common.types.Currency;
import com.ihg.automation.selenium.common.types.program.ProgramStatus;
import com.ihg.automation.selenium.common.types.program.RewardClubLevel;
import com.ihg.automation.selenium.gwt.components.Component;
import com.ihg.automation.selenium.gwt.components.Mode;
import com.ihg.automation.selenium.gwt.components.input.Select;
import com.ihg.automation.selenium.servicecenter.pages.LeftPanel;
import com.ihg.automation.selenium.servicecenter.pages.annual.AnnualActivityPage;
import com.ihg.automation.selenium.servicecenter.pages.annual.AnnualActivityRow;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsGridRow;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsPage;
import com.ihg.automation.selenium.servicecenter.pages.event.stay.StayEventsPage;
import com.ihg.automation.selenium.servicecenter.pages.event.unit.UnitDetails;
import com.ihg.automation.selenium.servicecenter.pages.event.unit.UnitDetailsPopUp;
import com.ihg.automation.selenium.servicecenter.pages.event.unit.UnitDetailsTab;
import com.ihg.automation.selenium.servicecenter.pages.event.unit.UnitGridRowContainer;
import com.ihg.automation.selenium.servicecenter.pages.personal.PersonalInfoPage;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.RewardClubPage;
import com.ihg.automation.selenium.servicecenter.pages.unit.GoodwillPopUp;
import com.ihg.automation.selenium.servicecenter.pages.unit.TransferUnitsPopUp;

public class TransferUnitsTest extends UnitsCommon
{
    private Member originator, recipient;
    private Stay stay = new Stay();

    private static final int POINTS_TO_SEND = 20000;
    private static final String POINTS_TO_SEND_STRING = Integer.toString(POINTS_TO_SEND);
    private static final String POINTS_REDUCED_STRING = Integer.toString(-POINTS_TO_SEND);

    private static final String EXPECTED_AMOUNT = "100.00";
    private static final String EXPECTED_CURRENCY = Currency.USD.getCode();

    private CreditCardPayment creditCard = new CreditCardPayment(EXPECTED_AMOUNT, EXPECTED_CURRENCY, "4966648225694538",
            CreditCardType.VISA);

    private TransferUnitsPopUp transUnitsPopUp = new TransferUnitsPopUp();
    private PaymentDetailsPopUp paymentPopUp = new PaymentDetailsPopUp();

    @BeforeClass
    public void beforeClass()
    {
        stay.setHotelCode("ATLCP");
        stay.setAvgRoomRate(100.00);
        stay.setRateCode("TEST");
        stay.setHotelCurrency(Currency.USD);
        stay.setCheckInOutByNights(6);

        login();

        recipient = new AnnualActivityPage().captureCounters(enrollMember(), jdbcTemplate);

        new LeftPanel().goToNewSearch();

        originator = enrollMember();

        /** Qualified Units and Nights **/
        new StayEventsPage().createStay(stay);
        /** Total Units **/
        new GoodwillPopUp().goodwillPoints("15000");

        AnnualActivityPage annualActivityPage = new AnnualActivityPage();
        annualActivityPage.captureCounters(originator, jdbcTemplate);

        verifyThat(annualActivityPage.getBaseUnits().getAmount(), hasTextAsInt(greaterThan(0)), "For stay");

        // Do basic amount verification
        verifyThat(annualActivityPage.getCurrentBalance().getAmount(), hasTextAsInt(greaterThan(POINTS_TO_SEND)));

        AnnualActivityRow activityRow = annualActivityPage.getAnnualActivities().getCurrentYearRow();
        verifyThat(activityRow.getCell(TIER_LEVEL_NIGHT), hasTextAsInt(greaterThan(0)));
        verifyThat(activityRow.getCell(QUALIFIED_ROOM), hasTextAsInt(greaterThan(0)));
    }

    @Test
    public void openTransferUnitsPopUp()
    {
        transUnitsPopUp.goTo();
    }

    @Test(dependsOnMethods = { "openTransferUnitsPopUp" })
    public void verifyTransferUnitsPopUp()
    {
        verifyThat(transUnitsPopUp.getMembershipId(), hasDefault());
        verifyThat(transUnitsPopUp.getMemberName(), hasDefault());

        verifyThat(transUnitsPopUp.getBalanceTransfer(), isSelected(false));

        Select amount = transUnitsPopUp.getAmount();
        verifyThat(amount, hasText("Select Quantity"));
        verifyThat(amount, hasSelectItems(getExpectedAmounts(1000, 40000, 1000)));

        Select unitType = transUnitsPopUp.getUnitType();
        verifyThat(unitType, hasText(RC_POINTS));
        verifyThat(unitType, hasSelectItems(Arrays.asList(RC_POINTS)));
    }

    @Test(dependsOnMethods = { "verifyTransferUnitsPopUp" }, alwaysRun = true)
    public void verifyExceedingAmount()
    {
        Component balance = new LeftPanel().getProgramInfoPanel().getPrograms().getRewardClubProgram()
                .getPointsBalance();

        assertThat(balance, hasTextAsInt(lessThan(30000)),
                "Assert that points balance is less than selected amount = 30000");

        transUnitsPopUp.getMembershipId().typeAndWait(recipient.getRCProgramId());
        transUnitsPopUp.getAmount().select(30000);
        transUnitsPopUp.getSubmit().clickAndWait();
        verifyThat(transUnitsPopUp, displayed(true));

        verifyThat(transUnitsPopUp.getAmount(), isHighlightedAsInvalid(true));
    }

    @Test(dependsOnMethods = { "verifyExceedingAmount" }, alwaysRun = true)
    public void submitTransferUnitsPopUp()
    {
        transUnitsPopUp.getMembershipId().typeAndWait(recipient.getRCProgramId());
        transUnitsPopUp.getAmount().select(POINTS_TO_SEND);

        verifyThat(transUnitsPopUp.getMemberName(), hasTextWithWait(recipient.getName().getNameOnPanel()));
        transUnitsPopUp.clickSubmitNoError();
    }

    @Test(dependsOnMethods = { "submitTransferUnitsPopUp" })
    public void verifyPaymentDetailsPopUp()
    {
        verifyThat(paymentPopUp, isDisplayedWithWait());

        verifyThat(paymentPopUp.getMembershipId(), hasText(recipient.getRCProgramId()));
        verifyThat(paymentPopUp.getMemberName(), hasTextWithWait(recipient.getName().getNameOnPanel()));

        verifyThat(paymentPopUp.getTotalAmount(), hasTextInView(EXPECTED_AMOUNT + " " + EXPECTED_CURRENCY));
    }

    @Test(dependsOnMethods = { "verifyPaymentDetailsPopUp" }, alwaysRun = true)
    public void submitPayment()
    {
        paymentPopUp.selectPaymentType(PaymentMethod.CREDIT_CARD);
        CreditCardPaymentContainer creditCardPayment = paymentPopUp.getCreditCardPayment();
        verifyThat(creditCardPayment.getType(), hasSelectItems(CreditCardType.getDataList()));
        creditCardPayment.populate(creditCard);

        paymentPopUp.clickSubmitAndVerifySuccessMsg();
    }

    @Test(dependsOnMethods = { "submitPayment" })
    public void verifyOriginatorGiftPurchaseEvent()
    {
        AllEventsPage page = new AllEventsPage();
        page.goTo();

        AllEventsGridRow row = page.getGrid().getRow(1);
        AllEventsRow transferPurchase = AllEventsRowFactory.getPointEvent(TRANSFER_PURCHASE, POINTS_REDUCED_STRING);
        row.verify(transferPurchase);

        UnitDetails details = row.expand(UnitGridRowContainer.class).getUnitDetails();
        details.verifyRelatedMemberID(recipient);
        details.getPaymentDetails().verify(creditCard, Mode.VIEW);
        details.verifySource(helper);
    }

    @Test(dependsOnMethods = { "verifyOriginatorGiftPurchaseEvent" }, alwaysRun = true)
    public void navigateOriginatorDetailsPopUp()
    {
        openDetailsPopUp(TRANSFER_PURCHASE);
    }

    @Test(dependsOnMethods = { "navigateOriginatorDetailsPopUp" })
    public void verifyOriginatorLoyaltyUnitsDetailsTab()
    {
        UnitDetailsTab unitDetailsTab = new UnitDetailsPopUp().getUnitDetailsTab();
        unitDetailsTab.goTo();

        UnitDetails details = unitDetailsTab.getUnitDetails();
        details.verifyRelatedMemberID(recipient);
        details.getPaymentDetails().verify(creditCard, Mode.VIEW);
        details.verifySource(helper);
    }

    @Test(dependsOnMethods = { "verifyOriginatorLoyaltyUnitsDetailsTab" }, alwaysRun = true)
    public void verifyOriginatorEarningDetailsTab()
    {
        new UnitDetailsPopUp().getEarningDetailsTab().verifyForBaseUnitsEvent(-POINTS_TO_SEND, 0);
    }

    @Test(dependsOnMethods = { "verifyOriginatorEarningDetailsTab" }, alwaysRun = true)
    public void verifyOriginatorBillingDetailsTab()
    {
        EventBillingDetailsTab billingDetailsTab = new UnitDetailsPopUp().getBillingDetailsTab();
        billingDetailsTab.goTo();
        billingDetailsTab.verifyTabIsEmpty(TRANSFER_PURCHASE);
    }

    @Test(dependsOnMethods = { "verifyOriginatorBillingDetailsTab" }, alwaysRun = true)
    public void closeOriginatorUnitDetailsPopUp()
    {
        new UnitDetailsPopUp().close();
    }

    @Test(dependsOnMethods = { "closeOriginatorUnitDetailsPopUp" })
    public void verifyOriginatorAnnualActivityPage()
    {
        LifetimeActivity lifetimeActivity = originator.getLifetimeActivity();
        lifetimeActivity.setCurrentBalance(lifetimeActivity.getCurrentBalance() - POINTS_TO_SEND);
        lifetimeActivity.setAdjustedUnits(lifetimeActivity.getAdjustedUnits() - POINTS_TO_SEND);

        originator.getAnnualActivity().setTotalUnits(originator.getAnnualActivity().getTotalUnits() - POINTS_TO_SEND);

        new AnnualActivityPage().verifyCounters(originator);
    }

    @Test(dependsOnMethods = { "verifyOriginatorAnnualActivityPage" }, alwaysRun = true)
    public void navigateRecipientAccount()
    {
        openRecipientProfileByLink(recipient, 1);

        new PersonalInfoPage().getCustomerInfo().verify(recipient, Mode.VIEW);
    }

    @Test(dependsOnMethods = { "navigateRecipientAccount" })
    public void verifyRecipientAnnualActivityPage()
    {
        LifetimeActivity lifetimeActivity = recipient.getLifetimeActivity();
        lifetimeActivity.setAdjustedUnits(POINTS_TO_SEND);
        lifetimeActivity.setTotalUnits(POINTS_TO_SEND);
        lifetimeActivity.setCurrentBalance(POINTS_TO_SEND);

        recipient.getAnnualActivity().setTotalUnits(POINTS_TO_SEND);

        new AnnualActivityPage().verifyCounters(recipient);
    }

    @Test(dependsOnMethods = { "verifyRecipientAnnualActivityPage" }, alwaysRun = true)
    public void verifyRecipientTierLevel()
    {
        RewardClubPage rwrdsPage = new RewardClubPage();
        rwrdsPage.goTo();
        rwrdsPage.getSummary().verify(ProgramStatus.OPEN, RewardClubLevel.CLUB);
    }

    @Test(dependsOnMethods = { "verifyRecipientAnnualActivityPage" }, alwaysRun = true)
    public void verifyRecipientTransferEvent()
    {
        AllEventsPage page = new AllEventsPage();
        page.goTo();

        AllEventsGridRow row = page.getGrid().getRow(1);
        AllEventsRow transfer = AllEventsRowFactory.getPointEvent(TRANSFER, POINTS_TO_SEND_STRING);
        row.verify(transfer);

        UnitDetails unitDetails = row.expand(UnitGridRowContainer.class).getUnitDetails();
        unitDetails.verifyRelatedMemberID(originator);
        verifyThat(unitDetails.getPaymentDetails().getMethod(), displayed(false));
        unitDetails.verifySource(helper);
    }

    @Test(dependsOnMethods = { "verifyRecipientTransferEvent" }, alwaysRun = true)
    public void navigateRecipientDetailsPopUp()
    {
        openDetailsPopUp(TRANSFER);
    }

    @Test(dependsOnMethods = { "navigateRecipientDetailsPopUp" })
    public void verifyRecipientBillingDetails()
    {
        new UnitDetailsPopUp().getBillingDetailsTab().verifyBaseUSDBilling(TRANSFER, "100", "CPMPT");
    }

    @Test(dependsOnMethods = { "navigateRecipientDetailsPopUp" })
    public void verifyRecipientEarningDetails()
    {
        new UnitDetailsPopUp().getEarningDetailsTab().verifyForBaseUnitsEvent(POINTS_TO_SEND, 0);
    }

    @Test(dependsOnMethods = { "navigateRecipientDetailsPopUp" })
    public void verifyRecipientLoyaltyUnitsDetails()
    {
        UnitDetailsPopUp unitPopUp = new UnitDetailsPopUp();
        unitPopUp.getUnitDetailsTab().goTo();

        UnitDetails details = unitPopUp.getUnitDetailsTab().getUnitDetails();
        details.verifyRelatedMemberID(originator);
        verifyThat(details.getPaymentDetails().getMethod(), displayed(false));
        details.verifySource(helper);
    }

}
