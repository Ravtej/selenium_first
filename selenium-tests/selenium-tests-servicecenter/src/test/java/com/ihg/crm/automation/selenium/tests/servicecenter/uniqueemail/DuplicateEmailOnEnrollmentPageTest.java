package com.ihg.crm.automation.selenium.tests.servicecenter.uniqueemail;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.HasTipText.hasWarningTipTextWithWait;
import static com.ihg.automation.selenium.servicecenter.pages.Messages.EMAIL_ADDRESS_ALREADY_EXISTS;
import static com.ihg.automation.selenium.servicecenter.pages.Messages.EXCEEDS_THE_LIMIT;
import static com.ihg.automation.selenium.common.enroll.EmailConstant.EMAIL_WITH_10_DUPLICATES;
import static com.ihg.automation.selenium.common.enroll.EmailConstant.EMAIL_WITH_LESS_THAN_10_DUPLICATES;
import static com.ihg.automation.selenium.common.enroll.EmailConstant.EMAIL_WITH_MORE_THAN_10_DUPLICATES;

import org.springframework.beans.factory.annotation.Value;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.enroll.DuplicateEmailFoundPopUp;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.servicecenter.pages.GuestSearch;
import com.ihg.automation.selenium.servicecenter.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.RewardClubPage;
import com.ihg.crm.automation.selenium.tests.servicecenter.LoginLogout;

public class DuplicateEmailOnEnrollmentPageTest extends LoginLogout
{
    private EnrollmentPage enrollmentPage;

    @Value("${closedMemberWith10DuplicateEmails}")
    protected String closedMemberWith10DuplicateEmails;

    @BeforeClass
    public void before()
    {
        Member member = new Member();
        member.addProgram(Program.RC);
        member.setPersonalInfo(MemberPopulateHelper.getSimplePersonalInfo());
        member.addAddress(MemberPopulateHelper.getUnitedStatesAddress());

        login();

        enrollmentPage = new EnrollmentPage();
        enrollmentPage.goTo();
        enrollmentPage.populate(member);
    }

    @DataProvider(name = "emailProvider")
    public Object[][] emailProvider()
    {
        return new Object[][] { { EMAIL_WITH_MORE_THAN_10_DUPLICATES, EXCEEDS_THE_LIMIT },
                { EMAIL_WITH_10_DUPLICATES, EXCEEDS_THE_LIMIT }, { EMAIL_WITH_LESS_THAN_10_DUPLICATES, "" } };
    }

    @Test(dataProvider = "emailProvider")
    public void tryToEnrollMemberWithDuplicatedEmail(String duplicateEmail, String message)
    {
        enrollmentPage.getEmail().getEmail().type(duplicateEmail);
        enrollmentPage.clickSubmit();

        DuplicateEmailFoundPopUp duplicateEmailFoundPopUp = new DuplicateEmailFoundPopUp();
        duplicateEmailFoundPopUp.verifyWarningMessage(message);
        duplicateEmailFoundPopUp.clickUpdate();

        verifyThat(enrollmentPage.getEmail().getEmail(), hasWarningTipTextWithWait(EMAIL_ADDRESS_ALREADY_EXISTS));
    }

    @Test(dependsOnMethods = { "tryToEnrollMemberWithDuplicatedEmail" }, alwaysRun = true)
    public void tryToOpenClosedMemberWithDuplicatedEmail()
    {
        new GuestSearch().byMemberNumber(closedMemberWith10DuplicateEmails);
        RewardClubPage rewardClubPage = new RewardClubPage();
        rewardClubPage.goTo();
        rewardClubPage.getSummary().setOpenStatus();

        DuplicateEmailFoundPopUp duplicateEmailFoundPopUp = new DuplicateEmailFoundPopUp();
        duplicateEmailFoundPopUp.verifyWarningMessage(EXCEEDS_THE_LIMIT);
        duplicateEmailFoundPopUp.clickAbandonReopen();
    }
}
