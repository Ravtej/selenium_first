package com.ihg.crm.automation.selenium.tests.servicecenter.enrollment.dr;

import static com.ihg.automation.common.DateUtils.DATE_FORMAT;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.event.EventTransactionType.Order.ORDER_SYSTEM;
import static com.ihg.automation.selenium.common.offer.OfferFactory.DELAYED_FULFILMENT_GLOBAL;
import static com.ihg.automation.selenium.common.types.program.Program.RC;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.automation.selenium.matchers.component.GridSize.size;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static com.ihg.automation.selenium.matchers.component.ViewableMatcher.hasTextInView;
import static org.hamcrest.core.StringContains.containsString;
import static org.hamcrest.core.StringStartsWith.startsWith;

import java.util.List;

import javax.annotation.Resource;

import org.joda.time.LocalDate;

import com.ihg.automation.common.DateUtils;
import com.ihg.automation.selenium.common.Constant;
import com.ihg.automation.selenium.common.User;
import com.ihg.automation.selenium.common.event.AllEventsRow;
import com.ihg.automation.selenium.common.event.AllEventsRowFactory;
import com.ihg.automation.selenium.common.types.program.ProgramStatus;
import com.ihg.automation.selenium.servicecenter.pages.account.AccountInfoPage;
import com.ihg.automation.selenium.servicecenter.pages.account.FailedSelfServiceLoginAttempts;
import com.ihg.automation.selenium.servicecenter.pages.account.Security;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsGrid;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsPage;
import com.ihg.automation.selenium.servicecenter.pages.programs.ProgramPageBase.EnrollmentDetails;
import com.ihg.automation.selenium.servicecenter.pages.programs.RenewExtendProgramSummary;
import com.ihg.automation.selenium.servicecenter.pages.programs.dr.DiningRewardsPage;
import com.ihg.crm.automation.selenium.tests.servicecenter.LoginLogout;

public class EnrollToDrCommon extends LoginLogout
{
    @Resource(name = "user15U")
    protected User user15U;

    protected AllEventsRow enrollRC = AllEventsRowFactory.getEnrollEvent(RC);
    protected AllEventsRow orderSystem = new AllEventsRow(ORDER_SYSTEM, null);
    protected AllEventsRow delayedFulfilmentOffer = AllEventsRowFactory
            .getOfferRegistrationEvent(DELAYED_FULFILMENT_GLOBAL);

    protected final static String EXP_DATE = DateUtils.getMonthsForward(13, DateUtils.EXPIRATION_DATE_PATTERN);

    public void validateAccountInformation()
    {
        AccountInfoPage accountInfoPage = new AccountInfoPage();
        accountInfoPage.goTo();
        accountInfoPage.verify(ProgramStatus.OPEN);

        Security secur = accountInfoPage.getSecurity();
        verifyThat(secur.getLockout(), hasTextInView(Constant.NOT_AVAILABLE));
        verifyThat(secur.getPin(), hasTextInView(Constant.NOT_AVAILABLE));
        verifyThat(secur.getUserName(), hasText(Constant.NOT_AVAILABLE));
        verifyThat(accountInfoPage.getFlags(), hasText(startsWith("No flags selected")));
        FailedSelfServiceLoginAttempts attempts = accountInfoPage.getFailedSelfServiceLoginAttempts();
        attempts.expand();
        verifyThat(attempts.getGrid(), size(0));
    }

    public void validateDiningRewardClubPage(String memberId, String expDate, LocalDate enrollDate)
    {
        DiningRewardsPage diningRewardsPage = new DiningRewardsPage();
        diningRewardsPage.goTo();

        RenewExtendProgramSummary summary = diningRewardsPage.getSummary();
        verifyThat(summary.getMemberId(), hasText(memberId));
        verifyThat(summary, hasText(containsString("No flags selected")));
        verifyThat(summary.getExpirationDate(), hasText(expDate));

        EnrollmentDetails enrollDetails = diningRewardsPage.getEnrollmentDetails();

        enrollDetails.verifyScSource(helper);
        verifyThat(enrollDetails.getEnrollDate(), hasText(enrollDate.toString(DATE_FORMAT)));
        verifyThat(enrollDetails.getReferringMember(), hasText(Constant.NOT_AVAILABLE));
        verifyThat(enrollDetails.getOfferCode(), hasText(Constant.NOT_AVAILABLE));
        verifyThat(enrollDetails.getRenewalDate(), hasText(Constant.NOT_AVAILABLE));
    }

    public void validateEvents(List<AllEventsRow> events, int gridSize)
    {
        AllEventsPage allEventsPage = new AllEventsPage();
        allEventsPage.goTo();
        AllEventsGrid allEventsGrid = allEventsPage.getGrid();

        verifyThat(allEventsGrid, size(gridSize));
        for (int i = 0; i < events.size(); i++)
        {
            verifyThat(allEventsGrid.getRow(events.get(i).getTransType(), events.get(i).getDetail()), displayed(true));
        }
    }
}
