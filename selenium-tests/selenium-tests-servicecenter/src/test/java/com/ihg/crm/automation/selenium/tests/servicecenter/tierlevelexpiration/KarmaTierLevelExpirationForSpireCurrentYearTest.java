package com.ihg.crm.automation.selenium.tests.servicecenter.tierlevelexpiration;

import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyNoError;
import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifySuccess;
import static com.ihg.automation.selenium.common.types.program.KarmaLevel.KAR;
import static com.ihg.automation.selenium.common.types.program.KarmaLevel.KIC;
import static com.ihg.automation.selenium.common.types.program.ProgramStatus.OPEN;
import static com.ihg.automation.selenium.common.types.program.ProgramStatusReason.FROZEN;
import static com.ihg.automation.selenium.common.types.program.RewardClubLevel.SPRE;
import static com.ihg.automation.selenium.common.types.program.RewardClubLevelReason.BASE;
import static com.ihg.automation.selenium.common.types.program.RewardClubLevelReason.INNER_CIRCLE;
import static com.ihg.automation.selenium.common.types.program.TierLevelExpiration.EXPIRE_CURRENT_YEAR;
import static com.ihg.automation.selenium.common.types.program.TierLevelExpiration.EXPIRE_NEXT_YEAR;
import static com.ihg.automation.selenium.common.types.program.TierLevelExpiration.LIFETIME;
import static com.ihg.automation.selenium.servicecenter.pages.personal.PersonalInfoPage.SUCCESS_MESSAGE;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.servicecenter.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.servicecenter.pages.programs.KarmaPage;
import com.ihg.automation.selenium.servicecenter.pages.programs.ProgramSummary;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.RewardClubPage;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.RewardClubSummary;
import com.ihg.crm.automation.selenium.tests.servicecenter.LoginLogout;

@Test(groups = { "karma" })
public class KarmaTierLevelExpirationForSpireCurrentYearTest extends LoginLogout
{
    private KarmaPage karmaPage = new KarmaPage();
    private ProgramSummary karmaSummary;
    private RewardClubPage rewardClubPage = new RewardClubPage();

    private RewardClubSummary rewardClubSummary;

    @BeforeClass
    public void beforeClass()
    {
        Member member = new Member();
        member.setPersonalInfo(MemberPopulateHelper.getSimplePersonalInfo());
        member.addAddress(MemberPopulateHelper.getUnitedStatesAddress());
        member.addProgram(Program.KAR);

        login();

        new EnrollmentPage().enroll(member);
        rewardClubPage.goTo();
        rewardClubSummary = rewardClubPage.getSummary();
        rewardClubSummary.setTierLevelWithExpiration(SPRE, BASE, EXPIRE_CURRENT_YEAR, verifyNoError(),
                verifySuccess(SUCCESS_MESSAGE));
        rewardClubSummary.verify(OPEN, SPRE, 0, BASE);
    }

    @Test(priority = 1)
    public void setKicWithCurrentYearExpVerifySpireNotChanged()
    {
        karmaPage.goTo();
        karmaSummary = karmaPage.getSummary();
        karmaSummary.setTierLevelWithExpiration(KIC, BASE, EXPIRE_CURRENT_YEAR, verifyNoError(),
                verifySuccess(SUCCESS_MESSAGE));

        rewardClubPage.goTo();
        rewardClubSummary.verify(OPEN, SPRE, 0, BASE);
    }

    @Test(priority = 10)
    public void setKicLevelWithNextYearExpiration()
    {
        karmaPage.goTo();
        karmaSummary = karmaPage.getSummary();
        karmaSummary.setTierLevelWithoutExpiration(KAR, BASE);
        karmaSummary.setTierLevelWithExpiration(KIC, BASE, EXPIRE_NEXT_YEAR, verifyNoError(),
                verifySuccess(SUCCESS_MESSAGE));

        rewardClubPage.goTo();
        rewardClubSummary.verify(OPEN, SPRE, 1, INNER_CIRCLE);
    }

    @Test(priority = 15)
    public void setKarAndVerifyRCLevelExpirationNotChanged()
    {
        karmaPage.goTo();
        karmaSummary.setTierLevelWithoutExpiration(KAR, BASE);

        rewardClubPage.goTo();
        rewardClubSummary.verify(OPEN, SPRE, 1, INNER_CIRCLE);
    }

    @Test(priority = 20)
    public void setKicLevelWithLifetimeExpiration()
    {
        karmaPage.goTo();
        karmaSummary.setTierLevelWithExpiration(KIC, BASE, LIFETIME, verifyNoError(), verifySuccess(SUCCESS_MESSAGE));

        rewardClubPage.goTo();
        rewardClubSummary.verifyLifetimeTierLevel(OPEN, SPRE, INNER_CIRCLE);
    }

    @Test(priority = 25)
    public void closeKarmaAndVerifyRCLevelExpirationNotChanged()
    {
        karmaPage.goTo();
        karmaSummary.setClosedStatus(FROZEN);

        rewardClubPage.goTo();
        rewardClubSummary.verifyLifetimeTierLevel(OPEN, SPRE, INNER_CIRCLE);
    }

    @Test(priority = 30)
    public void reOpenKarmaAndVerifyRCLevelExpirationNotChanged()
    {
        karmaPage.goTo();
        karmaSummary.setOpenStatus();

        rewardClubPage.goTo();
        rewardClubSummary.verifyLifetimeTierLevel(OPEN, SPRE, INNER_CIRCLE);
    }

}
