package com.ihg.crm.automation.selenium.tests.servicecenter.enrollment.br;

import static com.ihg.atp.schema.crm.guest.membership.datatypes.v1.SourceType.SC;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasTextWithWait;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static com.ihg.automation.selenium.matchers.component.ViewableMatcher.hasTextInView;

import java.util.ArrayList;
import java.util.List;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.atp.schema.crm.common.contact.datatypes.v2.AddressContactType;
import com.ihg.atp.schema.crm.common.contact.datatypes.v2.UsageTypeType;
import com.ihg.atp.schema.crm.common.location.address.datatypes.v2.AddressType;
import com.ihg.atp.schema.crm.common.loyalty.loyalty.datatypes.v2.SourceType;
import com.ihg.atp.schema.crm.guest.guest.datatypes.v2.AddProfileItemChoiceType;
import com.ihg.atp.schema.crm.guest.guest.datatypes.v2.Addresses;
import com.ihg.atp.schema.crm.guest.guest.datatypes.v2.ExtendedPersonNameType;
import com.ihg.atp.schema.crm.guest.guest.datatypes.v2.GuestConsentType;
import com.ihg.atp.schema.crm.guest.guest.datatypes.v2.GuestIdentityType;
import com.ihg.atp.schema.crm.guest.guest.datatypes.v2.Names;
import com.ihg.atp.schema.crm.guest.guest.datatypes.v2.ProfileType;
import com.ihg.atp.schema.crm.guest.guest.member.v2.ProgramMembershipKey;
import com.ihg.atp.schema.crm.guest.guest.servicetypes.v2.AddProfileItemRequestType;
import com.ihg.atp.schema.crm.guest.membership.datatypes.v1.ActivitySourceType;
import com.ihg.atp.schema.crm.guest.membership.datatypes.v1.EnrollmentRequestType;
import com.ihg.atp.schema.crm.guest.membership.datatypes.v1.EnrollmentRequests;
import com.ihg.atp.schema.crm.guest.membership.datatypes.v1.MembershipInfoType;
import com.ihg.atp.schema.crm.guest.membership.servicetypes.v1.EnrollGuestRequestType;
import com.ihg.atp.schema.crm.guest.membership.servicetypes.v1.EnrollGuestResponseType;
import com.ihg.automation.common.DateUtils;
import com.ihg.automation.selenium.common.UI;
import com.ihg.automation.selenium.common.member.GuestAddress;
import com.ihg.automation.selenium.common.member.GuestName;
import com.ihg.automation.selenium.common.member.MemberJdbcUtils;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.common.types.program.ProgramStatus;
import com.ihg.automation.selenium.servicecenter.pages.GuestSearch;
import com.ihg.automation.selenium.servicecenter.pages.programs.br.BusinessRewardsPage;
import com.ihg.automation.selenium.servicecenter.pages.programs.br.BusinessRewardsSummary;
import com.ihg.crm.automation.selenium.tests.servicecenter.LoginLogout;

@Test(groups = { "brTest" })
public class AcceptTsCsTest extends LoginLogout
{
    private BusinessRewardsPage businessRewardsPage;

    @BeforeClass
    public void beforeClass()
    {
        EnrollGuestResponseType enrollGuestResponseType = membershipClient.enrollGuest(createEnrollRequest());

        String memberId = enrollGuestResponseType.getMemberships().getMembership().get(0).getMemberId();
        MemberJdbcUtils.waitAnnualActivitiesCounter(jdbcTemplate, memberId);

        guestClient.addProfileItem(createAcceptRequest(memberId));

        login();

        new GuestSearch().byMemberNumber(memberId);
    }

    @Test
    public void verifyBusinessRewardsPage()
    {
        businessRewardsPage = new BusinessRewardsPage();
        businessRewardsPage.goTo();

        BusinessRewardsSummary summary = businessRewardsPage.getSummary();
        verifyThat(summary.getStatus(), hasTextInView(ProgramStatus.OPEN));
        verifyThat(summary.getTermsAndConditions(), hasTextWithWait("Accepted"));
        verifyThat(summary.getSource(), hasText("Website"));
        verifyThat(summary.getDateOfResponse(), hasText(DateUtils.getFormattedDate()));

        verifyThat(summary.getDeclinedTsAndCs(), displayed(false));
    }

    private EnrollGuestRequestType createEnrollRequest()
    {
        EnrollGuestRequestType request = new EnrollGuestRequestType();

        ProfileType profileType = new ProfileType();
        profileType.setAddresses(getAddresses());
        profileType.setNames(getName());

        request.setEnrollmentRequests(getEnrollRequests());
        request.setProfile(profileType);

        return request;
    }

    private Addresses getAddresses()
    {
        GuestAddress guestAddress = MemberPopulateHelper.getUnitedStatesAddress();
        AddressType addressType = new AddressType();

        addressType.setCountryCode(guestAddress.getCountry().getCode());
        addressType.setLine1(guestAddress.getLine1());
        addressType.setLocality1(guestAddress.getLocality1());
        addressType.setRegion1(guestAddress.getRegion1().getCode());
        addressType.setPostalCode(guestAddress.getPostalCode());

        AddressContactType addressContactType = new AddressContactType();
        addressContactType.setAddress(addressType);
        addressContactType.setUsageType(UsageTypeType.HOME);

        List<AddressContactType> addressContactTypes = new ArrayList<AddressContactType>();
        addressContactTypes.add(addressContactType);

        Addresses addresses = new Addresses();
        addresses.setAddress(addressContactTypes);

        return addresses;
    }

    private Names getName()
    {
        GuestName guestName = MemberPopulateHelper.getRandomName();
        ExtendedPersonNameType personNameType = new ExtendedPersonNameType();

        personNameType.setGiven(guestName.getGiven());
        personNameType.setSurname(guestName.getSurname());

        List<ExtendedPersonNameType> namesList = new ArrayList<ExtendedPersonNameType>();
        namesList.add(personNameType);

        Names name = new Names();
        name.setName(namesList);

        return name;
    }

    private EnrollmentRequests getEnrollRequests()
    {
        MembershipInfoType membershipInfoType = new MembershipInfoType();
        membershipInfoType.setProgramCode("PC");

        ActivitySourceType activitySourceType = new ActivitySourceType();
        activitySourceType.setChannel(UI.SC.getChannel());
        activitySourceType.setCode(SC);
        activitySourceType.setLocationCode("ATLCP");

        EnrollmentRequestType enrollmentRequestType = new EnrollmentRequestType();
        enrollmentRequestType.setMembership(membershipInfoType);
        enrollmentRequestType.setSource(activitySourceType);
        enrollmentRequestType.setOfferTreatmentId("SCCAL");

        MembershipInfoType membershipInfoTypeBR = new MembershipInfoType();
        membershipInfoTypeBR.setProgramCode(Program.BR.getCode());

        ActivitySourceType activitySourceTypeBR = new ActivitySourceType();
        activitySourceTypeBR.setChannel(UI.SC.getChannel());
        activitySourceTypeBR.setCode(SC);
        activitySourceTypeBR.setLocationCode("ATLCP");

        EnrollmentRequestType enrollmentRequestTypeBR = new EnrollmentRequestType();
        enrollmentRequestTypeBR.setMembership(membershipInfoTypeBR);
        enrollmentRequestTypeBR.setSource(activitySourceTypeBR);

        List<EnrollmentRequestType> enrollmentRequestList = new ArrayList<EnrollmentRequestType>();
        enrollmentRequestList.add(enrollmentRequestType);
        enrollmentRequestList.add(enrollmentRequestTypeBR);

        EnrollmentRequests enrollmentRequests = new EnrollmentRequests();
        enrollmentRequests.setEnrollmentRequest(enrollmentRequestList);

        return enrollmentRequests;
    }

    private AddProfileItemRequestType createAcceptRequest(String memberId)
    {
        AddProfileItemRequestType request = new AddProfileItemRequestType();

        ProgramMembershipKey membershipKey = new ProgramMembershipKey();
        membershipKey.setMemberId(memberId);
        membershipKey.setCode("PC");

        GuestIdentityType guestIdentityType = new GuestIdentityType();
        guestIdentityType.setProgramMembershipKey(membershipKey);

        com.ihg.atp.schema.crm.common.loyalty.loyalty.datatypes.v2.ActivitySourceType activitySourceType = new com.ihg.atp.schema.crm.common.loyalty.loyalty.datatypes.v2.ActivitySourceType();
        activitySourceType.setChannel("WEB");
        activitySourceType.setLocationCode("WEBWB");
        activitySourceType.setCode(SourceType.WEB);

        GuestConsentType guestConsentType = new GuestConsentType();
        guestConsentType.setConsentSignInd(true);
        guestConsentType.setProgramCode(Program.BR.getCode());
        guestConsentType.setConsentActivitySource(activitySourceType);

        AddProfileItemChoiceType addProfileItemChoiceType = new AddProfileItemChoiceType();
        addProfileItemChoiceType.setSecureConsent(guestConsentType);

        request.setGuestIdentity(guestIdentityType);
        request.setProfileItem(addProfileItemChoiceType);

        return request;
    }
}
