package com.ihg.crm.automation.selenium.tests.servicecenter.offer;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasEmptyText;
import static com.ihg.automation.selenium.matchers.component.GridSize.size;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static com.ihg.automation.selenium.matchers.component.IsDisplayed.displayed;
import static com.ihg.automation.selenium.servicecenter.pages.event.offer.SubOffersGridRow.SubOfferCell.STATUS;
import static org.hamcrest.core.IsNot.not;

import java.util.Map;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.offer.GuestOffer;
import com.ihg.automation.selenium.common.offer.Offer;
import com.ihg.automation.selenium.common.offer.SubOffers;
import com.ihg.automation.selenium.gwt.components.grid.GridHeader;
import com.ihg.automation.selenium.servicecenter.pages.LeftPanel;
import com.ihg.automation.selenium.servicecenter.pages.event.offer.CampaignDashboardDetails;
import com.ihg.automation.selenium.servicecenter.pages.event.offer.CampaignDashboardTab;
import com.ihg.automation.selenium.servicecenter.pages.event.offer.CampaignDetailsContainer;
import com.ihg.automation.selenium.servicecenter.pages.event.offer.CampaignSummaryContainer;
import com.ihg.automation.selenium.servicecenter.pages.event.offer.OfferEventPage;
import com.ihg.automation.selenium.servicecenter.pages.event.offer.OfferPopUp;
import com.ihg.automation.selenium.servicecenter.pages.event.offer.QualifyingStayDetails;
import com.ihg.automation.selenium.servicecenter.pages.event.offer.SubOfferGridRowContainer;
import com.ihg.automation.selenium.servicecenter.pages.event.offer.SubOffersGrid;
import com.ihg.automation.selenium.servicecenter.pages.event.offer.SubOffersGridRow;
import com.ihg.crm.automation.selenium.tests.servicecenter.LoginLogout;

public class ExternalCampaignTest extends LoginLogout
{
    private static final String OFFER_CODE_WITH_EXTERNAL_CAMPAIGN = "8710487";
    private static final String OFFER_CODE_WITHOUT_EXTERNAL_CAMPAIGN = "8050504";
    private OfferEventPage offerEventPage = new OfferEventPage();
    private CampaignDashboardTab tab;
    private CampaignDashboardDetails details;
    public static final Offer CAMPAIGN_OFFER = Offer.builder("GMB0315", "Accelerate 2015").startDate("01Sep15")
            .endDate("31Dec15").build();
    private SubOffers completedSubOffer;
    private SubOffers activeSubOffer;
    private SubOffers subOfferWithoutStays;
    private SubOffersGrid grid;
    private GuestOffer guestOffer = new GuestOffer(CAMPAIGN_OFFER);
    private static final String SUB_OFFER_DETAILS = "SELECT so.* FROM OFFER.SUB_OFFER_GST_PARTIC sog"//
            + " JOIN OFFER.EXTL_CAMPAIGN_GST_PARTIC cg ON sog.EXTL_CAMPAIGN_GST_PARTIC_KEY=cg.EXTL_CAMPAIGN_GST_PARTIC_KEY"//
            + " JOIN OFFER.SUB_OFFER so ON sog.SUB_OFFER_KEY = so.SUB_OFFER_KEY"//
            + " JOIN DGST.MBRSHP m ON m.DGST_MSTR_KEY = cg.DGST_MSTR_KEY"//
            + " WHERE m.MBRSHP_ID = '%s' AND SUB_OFFER_CD = '%s'";

    @BeforeClass
    public void beforeClass()
    {
        completedSubOffer = getSubOffer("BRXX");
        completedSubOffer.setStatus("Completed");
        completedSubOffer.setEvents("4 Stay");
        completedSubOffer.setGoal("4 Stay");
        completedSubOffer.setReward("2,000 Points");
        completedSubOffer.setAwardPosted("10Jul15");

        activeSubOffer = getSubOffer("HIXX");
        activeSubOffer.setStatus("Active");
        activeSubOffer.setEvents("0 Stay");
        activeSubOffer.setGoal("2 Stay");
        activeSubOffer.setReward("4,800 Points");

        subOfferWithoutStays = getSubOffer("KKXX");
        subOfferWithoutStays.setStatus("Active");
        subOfferWithoutStays.setEvents("0 Finish");
        subOfferWithoutStays.setGoal("3 Finish");
        subOfferWithoutStays.setReward("32,800 Points");

        login();
        new LeftPanel().reOpenMemberProfile(memberWithExternalCampaign);
    }

    @Test
    public void verifyEnabledExternalTrackingTab()
    {
        offerEventPage.goTo();

        OfferPopUp popUp = offerEventPage.getMemberOffersGrid().getOfferPopUp(OFFER_CODE_WITHOUT_EXTERNAL_CAMPAIGN);

        tab = popUp.getCampaignDashboardTab();

        verifyThat(tab.getTab(), displayed(false));
        popUp.close();
    }

    @Test(dependsOnMethods = { "verifyEnabledExternalTrackingTab" }, alwaysRun = true)
    public void verifyExternalTrackingTab()
    {
        tab = offerEventPage.getMemberOffersGrid().getOfferPopUp(OFFER_CODE_WITH_EXTERNAL_CAMPAIGN)
                .getCampaignDashboardTab();

        verifyThat(tab, displayed(true));

        details = tab.getExternalTrackingDetails();
        CampaignDetailsContainer campaignDetails = details.getCampaignDetails();
        verifyThat(campaignDetails.getCode(), displayed(true));
        verifyThat(campaignDetails.getName(), displayed(true));
        verifyThat(campaignDetails.getRegistrationDate(), displayed(true));
        verifyThat(campaignDetails.getStartDate(), displayed(true));
        verifyThat(campaignDetails.getEndDate(), displayed(true));

        CampaignSummaryContainer campaignSummary = details.getCampaignSummary();
        verifyThat(campaignSummary.getEarned(), displayed(true));
        verifyThat(campaignSummary.getDaysLeftToEarn(), displayed(true));
        verifyThat(campaignSummary.getOffersCompleted(), displayed(true));

        SubOffersGrid grid = tab.getMemberOffersGrid();
        verifyThat(grid, displayed(true));

        grid.verifyHeader();
    }

    @Test(dependsOnMethods = { "verifyExternalTrackingTab" }, alwaysRun = true)
    public void verifyOfferDetails()
    {
        details = tab.getExternalTrackingDetails();
        details.getCampaignDetails().verify(guestOffer);

        verifyThat(details.getCampaignSummary().getEarned(), hasText("12,400 of 50,000 points"));
        verifyThat(details.getCampaignSummary().getOffersCompleted(), hasText("3 of 5"));
        verifyThat(details.getCampaignSummary().getDaysLeftToEarn(), not(hasEmptyText()));
    }

    @Test(dependsOnMethods = { "verifyOfferDetails" }, enabled = false)
    public void sortSubOffersByStatus()
    {
        grid = tab.getMemberOffersGrid();
        verifyThat(grid, size(5));

        GridHeader<SubOffersGridRow.SubOfferCell> header = grid.getHeader();
        header.getCell(STATUS).clickAndWait();

        int ACTIVE_SUBOFFERS_AMOUNT = 2;
        int COMPLETED_SUBOFFERS_AMOUNT = 3;

        for (int index = 1; index <= ACTIVE_SUBOFFERS_AMOUNT; index++)
        {
            verifyThat(grid.getRow(index).getCell(SubOffersGridRow.SubOfferCell.STATUS), hasText("Active"));
        }

        for (int index = ACTIVE_SUBOFFERS_AMOUNT + 1; index <= COMPLETED_SUBOFFERS_AMOUNT + ACTIVE_SUBOFFERS_AMOUNT; index++)
        {
            verifyThat(grid.getRow(index).getCell(SubOffersGridRow.SubOfferCell.STATUS), hasText("Completed"));
        }

        header.getCell(STATUS).clickAndWait();

        for (int index = 1; index <= COMPLETED_SUBOFFERS_AMOUNT; index++)
        {
            verifyThat(grid.getRow(index).getCell(SubOffersGridRow.SubOfferCell.STATUS), hasText("Completed"));
        }

        for (int index = COMPLETED_SUBOFFERS_AMOUNT + 1; index <= COMPLETED_SUBOFFERS_AMOUNT + ACTIVE_SUBOFFERS_AMOUNT; index++)
        {
            verifyThat(grid.getRow(index).getCell(SubOffersGridRow.SubOfferCell.STATUS), hasText("Active"));
        }
    }

    @Test(dependsOnMethods = { "verifyOfferDetails" }, alwaysRun = true)
    public void verifyCompleteSubOfferGridDetails()
    {
        grid = tab.getMemberOffersGrid();

        SubOffersGridRow completedSubOfferRow = grid.getRowByOfferCode(completedSubOffer.getCode());

        completedSubOfferRow.verify(completedSubOffer);

        SubOfferGridRowContainer completedSubOfferGridRowContainer = completedSubOfferRow
                .expand(SubOfferGridRowContainer.class);
        completedSubOfferGridRowContainer.getSubOfferDetails().verify(completedSubOffer);

        QualifyingStayDetails completedSubOfferQualifyingStayDetails = completedSubOfferGridRowContainer
                .getQualifyingStayDetails();

        completedSubOfferQualifyingStayDetails.getStayDetailsItem(1).verify("FRAAO", "12May15", "16May15");
        completedSubOfferQualifyingStayDetails.getStayDetailsItem(2).verify("MIAAW", "04May15", "07May15");
        completedSubOfferQualifyingStayDetails.getStayDetailsItem(3).verify("ATLCP", "05May15", "06May15");
    }

    @Test(dependsOnMethods = { "verifyCompleteSubOfferGridDetails" }, alwaysRun = true)
    public void verifyActiveSubOfferGridDetails()
    {
        SubOffersGridRow activeSubOfferRow = grid.getRowByOfferCode(activeSubOffer.getCode());

        activeSubOfferRow.verify(activeSubOffer);

        SubOfferGridRowContainer activeSubOfferGridRowContainer = activeSubOfferRow
                .expand(SubOfferGridRowContainer.class);
        activeSubOfferGridRowContainer.getSubOfferDetails().verify(activeSubOffer);
    }

    @Test(dependsOnMethods = { "verifyActiveSubOfferGridDetails" }, alwaysRun = true)
    public void verifyGridDetailsForSubOfferWithoutStays()
    {
        SubOffersGridRow activeSubOfferRow = grid.getRowByOfferCode(subOfferWithoutStays.getCode());

        activeSubOfferRow.verify(subOfferWithoutStays);

        SubOfferGridRowContainer activeSubOfferGridRowContainer = activeSubOfferRow
                .expand(SubOfferGridRowContainer.class);
        activeSubOfferGridRowContainer.getSubOfferDetails().verify(subOfferWithoutStays);

        verifyThat(activeSubOfferGridRowContainer.getQualifyingStayDetails(), displayed(false));
    }

    private SubOffers getSubOffer(String subOfferCode)
    {
        SubOffers subOffer = new SubOffers();
        Map<String, Object> map = jdbcTemplate
                .queryForMap(String.format(SUB_OFFER_DETAILS, memberWithExternalCampaign, subOfferCode));

        subOffer.setCode(subOfferCode);
        subOffer.setName(map.get("SUB_OFFER_NM").toString());
        subOffer.setDescription(map.get("SUB_OFFER_DESC").toString());

        return subOffer;
    }
}
