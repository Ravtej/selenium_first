package com.ihg.crm.automation.selenium.tests.servicecenter.guest;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static com.ihg.automation.selenium.matchers.component.ModeMatcher.inView;
import static com.ihg.automation.selenium.servicecenter.pages.Messages.getSimpleSuccess;
import static org.hamcrest.core.Is.is;

import org.joda.time.LocalDate;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.components.Country;
import com.ihg.automation.selenium.common.member.GuestAddress;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.types.Gender;
import com.ihg.automation.selenium.common.types.address.AddressType;
import com.ihg.automation.selenium.common.types.address.Region;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.gwt.components.MessageBox;
import com.ihg.automation.selenium.gwt.components.MessageBoxType;
import com.ihg.automation.selenium.gwt.components.Mode;
import com.ihg.automation.selenium.matchers.text.EntityMatcherFactory;
import com.ihg.automation.selenium.servicecenter.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.servicecenter.pages.personal.AddressContactList;
import com.ihg.automation.selenium.servicecenter.pages.personal.CustomerInfoFields;
import com.ihg.automation.selenium.servicecenter.pages.personal.PersonalInfoPage;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.RewardClubPage;
import com.ihg.crm.automation.selenium.tests.servicecenter.LoginLogout;

public class AddSupplementaryAddressTest extends LoginLogout
{
    private EnrollmentPage enrollmentPage = new EnrollmentPage();
    private Member member = new Member();
    private PersonalInfoPage personalInfoPage = new PersonalInfoPage();
    private GuestAddress newAddress = new GuestAddress();
    private GuestAddress address;
    private static final String SUCCESS_SAVED_MESSAGE = "Personal info has been successfully saved";

    @BeforeClass
    public void before()
    {
        address = MemberPopulateHelper.getUnitedStatesAddress();

        newAddress.setCountryCode(Country.US);
        newAddress.setType(AddressType.BUSINESS);
        newAddress.setLine1("1 MADISON AVE");
        newAddress.setLocality1("NEW YORK");
        newAddress.setRegion1(Region.NY);
        newAddress.setPostalCode("10010-3603");
        newAddress.setPreferred(false);

        member.addProgram(Program.RC);
        member.setPersonalInfo(MemberPopulateHelper.getSimplePersonalInfo());
        member.addAddress(address);

        login();

        enrollmentPage.enroll(member);
    }

    @Test
    public void verifyAddingNewAddress()
    {
        addAddress(newAddress);
        verifyAddress();
    }

    @Test(dependsOnMethods = { "verifyAddingNewAddress" }, alwaysRun = true)
    public void verifyAddressWithChangingGender()
    {
        personalInfoPage = new PersonalInfoPage();

        CustomerInfoFields customerInfo = personalInfoPage.getCustomerInfo();
        customerInfo.clickEdit();
        customerInfo.getGender().select(Gender.FEMALE.getValue());
        customerInfo.clickSave();

        verifyThat(new MessageBox(MessageBoxType.SUCCESS), hasText(SUCCESS_SAVED_MESSAGE));

        verifyThat(customerInfo.getGender(), inView(hasText(EntityMatcherFactory.isValue(Gender.FEMALE))));

        goAwayAndComeBackToPersonalInfoPage();
        verifyAddress();

        customerInfo.clickEdit();
        customerInfo.getGender().select("");
        customerInfo.clickSave();

        verifyThat(new MessageBox(MessageBoxType.SUCCESS), hasText(SUCCESS_SAVED_MESSAGE));

        goAwayAndComeBackToPersonalInfoPage();
        verifyAddress();
    }

    @Test(dependsOnMethods = { "verifyAddressWithChangingGender" }, alwaysRun = true)
    public void verifyAddressWithChangingBirthDay()
    {
        personalInfoPage = new PersonalInfoPage();
        personalInfoPage.goTo();

        CustomerInfoFields customerInfo = personalInfoPage.getCustomerInfo();
        customerInfo.clickEdit();
        customerInfo.getBirthDate().populate(new LocalDate());
        customerInfo.clickSave();

        verifyThat(new MessageBox(MessageBoxType.SUCCESS), hasText(SUCCESS_SAVED_MESSAGE));

        goAwayAndComeBackToPersonalInfoPage();
        verifyAddress();

        customerInfo.clickEdit();

        customerInfo.getBirthDate().getMonth().select("");
        customerInfo.clickSave();

        verifyThat(new MessageBox(MessageBoxType.SUCCESS), hasText(SUCCESS_SAVED_MESSAGE));

        goAwayAndComeBackToPersonalInfoPage();
        verifyAddress();

    }

    @Test(dependsOnMethods = { "verifyAddressWithChangingGender" }, alwaysRun = true)
    public void verifyAddressWithChangingName()
    {
        personalInfoPage = new PersonalInfoPage();
        personalInfoPage.goTo();

        CustomerInfoFields customerInfo = personalInfoPage.getCustomerInfo();
        customerInfo.clickEdit();

        customerInfo.getName().getMiddleName().type("NewMiddleName");
        customerInfo.clickSave();

        verifyThat(new MessageBox(MessageBoxType.SUCCESS), hasText(SUCCESS_SAVED_MESSAGE));

        goAwayAndComeBackToPersonalInfoPage();
        verifyAddress();

        customerInfo.clickEdit();

        customerInfo.getName().getMiddleName().clear();
        customerInfo.clickSave();

        verifyThat(new MessageBox(MessageBoxType.SUCCESS), hasText(SUCCESS_SAVED_MESSAGE));

        goAwayAndComeBackToPersonalInfoPage();
        verifyAddress();

    }

    private void addAddress(GuestAddress newAddress)
    {
        personalInfoPage = new PersonalInfoPage();
        personalInfoPage.goTo();

        AddressContactList addressContactList = personalInfoPage.getAddressList();

        addressContactList.add(newAddress);

        verifyThat(new MessageBox(MessageBoxType.SUCCESS), hasText(getSimpleSuccess()));
    }

    private void verifyAddress()
    {
        personalInfoPage = new PersonalInfoPage();

        AddressContactList addressContactList = personalInfoPage.getAddressList();
        verifyThat(addressContactList, addressContactList.getContactsCount(), is(2),
                "Two address on personal info page");
        addressContactList.getExpandedContact(1).verify(address, Mode.VIEW);
        addressContactList.getExpandedContact(2).verify(newAddress, Mode.VIEW);
    }

    private void goAwayAndComeBackToPersonalInfoPage()
    {
        personalInfoPage = new PersonalInfoPage();
        RewardClubPage rewardClubPage = new RewardClubPage();
        rewardClubPage.goTo();
        verifyThat(personalInfoPage, displayed(false));

        personalInfoPage.goTo();
    }
}
