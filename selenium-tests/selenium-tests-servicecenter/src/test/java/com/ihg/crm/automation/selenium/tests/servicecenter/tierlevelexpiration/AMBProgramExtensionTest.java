package com.ihg.crm.automation.selenium.tests.servicecenter.tierlevelexpiration;

import static com.ihg.automation.common.DateUtils.now;
import static com.ihg.automation.selenium.assertions.Assume.assumeThat;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.member.MemberJdbcUtils.UPDATE_EXP;
import static com.ihg.automation.selenium.common.types.ExtendReason.COMMUNICATION_ERROR;
import static com.ihg.automation.selenium.common.types.program.Program.AMB;
import static com.ihg.automation.selenium.common.types.program.ProgramStatus.OPEN;
import static com.ihg.automation.selenium.common.types.program.RewardClubLevel.SPRE;
import static com.ihg.automation.selenium.common.types.program.RewardClubLevelReason.BASE;
import static com.ihg.automation.selenium.common.types.program.TierLevelExpiration.EXPIRE_CURRENT_YEAR;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static com.ihg.automation.selenium.servicecenter.pages.programs.ProgramSummary.SHIFT_FOR_CURRENT_YEAR;
import static com.ihg.automation.selenium.servicecenter.pages.programs.ProgramSummary.SHIFT_FOR_NEXT_YEAR;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNot.not;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.common.DateUtils;
import com.ihg.automation.selenium.common.member.AmbassadorOfferCode;
import com.ihg.automation.selenium.common.member.AmbassadorUtils;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberJdbcUtils;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.servicecenter.pages.LeftPanel;
import com.ihg.automation.selenium.servicecenter.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.servicecenter.pages.programs.RenewExtendProgramSummary;
import com.ihg.automation.selenium.servicecenter.pages.programs.amb.AmbassadorPage;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.RewardClubPage;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.RewardClubSummary;
import com.ihg.crm.automation.selenium.tests.servicecenter.LoginLogout;

public class AMBProgramExtensionTest extends LoginLogout
{

    private static final int MONTHS_TO_UPDATE = 2;

    @BeforeClass
    public void beforeClass()
    {
        Member member = new Member();
        member.setPersonalInfo(MemberPopulateHelper.getSimplePersonalInfo());
        member.addAddress(MemberPopulateHelper.getUnitedStatesAddress());
        member.addProgram(AMB);
        member.setAmbassadorOfferCode(AmbassadorOfferCode.CHSRO);
        member.setAmbassadorAmount(AmbassadorUtils.AMBASSADOR_COMP_PROMO_$0);

        login();
        member = new EnrollmentPage().enroll(member);

        MemberJdbcUtils.update(jdbcTemplateUpdate, UPDATE_EXP, member, AMB, MONTHS_TO_UPDATE);

        new LeftPanel().reOpenMemberProfile(member);

        AmbassadorPage ambassadorPage = new AmbassadorPage();
        ambassadorPage.goTo();
        verifyThat(ambassadorPage.getSummary().getExpirationDate(),
                hasText(DateUtils.getMonthsForward(2, DateUtils.EXPIRATION_DATE_PATTERN)));

        RewardClubPage rewardClubPage = new RewardClubPage();
        rewardClubPage.goTo();
        RewardClubSummary summary = rewardClubPage.getSummary();
        summary.setTierLevelWithExpiration(SPRE, BASE, EXPIRE_CURRENT_YEAR);
        summary.verify(OPEN, SPRE, SHIFT_FOR_CURRENT_YEAR);
    }

    @Test
    public void extendInCurrentYearTierLevel()
    {
        assumeThat(now().getMonthOfYear(), not(is(12)),
                "After program extending the expiration date should be in current year");

        extendProgramAndVerifyTierLevel(1, SHIFT_FOR_CURRENT_YEAR);
    }

    @Test(dependsOnMethods = { "extendInCurrentYearTierLevel" }, alwaysRun = true)
    public void extendInFollowingYearTierLevel()
    {
        assumeThat(now().getMonthOfYear(), not(is(1)),
                "After program extending the expiration date should be in following year");

        extendProgramAndVerifyTierLevel(11, SHIFT_FOR_NEXT_YEAR);

    }

    private void extendProgramAndVerifyTierLevel(int months, int shiftYears)
    {
        AmbassadorPage ambassadorPage = new AmbassadorPage();
        ambassadorPage.goTo();
        RenewExtendProgramSummary ambSummary = ambassadorPage.getSummary();
        ambSummary.extend(months, COMMUNICATION_ERROR);

        RewardClubPage rewardClubPage = new RewardClubPage();
        rewardClubPage.goTo();
        rewardClubPage.getSummary().verify(OPEN, SPRE, shiftYears);
    }

}
