package com.ihg.crm.automation.selenium.tests.servicecenter.commpref;

import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyError;
import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyNoError;
import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifySuccess;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.communication.CommunicationPreferencesConstants.CANNOT_SAVE_PREFERENCES;
import static com.ihg.automation.selenium.common.communication.CommunicationPreferencesConstants.SUCCESS_MESSAGE_SC;
import static com.ihg.automation.selenium.matchers.component.IsDisplayed.displayed;
import static com.ihg.automation.selenium.matchers.component.IsInvalid.isHighlightedAsInvalid;
import static com.ihg.automation.selenium.servicecenter.pages.communication.DaysMenu.ANY_AUDIT;
import static com.ihg.automation.selenium.servicecenter.pages.history.AuditConstant.CommunicationPreferences.COMMUNICATION_PREFERENCES;
import static com.ihg.automation.selenium.servicecenter.pages.history.AuditConstant.CommunicationPreferences.DAYS_OF_THE_WEEK;
import static com.ihg.automation.selenium.servicecenter.pages.history.AuditConstant.CommunicationPreferences.PHONE;
import static com.ihg.automation.selenium.servicecenter.pages.history.AuditConstant.CommunicationPreferences.PHONE_FOR_CUSTOMER_SERVICE;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.member.GuestEmail;
import com.ihg.automation.selenium.common.member.GuestPhone;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.personal.PhoneContact;
import com.ihg.automation.selenium.common.personal.SmsContactList;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.gwt.components.dialog.WarningDialog;
import com.ihg.automation.selenium.gwt.components.input.Select;
import com.ihg.automation.selenium.servicecenter.pages.communication.CommunicationPreferences;
import com.ihg.automation.selenium.servicecenter.pages.communication.CommunicationPreferencesPage;
import com.ihg.automation.selenium.servicecenter.pages.communication.CustomerServicePhone;
import com.ihg.automation.selenium.servicecenter.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.servicecenter.pages.history.ProfileHistoryGridRow;
import com.ihg.automation.selenium.servicecenter.pages.history.ProfileHistoryPage;
import com.ihg.automation.selenium.servicecenter.pages.personal.PersonalInfoPage;
import com.ihg.crm.automation.selenium.tests.servicecenter.LoginLogout;

public class MarkPhoneAsInvalidTest extends LoginLogout
{
    private Member member = new Member();
    private EnrollmentPage enrollmentPage = new EnrollmentPage();
    private GuestPhone phone = new GuestPhone();
    private CustomerServicePhone customerServicePhone;
    private GuestPhone secondPhone = new GuestPhone();
    private GuestPhone smsPhone = new GuestPhone();

    @BeforeClass
    public void beforeClass()
    {
        GuestEmail guestEmail = MemberPopulateHelper.getRandomEmail();

        phone = MemberPopulateHelper.getRandomPhone();
        secondPhone = MemberPopulateHelper.getRandomPhone();
        secondPhone.setPreferred(false);
        smsPhone = MemberPopulateHelper.getRandomPhone();
        smsPhone.setValid(false);

        member.addProgram(Program.RC);
        member.setPersonalInfo(MemberPopulateHelper.getSimplePersonalInfo());
        member.addAddress(MemberPopulateHelper.getUnitedStatesAddress());
        member.addEmail(guestEmail);
        member.addPhone(phone);

        login();
        enrollmentPage.enroll(member);
    }

    @Test
    public void selectPhoneAsContact()
    {
        CommunicationPreferencesPage commPrefPage = new CommunicationPreferencesPage();
        commPrefPage.goTo();
        CommunicationPreferences commPrefs = commPrefPage.getCommunicationPreferences();
        commPrefs.clickEdit(verifyNoError());

        customerServicePhone = commPrefPage.getCommunicationPreferences().getCustomerService().getPhone();
        customerServicePhone.getPhoneCheckBox().check();

        customerServicePhone.getPhone().select(phone.getFullPhone());

        commPrefs.clickSave(verifySuccess(SUCCESS_MESSAGE_SC));
    }

    @Test(dependsOnMethods = { "selectPhoneAsContact" }, alwaysRun = true)
    public void markPhoneAsInvalid()
    {
        PersonalInfoPage personalInfoPage = new PersonalInfoPage();
        personalInfoPage.goTo();
        PhoneContact phoneContact = personalInfoPage.getPhoneList().getContact();
        phoneContact.clickEdit();
        phoneContact.getInvalid().check();
        phoneContact.clickSave();

        String INVALID_CONTACT_SUCCESS_SAVE = "Contact info has been successfully saved. "
                + "The Contact flagged as Invalid is used in Customer Service Preferences.\n"
                + "Please update Customer Service Preferences at Communication Preferences tab.";

        new WarningDialog().clickOk(verifyNoError(), verifySuccess(INVALID_CONTACT_SUCCESS_SAVE));
    }

    @Test(dependsOnMethods = { "markPhoneAsInvalid" }, alwaysRun = true)
    public void verifyCommPrefWithInvalidPhone()
    {
        CommunicationPreferencesPage commPrefPage = new CommunicationPreferencesPage();
        commPrefPage.goTo();
        CommunicationPreferences commPrefs = commPrefPage.getCommunicationPreferences();
        commPrefs.clickEdit(verifyNoError());

        verifyThat(commPrefs.getSmsMobileTextingMarketingPreferences().getInvalidSmsImage(), displayed(false));

        customerServicePhone = commPrefs.getCustomerService().getPhone();
        customerServicePhone.getPhoneCheckBox().check();

        verifyThat(customerServicePhone.getPhone(), isHighlightedAsInvalid(true));

        commPrefs.clickSave(verifyError(CANNOT_SAVE_PREFERENCES));
    }

    @Test(dependsOnMethods = { "verifyCommPrefWithInvalidPhone" }, alwaysRun = true)
    public void addSecondPhone()
    {
        PersonalInfoPage personalInfoPage = new PersonalInfoPage();
        personalInfoPage.goTo();
        personalInfoPage.getPhoneList().add(secondPhone);
    }

    @Test(dependsOnMethods = { "addSecondPhone" }, alwaysRun = true)
    public void verifyCommPref()
    {
        CommunicationPreferencesPage commPrefPage = new CommunicationPreferencesPage();
        commPrefPage.goTo();
        CommunicationPreferences commPrefs = commPrefPage.getCommunicationPreferences();
        commPrefs.clickEdit(verifyNoError());

        customerServicePhone = commPrefs.getCustomerService().getPhone();
        customerServicePhone.getPhoneCheckBox().check();

        Select phoneSelect = customerServicePhone.getPhone();
        verifyThat(phoneSelect, isHighlightedAsInvalid(true));
        phoneSelect.select(secondPhone.getFullPhone());
        verifyThat(phoneSelect, isHighlightedAsInvalid(false));

        commPrefs.clickSave(verifySuccess(SUCCESS_MESSAGE_SC));
    }

    @Test(dependsOnMethods = { "verifyCommPref" }, alwaysRun = true)
    public void addInvalidSms()
    {
        PersonalInfoPage personalInfoPage = new PersonalInfoPage();
        personalInfoPage.goTo();
        SmsContactList smsContactList = personalInfoPage.getSmsList();
        smsContactList.add(smsPhone);
    }

    @Test(dependsOnMethods = { "addInvalidSms" }, alwaysRun = true)
    public void verifyCommPrefWithInvalidSms()
    {
        CommunicationPreferencesPage commPrefPage = new CommunicationPreferencesPage();
        commPrefPage.goTo();

        CommunicationPreferences commPrefs = commPrefPage.getCommunicationPreferences();
        verifyThat(commPrefs.getSmsMobileTextingMarketingPreferences().getInvalidSmsImage(), displayed(true));

        commPrefs.clickEdit();
        verifyThat(commPrefs.getSmsMobileTextingMarketingPreferences().getInvalidSmsImage(), displayed(true));
    }
}
