package com.ihg.crm.automation.selenium.tests.servicecenter.unit;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.event.EventTransactionType.Order.ORDER_SYSTEM;
import static com.ihg.automation.selenium.common.event.EventTransactionType.TierLevel.TIER_LEVEL_PURCHASE;
import static com.ihg.automation.selenium.common.types.program.ProgramStatus.OPEN;
import static com.ihg.automation.selenium.common.types.program.RewardClubLevel.GOLD;
import static com.ihg.automation.selenium.common.types.program.RewardClubLevelReason.BOUGHT_ELITE_STATUS;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.isDisplayedWithWait;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static org.hamcrest.core.StringEndsWith.endsWith;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.common.DateUtils;
import com.ihg.automation.selenium.common.ProgramInfoPanel;
import com.ihg.automation.selenium.common.catalog.CatalogItem;
import com.ihg.automation.selenium.common.earning.EventEarningDetailsTab;
import com.ihg.automation.selenium.common.event.AllEventsRow;
import com.ihg.automation.selenium.common.event.AllEventsRowConverter;
import com.ihg.automation.selenium.common.event.EarningEvent;
import com.ihg.automation.selenium.common.event.EarningEventFactory;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.order.Order;
import com.ihg.automation.selenium.common.order.OrderFactory;
import com.ihg.automation.selenium.common.pages.PaymentMethod;
import com.ihg.automation.selenium.common.payment.Payment;
import com.ihg.automation.selenium.common.payment.PaymentDetailsPopUp;
import com.ihg.automation.selenium.common.payment.PaymentFactory;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.servicecenter.pages.annual.AnnualActivityPage;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsGridRow;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsPage;
import com.ihg.automation.selenium.servicecenter.pages.event.level.TierLevelDetailsPopUp;
import com.ihg.automation.selenium.servicecenter.pages.event.level.TierLevelDetailsTab;
import com.ihg.automation.selenium.servicecenter.pages.event.level.TierLevelGridRowContainer;
import com.ihg.automation.selenium.servicecenter.pages.event.order.OrderDetailsPopUp;
import com.ihg.automation.selenium.servicecenter.pages.event.order.OrderSystemDetailsGridRowContainer;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.RewardClubPage;
import com.ihg.automation.selenium.servicecenter.pages.unit.PurchaseTierLevelPopUp;

public class PurchaseGoldTierLevelTest extends UnitsCommon
{
    private Member member;
    private AllEventsPage allEventsPage = new AllEventsPage();
    private TierLevelGridRowContainer tierLevelRowContainer;
    private OrderSystemDetailsGridRowContainer orderSystGridRowContainer;
    private TierLevelDetailsPopUp tierLevelPopUp = new TierLevelDetailsPopUp();
    private OrderDetailsPopUp orderSysDetailsPopUp = new OrderDetailsPopUp();

    private static final Program RC_PROGRAM = Program.RC;
    private static final String RC_GOLD_DETAIL = "RC - GOLD";
    private static final Payment PAYMENT = PaymentFactory.getComplimentaryPayment("50.00");

    private static final CatalogItem upgradeAward = CatalogItem.builder("F0010", "ELITE UPGRADE CERTIFICATE KIT")
            .build();

    private AllEventsRow tierLevelPurchRow = new AllEventsRow(DateUtils.getFormattedDate(), TIER_LEVEL_PURCHASE,
            RC_GOLD_DETAIL);
    private EarningEvent baseAwardsEvent = EarningEventFactory.getAwardEvent(upgradeAward);

    @BeforeClass
    public void beforeClass()
    {
        login();
        member = new AnnualActivityPage().captureCounters(enrollMember(), jdbcTemplate);
    }

    @Test
    public void purchaseGoldTierLevel()
    {
        PurchaseTierLevelPopUp purchaseTierLevelPopUp = new PurchaseTierLevelPopUp();
        purchaseTierLevelPopUp.goTo();
        purchaseTierLevelPopUp.setLevel(RC_PROGRAM, GOLD.getValue());

        PaymentDetailsPopUp paymentPopUp = new PaymentDetailsPopUp();
        verifyThat(paymentPopUp, isDisplayedWithWait());
        paymentPopUp.selectPaymentType(PaymentMethod.COMPLIMENTARY);
        paymentPopUp.clickSubmitAndVerifySuccessMsg();
    }

    @Test(dependsOnMethods = { "purchaseGoldTierLevel" })
    public void verifyProgramInformationPanelLevel()
    {
        verifyThat(new ProgramInfoPanel().getPrograms().getRewardClubProgram().getLevelCode(),
                hasText(GOLD.getValue()));
    }

    @Test(dependsOnMethods = { "verifyProgramInformationPanelLevel" }, alwaysRun = true)
    public void verifyRewardsPage()
    {
        RewardClubPage rewardClubPage = new RewardClubPage();
        rewardClubPage.goTo();
        rewardClubPage.getSummary().verify(OPEN, GOLD, BOUGHT_ELITE_STATUS);

        rewardClubPage.getProgramOverview().expand();
        verifyThat(rewardClubPage.getProgramOverview(), hasText(endsWith("Gold level")));
    }

    @Test(dependsOnMethods = { "verifyRewardsPage" }, alwaysRun = true)
    public void verifyActivityCounters()
    {
        new AnnualActivityPage().verifyCounters(member);
    }

    @Test(dependsOnMethods = { "verifyActivityCounters" }, alwaysRun = true)
    public void verifyOrderSystemEvent()
    {
        Order order = OrderFactory.getSystemOrder(upgradeAward, member, helper);
        AllEventsRow tierLevelOrderSystemEvent = AllEventsRowConverter.convert(order);

        allEventsPage.goTo();

        AllEventsGridRow row = allEventsPage.getGrid().getRow(tierLevelOrderSystemEvent.getTransType());
        row.verify(tierLevelOrderSystemEvent);

        orderSystGridRowContainer = row.expand(OrderSystemDetailsGridRowContainer.class);
        orderSystGridRowContainer.verify(order);
    }

    @Test(dependsOnMethods = { "verifyOrderSystemEvent" }, alwaysRun = true)
    public void openOrderSystemDetailsPopUp()
    {
        orderSystGridRowContainer.clickDetails();
        verifyThat(orderSysDetailsPopUp, isDisplayedWithWait());
    }

    @Test(dependsOnMethods = { "openOrderSystemDetailsPopUp" })
    public void verifyOrderSystemEarningDetails()
    {
        EventEarningDetailsTab tab = orderSysDetailsPopUp.getEarningDetailsTab();
        tab.goTo();
        tab.verifyBasicDetailsNoPointsAndMiles();
        tab.getGrid().verify(baseAwardsEvent);
    }

    @Test(dependsOnMethods = { "openOrderSystemDetailsPopUp" })
    public void verifyOrderSystemBillingDetails()
    {
        orderSysDetailsPopUp.getBillingDetailsTab().verifyBaseAwardUSDBilling(ORDER_SYSTEM, "50", "CPMGU");
    }

    @Test(dependsOnMethods = { "openOrderSystemDetailsPopUp", "verifyOrderSystemBillingDetails",
            "verifyOrderSystemEarningDetails" }, alwaysRun = true)
    public void closeOrderSystemDetails()
    {
        orderSysDetailsPopUp.close();
    }

    @Test(dependsOnMethods = { "closeOrderSystemDetails" })
    public void verifyTierLevelPurchaseEvent()
    {
        allEventsPage.goTo();

        AllEventsGridRow row = allEventsPage.getGrid().getRow(tierLevelPurchRow.getTransType());
        row.verify(tierLevelPurchRow);

        tierLevelRowContainer = row.expand(TierLevelGridRowContainer.class);
        tierLevelRowContainer.getTierLevelDetails().verify(upgradeAward, PAYMENT, helper);
    }

    @Test(dependsOnMethods = { "verifyTierLevelPurchaseEvent" })
    public void openTierLevelPurchaseDetailsPopUp()
    {
        tierLevelRowContainer.clickDetails();
        verifyThat(tierLevelPopUp, isDisplayedWithWait());
    }

    @Test(dependsOnMethods = { "openTierLevelPurchaseDetailsPopUp" })
    public void verifyTierLevelDetailsPopUpTab()
    {
        TierLevelDetailsTab tab = tierLevelPopUp.getTierLevelDetailsTab();
        tab.goTo();
        tab.getDetails().verify(upgradeAward, PAYMENT, helper);
    }

    @Test(dependsOnMethods = { "openTierLevelPurchaseDetailsPopUp" })
    public void verifyTierLevelEarningDetailsTab()
    {
        EventEarningDetailsTab tab = tierLevelPopUp.getEarningDetailsTab();
        tab.goTo();
        tab.verifyBasicDetailsNoPointsAndMiles();
        tab.getGrid().verifyRowsFoundByType(EarningEventFactory.getBaseUnitsEvent("0","0"), baseAwardsEvent);
    }

    @Test(dependsOnMethods = { "openTierLevelPurchaseDetailsPopUp" })
    public void verifyTierLevelBillingDetailsTab()
    {
        tierLevelPopUp.getBillingDetailsTab().verifyBaseAwardUSDBilling(TIER_LEVEL_PURCHASE, "50", "CPMGU");
    }

}
