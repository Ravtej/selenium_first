package com.ihg.crm.automation.selenium.tests.servicecenter.preferences;

import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyNoError;
import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifySuccess;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.preferences.RatePreferencesType;
import com.ihg.automation.selenium.common.preferences.TravelProfileType;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.gwt.components.DualList;
import com.ihg.automation.selenium.servicecenter.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.servicecenter.pages.preferences.AddTravelProfilePopUp;
import com.ihg.automation.selenium.servicecenter.pages.preferences.EditTravelProfilePopUp;
import com.ihg.automation.selenium.servicecenter.pages.preferences.StayPreferencesPage;
import com.ihg.automation.selenium.servicecenter.pages.preferences.TravelProfileGridRow;
import com.ihg.automation.selenium.servicecenter.pages.preferences.TravelProfilePopUpBase;
import com.ihg.crm.automation.selenium.tests.servicecenter.LoginLogout;

public class RatePreferencesTest extends LoginLogout
{
    private StayPreferencesPage stayPreferencesPage = new StayPreferencesPage();
    private TravelProfilePopUpBase addTravelProfilePopUp = new AddTravelProfilePopUp();
    private TravelProfilePopUpBase editTravelProfilePopUp = new EditTravelProfilePopUp();
    private TravelProfileGridRow row;

    private static final String DEFAULT_TP = "Leisure";
    private static final String SUPP_TP = "Test";

    @BeforeClass
    public void beforeClass()
    {
        Member member = new Member();
        member.addProgram(Program.RC);
        member.addAddress(MemberPopulateHelper.getUnitedStatesAddress());
        member.setPersonalInfo(MemberPopulateHelper.getSimplePersonalInfo());

        login();
        new EnrollmentPage().enroll(member);
    }

    @Test
    public void addRatePreferencesOnCreate()
    {
        stayPreferencesPage.goTo();
        stayPreferencesPage.getAddTravelProfile().clickAndWait();
        addTravelProfilePopUp.getTravelProfileName().type(SUPP_TP);
        addTravelProfilePopUp.getType().select(TravelProfileType.LEISURE.getCodeWithValue());

        DualList ratePreferences = addTravelProfilePopUp.getRatePreferences();
        ratePreferences.select(RatePreferencesType.AAA.getName());
        ratePreferences.select(RatePreferencesType.ENTERTAINMENT_CARD.getName());
        ratePreferences.select(RatePreferencesType.GOVERNMENT_CANADA.getName());
        addTravelProfilePopUp.clickSave(verifyNoError(), verifySuccess(TravelProfilePopUpBase.SUCCESS_MESSAGE_CREATE));
    }

    @Test(dependsOnMethods = { "addRatePreferencesOnCreate" }, alwaysRun = true)
    public void addRatePreferencesOnUpdate()
    {
        row = stayPreferencesPage.getTravelProfilesGrid().getRowByName(DEFAULT_TP);
        editTravelProfilePopUp = row.openTravelProfile();

        DualList ratePreferences = editTravelProfilePopUp.getRatePreferences();
        ratePreferences.select(RatePreferencesType.AAA.getName());
        ratePreferences.select(RatePreferencesType.STATE_GOVERNMENT.getName());
        editTravelProfilePopUp.clickSave(verifyNoError(), verifySuccess(TravelProfilePopUpBase.SUCCESS_MESSAGE_UPDATE));
    }

    @Test(dependsOnMethods = { "addRatePreferencesOnUpdate" }, alwaysRun = true)
    public void updateRatePreferences()
    {
        row = stayPreferencesPage.getTravelProfilesGrid().getRowByName(DEFAULT_TP);
        editTravelProfilePopUp = row.openTravelProfile();

        DualList ratePreferences = editTravelProfilePopUp.getRatePreferences();
        ratePreferences.remove(RatePreferencesType.AAA.getName());
        editTravelProfilePopUp.clickSave(verifyNoError(), verifySuccess(TravelProfilePopUpBase.SUCCESS_MESSAGE_UPDATE));
    }

    @Test(dependsOnMethods = { "addRatePreferencesOnUpdate" }, alwaysRun = true)
    public void deleteRatePreferences()
    {
        row = stayPreferencesPage.getTravelProfilesGrid().getRowByName(SUPP_TP);
        editTravelProfilePopUp = row.openTravelProfile();

        DualList ratePreferences = editTravelProfilePopUp.getRatePreferences();
        ratePreferences.removeAll();
        editTravelProfilePopUp.clickSave(verifyNoError(), verifySuccess(TravelProfilePopUpBase.SUCCESS_MESSAGE_UPDATE));
    }
}
