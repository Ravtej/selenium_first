package com.ihg.crm.automation.selenium.tests.servicecenter.business;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.Constant.NOT_AVAILABLE;
import static com.ihg.automation.selenium.common.business.BusinessEventType.BUSINESS_EVENT_TYPE_LIST;
import static com.ihg.automation.selenium.common.business.BusinessStatus.BUSINESS_STATUS_LIST;
import static com.ihg.automation.selenium.matchers.component.HasSelectItems.hasSelectItems;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static com.ihg.automation.selenium.matchers.component.IsDisplayed.displayed;
import static com.ihg.automation.selenium.servicecenter.pages.event.business.BusinessEventsGridRow.BusinessEventsCell.ALLIANCE_UNITS;
import static com.ihg.automation.selenium.servicecenter.pages.event.business.BusinessEventsGridRow.BusinessEventsCell.END_DATE;
import static com.ihg.automation.selenium.servicecenter.pages.event.business.BusinessEventsGridRow.BusinessEventsCell.EVENT_ID;
import static com.ihg.automation.selenium.servicecenter.pages.event.business.BusinessEventsGridRow.BusinessEventsCell.HOTEL;
import static com.ihg.automation.selenium.servicecenter.pages.event.business.BusinessEventsGridRow.BusinessEventsCell.IHG_UNITS;
import static com.ihg.automation.selenium.servicecenter.pages.event.business.BusinessEventsGridRow.BusinessEventsCell.START_DATE;
import static com.ihg.automation.selenium.servicecenter.pages.event.business.BusinessEventsGridRow.BusinessEventsCell.STATUS;
import static com.ihg.automation.selenium.servicecenter.pages.event.business.BusinessEventsGridRow.BusinessEventsCell.TRANS_DATE;
import static com.ihg.automation.selenium.servicecenter.pages.event.business.BusinessEventsGridRow.BusinessEventsCell.TRANS_TYPE;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.business.AllPastStaysGrid;
import com.ihg.automation.selenium.common.business.BusinessEvent;
import com.ihg.automation.selenium.common.business.BusinessRewardsEventDetailsPopUp;
import com.ihg.automation.selenium.common.business.DiscretionaryPointsFields;
import com.ihg.automation.selenium.common.business.EventInformationFields;
import com.ihg.automation.selenium.common.business.EventStaysGrid;
import com.ihg.automation.selenium.common.business.GuestRoomsFields;
import com.ihg.automation.selenium.common.business.MeetingFields;
import com.ihg.automation.selenium.common.business.MeetingRevenueDetails;
import com.ihg.automation.selenium.common.business.OffersFieldsEdit;
import com.ihg.automation.selenium.common.business.RoomsTab;
import com.ihg.automation.selenium.common.business.StaySearch;
import com.ihg.automation.selenium.common.business.TotalEventAwardFields;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.types.Currency;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.gwt.components.grid.GridHeader;
import com.ihg.automation.selenium.servicecenter.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.servicecenter.pages.event.business.BusinessEventsGridRow;
import com.ihg.automation.selenium.servicecenter.pages.event.business.BusinessEventsPage;
import com.ihg.automation.selenium.servicecenter.pages.event.business.BusinessSearch;
import com.ihg.crm.automation.selenium.tests.servicecenter.LoginLogout;

public class VerificationBusinessTabTest extends LoginLogout
{
    private Member member = new Member();
    private BusinessEventsPage businessEventsPage;
    private BusinessRewardsEventDetailsPopUp popUp;
    private EventInformationFields eventInformationFields;

    @BeforeClass
    public void beforeClass()
    {
        member.addProgram(Program.BR);
        member.addAddress(MemberPopulateHelper.getUnitedStatesAddress());
        member.setPersonalInfo(MemberPopulateHelper.getSimplePersonalInfo());

        login();
        new EnrollmentPage().enroll(member);
    }

    @Test
    public void verifyEventsSearchSection()
    {
        businessEventsPage = new BusinessEventsPage();
        businessEventsPage.goTo();

        BusinessSearch businessSearch = businessEventsPage.getSearchFields();
        verifyThat(businessSearch.getHotel(), displayed(true));
        verifyThat(businessSearch.getStatus(), displayed(true));
        verifyThat(businessSearch.getEventID(), displayed(true));
        verifyThat(businessSearch.getEventActiveDate().getDate(), displayed(true));
        verifyThat(businessSearch.getStatus(), hasSelectItems(BUSINESS_STATUS_LIST));

        BusinessEventsPage.BusinessEventsButtonBar buttons = businessEventsPage.getButtonBar();
        verifyThat(buttons.getCreateEvent(), displayed(true));
        verifyThat(buttons.getClearCriteria(), displayed(true));
        verifyThat(buttons.getSearch(), displayed(true));

        GridHeader<BusinessEventsGridRow.BusinessEventsCell> header = businessEventsPage.getGrid().getHeader();
        verifyThat(header.getCell(TRANS_DATE), hasText(TRANS_DATE.getValue()));
        verifyThat(header.getCell(TRANS_TYPE), hasText(TRANS_TYPE.getValue()));
        verifyThat(header.getCell(EVENT_ID), hasText(EVENT_ID.getValue()));
        verifyThat(header.getCell(HOTEL), hasText(HOTEL.getValue()));
        verifyThat(header.getCell(START_DATE), hasText(START_DATE.getValue()));
        verifyThat(header.getCell(END_DATE), hasText(END_DATE.getValue()));
        verifyThat(header.getCell(STATUS), hasText(STATUS.getValue()));
        verifyThat(header.getCell(IHG_UNITS), hasText(IHG_UNITS.getValue()));
        verifyThat(header.getCell(ALLIANCE_UNITS), hasText(ALLIANCE_UNITS.getValue()));
    }

    @Test(dependsOnMethods = { "verifyEventsSearchSection" }, alwaysRun = true)
    public void verifyEventInformationSectionOnCreateEvent()
    {
        businessEventsPage = new BusinessEventsPage();
        popUp = businessEventsPage.createEvent();

        eventInformationFields = popUp.getEventSummaryTab().getEventInformation();

        verifyThat(eventInformationFields.getHotel(), displayed(true));
        verifyThat(eventInformationFields.getHotelCurrency(), displayed(true));
        verifyThat(eventInformationFields.getEventContractDate(), displayed(true));
        verifyThat(eventInformationFields.getEventName(), displayed(true));
        verifyThat(eventInformationFields.getHotelContactEmail(), displayed(true));
        verifyThat(eventInformationFields.getHotelContactName(), displayed(true));
        verifyThat(eventInformationFields.getHotelContactPhone(), displayed(true));
        verifyThat(eventInformationFields.getEventType(), hasSelectItems(BUSINESS_EVENT_TYPE_LIST));
    }

    @Test(dependsOnMethods = { "verifyEventInformationSectionOnCreateEvent" }, priority = 0, alwaysRun = true)
    public void verifyMeetingSectionOnCreateEvent()
    {
        MeetingFields meetingFields = popUp.getEventSummaryTab().getEventDetails().getMeetingFields();
        meetingFields.expand();

        verifyThat(meetingFields.getIncludeMeeting(), displayed(true));
        verifyThat(meetingFields.getStartDate(), displayed(true));
        verifyThat(meetingFields.getEndDate(), displayed(true));
        verifyThat(meetingFields.getNumberOfMeetingRooms(), displayed(true));
        verifyThat(meetingFields.getTotalDelegates(), displayed(true));

        MeetingRevenueDetails meetingRevenueDetail = meetingFields.getRevenueDetails();
        meetingRevenueDetail.getMeetingRoom().verifyDefault();
        meetingRevenueDetail.getFoodAndBeverage().verifyDefault();
        meetingRevenueDetail.getMiscellaneous().verifyDefault();
        meetingRevenueDetail.getTotalMeetingRevenue().verifyDefault();
    }

    @Test(dependsOnMethods = { "verifyEventInformationSectionOnCreateEvent" }, priority = 0, alwaysRun = true)
    public void verifyGuestRoomsSectionOnCreateEvent()
    {
        GuestRoomsFields guestRoomsFields = popUp.getEventSummaryTab().getEventDetails().getGuestRoomsFields();
        guestRoomsFields.expand();
        guestRoomsFields.getEligibleRoomRevenue().verifyDefault();
        guestRoomsFields.getTotalRoom().verifyDefault();

        verifyThat(guestRoomsFields.getCorporateID(), displayed(true));
        verifyThat(guestRoomsFields.getIATANumber(), displayed(true));
        verifyThat(guestRoomsFields.getIncludeGuestRoomsRevenue(), displayed(true));
        verifyThat(guestRoomsFields.getTotalGuestRooms(), displayed(true));
        verifyThat(guestRoomsFields.getStayEndDate(), displayed(true));
        verifyThat(guestRoomsFields.getStayStartDate(), displayed(true));
        verifyThat(guestRoomsFields.getTotalGuests(), displayed(true));
        verifyThat(guestRoomsFields.getTotalRoomNights(), displayed(true));
    }

    @Test(dependsOnMethods = { "verifyEventInformationSectionOnCreateEvent" }, priority = 0, alwaysRun = true)
    public void verifyTotalEventRevenueSectionOnCreateEvent()
    {
        popUp.getEventSummaryTab().getTotalEligibleRevenue().verifyDefault();
    }

    @Test(dependsOnMethods = { "verifyEventInformationSectionOnCreateEvent" }, priority = 0, alwaysRun = true)
    public void verifyDiscretionaryPointsSectionOnCreateEvent()
    {
        DiscretionaryPointsFields discretionaryPointsFields = popUp.getEventSummaryTab().getDiscretionaryPoints();
        discretionaryPointsFields.expand();

        verifyThat(discretionaryPointsFields.getAdditionalPointsToAward(), displayed(true));
        verifyThat(discretionaryPointsFields.getComments(), displayed(true));
        verifyThat(discretionaryPointsFields.getIncludeDiscretionaryPoints(), displayed(true));
        verifyThat(discretionaryPointsFields.getReason(), displayed(true));
        verifyThat(discretionaryPointsFields.getEstimatedCost(), hasText(NOT_AVAILABLE));
    }

    @Test(dependsOnMethods = { "verifyEventInformationSectionOnCreateEvent" }, priority = 0, alwaysRun = true)
    public void verifyOfferFieldsOnCreateEvent()
    {
        OffersFieldsEdit offerFields = popUp.getEventSummaryTab().getOffers();
        offerFields.expand();

        verifyThat(offerFields.getIncludeOffers(), displayed(true));
        verifyThat(offerFields.getOffer(), displayed(true));
        verifyThat(offerFields.getPointsToAward(), displayed(true));
        verifyThat(offerFields.getEstimatedCost(), hasText(NOT_AVAILABLE));
    }

    @Test(dependsOnMethods = { "verifyEventInformationSectionOnCreateEvent" }, priority = 0, alwaysRun = true)
    public void verifyTotalEventAwardSectionOnCreateEvent()
    {
        TotalEventAwardFields totalEventAwardFields = popUp.getEventSummaryTab().getTotalEventAward();
        totalEventAwardFields.getBasePoints().verifyDefault();
        totalEventAwardFields.getDiscretionaryPoints().verifyDefault();
        totalEventAwardFields.getOfferPoints().verifyDefault();
        totalEventAwardFields.getTotalPoints().verifyDefault();
    }

    @Test(dependsOnMethods = { "verifyEventInformationSectionOnCreateEvent" }, priority = 1, alwaysRun = true)
    public void verifyRoomsTab()
    {
        BusinessEvent businessEvent = new BusinessEvent("NewEvent011", helper.getUser().getLocation(), Currency.USD);

        popUp.getEventSummaryTab().getEventInformation().populate(businessEvent);

        RoomsTab roomTab = popUp.getRoomsTab();
        roomTab.goTo();

        EventStaysGrid eventStaysGrid = roomTab.getEventStaysGrid();
        verifyThat(eventStaysGrid, displayed(true));
        eventStaysGrid.verifyHeader();

        AllPastStaysGrid allPastStaysGrid = roomTab.getAllPastStaysGrid();
        verifyThat(allPastStaysGrid, displayed(true));
        allPastStaysGrid.verifyHeader();

        StaySearch staySearch = roomTab.getSearchFields();

        verifyThat(staySearch.getHotel(), hasText(businessEvent.getHotel()));
        roomTab.getTotalEligibleRoomRevenue().verify(NOT_AVAILABLE, Currency.USD);

        verifyThat(staySearch.getGuestName(), displayed(true));
        verifyThat(staySearch.getConfirmationNumber(), displayed(true));
        verifyThat(staySearch.getToDate(), displayed(true));
        verifyThat(staySearch.getFromDate(), displayed(true));
    }

}
