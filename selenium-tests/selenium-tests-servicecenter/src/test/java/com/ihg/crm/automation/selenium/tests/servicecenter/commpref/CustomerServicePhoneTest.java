package com.ihg.crm.automation.selenium.tests.servicecenter.commpref;

import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyNoError;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasDefault;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static com.ihg.automation.selenium.matchers.component.IsEnabled.enabled;
import static com.ihg.automation.selenium.matchers.component.IsSelected.isSelected;
import static com.ihg.automation.selenium.servicecenter.pages.communication.CustomerServicePhone.AFTERNOON;
import static com.ihg.automation.selenium.servicecenter.pages.communication.CustomerServicePhone.AFTERNOON_END_TIME;
import static com.ihg.automation.selenium.servicecenter.pages.communication.CustomerServicePhone.AFTERNOON_START_TIME;
import static com.ihg.automation.selenium.servicecenter.pages.communication.CustomerServicePhone.EVENING;
import static com.ihg.automation.selenium.servicecenter.pages.communication.CustomerServicePhone.EVENING_END_TIME;
import static com.ihg.automation.selenium.servicecenter.pages.communication.CustomerServicePhone.EVENING_START_TIME;
import static com.ihg.automation.selenium.servicecenter.pages.communication.CustomerServicePhone.MORNING;
import static com.ihg.automation.selenium.servicecenter.pages.communication.CustomerServicePhone.MORNING_END_TIME;
import static com.ihg.automation.selenium.servicecenter.pages.communication.CustomerServicePhone.MORNING_START_TIME;
import static com.ihg.automation.selenium.servicecenter.pages.communication.CustomerServicePhone.NIGHT;
import static com.ihg.automation.selenium.servicecenter.pages.communication.CustomerServicePhone.NIGHT_END_TIME;
import static com.ihg.automation.selenium.servicecenter.pages.communication.CustomerServicePhone.NIGHT_START_TIME;
import static com.ihg.automation.selenium.servicecenter.pages.communication.DaysMenu.ANY;
import static com.ihg.automation.selenium.servicecenter.pages.communication.DaysMenu.ANY_AUDIT;
import static com.ihg.automation.selenium.servicecenter.pages.communication.DaysMenu.MONDAY;
import static com.ihg.automation.selenium.servicecenter.pages.communication.DaysMenu.MONDAY_AUDIT;
import static com.ihg.automation.selenium.servicecenter.pages.communication.DaysMenu.WEEKDAYS;
import static com.ihg.automation.selenium.servicecenter.pages.communication.DaysMenu.WEEKDAYS_AUDIT;
import static com.ihg.automation.selenium.servicecenter.pages.communication.DaysMenu.WEEKENDS;
import static com.ihg.automation.selenium.servicecenter.pages.history.AuditConstant.CommunicationPreferences.COMMUNICATION_PREFERENCES;
import static com.ihg.automation.selenium.servicecenter.pages.history.AuditConstant.CommunicationPreferences.DAYS_OF_THE_WEEK;
import static com.ihg.automation.selenium.servicecenter.pages.history.AuditConstant.CommunicationPreferences.PHONE;
import static com.ihg.automation.selenium.servicecenter.pages.history.AuditConstant.CommunicationPreferences.PHONE_FOR_CUSTOMER_SERVICE;
import static com.ihg.automation.selenium.servicecenter.pages.history.AuditConstant.CommunicationPreferences.PREFERRED_TIME;
import static org.hamcrest.Matchers.containsString;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.ihg.automation.common.RandomUtils;
import com.ihg.automation.selenium.common.Constant;
import com.ihg.automation.selenium.common.member.GuestEmail;
import com.ihg.automation.selenium.common.member.GuestPhone;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.gwt.components.MessageBox;
import com.ihg.automation.selenium.gwt.components.MessageBoxType;
import com.ihg.automation.selenium.gwt.components.input.Select;
import com.ihg.automation.selenium.servicecenter.pages.LeftPanel;
import com.ihg.automation.selenium.servicecenter.pages.Messages;
import com.ihg.automation.selenium.servicecenter.pages.communication.CommunicationPreferences;
import com.ihg.automation.selenium.servicecenter.pages.communication.CommunicationPreferencesPage;
import com.ihg.automation.selenium.servicecenter.pages.communication.CustomerServicePhone;
import com.ihg.automation.selenium.servicecenter.pages.communication.DaysMenu;
import com.ihg.automation.selenium.servicecenter.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.servicecenter.pages.history.ProfileHistoryGridRow;
import com.ihg.automation.selenium.servicecenter.pages.history.ProfileHistoryPage;
import com.ihg.automation.selenium.servicecenter.pages.personal.PersonalInfoPage;
import com.ihg.crm.automation.selenium.tests.servicecenter.LoginLogout;

public class CustomerServicePhoneTest extends LoginLogout
{
    private Member member = new Member();
    private EnrollmentPage enrollmentPage = new EnrollmentPage();
    private CustomerServicePhone customerServicePhone;
    private GuestPhone secondPhone = new GuestPhone();
    private GuestPhone thirdPhone = new GuestPhone();
    private GuestPhone smsPhone = new GuestPhone();
    private GuestPhone updatePhone = new GuestPhone();
    private PersonalInfoPage personalInfoPage;

    @BeforeClass
    public void beforeClass()
    {
        GuestEmail guestEmail = MemberPopulateHelper.getRandomEmail();

        updatePhone = MemberPopulateHelper.getRandomPhone();

        secondPhone = MemberPopulateHelper.getRandomPhone();
        secondPhone.setPreferred(false);

        thirdPhone = MemberPopulateHelper.getRandomPhone();
        thirdPhone.setPreferred(false);

        smsPhone.setNumber(RandomUtils.getRandomNumber(7));

        member.addProgram(Program.RC);
        member.setPersonalInfo(MemberPopulateHelper.getSimplePersonalInfo());
        member.addAddress(MemberPopulateHelper.getUnitedStatesAddress());
        member.addEmail(guestEmail);
        member.addPhone(MemberPopulateHelper.getRandomPhone());

        login();
        enrollmentPage.enroll(member);
    }

    @DataProvider(name = "verifyDayPartProvider")
    public Object[][] verifyDayPartProvider()
    {
        return new Object[][] { { AFTERNOON, AFTERNOON_START_TIME, AFTERNOON_END_TIME },
                { EVENING, EVENING_START_TIME, EVENING_END_TIME }, { MORNING, MORNING_START_TIME, MORNING_END_TIME },
                { NIGHT, NIGHT_START_TIME, NIGHT_END_TIME } };
    }

    @Test
    public void verifyPhoneEnabled()
    {
        CommunicationPreferencesPage commPrefPage = new CommunicationPreferencesPage();
        commPrefPage.goTo();
        CommunicationPreferences commPrefs = commPrefPage.getCommunicationPreferences();
        commPrefs.clickEdit(verifyNoError());

        customerServicePhone = commPrefs.getCustomerService().getPhone();
        customerServicePhone.getPhoneCheckBox().check();

        Select phone = customerServicePhone.getPhone();
        verifyThat(phone, enabled(true));
        verifyThat(phone, hasText(member.getPreferredPhone().getFullPhone()));

        commPrefs.clickSave();
    }

    @Test(dependsOnMethods = { "verifyPhoneEnabled" }, alwaysRun = true)
    public void verifyHistoryForPhoneEnabled()
    {
        ProfileHistoryPage historyPage = new ProfileHistoryPage();
        historyPage.goTo();
        verifyNoError();

        ProfileHistoryGridRow phoneRow = historyPage.getProfileHistoryGrid().getRow(1)
                .getSubRowByName(COMMUNICATION_PREFERENCES).getSubRowByName(PHONE_FOR_CUSTOMER_SERVICE);

        phoneRow.getSubRowByName(PHONE).verifyAddAction(member.getPreferredPhone().getFullPhone());
        phoneRow.getSubRowByName(DAYS_OF_THE_WEEK).verifyAddAction(ANY_AUDIT);
    }

    @Test(dataProvider = "verifyDayPartProvider", dependsOnMethods = {
            "verifyHistoryForPhoneEnabled" }, alwaysRun = true)
    public void verifyCustomerServicePhoneDayPart(String dayPart, String startTime, String endTime)
    {
        CommunicationPreferencesPage commPrefPage = new CommunicationPreferencesPage();
        commPrefPage.goTo();
        CommunicationPreferences commPrefs = commPrefPage.getCommunicationPreferences();
        customerServicePhone = commPrefs.getCustomerService().getPhone();
        commPrefs.clickEdit();

        customerServicePhone.getDayPart().select(dayPart);
        verifyThat(customerServicePhone.getStartTime(), hasText(startTime));
        verifyThat(customerServicePhone.getEndTime(), hasText(endTime));

        commPrefs.clickSave();

        verifyThat(customerServicePhone.getStartTime().getView(), hasText(startTime));
        verifyThat(customerServicePhone.getEndTime().getView(), hasText(endTime));
        verifyThat(customerServicePhone.getDayPart().getView(), hasText(dayPart));
    }

    @Test(dependsOnMethods = { "verifyCustomerServicePhoneDayPart" }, alwaysRun = true)
    public void verifyUpdatingPhone()
    {
        PersonalInfoPage personalInfoPage = new PersonalInfoPage();
        personalInfoPage.goTo();
        personalInfoPage.getPhoneList().getContact().update(updatePhone);

        verifyThat(new MessageBox(MessageBoxType.SUCCESS),
                hasText(containsString(Messages.getUpdatedMessage(false, true))));

        CommunicationPreferencesPage commPrefPage = new CommunicationPreferencesPage();
        commPrefPage.goTo();
        customerServicePhone = commPrefPage.getCommunicationPreferences().getCustomerService().getPhone();
        verifyThat(customerServicePhone.getPhone().getView(), hasText(updatePhone.getFullPhone()));
    }

    @Test(dependsOnMethods = { "verifyUpdatingPhone" }, alwaysRun = true)
    public void verifyAddingSecondPhoneNumber()
    {
        CommunicationPreferencesPage commPrefPage = new CommunicationPreferencesPage();
        commPrefPage.goTo();
        CommunicationPreferences commPrefs = commPrefPage.getCommunicationPreferences();
        commPrefs.clickEdit(verifyNoError());

        customerServicePhone = commPrefs.getCustomerService().getPhone();
        customerServicePhone.getPhoneCheckBox().check();

        customerServicePhone.getStartTime().select(Constant.NOT_AVAILABLE.toUpperCase());
        customerServicePhone.getEndTime().select(Constant.NOT_AVAILABLE.toUpperCase());

        commPrefs.clickSave();

        personalInfoPage = new PersonalInfoPage();
        personalInfoPage.goTo();
        personalInfoPage.getPhoneList().add(secondPhone);

        commPrefPage.goTo();
        commPrefPage.getCommunicationPreferences().clickEdit(verifyNoError());

        customerServicePhone.getPhoneCheckBox().check();
        verifyThat(customerServicePhone.getPhone(), hasText(updatePhone.getFullPhone()));

        customerServicePhone.getPhone().select(secondPhone.getFullPhone());
        commPrefs.clickSave();
        verifyThat(customerServicePhone.getPhone().getView(), hasText(secondPhone.getFullPhone()));
    }

    @Test(dependsOnMethods = { "verifyAddingSecondPhoneNumber" }, alwaysRun = true)
    public void verifyHistoryAfterAddingSecondPhone()
    {
        ProfileHistoryPage historyPage = new ProfileHistoryPage();
        historyPage.goTo();
        verifyNoError();

        ProfileHistoryGridRow phoneRow = historyPage.getProfileHistoryGrid().getRow(1)
                .getSubRowByName(COMMUNICATION_PREFERENCES).getSubRowByName(PHONE_FOR_CUSTOMER_SERVICE);

        phoneRow.getSubRowByName(PHONE).verify(updatePhone.getFullPhone(), secondPhone.getFullPhone());
    }

    @Test(dependsOnMethods = { "verifyHistoryAfterAddingSecondPhone" }, alwaysRun = true)
    public void verifyAddingThirdPhoneNumber()
    {
        personalInfoPage = new PersonalInfoPage();
        personalInfoPage.goTo();
        personalInfoPage.getPhoneList().add(thirdPhone);

        CommunicationPreferencesPage commPrefPage = new CommunicationPreferencesPage();
        commPrefPage.goTo();
        CommunicationPreferences commPrefs = commPrefPage.getCommunicationPreferences();
        commPrefs.clickEdit(verifyNoError());

        customerServicePhone.getPhoneCheckBox().check();

        customerServicePhone.getPhone().select(thirdPhone.getFullPhone());
        commPrefs.clickSave();
        verifyThat(customerServicePhone.getPhone().getView(), hasText(thirdPhone.getFullPhone()));
    }

    @Test(dependsOnMethods = { "verifyAddingThirdPhoneNumber" }, alwaysRun = true)
    public void verifyCustomerServicePhoneWeekdays()
    {
        new LeftPanel().reOpenMemberProfile(member);

        CommunicationPreferencesPage commPrefPage = new CommunicationPreferencesPage();
        commPrefPage.goTo();

        CommunicationPreferences commPrefs = commPrefPage.getCommunicationPreferences();
        customerServicePhone = commPrefs.getCustomerService().getPhone();
        commPrefs.clickEdit();

        DaysMenu daysMenu = customerServicePhone.getDays();
        daysMenu.click();
        daysMenu.getWeekdays().click();
        daysMenu.click();

        verifyThat(daysMenu.getSaturday(), isSelected(false));
        verifyThat(daysMenu.getSunday(), isSelected(false));

        verifyThat(daysMenu.getMonday(), isSelected(true));
        verifyThat(daysMenu.getTuesday(), isSelected(true));
        verifyThat(daysMenu.getWednesday(), isSelected(true));
        verifyThat(daysMenu.getThursday(), isSelected(true));
        verifyThat(daysMenu.getFriday(), isSelected(true));

        commPrefs.clickSave();
        verifyThat(customerServicePhone.getDaysInView(), hasText(WEEKDAYS));
    }

    @Test(dependsOnMethods = { "verifyCustomerServicePhoneWeekdays" }, alwaysRun = true)
    public void verifyHistoryAfterAddPhoneWeekdays()
    {
        ProfileHistoryPage historyPage = new ProfileHistoryPage();
        historyPage.goTo();
        verifyNoError();

        ProfileHistoryGridRow phoneRow = historyPage.getProfileHistoryGrid().getRow(1)
                .getSubRowByName(COMMUNICATION_PREFERENCES).getSubRowByName(PHONE_FOR_CUSTOMER_SERVICE);

        phoneRow.getSubRowByName(DAYS_OF_THE_WEEK).verify(ANY_AUDIT, WEEKDAYS_AUDIT);
    }

    @Test(dependsOnMethods = { "verifyHistoryAfterAddPhoneWeekdays" }, alwaysRun = true)
    public void verifyCustomerServicePhoneWeekends()
    {
        CommunicationPreferencesPage commPrefPage = new CommunicationPreferencesPage();
        commPrefPage.goTo();
        CommunicationPreferences commPrefs = commPrefPage.getCommunicationPreferences();
        customerServicePhone = commPrefs.getCustomerService().getPhone();
        commPrefs.clickEdit();

        DaysMenu daysMenu = customerServicePhone.getDays();
        daysMenu.click();
        daysMenu.getWeekends().click();
        daysMenu.click();

        verifyThat(daysMenu.getSaturday(), isSelected(true));
        verifyThat(daysMenu.getSunday(), isSelected(true));

        verifyThat(daysMenu.getMonday(), isSelected(false));
        verifyThat(daysMenu.getTuesday(), isSelected(false));
        verifyThat(daysMenu.getWednesday(), isSelected(false));
        verifyThat(daysMenu.getThursday(), isSelected(false));
        verifyThat(daysMenu.getFriday(), isSelected(false));

        commPrefs.clickSave();
        verifyThat(customerServicePhone.getDaysInView(), hasText(WEEKENDS));
    }

    @Test(dependsOnMethods = { "verifyCustomerServicePhoneWeekends" }, alwaysRun = true)
    public void verifyCustomerServicePhoneAny()
    {
        CommunicationPreferencesPage commPrefPage = new CommunicationPreferencesPage();
        CommunicationPreferences commPrefs = commPrefPage.getCommunicationPreferences();
        customerServicePhone = commPrefs.getCustomerService().getPhone();
        commPrefs.clickEdit();

        DaysMenu daysMenu = customerServicePhone.getDays();
        daysMenu.click();
        daysMenu.getAny().click();
        daysMenu.click();

        verifyThat(daysMenu.getSaturday(), isSelected(true));
        verifyThat(daysMenu.getSunday(), isSelected(true));

        verifyThat(daysMenu.getMonday(), isSelected(true));
        verifyThat(daysMenu.getTuesday(), isSelected(true));
        verifyThat(daysMenu.getWednesday(), isSelected(true));
        verifyThat(daysMenu.getThursday(), isSelected(true));
        verifyThat(daysMenu.getFriday(), isSelected(true));

        commPrefs.clickSave();
        verifyThat(customerServicePhone.getDaysInView(), hasText(ANY));
    }

    @Test(dependsOnMethods = { "verifyCustomerServicePhoneAny" }, alwaysRun = true)
    public void changeCustomerServicePhone()
    {
        new LeftPanel().reOpenMemberProfile(member);

        CommunicationPreferencesPage commPrefPage = new CommunicationPreferencesPage();
        commPrefPage.goTo();
        CommunicationPreferences commPrefs = commPrefPage.getCommunicationPreferences();
        commPrefs.clickEdit(verifyNoError());

        customerServicePhone = commPrefs.getCustomerService().getPhone();
        customerServicePhone.getPhoneCheckBox().check();

        customerServicePhone.getStartTime().select("05:00");
        customerServicePhone.getEndTime().select("15:00");

        verifyThat(customerServicePhone.getDayPart(), hasDefault());

        DaysMenu daysMenu = customerServicePhone.getDays();

        daysMenu.uncheckAll();
        daysMenu.getMonday().check();

        commPrefs.clickSave();

        verifyThat(customerServicePhone.getDaysInView(), hasText(MONDAY));
        verifyThat(customerServicePhone.getStartTime().getView(), hasText("05:00"));
        verifyThat(customerServicePhone.getEndTime().getView(), hasText("15:00"));
    }

    @Test(dependsOnMethods = { "changeCustomerServicePhone" }, alwaysRun = true)
    public void verifyHistoryAfterChangeCustomerServicePhone()
    {
        ProfileHistoryPage historyPage = new ProfileHistoryPage();
        historyPage.goTo();
        verifyNoError();

        ProfileHistoryGridRow phoneRow = historyPage.getProfileHistoryGrid().getRow(1)
                .getSubRowByName(COMMUNICATION_PREFERENCES).getSubRowByName(PHONE_FOR_CUSTOMER_SERVICE);

        phoneRow.getSubRowByName(PREFERRED_TIME).verifyAddAction("05:00-15:00");
        phoneRow.getSubRowByName(DAYS_OF_THE_WEEK).verify(ANY_AUDIT, MONDAY_AUDIT);
    }

    @Test(dependsOnMethods = { "verifyHistoryAfterChangeCustomerServicePhone" }, alwaysRun = true)
    public void removeCustomerServicePhone()
    {
        new LeftPanel().reOpenMemberProfile(member);

        CommunicationPreferencesPage commPrefPage = new CommunicationPreferencesPage();
        commPrefPage.goTo();
        CommunicationPreferences commPrefs = commPrefPage.getCommunicationPreferences();
        commPrefs.clickEdit(verifyNoError());

        customerServicePhone = commPrefs.getCustomerService().getPhone();
        customerServicePhone.getPhoneCheckBox().uncheck();

        commPrefs.clickSave();
    }

    @Test(dependsOnMethods = { "removeCustomerServicePhone" }, alwaysRun = true)
    public void verifyHistoryAfterRemoveCustomerServicePhone()
    {
        ProfileHistoryPage historyPage = new ProfileHistoryPage();
        historyPage.goTo();
        verifyNoError();

        ProfileHistoryGridRow phoneRow = historyPage.getProfileHistoryGrid().getRow(1)
                .getSubRowByName(COMMUNICATION_PREFERENCES).getSubRowByName(PHONE_FOR_CUSTOMER_SERVICE);

        phoneRow.getSubRowByName(PHONE).verifyDeleteAction(thirdPhone.getFullPhone());
        phoneRow.getSubRowByName(PREFERRED_TIME).verifyDeleteAction("05:00-15:00");
        phoneRow.getSubRowByName(DAYS_OF_THE_WEEK).verifyDeleteAction(MONDAY_AUDIT);
    }

}
