package com.ihg.crm.automation.selenium.tests.servicecenter.programinfo.ambassador;

import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyNoError;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.event.EventTransactionType.Membership.MEMBERSHIP_OPEN;
import static com.ihg.automation.selenium.common.member.AmbassadorUtils.AMBASSADOR_CORP_DISC_$150;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.common.DateUtils;
import com.ihg.automation.selenium.common.catalog.CatalogItem;
import com.ihg.automation.selenium.common.member.AmbassadorJdbcUtils;
import com.ihg.automation.selenium.common.member.AmbassadorOfferCode;
import com.ihg.automation.selenium.common.member.AmbassadorUtils;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.pages.PaymentMethod;
import com.ihg.automation.selenium.common.payment.CheckPayment;
import com.ihg.automation.selenium.common.payment.CheckPaymentContainer;
import com.ihg.automation.selenium.common.payment.PaymentDetailsPopUp;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.common.types.program.ProgramStatus;
import com.ihg.automation.selenium.gwt.components.Mode;
import com.ihg.automation.selenium.servicecenter.pages.LeftPanel;
import com.ihg.automation.selenium.servicecenter.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsGridRow;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsPage;
import com.ihg.automation.selenium.servicecenter.pages.event.enroll.MembershipDetails;
import com.ihg.automation.selenium.servicecenter.pages.event.enroll.MembershipGridRowContainer;
import com.ihg.automation.selenium.servicecenter.pages.programs.RenewExtendProgramSummary;
import com.ihg.automation.selenium.servicecenter.pages.programs.amb.AmbassadorPage;

public class ReactivateByCheckTest extends AmbassadorCommon
{
    private static final CatalogItem ENROLMENT_AWARD = AMBASSADOR_CORP_DISC_$150;

    private AmbassadorPage ambassadorPage = new AmbassadorPage();
    private PaymentDetailsPopUp paymentDetailsPopUp = new PaymentDetailsPopUp();
    private AllEventsPage allEventsPage = new AllEventsPage();
    private CheckPaymentContainer checkPayment;
    private final static String DEPOSIT_DATE = DateUtils.getFormattedDate();
    private final static String DEPOSIT_NUMBER = "1234567";
    private final static String ISSUING_BANK = "Issuing bank";
    private final static String NAME_ON_ACCOUNT = "Name on account";
    private final static String CHECK_NUMBER = "7788999000";
    private CheckPayment checkPaymentItem = new CheckPayment(ENROLMENT_AWARD, CHECK_NUMBER, NAME_ON_ACCOUNT,
            ISSUING_BANK, DEPOSIT_DATE, DEPOSIT_NUMBER);
    private Member member;
    private EnrollmentPage enrollmentPage = new EnrollmentPage();

    @BeforeClass
    public void beforeClass()
    {
        login();

        member = enrollAMBmember();
        new AmbassadorJdbcUtils(jdbcTemplateUpdate, lastUpdateUserId).updateForReactivate(member);
        new LeftPanel().reOpenMemberProfile(member);

        enrollmentPage.goTo();
        enrollmentPage.getPrograms().selectProgram(Program.AMB);
    }

    @Test()
    public void verifyCheckPaymentFields()
    {
        enrollmentPage.getAMBOfferCode().selectByCode(AmbassadorOfferCode.CHNRO);
        enrollmentPage.clickSubmit(verifyNoError());
        paymentDetailsPopUp.selectAmountAndPaymentMethod(ENROLMENT_AWARD, PaymentMethod.CHECK);
        checkPayment = paymentDetailsPopUp.getCheckPayment();
        verifyThat(checkPayment.getCheckNumber(), displayed(true));
        verifyThat(checkPayment.getNameOnAccount(), displayed(true));
        verifyThat(checkPayment.getIssuingBank(), displayed(true));
        verifyThat(checkPayment.getDepositDate(), displayed(true));
        verifyThat(checkPayment.getDepositNumber(), displayed(true));
    }

    @Test(dependsOnMethods = { "verifyCheckPaymentFields" }, alwaysRun = true)
    public void populateCheckPaymentFields()
    {
        checkPayment = paymentDetailsPopUp.getCheckPayment();
        checkPayment.populate(checkPaymentItem);
        paymentDetailsPopUp.clickSubmitNoError();
    }

    @Test(dependsOnMethods = { "populateCheckPaymentFields" }, alwaysRun = true)
    public void verifySuccessEnrollmentPopUp()
    {
        validateSuccessEnrollmentPopUp(Program.AMB, member);
    }

    @Test(dependsOnMethods = { "verifySuccessEnrollmentPopUp" }, alwaysRun = true)
    public void verifySummary()
    {
        ambassadorPage.goTo();

        RenewExtendProgramSummary ambSummary = ambassadorPage.getSummary();
        verifyThat(ambSummary.getExpirationDate(), hasText(AmbassadorUtils.getExpirationDate()));

        ambSummary.verifyRenewAndExtendButtons(false, true);
        ambSummary.verifyStatus(ProgramStatus.OPEN);
    }

    @Test(dependsOnMethods = "verifySummary", alwaysRun = true)
    public void verifyAllEventsContainer()
    {
        allEventsPage.goTo();
        AllEventsGridRow row = allEventsPage.getGrid().getRow(MEMBERSHIP_OPEN);
        verifyThat(row, displayed(true));

        MembershipGridRowContainer container = row.expand(MembershipGridRowContainer.class);
        MembershipDetails membershipDetails = container.getMembershipDetails();
        membershipDetails.getPaymentDetails().verify(checkPaymentItem, Mode.VIEW);
        membershipDetails.verifyScSource(helper);
    }

    @Test(dependsOnMethods = "verifyAllEventsContainer", alwaysRun = true)
    public void verifyOrderSystemContainer()
    {
        verifyOrderSystemDetailsAfterReactivate(member);
    }
}
