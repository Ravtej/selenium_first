package com.ihg.crm.automation.selenium.tests.servicecenter.levelbenefits;

import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyError;
import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyNoError;
import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifySuccess;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.event.EventTransactionType.TierLevel.BENEFIT_REDEEM;
import static com.ihg.automation.selenium.common.event.EventTransactionType.TierLevel.BENEFIT_UPGRADE;
import static com.ihg.automation.selenium.common.types.program.ProgramStatus.OPEN;
import static com.ihg.automation.selenium.common.types.program.RewardClubLevel.PLTN;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.enabled;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static com.ihg.automation.selenium.matchers.component.IsDisplayed.displayed;
import static com.ihg.automation.selenium.matchers.component.IsInvalid.isHighlightedAsInvalid;
import static org.hamcrest.Matchers.containsString;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.ihg.automation.common.DateUtils;
import com.ihg.automation.selenium.common.Constant;
import com.ihg.automation.selenium.common.billing.EventBillingDetailsTab;
import com.ihg.automation.selenium.common.earning.EventEarningDetailsTab;
import com.ihg.automation.selenium.common.event.AllEventsRow;
import com.ihg.automation.selenium.common.event.EarningEventFactory;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberJdbcUtils;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.types.program.EmployeeLevel;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.common.types.program.RewardClubLevel;
import com.ihg.automation.selenium.common.types.program.RewardClubLevelReason;
import com.ihg.automation.selenium.gwt.components.dialog.ConfirmDialog;
import com.ihg.automation.selenium.gwt.components.input.Input;
import com.ihg.automation.selenium.servicecenter.pages.LeftPanel;
import com.ihg.automation.selenium.servicecenter.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsGridRow;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsPage;
import com.ihg.automation.selenium.servicecenter.pages.event.benefit.BenefitContainer;
import com.ihg.automation.selenium.servicecenter.pages.event.benefit.BenefitRedeemDetailsPopUp;
import com.ihg.automation.selenium.servicecenter.pages.event.benefit.BenefitUpgradeDetailsPopUp;
import com.ihg.automation.selenium.servicecenter.pages.event.offer.TierLevelUpgradePopUp;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.BenefitsGrid;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.BenefitsGridRow;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.BenefitsGridRowContainer;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.RewardClubPage;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.RewardClubSummary;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.TierLevelBenefitsDetailsPopUp;

public class RedeemLevelBenefitTest extends LevelBenefitsCommon
{
    private String platinumMember;
    private String spireMember;
    private String closedMember;
    private String notRcMember;
    private String receivingMemberId;
    private Member giftingMember = new Member();
    private Member receivingMember = new Member();
    private BenefitsGridRow benefitGiftPlatinumItemRow;
    private RewardClubSummary rewardClubSummary;
    private BenefitRedeemDetailsPopUp benefitRedeemDetailsPopUp;
    private BenefitContainer benefitRedeemContainer;
    private BenefitContainer benefitUpgradeContainer;
    private TierLevelBenefitsDetailsPopUp tierLevelBenefitsDetailsPopUp;
    private BenefitUpgradeDetailsPopUp benefitUpgradeDetailsPopUp;
    private final static String MEMBER_ID = "SELECT MBRSHP_ID FROM DGST.MBRSHP"//
            + " WHERE LYTY_PGM_CD='%s' "//
            + " AND MBRSHP_LVL_CD ='%s' "//
            + " AND MBRSHP_STAT_CD='%s'"//
            + " AND LST_UPDT_TS < SYSDATE"//
            + " AND ROWNUM < 2";

    @BeforeClass
    public void before()
    {
        platinumMember = MemberJdbcUtils.getMemberId(jdbcTemplate, String.format(MEMBER_ID, "PC", PLTN.getCode(), "O"));
        spireMember = MemberJdbcUtils.getMemberId(jdbcTemplate,
                String.format(MEMBER_ID, "PC", RewardClubLevel.SPRE.getCode(), "O"));
        closedMember = MemberJdbcUtils.getMemberId(jdbcTemplate,
                String.format(MEMBER_ID, "PC", RewardClubLevel.CLUB.getCode(), "C"));
        notRcMember = MemberJdbcUtils.getMemberId(jdbcTemplate,
                String.format(MEMBER_ID, Program.EMP.getCode(), EmployeeLevel.EMPL.getCode(), "O"));

        giftingMember.addProgram(Program.RC);
        giftingMember.setPersonalInfo(MemberPopulateHelper.getSimplePersonalInfo());
        giftingMember.addAddress(MemberPopulateHelper.getUnitedStatesAddress());

        receivingMember.addProgram(Program.RC);
        receivingMember.setPersonalInfo(MemberPopulateHelper.getSimplePersonalInfo());
        receivingMember.addAddress(MemberPopulateHelper.getUnitedStatesAddress());

        login();

        new EnrollmentPage().enroll(receivingMember);
        receivingMemberId = receivingMember.getRCProgramId();

        new LeftPanel().clickNewSearch();
        new EnrollmentPage().enroll(giftingMember);

        RewardClubPage rewardClubPage = new RewardClubPage();
        rewardClubPage.goTo();
        rewardClubSummary = rewardClubPage.getSummary();
        rewardClubSummary.setTierLevelWithExpiration(RewardClubLevel.SPRE, RewardClubLevelReason.POINTS);
    }

    @Test
    public void cancelRedeemLevelBenefits()
    {
        tierLevelBenefitsDetailsPopUp = rewardClubSummary.openTierLevelBenefitsDetailsPopUp();

        benefitGiftPlatinumItemRow = tierLevelBenefitsDetailsPopUp.getBenefitsGrid().getRow(benefitGiftPlatinumItem);
        benefitGiftPlatinumItemRow.clickRedeem();

        TierLevelUpgradePopUp tierLevelUpgradePopUp = new TierLevelUpgradePopUp();
        verifyThat(tierLevelUpgradePopUp, displayed(true));
        tierLevelUpgradePopUp.close();
    }

    @DataProvider(name = "invalidMemberIdProvider")
    protected Object[][] invalidMemberIdProvider()
    {
        return new Object[][] { { "1111" }, { notRcMember }, { closedMember }, { giftingMember.getRCProgramId() } };
    }

    @Test(dependsOnMethods = {
            "cancelRedeemLevelBenefits" }, dataProvider = "invalidMemberIdProvider", alwaysRun = true)
    public void tryToRedeemWithInvalidMemberId(String memberId)
    {
        benefitGiftPlatinumItemRow.clickRedeem();

        TierLevelUpgradePopUp tierLevelUpgradePopUp = new TierLevelUpgradePopUp();
        Input memberIdInput = tierLevelUpgradePopUp.getMemberId();
        memberIdInput.typeAndSubmit(memberId);

        verifyThat(memberIdInput, isHighlightedAsInvalid(true));
        verifyThat(tierLevelUpgradePopUp.getSubmit(), enabled(false));

        memberIdInput.clear();
        tierLevelUpgradePopUp.clickClose();
    }

    @DataProvider(name = "platinumAndSpireMemberIdProvider")
    protected Object[][] platinumAndSpireMemberIdProvider()
    {
        return new Object[][] { { platinumMember, "Member is already PLATINUM Tier-Level" },
                { spireMember, "Member is already SPIRE Tier-Level" } };
    }

    @Test(dataProvider = "platinumAndSpireMemberIdProvider", dependsOnMethods = {
            "cancelRedeemLevelBenefits" }, alwaysRun = true)
    public void tryToRedeemWithPlatinumAndSpireStatusMemberId(String memberId, String message)
    {
        benefitGiftPlatinumItemRow.clickRedeem();

        TierLevelUpgradePopUp tierLevelUpgradePopUp = new TierLevelUpgradePopUp();
        Input memberIdInput = tierLevelUpgradePopUp.getMemberId();
        memberIdInput.typeAndWait(memberId);

        tierLevelUpgradePopUp.getSubmit().clickAndWait();

        new ConfirmDialog().clickYes(verifyError(message));

        memberIdInput.clear();
        tierLevelUpgradePopUp.close();
    }

    @Test(dependsOnMethods = { "tryToRedeemWithPlatinumAndSpireStatusMemberId" }, alwaysRun = true)
    public void redeemOnlyLevelBenefits()
    {
        benefitGiftPlatinumItemRow.clickRedeem();

        TierLevelUpgradePopUp tierLevelUpgradePopUp = new TierLevelUpgradePopUp();
        tierLevelUpgradePopUp.getMemberId().typeAndWait(receivingMemberId);
        verifyThat(tierLevelUpgradePopUp, hasText(containsString(receivingMember.getName().getFullName())));
        tierLevelUpgradePopUp.clickSubmit(verifyNoError());

        new ConfirmDialog().clickYes(verifySuccess(SUCCESS_BENEFIT_REDEEM_MESSAGE));

        tierLevelBenefitsDetailsPopUp = rewardClubSummary.openTierLevelBenefitsDetailsPopUp();

        BenefitsGrid benefitsGrid = tierLevelBenefitsDetailsPopUp.getBenefitsGrid();

        benefitGiftPlatinumItemRow = benefitsGrid.getRow(benefitGiftPlatinumItem);
        benefitGiftPlatinumItemRow.verifyAfterRedeemForRedeemedBenefit(benefitGiftPlatinumItem,
                DateUtils.getFormattedDate());
        benefitGiftPlatinumItemRow.expand(BenefitsGridRowContainer.class).verifyForLevelBenefit(benefitGiftPlatinumItem,
                DateUtils.getFormattedDate(), receivingMember);

        BenefitsGridRow benefitPointsItemRow = benefitsGrid.getRow(benefitPointsItem);
        benefitPointsItemRow.verifyAfterRedeemForNotRedeemedBenefit(benefitPointsItem);

        tierLevelBenefitsDetailsPopUp.close();
    }

    @Test(dependsOnMethods = { "redeemOnlyLevelBenefits" }, alwaysRun = true)
    public void verifyAllEventsBenefitRedeemEvent()
    {
        AllEventsPage allEventsPage = new AllEventsPage();
        allEventsPage.goTo();

        AllEventsRow benefitUpgradeRowData = new AllEventsRow(DateUtils.getFormattedDate(), BENEFIT_REDEEM,
                benefitGiftPlatinumItem.getItemName(), "", "");

        AllEventsGridRow benefitRedeemRow = allEventsPage.getGrid().getRow(BENEFIT_REDEEM);
        benefitRedeemRow.verify(benefitUpgradeRowData);

        benefitRedeemContainer = benefitRedeemRow.expand(BenefitContainer.class);
        benefitRedeemContainer.getBenefitRedeemDetails().verifyForLevelBenefit(benefitGiftPlatinumItem, receivingMember,
                helper);
    }

    @Test(dependsOnMethods = { "verifyAllEventsBenefitRedeemEvent" }, alwaysRun = true)
    public void verifyBenefitRedeemEventDetails()
    {
        benefitRedeemContainer.clickDetails();
        benefitRedeemDetailsPopUp = new BenefitRedeemDetailsPopUp();
        benefitRedeemDetailsPopUp.getBenefitRedeemDetailsTab().getBenefitRedeemDetails()
                .verifyForLevelBenefit(benefitGiftPlatinumItem, receivingMember, helper);

    }

    @Test(dependsOnMethods = { "verifyBenefitRedeemEventDetails" }, alwaysRun = true)
    public void verifyEarningDetails()
    {
        EventEarningDetailsTab eventEarningDetailsTab = benefitRedeemDetailsPopUp.getEarningDetailsTab();
        eventEarningDetailsTab.goTo();

        eventEarningDetailsTab.verifyBasicDetailsForPoints(Constant.NOT_AVAILABLE, Constant.NOT_AVAILABLE);
        eventEarningDetailsTab.getGrid().verify(EarningEventFactory.getAwardEvent(benefitGiftPlatinumItem));
    }

    @Test(dependsOnMethods = { "verifyEarningDetails" }, alwaysRun = true)
    public void verifyBillingDetails()
    {
        EventBillingDetailsTab billingDetailsTab = benefitRedeemDetailsPopUp.getBillingDetailsTab();
        billingDetailsTab.goTo();

        billingDetailsTab.verifyTabIsEmpty(BENEFIT_REDEEM);
        benefitRedeemDetailsPopUp.close();
    }

    @Test(dependsOnMethods = { "verifyBillingDetails" }, alwaysRun = true)
    public void openReceivingMember()
    {
        new LeftPanel().reOpenMemberProfile(receivingMember);

        RewardClubPage rewardClubPage = new RewardClubPage();
        rewardClubPage.goTo();
        rewardClubPage.getSummary().verify(OPEN, PLTN, 1);
    }

    @Test(dependsOnMethods = { "openReceivingMember" }, alwaysRun = true)
    public void verifyBenefitUpgradeEventForReceivingMember()
    {
        AllEventsPage allEventsPage = new AllEventsPage();
        allEventsPage.goTo();
        AllEventsRow benefitUpgradeRowData = new AllEventsRow(DateUtils.getFormattedDate(), BENEFIT_UPGRADE,
                benefitGiftPlatinumItem.getItemName(), "", "");

        AllEventsGridRow benefitUpgradeRow = allEventsPage.getGrid().getRow(BENEFIT_UPGRADE);
        benefitUpgradeRow.verify(benefitUpgradeRowData);

        benefitUpgradeContainer = benefitUpgradeRow.expand(BenefitContainer.class);
        benefitUpgradeContainer.getBenefitRedeemDetails().verifyForLevelBenefit(benefitGiftPlatinumItem, giftingMember,
                helper);
    }

    @Test(dependsOnMethods = { "verifyBenefitUpgradeEventForReceivingMember" }, alwaysRun = true)
    public void verifyBenefitRedeemEventDetailsForReceivingMember()
    {
        benefitUpgradeContainer.clickDetails();
        benefitUpgradeDetailsPopUp = new BenefitUpgradeDetailsPopUp();
        benefitUpgradeDetailsPopUp.getBenefitUpgradeDetailsTab().getBenefitUpgradeDetails()
                .verifyForLevelBenefit(benefitGiftPlatinumItem, giftingMember, helper);
    }

    @Test(dependsOnMethods = { "verifyBenefitRedeemEventDetailsForReceivingMember" }, alwaysRun = true)
    public void verifyEarningDetailsForReceivingMember()
    {
        EventEarningDetailsTab eventEarningDetailsTab = benefitUpgradeDetailsPopUp.getEarningDetailsTab();
        eventEarningDetailsTab.goTo();

        eventEarningDetailsTab.verifyBasicDetailsNoPointsAndMiles();
        eventEarningDetailsTab.getGrid().verify(EarningEventFactory.getAwardEvent(benefitGiftPlatinumItem));
    }

    @Test(dependsOnMethods = { "verifyEarningDetailsForReceivingMember" }, alwaysRun = true)
    public void verifyBillingDetailsForReceivingMember()
    {
        EventBillingDetailsTab billingDetailsTab = benefitUpgradeDetailsPopUp.getBillingDetailsTab();
        billingDetailsTab.goTo();

        billingDetailsTab.verifyTabIsEmpty(BENEFIT_UPGRADE);
    }
}
