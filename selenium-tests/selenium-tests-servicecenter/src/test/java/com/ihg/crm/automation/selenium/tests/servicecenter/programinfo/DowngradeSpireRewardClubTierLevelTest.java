package com.ihg.crm.automation.selenium.tests.servicecenter.programinfo;

import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifySuccess;
import static com.ihg.automation.selenium.common.types.program.RewardClubLevel.CLUB;
import static com.ihg.automation.selenium.common.types.program.RewardClubLevel.GOLD;
import static com.ihg.automation.selenium.common.types.program.RewardClubLevel.PLTN;
import static com.ihg.automation.selenium.common.types.program.RewardClubLevel.SPRE;
import static com.ihg.automation.selenium.common.types.program.RewardClubLevelReason.BASE;
import static com.ihg.automation.selenium.common.types.program.TierLevelExpiration.LIFETIME;
import static com.ihg.automation.selenium.servicecenter.pages.programs.ProgramPageBase.SUCCESS_MESSAGE;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.common.types.program.ProgramLevel;
import com.ihg.automation.selenium.common.types.program.ProgramStatus;
import com.ihg.automation.selenium.servicecenter.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.RewardClubPage;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.RewardClubSummary;
import com.ihg.crm.automation.selenium.tests.servicecenter.LoginLogout;

@Test(groups = "karma")
public class DowngradeSpireRewardClubTierLevelTest extends LoginLogout
{

    private Member member = new Member();
    private RewardClubPage rewardClubPage = new RewardClubPage();
    private RewardClubSummary rcSummary;

    @BeforeClass
    public void before()
    {
        member.setPersonalInfo(MemberPopulateHelper.getSimplePersonalInfo());
        member.addAddress(MemberPopulateHelper.getUnitedStatesAddress());

        member.addProgram(Program.RC);

        login();

        new EnrollmentPage().enroll(member);
    }

    @BeforeMethod
    public void setSpireTierLevel()
    {
        rewardClubPage.goTo();
        rcSummary = rewardClubPage.getSummary();
        rcSummary.setTierLevelWithExpiration(SPRE, BASE, LIFETIME);
    }

    @Test(priority = 10, dataProvider = "tierLevel")
    public void downgradeRcTierLevelPlatinumAndGold(ProgramLevel tierLevel)
    {
        rcSummary.clickEdit();
        rcSummary.populateTierLevelWithExpirationAndNoReason(tierLevel, LIFETIME);
        rcSummary.clickSave();
        verifySuccess(SUCCESS_MESSAGE);

        rcSummary.verifyLifetimeTierLevel(ProgramStatus.OPEN, tierLevel);
    }

    @Test(priority = 20)
    public void downgradeRcTierLevelToClub()
    {
        rcSummary.setTierLevelWithoutExpiration(CLUB, BASE, verifySuccess(SUCCESS_MESSAGE));

        rcSummary.verify(ProgramStatus.OPEN, CLUB);
    }

    @DataProvider(name = "tierLevel")
    public Object[][] tierLevelProvider()
    {
        return new Object[][] { { PLTN }, { GOLD } };
    }
}
