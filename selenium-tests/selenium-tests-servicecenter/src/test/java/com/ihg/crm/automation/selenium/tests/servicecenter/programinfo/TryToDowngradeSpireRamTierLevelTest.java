package com.ihg.crm.automation.selenium.tests.servicecenter.programinfo;

import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyError;
import static com.ihg.automation.selenium.common.types.program.AmbassadorLevel.RAM;
import static com.ihg.automation.selenium.common.types.program.AmbassadorLevelReason.SYSTEM_ERROR;
import static com.ihg.automation.selenium.common.types.program.RewardClubLevel.CLUB;
import static com.ihg.automation.selenium.common.types.program.RewardClubLevel.GOLD;
import static com.ihg.automation.selenium.common.types.program.RewardClubLevel.PLTN;
import static com.ihg.automation.selenium.common.types.program.RewardClubLevelReason.BASE;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.member.AmbassadorOfferCode;
import com.ihg.automation.selenium.common.member.AmbassadorUtils;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.common.types.program.ProgramLevel;
import com.ihg.automation.selenium.common.types.program.TierLevelExpiration;
import com.ihg.automation.selenium.servicecenter.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.servicecenter.pages.programs.RenewExtendProgramSummary;
import com.ihg.automation.selenium.servicecenter.pages.programs.amb.AmbassadorPage;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.RewardClubPage;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.RewardClubSummary;
import com.ihg.crm.automation.selenium.tests.servicecenter.LoginLogout;

@Test(groups = "karma")
public class TryToDowngradeSpireRamTierLevelTest extends LoginLogout
{
    private static final String RAM_DOWNGRADE_ERROR = "Royal Ambassadors cannot be downgraded below Spire Elite.";

    private Member member = new Member();
    private AmbassadorPage ambassadorPage = new AmbassadorPage();
    private RewardClubPage rewardClubPage = new RewardClubPage();
    private RewardClubSummary summary = rewardClubPage.getSummary();

    @BeforeClass
    public void beforeClass()
    {
        member.setPersonalInfo(MemberPopulateHelper.getSimplePersonalInfo());
        member.addAddress(MemberPopulateHelper.getUnitedStatesAddress());
        member.addProgram(Program.AMB);
        member.setAmbassadorOfferCode(AmbassadorOfferCode.CHSRO);
        member.setAmbassadorAmount(AmbassadorUtils.AMBASSADOR_COMP_PROMO_$0);

        login();

        new EnrollmentPage().enroll(member);

        ambassadorPage.goTo();
        RenewExtendProgramSummary ambSummary = ambassadorPage.getSummary();
        ambSummary.setTierLevelWithoutExpiration(RAM, SYSTEM_ERROR);
    }

    @Test(priority = 10, dataProvider = "tierLevel")
    public void tryToDowngradeRamTierLevel(ProgramLevel tierLevel)
    {
        rewardClubPage.goTo();

        summary.setTierLevelWithExpiration(tierLevel, BASE, TierLevelExpiration.LIFETIME,
                verifyError(RAM_DOWNGRADE_ERROR));
        summary.clickCancel();
    }

    @Test(priority = 20)
    public void tryToDowngradeRamTierLevelToClub()
    {
        summary.setTierLevelWithoutExpiration(CLUB, BASE, verifyError(RAM_DOWNGRADE_ERROR));
    }

    @DataProvider(name = "tierLevel")
    public Object[][] tierLevelProvider()
    {
        return new Object[][] { { PLTN }, { GOLD } };
    }
}
