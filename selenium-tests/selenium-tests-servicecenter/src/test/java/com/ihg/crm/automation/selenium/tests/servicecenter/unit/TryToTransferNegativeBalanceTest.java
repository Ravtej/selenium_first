package com.ihg.crm.automation.selenium.tests.servicecenter.unit;

import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyError;
import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyNoError;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.assertThat;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.Constant.RC_POINTS;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasText;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasTextAsInt;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.isDisplayedWithWait;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.hamcrest.Matchers;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.types.GoodwillReason;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.gwt.components.dialog.ConfirmDialog;
import com.ihg.automation.selenium.servicecenter.pages.GuestSearch;
import com.ihg.automation.selenium.servicecenter.pages.LeftPanel;
import com.ihg.automation.selenium.servicecenter.pages.unit.GoodwillPopUp;
import com.ihg.automation.selenium.servicecenter.pages.unit.TransferUnitsPopUp;

public class TryToTransferNegativeBalanceTest extends UnitsCommon
{
    public static final String SELECT_MEMBER = "SELECT MBRSHP_ID FROM DGST.MBRSHP WHERE" //
            + " LYTY_PGM_CD = 'PC'"//
            + " AND MBRSHP_STAT_CD = 'O'"//
            + " AND LST_UPDT_TS < (SYSDATE - 5)"//
            + " AND CURRENT_BAL_UNIT_AMT < 0"//
            + " AND ROWNUM < 2";

    public static final String SELECT_MEMBER_TRANSFER_TO = "SELECT MBRSHP_ID FROM DGST.MBRSHP WHERE"//
            + " LYTY_PGM_CD = 'PC'"//
            + " AND MBRSHP_STAT_CD = 'O'"//
            + " AND LST_UPDT_TS < (SYSDATE - 5)"//
            + " AND MBRSHP_ID <> %s"//
            + " AND ROWNUM < 2";

    private String recipientMemberNumber;

    @BeforeClass
    public void beforeClass()
    {
        login();

        String originatorMemberNumber;
        List<String> list = jdbcTemplate.queryForList(SELECT_MEMBER, String.class);

        if (CollectionUtils.isNotEmpty(list))
        {
            originatorMemberNumber = list.get(0);
            new GuestSearch().byMemberNumber(originatorMemberNumber);
        }
        else
        {
            originatorMemberNumber = enrollMember().getRCProgramId();
            GoodwillPopUp goodwillPopUp = new GoodwillPopUp();
            goodwillPopUp.goTo();
            goodwillPopUp.populate("-1000", RC_POINTS, GoodwillReason.AGENT_ERROR);
            goodwillPopUp.clickSubmitNoError();
            assertThat(goodwillPopUp, isDisplayedWithWait());

            new ConfirmDialog().clickYes(verifyNoError());
        }

        LeftPanel leftPanel = new LeftPanel();
        assertThat(leftPanel.getCustomerInfoPanel().getMemberNumber(Program.RC), hasText(originatorMemberNumber));
        assertThat(leftPanel.getProgramInfoPanel().getPrograms().getRewardClubProgram().getPointsBalance(),
                hasTextAsInt(Matchers.lessThan(0)));

        recipientMemberNumber = jdbcTemplate
                .queryForMap(String.format(SELECT_MEMBER_TRANSFER_TO, originatorMemberNumber)).get("MBRSHP_ID")
                .toString();

    }

    @Test
    public void tryToTransferNegativeBalance()
    {
        TransferUnitsPopUp transUnitsPopUp = new TransferUnitsPopUp();
        transUnitsPopUp.goTo();
        transUnitsPopUp.getMembershipId().typeAndWait(recipientMemberNumber);

        transUnitsPopUp.getBalanceTransfer().check();
        transUnitsPopUp
                .clickSubmit(verifyError("[UNITS.INCORRECT_AMOUNT] Request must be for positive Loyalty Units only"));

        verifyThat(transUnitsPopUp, displayed(true));
    }

}
