package com.ihg.crm.automation.selenium.tests.servicecenter.deposit;

import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyError;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.event.EventTransactionType.Deposit.GOODWILL_DEPOSIT;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.automation.selenium.matchers.component.ViewableMatcher.hasTextInView;
import static com.ihg.automation.selenium.servicecenter.pages.deposit.CreateDepositPopUp.CUSTOMER_IS_NOT_ELIGIBLE;
import static org.hamcrest.core.StringStartsWith.startsWith;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.Constant;
import com.ihg.automation.selenium.common.deposit.Deposit;
import com.ihg.automation.selenium.common.event.AllEventsRowConverter;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.common.types.program.RewardClubLevel;
import com.ihg.automation.selenium.common.types.program.RewardClubLevelReason;
import com.ihg.automation.selenium.servicecenter.pages.LeftPanel;
import com.ihg.automation.selenium.servicecenter.pages.annual.AnnualActivityPage;
import com.ihg.automation.selenium.servicecenter.pages.deposit.CreateDepositPopUp;
import com.ihg.automation.selenium.servicecenter.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsPage;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.RewardClubPage;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.RewardClubSummary;
import com.ihg.crm.automation.selenium.tests.servicecenter.LoginLogout;

public class DepositEligibilityByProgramLevelTest extends LoginLogout
{
    private Member member = new Member();
    private Deposit testDeposit = new Deposit();

    @BeforeClass
    public void beforeClass()
    {
        member.addProgram(Program.RC);
        member.setPersonalInfo(MemberPopulateHelper.getSimplePersonalInfo());
        member.addAddress(MemberPopulateHelper.getUnitedStatesAddress());

        testDeposit.setTransactionId("72HRGW");
        testDeposit.setTransactionType(GOODWILL_DEPOSIT);
        testDeposit.setTransactionName("PLATINUM, SPIRE GUARANTEE GOODWILL");
        testDeposit.setTransactionDescription("PLATINUM, SPIRE GUARANTEE GOODWILL");
        testDeposit.setLoyaltyUnitsType(Constant.RC_POINTS);

        login();
        member = new AnnualActivityPage().captureCounters(new EnrollmentPage().enroll(member), jdbcTemplate);
        new LeftPanel().verifyRCLevel(RewardClubLevel.CLUB);
    }

    @Test
    public void createDepositWithInvalidData()
    {
        CreateDepositPopUp createDepositPopUp = new CreateDepositPopUp();
        createDepositPopUp.goTo();
        createDepositPopUp.setDepositData(testDeposit);
        createDepositPopUp.clickCreateDeposit(verifyError(CUSTOMER_IS_NOT_ELIGIBLE));
        verifyThat(createDepositPopUp, displayed(true));

        createDepositPopUp.close();
    }

    @Test(dependsOnMethods = { "createDepositWithInvalidData" }, alwaysRun = true)
    public void verifyInitialTierLevel()
    {
        new LeftPanel().verifyRCLevel(RewardClubLevel.CLUB);
    }

    @Test(dependsOnMethods = { "verifyInitialTierLevel" }, alwaysRun = true)
    public void verifyInitialActivityCounters()
    {
        new AnnualActivityPage().verifyCounters(member);
    }

    @Test(dependsOnMethods = { "verifyInitialActivityCounters" }, alwaysRun = true)
    public void verifyIncentiveEventAbsence()
    {
        AllEventsPage allEventsPage = new AllEventsPage();
        allEventsPage.goTo();
        verifyThat(allEventsPage.getGrid().getRow(testDeposit.getTransactionType()), displayed(false));
    }

    @Test(dependsOnMethods = { "verifyIncentiveEventAbsence" }, alwaysRun = true)
    public void updateMemberTierLevel()
    {
        RewardClubPage rewardClubPage = new RewardClubPage();
        rewardClubPage.goTo();
        RewardClubSummary rewardClubSummary = rewardClubPage.getSummary();
        rewardClubSummary.setTierLevelWithExpiration(RewardClubLevel.PLTN, RewardClubLevelReason.AMBASSADOR);

        verifyThat(rewardClubSummary.getTierLevel(), hasTextInView(startsWith(RewardClubLevel.PLTN.getValue())));
    }

    @Test(dependsOnMethods = { "updateMemberTierLevel" }, alwaysRun = true)
    public void createDeposit()
    {
        new CreateDepositPopUp().createDeposit(testDeposit);
    }

    @Test(dependsOnMethods = { "createDeposit" }, alwaysRun = true)
    public void verifyResultingIncentiveEvent()
    {
        AllEventsPage allEventsPage = new AllEventsPage();
        allEventsPage.goTo();
        allEventsPage.getGrid().getRow(testDeposit.getTransactionType())
                .verify(AllEventsRowConverter.convert(testDeposit));
    }

    @Test(dependsOnMethods = { "verifyResultingIncentiveEvent" }, alwaysRun = true)
    public void verifyResultingTierLevel()
    {
        new LeftPanel().verifyRCLevel(RewardClubLevel.PLTN);
    }

    @Test(dependsOnMethods = { "verifyResultingTierLevel" }, alwaysRun = true)
    public void verifyResultingActivityCounters()
    {
        member.getAnnualActivity().setTotalUnits(20000);
        member.getLifetimeActivity().setBonusUnits(20000);
        member.getLifetimeActivity().setTotalUnits(20000);
        member.getLifetimeActivity().setCurrentBalance(20000);

        new AnnualActivityPage().verifyCounters(member);
    }
}
