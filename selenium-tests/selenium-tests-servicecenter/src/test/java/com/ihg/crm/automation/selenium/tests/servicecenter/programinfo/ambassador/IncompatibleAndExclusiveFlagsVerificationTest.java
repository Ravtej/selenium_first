package com.ihg.crm.automation.selenium.tests.servicecenter.programinfo.ambassador;

import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyError;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasTextNoWait;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.core.IsNot.not;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.CustomerInfoFlags;
import com.ihg.automation.selenium.common.member.AmbassadorOfferCode;
import com.ihg.automation.selenium.common.member.AmbassadorUtils;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.common.types.program.TierLevelFlag;
import com.ihg.automation.selenium.servicecenter.pages.LeftPanel;
import com.ihg.automation.selenium.servicecenter.pages.account.AccountInfoPage;
import com.ihg.automation.selenium.servicecenter.pages.account.Flags;
import com.ihg.automation.selenium.servicecenter.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.servicecenter.pages.programs.RenewExtendProgramSummary;
import com.ihg.automation.selenium.servicecenter.pages.programs.amb.AmbassadorPage;
import com.ihg.crm.automation.selenium.tests.servicecenter.LoginLogout;

public class IncompatibleAndExclusiveFlagsVerificationTest extends LoginLogout
{
    private Member member = new Member();
    private AmbassadorPage ambassadorPage = new AmbassadorPage();
    private List<String> exclusiveFlags = Arrays.asList(TierLevelFlag.AMEX_CENTURION.getValue(),
            TierLevelFlag.CHASE_AFFLUENT_REWARDS.getValue());
    private List<String> incompatibleFlags = Arrays.asList(TierLevelFlag.AUTO_RENEW.getValue(),
            TierLevelFlag.CORPORATE.getValue());
    private AccountInfoPage accountInfoPage = new AccountInfoPage();

    @BeforeClass
    public void beforeClass()
    {
        member.setPersonalInfo(MemberPopulateHelper.getSimplePersonalInfo());
        member.addAddress(MemberPopulateHelper.getUnitedStatesAddress());
        member.addProgram(Program.AMB);
        member.setAmbassadorOfferCode(AmbassadorOfferCode.CHSRO);
        member.setAmbassadorAmount(AmbassadorUtils.AMBASSADOR_COMP_PROMO_$0);

        login();
        member = new EnrollmentPage().enroll(member);
    }

    @DataProvider(name = "verifyInvalidFlagsUpdateProvider")
    public Object[][] verifyInvalidFlagsUpdateProvider()
    {
        return new Object[][] { { new HashSet<String>(exclusiveFlags), "Selected Flags are mutually exclusive." },
                { new HashSet<String>(incompatibleFlags), "Current flags combination is incompatible with FMDS." } };
    }

    @DataProvider(name = "verifyFlagsProvider")
    public Object[][] verifyFlagsProvider()
    {
        return new Object[][] { { exclusiveFlags }, { incompatibleFlags } };
    }

    @Test(dataProvider = "verifyInvalidFlagsUpdateProvider")
    public void verifyInvalidFlagsUpdate(Set<String> flags, String warning)
    {
        ambassadorPage.goTo();
        RenewExtendProgramSummary ambSummary = ambassadorPage.getSummary();
        ambSummary.clickEdit();
        ambSummary.getFlags().select(flags);
        ambSummary.clickSave(verifyError(warning));
        ambSummary.clickCancel();
    }

    @Test(dataProvider = "verifyFlagsProvider", dependsOnMethods = { "verifyInvalidFlagsUpdate" }, alwaysRun = true)
    public void verifyLeftPanelFlags(List<String> flags)
    {
        CustomerInfoFlags customerInfoFlags = new LeftPanel().getCustomerInfoPanel().getFlags();
        for (String flag : flags)
        {
            verifyThat(customerInfoFlags, hasTextNoWait(not(containsString(flag))));
        }
    }

    @Test(dataProvider = "verifyFlagsProvider", dependsOnMethods = { "verifyLeftPanelFlags" }, alwaysRun = true)
    public void verifyAccountInformationFlags(List<String> flags)
    {
        accountInfoPage.goTo();
        Flags flagsTab = accountInfoPage.getFlags();

        for (String flag : flags)
        {
            verifyThat(flagsTab, hasText(not(containsString(flag))));
        }
    }
}
