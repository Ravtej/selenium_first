package com.ihg.crm.automation.selenium.tests.servicecenter.hotel;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.hotel.HotelCertificateStatus.INITIATED;
import static com.ihg.automation.selenium.common.hotel.HotelCertificateStatus.PAID;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.automation.selenium.matchers.component.GridSize.size;

import org.apache.commons.lang3.StringUtils;
import org.hamcrest.Matchers;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.common.DateUtils;
import com.ihg.automation.common.RandomUtils;
import com.ihg.automation.selenium.common.hotel.HotelCertificate;
import com.ihg.automation.selenium.common.types.Currency;
import com.ihg.automation.selenium.servicecenter.pages.hotel.certificate.HotelCertificatePage;
import com.ihg.automation.selenium.servicecenter.pages.hotel.certificate.HotelCertificateSearchGrid;
import com.ihg.crm.automation.selenium.tests.servicecenter.LoginLogout;

public class HotelCertificateOptionalCriteriaSearchTest extends LoginLogout
{
    public static final String HOTEL_CODE = "ATACA";
    public static final String CERTIFICATE_NUMBER = RandomUtils.getRandomNumber(11);
    public static final String CONFIRMATION_NUMBER = RandomUtils.getRandomNumber(8);
    public static final String CHECK_IN_DATE = DateUtils.getDateBackward(1);

    private HotelCertificatePage hotelCertificatePage = new HotelCertificatePage();

    private HotelCertificate hotelCertificate;

    @BeforeClass
    public void beforeClass()
    {
        hotelCertificate = new HotelCertificate();
        hotelCertificate.setHotelCode(HOTEL_CODE);
        hotelCertificate.setCheckInDate(CHECK_IN_DATE);
        hotelCertificate.setItemId("HQ250");
        hotelCertificate.setCertificateNumber(CERTIFICATE_NUMBER);
        hotelCertificate.setDescriptionGuestName("auto test certificate " + CERTIFICATE_NUMBER);
        hotelCertificate.setConfirmationNumber(CONFIRMATION_NUMBER);
        hotelCertificate.setReimbursementAmount("100.00");
        hotelCertificate.setBillingEntity("CORPC");
        hotelCertificate.setCurrency(Currency.USD);
        hotelCertificate.setStatus(INITIATED);

        login();
        hotelCertificatePage.goTo();
        hotelCertificatePage.createCertificate(hotelCertificate);
    }

    @Test
    public void searchByConfirmationAndCheckIn()
    {
        hotelCertificatePage.search(HOTEL_CODE, null, CONFIRMATION_NUMBER, null, null, true, false, CHECK_IN_DATE,
                null);

        HotelCertificateSearchGrid grid = hotelCertificatePage.getGrid();
        verifyThat(grid, size(1));
        verifyThat(grid.getRow(hotelCertificate.getCertificateNumber()), displayed(true));
    }

    @Test
    public void searchByConfirmationCheckInAndStatus()
    {
        hotelCertificatePage.search(HOTEL_CODE, null, CONFIRMATION_NUMBER, null, INITIATED, true, false, CHECK_IN_DATE,
                null);

        HotelCertificateSearchGrid grid = hotelCertificatePage.getGrid();
        verifyThat(grid, size(1));
        verifyThat(grid.getRow(hotelCertificate.getCertificateNumber()), displayed(true));
    }

    @Test
    public void trySearchByConfirmationAndInvalidStatus()
    {
        hotelCertificatePage.search(HOTEL_CODE, null, CONFIRMATION_NUMBER, null, PAID, true, false, CHECK_IN_DATE,
                null);
        verifyThat(hotelCertificatePage.getGrid(), size(0));
    }

    @Test
    public void searchByCertificateAndCheckIn()
    {
        hotelCertificatePage.search(HOTEL_CODE, CERTIFICATE_NUMBER, null, null, null, true, false, CHECK_IN_DATE, null);

        HotelCertificateSearchGrid grid = hotelCertificatePage.getGrid();
        verifyThat(grid, size(1));
        verifyThat(grid.getRow(hotelCertificate.getCertificateNumber()), displayed(true));
    }

    @Test
    public void searchByCertificateNumberLeadingDigit()
    {
        hotelCertificatePage.search(HOTEL_CODE, StringUtils.substring(CERTIFICATE_NUMBER, 0, 9), null, null, null, true,
                false, CHECK_IN_DATE, null);

        HotelCertificateSearchGrid grid = hotelCertificatePage.getGrid();
        verifyThat(grid, size(1));
        verifyThat(grid.getRow(hotelCertificate.getCertificateNumber()), displayed(true));
    }

    @Test
    public void searchByItemId()
    {
        hotelCertificatePage.search(HOTEL_CODE, null, null, hotelCertificate.getItemId(), null, true, false,
                CHECK_IN_DATE, null);

        HotelCertificateSearchGrid grid = hotelCertificatePage.getGrid();
        verifyThat(grid, size(Matchers.greaterThanOrEqualTo(1)));
        verifyThat(grid.getRow(hotelCertificate.getCertificateNumber()), displayed(true));
    }

}
