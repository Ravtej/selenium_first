package com.ihg.crm.automation.selenium.tests.servicecenter.thresholder;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.event.EventTransactionType.TierLevel.UPGRADE_ACHIEVE;
import static com.ihg.automation.selenium.common.event.EventTransactionType.TierLevel.UPGRADE_MAINTAIN;
import static com.ihg.automation.selenium.common.types.program.ProgramStatus.OPEN;
import static com.ihg.automation.selenium.common.types.program.RewardClubLevel.CLUB;
import static com.ihg.automation.selenium.common.types.program.RewardClubLevel.SPRE;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasTextAsInt;
import static com.ihg.automation.selenium.matchers.component.GridSize.size;
import static org.hamcrest.Matchers.not;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.stay.Stay;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.common.types.program.RewardClubLevel;
import com.ihg.automation.selenium.servicecenter.pages.LeftPanel;
import com.ihg.automation.selenium.servicecenter.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsPage;
import com.ihg.automation.selenium.servicecenter.pages.event.stay.StayEventsPage;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.RewardClubPage;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.RewardClubSummary;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.TierLevelBenefitsDetailsPopUp;
import com.ihg.automation.selenium.tests.common.RulesDBUtils;
import com.ihg.crm.automation.selenium.tests.servicecenter.LoginLogout;

public class AchieveMaintainSpireByPointsTest extends LoginLogout
{
    private Stay stayInPreviousYear = new Stay();
    private Stay stayInCurrentYear = new Stay();
    private static int pointsToMaintainSpire;
    private static int pointsToAchieveSpirePrevious;

    @BeforeClass
    public void before()
    {
        RulesDBUtils rulesDBUtils = new RulesDBUtils(jdbcTemplate);
        pointsToMaintainSpire = rulesDBUtils.getCurrentYearRulePoints(SPRE, SPRE);
        pointsToAchieveSpirePrevious = rulesDBUtils.getPreviousYearRulePoints(CLUB, SPRE);

        stayInPreviousYear.setHotelCode("CEQHA");
        stayInPreviousYear.setCheckInOutByNightsInPreviousYear(1);
        stayInPreviousYear.setUsdAvgRoomRateByPoints(pointsToAchieveSpirePrevious);
        stayInPreviousYear.setRateCode("Test");

        stayInCurrentYear.setHotelCode("CEQHA");
        stayInCurrentYear.setCheckInOutByNights(1);
        stayInCurrentYear.setUsdAvgRoomRateByPoints(pointsToMaintainSpire);
        stayInCurrentYear.setRateCode("Test");

        Member member = new Member();
        member.addProgram(Program.RC);
        member.setPersonalInfo(MemberPopulateHelper.getSimplePersonalInfo());
        member.addAddress(MemberPopulateHelper.getUnitedStatesAddress());

        login();

        new EnrollmentPage().enroll(member);

        new LeftPanel().verifyRCLevel(CLUB);
    }

    @Test
    public void achieveSpireTierLevel()
    {
        new StayEventsPage().createStay(stayInPreviousYear);
        new LeftPanel().verifyRCLevel(RewardClubLevel.SPRE);

        RewardClubPage rewardClubPage = new RewardClubPage();
        rewardClubPage.goTo();

        rewardClubPage.getPreviousYearAnnualActivities().getPointsRow()
                .verify(hasTextAsInt(pointsToAchieveSpirePrevious), displayed(false));
        rewardClubPage.getCurrentYearAnnualActivities().getPointsRow().verify(hasTextAsInt(0), displayed(false),
                hasTextAsInt(pointsToMaintainSpire));

        AllEventsPage allEventsPage = new AllEventsPage();
        allEventsPage.goTo();

        verifyThat(allEventsPage.getGrid().getRow(UPGRADE_ACHIEVE, "RC - SPIRE"), displayed(true));
    }

    @Test(dependsOnMethods = { "achieveSpireTierLevel" }, alwaysRun = true)
    public void maintainSpireTierLevel()
    {
        new StayEventsPage().createStay(stayInCurrentYear);
        RewardClubPage rewardClubPage = new RewardClubPage();
        rewardClubPage.goTo();

        RewardClubSummary summary = rewardClubPage.getSummary();
        summary.verify(OPEN, SPRE, 1);

        TierLevelBenefitsDetailsPopUp tierLevelBenefitsDetailsPopUp = summary.openTierLevelBenefitsDetailsPopUp();
        verifyThat(tierLevelBenefitsDetailsPopUp.getBenefitsGrid(), size(not(0)));
        tierLevelBenefitsDetailsPopUp.close();

        rewardClubPage.getCurrentYearAnnualActivities().getPointsRow()
                .verifyMaintainIsAchieved(hasTextAsInt(pointsToMaintainSpire), displayed(false));

        AllEventsPage allEventsPage = new AllEventsPage();
        allEventsPage.goTo();
        verifyThat(allEventsPage.getGrid().getRow(UPGRADE_MAINTAIN, "RC - SPIRE"), displayed(true));
    }
}
