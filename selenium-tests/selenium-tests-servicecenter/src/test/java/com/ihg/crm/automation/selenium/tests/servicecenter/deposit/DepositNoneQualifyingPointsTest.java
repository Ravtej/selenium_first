package com.ihg.crm.automation.selenium.tests.servicecenter.deposit;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.event.EventTransactionType.Deposit.CO_PARTNER;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.enabled;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;

import org.hamcrest.core.AllOf;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.common.DateUtils;
import com.ihg.automation.common.RandomUtils;
import com.ihg.automation.selenium.common.Constant;
import com.ihg.automation.selenium.common.deposit.Deposit;
import com.ihg.automation.selenium.common.event.AllEventsRowConverter;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.types.Partner;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.common.types.program.ProgramStatus;
import com.ihg.automation.selenium.common.types.program.RewardClubLevel;
import com.ihg.automation.selenium.servicecenter.pages.LeftPanel;
import com.ihg.automation.selenium.servicecenter.pages.annual.AnnualActivityPage;
import com.ihg.automation.selenium.servicecenter.pages.deposit.CreateDepositPopUp;
import com.ihg.automation.selenium.servicecenter.pages.deposit.DepositDetailsPopUp;
import com.ihg.automation.selenium.servicecenter.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsGridRow;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsPage;
import com.ihg.automation.selenium.servicecenter.pages.event.copartner.CoPartnerEventGridRow;
import com.ihg.automation.selenium.servicecenter.pages.event.copartner.CoPartnerEventsPage;
import com.ihg.automation.selenium.servicecenter.pages.event.deposit.DepositGridRowContainer;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.RewardClubPage;
import com.ihg.crm.automation.selenium.tests.servicecenter.LoginLogout;

public class DepositNoneQualifyingPointsTest extends LoginLogout
{
    private DepositGridRowContainer rowContainer;
    private DepositGridRowContainer depositGridRowContainer;

    private Member member = new Member();
    private Deposit testDeposit = new Deposit();

    @BeforeClass
    public void beforeClass()
    {
        member.addProgram(Program.RC);
        member.setPersonalInfo(MemberPopulateHelper.getSimplePersonalInfo());
        member.addAddress(MemberPopulateHelper.getUnitedStatesAddress());

        testDeposit.setTransactionId("CO130K");
        testDeposit.setTransactionType(CO_PARTNER);
        testDeposit.setTransactionName("Capital One - 30K for sign up");
        testDeposit.setTransactionDescription("Members get 30K points for signing up with Capital One");
        testDeposit.setHotel("ATLCP");
        testDeposit.setCheckInDate(DateUtils.getDateBackward(2));
        testDeposit.setCheckOutDate(DateUtils.getDateBackward(2));
        testDeposit.setPartnerTransactionDate(DateUtils.getDateBackward(1));
        testDeposit.setPartnerComment("SC UI test automation" + DateUtils.now());
        testDeposit.setOrderTrackingNumber(RandomUtils.getRandomNumber(8));
        testDeposit.setLoyaltyUnitsAmount("30000");
        testDeposit.setLoyaltyUnitsType(Constant.RC_POINTS);
        testDeposit.setCoPartner(Partner.CPO);
        testDeposit.setBillingTo(Partner.CPO);

        login();
        member = new AnnualActivityPage().captureCounters(new EnrollmentPage().enroll(member), jdbcTemplate);
        new LeftPanel().verifyRCLevel(RewardClubLevel.CLUB);
    }

    @Test
    public void verifyCreateDepositDialog()
    {
        CreateDepositPopUp createDepositPopUp = new CreateDepositPopUp();
        createDepositPopUp.goTo();

        createDepositPopUp.getTransactionId().typeAndWait(testDeposit.getTransactionId());
        createDepositPopUp.verifyTransactionInfo(testDeposit);
        createDepositPopUp.getLoyaltyUnitsSelect().select(Constant.RC_POINTS);
        verifyThat(createDepositPopUp.getLoyaltyUnitsInput(),
                AllOf.allOf(enabled(false), hasText(testDeposit.getLoyaltyUnitsAmount())));

        createDepositPopUp.close();
    }

    @Test(dependsOnMethods = { "verifyCreateDepositDialog" }, alwaysRun = true)
    public void createDeposit()
    {
        new CreateDepositPopUp().createDeposit(testDeposit);
    }

    @Test(dependsOnMethods = { "createDeposit" })
    public void verifyCoPartnerEventOnCoPartnerPage()
    {
        CoPartnerEventGridRow row = new CoPartnerEventsPage().getGrid().getRow(1);
        row.verify(testDeposit);
        rowContainer = row.expand(DepositGridRowContainer.class);
        rowContainer.verify(testDeposit, helper);
    }

    @Test(dependsOnMethods = { "verifyCoPartnerEventOnCoPartnerPage" }, alwaysRun = true)
    public void verifyDepositDetailsFromCoPartnerPage()
    {
        rowContainer.clickDetails();

        verifyDepositDetails();
    }

    @Test(dependsOnMethods = { "verifyDepositDetailsFromCoPartnerPage" }, alwaysRun = true)
    public void closeCoPartnersDepositDetailsPopUp()
    {
        new DepositDetailsPopUp().close();
    }

    @Test(dependsOnMethods = { "closeCoPartnersDepositDetailsPopUp" }, alwaysRun = true)
    public void verifyCoPartnerEventWithDetailsInGrid()
    {
        AllEventsPage allEventsPage = new AllEventsPage();
        allEventsPage.goTo();
        AllEventsGridRow row = allEventsPage.getGrid().getRow(testDeposit.getTransactionType());
        row.verify(AllEventsRowConverter.convert(testDeposit));
        depositGridRowContainer = row.expand(DepositGridRowContainer.class);
        depositGridRowContainer.verify(testDeposit, helper);
    }

    @Test(dependsOnMethods = { "verifyCoPartnerEventWithDetailsInGrid" })
    public void verifyDepositDetailsFromAllEventsPage()
    {
        depositGridRowContainer.clickDetails();

        verifyDepositDetails();
    }

    @Test(dependsOnMethods = { "verifyDepositDetailsFromAllEventsPage" }, alwaysRun = true)
    public void closeAllEventsCoPartnersDepositDetailsPopUp()
    {
        new DepositDetailsPopUp().close();
    }

    @Test(dependsOnMethods = { "closeAllEventsCoPartnersDepositDetailsPopUp" }, alwaysRun = true)
    public void verifyResultingTierLevelAndQualifyingPoints()
    {
        RewardClubPage rewardClubPage = new RewardClubPage();
        rewardClubPage.goTo();
        rewardClubPage.getSummary().verify(ProgramStatus.OPEN, RewardClubLevel.CLUB);
        rewardClubPage.getCurrentYearAnnualActivities().getPointsRow().verify("0", achieveGoldPoints);
    }

    @Test(dependsOnMethods = { "verifyResultingTierLevelAndQualifyingPoints" }, alwaysRun = true)
    public void verifyResultingLeftPanelProgramInformation()
    {
        new LeftPanel().getProgramInfoPanel().getPrograms().getRewardClubProgram().verify(RewardClubLevel.CLUB,
                "30,000");
    }

    @Test(dependsOnMethods = { "verifyResultingLeftPanelProgramInformation" }, alwaysRun = true)
    public void verifyResultingActivities()
    {
        member.getLifetimeActivity().setBonusUnits(30000);
        member.getLifetimeActivity().setTotalUnits(30000);
        member.getLifetimeActivity().setCurrentBalance(30000);
        member.getAnnualActivity().setTotalUnits(30000);

        new AnnualActivityPage().verifyCounters(member);
    }

    private void verifyDepositDetails()
    {
        DepositDetailsPopUp depositDetailsPopUp = new DepositDetailsPopUp();
        depositDetailsPopUp.getDepositDetailsTab().getDepositDetails().verify(testDeposit);
        depositDetailsPopUp.getEarningDetailsTab().verifyForBaseUnitsEvent("30,000", "0");
        depositDetailsPopUp.getBillingDetailsTab().verifyBaseUSDBilling(CO_PARTNER, "105", testDeposit.getBillingTo());
    }

}
