package com.ihg.crm.automation.selenium.tests.servicecenter.stay;

import static com.ihg.automation.common.DateUtils.getFormattedDate;
import static com.ihg.automation.common.MapUtils.queryResult;
import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.assertNoError;
import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifySuccess;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.assertNoErrors;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyNoErrors;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.Constant.NOT_AVAILABLE;
import static com.ihg.automation.selenium.common.Constant.RC_POINTS;
import static com.ihg.automation.selenium.common.UI.SC;
import static com.ihg.automation.selenium.common.event.EarningEventFactory.getBaseUnitsEvent;
import static com.ihg.automation.selenium.common.history.EventAuditConstant.CREATE;
import static com.ihg.automation.selenium.common.history.EventAuditConstant.CREATED;
import static com.ihg.automation.selenium.common.history.EventAuditConstant.NO;
import static com.ihg.automation.selenium.common.history.EventAuditConstant.YES;
import static com.ihg.automation.selenium.common.history.EventAuditConstant.ZERO_VALUE;
import static com.ihg.automation.selenium.common.history.EventAuditConstant.Stay.AVERAGE_ROOM_RATE;
import static com.ihg.automation.selenium.common.history.EventAuditConstant.Stay.CURRENCY;
import static com.ihg.automation.selenium.common.history.EventAuditConstant.Stay.EARNING_PREFERENCE;
import static com.ihg.automation.selenium.common.history.EventAuditConstant.Stay.ENROLLING_STAY_INDICATOR;
import static com.ihg.automation.selenium.common.history.EventAuditConstant.Stay.EVENT_TYPE;
import static com.ihg.automation.selenium.common.history.EventAuditConstant.Stay.FOLIO_NUMBER;
import static com.ihg.automation.selenium.common.history.EventAuditConstant.Stay.HOTEL;
import static com.ihg.automation.selenium.common.history.EventAuditConstant.Stay.OVERLAPPING_INDICATOR;
import static com.ihg.automation.selenium.common.history.EventAuditConstant.Stay.QUALIFYING_NIGHTS;
import static com.ihg.automation.selenium.common.history.EventAuditConstant.Stay.TRANSACTION_DATE;
import static com.ihg.automation.selenium.common.pages.CreditCardType.VISA;
import static com.ihg.automation.selenium.common.pages.PaymentType.CREDIT_CARD;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasDefault;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasEmptyText;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasTextAsDouble;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasTextAsInt;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static com.ihg.automation.selenium.matchers.component.ViewableMatcher.hasTextInView;
import static com.ihg.automation.selenium.servicecenter.pages.event.stay.StayEventsPage.STAY_SUCCESS_CREATED;
import static org.hamcrest.core.StringContains.containsString;

import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.ihg.automation.common.DateUtils;
import com.ihg.automation.selenium.common.ProgramInfoPanel;
import com.ihg.automation.selenium.common.Revenue;
import com.ihg.automation.selenium.common.RewardClubGridRow;
import com.ihg.automation.selenium.common.billing.EventBillingDetailsTab;
import com.ihg.automation.selenium.common.components.IndicatorSelect;
import com.ihg.automation.selenium.common.earning.EventEarningDetailsTab;
import com.ihg.automation.selenium.common.history.EventAuditConstant;
import com.ihg.automation.selenium.common.history.EventHistoryGridRow;
import com.ihg.automation.selenium.common.history.EventHistoryRecords;
import com.ihg.automation.selenium.common.history.EventHistoryTab;
import com.ihg.automation.selenium.common.member.AnnualActivity;
import com.ihg.automation.selenium.common.member.LifetimeActivity;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.pages.program.NightGridRowContainer;
import com.ihg.automation.selenium.common.pages.program.RewardClubTierLevelActivityGrid;
import com.ihg.automation.selenium.common.types.Currency;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.servicecenter.pages.annual.AnnualActivityPage;
import com.ihg.automation.selenium.servicecenter.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsPage;
import com.ihg.automation.selenium.servicecenter.pages.event.stay.AdjustStayPopUp;
import com.ihg.automation.selenium.servicecenter.pages.event.stay.CreateStayPopUp;
import com.ihg.automation.selenium.servicecenter.pages.event.stay.GuestInfo;
import com.ihg.automation.selenium.servicecenter.pages.event.stay.RevenueDetailsEdit;
import com.ihg.automation.selenium.servicecenter.pages.event.stay.RevenueDetailsView;
import com.ihg.automation.selenium.servicecenter.pages.event.stay.RevenueExtended;
import com.ihg.automation.selenium.servicecenter.pages.event.stay.StayBookingInfoBase;
import com.ihg.automation.selenium.servicecenter.pages.event.stay.StayBookingInfoEdit;
import com.ihg.automation.selenium.servicecenter.pages.event.stay.StayDetailsAdjust;
import com.ihg.automation.selenium.servicecenter.pages.event.stay.StayEventsPage;
import com.ihg.automation.selenium.servicecenter.pages.event.stay.StayEventsPage.StayEventsPageButtonBar;
import com.ihg.automation.selenium.servicecenter.pages.event.stay.StayGuestGridRowContainer;
import com.ihg.automation.selenium.servicecenter.pages.event.stay.StayInfoBase;
import com.ihg.automation.selenium.servicecenter.pages.event.stay.StayInfoCreate;
import com.ihg.automation.selenium.servicecenter.pages.event.stay.StayInfoEdit;
import com.ihg.automation.selenium.servicecenter.pages.event.stay.StayInfoView;
import com.ihg.automation.selenium.servicecenter.pages.event.stay.StaySearch;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.RewardClubPage;
import com.ihg.crm.automation.selenium.tests.servicecenter.LoginLogout;

public class CreateMissingStayTest extends LoginLogout
{
    private StayEventsPage stayPage = new StayEventsPage();
    private CreateStayPopUp createStay = new CreateStayPopUp();
    private AdjustStayPopUp adjustStayPopUp = new AdjustStayPopUp();

    private Member member = new Member();

    private static final String STAY_HOTEL = "ABQMB";
    private static final String HOTEL_CHAIN = "Candlewood Suites";
    private static final String BOOKING_SOURCE = "HOTEL";
    private static final String ROOM_TYPE = "DBL";
    private static final String RATE_CODE = "TEST";
    private static final String ROOM_NUMBER = "405";
    private static final int NIGHTS = 2;
    private static final int DAYS_SHIFT = -5;
    private static final String CHECK_IN = getFormattedDate(DAYS_SHIFT);
    private static final String CHECK_OUT = getFormattedDate(DAYS_SHIFT + NIGHTS);

    private static final int AVG_AMOUNT = 100;
    private static final int TOTAL_ROOM = AVG_AMOUNT * NIGHTS;

    private static final int FOOD = 25;
    private static final int BEVERAGE = 15;
    private static final int PHONE = 10;
    private static final int MEETING = 40;
    private static final int MANDATORY = 7;
    private static final int OTHER = -20;
    private static final int NO_REVENUE = 16;

    int TOTAL_QUALIFYING = TOTAL_ROOM + FOOD + BEVERAGE + PHONE + MANDATORY + OTHER;
    int TOTAL_REVENUE = TOTAL_QUALIFYING + MEETING + NO_REVENUE;
    int POINTS_FOR_STAY = 1185;
    String POINTS_FOR_STAY_STR = String.valueOf(POINTS_FOR_STAY);

    public static final String GUEST = "SELECT gm.ENTERPRISE_ID, m.DGST_MSTR_KEY FROM DGST.GST_MSTR gm "//
            + " JOIN DGST.MBRSHP m ON m.DGST_MSTR_KEY = gm.DGST_MSTR_KEY "//
            + " WHERE m.MBRSHP_ID =  '%s' ";

    @BeforeClass
    public void before()
    {
        member.addProgram(Program.RC);
        member.setPersonalInfo(MemberPopulateHelper.getSimplePersonalInfo());
        member.addAddress(MemberPopulateHelper.getUnitedStatesAddress());
        member.addPhone(MemberPopulateHelper.getPhone());
        member.addEmail(MemberPopulateHelper.getRandomEmail());

        login();

        new AnnualActivityPage().captureCounters(new EnrollmentPage().enroll(member), jdbcTemplate);
    }

    @Test
    public void goToStayEventsPage()
    {
        stayPage.goTo();
        assertNoErrors();
    }

    @Test(dependsOnMethods = { "goToStayEventsPage" }, alwaysRun = true)
    public void verifySearchFieldsDefaultValues()
    {
        StaySearch search = stayPage.getSearchFields();

        verifyThat(search.getGuestName(), hasText(member.getPersonalInfo().getName().getSurname()));
        verifyThat(search.getConfirmationNumber(), hasDefault());
        verifyThat(search.getCCNumber(), hasDefault());
        verifyThat(search.getHotel(), hasText("Hotel"));
        verifyThat(search.getHotelBrand(), hasDefault());
        verifyThat(search.getCountry(), hasDefault());
        verifyThat(search.getStayDate().getDate(), hasDefault());
        verifyThat(search.getStayDate().getRange(), hasDefault());
        verifyThat(search.getCity(), hasDefault());
        verifyThat(search.getState(), hasDefault());
    }

    @Test(dependsOnMethods = { "verifySearchFieldsDefaultValues" }, alwaysRun = true)
    public void verifyButtonsAvailability()
    {
        StayEventsPageButtonBar buttonBar = stayPage.getButtonBar();
        verifyThat(buttonBar.getShowStays(), displayed(true));
        verifyThat(buttonBar.getCreateStay(), displayed(true));
        verifyThat(buttonBar.getClearCriteria(), displayed(true));
        verifyThat(buttonBar.getSearch(), displayed(true));
    }

    @Test(dependsOnMethods = { "verifyButtonsAvailability" }, alwaysRun = true)
    public void verifyGridsAvailability()
    {
        verifyThat(stayPage.getGuestGrid(), displayed(true));
        verifyThat(stayPage.getUniverseStaysPanel(), displayed(false));
    }

    @Test(dependsOnMethods = { "verifyGridsAvailability" }, alwaysRun = true)
    public void openStayDetails()
    {
        stayPage.getButtonBar().clickCreateStay(assertNoError());
    }

    @Test(dependsOnMethods = { "openStayDetails" }, alwaysRun = true)
    public void verifyGuestDetails()
    {
        GuestInfo guest = createStay.getStayDetails().getGuestInfo();

        verifyThat(guest.getGuestName(), hasText(member.getPersonalInfo().getName().toString()));
        verifyThat(guest.getMemberName(), hasText(member.getPersonalInfo().getName().toString()));
        verifyThat(guest.getPhone(), hasText(StringUtils.substring(member.getPhones().get(0).getNumber(), 0, 15)));
        verifyThat(guest.getEmail(), hasText(member.getEmails().get(0).getEmail()));
        verifyThat(guest.getMemberNumber(), hasText(member.getRCProgramId()));
        verifyThat(guest.getProgram(), hasText(Program.RC.getCode()));
        verifyThat(guest.getAddress(), hasText(containsString(member.getAddresses().get(0).getLine1())));
    }

    @Test(dependsOnMethods = { "verifyGuestDetails" }, alwaysRun = true)
    public void verifyStayDetails()
    {
        StayInfoCreate stayInfo = createStay.getStayDetails().getStayInfo();

        verifyThat(stayInfo.getHotel(), hasEmptyText());
        verifyThat(stayInfo.getCheckIn(), hasDefault());
        verifyThat(stayInfo.getCheckOut(), hasDefault());
        verifyThat(stayInfo.getAvgRoomRate(), hasTextAsDouble(0.0));
        verifyThat(stayInfo.getRateCode(), hasDefault());
        verifyThat(stayInfo.getCorpAcctNumber(), hasDefault());
        verifyThat(stayInfo.getEnrollingStay(), hasText(IndicatorSelect.NO));
        verifyThat(stayInfo.getHotelCurrency(), hasText(Currency.USD.getCodeWithValue()));

        verifyThat(stayInfo.getRoomType(), hasDefault());
        verifyThat(stayInfo.getRoomNumber(), hasDefault());
        verifyThat(stayInfo.getIATANumber(), hasDefault());

        verifyThat(stayInfo.getNights(), hasTextAsInt(0));
        verifyThat(stayInfo.getEarningPreference(), hasText(RC_POINTS));

        verifyThat(stayInfo.getConfirmationNumber(), hasDefault());
        verifyThat(stayInfo.getOverlappingStay(), hasDefault());
        verifyThat(stayInfo.getFolioNumber(), hasDefault());
        verifyThat(stayInfo.getQualifiedNights(), hasDefault());
    }

    @Test(dependsOnMethods = { "verifyStayDetails" }, alwaysRun = true)
    public void verifyBookingDetails()
    {
        StayBookingInfoEdit bookingInfo = createStay.getStayDetails().getBookingInfo();

        verifyThat(bookingInfo.getBookingSource(), hasDefault());
        verifyThat(bookingInfo.getStayPayment(), hasDefault());

        verifyThat(bookingInfo.getBookingDate(), hasDefault());
        verifyThat(bookingInfo.getDataSource(), hasText("SC"));
    }

    @DataProvider(name = "extendedRevenue")
    private Object[][] extendedRevenue()
    {
        RevenueDetailsEdit revenueDetails = createStay.getStayDetails().getRevenueDetails();

        return new Object[][] { { revenueDetails.getTotalRoom() }, //
                { revenueDetails.getFood() }, //
                { revenueDetails.getBeverage() }, //
                { revenueDetails.getPhone() }, //
                { revenueDetails.getMeeting() }, //
                { revenueDetails.getMandatoryRevenue() }, //
                { revenueDetails.getOtherRevenue() }, //
                { revenueDetails.getNoRevenue() } };
    }

    @Test(dataProvider = "extendedRevenue", dependsOnMethods = { "verifyBookingDetails" }, alwaysRun = true)
    public void verifyExtendedRevenueDetails(RevenueExtended revenue)
    {
        verifyThat(revenue.getLocalAmount(), hasTextAsDouble(0.0));
        verifyThat(revenue.getUSDAmount(), hasTextAsDouble(0.0));
        verifyThat(revenue.getQualifying(), hasTextInView(IndicatorSelect.YES));
        verifyThat(revenue.getBilHotel(), hasTextInView(IndicatorSelect.YES));
        verifyThat(revenue.getFlagChangeReason(), hasTextInView(NOT_AVAILABLE));
    }

    @DataProvider(name = "revenue")
    private Object[][] revenue()
    {
        RevenueDetailsEdit revenueDetails = createStay.getStayDetails().getRevenueDetails();

        return new Object[][] { { revenueDetails.getRoomTax() }, //
                { revenueDetails.getTotalRevenue() }, //
                { revenueDetails.getTotalNonQualifyingRevenue() }, //
                { revenueDetails.getTotalQualifyingRevenue() } };
    }

    @Test(dataProvider = "revenue", dependsOnMethods = { "verifyExtendedRevenueDetails" }, alwaysRun = true)
    public void verifyRevenueDetails(Revenue revenue)
    {
        verifyThat(revenue.getLocalAmount(), hasTextAsDouble(0.0));
        verifyThat(revenue.getUSDAmount(), hasTextAsDouble(0.0));
    }

    @Test(dependsOnMethods = { "verifyRevenueDetails" }, alwaysRun = true)
    public void fillInHotel()
    {
        StayInfoCreate stayInfo = createStay.getStayDetails().getStayInfo();

        stayInfo.getHotel().typeAndWait(STAY_HOTEL);

        RevenueDetailsEdit revenueDetails = createStay.getStayDetails().getRevenueDetails();

        // Total Room
        RevenueExtended totalRoom = revenueDetails.getTotalRoom();

        verifyThat(totalRoom.getQualifying(), displayed(true));
        verifyThat(totalRoom.getQualifying(), hasText(IndicatorSelect.YES));

        verifyThat(totalRoom.getBilHotel(), displayed(true));
        verifyThat(totalRoom.getBilHotel(), hasText(IndicatorSelect.YES));

        verifyThat(totalRoom.getFlagChangeReason(), displayed(true));

        // Meeting
        RevenueExtended meeting = revenueDetails.getMeeting();

        verifyThat(meeting.getQualifying(), displayed(true));
        verifyThat(meeting.getQualifying(), hasText(IndicatorSelect.NO));

        verifyThat(meeting.getBilHotel(), displayed(true));
        verifyThat(meeting.getBilHotel(), hasText(IndicatorSelect.NO));

        verifyThat(meeting.getFlagChangeReason(), displayed(true));

        // Meeting
        RevenueExtended noRevenue = revenueDetails.getNoRevenue();

        verifyThat(noRevenue.getQualifying(), displayed(false));
        verifyThat(noRevenue.getQualifying(), hasTextInView(IndicatorSelect.NO));

        verifyThat(noRevenue.getBilHotel(), displayed(false));
        verifyThat(noRevenue.getBilHotel(), hasTextInView(IndicatorSelect.NO));
    }

    @Test(dependsOnMethods = { "fillInHotel" })
    public void fillInCheckIn()
    {
        final int nightsCount = 1;

        StayInfoCreate stayInfo = createStay.getStayDetails().getStayInfo();

        stayInfo.getCheckIn().type(CHECK_IN);
        stayInfo.getAvgRoomRate().clickAndWait();

        verifyThat(stayInfo.getCheckOut(), hasText(getFormattedDate(DAYS_SHIFT + nightsCount)));
        verifyThat(stayInfo.getNights(), hasTextAsInt(nightsCount));
    }

    @Test(dependsOnMethods = { "fillInCheckIn" })
    public void fillInNights()
    {
        StayInfoCreate stayInfo = createStay.getStayDetails().getStayInfo();

        stayInfo.getNights().type(NIGHTS);
        stayInfo.getAvgRoomRate().clickAndWait();

        verifyThat(stayInfo.getCheckOut(), hasText(CHECK_OUT));
    }

    @Test(dependsOnMethods = { "fillInNights" })
    public void fillInAvgRoomRate()
    {
        StayInfoCreate stayInfo = createStay.getStayDetails().getStayInfo();

        stayInfo.getAvgRoomRate().typeAndWait(AVG_AMOUNT);

        RevenueExtended totalRoom = createStay.getStayDetails().getRevenueDetails().getTotalRoom();
        verifyThat(totalRoom.getLocalAmount(), hasTextAsInt(TOTAL_ROOM));
    }

    @Test(dependsOnMethods = { "fillInAvgRoomRate" })
    public void fillInRevenue()
    {
        RevenueDetailsEdit revenueDetails = createStay.getStayDetails().getRevenueDetails();

        revenueDetails.getFood().getLocalAmount().type(FOOD);
        revenueDetails.getBeverage().getLocalAmount().type(BEVERAGE);
        revenueDetails.getPhone().getLocalAmount().type(PHONE);
        revenueDetails.getMeeting().getLocalAmount().type(MEETING);
        revenueDetails.getMandatoryRevenue().getLocalAmount().type(MANDATORY);
        revenueDetails.getOtherRevenue().getLocalAmount().type(OTHER);
        revenueDetails.getNoRevenue().getLocalAmount().typeAndWait(NO_REVENUE);

        verifyThat(revenueDetails.getTotalRevenue().getLocalAmount(), hasTextAsInt(TOTAL_REVENUE));
        verifyThat(revenueDetails.getTotalQualifyingRevenue().getLocalAmount(), hasTextAsInt(TOTAL_QUALIFYING));
    }

    @Test(dependsOnMethods = { "fillInRevenue" })
    public void fillInOtherDetails()
    {
        StayInfoCreate stayInfo = createStay.getStayDetails().getStayInfo();

        stayInfo.getRateCode().type(RATE_CODE);
        stayInfo.getRoomType().type(ROOM_TYPE);
        stayInfo.getRoomNumber().type(ROOM_NUMBER);

        StayBookingInfoEdit booking = createStay.getStayDetails().getBookingInfo();
        booking.getBookingSource().type(BOOKING_SOURCE);
        booking.getStayPayment().selectByCode(VISA);
    }

    @Test(dependsOnMethods = { "fillInOtherDetails" })
    public void clickSave()
    {
        createStay.clickCreateStay(assertNoError().add(verifySuccess(STAY_SUCCESS_CREATED)));
        stayPage.waitUntilFirstStayProcess();
    }

    @Test(dependsOnMethods = { "clickSave" })
    public void verifyExpandedStayDetails()
    {
        StayGuestGridRowContainer rowContainer = stayPage.getGuestGrid().getRow(1).getDetails();
        verifyStayDetails(rowContainer);
    }

    @Test(dependsOnMethods = { "verifyExpandedStayDetails" }, alwaysRun = true)
    public void verifyStayDetailsInPopUp()
    {
        stayPage.getGuestGrid().getRow(1).getDetails().clickDetails();

        StayDetailsAdjust stayDetails = adjustStayPopUp.getStayDetailsTab().getStayDetails();

        StayInfoEdit stayInfo = stayDetails.getStayInfo();
        verifyThat(stayInfo.getHotel(), hasText(STAY_HOTEL));
        verifyThat(stayInfo.getEarningPreference(), hasText(RC_POINTS));
        verifyThat(stayInfo.getAvgRoomRate(), hasTextAsInt(AVG_AMOUNT));

        verifyStayInfo(stayInfo);
        verifyThat(stayInfo.getCorpAcctNumber(), hasEmptyText());
        verifyThat(stayInfo.getIATANumber(), hasEmptyText());
        verifyThat(stayInfo.getHotelCurrency(), hasText(Currency.USD.getCodeWithValue()));

        verifyBookingInfo(stayDetails.getBookingInfo());

        RevenueDetailsEdit revenueDetails = stayDetails.getRevenueDetails();
        verifyThat(revenueDetails.getTotalRoom().getLocalAmount(), hasTextAsInt(TOTAL_ROOM));
        verifyThat(revenueDetails.getFood().getLocalAmount(), hasTextAsInt(FOOD));
        verifyThat(revenueDetails.getBeverage().getLocalAmount(), hasTextAsInt(BEVERAGE));
        verifyThat(revenueDetails.getPhone().getLocalAmount(), hasTextAsInt(PHONE));
        verifyThat(revenueDetails.getMeeting().getLocalAmount(), hasTextAsInt(MEETING));
        verifyThat(revenueDetails.getMandatoryRevenue().getLocalAmount(), hasTextAsInt(MANDATORY));
        verifyThat(revenueDetails.getOtherRevenue().getLocalAmount(), hasTextAsInt(OTHER));
        verifyThat(revenueDetails.getNoRevenue().getLocalAmount(), hasTextAsInt(NO_REVENUE));
        verifyThat(revenueDetails.getRoomTax().getLocalAmount(), hasTextAsInt(0));
        verifyThat(revenueDetails.getTotalRevenue().getLocalAmount(), hasTextAsInt(TOTAL_REVENUE));
        verifyThat(revenueDetails.getTotalNonQualifyingRevenue().getLocalAmount(),
                hasTextAsInt(TOTAL_REVENUE - TOTAL_QUALIFYING));
        verifyThat(revenueDetails.getTotalQualifyingRevenue().getLocalAmount(), hasTextAsInt(TOTAL_QUALIFYING));
    }

    @Test(dependsOnMethods = { "verifyStayDetailsInPopUp" }, alwaysRun = true)
    public void verifyEarningDetailsInPopUp()
    {
        EventEarningDetailsTab earningDetailsTab = adjustStayPopUp.getEarningDetailsTab();
        earningDetailsTab.goTo();
        earningDetailsTab.verifyBasicDetailsForPoints(POINTS_FOR_STAY_STR, POINTS_FOR_STAY_STR);

        earningDetailsTab.getGrid().verifyRowsFoundByType(getBaseUnitsEvent(POINTS_FOR_STAY_STR, POINTS_FOR_STAY_STR));
    }

    @Test(dependsOnMethods = { "verifyEarningDetailsInPopUp" }, alwaysRun = true)
    public void verifyBillingDetailsInPopUp()
    {
        EventBillingDetailsTab billingDetailsTab = adjustStayPopUp.getBillingDetailsTab();
        billingDetailsTab.goTo();
        billingDetailsTab.getEventBillingDetail(1).getBaseLoyaltyUnits().verifyUSDAmount("5.68", STAY_HOTEL);
    }

    @Test(dependsOnMethods = { "verifyBillingDetailsInPopUp" }, alwaysRun = true)
    public void verifyStayHistoryAfterCreateStay()
    {
        Map<String, String> map = queryResult(jdbcTemplate.queryForMap(String.format(GUEST, member.getRCProgramId())));

        EventHistoryTab historyTab = adjustStayPopUp.getHistoryTab();
        historyTab.goTo();

        EventHistoryGridRow historyRow = historyTab.getGrid().getRow(1);
        historyRow.verify(CREATE, helper.getUser().getNameFull());

        EventHistoryRecords stayRecords = historyRow.getAllRecords();
        stayRecords.getRecord(AVERAGE_ROOM_RATE).verifyAdd(hasTextAsInt(AVG_AMOUNT));
        stayRecords.getStayAmountRecord(EventAuditConstant.Stay.Revenue.BEVERAGE).verifyAdd(hasTextAsInt(BEVERAGE));
        stayRecords.getStayBillHotelRecord(EventAuditConstant.Stay.Revenue.BEVERAGE).verifyAdd(YES);
        stayRecords.getStayQualifyingRecord(EventAuditConstant.Stay.Revenue.BEVERAGE).verifyAdd(YES);
        stayRecords.getRecord(EventAuditConstant.Stay.Booking.SOURCE).verifyAdd(BOOKING_SOURCE);
        stayRecords.getRecord(EventAuditConstant.Stay.CHECK_IN).verifyAdd(CHECK_IN);
        stayRecords.getRecord(EventAuditConstant.Stay.CHECK_OUT).verifyAdd(CHECK_OUT);
        stayRecords.getRecord(EventAuditConstant.Stay.Booking.CREDIT_CARD_TYPE).verifyAdd(VISA.getCode());
        stayRecords.getRecord(CURRENCY).verifyAdd(Currency.USD.getCodeWithValue());
        stayRecords.getRecord(EventAuditConstant.Stay.Booking.DATA_SOURCE).verifyAdd(SC.getType());
        stayRecords.getRecord(EARNING_PREFERENCE).verifyAdd(RC_POINTS);
        stayRecords.getRecord(ENROLLING_STAY_INDICATOR).verifyAdd(NO);
        stayRecords.getRecord(EVENT_TYPE).verifyAdd(CREATED);
        stayRecords.getRecord(EventAuditConstant.Stay.Guest.FIRST_NAME).verifyAdd(member.getName().getGiven());
        stayRecords.getRecord(FOLIO_NUMBER).verifyAdd("-1");
        stayRecords.getStayAmountRecord(EventAuditConstant.Stay.Revenue.FOOD).verifyAdd(hasTextAsInt(FOOD));
        stayRecords.getStayBillHotelRecord(EventAuditConstant.Stay.Revenue.FOOD).verifyAdd(YES);
        stayRecords.getStayQualifyingRecord(EventAuditConstant.Stay.Revenue.FOOD).verifyAdd(YES);
        stayRecords.getRecord(EventAuditConstant.Stay.Guest.GUEST_BY_EID)
                .verifyAdd(hasText(String.format("%s (%s)", member.getName().getFullName(), map.get("ENTERPRISE_ID"))));
        stayRecords.getRecord(EventAuditConstant.Stay.Guest.GUEST_BY_MASTER_KEY)
                .verifyAdd(hasText(String.format("%s (%s)", member.getName().getFullName(), map.get("DGST_MSTR_KEY"))));
        stayRecords.getRecord(HOTEL).verifyAdd(STAY_HOTEL);
        stayRecords.getRecord(EventAuditConstant.Stay.Guest.LAST_NAME).verifyAdd(member.getName().getSurname());
        stayRecords.getStayAmountRecord(EventAuditConstant.Stay.Revenue.MEETING).verifyAdd(hasTextAsInt(MEETING));
        stayRecords.getStayBillHotelRecord(EventAuditConstant.Stay.Revenue.MEETING).verifyAdd(NO);
        stayRecords.getStayQualifyingRecord(EventAuditConstant.Stay.Revenue.MEETING).verifyAdd(NO);
        stayRecords.getRecord(EventAuditConstant.Stay.Guest.MEMBERSHIP_ID).verifyAdd(member.getRCProgramId());
        stayRecords.getStayAmountRecord(EventAuditConstant.Stay.Revenue.MISCELLANEOUS_MANDATORY_LOYALTY_REVENUE)
                .verifyAdd(hasTextAsInt(MANDATORY));
        stayRecords.getStayBillHotelRecord(EventAuditConstant.Stay.Revenue.MISCELLANEOUS_MANDATORY_LOYALTY_REVENUE)
                .verifyAdd(YES);
        stayRecords.getStayQualifyingRecord(EventAuditConstant.Stay.Revenue.MISCELLANEOUS_MANDATORY_LOYALTY_REVENUE)
                .verifyAdd(YES);
        stayRecords.getStayAmountRecord(EventAuditConstant.Stay.Revenue.MISCELLANEOUS_NO_LOYALTY_REVENUE)
                .verifyAdd(hasTextAsInt(NO_REVENUE));
        stayRecords.getStayBillHotelRecord(EventAuditConstant.Stay.Revenue.MISCELLANEOUS_NO_LOYALTY_REVENUE).verifyAdd(NO);
        stayRecords.getStayQualifyingRecord(EventAuditConstant.Stay.Revenue.MISCELLANEOUS_NO_LOYALTY_REVENUE).verifyAdd(NO);
        stayRecords.getStayAmountRecord(EventAuditConstant.Stay.Revenue.MISCELLANEOUS_OTHER_LOYALTY_REVENUE)
                .verifyAdd(hasTextAsInt(OTHER));
        stayRecords.getStayBillHotelRecord(EventAuditConstant.Stay.Revenue.MISCELLANEOUS_OTHER_LOYALTY_REVENUE)
                .verifyAdd(YES);
        stayRecords.getStayQualifyingRecord(EventAuditConstant.Stay.Revenue.MISCELLANEOUS_OTHER_LOYALTY_REVENUE)
                .verifyAdd(YES);
        stayRecords.getRecord(EventAuditConstant.Stay.NIGHTS).verifyAdd(hasTextAsInt(NIGHTS));
        stayRecords.getRecord(OVERLAPPING_INDICATOR).verifyAdd(NO);
        stayRecords.getRecord(EventAuditConstant.Stay.Booking.PAYMENT_TYPE_CODE).verifyAdd(CREDIT_CARD.getCode());
        stayRecords.getStayAmountRecord(EventAuditConstant.Stay.Revenue.PHONE).verifyAdd(hasTextAsInt(PHONE));
        stayRecords.getStayBillHotelRecord(EventAuditConstant.Stay.Revenue.PHONE).verifyAdd(YES);
        stayRecords.getStayQualifyingRecord(EventAuditConstant.Stay.Revenue.PHONE).verifyAdd(YES);
        stayRecords.getRecord(QUALIFYING_NIGHTS).verifyAdd(hasTextAsInt(NIGHTS));
        stayRecords.getRecord(EventAuditConstant.Stay.RATE_CODE).verifyAdd(RATE_CODE);
        stayRecords.getRecord(EventAuditConstant.Stay.ROOM_NUMBER).verifyAdd(ROOM_NUMBER);
        stayRecords.getStayAmountRecord(EventAuditConstant.Stay.Revenue.ROOM_TAX_FEE_NON_REVENUE)
                .verifyAdd(hasTextAsDouble(ZERO_VALUE));
        stayRecords.getStayBillHotelRecord(EventAuditConstant.Stay.Revenue.ROOM_TAX_FEE_NON_REVENUE).verifyAdd(NO);
        stayRecords.getStayQualifyingRecord(EventAuditConstant.Stay.Revenue.ROOM_TAX_FEE_NON_REVENUE).verifyAdd(NO);
        stayRecords.getRecord(EventAuditConstant.Stay.ROOM_TYPE).verifyAdd(ROOM_TYPE);
        stayRecords.getStayAmountRecord(EventAuditConstant.Stay.Revenue.TOTAL_ROOM).verifyAdd(hasTextAsInt(TOTAL_ROOM));
        stayRecords.getStayBillHotelRecord(EventAuditConstant.Stay.Revenue.TOTAL_ROOM).verifyAdd(YES);
        stayRecords.getStayQualifyingRecord(EventAuditConstant.Stay.Revenue.TOTAL_ROOM).verifyAdd(YES);
        stayRecords.getRecord(TRANSACTION_DATE).verifyAdd(getFormattedDate());
    }

    @Test(dependsOnMethods = { "verifyStayHistoryAfterCreateStay" }, alwaysRun = true)
    public void closeAdjustStay()
    {
        adjustStayPopUp.clickClose(assertNoError());
    }

    @Test(dependsOnMethods = { "closeAdjustStay" }, alwaysRun = true)
    public void verifyExpandedStayDetailsOnAllEvents()
    {
        AllEventsPage allEvents = new AllEventsPage();
        allEvents.goTo();
        verifyNoErrors();

        allEvents.getSearchFields().getEventType().select("Stay");
        allEvents.getButtonBar().getSearch().clickAndWait();

        verifyStayDetails(allEvents.getGrid().getRow(1).expand(StayGuestGridRowContainer.class));
    }

    @Test(dependsOnMethods = { "verifyExpandedStayDetailsOnAllEvents" })
    public void verifyCountersOnAnnualActivityPage()
    {
        LifetimeActivity lifetimeActivity = member.getLifetimeActivity();
        lifetimeActivity.setBaseUnits(POINTS_FOR_STAY);
        lifetimeActivity.setTotalUnits(POINTS_FOR_STAY);
        lifetimeActivity.setCurrentBalance(POINTS_FOR_STAY);

        AnnualActivity activity = member.getAnnualActivity();
        activity.setTierLevelNights(NIGHTS);
        activity.setQualifiedNights(NIGHTS);
        activity.setTotalQualifiedUnits(POINTS_FOR_STAY);
        activity.setTotalUnits(POINTS_FOR_STAY);

        new AnnualActivityPage().verifyCounters(member);

        RewardClubGridRow rcProgram = new ProgramInfoPanel().getPrograms().getRewardClubProgram();
        verifyThat(rcProgram.getPointsBalance(), hasTextAsInt(POINTS_FOR_STAY));
    }

    @Test(dependsOnMethods = { "closeAdjustStay" })
    public void verifyCountersOnProgramPage()
    {
        RewardClubPage rcPage = new RewardClubPage();
        rcPage.goTo();
        verifyNoErrors();

        RewardClubTierLevelActivityGrid activities = rcPage.getAnnualActivities(DateUtils.currentYear());

        verifyThat(activities.getPointsRowContainer().getTotalQualifyingPoints(), hasTextAsInt(POINTS_FOR_STAY));

        NightGridRowContainer nights = activities.getNightsRowContainer();
        verifyThat(nights.getRewardNights(), hasTextAsInt(0));
        verifyThat(nights.getQualifyingNights(), hasTextAsInt(NIGHTS));
        verifyThat(nights.getTotalQualifyingNights(), hasTextAsInt(NIGHTS));
    }

    private void verifyStayInfo(StayInfoBase stayInfo)
    {
        verifyThat(stayInfo.getCheckIn(), hasText(CHECK_IN));
        verifyThat(stayInfo.getCheckOut(), hasText(CHECK_OUT));
        verifyThat(stayInfo.getConfirmationNumber(), hasDefault());
        verifyThat(stayInfo.getFolioNumber(), hasDefault());
        verifyThat(stayInfo.getRoomNumber(), hasText(ROOM_NUMBER));
        verifyThat(stayInfo.getRoomType(), hasText(ROOM_TYPE));
        verifyThat(stayInfo.getRateCode(), hasText(RATE_CODE));
        verifyThat(stayInfo.getOverlappingStay(), hasText(IndicatorSelect.NO));
        verifyThat(stayInfo.getEnrollingStay(), hasText(IndicatorSelect.NO));
        verifyThat(stayInfo.getNights(), hasTextAsInt(NIGHTS));
        verifyThat(stayInfo.getQualifiedNights(), hasTextAsInt(NIGHTS));
    }

    private void verifyStayDetails(StayGuestGridRowContainer rowContainer)
    {
        StayInfoView stayInfo = rowContainer.getStayInfo();
        verifyStayInfo(stayInfo);
        verifyThat(stayInfo.getCorpAcctNumber(), hasText(NOT_AVAILABLE));
        verifyThat(stayInfo.getIATANumber(), hasText(NOT_AVAILABLE));
        verifyThat(stayInfo.getHotelCurrency(), hasText(Currency.USD.getValue()));

        verifyBookingInfo(rowContainer.getBookingInfo());

        RevenueDetailsView revenueDetails = rowContainer.getRevenueDetails();
        verifyThat(revenueDetails.getAverageRoomRate(), hasTextAsInt(AVG_AMOUNT));
        verifyThat(revenueDetails.getTotalRoom(), hasTextAsInt(TOTAL_ROOM));
        verifyThat(revenueDetails.getFood(), hasTextAsInt(FOOD));
        verifyThat(revenueDetails.getBeverage(), hasTextAsInt(BEVERAGE));
        verifyThat(revenueDetails.getPhone(), hasTextAsInt(PHONE));
        verifyThat(revenueDetails.getMeeting(), hasTextAsInt(MEETING));
        verifyThat(revenueDetails.getMandatoryRevenue(), hasTextAsInt(MANDATORY));
        verifyThat(revenueDetails.getOtherRevenue(), hasTextAsInt(OTHER));
        verifyThat(revenueDetails.getNoRevenue(), hasTextAsInt(NO_REVENUE));
        verifyThat(revenueDetails.getRoomTax(), hasTextAsInt(0));
        verifyThat(revenueDetails.getTotalRevenue(), hasTextAsInt(TOTAL_REVENUE));
        verifyThat(revenueDetails.getTotalNonQualifyingRevenue(), hasTextAsInt(TOTAL_REVENUE - TOTAL_QUALIFYING));
        verifyThat(revenueDetails.getTotalQualifyingRevenue(), hasTextAsInt(TOTAL_QUALIFYING));

        rowContainer.getSource().verify(helper.getSourceFactory().getNoEmployeeIdSource(), NOT_AVAILABLE);
    }

    private void verifyBookingInfo(StayBookingInfoBase bookingInfo)
    {
        verifyThat(bookingInfo.getBookingDate(), hasDefault());
        verifyThat(bookingInfo.getBookingSource(), hasText(BOOKING_SOURCE));
        verifyThat(bookingInfo.getDataSource(), hasText("SC"));
    }
}
