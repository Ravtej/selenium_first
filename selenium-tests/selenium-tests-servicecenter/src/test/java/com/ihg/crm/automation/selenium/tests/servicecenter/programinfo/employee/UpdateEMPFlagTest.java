package com.ihg.crm.automation.selenium.tests.servicecenter.programinfo.employee;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static org.hamcrest.CoreMatchers.containsString;

import java.util.ArrayList;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.CustomerInfoFlags;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.common.types.program.TierLevelFlag;
import com.ihg.automation.selenium.matchers.component.ContainsDualListItems;
import com.ihg.automation.selenium.servicecenter.pages.LeftPanel;
import com.ihg.automation.selenium.servicecenter.pages.account.AccountInfoPage;
import com.ihg.automation.selenium.servicecenter.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.servicecenter.pages.programs.EmployeePage;
import com.ihg.automation.selenium.servicecenter.pages.programs.ProgramSummary;
import com.ihg.crm.automation.selenium.tests.servicecenter.LoginLogout;

public class UpdateEMPFlagTest extends LoginLogout
{
    private Member member = new Member();
    private EmployeePage employeePage = new EmployeePage();
    private ProgramSummary summary;
    private ArrayList<String> expectedListContent = new ArrayList<String>(TierLevelFlag.getNoProgramValueList());

    private final static String FLAG_TO_SELECT = TierLevelFlag.CUSTOMER_RETENTION.getValue();

    @BeforeClass
    public void beforeClass()
    {
        member.setPersonalInfo(MemberPopulateHelper.getSimplePersonalInfo());
        member.addAddress(MemberPopulateHelper.getUnitedStatesAddress());
        member.addProgram(Program.EMP);

        login();

        member = new EnrollmentPage().enroll(member);
    }

    @Test
    public void navigateEMPProgramSummary()
    {
        employeePage.goTo();
        summary = employeePage.getSummary();
        summary.clickEdit();
    }

    @Test(dependsOnMethods = { "navigateEMPProgramSummary" })
    public void verifyAvailableSummaryFlags()
    {
        verifyThat(summary.getFlags().getLeftList(), ContainsDualListItems.containsItems(expectedListContent));
    }

    @Test(dependsOnMethods = { "verifyAvailableSummaryFlags", "navigateEMPProgramSummary" }, alwaysRun = true)
    public void selectTheFlag()
    {
        summary.getFlags().select(FLAG_TO_SELECT);
        summary.clickSave();
    }

    @Test(dependsOnMethods = { "selectTheFlag" }, alwaysRun = true)
    public void verifyProgramInformationFlags()
    {
        employeePage = new EmployeePage();
        verifyThat(employeePage.getSummary(), hasText(containsString(FLAG_TO_SELECT)));
    }

    @Test(dependsOnMethods = { "verifyProgramInformationFlags" }, alwaysRun = true)
    public void verifyLeftPanelFlags()
    {
        CustomerInfoFlags flags = new LeftPanel().getCustomerInfoPanel().getFlags();
        verifyThat(flags, hasText(containsString(FLAG_TO_SELECT)));
    }

    @Test(dependsOnMethods = { "verifyLeftPanelFlags" }, alwaysRun = true)
    public void verifyAccountInformationFlags()
    {
        AccountInfoPage page = new AccountInfoPage();
        page.goTo();
        verifyThat(page.getFlags(), hasText(containsString(FLAG_TO_SELECT)));
    }
}
