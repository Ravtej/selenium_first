package com.ihg.crm.automation.selenium.tests.servicecenter.enrollment.ambassador;

import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyNoError;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.isDisplayedWithWait;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.common.DateUtils;
import com.ihg.automation.selenium.common.Constant;
import com.ihg.automation.selenium.common.catalog.CatalogItem;
import com.ihg.automation.selenium.common.enroll.SuccessEnrolmentPopUp;
import com.ihg.automation.selenium.common.member.AmbassadorOfferCode;
import com.ihg.automation.selenium.common.member.AmbassadorUtils;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.pages.PaymentMethod;
import com.ihg.automation.selenium.common.payment.CheckPayment;
import com.ihg.automation.selenium.common.payment.CheckPaymentContainer;
import com.ihg.automation.selenium.common.payment.PaymentDetailsPopUp;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.gwt.components.Mode;
import com.ihg.automation.selenium.servicecenter.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.servicecenter.pages.event.enroll.MembershipDetails;
import com.ihg.automation.selenium.servicecenter.pages.event.enroll.MembershipDetailsPopUp;
import com.ihg.automation.selenium.servicecenter.pages.event.enroll.MembershipDetailsTab;
import com.ihg.automation.selenium.servicecenter.pages.personal.PersonalInfoPage;
import com.ihg.automation.selenium.servicecenter.pages.programs.ProgramPageBase.EnrollmentDetails;
import com.ihg.automation.selenium.servicecenter.pages.programs.amb.AmbassadorPage;

public class EnrollmentByCheckTest extends AMBEnrollmentCommon
{
    private static final CatalogItem ENROLMENT_AWARD = AmbassadorUtils.AMBASSADOR_CROSS_CHARGE_$150;

    private Member member = new Member();
    private PaymentDetailsPopUp ambEnrollPaymentDetailsPopUp = new PaymentDetailsPopUp();
    private PersonalInfoPage personalInfoPage = new PersonalInfoPage();
    private AmbassadorPage ambassadorPage = new AmbassadorPage();
    private MembershipDetailsPopUp membershipDetailsPopUp = new MembershipDetailsPopUp();
    private final static String DEPOSIT_DATE = DateUtils.getFormattedDate();
    private final static String DEPOSIT_NUMBER = "1234567";
    private final static String ISSUING_BANK = "Issuing bank";
    private final static String NAME_ON_ACCOUNT = "Name on account";
    private final static String CHECK_NUMBER = "123456";
    private CheckPayment checkPayment = new CheckPayment(ENROLMENT_AWARD, CHECK_NUMBER, NAME_ON_ACCOUNT, ISSUING_BANK,
            DEPOSIT_DATE, DEPOSIT_NUMBER);

    @BeforeClass
    public void beforeClass()
    {
        member.addProgram(Program.AMB);
        member.addAddress(MemberPopulateHelper.getUnitedStatesAddress());
        member.setPersonalInfo(MemberPopulateHelper.getSimplePersonalInfo());
        member.setAmbassadorOfferCode(AmbassadorOfferCode.CHSRO);

        login();
    }

    @Test
    public void populateAMBMemberEnrollmentData()
    {
        EnrollmentPage enrollmentPage = new EnrollmentPage();
        enrollmentPage.goTo();
        enrollmentPage.populate(member);
        enrollmentPage.clickSubmit(verifyNoError());
    }

    @Test(dependsOnMethods = { "populateAMBMemberEnrollmentData" })
    public void verifyAvailablePaymentTypes()
    {
        ambEnrollPaymentDetailsPopUp.selectAmount(ENROLMENT_AWARD);
        verifyThat(ambEnrollPaymentDetailsPopUp, isDisplayedWithWait());
        verifyThat(ambEnrollPaymentDetailsPopUp.getPaymentTypeButton(PaymentMethod.CASH), displayed(true));
        verifyThat(ambEnrollPaymentDetailsPopUp.getPaymentTypeButton(PaymentMethod.CREDIT_CARD), displayed(true));
        verifyThat(ambEnrollPaymentDetailsPopUp.getPaymentTypeButton(PaymentMethod.CHECK), displayed(true));
    }

    @Test(dependsOnMethods = { "verifyAvailablePaymentTypes" }, alwaysRun = true)
    public void verifyCheckPaymentFields()
    {
        ambEnrollPaymentDetailsPopUp.selectPaymentType(PaymentMethod.CHECK);
        CheckPaymentContainer payment = ambEnrollPaymentDetailsPopUp.getCheckPayment();
        verifyThat(payment.getCheckNumber(), displayed(true));
        verifyThat(payment.getDepositDate(), displayed(true));
        verifyThat(payment.getDepositNumber(), displayed(true));
        verifyThat(payment.getNameOnAccount(), displayed(true));
        verifyThat(payment.getIssuingBank(), displayed(true));
    }

    @Test(dependsOnMethods = { "verifyCheckPaymentFields" }, alwaysRun = true)
    public void enrollAMBMember()
    {
        CheckPaymentContainer payment = ambEnrollPaymentDetailsPopUp.getCheckPayment();
        payment.populate(checkPayment);
        ambEnrollPaymentDetailsPopUp.clickSubmitNoError();

        SuccessEnrolmentPopUp popUp = new SuccessEnrolmentPopUp();
        verifyThat(popUp, isDisplayedWithWait());
        member = popUp.putPrograms(member);
        verifyThat(popUp.getMemberId(Program.RC), hasText(popUp.getMemberId(Program.AMB).getText()));
        popUp.clickDone(verifyNoError());
    }

    @Test(dependsOnMethods = { "enrollAMBMember" })
    public void verifyAMBProgramInfoEnrollmentDetails()
    {
        verifyThat(personalInfoPage, isDisplayedWithWait());
        ambassadorPage.goTo();
        EnrollmentDetails details = ambassadorPage.getEnrollmentDetails();
        details.verifyScSource(helper);
        verifyThat(details.getEnrollDate(), hasText(DateUtils.getFormattedDate()));
        verifyThat(details.getRenewalDate(), hasText(Constant.NOT_AVAILABLE));
        verifyThat(details.getOfferCode(), hasText(member.getAmbassadorOfferCode().getCode()));
        verifyThat(details.getReferringMember(), hasText(Constant.NOT_AVAILABLE));
        details.getPaymentDetails().verify(checkPayment, Mode.VIEW);
    }

    @Test(dependsOnMethods = { "verifyAMBProgramInfoEnrollmentDetails" }, alwaysRun = true)
    public void openAllEventsAMBEventDetailsPopUp()
    {
        openAMBEventDetailsPopUp();
    }

    @Test(dependsOnMethods = { "openAllEventsAMBEventDetailsPopUp" })
    public void verifyAllEventsAMBEventMembershipDetails()
    {
        MembershipDetailsTab tab = membershipDetailsPopUp.getMembershipDetailsTab();
        tab.goTo();
        MembershipDetails membershipDetails = tab.getMembershipDetails();
        membershipDetails.getPaymentDetails().verify(checkPayment, Mode.VIEW);
        membershipDetails.verifyScSource(helper);
    }

    @Test(dependsOnMethods = { "openAllEventsAMBEventDetailsPopUp" })
    public void verifyAllEventsAMBEventBillingDetails()
    {
        verifyAMBEventBillingDetails(ENROLMENT_AWARD);
    }

    @Test(dependsOnMethods = { "openAllEventsAMBEventDetailsPopUp" })
    public void verifyAllEventsAMBEventEarningDetails()
    {
        verifyAMBEventEarningDetails(ENROLMENT_AWARD);
    }

}
