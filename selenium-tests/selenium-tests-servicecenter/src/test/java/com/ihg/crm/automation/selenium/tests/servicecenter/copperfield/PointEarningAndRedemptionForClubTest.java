package com.ihg.crm.automation.selenium.tests.servicecenter.copperfield;

import static com.ihg.automation.common.DateUtils.now;
import static com.ihg.crm.automation.selenium.tests.servicecenter.copperfield.VerificationDatesUtils.verifyDatesWithShiftedExpiration;

public class PointEarningAndRedemptionForClubTest extends PointEarningAndRedemptionBase
{
    @Override
    protected void postEnrollActions()
    {
        // Nothing to do
    }

    @Override
    protected void verifyAfterPointEvent()
    {
        verifyDatesWithShiftedExpiration(now());
    }

}
