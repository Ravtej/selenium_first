package com.ihg.crm.automation.selenium.tests.servicecenter.thresholder;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.event.EventTransactionType.TierLevel.UPGRADE_ACHIEVE;
import static com.ihg.automation.selenium.common.types.program.ProgramStatus.OPEN;
import static com.ihg.automation.selenium.common.types.program.RewardClubLevel.CLUB;
import static com.ihg.automation.selenium.common.types.program.RewardClubLevel.GOLD;
import static com.ihg.automation.selenium.common.types.program.RewardClubLevel.PLTN;
import static com.ihg.automation.selenium.common.types.program.RewardClubLevel.SPRE;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasTextAsInt;
import static com.ihg.automation.selenium.matchers.component.GridSize.size;
import static org.hamcrest.Matchers.not;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.stay.Stay;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.servicecenter.pages.LeftPanel;
import com.ihg.automation.selenium.servicecenter.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsPage;
import com.ihg.automation.selenium.servicecenter.pages.event.stay.StayEventsPage;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.RewardClubPage;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.RewardClubSummary;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.TierLevelBenefitsDetailsPopUp;
import com.ihg.automation.selenium.tests.common.RulesDBUtils;
import com.ihg.crm.automation.selenium.tests.servicecenter.LoginLogout;

public class AchieveLevelsByPointsTest extends LoginLogout
{
    private Stay stayToAchieveGold = new Stay();
    private Stay stayToAchievePlatinum;
    private Stay stayToAchieveSpire;
    private static int pointsToAchieveGold;
    private static int pointsToAchievePlatinum;
    private static int pointsToAchieveSpire;
    private static int pointsFromGoldToPlatinum;
    private static int pointsFromPlatinumToSpire;

    @BeforeClass
    public void before()
    {
        RulesDBUtils rulesDBUtils = new RulesDBUtils(jdbcTemplate);

        pointsToAchieveGold = rulesDBUtils.getCurrentYearRulePoints(CLUB, GOLD);
        pointsToAchievePlatinum = rulesDBUtils.getCurrentYearRulePoints(GOLD, PLTN);
        pointsToAchieveSpire = rulesDBUtils.getCurrentYearRulePoints(PLTN, SPRE);

        pointsFromGoldToPlatinum = pointsToAchievePlatinum - pointsToAchieveGold;
        pointsFromPlatinumToSpire = pointsToAchieveSpire - pointsToAchievePlatinum;

        stayToAchieveGold.setHotelCode("SHGHA");
        stayToAchieveGold.setCheckInOutByNights(1);
        stayToAchieveGold.setUsdAvgRoomRateByPoints(pointsToAchieveGold);
        stayToAchieveGold.setRateCode("Test");

        stayToAchievePlatinum = stayToAchieveGold.clone();
        stayToAchievePlatinum.setHotelCode("CEQHA");
        stayToAchievePlatinum.setUsdAvgRoomRateByPoints(pointsFromGoldToPlatinum);

        stayToAchieveSpire = stayToAchieveGold.clone();
        stayToAchieveSpire.setHotelCode("ATLFX");
        stayToAchieveSpire.setUsdAvgRoomRateByPoints(pointsFromPlatinumToSpire);

        Member member = new Member();
        member.addProgram(Program.RC);
        member.setPersonalInfo(MemberPopulateHelper.getSimplePersonalInfo());
        member.addAddress(MemberPopulateHelper.getUnitedStatesAddress());

        login();

        new EnrollmentPage().enroll(member);

        new LeftPanel().verifyRCLevel(CLUB);
    }

    @Test
    public void verifyAnnualActivitiesForClub()
    {
        RewardClubPage rewardClubPage = new RewardClubPage();
        rewardClubPage.goTo();
        rewardClubPage.getCurrentYearAnnualActivities().getPointsRow().verify(0, pointsToAchieveGold);
    }

    @Test(dependsOnMethods = { "verifyAnnualActivitiesForClub" }, alwaysRun = true)
    public void achieveGoldTierLevel()
    {
        new StayEventsPage().createStay(stayToAchieveGold);

        new LeftPanel().verifyRCLevel(GOLD);

        RewardClubPage rewardClubPage = new RewardClubPage();
        rewardClubPage.goTo();

        rewardClubPage.getCurrentYearAnnualActivities().getPointsRow().verify(hasTextAsInt(pointsToAchieveGold),
                hasTextAsInt(pointsFromGoldToPlatinum));

        AllEventsPage allEventsPage = new AllEventsPage();
        allEventsPage.goTo();

        verifyThat(allEventsPage.getGrid().getRow(UPGRADE_ACHIEVE, "RC - GOLD"), displayed(true));
    }

    @Test(dependsOnMethods = { "achieveGoldTierLevel" }, alwaysRun = true)
    public void achievePlatinumTierLevel()
    {
        new StayEventsPage().createStay(stayToAchievePlatinum);

        new LeftPanel().verifyRCLevel(PLTN);

        RewardClubPage rewardClubPage = new RewardClubPage();
        rewardClubPage.goTo();

        rewardClubPage.getCurrentYearAnnualActivities().getPointsRow().verify(hasTextAsInt(pointsToAchievePlatinum),
                hasTextAsInt(pointsFromPlatinumToSpire));

        AllEventsPage allEventsPage = new AllEventsPage();
        allEventsPage.goTo();

        verifyThat(allEventsPage.getGrid().getRow(UPGRADE_ACHIEVE, "RC - PLATINUM"), displayed(true));
    }

    @Test(dependsOnMethods = { "achievePlatinumTierLevel" }, alwaysRun = true)
    public void achieveSpireTierLevel()
    {
        new StayEventsPage().createStay(stayToAchieveSpire);

        new LeftPanel().verifyRCLevel(SPRE);

        RewardClubPage rewardClubPage = new RewardClubPage();
        rewardClubPage.goTo();

        RewardClubSummary summary = rewardClubPage.getSummary();
        summary.verify(OPEN, SPRE, 1);

        TierLevelBenefitsDetailsPopUp tierLevelBenefitsDetailsPopUp = summary.openTierLevelBenefitsDetailsPopUp();
        verifyThat(tierLevelBenefitsDetailsPopUp.getBenefitsGrid(), size(not(0)));
        tierLevelBenefitsDetailsPopUp.close();

        rewardClubPage.getCurrentYearAnnualActivities().getPointsRow().verify(hasTextAsInt(pointsToAchieveSpire),
                displayed(false));

        AllEventsPage allEventsPage = new AllEventsPage();
        allEventsPage.goTo();

        verifyThat(allEventsPage.getGrid().getRow(UPGRADE_ACHIEVE, "RC - SPIRE"), displayed(true));
    }
}
