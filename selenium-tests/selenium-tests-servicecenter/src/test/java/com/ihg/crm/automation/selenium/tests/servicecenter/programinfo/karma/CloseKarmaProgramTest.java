package com.ihg.crm.automation.selenium.tests.servicecenter.programinfo.karma;

import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifySuccess;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.types.program.ProgramStatus.CLOSED;
import static com.ihg.automation.selenium.common.types.program.ProgramStatus.OPEN;
import static com.ihg.automation.selenium.matchers.component.ViewableMatcher.hasTextInView;
import static com.ihg.automation.selenium.servicecenter.pages.personal.PersonalInfoPage.SUCCESS_MESSAGE;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.common.types.program.ProgramStatusReason;
import com.ihg.automation.selenium.servicecenter.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.servicecenter.pages.programs.KarmaPage;
import com.ihg.automation.selenium.servicecenter.pages.programs.ProgramSummary;
import com.ihg.crm.automation.selenium.tests.servicecenter.LoginLogout;

@Test(groups = { "karma" })
public class CloseKarmaProgramTest extends LoginLogout
{
    @BeforeClass
    public void beforeClass()
    {
        Member member = new Member();
        member.setPersonalInfo(MemberPopulateHelper.getSimplePersonalInfo());
        member.addAddress(MemberPopulateHelper.getUnitedStatesAddress());
        member.addProgram(Program.KAR);

        login();

        new EnrollmentPage().enroll(member);
    }

    @DataProvider(name = "reasonProvider")
    protected Object[][] reasonProvider()
    {
        return new Object[][] { { ProgramStatusReason.CUSTOMER_REQUEST }, { ProgramStatusReason.FROZEN },
                { ProgramStatusReason.DUPLICATE }, { ProgramStatusReason.IHG_REQUEST },
                { ProgramStatusReason.MUTUALLY_EXCLUSIVE } };
    }

    @Test(dataProvider = "reasonProvider")
    public void closeAndReOpenKarmaProgram(String reason)
    {
        KarmaPage karmaPage = new KarmaPage();
        karmaPage.goTo();
        ProgramSummary summary = karmaPage.getSummary();
        summary.setClosedStatus(reason, verifySuccess(SUCCESS_MESSAGE));

        verifyThat(summary.getStatus(), hasTextInView(CLOSED));
        verifyThat(summary.getStatusReason(), hasTextInView(reason));

        summary.setOpenStatus(verifySuccess(SUCCESS_MESSAGE));
        verifyThat(summary.getStatus(), hasTextInView(OPEN));
    }
}
