package com.ihg.crm.automation.selenium.tests.servicecenter.programinfo.ambassador;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.Constant.RC_POINTS;
import static com.ihg.automation.selenium.common.event.EventTransactionType.Goodwill.GOODWILL;
import static com.ihg.automation.selenium.common.event.EventTransactionType.Membership.MEMBERSHIP_RENEWAL;
import static com.ihg.automation.selenium.common.member.AmbassadorUtils.RENEW_GOLD_AMBASSADOR_KIT_150;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.isDisplayedWithWait;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.common.DateUtils;
import com.ihg.automation.selenium.common.catalog.CatalogItem;
import com.ihg.automation.selenium.common.event.AllEventsRow;
import com.ihg.automation.selenium.common.member.AmbassadorJdbcUtils;
import com.ihg.automation.selenium.common.member.AmbassadorUtils;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.pages.PaymentMethod;
import com.ihg.automation.selenium.common.payment.Payment;
import com.ihg.automation.selenium.common.payment.PaymentDetailsPopUp;
import com.ihg.automation.selenium.common.payment.PaymentFactory;
import com.ihg.automation.selenium.servicecenter.pages.LeftPanel;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsGridRow;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsPage;
import com.ihg.automation.selenium.servicecenter.pages.event.enroll.MembershipDetailsPopUp;
import com.ihg.automation.selenium.servicecenter.pages.event.enroll.MembershipDetailsTab;
import com.ihg.automation.selenium.servicecenter.pages.event.enroll.MembershipGridRowContainer;
import com.ihg.automation.selenium.servicecenter.pages.programs.RenewExtendProgramSummary;
import com.ihg.automation.selenium.servicecenter.pages.programs.amb.AmbassadorPage;
import com.ihg.automation.selenium.servicecenter.pages.unit.GoodwillPopUp;

public class RenewByPointsTest extends AmbassadorCommon
{
    public static final CatalogItem RENEW_AWARD = AmbassadorUtils.AMBASSADOR_RENEWAL_24000_PTS;

    private AmbassadorPage ambassadorPage = new AmbassadorPage();
    private AllEventsPage allEventsPage = new AllEventsPage();
    private MembershipGridRowContainer container;
    private AllEventsRow rowData = new AllEventsRow(DateUtils.getFormattedDate(), GOODWILL, RC_POINTS, "50000", "");
    private Payment payment = PaymentFactory.getPointsPayment(RENEW_AWARD);
    private int daysShift;

    @BeforeClass
    public void beforeClass()
    {
        login();

        Member member = enrollAMBmember();
        daysShift = new AmbassadorJdbcUtils(jdbcTemplateUpdate, lastUpdateUserId).updateForRenew(member);
        new LeftPanel().reOpenMemberProfile(member);
    }

    @Test
    public void populateGoodwillLoyaltyUnitsPopUp()
    {
        new GoodwillPopUp().goodwillPoints("50000");
        verifyThat(allEventsPage, isDisplayedWithWait());
        AllEventsGridRow row = allEventsPage.getGrid().getRow(GOODWILL);
        verifyThat(row, displayed(true));
        row.verify(rowData);
    }

    @Test(dependsOnMethods = { "populateGoodwillLoyaltyUnitsPopUp" }, alwaysRun = true)
    public void verifyRenewButtonAndExpirationDate()
    {
        ambassadorPage.goTo();

        RenewExtendProgramSummary ambSummary = ambassadorPage.getSummary();
        ambSummary.verifyRenewAndExtendButtons(true, true);
    }

    @Test(dependsOnMethods = "verifyRenewButtonAndExpirationDate", alwaysRun = true)
    public void populatePaymentDetailsPopUp()
    {
        ambassadorPage.getSummary().clickRenew();
        PaymentDetailsPopUp paymentDetailsPopUp = new PaymentDetailsPopUp();
        paymentDetailsPopUp.selectAmount(RENEW_AWARD);
        verifyThat(paymentDetailsPopUp.getPaymentTypeButton(PaymentMethod.LOYALTY_UNITS), displayed(true));
        paymentDetailsPopUp.clickSubmitNoError();
    }

    @Test(dependsOnMethods = "populatePaymentDetailsPopUp", alwaysRun = true)
    public void verifyExpiredDate()
    {
        ambassadorPage.getSummary().verifyExpDateAfterRenew(daysShift);
    }

    @Test(dependsOnMethods = "verifyExpiredDate", alwaysRun = true)
    public void verifyAllEventsContainer()
    {
        allEventsPage.goTo();
        AllEventsGridRow row = allEventsPage.getGrid().getRow(MEMBERSHIP_RENEWAL);
        verifyThat(row, displayed(true));
        container = row.expand(MembershipGridRowContainer.class);
        container.getMembershipDetails().verify(payment, helper);
    }

    @Test(dependsOnMethods = "verifyAllEventsContainer", alwaysRun = true)
    public void openMembershipDetailsPopUpVerify()
    {
        container.clickDetails();
        verifyThat(new MembershipDetailsPopUp(), isDisplayedWithWait());
    }

    @Test(dependsOnMethods = "openMembershipDetailsPopUpVerify", alwaysRun = true)
    public void verifyAllEventsMembershipDetailsTab()
    {
        MembershipDetailsPopUp membershipDetailsPopUp = new MembershipDetailsPopUp();
        MembershipDetailsTab membershipDetailsTab = membershipDetailsPopUp.getMembershipDetailsTab();
        membershipDetailsTab.goTo();
        membershipDetailsTab.getMembershipDetails().verify(payment, helper);
    }

    @Test(dependsOnMethods = "verifyAllEventsMembershipDetailsTab")
    public void verifyAllEventsEarningDetailsTab()
    {
        verifyEarningDetailsTab(RENEW_AWARD, RENEW_GOLD_AMBASSADOR_KIT_150);
    }

    @Test(dependsOnMethods = { "verifyAllEventsEarningDetailsTab" }, alwaysRun = true)
    public void validateBillingDetailsTab()
    {
        validateBillingDetails(RENEW_AWARD);
    }

}
