package com.ihg.crm.automation.selenium.tests.servicecenter.enrollment;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.compareLists;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static org.hamcrest.CoreMatchers.containsString;

import java.util.Arrays;
import java.util.List;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.CustomerInfoFlags;
import com.ihg.automation.selenium.common.CustomerInfoPanel;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.common.types.program.TierLevelFlag;
import com.ihg.automation.selenium.servicecenter.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.RewardClubPage;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.RewardClubSummary;
import com.ihg.crm.automation.selenium.tests.servicecenter.LoginLogout;

public class RcEnrollmentWithFlagsTest extends LoginLogout
{
    private EnrollmentPage enrollmentPage = new EnrollmentPage();
    private List<String> flags = Arrays.asList(TierLevelFlag.CISCO.getValue(),
            TierLevelFlag.CUSTOMER_RETENTION.getValue());

    @BeforeClass
    public void before()
    {
        Member member = new Member();
        member.addProgram(Program.RC);
        member.setPersonalInfo(MemberPopulateHelper.getSimplePersonalInfo());
        member.addAddress(MemberPopulateHelper.getUnitedStatesAddress());
        member.addFlags(flags);

        login();

        enrollmentPage.enroll(member);
    }

    @Test
    public void customerInfoPanelFlags()
    {
        CustomerInfoFlags customerInfoFlags = new CustomerInfoPanel().getFlags();
        compareLists(customerInfoFlags, flags, customerInfoFlags.getFlags());
    }

    @Test
    public void programInfoFlags()
    {
        RewardClubPage rewardClubPage = new RewardClubPage();
        rewardClubPage.goTo();
        RewardClubSummary rcSummary = rewardClubPage.getSummary();
        for (String flag : flags)
        {
            verifyThat(rcSummary, hasText(containsString(flag)));
        }
    }
}
