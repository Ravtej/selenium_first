package com.ihg.crm.automation.selenium.tests.servicecenter.preferences;

import static com.ihg.automation.common.DateUtils.YYYY_MM;
import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyInfo;
import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyNoError;
import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifySuccess;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.pages.CreditCardType.DISCOVER_CARD;
import static com.ihg.automation.selenium.common.pages.CreditCardType.MASTER_CARD;
import static com.ihg.automation.selenium.common.pages.CreditCardType.VISA;
import static com.ihg.automation.selenium.common.preferences.TravelProfileType.BUSINESS;
import static com.ihg.automation.selenium.common.types.program.Program.AMB;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.isDisplayedWithWait;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.isHighlightedAsInvalid;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static com.ihg.automation.selenium.matchers.text.EntityMatcherFactory.isValue;
import static com.ihg.automation.selenium.servicecenter.pages.history.AuditConstant.CreditCard.CREDIT_CARD;
import static com.ihg.automation.selenium.servicecenter.pages.history.AuditConstant.CreditCard.EXPIRATION_DATE;
import static com.ihg.automation.selenium.servicecenter.pages.history.AuditConstant.CreditCard.NUMBER;
import static com.ihg.automation.selenium.servicecenter.pages.history.AuditConstant.CreditCard.TYPE;
import static com.ihg.automation.selenium.servicecenter.pages.history.AuditConstant.TravelProfile.DEFAULT;
import static com.ihg.automation.selenium.servicecenter.pages.history.AuditConstant.TravelProfile.TRAVEL_PROFILE;
import static org.hamcrest.core.Is.is;

import org.joda.time.YearMonth;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.member.AmbassadorOfferCode;
import com.ihg.automation.selenium.common.member.AmbassadorUtils;
import com.ihg.automation.selenium.common.member.CreditCard;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.preferences.TravelProfileType;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.gwt.components.Mode;
import com.ihg.automation.selenium.gwt.components.dialog.ConfirmDialog;
import com.ihg.automation.selenium.matchers.component.ComponentMatcher;
import com.ihg.automation.selenium.servicecenter.pages.LeftPanel;
import com.ihg.automation.selenium.servicecenter.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.servicecenter.pages.history.ProfileHistoryGridRow;
import com.ihg.automation.selenium.servicecenter.pages.history.ProfileHistoryPage;
import com.ihg.automation.selenium.servicecenter.pages.personal.AddressContactList;
import com.ihg.automation.selenium.servicecenter.pages.personal.PersonalInfoPage;
import com.ihg.automation.selenium.servicecenter.pages.preferences.AddTravelProfilePopUp;
import com.ihg.automation.selenium.servicecenter.pages.preferences.CreditCardFields;
import com.ihg.automation.selenium.servicecenter.pages.preferences.EditTravelProfilePopUp;
import com.ihg.automation.selenium.servicecenter.pages.preferences.StayPreferencesPage;
import com.ihg.automation.selenium.servicecenter.pages.preferences.TravelProfile;
import com.ihg.automation.selenium.servicecenter.pages.preferences.TravelProfileGridRow;
import com.ihg.automation.selenium.servicecenter.pages.preferences.TravelProfilePopUpBase;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.ExpirationDate;
import com.ihg.crm.automation.selenium.tests.servicecenter.LoginLogout;

public class CreditCardOnTravelProfileTest extends LoginLogout
{
    private static final String SUPP_TP = "Test";
    private static final String NEGATIVE = "Negative";
    private Member member = new Member();
    private CreditCard creditCardMasterCard;
    private CreditCard creditCardVisa;
    private CreditCard creditCardDiscover;
    private StayPreferencesPage stayPreferencesPage = new StayPreferencesPage();
    private TravelProfilePopUpBase addTravelProfilePopUp = new AddTravelProfilePopUp();
    private TravelProfilePopUpBase editTravelProfilePopUp = new EditTravelProfilePopUp();

    @BeforeClass
    public void beforeClass()
    {
        member.addProgram(Program.RC);
        member.addAddress(MemberPopulateHelper.getUnitedStatesAddress());
        member.setPersonalInfo(MemberPopulateHelper.getSimplePersonalInfo());

        login();
        new EnrollmentPage().enroll(member);
    }

    @Test
    public void addCreditCardToSupplementaryTravelProfile()
    {
        creditCardMasterCard = new CreditCard(MASTER_CARD, "5128668288017734", YearMonth.now().plusYears(2));
        TravelProfile travelProfile = new TravelProfile(SUPP_TP, BUSINESS, true);

        travelProfile.setCreditCard(creditCardMasterCard);
        stayPreferencesPage.goTo();
        stayPreferencesPage.getAddTravelProfile().clickAndWait();
        addTravelProfilePopUp.getTravelProfileName().type(travelProfile.getProfileName());
        addTravelProfilePopUp.getType().select(travelProfile.getProfileType().getCodeWithValue());
        addTravelProfilePopUp.getCreditCardFields().populate(creditCardMasterCard);
        addTravelProfilePopUp.clickSave(verifyNoError(), verifySuccess(TravelProfilePopUpBase.SUCCESS_MESSAGE_CREATE));

        TravelProfileGridRow row = stayPreferencesPage.getTravelProfilesGrid().getRowByName(SUPP_TP);
        row.verify(travelProfile);
        editTravelProfilePopUp = row.openTravelProfile();

        CreditCardFields creditCardFields = editTravelProfilePopUp.getCreditCardFields();
        creditCardFields.verify(creditCardMasterCard);
        editTravelProfilePopUp.clickClose(verifyNoError());
    }

    @Test(dependsOnMethods = { "addCreditCardToSupplementaryTravelProfile" }, alwaysRun = true)
    public void verifyHistoryAfterAddCreditCardToSupplementaryTravelProfile()
    {
        ProfileHistoryPage historyPage = new ProfileHistoryPage();
        historyPage.goTo();
        verifyNoError();

        ProfileHistoryGridRow travelProfileRow = historyPage.getProfileHistoryGrid().getRow(1)
                .getSubRowByName(TRAVEL_PROFILE);

        travelProfileRow.getSubRowByName(DEFAULT).verifyAddAction("No");
        travelProfileRow
                .getSubRowByName(
                        com.ihg.automation.selenium.servicecenter.pages.history.AuditConstant.TravelProfile.TYPE)
                .verifyAddAction(ComponentMatcher.hasText(isValue(BUSINESS)));
        travelProfileRow
                .getSubRowByName(
                        com.ihg.automation.selenium.servicecenter.pages.history.AuditConstant.TravelProfile.NAME)
                .verifyAddAction(SUPP_TP);

        ProfileHistoryGridRow creditCardRow = travelProfileRow.getSubRowByName(CREDIT_CARD);

        creditCardRow.getSubRowByName(NUMBER).verifyAddAction(creditCardMasterCard.getMaskedNumber());
        creditCardRow.getSubRowByName(EXPIRATION_DATE)
                .verifyAddAction(creditCardMasterCard.getExpirationDate().toString(YYYY_MM));
        creditCardRow.getSubRowByName(TYPE).verifyAddAction(hasText(isValue(creditCardMasterCard.getType())));
    }

    @Test(dependsOnMethods = {
            "verifyHistoryAfterAddCreditCardToSupplementaryTravelProfile" }, description = "added due to the DE5608 - SCUI - Duplicate "
                    + "addresses are getting added to member profile when credit card is added which is preventing addon"
                    + " program enrollment", alwaysRun = true)
    public void verifyAddressAfterCreditCardAdding()
    {
        PersonalInfoPage persInfo = new PersonalInfoPage();
        persInfo.goTo();

        AddressContactList addressList = persInfo.getAddressList();
        addressList.getExpandedContact().verify(member.getPreferredAddress(), Mode.VIEW);
        verifyThat(addressList, addressList.getContactsCount(), is(1),
                "that there are no dublicate addresses after adding the credit card (DE5608)");
    }

    @Test(dependsOnMethods = { "verifyAddressAfterCreditCardAdding" }, alwaysRun = true)
    public void updateSupplementaryToNothing()
    {
        stayPreferencesPage.goTo();

        TravelProfileGridRow row = stayPreferencesPage.getTravelProfilesGrid().getRowByName(SUPP_TP);
        editTravelProfilePopUp = row.openTravelProfile();

        editTravelProfilePopUp.clickSave(verifyNoError(), verifyInfo(TravelProfilePopUpBase.NO_CHANGES));
        editTravelProfilePopUp.clickClose(verifyNoError());
    }

    @Test(dependsOnMethods = { "updateSupplementaryToNothing" }, alwaysRun = true)
    public void addCreditCardToDefaultProfileOnUpdate()
    {
        new LeftPanel().reOpenMemberProfile(member);
        stayPreferencesPage.goTo();

        creditCardVisa = new CreditCard(VISA, "4111111111111111", YearMonth.now().plusYears(3));
        TravelProfile travelProfile = TravelProfile.getDefaultTravelProfile();
        TravelProfileGridRow row = stayPreferencesPage.getTravelProfilesGrid()
                .getRowByName(TravelProfileType.LEISURE.getValue());

        editTravelProfilePopUp = row.openTravelProfile();
        editTravelProfilePopUp.getCreditCardFields().populate(creditCardVisa);
        editTravelProfilePopUp.clickSave(verifySuccess(TravelProfilePopUpBase.SUCCESS_MESSAGE_UPDATE));
        row = stayPreferencesPage.getTravelProfilesGrid().getRowByName(TravelProfileType.LEISURE.getValue());

        travelProfile.setCreditCard(creditCardVisa);
        row.verify(travelProfile);

        editTravelProfilePopUp = row.openTravelProfile();
        editTravelProfilePopUp.getCreditCardFields().verify(creditCardVisa);
        editTravelProfilePopUp.clickClose(verifyNoError());
    }

    @Test(dependsOnMethods = { "addCreditCardToDefaultProfileOnUpdate" }, alwaysRun = true)
    public void verifyHistoryAfterAddCreditCardOnUpdate()
    {
        ProfileHistoryPage historyPage = new ProfileHistoryPage();
        historyPage.goTo();
        verifyNoError();

        ProfileHistoryGridRow travelProfileRow = historyPage.getProfileHistoryGrid().getRow(1)
                .getSubRowByName(TRAVEL_PROFILE).getSubRowByName(CREDIT_CARD);

        travelProfileRow.getSubRowByName(NUMBER).verifyAddAction(creditCardVisa.getMaskedNumber());
        travelProfileRow.getSubRowByName(EXPIRATION_DATE)
                .verifyAddAction(creditCardVisa.getExpirationDate().toString(YYYY_MM));
        travelProfileRow.getSubRowByName(TYPE).verifyAddAction(hasText(isValue(creditCardVisa.getType())));
    }

    @Test(dependsOnMethods = { "verifyHistoryAfterAddCreditCardOnUpdate" }, alwaysRun = true)
    public void updateToExpiredDate()
    {
        new LeftPanel().reOpenMemberProfile(member);

        CreditCard creditCardDiscover = new CreditCard(DISCOVER_CARD, "6011644005987788",
                YearMonth.now().minusMonths(1));

        stayPreferencesPage.goTo();
        TravelProfileGridRow row = stayPreferencesPage.getTravelProfilesGrid()
                .getRowByName(TravelProfileType.LEISURE.getValue());
        editTravelProfilePopUp = row.openTravelProfile();
        editTravelProfilePopUp.getCreditCardFields().populate(creditCardDiscover);
        editTravelProfilePopUp.clickSave();

        ExpirationDate expirationDate = editTravelProfilePopUp.getCreditCardFields().getExpirationDate();
        verifyThat(expirationDate.getMonth(), isHighlightedAsInvalid(true));
        verifyThat(expirationDate.getYear(), isHighlightedAsInvalid(true));
    }

    @Test(dependsOnMethods = { "updateToExpiredDate" }, alwaysRun = true)
    public void updateToInvalidType()
    {
        TravelProfile travelProfile = TravelProfile.getDefaultTravelProfile();
        CreditCard creditCardInvalidType = new CreditCard(VISA, "6011644005987788", YearMonth.now().plusYears(2));
        creditCardDiscover = new CreditCard(DISCOVER_CARD, "6011644005987788", YearMonth.now().plusYears(2));
        travelProfile.setCreditCard(creditCardDiscover);

        editTravelProfilePopUp.getCreditCardFields().populate(creditCardInvalidType);
        editTravelProfilePopUp.clickSave(verifySuccess(TravelProfilePopUpBase.SUCCESS_MESSAGE_UPDATE));

        TravelProfileGridRow row = stayPreferencesPage.getTravelProfilesGrid()
                .getRowByName(TravelProfileType.LEISURE.getValue());
        row.verify(travelProfile);

        editTravelProfilePopUp = row.openTravelProfile();
        editTravelProfilePopUp.getCreditCardFields().verify(creditCardDiscover);
        editTravelProfilePopUp.clickClose(verifyNoError());
    }

    @Test(dependsOnMethods = { "updateToInvalidType" }, description = "DE5847 - Incorrect History record "
            + "for updated Corporate Account Number", alwaysRun = true)
    public void verifyHistoryAfterUpdatingCreditCardType()
    {
        ProfileHistoryPage historyPage = new ProfileHistoryPage();
        historyPage.goTo();
        verifyNoError();

        ProfileHistoryGridRow travelProfileRow = historyPage.getProfileHistoryGrid().getRow(1)
                .getSubRowByName(TRAVEL_PROFILE).getSubRowByName(CREDIT_CARD);

        travelProfileRow.getSubRowByName(NUMBER).verify(creditCardVisa.getMaskedNumber(),
                creditCardDiscover.getMaskedNumber());
        travelProfileRow.getSubRowByName(EXPIRATION_DATE).verify(creditCardVisa.getExpirationDate().toString(YYYY_MM),
                creditCardDiscover.getExpirationDate().toString(YYYY_MM));
        travelProfileRow.getSubRowByName(TYPE).verify(hasText(isValue(creditCardVisa.getType())),
                hasText(isValue(creditCardDiscover.getType())));
    }

    @Test(dependsOnMethods = { "verifyHistoryAfterUpdatingCreditCardType" }, alwaysRun = true)
    public void doNotAddCreditCardNumberAndSave()
    {
        CreditCard noCcNumber = new CreditCard(DISCOVER_CARD, "", YearMonth.now().plusYears(2));

        stayPreferencesPage.goTo();
        stayPreferencesPage.getAddTravelProfile().clickAndWait();

        addTravelProfilePopUp.getTravelProfileName().type(NEGATIVE);
        addTravelProfilePopUp.getType().select(TravelProfileType.BUSINESS_GROUP.getCodeWithValue());

        CreditCardFields creditCardFieldsAdd = addTravelProfilePopUp.getCreditCardFields();
        creditCardFieldsAdd.populate(noCcNumber);
        addTravelProfilePopUp.clickSave();
        CreditCardFields creditCardFieldsEdit = editTravelProfilePopUp.getCreditCardFields();
        verifyThat(creditCardFieldsEdit.getCreditCardNumber(), isHighlightedAsInvalid(true));
    }

    @Test(dependsOnMethods = { "doNotAddCreditCardNumberAndSave" }, alwaysRun = true)
    public void addCreditCardWithoutType()
    {
        CreditCardFields creditCardFieldsEdit = editTravelProfilePopUp.getCreditCardFields();

        creditCardFieldsEdit.getCreditCardNumber().type("41111111111111");
        creditCardFieldsEdit.getCreditCardType().select("");
        creditCardFieldsEdit.getExpirationDate().getMonth().select("05");
        creditCardFieldsEdit.getExpirationDate().getYear().select("2018");
        addTravelProfilePopUp.clickSave();

        verifyThat(creditCardFieldsEdit.getCreditCardType(), isHighlightedAsInvalid(true));
    }

    @Test(dependsOnMethods = { "addCreditCardWithoutType" }, alwaysRun = true)
    public void addInvalidCreditCardNumber()
    {
        CreditCard invalidCcNumber = new CreditCard(DISCOVER_CARD, "****************", YearMonth.now().plusYears(2));

        CreditCardFields creditCardFieldsEdit = editTravelProfilePopUp.getCreditCardFields();
        creditCardFieldsEdit.populate(invalidCcNumber);
        editTravelProfilePopUp.clickSave();

        verifyThat(creditCardFieldsEdit.getCreditCardNumber(), isHighlightedAsInvalid(true));
        editTravelProfilePopUp.clickClose(verifyNoError());
    }

    @Test(dependsOnMethods = { "addInvalidCreditCardNumber" }, alwaysRun = true)
    public void checkAbandonMessageClickNo()
    {
        CreditCardFields creditCardFieldsEdit = editTravelProfilePopUp.getCreditCardFields();

        ConfirmDialog dialog = new ConfirmDialog();
        verifyThat(dialog, isDisplayedWithWait());
        verifyThat(dialog, hasText(TravelProfilePopUpBase.ABANDON_MESSAGE));
        dialog.clickNo(verifyNoError());
        verifyThat(dialog, displayed(false));
        verifyThat(creditCardFieldsEdit.getCreditCardNumber(), isHighlightedAsInvalid(true));
    }

    @Test(dependsOnMethods = { "checkAbandonMessageClickNo" }, alwaysRun = true)
    public void checkAbandonMessageClickYes()
    {
        editTravelProfilePopUp.clickClose(verifyNoError());
        ConfirmDialog dialogOne = new ConfirmDialog();
        verifyThat(dialogOne, isDisplayedWithWait());
        verifyThat(dialogOne, hasText(TravelProfilePopUpBase.ABANDON_MESSAGE));
        dialogOne.clickYes(verifyNoError());
        verifyThat(dialogOne, displayed(false));
        verifyThat(stayPreferencesPage, displayed(true));
    }

    @Test(dependsOnMethods = { "checkAbandonMessageClickYes" }, alwaysRun = true)
    public void verifyChangesWereNotSaved()
    {
        CreditCard creditCardDiscover = new CreditCard(DISCOVER_CARD, "6011644005987788", YearMonth.now().plusYears(2));
        TravelProfile travelProfile = TravelProfile.getDefaultTravelProfile();

        travelProfile.setCreditCard(creditCardDiscover);
        TravelProfileGridRow row = stayPreferencesPage.getTravelProfilesGrid()
                .getRowByName(TravelProfileType.LEISURE.getValue());
        row.verify(travelProfile);
    }

    @Test(dependsOnMethods = { "verifyChangesWereNotSaved" }, alwaysRun = true)
    public void doNotRemoveCreditCardFromDefaultTravelProfile()
    {
        CreditCard creditCardDiscover = new CreditCard(DISCOVER_CARD, "6011644005987788", YearMonth.now().plusYears(2));
        TravelProfile travelProfile = TravelProfile.getDefaultTravelProfile();

        TravelProfileGridRow row = stayPreferencesPage.getTravelProfilesGrid()
                .getRowByName(TravelProfileType.LEISURE.getValue());
        editTravelProfilePopUp = row.openTravelProfile();

        CreditCardFields creditCardFields = editTravelProfilePopUp.getCreditCardFields();
        creditCardFields.clickClear();
        creditCardFields.verifyEmptyFields();
        editTravelProfilePopUp.clickClose(verifyNoError());

        ConfirmDialog dialog = new ConfirmDialog();
        dialog.clickYes(verifyNoError());
        TravelProfileGridRow rowUpdated = stayPreferencesPage.getTravelProfilesGrid()
                .getRowByName(TravelProfileType.LEISURE.getValue());

        travelProfile.setCreditCard(creditCardDiscover);
        rowUpdated.verify(travelProfile);
    }

    @Test(dependsOnMethods = { "doNotRemoveCreditCardFromDefaultTravelProfile" }, alwaysRun = true)
    public void removeCreditCardFromDefaultTravelProfile()
    {
        new LeftPanel().reOpenMemberProfile(member);
        stayPreferencesPage.goTo();

        CreditCard creditCardEmpty = new CreditCard(null, "", null);
        TravelProfile travelProfile = TravelProfile.getDefaultTravelProfile();

        TravelProfileGridRow row = stayPreferencesPage.getTravelProfilesGrid()
                .getRowByName(TravelProfileType.LEISURE.getValue());
        editTravelProfilePopUp = row.openTravelProfile();

        CreditCardFields creditCardFields = editTravelProfilePopUp.getCreditCardFields();
        creditCardFields.clickClear();
        creditCardFields.verifyEmptyFields();
        editTravelProfilePopUp.clickSave(verifyNoError(), verifySuccess(TravelProfilePopUpBase.SUCCESS_MESSAGE_UPDATE));

        travelProfile.setCreditCard(creditCardEmpty);
        TravelProfileGridRow rowRemoved = stayPreferencesPage.getTravelProfilesGrid()
                .getRowByName(TravelProfileType.LEISURE.getValue());
        rowRemoved.verify(travelProfile);

        editTravelProfilePopUp = rowRemoved.openTravelProfile();
        creditCardFields.verifyEmptyFields();
        editTravelProfilePopUp.clickClose(verifyNoError());
    }

    @Test(dependsOnMethods = { "removeCreditCardFromDefaultTravelProfile" }, alwaysRun = true)
    public void verifyHistoryAfterRemovingCreditCardType()
    {
        ProfileHistoryPage historyPage = new ProfileHistoryPage();
        historyPage.goTo();
        verifyNoError();

        ProfileHistoryGridRow travelProfileRow = historyPage.getProfileHistoryGrid().getRow(1)
                .getSubRowByName(TRAVEL_PROFILE).getSubRowByName(CREDIT_CARD);

        travelProfileRow.getSubRowByName(NUMBER).verifyDeleteAction(creditCardDiscover.getMaskedNumber());
        travelProfileRow.getSubRowByName(EXPIRATION_DATE)
                .verifyDeleteAction(creditCardDiscover.getExpirationDate().toString(YYYY_MM));
        travelProfileRow.getSubRowByName(TYPE).verifyDeleteAction(hasText(isValue(creditCardDiscover.getType())));
    }

    @Test(dependsOnMethods = {
            "verifyHistoryAfterRemovingCreditCardType" }, description = "added due to the DE5608 - SCUI - Duplicate "
                    + "addresses are getting added to member profile when credit card is added which is preventing addon"
                    + " program enrollment", alwaysRun = true)
    public void enrollToAmbassador()
    {
        member.setAmbassadorOfferCode(AmbassadorOfferCode.CHSRO);
        new EnrollmentPage().enrollToDROrAMBProgramWithoutProfileChange(member, AMB,
                AmbassadorUtils.AMBASSADOR_COMP_PROMO_$0);
    }
}
