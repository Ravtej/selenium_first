package com.ihg.crm.automation.selenium.tests.servicecenter.uniqueemail;

import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyError;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.isHighlightedAsInvalid;
import static com.ihg.automation.selenium.matchers.component.HasTipText.hasWarningTipTextWithWait;
import static com.ihg.automation.selenium.servicecenter.pages.Messages.EMAIL_ADDRESS_ALREADY_EXISTS;
import static com.ihg.automation.selenium.servicecenter.pages.Messages.HIGHLIGHTED_FIELDS_DID_NOT_PASS_VALIDATION;
import static com.ihg.automation.selenium.common.enroll.EmailConstant.EMAIL_WITH_10_DUPLICATES;
import static org.hamcrest.core.AllOf.allOf;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.member.MemberJdbcUtils;
import com.ihg.automation.selenium.servicecenter.pages.GuestSearch;
import com.ihg.automation.selenium.servicecenter.pages.catalog.CatalogPage;
import com.ihg.automation.selenium.servicecenter.pages.order.OrderSummaryPopUp;
import com.ihg.crm.automation.selenium.tests.servicecenter.LoginLogout;

public class DuplicateEmailOnCatalogPageTest extends LoginLogout
{
    @BeforeClass
    public void before()
    {
        login();
        new GuestSearch().byMemberNumber(MemberJdbcUtils.getMemberIdWithoutEmail(jdbcTemplate));
    }

    @Test
    public void searchAndCartWithSavingEmail()
    {
        CatalogPage catalogPage = new CatalogPage();
        catalogPage.goTo();
        catalogPage.getButtonBar().clickSearch();

        OrderSummaryPopUp orderSummaryPopUp = catalogPage.openCatalogItemDetailsPopUp().openOrderSummaryPopUp();
        orderSummaryPopUp.getSelectEmail().type(EMAIL_WITH_10_DUPLICATES);

        orderSummaryPopUp.getSaveEmail().click();
        orderSummaryPopUp.clickCheckout(verifyError(HIGHLIGHTED_FIELDS_DID_NOT_PASS_VALIDATION));

        verifyThat(orderSummaryPopUp.getSelectEmail(),
                allOf(isHighlightedAsInvalid(true), hasWarningTipTextWithWait(EMAIL_ADDRESS_ALREADY_EXISTS)));
    }
}
