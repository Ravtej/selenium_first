package com.ihg.crm.automation.selenium.tests.servicecenter.signoff;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.isDisplayedWithWait;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.pages.login.SingleSignOn;
import com.ihg.automation.selenium.common.pages.login.TopUserSection;
import com.ihg.automation.selenium.gwt.components.dialog.ConfirmDialog;
import com.ihg.automation.selenium.servicecenter.pages.GuestSearch;
import com.ihg.automation.selenium.servicecenter.pages.Role;
import com.ihg.automation.selenium.servicecenter.pages.personal.PersonalInfoPage;
import com.ihg.crm.automation.selenium.tests.servicecenter.LoginLogout;

public class SignOffTest extends LoginLogout
{
    @BeforeMethod
    public void beforeMethod()
    {
        helper.login(user10T2, Role.SYSTEMS_MANAGER, false);
    }

    @AfterMethod
    public void after()
    {
        logout();
    }

    @DataProvider(name = "signOffProvider")
    protected Object[][] signOffProvider()
    {
        return new Object[][] { { true }, { false } };
    }

    @DataProvider(name = "signOffFromEditModeProvider")
    protected Object[][] signOffFromEditModeProvider()
    {
        return new Object[][] { { memberID, false }, { memberID, true } };
    }

    @Test(dataProvider = "signOffProvider")
    public void simpleSignOff(boolean bConfirm)
    {
        TopUserSection userSection = new TopUserSection();
        verifyThat(userSection.getSignOff(), isDisplayedWithWait());
        ConfirmDialog dialog = userSection.clickSignOff();
        verifyThat(dialog, isDisplayedWithWait());
        verifyThat(dialog, hasText("Are you sure you want to sign off?"));
        dialog.close(bConfirm);
        verifyThat(new SingleSignOn().getPassword(), displayed(bConfirm, bConfirm));
    }

    @Test(dataProvider = "signOffFromEditModeProvider")
    public void signOffFromEditMode(String memberNumber, boolean doneAssisting)
    {
        GuestSearch search = new GuestSearch();
        search.byMemberNumber(memberNumber);
        PersonalInfoPage page = new PersonalInfoPage();
        page.getCustomerInfo().clickEdit();
        TopUserSection userSection = new TopUserSection();
        ConfirmDialog dialog = userSection.clickSignOff();
        verifyThat(dialog, isDisplayedWithWait());
        verifyThat(dialog, hasText("Are you sure you want to sign off?"));
        dialog.close(true);
        dialog = new ConfirmDialog();
        verifyThat(dialog, hasText("Are you done assisting this guest?"));
        dialog.close(doneAssisting);
        verifyThat(new SingleSignOn().getPassword(), displayed(doneAssisting, doneAssisting));
    }
}
