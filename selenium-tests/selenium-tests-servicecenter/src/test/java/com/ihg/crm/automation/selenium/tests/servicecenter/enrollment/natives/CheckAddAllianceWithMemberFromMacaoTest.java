package com.ihg.crm.automation.selenium.tests.servicecenter.enrollment.natives;

import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifySuccess;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.components.NativeLanguage.SIMPLIFIED_CHINESE;
import static com.ihg.automation.selenium.common.components.NativeLanguage.TRADITIONAL_CHINESE;
import static com.ihg.automation.selenium.matchers.component.GridSize.size;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static org.hamcrest.core.StringContains.containsString;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.components.Country;
import com.ihg.automation.selenium.common.contact.Address;
import com.ihg.automation.selenium.common.member.GuestAddress;
import com.ihg.automation.selenium.common.member.GuestName;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberAlliance;
import com.ihg.automation.selenium.common.name.PersonNameFields;
import com.ihg.automation.selenium.common.types.Alliance;
import com.ihg.automation.selenium.common.types.address.AddressType;
import com.ihg.automation.selenium.common.types.name.NameConfiguration;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.gwt.components.Mode;
import com.ihg.automation.selenium.gwt.components.dialog.ConfirmDialog;
import com.ihg.automation.selenium.servicecenter.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.servicecenter.pages.personal.AddressContact;
import com.ihg.automation.selenium.servicecenter.pages.personal.CustomerInfoFields;
import com.ihg.automation.selenium.servicecenter.pages.personal.PersonalInfoPage;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.RewardClubPage;
import com.ihg.crm.automation.selenium.tests.servicecenter.LoginLogout;

public class CheckAddAllianceWithMemberFromMacaoTest extends LoginLogout
{
    private EnrollmentPage enrollmentPage = new EnrollmentPage();
    private GuestName name = new GuestName();
    private GuestName nativeName = new GuestName();
    private GuestAddress address = new GuestAddress();
    private GuestAddress nativeAddress = new GuestAddress();
    private RewardClubPage rewardClubPage = new RewardClubPage();
    private Alliance alliance = Alliance.SUA;
    private static final String allianceNumber = "50101074";
    private PersonalInfoPage persInfo = new PersonalInfoPage();
    private MemberAlliance memberAlliance = new MemberAlliance(alliance, allianceNumber, name);
    private Member member = new Member();

    @BeforeClass
    public void before()
    {
        nativeName.setGiven("名");
        nativeName.setSurname("姓");
        nativeName.setNativeLanguage(SIMPLIFIED_CHINESE);

        name.setGiven("ming");
        name.setSurname("xing");
        name.setNativeName(nativeName);

        nativeAddress.setLine1("中山区中山北路10");
        nativeAddress.setRegion2("北");

        address.setCountryCode(Country.MO);
        address.setType(AddressType.RESIDENCE);
        address.setRegion2("bei");
        address.setLine1("zhong shan qu zhong shan bei lu 10");

        address.setNativeAddress(nativeAddress);

        login();
    }

    @Test
    public void populateNameNativeFields()
    {
        enrollmentPage.goTo();
        enrollmentPage.getPrograms().selectProgram(Program.RC);
        enrollmentPage.getCustomerInfo().getResidenceCountry().selectByCode(Country.MO);
        PersonNameFields nameFields = enrollmentPage.getCustomerInfo().getName();
        nameFields.setConfiguration(Country.MO);
        nameFields.populateNativeFields(name);
        nameFields.verify(name, Mode.EDIT);
    }

    @Test(dependsOnMethods = { "populateNameNativeFields" })
    public void populateAddressNativeFields()
    {
        Address addressFields = enrollmentPage.getAddress();
        verifyThat(addressFields, hasText(containsString(TRADITIONAL_CHINESE)));
        addressFields.populateNativeFields(address);
        addressFields.verify(address, Mode.EDIT);
    }

    @Test(dependsOnMethods = { "populateAddressNativeFields" })
    public void completeEnrollment()
    {
        enrollmentPage.completeEnrollWithDuplicatePopUp(member);
    }

    @Test(dependsOnMethods = { "completeEnrollment" })
    public void addAlliance()
    {
        rewardClubPage.addAlliance(memberAlliance);
        rewardClubPage.getAlliances().getRow(1).verify(memberAlliance);
    }

    @Test(dependsOnMethods = { "addAlliance" })
    public void editCustomerInfoDetails()
    {
        persInfo.goTo();
        name.setSurname("XingXing");

        CustomerInfoFields customerInfoFields = persInfo.getCustomerInfo();
        PersonNameFields personNameFields = customerInfoFields.getName();
        customerInfoFields.clickEdit();
        personNameFields.setConfiguration(Country.TW);
        personNameFields.getSurname().type(name.getSurname());

        customerInfoFields.clickSave();
        verifyThat(personNameFields.getFullName(),
                hasText(name.getFullName(NameConfiguration.getEasternAsiaConfiguration())));
    }

    @Test(dependsOnMethods = { "editCustomerInfoDetails" })
    public void deleteAlliance()
    {
        rewardClubPage.goTo();
        rewardClubPage.getAlliances().getRow(alliance).delete();
        new ConfirmDialog().clickYes(verifySuccess("Program info has been successfully saved."));
        verifyThat(rewardClubPage.getAlliances(), size(0));
    }

    @Test(dependsOnMethods = { "deleteAlliance" })
    public void verifyAddressDetailsInEditMode()
    {
        PersonalInfoPage persInfo = new PersonalInfoPage();
        persInfo.goTo();

        AddressContact contact = persInfo.getAddressList().getContact();
        contact.clickEdit();
        contact.getBaseContact().verifyNativeFieldsBase(TRADITIONAL_CHINESE);
    }

}
