package com.ihg.crm.automation.selenium.tests.servicecenter.thresholder;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.event.EventTransactionType.TierLevel.UPGRADE_ACHIEVE;
import static com.ihg.automation.selenium.common.event.EventTransactionType.TierLevel.UPGRADE_MAINTAIN;
import static com.ihg.automation.selenium.common.types.program.RewardClubLevel.CLUB;
import static com.ihg.automation.selenium.common.types.program.RewardClubLevel.PLTN;
import static com.ihg.automation.selenium.common.types.program.RewardClubLevel.SPRE;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.stay.Stay;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.servicecenter.pages.LeftPanel;
import com.ihg.automation.selenium.servicecenter.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsPage;
import com.ihg.automation.selenium.servicecenter.pages.event.stay.StayEventsPage;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.RewardClubPage;
import com.ihg.automation.selenium.tests.common.RulesDBUtils;
import com.ihg.crm.automation.selenium.tests.servicecenter.LoginLogout;

public class AchieveMaintainPlatinumByNightsTest extends LoginLogout
{
    private Stay stayInPreviousYear = new Stay();
    private Stay stayInCurrentYear;
    private static int nightsToMaintainPlatinum;
    private static int nightsToAchieveSpire;
    private static int nightsToAchieveSpirePrevious;
    private static int nightsToAchievePlatinumPrevious;

    @BeforeClass
    public void before()
    {
        RulesDBUtils rulesDBUtils = new RulesDBUtils(jdbcTemplate);
        nightsToMaintainPlatinum = rulesDBUtils.getCurrentYearRuleNights(PLTN, PLTN);
        nightsToAchieveSpire = rulesDBUtils.getCurrentYearRuleNights(PLTN, SPRE);
        nightsToAchieveSpirePrevious = rulesDBUtils.getPreviousYearRuleNights(PLTN, SPRE);
        nightsToAchievePlatinumPrevious = rulesDBUtils.getPreviousYearRuleNights(CLUB, PLTN);

        stayInPreviousYear.setHotelCode("ATLCP");
        stayInPreviousYear.setCheckInOutByNightsInPreviousYear(nightsToAchievePlatinumPrevious);
        stayInPreviousYear.setAvgRoomRate(15.00);
        stayInPreviousYear.setRateCode("Test");

        stayInCurrentYear = stayInPreviousYear.clone();
        stayInCurrentYear.setCheckInOutByNights(nightsToMaintainPlatinum);

        Member member = new Member();
        member.addProgram(Program.RC);
        member.setPersonalInfo(MemberPopulateHelper.getSimplePersonalInfo());
        member.addAddress(MemberPopulateHelper.getUnitedStatesAddress());

        login();

        new EnrollmentPage().enroll(member);

        new LeftPanel().verifyRCLevel(CLUB);
    }

    @Test
    public void achievePlatinumTierLevel()
    {
        new StayEventsPage().createStay(stayInPreviousYear);
        new LeftPanel().verifyRCLevel(PLTN);

        RewardClubPage rewardClubPage = new RewardClubPage();
        rewardClubPage.goTo();

        rewardClubPage.getPreviousYearAnnualActivities().getNightsRow().verify(nightsToAchievePlatinumPrevious,
                nightsToAchieveSpirePrevious);

        rewardClubPage.getCurrentYearAnnualActivities().getNightsRow().verify(0, nightsToAchieveSpire,
                nightsToMaintainPlatinum);

        AllEventsPage allEventsPage = new AllEventsPage();
        allEventsPage.goTo();

        verifyThat(allEventsPage.getGrid().getRow(UPGRADE_ACHIEVE, "RC - PLATINUM"), displayed(true));
    }

    @Test(dependsOnMethods = { "achievePlatinumTierLevel" }, alwaysRun = true)
    public void maintainPlatinumTierLevel()
    {
        new StayEventsPage().createStay(stayInCurrentYear);

        RewardClubPage rewardClubPage = new RewardClubPage();
        rewardClubPage.goTo();

        rewardClubPage.getCurrentYearAnnualActivities().getNightsRow()
                .verifyMaintainIsAchieved(nightsToMaintainPlatinum, nightsToAchieveSpire);

        AllEventsPage allEventsPage = new AllEventsPage();
        allEventsPage.goTo();
        verifyThat(allEventsPage.getGrid().getRow(UPGRADE_MAINTAIN, "RC - PLATINUM"), displayed(true));
    }
}
