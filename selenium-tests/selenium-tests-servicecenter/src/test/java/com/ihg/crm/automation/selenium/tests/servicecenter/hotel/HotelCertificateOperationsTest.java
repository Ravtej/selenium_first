package com.ihg.crm.automation.selenium.tests.servicecenter.hotel;

import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyError;
import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyNoError;
import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifySuccess;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.assertThat;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.hotel.HotelCertificateStatus.ACCEPTED;
import static com.ihg.automation.selenium.common.hotel.HotelCertificateStatus.CANCELED;
import static com.ihg.automation.selenium.common.hotel.HotelCertificateStatus.PAID;
import static com.ihg.automation.selenium.matchers.component.GridSize.size;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static com.ihg.automation.selenium.servicecenter.pages.hotel.certificate.HotelCertificatePage.CERTIFICATES_SUCCESSFULLY_CANCELED;
import static com.ihg.automation.selenium.servicecenter.pages.hotel.certificate.HotelCertificatePage.RECORDS_CAN_NOT_BE_ACCEPTED;
import static com.ihg.automation.selenium.servicecenter.pages.hotel.certificate.HotelCertificatePage.RECORDS_CAN_NOT_BE_CANCELED;
import static com.ihg.automation.selenium.servicecenter.pages.hotel.certificate.HotelCertificateSearchGridRow.HotelCertificateSearchGridRowCell.STATUS;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.ihg.automation.common.DateUtils;
import com.ihg.automation.common.RandomUtils;
import com.ihg.automation.selenium.common.hotel.HotelCertificate;
import com.ihg.automation.selenium.common.types.Currency;
import com.ihg.automation.selenium.servicecenter.pages.hotel.certificate.HotelCertificatePage;
import com.ihg.automation.selenium.servicecenter.pages.hotel.certificate.HotelCertificateSearchGrid;
import com.ihg.automation.selenium.servicecenter.pages.hotel.certificate.HotelCertificateSearchGridRow;
import com.ihg.crm.automation.selenium.tests.servicecenter.LoginLogout;

public class HotelCertificateOperationsTest extends LoginLogout
{
    private HotelCertificatePage hotelCertificatePage = new HotelCertificatePage();
    private HotelCertificateSearchGrid hotelCertificateSearchGrid;
    private HotelCertificateSearchGridRow hotelCertificateSearchGridRow;
    private HotelCertificate certificate = new HotelCertificate();
    private HotelCertificate certificateApproved = new HotelCertificate();

    private final static String ATLCP_HOTEL = "ATLCP";

    @BeforeClass
    public void beforeClass()
    {
        certificate.setHotelCode(ATLCP_HOTEL);
        certificate.setCheckInDate(DateUtils.getDateBackward(1));
        certificate.setCheckOutDate(DateUtils.getFormattedDate());
        certificate.setItemId("HQ250");
        certificate.setCertificateNumber(RandomUtils.getRandomNumber(8));
        certificate.setDescriptionGuestName("auto test certificate " + certificate.getCertificateNumber());
        certificate.setReimbursementAmount("100");
        certificate.setBillingEntity("CORPC");
        certificate.setCurrency(Currency.USD);

        certificateApproved = certificate.clone();
        certificateApproved.setCertificateNumber(RandomUtils.getRandomNumber(8));
        certificateApproved
                .setDescriptionGuestName("auto test certificate " + certificateApproved.getCertificateNumber());

        login();
        hotelCertificatePage.goTo();
        hotelCertificatePage.createApprovedCertificate(certificateApproved);
    }

    @Test
    public void cancelInitiatedCertificate()
    {
        hotelCertificatePage.createCertificate(certificate);
        hotelCertificatePage.search(certificate);
        hotelCertificateSearchGrid = hotelCertificatePage.getGrid();
        verifyThat(hotelCertificateSearchGrid, size(1));

        hotelCertificateSearchGrid.getRow(1).check();
        hotelCertificatePage.getButtonBar().clickCancelCertificate(verifyNoError(),
                verifySuccess(CERTIFICATES_SUCCESSFULLY_CANCELED));

        certificate.setStatus(CANCELED);
        hotelCertificateSearchGrid.getRow(1).verify(certificate);
    }

    @DataProvider(name = "tryToCancelCertificateProvider")
    public Object[][] tryToCancelCertificateProvider()
    {
        return new Object[][] { { CANCELED }, { PAID } };
    }

    @Test(dataProvider = "tryToCancelCertificateProvider")
    public void tryToCancelCertificate(String status)
    {
        hotelCertificatePage.search(ATLCP_HOTEL, null, null, null, status, null, null, null, null);

        hotelCertificateSearchGrid = hotelCertificatePage.getGrid();
        hotelCertificateSearchGridRow = hotelCertificateSearchGrid.getRow(1);

        assertThat(hotelCertificateSearchGridRow.getCell(STATUS), hasText(status));

        hotelCertificateSearchGridRow.check();
        hotelCertificatePage.getButtonBar().clickCancelCertificate(verifyError(RECORDS_CAN_NOT_BE_CANCELED));
    }

    @DataProvider(name = "tryToAcceptCertificateProvider")
    public Object[][] tryToAcceptCertificateProvider()
    {
        return new Object[][] { { ACCEPTED }, { PAID }, { CANCELED } };
    }

    @Test(dataProvider = "tryToAcceptCertificateProvider")
    public void tryToAcceptCertificate(String status)
    {
        hotelCertificatePage.search(ATLCP_HOTEL, null, null, null, status, null, null, null, null);

        hotelCertificateSearchGrid = hotelCertificatePage.getGrid();
        hotelCertificateSearchGridRow = hotelCertificateSearchGrid.getRow(1);

        assertThat(hotelCertificateSearchGridRow.getCell(STATUS), hasText(status));

        hotelCertificateSearchGridRow.check();
        hotelCertificatePage.getButtonBar().clickAccept(verifyError(RECORDS_CAN_NOT_BE_ACCEPTED));
    }

}
