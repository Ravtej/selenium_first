package com.ihg.crm.automation.selenium.tests.servicecenter.stay;

import static com.ihg.automation.common.DateUtils.DATE_FORMAT;
import static com.ihg.automation.common.DateUtils.currentYear;
import static com.ihg.automation.common.DateUtils.now;
import static com.ihg.automation.selenium.assertions.Assume.assumeThat;
import static com.ihg.automation.selenium.hotel.pages.hoteloperations.stay.lpu.missingstay.StayCreatePopUp.STAY_SUCCESS_CREATED;
import static com.ihg.automation.selenium.matchers.date.IsAfterDate.isAfter;

import org.joda.time.LocalDate;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.stay.Revenues;
import com.ihg.automation.selenium.common.stay.Stay;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.servicecenter.pages.LeftPanel;
import com.ihg.automation.selenium.servicecenter.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.servicecenter.pages.event.stay.StayEventsPage;
import com.ihg.automation.selenium.servicecenter.pages.event.stay.StayGuestGridRow;
import com.ihg.automation.selenium.servicecenter.pages.event.stay.StayGuestGridRowContainer;
import com.ihg.crm.automation.selenium.tests.servicecenter.LoginLogout;

public class EnrollingStayTest extends LoginLogout
{
    private static final String HOTEL_CODE = "MIAHA";
    private static final String STAY_SUCCESS_SWITCH_FLAG = "Stay has been successfully created.\nEnrolling Stay Indicator set to YES by System Rules.";
    private static final String NO = "No";
    private static final String YES = "Yes";
    private static final int SHIFT_DAYS = 6;
    private static final LocalDate NEW_ENROLL_DATE = LocalDate.now().minusDays(SHIFT_DAYS);

    private Stay stay = new Stay();
    private StayEventsPage stayEventsPage = new StayEventsPage();

    // update enroll date in range SYSDATE - 6, so you'll be sure you can create
    // stay for member with checkout date >= ENROLL_DT - 3 (to avoid stay logic
    // restrictions)
    private static final String UPDATE_ENROLL_INFO_MBRSHP_SQL = "BEGIN " //
            + "UPDATE DGST.MBRSHP " //
            + "SET ENROLL_DT = SYSDATE - '%1$s', " //
            + "ORIG_ENROLL_DT = SYSDATE - '%1$s', " //
            + "LST_UPDT_USR_ID = '%2$s', " //
            + "LST_UPDT_TS = SYSDATE " //
            + "WHERE MBRSHP_ID = '%3$s';"

            + "UPDATE DGST.MBRSHP_ACTV " //
            + "SET SRC_CHNL_CD = 'MHUI', " //
            + "SRC_LOC_CD = '%4$s', " //
            + "SRC_TYP_CD = 'HOTEL', " //
            + "EVN_TS = SYSDATE - '%1$s', " //
            + "LST_UPDT_USR_ID = '%2$s', " //
            + "LST_UPDT_TS = SYSDATE " //
            + "WHERE MBRSHP_ID = '%3$s';" //
            + "COMMIT; " //
            + "END;";

    @BeforeClass
    public void beforeClass()
    {
        assumeThat(now(), isAfter(new LocalDate(currentYear(), 1, 4)), "Checkout date should be in current year");

        stay.setHotelCode(HOTEL_CODE);
        stay.setBrandCode("ICON");
        stay.setNights(1);
        stay.setQualifyingNights(1);
        stay.setTierLevelNights("1");
        stay.setIhgUnits("1,000");
        stay.setAvgRoomRate(100.00);
        stay.setRateCode("1");
        stay.setConfirmationNumber("n/a");
        stay.setFolioNumber("n/a");
        stay.setOverlappingStay("No");
        stay.setRoomNumber("3");
        stay.setRoomType("Loft");

        Revenues revenues = stay.getRevenues();
        revenues.getRoom().setAmount(100.0);
        revenues.getFood().setAmount(0.00);
        revenues.getBeverage().setAmount(0.00);
        revenues.getPhone().setAmount(0.00);
        revenues.getMeeting().setAmount(0.00);
        revenues.getMandatoryLoyalty().setAmount(0.00);
        revenues.getOtherLoyalty().setAmount(0.00);
        revenues.getNoLoyalty().setAmount(0.00);
        revenues.getTax().setAmount(0.00);
        revenues.getOverallTotal().setAmount(100.00);
        revenues.getTotalNonQualifying().setAmount(0.00);
        revenues.getTotalQualifying().setAmount(100.00);

        login();
    }

    @BeforeMethod
    private void prepareMember()
    {
        Member member = new Member();
        member.setPersonalInfo(MemberPopulateHelper.getSimplePersonalInfo());
        member.addAddress(MemberPopulateHelper.getUnitedStatesAddress());
        member.addProgram(Program.RC);

        new EnrollmentPage().enroll(member);

        String membershipId = member.getRCProgramId();
        jdbcTemplateUpdate.update(
                String.format(UPDATE_ENROLL_INFO_MBRSHP_SQL, SHIFT_DAYS, lastUpdateUserId, membershipId, HOTEL_CODE));
    }

    @AfterMethod
    public void closeMember()
    {
        new LeftPanel().goToNewSearch();
    }

    @Test(dataProvider = "checkInOutDataProvider", priority = 10)
    public void verifyEnrollIndicatorByCheckOut(int checkInShift, int checkOutShift, String successMessage,
            String enrollingIndicator)
    {
        stay.setCheckIn(NEW_ENROLL_DATE.plusDays(checkInShift).toString(DATE_FORMAT));
        stay.setCheckOut(NEW_ENROLL_DATE.plusDays(checkOutShift).toString(DATE_FORMAT));
        stay.setEnrollingStay("No");

        stayEventsPage.createStay(stay, successMessage);

        verifyStay(enrollingIndicator);
    }

    @Test(priority = 20)
    public void verifyEnrollIndicatorDatesInRange()
    {
        stay.setCheckIn(NEW_ENROLL_DATE.plusDays(-1).toString(DATE_FORMAT));
        stay.setCheckOut(NEW_ENROLL_DATE.plusDays(1).toString(DATE_FORMAT));
        stay.setEnrollingStay("No");

        stayEventsPage.createStay(stay, STAY_SUCCESS_SWITCH_FLAG);

        stay.setNights(2);
        stay.setQualifyingNights(2);
        stay.setTierLevelNights("2");
        stay.setAvgRoomRate(50.00);

        verifyStay(YES);
    }

    @DataProvider(name = "checkInOutDataProvider")
    public Object[][] checkInOutDataProvider()
    {
        return new Object[][] { { 2, 3, STAY_SUCCESS_CREATED, NO }, //
                { 1, 2, STAY_SUCCESS_SWITCH_FLAG, YES }, //
                { 0, 1, STAY_SUCCESS_SWITCH_FLAG, YES }, //
                { -2, -1, STAY_SUCCESS_SWITCH_FLAG, YES }, //
                { -3, -2, STAY_SUCCESS_CREATED, NO }, //
                { -1, 0, STAY_SUCCESS_SWITCH_FLAG, YES }//
        };
    }

    private void verifyStay(String indicatorValue)
    {
        stay.setEnrollingStay(indicatorValue);

        // verify Stay Row
        StayGuestGridRow stayGuestGridRow = stayEventsPage.getGuestGrid().getRow(1);
        stayGuestGridRow.verify(stay);

        // verify Stay in View Mode
        stayGuestGridRow.expand(StayGuestGridRowContainer.class).verify(stay, helper);
    }
}
