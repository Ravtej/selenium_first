package com.ihg.crm.automation.selenium.tests.servicecenter.commpref;

import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyNoError;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.communication.CommunicationPreferencesConstants.OPT_IN;
import static com.ihg.automation.selenium.common.communication.CommunicationPreferencesConstants.OPT_OUT;
import static com.ihg.automation.selenium.matchers.component.ViewableMatcher.hasTextInView;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.communication.ContactPermissionItems;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.servicecenter.pages.communication.CommunicationPreferences;
import com.ihg.automation.selenium.servicecenter.pages.communication.CommunicationPreferencesPage;
import com.ihg.automation.selenium.servicecenter.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.servicecenter.pages.personal.PersonalInfoPage;
import com.ihg.crm.automation.selenium.tests.servicecenter.LoginLogout;

public class VerifyStatusTest extends LoginLogout
{
    @BeforeClass
    public void beforeClass()
    {
        Member member = new Member();
        member.addProgram(Program.RC);
        member.setPersonalInfo(MemberPopulateHelper.getSimplePersonalInfo());
        member.addAddress(MemberPopulateHelper.getUnitedStatesAddress());

        login();
        new EnrollmentPage().enroll(member);
    }

    @Test
    public void verifyCommPrefsStatus()
    {
        virifyStatus(OPT_OUT, OPT_OUT);
    }

    @Test(dependsOnMethods = { "verifyCommPrefsStatus" }, alwaysRun = true)
    public void addEmailAndVerifyCommPrefsStatus()
    {
        PersonalInfoPage personalInfoPage = new PersonalInfoPage();
        personalInfoPage.goTo();
        personalInfoPage.getEmailList().add(MemberPopulateHelper.getRandomEmail(), verifyNoError());

        virifyStatus(OPT_IN, OPT_OUT);
    }

    @Test(dependsOnMethods = { "addEmailAndVerifyCommPrefsStatus" }, alwaysRun = true)
    public void addSmsAndVerifyCommPrefsStatus()
    {
        PersonalInfoPage personalInfoPage = new PersonalInfoPage();
        personalInfoPage.goTo();
        personalInfoPage.getSmsList().add(MemberPopulateHelper.getRandomPhone(), verifyNoError());

        virifyStatus(OPT_IN, OPT_IN);
    }

    private void virifyStatus(String mailStatus, String smsStatus)
    {
        CommunicationPreferencesPage commPrefPage = new CommunicationPreferencesPage();

        commPrefPage.goTo();

        CommunicationPreferences commPrefs = commPrefPage.getCommunicationPreferences();

        ContactPermissionItems marketingPermissionItems = commPrefs.getMarketingPreferences()
                .getContactPermissionItems();

        for (int index = 1; index <= marketingPermissionItems.getContactPermissionItemsCount(); index++)
        {
            verifyThat(marketingPermissionItems.getContactPermissionItem(index).getSubscribe(),
                    hasTextInView(mailStatus));
        }

        ContactPermissionItems smsPermissionItems = commPrefs.getSmsMobileTextingMarketingPreferences()
                .getContactPermissionItems();
        for (int index = 1; index <= smsPermissionItems.getContactPermissionItemsCount(); index++)
        {
            verifyThat(smsPermissionItems.getContactPermissionItem(index).getSubscribe(), hasTextInView(smsStatus));
        }
    }
}
