package com.ihg.crm.automation.selenium.tests.servicecenter.unit;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.isDisplayedWithWait;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;

import java.util.ArrayList;
import java.util.List;

import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.gwt.components.Link;
import com.ihg.automation.selenium.gwt.components.dialog.ConfirmDialog;
import com.ihg.automation.selenium.gwt.components.utils.WaitUtils;
import com.ihg.automation.selenium.servicecenter.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsGridRow;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsPage;
import com.ihg.automation.selenium.servicecenter.pages.event.unit.UnitGridRowContainer;
import com.ihg.automation.selenium.servicecenter.pages.personal.PersonalInfoPage;
import com.ihg.crm.automation.selenium.tests.servicecenter.LoginLogout;

public class UnitsCommon extends LoginLogout
{

    protected static List<String> getExpectedAmounts(int min, int max, int delta)
    {
        List<String> expectedContent = new ArrayList<String>();
        for (int i = min; i <= max; i += delta)
        {
            expectedContent.add(Integer.toString(i));
        }
        return expectedContent;
    }

    protected Member enrollMember()
    {
        Member member = new Member();
        member.addProgram(Program.RC);
        member.setPersonalInfo(MemberPopulateHelper.getSimplePersonalInfo());
        member.addAddress(MemberPopulateHelper.getUnitedStatesAddress());
        member = new EnrollmentPage().enroll(member);

        return member;
    }

    protected void openDetailsPopUp(String transType)
    {
        AllEventsPage allEvents = new AllEventsPage();
        allEvents.goTo();
        AllEventsGridRow row = allEvents.getGrid().getRow(transType);
        row.expand(UnitGridRowContainer.class).clickDetails();
    }

    protected void openRecipientProfileByLink(Member recipient, int row)
    {
        AllEventsPage allEvents = new AllEventsPage();
        allEvents.goTo();

        UnitGridRowContainer rowCont = allEvents.getGrid().getRow(row).expand(UnitGridRowContainer.class);

        Link relatedMemberId = rowCont.getUnitDetails().getRelatedMemberId();
        verifyThat(relatedMemberId, hasText(recipient.getRCProgramId()));
        relatedMemberId.clickAndWait();

        ConfirmDialog dialog = new ConfirmDialog();
        verifyThat(dialog, isDisplayedWithWait());
        verifyThat(dialog, hasText("Are you done assisting this guest?"));
        dialog.close(true);

        PersonalInfoPage personalPage = new PersonalInfoPage();
        verifyThat(personalPage, isDisplayedWithWait());
        new WaitUtils().waitExecutingRequest();
    }

}
