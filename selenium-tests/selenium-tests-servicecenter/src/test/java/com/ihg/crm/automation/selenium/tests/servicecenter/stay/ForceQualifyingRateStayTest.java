package com.ihg.crm.automation.selenium.tests.servicecenter.stay;

import static com.ihg.automation.common.DateUtils.currentYear;
import static com.ihg.automation.common.DateUtils.now;
import static com.ihg.automation.selenium.assertions.Assume.assumeThat;
import static com.ihg.automation.selenium.matchers.date.IsAfterDate.isAfter;
import static com.ihg.automation.selenium.servicecenter.pages.event.stay.StayEventsPage.FORCE_QUALIFY_REASON;

import org.joda.time.LocalDate;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.event.AllEventsRow;
import com.ihg.automation.selenium.common.event.AllEventsRowConverter;
import com.ihg.automation.selenium.common.member.AnnualActivity;
import com.ihg.automation.selenium.common.member.LifetimeActivity;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.stay.Revenue;
import com.ihg.automation.selenium.common.stay.Stay;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.common.types.program.RewardClubLevel;
import com.ihg.automation.selenium.gwt.components.Mode;
import com.ihg.automation.selenium.servicecenter.pages.LeftPanel;
import com.ihg.automation.selenium.servicecenter.pages.annual.AnnualActivityPage;
import com.ihg.automation.selenium.servicecenter.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsGridRow;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsPage;
import com.ihg.automation.selenium.servicecenter.pages.event.stay.StayEventsPage;
import com.ihg.automation.selenium.servicecenter.pages.event.stay.StayGuestGridRow;
import com.ihg.automation.selenium.servicecenter.pages.event.stay.StayGuestGridRowContainer;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.RewardClubPage;
import com.ihg.crm.automation.selenium.tests.servicecenter.LoginLogout;

public class ForceQualifyingRateStayTest extends LoginLogout
{
    private Member member = new Member();
    private Stay stay = new Stay();
    private StayEventsPage stayEventsPage = new StayEventsPage();
    private StayGuestGridRow stayGuestGridRow;
    private AllEventsGridRow allEventsGridRow;

    @BeforeClass
    public void beforeClass()
    {
        assumeThat(now(), isAfter(new LocalDate(currentYear(), 1, 4)), "Checkout date should be in current year");

        stay.setHotelCode("MIAHA");
        stay.setBrandCode("ICON");
        stay.setNights(2);
        stay.setCheckInOutByNights(2);
        stay.setQualifyingNights(2);
        stay.setTierLevelNights("2");
        stay.setIhgUnits("2,000");
        stay.setAvgRoomRate(100.00);
        stay.setRateCode("IBBOI");

        Revenue room = stay.getRevenues().getRoom();
        room.setAmount(200.00);
        room.setUsdAmount(200.00);
        room.setUsdAmount(200.00);
        room.setAmount(200.00);
        room.setAllowOverride(true);
        room.setQualifying(false);
        room.setBillHotel(false);
        room.setReasonCode("Additional Revenue");

        member.setPersonalInfo(MemberPopulateHelper.getSimplePersonalInfo());
        member.addAddress(MemberPopulateHelper.getUnitedStatesAddress());
        member.addProgram(Program.RC);

        login();

        member = new AnnualActivityPage().captureCounters(new EnrollmentPage().enroll(member), jdbcTemplate);
    }

    @Test(priority = 10)
    public void createForceQualifyingRateStay()
    {
        stayEventsPage.createStay(stay, String.format(FORCE_QUALIFY_REASON, "Force Qualifying Rate Category."));
    }

    @Test(priority = 20)
    public void verifyStayCreationEvent()
    {
        stayGuestGridRow = stayEventsPage.getGuestGrid().getRow(1);
        stayGuestGridRow.verify(stay);
    }

    @Test(priority = 30)
    public void verifyRevenueDetails()
    {
        // re-set 2 indicators to pass stay verification since both should
        // change their state after stay qualification
        Revenue room = stay.getRevenues().getRoom();
        room.setQualifying(true);
        room.setBillHotel(true);

        stayGuestGridRow.expand(StayGuestGridRowContainer.class).verifyRoomRevenueDetails(room, Mode.EDIT);
    }

    @Test(priority = 40)
    public void verifyStayCreateEvent()
    {
        AllEventsRow stayEvent = AllEventsRowConverter.convert(stay);

        AllEventsPage allEventsPage = new AllEventsPage();
        allEventsPage.goTo();
        allEventsGridRow = allEventsPage.getGrid().getRow(stayEvent.getTransType());
        allEventsGridRow.verify(stayEvent);
    }

    @Test(priority = 50)
    public void verifyAllEventsGridRowDetails()
    {
        allEventsGridRow.expand(StayGuestGridRowContainer.class).verifyRoomRevenueDetails(stay.getRevenues().getRoom(),
                Mode.VIEW);
    }

    @Test(priority = 60)
    public void verifyLeftPanelRCPointsBalance()
    {
        new LeftPanel().getProgramInfoPanel().getPrograms().getRewardClubProgram().verify(RewardClubLevel.CLUB,
                "2,000");
    }

    @Test(priority = 70)
    public void verifyActivityCounters()
    {
        AnnualActivity annualActivity = member.getAnnualActivity();
        annualActivity.setQualifiedNights(stay.getNights());
        annualActivity.setTierLevelNightsFromString(stay.getTierLevelNights());
        annualActivity.setTotalQualifiedUnitsFromString(stay.getIhgUnits());
        annualActivity.setTotalUnitsFromString(stay.getIhgUnits());

        LifetimeActivity lifetimeActivity = member.getLifetimeActivity();
        lifetimeActivity.setBaseUnitsFromString(stay.getIhgUnits());
        lifetimeActivity.setTotalUnitsFromString(stay.getIhgUnits());
        lifetimeActivity.setCurrentBalanceFromString(stay.getIhgUnits());

        new AnnualActivityPage().verifyCounters(member);
    }

    @Test(priority = 80)
    public void verifyRewardNightTierLevelCounters()
    {
        RewardClubPage rewardClubPage = new RewardClubPage();
        rewardClubPage.goTo();
        rewardClubPage.getCurrentYearAnnualActivities().getPointsRow().verifyCurrent("2000");
    }
}
