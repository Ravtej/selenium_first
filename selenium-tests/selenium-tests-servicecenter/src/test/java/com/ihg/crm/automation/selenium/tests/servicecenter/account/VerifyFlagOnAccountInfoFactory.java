package com.ihg.crm.automation.selenium.tests.servicecenter.account;

import static com.ihg.automation.selenium.common.types.program.TierLevelFlag.AMERICA_VIP_CUSTOMER;
import static com.ihg.automation.selenium.common.types.program.TierLevelFlag.AMEX_CENTURION;
import static com.ihg.automation.selenium.common.types.program.TierLevelFlag.CISCO;
import static com.ihg.automation.selenium.common.types.program.TierLevelFlag.INACTIVE_FOR_6_MONTHS;
import static com.ihg.automation.selenium.common.types.program.TierLevelFlag.POSSIBLE_FRAUD;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Factory;

import com.ihg.automation.selenium.common.member.AmbassadorOfferCode;
import com.ihg.automation.selenium.common.member.AmbassadorUtils;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.types.program.Program;

public class VerifyFlagOnAccountInfoFactory
{
    @DataProvider(name = "verifyFlagOnAccountInfoProvider")
    public Object[][] verifyFlagOnAccountInfoProvider()
    {
        Member memberRc = new Member();
        memberRc.addProgram(Program.RC);
        memberRc.addFlag(CISCO);
        memberRc.addFlag(AMERICA_VIP_CUSTOMER);

        Member memberAmb = new Member();
        memberAmb.addProgram(Program.AMB);
        memberAmb.addFlag(AMEX_CENTURION);
        memberAmb.setAmbassadorOfferCode(AmbassadorOfferCode.CHSRO);
        memberAmb.setAmbassadorAmount(AmbassadorUtils.AMBASSADOR_COMP_PROMO_$0);

        Member memberBr = new Member();
        memberBr.addProgram(Program.BR);
        memberBr.addFlag(POSSIBLE_FRAUD);

        Member memberWithCommonFlag = new Member();
        memberWithCommonFlag.addProgram(Program.RC);
        memberWithCommonFlag.addFlag(INACTIVE_FOR_6_MONTHS);

        return new Object[][] { { memberRc }, { memberAmb }, { memberBr }, { memberWithCommonFlag } };
    }

    @Factory(dataProvider = "verifyFlagOnAccountInfoProvider")
    public Object[] createInstances(Member member)
    {
        return new Object[] { new VerifyFlagOnAccountInfoFactoryTest(member) };
    }
}
