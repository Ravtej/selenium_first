package com.ihg.crm.automation.selenium.tests.servicecenter.enrollment.br;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.Constant.NOT_AVAILABLE;
import static com.ihg.automation.selenium.common.event.EventTransactionType.Membership.ENROLL;
import static com.ihg.automation.selenium.common.event.EventTransactionType.Offer.OFFER_REGISTRATION;
import static com.ihg.automation.selenium.common.types.program.Program.BR;
import static com.ihg.automation.selenium.common.types.program.Program.RC;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasDefault;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.isDisplayedWithWait;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static com.ihg.automation.selenium.matchers.component.ViewableMatcher.hasTextInView;
import static org.hamcrest.core.StringContains.containsString;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.common.DateUtils;
import com.ihg.automation.selenium.common.BusinessRewardsGridRow;
import com.ihg.automation.selenium.common.CustomerInfoPanel;
import com.ihg.automation.selenium.common.ProgramInfoPanel;
import com.ihg.automation.selenium.common.ProgramsGrid;
import com.ihg.automation.selenium.common.billing.EventBillingDetailsTab;
import com.ihg.automation.selenium.common.contact.Address;
import com.ihg.automation.selenium.common.earning.EventEarningDetailsTab;
import com.ihg.automation.selenium.common.event.AllEventsRowFactory;
import com.ihg.automation.selenium.common.member.GuestAddress;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.offer.OfferFactory;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.common.types.program.ProgramStatus;
import com.ihg.automation.selenium.common.types.program.ProgramStatusReason;
import com.ihg.automation.selenium.common.types.program.RewardClubLevel;
import com.ihg.automation.selenium.gwt.components.Component;
import com.ihg.automation.selenium.gwt.components.Mode;
import com.ihg.automation.selenium.matchers.component.ComponentMatcher;
import com.ihg.automation.selenium.matchers.component.HasBackgroundColor;
import com.ihg.automation.selenium.servicecenter.pages.LeftPanel;
import com.ihg.automation.selenium.servicecenter.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsGrid;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsGridRow;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsPage;
import com.ihg.automation.selenium.servicecenter.pages.event.all.OfferGridRowContainer;
import com.ihg.automation.selenium.servicecenter.pages.event.enroll.MembershipDetailsPopUp;
import com.ihg.automation.selenium.servicecenter.pages.event.enroll.MembershipGridRowContainer;
import com.ihg.automation.selenium.servicecenter.pages.personal.AddressContact;
import com.ihg.automation.selenium.servicecenter.pages.personal.PersonalInfoPage;
import com.ihg.automation.selenium.servicecenter.pages.programs.ProgramPageBase;
import com.ihg.automation.selenium.servicecenter.pages.programs.br.BusinessRewardsPage;
import com.ihg.automation.selenium.servicecenter.pages.programs.br.BusinessRewardsSummary;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.RewardClubPage;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.RewardClubSummary;
import com.ihg.crm.automation.selenium.tests.servicecenter.LoginLogout;

@Test(groups = { "brTest" })
public class EnrollmentToBRTest extends LoginLogout
{
    private PersonalInfoPage personalInfoPage = new PersonalInfoPage();
    private Member member = new Member();
    private BusinessRewardsPage businessRewardsPage = new BusinessRewardsPage();
    private AllEventsPage allEventsPage = new AllEventsPage();
    private AllEventsGrid allEventsGrid;
    private AllEventsGridRow allEventsGridRow;
    private MembershipDetailsPopUp membershipDetailsPopUp;
    private MembershipGridRowContainer membershipGridRowContainer;
    private AllEventsGridRow offerRow;
    private OfferGridRowContainer offerContainer;

    @BeforeClass
    public void beforeClass()
    {
        member.addProgram(Program.BR);
        member.addAddress(MemberPopulateHelper.getUnitedStatesAddress());
        member.setPersonalInfo(MemberPopulateHelper.getSimplePersonalInfo());

        login();
        new EnrollmentPage().enroll(member);
    }

    @Test
    public void verifyPersonalInformationTab()
    {
        personalInfoPage = new PersonalInfoPage();
        verifyThat(personalInfoPage, isDisplayedWithWait());
        personalInfoPage.getCustomerInfo().verify(member, Mode.VIEW);

        AddressContact addressContact = personalInfoPage.getAddressList().getExpandedContact();
        Address address = addressContact.getBaseContact();
        address.verify(member.getPreferredAddress(), Mode.VIEW);
    }

    @Test(dependsOnMethods = { "verifyPersonalInformationTab" }, alwaysRun = true)
    public void verifyRewardsClubPage()
    {
        RewardClubPage rewardClubPage = new RewardClubPage();
        rewardClubPage.goTo();
        RewardClubSummary rcSummary = rewardClubPage.getSummary();
        rcSummary.verify(ProgramStatus.OPEN, RewardClubLevel.CLUB);
        verifyThat(rcSummary.getUnitsBalance(), hasText("0"));
    }

    @Test(dependsOnMethods = { "verifyRewardsClubPage" })
    public void validateLeftPanelProgramInfoNavigation()
    {
        verifyThat(businessRewardsPage, displayed(false));
        ProgramsGrid grid = new LeftPanel().getProgramInfoPanel().getPrograms();
        grid.getBusinessRewardsProgram().goToProgramPage();
        verifyThat(businessRewardsPage, isDisplayedWithWait());
    }

    @Test(dependsOnMethods = { "validateLeftPanelProgramInfoNavigation" }, alwaysRun = true)
    public void verifyBusinessRewardsPage()
    {
        businessRewardsPage.goTo();

        BusinessRewardsSummary summary = businessRewardsPage.getSummary();
        verifyThat(summary.getStatus(), hasTextInView(ProgramStatus.OPEN));
        verifyThat(summary.getStatusReason(), hasTextInView(ProgramStatusReason.PENDING));
        verifyThat(summary.getMemberId(), hasText(member.getBRProgramId()));
        verifyThat(summary.getDeclinedTsAndCs(), displayed(true));
        verifyThat(summary, hasText(containsString("No flags selected")));

        ProgramPageBase.EnrollmentDetails enrollDetails = businessRewardsPage.getEnrollmentDetails();

        enrollDetails.verifyScSource(helper);
        verifyThat(enrollDetails.getPaymentDetails().getMethod(), hasDefault());
        verifyThat(enrollDetails.getReferringMember(), hasText(NOT_AVAILABLE));
        verifyThat(enrollDetails.getRenewalDate(), hasDefault());
        verifyThat(enrollDetails.getEnrollDate(), hasText(DateUtils.getFormattedDate()));
    }

    @Test(dependsOnMethods = { "verifyBusinessRewardsPage" }, alwaysRun = true)
    public void openAllEventsEnrollBREventDetails()
    {
        allEventsPage.goTo();
        allEventsGrid = allEventsPage.getGrid();

        allEventsGridRow = allEventsGrid.getEnrollRow(Program.BR);
        allEventsGridRow.verify(AllEventsRowFactory.getEnrollEvent(BR));

        membershipGridRowContainer = allEventsGridRow.expand(MembershipGridRowContainer.class);
        membershipGridRowContainer.getMembershipDetails().verifyScSource(helper);
    }

    @Test(dependsOnMethods = { "openAllEventsEnrollBREventDetails" })
    public void verifyEnrollmentAllEventsMembershipDetailsTab()
    {
        membershipGridRowContainer.getDetails().clickAndWait();
        membershipDetailsPopUp = new MembershipDetailsPopUp();
        membershipDetailsPopUp.getMembershipDetailsTab().getMembershipDetails().verifyScSource(helper);
    }

    @Test(dependsOnMethods = { "verifyEnrollmentAllEventsMembershipDetailsTab" })
    public void verifyAllEventsEarningDetails()
    {
        EventEarningDetailsTab tab = membershipDetailsPopUp.getEarningDetailsTab();
        tab.goTo();

        tab.verifyBasicDetailsNoPointsAndMiles();
    }

    @Test(dependsOnMethods = { "verifyAllEventsEarningDetails" })
    public void verifyAllEventsBillingDetails()
    {
        EventBillingDetailsTab tab = membershipDetailsPopUp.getBillingDetailsTab();
        tab.goTo();
        tab.verifyTabIsEmpty(ENROLL);
        membershipDetailsPopUp.close();
    }

    @Test(dependsOnMethods = { "verifyAllEventsBillingDetails" }, alwaysRun = true)
    public void openAllEventsEnrollRCEventDetails()
    {
        allEventsPage.goTo();
        allEventsGrid = allEventsPage.getGrid();

        allEventsGridRow = allEventsGrid.getEnrollRow(Program.RC);
        allEventsGridRow.verify(AllEventsRowFactory.getEnrollEvent(RC));

        membershipGridRowContainer = allEventsGridRow.expand(MembershipGridRowContainer.class);
        membershipGridRowContainer.getMembershipDetails().verifyScSource(helper);
    }

    @Test(dependsOnMethods = { "openAllEventsEnrollRCEventDetails" })
    public void verifyOfferDetails()
    {
        offerRow = allEventsGrid.getRow(OFFER_REGISTRATION);
        offerContainer = offerRow.expand(OfferGridRowContainer.class);
        offerContainer.getOfferDetails().verify(OfferFactory.DELAYED_FULFILMENT_GLOBAL, helper);
    }

    @Test(dependsOnMethods = { "verifyOfferDetails" })
    public void verifyCustomerInformationPanel()
    {
        CustomerInfoPanel custPanel = new CustomerInfoPanel();
        verifyThat(custPanel.getFullName(), hasText(member.getPersonalInfo().getName().getNameOnPanel()));

        GuestAddress guestAddr = member.getPreferredAddress();
        Component panelAddr = custPanel.getAddress();

        verifyThat(panelAddr, hasText(guestAddr.getFormattedAddress()));
    }

    @Test(dependsOnMethods = { "verifyCustomerInformationPanel" })
    public void verifyProgramInformationPanelLevel()
    {
        BusinessRewardsGridRow rowBR = new ProgramInfoPanel().getPrograms().getBusinessRewardsProgram();
        verifyThat(rowBR, HasBackgroundColor.hasBackgroundColor(LeftPanel.YELLOW_COLOR));

        verifyThat(rowBR.getStatus(), ComponentMatcher.hasText(ProgramStatusReason.PENDING));
    }
}
