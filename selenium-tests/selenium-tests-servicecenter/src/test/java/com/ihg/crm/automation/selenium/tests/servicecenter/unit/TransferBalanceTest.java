package com.ihg.crm.automation.selenium.tests.servicecenter.unit;

import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifySuccess;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.assertThat;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.event.EventTransactionType.Loyalty.BALANCE_TRANSFER;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.enabled;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasTextAsInt;
import static com.ihg.automation.selenium.servicecenter.pages.annual.AnnualActivityRow.AnnualActivityCell.TOTAL_UNIT;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.billing.EventBillingDetailsTab;
import com.ihg.automation.selenium.common.event.AllEventsRow;
import com.ihg.automation.selenium.common.event.AllEventsRowFactory;
import com.ihg.automation.selenium.common.member.LifetimeActivity;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.payment.PaymentDetailsPopUp;
import com.ihg.automation.selenium.servicecenter.pages.LeftPanel;
import com.ihg.automation.selenium.servicecenter.pages.annual.AnnualActivityPage;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsGridRow;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsPage;
import com.ihg.automation.selenium.servicecenter.pages.event.unit.UnitDetails;
import com.ihg.automation.selenium.servicecenter.pages.event.unit.UnitDetailsPopUp;
import com.ihg.automation.selenium.servicecenter.pages.event.unit.UnitDetailsTab;
import com.ihg.automation.selenium.servicecenter.pages.event.unit.UnitGridRowContainer;
import com.ihg.automation.selenium.servicecenter.pages.unit.GoodwillPopUp;
import com.ihg.automation.selenium.servicecenter.pages.unit.TransferUnitsPopUp;

public class TransferBalanceTest extends UnitsCommon
{
    private static final int BALANCE = 5000;

    private Member originator, recipient;

    @BeforeClass
    public void beforeClass()
    {
        login();

        recipient = new AnnualActivityPage().captureCounters(enrollMember(), jdbcTemplate);

        new LeftPanel().goToNewSearch();

        originator = enrollMember();

        new GoodwillPopUp().goodwillPoints(BALANCE);

        AnnualActivityPage annualActivityPage = new AnnualActivityPage();
        annualActivityPage.captureCounters(originator, jdbcTemplate);

        // Do basic amount verification
        verifyThat(annualActivityPage.getCurrentBalance().getAmount(), hasTextAsInt(BALANCE));
        verifyThat(annualActivityPage.getAdjustedUnits().getAmount(), hasTextAsInt(BALANCE));
        verifyThat(annualActivityPage.getTotalUnits().getAmount(), hasTextAsInt(BALANCE));
        verifyThat(annualActivityPage.getAnnualActivities().getCurrentYearRow().getCell(TOTAL_UNIT),
                hasTextAsInt(BALANCE));
    }

    @Test
    public void submitTransferUnitsPopUp()
    {
        TransferUnitsPopUp transUnitsPopUp = new TransferUnitsPopUp();
        transUnitsPopUp.goTo();
        transUnitsPopUp.getMembershipId().typeAndWait(recipient.getRCProgramId());
        transUnitsPopUp.getAmount().select("1000");

        transUnitsPopUp.getBalanceTransfer().check();
        verifyThat(transUnitsPopUp.getAmount(), enabled(false));
        verifyThat(transUnitsPopUp.getAmount(), hasTextAsInt(BALANCE));
        transUnitsPopUp.clickSubmitNoError(verifySuccess("Balance Transfer is successfully completed"));

        assertThat(new PaymentDetailsPopUp(), displayed(false), "No payment expected (US5747 mingle).");
    }

    @Test(dependsOnMethods = { "submitTransferUnitsPopUp" }, priority = 10)
    public void verifyOriginatorAnnualActivityPage()
    {
        LifetimeActivity lifetimeActivity = originator.getLifetimeActivity();
        lifetimeActivity.setCurrentBalance(0);
        lifetimeActivity.setAdjustedUnits(0);

        originator.getAnnualActivity().setTotalUnits(0);

        new AnnualActivityPage().verifyCounters(originator);
    }

    @Test(dependsOnMethods = { "submitTransferUnitsPopUp" }, priority = 20)
    public void verifyOriginatorBalanceTransferEvent()
    {
        AllEventsPage page = new AllEventsPage();
        page.goTo();

        AllEventsGridRow row = page.getGrid().getRow(1);
        AllEventsRow transferPurchase = AllEventsRowFactory.getPointEvent(BALANCE_TRANSFER, -BALANCE);
        row.verify(transferPurchase);

        UnitDetails details = row.expand(UnitGridRowContainer.class).getUnitDetails();
        details.verifyRelatedMemberID(recipient);
        verifyThat(details.getPaymentDetails().getMethod(), displayed(false));
        details.verifySource(helper);
    }

    @Test(dependsOnMethods = { "submitTransferUnitsPopUp" }, priority = 30)
    public void verifyOriginatorUnitDetailsPopUp()
    {
        openDetailsPopUp(BALANCE_TRANSFER);

        verifyUnitDetailsPopUp(recipient, -BALANCE);
    }

    @Test(dependsOnMethods = { "verifyOriginatorUnitDetailsPopUp" }, alwaysRun = true)
    public void closeOriginatorUnitDetailsPopUp()
    {
        new UnitDetailsPopUp().close();
    }

    @Test(dependsOnMethods = { "closeOriginatorUnitDetailsPopUp" }, alwaysRun = true)
    public void navigateRecipientAccount()
    {
        openRecipientProfileByLink(recipient, 1);
    }

    @Test(dependsOnMethods = { "navigateRecipientAccount" }, priority = 40)
    public void verifyRecipientAnnualActivityPage()
    {
        LifetimeActivity lifetimeActivity = recipient.getLifetimeActivity();
        lifetimeActivity.setAdjustedUnits(BALANCE);
        lifetimeActivity.setTotalUnits(BALANCE);
        lifetimeActivity.setCurrentBalance(BALANCE);

        recipient.getAnnualActivity().setTotalUnits(BALANCE);

        new AnnualActivityPage().verifyCounters(recipient);
    }

    @Test(dependsOnMethods = { "navigateRecipientAccount" }, priority = 50)
    public void verifyRecipientBalanceTransferEvent()
    {
        AllEventsPage page = new AllEventsPage();
        page.goTo();

        AllEventsGridRow row = page.getGrid().getRow(1);
        AllEventsRow transfer = AllEventsRowFactory.getPointEvent(BALANCE_TRANSFER, BALANCE);
        row.verify(transfer);

        UnitDetails unitDetails = row.expand(UnitGridRowContainer.class).getUnitDetails();
        unitDetails.verifyRelatedMemberID(originator);
        verifyThat(unitDetails.getPaymentDetails().getMethod(), displayed(false));
        unitDetails.verifySource(helper);
    }

    @Test(dependsOnMethods = { "navigateRecipientAccount" }, priority = 60)
    public void verifyRecipientUnitDetailsPopUp()
    {
        openDetailsPopUp(BALANCE_TRANSFER);

        verifyUnitDetailsPopUp(originator, BALANCE);
    }

    private void verifyUnitDetailsPopUp(Member member, Integer pointsAmount)
    {
        UnitDetailsPopUp unitPopUp = new UnitDetailsPopUp();

        UnitDetailsTab unitDetailsTab = unitPopUp.getUnitDetailsTab();
        unitDetailsTab.goTo();
        UnitDetails details = unitDetailsTab.getUnitDetails();
        details.verifyRelatedMemberID(member);
        verifyThat(details.getPaymentDetails().getMethod(), displayed(false));
        details.verifySource(helper);

        // Earning Details Tab
        unitPopUp.getEarningDetailsTab().verifyForBaseUnitsEvent(pointsAmount, 0);

        // Billing Details Tab
        EventBillingDetailsTab billingDetailsTab = unitPopUp.getBillingDetailsTab();
        billingDetailsTab.goTo();
        billingDetailsTab.verifyTabIsEmpty(BALANCE_TRANSFER);
    }

}
