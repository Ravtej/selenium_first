package com.ihg.crm.automation.selenium.tests.servicecenter.programinfo.karma;

import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyError;
import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyNoError;
import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifySuccess;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.types.program.KarmaLevel.KAR;
import static com.ihg.automation.selenium.common.types.program.KarmaLevel.KIC;
import static com.ihg.automation.selenium.common.types.program.KarmaLevelReason.BASE;
import static com.ihg.automation.selenium.common.types.program.ProgramStatus.CLOSED;
import static com.ihg.automation.selenium.common.types.program.RewardClubLevel.CLUB;
import static com.ihg.automation.selenium.common.types.program.RewardClubLevel.SPRE;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static com.ihg.automation.selenium.servicecenter.pages.personal.PersonalInfoPage.SUCCESS_MESSAGE;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.common.types.program.ProgramStatusReason;
import com.ihg.automation.selenium.servicecenter.pages.LeftPanel;
import com.ihg.automation.selenium.servicecenter.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.servicecenter.pages.programs.KarmaPage;
import com.ihg.automation.selenium.servicecenter.pages.programs.ProgramSummary;
import com.ihg.crm.automation.selenium.tests.servicecenter.LoginLogout;

@Test(groups = { "karma" })
public class KarmaTierLevelForClosedMemberTest extends LoginLogout
{
    private static final String WARNING_MESSAGE = "Customer Status is Closed. This Transaction is not allowed.";
    private ProgramSummary summary;

    @BeforeClass
    public void beforeClass()
    {
        Member member = new Member();
        member.setPersonalInfo(MemberPopulateHelper.getSimplePersonalInfo());
        member.addAddress(MemberPopulateHelper.getUnitedStatesAddress());
        member.addProgram(Program.KAR);

        login();

        new EnrollmentPage().enroll(member);

        KarmaPage karmaPage = new KarmaPage();
        karmaPage.goTo();
        summary = karmaPage.getSummary();
        summary.setClosedStatus(ProgramStatusReason.FROZEN);
        summary.verify(CLOSED, KAR);
    }

    @Test
    public void tryToSetKicTierLevel()
    {
        summary.setTierLevelWithExpiration(KIC, BASE, verifyError(WARNING_MESSAGE));
        summary.clickCancel(verifyNoError());

        summary.verify(CLOSED, KAR);
    }

    @Test(dependsOnMethods = { "tryToSetKicTierLevel" })
    public void tryToSetKarTierLevel()
    {
        openSetKicTierLevelAndCloseKarma();

        summary.clickEdit();
        summary.getTierLevel().selectByValue(KAR);
        summary.clickSave(verifyError(WARNING_MESSAGE));
        summary.clickCancel(verifyNoError());

        summary.verify(CLOSED, KIC);
    }

    @Test(dependsOnMethods = { "tryToSetKarTierLevel" })
    public void openKarmaProgramAndVerifySpireLevel()
    {
        summary.setOpenStatus();
        verifyThat(new LeftPanel().getProgramInfoPanel().getPrograms().getRewardClubProgram().getLevelCode(),
                hasText(SPRE.getCode()));
    }

    private void openSetKicTierLevelAndCloseKarma()
    {
        verifyThat(new LeftPanel().getProgramInfoPanel().getPrograms().getRewardClubProgram().getLevelCode(),
                hasText(CLUB.getCode()));

        summary.setOpenStatus();
        summary.setTierLevelWithExpiration(KIC, BASE, verifyNoError(), verifySuccess(SUCCESS_MESSAGE));
        verifyThat(new LeftPanel().getProgramInfoPanel().getPrograms().getRewardClubProgram().getLevelCode(),
                hasText(SPRE.getCode()));

        summary.setClosedStatus(ProgramStatusReason.FROZEN);
        summary.verify(CLOSED, KIC);
        verifyThat(new LeftPanel().getProgramInfoPanel().getPrograms().getRewardClubProgram().getLevelCode(),
                hasText(SPRE.getCode()));
    }
}
