package com.ihg.crm.automation.selenium.tests.servicecenter.commpref;

import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.assertNoError;
import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyNoError;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.communication.CommunicationPreferencesConstants.OPT_IN;
import static com.ihg.automation.selenium.common.communication.CommunicationPreferencesConstants.OPT_OUT;
import static com.ihg.automation.selenium.common.types.WrittenLanguage.ENGLISH;
import static com.ihg.automation.selenium.common.types.WrittenLanguage.SPANISH;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static com.ihg.automation.selenium.matchers.component.IsSelected.isSelected;
import static com.ihg.automation.selenium.matchers.component.ViewableMatcher.hasTextInView;
import static com.ihg.automation.selenium.matchers.text.EntityMatcherFactory.isValue;
import static com.ihg.automation.selenium.servicecenter.pages.history.AuditConstant.CommunicationPreferences.COMMUNICATIONS;
import static com.ihg.automation.selenium.servicecenter.pages.history.AuditConstant.CommunicationPreferences.COMMUNICATION_PREFERENCES;
import static com.ihg.automation.selenium.servicecenter.pages.history.AuditConstant.CommunicationPreferences.EMAIL;
import static com.ihg.automation.selenium.servicecenter.pages.history.AuditConstant.CommunicationPreferences.STATUS;
import static com.ihg.automation.selenium.servicecenter.pages.history.AuditConstant.CommunicationPreferences.WRITTEN_LANGUAGE;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.common.DateUtils;
import com.ihg.automation.selenium.common.communication.Communication;
import com.ihg.automation.selenium.common.communication.ContactPermissionItem;
import com.ihg.automation.selenium.common.communication.ContactPermissionItems;
import com.ihg.automation.selenium.common.communication.MarketingPreferences;
import com.ihg.automation.selenium.common.member.GuestEmail;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.servicecenter.pages.LeftPanel;
import com.ihg.automation.selenium.servicecenter.pages.communication.CommunicationPreferences;
import com.ihg.automation.selenium.servicecenter.pages.communication.CommunicationPreferencesPage;
import com.ihg.automation.selenium.servicecenter.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.servicecenter.pages.history.AuditConstant;
import com.ihg.automation.selenium.servicecenter.pages.history.ProfileHistoryGridRow;
import com.ihg.automation.selenium.servicecenter.pages.history.ProfileHistoryPage;
import com.ihg.automation.selenium.servicecenter.pages.personal.PersonalInfoPage;
import com.ihg.crm.automation.selenium.tests.servicecenter.LoginLogout;

public class RCMemberWithEmailAndPhoneTest extends LoginLogout
{
    private MarketingPreferences marketingPrefs;
    private Member member = new Member();
    private ContactPermissionItems marketingPermissionItems;
    private EnrollmentPage enrollmentPage = new EnrollmentPage();
    private GuestEmail guestEmail;

    @BeforeClass
    public void beforeClass()
    {
        guestEmail = MemberPopulateHelper.getRandomEmail();

        member.addProgram(Program.RC);
        member.setPersonalInfo(MemberPopulateHelper.getSimplePersonalInfo());
        member.addAddress(MemberPopulateHelper.getUnitedStatesAddress());
        member.addPhone(MemberPopulateHelper.getRandomPhone());

        login();
        enrollmentPage.goTo();
        enrollmentPage.populate(member);
        enrollmentPage.getCustomerInfo().getDoNotContact().check();
        enrollmentPage.clickSubmit(assertNoError());

        member = enrollmentPage.successEnrollmentPopUp(member);

        PersonalInfoPage personalInfoPage = new PersonalInfoPage();
        personalInfoPage.goTo();
        personalInfoPage.getEmailList().add(guestEmail, verifyNoError());
    }

    @Test
    public void verifyCommunicationsSettings()
    {
        CommunicationPreferencesPage commPrefPage = new CommunicationPreferencesPage();

        commPrefPage.goTo();
        CommunicationPreferences commPrefs = commPrefPage.getCommunicationPreferences();

        verifyThat(commPrefs.getDoNotContactLabel(), hasText("Yes"));

        marketingPrefs = commPrefs.getMarketingPreferences();
        marketingPermissionItems = marketingPrefs.getContactPermissionItems();

        for (int index = 1; index <= marketingPermissionItems.getContactPermissionItemsCount(); index++)
        {
            verifyThat(marketingPermissionItems.getContactPermissionItem(index).getSubscribe(), hasTextInView(OPT_OUT));
        }

        commPrefs.clickEdit();

        ContactPermissionItem allianceItem = marketingPrefs.getContactPermissionItems()
                .getContactPermissionItem(Communication.E_STATEMENT);

        ContactPermissionItem specialOffersAndPromotionsItem = marketingPrefs.getContactPermissionItems()
                .getContactPermissionItem(Communication.PARTNER_OFFERS_AND_PROMOTIONS);

        allianceItem.getSubscribe().select(OPT_OUT);

        specialOffersAndPromotionsItem.getSubscribe().select(OPT_IN);
        commPrefs.clickSave();

        commPrefs.clickEdit();
        allianceItem = marketingPrefs.getContactPermissionItems().getContactPermissionItem(Communication.E_STATEMENT);
        allianceItem.getSubscribe().select(OPT_IN);
        commPrefs.clickSave();
    }

    @Test(dependsOnMethods = { "verifyCommunicationsSettings" }, alwaysRun = true)
    public void verifyHistoryAfterItemStatusUpdate()
    {
        ProfileHistoryPage historyPage = new ProfileHistoryPage();
        historyPage.goTo();
        verifyNoError();

        ProfileHistoryGridRow communicationRow = historyPage.getProfileHistoryGrid().getRow(1)
                .getSubRowByName(COMMUNICATION_PREFERENCES).getSubRowByName(COMMUNICATIONS);

        ProfileHistoryGridRow eStatementRow = communicationRow.getSubRowByName(Communication.E_STATEMENT.getValue());
        eStatementRow.getSubRowByName(STATUS).verify(AuditConstant.OPT_OUT, AuditConstant.OPT_IN);
        eStatementRow.getSubRowByName(EMAIL).verifyAddAction(guestEmail.getEmail());

        ProfileHistoryGridRow partnerOfferRow = communicationRow
                .getSubRowByName(Communication.PARTNER_OFFERS_AND_PROMOTIONS.getValue());
        partnerOfferRow.getSubRowByName(STATUS).verify(AuditConstant.OPT_OUT, AuditConstant.OPT_IN);
        partnerOfferRow.getSubRowByName(EMAIL).verifyAddAction(guestEmail.getEmail());
    }

    @Test(dependsOnMethods = { "verifyHistoryAfterItemStatusUpdate" }, alwaysRun = true)
    public void verifyLanguagePreference()
    {
        CommunicationPreferencesPage commPrefPage = new CommunicationPreferencesPage();
        commPrefPage.goTo();
        CommunicationPreferences commPrefs = commPrefPage.getCommunicationPreferences();
        commPrefs.clickEdit(verifyNoError());

        commPrefs.getPreferredLanguage().select(SPANISH.getValue());
        commPrefs.clickSave();
        commPrefs.verify(SPANISH);
    }

    @Test(dependsOnMethods = { "verifyLanguagePreference" }, alwaysRun = true)
    public void verifyHistoryAfterEditLanguage()
    {
        ProfileHistoryPage historyPage = new ProfileHistoryPage();
        historyPage.goTo();
        verifyNoError();

        ProfileHistoryGridRow communicationRow = historyPage.getProfileHistoryGrid().getRow(1)
                .getSubRowByName(COMMUNICATION_PREFERENCES);

        communicationRow.getSubRowByName(WRITTEN_LANGUAGE).verify(hasText(isValue(ENGLISH)), hasText(isValue(SPANISH)));
    }

    @Test(dependsOnMethods = { "verifyHistoryAfterEditLanguage" }, alwaysRun = true)
    public void clickUnSubscribeAllLink()
    {
        new LeftPanel().reOpenMemberProfile(member);

        CommunicationPreferencesPage commPrefPage = new CommunicationPreferencesPage();
        commPrefPage.goTo();
        CommunicationPreferences commPrefs = commPrefPage.getCommunicationPreferences();
        commPrefs.clickEdit(verifyNoError());

        marketingPrefs = commPrefs.getMarketingPreferences();
        marketingPrefs.getUnSubscribeAllLink().click();
        commPrefs.clickSave();

        marketingPermissionItems = marketingPrefs.getContactPermissionItems();

        for (int index = 1; index <= marketingPermissionItems.getContactPermissionItemsCount(); index++)
        {
            ContactPermissionItem contactPermissionItem = marketingPermissionItems.getContactPermissionItem(index);
            verifyThat(contactPermissionItem.getSubscribe(), hasTextInView(OPT_OUT));
            verifyThat(contactPermissionItem.getDateUpdated(), hasText(DateUtils.getFormattedDate()));
        }
    }

    @Test(dependsOnMethods = { "clickUnSubscribeAllLink" }, alwaysRun = true)
    public void verifyHistoryAfterClickUnSubscribeAllLink()
    {
        ProfileHistoryPage historyPage = new ProfileHistoryPage();
        historyPage.goTo();
        verifyNoError();

        ProfileHistoryGridRow communicationRow = historyPage.getProfileHistoryGrid().getRow(1)
                .getSubRowByName(COMMUNICATION_PREFERENCES).getSubRowByName(COMMUNICATIONS);

        ProfileHistoryGridRow eStatementRow = communicationRow.getSubRowByName(Communication.E_STATEMENT.getValue());
        eStatementRow.getSubRowByName(STATUS).verify(AuditConstant.OPT_IN, AuditConstant.OPT_OUT);

        ProfileHistoryGridRow partnerOfferRow = communicationRow
                .getSubRowByName(Communication.PARTNER_OFFERS_AND_PROMOTIONS.getValue());
        partnerOfferRow.getSubRowByName(STATUS).verify(AuditConstant.OPT_IN, AuditConstant.OPT_OUT);
    }

    @Test(dependsOnMethods = { "verifyHistoryAfterClickUnSubscribeAllLink" }, alwaysRun = true)
    public void clickSubscribeAllLink()
    {
        new LeftPanel().reOpenMemberProfile(member);

        CommunicationPreferencesPage commPrefPage = new CommunicationPreferencesPage();
        commPrefPage.goTo();
        CommunicationPreferences commPrefs = commPrefPage.getCommunicationPreferences();
        commPrefs.clickEdit(verifyNoError());

        marketingPrefs = commPrefs.getMarketingPreferences();
        marketingPrefs.getSubscribeAllLink().click();
        commPrefs.clickSave();

        marketingPermissionItems = marketingPrefs.getContactPermissionItems();
        for (int index = 1; index <= marketingPermissionItems.getContactPermissionItemsCount(); index++)
        {
            verifyThat(marketingPermissionItems.getContactPermissionItem(index).getSubscribe(), hasTextInView(OPT_IN));
        }
    }

    @Test(dependsOnMethods = { "clickSubscribeAllLink" }, alwaysRun = true)
    public void verifyHistoryAfterClickSubscribeAllLink()
    {
        ProfileHistoryPage historyPage = new ProfileHistoryPage();
        historyPage.goTo();
        verifyNoError();

        ProfileHistoryGridRow communicationRow = historyPage.getProfileHistoryGrid().getRow(1)
                .getSubRowByName(COMMUNICATION_PREFERENCES).getSubRowByName(COMMUNICATIONS);

        ProfileHistoryGridRow eStatementRow = communicationRow.getSubRowByName(Communication.E_STATEMENT.getValue());
        eStatementRow.getSubRowByName(STATUS).verify(AuditConstant.OPT_OUT, AuditConstant.OPT_IN);
        eStatementRow.getSubRowByName(EMAIL).verifyAddAction(guestEmail.getEmail());

        ProfileHistoryGridRow partnerOfferRow = communicationRow
                .getSubRowByName(Communication.PARTNER_OFFERS_AND_PROMOTIONS.getValue());
        partnerOfferRow.getSubRowByName(STATUS).verify(AuditConstant.OPT_OUT, AuditConstant.OPT_IN);
        partnerOfferRow.getSubRowByName(EMAIL).verifyAddAction(guestEmail.getEmail());

        ProfileHistoryGridRow creditCardOffersRow = communicationRow
                .getSubRowByName(Communication.IHG_REWARDS_CLUB_CREDIT_CARD_OFFERS_AND_PROMOTIONS.getValue());
        creditCardOffersRow.getSubRowByName(STATUS).verify(AuditConstant.OPT_OUT, AuditConstant.OPT_IN);
        creditCardOffersRow.getSubRowByName(EMAIL).verifyAddAction(guestEmail.getEmail());

        ProfileHistoryGridRow memberSurveysRow = communicationRow
                .getSubRowByName(Communication.MEMBER_SURVEYS.getValue());
        memberSurveysRow.getSubRowByName(STATUS).verify(AuditConstant.OPT_OUT, AuditConstant.OPT_IN);
        memberSurveysRow.getSubRowByName(EMAIL).verifyAddAction(guestEmail.getEmail());

        ProfileHistoryGridRow heartbeatRow = communicationRow.getSubRowByName(Communication.HEARTBEAT.getValue());
        heartbeatRow.getSubRowByName(STATUS).verify(AuditConstant.OPT_OUT, AuditConstant.OPT_IN);
        heartbeatRow.getSubRowByName(EMAIL).verifyAddAction(guestEmail.getEmail());

        ProfileHistoryGridRow specialOffersRow = communicationRow
                .getSubRowByName(Communication.IHG_REWARDS_CLUB_SPECIAL_OFFERS_AND_PROMOTIONS.getValue());
        specialOffersRow.getSubRowByName(STATUS).verify(AuditConstant.OPT_OUT, AuditConstant.OPT_IN);
        specialOffersRow.getSubRowByName(EMAIL).verifyAddAction(guestEmail.getEmail());
    }

    @Test(dependsOnMethods = { "verifyHistoryAfterClickSubscribeAllLink" }, alwaysRun = true)
    public void verifyNoChangesAfterSelectDoNotCall()
    {
        CommunicationPreferencesPage commPrefPage = new CommunicationPreferencesPage();
        commPrefPage.goTo();
        CommunicationPreferences commPrefs = commPrefPage.getCommunicationPreferences();
        commPrefs.clickEdit(verifyNoError());

        commPrefs.getDoNotCall().check();

        commPrefs.clickSave();

        marketingPermissionItems = marketingPrefs.getContactPermissionItems();
        for (int index = 1; index <= marketingPermissionItems.getContactPermissionItemsCount(); index++)
        {
            verifyThat(marketingPermissionItems.getContactPermissionItem(index).getSubscribe(), hasTextInView(OPT_IN));
        }
    }

    @Test(dependsOnMethods = { "verifyNoChangesAfterSelectDoNotCall" }, alwaysRun = true)
    public void selectDoNotContact()
    {
        new LeftPanel().reOpenMemberProfile(member);
        CommunicationPreferencesPage commPrefPage = new CommunicationPreferencesPage();

        commPrefPage.goTo();
        commPrefPage.getCommunicationPreferences().clickEdit(verifyNoError());

        CommunicationPreferences commPrefs = commPrefPage.getCommunicationPreferences();
        commPrefs.getDoNotContact().check();

        commPrefs.clickSave();

        marketingPermissionItems = marketingPrefs.getContactPermissionItems();
        for (int index = 1; index <= marketingPermissionItems.getContactPermissionItemsCount(); index++)
        {
            verifyThat(marketingPermissionItems.getContactPermissionItem(index).getSubscribe(), hasTextInView(OPT_OUT));
        }
    }

    @Test(dependsOnMethods = { "selectDoNotContact" }, alwaysRun = true)
    public void verifyHistoryAfterSelectDoNotContact()
    {
        ProfileHistoryPage historyPage = new ProfileHistoryPage();
        historyPage.goTo();
        verifyNoError();

        ProfileHistoryGridRow communicationRow = historyPage.getProfileHistoryGrid().getRow(1)
                .getSubRowByName(COMMUNICATION_PREFERENCES).getSubRowByName(COMMUNICATIONS);

        ProfileHistoryGridRow eStatementRow = communicationRow.getSubRowByName(Communication.E_STATEMENT.getValue());
        eStatementRow.getSubRowByName(STATUS).verify(AuditConstant.OPT_IN, AuditConstant.OPT_OUT);

        ProfileHistoryGridRow partnerOfferRow = communicationRow
                .getSubRowByName(Communication.PARTNER_OFFERS_AND_PROMOTIONS.getValue());
        partnerOfferRow.getSubRowByName(STATUS).verify(AuditConstant.OPT_IN, AuditConstant.OPT_OUT);

        ProfileHistoryGridRow creditCardOffersRow = communicationRow
                .getSubRowByName(Communication.IHG_REWARDS_CLUB_CREDIT_CARD_OFFERS_AND_PROMOTIONS.getValue());
        creditCardOffersRow.getSubRowByName(STATUS).verify(AuditConstant.OPT_IN, AuditConstant.OPT_OUT);

        ProfileHistoryGridRow memberSurveysRow = communicationRow
                .getSubRowByName(Communication.MEMBER_SURVEYS.getValue());
        memberSurveysRow.getSubRowByName(STATUS).verify(AuditConstant.OPT_IN, AuditConstant.OPT_OUT);

        ProfileHistoryGridRow heartbeatRow = communicationRow.getSubRowByName(Communication.HEARTBEAT.getValue());
        heartbeatRow.getSubRowByName(STATUS).verify(AuditConstant.OPT_IN, AuditConstant.OPT_OUT);

        ProfileHistoryGridRow specialOffersRow = communicationRow
                .getSubRowByName(Communication.IHG_REWARDS_CLUB_SPECIAL_OFFERS_AND_PROMOTIONS.getValue());
        specialOffersRow.getSubRowByName(STATUS).verify(AuditConstant.OPT_IN, AuditConstant.OPT_OUT);
    }

    @Test(dependsOnMethods = { "verifyHistoryAfterSelectDoNotContact" }, alwaysRun = true)
    public void editSubscribtionAfterDoNotContact()
    {
        new LeftPanel().reOpenMemberProfile(member);

        CommunicationPreferencesPage commPrefPage = new CommunicationPreferencesPage();
        commPrefPage.goTo();
        CommunicationPreferences commPrefs = commPrefPage.getCommunicationPreferences();
        commPrefs.clickEdit(verifyNoError());

        ContactPermissionItem item = marketingPrefs.getContactPermissionItems().getContactPermissionItem(1);

        item.getSubscribe().select(OPT_IN);

        verifyThat(commPrefs.getDoNotContact(), isSelected(false));
        commPrefs.clickSave();

        item = marketingPrefs.getContactPermissionItems().getContactPermissionItem(1);
        verifyThat(item.getSubscribe(), hasTextInView(OPT_IN));
        verifyThat(item.getDateUpdated(), hasText(DateUtils.getFormattedDate()));
    }

    @Test(dependsOnMethods = { "editSubscribtionAfterDoNotContact" }, alwaysRun = true)
    public void verifyHistoryAfterEditSubscribtionAfterDoNotContact()
    {
        ProfileHistoryPage historyPage = new ProfileHistoryPage();
        historyPage.goTo();
        verifyNoError();

        ProfileHistoryGridRow communicationRow = historyPage.getProfileHistoryGrid().getRow(1)
                .getSubRowByName(COMMUNICATION_PREFERENCES).getSubRowByName(COMMUNICATIONS);

        ProfileHistoryGridRow eStatementRow = communicationRow.getSubRowByName(Communication.E_STATEMENT.getValue());
        eStatementRow.getSubRowByName(STATUS).verify(AuditConstant.OPT_OUT, AuditConstant.OPT_IN);
        eStatementRow.getSubRowByName(EMAIL).verifyAddAction(guestEmail.getEmail());
    }
}
