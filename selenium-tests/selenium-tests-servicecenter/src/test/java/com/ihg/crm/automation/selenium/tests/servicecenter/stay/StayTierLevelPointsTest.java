package com.ihg.crm.automation.selenium.tests.servicecenter.stay;

import static com.ihg.automation.selenium.common.Constant.NOT_AVAILABLE;
import static com.ihg.automation.selenium.common.types.program.ProgramStatus.OPEN;
import static com.ihg.automation.selenium.common.types.program.RewardClubLevel.GOLD;
import static com.ihg.automation.selenium.common.types.program.RewardClubLevel.PLTN;
import static com.ihg.automation.selenium.common.types.program.RewardClubLevel.SPRE;
import static com.ihg.automation.selenium.common.types.program.RewardClubLevelReason.BASE;
import static com.ihg.automation.selenium.common.types.program.TierLevelExpiration.EXPIRE_CURRENT_YEAR;
import static com.ihg.automation.selenium.formatter.Converter.integerToString;
import static com.ihg.automation.selenium.formatter.Converter.stringToInteger;
import static com.ihg.automation.selenium.servicecenter.pages.event.stay.StayEventsPage.STAY_SUCCESS_CREATED;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.stay.Revenues;
import com.ihg.automation.selenium.common.stay.Stay;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.common.types.program.RewardClubLevel;
import com.ihg.automation.selenium.hotel.pages.LeftPanel;
import com.ihg.automation.selenium.servicecenter.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.servicecenter.pages.event.stay.StayEventsPage;
import com.ihg.automation.selenium.servicecenter.pages.event.stay.StayGuestGridRow;
import com.ihg.automation.selenium.servicecenter.pages.event.stay.StayGuestGridRowContainer;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.RewardClubPage;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.RewardClubSummary;
import com.ihg.crm.automation.selenium.tests.servicecenter.LoginLogout;

public class StayTierLevelPointsTest extends LoginLogout
{
    private static final String HOTEL_CODE = "MIAHA";

    private Stay stay = new Stay();
    private Integer storeBalance = 0;
    private Revenues revenues = stay.getRevenues();

    @BeforeClass
    public void beforeClass()
    {
        stay.setHotelCode(HOTEL_CODE);
        stay.setBrandCode("ICON");
        stay.setNights(1);
        stay.setQualifyingNights(1);
        stay.setTierLevelNights("1");
        stay.setAvgRoomRate(100.00);
        stay.setRateCode("1");
        stay.setConfirmationNumber(NOT_AVAILABLE);
        stay.setFolioNumber(NOT_AVAILABLE);
        stay.setOverlappingStay("No");
        stay.setRoomNumber("3");
        stay.setRoomType("Loft");

        revenues.getRoom().setAmount(100.0);
        revenues.getFood().setAmount(0.00);
        revenues.getBeverage().setAmount(0.00);
        revenues.getPhone().setAmount(0.00);
        revenues.getMeeting().setAmount(0.00);
        revenues.getMandatoryLoyalty().setAmount(0.00);
        revenues.getOtherLoyalty().setAmount(0.00);
        revenues.getNoLoyalty().setAmount(0.00);
        revenues.getTax().setAmount(0.00);
        revenues.getOverallTotal().setAmount(100.00);
        revenues.getTotalNonQualifying().setAmount(0.00);
        revenues.getTotalQualifying().setAmount(100.00);

        login();

        Member member = new Member();
        member.setPersonalInfo(MemberPopulateHelper.getSimplePersonalInfo());
        member.addAddress(MemberPopulateHelper.getUnitedStatesAddress());
        member.addProgram(Program.RC);

        new EnrollmentPage().enroll(member);
    }

    @BeforeMethod
    public void captureMembersBalance()
    {
        storeBalance = stringToInteger(new LeftPanel().getProgramInfoPanel().getPrograms().getRewardClubProgram()
                .getPointsBalance().getText());
    }

    @Test(dataProvider = "testProvider")
    public void verifyPointsForStayDependingOnTierLevel(RewardClubLevel level, String ihgUnits, int shift)
    {
        RewardClubPage rewardClubPage = new RewardClubPage();
        rewardClubPage.goTo();

        RewardClubSummary summary = rewardClubPage.getSummary();
        summary.setTierLevelWithExpiration(level, BASE, EXPIRE_CURRENT_YEAR);
        summary.verify(OPEN, level);

        stay.setCheckInOutByNights(1, shift);
        StayEventsPage stayEventsPage = new StayEventsPage();
        stayEventsPage.createStay(stay, STAY_SUCCESS_CREATED);

        stay.setIhgUnits(ihgUnits);
        StayGuestGridRow stayGuestGridRow = stayEventsPage.getGuestGrid().getRow(1);
        stayGuestGridRow.verify(stay);

        // verify Stay in View Mode
        stayGuestGridRow.expand(StayGuestGridRowContainer.class).verify(stay, helper);

        new LeftPanel().verifyBalance(integerToString(storeBalance + stringToInteger(stay.getIhgUnits())));
    }

    @DataProvider(name = "testProvider")
    public Object[][] testProviderDataProvider()
    {
        // depending on level, Member gets different Points amount:
        // GOLD - totalAmount + 10%, PLATINUM - totalAmount + 50%, SPIRE - totalAmount + 100%//

        return new Object[][] { { GOLD, "1,100", 0 }, //
                { PLTN, "1,500", 4 }, //
                {SPRE, "2,000", 8 } //
        };
    }
}
