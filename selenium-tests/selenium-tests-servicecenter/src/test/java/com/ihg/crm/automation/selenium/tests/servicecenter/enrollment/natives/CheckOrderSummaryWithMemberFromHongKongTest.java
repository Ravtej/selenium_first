package com.ihg.crm.automation.selenium.tests.servicecenter.enrollment.natives;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.components.NativeLanguage.SIMPLIFIED_CHINESE;
import static com.ihg.automation.selenium.common.components.NativeLanguage.TRADITIONAL_CHINESE;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static org.hamcrest.core.StringContains.containsString;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.components.Country;
import com.ihg.automation.selenium.common.contact.Address;
import com.ihg.automation.selenium.common.member.GuestAddress;
import com.ihg.automation.selenium.common.member.GuestName;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.name.PersonNameFields;
import com.ihg.automation.selenium.common.types.address.AddressType;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.gwt.components.Mode;
import com.ihg.automation.selenium.servicecenter.pages.catalog.CatalogItemDetailsPopUp;
import com.ihg.automation.selenium.servicecenter.pages.catalog.CatalogPage;
import com.ihg.automation.selenium.servicecenter.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.servicecenter.pages.order.OrderSummaryPopUp;
import com.ihg.automation.selenium.servicecenter.pages.personal.AddressContact;
import com.ihg.automation.selenium.servicecenter.pages.personal.PersonalInfoPage;
import com.ihg.automation.selenium.servicecenter.pages.unit.GoodwillPopUp;
import com.ihg.crm.automation.selenium.tests.servicecenter.LoginLogout;

public class CheckOrderSummaryWithMemberFromHongKongTest extends LoginLogout
{
    private EnrollmentPage enrollmentPage = new EnrollmentPage();
    private GuestName name = new GuestName();
    private GuestName nativeName = new GuestName();
    private GuestAddress address = new GuestAddress();
    private GuestAddress nativeAddress = new GuestAddress();
    private CatalogPage catalogPage = new CatalogPage();
    private static final String CATALOG_ID = "AMA01";
    private Member member = new Member();

    @BeforeClass
    public void before()
    {
        nativeName.setGiven("名");
        nativeName.setSurname("姓");
        nativeName.setNativeLanguage(SIMPLIFIED_CHINESE);

        name.setGiven("ming");
        name.setSurname("xing");
        name.setNativeName(nativeName);

        nativeAddress.setLine1("中山区中山北路10a");
        nativeAddress.setRegion2("北");

        address.setCountryCode(Country.HK);
        address.setType(AddressType.RESIDENCE);
        address.setRegion2("bei");
        address.setLine1("zhong shan qu zhong shan bei lu 10a");

        address.setNativeAddress(nativeAddress);

        login();
    }

    @Test
    public void populateNameNativeFields()
    {
        enrollmentPage.goTo();
        enrollmentPage.getPrograms().selectProgram(Program.RC);
        enrollmentPage.getCustomerInfo().getResidenceCountry().selectByCode(Country.HK);
        PersonNameFields nameFields = enrollmentPage.getCustomerInfo().getName();
        nameFields.setConfiguration(Country.HK);
        nameFields.populateNativeFields(name);
        nameFields.verify(name, Mode.EDIT);
    }

    @Test(dependsOnMethods = { "populateNameNativeFields" })
    public void populateAddressNativeFields()
    {
        Address addressFields = enrollmentPage.getAddress();
        verifyThat(addressFields, hasText(containsString(TRADITIONAL_CHINESE)));
        addressFields.populateNativeFields(address);
        addressFields.verify(address, Mode.EDIT);
    }

    @Test(dependsOnMethods = { "populateAddressNativeFields" })
    public void completeEnrollAndGoodwill()
    {
        enrollmentPage.completeEnrollWithDuplicatePopUp(member);

        new GoodwillPopUp().goodwillPoints("50000");
    }

    @Test(dependsOnMethods = { "completeEnrollAndGoodwill" })
    public void verifyAddressDetailsInEditMode()
    {
        PersonalInfoPage persInfo = new PersonalInfoPage();
        persInfo.goTo();

        AddressContact contact = persInfo.getAddressList().getContact();
        contact.clickEdit();
        contact.getBaseContact().verifyNativeFieldsBase(TRADITIONAL_CHINESE);
    }

    @Test(dependsOnMethods = { "verifyAddressDetailsInEditMode" })
    public void verifyCatalog()
    {
        catalogPage.goTo();
        catalogPage.getButtonBar().clickClearCriteria();
        catalogPage.search(CATALOG_ID, null, null, null, null);
        CatalogItemDetailsPopUp catalogItemDetailsPopUp = catalogPage.openCatalogItemDetailsPopUp();
        OrderSummaryPopUp orderSummaryPopUp = catalogItemDetailsPopUp.openOrderSummaryPopUp();
        orderSummaryPopUp.getName().verify(name, Mode.EDIT);
        orderSummaryPopUp.getAddress().verify(address, Mode.EDIT);
    }
}
