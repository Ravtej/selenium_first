package com.ihg.crm.automation.selenium.tests.servicecenter.offer;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static com.ihg.automation.selenium.matchers.component.IsDisplayed.displayed;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.servicecenter.pages.GuestSearch;
import com.ihg.automation.selenium.servicecenter.pages.event.offer.CampaignDashboardTab;
import com.ihg.automation.selenium.servicecenter.pages.event.offer.OfferEventPage;
import com.ihg.automation.selenium.servicecenter.pages.event.offer.OfferPopUp;
import com.ihg.automation.selenium.servicecenter.pages.event.offer.SubOffersGrid;
import com.ihg.automation.selenium.servicecenter.pages.event.offer.SubOffersGridRow;
import com.ihg.crm.automation.selenium.tests.servicecenter.LoginLogout;

public class ExternalCampaignWithNightsEarnedTest extends LoginLogout
{
    private static final String OFFER_CODE_WITH_EXTERNAL_CAMPAIGN = "8710487";

    @BeforeClass
    public void beforeClass()
    {
        login();
        new GuestSearch().byMemberNumber(memberWithExternalCampaignWithNightsEarned);
    }

    @Test
    public void verifyExternalTrackingTab()
    {
        OfferEventPage offerEventPage = new OfferEventPage();
        offerEventPage.goTo();

        OfferPopUp popUp = offerEventPage.getMemberOffersGrid().getOfferPopUp(OFFER_CODE_WITH_EXTERNAL_CAMPAIGN);

        CampaignDashboardTab tab = popUp.getCampaignDashboardTab();

        verifyThat(tab, displayed(true));

        SubOffersGrid grid = tab.getMemberOffersGrid();

        verifyThat(grid.getRowByOfferCode("BDXX").getCell(SubOffersGridRow.SubOfferCell.REWARD),
                hasText("5,000 Points"));
        verifyThat(grid.getRowByOfferCode("ST01").getCell(SubOffersGridRow.SubOfferCell.REWARD),
                hasText("1 Free Nights"));

        verifyThat(tab.getExternalTrackingDetails().getCampaignSummary().getEarned(), hasText("0 of 1 nights"));
    }

}
