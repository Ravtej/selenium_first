package com.ihg.crm.automation.selenium.tests.servicecenter.programinfo.karma;

import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifySuccess;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.Constant.NOT_AVAILABLE;
import static com.ihg.automation.selenium.common.member.MemberJdbcUtils.MEMBER_ID;
import static com.ihg.automation.selenium.common.types.program.KarmaLevel.KAR;
import static com.ihg.automation.selenium.common.types.program.KarmaLevel.KIC;
import static com.ihg.automation.selenium.common.types.program.KarmaLevelReason.BASE;
import static com.ihg.automation.selenium.common.types.program.ProgramStatus.OPEN;
import static com.ihg.automation.selenium.matchers.component.ViewableMatcher.hasTextInView;
import static com.ihg.automation.selenium.servicecenter.pages.personal.PersonalInfoPage.SUCCESS_MESSAGE;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberJdbcUtils;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.gwt.components.input.Input;
import com.ihg.automation.selenium.servicecenter.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.servicecenter.pages.programs.KarmaPage;
import com.ihg.automation.selenium.servicecenter.pages.programs.ProgramSummary;
import com.ihg.crm.automation.selenium.tests.servicecenter.LoginLogout;

@Test(groups = { "karma" })
public class KarTierLevelWithoutReasonTest extends LoginLogout
{
    private ProgramSummary summary;
    private String openedReferringMemberId;

    @BeforeClass
    public void beforeClass()
    {
        Member member = new Member();
        member.setPersonalInfo(MemberPopulateHelper.getSimplePersonalInfo());
        member.addAddress(MemberPopulateHelper.getUnitedStatesAddress());
        member.addProgram(Program.KAR);

        openedReferringMemberId = MemberJdbcUtils.getMemberId(jdbcTemplate,
                String.format(MEMBER_ID, "O", Program.KAR.getCode()));

        login();

        new EnrollmentPage().enroll(member);

        KarmaPage karmaPage = new KarmaPage();
        karmaPage.goTo();
        summary = karmaPage.getSummary();
    }

    @BeforeMethod
    public void beforeMethod()
    {
        summary.setTierLevelWithExpiration(KIC, BASE, verifySuccess(SUCCESS_MESSAGE));
    }

    @Test
    public void setKarWithReferringMember()
    {
        summary.clickEdit();
        summary.getTierLevel().selectByValue(KAR);

        Input referringMember = summary.getReferringMember();
        referringMember.typeAndWait(openedReferringMemberId);
        summary.clickSave(verifySuccess(SUCCESS_MESSAGE));

        summary.verify(OPEN, KAR);
        verifyThat(summary.getTierLevelExpiration(), hasTextInView(NOT_AVAILABLE));
        verifyThat(referringMember, hasTextInView(openedReferringMemberId));
    }

    @Test
    public void setKarWithoutReferringMember()
    {
        summary.clickEdit();
        summary.getTierLevel().selectByValue(KAR);
        summary.clickSave(verifySuccess(SUCCESS_MESSAGE));

        summary.verify(OPEN, KAR);
        verifyThat(summary.getTierLevelExpiration(), hasTextInView(NOT_AVAILABLE));
    }
}
