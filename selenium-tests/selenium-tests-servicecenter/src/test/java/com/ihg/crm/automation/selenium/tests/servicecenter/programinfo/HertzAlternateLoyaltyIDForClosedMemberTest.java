package com.ihg.crm.automation.selenium.tests.servicecenter.programinfo;

import static com.ihg.automation.common.RandomUtils.getRandomNumber;
import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyNoError;
import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifySuccess;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.types.program.Program.RC;
import static com.ihg.automation.selenium.common.types.program.ProgramStatus.CLOSED;
import static com.ihg.automation.selenium.common.types.program.ProgramStatus.REMOVED;
import static com.ihg.automation.selenium.common.types.program.ProgramStatusReason.FROZEN;
import static com.ihg.automation.selenium.gwt.components.utils.ByStringText.contains;
import static com.ihg.automation.selenium.hotel.pages.search.GuestSearchResultsGridRow.GuestSearchCell.GUEST_NAME;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.automation.selenium.matchers.component.GridSize.size;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static com.ihg.automation.selenium.servicecenter.pages.history.AuditConstant.AccountInformation.ACCOUNT_INFORMATION;
import static com.ihg.automation.selenium.servicecenter.pages.history.AuditConstant.AccountInformation.ALTERNATE_LOYALTY_ID_HERTZ_ADDED;
import static com.ihg.automation.selenium.servicecenter.pages.history.AuditConstant.AccountInformation.ALTERNATE_LOYALTY_ID_HERTZ_CHANGED;
import static com.ihg.automation.selenium.servicecenter.pages.history.AuditConstant.AccountInformation.ALTERNATE_LOYALTY_ID_HERTZ_DELETED;
import static com.ihg.automation.selenium.servicecenter.pages.history.AuditConstant.AccountInformation.NUMBER;
import static com.ihg.automation.selenium.servicecenter.pages.history.AuditConstant.AccountInformation.TYPE;
import static com.ihg.automation.selenium.servicecenter.pages.programs.ProgramPageBase.SUCCESS_MESSAGE;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.types.program.AlternateLoyaltyIDType;
import com.ihg.automation.selenium.common.types.program.ProgramStatus;
import com.ihg.automation.selenium.gwt.components.dialog.ConfirmDialog;
import com.ihg.automation.selenium.hotel.pages.search.GuestSearchResultsGrid;
import com.ihg.automation.selenium.servicecenter.pages.GuestSearch;
import com.ihg.automation.selenium.servicecenter.pages.LeftPanel;
import com.ihg.automation.selenium.servicecenter.pages.account.AccountInfoPage;
import com.ihg.automation.selenium.servicecenter.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.servicecenter.pages.history.ProfileHistoryGridRow;
import com.ihg.automation.selenium.servicecenter.pages.history.ProfileHistoryPage;
import com.ihg.automation.selenium.servicecenter.pages.personal.PersonalInfoPage;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.AlternateLoyaltyIDGrid;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.EditAlternateLoyaltyIDPopUp;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.RewardClubPage;
import com.ihg.crm.automation.selenium.tests.servicecenter.LoginLogout;

public class HertzAlternateLoyaltyIDForClosedMemberTest extends LoginLogout
{
    public static final String CREATE_NUMBER = getRandomNumber(9);
    private static final String UPDATE_NUMBER = getRandomNumber(9);
    private RewardClubPage rewardClubPage = new RewardClubPage();
    private Member member = new Member();

    @BeforeClass
    public void beforeClass()
    {
        member.setPersonalInfo(MemberPopulateHelper.getSimplePersonalInfo());
        member.addAddress(MemberPopulateHelper.getUnitedStatesAddress());
        member.addProgram(RC);

        login();
        member = new EnrollmentPage().enroll(member);

        rewardClubPage.goTo();
        rewardClubPage.getSummary().setClosedStatus(FROZEN);
    }

    @Test(description = "DE8749 - Getting error message on adding/update Alternate Loyalty ID for Closed Member")
    public void addAlternateLoyaltyID()
    {
        AlternateLoyaltyIDGrid alternateGrid = rewardClubPage.getAlternateLoyaltyID();
        verifyThat(alternateGrid, size(0));

        rewardClubPage.addAlternateLoyaltyId(CREATE_NUMBER);

        verifyThat(alternateGrid, size(1));
        alternateGrid.getRow(1).verifyHertz(CREATE_NUMBER);
    }

    @Test(dependsOnMethods = { "addAlternateLoyaltyID" })
    public void verifyHistoryAfterAddAlternateLoyaltyID()
    {
        ProfileHistoryPage historyPage = new ProfileHistoryPage();
        historyPage.goTo();
        verifyNoError();

        ProfileHistoryGridRow hertzRow = historyPage.getProfileHistoryGrid().getRow(1)
                .getSubRowByName(ACCOUNT_INFORMATION).getSubRowByName(contains(ALTERNATE_LOYALTY_ID_HERTZ_ADDED));

        hertzRow.getSubRowByName(TYPE).verifyAddAction(AlternateLoyaltyIDType.HERTZ);
        hertzRow.getSubRowByName(NUMBER).verifyAddAction(CREATE_NUMBER);
    }

    @Test(dependsOnMethods = { "verifyHistoryAfterAddAlternateLoyaltyID" })
    public void searchByAlternateNumber()
    {
        searchByNumberAndVerify();
    }

    @Test(dependsOnMethods = { "searchByAlternateNumber" })
    public void removeAccountAndSearchByAlternateNumber()
    {
        AccountInfoPage accountInfoPage = new AccountInfoPage();
        accountInfoPage.removedAccount(false);

        searchByNumberAndVerify();

        accountInfoPage.verifyStatus(REMOVED);
        accountInfoPage.clickReinstateAccount();
        verifyThat(accountInfoPage.getAccountStatus(), hasText(CLOSED));
    }

    @Test(dependsOnMethods = { "removeAccountAndSearchByAlternateNumber" })
    public void updateAlternateWithNewNumber()
    {
        rewardClubPage.goTo();
        AlternateLoyaltyIDGrid alternateGrid = rewardClubPage.getAlternateLoyaltyID();
        EditAlternateLoyaltyIDPopUp editAlternateLoyaltyIDPopUp = alternateGrid.getRow(1).openEditAlternateIdPopUp();

        editAlternateLoyaltyIDPopUp.getNumber().type(UPDATE_NUMBER);
        editAlternateLoyaltyIDPopUp.clickSave(verifySuccess(SUCCESS_MESSAGE));

        alternateGrid.getRow(1).verifyHertz(UPDATE_NUMBER);
    }

    @Test(dependsOnMethods = { "updateAlternateWithNewNumber" })
    public void verifyHistoryAfterUpdateAlternateLoyaltyID()
    {
        ProfileHistoryPage historyPage = new ProfileHistoryPage();
        historyPage.goTo();
        verifyNoError();

        ProfileHistoryGridRow hertzRow = historyPage.getProfileHistoryGrid().getRow(1)
                .getSubRowByName(ACCOUNT_INFORMATION).getSubRowByName(contains(ALTERNATE_LOYALTY_ID_HERTZ_CHANGED));

        hertzRow.getSubRowByName(NUMBER).verify(CREATE_NUMBER, UPDATE_NUMBER);
    }

    @Test(dependsOnMethods = { "verifyHistoryAfterUpdateAlternateLoyaltyID" })
    public void deleteAlternateLoyaltyID()
    {
        rewardClubPage.goTo();
        AlternateLoyaltyIDGrid alternateGrid = rewardClubPage.getAlternateLoyaltyID();

        alternateGrid.getRow(1).clickDelete();
        new ConfirmDialog().clickYes(verifyNoError(), verifySuccess(SUCCESS_MESSAGE));

        verifyThat(alternateGrid, size(0));
    }

    @Test(dependsOnMethods = { "deleteAlternateLoyaltyID" })
    public void verifyProfileHistoryAfterDeleteAlternateLoyaltyID()
    {
        ProfileHistoryPage historyPage = new ProfileHistoryPage();
        historyPage.goTo();
        verifyNoError();

        ProfileHistoryGridRow hertzRow = historyPage.getProfileHistoryGrid().getRow(1)
                .getSubRowByName(ACCOUNT_INFORMATION).getSubRowByName(contains(ALTERNATE_LOYALTY_ID_HERTZ_DELETED));

        hertzRow.getSubRowByName(NUMBER).verifyDeleteAction(UPDATE_NUMBER);
    }

    private void searchByNumberAndVerify()
    {
        new LeftPanel().goToNewSearch();
        GuestSearch guestSearch = new GuestSearch();
        guestSearch.getCustomerSearch().getNumber().type(CREATE_NUMBER);
        guestSearch.clickSearch(verifyNoError());

        PersonalInfoPage personalInfoPage = new PersonalInfoPage();

        if (!personalInfoPage.isDisplayed())
        {
            new GuestSearchResultsGrid().getRow(member).getCell(GUEST_NAME).asLink().clickAndWait();
        }
        verifyThat(personalInfoPage, displayed(true));
        new LeftPanel().getCustomerInfoPanel().verifyStatus(ProgramStatus.CLOSED);
        rewardClubPage.goTo();
        verifyThat(rewardClubPage.getAlternateLoyaltyID().getRow(CREATE_NUMBER), displayed(true));
    }
}
