package com.ihg.crm.automation.selenium.tests.servicecenter.enrollment;

import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyError;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.isHighlightedAsInvalid;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.name.PersonNameFields;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.gwt.components.input.InputBase;
import com.ihg.automation.selenium.servicecenter.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.servicecenter.pages.personal.CustomerInfoFields;
import com.ihg.automation.selenium.servicecenter.pages.personal.PersonalInfoPage;
import com.ihg.crm.automation.selenium.tests.servicecenter.LoginLogout;

public class UpdateNameNegativeTest extends LoginLogout
{
    private static final String WARNING_MESSAGE = "Personal info can't be saved, some fields didn't pass validation.";
    private Member member = new Member();
    private PersonNameFields personNameEdit;
    private CustomerInfoFields customerInfo;
    private PersonalInfoPage personalInfoPage = new PersonalInfoPage();

    @BeforeClass
    public void before()
    {
        member.addProgram(Program.RC);
        member.setPersonalInfo(MemberPopulateHelper.getSimplePersonalInfo());
        member.addAddress(MemberPopulateHelper.getUnitedStatesAddress());

        login();
        new EnrollmentPage().enroll(member);
        personalInfoPage.goTo();

        customerInfo = personalInfoPage.getCustomerInfo();
        customerInfo.clickEdit();
        personNameEdit = customerInfo.getName();
    }

    @BeforeMethod
    public void beforeMethod()
    {
        personNameEdit.getSalutation().clear();
        personNameEdit.getGivenName().type("Test");
        personNameEdit.getMiddleName().clear();
        personNameEdit.getSurname().type("Test");
        personNameEdit.getSuffix().clear();
        personNameEdit.getTitle().clear();
        personNameEdit.getDegree().clear();
    }

    @Test(dataProviderClass = NegativeNamesDataProvider.class, dataProvider = "negativeNames")
    public void verifySalutationWithInvalidValue(String salutation)
    {
        verifyFieldWithInvalidValue(personNameEdit.getSalutation(), salutation);
    }

    @Test(dataProviderClass = NegativeNamesDataProvider.class, dataProvider = "negativeNames")
    public void verifyFirstNameWithInvalidValue(String firstName)
    {
        verifyFieldWithInvalidValue(personNameEdit.getGivenName(), firstName);
    }

    @Test(dataProviderClass = NegativeNamesDataProvider.class, dataProvider = "negativeNames")
    public void verifyMiddleNameWithInvalidValue(String middleName)
    {
        verifyFieldWithInvalidValue(personNameEdit.getMiddleName(), middleName);
    }

    @Test(dataProviderClass = NegativeNamesDataProvider.class, dataProvider = "negativeNames")
    public void verifyLastNameWithInvalidValue(String lastName)
    {
        verifyFieldWithInvalidValue(personNameEdit.getSurname(), lastName);
    }

    @Test(dataProviderClass = NegativeNamesDataProvider.class, dataProvider = "negativeNames")
    public void verifySuffixWithInvalidValue(String suffix)
    {
        verifyFieldWithInvalidValue(personNameEdit.getSuffix(), suffix);
    }

    @Test(dataProviderClass = NegativeNamesDataProvider.class, dataProvider = "negativeNames")
    public void verifyTitleWithInvalidValue(String title)
    {
        verifyFieldWithInvalidValue(personNameEdit.getTitle(), title);
    }

    @Test(dataProviderClass = NegativeNamesDataProvider.class, dataProvider = "negativeNames")
    public void verifyDegreeWithInvalidValue(String degree)
    {
        verifyFieldWithInvalidValue(personNameEdit.getDegree(), degree);
    }

    private void verifyFieldWithInvalidValue(InputBase inputField, String inputValue)
    {
        inputField.clickAndWait();
        inputField.typeAndWait(inputValue);
        customerInfo.clickSave(verifyError(WARNING_MESSAGE));
        verifyThat(inputField, isHighlightedAsInvalid(true));
    }
}
