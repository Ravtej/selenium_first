package com.ihg.crm.automation.selenium.tests.servicecenter.copperfield;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.Constant.NOT_AVAILABLE;
import static com.ihg.automation.selenium.common.types.program.ProgramStatusReason.CUSTOMER_REQUEST;
import static com.ihg.automation.selenium.common.types.program.ProgramStatusReason.DUPLICATE;
import static com.ihg.automation.selenium.common.types.program.ProgramStatusReason.FROZEN;
import static com.ihg.automation.selenium.common.types.program.ProgramStatusReason.IHG_REQUEST;
import static com.ihg.automation.selenium.common.types.program.ProgramStatusReason.MUTUALLY_EXCLUSIVE;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.enabled;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasDefault;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasText;
import static org.hamcrest.Matchers.allOf;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.hotel.pages.personal.PersonalInfoPage;
import com.ihg.automation.selenium.servicecenter.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.RewardClubPage;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.RewardClubSummary;
import com.ihg.crm.automation.selenium.tests.servicecenter.LoginLogout;

public class RestoreButtonAvailabilityTest extends LoginLogout
{
    private static final String ZERO_EXP_AMOUNT = "0";
    private static final String NON_ZERO_EXP_AMOUNT = "20";
    private final static String UPDATE_EXPIRED_POINT_BALANCE = "UPDATE DGST.MBRSHP SET"//
            + " EXPIRED_PT_BAL_AMT = '%1$s',"//
            + " LST_UPDT_USR_ID='%3$s',"//
            + " LST_UPDT_TS = SYSDATE"//
            + " WHERE MBRSHP_ID='%2$s' AND LYTY_PGM_CD='PC'";

    private RewardClubPage rewardClubPage = new RewardClubPage();
    private RewardClubSummary summary = rewardClubPage.getSummary();
    private Member member = new Member();

    @BeforeClass
    public void before()
    {
        member.setPersonalInfo(MemberPopulateHelper.getSimplePersonalInfo());
        member.addAddress(MemberPopulateHelper.getUnitedStatesAddress());
        member.addProgram(Program.RC);

        login();

        new EnrollmentPage().enroll(member);

        rewardClubPage.goTo();
    }

    @Test(priority = 10, groups = { "copperfield" })
    public void verifyRestoreDisabledAfterEnrollment()
    {
        verifySummaryExpirationFields(NOT_AVAILABLE, false);
    }

    @Test(priority = 20, groups = { "copperfield" })
    public void updateExpBalanceToZeroAndVerify()
    {
        updateExpiredPointBalance(ZERO_EXP_AMOUNT);

        switchBetweenTabs();

        verifySummaryExpirationFields(ZERO_EXP_AMOUNT, false);
    }

    @Test(priority = 25, groups = { "copperfield" })
    public void updateExpBalanceVerifyRestoreAvailable()
    {
        updateExpiredPointBalance(NON_ZERO_EXP_AMOUNT);

        switchBetweenTabs();

        verifySummaryExpirationFields(NON_ZERO_EXP_AMOUNT, true);
    }

    @Test(priority = 30, groups = { "copperfield" }, dataProvider = "tierLevelReason")
    public void updateExpBalanceCloseMember(String closedStatus)
    {
        summary.setClosedStatus(closedStatus);

        verifySummaryExpirationFields(NON_ZERO_EXP_AMOUNT, false);
    }

    private void verifySummaryExpirationFields(String expAmount, boolean enabled)
    {
        verifyThat(summary.getBalance(), hasText("0"));
        verifyThat(summary.getExpiredLoyaltyUnitBalance(), hasText(expAmount));
        verifyThat(summary.getBalanceExpirationDate(), hasDefault());
        verifyThat(summary.getLastActivityDate(), hasDefault());
        verifyThat(summary.getLastAdjusted(), hasDefault());

        verifyThat(summary.getRestore(), allOf(displayed(true), enabled(enabled)));
    }

    private void updateExpiredPointBalance(String amount)
    {
        jdbcTemplateUpdate
                .update(String.format(UPDATE_EXPIRED_POINT_BALANCE, amount, member.getRCProgramId(), lastUpdateUserId));
    }

    private void switchBetweenTabs()
    {
        // switch between Tabs for new expired balance to appear
        new PersonalInfoPage().goTo();
        rewardClubPage.goTo();
    }

    @DataProvider(name = "tierLevelReason")
    public Object[][] invalidVouchers()
    {
        return new Object[][] { { CUSTOMER_REQUEST }, { DUPLICATE }, { FROZEN }, { IHG_REQUEST },
                { MUTUALLY_EXCLUSIVE } };
    }
}
