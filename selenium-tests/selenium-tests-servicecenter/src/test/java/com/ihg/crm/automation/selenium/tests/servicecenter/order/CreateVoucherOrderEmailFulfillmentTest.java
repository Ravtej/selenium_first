package com.ihg.crm.automation.selenium.tests.servicecenter.order;

import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyNoError;
import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifySuccess;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.enroll.EmailConstant.EMAIL_WITH_10_DUPLICATES;
import static com.ihg.automation.selenium.common.order.DeliveryOption.EMAIL;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.enabled;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static org.apache.commons.lang3.StringUtils.EMPTY;
import static org.hamcrest.core.IsNot.not;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.common.DateUtils;
import com.ihg.automation.selenium.common.Constant;
import com.ihg.automation.selenium.common.event.EarningEvent;
import com.ihg.automation.selenium.common.order.Voucher;
import com.ihg.automation.selenium.common.order.VoucherOrder;
import com.ihg.automation.selenium.common.order.VoucherOrderStatus;
import com.ihg.automation.selenium.common.order.VoucherRule;
import com.ihg.automation.selenium.common.order.VoucherRuleType;
import com.ihg.automation.selenium.common.order.VoucherStatusType;
import com.ihg.automation.selenium.common.types.program.RewardClubLevel;
import com.ihg.automation.selenium.common.voucher.VoucherOrderPopUp;
import com.ihg.automation.selenium.gwt.components.input.Select;
import com.ihg.automation.selenium.servicecenter.pages.LeftPanel;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsGridRow;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsPage;
import com.ihg.automation.selenium.servicecenter.pages.event.order.OrderDetailsEdit;
import com.ihg.automation.selenium.servicecenter.pages.event.order.OrderDetailsPopUp;
import com.ihg.automation.selenium.servicecenter.pages.event.order.OrderDetailsTab;
import com.ihg.automation.selenium.servicecenter.pages.event.order.OrderEventsPage;
import com.ihg.automation.selenium.servicecenter.pages.event.order.OrderGridRow;
import com.ihg.automation.selenium.servicecenter.pages.event.order.VoucherOrderGridRowContainer;
import com.ihg.automation.selenium.servicecenter.pages.voucher.VoucherGridRow;
import com.ihg.automation.selenium.servicecenter.pages.voucher.VoucherLookupTab;
import com.ihg.automation.selenium.servicecenter.pages.voucher.VoucherPage;
import com.ihg.automation.selenium.servicecenter.pages.voucher.VoucherPopUp;

public class CreateVoucherOrderEmailFulfillmentTest extends VoucherOrderCommon
{
    private VoucherRule voucherRule;
    private VoucherOrder voucherOrder;
    private VoucherPage voucherPage = new VoucherPage();
    private OrderEventsPage orderEventsPage = new OrderEventsPage();
    private AllEventsPage allEventsPage = new AllEventsPage();

    @BeforeClass
    public void beforeClass()
    {
        voucherRule = new VoucherRule("CTUCGE", "Corporate Gold Upgrade for EMEA");
        voucherRule.setVoucherName("GOLD UPGRADE");
        voucherRule.setRuleType(VoucherRuleType.Upgrade);
        voucherRule.setCostAmount("20.00");
        voucherRule.setTierLevelCodeOfUpgrade(RewardClubLevel.GOLD.getCode());
        voucherRule.setTotalCostAmount("500.00");

        loginAndOpenHotelMemberProfile();
    }

    @Test
    public void searchVoucher()
    {
        VoucherGridRow row = searchVoucherRow(voucherRule);
        row.verify(voucherRule);
    }

    @Test(dependsOnMethods = { "searchVoucher" })
    public void tryToOrderVoucherWithDuplicateEmail()
    {
        voucherPage.getVouchers().getRow(voucherRule.getPromotionId()).clickById();
        VoucherOrderPopUp orderPopUp = new VoucherOrderPopUp();
        orderPopUp.getQuantity().select(voucherRule.getQuantity());
        orderPopUp.getDeliveryOption().select(EMAIL);
        Select email = orderPopUp.getEmail();
        email.type(EMAIL_WITH_10_DUPLICATES);
        orderPopUp.clickPlaceOrder(verifyNoError(), verifySuccess(VOUCHERS_ORDER_SUCCESS_MESSAGE));
        verifyThat(new LeftPanel().getCustomerInfoPanel().getEmail(), hasText(not(EMAIL_WITH_10_DUPLICATES)));
    }

    @Test(dependsOnMethods = { "tryToOrderVoucherWithDuplicateEmail" })
    public void orderVouchers()
    {
        voucherPage.getVouchers().getRow(voucherRule.getPromotionId()).clickById();
        VoucherOrderPopUp orderPopUp = new VoucherOrderPopUp();
        orderPopUp.getQuantity().select(voucherRule.getQuantity());
        orderPopUp.getDeliveryOption().select(EMAIL);
        orderPopUp.verifyShippingInfoEmailFulfillment();
        orderPopUp.clickPlaceOrder(verifyNoError(), verifySuccess(VOUCHERS_ORDER_SUCCESS_MESSAGE));
        voucherPage.clickClose();
    }

    @Test(dependsOnMethods = { "orderVouchers" })
    public void verifyEventOnOrder()
    {
        voucherOrder = new VoucherOrder(voucherRule);
        voucherOrder.setStatus(VoucherOrderStatus.AT_VENDOR);
        voucherOrder.setDeliveryOption(EMAIL);
        voucherOrder.setSource(helper);
        initVoucherOrderRange(voucherOrder);

        orderEventsPage.goTo();

        OrderGridRow row = orderEventsPage.getOrdersGrid().getRow(1);
        row.verify(voucherOrder);

        VoucherOrderGridRowContainer rowContainer = row.expand(VoucherOrderGridRowContainer.class);
        rowContainer.verify(voucherOrder);
    }

    @Test(dependsOnMethods = { "verifyEventOnOrder" })
    public void verifyOrderDetailsPopUpFromOrder()
    {
        EarningEvent earningEvent = new EarningEvent(Constant.BASE_AWARDS, "1", EMPTY, EMPTY, "at Vendor");
        earningEvent.setAward(voucherOrder.getPromotionId());
        earningEvent.setAllianceSentDate(DateUtils.getFormattedDate(DateUtils.dd_MMM_YYYY));

        orderEventsPage.getOrdersGrid().getRow(1).expand(VoucherOrderGridRowContainer.class).clickDetails();
        OrderDetailsPopUp orderDetailsPopUp = new OrderDetailsPopUp();

        OrderDetailsTab orderDetailsTab = orderDetailsPopUp.getOrderDetailsTab();
        orderDetailsTab.goTo();
        orderDetailsTab.verify(voucherOrder);

        OrderDetailsEdit orderDetails = orderDetailsTab.getOrderDetails();
        verifyThat(orderDetails.getEstimatedDeliveryDate(), enabled(false));
        verifyThat(orderDetails.getActualDeliveryDate(), enabled(false));
        verifyThat(orderDetails.getDeliveryStatus(), enabled(false));
        verifyThat(orderDetails.getTrackingUniqueNumber(), enabled(true));
        verifyThat(orderDetails.getSignedForBy(), enabled(true));
        verifyThat(orderDetails.getComments(), enabled(true));

        verifyEarningDetailsTab(orderDetailsPopUp, earningEvent);

        verifyBillingDetailsTab(orderDetailsPopUp, "500", voucherOrder.getCurrencyCode());

        verifyHistoryDetailsTab(orderDetailsPopUp);

        orderDetailsPopUp.clickClose();
    }

    @Test(dependsOnMethods = { "verifyOrderDetailsPopUpFromOrder" })
    public void verifyOrderStatusOnAllEvents()
    {
        allEventsPage.goTo();

        AllEventsGridRow row = allEventsPage.getGrid().getRow(1);

        VoucherOrderGridRowContainer rowContainer = row.expand(VoucherOrderGridRowContainer.class);
        verifyThat(rowContainer.getOrderDetails().getDeliveryStatus(), hasText(voucherOrder.getStatus()));
    }

    @Test(dependsOnMethods = { "verifyOrderStatusOnAllEvents" })
    public void verifyVoucherLookup()
    {
        VoucherLookupTab lookupTab = new VoucherPopUp().getVoucherLookupTab();
        lookupTab.goTo();

        final String voucherNumber = getFirstVoucherNumber(voucherOrder.getPromotionId());
        lookupTab.lookup(voucherNumber);

        Voucher voucher = new Voucher(voucherNumber, voucherRule.getVoucherName(), VoucherStatusType.Active, helper);
        lookupTab.getVoucherDetailsPanel().verify(voucher);
        lookupTab.getStatusGrid().getRow(1).verify(voucher);
        lookupTab.getOrderDetailsPanel().verify(voucherOrder);
    }
}
