package com.ihg.crm.automation.selenium.tests.servicecenter.business;

import static com.ihg.automation.common.DateUtils.getFormattedDate;
import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyError;
import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyNoError;
import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifySuccess;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.Constant.NOT_AVAILABLE;
import static com.ihg.automation.selenium.common.business.BusinessEventType.INCENTIVE;
import static com.ihg.automation.selenium.common.business.BusinessStatus.BLOCKED;
import static com.ihg.automation.selenium.common.history.EventAuditConstant.CREATE;
import static com.ihg.automation.selenium.common.history.EventAuditConstant.NO;
import static com.ihg.automation.selenium.common.history.EventAuditConstant.YES;
import static com.ihg.automation.selenium.common.history.EventAuditConstant.BusinessReward.END_DATE;
import static com.ihg.automation.selenium.common.history.EventAuditConstant.BusinessReward.ESTIMATED_BILLING_AMOUNT;
import static com.ihg.automation.selenium.common.history.EventAuditConstant.BusinessReward.ESTIMATED_CURRENCY;
import static com.ihg.automation.selenium.common.history.EventAuditConstant.BusinessReward.START_DATE;
import static com.ihg.automation.selenium.common.history.EventAuditConstant.BusinessReward.Discretionary.POINTS;
import static com.ihg.automation.selenium.common.history.EventAuditConstant.BusinessReward.Discretionary.REASON;
import static com.ihg.automation.selenium.common.history.EventAuditConstant.BusinessReward.Event.EVENT_ID;
import static com.ihg.automation.selenium.common.history.EventAuditConstant.BusinessReward.Event.EVENT_NAME;
import static com.ihg.automation.selenium.common.history.EventAuditConstant.BusinessReward.Event.EVENT_STATUS;
import static com.ihg.automation.selenium.common.history.EventAuditConstant.BusinessReward.Event.HOTEL;
import static com.ihg.automation.selenium.common.history.EventAuditConstant.BusinessReward.Event.HOTEL_CURRENCY;
import static com.ihg.automation.selenium.common.history.EventAuditConstant.BusinessReward.Event.MEMBER_NUMBER;
import static com.ihg.automation.selenium.common.history.EventAuditConstant.BusinessReward.Meeting.FOOD_BEVERAGE;
import static com.ihg.automation.selenium.common.history.EventAuditConstant.BusinessReward.Meeting.MEETING_ROOM;
import static com.ihg.automation.selenium.common.history.EventAuditConstant.BusinessReward.Meeting.MISCELLANEOUS;
import static com.ihg.automation.selenium.common.history.EventAuditConstant.BusinessReward.Meeting.NUMBER_OF_MEETING_ROOMS;
import static com.ihg.automation.selenium.common.history.EventAuditConstant.BusinessReward.Meeting.TOTAL_DELEGATES;
import static com.ihg.automation.selenium.common.history.EventAuditConstant.BusinessReward.Room.FORCE_QUALIFIED_INDICATOR;
import static com.ihg.automation.selenium.common.history.EventAuditConstant.BusinessReward.Room.QUALIFIED_INDICATOR;
import static com.ihg.automation.selenium.common.history.EventAuditConstant.BusinessReward.Room.STAY_END_DATE;
import static com.ihg.automation.selenium.common.history.EventAuditConstant.BusinessReward.Room.STAY_START_DATE;
import static com.ihg.automation.selenium.common.history.EventAuditConstant.BusinessReward.Room.TOTAL_GUESTS;
import static com.ihg.automation.selenium.common.history.EventAuditConstant.BusinessReward.Room.TOTAL_GUEST_ROOMS;
import static com.ihg.automation.selenium.common.history.EventAuditConstant.BusinessReward.Room.TOTAL_ROOM_NIGHTS;
import static com.ihg.automation.selenium.common.types.Currency.EUR;
import static com.ihg.automation.selenium.common.types.Currency.USD;
import static com.ihg.automation.selenium.formatter.Converter.doubleToString;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasAnyText;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasTextAsInt;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static com.ihg.automation.selenium.matchers.component.IsDisplayed.displayed;
import static com.ihg.automation.selenium.matchers.component.IsEnabled.enabled;
import static com.ihg.automation.selenium.matchers.text.ContainsIgnoreCase.containsIgnoreCase;
import static com.ihg.automation.selenium.servicecenter.pages.Messages.HIGHLIGHTED_FIELDS_DID_NOT_PASS_VALIDATION;
import static org.hamcrest.core.IsNot.not;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.common.DateUtils;
import com.ihg.automation.common.GregorianCalendarUtils;
import com.ihg.automation.selenium.common.business.BusinessEvent;
import com.ihg.automation.selenium.common.business.BusinessOffer;
import com.ihg.automation.selenium.common.business.BusinessRevenueBean;
import com.ihg.automation.selenium.common.business.BusinessRevenueView;
import com.ihg.automation.selenium.common.business.BusinessRewardsEventDetailsPopUp;
import com.ihg.automation.selenium.common.business.DiscretionaryPoints;
import com.ihg.automation.selenium.common.business.EventSummaryTab;
import com.ihg.automation.selenium.common.business.GuestRoom;
import com.ihg.automation.selenium.common.business.GuestRoomsFields;
import com.ihg.automation.selenium.common.business.MeetingDetails;
import com.ihg.automation.selenium.common.business.MeetingFields;
import com.ihg.automation.selenium.common.business.MeetingRevenueDetails;
import com.ihg.automation.selenium.common.business.OffersFieldsEdit;
import com.ihg.automation.selenium.common.history.EventAuditConstant;
import com.ihg.automation.selenium.common.history.EventHistoryGrid;
import com.ihg.automation.selenium.common.history.EventHistoryGridRow;
import com.ihg.automation.selenium.common.history.EventHistoryRecords;
import com.ihg.automation.selenium.common.history.EventHistoryTab;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.gwt.components.Component;
import com.ihg.automation.selenium.gwt.components.dialog.ConfirmDialog;
import com.ihg.automation.selenium.servicecenter.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.servicecenter.pages.event.business.BusinessEventsPage;
import com.ihg.crm.automation.selenium.tests.servicecenter.LoginLogout;
import com.ihg.crm.automation.service.refdata.ConvertCurrenciesRequestHelper;

public class CreateEventTest extends LoginLogout
{
    private static final String OFFER_NAME = "TEST OFFER DON'T MIGRATE";
    private BusinessEventsPage businessEventsPage = new BusinessEventsPage();
    private BusinessRewardsEventDetailsPopUp popUp;
    private BusinessRevenueView totalEligibleRevenue;
    private EventSummaryTab eventSummaryTab;
    private MeetingFields meetingFields;
    private BusinessEvent businessEvent;
    private Member member = new Member();
    private MeetingDetails meetingDetails = new MeetingDetails();

    @BeforeClass
    public void beforeClass()
    {
        member.addProgram(Program.BR);
        member.addAddress(MemberPopulateHelper.getUnitedStatesAddress());
        member.setPersonalInfo(MemberPopulateHelper.getSimplePersonalInfo());

        businessEvent = new BusinessEvent("NewEvent001", "CEQHA", EUR);
        businessEvent.setStatus(BLOCKED);

        meetingDetails.setStartDate(getFormattedDate(-7));
        meetingDetails.setEndDate(getFormattedDate(-5));
        meetingDetails.setNumberOfMeetingRooms(100);
        meetingDetails.setTotalDelegates(100);
        meetingDetails.setMeetingRoom(new BusinessRevenueBean(100.00));
        meetingDetails.setFoodBeverage(new BusinessRevenueBean(100.00));
        meetingDetails.setMiscellaneous(new BusinessRevenueBean(100.00));

        GuestRoom guestRoom = new GuestRoom();
        guestRoom.setStartDate(meetingDetails.getStartDate());
        guestRoom.setEndDate(meetingDetails.getEndDate());
        guestRoom.setTotalGuestRooms(12);
        guestRoom.setTotalRoomNights(10);
        guestRoom.setTotalGuests(12);
        guestRoom.setTotalRoom(new BusinessRevenueBean(1000.00));

        businessEvent.setMeetingDetails(meetingDetails);
        businessEvent.setGuestRoom(guestRoom);
        businessEvent.setDiscretionaryPoints(new DiscretionaryPoints("1111", INCENTIVE));
        businessEvent.setOffer(new BusinessOffer(OFFER_NAME, "500"));

        login();
        new EnrollmentPage().enroll(member);

        businessEventsPage.goTo();
    }

    @Test
    public void populateEventInformation()
    {
        popUp = businessEventsPage.createEvent();
        popUp.getEventSummaryTab().getEventInformation().populate(businessEvent);
    }

    @Test(dependsOnMethods = { "populateEventInformation" }, alwaysRun = true)
    public void checkIncludeMeeting()
    {
        meetingFields = popUp.getEventSummaryTab().getEventDetails().getMeetingFields();
        meetingFields.expand();
        meetingFields.verifyEnabled(false);
        meetingFields.getIncludeMeeting().check();
        meetingFields.verifyEnabled(true);
    }

    @Test(dependsOnMethods = { "checkIncludeMeeting" }, alwaysRun = true)
    public void createEvent()
    {
        String SUCCESS_CREATION_MESSAGE = "IHG Business Rewards Event has been successfully created. Member is Pending and will not earn Points. Member must agree to Terms and Conditions.";

        String currencyConversion = refdataClient
                .convertCurrencies(ConvertCurrenciesRequestHelper.getCurrencies(100, EUR.getCode(),
                        GregorianCalendarUtils.convert(DateUtils.now().minusDays(5))))
                .getConversionResponseItem().get(0).getConvertedAmount().toString();

        meetingFields.populate(businessEvent.getMeetingDetails());

        MeetingRevenueDetails meetingRevenueDetail = meetingFields.getRevenueDetails();
        meetingRevenueDetail.populateLocalAmount(businessEvent.getMeetingDetails());

        Component meetingRoomAmount = meetingRevenueDetail.getMeetingRoom().getUSDAmount();
        Component foodAndBeverageAmount = meetingRevenueDetail.getFoodAndBeverage().getUSDAmount();
        Component miscellaneousAmount = meetingRevenueDetail.getMiscellaneous().getUSDAmount();

        Double totalCurrencyConversion = Double.parseDouble(meetingRoomAmount.getText())
                + Double.parseDouble(foodAndBeverageAmount.getText())
                + Double.parseDouble(miscellaneousAmount.getText());

        verifyThat(meetingRoomAmount, hasText(currencyConversion));
        verifyThat(foodAndBeverageAmount, hasText(currencyConversion));
        verifyThat(miscellaneousAmount, hasText(currencyConversion));

        BusinessRevenueView businessRevenueView = meetingRevenueDetail.getTotalMeetingRevenue();
        verifyThat(businessRevenueView.getLocalAmount(), hasText("300.00"));
        verifyThat(businessRevenueView.getUSDAmount(), hasText(doubleToString(totalCurrencyConversion)));

        totalEligibleRevenue = popUp.getEventSummaryTab().getTotalEligibleRevenue();
        totalEligibleRevenue.verifyDefault();

        popUp.clickCalculateEstimates();
        verifyThat(totalEligibleRevenue.getEstimatedCost(), hasText(not(NOT_AVAILABLE)));
        verifyThat(totalEligibleRevenue.getEstimatedPointEarning(), hasText(not(NOT_AVAILABLE)));
        popUp.clickSave(verifySuccess(SUCCESS_CREATION_MESSAGE));

        businessEventsPage = new BusinessEventsPage();
        businessEventsPage.getGrid().getRow(1).verify(businessEvent);
    }

    @Test(dependsOnMethods = { "createEvent" }, alwaysRun = true)
    public void verifyHistoryAfterCreateEvent()
    {
        popUp = businessEventsPage.getGrid().getRow(1).openBRDetailsPopUp();

        EventHistoryTab historyTab = popUp.getHistoryTab();
        historyTab.goTo();

        EventHistoryGrid grid = historyTab.getGrid();
        EventHistoryGridRow historyRow = grid.getRow(1);
        historyRow.verify(CREATE, helper.getUser().getNameFull());

        EventHistoryRecords brRecords = historyRow.getAllRecords();
        brRecords.getRecord(2).verifyAdd(EVENT_ID, businessEvent.getEventId());
        brRecords.getRecord(3).verifyAdd(EVENT_NAME, businessEvent.getEventName());
        brRecords.getRecord(4).verifyAdd(EVENT_STATUS, hasText(containsIgnoreCase(businessEvent.getStatus())));
        brRecords.getRecord(5).verifyAdd(START_DATE, meetingDetails.getStartDate());
        brRecords.getRecord(6).verifyAdd(END_DATE, meetingDetails.getEndDate());
        brRecords.getRecord(7).verifyAdd(MEMBER_NUMBER, member.getRCProgramId());
        brRecords.getRecord(8).verifyAdd(HOTEL, businessEvent.getHotel());
        brRecords.getRecord(9).verifyAdd(HOTEL_CURRENCY, businessEvent.getHotelCurrency().getCode());

        brRecords.getRecord(11).verifyAdd(START_DATE, meetingDetails.getStartDate());
        brRecords.getRecord(12).verifyAdd(END_DATE, meetingDetails.getEndDate());
        brRecords.getRecord(13).verifyAdd(NUMBER_OF_MEETING_ROOMS,
                hasTextAsInt(meetingDetails.getNumberOfMeetingRooms()));
        brRecords.getRecord(14).verifyAdd(TOTAL_DELEGATES, hasTextAsInt(meetingDetails.getTotalDelegates()));

        brRecords.verifyAddBusinessRevenue(MEETING_ROOM, meetingDetails.getMeetingRoom());
        brRecords.verifyAddBusinessRevenue(FOOD_BEVERAGE, meetingDetails.getFoodBeverage());
        brRecords.verifyAddBusinessRevenue(MISCELLANEOUS, meetingDetails.getMiscellaneous());

        popUp.clickClose();
    }

    @Test(dependsOnMethods = { "verifyHistoryAfterCreateEvent" }, alwaysRun = true)
    public void cancelEventCreation()
    {
        businessEvent.setEventName("NewEvent002");

        businessEventsPage = new BusinessEventsPage();
        popUp = businessEventsPage.createEvent();
        popUp.getEventSummaryTab().populateEventAndMeetingFields(businessEvent);
        popUp.clickCancel();

        new ConfirmDialog().clickYes(verifyNoError());
        verifyThat(businessEventsPage.getGrid().getRow(businessEvent), displayed(false));
    }

    @Test(dependsOnMethods = { "cancelEventCreation" }, alwaysRun = true)
    public void createEventWithGuestRoomsAndDiscretionaryPoints()
    {
        businessEvent.setEventName("NewEvent003");

        businessEventsPage = new BusinessEventsPage();
        popUp = businessEventsPage.createEvent();

        eventSummaryTab = popUp.getEventSummaryTab();
        eventSummaryTab.getEventInformation().populate(businessEvent);
        eventSummaryTab.getDiscretionaryPoints().populate(businessEvent.getDiscretionaryPoints());
        verifyThat(popUp.getCalculateEstimates(), enabled(false));

        GuestRoomsFields guestRoomsFields = eventSummaryTab.getEventDetails().getGuestRoomsFields();

        guestRoomsFields.expand();
        guestRoomsFields.verifyEnabled(false);
        guestRoomsFields.getIncludeGuestRoomsRevenue().check();
        guestRoomsFields.verifyEnabled(true);

        GuestRoom guestRoom = businessEvent.getGuestRoom();
        guestRoomsFields.getStayStartDate().type(guestRoom.getStartDate());
        guestRoomsFields.getStayEndDate().type(guestRoom.getEndDate());
        guestRoomsFields.getTotalGuestRooms().type(guestRoom.getTotalGuestRooms());
        guestRoomsFields.getTotalRoomNights().type(guestRoom.getTotalRoomNights());
        guestRoomsFields.getTotalGuests().type(guestRoom.getTotalGuests());
        guestRoomsFields.getTotalRoom().populateLocalAmount(guestRoom.getTotalRoom());

        totalEligibleRevenue = popUp.getEventSummaryTab().getTotalEligibleRevenue();
        totalEligibleRevenue.verifyDefault();

        popUp.clickCalculateEstimates();
        verifyThat(totalEligibleRevenue.getEstimatedCost(), hasText(not(NOT_AVAILABLE)));
        verifyThat(totalEligibleRevenue.getEstimatedPointEarning(), hasText(not(NOT_AVAILABLE)));
        popUp.clickSave(verifyNoError());

        businessEventsPage = new BusinessEventsPage();
        businessEventsPage.getGrid().getRow(1).verify(businessEvent);
    }

    @Test(dependsOnMethods = { "createEventWithGuestRoomsAndDiscretionaryPoints" }, alwaysRun = true)
    public void verifyHistoryAfterCreateEventWithRoomsAndDiscretionary()
    {
        popUp = businessEventsPage.getGrid().getRow(1).openBRDetailsPopUp();

        EventHistoryTab historyTab = popUp.getHistoryTab();
        historyTab.goTo();

        EventHistoryGrid grid = historyTab.getGrid();
        EventHistoryGridRow historyRow = grid.getRow(1);
        historyRow.verify(CREATE, helper.getUser().getNameFull());

        EventHistoryRecords brRecords = historyRow.getAllRecords();
        brRecords.getRecord(EVENT_ID).verifyAdd(businessEvent.getEventId());
        brRecords.getRecord(EVENT_NAME).verifyAdd(businessEvent.getEventName());
        brRecords.getRecord(EVENT_STATUS).verifyAdd(hasText(containsIgnoreCase(businessEvent.getStatus())));
        brRecords.getRecord(START_DATE).verifyAdd(meetingDetails.getStartDate());
        brRecords.getRecord(END_DATE).verifyAdd(meetingDetails.getEndDate());
        brRecords.getRecord(MEMBER_NUMBER).verifyAdd(member.getRCProgramId());
        brRecords.getRecord(HOTEL).verifyAdd(businessEvent.getHotel());
        brRecords.getRecord(HOTEL_CURRENCY).verifyAdd(businessEvent.getHotelCurrency().getCode());

        DiscretionaryPoints discretionaryPoints = businessEvent.getDiscretionaryPoints();
        brRecords.getRecord(POINTS).verifyAdd(discretionaryPoints.getAdditionalPointsToAward());
        brRecords.getRecord(REASON).verifyAdd(discretionaryPoints.getReason());
        brRecords.getRecord(ESTIMATED_CURRENCY).verifyAdd(USD.getCode());
        brRecords.getRecord(ESTIMATED_BILLING_AMOUNT).verifyAdd(hasAnyText());

        GuestRoom guestRoom = businessEvent.getGuestRoom();
        brRecords.getRecord(STAY_START_DATE).verifyAdd(guestRoom.getStartDate());
        brRecords.getRecord(STAY_END_DATE).verifyAdd(guestRoom.getEndDate());
        brRecords.getRecord(TOTAL_GUEST_ROOMS).verifyAdd(hasTextAsInt(guestRoom.getTotalGuestRooms()));
        brRecords.getRecord(TOTAL_ROOM_NIGHTS).verifyAdd(hasTextAsInt(guestRoom.getTotalRoomNights()));
        brRecords.getRecord(TOTAL_GUESTS).verifyAdd(hasTextAsInt(guestRoom.getTotalGuests()));

        brRecords.getRecord(QUALIFIED_INDICATOR).verifyAdd(YES);
        brRecords.getRecord(FORCE_QUALIFIED_INDICATOR).verifyAdd(NO);

        brRecords.verifyAddBusinessRevenue(guestRoom.getTotalRoom());

        popUp.clickClose();
    }

    @Test(dependsOnMethods = { "verifyHistoryAfterCreateEventWithRoomsAndDiscretionary" }, alwaysRun = true)
    public void tryToCreateEventWithDiscretionaryPointsMoreThan20M()
    {
        String WARNING_MESSAGE = "The event you are trying to save exceeds the maximum of 20,000,000 discretionary bonus points. Please adjust your event and try to save again.";
        businessEvent.setEventName("NewEvent004");
        businessEvent.getDiscretionaryPoints().setAdditionalPointsToAward("20000001");

        popUp = new BusinessEventsPage().createEvent();
        eventSummaryTab = popUp.getEventSummaryTab();
        eventSummaryTab.populateEventAndMeetingFields(businessEvent);
        eventSummaryTab.getDiscretionaryPoints().populate(businessEvent.getDiscretionaryPoints());

        popUp.clickCalculateEstimates(verifyError(WARNING_MESSAGE));
        popUp.clickSave(verifyError(WARNING_MESSAGE));

        verifyThat(totalEligibleRevenue.getEstimatedCost(), hasText(NOT_AVAILABLE));
        verifyThat(totalEligibleRevenue.getEstimatedPointEarning(), hasText(NOT_AVAILABLE));
    }

    @Test(dependsOnMethods = { "tryToCreateEventWithDiscretionaryPointsMoreThan20M" }, alwaysRun = true)
    public void createEventWithDiscretionaryPointsWith20M()
    {
        eventSummaryTab.getDiscretionaryPoints().getAdditionalPointsToAward().type("20000000");
        popUp.clickCalculateEstimates(verifyNoError());
        verifyThat(totalEligibleRevenue.getEstimatedCost(), hasText(not(NOT_AVAILABLE)));
        verifyThat(totalEligibleRevenue.getEstimatedPointEarning(), hasText(not(NOT_AVAILABLE)));
        popUp.clickSave(verifyNoError());

        businessEventsPage = new BusinessEventsPage();
        businessEventsPage.getGrid().getRow(1).verify(businessEvent);
    }

    @Test(dependsOnMethods = { "createEventWithDiscretionaryPointsWith20M" }, alwaysRun = true)
    public void tryToCreateEventWithoutMeeting()
    {
        businessEvent.setHotel("ATLFX");
        businessEvent.setHotelCurrency(USD);
        businessEvent.setEventName("NewEvent005");

        eventSummaryTab = businessEventsPage.createEvent().getEventSummaryTab();
        eventSummaryTab.getEventInformation().populate(businessEvent);

        OffersFieldsEdit offers = eventSummaryTab.getOffers();
        offers.expand();
        offers.getIncludeOffers().click(verifyError("A date range for this event must be selected first"));
    }

    @Test(dependsOnMethods = { "tryToCreateEventWithoutMeeting" }, alwaysRun = true)
    public void createEventWithOfferAndMeeting()
    {
        meetingFields = eventSummaryTab.getEventDetails().getMeetingFields();
        meetingFields.populate(businessEvent.getMeetingDetails());
        meetingFields.getRevenueDetails().populateLocalAmount(businessEvent.getMeetingDetails());
        eventSummaryTab.getOffers().populate(businessEvent.getOffer());

        totalEligibleRevenue = eventSummaryTab.getTotalEligibleRevenue();
        totalEligibleRevenue.verifyDefault();

        popUp.clickCalculateEstimates();
        verifyThat(totalEligibleRevenue.getEstimatedCost(), hasText(not(NOT_AVAILABLE)));
        verifyThat(totalEligibleRevenue.getEstimatedPointEarning(), hasText(not(NOT_AVAILABLE)));
        popUp.clickSave(verifyNoError());

        businessEventsPage = new BusinessEventsPage();
        businessEventsPage.getGrid().getRow(1).verify(businessEvent);
    }

    @Test(dependsOnMethods = { "createEventWithOfferAndMeeting" }, alwaysRun = true)
    public void verifyHistoryAfterCreateEventWithOfferAndMeeting()
    {
        popUp = businessEventsPage.getGrid().getRow(1).openBRDetailsPopUp();

        EventHistoryTab historyTab = popUp.getHistoryTab();
        historyTab.goTo();

        EventHistoryGrid grid = historyTab.getGrid();
        EventHistoryGridRow historyRow = grid.getRow(1);
        historyRow.verify(CREATE, helper.getUser().getNameFull());

        EventHistoryRecords brRecords = historyRow.getAllRecords();
        brRecords.getRecord(2).verifyAdd(EVENT_ID, businessEvent.getEventId());
        brRecords.getRecord(3).verifyAdd(EVENT_NAME, businessEvent.getEventName());
        brRecords.getRecord(4).verifyAdd(EVENT_STATUS, hasText(containsIgnoreCase(businessEvent.getStatus())));
        brRecords.getRecord(5).verifyAdd(START_DATE, meetingDetails.getStartDate());
        brRecords.getRecord(6).verifyAdd(END_DATE, meetingDetails.getEndDate());
        brRecords.getRecord(7).verifyAdd(MEMBER_NUMBER, member.getRCProgramId());
        brRecords.getRecord(8).verifyAdd(HOTEL, businessEvent.getHotel());
        brRecords.getRecord(9).verifyAdd(HOTEL_CURRENCY, businessEvent.getHotelCurrency().getCode());

        brRecords.getRecord(11).verifyAdd(START_DATE, meetingDetails.getStartDate());
        brRecords.getRecord(12).verifyAdd(END_DATE, meetingDetails.getEndDate());
        brRecords.getRecord(13).verifyAdd(NUMBER_OF_MEETING_ROOMS,
                hasTextAsInt(meetingDetails.getNumberOfMeetingRooms()));
        brRecords.getRecord(14).verifyAdd(TOTAL_DELEGATES, hasTextAsInt(meetingDetails.getTotalDelegates()));

        brRecords.verifyAddBusinessRevenue(MEETING_ROOM, meetingDetails.getMeetingRoom());
        brRecords.verifyAddBusinessRevenue(FOOD_BEVERAGE, meetingDetails.getFoodBeverage());
        brRecords.verifyAddBusinessRevenue(MISCELLANEOUS, meetingDetails.getMiscellaneous());

        BusinessOffer offer = businessEvent.getOffer();
        brRecords.getRecord(EventAuditConstant.BusinessReward.Offer.OFFER_NAME).verifyAdd(offer.getOfferName());
        brRecords.getRecord(POINTS).verifyAdd(offer.getPointsToAward());
        brRecords.getRecord(ESTIMATED_CURRENCY).verifyAdd(USD.getCode());
        brRecords.getRecord(ESTIMATED_BILLING_AMOUNT).verifyAdd(hasAnyText());

        popUp.clickClose();
    }

    @Test(dependsOnMethods = { "verifyHistoryAfterCreateEventWithOfferAndMeeting" }, alwaysRun = true)
    public void createEventWithOfferPointMoreThan20M()
    {
        String WARNING_MESSAGE = "The event you are trying to save exceeds the maximum of 20,000,000 offer bonus points. Please adjust your event and try to save again.";
        businessEvent.setEventName("NewEvent006");
        businessEvent.getOffer().setPointsToAward("20000001");

        businessEventsPage = new BusinessEventsPage();
        popUp = businessEventsPage.createEvent();

        eventSummaryTab = popUp.getEventSummaryTab();
        eventSummaryTab.populateEventAndMeetingFields(businessEvent);
        eventSummaryTab.getOffers().populate(businessEvent.getOffer());

        popUp.clickCalculateEstimates(verifyError(WARNING_MESSAGE));
        verifyThat(totalEligibleRevenue.getEstimatedCost(), hasText(NOT_AVAILABLE));
        verifyThat(totalEligibleRevenue.getEstimatedPointEarning(), hasText(NOT_AVAILABLE));

        popUp.clickSave(verifyError(WARNING_MESSAGE));
    }

    @Test(dependsOnMethods = { "createEventWithOfferPointMoreThan20M" }, alwaysRun = true)
    public void createEventWithOfferPoint20M()
    {
        eventSummaryTab.getOffers().getPointsToAward().typeAndWait(20000000);

        popUp.clickCalculateEstimates(verifyNoError());
        verifyThat(totalEligibleRevenue.getEstimatedCost(), hasText(not(NOT_AVAILABLE)));
        verifyThat(totalEligibleRevenue.getEstimatedPointEarning(), hasText(not(NOT_AVAILABLE)));

        popUp.clickSave(verifyNoError());
        businessEventsPage = new BusinessEventsPage();
        businessEventsPage.getGrid().getRow(1).verify(businessEvent);
    }

    @Test(dependsOnMethods = { "createEventWithOfferPoint20M" }, alwaysRun = true)
    public void createEventWithBasePointsMoreThan500K()
    {
        String WARNING_MESSAGE = "IHG Business Rewards Event cannot be > 500000 Points";
        businessEvent.setEventName("NewEvent007");
        businessEvent.getMeetingDetails().getMeetingRoom().setAmount(200000.00);

        popUp = new BusinessEventsPage().createEvent();
        popUp.getEventSummaryTab().getEventInformation().populate(businessEvent);
        meetingFields.populate(businessEvent.getMeetingDetails());
        meetingFields.getRevenueDetails().populateLocalAmount(businessEvent.getMeetingDetails());

        popUp.clickCalculateEstimates(verifyError(WARNING_MESSAGE));
        popUp.clickSave(verifyError(WARNING_MESSAGE));
        popUp.clickCancel();

        new ConfirmDialog().clickYes(verifyNoError());
        verifyThat(popUp, displayed(false));
    }

    @Test(dependsOnMethods = { "createEventWithBasePointsMoreThan500K" }, alwaysRun = true)
    public void tryToCreateEventWithWrongEndDate()
    {
        businessEvent.setEventName("NewEvent008");
        businessEvent.getMeetingDetails().setStartDate(getFormattedDate(-2));
        businessEvent.getMeetingDetails().setStartDate(getFormattedDate(1));

        businessEventsPage = new BusinessEventsPage();
        popUp = businessEventsPage.createEvent();
        popUp.getEventSummaryTab().populateEventAndMeetingFields(businessEvent);

        popUp.clickCalculateEstimates(verifyError(HIGHLIGHTED_FIELDS_DID_NOT_PASS_VALIDATION));
        popUp.clickSave(verifyError(HIGHLIGHTED_FIELDS_DID_NOT_PASS_VALIDATION));
        popUp.clickCancel();

        new ConfirmDialog().clickYes(verifyNoError());
        verifyThat(popUp, displayed(false));
        verifyThat(businessEventsPage.getGrid().getRow(businessEvent), displayed(false));
    }

}
