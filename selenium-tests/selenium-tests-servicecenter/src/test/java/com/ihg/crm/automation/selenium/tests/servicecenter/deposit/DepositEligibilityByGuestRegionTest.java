package com.ihg.crm.automation.selenium.tests.servicecenter.deposit;

import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyError;
import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyNoError;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.event.EventTransactionType.Deposit.INCENTIVE;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.automation.selenium.servicecenter.pages.deposit.CreateDepositPopUp.CUSTOMER_IS_NOT_ELIGIBLE;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.components.Country;
import com.ihg.automation.selenium.common.deposit.Deposit;
import com.ihg.automation.selenium.common.event.AllEventsRowConverter;
import com.ihg.automation.selenium.common.member.GuestAddress;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.common.types.program.RewardClubLevel;
import com.ihg.automation.selenium.servicecenter.pages.LeftPanel;
import com.ihg.automation.selenium.servicecenter.pages.annual.AnnualActivityPage;
import com.ihg.automation.selenium.servicecenter.pages.deposit.CreateDepositPopUp;
import com.ihg.automation.selenium.servicecenter.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsPage;
import com.ihg.automation.selenium.servicecenter.pages.personal.PersonalInfoPage;
import com.ihg.crm.automation.selenium.tests.servicecenter.LoginLogout;

public class DepositEligibilityByGuestRegionTest extends LoginLogout
{
    private Member member = new Member();
    private Deposit testDeposit = new Deposit();
    private GuestAddress incorrectAddress = new GuestAddress();
    private GuestAddress correctAddress = new GuestAddress();

    private CreateDepositPopUp createDepositPopUp = new CreateDepositPopUp();

    @BeforeClass
    public void beforeClass()
    {
        incorrectAddress.setCountryCode(Country.FR);
        incorrectAddress.setPostalCode("75003");
        incorrectAddress.setLocality1("Paris");
        incorrectAddress.setLine1("22, rue du Grenier-Saint-Lazare");

        correctAddress = MemberPopulateHelper.getCanadaAddress();

        member.addProgram(Program.RC);
        member.setPersonalInfo(MemberPopulateHelper.getSimplePersonalInfo());
        member.addAddress(incorrectAddress);

        testDeposit.setTransactionId("CPCN10");
        testDeposit.setTransactionType(INCENTIVE);
        testDeposit.setTransactionName("HONEYMOON ON US");
        testDeposit.setTransactionDescription("HONEYMOON ON US-2011");
        testDeposit.setHotel("ATLCP");

        login();
        member = new AnnualActivityPage().captureCounters(new EnrollmentPage().enroll(member), jdbcTemplate);
        new LeftPanel().verifyRCLevel(RewardClubLevel.CLUB);
    }

    @Test
    public void createDepositWithInvalidData()
    {
        createDepositPopUp.goTo();
        createDepositPopUp.setDepositData(testDeposit);
        createDepositPopUp.clickCreateDeposit(verifyError(CUSTOMER_IS_NOT_ELIGIBLE));
        verifyThat(createDepositPopUp, displayed(true));
    }

    @Test(dependsOnMethods = { "createDepositWithInvalidData" }, alwaysRun = true)
    public void closeCreateDepositPopUp()
    {
        createDepositPopUp.close();
    }

    @Test(dependsOnMethods = { "closeCreateDepositPopUp" }, alwaysRun = true)
    public void verifyInitialTierLevel()
    {
        new LeftPanel().verifyRCLevel(RewardClubLevel.CLUB);
    }

    @Test(dependsOnMethods = { "verifyInitialTierLevel" }, alwaysRun = true)
    public void verifyInitialActivityCounters()
    {
        new AnnualActivityPage().verifyCounters(member);
    }

    @Test(dependsOnMethods = { "verifyInitialActivityCounters" }, alwaysRun = true)
    public void verifyIncentiveEventAbsence()
    {
        AllEventsPage allEventsPage = new AllEventsPage();
        allEventsPage.goTo();
        verifyThat(allEventsPage.getGrid().getRow(testDeposit.getTransactionType()), displayed(false));
    }

    @Test(dependsOnMethods = { "verifyIncentiveEventAbsence" }, alwaysRun = true)
    public void updateMemberAddress()
    {
        PersonalInfoPage personalInfoPage = new PersonalInfoPage();
        personalInfoPage.goTo();
        personalInfoPage.getAddressList().getContact().update(correctAddress, verifyNoError());
    }

    @Test(dependsOnMethods = { "updateMemberAddress" }, alwaysRun = true)
    public void createDeposit()
    {
        new CreateDepositPopUp().createDeposit(testDeposit);
    }

    @Test(dependsOnMethods = { "createDeposit" }, alwaysRun = true)
    public void verifyResultingIncentiveEvent()
    {
        AllEventsPage allEventsPage = new AllEventsPage();
        allEventsPage.goTo();
        allEventsPage.getGrid().getRow(testDeposit.getTransactionType())
                .verify(AllEventsRowConverter.convert(testDeposit));
    }

    @Test(dependsOnMethods = { "verifyResultingIncentiveEvent" }, alwaysRun = true)
    public void verifyResultingTierLevelAndActivityCounters()
    {
        new LeftPanel().verifyRCLevel(RewardClubLevel.CLUB);

        new AnnualActivityPage().verifyCounters(member);
    }

}
