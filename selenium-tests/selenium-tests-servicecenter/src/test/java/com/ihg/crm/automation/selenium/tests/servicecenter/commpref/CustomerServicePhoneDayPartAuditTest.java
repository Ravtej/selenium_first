package com.ihg.crm.automation.selenium.tests.servicecenter.commpref;

import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyNoError;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static com.ihg.automation.selenium.servicecenter.pages.communication.CustomerServicePhone.AFTERNOON;
import static com.ihg.automation.selenium.servicecenter.pages.communication.CustomerServicePhone.AFTERNOON_AUDIT;
import static com.ihg.automation.selenium.servicecenter.pages.communication.CustomerServicePhone.AFTERNOON_END_TIME;
import static com.ihg.automation.selenium.servicecenter.pages.communication.CustomerServicePhone.AFTERNOON_START_TIME;
import static com.ihg.automation.selenium.servicecenter.pages.communication.CustomerServicePhone.EVENING;
import static com.ihg.automation.selenium.servicecenter.pages.communication.CustomerServicePhone.EVENING_AUDIT;
import static com.ihg.automation.selenium.servicecenter.pages.communication.CustomerServicePhone.EVENING_END_TIME;
import static com.ihg.automation.selenium.servicecenter.pages.communication.CustomerServicePhone.EVENING_START_TIME;
import static com.ihg.automation.selenium.servicecenter.pages.communication.DaysMenu.ANY_AUDIT;
import static com.ihg.automation.selenium.servicecenter.pages.history.AuditConstant.CommunicationPreferences.COMMUNICATION_PREFERENCES;
import static com.ihg.automation.selenium.servicecenter.pages.history.AuditConstant.CommunicationPreferences.DAYS_OF_THE_WEEK;
import static com.ihg.automation.selenium.servicecenter.pages.history.AuditConstant.CommunicationPreferences.PHONE;
import static com.ihg.automation.selenium.servicecenter.pages.history.AuditConstant.CommunicationPreferences.PHONE_FOR_CUSTOMER_SERVICE;
import static com.ihg.automation.selenium.servicecenter.pages.history.AuditConstant.CommunicationPreferences.PREFERRED_TIME;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.servicecenter.pages.LeftPanel;
import com.ihg.automation.selenium.servicecenter.pages.communication.CommunicationPreferences;
import com.ihg.automation.selenium.servicecenter.pages.communication.CommunicationPreferencesPage;
import com.ihg.automation.selenium.servicecenter.pages.communication.CustomerServicePhone;
import com.ihg.automation.selenium.servicecenter.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.servicecenter.pages.history.ProfileHistoryGridRow;
import com.ihg.automation.selenium.servicecenter.pages.history.ProfileHistoryPage;
import com.ihg.crm.automation.selenium.tests.servicecenter.LoginLogout;

public class CustomerServicePhoneDayPartAuditTest extends LoginLogout
{
    private Member member = new Member();
    private CustomerServicePhone customerServicePhone;

    @BeforeClass
    public void beforeClass()
    {
        member.addProgram(Program.RC);
        member.setPersonalInfo(MemberPopulateHelper.getSimplePersonalInfo());
        member.addAddress(MemberPopulateHelper.getUnitedStatesAddress());
        member.addPhone(MemberPopulateHelper.getRandomPhone());

        login();
        new EnrollmentPage().enroll(member);
    }

    @Test
    public void addPhoneDayPart()
    {
        CommunicationPreferencesPage commPrefPage = new CommunicationPreferencesPage();
        commPrefPage.goTo();
        CommunicationPreferences commPrefs = commPrefPage.getCommunicationPreferences();
        customerServicePhone = commPrefs.getCustomerService().getPhone();
        commPrefs.clickEdit();

        customerServicePhone.getPhoneCheckBox().check();
        customerServicePhone.getDayPart().select(AFTERNOON);
        verifyThat(customerServicePhone.getStartTime(), hasText(AFTERNOON_START_TIME));
        verifyThat(customerServicePhone.getEndTime(), hasText(AFTERNOON_END_TIME));

        commPrefs.clickSave();

        verifyThat(customerServicePhone.getStartTime().getView(), hasText(AFTERNOON_START_TIME));
        verifyThat(customerServicePhone.getEndTime().getView(), hasText(AFTERNOON_END_TIME));
        verifyThat(customerServicePhone.getDayPart().getView(), hasText(AFTERNOON));
    }

    @Test(dependsOnMethods = { "addPhoneDayPart" }, alwaysRun = true)
    public void verifyHistoryAfterAddPhoneDayPart()
    {
        ProfileHistoryPage historyPage = new ProfileHistoryPage();
        historyPage.goTo();
        verifyNoError();

        ProfileHistoryGridRow phoneRow = historyPage.getProfileHistoryGrid().getRow(1)
                .getSubRowByName(COMMUNICATION_PREFERENCES).getSubRowByName(PHONE_FOR_CUSTOMER_SERVICE);

        phoneRow.getSubRowByName(PHONE).verifyAddAction(member.getPreferredPhone().getFullPhone());
        phoneRow.getSubRowByName(DAYS_OF_THE_WEEK).verifyAddAction(ANY_AUDIT);
        phoneRow.getSubRowByName(PREFERRED_TIME).verifyAddAction(AFTERNOON_AUDIT);
    }

    @Test(dependsOnMethods = { "verifyHistoryAfterAddPhoneDayPart" }, alwaysRun = true)
    public void updatePhoneDayPart()
    {
        new LeftPanel().reOpenMemberProfile(member);

        CommunicationPreferencesPage commPrefPage = new CommunicationPreferencesPage();
        commPrefPage.goTo();
        CommunicationPreferences commPrefs = commPrefPage.getCommunicationPreferences();
        customerServicePhone = commPrefs.getCustomerService().getPhone();
        commPrefs.clickEdit();

        customerServicePhone.getPhoneCheckBox().check();
        customerServicePhone.getDayPart().select(EVENING);
        verifyThat(customerServicePhone.getStartTime(), hasText(EVENING_START_TIME));
        verifyThat(customerServicePhone.getEndTime(), hasText(EVENING_END_TIME));

        commPrefs.clickSave();

        verifyThat(customerServicePhone.getStartTime().getView(), hasText(EVENING_START_TIME));
        verifyThat(customerServicePhone.getEndTime().getView(), hasText(EVENING_END_TIME));
        verifyThat(customerServicePhone.getDayPart().getView(), hasText(EVENING));
    }

    @Test(dependsOnMethods = { "updatePhoneDayPart" }, alwaysRun = true)
    public void verifyHistoryAfterUpdatePhoneDayPart()
    {
        ProfileHistoryPage historyPage = new ProfileHistoryPage();
        historyPage.goTo();
        verifyNoError();

        ProfileHistoryGridRow phoneRow = historyPage.getProfileHistoryGrid().getRow(1)
                .getSubRowByName(COMMUNICATION_PREFERENCES).getSubRowByName(PHONE_FOR_CUSTOMER_SERVICE);

        phoneRow.getSubRowByName(PREFERRED_TIME).verify(AFTERNOON_AUDIT, EVENING_AUDIT);
    }
}
