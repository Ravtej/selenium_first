package com.ihg.crm.automation.selenium.tests.servicecenter.deposit;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.event.EventTransactionType.Deposit.INCENTIVE;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.enabled;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.isHighlightedAsInvalid;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.common.RandomUtils;
import com.ihg.automation.selenium.common.Constant;
import com.ihg.automation.selenium.common.billing.EventBillingDetailsTab;
import com.ihg.automation.selenium.common.deposit.Deposit;
import com.ihg.automation.selenium.common.earning.EventEarningDetailsTab;
import com.ihg.automation.selenium.common.event.AllEventsRowConverter;
import com.ihg.automation.selenium.common.event.EarningEvent;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberAlliance;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.types.Alliance;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.common.types.program.RewardClubLevel;
import com.ihg.automation.selenium.gwt.components.input.Input;
import com.ihg.automation.selenium.servicecenter.pages.LeftPanel;
import com.ihg.automation.selenium.servicecenter.pages.annual.AnnualActivityPage;
import com.ihg.automation.selenium.servicecenter.pages.deposit.CreateDepositPopUp;
import com.ihg.automation.selenium.servicecenter.pages.deposit.DepositDetailsPopUp;
import com.ihg.automation.selenium.servicecenter.pages.deposit.DepositDetailsTab;
import com.ihg.automation.selenium.servicecenter.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsGridRow;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsPage;
import com.ihg.automation.selenium.servicecenter.pages.event.deposit.DepositGridRowContainer;
import com.ihg.crm.automation.selenium.tests.servicecenter.LoginLogout;

public class DepositMilesTest extends LoginLogout
{
    private DepositDetailsPopUp depositDetailsPopUp = new DepositDetailsPopUp();
    private DepositGridRowContainer depositGridRowContainer;

    private Member member = new Member();
    private Deposit testDeposit = new Deposit();
    private static final String ALLIANCE_NUMBER = RandomUtils.getRandomNumber(10);

    @BeforeClass
    public void beforeClass()
    {
        member.setPersonalInfo(MemberPopulateHelper.getSimplePersonalInfo());
        member.addAddress(MemberPopulateHelper.getUnitedStatesAddress());
        MemberAlliance alliance = new MemberAlliance(Alliance.DJA, ALLIANCE_NUMBER, member.getName());
        member.setMilesEarningPreference(alliance);
        member.addProgram(Program.RC);

        testDeposit.setTransactionId("AIRTS2");
        testDeposit.setTransactionType(INCENTIVE);
        testDeposit.setTransactionName("AIRLINE COPRT TEST W/ MNEMONIC");
        testDeposit.setTransactionDescription("AIRLINE COPRT TEST W/ MNEMONIC");
        testDeposit.setAllianceLoyaltyUnitsAmount("200");
        testDeposit.setLoyaltyUnitsType(Alliance.DJA.getCode());

        login();
        member = new AnnualActivityPage().captureCounters(new EnrollmentPage().enroll(member), jdbcTemplate);

        new LeftPanel().getProgramInfoPanel().getPrograms().getRewardClubProgram().verify(RewardClubLevel.CLUB,
                Alliance.DJA, "0");
    }

    @Test
    public void verifyCreateDepositDialog()
    {
        CreateDepositPopUp createDepositPopUp = new CreateDepositPopUp();
        createDepositPopUp.goTo();

        createDepositPopUp.getTransactionId().typeAndWait(testDeposit.getTransactionId());
        createDepositPopUp.verifyTransactionInfo(testDeposit);
        createDepositPopUp.getLoyaltyUnitsSelect().select(testDeposit.getLoyaltyUnitsType());

        Input loyaltyUnitsInput = createDepositPopUp.getLoyaltyUnitsInput();
        verifyThat(loyaltyUnitsInput, enabled(false));
        verifyThat(loyaltyUnitsInput, hasText(testDeposit.getAllianceLoyaltyUnitsAmount()));

        createDepositPopUp.clickCreateDeposit();
        verifyThat(createDepositPopUp.getHotel(), isHighlightedAsInvalid(true));

        createDepositPopUp.clickClose();
    }

    @Test(dependsOnMethods = { "verifyCreateDepositDialog" }, alwaysRun = true)
    public void createDeposit()
    {
        testDeposit.setHotel("ATLCP");
        new CreateDepositPopUp().createDeposit(testDeposit);
    }

    @Test(dependsOnMethods = { "createDeposit" })
    public void verifyIncentiveEventWithDetailsInGrid()
    {
        AllEventsPage allEventsPage = new AllEventsPage();
        allEventsPage.goTo();
        AllEventsGridRow row = allEventsPage.getGrid().getRow(testDeposit.getTransactionType());
        row.verify(AllEventsRowConverter.convert(testDeposit));
        depositGridRowContainer = row.expand(DepositGridRowContainer.class);
        depositGridRowContainer.verify(testDeposit, helper);
    }

    @Test(dependsOnMethods = { "verifyIncentiveEventWithDetailsInGrid" }, alwaysRun = true)
    public void openAllEventsIncentiveEventDetailsPopUp()
    {
        depositGridRowContainer.clickDetails();
    }

    @Test(dependsOnMethods = { "openAllEventsIncentiveEventDetailsPopUp" })
    public void verifyIncentiveEventDepositDetailsTab()
    {
        DepositDetailsTab depositDetailsTab = depositDetailsPopUp.getDepositDetailsTab();
        depositDetailsTab.goTo();
        depositDetailsTab.getDepositDetails().verify(testDeposit);
    }

    @Test(dependsOnMethods = { "openAllEventsIncentiveEventDetailsPopUp" })
    public void verifyIncentiveEventEarningDetailsTab()
    {
        EventEarningDetailsTab tab = depositDetailsPopUp.getEarningDetailsTab();
        tab.goTo();
        tab.verifyBasicDetailsForMiles(Alliance.DJA, "200");
        tab.getGrid().verify(new EarningEvent(Constant.BASE_UNITS, "200", "0", Alliance.DJA.getValue(), "Pending"));
    }

    @Test(dependsOnMethods = { "openAllEventsIncentiveEventDetailsPopUp" })
    public void verifyIncentiveEventBillingDetailsTab()
    {
        EventBillingDetailsTab tab = depositDetailsPopUp.getBillingDetailsTab();
        tab.goTo();
        tab.verifyTabIsEmpty(INCENTIVE);
    }

    @Test(dependsOnMethods = { "verifyIncentiveEventBillingDetailsTab", "verifyIncentiveEventEarningDetailsTab",
            "verifyIncentiveEventDepositDetailsTab" }, alwaysRun = true)
    public void closeAllEventsCoPartnersDepositDetailsPopUp()
    {
        depositDetailsPopUp.close();
    }

    @Test(dependsOnMethods = { "closeAllEventsCoPartnersDepositDetailsPopUp" })
    public void verifyResultingLeftPanelProgramInformation()
    {
        new LeftPanel().getProgramInfoPanel().getPrograms().getRewardClubProgram().verify(RewardClubLevel.CLUB,
                Alliance.DJA, "0");
    }

    @Test(dependsOnMethods = { "closeAllEventsCoPartnersDepositDetailsPopUp" })
    public void verifyResultingActivities()
    {
        new AnnualActivityPage().verifyCounters(member);
    }

}
