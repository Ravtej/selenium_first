package com.ihg.crm.automation.selenium.tests.servicecenter.programinfo;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.types.program.Program.AMB;
import static com.ihg.automation.selenium.common.types.program.Program.BR;
import static com.ihg.automation.selenium.common.types.program.Program.EMP;
import static com.ihg.automation.selenium.common.types.program.Program.IHG;
import static com.ihg.automation.selenium.common.types.program.Program.KAR;
import static com.ihg.automation.selenium.common.types.program.Program.RC;
import static com.ihg.automation.selenium.common.types.program.ProgramStatusReason.FROZEN;
import static com.ihg.automation.selenium.matchers.component.ViewableMatcher.hasTextInView;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.member.AmbassadorOfferCode;
import com.ihg.automation.selenium.common.member.AmbassadorUtils;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.types.program.ProgramStatus;
import com.ihg.automation.selenium.servicecenter.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.servicecenter.pages.programs.EmployeePage;
import com.ihg.automation.selenium.servicecenter.pages.programs.GuestAccountPage;
import com.ihg.automation.selenium.servicecenter.pages.programs.KarmaPage;
import com.ihg.automation.selenium.servicecenter.pages.programs.ProgramPageBase;
import com.ihg.automation.selenium.servicecenter.pages.programs.ProgramSummary;
import com.ihg.automation.selenium.servicecenter.pages.programs.amb.AmbassadorPage;
import com.ihg.automation.selenium.servicecenter.pages.programs.br.BusinessRewardsPage;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.RewardClubPage;
import com.ihg.crm.automation.selenium.tests.servicecenter.LoginLogout;

public class CloseAllProgramsTest extends LoginLogout
{
    private Member member = new Member();
    private EnrollmentPage enrollmentPage = new EnrollmentPage();
    private EmployeePage employeePage = new EmployeePage();
    private AmbassadorPage ambassadorPage = new AmbassadorPage();
    private BusinessRewardsPage businessRewardsPage = new BusinessRewardsPage();
    private GuestAccountPage guestAccountPage = new GuestAccountPage();

    @BeforeClass
    public void beforeClass()
    {
        member.addProgram(RC, IHG, EMP, BR);
        member.setPersonalInfo(MemberPopulateHelper.getSimplePersonalInfo());
        member.addAddress(MemberPopulateHelper.getUnitedStatesAddress());

        login();
        enrollmentPage.enroll(member);

        member.setAmbassadorOfferCode(AmbassadorOfferCode.CHSRO);
        enrollmentPage.enrollToDROrAMBProgramWithoutProfileChange(member, AMB,
                AmbassadorUtils.AMBASSADOR_COMP_PROMO_$0);
    }

    @Test(priority = 5, dataProvider = "statusProvider")
    public void closeProgramsAndVerifyRcStatus(ProgramPageBase page)
    {
        page.goTo();

        closeMinorProgramAndVerifyRc(page);
    }

    @Test(priority = 10, groups = { "karma" })
    public void closeKarmaAndVerifyRcStatus()
    {
        enrollmentPage.enrollToAnotherProgram(member, KAR);

        KarmaPage karmaPage = new KarmaPage();
        karmaPage.goTo();
        closeMinorProgramAndVerifyRc(karmaPage);
    }

    private void closeMinorProgramAndVerifyRc(ProgramPageBase page)
    {
        ProgramSummary summary = page.getSummary();
        summary.setClosedStatus(FROZEN);

        verifyThat(summary.getStatus(), hasTextInView(ProgramStatus.CLOSED));

        RewardClubPage rewardClubPage = new RewardClubPage();
        rewardClubPage.goTo();
        verifyThat(rewardClubPage.getSummary().getStatus(), hasTextInView(ProgramStatus.OPEN));
    }

    @DataProvider(name = "statusProvider")
    protected Object[][] statusProvider()
    {
        return new Object[][] { { employeePage }, { guestAccountPage }, { ambassadorPage }, { businessRewardsPage } };
    }
}
