package com.ihg.crm.automation.selenium.tests.servicecenter.unit;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.event.EventTransactionType.Loyalty.Q_NIGHT_TRANSFER;
import static com.ihg.automation.selenium.common.types.program.ProgramStatus.OPEN;
import static com.ihg.automation.selenium.common.types.program.RewardClubLevel.GOLD;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasTextAsInt;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasTextWithWait;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.isDisplayedWithWait;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static com.ihg.automation.selenium.servicecenter.pages.annual.AnnualActivityRow.AnnualActivityCell.QUALIFIED_ROOM;
import static com.ihg.automation.selenium.servicecenter.pages.annual.AnnualActivityRow.AnnualActivityCell.TIER_LEVEL_NIGHT;
import static org.hamcrest.number.OrderingComparison.greaterThan;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.common.DateUtils;
import com.ihg.automation.selenium.common.billing.EventBillingDetailsTab;
import com.ihg.automation.selenium.common.event.AllEventsRow;
import com.ihg.automation.selenium.common.event.AllEventsRowFactory;
import com.ihg.automation.selenium.common.member.AnnualActivity;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.pages.program.RewardClubTierLevelActivityGrid;
import com.ihg.automation.selenium.common.stay.Stay;
import com.ihg.automation.selenium.common.types.Currency;
import com.ihg.automation.selenium.common.types.program.RewardClubLevel;
import com.ihg.automation.selenium.gwt.components.dialog.ConfirmDialog;
import com.ihg.automation.selenium.servicecenter.pages.LeftPanel;
import com.ihg.automation.selenium.servicecenter.pages.annual.AnnualActivityPage;
import com.ihg.automation.selenium.servicecenter.pages.annual.AnnualActivityRow;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsGridRow;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsPage;
import com.ihg.automation.selenium.servicecenter.pages.event.stay.StayEventsPage;
import com.ihg.automation.selenium.servicecenter.pages.event.unit.UnitDetails;
import com.ihg.automation.selenium.servicecenter.pages.event.unit.UnitDetailsPopUp;
import com.ihg.automation.selenium.servicecenter.pages.event.unit.UnitGridRowContainer;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.RewardClubPage;
import com.ihg.automation.selenium.servicecenter.pages.unit.GoodwillPopUp;
import com.ihg.automation.selenium.servicecenter.pages.unit.TransferNightsPopUp;
import com.ihg.automation.selenium.tests.common.RulesDBUtils;

public class TransferQualifyingNightsTest extends UnitsCommon
{
    private static int nightsToAchievePlatinum;
    private static final String CURRENT_YEAR = Integer.toString((DateUtils.currentYear()));

    private static int TRANSFER_NIGHTS_AMOUNT_INT;
    private static String TRANSFER_NIGHTS_AMOUNT;
    private static String ORIGINATOR_BALANCE;

    private TransferNightsPopUp nightsPopUp = new TransferNightsPopUp();
    private UnitDetailsPopUp unitPopUp = new UnitDetailsPopUp();

    private Member originator, recipient;
    private Stay missingStay = new Stay();

    @BeforeClass
    public void beforeClass()
    {
        nightsToAchievePlatinum = new RulesDBUtils(jdbcTemplate).getCurrentYearRuleNights(GOLD, RewardClubLevel.PLTN);

        missingStay.setHotelCode("ATLCP");
        missingStay.setAvgRoomRate(50.00);
        missingStay.setRateCode("TEST");
        missingStay.setHotelCurrency(Currency.USD);
        missingStay.setCheckInOutByNights(achieveGoldNights);

        // Transfer all qualified available nights
        TRANSFER_NIGHTS_AMOUNT_INT = missingStay.getNights();
        TRANSFER_NIGHTS_AMOUNT = missingStay.getNightsAsString();

        login();

        recipient = new AnnualActivityPage().captureCounters(enrollMember(), jdbcTemplate);

        new LeftPanel().goToNewSearch();

        originator = enrollMember();

        /** Qualified Units and Nights **/
        new StayEventsPage().createStay(missingStay);
        /** Total Units **/
        new GoodwillPopUp().goodwillPoints("1000");

        AnnualActivityPage annualActivityPage = new AnnualActivityPage();
        annualActivityPage.captureCounters(originator, jdbcTemplate);

        // Do basic amount and level verification
        verifyThat(annualActivityPage.getCurrentBalance().getAmount(), hasTextAsInt(greaterThan(0)));

        AnnualActivityRow activityRow = annualActivityPage.getAnnualActivities().getCurrentYearRow();
        verifyThat(activityRow.getCell(TIER_LEVEL_NIGHT), hasText(TRANSFER_NIGHTS_AMOUNT));
        verifyThat(activityRow.getCell(QUALIFIED_ROOM), hasText(TRANSFER_NIGHTS_AMOUNT));

        RewardClubPage rewardClubPage = new RewardClubPage();
        rewardClubPage.goTo();

        rewardClubPage.getSummary().verify(OPEN, GOLD); // GOLD by nights

        ORIGINATOR_BALANCE = String.valueOf(originator.getAnnualActivity().getTotalQualifiedUnits());
        RewardClubTierLevelActivityGrid currentYearAnnualActivities = rewardClubPage.getCurrentYearAnnualActivities();
        currentYearAnnualActivities.getPointsRow().verifyCurrent(ORIGINATOR_BALANCE);
        currentYearAnnualActivities.getNightsRow().verifyCurrent(TRANSFER_NIGHTS_AMOUNT);
    }

    @Test
    public void openTransferQualifyingNightsPopUp()
    {
        nightsPopUp.goTo();
    }

    @Test(dependsOnMethods = { "openTransferQualifyingNightsPopUp" })
    public void verifyTransferQualifyingNightsPopUp()
    {
        nightsPopUp.verify(CURRENT_YEAR, TRANSFER_NIGHTS_AMOUNT);
        nightsPopUp.getMembershipId().typeAndWait(recipient.getRCProgramId());
        verifyThat(nightsPopUp.getMemberName(), hasTextWithWait(recipient.getName().getNameOnPanel()));
    }

    @Test(dependsOnMethods = { "verifyTransferQualifyingNightsPopUp" }, alwaysRun = true)
    public void transferQualifyingNights()
    {
        nightsPopUp.getMembershipId().typeAndWait(recipient.getRCProgramId());
        nightsPopUp.getYear().select(CURRENT_YEAR);
        nightsPopUp.getQuantity().typeAndWait(TRANSFER_NIGHTS_AMOUNT);
        nightsPopUp.clickSubmitNoError();

        ConfirmDialog dialog = new ConfirmDialog();
        verifyThat(dialog, isDisplayedWithWait());
        verifyThat(dialog, hasText("Would you also like to transfer Units Balance?"));
        dialog.clickNo();
        verifyThat(dialog, displayed(false));
    }

    @Test(dependsOnMethods = { "transferQualifyingNights" })
    public void verifyOriginatorTransferNightsEvent()
    {
        AllEventsRow transferNight = AllEventsRowFactory.getNightTransfer(-TRANSFER_NIGHTS_AMOUNT_INT);

        AllEventsPage allEvents = new AllEventsPage();
        allEvents.goTo();

        AllEventsGridRow row = allEvents.getGrid().getRow(Q_NIGHT_TRANSFER);

        row.verify(transferNight);

        UnitDetails unitDetails = row.expand(UnitGridRowContainer.class).getUnitDetails();
        unitDetails.verifyRelatedMemberID(recipient);
        verifyThat(unitDetails.getPaymentDetails().getMethod(), displayed(false));
        unitDetails.verifySource(helper);
    }

    @Test(dependsOnMethods = { "verifyOriginatorTransferNightsEvent" }, alwaysRun = true)
    public void navigateOriginatorEventDetailsPopUp()
    {
        openDetailsPopUp(Q_NIGHT_TRANSFER);
    }

    @Test(dependsOnMethods = { "navigateOriginatorEventDetailsPopUp" })
    public void verifyOriginatorTransferNightsEventEarningDetails()
    {
        unitPopUp.getEarningDetailsTab().verifyForEmptyBaseUnitsEvent();
    }

    @Test(dependsOnMethods = { "navigateOriginatorEventDetailsPopUp" })
    public void verifyOriginatorTransferNightsEventBillingDetails()
    {
        EventBillingDetailsTab tab = unitPopUp.getBillingDetailsTab();
        tab.goTo();
        tab.verifyTabIsEmpty(Q_NIGHT_TRANSFER);
    }

    @Test(dependsOnMethods = { "verifyOriginatorTransferNightsEventBillingDetails" }, alwaysRun = true)
    public void closeOriginatorUnitDetailsPopUp()
    {
        unitPopUp.close();
    }

    @Test(dependsOnMethods = { "closeOriginatorUnitDetailsPopUp" }, alwaysRun = true)
    public void verifyOriginatorAnnualActivityPage()
    {
        AnnualActivity annualActivity = originator.getAnnualActivity();
        annualActivity.setTierLevelNights(0);
        annualActivity.setQualifiedNights(0);

        new AnnualActivityPage().verifyCounters(originator);
    }

    @Test(dependsOnMethods = { "verifyOriginatorAnnualActivityPage" }, alwaysRun = true)
    public void verifyOriginatorRewardClubPage()
    {
        RewardClubPage rewardClubPage = new RewardClubPage();
        rewardClubPage.goTo();
        rewardClubPage.getSummary().verify(OPEN, GOLD);

        RewardClubTierLevelActivityGrid currentYearAnnualActivities = rewardClubPage.getCurrentYearAnnualActivities();
        currentYearAnnualActivities.getPointsRow().verifyCurrent(ORIGINATOR_BALANCE);
        currentYearAnnualActivities.getNightsRow().verifyCurrent("0");
    }

    @Test(dependsOnMethods = { "verifyOriginatorRewardClubPage" }, alwaysRun = true)
    public void navigateRecipientProfile()
    {
        openRecipientProfileByLink(recipient, 1);
    }

    @Test(dependsOnMethods = { "navigateRecipientProfile" })
    public void verifyRecipientAnnualActivityPage()
    {
        AnnualActivity annualActivity = recipient.getAnnualActivity();
        annualActivity.setTierLevelNightsFromString(TRANSFER_NIGHTS_AMOUNT);
        annualActivity.setQualifiedNightsFromString(TRANSFER_NIGHTS_AMOUNT);

        new AnnualActivityPage().verifyCounters(recipient);
    }

    @Test(dependsOnMethods = { "verifyRecipientAnnualActivityPage" }, alwaysRun = true)
    public void verifyRecipientRewardClubPage()
    {
        RewardClubPage rewardClubPage = new RewardClubPage();
        rewardClubPage.goTo();
        rewardClubPage.getSummary().verify(OPEN, GOLD);

        RewardClubTierLevelActivityGrid currentYearAnnualActivities = rewardClubPage.getCurrentYearAnnualActivities();
        currentYearAnnualActivities.getPointsRow().verifyCurrent("0");
        currentYearAnnualActivities.getNightsRow().verify(TRANSFER_NIGHTS_AMOUNT_INT, nightsToAchievePlatinum);
    }

    @Test(dependsOnMethods = { "verifyRecipientAnnualActivityPage" }, alwaysRun = true)
    public void verifyRecipientTransferNightsEvent()
    {
        AllEventsRow transferNight = AllEventsRowFactory.getNightTransfer(TRANSFER_NIGHTS_AMOUNT_INT);

        AllEventsPage allEvents = new AllEventsPage();
        allEvents.goTo();

        AllEventsGridRow row = allEvents.getGrid().getRow(Q_NIGHT_TRANSFER);

        row.verify(transferNight);

        UnitDetails unitDetails = row.expand(UnitGridRowContainer.class).getUnitDetails();
        unitDetails.verifyRelatedMemberID(originator);
        verifyThat(unitDetails.getPaymentDetails().getMethod(), displayed(false));
        unitDetails.verifySource(helper);
    }

    @Test(dependsOnMethods = { "verifyRecipientTransferNightsEvent" }, alwaysRun = true)
    public void navigateRecipientEventDetailsPopUp()
    {
        openDetailsPopUp(Q_NIGHT_TRANSFER);
        unitPopUp = new UnitDetailsPopUp();
    }

    @Test(dependsOnMethods = { "navigateRecipientEventDetailsPopUp" })
    public void verifyRecipientTransferNightsEventEarningDetails()
    {
        unitPopUp.getEarningDetailsTab().verifyForEmptyBaseUnitsEvent();
    }

    @Test(dependsOnMethods = { "navigateRecipientEventDetailsPopUp" })
    public void verifyRecipientTransferNightsEventBillingDetails()
    {
        EventBillingDetailsTab tab = unitPopUp.getBillingDetailsTab();
        tab.goTo();
        tab.verifyTabIsEmpty(Q_NIGHT_TRANSFER);
    }

}
