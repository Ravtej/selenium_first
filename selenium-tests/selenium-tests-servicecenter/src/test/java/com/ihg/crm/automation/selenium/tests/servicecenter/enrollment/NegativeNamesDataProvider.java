package com.ihg.crm.automation.selenium.tests.servicecenter.enrollment;

import org.testng.annotations.DataProvider;

public class NegativeNamesDataProvider
{
    @DataProvider(name = "negativeNames")
    public static Object[][] negativeNames()
    {
        return new Object[][] { { "4" }, { "T8est" }, { "9Test" }, { "Test5" } };
    }

}
