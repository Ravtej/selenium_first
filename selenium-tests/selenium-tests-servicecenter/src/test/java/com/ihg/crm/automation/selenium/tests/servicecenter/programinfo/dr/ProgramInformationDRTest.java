package com.ihg.crm.automation.selenium.tests.servicecenter.programinfo.dr;

import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifySuccess;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.event.EventTransactionType.Membership.MEMBERSHIP_CLOSE;
import static com.ihg.automation.selenium.common.event.EventTransactionType.Membership.MEMBERSHIP_OPEN;
import static com.ihg.automation.selenium.common.member.MemberJdbcUtils.UPDATE_EXP;
import static com.ihg.automation.selenium.common.types.program.Program.DR;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.isHighlightedAsInvalid;
import static com.ihg.automation.selenium.matchers.component.HasSelectItems.hasSelectItems;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static com.ihg.automation.selenium.matchers.component.ViewableMatcher.hasTextInView;
import static com.ihg.automation.selenium.servicecenter.pages.programs.ProgramPageBase.SUCCESS_MESSAGE;

import java.util.ArrayList;
import java.util.Arrays;

import org.springframework.beans.factory.annotation.Value;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.common.DateUtils;
import com.ihg.automation.selenium.common.CustomerInfoPanel;
import com.ihg.automation.selenium.common.ProgramsGrid;
import com.ihg.automation.selenium.common.event.AllEventsRow;
import com.ihg.automation.selenium.common.member.MemberJdbcUtils;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.common.types.program.ProgramStatus;
import com.ihg.automation.selenium.common.types.program.ProgramStatusReason;
import com.ihg.automation.selenium.servicecenter.pages.GuestSearch;
import com.ihg.automation.selenium.servicecenter.pages.LeftPanel;
import com.ihg.automation.selenium.servicecenter.pages.Role;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsGrid;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsGridRow;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsPage;
import com.ihg.automation.selenium.servicecenter.pages.event.enroll.MembershipGridRowContainer;
import com.ihg.automation.selenium.servicecenter.pages.programs.RenewExtendProgramSummary;
import com.ihg.automation.selenium.servicecenter.pages.programs.dr.DiningRewardsPage;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.RewardClubPage;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.RewardClubSummary;

public class ProgramInformationDRTest extends DRCommon
{
    @Value("${memberProgramDR}")
    protected String memberProgramDR;

    private DiningRewardsPage diningRewardsPage = new DiningRewardsPage();
    private RenewExtendProgramSummary diningSummary;
    private AllEventsGridRow drRow;
    private AllEventsPage allEvents;
    private AllEventsGrid grid;
    private RewardClubPage rewardClubPage = new RewardClubPage();
    private AllEventsRow rowData = new AllEventsRow(DateUtils.getFormattedDate(), MEMBERSHIP_CLOSE,
            Program.DR.getCode(), "", "");
    private ArrayList<String> expectedStatusReasonList = new ArrayList<String>(Arrays.asList(
            ProgramStatusReason.CUSTOMER_REQUEST, ProgramStatusReason.FROZEN, ProgramStatusReason.MUTUALLY_EXCLUSIVE,
            ProgramStatusReason.DUPLICATE, ProgramStatusReason.IHG_REQUEST));

    @BeforeClass
    public void beforeClass()
    {
        MemberJdbcUtils.update(jdbcTemplateUpdate, UPDATE_EXP, memberProgramDR, DR, 13);

        login(user15U, Role.AGENT_TIER2);
        new GuestSearch().byMemberNumber(memberProgramDR);
    }

    @Test
    public void validateLeftPanelProgramInfoNavigation()
    {
        verifyThat(diningRewardsPage, displayed(false));
        ProgramsGrid grid = new LeftPanel().getProgramInfoPanel().getPrograms();
        grid.getProgram(Program.DR).getCell(1).clickAndWait();
        verifyThat(diningRewardsPage, displayed(true));
    }

    @Test(dependsOnMethods = { "validateLeftPanelProgramInfoNavigation" }, alwaysRun = true)
    public void closeDRProgram()
    {
        diningRewardsPage.goTo();
        diningSummary = diningRewardsPage.getSummary();
        diningSummary.clickEdit();
        diningSummary.getStatus().select(ProgramStatus.CLOSED);
        verifyThat(diningSummary.getStatusReason(), hasSelectItems(expectedStatusReasonList));
        diningSummary.getStatusReason().select(ProgramStatusReason.FROZEN);
        diningSummary.clickSave(verifySuccess(SUCCESS_MESSAGE));

        verifyThat(diningSummary.getStatus(), hasTextInView(ProgramStatus.CLOSED));
    }

    @Test(dependsOnMethods = { "closeDRProgram" }, alwaysRun = true)
    public void allEventRowValidate()
    {
        allEvents = new AllEventsPage();
        allEvents.goTo();
        grid = allEvents.getGrid();

        drRow = grid.getRow(MEMBERSHIP_CLOSE, Program.DR.getCode());

        drRow.verify(rowData);
    }

    @Test(dependsOnMethods = { "allEventRowValidate" }, alwaysRun = true)
    public void allEventGridContainerValidate()
    {
        MembershipGridRowContainer membershipContainer = drRow.expand(MembershipGridRowContainer.class);

        verifyThat(membershipContainer.getReason(), hasText(ProgramStatusReason.FROZEN));
        membershipContainer.getMembershipDetails().getSource()
                .verify(helper.getSourceFactory().getNoEmployeeIdSource());
    }

    @Test(dependsOnMethods = { "allEventGridContainerValidate" }, alwaysRun = true)
    public void closeRCProgram()
    {
        rewardClubPage.goTo();
        RewardClubSummary rewardClubSummery = rewardClubPage.getSummary();
        verifyThat(rewardClubSummery.getStatus(), hasTextInView(ProgramStatus.OPEN));
        rewardClubSummery.setClosedStatus(ProgramStatusReason.FROZEN);

        verifyThat(rewardClubSummery.getStatus(), hasTextInView(ProgramStatus.CLOSED));
    }

    @Test(dependsOnMethods = { "closeRCProgram" }, alwaysRun = true)
    public void verifyCustomerInformationPanel()
    {
        CustomerInfoPanel custPanel = new CustomerInfoPanel();
        custPanel.verifyStatus(ProgramStatus.CLOSED);
    }

    @Test(dependsOnMethods = { "verifyCustomerInformationPanel" }, alwaysRun = true)
    public void tryToOpenDRProgram()
    {
        diningRewardsPage.goTo();
        diningSummary = diningRewardsPage.getSummary();

        diningSummary.setOpenStatus();
        verifyThat(diningSummary.getStatus(), isHighlightedAsInvalid(true));
        diningSummary.clickCancel();
    }

    @Test(dependsOnMethods = { "tryToOpenDRProgram" }, alwaysRun = true)
    public void openRCProgram()
    {
        rewardClubPage.goTo();
        RewardClubSummary rewardClubSummery = rewardClubPage.getSummary();
        rewardClubSummery.setOpenStatus();
        verifyThat(rewardClubSummery.getStatus(), hasTextInView(ProgramStatus.OPEN));
    }

    @Test(dependsOnMethods = { "openRCProgram" }, alwaysRun = true)
    public void openDRProgram()
    {
        diningRewardsPage.goTo();
        diningSummary = diningRewardsPage.getSummary();

        diningSummary.setOpenStatus();
        verifyThat(diningSummary.getStatus(), hasTextInView(ProgramStatus.OPEN));
    }

    @Test(dependsOnMethods = { "openDRProgram" }, alwaysRun = true)
    public void verifyMembershipOpenEventsCreated()
    {
        allEvents = new AllEventsPage();
        allEvents.goTo();
        grid = allEvents.getGrid();
        verifyThat(grid.getRow(MEMBERSHIP_OPEN, Program.RC.getCode()), displayed(true));
        verifyThat(grid.getRow(MEMBERSHIP_OPEN, Program.DR.getCode()), displayed(true));
    }
}
