package com.ihg.crm.automation.selenium.tests.servicecenter.smoke;

import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifySuccess;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyNoErrors;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.communication.CommunicationPreferencesConstants.OPT_OUT;
import static com.ihg.automation.selenium.common.types.WrittenLanguage.ENGLISH;
import static com.ihg.automation.selenium.common.types.program.Program.AMB;
import static com.ihg.automation.selenium.common.types.program.Program.BR;
import static com.ihg.automation.selenium.common.types.program.Program.EMP;
import static com.ihg.automation.selenium.common.types.program.Program.IHG;
import static com.ihg.automation.selenium.common.types.program.Program.RC;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasTextAsInt;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static com.ihg.automation.selenium.matchers.component.IsEnabled.enabled;
import static com.ihg.automation.selenium.matchers.component.ViewableMatcher.hasTextInView;
import static com.ihg.automation.selenium.servicecenter.pages.Messages.CONTACT_SUCCESS_SAVE;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.lessThan;

import org.joda.time.LocalDate;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.ihg.automation.common.DateUtils;
import com.ihg.automation.common.RandomUtils;
import com.ihg.automation.selenium.common.catalog.CatalogItem;
import com.ihg.automation.selenium.common.communication.ContactPermissionItem;
import com.ihg.automation.selenium.common.communication.ContactPermissionItems;
import com.ihg.automation.selenium.common.communication.MarketingPreferences;
import com.ihg.automation.selenium.common.member.AmbassadorOfferCode;
import com.ihg.automation.selenium.common.member.AmbassadorUtils;
import com.ihg.automation.selenium.common.member.GuestPhone;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.order.OrderFactory;
import com.ihg.automation.selenium.common.pages.TabCustomContainerBase;
import com.ihg.automation.selenium.common.personal.PhoneContactList;
import com.ihg.automation.selenium.common.personal.SmsContactList;
import com.ihg.automation.selenium.common.stay.Revenues;
import com.ihg.automation.selenium.common.stay.Stay;
import com.ihg.automation.selenium.common.types.program.RewardClubLevel;
import com.ihg.automation.selenium.formatter.Converter;
import com.ihg.automation.selenium.gwt.components.Mode;
import com.ihg.automation.selenium.servicecenter.pages.LeftPanel;
import com.ihg.automation.selenium.servicecenter.pages.account.AccountInfoPage;
import com.ihg.automation.selenium.servicecenter.pages.account.Security;
import com.ihg.automation.selenium.servicecenter.pages.annual.AnnualActivityPage;
import com.ihg.automation.selenium.servicecenter.pages.annual.AnnualActivityRow;
import com.ihg.automation.selenium.servicecenter.pages.catalog.CatalogItemDetailsPopUp;
import com.ihg.automation.selenium.servicecenter.pages.catalog.CatalogPage;
import com.ihg.automation.selenium.servicecenter.pages.communication.CommunicationPreferences;
import com.ihg.automation.selenium.servicecenter.pages.communication.CommunicationPreferencesPage;
import com.ihg.automation.selenium.servicecenter.pages.communication.CustomerServiceEmail;
import com.ihg.automation.selenium.servicecenter.pages.communication.CustomerServicePhone;
import com.ihg.automation.selenium.servicecenter.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsPage;
import com.ihg.automation.selenium.servicecenter.pages.event.business.BusinessEventsPage;
import com.ihg.automation.selenium.servicecenter.pages.event.copartner.CoPartnerEventsPage;
import com.ihg.automation.selenium.servicecenter.pages.event.freenight.FreeNightEventPage;
import com.ihg.automation.selenium.servicecenter.pages.event.meeting.MeetingEventPage;
import com.ihg.automation.selenium.servicecenter.pages.event.offer.OfferEventPage;
import com.ihg.automation.selenium.servicecenter.pages.event.order.OrderEventsPage;
import com.ihg.automation.selenium.servicecenter.pages.event.order.OrderGridRow;
import com.ihg.automation.selenium.servicecenter.pages.event.rewardnight.RewardNightEventPage;
import com.ihg.automation.selenium.servicecenter.pages.event.stay.StayEventsPage;
import com.ihg.automation.selenium.servicecenter.pages.event.stay.StayGuestGridRowContainer;
import com.ihg.automation.selenium.servicecenter.pages.order.OrderSummaryPopUp;
import com.ihg.automation.selenium.servicecenter.pages.personal.PersonalInfoPage;
import com.ihg.automation.selenium.servicecenter.pages.programs.EmployeePage;
import com.ihg.automation.selenium.servicecenter.pages.programs.GuestAccountPage;
import com.ihg.automation.selenium.servicecenter.pages.programs.amb.AmbassadorPage;
import com.ihg.automation.selenium.servicecenter.pages.programs.br.BusinessRewardsPage;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.RewardClubPage;
import com.ihg.crm.automation.selenium.tests.servicecenter.LoginLogout;

public class SmokeTest extends LoginLogout
{
    private Member member = new Member();
    private PersonalInfoPage personalInfoPage = new PersonalInfoPage();
    private final static CatalogItem CATALOG_ITEM = CatalogItem.builder("PS205", "PERSONAL SHOPPER 1000 POINTS")
            .loyaltyUnitCost("1000").build();

    @BeforeClass
    public void beforeClass()
    {
        member.addProgram(RC, IHG, EMP, BR);
        member.setPersonalInfo(MemberPopulateHelper.getSimplePersonalInfo());
        member.addAddress(MemberPopulateHelper.getUnitedStatesAddress());
        member.addEmail(MemberPopulateHelper.getRandomEmail());
        member.addPhone(MemberPopulateHelper.getRandomPhone());
        member.getPersonalInfo().setBirthDate(new LocalDate());

        login();
    }

    @Test(priority = 1)
    public void enrollToPrograms()
    {
        new EnrollmentPage().enroll(member);

        member.setAmbassadorOfferCode(AmbassadorOfferCode.CHSRO);
        new EnrollmentPage().enrollToDROrAMBProgramWithoutProfileChange(member, AMB,
                AmbassadorUtils.AMBASSADOR_COMP_PROMO_$0);
    }

    @Test(priority = 20)
    public void validatePersonalInformation()
    {
        personalInfoPage.goTo();

        personalInfoPage.getCustomerInfo().verify(member, Mode.VIEW);
        personalInfoPage.getAddressList().getExpandedContact().verify(member.getPreferredAddress(), Mode.VIEW);
        personalInfoPage.getPhoneList().getExpandedContact().verify(member.getPreferredPhone(), Mode.VIEW);
        personalInfoPage.getEmailList().getExpandedContact().verify(member.getPreferredEmail(), Mode.VIEW);
    }

    @Test(priority = 25)
    public void addSecondPhone()
    {
        GuestPhone secondPhone = MemberPopulateHelper.getRandomPhone();
        secondPhone.setPreferred(false);

        PhoneContactList phoneContactList = personalInfoPage.getPhoneList();
        phoneContactList.add(secondPhone, verifySuccess(CONTACT_SUCCESS_SAVE));
        phoneContactList.getExpandedContact(2).verify(secondPhone, Mode.VIEW);
    }

    @Test(priority = 30)
    public void addThirdPhone()
    {
        GuestPhone thirdPhone = MemberPopulateHelper.getRandomPhone();
        thirdPhone.setPreferred(false);

        PhoneContactList phoneContactList = personalInfoPage.getPhoneList();
        phoneContactList.add(thirdPhone, verifySuccess(CONTACT_SUCCESS_SAVE));
        phoneContactList.getExpandedContact(3).verify(thirdPhone, Mode.VIEW);
    }

    @Test(priority = 30)
    public void addSms()
    {
        GuestPhone smsPhone = new GuestPhone();
        smsPhone.setNumber(RandomUtils.getRandomNumber(7));

        SmsContactList smsContactList = personalInfoPage.getSmsList();
        smsContactList.add(smsPhone, verifySuccess(CONTACT_SUCCESS_SAVE));
        smsContactList.getExpandedContact().verify(smsPhone, Mode.VIEW);
    }

    @DataProvider(name = "pageProvider")
    protected Object[][] pageProvider()
    {
        return new Object[][] { { new EmployeePage() }, { new GuestAccountPage() }, { new RewardClubPage() },
                { new AmbassadorPage() }, { new BusinessRewardsPage() }, { new AllEventsPage() },
                { new StayEventsPage() }, { new OrderEventsPage() }, { new OfferEventPage() },
                { new RewardNightEventPage() }, { new FreeNightEventPage() }, { new CoPartnerEventsPage() },
                { new MeetingEventPage() }, { new BusinessEventsPage() } };
    }

    @Test(dataProvider = "pageProvider", priority = 40)
    public <T extends TabCustomContainerBase> void checkTabs(T page)
    {
        page.goTo();
        verifyNoErrors();
    }

    @Test(priority = 40)
    public void editCommPref()
    {
        String COMMENT = "Test";
        CommunicationPreferencesPage commPrefPage = new CommunicationPreferencesPage();

        commPrefPage.goTo();
        CommunicationPreferences commPrefs = commPrefPage.getCommunicationPreferences();
        commPrefs.verify(ENGLISH);

        commPrefs.clickEdit();
        commPrefs.getDoNotContact().check();

        CustomerServicePhone phone = commPrefs.getCustomerService().getPhone();
        phone.getPhoneCheckBox().check();
        verifyThat(phone.getPhone(), allOf(enabled(true), hasText(member.getPreferredPhone().getFullPhone())));

        CustomerServiceEmail email = commPrefs.getCustomerService().getEmail();
        email.getEmailCheckBox().check();
        verifyThat(email.getEmail(), allOf(enabled(true), hasText(member.getPreferredEmail().getEmail())));

        commPrefs.getComments().type(COMMENT);
        commPrefs.clickSave(verifySuccess("Communication preferences have been successfully saved"));

        MarketingPreferences marketingPrefs = commPrefs.getMarketingPreferences();
        ContactPermissionItems marketingPermissionItems = marketingPrefs.getContactPermissionItems();
        int marketingItemsCount = marketingPermissionItems.getContactPermissionItemsCount();
        verifyThat(marketingPrefs, marketingItemsCount, greaterThan(0), "There are items in marketing preferences");
        String formattedDate = DateUtils.getFormattedDate();
        for (int index = 1; index <= marketingItemsCount; index++)
        {
            ContactPermissionItem contactPermissionItem = marketingPermissionItems.getContactPermissionItem(index);
            verifyThat(contactPermissionItem.getSubscribe(), hasTextInView(OPT_OUT));
            verifyThat(contactPermissionItem.getDateUpdated(), hasText(formattedDate));
        }

        ContactPermissionItems smsPermissionItems = commPrefs.getSmsMobileTextingMarketingPreferences()
                .getContactPermissionItems();
        int smsItemsCount = smsPermissionItems.getContactPermissionItemsCount();
        verifyThat(smsPermissionItems, smsItemsCount, greaterThan(0), "There are items in sms preferences");
        for (int index = 1; index <= smsItemsCount; index++)
        {
            ContactPermissionItem contactPermissionItem = smsPermissionItems.getContactPermissionItem(index);
            verifyThat(contactPermissionItem.getSubscribe(), hasTextInView(OPT_OUT));
            verifyThat(contactPermissionItem.getDateUpdated(), hasText(formattedDate));
        }

        verifyThat(commPrefs.getComments(), hasTextInView(COMMENT));

        verifyThat(phone.getPhone(), hasTextInView(member.getPreferredPhone().getFullPhone()));
        verifyThat(email.getEmail(), hasTextInView(member.getPreferredEmail().getEmail()));
    }

    @Test(priority = 40)
    public void addPIN()
    {
        AccountInfoPage accountInfo = new AccountInfoPage();
        accountInfo.goTo();
        Security security = accountInfo.getSecurity();
        security.addPIN("5490");
        verifyThat(security.getPin(), hasTextInView("••••"));
    }

    @Test(priority = 50)
    public void createStay()
    {
        Stay stay = new Stay();
        stay.setHotelCode("ABQMB");
        stay.setCheckInOutByNights(2);
        stay.setAvgRoomRate(100.00);
        stay.setRateCode("TEST");
        Revenues revenues = stay.getRevenues();
        revenues.getFood().setAmount(50.0);

        StayEventsPage stayPage = new StayEventsPage();
        stayPage.createStay(stay);

        StayGuestGridRowContainer details = stayPage.getGuestGrid().getRow(1).getDetails();
        details.getStayInfo().verify(stay);

        verifyThat(new LeftPanel().getProgramInfoPanel().getPrograms().getRewardClubProgram().getPointsBalance(),
                hasTextAsInt(greaterThan(0)));

        AnnualActivityPage annualActivityPage = new AnnualActivityPage();
        annualActivityPage.goTo();

        verifyThat(annualActivityPage.getBaseUnits().getAmount(), hasTextAsInt(greaterThan(0)));
        verifyThat(annualActivityPage.getBonusUnits().getAmount(), hasTextAsInt(greaterThan(0)));
        verifyThat(annualActivityPage.getTotalUnits().getAmount(), hasTextAsInt(greaterThan(0)));
        verifyThat(annualActivityPage.getCurrentBalance().getAmount(), hasTextAsInt(greaterThan(0)));

        AnnualActivityRow annualActivityRow = annualActivityPage.getAnnualActivities().getCurrentYearRow();
        verifyThat(annualActivityRow, displayed(true));
        verifyThat(annualActivityRow.getCell(AnnualActivityRow.AnnualActivityCell.TIER_LEVEL_NIGHT),
                hasTextAsInt(stay.getNights()));
    }

    @Test(priority = 60)
    public void makeNewOrder()
    {
        int balanceBeforeOrder = Converter.stringToInteger(new LeftPanel().getProgramInfoPanel().getPrograms()
                .getRewardClubProgram().getPointsBalance().getText());

        CatalogPage catalogPage = new CatalogPage();
        catalogPage.goTo();

        catalogPage.searchByItemId(CATALOG_ITEM);

        CatalogItemDetailsPopUp catalogItemDetailsPopUp = catalogPage.openCatalogItemDetailsPopUp();
        catalogItemDetailsPopUp.getQuantity().select("1");

        OrderSummaryPopUp orderSummaryPopUp = catalogItemDetailsPopUp.openOrderSummaryPopUp();
        verifyThat(orderSummaryPopUp, displayed(true));
        orderSummaryPopUp.clickCheckout();
        verifyThat(orderSummaryPopUp, displayed(false));

        OrderEventsPage orderEventsPage = new OrderEventsPage();
        verifyThat(orderEventsPage, displayed(true));

        OrderGridRow redeemOrderRow = orderEventsPage.getOrdersGrid().getRow(1);
        redeemOrderRow.verify(OrderFactory.getRedeemOrder(CATALOG_ITEM, member, helper));
        redeemOrderRow.verifyRowGreenColor();

        verifyThat(new LeftPanel().getProgramInfoPanel().getPrograms().getRewardClubProgram().getPointsBalance(),
                hasTextAsInt(lessThan(balanceBeforeOrder)));
    }

    @Test(priority = 70)
    public void verifyTierLevel()
    {
        verifyThat(new LeftPanel().getProgramInfoPanel().getPrograms().getRewardClubProgram().getLevelCode(),
                hasText(RewardClubLevel.GOLD.getCode()));
    }
}
