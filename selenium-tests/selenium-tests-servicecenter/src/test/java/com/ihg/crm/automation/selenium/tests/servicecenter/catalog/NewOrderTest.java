package com.ihg.crm.automation.selenium.tests.servicecenter.catalog;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.event.EventTransactionType.Order.REDEEM;
import static com.ihg.automation.selenium.common.types.name.Salutation.MR;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.common.DateUtils;
import com.ihg.automation.selenium.common.catalog.CatalogItem;
import com.ihg.automation.selenium.common.event.AllEventsRowConverter;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.member.PersonalInfo;
import com.ihg.automation.selenium.common.order.Order;
import com.ihg.automation.selenium.common.order.OrderItem;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.servicecenter.pages.catalog.CatalogItemDetailsPopUp;
import com.ihg.automation.selenium.servicecenter.pages.catalog.CatalogPage;
import com.ihg.automation.selenium.servicecenter.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsPage;
import com.ihg.automation.selenium.servicecenter.pages.event.order.OrderEventsPage;
import com.ihg.automation.selenium.servicecenter.pages.event.order.OrderGridRow;
import com.ihg.automation.selenium.servicecenter.pages.event.order.OrderGridRow.OrderCell;
import com.ihg.automation.selenium.servicecenter.pages.event.order.OrderSystemDetailsGridRowContainer;
import com.ihg.automation.selenium.servicecenter.pages.order.OrderSummaryPopUp;
import com.ihg.automation.selenium.servicecenter.pages.unit.GoodwillPopUp;

public class NewOrderTest extends CatalogCommon
{
    private Member member = new Member();
    private OrderSummaryPopUp orderSummaryPopUp;
    private String POINTS = "50000";
    private CatalogPage catalogPage = new CatalogPage();
    private final static CatalogItem CATALOG_ITEM = CatalogItem.builder("PS205", "PERSONAL SHOPPER 1000 POINTS")
            .loyaltyUnitCost("1000").build();
    private OrderEventsPage orderPage;
    private Order order = new Order();
    private String QUANTITY = "2";

    @BeforeClass
    public void beforeClass()
    {
        PersonalInfo personalInfo = MemberPopulateHelper.getSimplePersonalInfo();
        personalInfo.getName().setSalutation(MR);
        member.setPersonalInfo(personalInfo);
        member.addAddress(MemberPopulateHelper.getUnitedStatesAddress());
        member.addProgram(Program.RC);

        OrderItem orderItem = new OrderItem(CATALOG_ITEM.getItemId(), CATALOG_ITEM.getItemName());
        orderItem.setQuantity(QUANTITY);
        orderItem.setLoyaltyUnits((String.valueOf(
                Integer.parseInt(CATALOG_ITEM.getLoyaltyUnitCost()) * Integer.parseInt(orderItem.getQuantity()))));

        order.getOrderItems().add(orderItem);
        order.setTotalLoyaltyUnits(orderItem.getLoyaltyUnits());
        order.setTransactionType(REDEEM);
        order.setTransactionDate(DateUtils.getFormattedDate());
        order.setShippingInfo(member, true);

        login();

        new EnrollmentPage().enroll(member);
        new GoodwillPopUp().goodwillPoints(POINTS);
    }

    @Test
    public void verifyOrderSummaryDetails()
    {
        catalogPage.goTo();

        catalogPage.searchByItemId("PS205");
        CatalogItemDetailsPopUp catalogItemDetailsPopUp = catalogPage.openCatalogItemDetailsPopUp();

        catalogItemDetailsPopUp.getQuantity().select(QUANTITY);

        orderSummaryPopUp = catalogItemDetailsPopUp.openOrderSummaryPopUp();

        verifyThat(orderSummaryPopUp, displayed(true));
        orderSummaryPopUp.getRow(1).verify(CATALOG_ITEM, "Expected Delivery Time: Weeks");
        orderSummaryPopUp.verify(member, Integer.parseInt(POINTS));

    }

    @Test(dependsOnMethods = { "verifyOrderSummaryDetails" }, alwaysRun = true)
    public void makeNewOrder()
    {
        orderSummaryPopUp.clickCheckout();

        verifyThat(orderSummaryPopUp, displayed(false));
        orderPage = new OrderEventsPage();
        verifyThat(orderPage, displayed(true));
    }

    @Test(dependsOnMethods = { "makeNewOrder" }, alwaysRun = true)
    public void verifyNewOrder()
    {
        OrderGridRow itemRow = orderPage.getOrdersGrid().getRow(1);

        itemRow.verify(order);

        itemRow.verifyRowGreenColor();

        order.setOrderNumber(itemRow.getCell(OrderCell.ORDER_NUMBER).getText());
    }

    @Test(dependsOnMethods = { "verifyNewOrder" })
    public void verifyRedeemEventOnAllEvents()
    {
        verifyRedeemEventOnAllEventsPage(AllEventsRowConverter.getRedeemEvent(order));
    }

    @Test(dependsOnMethods = { "verifyRedeemEventOnAllEvents" })
    public void verifyRedeemEventContainerOnAllEvents()
    {
        new AllEventsPage().getGrid().getRow(REDEEM).expand(OrderSystemDetailsGridRowContainer.class).getShippingInfo()
                .verify(order, true);
    }

}
