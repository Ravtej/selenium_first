package com.ihg.crm.automation.selenium.tests.servicecenter.programinfo.ambassador;

import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyNoError;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.Constant.NOT_AVAILABLE;
import static com.ihg.automation.selenium.common.event.EventTransactionType.Membership.MEMBERSHIP_OPEN;
import static com.ihg.automation.selenium.common.member.AmbassadorUtils.AMBASSADOR_COMP_PROMO_$0;
import static com.ihg.automation.selenium.common.types.program.ProgramStatus.OPEN;
import static com.ihg.automation.selenium.common.types.program.RewardClubLevel.GOLD;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.isDisplayedWithWait;
import static com.ihg.automation.selenium.matchers.component.HasSelectItems.hasSelectItems;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.catalog.CatalogItem;
import com.ihg.automation.selenium.common.enroll.EnrollProgramContainer;
import com.ihg.automation.selenium.common.member.AmbassadorJdbcUtils;
import com.ihg.automation.selenium.common.member.AmbassadorOfferCode;
import com.ihg.automation.selenium.common.member.AmbassadorUtils;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.pages.PaymentMethod;
import com.ihg.automation.selenium.common.payment.PaymentDetailsPopUp;
import com.ihg.automation.selenium.common.payment.PaymentFactory;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.common.types.program.ProgramStatus;
import com.ihg.automation.selenium.gwt.components.Mode;
import com.ihg.automation.selenium.servicecenter.pages.LeftPanel;
import com.ihg.automation.selenium.servicecenter.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsGridRow;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsPage;
import com.ihg.automation.selenium.servicecenter.pages.event.enroll.MembershipGridRowContainer;
import com.ihg.automation.selenium.servicecenter.pages.programs.RenewExtendProgramSummary;
import com.ihg.automation.selenium.servicecenter.pages.programs.amb.AmbassadorPage;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.RewardClubPage;

public class ReactivateByComplimentaryTest extends AmbassadorCommon
{
    private static final CatalogItem ENROLMENT_AWARD = AMBASSADOR_COMP_PROMO_$0;

    private AmbassadorPage ambassadorPage = new AmbassadorPage();
    private PaymentDetailsPopUp paymentDetailsPopUp;
    private EnrollmentPage enrollmentPage = new EnrollmentPage();
    private AllEventsPage allEventsPage = new AllEventsPage();
    private Member member;

    @BeforeClass
    public void beforeClass()
    {
        login();

        member = enrollAMBmember();
        new AmbassadorJdbcUtils(jdbcTemplateUpdate, lastUpdateUserId).updateForReactivate(member);
        new LeftPanel().reOpenMemberProfile(member);
    }

    @Test
    public void verifyEnrollment()
    {
        enrollmentPage.goTo();
        EnrollProgramContainer enrollProgramSection = enrollmentPage.getPrograms();
        verifyThat(enrollProgramSection.getProgramCheckbox(Program.AMB), displayed(true));
        enrollProgramSection.selectProgram(Program.AMB);
        verifyThat(enrollmentPage.getAMBOfferCode(), displayed(true));
        enrollmentPage.getCustomerInfo().getName().verify(member.getName(), Mode.EDIT);
        enrollmentPage.getAddress().verify(member.getAddresses().get(0), Mode.EDIT);
    }

    @Test(dependsOnMethods = { "verifyEnrollment" }, alwaysRun = true)
    public void verifyPaymentDetailsPopUp()
    {
        enrollmentPage.getAMBOfferCode().selectByCode(AmbassadorOfferCode.CHNRO);
        enrollmentPage.clickSubmit(verifyNoError());
        paymentDetailsPopUp = new PaymentDetailsPopUp();
        verifyThat(paymentDetailsPopUp, isDisplayedWithWait());
        verifyThat(paymentDetailsPopUp.getTotalAmount(), hasSelectItems(AmbassadorUtils.ENROLL_AWARD_NAME_LIST));
    }

    @Test(dependsOnMethods = { "verifyPaymentDetailsPopUp" }, alwaysRun = true)
    public void populateComplimentaryPaymentFields()
    {
        paymentDetailsPopUp = new PaymentDetailsPopUp();
        paymentDetailsPopUp.selectAmount(ENROLMENT_AWARD);
        verifyThat(paymentDetailsPopUp.getPaymentTypeButton(PaymentMethod.COMPLIMENTARY), displayed(true));
        paymentDetailsPopUp.clickSubmitNoError();
    }

    @Test(dependsOnMethods = { "populateComplimentaryPaymentFields" }, alwaysRun = true)
    public void verifySuccessEnrollmentPopUp()
    {
        validateSuccessEnrollmentPopUp(Program.AMB, member);
    }

    @Test(dependsOnMethods = { "verifySuccessEnrollmentPopUp" }, alwaysRun = true)
    public void verifySummary()
    {
        ambassadorPage.goTo();

        RenewExtendProgramSummary ambSummary = ambassadorPage.getSummary();
        verifyThat(ambSummary.getExpirationDate(), hasText(AmbassadorUtils.getExpirationDate()));

        ambSummary.verifyRenewAndExtendButtons(false, true);
        ambSummary.verifyStatus(ProgramStatus.OPEN);
    }

    @Test(dependsOnMethods = { "verifySummary" }, alwaysRun = true)
    public void verifyTierLevel()
    {
        RewardClubPage rewardClubPage = new RewardClubPage();
        rewardClubPage.goTo();
        rewardClubPage.getSummary().verify(OPEN, GOLD, 1);
    }

    @Test(dependsOnMethods = "verifyTierLevel", alwaysRun = true)
    public void verifyAllEventsContainer()
    {
        allEventsPage.goTo();
        AllEventsGridRow row = allEventsPage.getGrid().getRow(MEMBERSHIP_OPEN);
        verifyThat(row, displayed(true));

        MembershipGridRowContainer container = row.expand(MembershipGridRowContainer.class);
        container.getMembershipDetails().verify(PaymentFactory.COMPLIMENTARY, helper);
        verifyThat(container.getReason(), hasText(NOT_AVAILABLE));
    }

    @Test(dependsOnMethods = "verifyAllEventsContainer", alwaysRun = true)
    public void verifyOrderSystemContainer()
    {
        verifyOrderSystemDetailsAfterReactivate(member);
    }
}
