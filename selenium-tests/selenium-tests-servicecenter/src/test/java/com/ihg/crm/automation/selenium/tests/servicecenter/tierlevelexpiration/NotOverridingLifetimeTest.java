package com.ihg.crm.automation.selenium.tests.servicecenter.tierlevelexpiration;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.types.program.ProgramStatus.OPEN;
import static com.ihg.automation.selenium.common.types.program.RewardClubLevel.GOLD;
import static com.ihg.automation.selenium.common.types.program.RewardClubLevelReason.BASE;
import static com.ihg.automation.selenium.common.types.program.TierLevelExpiration.LIFETIME;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasTextAsInt;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.isDisplayedWithWait;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.deposit.Deposit;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.stay.Stay;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.servicecenter.pages.deposit.CreateDepositPopUp;
import com.ihg.automation.selenium.servicecenter.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.servicecenter.pages.event.offer.MemberOfferGridRow;
import com.ihg.automation.selenium.servicecenter.pages.event.offer.OfferEventPage;
import com.ihg.automation.selenium.servicecenter.pages.event.offer.OfferPopUp;
import com.ihg.automation.selenium.servicecenter.pages.event.stay.StayEventsPage;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.RewardClubPage;
import com.ihg.crm.automation.selenium.tests.servicecenter.LoginLogout;

public class NotOverridingLifetimeTest extends LoginLogout
{
    private static final String OFFER_ID = "8310441";
    private Stay stay = new Stay();

    @BeforeClass
    public void beforeClass()
    {
        Member member = new Member();
        member.setPersonalInfo(MemberPopulateHelper.getSimplePersonalInfo());
        member.addAddress(MemberPopulateHelper.getUnitedStatesAddress());
        member.addProgram(Program.RC);

        stay.setHotelCode("ATLCP");
        stay.setCheckInOutByNights(2);
        stay.setAvgRoomRate(100.00);
        stay.setRateCode("Test");

        login();
        new EnrollmentPage().enroll(member);

        RewardClubPage rewardClubPage = new RewardClubPage();
        rewardClubPage.goTo();
        rewardClubPage.getSummary().setTierLevelWithExpiration(GOLD, BASE, LIFETIME);

        verifyTierLevelLifetime();
    }

    /*
     * Gold Deposit with Following Year TL Exp doesn't change LIFETIME
     */
    @Test
    public void createDepositAndVerifyTierLevelLifetime()
    {
        Deposit testDeposit = new Deposit();
        testDeposit.setTransactionId("CBCGU");
        testDeposit.setHotel("ATLCP");

        new CreateDepositPopUp().createDeposit(testDeposit, false);
        verifyTierLevelLifetime();
    }

    @Test(dependsOnMethods = { "createDepositAndVerifyTierLevelLifetime" }, alwaysRun = true)
    public void tierLevelLifetimeNotChangedThroughRules()
    {
        OfferEventPage offerPage = new OfferEventPage();
        offerPage.goTo();
        offerPage.searchByOfferId(OFFER_ID, true);

        OfferPopUp offerPopUp = new OfferPopUp();
        offerPopUp.getRegister().clickAndWait();

        offerPopUp = new OfferPopUp();
        verifyThat(offerPopUp, isDisplayedWithWait());
        offerPopUp.waitAndClose();

        verifyOfferWins(0);

        new StayEventsPage().createStay(stay);
        verifyOfferWins(1);

        verifyTierLevelLifetime();
    }

    private void verifyOfferWins(int winsAmount)
    {
        OfferEventPage offerPage = new OfferEventPage();
        offerPage.goTo();

        MemberOfferGridRow offerRow = offerPage.getMemberOffersGrid().getRowByOfferCode(OFFER_ID);
        verifyThat(offerRow, isDisplayedWithWait());
        verifyThat(offerRow.getCell(MemberOfferGridRow.MemberOfferCell.WINS), hasTextAsInt(winsAmount));
    }

    private void verifyTierLevelLifetime()
    {
        RewardClubPage rewardClubPage = new RewardClubPage();
        rewardClubPage.goTo();
        rewardClubPage.getSummary().verifyLifetimeTierLevel(OPEN, GOLD);
    }
}
