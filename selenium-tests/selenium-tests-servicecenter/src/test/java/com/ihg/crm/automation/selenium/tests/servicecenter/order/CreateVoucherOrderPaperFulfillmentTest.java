package com.ihg.crm.automation.selenium.tests.servicecenter.order;

import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifySuccess;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.Constant.NOT_AVAILABLE;
import static com.ihg.automation.selenium.common.order.DeliveryOption.PAPER;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.enabled;
import static com.ihg.automation.selenium.matchers.component.HasSelectItems.hasSelectItems;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static org.hamcrest.text.IsEmptyString.isEmptyString;

import java.util.Arrays;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.google.common.collect.ImmutableList;
import com.ihg.automation.selenium.common.catalog.CatalogItem;
import com.ihg.automation.selenium.common.event.EarningEvent;
import com.ihg.automation.selenium.common.event.EarningEventFactory;
import com.ihg.automation.selenium.common.order.DeliveryOption;
import com.ihg.automation.selenium.common.order.Voucher;
import com.ihg.automation.selenium.common.order.VoucherOrder;
import com.ihg.automation.selenium.common.order.VoucherRule;
import com.ihg.automation.selenium.common.order.VoucherRuleType;
import com.ihg.automation.selenium.common.order.VoucherStatusType;
import com.ihg.automation.selenium.common.types.Currency;
import com.ihg.automation.selenium.common.voucher.VoucherOrderPopUp;
import com.ihg.automation.selenium.gwt.components.grid.Grid;
import com.ihg.automation.selenium.servicecenter.pages.catalog.CatalogItemDetailsPopUp;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsGridRow;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsGridRow.AllEventsCell;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsPage;
import com.ihg.automation.selenium.servicecenter.pages.event.order.OrderDetailsEdit;
import com.ihg.automation.selenium.servicecenter.pages.event.order.OrderDetailsPopUp;
import com.ihg.automation.selenium.servicecenter.pages.event.order.OrderDetailsTab;
import com.ihg.automation.selenium.servicecenter.pages.event.order.OrderEventsPage;
import com.ihg.automation.selenium.servicecenter.pages.event.order.OrderGridRow;
import com.ihg.automation.selenium.servicecenter.pages.event.order.VoucherOrderGridRowContainer;
import com.ihg.automation.selenium.servicecenter.pages.voucher.VoucherGridRow;
import com.ihg.automation.selenium.servicecenter.pages.voucher.VoucherLookupTab;
import com.ihg.automation.selenium.servicecenter.pages.voucher.VoucherPage;
import com.ihg.automation.selenium.servicecenter.pages.voucher.VoucherPopUp;

public class CreateVoucherOrderPaperFulfillmentTest extends VoucherOrderCommon
{
    private VoucherRule voucherRule;
    private VoucherOrder voucherOrder;

    private VoucherPage voucherPage = new VoucherPage();
    private OrderEventsPage orderEventsPage = new OrderEventsPage();
    private AllEventsPage allEventsPage = new AllEventsPage();

    private final static CatalogItem CATALOG_ITEM = CatalogItem.builder("AMB10K", "POINT VOUCHERS(25)10000 PNT EA")
            .startDate("07Aug12").endDate("31Dec99").status("Active").orderLimit("99").loyaltyUnitCost("0")
            .awardCost("1000", Currency.USD)
            .regions(ImmutableList.of("UNITED STATES", "CANADA", "MSAC", "EMEA", "ASIA PACIFIC"))
            .countries(ImmutableList.of("All")).tierLevels(ImmutableList.of("%", "RC Employee Member", "HOTEL PTS PGM"))
            .build();

    private final static EarningEvent earningEvent = EarningEventFactory.getAwardEvent(CATALOG_ITEM);

    @BeforeClass
    public void beforeClass()
    {
        voucherRule = new VoucherRule("AMB10K", "Ambassador Point Voucher 10000");
        voucherRule.setVoucherName("Bonus Point Voucher");
        voucherRule.setRuleType(VoucherRuleType.Unit);
        voucherRule.setCostAmount("40.00");
        voucherRule.setTierLevelCodeOfUpgrade(NOT_AVAILABLE);
        voucherRule.setTotalCostAmount("1,000.00");

        loginAndOpenHotelMemberProfile();
    }

    @Test
    public void searchVoucher()
    {
        VoucherGridRow row = searchVoucherRow(voucherRule);
        row.verify(voucherRule);
    }

    @Test(dependsOnMethods = { "searchVoucher" })
    public void orderVouchers()
    {
        voucherPage.getVouchers().getRow(voucherRule.getPromotionId()).clickById();
        VoucherOrderPopUp orderPopUp = new VoucherOrderPopUp();

        orderPopUp.verify(voucherRule);
        verifyThat(orderPopUp.getDeliveryOption(), hasText(PAPER));
        orderPopUp.verifyShippingInfoPaperFulfillment();

        verifyThat(orderPopUp.getDeliveryOption(), hasSelectItems(Arrays.asList(PAPER, DeliveryOption.EMAIL)));

        orderPopUp.clickPlaceOrder(verifySuccess(VOUCHERS_ORDER_SUCCESS_MESSAGE));
        voucherPage.clickClose();
    }

    @Test(dependsOnMethods = { "orderVouchers" })
    public void verifyEventOnOrder()
    {
        voucherOrder = new VoucherOrder(voucherRule);
        voucherOrder.setSource(helper);
        voucherOrder.setDeliveryOption(PAPER);
        initVoucherOrderRange(voucherOrder);

        orderEventsPage.goTo();

        OrderGridRow row = orderEventsPage.getOrdersGrid().getRow(1);
        row.verify(voucherOrder);

        VoucherOrderGridRowContainer rowContainer = row.expand(VoucherOrderGridRowContainer.class);
        rowContainer.verify(voucherOrder);
    }

    @Test(dependsOnMethods = { "verifyEventOnOrder" })
    public void verifyCatalogItemDetails()
    {
        VoucherOrderGridRowContainer gridContainer = orderEventsPage.getOrdersGrid().getRow(1)
                .expand(VoucherOrderGridRowContainer.class);
        gridContainer.getOrderDetails().getItemId().clickAndWait();

        CatalogItemDetailsPopUp catalogItemPopUp = new CatalogItemDetailsPopUp();
        catalogItemPopUp.verify(CATALOG_ITEM);
        catalogItemPopUp.clickClose();
    }

    @Test(dependsOnMethods = { "verifyCatalogItemDetails" })
    public void verifyOrderDetailsPopUpFromOrder()
    {
        verifyOrderDetails(orderEventsPage.getOrdersGrid());
    }

    @Test(dependsOnMethods = { "verifyOrderDetailsPopUpFromOrder" })
    public void verifyEventOnAllEvents()
    {
        allEventsPage.goTo();

        AllEventsGridRow row = allEventsPage.getGrid().getRow(1);
        verifyThat(row.getCell(AllEventsCell.TRANS_DATE), hasText(voucherOrder.getTransactionDate()));
        verifyThat(row.getCell(AllEventsCell.TRANS_TYPE), hasText(VoucherOrder.TRANSACTION_TYPE));
        verifyThat(row.getCell(AllEventsCell.DETAIL), hasText(voucherOrder.getVoucherName()));
        verifyThat(row.getCell(AllEventsCell.IHG_UNITS), hasText(isEmptyString()));
        verifyThat(row.getCell(AllEventsCell.ALLIANCE_UNITS), hasText(isEmptyString()));

        VoucherOrderGridRowContainer rowContainer = row.expand(VoucherOrderGridRowContainer.class);
        rowContainer.verify(voucherOrder);
    }

    @Test(dependsOnMethods = { "verifyEventOnAllEvents" })
    public void verifyOrderDetailsPopUpFromAllEvents()
    {
        verifyOrderDetails(allEventsPage.getGrid());
    }

    @Test(dependsOnMethods = { "verifyOrderDetailsPopUpFromAllEvents" })
    public void verifyVoucherLookup()
    {
        VoucherLookupTab lookupTab = new VoucherPopUp().getVoucherLookupTab();
        lookupTab.goTo();

        final String voucherNumber = getFirstVoucherNumber(voucherOrder.getPromotionId());
        lookupTab.lookup(voucherNumber);

        Voucher voucher = new Voucher(voucherNumber, voucherRule.getVoucherName(), VoucherStatusType.Created, helper);
        lookupTab.getVoucherDetailsPanel().verify(voucher);
        lookupTab.getStatusGrid().getRow(1).verify(voucher);
        lookupTab.getOrderDetailsPanel().verify(voucherOrder);
    }

    private void verifyOrderDetails(Grid grid)
    {
        new VoucherOrderGridRowContainer(grid.getRow(1).expand()).clickDetails();

        OrderDetailsPopUp orderDetailsPopUp = new OrderDetailsPopUp();

        OrderDetailsTab orderDetailsTab = orderDetailsPopUp.getOrderDetailsTab();
        orderDetailsTab.goTo();
        orderDetailsTab.verify(voucherOrder);

        OrderDetailsEdit orderDetails = orderDetailsTab.getOrderDetails();
        verifyThat(orderDetails.getEstimatedDeliveryDate(), enabled(false));
        verifyThat(orderDetails.getActualDeliveryDate(), enabled(false));
        verifyThat(orderDetails.getDeliveryStatus(), enabled(true));
        verifyThat(orderDetails.getTrackingUniqueNumber(), enabled(true));
        verifyThat(orderDetails.getSignedForBy(), enabled(true));
        verifyThat(orderDetails.getComments(), enabled(true));

        verifyEarningDetailsTab(orderDetailsPopUp, earningEvent);

        verifyBillingDetailsTab(orderDetailsPopUp, "1000", voucherOrder.getCurrencyCode());

        verifyHistoryDetailsTab(orderDetailsPopUp);

        orderDetailsPopUp.clickClose();
    }
}
