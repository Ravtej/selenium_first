package com.ihg.crm.automation.selenium.tests.servicecenter.programinfo;

import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyError;
import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyNoError;
import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifySuccess;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.types.program.AlternateLoyaltyIDType.HERTZ;
import static com.ihg.automation.selenium.common.types.program.Program.RC;
import static com.ihg.automation.selenium.common.types.program.RewardClubLevel.SPRE;
import static com.ihg.automation.selenium.common.types.program.RewardClubLevelReason.BASE;
import static com.ihg.automation.selenium.gwt.components.utils.ByStringText.contains;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasEmptyText;
import static com.ihg.automation.selenium.matchers.component.GridSize.size;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static com.ihg.automation.selenium.servicecenter.pages.history.AuditConstant.AccountInformation.ACCOUNT_INFORMATION;
import static com.ihg.automation.selenium.servicecenter.pages.history.AuditConstant.AccountInformation.ALTERNATE_LOYALTY_ID_HERTZ_ADDED;
import static com.ihg.automation.selenium.servicecenter.pages.history.AuditConstant.AccountInformation.ALTERNATE_LOYALTY_ID_HERTZ_CHANGED;
import static com.ihg.automation.selenium.servicecenter.pages.history.AuditConstant.AccountInformation.ALTERNATE_LOYALTY_ID_HERTZ_DELETED;
import static com.ihg.automation.selenium.servicecenter.pages.history.AuditConstant.AccountInformation.NUMBER;
import static com.ihg.automation.selenium.servicecenter.pages.history.AuditConstant.AccountInformation.TYPE;
import static com.ihg.automation.selenium.servicecenter.pages.history.ProfileHistoryGridRow.ProfileHistoryCell.NAME;
import static com.ihg.automation.selenium.servicecenter.pages.programs.ProgramPageBase.SUCCESS_MESSAGE;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.types.program.AlternateLoyaltyIDType;
import com.ihg.automation.selenium.gwt.components.dialog.ConfirmDialog;
import com.ihg.automation.selenium.gwt.components.input.Input;
import com.ihg.automation.selenium.servicecenter.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.servicecenter.pages.history.ProfileHistoryGridRow;
import com.ihg.automation.selenium.servicecenter.pages.history.ProfileHistoryPage;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.AddAlternateLoyaltyIDPopUp;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.AlternateLoyaltyIDGrid;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.AlternateLoyaltyIDRow;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.EditAlternateLoyaltyIDPopUp;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.RewardClubPage;
import com.ihg.crm.automation.selenium.tests.servicecenter.LoginLogout;

public class AddHertzAlternateLoyaltyIDTest extends LoginLogout
{
    public static final String CREATE_NUMBER = "123456789";
    private static final String NUMBER_WITH_MORE_THAN_9_DIGITS = "1234567890";
    private static final String NON_NUMERIC_NUMBER = "abcdef";
    private static final String UPDATE_NUMBER = "987654";
    private RewardClubPage rewardClubPage = new RewardClubPage();
    private AlternateLoyaltyIDRow loyaltyIDRow;

    @BeforeClass
    public void beforeClass()
    {
        Member member = new Member();
        member.setPersonalInfo(MemberPopulateHelper.getSimplePersonalInfo());
        member.addAddress(MemberPopulateHelper.getUnitedStatesAddress());
        member.addProgram(RC);

        login();
        new EnrollmentPage().enroll(member);

        rewardClubPage.goTo();
        rewardClubPage.getSummary().setTierLevelWithExpiration(SPRE, BASE);
    }

    @Test
    public void verifyAlternateLoyaltyIDPopUp()
    {
        rewardClubPage.clickAddAlternateID();
        AddAlternateLoyaltyIDPopUp alternateLoyaltyIDPopUp = new AddAlternateLoyaltyIDPopUp();
        verifyThat(alternateLoyaltyIDPopUp, displayed(true));
        verifyThat(alternateLoyaltyIDPopUp.getNumber(), displayed(true));
        verifyThat(alternateLoyaltyIDPopUp.getType(), displayed(true));

        verifyThat(alternateLoyaltyIDPopUp.getSave(), displayed(true));
        verifyThat(alternateLoyaltyIDPopUp.getClose(), displayed(true));

        alternateLoyaltyIDPopUp.clickClose();
    }

    @Test(dependsOnMethods = "verifyAlternateLoyaltyIDPopUp", alwaysRun = true)
    public void tryToAddNotValidNumber()
    {
        rewardClubPage.clickAddAlternateID();
        AddAlternateLoyaltyIDPopUp alternateLoyaltyIDPopUp = new AddAlternateLoyaltyIDPopUp();

        Input number = alternateLoyaltyIDPopUp.getNumber();

        alternateLoyaltyIDPopUp.getType().select(HERTZ);
        number.type(NUMBER_WITH_MORE_THAN_9_DIGITS);
        verifyThat(number, hasText(CREATE_NUMBER));

        number.type(NON_NUMERIC_NUMBER);
        verifyThat(number, hasEmptyText());

        alternateLoyaltyIDPopUp.clickClose();
    }

    @Test(dependsOnMethods = "tryToAddNotValidNumber", alwaysRun = true)
    public void addAlternateLoyaltyID()
    {
        AlternateLoyaltyIDGrid alternateGrid = rewardClubPage.getAlternateLoyaltyID();
        verifyThat(alternateGrid, size(0));

        rewardClubPage.clickAddAlternateID();
        AddAlternateLoyaltyIDPopUp alternateLoyaltyIDPopUp = new AddAlternateLoyaltyIDPopUp();
        alternateLoyaltyIDPopUp.populateHertzID(CREATE_NUMBER);
        alternateLoyaltyIDPopUp.clickSave(verifySuccess(SUCCESS_MESSAGE));

        verifyThat(alternateGrid, size(1));
        loyaltyIDRow = alternateGrid.getRow(1);
        loyaltyIDRow.verifyHertz(CREATE_NUMBER);
    }

    @Test(dependsOnMethods = { "addAlternateLoyaltyID" })
    public void verifyHistoryAfterAddAlternateLoyaltyID()
    {
        ProfileHistoryPage historyPage = new ProfileHistoryPage();
        historyPage.goTo();
        verifyNoError();

        ProfileHistoryGridRow hertzRow = historyPage.getProfileHistoryGrid().getRow(1)
                .getSubRowByName(ACCOUNT_INFORMATION).getSubRowByName(contains(ALTERNATE_LOYALTY_ID_HERTZ_ADDED));

        hertzRow.getSubRowByName(TYPE).verifyAddAction(AlternateLoyaltyIDType.HERTZ);
        hertzRow.getSubRowByName(NUMBER).verifyAddAction(CREATE_NUMBER);
    }

    @Test(dependsOnMethods = "verifyHistoryAfterAddAlternateLoyaltyID", alwaysRun = true)
    public void upgradeAlternateWithoutChanges()
    {
        rewardClubPage.goTo();
        AlternateLoyaltyIDGrid alternateGrid = rewardClubPage.getAlternateLoyaltyID();
        loyaltyIDRow = alternateGrid.getRow(1);

        loyaltyIDRow.openEditAlternateIdPopUp().clickUpgrade(verifySuccess(SUCCESS_MESSAGE));

        loyaltyIDRow = alternateGrid.getRow(1);
        loyaltyIDRow.verifyHertz(CREATE_NUMBER);
    }

    @Test(dependsOnMethods = "upgradeAlternateWithoutChanges", alwaysRun = true)
    public void updateAlternateWithNewNumber()
    {
        AlternateLoyaltyIDGrid alternateGrid = rewardClubPage.getAlternateLoyaltyID();
        loyaltyIDRow = alternateGrid.getRow(1);
        EditAlternateLoyaltyIDPopUp editAlternateLoyaltyIDPopUp = loyaltyIDRow.openEditAlternateIdPopUp();

        Input number = editAlternateLoyaltyIDPopUp.getNumber();
        number.type(NUMBER_WITH_MORE_THAN_9_DIGITS);
        verifyThat(editAlternateLoyaltyIDPopUp.getNumber(), hasText(CREATE_NUMBER));

        number.type(UPDATE_NUMBER);
        editAlternateLoyaltyIDPopUp.clickSave(verifySuccess(SUCCESS_MESSAGE));

        loyaltyIDRow = alternateGrid.getRow(1);
        loyaltyIDRow.verifyHertz(UPDATE_NUMBER);
    }

    @Test(dependsOnMethods = { "updateAlternateWithNewNumber" })
    public void verifyHistoryAfterUpdateAlternateLoyaltyID()
    {
        ProfileHistoryPage historyPage = new ProfileHistoryPage();
        historyPage.goTo();
        verifyNoError();

        ProfileHistoryGridRow hertzRow = historyPage.getProfileHistoryGrid().getRow(1)
                .getSubRowByName(ACCOUNT_INFORMATION).getSubRowByName(contains(ALTERNATE_LOYALTY_ID_HERTZ_CHANGED));

        hertzRow.getSubRowByName(NUMBER).verify(CREATE_NUMBER, UPDATE_NUMBER);
    }

    @Test(dependsOnMethods = "verifyHistoryAfterUpdateAlternateLoyaltyID", alwaysRun = true)
    public void tryToAddSecondAlternateLoyaltyID()
    {
        rewardClubPage.goTo();
        AlternateLoyaltyIDGrid alternateGrid = rewardClubPage.getAlternateLoyaltyID();
        verifyThat(alternateGrid, size(1));

        rewardClubPage.clickAddAlternateID();
        AddAlternateLoyaltyIDPopUp alternateLoyaltyIDPopUp = new AddAlternateLoyaltyIDPopUp();
        alternateLoyaltyIDPopUp.populateHertzID(CREATE_NUMBER);
        alternateLoyaltyIDPopUp.clickSave(verifyError("Member already has Hertz Membership"));
        alternateLoyaltyIDPopUp.clickClose();

        verifyThat(alternateGrid, size(1));
        alternateGrid.getRow(1).verifyHertz(UPDATE_NUMBER);
    }

    @Test(dependsOnMethods = "tryToAddSecondAlternateLoyaltyID", alwaysRun = true)
    public void cancelDeletingAlternateLoyaltyID()
    {
        AlternateLoyaltyIDGrid alternateGrid = rewardClubPage.getAlternateLoyaltyID();
        verifyThat(alternateGrid, size(1));

        alternateGrid.getRow(1).clickDelete();

        ConfirmDialog confirmDialog = new ConfirmDialog();
        verifyThat(confirmDialog, displayed(true));
        verifyThat(confirmDialog, hasText("Are you sure you want to delete the Alternate ID?"));
        confirmDialog.clickNo(verifyNoError());

        verifyThat(alternateGrid, size(1));
    }

    @Test(dependsOnMethods = { "cancelDeletingAlternateLoyaltyID" })
    public void verifyHistoryAfterCancelDeletingAlternateLoyaltyID()
    {
        ProfileHistoryPage historyPage = new ProfileHistoryPage();
        historyPage.goTo();
        verifyNoError();

        ProfileHistoryGridRow hertzEditRow = historyPage.getProfileHistoryGrid().getRow(1)
                .getSubRowByName(ACCOUNT_INFORMATION).getSubRow(1);
        verifyThat(hertzEditRow.getCell(NAME),
                hasText(String.format("%s\n%s", ALTERNATE_LOYALTY_ID_HERTZ_CHANGED, UPDATE_NUMBER)));
    }

    @Test(dependsOnMethods = "verifyHistoryAfterCancelDeletingAlternateLoyaltyID", alwaysRun = true)
    public void deleteAlternateLoyaltyID()
    {
        rewardClubPage.goTo();
        AlternateLoyaltyIDGrid alternateGrid = rewardClubPage.getAlternateLoyaltyID();

        alternateGrid.getRow(1).clickDelete();
        new ConfirmDialog().clickYes(verifyNoError(), verifySuccess(SUCCESS_MESSAGE));

        verifyThat(alternateGrid, size(0));
    }

    @Test(dependsOnMethods = { "deleteAlternateLoyaltyID" })
    public void verifyProfileHistoryAfterDeleteAlternateLoyaltyID()
    {
        ProfileHistoryPage historyPage = new ProfileHistoryPage();
        historyPage.goTo();
        verifyNoError();

        ProfileHistoryGridRow hertzRow = historyPage.getProfileHistoryGrid().getRow(1)
                .getSubRowByName(ACCOUNT_INFORMATION).getSubRowByName(contains(ALTERNATE_LOYALTY_ID_HERTZ_DELETED));

        hertzRow.getSubRowByName(NUMBER).verifyDeleteAction(UPDATE_NUMBER);
    }
}
