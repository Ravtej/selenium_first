package com.ihg.crm.automation.selenium.tests.servicecenter.programinfo.dr;

import static com.ihg.automation.common.DateUtils.getStringToDate;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.event.EarningEventFactory.getProgramEventWithoutStatus;
import static com.ihg.automation.selenium.common.event.EventTransactionType.Membership.MEMBERSHIP_RENEWAL;
import static com.ihg.automation.selenium.common.member.DiningRewardAward.GOLD_TO_PLAT_RENEWAL_NON_HOTEL;
import static com.ihg.automation.selenium.common.member.MemberJdbcUtils.UPDATE_EXP;
import static com.ihg.automation.selenium.common.offer.OfferFactory.FREE_NIGHT_OFFER_CODE;
import static com.ihg.automation.selenium.common.types.program.Program.DR;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.isDisplayedWithWait;

import org.joda.time.LocalDate;
import org.springframework.beans.factory.annotation.Value;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.common.DateUtils;
import com.ihg.automation.selenium.common.Constant;
import com.ihg.automation.selenium.common.catalog.CatalogItem;
import com.ihg.automation.selenium.common.event.AllEventsRow;
import com.ihg.automation.selenium.common.event.AllEventsRowConverter;
import com.ihg.automation.selenium.common.event.EarningEvent;
import com.ihg.automation.selenium.common.event.EarningEventFactory;
import com.ihg.automation.selenium.common.member.DiningRewardAward;
import com.ihg.automation.selenium.common.member.MemberJdbcUtils;
import com.ihg.automation.selenium.common.order.Order;
import com.ihg.automation.selenium.common.order.OrderFactory;
import com.ihg.automation.selenium.common.pages.CreditCardType;
import com.ihg.automation.selenium.common.payment.CreditCardPayment;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.common.types.program.RewardClubLevel;
import com.ihg.automation.selenium.gwt.components.Mode;
import com.ihg.automation.selenium.servicecenter.pages.GuestSearch;
import com.ihg.automation.selenium.servicecenter.pages.Role;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsGridRow;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsPage;
import com.ihg.automation.selenium.servicecenter.pages.event.enroll.MembershipDetails;
import com.ihg.automation.selenium.servicecenter.pages.event.enroll.MembershipDetailsPopUp;
import com.ihg.automation.selenium.servicecenter.pages.event.enroll.MembershipDetailsTab;
import com.ihg.automation.selenium.servicecenter.pages.event.enroll.MembershipGridRowContainer;
import com.ihg.automation.selenium.servicecenter.pages.event.order.OrderSystemDetailsGridRowContainer;
import com.ihg.automation.selenium.servicecenter.pages.programs.ProgramPageBase.EnrollmentDetails;
import com.ihg.automation.selenium.servicecenter.pages.programs.dr.DiningRewardsPage;

public class RenewByCreditCardTest extends DRCommon
{
    @Value("${memberRenewByCreditCard}")
    protected String memberRenewByCreditCard;

    @Value("${memberRenewByCreditCard.date}")
    protected String date;

    private LocalDate enrollDate;
    private String enrollDateToString;
    private static final CatalogItem RENEW_AWARD = DiningRewardAward.RENEW_RMB2188_SC;
    private AllEventsPage allEventsPage = new AllEventsPage();
    private AllEventsGridRow membershipRenewalRow;
    private MembershipDetails membershipDetails;
    private CreditCardPayment creditCardItem = new CreditCardPayment(RENEW_AWARD, "6240008631401148",
            CreditCardType.CHINA_UNION_PAY);

    @BeforeClass
    public void beforeClass()
    {
        MemberJdbcUtils.update(jdbcTemplateUpdate, UPDATE_EXP, memberRenewByCreditCard, DR, 13);

        enrollDate = getStringToDate(date);
        enrollDateToString = enrollDate.toString(DateUtils.DATE_FORMAT);

        login(user15U, Role.AGENT_TIER2);

        new GuestSearch().byMemberNumber(memberRenewByCreditCard);
    }

    @Test
    public void verifyRCProgramInformation()
    {
        verifyRCProgramInformationDetails(RewardClubLevel.PLTN);
    }

    @Test(dependsOnMethods = "verifyRCProgramInformation", alwaysRun = true)
    public void verifyProgramSummaryDetails()
    {
        verifyProgramSummary(memberRenewByCreditCard, 13);
    }

    @Test(dependsOnMethods = "verifyProgramSummaryDetails", alwaysRun = true)
    public void verifyEnrollmentDetails()
    {
        DiningRewardsPage diningRewardsPage = new DiningRewardsPage();
        EnrollmentDetails enrollmentDetails = diningRewardsPage.getEnrollmentDetails();
        enrollmentDetails.getPaymentDetails().verify(creditCardItem, Mode.VIEW);
        enrollmentDetails.verifyScSource(helper);
    }

    @Test(dependsOnMethods = "verifyEnrollmentDetails", alwaysRun = true)
    public void verifyOrderSystemEventDetails()
    {
        Order systemOrder = OrderFactory.getSystemOrder(GOLD_TO_PLAT_RENEWAL_NON_HOTEL, null);
        systemOrder.setTransactionDate(enrollDateToString);
        systemOrder.getOrderItems().get(0).setDeliveryStatus(null);

        allEventsPage = new AllEventsPage();
        allEventsPage.goTo();

        AllEventsGridRow orderSystemRow = allEventsPage.getGrid().getRow(systemOrder.getTransactionType());
        orderSystemRow.verify(AllEventsRowConverter.convert(systemOrder));
        orderSystemRow.expand(OrderSystemDetailsGridRowContainer.class).verifyWithShippingAsOnLeftPanel(systemOrder);
    }

    @Test(dependsOnMethods = "verifyOrderSystemEventDetails", alwaysRun = true)
    public void verifyMembershipRenewEventInContainer()
    {
        AllEventsRow membershipRenewalRowData = new AllEventsRow(enrollDateToString, MEMBERSHIP_RENEWAL,
                Program.DR.getCode(), "5,000", "");

        membershipRenewalRow = allEventsPage.getGrid().getRow(MEMBERSHIP_RENEWAL);
        membershipRenewalRow.verify(membershipRenewalRowData);

        MembershipGridRowContainer container = membershipRenewalRow.expand(MembershipGridRowContainer.class);
        membershipDetails = container.getMembershipDetails();
        membershipDetails.getPaymentDetails().verify(creditCardItem, Mode.VIEW);
        membershipDetails.verifyScSource(helper);
    }

    @Test(dependsOnMethods = "verifyMembershipRenewEventInContainer", alwaysRun = true)
    public void verifyMembershipRenewEventDetailsPopUp()
    {
        MembershipGridRowContainer container = membershipRenewalRow.expand(MembershipGridRowContainer.class);
        container.clickDetails();
        MembershipDetailsPopUp membershipDetailsPopUp = new MembershipDetailsPopUp();
        verifyThat(membershipDetailsPopUp, isDisplayedWithWait());

        MembershipDetailsTab membershipDetailsTab = membershipDetailsPopUp.getMembershipDetailsTab();
        membershipDetailsTab.goTo();
        membershipDetails = membershipDetailsTab.getMembershipDetails();
        membershipDetails.getPaymentDetails().verify(creditCardItem, Mode.VIEW);
        membershipDetails.verifyScSource(helper);
    }

    @Test(dependsOnMethods = "verifyMembershipRenewEventDetailsPopUp", alwaysRun = true)
    public void validateMembershipRenewEventEarningDetails()
    {
        verifyMembershipRenewEarningDetails();
    }

    @Test(dependsOnMethods = "validateMembershipRenewEventEarningDetails", alwaysRun = true)
    public void validateMembershipRenewEventEarningDetailsGrid()
    {
        EarningEvent rcPointEvent = new EarningEvent(FREE_NIGHT_OFFER_CODE, "0", "0", Constant.RC_POINTS, enrollDate);

        new MembershipDetailsPopUp().getEarningDetailsTab().getGrid().verifyRowsFoundByTypeAndAward(
                EarningEventFactory.getAwardEvent(RENEW_AWARD, enrollDate),
                EarningEventFactory.getRenewDREvent(enrollDate), rcPointEvent, rcPointEvent,
                getProgramEventWithoutStatus(DR, GOLD_TO_PLAT_RENEWAL_NON_HOTEL, enrollDate));
    }

    @Test(dependsOnMethods = "validateMembershipRenewEventEarningDetailsGrid", alwaysRun = true)
    public void verifyMembershipRenewEventBillingDetails()
    {
        validateMembershipRenewBillingDetails(RENEW_AWARD, enrollDate);
    }

    @Test(dependsOnMethods = { "verifyMembershipRenewEventBillingDetails" }, alwaysRun = true)
    public void verifyOfferRows()
    {
        verifyOfferRow();
    }

    @Test(dependsOnMethods = { "verifyOfferRows" }, alwaysRun = true)
    public void verifyFreeNightTab()
    {
        verifyFreeNightHasTwoWinsAfterRenew();
    }

}
