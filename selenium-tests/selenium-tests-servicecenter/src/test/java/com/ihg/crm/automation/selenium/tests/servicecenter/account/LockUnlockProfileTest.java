package com.ihg.crm.automation.selenium.tests.servicecenter.account;

import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifySuccess;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.types.program.Program.RC;
import static com.ihg.automation.selenium.matchers.component.HasSelectItems.hasSelectItems;
import static com.ihg.automation.selenium.matchers.component.ViewableMatcher.hasTextInView;

import java.util.ArrayList;
import java.util.Arrays;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.servicecenter.pages.account.AccountInfoPage;
import com.ihg.automation.selenium.servicecenter.pages.account.Security;
import com.ihg.automation.selenium.servicecenter.pages.enroll.EnrollmentPage;
import com.ihg.crm.automation.selenium.tests.servicecenter.LoginLogout;

public class LockUnlockProfileTest extends LoginLogout
{
    private Member member = new Member();
    private AccountInfoPage accountInfo;
    private Security security;
    private ArrayList<String> expectedLockoutIndicator = new ArrayList<String>(Arrays.asList("Locked", "Unlocked"));

    @BeforeClass
    public void beforeClass()
    {
        member.addProgram(RC);
        member.setPersonalInfo(MemberPopulateHelper.getSimplePersonalInfo());
        member.addEmail(MemberPopulateHelper.getRandomEmail());
        member.addAddress(MemberPopulateHelper.getUnitedStatesAddress());

        login();

        new EnrollmentPage().enroll(member);
    }

    @Test
    public void addPIN()
    {
        accountInfo = new AccountInfoPage();
        accountInfo.goTo();
        security = accountInfo.getSecurity();
        security.addPIN("7686");
        verifyThat(security.getPin(), hasTextInView("••••"));
    }

    @Test(dependsOnMethods = { "addPIN" }, alwaysRun = true)
    public void verifyLockoutDropDown()
    {
        security.clickEdit();
        verifyThat(security.getLockout(), hasSelectItems(expectedLockoutIndicator));
    }

    @Test(dependsOnMethods = { "verifyLockoutDropDown" })
    public void lockProfile()
    {
        security.getLockout().select("Locked");
        security.clickSave(verifySuccess("Account information has been successfully saved."));
        verifyThat(security.getLockout(), hasTextInView("Locked"));
    }

    @Test(dependsOnMethods = { "lockProfile" }, alwaysRun = true)
    public void unLockProfile()
    {
        security.clickEdit();
        security.getLockout().select("Unlocked");
        security.clickSave(verifySuccess("Account information has been successfully saved."));
        verifyThat(security.getLockout(), hasTextInView("Unlocked"));
    }
}
