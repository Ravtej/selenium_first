package com.ihg.crm.automation.selenium.tests.servicecenter.business;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.Constant.NOT_AVAILABLE;
import static com.ihg.automation.selenium.common.event.EventTransactionType.BusinessRewards.BUSINESS_REWARDS;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasDefault;
import static com.ihg.automation.selenium.matchers.component.GridSize.size;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static com.ihg.automation.selenium.matchers.component.ViewableMatcher.hasTextInView;
import static org.apache.commons.lang.StringUtils.EMPTY;
import static org.hamcrest.core.IsNot.not;

import org.springframework.beans.factory.annotation.Value;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.CustomerInfoPanel;
import com.ihg.automation.selenium.common.business.BusinessRevenueView;
import com.ihg.automation.selenium.common.business.BusinessRewardsDetails;
import com.ihg.automation.selenium.common.business.BusinessRewardsEventDetailsPopUp;
import com.ihg.automation.selenium.common.business.BusinessRewardsGridRowContainer;
import com.ihg.automation.selenium.common.business.DiscretionaryPointsFields;
import com.ihg.automation.selenium.common.business.EventInformationFields;
import com.ihg.automation.selenium.common.business.EventSummaryTab;
import com.ihg.automation.selenium.common.business.OffersFieldsEdit;
import com.ihg.automation.selenium.common.business.OffersFieldsView;
import com.ihg.automation.selenium.common.business.TotalEventAwardFields;
import com.ihg.automation.selenium.common.business.TotalEventAwardItem;
import com.ihg.automation.selenium.common.types.Currency;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.matchers.component.ComponentMatcher;
import com.ihg.automation.selenium.servicecenter.pages.GuestSearch;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsGrid;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsGridRow;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsPage;
import com.ihg.crm.automation.selenium.tests.servicecenter.LoginLogout;

public class BusinessEventOnAllEventPageTest extends LoginLogout
{
    @Value("${memberBusiness}")
    protected String memberBusiness;

    private BusinessRewardsGridRowContainer container;

    @BeforeClass
    public void beforeClass()
    {
        login();

        new GuestSearch().byMemberNumber(memberBusiness);
        verifyThat(new CustomerInfoPanel().getMemberNumber(Program.RC), ComponentMatcher.hasText(memberBusiness));
    }

    @Test
    public void verifyBusinessRewardsContainer()
    {
        AllEventsPage allEventsPage = new AllEventsPage();
        allEventsPage.goTo();

        allEventsPage.getSearchFields().getEventType().select("Business Rewards");
        allEventsPage.getButtonBar().clickSearch();

        AllEventsGrid grid = allEventsPage.getGrid();
        verifyThat(grid, size(not(0)));

        AllEventsGridRow row = grid.getRow(BUSINESS_REWARDS, "CEQHA 28Feb16 - 29Feb16 Event01");

        container = row.expand(BusinessRewardsGridRowContainer.class);
        BusinessRewardsDetails businessRewardsDetails = container.getBusinessRewardsDetails();

        EventInformationFields eventInformationFields = businessRewardsDetails.getEventInformation();
        verifyThat(eventInformationFields.getHotel(), hasTextInView("CEQHA"));
        verifyThat(eventInformationFields.getHotelCurrency(), hasText(Currency.EUR.getValue()));
        verifyThat(eventInformationFields.getEventContractDate(), hasTextInView(NOT_AVAILABLE));
        verifyThat(eventInformationFields.getEventType(), hasTextInView(NOT_AVAILABLE));
        verifyThat(eventInformationFields.getEventName(), hasTextInView("Event01"));
        verifyThat(eventInformationFields.getHotelContactName(), hasTextInView(NOT_AVAILABLE));
        verifyThat(eventInformationFields.getHotelContactEmail(), hasTextInView(NOT_AVAILABLE));
        verifyThat(eventInformationFields.getHotelContactPhone(), hasTextInView(NOT_AVAILABLE));

        verifyBusinessRevenue(businessRewardsDetails.getTotalEligibleRevenue());

        DiscretionaryPointsFields discretionaryPointsFields = businessRewardsDetails.getDiscretionaryPoints();
        verifyThat(discretionaryPointsFields.getAdditionalPointsToAward(), hasTextInView(NOT_AVAILABLE));
        verifyThat(discretionaryPointsFields.getComments(), hasTextInView(NOT_AVAILABLE));
        verifyThat(discretionaryPointsFields.getReason(), hasTextInView(NOT_AVAILABLE));
        verifyThat(discretionaryPointsFields.getEstimatedCost(), hasText(NOT_AVAILABLE));

        OffersFieldsView offerFields = businessRewardsDetails.getOffers();
        verifyThat(offerFields.getOffer(), hasText("TEST OFFER DON'T MIGRATE"));
        verifyThat(offerFields.getPointsToAward(), hasText("500"));
        verifyThat(offerFields.getEstimatedCost(), hasText("5.00"));

        verifyTotalEventAwardFields(businessRewardsDetails.getTotalEventAward());
    }

    @Test(dependsOnMethods = { "verifyBusinessRewardsContainer" })
    public void verifyBusinessRewardsDetailsPopUp()
    {
        container.clickDetails();

        BusinessRewardsEventDetailsPopUp popUp = new BusinessRewardsEventDetailsPopUp();

        EventSummaryTab eventSummaryTab = popUp.getEventSummaryTab();

        EventInformationFields eventInformationFields = eventSummaryTab.getEventInformation();
        verifyThat(eventInformationFields.getHotel(), hasText("CEQHA"));
        verifyThat(eventInformationFields.getHotelCurrency(), hasText(Currency.EUR.getValue()));
        verifyThat(eventInformationFields.getEventContractDate(), hasDefault());
        verifyThat(eventInformationFields.getEventType(), hasDefault());
        verifyThat(eventInformationFields.getEventName(), hasText("Event01"));
        verifyThat(eventInformationFields.getHotelContactName(), hasDefault());
        verifyThat(eventInformationFields.getHotelContactEmail(), hasDefault());
        verifyThat(eventInformationFields.getHotelContactPhone(), hasDefault());

        DiscretionaryPointsFields discretionaryPoints = eventSummaryTab.getDiscretionaryPoints();
        discretionaryPoints.expand();
        verifyThat(discretionaryPoints.getAdditionalPointsToAward(), hasDefault());
        verifyThat(discretionaryPoints.getComments(), hasText(EMPTY));
        verifyThat(discretionaryPoints.getReason(), hasDefault());
        verifyThat(discretionaryPoints.getEstimatedCost(), hasDefault());

        verifyBusinessRevenue(eventSummaryTab.getTotalEligibleRevenue());

        OffersFieldsEdit offerFields = eventSummaryTab.getOffers();
        offerFields.expand();
        verifyThat(offerFields.getOffer(), hasText("TEST OFFER DON'T MIGRATE"));
        verifyThat(offerFields.getPointsToAward(), hasText("500"));
        verifyThat(offerFields.getEstimatedCost(), hasText("5.00"));

        verifyTotalEventAwardFields(eventSummaryTab.getTotalEventAward());
    }

    private void verifyBusinessRevenue(BusinessRevenueView businessRevenue)
    {
        verifyThat(businessRevenue.getLocalAmount(), hasText("150.00"));
        verifyThat(businessRevenue.getUSDAmount(), hasText("163.74"));
        verifyThat(businessRevenue.getEstimatedPointEarning(), hasText("490"));
        verifyThat(businessRevenue.getEstimatedCost(), hasText("2.32"));
    }

    private void verifyTotalEventAwardFields(TotalEventAwardFields totalEventAwardFields)
    {
        TotalEventAwardItem basePoints = totalEventAwardFields.getBasePoints();
        verifyThat(basePoints.getEstimatedPointEarning(), hasText("490"));
        verifyThat(basePoints.getEstimatedCostToHotel(), hasText("2.32"));

        TotalEventAwardItem discretionaryPoints = totalEventAwardFields.getDiscretionaryPoints();
        verifyThat(discretionaryPoints.getEstimatedPointEarning(), hasText(NOT_AVAILABLE));
        verifyThat(discretionaryPoints.getEstimatedCostToHotel(), hasText(NOT_AVAILABLE));

        TotalEventAwardItem offerPoints = totalEventAwardFields.getOfferPoints();
        verifyThat(offerPoints.getEstimatedPointEarning(), hasText("500"));
        verifyThat(offerPoints.getEstimatedCostToHotel(), hasText("5.00"));

        TotalEventAwardItem totalPoints = totalEventAwardFields.getTotalPoints();
        verifyThat(totalPoints.getEstimatedPointEarning(), hasText("990"));
        verifyThat(totalPoints.getEstimatedCostToHotel(), hasText("7.32"));
    }
}
