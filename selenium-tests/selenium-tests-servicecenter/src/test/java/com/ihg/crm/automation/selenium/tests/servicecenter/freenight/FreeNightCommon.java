package com.ihg.crm.automation.selenium.tests.servicecenter.freenight;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.isDisplayedWithWait;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;

import com.ihg.automation.selenium.common.freenight.Redemption;
import com.ihg.automation.selenium.servicecenter.pages.event.freenight.BookFreeNightDetailsPopUp;
import com.ihg.automation.selenium.servicecenter.pages.event.freenight.FreeNightEventPage;
import com.ihg.automation.selenium.servicecenter.pages.event.freenight.FreeNightGridRow;
import com.ihg.automation.selenium.servicecenter.pages.event.offer.MemberOfferGridRow;
import com.ihg.automation.selenium.servicecenter.pages.event.offer.MemberOfferGridRow.MemberOfferCell;
import com.ihg.automation.selenium.servicecenter.pages.event.offer.OfferEventPage;
import com.ihg.crm.automation.selenium.tests.servicecenter.LoginLogout;

public class FreeNightCommon extends LoginLogout
{

    private FreeNightEventPage freeNightPage = new FreeNightEventPage();
    private OfferEventPage offerPage = new OfferEventPage();

    protected void bookFreeNights(String offerCode, Redemption redemption)
    {
        freeNightPage.goTo();
        FreeNightGridRow offerRow = freeNightPage.getFreeNightGrid().getRowByOfferCode(offerCode);
        offerRow.getFreeNightDetails().clickBookFreeNightButton();
        new BookFreeNightDetailsPopUp().create(redemption);
    }

    protected void verifyOfferWinAmount(String offerCode, String winAmount)
    {
        offerPage.goTo();
        MemberOfferGridRow offerRow = offerPage.getMemberOffersGrid().getRowByOfferCode(offerCode);
        verifyThat(offerRow, isDisplayedWithWait());
        verifyThat(offerRow.getCell(MemberOfferCell.WINS), hasText(winAmount));
    }
}
