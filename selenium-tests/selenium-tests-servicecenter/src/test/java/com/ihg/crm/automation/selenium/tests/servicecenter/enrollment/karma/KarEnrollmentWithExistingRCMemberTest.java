package com.ihg.crm.automation.selenium.tests.servicecenter.enrollment.karma;

import static com.ihg.automation.common.DateUtils.dd_MMM_YYYY;
import static com.ihg.automation.common.DateUtils.getFormattedDate;
import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.assertNoError;
import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyError;
import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyNoError;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.Constant.NOT_AVAILABLE;
import static com.ihg.automation.selenium.common.LeftPanelBase.GREY_COLOR;
import static com.ihg.automation.selenium.common.types.program.Program.KAR;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasDefault;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static com.ihg.automation.selenium.matchers.component.ViewableMatcher.hasTextInView;
import static com.ihg.automation.selenium.servicecenter.pages.Messages.HIGHLIGHTED_FIELDS_DID_NOT_PASS_VALIDATION;
import static com.ihg.automation.selenium.servicecenter.pages.history.AuditConstant.ProgramInformation.KARMA_MEMBER_SINCE_DATE;
import static com.ihg.automation.selenium.servicecenter.pages.history.AuditConstant.ProgramInformation.PROGRAM_INFORMATION;
import static org.hamcrest.core.StringContains.containsString;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.common.DateUtils;
import com.ihg.automation.selenium.common.event.AllEventsRowFactory;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.common.types.program.ProgramStatus;
import com.ihg.automation.selenium.matchers.component.HasBackgroundColor;
import com.ihg.automation.selenium.servicecenter.pages.LeftPanel;
import com.ihg.automation.selenium.servicecenter.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsGridRow;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsPage;
import com.ihg.automation.selenium.servicecenter.pages.event.enroll.MembershipDetailsPopUp;
import com.ihg.automation.selenium.servicecenter.pages.event.enroll.MembershipGridRowContainer;
import com.ihg.automation.selenium.servicecenter.pages.history.ProfileHistoryGridRow;
import com.ihg.automation.selenium.servicecenter.pages.history.ProfileHistoryPage;
import com.ihg.automation.selenium.servicecenter.pages.programs.KarmaPage;
import com.ihg.automation.selenium.servicecenter.pages.programs.ProgramPageBase;
import com.ihg.automation.selenium.servicecenter.pages.programs.ProgramSummary;
import com.ihg.crm.automation.selenium.tests.servicecenter.LoginLogout;

@Test(groups = { "karma" })
public class KarEnrollmentWithExistingRCMemberTest extends LoginLogout
{
    private Member member = new Member();
    EnrollmentPage enrollmentPage = new EnrollmentPage();
    private KarmaPage karmaPage;
    private MembershipGridRowContainer membershipGridRowContainer;

    @BeforeClass
    public void beforeClass()
    {
        member.addProgram(Program.RC);
        member.addAddress(MemberPopulateHelper.getUnitedStatesAddress());
        member.setPersonalInfo(MemberPopulateHelper.getSimplePersonalInfo());

        login();

        enrollmentPage.enroll(member);
    }

    @Test(priority = 10)
    public void tryToEnrollToKarmaWithoutMandatoryFields()
    {
        enrollmentPage.goTo();
        enrollmentPage.getPrograms().selectProgram(Program.KAR);
        enrollmentPage.getCustomerInfo().getName().getSurname().clear();
        enrollmentPage.clickSubmit(verifyError(HIGHLIGHTED_FIELDS_DID_NOT_PASS_VALIDATION));
    }

    @Test(priority = 15)
    public void enrollToKarma()
    {
        enrollmentPage.getCustomerInfo().getName().getSurname().type(member.getPersonalInfo().getName().getSurname());
        enrollmentPage.clickSubmitAndSkipWarnings(assertNoError());
        enrollmentPage.successEnrollmentPopUp(member);
    }

    @Test(priority = 16)
    public void verifyHistoryProgramInfoAfterEnrollToKarma()
    {
        ProfileHistoryPage historyPage = new ProfileHistoryPage();
        historyPage.goTo();
        verifyNoError();

        ProfileHistoryGridRow programInfoRow = historyPage.getProfileHistoryGrid().getRow(1)
                .getSubRowByName(PROGRAM_INFORMATION);

        programInfoRow.getSubRowByName(KARMA_MEMBER_SINCE_DATE).verifyAddAction(getFormattedDate(dd_MMM_YYYY));
    }

    @Test(priority = 20)
    public void verifyKarmaPage()
    {
        karmaPage = new KarmaPage();
        karmaPage.goTo();

        ProgramSummary summary = karmaPage.getSummary();
        verifyThat(summary.getStatus(), hasTextInView(ProgramStatus.OPEN));
        verifyThat(summary.getMemberId(), hasText(member.getKARProgramId()));
        verifyThat(summary, hasText(containsString("No flags selected")));

        ProgramPageBase.EnrollmentDetails enrollDetails = karmaPage.getEnrollmentDetails();

        enrollDetails.verifyScSource(helper);
        verifyThat(enrollDetails.getPaymentDetails().getMethod(), hasDefault());
        verifyThat(enrollDetails.getReferringMember(), hasText(NOT_AVAILABLE));
        verifyThat(enrollDetails.getRenewalDate(), hasDefault());

        String currentDate = DateUtils.getFormattedDate();
        verifyThat(enrollDetails.getEnrollDate(), hasText(currentDate));
        verifyThat(enrollDetails.getMemberSince(), hasText(currentDate));
    }

    @Test(priority = 30)
    public void openAllEventsEnrollKarEventDetails()
    {
        AllEventsPage allEventsPage = new AllEventsPage();
        allEventsPage.goTo();

        AllEventsGridRow allEventsGridRow = allEventsPage.getGrid().getEnrollRow(Program.KAR);
        allEventsGridRow.verify(AllEventsRowFactory.getEnrollEvent(KAR));

        membershipGridRowContainer = allEventsGridRow.expand(MembershipGridRowContainer.class);
        membershipGridRowContainer.getMembershipDetails().verifyScSource(helper);
    }

    @Test(priority = 40)
    public void verifyEnrollmentAllEventsMembershipDetailsTab()
    {
        membershipGridRowContainer.getDetails().clickAndWait();
        MembershipDetailsPopUp membershipDetailsPopUp = new MembershipDetailsPopUp();
        membershipDetailsPopUp.getMembershipDetailsTab().getMembershipDetails().verifyScSource(helper);
        membershipDetailsPopUp.clickClose();
    }

    @Test(priority = 50)
    public void verifyProgramInformationPanelLevel()
    {
        verifyThat(new LeftPanel().getProgramInfoPanel().getPrograms().getProgram(KAR),
                HasBackgroundColor.hasBackgroundColor(GREY_COLOR));
    }
}
