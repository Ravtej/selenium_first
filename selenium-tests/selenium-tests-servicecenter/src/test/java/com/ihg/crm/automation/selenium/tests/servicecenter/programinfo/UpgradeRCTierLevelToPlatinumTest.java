package com.ihg.crm.automation.selenium.tests.servicecenter.programinfo;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.Constant.NOT_AVAILABLE;
import static com.ihg.automation.selenium.common.event.EventTransactionType.TierLevel.UPGRADE_MANUAL;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.isDisplayedWithWait;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static com.ihg.automation.selenium.matchers.component.ViewableMatcher.hasTextInView;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.startsWith;

import java.util.Arrays;
import java.util.List;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.common.DateUtils;
import com.ihg.automation.selenium.common.CustomerInfoFlags;
import com.ihg.automation.selenium.common.event.AllEventsRow;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.common.types.program.RewardClubLevel;
import com.ihg.automation.selenium.common.types.program.RewardClubLevelReason;
import com.ihg.automation.selenium.common.types.program.TierLevelFlag;
import com.ihg.automation.selenium.gwt.components.DualList;
import com.ihg.automation.selenium.gwt.components.input.Select;
import com.ihg.automation.selenium.matchers.component.HasDualListItems;
import com.ihg.automation.selenium.servicecenter.pages.LeftPanel;
import com.ihg.automation.selenium.servicecenter.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsGridRow;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsPage;
import com.ihg.automation.selenium.servicecenter.pages.event.level.ManualTierLevelChangeGridRowContainer;
import com.ihg.automation.selenium.servicecenter.pages.programs.ProgramSummary;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.RewardClubPage;
import com.ihg.crm.automation.selenium.tests.servicecenter.LoginLogout;

public class UpgradeRCTierLevelToPlatinumTest extends LoginLogout
{
    private List<String> expectedBoldContent = Arrays.asList(TierLevelFlag.AMERICA_AGENCY_CONTACTS.getValue(),
            TierLevelFlag.AMERICA_ALLIANCES_PARTNERS.getValue(), TierLevelFlag.AMERICA_FRIENDS_OF_MANAGEMENT.getValue(),
            TierLevelFlag.AMERICA_RELATIONSHIP_MARKETING.getValue(), TierLevelFlag.AMERICA_VIP_CUSTOMER.getValue(),
            TierLevelFlag.AMEX_CENTURION.getValue(), TierLevelFlag.ASIA_PACIFIC_ALLIANCES_PARTNERS.getValue(),
            TierLevelFlag.ASIA_PACIFIC_RELATIONSHIP_MARKETING.getValue(),
            TierLevelFlag.EMEA_ALLIANCES_PARTNERS.getValue(), TierLevelFlag.EMEA_BRAND_CONTACTS.getValue(),
            TierLevelFlag.EMEA_RELATIONSHIP_MARKETING.getValue(), TierLevelFlag.GENERAL_VIP.getValue(),
            TierLevelFlag.RELATIONSHIP_MARKETING_DEVELOPMENT_CONTACTS.getValue(), TierLevelFlag.QASP.getValue());
    private Member member = new Member();

    private RewardClubPage rewardClubPage = new RewardClubPage();
    private AllEventsPage allEventsPage = new AllEventsPage();
    private AllEventsRow upgradePlatinumEventRow;
    private ProgramSummary summary;

    @BeforeClass
    public void beforeClass()
    {
        member.setPersonalInfo(MemberPopulateHelper.getSimplePersonalInfo());
        member.addAddress(MemberPopulateHelper.getUnitedStatesAddress());
        member.addProgram(Program.RC);

        login();

        member = new EnrollmentPage().enroll(member);
        upgradePlatinumEventRow = new AllEventsRow(DateUtils.getFormattedDate(), UPGRADE_MANUAL, "RC - PLATINUM", "",
                "");
    }

    @Test
    public void navigateSummaryPage()
    {
        rewardClubPage.goTo();
        summary = rewardClubPage.getSummary();
        summary.clickEdit();
    }

    @Test(dependsOnMethods = { "navigateSummaryPage" }, alwaysRun = true)
    public void verifyVIPTierLevelReason()
    {
        verifyThat(summary.getTierLevel(), isDisplayedWithWait());
        summary.getTierLevel().select(RewardClubLevel.PLTN.getValue());

        Select tierLevelReason = summary.getTierLevelReason();
        verifyThat(tierLevelReason, isDisplayedWithWait());
        tierLevelReason.selectByValue(RewardClubLevelReason.VIP);
        summary.getTierLevelExpiration().select("Expire Next Year");
        summary.clickSave();
        // verifyThat(summary.getFlags().getRightList(),
        // isHighlightedAsInvalid(true));
    }

    @Test(dependsOnMethods = { "verifyVIPTierLevelReason" }, alwaysRun = true)
    public void verifyBoldTierLevelFlags()
    {
        verifyThat(summary.getFlags().getLeftList(), HasDualListItems.hasBoldItems(expectedBoldContent));
    }

    @Test(dependsOnMethods = { "verifyVIPTierLevelReason" }, alwaysRun = true)
    public void verifyNotBoldTierLevelFlag()
    {
        DualList lst = summary.getFlags();
        lst.select(TierLevelFlag.MAJOR_LEAGUE_BASEBALL.getValue());
        summary.clickSave();
        // verifyThat(lst.getRightList(), isHighlightedAsInvalid(true));
    }

    @Test(dependsOnMethods = { "verifyNotBoldTierLevelFlag" }, alwaysRun = true)
    public void verifyBoldTierLevelFlag()
    {
        DualList lst = summary.getFlags();
        lst.select(TierLevelFlag.GENERAL_VIP.getValue());
        summary.clickSave();
        verifyThat(lst, displayed(false));
    }

    @Test(dependsOnMethods = { "verifyBoldTierLevelFlag" }, alwaysRun = true)
    public void verifyResultingTierLevel()
    {
        rewardClubPage = new RewardClubPage();
        summary = rewardClubPage.getSummary();
        verifyThat(summary.getTierLevel(), hasTextInView(startsWith(RewardClubLevel.PLTN.getValue())));
        verifyThat(summary.getTierLevelReason(), hasTextInView(RewardClubLevelReason.VIP.getValue()));
        verifyThat(summary, hasText(containsString(TierLevelFlag.MAJOR_LEAGUE_BASEBALL.getValue())));
        verifyThat(summary, hasText(containsString(TierLevelFlag.GENERAL_VIP.getValue())));
        CustomerInfoFlags flags = new LeftPanel().getCustomerInfoPanel().getFlags();
        verifyThat(flags, hasText(containsString(TierLevelFlag.MAJOR_LEAGUE_BASEBALL.getValue())));
        verifyThat(flags, hasText(containsString(TierLevelFlag.GENERAL_VIP.getValue())));
    }

    @Test(dependsOnMethods = { "verifyResultingTierLevel" }, alwaysRun = true)
    public void verifyAllEventsGridRow()
    {
        allEventsPage.goTo();
        AllEventsGridRow row = allEventsPage.getGrid().getRow(upgradePlatinumEventRow.getTransType(),
                upgradePlatinumEventRow.getDetail());
        row.verify(upgradePlatinumEventRow);
    }

    @Test(dependsOnMethods = { "verifyAllEventsGridRow" }, alwaysRun = true)
    public void verifyAllEventsGridRowDetails()
    {
        allEventsPage.goTo();
        AllEventsGridRow row = allEventsPage.getGrid().getRow(upgradePlatinumEventRow.getTransType(),
                upgradePlatinumEventRow.getDetail());
        ManualTierLevelChangeGridRowContainer container = row.expand(ManualTierLevelChangeGridRowContainer.class);
        container.verify(NOT_AVAILABLE, RewardClubLevelReason.VIP.getValue(), NOT_AVAILABLE, helper);
    }

}
