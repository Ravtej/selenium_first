package com.ihg.crm.automation.selenium.tests.servicecenter.programinfo;

import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyNoError;
import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifySuccess;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.event.EventTransactionType.Goodwill.GOODWILL;
import static com.ihg.automation.selenium.common.types.program.ProgramStatusReason.CUSTOMER_REQUEST;
import static com.ihg.automation.selenium.common.types.program.ProgramStatusReason.DUPLICATE;
import static com.ihg.automation.selenium.common.types.program.ProgramStatusReason.FROZEN;
import static com.ihg.automation.selenium.common.types.program.ProgramStatusReason.IHG_REQUEST;
import static com.ihg.automation.selenium.common.types.program.ProgramStatusReason.MUTUALLY_EXCLUSIVE;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasTextAsInt;
import static com.ihg.automation.selenium.matchers.component.GridSize.size;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static com.ihg.automation.selenium.matchers.component.ViewableMatcher.hasTextInView;
import static com.ihg.automation.selenium.servicecenter.pages.programs.ProgramPageBase.BALANCE_MESSAGE;
import static org.hamcrest.Matchers.greaterThan;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.event.AllEventsRowFactory;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.gwt.components.Component;
import com.ihg.automation.selenium.gwt.components.dialog.ConfirmDialog;
import com.ihg.automation.selenium.servicecenter.pages.LeftPanel;
import com.ihg.automation.selenium.servicecenter.pages.account.AccountInfoPage;
import com.ihg.automation.selenium.servicecenter.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsGrid;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsPage;
import com.ihg.automation.selenium.servicecenter.pages.programs.ProgramPageBase;
import com.ihg.automation.selenium.servicecenter.pages.programs.ProgramSummary;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.RewardClubPage;
import com.ihg.automation.selenium.servicecenter.pages.unit.GoodwillPopUp;
import com.ihg.crm.automation.selenium.tests.servicecenter.LoginLogout;

public class ClosedMemberWithReasonsTest extends LoginLogout
{

    private static final String FAVOURABLE_BALANCE = "5,000";
    private static final String ZERO_BALANCE = "0";

    private String balanceAmount;

    // since we cannot differentiate between negative and positive Goodwill
    // events this count will get great total and compare it with the final
    // result after filtering goodwill events on All Events Tab
    private int goodwillEventsCount = 0;

    public Member member = new Member();

    private RewardClubPage rewardClubPage = new RewardClubPage();
    private ProgramSummary rcSummary;

    @BeforeClass
    public void beforeClass()
    {
        member.setPersonalInfo(MemberPopulateHelper.getSimplePersonalInfo());
        member.addAddress(MemberPopulateHelper.getUnitedStatesAddress());
        member.addProgram(Program.RC);

        login();

        member = new EnrollmentPage().enroll(member);
        makePositiveGoodwillEvent();

        rewardClubPage.goTo();
    }

    @Test(dataProvider = "leaveBalanceReasonDataProvider", priority = 10)
    public void closeMemberAndVerifyBalanceLeft(String statusReason)
    {
        rcSummary = rewardClubPage.getSummary();
        rcSummary.setClosedStatus(statusReason, verifySuccess(ProgramPageBase.SUCCESS_MESSAGE));

        verifyReasonAndBalance(statusReason, FAVOURABLE_BALANCE);
    }

    @Test(dataProvider = "reduceBalanceReasonDataProvider", priority = 20)
    public void closeMemberAndVerifyBalanceReduce(String statusReason)
    {
        rcSummary.setOpenStatus();

        makePositiveGoodwillEvent();

        rewardClubPage.goTo();

        rcSummary = rewardClubPage.getSummary();
        rcSummary.setClosedStatus(statusReason);

        ConfirmDialog confirmDialog = new ConfirmDialog();
        verifyThat(confirmDialog, hasText(BALANCE_MESSAGE));
        confirmDialog.clickYes(verifyNoError());

        verifyReasonAndBalance(statusReason, ZERO_BALANCE);
        goodwillEventsCount++;

        verifyGoodwillEventReduce();

        rewardClubPage.goTo();
    }

    @Test(priority = 30)
    public void removeMemberAccount()
    {
        rcSummary.setOpenStatus();

        makePositiveGoodwillEvent();

        AccountInfoPage accountInfoPage = new AccountInfoPage();
        accountInfoPage.goTo();
        accountInfoPage.removedAccount();

        new LeftPanel().verifyBalance(ZERO_BALANCE);
        goodwillEventsCount++;

        verifyGoodwillEventReduce();
    }

    private void verifyGoodwillEventReduce()
    {
        AllEventsPage allEventsPage = new AllEventsPage();
        allEventsPage.goTo();

        allEventsPage.searchByEventType("Goodwill");

        AllEventsGrid allEventsGrid = allEventsPage.getGrid();
        verifyThat(allEventsGrid, size(goodwillEventsCount));

        allEventsGrid.getRow(GOODWILL).verify(AllEventsRowFactory.getGoodwillEvent("-" + balanceAmount));
    }

    private void makePositiveGoodwillEvent()
    {
        new GoodwillPopUp().goodwillPoints(FAVOURABLE_BALANCE);
        Component pointsBalance = new LeftPanel().getProgramInfoPanel().getPrograms().getRewardClubProgram()
                .getPointsBalance();
        verifyThat(pointsBalance, hasTextAsInt(greaterThan(0)));
        balanceAmount = pointsBalance.getText();
        goodwillEventsCount++;
    }

    private void verifyReasonAndBalance(String statusReason, String pointsBalance)
    {
        verifyThat(rcSummary.getUnitsBalance(), hasText(pointsBalance));
        verifyThat(rcSummary.getStatusReason(), hasTextInView(statusReason));
        new LeftPanel().verifyBalance(pointsBalance);
    }

    @DataProvider(name = "leaveBalanceReasonDataProvider")
    public Object[][] leaveBalanceProvider()
    {
        return new Object[][] { { FROZEN }, { IHG_REQUEST }, { MUTUALLY_EXCLUSIVE } };
    }

    @DataProvider(name = "reduceBalanceReasonDataProvider")
    public Object[][] reduceBalanceProvider()
    {
        return new Object[][] { { CUSTOMER_REQUEST }, { DUPLICATE } };
    }
}
