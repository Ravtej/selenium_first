package com.ihg.crm.automation.selenium.tests.servicecenter.enrollment;

import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyNoError;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.Constant.RC_POINTS;
import static com.ihg.automation.selenium.common.event.AllEventsRowFactory.getEnrollEvent;
import static com.ihg.automation.selenium.common.event.AllEventsRowFactory.getOfferRegistrationEvent;
import static com.ihg.automation.selenium.common.event.EventTransactionType.Membership.ENROLL;
import static com.ihg.automation.selenium.common.event.EventTransactionType.Offer.OFFER_REGISTRATION;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.CustomerInfoPanel;
import com.ihg.automation.selenium.common.billing.EventBillingDetailsTab;
import com.ihg.automation.selenium.common.earning.EventEarningDetailsTab;
import com.ihg.automation.selenium.common.enroll.SuccessEnrolmentPopUp;
import com.ihg.automation.selenium.common.event.EarningEvent;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberJdbcUtils;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.offer.OfferFactory;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.gwt.components.Mode;
import com.ihg.automation.selenium.servicecenter.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsGrid;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsGridRow;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsPage;
import com.ihg.automation.selenium.servicecenter.pages.event.all.OfferGridRowContainer;
import com.ihg.automation.selenium.servicecenter.pages.event.enroll.MembershipDetailsPopUp;
import com.ihg.automation.selenium.servicecenter.pages.event.enroll.MembershipGridRowContainer;
import com.ihg.automation.selenium.servicecenter.pages.personal.PersonalInfoPage;
import com.ihg.crm.automation.selenium.tests.servicecenter.LoginLogout;

public class EnrollmentWithPrePrintedMembershipIdTest extends LoginLogout
{
    private Member member = new Member();
    private EnrollmentPage enrollmentPage = new EnrollmentPage();
    private EarningEvent event = new EarningEvent("PC", "0", "0", RC_POINTS);
    private static String memberId;
    private AllEventsGridRow enrollRow;
    private AllEventsGridRow offerRow;
    private MembershipGridRowContainer rowCont;
    private MembershipDetailsPopUp popUp;
    private AllEventsPage allEvents;
    private static final String MEMBER_ID = "SELECT MBRSHP_ID"//
            + " FROM DGST.AVAIL_MBRSHP_ID "//
            + " WHERE STAT_CD = 'A'"//
            + " AND ROWNUM = 1 ";

    @BeforeClass
    public void beforeClass()
    {
        member.addProgram(Program.RC);
        member.addAddress(MemberPopulateHelper.getUnitedStatesAddress());
        member.setPersonalInfo(MemberPopulateHelper.getSimplePersonalInfo());

        memberId = MemberJdbcUtils.getMemberId(jdbcTemplate, String.format(MEMBER_ID));

        login();
    }

    @Test
    public void createEnrollmentToRC()
    {
        enrollmentPage.goTo();
        enrollmentPage.populate(member);
        enrollmentPage.getAdditionalInformation().getRCMemberID().type(memberId);
        enrollmentPage.clickSubmit(verifyNoError());

        SuccessEnrolmentPopUp popUp = new SuccessEnrolmentPopUp();
        member = popUp.putPrograms(member);
        verifyThat(popUp.getMemberId(Program.RC), displayed(true));
        popUp.clickDone(verifyNoError());
    }

    @Test(dependsOnMethods = { "createEnrollmentToRC" })
    public void validatePersonalInformation()
    {
        PersonalInfoPage persInfo = new PersonalInfoPage();
        persInfo.goTo();
        persInfo.getCustomerInfo().verify(member, Mode.VIEW);
        persInfo.getAddressList().getExpandedContact().verify(member.getPreferredAddress(), Mode.VIEW);
    }

    @Test(dependsOnMethods = { "createEnrollmentToRC" })
    public void verifyCustomerInformationPanel()
    {
        CustomerInfoPanel custPanel = new CustomerInfoPanel();
        verifyThat(custPanel.getMemberNumber(Program.RC), hasText(memberId));
    }

    @Test(dependsOnMethods = { "createEnrollmentToRC" })
    public void verifyEnrollmentAllEventsGrid()
    {
        allEvents = new AllEventsPage();
        allEvents.goTo();

        AllEventsGrid allEventsGrid = allEvents.getGrid();
        allEventsGrid.getEnrollRow(Program.RC).verify(getEnrollEvent(Program.RC));
        allEventsGrid.getRow(OFFER_REGISTRATION)
                .verify(getOfferRegistrationEvent(OfferFactory.DELAYED_FULFILMENT_GLOBAL));
    }

    @Test(dependsOnMethods = { "createEnrollmentToRC", "verifyEnrollmentAllEventsGrid" })
    public void verifyEnrollmentDetails()
    {
        allEvents = new AllEventsPage();
        allEvents.goTo();
        enrollRow = allEvents.getGrid().getEnrollRow(Program.RC);
        rowCont = enrollRow.expand(MembershipGridRowContainer.class);
        rowCont.getMembershipDetails().verifyScSource(helper);
    }

    @Test(dependsOnMethods = { "verifyEnrollmentDetails" })
    public void verifyMembershipDetailsTab()
    {
        rowCont.clickDetails();
        popUp = new MembershipDetailsPopUp();
        popUp.getMembershipDetailsTab().getMembershipDetails().verifyScSource(helper);
    }

    @Test(dependsOnMethods = { "verifyMembershipDetailsTab" })
    public void verifyEarningDetailsTab()
    {
        popUp = new MembershipDetailsPopUp();
        EventEarningDetailsTab earningDetailsTab = popUp.getEarningDetailsTab();
        earningDetailsTab.goTo();
        earningDetailsTab.verifyBasicDetailsNoPointsAndMiles();
        earningDetailsTab.getGrid().verify(event);
    }

    @Test(dependsOnMethods = { "verifyEarningDetailsTab" })
    public void verifyBillingDetailsTab()
    {
        popUp = new MembershipDetailsPopUp();
        EventBillingDetailsTab billingDetailsTab = popUp.getBillingDetailsTab();
        billingDetailsTab.goTo();
        billingDetailsTab.verifyTabIsEmpty(ENROLL);
        popUp.clickClose();
    }

    @Test(dependsOnMethods = { "verifyBillingDetailsTab" })
    public void verifyOfferDetails()
    {
        offerRow = allEvents.getGrid().getRow(OFFER_REGISTRATION);
        OfferGridRowContainer offerContainer = offerRow.expand(OfferGridRowContainer.class);
        offerContainer.getOfferDetails().verify(OfferFactory.DELAYED_FULFILMENT_GLOBAL, helper);
    }
}
