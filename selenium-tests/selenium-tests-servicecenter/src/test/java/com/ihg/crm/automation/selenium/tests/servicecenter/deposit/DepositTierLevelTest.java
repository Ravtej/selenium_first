package com.ihg.crm.automation.selenium.tests.servicecenter.deposit;

import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyError;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.event.EventTransactionType.Deposit.DEPOSIT;
import static com.ihg.automation.selenium.common.event.EventTransactionType.Order.ORDER_SYSTEM;
import static com.ihg.automation.selenium.common.types.program.ProgramStatus.OPEN;
import static com.ihg.automation.selenium.common.types.program.RewardClubLevel.GOLD;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.enabled;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.isDisplayedWithWait;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static org.hamcrest.Matchers.containsString;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.google.common.collect.ImmutableList;
import com.ihg.automation.selenium.common.Constant;
import com.ihg.automation.selenium.common.billing.EventBillingDetailsTab;
import com.ihg.automation.selenium.common.catalog.CatalogItem;
import com.ihg.automation.selenium.common.deposit.Deposit;
import com.ihg.automation.selenium.common.earning.EventEarningDetailsTab;
import com.ihg.automation.selenium.common.earning.EventEarningGridRow;
import com.ihg.automation.selenium.common.earning.EventEarningGridRow.EventEarningCell;
import com.ihg.automation.selenium.common.event.AllEventsRow;
import com.ihg.automation.selenium.common.event.AllEventsRowConverter;
import com.ihg.automation.selenium.common.event.EarningEvent;
import com.ihg.automation.selenium.common.event.EarningEventFactory;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.order.Order;
import com.ihg.automation.selenium.common.order.OrderFactory;
import com.ihg.automation.selenium.common.order.OrderItem;
import com.ihg.automation.selenium.common.pages.PaymentMethod;
import com.ihg.automation.selenium.common.payment.PaymentDetailsPopUp;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.common.types.program.RewardClubLevel;
import com.ihg.automation.selenium.servicecenter.pages.LeftPanel;
import com.ihg.automation.selenium.servicecenter.pages.annual.AnnualActivityPage;
import com.ihg.automation.selenium.servicecenter.pages.catalog.CatalogItemDetailsPopUp;
import com.ihg.automation.selenium.servicecenter.pages.deposit.CreateDepositPopUp;
import com.ihg.automation.selenium.servicecenter.pages.deposit.DepositDetailsPopUp;
import com.ihg.automation.selenium.servicecenter.pages.deposit.DepositDetailsTab;
import com.ihg.automation.selenium.servicecenter.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsGridRow;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsPage;
import com.ihg.automation.selenium.servicecenter.pages.event.deposit.DepositGridRowContainer;
import com.ihg.automation.selenium.servicecenter.pages.event.order.OrderDetailsPopUp;
import com.ihg.automation.selenium.servicecenter.pages.event.order.OrderDetailsTab;
import com.ihg.automation.selenium.servicecenter.pages.event.order.OrderSystemDetailsGridRowContainer;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.RewardClubPage;
import com.ihg.automation.selenium.servicecenter.pages.unit.PurchaseTierLevelPopUp;
import com.ihg.crm.automation.selenium.tests.servicecenter.LoginLogout;

public class DepositTierLevelTest extends LoginLogout
{
    private Member member = new Member();
    private Deposit testDeposit = new Deposit();
    private Order order;

    private DepositDetailsPopUp depositDetailsPopUp = new DepositDetailsPopUp();
    private CatalogItemDetailsPopUp catItemDetailPopUp = new CatalogItemDetailsPopUp();
    private OrderDetailsPopUp orderSysDetailsPopUp = new OrderDetailsPopUp();
    private OrderSystemDetailsGridRowContainer orderSystGridRowContainer;
    private DepositGridRowContainer depositGridRowContainer;

    private AllEventsRow orderSystemEvent;

    private final static CatalogItem CATALOG_ITEM = CatalogItem.builder("IHG02", "IHG Rewards Club - GOLD - 2013")
            .vendor("FISERV").startDate("07May13").endDate("31Dec99").status("Active").orderLimit("1")
            .loyaltyUnitCost("0").regions(ImmutableList.of("UNITED STATES", "CANADA", "MSAC", "EMEA", "ASIA PACIFIC"))
            .countries(ImmutableList.of("All")).tierLevels(ImmutableList.of("All")).build();

    private EarningEvent baseAwardsEarningEvent = EarningEventFactory.getAwardEvent(CATALOG_ITEM);
    private RewardClubPage rewardClubPage;

    @BeforeClass
    public void beforeClass()
    {
        member.setPersonalInfo(MemberPopulateHelper.getSimplePersonalInfo());
        member.addAddress(MemberPopulateHelper.getUnitedStatesAddress());
        member.addProgram(Program.RC);

        testDeposit.setTransactionId("IHGGLD");
        testDeposit.setTransactionType(DEPOSIT);
        testDeposit.setTransactionName("IHG Rewards Club - GOLD - 2013");
        testDeposit.setTransactionDescription("IHG Rewards Club - GOLD - 2013");
        testDeposit.setHotel("ATLCP");
        testDeposit.setCatalogItem("IHG02");
        testDeposit.setTierLevel(GOLD.getCode());

        login();

        order = OrderFactory.getSystemOrder(CATALOG_ITEM, member, helper);
        orderSystemEvent = AllEventsRowConverter.convert(order);

        member = new AnnualActivityPage().captureCounters(new EnrollmentPage().enroll(member), jdbcTemplate);
    }

    @Test
    public void verifyCreateDepositDialog()
    {
        CreateDepositPopUp createDepositPopUp = new CreateDepositPopUp();

        createDepositPopUp.goTo();

        createDepositPopUp.getTransactionId().typeAndWait(testDeposit.getTransactionId());
        verifyThat(createDepositPopUp.getLoyaltyUnitsInput(), enabled(false));
        verifyThat(createDepositPopUp.getLoyaltyUnitsSelect(), enabled(false));
        verifyThat(createDepositPopUp.getPartnerTransactionDate(), enabled(false));
        verifyThat(createDepositPopUp.getOrderTrackingNumber(), enabled(false));
        verifyThat(createDepositPopUp.getPartnerComment(), enabled(false));
        verifyThat(createDepositPopUp.getReason(), displayed(false));
        verifyThat(createDepositPopUp.getCustomerServiceComment(), displayed(false));

        createDepositPopUp.verifyTransactionInfo(testDeposit);
        verifyThat(createDepositPopUp.getCatalogItem(), hasText(testDeposit.getCatalogItem()));
        verifyThat(createDepositPopUp.getTierLevel(), hasText(testDeposit.getTierLevel()));

        createDepositPopUp.clickClose();
    }

    @Test(dependsOnMethods = { "verifyCreateDepositDialog" }, alwaysRun = true)
    public void createDeposit()
    {
        new CreateDepositPopUp().createDeposit(testDeposit);
    }

    @Test(dependsOnMethods = { "createDeposit" })
    public void verifyTierLevel()
    {
        rewardClubPage = new RewardClubPage();
        rewardClubPage.goTo();
        rewardClubPage.getSummary().verify(OPEN, GOLD, 1);
    }

    @Test(dependsOnMethods = { "verifyTierLevel" }, alwaysRun = true)
    public void tryToPurchaseGold()
    {
        rewardClubPage.getSummary().clickPurchase();

        PurchaseTierLevelPopUp purchaseTierLevelPopUp = new PurchaseTierLevelPopUp();
        purchaseTierLevelPopUp.setLevel(Program.RC, RewardClubLevel.GOLD.getCode());
        verifyThat(purchaseTierLevelPopUp, displayed(false));

        PaymentDetailsPopUp paymentDetailsPopUp = new PaymentDetailsPopUp();
        verifyThat(paymentDetailsPopUp, isDisplayedWithWait());
        paymentDetailsPopUp.selectPaymentType(PaymentMethod.CASH);

        paymentDetailsPopUp
                .clickSubmit(verifyError(hasText(containsString("Attempt to set lower tier level expiration date"))));

        verifyThat(paymentDetailsPopUp, displayed(true));

        paymentDetailsPopUp.clickClose();
    }

    @Test(dependsOnMethods = { "tryToPurchaseGold" })
    public void verifyDepositEventWithDetailsInGrid()
    {
        AllEventsPage allEventsPage = new AllEventsPage();
        allEventsPage.goTo();
        AllEventsGridRow row = allEventsPage.getGrid().getRow(testDeposit.getTransactionType());
        row.verify(AllEventsRowConverter.convert(testDeposit));
        depositGridRowContainer = row.expand(DepositGridRowContainer.class);
        depositGridRowContainer.verify(testDeposit, helper);
    }

    @Test(dependsOnMethods = { "verifyDepositEventWithDetailsInGrid" }, alwaysRun = true)
    public void openAllEventsDepositEventDetailsPopUp()
    {
        depositGridRowContainer.clickDetails();
    }

    @Test(dependsOnMethods = { "openAllEventsDepositEventDetailsPopUp" })
    public void verifyDepositEventDetailsPopUpTab()
    {
        DepositDetailsTab depositDetailsTab = depositDetailsPopUp.getDepositDetailsTab();
        depositDetailsTab.goTo();
        depositDetailsTab.getDepositDetails().verify(testDeposit);
    }

    @Test(dependsOnMethods = { "verifyDepositEventDetailsPopUpTab" })
    public void verifyDepositEventEarningDetailsPopUpTab()
    {
        EventEarningDetailsTab tab = depositDetailsPopUp.getEarningDetailsTab();
        tab.goTo();
        tab.verifyBasicDetailsNoPointsAndMiles();
        tab.getGrid().verifyRowsFoundByType(EarningEventFactory.getBaseUnitsEvent("0", "0"), baseAwardsEarningEvent);
    }

    @Test(dependsOnMethods = { "verifyDepositEventEarningDetailsPopUpTab" }, alwaysRun = true)
    public void verifyCatalogItemDetails()
    {
        EventEarningDetailsTab tab = depositDetailsPopUp.getEarningDetailsTab();
        tab.goTo();
        EventEarningGridRow row = tab.getGrid().getRowByType(Constant.BASE_AWARDS);
        row.getCell(EventEarningCell.AWARD).clickByLabel();
        catItemDetailPopUp = new CatalogItemDetailsPopUp();
        verifyThat(catItemDetailPopUp, displayed(true));
        catItemDetailPopUp.verify(CATALOG_ITEM);
    }

    @Test(dependsOnMethods = { "verifyCatalogItemDetails" }, alwaysRun = true)
    public void closeCatalogItemDetailsPopUp()
    {
        catItemDetailPopUp.close();
    }

    @Test(dependsOnMethods = { "closeCatalogItemDetailsPopUp" }, alwaysRun = true)
    public void verifyDepositEventBillingDetailsPopUpTab()
    {
        EventBillingDetailsTab tab = depositDetailsPopUp.getBillingDetailsTab();
        tab.goTo();
        tab.verifyTabIsEmpty(testDeposit.getTransactionType());
    }

    @Test(dependsOnMethods = { "verifyDepositEventBillingDetailsPopUpTab", "verifyDepositEventEarningDetailsPopUpTab",
            "verifyDepositEventDetailsPopUpTab" }, alwaysRun = true)
    public void closeAllEventsCoPartnersDepositDetailsPopUp()
    {
        depositDetailsPopUp.close();
    }

    @Test(dependsOnMethods = { "closeAllEventsCoPartnersDepositDetailsPopUp" })
    public void verifyOrderSystemEventWithDetailsInGrid()
    {
        AllEventsPage allEventsPage = new AllEventsPage();
        allEventsPage.goTo();
        AllEventsGridRow row = allEventsPage.getGrid().getRow(orderSystemEvent.getTransType(),
                orderSystemEvent.getDetail());
        row.verify(orderSystemEvent);

        orderSystGridRowContainer = row.expand(OrderSystemDetailsGridRowContainer.class);
        orderSystGridRowContainer.verify(order);
    }

    @Test(dependsOnMethods = { "verifyOrderSystemEventWithDetailsInGrid" }, alwaysRun = true)
    public void openAllEventsOrderSystemEventDetailsPopUp()
    {
        orderSystGridRowContainer.clickDetails();
        verifyThat(orderSysDetailsPopUp, isDisplayedWithWait());
    }

    @Test(dependsOnMethods = { "openAllEventsOrderSystemEventDetailsPopUp" })
    public void verifyAllEventsOrderSystemEventEarningDetails()
    {
        EventEarningDetailsTab tab = orderSysDetailsPopUp.getEarningDetailsTab();
        tab.goTo();
        tab.verifyBasicDetailsNoPointsAndMiles();
        tab.getGrid().verify(baseAwardsEarningEvent);
    }

    @Test(dependsOnMethods = { "openAllEventsOrderSystemEventDetailsPopUp" })
    public void verifyAllEventsOrderSystemEventBillingDetails()
    {
        EventBillingDetailsTab tab = orderSysDetailsPopUp.getBillingDetailsTab();
        tab.goTo();
        tab.verifyTabIsEmpty(ORDER_SYSTEM);
    }

    @Test(dependsOnMethods = { "openAllEventsOrderSystemEventDetailsPopUp" })
    public void verifyAllEventsOrderSystemEventDetailsPopUp()
    {
        OrderItem orderItem = order.getOrderItems().get(0);
        orderItem.setTrackingUniqueNumber("Tracking Unique Number");
        orderItem.setEstimatedDeliveryDate("ddMMMyy");
        orderItem.setSignedForBy("Signed For By");
        orderItem.setComments("Comments");
        orderItem.setActualDeliveryDate("ddMMMyy");

        OrderDetailsTab tab = orderSysDetailsPopUp.getOrderDetailsTab();
        tab.goTo();
        tab.getOrderDetails().verify(orderItem);
        tab.getShippingInfo().verify(order);
    }

    @Test(dependsOnMethods = { "verifyAllEventsOrderSystemEventDetailsPopUp",
            "verifyAllEventsOrderSystemEventBillingDetails", "verifyAllEventsOrderSystemEventEarningDetails" }, alwaysRun = true)
    public void closeAllEventsOrderSystemEventDetailsPopUp()
    {
        orderSysDetailsPopUp.close();
    }

    @Test(dependsOnMethods = { "closeAllEventsOrderSystemEventDetailsPopUp" }, alwaysRun = true)
    public void verifyResultingTierLevel()
    {
        new LeftPanel().verifyRCLevel(GOLD);
    }
}
