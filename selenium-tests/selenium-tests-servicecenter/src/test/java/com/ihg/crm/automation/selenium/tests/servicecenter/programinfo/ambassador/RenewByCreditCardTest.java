package com.ihg.crm.automation.selenium.tests.servicecenter.programinfo.ambassador;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.event.EventTransactionType.Membership.MEMBERSHIP_RENEWAL;
import static com.ihg.automation.selenium.common.member.AmbassadorUtils.RENEW_GOLD_AMBASSADOR_KIT_200;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.isDisplayedWithWait;
import static com.ihg.automation.selenium.matchers.component.HasSelectItems.hasSelectItems;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.catalog.CatalogItem;
import com.ihg.automation.selenium.common.member.AmbassadorJdbcUtils;
import com.ihg.automation.selenium.common.member.AmbassadorUtils;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.pages.CreditCardType;
import com.ihg.automation.selenium.common.pages.PaymentMethod;
import com.ihg.automation.selenium.common.payment.CreditCardPayment;
import com.ihg.automation.selenium.common.payment.CreditCardPaymentContainer;
import com.ihg.automation.selenium.common.payment.PaymentDetailsPopUp;
import com.ihg.automation.selenium.gwt.components.Mode;
import com.ihg.automation.selenium.servicecenter.pages.LeftPanel;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsGridRow;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsPage;
import com.ihg.automation.selenium.servicecenter.pages.event.enroll.MembershipDetails;
import com.ihg.automation.selenium.servicecenter.pages.event.enroll.MembershipDetailsPopUp;
import com.ihg.automation.selenium.servicecenter.pages.event.enroll.MembershipDetailsTab;
import com.ihg.automation.selenium.servicecenter.pages.event.enroll.MembershipGridRowContainer;
import com.ihg.automation.selenium.servicecenter.pages.programs.amb.AmbassadorPage;

public class RenewByCreditCardTest extends AmbassadorCommon
{
    public static final CatalogItem RENEW_AWARD = AmbassadorUtils.AMBASSADOR_RENEWAL_$200;

    private AmbassadorPage ambassadorPage = new AmbassadorPage();
    private MembershipDetailsPopUp membershipDetailsPopUp = new MembershipDetailsPopUp();
    private AllEventsPage allEventsPage = new AllEventsPage();
    private MembershipGridRowContainer container;
    private MembershipDetails membershipDetails;
    private CreditCardPayment creditCardItem = new CreditCardPayment(RENEW_AWARD, "342507263407463",
            CreditCardType.AMERICAN_EXPRESS);
    private PaymentDetailsPopUp paymentDetailsPopUp = new PaymentDetailsPopUp();
    private int daysShift;

    @BeforeClass
    public void beforeClass()
    {
        login();

        Member member = enrollAMBmember();
        daysShift = new AmbassadorJdbcUtils(jdbcTemplateUpdate, lastUpdateUserId).updateForRenew(member);
        new LeftPanel().reOpenMemberProfile(member);

        ambassadorPage.goTo();
    }

    @Test
    public void verifyRenewButtonAndExpirationDate()
    {
        ambassadorPage.getSummary().verifyRenewAndExtendButtons(true, true);
    }

    @Test(dependsOnMethods = "verifyRenewButtonAndExpirationDate", alwaysRun = true)
    public void populatePaymentDetailsPopUp()
    {
        ambassadorPage.getSummary().clickRenew();
        paymentDetailsPopUp.selectAmountAndPaymentMethod(RENEW_AWARD, PaymentMethod.CREDIT_CARD);
        CreditCardPaymentContainer creditCardPayment = paymentDetailsPopUp.getCreditCardPayment();
        creditCardPayment.verifyPaymentFieldsDisplayed();
        verifyThat(creditCardPayment.getType(), hasSelectItems(CreditCardType.getDataList()));
        creditCardPayment.populate(creditCardItem);
        paymentDetailsPopUp.clickSubmitNoError();
    }

    @Test(dependsOnMethods = "populatePaymentDetailsPopUp", alwaysRun = true)
    public void verifyExpiredDate()
    {
        ambassadorPage.getSummary().verifyExpDateAfterRenew(daysShift);
    }

    @Test(dependsOnMethods = "verifyExpiredDate", alwaysRun = true)
    public void verifyAllEventsContainer()
    {
        allEventsPage.goTo();
        AllEventsGridRow row = allEventsPage.getGrid().getRow(MEMBERSHIP_RENEWAL);

        container = row.expand(MembershipGridRowContainer.class);
        membershipDetails = container.getMembershipDetails();
        membershipDetails.getPaymentDetails().verify(creditCardItem, Mode.VIEW);
        membershipDetails.verifyScSource(helper);
    }

    @Test(dependsOnMethods = "verifyAllEventsContainer", alwaysRun = true)
    public void openMembershipDetailsPopUpVerify()
    {
        container.clickDetails();
        membershipDetailsPopUp = new MembershipDetailsPopUp();
        verifyThat(membershipDetailsPopUp, isDisplayedWithWait());
    }

    @Test(dependsOnMethods = "openMembershipDetailsPopUpVerify", alwaysRun = true)
    public void verifyAllEventsMembershipDetailsTab()
    {
        MembershipDetailsTab membershipDetailsTab = membershipDetailsPopUp.getMembershipDetailsTab();
        membershipDetailsTab.goTo();
        membershipDetails = membershipDetailsTab.getMembershipDetails();
        membershipDetails.getPaymentDetails().verify(creditCardItem, Mode.VIEW);
        membershipDetails.verifyScSource(helper);
    }

    @Test(dependsOnMethods = "verifyAllEventsMembershipDetailsTab", alwaysRun = true)
    public void verifyAllEventsEarningDetailsTab()
    {
        verifyEarningDetailsTab(RENEW_AWARD, RENEW_GOLD_AMBASSADOR_KIT_200);
    }

    @Test(dependsOnMethods = { "verifyAllEventsEarningDetailsTab" }, alwaysRun = true)
    public void validateBillingDetailsTab()
    {
        validateBillingDetails(RENEW_AWARD);
    }
}
