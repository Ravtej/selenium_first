package com.ihg.crm.automation.selenium.tests.servicecenter.enrollment.natives;

import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyNoError;
import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifySuccess;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.Constant.NOT_AVAILABLE;
import static com.ihg.automation.selenium.common.components.NativeLanguage.SIMPLIFIED_CHINESE;
import static com.ihg.automation.selenium.gwt.components.utils.ByStringText.contains;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static com.ihg.automation.selenium.matchers.component.ViewableMatcher.hasTextInView;
import static com.ihg.automation.selenium.matchers.text.ContainsIgnoreCase.containsIgnoreCase;
import static com.ihg.automation.selenium.matchers.text.EntityMatcherFactory.isValue;
import static com.ihg.automation.selenium.servicecenter.pages.history.AuditConstant.FALSE;
import static com.ihg.automation.selenium.servicecenter.pages.history.AuditConstant.NOT_SET;
import static com.ihg.automation.selenium.servicecenter.pages.history.AuditConstant.SET;
import static com.ihg.automation.selenium.servicecenter.pages.history.AuditConstant.ContactInformation.CONTACT_INFORMATION;
import static com.ihg.automation.selenium.servicecenter.pages.history.AuditConstant.ContactInformation.MARK_INVALID;
import static com.ihg.automation.selenium.servicecenter.pages.history.AuditConstant.ContactInformation.NON_PREFERRED;
import static com.ihg.automation.selenium.servicecenter.pages.history.AuditConstant.ContactInformation.PREFERRED;
import static com.ihg.automation.selenium.servicecenter.pages.history.AuditConstant.ContactInformation.TYPE;
import static com.ihg.automation.selenium.servicecenter.pages.history.AuditConstant.ContactInformation.Address.ADDRESS_ADDED;
import static com.ihg.automation.selenium.servicecenter.pages.history.AuditConstant.ContactInformation.Address.CITY;
import static com.ihg.automation.selenium.servicecenter.pages.history.AuditConstant.ContactInformation.Address.DO_NOT_CLEAN;
import static com.ihg.automation.selenium.servicecenter.pages.history.AuditConstant.ContactInformation.Address.NATIVE_ADDRESS_ADDED;
import static com.ihg.automation.selenium.servicecenter.pages.history.AuditConstant.ContactInformation.Address.STATE;
import static com.ihg.automation.selenium.servicecenter.pages.history.AuditConstant.PersonalInformation.COUNTRY;
import static com.ihg.automation.selenium.servicecenter.pages.history.AuditConstant.PersonalInformation.NAME;
import static com.ihg.automation.selenium.servicecenter.pages.history.AuditConstant.PersonalInformation.NATIVE_NAME;
import static com.ihg.automation.selenium.servicecenter.pages.history.AuditConstant.PersonalInformation.PERSONAL_INFORMATION;
import static com.ihg.automation.selenium.servicecenter.pages.history.AuditConstant.StayPreferences.BANK_ACCOUNT_NO;
import static com.ihg.automation.selenium.servicecenter.pages.history.AuditConstant.StayPreferences.COMPANY_ADDRESS;
import static com.ihg.automation.selenium.servicecenter.pages.history.AuditConstant.StayPreferences.COMPANY_BANK;
import static com.ihg.automation.selenium.servicecenter.pages.history.AuditConstant.StayPreferences.COMPANY_NAME;
import static com.ihg.automation.selenium.servicecenter.pages.history.AuditConstant.StayPreferences.COMPANY_TELEPHONE;
import static com.ihg.automation.selenium.servicecenter.pages.history.AuditConstant.StayPreferences.FAPIAO;
import static com.ihg.automation.selenium.servicecenter.pages.history.AuditConstant.StayPreferences.STAY_PREFERENCES;
import static com.ihg.automation.selenium.servicecenter.pages.history.AuditConstant.StayPreferences.TAX_IDENTIFICATION_NO;
import static com.ihg.automation.selenium.servicecenter.pages.history.AuditConstant.StayPreferences.VAT_FAPIAO;
import static com.ihg.automation.selenium.servicecenter.pages.preferences.TravelProfilePopUpBase.STAY_PREF_SUCCESS_MESSAGE;
import static org.hamcrest.core.StringContains.containsString;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.components.Country;
import com.ihg.automation.selenium.common.contact.Address;
import com.ihg.automation.selenium.common.member.GuestAddress;
import com.ihg.automation.selenium.common.member.GuestName;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.name.PersonNameFields;
import com.ihg.automation.selenium.common.types.address.AddressType;
import com.ihg.automation.selenium.common.types.address.RegionChinaNative;
import com.ihg.automation.selenium.common.types.address.RegionChinaRoman;
import com.ihg.automation.selenium.common.types.name.NameConfiguration;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.gwt.components.Mode;
import com.ihg.automation.selenium.servicecenter.pages.LeftPanel;
import com.ihg.automation.selenium.servicecenter.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.servicecenter.pages.history.AuditConstant;
import com.ihg.automation.selenium.servicecenter.pages.history.ProfileHistoryGridRow;
import com.ihg.automation.selenium.servicecenter.pages.history.ProfileHistoryPage;
import com.ihg.automation.selenium.servicecenter.pages.personal.AddressContact;
import com.ihg.automation.selenium.servicecenter.pages.personal.AddressContactList;
import com.ihg.automation.selenium.servicecenter.pages.personal.CustomerInfoFields;
import com.ihg.automation.selenium.servicecenter.pages.personal.PersonalInfoPage;
import com.ihg.automation.selenium.servicecenter.pages.preferences.StayPreferencesPage;
import com.ihg.automation.selenium.servicecenter.pages.preferences.VatFapiaoDetails;
import com.ihg.crm.automation.selenium.tests.servicecenter.LoginLogout;

public class EnrollmentWithMemberFromChinaTest extends LoginLogout
{
    public static final String COMPANY = "群邑 （上海）广告有限公司3";
    public static final String TAX_NO = "3101067178805513";
    public static final String ADDRESS = "上海市徐汇区长乐路989号31楼3102室3";
    public static final String TELEPHONE = "021-2405118823";
    public static final String BANK = "汇丰银行上海分行3";
    public static final String BANK_ACCOUNT = "720-042316-013";
    public static final String UPDATED_TELEPHONE = "021-1111111111";
    private EnrollmentPage enrollmentPage = new EnrollmentPage();
    private GuestName name = new GuestName();
    private GuestName nativeName = new GuestName();
    private GuestAddress address = new GuestAddress();
    private GuestAddress nativeAddress = new GuestAddress();
    private PersonalInfoPage persInfo = new PersonalInfoPage();
    private CustomerInfoFields customerInfoFields;
    private PersonNameFields personNameFields;
    private AddressContactList addressContactList;
    private Member member = new Member();

    @BeforeClass
    public void before()
    {
        nativeName.setGiven("名字");
        nativeName.setSurname("姓");
        nativeName.setNativeLanguage(SIMPLIFIED_CHINESE);

        name.setGiven("ming zi");
        name.setSurname("xing");
        name.setNativeName(nativeName);

        nativeAddress.setLine1("东直门北中街4");
        nativeAddress.setLocality1("北京市");
        nativeAddress.setRegion1(RegionChinaNative.BEI_JING_SHI);

        address.setCountryCode(Country.CN);
        address.setType(AddressType.RESIDENCE);
        address.setLocality1("bei jing shi");
        address.setLine1("dong zhi men bei zhong jie 4");
        address.setRegion1(RegionChinaRoman.BEI_JING_SHI);

        address.setNativeAddress(nativeAddress);

        login();
    }

    @Test
    public void populateNameNativeFields()
    {
        enrollmentPage.goTo();
        enrollmentPage.getPrograms().selectProgram(Program.RC);
        enrollmentPage.getCustomerInfo().getResidenceCountry().selectByCode(Country.CN);

        PersonNameFields nameFields = enrollmentPage.getCustomerInfo().getName();
        nameFields.setConfiguration(Country.CN);
        nameFields.populateNativeFields(name);
        nameFields.verify(name, Mode.EDIT);
    }

    @Test(dependsOnMethods = { "populateNameNativeFields" })
    public void populateAddressNativeFields()
    {
        Address addressFields = enrollmentPage.getAddress();
        verifyThat(addressFields, hasText(containsString(SIMPLIFIED_CHINESE)));
        addressFields.populateNativeFields(address);
        addressFields.verify(address, Mode.EDIT);

        addressFields.getClearNativeAddress().clickAndWait();

        addressFields.populateNativeFields(address);
    }

    @Test(dependsOnMethods = { "populateAddressNativeFields" })
    public void completeEnrollment()
    {
        enrollmentPage.completeEnrollWithDuplicatePopUp(member);
    }

    @Test(dependsOnMethods = { "completeEnrollment" })
    public void verifyNativeFieldsHistoryAfterEnrollment()
    {
        ProfileHistoryPage historyPage = new ProfileHistoryPage();
        historyPage.goTo();
        verifyNoError();

        ProfileHistoryGridRow historyGridRow = historyPage.getProfileHistoryGrid().getRow(1);
        ProfileHistoryGridRow personalInfoRow = historyGridRow.getSubRowByName(PERSONAL_INFORMATION);

        personalInfoRow.getSubRowByName(COUNTRY).verifyAddAction(hasText(isValue(address.getCountry())));
        personalInfoRow.getSubRowByName(NAME).verifyAddAction(name.getNameOnPanel());
        personalInfoRow.getSubRowByName(NATIVE_NAME).verifyAddAction(nativeName.getNameOnPanel());

        ProfileHistoryGridRow contactInfoSubRow = historyGridRow.getSubRowByName(CONTACT_INFORMATION);
        ProfileHistoryGridRow addressRow = contactInfoSubRow.getSubRowByName(contains(NATIVE_ADDRESS_ADDED));
        addressRow.getSubRowByName(STATE).verifyAddAction(hasText(isValue(nativeAddress.getRegion1())));
        addressRow.getSubRowByName(CITY).verifyAddAction(nativeAddress.getLocality1());
        addressRow.getSubRowByName(DO_NOT_CLEAN).verifyAddAction(FALSE);
        addressRow.getSubRowByName(MARK_INVALID).verifyAddAction(NOT_SET);
        addressRow.getSubRowByName(AuditConstant.ContactInformation.Address.COUNTRY)
                .verifyAddAction(hasText(isValue(address.getCountry())));
        addressRow.getSubRowByName(TYPE).verifyAddAction(hasText(isValue(address.getType())));
        addressRow.getSubRowByName(PREFERRED).verifyAddAction(NON_PREFERRED);

        verifyThat(contactInfoSubRow.getSubRowByName(contains(ADDRESS_ADDED)), displayed(true));
    }

    @Test(dependsOnMethods = { "verifyNativeFieldsHistoryAfterEnrollment" })
    public void verifyCustomerInfoDetailsViewMode()
    {
        persInfo.goTo();
        customerInfoFields = persInfo.getCustomerInfo();
        personNameFields = customerInfoFields.getName();
        verifyThat(personNameFields.getFullName(),
                hasText(name.getFullName(NameConfiguration.getEasternAsiaConfiguration())));
        verifyThat(personNameFields.getFullNativeName(),
                hasText(name.getNativeName().getFullName(NameConfiguration.getEasternAsiaConfiguration())));
        verifyThat(customerInfoFields.getNativeLanguage(), hasText(SIMPLIFIED_CHINESE));
        verifyThat(personNameFields.getTransliterate(), displayed(false));
    }

    @Test(dependsOnMethods = { "verifyCustomerInfoDetailsViewMode" })
    public void verifyCustomerInfoDetailsEditMode()
    {
        customerInfoFields.clickEdit();
        personNameFields = customerInfoFields.getName();
        personNameFields.setConfiguration(Country.CN);
        verifyThat(personNameFields.getNativeGivenName(), displayed(true));
        verifyThat(personNameFields.getNativeMiddleName(), displayed(true));
        verifyThat(personNameFields.getNativeSurname(), displayed(true));
        verifyThat(personNameFields.getTransliterate(), displayed(true));
    }

    @Test(dependsOnMethods = { "verifyCustomerInfoDetailsEditMode" })
    public void verifyAddressDetailsInView()
    {
        addressContactList = persInfo.getAddressList();

        verifyThat(addressContactList.getContact(), hasText(containsString(address.getCollapseAddress())));
    }

    @Test(dependsOnMethods = { "verifyAddressDetailsInView" })
    public void verifyAddressDetailsExpanded()
    {
        addressContactList.getContact().expand();
        Address addressExpanded = addressContactList.getExpandedContact().getBaseContact();
        verifyThat(addressExpanded.getTransliterate(), displayed(false));
        verifyThat(addressExpanded.getClearNativeAddress(), displayed(false));
        verifyThat(addressExpanded, hasText(containsString(SIMPLIFIED_CHINESE)));
    }

    @Test(dependsOnMethods = { "verifyAddressDetailsExpanded" })
    public void verifyAddressDetailsEditMode()
    {
        AddressContact contact = addressContactList.getContact();
        contact.clickEdit();
        contact.getBaseContact().verifyNativeFields(address, SIMPLIFIED_CHINESE, Mode.EDIT);
    }

    @Test(dependsOnMethods = { "verifyAddressDetailsEditMode" })
    public void addVatFapiao()
    {
        StayPreferencesPage stayPreferencesPage = new StayPreferencesPage();
        stayPreferencesPage.goTo();

        VatFapiaoDetails vatFapiaoDetails = stayPreferencesPage.getVatFapiaoDetails();
        stayPreferencesPage.getEdit().clickAndWait();
        vatFapiaoDetails.getFapiao().check();
        vatFapiaoDetails.getCompanyName().type(COMPANY);
        vatFapiaoDetails.getTaxIdentificationNo().type(TAX_NO);
        vatFapiaoDetails.getCompanyAddress().type(ADDRESS);
        vatFapiaoDetails.getCompanyTelephone().type(TELEPHONE);
        vatFapiaoDetails.getCompanyBank().type(BANK);
        vatFapiaoDetails.getBankAccountNo().type(BANK_ACCOUNT);

        stayPreferencesPage.getSave().clickAndWait(verifySuccess(STAY_PREF_SUCCESS_MESSAGE));

        vatFapiaoDetails = stayPreferencesPage.getVatFapiaoDetails();
        verifyThat(vatFapiaoDetails.getFapiaoLabel(), hasText("Yes"));
        verifyThat(vatFapiaoDetails.getCompanyName(), hasTextInView(COMPANY));
        verifyThat(vatFapiaoDetails.getTaxIdentificationNo(), hasTextInView(TAX_NO));
        verifyThat(vatFapiaoDetails.getCompanyAddress(), hasTextInView(ADDRESS));
        verifyThat(vatFapiaoDetails.getCompanyTelephone(), hasTextInView(TELEPHONE));
        verifyThat(vatFapiaoDetails.getCompanyBank(), hasTextInView(BANK));
        verifyThat(vatFapiaoDetails.getBankAccountNo(), hasTextInView(BANK_ACCOUNT));
    }

    @Test(dependsOnMethods = { "addVatFapiao" })
    public void verifyHistoryAfterAddVatFapiao()
    {
        ProfileHistoryPage historyPage = new ProfileHistoryPage();
        historyPage.goTo();
        verifyNoError();

        ProfileHistoryGridRow stayPrefInfoRow = historyPage.getProfileHistoryGrid().getRow(1)
                .getSubRowByName(STAY_PREFERENCES).getSubRowByName(VAT_FAPIAO);

        stayPrefInfoRow.getSubRowByName(FAPIAO).verifyAddAction(SET);
        stayPrefInfoRow.getSubRowByName(COMPANY_NAME).verifyAddAction(COMPANY);
        stayPrefInfoRow.getSubRowByName(TAX_IDENTIFICATION_NO).verifyAddAction(TAX_NO);
        stayPrefInfoRow.getSubRowByName(COMPANY_ADDRESS).verifyAddAction(ADDRESS);
        stayPrefInfoRow.getSubRowByName(COMPANY_TELEPHONE).verifyAddAction(TELEPHONE);
        stayPrefInfoRow.getSubRowByName(COMPANY_BANK).verifyAddAction(BANK);
        stayPrefInfoRow.getSubRowByName(BANK_ACCOUNT_NO).verifyAddAction(BANK_ACCOUNT);
    }

    @Test(dependsOnMethods = { "verifyHistoryAfterAddVatFapiao" })
    public void updateVatFapiao()
    {
        new LeftPanel().reOpenMemberProfile(member);
        StayPreferencesPage stayPreferencesPage = new StayPreferencesPage();
        stayPreferencesPage.goTo();

        VatFapiaoDetails vatFapiaoDetails = stayPreferencesPage.getVatFapiaoDetails();
        stayPreferencesPage.getEdit().clickAndWait();
        vatFapiaoDetails.getFapiao().check();
        vatFapiaoDetails.getCompanyTelephone().type(UPDATED_TELEPHONE);

        stayPreferencesPage.getSave().clickAndWait(verifySuccess(STAY_PREF_SUCCESS_MESSAGE));

        vatFapiaoDetails = stayPreferencesPage.getVatFapiaoDetails();
        verifyThat(vatFapiaoDetails.getCompanyTelephone(), hasTextInView(UPDATED_TELEPHONE));
    }

    @Test(dependsOnMethods = { "updateVatFapiao" })
    public void verifyHistoryAfterUpdateVatFapiao()
    {
        ProfileHistoryPage historyPage = new ProfileHistoryPage();
        historyPage.goTo();
        verifyNoError();

        ProfileHistoryGridRow stayPrefInfoRow = historyPage.getProfileHistoryGrid().getRow(1)
                .getSubRowByName(STAY_PREFERENCES).getSubRowByName(VAT_FAPIAO);

        stayPrefInfoRow.getSubRowByName(COMPANY_TELEPHONE).verify(TELEPHONE, UPDATED_TELEPHONE);
    }

    @Test(dependsOnMethods = { "verifyHistoryAfterUpdateVatFapiao" })
    public void removeVatFapiao()
    {
        new LeftPanel().reOpenMemberProfile(member);
        StayPreferencesPage stayPreferencesPage = new StayPreferencesPage();
        stayPreferencesPage.goTo();

        VatFapiaoDetails vatFapiaoDetails = stayPreferencesPage.getVatFapiaoDetails();
        stayPreferencesPage.getEdit().clickAndWait();
        vatFapiaoDetails.getFapiao().uncheck();
        vatFapiaoDetails.getCompanyName().clear();
        vatFapiaoDetails.getTaxIdentificationNo().clear();
        vatFapiaoDetails.getCompanyAddress().clear();
        vatFapiaoDetails.getCompanyTelephone().clear();
        vatFapiaoDetails.getCompanyBank().clear();
        vatFapiaoDetails.getBankAccountNo().clear();

        stayPreferencesPage.getSave().clickAndWait(verifySuccess(STAY_PREF_SUCCESS_MESSAGE));

        vatFapiaoDetails = stayPreferencesPage.getVatFapiaoDetails();
        verifyThat(vatFapiaoDetails.getFapiaoLabel(), hasText("No"));
        verifyThat(vatFapiaoDetails.getCompanyName(), hasTextInView(NOT_AVAILABLE));
        verifyThat(vatFapiaoDetails.getTaxIdentificationNo(), hasTextInView(NOT_AVAILABLE));
        verifyThat(vatFapiaoDetails.getCompanyAddress(), hasTextInView(NOT_AVAILABLE));
        verifyThat(vatFapiaoDetails.getCompanyTelephone(), hasTextInView(NOT_AVAILABLE));
        verifyThat(vatFapiaoDetails.getCompanyBank(), hasTextInView(NOT_AVAILABLE));
        verifyThat(vatFapiaoDetails.getBankAccountNo(), hasTextInView(NOT_AVAILABLE));
    }

    @Test(dependsOnMethods = { "removeVatFapiao" })
    public void verifyHistoryAfterRemoveVatFapiao()
    {
        ProfileHistoryPage historyPage = new ProfileHistoryPage();
        historyPage.goTo();
        verifyNoError();

        ProfileHistoryGridRow stayPrefInfoRow = historyPage.getProfileHistoryGrid().getRow(1)
                .getSubRowByName(STAY_PREFERENCES).getSubRowByName(VAT_FAPIAO);

        stayPrefInfoRow.getSubRowByName(FAPIAO).verify(hasText(SET), hasText(containsIgnoreCase(NOT_SET)));
        stayPrefInfoRow.getSubRowByName(COMPANY_NAME).verifyDeleteAction(COMPANY);
        stayPrefInfoRow.getSubRowByName(TAX_IDENTIFICATION_NO).verifyDeleteAction(TAX_NO);
        stayPrefInfoRow.getSubRowByName(COMPANY_ADDRESS).verifyDeleteAction(ADDRESS);
        stayPrefInfoRow.getSubRowByName(COMPANY_TELEPHONE).verifyDeleteAction(UPDATED_TELEPHONE);
        stayPrefInfoRow.getSubRowByName(COMPANY_BANK).verifyDeleteAction(BANK);
        stayPrefInfoRow.getSubRowByName(BANK_ACCOUNT_NO).verifyDeleteAction(BANK_ACCOUNT);
    }
}
