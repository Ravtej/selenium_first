package com.ihg.crm.automation.selenium.tests.servicecenter.enrollment.dr;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.event.EarningEventFactory.getAwardEvent;
import static com.ihg.automation.selenium.common.event.EarningEventFactory.getProgramEventWithoutStatus;
import static com.ihg.automation.selenium.common.event.EventTransactionType.Membership.ENROLL;
import static com.ihg.automation.selenium.common.member.DiningRewardAward.FUL_WELCOME_TO_GOLD;
import static com.ihg.automation.selenium.common.member.MemberJdbcUtils.UPDATE_EXP;
import static com.ihg.automation.selenium.common.offer.OfferFactory.DR_FREE_NIGHT;
import static com.ihg.automation.selenium.common.types.program.Program.DR;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasAnyText;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.isDisplayedWithWait;

import java.util.Arrays;

import org.joda.time.LocalDate;
import org.springframework.beans.factory.annotation.Value;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.common.DateUtils;
import com.ihg.automation.selenium.common.Constant;
import com.ihg.automation.selenium.common.catalog.CatalogItem;
import com.ihg.automation.selenium.common.components.Country;
import com.ihg.automation.selenium.common.contact.Address;
import com.ihg.automation.selenium.common.earning.EventEarningDetailsTab;
import com.ihg.automation.selenium.common.event.AllEventsRowFactory;
import com.ihg.automation.selenium.common.event.EarningEvent;
import com.ihg.automation.selenium.common.member.DiningRewardAward;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberJdbcUtils;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.name.PersonNameFields;
import com.ihg.automation.selenium.common.payment.CashPayment;
import com.ihg.automation.selenium.common.payment.PaymentDetails;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.common.types.program.ProgramStatus;
import com.ihg.automation.selenium.common.types.program.RewardClubLevel;
import com.ihg.automation.selenium.gwt.components.Mode;
import com.ihg.automation.selenium.servicecenter.pages.GuestSearch;
import com.ihg.automation.selenium.servicecenter.pages.Role;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsGrid;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsPage;
import com.ihg.automation.selenium.servicecenter.pages.event.enroll.MembershipDetailsPopUp;
import com.ihg.automation.selenium.servicecenter.pages.event.enroll.MembershipGridRowContainer;
import com.ihg.automation.selenium.servicecenter.pages.personal.AddressContact;
import com.ihg.automation.selenium.servicecenter.pages.personal.PersonalInfoPage;
import com.ihg.automation.selenium.servicecenter.pages.programs.dr.DiningRewardsPage;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.RewardClubPage;

public class ChineseMemberByCashTest extends EnrollToDrCommon
{
    @Value("${memberByCashCN}")
    protected String memberByCashCN;

    @Value("${memberByCashCN.date}")
    protected String date;

    private LocalDate enrollDate;
    private Member member = new Member();
    protected static final CatalogItem ENROLLMENT_AWARD = DiningRewardAward.ENROLL_RMB2388;
    private MembershipDetailsPopUp membershipDetailsPopUp = new MembershipDetailsPopUp();
    private CashPayment cashPayment = new CashPayment(ENROLLMENT_AWARD, Constant.NOT_AVAILABLE, Constant.NOT_AVAILABLE,
            Constant.NOT_AVAILABLE, Constant.NOT_AVAILABLE);

    @BeforeClass
    private void beforeClass()
    {
        MemberJdbcUtils.update(jdbcTemplateUpdate, UPDATE_EXP, memberByCashCN, DR, 13);

        enrollDate = DateUtils.getStringToDate(date);

        member.addAddress(MemberPopulateHelper.getChinaAddress());
        member.addProgram(DR);

        login(user15U, Role.AGENT_TIER2);
        new GuestSearch().byMemberNumber(memberByCashCN);
    }

    @Test
    public void verifyPersonalInformationTab()
    {
        PersonalInfoPage personalInfoPage = new PersonalInfoPage();

        PersonNameFields name = personalInfoPage.getCustomerInfo().getName();
        name.setConfiguration(Country.CN);
        verifyThat(name.getFullName(), hasAnyText());

        AddressContact addressContact = personalInfoPage.getAddressList().getExpandedContact();
        Address address = addressContact.getBaseContact();
        address.verify(member.getPreferredAddress(), Mode.VIEW);
    }

    @Test(dependsOnMethods = { "verifyPersonalInformationTab" }, alwaysRun = true)
    public void verifyAccountInformation()
    {
        validateAccountInformation();
    }

    @Test(dependsOnMethods = { "verifyAccountInformation" }, alwaysRun = true)
    public void verifyRewardsClubPage()
    {
        RewardClubPage rewardClubPage = new RewardClubPage();
        rewardClubPage.goTo();
        rewardClubPage.getSummary().verify(ProgramStatus.OPEN, RewardClubLevel.GOLD);
    }

    @Test(dependsOnMethods = { "verifyRewardsClubPage" }, alwaysRun = true)
    public void verifyDiningRewardClubPage()
    {
        validateDiningRewardClubPage(memberByCashCN, EXP_DATE, enrollDate);
        PaymentDetails details = new DiningRewardsPage().getEnrollmentDetails().getPaymentDetails();
        details.verify(cashPayment, Mode.VIEW);
    }

    @Test(dependsOnMethods = { "verifyDiningRewardClubPage" }, alwaysRun = true)
    public void verifyEvents()
    {
        validateEvents(Arrays.asList(AllEventsRowFactory.getEnrollEvent(enrollDate, Program.DR), enrollRC,
                AllEventsRowFactory.getOfferRegistrationEvent(DR_FREE_NIGHT), delayedFulfilmentOffer, orderSystem), 5);
    }

    @Test(dependsOnMethods = { "verifyEvents" }, alwaysRun = true)
    public void openAllEventsEnrollDREventDetails()
    {
        AllEventsPage allEventsPage = new AllEventsPage();
        allEventsPage.goTo();
        AllEventsGrid allEventsGrid = allEventsPage.getGrid();

        allEventsGrid.getEnrollRow(DR).expand(MembershipGridRowContainer.class).clickDetails();
        verifyThat(membershipDetailsPopUp, isDisplayedWithWait());
    }

    @Test(dependsOnMethods = { "openAllEventsEnrollDREventDetails" })
    public void verifyAllEventsEarningDetails()
    {
        EventEarningDetailsTab tab = membershipDetailsPopUp.getEarningDetailsTab();
        tab.goTo();

        tab.verifyBasicDetailsNoPointsAndMiles();

        EarningEvent rcEvent = new EarningEvent(DR_FREE_NIGHT.getCode(), "0", "0", Constant.RC_POINTS, enrollDate);
        tab.getGrid().verifyRowsFoundByType(getAwardEvent(ENROLLMENT_AWARD, enrollDate),
                getProgramEventWithoutStatus(DR, FUL_WELCOME_TO_GOLD, enrollDate), rcEvent, rcEvent);
    }

    @Test(dependsOnMethods = { "openAllEventsEnrollDREventDetails" })
    public void verifyAllEventsBillingDetails()
    {
        membershipDetailsPopUp.getBillingDetailsTab().verifyBaseAwardUSDBilling(ENROLL, ENROLLMENT_AWARD, user15U,
                enrollDate);
    }

    @Test(dependsOnMethods = { "openAllEventsEnrollDREventDetails", "verifyAllEventsEarningDetails",
            "verifyAllEventsBillingDetails" }, alwaysRun = true)
    public void closeAllEventsEnrollDREventDetails()
    {
        membershipDetailsPopUp.close();
    }

}
