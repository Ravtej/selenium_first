package com.ihg.crm.automation.selenium.tests.servicecenter.order;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.assertThat;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.history.EventAuditConstant.CREATE;
import static com.ihg.automation.selenium.common.history.EventAuditConstant.CREATED;
import static com.ihg.automation.selenium.common.history.EventAuditConstant.Order.STATUS;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.automation.selenium.matchers.component.GridSize.size;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static org.hamcrest.number.OrderingComparison.greaterThan;

import java.util.Map;

import org.springframework.beans.factory.annotation.Value;
import org.testng.Assert;

import com.ihg.automation.selenium.common.CustomerInfoPanel;
import com.ihg.automation.selenium.common.billing.EventBillingDetailsTab;
import com.ihg.automation.selenium.common.earning.EventEarningDetailsTab;
import com.ihg.automation.selenium.common.earning.EventEarningGridRow;
import com.ihg.automation.selenium.common.earning.EventEarningGridRow.EventEarningCell;
import com.ihg.automation.selenium.common.event.EarningEvent;
import com.ihg.automation.selenium.common.history.EventHistoryGridRow;
import com.ihg.automation.selenium.common.history.EventHistoryRecords;
import com.ihg.automation.selenium.common.history.EventHistoryTab;
import com.ihg.automation.selenium.common.order.VoucherOrder;
import com.ihg.automation.selenium.common.order.VoucherRule;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.servicecenter.pages.GuestSearch;
import com.ihg.automation.selenium.servicecenter.pages.event.order.OrderDetailsPopUp;
import com.ihg.automation.selenium.servicecenter.pages.voucher.VoucherGridRow;
import com.ihg.automation.selenium.servicecenter.pages.voucher.VoucherPage;
import com.ihg.crm.automation.selenium.tests.servicecenter.LoginLogout;

public class VoucherOrderCommon extends LoginLogout
{
    @Value("${memberID.htl}")
    protected String hotelMemberId;

    @Value("${hotel}")
    protected String hotel;

    @Value("${hotel.brand}")
    protected String hotelBrand;

    protected static final String VOUCHERS_ORDER_SUCCESS_MESSAGE = "Vouchers are ordered successfully.";

    private Map<String, Object> map;

    private String today_mpe_sql = "WITH TODAY_MPE AS"//
            + " (SELECT * FROM FMDS.TMMBRPT MPE"//
            + "  WHERE mpe.POINT_EVENT_TYPE = 'COPRT'"//
            + "  AND mpe.MEMBERSHIP_ID = '%s'"//
            + "  AND mpe.COPARTNER_PROMO_ID = '%s'"//
            + "  AND mpe.TRANSACTION_DATE = TRUNC(SYSDATE, 'DD')"//
            + "  ORDER BY MPE.LAST_UPDATE_TMSTMP DESC)"//
            + " SELECT * FROM TODAY_MPE WHERE ROWNUM < 2";

    private String vouchers_sql = " WITH VOUCHERS AS ("//
            + "SELECT "//
            + "  COUNT(*) OVER (ORDER BY BON_VOUCHER_ID RANGE BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) VOUCHER_COUNT,"//
            + "  FIRST_VALUE(VOUCHER) OVER (ORDER BY BON_VOUCHER_ID RANGE BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) FIRST_VOUCHER_ID,"//
            + "  LAST_VALUE(VOUCHER) OVER (ORDER BY BON_VOUCHER_ID RANGE BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) LAST_VOUCHER_ID"//
            + " FROM"//
            + " (SELECT TO_CHAR(v.BON_VOUCHER_ID,'FM000000000')|| TO_CHAR(v.FACILITY_ID,'FM00000')||'****' VOUCHER, v.* "//
            + " FROM FMDS.TMBNVCH v"//
            + " WHERE v.HOTEL_MBR_ID = '%s'"//
            + " AND v.PROMO_ID = '%s'"//
            + " AND v.ORIG_TRANS_DATE = TRUNC(SYSDATE, 'DD')"//
            + " AND v.ORIG_TRANS_TIME = '%s'"//
            + " ORDER BY v.BON_VOUCHER_ID)) "//
            + "SELECT * FROM VOUCHERS WHERE ROWNUM < 2";

    private static final String FIRST_VOUCHER_NUMBER = "SELECT * FROM"//
            + " (SELECT TO_CHAR(BON_VOUCHER_ID,'FM000000000')||TO_CHAR(FACILITY_ID,'FM00000')||TO_CHAR(BON_VOUCHER_PIN,'FM0000') VOUCHER_NUMBER"//
            + " FROM FMDS.TMBNVCH"//
            + " WHERE  PROMO_ID = '%s'"//
            + " AND ORIG_TRANS_DATE = TRUNC(SYSDATE, 'DD')"//
            + " AND ORIG_TRANS_TIME = '%s'"//
            + " ORDER BY LAST_UPDATE_TMSTMP"//
            + " ) WHERE ROWNUM < 2";

    protected void initVoucherOrderRange(VoucherOrder voucherOrder)
    {
        String transactionTime = getLatestTransactionTime(voucherOrder.getPromotionId());

        map = jdbcTemplate.queryForMap(
                String.format(vouchers_sql, hotelMemberId, voucherOrder.getPromotionId(), transactionTime));
        voucherOrder.setVoucherNumberMinValue(map.get("FIRST_VOUCHER_ID").toString());
        voucherOrder.setVoucherNumberMaxValue(map.get("LAST_VOUCHER_ID").toString());

        Assert.assertEquals(map.get("VOUCHER_COUNT").toString(), voucherOrder.getNumberOfVouchers(),
                "Incorrect amount of ordered vouchers");
    }

    private String getLatestTransactionTime(String promotionId)
    {
        map = jdbcTemplate.queryForMap(String.format(today_mpe_sql, hotelMemberId, promotionId));
        String transactionTime = map.get("TRANSACTION_TIME").toString();
        return transactionTime;
    }

    protected String getFirstVoucherNumber(String promotionId)
    {
        return getVoucherNumber(
                String.format(FIRST_VOUCHER_NUMBER, promotionId, getLatestTransactionTime(promotionId)));
    }

    protected void loginAndOpenHotelMemberProfile()
    {
        login();
        new GuestSearch().byMemberNumber(hotelMemberId);

        CustomerInfoPanel customerInfoPanel = new CustomerInfoPanel();
        verifyThat(customerInfoPanel.getFullName(), hasText(hotelBrand + " " + hotel));
        verifyThat(customerInfoPanel.getMemberNumber(Program.HTL), hasText(hotelMemberId));
    }

    protected VoucherGridRow searchVoucherRow(VoucherRule voucherRule)
    {
        VoucherPage voucherPage = new VoucherPage();
        voucherPage.goTo();

        verifyThat(voucherPage.getVouchers(), size(0));
        voucherPage.search(voucherRule.getRuleType());
        verifyThat(voucherPage.getVouchers(), size(greaterThan(1)));

        VoucherGridRow row = voucherPage.getVouchers().getRow(voucherRule.getPromotionId());
        assertThat(row, displayed(true));
        return row;
    }

    protected void verifyEarningDetailsTab(OrderDetailsPopUp orderDetailsPopUp, EarningEvent earningEvent)
    {
        EventEarningDetailsTab earningDetailsTab = orderDetailsPopUp.getEarningDetailsTab();
        earningDetailsTab.goTo();
        earningDetailsTab.verifyBasicDetailsNoPointsAndMiles();
        EventEarningGridRow earningRow = earningDetailsTab.getGrid().getRow(1);
        earningRow.verify(earningEvent);
        verifyThat(earningRow.getCell(EventEarningCell.AWARD).asLink(), displayed(true));
    }

    protected void verifyBillingDetailsTab(OrderDetailsPopUp orderDetailsPopUp, String amount, String currencyCode)
    {
        EventBillingDetailsTab billingDetailsTab = orderDetailsPopUp.getBillingDetailsTab();
        billingDetailsTab.goTo();
        billingDetailsTab.getEventBillingDetail(1).getBaseAwardLoyaltyUnits().verify(amount, currencyCode, hotel);
    }

    protected void verifyHistoryDetailsTab(OrderDetailsPopUp orderDetailsPopUp)
    {
        EventHistoryTab historyTab = orderDetailsPopUp.getHistoryTab();
        historyTab.goTo();

        EventHistoryGridRow historyRow = historyTab.getGrid().getRow(1);
        historyRow.verify(CREATE, helper.getUser().getNameFull());

        EventHistoryRecords stayRecords = historyRow.getAllRecords();
        stayRecords.getRecord(STATUS).verifyAdd(CREATED);
    }

    protected String getVoucherNumber(String sql)
    {
        map = jdbcTemplate.queryForMap(String.format(sql));
        return map.get("VOUCHER_NUMBER").toString();
    }
}
