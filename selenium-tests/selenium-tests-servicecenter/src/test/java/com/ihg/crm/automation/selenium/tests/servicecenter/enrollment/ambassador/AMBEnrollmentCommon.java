package com.ihg.crm.automation.selenium.tests.servicecenter.enrollment.ambassador;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.Constant.SC_LOCATION;
import static com.ihg.automation.selenium.common.event.EarningEventFactory.getAwardEvent;
import static com.ihg.automation.selenium.common.event.EventTransactionType.Membership.ENROLL;
import static com.ihg.automation.selenium.common.member.AmbassadorUtils.NEW_GOLD_AMBASSADOR_KIT;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.isDisplayedWithWait;
import static com.ihg.automation.selenium.matchers.component.GridSize.size;

import com.ihg.automation.selenium.common.catalog.CatalogItem;
import com.ihg.automation.selenium.common.earning.EventEarningDetailsTab;
import com.ihg.automation.selenium.common.earning.EventEarningGrid;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsGrid;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsGridRow;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsPage;
import com.ihg.automation.selenium.servicecenter.pages.event.enroll.MembershipDetailsPopUp;
import com.ihg.automation.selenium.servicecenter.pages.event.enroll.MembershipGridRowContainer;
import com.ihg.crm.automation.selenium.tests.servicecenter.LoginLogout;

public class AMBEnrollmentCommon extends LoginLogout
{
    private AllEventsPage allEventsPage = new AllEventsPage();
    private MembershipGridRowContainer ambMembershipGridRowContainer;
    private MembershipDetailsPopUp membershipDetailsPopUp = new MembershipDetailsPopUp();

    public void openAMBEventDetailsPopUp()
    {
        allEventsPage = new AllEventsPage();
        allEventsPage.goTo();
        AllEventsGrid grid = allEventsPage.getGrid();
        AllEventsGridRow row = grid.getEnrollRow(Program.AMB);
        ambMembershipGridRowContainer = row.expand(MembershipGridRowContainer.class);
        membershipDetailsPopUp = new MembershipDetailsPopUp();
        ambMembershipGridRowContainer.getDetails().click();
        verifyThat(membershipDetailsPopUp, isDisplayedWithWait());
    }

    public void verifyAMBEventBillingDetails(CatalogItem enrollAward)
    {
        membershipDetailsPopUp.getBillingDetailsTab().verifyBaseAwardUSDBilling(ENROLL, enrollAward, SC_LOCATION);
    }

    public void verifyAMBEventEarningDetails(CatalogItem enrollAward)
    {
        EventEarningDetailsTab tab = membershipDetailsPopUp.getEarningDetailsTab();
        tab.goTo();
        tab.verifyBasicDetailsNoPointsAndMiles();

        EventEarningGrid grid = tab.getGrid();
        verifyThat(grid, size(2));
        grid.getRowByAward(enrollAward.getItemId()).verify(getAwardEvent(enrollAward));
        grid.getRowByAward(NEW_GOLD_AMBASSADOR_KIT.getItemId()).verify(getAwardEvent(NEW_GOLD_AMBASSADOR_KIT));
    }

}
