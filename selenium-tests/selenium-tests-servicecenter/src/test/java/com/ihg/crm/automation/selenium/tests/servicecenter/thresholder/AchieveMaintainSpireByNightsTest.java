package com.ihg.crm.automation.selenium.tests.servicecenter.thresholder;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.event.EventTransactionType.TierLevel.UPGRADE_ACHIEVE;
import static com.ihg.automation.selenium.common.event.EventTransactionType.TierLevel.UPGRADE_MAINTAIN;
import static com.ihg.automation.selenium.common.stay.Stay.QUALIFYING_NIGHTS_MAX_AMOUNT_FOR_STAY;
import static com.ihg.automation.selenium.common.types.program.ProgramStatus.OPEN;
import static com.ihg.automation.selenium.common.types.program.RewardClubLevel.CLUB;
import static com.ihg.automation.selenium.common.types.program.RewardClubLevel.SPRE;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasTextAsInt;
import static com.ihg.automation.selenium.matchers.component.GridSize.size;
import static org.hamcrest.Matchers.not;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.stay.Stay;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.servicecenter.pages.LeftPanel;
import com.ihg.automation.selenium.servicecenter.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsPage;
import com.ihg.automation.selenium.servicecenter.pages.event.stay.StayEventsPage;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.RewardClubPage;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.RewardClubSummary;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.TierLevelBenefitsDetailsPopUp;
import com.ihg.automation.selenium.tests.common.RulesDBUtils;
import com.ihg.crm.automation.selenium.tests.servicecenter.LoginLogout;

public class AchieveMaintainSpireByNightsTest extends LoginLogout
{
    private Stay stayInPreviousYearFirst = new Stay();
    private Stay stayInPreviousYearSecond;
    private Stay stayInCurrentYearFirst;
    private Stay stayInCurrentYearSecond;
    private static int nightsToMaintainSpire;
    private static int nightsToAchieveSpirePrevious;

    @BeforeClass
    public void before()
    {
        RulesDBUtils rulesDBUtils = new RulesDBUtils(jdbcTemplate);
        nightsToMaintainSpire = rulesDBUtils.getCurrentYearRuleNights(SPRE, SPRE);
        nightsToAchieveSpirePrevious = rulesDBUtils.getPreviousYearRuleNights(CLUB, SPRE);

        stayInPreviousYearFirst.setHotelCode("ATLCP");
        stayInPreviousYearFirst.setCheckInOutByNightsInPreviousYear(QUALIFYING_NIGHTS_MAX_AMOUNT_FOR_STAY);
        stayInPreviousYearFirst.setAvgRoomRate(15.00);
        stayInPreviousYearFirst.setRateCode("Test");

        stayInPreviousYearSecond = stayInPreviousYearFirst.clone();
        stayInPreviousYearSecond.setCheckInOutByNightsInPreviousYear(
                nightsToAchieveSpirePrevious - QUALIFYING_NIGHTS_MAX_AMOUNT_FOR_STAY,
                QUALIFYING_NIGHTS_MAX_AMOUNT_FOR_STAY);

        stayInCurrentYearFirst = stayInPreviousYearFirst.clone();
        stayInCurrentYearFirst.setCheckInOutByNights(QUALIFYING_NIGHTS_MAX_AMOUNT_FOR_STAY);

        stayInCurrentYearSecond = stayInCurrentYearFirst.clone();
        stayInCurrentYearSecond.setCheckInOutByNights(nightsToMaintainSpire - QUALIFYING_NIGHTS_MAX_AMOUNT_FOR_STAY,
                QUALIFYING_NIGHTS_MAX_AMOUNT_FOR_STAY);

        Member member = new Member();
        member.addProgram(Program.RC);
        member.setPersonalInfo(MemberPopulateHelper.getSimplePersonalInfo());
        member.addAddress(MemberPopulateHelper.getUnitedStatesAddress());

        login();

        new EnrollmentPage().enroll(member);

        new LeftPanel().verifyRCLevel(CLUB);
    }

    @Test
    public void achieveSpireTierLevel()
    {
        StayEventsPage stayEventsPage = new StayEventsPage();
        stayEventsPage.createStay(stayInPreviousYearFirst);
        stayEventsPage.createStay(stayInPreviousYearSecond);

        new LeftPanel().verifyRCLevel(SPRE);

        RewardClubPage rewardClubPage = new RewardClubPage();
        rewardClubPage.goTo();

        rewardClubPage.getPreviousYearAnnualActivities().getNightsRow()
                .verify(hasTextAsInt(nightsToAchieveSpirePrevious), displayed(false));
        rewardClubPage.getCurrentYearAnnualActivities().getNightsRow().verify(hasTextAsInt(0), displayed(false),
                hasTextAsInt(nightsToMaintainSpire));

        AllEventsPage allEventsPage = new AllEventsPage();
        allEventsPage.goTo();

        verifyThat(allEventsPage.getGrid().getRow(UPGRADE_ACHIEVE, "RC - SPIRE"), displayed(true));
    }

    @Test(dependsOnMethods = { "achieveSpireTierLevel" }, alwaysRun = true)
    public void maintainSpireTierLevel()
    {
        StayEventsPage stayEventsPage = new StayEventsPage();
        stayEventsPage.createStay(stayInCurrentYearFirst);
        stayEventsPage.createStay(stayInCurrentYearSecond);

        RewardClubPage rewardClubPage = new RewardClubPage();
        rewardClubPage.goTo();

        RewardClubSummary summary = rewardClubPage.getSummary();
        summary.verify(OPEN, SPRE, 1);

        TierLevelBenefitsDetailsPopUp tierLevelBenefitsDetailsPopUp = summary.openTierLevelBenefitsDetailsPopUp();
        verifyThat(tierLevelBenefitsDetailsPopUp.getBenefitsGrid(), size(not(0)));
        tierLevelBenefitsDetailsPopUp.close();

        rewardClubPage.getCurrentYearAnnualActivities().getNightsRow()
                .verifyMaintainIsAchieved(hasTextAsInt(nightsToMaintainSpire), displayed(false));

        AllEventsPage allEventsPage = new AllEventsPage();
        allEventsPage.goTo();
        verifyThat(allEventsPage.getGrid().getRow(UPGRADE_MAINTAIN, "RC - SPIRE"), displayed(true));
    }
}
