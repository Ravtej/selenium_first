package com.ihg.crm.automation.selenium.tests.servicecenter.programinfo;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.assertNoErrors;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.event.EventTransactionType.Membership.MEMBERSHIP_CLOSE;
import static com.ihg.automation.selenium.common.event.EventTransactionType.Membership.MEMBERSHIP_OPEN;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.isHighlightedAsInvalid;
import static com.ihg.automation.selenium.matchers.component.ViewableMatcher.hasTextInView;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.ihg.automation.common.DateUtils;
import com.ihg.automation.selenium.common.Constant;
import com.ihg.automation.selenium.common.event.AllEventsRow;
import com.ihg.automation.selenium.common.member.AmbassadorOfferCode;
import com.ihg.automation.selenium.common.member.AmbassadorUtils;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.common.types.program.ProgramStatus;
import com.ihg.automation.selenium.common.types.program.ProgramStatusReason;
import com.ihg.automation.selenium.servicecenter.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsGridRow;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsPage;
import com.ihg.automation.selenium.servicecenter.pages.event.status.ProfileStatusGridRowContainer;
import com.ihg.automation.selenium.servicecenter.pages.programs.EmployeePage;
import com.ihg.automation.selenium.servicecenter.pages.programs.GuestAccountPage;
import com.ihg.automation.selenium.servicecenter.pages.programs.ProgramPageBase;
import com.ihg.automation.selenium.servicecenter.pages.programs.ProgramSummary;
import com.ihg.automation.selenium.servicecenter.pages.programs.RenewExtendProgramSummary;
import com.ihg.automation.selenium.servicecenter.pages.programs.SimpleProgramPageBase;
import com.ihg.automation.selenium.servicecenter.pages.programs.amb.AmbassadorPage;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.RewardClubPage;
import com.ihg.crm.automation.selenium.tests.servicecenter.LoginLogout;

public class CloseMembershipValidateStatusOfDependingProgramsTest extends LoginLogout
{
    private Member member = new Member();
    private RewardClubPage rewardClubPage = new RewardClubPage();
    private ProgramSummary rcSummary;
    private GuestAccountPage guestAccountPage = new GuestAccountPage();
    private EmployeePage employeePage = new EmployeePage();
    private AmbassadorPage ambassadorPage = new AmbassadorPage();
    private RenewExtendProgramSummary ambSummary;
    private AllEventsPage allEventsPage = new AllEventsPage();
    private AllEventsRow rcCloseEventRow = new AllEventsRow(DateUtils.getFormattedDate(), MEMBERSHIP_CLOSE,
            Program.RC.getCode(), "", "");
    private AllEventsRow ambCloseEventRow = new AllEventsRow(DateUtils.getFormattedDate(), MEMBERSHIP_CLOSE,
            Program.AMB.getCode(), "", "");

    private AllEventsRow rcOpenEventRow = new AllEventsRow(DateUtils.getFormattedDate(), MEMBERSHIP_OPEN,
            Program.RC.getCode(), "", "");
    private AllEventsRow ambOpenEventRow = new AllEventsRow(DateUtils.getFormattedDate(), MEMBERSHIP_OPEN,
            Program.AMB.getCode(), "", "");

    @BeforeClass
    public void beforeClass()
    {
        member.setPersonalInfo(MemberPopulateHelper.getSimplePersonalInfo());
        member.addAddress(MemberPopulateHelper.getUnitedStatesAddress());
        member.addProgram(Program.RC, Program.IHG, Program.AMB, Program.EMP);
        member.setAmbassadorOfferCode(AmbassadorOfferCode.CHSRO);
        member.setAmbassadorAmount(AmbassadorUtils.AMBASSADOR_COMP_PROMO_$0);

        login();

        member = new EnrollmentPage().enroll(member);
        rewardClubPage.goTo();
        rcSummary = rewardClubPage.getSummary();
        rcSummary.setClosedStatus(ProgramStatusReason.CUSTOMER_REQUEST);
        assertNoErrors();
    }

    @DataProvider(name = "verifySimpleProgramPageProvider")
    public Object[][] verifySimpleProgramPageProvider()
    {
        return new Object[][] { { guestAccountPage, ProgramStatus.OPEN, null },
                { employeePage, ProgramStatus.OPEN, null }, };
    }

    @DataProvider(name = "verifyProgramPageProvider")
    public Object[][] verifyProgramPageProvider()
    {
        return new Object[][] { { ambassadorPage }, { rewardClubPage } };
    }

    @DataProvider(name = "verifyAllEventsTabMemberCloseEventRow")
    public Object[][] verifyAllEventsTabMemberCloseEventRow()
    {
        return new Object[][] { { rcCloseEventRow }, { ambCloseEventRow } };
    }

    @DataProvider(name = "verifyAllEventsTabMemberOpenEventRow")
    public Object[][] verifyAllEventsTabMemberOpenEventRow()
    {
        return new Object[][] { { rcOpenEventRow }, { ambOpenEventRow } };
    }

    @Test(dataProvider = "verifySimpleProgramPageProvider")
    public void verifySimpleProgramPage(SimpleProgramPageBase page, String status, String statusReason)
    {
        page.goTo();
        verifyThat(page.getSummary().getStatus(), hasTextInView(status));
        if (null == statusReason)
        {
            verifyThat(page.getSummary().getStatusReason(), displayed(false));
        }
        else
        {
            verifyThat(page.getSummary().getStatus(), hasTextInView(ProgramStatus.CLOSED));
        }
    }

    @Test(dataProvider = "verifyProgramPageProvider", dependsOnMethods = {
            "verifySimpleProgramPage" }, alwaysRun = true)
    public void verifyProgramPage(ProgramPageBase page)
    {
        page.goTo();
        verifyThat(page.getSummary().getStatusReason(), hasTextInView(ProgramStatusReason.CUSTOMER_REQUEST));
    }

    @Test(dataProvider = "verifyAllEventsTabMemberCloseEventRow", dependsOnMethods = {
            "verifyProgramPage" }, alwaysRun = true)
    public void verifyAllEventsTabMemberCloseEventRow(AllEventsRow rowData)
    {
        allEventsPage.goTo();
        AllEventsGridRow row = allEventsPage.getGrid().getRow(rowData.getTransType(), rowData.getDetail());
        row.verify(rowData);
        row.expand(ProfileStatusGridRowContainer.class).verify(helper, ProgramStatusReason.CUSTOMER_REQUEST);
    }

    @Test(dependsOnMethods = { "verifyAllEventsTabMemberCloseEventRow" }, alwaysRun = true)
    public void tryToOpenAmbassadorProgram()
    {
        ambassadorPage = new AmbassadorPage();
        ambassadorPage.goTo();
        RenewExtendProgramSummary sum = ambassadorPage.getSummary();
        sum.setOpenStatus();
        verifyThat(sum.getStatus(), isHighlightedAsInvalid(true));
        sum.clickCancel();
    }

    @Test(dependsOnMethods = { "tryToOpenAmbassadorProgram" }, alwaysRun = true)
    public void openRCProgram()
    {
        rewardClubPage = new RewardClubPage();
        rcSummary = rewardClubPage.getSummary();
        rewardClubPage.goTo();
        rcSummary.setOpenStatus();
        rewardClubPage = new RewardClubPage();
        rcSummary = rewardClubPage.getSummary();
        verifyThat(rcSummary.getStatus(), hasTextInView(ProgramStatus.OPEN));
        verifyThat(rcSummary.getStatusReason(), displayed(false));
    }

    @Test(dependsOnMethods = { "openRCProgram" }, alwaysRun = true)
    public void verifyClosedAmbassadorProgram()
    {
        ambassadorPage = new AmbassadorPage();
        ambassadorPage.goTo();
        ambSummary = ambassadorPage.getSummary();
        verifyThat(ambSummary.getStatus(), hasTextInView(ProgramStatus.CLOSED));
        verifyThat(ambSummary.getStatusReason(), hasTextInView(ProgramStatusReason.CUSTOMER_REQUEST));
    }

    @Test(dependsOnMethods = { "verifyClosedAmbassadorProgram" }, alwaysRun = true)
    public void openAMBProgram()
    {
        ambSummary.setOpenStatus();
    }

    @Test(dataProvider = "verifyAllEventsTabMemberOpenEventRow", dependsOnMethods = {
            "openAMBProgram" }, alwaysRun = true)
    public void verifyAllEventsTabMemberOpenEventRow(AllEventsRow rowData)
    {
        allEventsPage.goTo();
        AllEventsGridRow row = allEventsPage.getGrid().getRow(rowData.getTransType(), rowData.getDetail());
        row.verify(rowData);
        row.expand(ProfileStatusGridRowContainer.class).verify(helper, Constant.NOT_AVAILABLE);
    }

}
