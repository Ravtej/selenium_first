package com.ihg.crm.automation.selenium.tests.servicecenter.meeting;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.event.EventTransactionType.Deposit.MEETING_BONUS;
import static com.ihg.automation.selenium.common.event.EventTransactionType.Order.ORDER_SYSTEM;
import static com.ihg.automation.selenium.common.event.EventTransactionType.TierLevel.UPGRADE_ACHIEVE;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.enabled;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.ihg.automation.common.DateUtils;
import com.ihg.automation.selenium.common.Constant;
import com.ihg.automation.selenium.common.deposit.Deposit;
import com.ihg.automation.selenium.common.event.AllEventsRow;
import com.ihg.automation.selenium.common.event.AllEventsRowConverter;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.common.types.program.ProgramStatus;
import com.ihg.automation.selenium.common.types.program.RewardClubLevel;
import com.ihg.automation.selenium.common.types.program.RewardClubLevelReason;
import com.ihg.automation.selenium.servicecenter.pages.LeftPanel;
import com.ihg.automation.selenium.servicecenter.pages.annual.AnnualActivityPage;
import com.ihg.automation.selenium.servicecenter.pages.deposit.CreateDepositPopUp;
import com.ihg.automation.selenium.servicecenter.pages.deposit.DepositDetailsPopUp;
import com.ihg.automation.selenium.servicecenter.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsGridRow;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsPage;
import com.ihg.automation.selenium.servicecenter.pages.event.meeting.MeetingBonusEventGridRowContainer;
import com.ihg.automation.selenium.servicecenter.pages.event.meeting.MeetingEventGridRow;
import com.ihg.automation.selenium.servicecenter.pages.event.meeting.MeetingEventPage;
import com.ihg.automation.selenium.servicecenter.pages.event.meeting.MeetingEventRow;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.RewardClubPage;
import com.ihg.crm.automation.selenium.tests.servicecenter.LoginLogout;

public class MeetingBonusTest extends LoginLogout
{
    private CreateDepositPopUp deposPopUp = new CreateDepositPopUp();
    private MeetingEventPage meetingEventsPage = new MeetingEventPage();
    private AllEventsPage allEventsPage = new AllEventsPage();
    private AllEventsGridRow meetingBonusAllEventsRow;
    private Member member = new Member();
    private Deposit testDeposit = new Deposit();
    private MeetingEventRow meetingEventRow;
    private MeetingBonusEventGridRowContainer meetingBonusEventContainer;
    private AllEventsRow orderSystemEvent = new AllEventsRow(DateUtils.getFormattedDate(), ORDER_SYSTEM,
            "16 GOLD ACHIEVED KIT (POINTS)", "", "");
    private AllEventsRow upgradeAchieveEvent = new AllEventsRow(DateUtils.getFormattedDate(), UPGRADE_ACHIEVE,
            "RC - GOLD", "", "");

    @BeforeClass
    public void beforeClass()
    {
        testDeposit.setTransactionId("MPIBNF");
        testDeposit.setTransactionType(MEETING_BONUS);
        testDeposit.setTransactionName("MEETING PLANNER INTERNATIONAL");
        testDeposit.setTransactionDescription("MEETING PLANNER INTERNATIONAL BONUS OFFER");
        testDeposit.setHotel("ATLCP");
        testDeposit.setCheckInDate(DateUtils.getDateBackward(5));
        testDeposit.setCheckOutDate(DateUtils.getDateBackward(2));
        testDeposit.setLoyaltyUnitsAmount("20000");
        testDeposit.setLoyaltyUnitsType(Constant.RC_POINTS);

        meetingEventRow = new MeetingEventRow("Meeting Bonus", "ATLCP", testDeposit.getCheckInDate(),
                testDeposit.getCheckOutDate(), "", testDeposit.getLoyaltyUnitsAmount(), "");

        member.setPersonalInfo(MemberPopulateHelper.getSimplePersonalInfo());
        member.addAddress(MemberPopulateHelper.getUnitedStatesAddress());
        member.addProgram(Program.RC);

        login();
        member = new AnnualActivityPage().captureCounters(new EnrollmentPage().enroll(member), jdbcTemplate);
        new LeftPanel().verifyRCLevel(RewardClubLevel.CLUB);
    }

    @Test
    public void openCreateDepositPopUp()
    {
        deposPopUp.goTo();
    }

    @Test(dependsOnMethods = { "openCreateDepositPopUp" })
    public void verifyCreateDepositPopUpControls()
    {
        deposPopUp.verifyControls();
    }

    @Test(dependsOnMethods = { "verifyCreateDepositPopUpControls" }, alwaysRun = true)
    public void verifyCreateDepositFieldsBehaviour()
    {
        deposPopUp.setDepositData(testDeposit);
        verifyThat(deposPopUp.getPartnerComment(), enabled(false));
        verifyThat(deposPopUp.getPartnerTransactionDate(), enabled(false));
        verifyThat(deposPopUp.getOrderTrackingNumber(), enabled(false));
    }

    @Test(dependsOnMethods = { "verifyCreateDepositFieldsBehaviour" }, alwaysRun = true)
    public void closeCreateDepositPopUp()
    {
        deposPopUp.close();
    }

    @Test(dependsOnMethods = { "closeCreateDepositPopUp" }, alwaysRun = true)
    public void createDeposit()
    {
        new CreateDepositPopUp().createDeposit(testDeposit);
    }

    @Test(dependsOnMethods = { "createDeposit" }, alwaysRun = true)
    public void verifyMeetingBonusEventRowWithDetails()
    {
        MeetingEventGridRow row = meetingEventsPage.getGrid().getRow(1);
        row.verify(meetingEventRow);
        meetingBonusEventContainer = row.expand(MeetingBonusEventGridRowContainer.class);
        meetingBonusEventContainer.verify(testDeposit, helper);
    }

    @Test(dependsOnMethods = { "verifyMeetingBonusEventRowWithDetails" }, alwaysRun = true)
    public void verifyDepositDetailsFromMeetingPage()
    {
        meetingBonusEventContainer.clickDetails();

        verifyDepositDetails();
    }

    @Test(dependsOnMethods = { "verifyDepositDetailsFromMeetingPage" }, alwaysRun = true)
    public void closeMeetingBonusEventDetailsPopUp()
    {
        new DepositDetailsPopUp().close();
    }

    @Test(dependsOnMethods = { "closeMeetingBonusEventDetailsPopUp" }, alwaysRun = true)
    public void verifyAllEventsMeetingBonusEventRow()
    {
        allEventsPage.goTo();
        meetingBonusAllEventsRow = allEventsPage.getGrid().getRow(MEETING_BONUS);
        meetingBonusAllEventsRow.verify(AllEventsRowConverter.convert(testDeposit));
    }

    @Test(dependsOnMethods = { "verifyAllEventsMeetingBonusEventRow" }, alwaysRun = true)
    public void verifyAllEventsMeetingBonusEventRowDetails()
    {
        allEventsPage.goTo();
        meetingBonusAllEventsRow = allEventsPage.getGrid().getRow(MEETING_BONUS);
        meetingBonusEventContainer = meetingBonusAllEventsRow.expand(MeetingBonusEventGridRowContainer.class);
        meetingBonusEventContainer.verify(testDeposit, helper);
    }

    @Test(dependsOnMethods = { "verifyAllEventsMeetingBonusEventRowDetails" }, alwaysRun = true)
    public void verifyDepositDetailsFromAllEventsPage()
    {
        meetingBonusEventContainer.clickDetails();

        verifyDepositDetails();
    }

    @Test(dependsOnMethods = { "verifyDepositDetailsFromAllEventsPage" }, alwaysRun = true)
    public void closeAllEventsMeetingBonusEventDetailsPopUp()
    {
        new DepositDetailsPopUp().close();
    }

    @DataProvider(name = "verifyAllEventsEventProvider")
    private Object[][] verifyAllEventsEventProvider()
    {
        return new Object[][] { { orderSystemEvent }, { upgradeAchieveEvent } };
    }

    @Test(dataProvider = "verifyAllEventsEventProvider", dependsOnMethods = {
            "closeAllEventsMeetingBonusEventDetailsPopUp" }, alwaysRun = true)
    public void verifyAllEventsMeetingBonusRelatedEvents(AllEventsRow eventRow)
    {
        allEventsPage.getGrid().getRow(eventRow.getTransType(), eventRow.getDetail()).verify(eventRow);
    }

    @Test(dependsOnMethods = { "verifyAllEventsMeetingBonusRelatedEvents" }, alwaysRun = true)
    public void verifyResultingLeftPanelProgramInformation()
    {
        new LeftPanel().getProgramInfoPanel().getPrograms().getRewardClubProgram().verify(RewardClubLevel.GOLD,
                "20,000");
    }

    @Test(dependsOnMethods = { "verifyResultingLeftPanelProgramInformation" }, alwaysRun = true)
    public void verifyResultingActivities()
    {
        member.getLifetimeActivity().setBonusUnits(20000);
        member.getLifetimeActivity().setTotalUnits(20000);
        member.getLifetimeActivity().setCurrentBalance(20000);
        member.getAnnualActivity().setTotalUnits(20000);
        member.getAnnualActivity().setTotalQualifiedUnits(20000);

        new AnnualActivityPage().verifyCounters(member);
    }

    @Test(dependsOnMethods = { "verifyResultingActivities" }, alwaysRun = true)
    public void verifyResultingTierLevelAndQualifyingPoints()
    {
        RewardClubPage rewardClubPage = new RewardClubPage();
        rewardClubPage.goTo();
        rewardClubPage.getSummary().verify(ProgramStatus.OPEN, RewardClubLevel.GOLD, RewardClubLevelReason.POINTS);
        rewardClubPage.getCurrentYearAnnualActivities().getPointsRow().verifyMaintainIsAchieved(20000,
                achievePlatinumPoints);

    }

    private void verifyDepositDetails()
    {
        DepositDetailsPopUp depositDetailsPopUp = new DepositDetailsPopUp();
        depositDetailsPopUp.getDepositDetailsTab().getDepositDetails().verify(testDeposit);
        depositDetailsPopUp.getEarningDetailsTab().verifyForBaseUnitsEvent("20,000", "20,000");
        depositDetailsPopUp.getBillingDetailsTab().verifyBaseUSDBilling(MEETING_BONUS, "80", testDeposit.getHotel());
    }

}
