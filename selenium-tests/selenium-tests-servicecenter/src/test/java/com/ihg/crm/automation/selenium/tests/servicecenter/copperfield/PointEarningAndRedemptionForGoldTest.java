package com.ihg.crm.automation.selenium.tests.servicecenter.copperfield;

import static com.ihg.automation.common.DateUtils.now;
import static com.ihg.automation.selenium.common.types.program.RewardClubLevel.GOLD;
import static com.ihg.crm.automation.selenium.tests.servicecenter.copperfield.VerificationDatesUtils.verifyDatesWithDefaultExpiration;

import com.ihg.automation.selenium.common.types.program.RewardClubLevelReason;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.RewardClubPage;

public class PointEarningAndRedemptionForGoldTest extends PointEarningAndRedemptionBase
{
    @Override
    protected void postEnrollActions()
    {
        RewardClubPage rewardClubPage = new RewardClubPage();
        rewardClubPage.goTo();
        rewardClubPage.getSummary().setTierLevelWithExpiration(GOLD, RewardClubLevelReason.BASE);
    }

    @Override
    protected void verifyAfterPointEvent()
    {
        verifyDatesWithDefaultExpiration(now());
    }
}
