package com.ihg.crm.automation.selenium.tests.servicecenter.programinfo;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.event.EventTransactionType.Membership.MEMBERSHIP_CLOSE;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.isDisplayedWithWait;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.isHighlightedAsInvalid;
import static com.ihg.automation.selenium.matchers.component.HasSelectItems.hasSelectItems;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;

import java.util.Arrays;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.common.DateUtils;
import com.ihg.automation.selenium.common.event.AllEventsRow;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.common.types.program.ProgramStatus;
import com.ihg.automation.selenium.common.types.program.ProgramStatusReason;
import com.ihg.automation.selenium.common.types.program.RewardClubLevel;
import com.ihg.automation.selenium.gwt.components.input.Select;
import com.ihg.automation.selenium.servicecenter.pages.LeftPanel;
import com.ihg.automation.selenium.servicecenter.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsGridRow;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsPage;
import com.ihg.automation.selenium.servicecenter.pages.event.status.ProfileStatusGridRowContainer;
import com.ihg.automation.selenium.servicecenter.pages.programs.ProgramSummary;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.RewardClubPage;
import com.ihg.crm.automation.selenium.tests.servicecenter.LoginLogout;

public class ChangeGuestAccountStatusTest extends LoginLogout
{
    private Member member = new Member();
    private RewardClubPage rewardClubPage = new RewardClubPage();
    private ProgramSummary summary;
    private AllEventsPage allEventsPage = new AllEventsPage();
    private AllEventsRow closedStatusEventRow;

    @BeforeClass
    public void beforeClass()
    {
        member.setPersonalInfo(MemberPopulateHelper.getSimplePersonalInfo());
        member.addAddress(MemberPopulateHelper.getUnitedStatesAddress());
        member.addProgram(Program.RC);
        closedStatusEventRow = new AllEventsRow(DateUtils.getFormattedDate(), MEMBERSHIP_CLOSE, Program.RC.getCode(),
                "", "");

        login();

        member = new EnrollmentPage().enroll(member);
    }

    @Test
    public void verifyInitialStatus()
    {
        rewardClubPage.goTo();
        summary = rewardClubPage.getSummary();
        summary.verify(ProgramStatus.OPEN, RewardClubLevel.CLUB);
    }

    @Test(dependsOnMethods = { "verifyInitialStatus" }, alwaysRun = true)
    public void navigateSummaryPage()
    {
        summary.clickEdit();
    }

    @Test(dependsOnMethods = { "navigateSummaryPage" })
    public void verifyNotEditableInformation()
    {
        verifyThat(summary.getMemberId(), hasText(member.getRCProgramId()));
        verifyThat(summary.getUnitsBalance(), hasText("0"));
    }

    @Test(dependsOnMethods = { "navigateSummaryPage" })
    public void verifyStatusDropDownListContent()
    {
        verifyThat(summary.getStatus(), hasSelectItems(Arrays.asList(ProgramStatus.OPEN, ProgramStatus.CLOSED)));
    }

    @Test(dependsOnMethods = { "navigateSummaryPage", "verifyNotEditableInformation",
            "verifyStatusDropDownListContent" }, alwaysRun = true)
    public void verifyReasonDropDownListContent()
    {
        summary.getStatus().select(ProgramStatus.CLOSED);

        Select statusReason = summary.getStatusReason();
        verifyThat(statusReason, isDisplayedWithWait());
        verifyThat(statusReason, hasText("Select"));
        verifyThat(statusReason,
                hasSelectItems(Arrays.asList(ProgramStatusReason.IHG_REQUEST, ProgramStatusReason.FROZEN,
                        ProgramStatusReason.DUPLICATE, ProgramStatusReason.CUSTOMER_REQUEST,
                        ProgramStatusReason.MUTUALLY_EXCLUSIVE)));
    }

    @Test(dependsOnMethods = { "verifyReasonDropDownListContent" }, alwaysRun = true)
    public void tryToSaveWithNoReasonSet()
    {
        summary.getStatus().select(ProgramStatus.CLOSED);
        summary.clickSave();
        verifyThat(summary.getStatusReason(), isHighlightedAsInvalid(true));
        summary.clickCancel();
    }

    @Test(dependsOnMethods = { "tryToSaveWithNoReasonSet" }, alwaysRun = true)
    public void setProfileStatusToClosed()
    {
        summary.setClosedStatus(ProgramStatusReason.FROZEN);
    }

    @Test(dependsOnMethods = { "setProfileStatusToClosed" }, alwaysRun = true)
    public void verifyResultingStatus()
    {
        rewardClubPage = new RewardClubPage();
        summary = rewardClubPage.getSummary();
        summary.verify(ProgramStatus.CLOSED, RewardClubLevel.CLUB);
        new LeftPanel().getCustomerInfoPanel().verifyStatus(ProgramStatus.CLOSED);
    }

    @Test(dependsOnMethods = { "verifyResultingStatus" }, alwaysRun = true)
    public void verifyAllEventsGridRow()
    {
        allEventsPage.goTo();
        AllEventsGridRow row = allEventsPage.getGrid().getRow(closedStatusEventRow.getTransType(),
                closedStatusEventRow.getDetail());
        row.verify(closedStatusEventRow);
    }

    @Test(dependsOnMethods = { "verifyAllEventsGridRow" }, alwaysRun = true)
    public void verifyAllEventsGridRowDetails()
    {
        allEventsPage.goTo();
        AllEventsGridRow row = allEventsPage.getGrid().getRow(closedStatusEventRow.getTransType(),
                closedStatusEventRow.getDetail());
        row.expand(ProfileStatusGridRowContainer.class).verify(helper, ProgramStatusReason.FROZEN);
    }

}
