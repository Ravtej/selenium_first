package com.ihg.crm.automation.selenium.tests.servicecenter.account;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.types.program.Program.AMB;
import static com.ihg.automation.selenium.common.types.program.Program.EMP;
import static com.ihg.automation.selenium.common.types.program.Program.IHG;
import static com.ihg.automation.selenium.common.types.program.Program.KAR;
import static com.ihg.automation.selenium.common.types.program.Program.RC;
import static com.ihg.automation.selenium.matchers.component.ViewableMatcher.hasTextInView;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.member.AmbassadorOfferCode;
import com.ihg.automation.selenium.common.member.AmbassadorUtils;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.types.program.ProgramStatus;
import com.ihg.automation.selenium.common.types.program.ProgramStatusReason;
import com.ihg.automation.selenium.servicecenter.pages.LeftPanel;
import com.ihg.automation.selenium.servicecenter.pages.account.AccountInfoPage;
import com.ihg.automation.selenium.servicecenter.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.servicecenter.pages.programs.EmployeePage;
import com.ihg.automation.selenium.servicecenter.pages.programs.GuestAccountPage;
import com.ihg.automation.selenium.servicecenter.pages.programs.KarmaPage;
import com.ihg.automation.selenium.servicecenter.pages.programs.ProgramPageBase;
import com.ihg.automation.selenium.servicecenter.pages.programs.amb.AmbassadorPage;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.RewardClubPage;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.RewardClubSummary;
import com.ihg.crm.automation.selenium.tests.servicecenter.LoginLogout;

public class DisplayDerivedGuestStatusTest extends LoginLogout
{
    private Member member = new Member();
    private EmployeePage emplPage = new EmployeePage();
    private AmbassadorPage ambPage = new AmbassadorPage();
    private KarmaPage karmaPage = new KarmaPage();
    private GuestAccountPage gstAccPage = new GuestAccountPage();
    private RewardClubPage rwdPage = new RewardClubPage();
    private EnrollmentPage enrollmentPage = new EnrollmentPage();
    private AccountInfoPage accountInfo = new AccountInfoPage();

    @BeforeClass
    public void beforeClass()
    {
        member.addProgram(RC);
        member.setPersonalInfo(MemberPopulateHelper.getSimplePersonalInfo());
        member.addAddress(MemberPopulateHelper.getUnitedStatesAddress());

        login();
        enrollmentPage.enroll(member);
    }

    @DataProvider(name = "statusProvider")
    protected Object[][] statusProvider()
    {
        return new Object[][] { { emplPage, ProgramStatus.CLOSED }, { gstAccPage, ProgramStatus.OPEN },
                { rwdPage, ProgramStatus.CLOSED }, { ambPage, ProgramStatus.CLOSED } };
    }

    @DataProvider(name = "pageProvider")
    protected Object[][] pageProvider()
    {
        return new Object[][] { { emplPage }, { gstAccPage }, { rwdPage }, { ambPage } };
    }

    @Test(priority = 1)
    public void verifyStatusAfterEnrollmentToRC()
    {
        accountInfo.verifyStatus(ProgramStatus.OPEN);
    }

    @Test(priority = 20)
    public void enrollMemberToAllPrograms()
    {
        enrollmentPage.enrollToAnotherProgram(member, IHG, EMP);

        member.setAmbassadorOfferCode(AmbassadorOfferCode.CHSRO);
        enrollmentPage.enrollToDROrAMBProgramWithoutProfileChange(member, AMB,
                AmbassadorUtils.AMBASSADOR_COMP_PROMO_$0);
    }

    @Test(priority = 25, groups = { "karma" })
    public void enrollToKarma()
    {
        enrollmentPage.enrollToAnotherProgram(member, KAR);
    }

    @Test(priority = 30)
    public void verifyStatusAfterEnrollmentToAllPrograms()
    {
        accountInfo.verifyStatus(ProgramStatus.OPEN);
    }

    @Test(priority = 40)
    public void closeEMPProgram()
    {
        emplPage.goTo();
        emplPage.getSummary().setClosedStatus(ProgramStatusReason.FROZEN);
    }

    @Test(priority = 50)
    public void verifyStatusAfterClosingEMPProgram()
    {
        accountInfo.verifyStatus(ProgramStatus.OPEN);
    }

    @Test(priority = 60)
    public void closeRCProgram()
    {
        rwdPage.goTo();
        RewardClubSummary summary = rwdPage.getSummary();
        summary.setClosedStatus(ProgramStatusReason.FROZEN);
        verifyThat(summary.getStatus(), hasTextInView(ProgramStatus.CLOSED));
    }

    @Test(priority = 70, dataProvider = "statusProvider")
    public void verifyAllProgramsStatusAfterRCClosing(ProgramPageBase page, String status)
    {
        page.goTo();
        verifyThat(page.getSummary().getStatus(), hasTextInView(status));
    }

    @Test(priority = 80, groups = { "karma" })
    public void verifyKarmaStatusAfterRCClosing()
    {
        karmaPage.goTo();
        verifyThat(karmaPage.getSummary().getStatus(), hasTextInView(ProgramStatus.CLOSED));
    }

    @Test(priority = 90)
    public void verifyStatusAfterClosingRCProgram()
    {
        accountInfo.verifyStatus(ProgramStatus.OPEN);
    }

    @Test(priority = 100)
    public void closeIHGProgram()
    {
        gstAccPage.goTo();
        gstAccPage.getSummary().setClosedStatus(ProgramStatusReason.FROZEN);
    }

    @Test(priority = 110)
    public void verifyClosedStatusAfterClosingIHGProgram()
    {
        accountInfo.verifyStatus(ProgramStatus.CLOSED);
        new LeftPanel().getCustomerInfoPanel().verifyStatus(ProgramStatus.CLOSED);
    }

    @Test(priority = 120)
    public void removeAccount()
    {
        accountInfo.removedAccount();
    }

    @Test(priority = 130, dataProvider = "pageProvider")
    public void verifyRemovedStatus(ProgramPageBase page)
    {
        page.goTo();
        verifyThat(page.getSummary().getStatus(), hasTextInView(ProgramStatus.REMOVED));
    }

    @Test(priority = 140, groups = { "karma" })
    public void verifyRemovedStatusForKarma()
    {
        karmaPage.goTo();
        verifyThat(karmaPage.getSummary().getStatus(), hasTextInView(ProgramStatus.REMOVED));
    }

}
