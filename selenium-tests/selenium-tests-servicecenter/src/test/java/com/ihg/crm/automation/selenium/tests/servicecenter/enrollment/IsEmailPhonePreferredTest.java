package com.ihg.crm.automation.selenium.tests.servicecenter.enrollment;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.pages.login.TopUserSection;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.servicecenter.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.servicecenter.pages.personal.PersonalInfoPage;
import com.ihg.crm.automation.selenium.tests.servicecenter.LoginLogout;

public class IsEmailPhonePreferredTest extends LoginLogout
{
    private EnrollmentPage enrollmentPage = new EnrollmentPage();
    private PersonalInfoPage personalInfoPage = new PersonalInfoPage();
    private Member member = new Member();

    @BeforeClass
    public void before()
    {
        member.addProgram(Program.RC);
        member.setPersonalInfo(MemberPopulateHelper.getSimplePersonalInfo());
        member.addAddress(MemberPopulateHelper.getUnitedStatesAddress());
        member.addPhone(MemberPopulateHelper.getPhone());
        member.addEmail(MemberPopulateHelper.getRandomEmail());

        login();
        enrollmentPage.enroll(member);
    }

    @AfterClass
    public void after()
    {
        new TopUserSection().signOff();
    }

    @Test
    public void phoneIsPreferred()
    {
        verifyThat(personalInfoPage.getPhoneList().getContact().getPreferredIcon(), displayed(true));
    }

    @Test
    public void emailIsPreferred()
    {
        verifyThat(personalInfoPage.getEmailList().getContact().getPreferredIcon(), displayed(true));
    }
}
