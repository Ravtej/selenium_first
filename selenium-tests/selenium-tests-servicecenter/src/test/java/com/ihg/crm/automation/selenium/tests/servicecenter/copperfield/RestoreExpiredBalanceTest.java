package com.ihg.crm.automation.selenium.tests.servicecenter.copperfield;

import static com.ihg.automation.common.DateUtils.DATE_FORMAT;
import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyNoError;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.Constant.NOT_AVAILABLE;
import static com.ihg.automation.selenium.common.event.EventTransactionType.PointExpiration.EXPIRED_POINTS_RESTORE;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.enabled;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasDefault;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasText;
import static org.apache.commons.lang3.StringUtils.EMPTY;
import static org.hamcrest.Matchers.allOf;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.common.DateUtils;
import com.ihg.automation.selenium.common.billing.EventBillingDetailsTab;
import com.ihg.automation.selenium.common.earning.EventEarningDetailsTab;
import com.ihg.automation.selenium.common.event.AllEventsRow;
import com.ihg.automation.selenium.common.event.EventTransactionType;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.gwt.components.dialog.ConfirmDialog;
import com.ihg.automation.selenium.matchers.component.GridSize;
import com.ihg.automation.selenium.servicecenter.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsGrid;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsGridRow;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsPage;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsSearch;
import com.ihg.automation.selenium.servicecenter.pages.event.point.ExpiredPointsRestoreDetailsPopUp;
import com.ihg.automation.selenium.servicecenter.pages.event.point.PointExpirationRowContainer;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.RewardClubPage;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.RewardClubSummary;
import com.ihg.crm.automation.selenium.tests.servicecenter.LoginLogout;

@Test(groups = { "copperfield" })
public class RestoreExpiredBalanceTest extends LoginLogout
{
    private RewardClubSummary summary;
    private PointExpirationRowContainer rowContainer;

    private AllEventsRow restorePointsEvent;

    private String expirationDate, lastActivityDate;

    private static final String EXPIRED_AMOUNT = "500";
    private final static String UPDATE_EXPIRED_POINT_BALANCE = "UPDATE DGST.MBRSHP SET"//
            + " EXPIRED_PT_BAL_AMT = '%1$s',"//
            + " LST_UPDT_USR_ID='%3$s',"//
            + " LST_UPDT_TS = SYSDATE"//
            + " WHERE MBRSHP_ID='%2$s' AND LYTY_PGM_CD='PC'";

    @BeforeClass
    public void beforeClass()
    {
        Member member = new Member();
        member.setPersonalInfo(MemberPopulateHelper.getSimplePersonalInfo());
        member.addAddress(MemberPopulateHelper.getUnitedStatesAddress());
        member.addProgram(Program.RC);

        login();

        member = new EnrollmentPage().enroll(member);

        jdbcTemplateUpdate.update(
                String.format(UPDATE_EXPIRED_POINT_BALANCE, EXPIRED_AMOUNT, member.getRCProgramId(), lastUpdateUserId));

        RewardClubPage rewardClubPage = new RewardClubPage();
        rewardClubPage.goTo();
        summary = rewardClubPage.getSummary();

        verifySummaryFieldsAfterUpdate();
    }

    @Test
    public void restoreBalanceClickNo()
    {
        summary.getRestore().clickAndWait(verifyNoError());
        ConfirmDialog dialog = new ConfirmDialog();
        verifyThat(dialog, hasText("Are you sure you want to restore the Expired Loyalty Unit balance?"));
        dialog.clickNo(verifyNoError());

        verifySummaryFieldsAfterUpdate();
    }

    @Test(dependsOnMethods = { "restoreBalanceClickNo" }, alwaysRun = true)
    public void restoreExpiredLoyaltyUnitBalance()
    {
        summary.getRestore().clickAndWait(verifyNoError());
        new ConfirmDialog().clickYes(verifyNoError());

        expirationDate = DateUtils.now().plusMonths(12).toString(DATE_FORMAT);
        lastActivityDate = DateUtils.now().toString(DATE_FORMAT);

        verifyThat(summary.getBalance(), hasText(EXPIRED_AMOUNT));
        verifyThat(summary.getExpiredLoyaltyUnitBalance(), hasText("0"));
        verifyThat(summary.getRestore(), allOf(displayed(true), enabled(false)));

        verifyThat(summary.getBalanceExpirationDate(), hasText(expirationDate));
        verifyThat(summary.getLastActivityDate(), hasText(lastActivityDate));
        verifyThat(summary.getLastAdjusted(), hasText(lastActivityDate));
    }

    @Test(dependsOnMethods = { "restoreExpiredLoyaltyUnitBalance" })
    public void verifyExpiredPointsRestoreEvent()
    {
        AllEventsPage allEventsPage = new AllEventsPage();
        allEventsPage.goTo();
        restorePointsEvent = new AllEventsRow(EXPIRED_POINTS_RESTORE, "Expired Points Restore");
        restorePointsEvent.setIhgUnits(EXPIRED_AMOUNT);
        restorePointsEvent.setAllianceUnits(EMPTY);

        AllEventsGridRow row = allEventsPage.getGrid().getRow(1);
        row.verify(restorePointsEvent);
        rowContainer = row.expand(PointExpirationRowContainer.class);
        rowContainer.getSource().verify(helper.getSourceFactory().getNoEmployeeIdSource(), NOT_AVAILABLE);
    }

    @Test(dependsOnMethods = { "restoreExpiredLoyaltyUnitBalance" }, alwaysRun = true)
    public void verifyExpiredPointsRestoreEventDetails()
    {
        rowContainer.clickDetails();

        ExpiredPointsRestoreDetailsPopUp restoreDetailsPopUp = new ExpiredPointsRestoreDetailsPopUp();
        restoreDetailsPopUp.getPointExpirationDetailsTab().verify(restorePointsEvent, helper);

        EventEarningDetailsTab earningDetailsTab = restoreDetailsPopUp.getEarningDetailsTab();
        earningDetailsTab.goTo();
        earningDetailsTab.verifyBasicDetailsForPoints(EXPIRED_AMOUNT, "0");

        EventBillingDetailsTab billingDetailsTab = restoreDetailsPopUp.getBillingDetailsTab();
        billingDetailsTab.goTo();
        billingDetailsTab.verifyTabIsEmpty(EXPIRED_POINTS_RESTORE);

        restoreDetailsPopUp.clickClose();
    }

    @Test(dependsOnMethods = { "verifyExpiredPointsRestoreEventDetails" }, alwaysRun = true)
    public void verifyExpiredPointsRestoreEventFiltering()
    {
        AllEventsPage allEventsPage = new AllEventsPage();
        allEventsPage.goTo();
        AllEventsSearch searchFields = allEventsPage.getSearchFields();
        searchFields.getEventType().select(EventTransactionType.PointExpiration.EVENT_TYPE);
        searchFields.getTransactionType().select(EXPIRED_POINTS_RESTORE);
        allEventsPage.getButtonBar().clickSearch();

        AllEventsGrid grid = allEventsPage.getGrid();
        verifyThat(grid, GridSize.size(1));
        verifyThat(grid.getRow(1).getCell(AllEventsGridRow.AllEventsCell.TRANS_TYPE), hasText(EXPIRED_POINTS_RESTORE));
    }

    private void verifySummaryFieldsAfterUpdate()
    {
        verifyThat(summary.getBalance(), hasText("0"));
        verifyThat(summary.getExpiredLoyaltyUnitBalance(), hasText(EXPIRED_AMOUNT));
        verifyThat(summary.getBalanceExpirationDate(), hasDefault());
        verifyThat(summary.getLastActivityDate(), hasDefault());
        verifyThat(summary.getLastAdjusted(), hasDefault());

        verifyThat(summary.getRestore(), allOf(displayed(true), enabled(true)));
    }
}
