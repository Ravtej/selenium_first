package com.ihg.crm.automation.selenium.tests.servicecenter.uniqueemail;

import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyError;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.isDisplayedWithWait;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.isHighlightedAsInvalid;
import static com.ihg.automation.selenium.servicecenter.pages.Messages.HIGHLIGHTED_FIELDS_DID_NOT_PASS_VALIDATION;
import static com.ihg.automation.selenium.common.enroll.EmailConstant.EMAIL_WITH_10_DUPLICATES;

import org.hamcrest.CoreMatchers;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.member.MemberJdbcUtils;
import com.ihg.automation.selenium.gwt.components.input.Input;
import com.ihg.automation.selenium.matchers.component.HasText;
import com.ihg.automation.selenium.servicecenter.pages.GuestSearch;
import com.ihg.automation.selenium.servicecenter.pages.account.AccountInfoPage;
import com.ihg.automation.selenium.servicecenter.pages.account.EmailPopUp;
import com.ihg.automation.selenium.servicecenter.pages.account.Security;
import com.ihg.crm.automation.selenium.tests.servicecenter.LoginLogout;

public class DuplicateEmailOnAccountInfoPageTest extends LoginLogout
{
    @BeforeClass
    public void before()
    {
        login();
        new GuestSearch().byMemberNumber(MemberJdbcUtils.getMemberIdWithoutEmail(jdbcTemplate));
    }

    @Test
    public void tryToSendPinWithDuplicateEmail()
    {
        AccountInfoPage accountInfoPage = new AccountInfoPage();
        accountInfoPage.goTo();
        Security security = accountInfoPage.getSecurity();
        security.clickEdit();
        security.getSendPin().clickAndWait();
        EmailPopUp emailPopUp = new EmailPopUp();
        verifyThat(emailPopUp, isDisplayedWithWait());
        Input email = emailPopUp.getEmail();
        email.type(EMAIL_WITH_10_DUPLICATES);
        verifyThat(emailPopUp, HasText.hasText(CoreMatchers.containsString(
                "The email address on the account is invalid. Please add a valid email address to the account to send a PIN.")));
        emailPopUp.clickSave(verifyError(HIGHLIGHTED_FIELDS_DID_NOT_PASS_VALIDATION));
        verifyThat(email, isHighlightedAsInvalid(true));
    }

}
