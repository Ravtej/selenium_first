package com.ihg.crm.automation.selenium.tests.servicecenter.uniqueemail;

import static com.ihg.automation.selenium.servicecenter.pages.Messages.EXCEEDS_THE_LIMIT;
import static com.ihg.automation.selenium.common.enroll.EmailConstant.EMAIL_WITH_10_DUPLICATES;
import static com.ihg.automation.selenium.common.enroll.EmailConstant.EMAIL_WITH_LESS_THAN_10_DUPLICATES;
import static com.ihg.automation.selenium.common.enroll.EmailConstant.EMAIL_WITH_MORE_THAN_10_DUPLICATES;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.enroll.DuplicateEmailFoundPopUp;
import com.ihg.automation.selenium.common.member.MemberJdbcUtils;
import com.ihg.automation.selenium.common.personal.EmailContact;
import com.ihg.automation.selenium.servicecenter.pages.GuestSearch;
import com.ihg.automation.selenium.servicecenter.pages.personal.PersonalInfoPage;
import com.ihg.crm.automation.selenium.tests.servicecenter.LoginLogout;

public class DuplicateEmailOnPersonalInfoPageTest extends LoginLogout
{
    private EmailContact contact;

    @BeforeClass
    public void before()
    {
        login();
        new GuestSearch().byMemberNumber(MemberJdbcUtils.getOpenedMemberId(jdbcTemplate));

        PersonalInfoPage personalInfoPage = new PersonalInfoPage();
        personalInfoPage.goTo();
        contact = personalInfoPage.getEmailList().getContact();
        contact.clickEdit();
    }

    @DataProvider(name = "emailProvider")
    public Object[][] emailProvider()
    {
        return new Object[][] { { EMAIL_WITH_MORE_THAN_10_DUPLICATES, EXCEEDS_THE_LIMIT },
                { EMAIL_WITH_10_DUPLICATES, EXCEEDS_THE_LIMIT }, { EMAIL_WITH_LESS_THAN_10_DUPLICATES, "" } };
    }

    @Test(dataProvider = "emailProvider")
    public void tryToUpdateMemberByDuplicatedEmail(String duplicateEmail, String message)
    {
        contact.getBaseContact().getEmail().type(duplicateEmail);
        contact.clickSave();

        DuplicateEmailFoundPopUp duplicateEmailFoundPopUp = new DuplicateEmailFoundPopUp();
        duplicateEmailFoundPopUp.verifyWarningMessage(message);
        duplicateEmailFoundPopUp.clickUpdate();
    }
}
