package com.ihg.crm.automation.selenium.tests.servicecenter.programinfo.karma;

import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyError;
import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyNoError;
import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifySuccess;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.types.program.KarmaLevel.KIC;
import static com.ihg.automation.selenium.common.types.program.ProgramStatus.CLOSED;
import static com.ihg.automation.selenium.common.types.program.ProgramStatus.OPEN;
import static com.ihg.automation.selenium.common.types.program.ProgramStatusReason.FROZEN;
import static com.ihg.automation.selenium.common.types.program.RewardClubLevel.CLUB;
import static com.ihg.automation.selenium.common.types.program.RewardClubLevel.GOLD;
import static com.ihg.automation.selenium.common.types.program.RewardClubLevel.PLTN;
import static com.ihg.automation.selenium.common.types.program.RewardClubLevel.SPRE;
import static com.ihg.automation.selenium.common.types.program.RewardClubLevelReason.BASE;
import static com.ihg.automation.selenium.common.types.program.TierLevelExpiration.EXPIRE_CURRENT_YEAR;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static com.ihg.automation.selenium.servicecenter.pages.personal.PersonalInfoPage.SUCCESS_MESSAGE;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.common.types.program.RewardClubLevel;
import com.ihg.automation.selenium.listener.Verifier;
import com.ihg.automation.selenium.servicecenter.pages.LeftPanel;
import com.ihg.automation.selenium.servicecenter.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.servicecenter.pages.programs.KarmaPage;
import com.ihg.automation.selenium.servicecenter.pages.programs.ProgramSummary;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.RewardClubPage;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.RewardClubSummary;
import com.ihg.crm.automation.selenium.tests.servicecenter.LoginLogout;

@Test(groups = { "karma" })
public class UpdateRCTierLevelForKarmaMemberTest extends LoginLogout
{
    public static final String DOWNGRADE_NOT_ALLOWED_WARNING_MESSAGE = "Inner Circle members cannot be downgraded below Spire Elite.";
    private RewardClubPage rewardClubPage = new RewardClubPage();
    private KarmaPage karmaPage = new KarmaPage();
    private RewardClubSummary rewardClubSummary;
    private ProgramSummary karmaSummary;

    @BeforeClass
    public void beforeClass()
    {
        Member member = new Member();
        member.setPersonalInfo(MemberPopulateHelper.getSimplePersonalInfo());
        member.addAddress(MemberPopulateHelper.getUnitedStatesAddress());
        member.addProgram(Program.KAR);

        login();

        new EnrollmentPage().enroll(member);

        verifyThat(new LeftPanel().getProgramInfoPanel().getPrograms().getRewardClubProgram().getLevelCode(),
                hasText(CLUB.getCode()));
    }

    @DataProvider(name = "levelProvider")
    protected Object[][] levelProvider()
    {
        return new Object[][] { { PLTN }, { GOLD }, { CLUB } };
    }

    @Test(priority = 10)
    public void upgradeToSpireForKarLevel()
    {
        rewardClubPage.goTo();
        rewardClubSummary = rewardClubPage.getSummary();
        rewardClubSummary.setTierLevelWithExpiration(SPRE, BASE, verifyNoError(), verifySuccess(SUCCESS_MESSAGE));
    }

    @Test(priority = 15, dataProvider = "levelProvider")
    public void downgradeRCLevelForKar(RewardClubLevel level)
    {
        setTierLevel(level, verifyNoError(), verifySuccess(SUCCESS_MESSAGE));
        rewardClubSummary.verify(OPEN, level);
    }

    @Test(priority = 20)
    public void setKicLevel()
    {
        karmaPage.goTo();
        karmaSummary = karmaPage.getSummary();
        karmaSummary.setTierLevelWithExpiration(KIC, BASE, EXPIRE_CURRENT_YEAR, verifyNoError(),
                verifySuccess(SUCCESS_MESSAGE));

        verifyThat(new LeftPanel().getProgramInfoPanel().getPrograms().getRewardClubProgram().getLevelCode(),
                hasText(SPRE.getCode()));
    }

    @Test(priority = 25, dataProvider = "levelProvider")
    public void tryToDowngradeSpireLevelForKic(RewardClubLevel level)
    {
        rewardClubPage.goTo();

        setTierLevel(level, verifyError(DOWNGRADE_NOT_ALLOWED_WARNING_MESSAGE));
        rewardClubSummary.clickCancel();
        rewardClubSummary.verify(OPEN, SPRE);
    }

    @Test(priority = 30)
    public void closeKarmaProgram()
    {
        karmaPage.goTo();
        karmaSummary.setClosedStatus(FROZEN);
        karmaSummary.verify(CLOSED, KIC);
    }

    @Test(priority = 35, dataProvider = "levelProvider")
    public void downgradeSpireLevelForClosedKicLevel(RewardClubLevel level)
    {
        rewardClubPage.goTo();

        setTierLevel(level, verifyNoError(), verifySuccess(SUCCESS_MESSAGE));
        rewardClubSummary.verify(OPEN, level);
    }

    @Test(priority = 40)
    public void reopenKarmaProgramAndVerifyUpgradedSpireRCLevel()
    {
        verifyThat(new LeftPanel().getProgramInfoPanel().getPrograms().getRewardClubProgram().getLevelCode(),
                hasText(CLUB.getCode()));

        karmaPage.goTo();
        karmaSummary.setOpenStatus(verifyNoError());
        karmaSummary.verify(OPEN, KIC);

        verifyThat(new LeftPanel().getProgramInfoPanel().getPrograms().getRewardClubProgram().getLevelCode(),
                hasText(SPRE.getCode()));
    }

    private void setTierLevel(RewardClubLevel level, Verifier... verifiers)
    {
        if (CLUB.equals(level))
        {
            rewardClubSummary.setTierLevelWithoutExpiration(level, BASE, verifiers);
        }
        else
        {
            rewardClubSummary.setTierLevelWithExpiration(level, BASE, verifiers);
        }
    }
}
