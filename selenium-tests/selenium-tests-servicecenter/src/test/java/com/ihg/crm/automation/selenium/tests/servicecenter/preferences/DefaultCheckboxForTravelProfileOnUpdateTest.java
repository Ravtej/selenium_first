package com.ihg.crm.automation.selenium.tests.servicecenter.preferences;

import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyNoError;
import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifySuccess;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.isSelected;
import static org.hamcrest.core.AllOf.allOf;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.preferences.TravelProfileType;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.gwt.components.checkbox.CheckBox;
import com.ihg.automation.selenium.servicecenter.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.servicecenter.pages.preferences.AddTravelProfilePopUp;
import com.ihg.automation.selenium.servicecenter.pages.preferences.EditTravelProfilePopUp;
import com.ihg.automation.selenium.servicecenter.pages.preferences.StayPreferencesPage;
import com.ihg.automation.selenium.servicecenter.pages.preferences.TravelProfileGridRow;
import com.ihg.automation.selenium.servicecenter.pages.preferences.TravelProfilePopUpBase;
import com.ihg.crm.automation.selenium.tests.servicecenter.LoginLogout;

public class DefaultCheckboxForTravelProfileOnUpdateTest extends LoginLogout
{
    private static final String TP_NAME = "Test10";
    private StayPreferencesPage stayPreferencesPage = new StayPreferencesPage();
    private AddTravelProfilePopUp addTravelProfilePopUp = new AddTravelProfilePopUp();
    private EditTravelProfilePopUp editTravelProfilePopUp = new EditTravelProfilePopUp();
    private TravelProfileGridRow row;

    @BeforeClass
    public void beforeClass()
    {
        Member member = new Member();
        member.addProgram(Program.RC);
        member.addAddress(MemberPopulateHelper.getUnitedStatesAddress());
        member.setPersonalInfo(MemberPopulateHelper.getSimplePersonalInfo());

        login();
        new EnrollmentPage().enroll(member);

        stayPreferencesPage.goTo();
        stayPreferencesPage.getTravelProfilesGrid();
        stayPreferencesPage.getAddTravelProfile().clickAndWait();
        addTravelProfilePopUp.getTravelProfileName().type(TP_NAME);
        addTravelProfilePopUp.getType().select(TravelProfileType.LEISURE.getCodeWithValue());
        addTravelProfilePopUp.clickSave(verifyNoError(), verifySuccess(TravelProfilePopUpBase.SUCCESS_MESSAGE_CREATE));
    }

    @Test
    public void checkImageForDefaultTravelProfile()
    {
        row = stayPreferencesPage.getTravelProfilesGrid().getRowByName(TravelProfileType.LEISURE.getValue());

        stayPreferencesPage.goTo();
        editTravelProfilePopUp = row.openTravelProfile();
        verifyThat(editTravelProfilePopUp.getDefaultIndicator(), displayed(true));
        editTravelProfilePopUp.clickClose(verifyNoError());
    }

    @Test(dependsOnMethods = { "checkImageForDefaultTravelProfile" }, alwaysRun = true)
    public void checkCheckboxForNonDefaultTravelProfile()
    {
        row = stayPreferencesPage.getTravelProfilesGrid().getRowByName(TP_NAME);

        editTravelProfilePopUp = row.openTravelProfile();
        CheckBox defaultCheckbox = editTravelProfilePopUp.getDefaultCheckbox();
        verifyThat(defaultCheckbox, allOf(displayed(true), isSelected(false)));

        defaultCheckbox.check();
        editTravelProfilePopUp.clickSave(verifyNoError(), verifySuccess(TravelProfilePopUpBase.SUCCESS_MESSAGE_UPDATE));
    }

    @Test(dependsOnMethods = { "checkCheckboxForNonDefaultTravelProfile" }, alwaysRun = true)
    public void checkCheckboxForDefaultTravelProfile()
    {
        row = stayPreferencesPage.getTravelProfilesGrid().getRowByName(TravelProfileType.LEISURE.getValue());

        editTravelProfilePopUp = row.openTravelProfile();
        CheckBox defaultCheckbox = editTravelProfilePopUp.getDefaultCheckbox();
        verifyThat(editTravelProfilePopUp.getDefaultIndicator(), displayed(false));
        verifyThat(defaultCheckbox, allOf(displayed(true), isSelected(false)));
        editTravelProfilePopUp.clickClose(verifyNoError());
    }

    @Test(dependsOnMethods = { "checkCheckboxForDefaultTravelProfile" }, alwaysRun = true)
    public void checkIndicatorForSecondaryTravelProfile()
    {
        row = stayPreferencesPage.getTravelProfilesGrid().getRowByName(TP_NAME);

        editTravelProfilePopUp = row.openTravelProfile();
        verifyThat(editTravelProfilePopUp.getDefaultIndicator(), displayed(true));
        verifyThat(editTravelProfilePopUp.getDefaultCheckbox(), displayed(false));
        editTravelProfilePopUp.clickClose(verifyNoError());
    }
}
