package com.ihg.crm.automation.selenium.tests.servicecenter.catalog;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.catalog.CatalogItem.builder;
import static com.ihg.automation.selenium.common.event.EventTransactionType.Order.REDEEM;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasTextAsInt;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.common.DateUtils;
import com.ihg.automation.selenium.common.ProgramInfoPanel;
import com.ihg.automation.selenium.common.catalog.CatalogItem;
import com.ihg.automation.selenium.common.event.AllEventsRowConverter;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.order.Order;
import com.ihg.automation.selenium.common.order.OrderItem;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.servicecenter.pages.catalog.CatalogItemDetailsPopUp;
import com.ihg.automation.selenium.servicecenter.pages.catalog.CatalogPage;
import com.ihg.automation.selenium.servicecenter.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.servicecenter.pages.event.order.OrderEventsPage;
import com.ihg.automation.selenium.servicecenter.pages.event.order.OrderGridRow;
import com.ihg.automation.selenium.servicecenter.pages.event.order.OrderGridRow.OrderCell;
import com.ihg.automation.selenium.servicecenter.pages.order.OrderSummaryPopUp;
import com.ihg.automation.selenium.servicecenter.pages.unit.GoodwillPopUp;

public class MultipleOrderTest extends CatalogCommon
{
    private Member member = new Member();
    private CatalogPage catalogPage = new CatalogPage();
    private final static CatalogItem FIRST_CATALOG_ITEM = builder().itemId("PF500").loyaltyUnitCost("10000").build();
    private final static CatalogItem SECOND_CATALOG_ITEM = builder().itemId("PT103").loyaltyUnitCost("20000").build();
    private String POINTS = "50000";
    private OrderEventsPage orderPage = new OrderEventsPage();
    private Order order = new Order();

    @BeforeClass
    public void beforeClass()
    {
        OrderItem firstOrderItem = new OrderItem(FIRST_CATALOG_ITEM.getItemId());
        firstOrderItem.setQuantity("1");
        firstOrderItem.setLoyaltyUnits(FIRST_CATALOG_ITEM.getLoyaltyUnitCost());

        OrderItem secondOrderItem = new OrderItem(SECOND_CATALOG_ITEM.getItemId());
        secondOrderItem.setQuantity("1");
        secondOrderItem.setLoyaltyUnits(SECOND_CATALOG_ITEM.getLoyaltyUnitCost());

        order.getOrderItems().add(firstOrderItem);
        order.getOrderItems().add(secondOrderItem);
        order.setTotalLoyaltyUnits(String.valueOf(Integer.parseInt(firstOrderItem.getLoyaltyUnits())
                + Integer.parseInt(secondOrderItem.getLoyaltyUnits())));
        order.setTransactionType(REDEEM);
        order.setTransactionDate(DateUtils.getFormattedDate());

        member.setPersonalInfo(MemberPopulateHelper.getSimplePersonalInfo());
        member.addAddress(MemberPopulateHelper.getUnitedStatesAddress());
        member.addProgram(Program.RC);

        login();

        new EnrollmentPage().enroll(member);
        new GoodwillPopUp().goodwillPoints(POINTS);

        catalogPage.goTo();
    }

    @Test
    public void verifyMultipleOrder()
    {
        catalogPage.searchByItemId(FIRST_CATALOG_ITEM);
        CatalogItemDetailsPopUp catalogItemDetailsPopUp = catalogPage.openCatalogItemDetailsPopUp();

        OrderSummaryPopUp orderSummaryPopUp = catalogItemDetailsPopUp.openOrderSummaryPopUp();
        orderSummaryPopUp.clickContinueShopping();

        verifyThat(orderSummaryPopUp, displayed(false));
        verifyThat(catalogPage, displayed(true));

        catalogPage.searchByItemId(SECOND_CATALOG_ITEM);
        catalogItemDetailsPopUp = catalogPage.openCatalogItemDetailsPopUp();
        orderSummaryPopUp = catalogItemDetailsPopUp.openOrderSummaryPopUp();
        orderSummaryPopUp.clickCheckout();

        verifyThat(new ProgramInfoPanel().getPrograms().getRewardClubProgram().getPointsBalance(),
                hasTextAsInt(Integer.parseInt(POINTS) - Integer.parseInt(order.getTotalLoyaltyUnits())));

        OrderGridRow itemRow = orderPage.getOrdersGrid().getRow(1);

        itemRow.verify(order);

        itemRow.verifyRowGreenColor();

        order.setOrderNumber(itemRow.getCell(OrderCell.ORDER_NUMBER).getText());
    }

    @Test(dependsOnMethods = { "verifyMultipleOrder" }, alwaysRun = true)
    public void verifyRedeemEventOnAllEvents()
    {
        verifyRedeemEventOnAllEventsPage(AllEventsRowConverter.getRedeemEvent(order));
    }
}
