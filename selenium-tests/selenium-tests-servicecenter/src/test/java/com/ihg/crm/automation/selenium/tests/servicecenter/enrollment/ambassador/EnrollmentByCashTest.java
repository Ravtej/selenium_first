package com.ihg.crm.automation.selenium.tests.servicecenter.enrollment.ambassador;

import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyNoError;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.isDisplayedWithWait;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.common.DateUtils;
import com.ihg.automation.selenium.common.Constant;
import com.ihg.automation.selenium.common.catalog.CatalogItem;
import com.ihg.automation.selenium.common.enroll.SuccessEnrolmentPopUp;
import com.ihg.automation.selenium.common.member.AmbassadorOfferCode;
import com.ihg.automation.selenium.common.member.AmbassadorUtils;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.pages.PaymentMethod;
import com.ihg.automation.selenium.common.payment.CashPayment;
import com.ihg.automation.selenium.common.payment.CashPaymentContainer;
import com.ihg.automation.selenium.common.payment.PaymentDetailsPopUp;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.gwt.components.Mode;
import com.ihg.automation.selenium.servicecenter.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.servicecenter.pages.event.enroll.MembershipDetails;
import com.ihg.automation.selenium.servicecenter.pages.event.enroll.MembershipDetailsPopUp;
import com.ihg.automation.selenium.servicecenter.pages.event.enroll.MembershipDetailsTab;
import com.ihg.automation.selenium.servicecenter.pages.programs.ProgramPageBase.EnrollmentDetails;
import com.ihg.automation.selenium.servicecenter.pages.programs.amb.AmbassadorPage;

public class EnrollmentByCashTest extends AMBEnrollmentCommon
{
    private static final CatalogItem ENROLMENT_AWARD = AmbassadorUtils.AMBASSADOR_$200;

    private Member member = new Member();
    private PaymentDetailsPopUp ambEnrollPaymentDetailsPopUp = new PaymentDetailsPopUp();
    private MembershipDetailsPopUp membershipDetailsPopUp = new MembershipDetailsPopUp();
    private CashPaymentContainer cashPaymentEdit;
    private AmbassadorPage ambassadorPage = new AmbassadorPage();
    private final static String CASH_DEPOSIT_DATE = DateUtils.getFormattedDate();
    private final static String CASH_DEPOSIT_NUMBER = "12345";
    private final static String CASH_NAME = "test name";
    private final static String CASH_LOCATION = "test location";
    private CashPayment cashPayment = new CashPayment(ENROLMENT_AWARD, CASH_NAME, CASH_LOCATION, CASH_DEPOSIT_DATE,
            CASH_DEPOSIT_NUMBER);

    @BeforeClass
    public void beforeClass()
    {
        member.addProgram(Program.AMB);
        member.addAddress(MemberPopulateHelper.getUnitedStatesAddress());
        member.setPersonalInfo(MemberPopulateHelper.getSimplePersonalInfo());
        member.setAmbassadorOfferCode(AmbassadorOfferCode.CHSRO);

        login();
    }

    @Test
    public void enrollAMBMember()
    {
        EnrollmentPage enrollmentPage = new EnrollmentPage();
        enrollmentPage.goTo();
        enrollmentPage.populate(member);
        enrollmentPage.clickSubmit(verifyNoError());
        verifyThat(ambEnrollPaymentDetailsPopUp, isDisplayedWithWait());
        ambEnrollPaymentDetailsPopUp.selectAmount(ENROLMENT_AWARD);
        ambEnrollPaymentDetailsPopUp.selectPaymentType(PaymentMethod.CASH);
    }

    @Test(dependsOnMethods = { "enrollAMBMember" })
    public void verifyCashPaymentFields()
    {
        cashPaymentEdit = ambEnrollPaymentDetailsPopUp.getCashPayment();
        verifyThat(cashPaymentEdit.getDepositDate(), isDisplayedWithWait());
        verifyThat(cashPaymentEdit.getDepositNumber(), isDisplayedWithWait());
        verifyThat(cashPaymentEdit.getName(), isDisplayedWithWait());
        verifyThat(cashPaymentEdit.getLocation(), isDisplayedWithWait());
    }

    @Test(dependsOnMethods = { "verifyCashPaymentFields" }, alwaysRun = true)
    public void enrollMemberToAMB()
    {
        cashPaymentEdit.populate(cashPayment);
        ambEnrollPaymentDetailsPopUp.clickSubmitNoError();

        SuccessEnrolmentPopUp popUp = new SuccessEnrolmentPopUp();
        verifyThat(popUp, isDisplayedWithWait());
        member = popUp.putPrograms(member);
        verifyThat(popUp.getMemberId(Program.RC), hasText(popUp.getMemberId(Program.AMB).getText()));
        popUp.clickDone(verifyNoError());
    }

    @Test(dependsOnMethods = { "enrollMemberToAMB" }, alwaysRun = true)
    public void verifyAMBProgramInfoEnrollmentDetails()
    {
        ambassadorPage.goTo();
        EnrollmentDetails details = ambassadorPage.getEnrollmentDetails();
        details.verifyScSource(helper);
        verifyThat(details.getEnrollDate(), hasText(DateUtils.getFormattedDate()));
        verifyThat(details.getRenewalDate(), hasText(Constant.NOT_AVAILABLE));
        verifyThat(details.getOfferCode(), hasText(member.getAmbassadorOfferCode().getCode()));
        verifyThat(details.getReferringMember(), hasText(Constant.NOT_AVAILABLE));
        details.getPaymentDetails().verify(cashPayment, Mode.VIEW);
    }

    @Test(dependsOnMethods = { "verifyAMBProgramInfoEnrollmentDetails" }, alwaysRun = true)
    public void openAllEventsAMBEventDetailsPopUp()
    {
        openAMBEventDetailsPopUp();
    }

    @Test(dependsOnMethods = { "openAllEventsAMBEventDetailsPopUp" })
    public void verifyAllEventsAMBEventMembershipDetails()
    {
        MembershipDetailsTab tab = membershipDetailsPopUp.getMembershipDetailsTab();
        tab.goTo();
        MembershipDetails membershipDetails = tab.getMembershipDetails();
        membershipDetails.getPaymentDetails().verify(cashPayment, Mode.VIEW);
        membershipDetails.verifyScSource(helper);
    }

    @Test(dependsOnMethods = { "openAllEventsAMBEventDetailsPopUp" })
    public void verifyAllEventsAMBEventBillingDetails()
    {
        verifyAMBEventBillingDetails(ENROLMENT_AWARD);
    }

    @Test(dependsOnMethods = { "openAllEventsAMBEventDetailsPopUp" })
    public void verifyAllEventsAMBEventEarningDetails()
    {
        verifyAMBEventEarningDetails(ENROLMENT_AWARD);
    }

}
