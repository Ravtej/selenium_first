package com.ihg.crm.automation.selenium.tests.servicecenter.programinfo.ambassador;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.types.WrittenLanguage.SPANISH;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static com.ihg.automation.selenium.matchers.component.ModeMatcher.inView;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.common.DateUtils;
import com.ihg.automation.common.RandomUtils;
import com.ihg.automation.selenium.common.communication.Communication;
import com.ihg.automation.selenium.common.communication.ContactPermissionItem;
import com.ihg.automation.selenium.common.communication.MarketingPreferences;
import com.ihg.automation.selenium.common.member.AmbassadorOfferCode;
import com.ihg.automation.selenium.common.member.AmbassadorUtils;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberAlliance;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.member.PersonalInfo;
import com.ihg.automation.selenium.common.types.Alliance;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.gwt.components.Mode;
import com.ihg.automation.selenium.servicecenter.pages.LeftPanel;
import com.ihg.automation.selenium.servicecenter.pages.communication.CommunicationPreferences;
import com.ihg.automation.selenium.servicecenter.pages.communication.CommunicationPreferencesPage;
import com.ihg.automation.selenium.servicecenter.pages.communication.CustomerServiceEmail;
import com.ihg.automation.selenium.servicecenter.pages.communication.CustomerServicePhone;
import com.ihg.automation.selenium.servicecenter.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.servicecenter.pages.personal.CustomerInfoFields;
import com.ihg.automation.selenium.servicecenter.pages.personal.PersonalInfoPage;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.RewardClubPage;
import com.ihg.crm.automation.selenium.tests.servicecenter.LoginLogout;

public class EnrollToAMBWithCommPrefUpdateTest extends LoginLogout
{
    private Member member = new Member();
    private Member memberAMB = new Member();
    private CustomerInfoFields customerInfo;
    private PersonalInfoPage personalInfoPage = new PersonalInfoPage();
    private RewardClubPage rewardClubPage = new RewardClubPage();
    private Alliance alliance = Alliance.JAL;
    private String allianceNumber = RandomUtils.getRandomNumber(9);
    private CommunicationPreferencesPage commPrefPage;
    private CommunicationPreferences commPrefs;
    private MarketingPreferences marketingPrefs;
    private CustomerServicePhone customerServicePhone;
    private CustomerServiceEmail customerServiceEmail;
    protected final static String STATUS_IN = "Opt-In";

    @BeforeClass
    public void enrollRC()
    {
        PersonalInfo personalInfo = MemberPopulateHelper.getSimplePersonalInfo();
        personalInfo.setBirthDate(DateUtils.now());
        member.setPersonalInfo(personalInfo);
        member.addAddress(MemberPopulateHelper.getUnitedStatesAddress());
        member.addPhone(MemberPopulateHelper.getRandomPhone());
        member.addProgram(Program.RC);
        member.addEmail(MemberPopulateHelper.getRandomEmail());
        member.setPersonalInfo(personalInfo);

        MemberAlliance milesEarningPreference = new MemberAlliance(alliance, allianceNumber, member.getName());
        member.setMilesEarningPreference(milesEarningPreference);

        memberAMB.setAmbassadorOfferCode(AmbassadorOfferCode.CHSRO);
        memberAMB.setAmbassadorAmount(AmbassadorUtils.AMBASSADOR_COMP_PROMO_$0);
        memberAMB.addProgram(Program.AMB);

        login();
        member = new EnrollmentPage().enroll(member);

        updateCommunicationPreferences();
    }

    @Test
    public void enrollAMBandReopenProfile()
    {
        memberAMB = new EnrollmentPage().enroll(memberAMB);

        new LeftPanel().reOpenMemberProfile(member);
    }

    @Test(dependsOnMethods = { "enrollAMBandReopenProfile" }, alwaysRun = true, description = "DE14694")
    public void verifyPersonalInformation()
    {
        customerInfo = personalInfoPage.getCustomerInfo();
        customerInfo.verify(member, Mode.VIEW);
        personalInfoPage.getAddressList().getExpandedContact().verify(member.getPreferredAddress(), Mode.VIEW);
        personalInfoPage.getPhoneList().getExpandedContact().verify(member.getPreferredPhone(), Mode.VIEW);
        personalInfoPage.getEmailList().getExpandedContact().verify(member.getPreferredEmail(), Mode.VIEW);
    }

    @Test(dependsOnMethods = { "verifyPersonalInformation" }, alwaysRun = true)
    public void verifyAlliances()
    {
        rewardClubPage.goTo();
        rewardClubPage.getAlliances().getRow(1).verify(new MemberAlliance(alliance, allianceNumber, member.getName()));
    }

    @Test(dependsOnMethods = { "verifyAlliances" }, alwaysRun = true)
    public void verifyCommPrefs()
    {
        commPrefPage.goTo();

        ContactPermissionItem hotelOffersItem = marketingPrefs.getContactPermissionItems()
                .getContactPermissionItem(Communication.IHG_REWARDS_CLUB_CREDIT_CARD_OFFERS_AND_PROMOTIONS);
        verifyThat(hotelOffersItem.getSubscribe(), inView(hasText(STATUS_IN)));
        verifyThat(hotelOffersItem.getFrequency(), hasText("Frequency: Varies"));

        verifyThat(customerServicePhone.getPhone().getView(), hasText(member.getPreferredPhone().getFullPhone()));
        verifyThat(customerServiceEmail.getEmail().getView(), hasText(member.getPreferredEmail().getEmail()));
    }

    private void updateCommunicationPreferences()
    {
        commPrefPage = new CommunicationPreferencesPage();
        commPrefPage.goTo();
        commPrefs = commPrefPage.getCommunicationPreferences();
        commPrefs.clickEdit();
        commPrefs.getPreferredLanguage().selectByValue(SPANISH);

        marketingPrefs = commPrefs.getMarketingPreferences();
        marketingPrefs.getSubscribeAllLink().click();

        commPrefs = commPrefPage.getCommunicationPreferences();
        customerServicePhone = commPrefPage.getCommunicationPreferences().getCustomerService().getPhone();
        customerServicePhone.getPhoneCheckBox().check();

        customerServiceEmail = commPrefPage.getCommunicationPreferences().getCustomerService().getEmail();
        customerServiceEmail.getEmailCheckBox().check();

        commPrefs.clickSave();
    }
}
