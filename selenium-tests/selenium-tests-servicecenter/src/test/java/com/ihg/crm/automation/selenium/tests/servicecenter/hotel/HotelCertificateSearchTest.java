package com.ihg.crm.automation.selenium.tests.servicecenter.hotel;

import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyError;
import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyNoError;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.hotel.HotelCertificateStatus.CANCELED;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasEmptyText;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.isHighlightedAsInvalid;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.isSelected;
import static com.ihg.automation.selenium.matchers.component.GridSize.size;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static com.ihg.automation.selenium.servicecenter.pages.hotel.certificate.HotelCertificateSearchGridRow.HotelCertificateSearchGridRowCell.AMOUNT;
import static com.ihg.automation.selenium.servicecenter.pages.hotel.certificate.HotelCertificateSearchGridRow.HotelCertificateSearchGridRowCell.BILLING_ENTITY;
import static com.ihg.automation.selenium.servicecenter.pages.hotel.certificate.HotelCertificateSearchGridRow.HotelCertificateSearchGridRowCell.BUNDLE;
import static com.ihg.automation.selenium.servicecenter.pages.hotel.certificate.HotelCertificateSearchGridRow.HotelCertificateSearchGridRowCell.CERTIFICATE_NUMBER;
import static com.ihg.automation.selenium.servicecenter.pages.hotel.certificate.HotelCertificateSearchGridRow.HotelCertificateSearchGridRowCell.CHECK_IN;
import static com.ihg.automation.selenium.servicecenter.pages.hotel.certificate.HotelCertificateSearchGridRow.HotelCertificateSearchGridRowCell.CURR;
import static com.ihg.automation.selenium.servicecenter.pages.hotel.certificate.HotelCertificateSearchGridRow.HotelCertificateSearchGridRowCell.FOLIO;
import static com.ihg.automation.selenium.servicecenter.pages.hotel.certificate.HotelCertificateSearchGridRow.HotelCertificateSearchGridRowCell.GUEST_NAME;
import static com.ihg.automation.selenium.servicecenter.pages.hotel.certificate.HotelCertificateSearchGridRow.HotelCertificateSearchGridRowCell.HOTEL;
import static com.ihg.automation.selenium.servicecenter.pages.hotel.certificate.HotelCertificateSearchGridRow.HotelCertificateSearchGridRowCell.ITEM_NAME;
import static com.ihg.automation.selenium.servicecenter.pages.hotel.certificate.HotelCertificateSearchGridRow.HotelCertificateSearchGridRowCell.STATUS;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNot.not;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.gwt.components.grid.GridHeader;
import com.ihg.automation.selenium.servicecenter.pages.hotel.certificate.HotelCertificatePage;
import com.ihg.automation.selenium.servicecenter.pages.hotel.certificate.HotelCertificateSearch;
import com.ihg.automation.selenium.servicecenter.pages.hotel.certificate.HotelCertificateSearchGrid;
import com.ihg.automation.selenium.servicecenter.pages.hotel.certificate.HotelCertificateSearchGridRow.HotelCertificateSearchGridRowCell;
import com.ihg.crm.automation.selenium.tests.servicecenter.LoginLogout;

public class HotelCertificateSearchTest extends LoginLogout
{
    private HotelCertificatePage hotelCertificatePage = new HotelCertificatePage();
    private HotelCertificateSearchGrid hotelCertificateSearchGrid;

    @BeforeClass
    public void beforeClass()
    {
        login();
        hotelCertificatePage.goTo();
    }

    @Test
    public void verifyHotelOperationsSearchControls()
    {
        HotelCertificateSearch hotelCertificateSearch = hotelCertificatePage.getSearchFields();
        verifyThat(hotelCertificateSearch.getHotelCode(), displayed(true));
        verifyThat(hotelCertificateSearch.getCertificateNumber(), displayed(true));
        verifyThat(hotelCertificateSearch.getConfirmationNumber(), displayed(true));
        verifyThat(hotelCertificateSearch.getItemId(), displayed(true));
        verifyThat(hotelCertificateSearch.getStatusOfRequest(), displayed(true));
        verifyThat(hotelCertificateSearch.getFromDate(), displayed(true));
        verifyThat(hotelCertificateSearch.getToDate(), displayed(true));
        verifyThat(hotelCertificateSearch.getCheckIn(), displayed(true));
        verifyThat(hotelCertificateSearch.getCheckOut(), displayed(true));
        verifyThat(hotelCertificateSearch.getEnterCertificate(), displayed(true));

        HotelCertificatePage.HotelCertificateSearchButtonBar buttonBar = hotelCertificatePage.getButtonBar();
        verifyThat(buttonBar.getSearch(), displayed(true));
        verifyThat(buttonBar.getClearCriteria(), displayed(true));
    }

    @Test
    public void verifyGridHeader()
    {
        GridHeader<HotelCertificateSearchGridRowCell> header = hotelCertificatePage.getGrid().getHeader();

        verifyThat(header.getCell(HOTEL), hasText(HOTEL.getValue()));
        verifyThat(header.getCell(CHECK_IN), hasText(CHECK_IN.getValue()));
        verifyThat(header.getCell(GUEST_NAME), hasText(GUEST_NAME.getValue()));
        verifyThat(header.getCell(ITEM_NAME), hasText(ITEM_NAME.getValue()));
        verifyThat(header.getCell(CERTIFICATE_NUMBER), hasText(CERTIFICATE_NUMBER.getValue()));
        verifyThat(header.getCell(STATUS), hasText(STATUS.getValue()));
        verifyThat(header.getCell(AMOUNT), hasText(AMOUNT.getValue()));
        verifyThat(header.getCell(CURR), hasText(CURR.getValue()));
        verifyThat(header.getCell(BILLING_ENTITY), hasText(BILLING_ENTITY.getValue()));
        verifyThat(header.getCell(FOLIO), hasText(FOLIO.getValue()));
        verifyThat(header.getCell(BUNDLE), hasText(BUNDLE.getValue()));
    }

    @DataProvider(name = "invalidHotelCodeProvider")
    protected Object[][] invalidHotelCodeProvider()
    {
        return new Object[][] { { "sdfsf", "Hotel mnemonic is invalid" }, { null, null } };
    }

    @Test(dataProvider = "invalidHotelCodeProvider")
    public void searchByInvalidHotelCode(String hotelCode, String message)
    {
        hotelCertificatePage.searchByHotelCode(hotelCode, verifyError(message));

        verifyThat(hotelCertificatePage.getSearchFields().getHotelCode(), isHighlightedAsInvalid(true));
        verifyThat(hotelCertificatePage.getGrid(), size(0));
    }

    @DataProvider(name = "validHotelCodeProvider")
    protected Object[][] validHotelCodeProvider()
    {
        return new Object[][] { { "CEQHA" }, { "ATLCP" } };
    }

    @Test(dataProvider = "validHotelCodeProvider")
    public void searchByValidHotelCode(String hotelCode)
    {
        hotelCertificatePage.searchByHotelCode(hotelCode, verifyNoError());

        verifyThat(hotelCertificatePage.getSearchFields().getHotelCode(), isHighlightedAsInvalid(false));

        hotelCertificateSearchGrid = hotelCertificatePage.getGrid();
        verifyThat(hotelCertificateSearchGrid, size(not(is(0))));
        verifyThat(hotelCertificateSearchGrid.getRow(1).getCell(HOTEL), hasText(hotelCode));
    }

    @Test
    public void clearSearchFields()
    {
        hotelCertificatePage.populateSearch("CEQHA", "bbb", "123", "aaa", CANCELED, false, true, "02Mar10", "03Mar10");
        hotelCertificatePage.getButtonBar().clickClearCriteria();

        HotelCertificateSearch search = hotelCertificatePage.getSearchFields();
        verifyThat(search.getHotelCode(), hasText("Hotel"));
        verifyThat(search.getCertificateNumber(), hasEmptyText());
        verifyThat(search.getItemId(), hasEmptyText());
        verifyThat(search.getStatusOfRequest(), hasText("All Statuses"));
        verifyThat(search.getConfirmationNumber(), hasEmptyText());
        verifyThat(search.getCheckIn(), isSelected(true));
        verifyThat(search.getCheckOut(), isSelected(false));
        verifyThat(search.getFromDate(), hasText("ddMMMyy"));
        verifyThat(search.getToDate(), hasText("ddMMMyy"));
    }

}
