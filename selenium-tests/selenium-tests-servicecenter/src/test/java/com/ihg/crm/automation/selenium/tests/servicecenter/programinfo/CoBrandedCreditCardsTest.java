package com.ihg.crm.automation.selenium.tests.servicecenter.programinfo;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.types.program.Program.RC;
import static com.ihg.automation.selenium.common.types.program.RewardClubLevel.valueOf;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static com.ihg.automation.selenium.servicecenter.pages.programs.rc.CoBrandedCreditCardsRow.CoBrandedCreditCardsCell.CARD_PRODUCT;
import static com.ihg.automation.selenium.servicecenter.pages.programs.rc.CoBrandedCreditCardsRow.CoBrandedCreditCardsCell.CARD_STATUS_DATE;
import static com.ihg.automation.selenium.servicecenter.pages.programs.rc.CoBrandedCreditCardsRow.CoBrandedCreditCardsCell.ELITE_LEVEL_DURATION;
import static com.ihg.automation.selenium.servicecenter.pages.programs.rc.CoBrandedCreditCardsRow.CoBrandedCreditCardsCell.ELITE_LEVEL_GIVEN;
import static com.ihg.automation.selenium.servicecenter.pages.programs.rc.CoBrandedCreditCardsRow.CoBrandedCreditCardsCell.PARTNER;
import static com.ihg.automation.selenium.servicecenter.pages.programs.rc.CoBrandedCreditCardsRow.CoBrandedCreditCardsCell.STATUS;
import static java.lang.String.format;

import java.util.Map;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.CoBrandedCreditCard;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.gwt.components.grid.GridHeader;
import com.ihg.automation.selenium.servicecenter.pages.GuestSearch;
import com.ihg.automation.selenium.servicecenter.pages.LeftPanel;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.CoBrandedCreditCardsRow;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.RewardClubPage;
import com.ihg.crm.automation.selenium.tests.servicecenter.LoginLogout;

public class CoBrandedCreditCardsTest extends LoginLogout
{
    private final static String CO_BRANDED_DATA_SQL = "SELECT DISTINCT m.MBRSHP_ID, pa.PARTNER_NM, pl.CARD_PRODUCT_NM, pl.TIER_LEVEL_BENEFIT_CODE, " //
            + " TO_CHAR(pc.%s,'DDMonYY') AS STATUS_DATE FROM DGST.MBRSHP m " //
            + " RIGHT JOIN DGST.GST_PARTNER_CCD pc ON pc.DGST_MSTR_KEY = m.DGST_MSTR_KEY"//
            + " LEFT JOIN DGST.CARD_PRODUCT_LKUP pl ON pl.CARD_PRODUCT_LKUP_KEY = pc.CARD_PRODUCT_LKUP_KEY" //
            + " LEFT JOIN PARTNER.LYTY_PARTNER pa ON pa.PARTNER_ID = pl.PARTNER_CD"//
            + " WHERE pl.TIER_LEVEL_BENEFIT_EXPIRATION = 'LIFETIME' AND pc.GST_PARTNER_CCD_STAT_CD = '%s'"//
            + " AND ROWNUM < 2 ";

    private Member memberWithOpenedPartnerCC;
    private Member memberWithClosedPartnerCC;

    @BeforeClass
    public void beforeClass()
    {
        memberWithOpenedPartnerCC = getCoBrandedCreditCardBean("O", "Open", "GST_PARTNER_CCD_DISP_OPN_DT");
        memberWithClosedPartnerCC = getCoBrandedCreditCardBean("C", "Close", "GST_PARTNER_CCD_CLS_DT");

        login();
    }

    @Test(priority = 10)
    public void verifyOpenCoBrandedCreditCardData()
    {
        new GuestSearch().byMemberNumber(memberWithOpenedPartnerCC.getRCProgramId());

        RewardClubPage rewardClubPage = new RewardClubPage();
        rewardClubPage.goTo();
        rewardClubPage.getCoBrandedCreditCards().getRow(1).verify(memberWithOpenedPartnerCC.getCoBrandedCreditCard());
    }

    @Test(priority = 20)
    public void verifyCloseCoBrandedCreditCardData()
    {
        new LeftPanel().reOpenMemberProfile(memberWithClosedPartnerCC);

        RewardClubPage rewardClubPage = new RewardClubPage();
        rewardClubPage.goTo();
        rewardClubPage.getCoBrandedCreditCards().getRow(1).verify(memberWithClosedPartnerCC.getCoBrandedCreditCard());
    }

    @Test(priority = 30)
    public void verifyCoBrandedCreditCardGridHead()
    {
        GridHeader<CoBrandedCreditCardsRow.CoBrandedCreditCardsCell> header = new RewardClubPage()
                .getCoBrandedCreditCards().getHeader();
        verifyThat(header.getCell(PARTNER), hasText(PARTNER.getValue()));
        verifyThat(header.getCell(CARD_PRODUCT), hasText(CARD_PRODUCT.getValue()));
        verifyThat(header.getCell(ELITE_LEVEL_GIVEN), hasText(ELITE_LEVEL_GIVEN.getValue()));
        verifyThat(header.getCell(ELITE_LEVEL_DURATION), hasText(ELITE_LEVEL_DURATION.getValue()));
        verifyThat(header.getCell(CARD_STATUS_DATE), hasText(CARD_STATUS_DATE.getValue()));
        verifyThat(header.getCell(STATUS), hasText(STATUS.getValue()));
    }

    private Member getCoBrandedCreditCardBean(String statusCode, String status, String dateColumn)
    {
        Map<String, Object> map = jdbcTemplate.queryForMap(format(CO_BRANDED_DATA_SQL, dateColumn, statusCode));

        CoBrandedCreditCard coBrandedCreditCard = new CoBrandedCreditCard();
        coBrandedCreditCard.setPartner(map.get("PARTNER_NM").toString());
        coBrandedCreditCard.setCreditCardName(map.get("CARD_PRODUCT_NM").toString());
        coBrandedCreditCard.setLevel(valueOf(map.get("TIER_LEVEL_BENEFIT_CODE").toString()));
        coBrandedCreditCard.setStatusDate(map.get("STATUS_DATE").toString());
        coBrandedCreditCard.setStatus(status);

        Member member = new Member();
        member.getPrograms().put(RC, map.get("MBRSHP_ID").toString());
        member.setCoBrandedCreditCard(coBrandedCreditCard);

        return member;
    }
}
