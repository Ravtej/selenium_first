package com.ihg.crm.automation.selenium.tests.servicecenter.enrollment.ambassador;

import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyNoError;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.member.AmbassadorUtils.NEW_GOLD_AMBASSADOR_KIT;
import static com.ihg.automation.selenium.common.types.name.Salutation.MR;
import static com.ihg.automation.selenium.common.types.program.Program.AMB;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.enabled;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.isDisplayedWithWait;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static org.hamcrest.CoreMatchers.endsWith;
import static org.hamcrest.core.StringContains.containsString;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.common.DateUtils;
import com.ihg.automation.selenium.common.Constant;
import com.ihg.automation.selenium.common.catalog.CatalogItem;
import com.ihg.automation.selenium.common.enroll.SuccessEnrolmentPopUp;
import com.ihg.automation.selenium.common.event.AllEventsRow;
import com.ihg.automation.selenium.common.event.AllEventsRowConverter;
import com.ihg.automation.selenium.common.member.AmbassadorOfferCode;
import com.ihg.automation.selenium.common.member.AmbassadorUtils;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.member.PersonalInfo;
import com.ihg.automation.selenium.common.order.Order;
import com.ihg.automation.selenium.common.order.OrderFactory;
import com.ihg.automation.selenium.common.pages.PaymentMethod;
import com.ihg.automation.selenium.common.payment.Payment;
import com.ihg.automation.selenium.common.payment.PaymentDetailsPopUp;
import com.ihg.automation.selenium.common.payment.PaymentFactory;
import com.ihg.automation.selenium.common.types.program.AmbassadorLevel;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.common.types.program.ProgramStatus;
import com.ihg.automation.selenium.common.types.program.RewardClubLevel;
import com.ihg.automation.selenium.gwt.components.Mode;
import com.ihg.automation.selenium.servicecenter.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsGrid;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsGridRow;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsPage;
import com.ihg.automation.selenium.servicecenter.pages.event.enroll.MembershipDetailsPopUp;
import com.ihg.automation.selenium.servicecenter.pages.event.enroll.MembershipDetailsTab;
import com.ihg.automation.selenium.servicecenter.pages.event.order.OrderRedeemGridRowContainer;
import com.ihg.automation.selenium.servicecenter.pages.event.order.OrderSystemDetailsGridRowContainer;
import com.ihg.automation.selenium.servicecenter.pages.personal.PersonalInfoPage;
import com.ihg.automation.selenium.servicecenter.pages.programs.ProgramPageBase.EnrollmentDetails;
import com.ihg.automation.selenium.servicecenter.pages.programs.RenewExtendProgramSummary;
import com.ihg.automation.selenium.servicecenter.pages.programs.amb.AmbassadorPage;
import com.ihg.automation.selenium.servicecenter.pages.programs.amb.AmbassadorTierLevelActivityGrid;
import com.ihg.automation.selenium.servicecenter.pages.programs.amb.AmbassadorTierLevelActivityGrid.AmbassadorTierLevelAccumulator;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.RewardClubPage;
import com.ihg.automation.selenium.servicecenter.pages.unit.GoodwillPopUp;

public class EnrollmentExistingMemberByPointsTest extends AMBEnrollmentCommon
{
    private static final CatalogItem ENROLMENT_AWARD = AmbassadorUtils.AMBASSADOR_32000_POINTS;

    private Member member = new Member();
    private EnrollmentPage enrollmentPage = new EnrollmentPage();
    private PaymentDetailsPopUp ambEnrollPaymentDetailsPopUp = new PaymentDetailsPopUp();
    private PersonalInfoPage personalInfoPage = new PersonalInfoPage();
    private AmbassadorPage ambassadorPage = new AmbassadorPage();
    private AllEventsPage allEventsPage = new AllEventsPage();
    private MembershipDetailsPopUp membershipDetailsPopUp = new MembershipDetailsPopUp();
    private Payment payment = PaymentFactory.getPointsPayment(ENROLMENT_AWARD);

    @BeforeClass
    public void beforeClass()
    {
        PersonalInfo personalInfo = MemberPopulateHelper.getSimplePersonalInfo();
        personalInfo.getName().setSalutation(MR);
        member.setPersonalInfo(personalInfo);
        member.addProgram(Program.RC);
        member.addAddress(MemberPopulateHelper.getUnitedStatesAddress());

        login();

        enrollmentPage.enroll(member);
        new GoodwillPopUp().goodwillPoints(ENROLMENT_AWARD.getLoyaltyUnitCost());
    }

    @Test
    public void enrollToAMB()
    {
        member.setAmbassadorOfferCode(AmbassadorOfferCode.CHSRO);

        enrollmentPage = new EnrollmentPage();
        enrollmentPage.goTo();
        enrollmentPage.getPrograms().getProgramCheckbox(AMB).check();
        enrollmentPage.getAMBOfferCode().selectByCode(member.getAmbassadorOfferCode());

        enrollmentPage.clickSubmit(verifyNoError());
    }

    @Test(dependsOnMethods = { "enrollToAMB" }, alwaysRun = true)
    public void verifyAMBPaymentDetailsPopUp()
    {
        verifyThat(ambEnrollPaymentDetailsPopUp, isDisplayedWithWait());
        ambEnrollPaymentDetailsPopUp.selectAmount(ENROLMENT_AWARD);
        verifyThat(ambEnrollPaymentDetailsPopUp.getPaymentTypeButton(PaymentMethod.LOYALTY_UNITS), displayed(true));
    }

    @Test(dependsOnMethods = { "verifyAMBPaymentDetailsPopUp" }, alwaysRun = true)
    public void completeEnrollment()
    {
        ambEnrollPaymentDetailsPopUp.clickSubmitNoError();

        SuccessEnrolmentPopUp popUp = new SuccessEnrolmentPopUp();
        verifyThat(popUp, isDisplayedWithWait());
        member = popUp.putPrograms(member);
        popUp.clickDone(verifyNoError());
    }

    @Test(dependsOnMethods = { "completeEnrollment" })
    public void verifyEnrollmentPersonalInfo()
    {
        verifyThat(personalInfoPage, isDisplayedWithWait());
        personalInfoPage.getCustomerInfo().verify(member, Mode.VIEW);
    }

    @Test(dependsOnMethods = { "verifyEnrollmentPersonalInfo" }, alwaysRun = true)
    public void verifyAMBProgramInfoSummary()
    {
        ambassadorPage.goTo();
        RenewExtendProgramSummary summ = ambassadorPage.getSummary();
        verifyThat(summ.getMemberId(), hasText(member.getAMBProgramId()));
        summ.verify(ProgramStatus.OPEN, AmbassadorLevel.AMB);
        verifyThat(summ.getExpirationDate(), hasText(AmbassadorUtils.getExpirationDate()));
        verifyThat(summ, hasText(containsString("No flags selected")));
        verifyThat(summ.getRenew(), enabled(false));
        verifyThat(summ.getExtend(), displayed(true));
    }

    @Test(dependsOnMethods = { "verifyAMBProgramInfoSummary" }, alwaysRun = true)
    public void verifyAMBProgramInfoAnnualActivity()
    {
        ambassadorPage.goTo();
        AmbassadorTierLevelActivityGrid grid = ambassadorPage.getAnnualActivities();
        grid.getRow(AmbassadorTierLevelAccumulator.IC_HOTELS).verify("0", Integer.toString(achieveRamIcHotels));
        grid.getRow(AmbassadorTierLevelAccumulator.QUALIFYING_SPEND).verify("0",
                Integer.toString(achieveRamIQualifyingSpend));
    }

    @Test(dependsOnMethods = { "verifyAMBProgramInfoAnnualActivity" }, alwaysRun = true)
    public void verifyAMBProgramInfoEnrollmentDetails()
    {
        ambassadorPage.goTo();
        EnrollmentDetails details = ambassadorPage.getEnrollmentDetails();
        details.verifyScSource(helper);
        verifyThat(details.getEnrollDate(), hasText(DateUtils.getFormattedDate()));
        verifyThat(details.getRenewalDate(), hasText(Constant.NOT_AVAILABLE));
        verifyThat(details.getOfferCode(), hasText(member.getAmbassadorOfferCode().getCode()));
        verifyThat(details.getReferringMember(), hasText(Constant.NOT_AVAILABLE));
        details.getPaymentDetails().verifyCommonDetails(payment);
    }

    @Test(dependsOnMethods = { "verifyAMBProgramInfoEnrollmentDetails" }, alwaysRun = true)
    public void verifyRewardClubProgramInfo()
    {
        RewardClubPage page = new RewardClubPage();
        page.goTo();
        page.getSummary().verify(ProgramStatus.OPEN, RewardClubLevel.GOLD);
        verifyThat(page.getSummary().getUnitsBalance(), hasText("0"));
    }

    @Test(dependsOnMethods = { "verifyRewardClubProgramInfo" }, alwaysRun = true)
    public void openAllEventsAMBEventDetailsPopUp()
    {
        openAMBEventDetailsPopUp();
    }

    @Test(dependsOnMethods = { "openAllEventsAMBEventDetailsPopUp" })
    public void verifyAllEventsAMBEventMembershipDetails()
    {
        MembershipDetailsTab tab = membershipDetailsPopUp.getMembershipDetailsTab();
        tab.goTo();
        tab.getMembershipDetails().verify(payment, helper);
    }

    @Test(dependsOnMethods = { "openAllEventsAMBEventDetailsPopUp" })
    public void verifyAllEventsAMBEventBillingDetails()
    {
        verifyAMBEventBillingDetails(ENROLMENT_AWARD);
    }

    @Test(dependsOnMethods = { "openAllEventsAMBEventDetailsPopUp" })
    public void verifyAllEventsAMBEventEarningDetails()
    {
        verifyAMBEventEarningDetails(ENROLMENT_AWARD);
    }

    @Test(dependsOnMethods = { "openAllEventsAMBEventDetailsPopUp", "verifyAllEventsAMBEventMembershipDetails",
            "verifyAllEventsAMBEventBillingDetails", "verifyAllEventsAMBEventEarningDetails" }, alwaysRun = true)
    public void closeAllEventsAMBEventMembershipDetailsPopUp()
    {
        membershipDetailsPopUp.close();
    }

    @Test(dependsOnMethods = { "closeAllEventsAMBEventMembershipDetailsPopUp" }, alwaysRun = true)
    public void verifyAllEventsSystemOrderEvent()
    {
        Order order = OrderFactory.getSystemOrder(NEW_GOLD_AMBASSADOR_KIT, member, helper);
        AllEventsRow orderEventRow = AllEventsRowConverter.convert(order);

        allEventsPage.goTo();
        AllEventsGrid grid = allEventsPage.getGrid();
        AllEventsGridRow row = grid.getRow(orderEventRow.getTransType());
        row.verify(orderEventRow);
        row.expand(OrderSystemDetailsGridRowContainer.class).verify(order);
    }

    @Test(dependsOnMethods = { "verifyAllEventsSystemOrderEvent" }, alwaysRun = true)
    public void verifyRedeemEventDetails()
    {
        Order redeemOrder = OrderFactory.getRedeemOrder(ENROLMENT_AWARD, member, helper);
        AllEventsRow redeemEventRow = AllEventsRowConverter.getRedeemEvent(redeemOrder);

        allEventsPage.goTo();
        AllEventsGrid grid = allEventsPage.getGrid();
        AllEventsGridRow row = grid.getRow(redeemEventRow.getTransType());
        row.verify(redeemEventRow, hasText(endsWith(redeemEventRow.getDetail())));
        OrderRedeemGridRowContainer container = row.expand(OrderRedeemGridRowContainer.class);

        container.getOrderDetails().verify(redeemOrder.getOrderItems().get(0), "RC Units");
        container.getShippingInfo().verify(redeemOrder, true);
        container.getSource().verify(redeemOrder.getSource(), Constant.NOT_AVAILABLE);
    }

}
