package com.ihg.crm.automation.selenium.tests.servicecenter.programinfo.ambassador;

import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifyNoError;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.event.EventTransactionType.Membership.MEMBERSHIP_OPEN;
import static com.ihg.automation.selenium.common.member.AmbassadorUtils.AMBASSADOR_CORP_DISC_$150;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.common.DateUtils;
import com.ihg.automation.selenium.common.catalog.CatalogItem;
import com.ihg.automation.selenium.common.member.AmbassadorJdbcUtils;
import com.ihg.automation.selenium.common.member.AmbassadorOfferCode;
import com.ihg.automation.selenium.common.member.AmbassadorUtils;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.pages.PaymentMethod;
import com.ihg.automation.selenium.common.payment.CashPayment;
import com.ihg.automation.selenium.common.payment.CashPaymentContainer;
import com.ihg.automation.selenium.common.payment.PaymentDetailsPopUp;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.common.types.program.ProgramStatus;
import com.ihg.automation.selenium.gwt.components.Mode;
import com.ihg.automation.selenium.servicecenter.pages.LeftPanel;
import com.ihg.automation.selenium.servicecenter.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsGridRow;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsPage;
import com.ihg.automation.selenium.servicecenter.pages.event.enroll.MembershipDetails;
import com.ihg.automation.selenium.servicecenter.pages.event.enroll.MembershipGridRowContainer;
import com.ihg.automation.selenium.servicecenter.pages.programs.RenewExtendProgramSummary;
import com.ihg.automation.selenium.servicecenter.pages.programs.amb.AmbassadorPage;

public class ReactivateByCashTest extends AmbassadorCommon
{
    private static final CatalogItem ENROLMENT_AWARD = AMBASSADOR_CORP_DISC_$150;

    private AmbassadorPage ambassadorPage = new AmbassadorPage();
    private PaymentDetailsPopUp paymentDetailsPopUp = new PaymentDetailsPopUp();
    private AllEventsPage allEventsPage = new AllEventsPage();
    private CashPaymentContainer cashPayment;
    private final static String CASH_DEPOSIT_DATE = DateUtils.getFormattedDate();
    private final static String CASH_DEPOSIT_NUMBER = "12345";
    private final static String CASH_NAME = "test name";
    private final static String CASH_LOCATION = "test location";
    private CashPayment cashPaymentItem = new CashPayment(ENROLMENT_AWARD, CASH_NAME, CASH_LOCATION, CASH_DEPOSIT_DATE,
            CASH_DEPOSIT_NUMBER);
    private Member member;
    private EnrollmentPage enrollmentPage = new EnrollmentPage();

    @BeforeClass
    public void beforeClass()
    {
        login();

        member = enrollAMBmember();
        new AmbassadorJdbcUtils(jdbcTemplateUpdate, lastUpdateUserId).updateForReactivate(member);
        new LeftPanel().reOpenMemberProfile(member);

        enrollmentPage.goTo();
        enrollmentPage.getPrograms().selectProgram(Program.AMB);
    }

    @Test
    public void verifyPaymentMethod()
    {
        enrollmentPage.getAMBOfferCode().selectByCode(AmbassadorOfferCode.CHNRO);
        enrollmentPage.clickSubmit(verifyNoError());
        paymentDetailsPopUp = new PaymentDetailsPopUp();
        paymentDetailsPopUp.selectAmount(ENROLMENT_AWARD);
        verifyThat(paymentDetailsPopUp.getPaymentTypeButton(PaymentMethod.CASH), displayed(true));
        verifyThat(paymentDetailsPopUp.getPaymentTypeButton(PaymentMethod.CHECK), displayed(true));
        verifyThat(paymentDetailsPopUp.getPaymentTypeButton(PaymentMethod.CREDIT_CARD), displayed(true));
    }

    @Test(dependsOnMethods = { "verifyPaymentMethod" }, alwaysRun = true)
    public void verifyCashPaymentFields()
    {
        paymentDetailsPopUp.getPaymentTypeButton(PaymentMethod.CASH).select();
        cashPayment = paymentDetailsPopUp.getCashPayment();
        verifyThat(cashPayment.getName(), displayed(true));
        verifyThat(cashPayment.getLocation(), displayed(true));
        verifyThat(cashPayment.getDepositDate(), displayed(true));
        verifyThat(cashPayment.getDepositNumber(), displayed(true));
    }

    @Test(dependsOnMethods = { "verifyCashPaymentFields" }, alwaysRun = true)
    public void populateCashPaymentFields()
    {
        cashPayment.populate(cashPaymentItem);
        paymentDetailsPopUp.clickSubmitNoError();
    }

    @Test(dependsOnMethods = { "populateCashPaymentFields" }, alwaysRun = true)
    public void verifySuccessEnrollmentPopUp()
    {
        validateSuccessEnrollmentPopUp(Program.AMB, member);
    }

    @Test(dependsOnMethods = { "verifySuccessEnrollmentPopUp" }, alwaysRun = true)
    public void verifySummary()
    {
        ambassadorPage.goTo();
        RenewExtendProgramSummary summary = ambassadorPage.getSummary();

        verifyThat(summary.getExpirationDate(), hasText(AmbassadorUtils.getExpirationDate()));
        summary.verifyRenewAndExtendButtons(false, true);
        summary.verifyStatus(ProgramStatus.OPEN);
    }

    @Test(dependsOnMethods = "verifySummary", alwaysRun = true)
    public void verifyAllEventsContainer()
    {
        allEventsPage.goTo();
        AllEventsGridRow row = allEventsPage.getGrid().getRow(MEMBERSHIP_OPEN);
        verifyThat(row, displayed(true));

        MembershipGridRowContainer container = row.expand(MembershipGridRowContainer.class);
        MembershipDetails membershipDetails = container.getMembershipDetails();
        membershipDetails.getPaymentDetails().verify(cashPaymentItem, Mode.VIEW);
        membershipDetails.verifyScSource(helper);
    }

    @Test(dependsOnMethods = "verifyAllEventsContainer", alwaysRun = true)
    public void verifyOrderSystemContainer()
    {
        verifyOrderSystemDetailsAfterReactivate(member);
    }
}
