package com.ihg.crm.automation.selenium.tests.servicecenter.order;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.event.EventTransactionType.Order.ORDER_SYSTEM;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static org.apache.commons.lang3.StringUtils.EMPTY;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.common.Constant;
import com.ihg.automation.selenium.common.billing.EventBillingDetailsTab;
import com.ihg.automation.selenium.common.earning.EventEarningDetailsTab;
import com.ihg.automation.selenium.common.event.EarningEvent;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.member.MemberPopulateHelper;
import com.ihg.automation.selenium.common.order.Order;
import com.ihg.automation.selenium.common.order.OrderItem;
import com.ihg.automation.selenium.common.types.program.Program;
import com.ihg.automation.selenium.common.types.program.ProgramStatus;
import com.ihg.automation.selenium.common.types.program.RewardClubLevel;
import com.ihg.automation.selenium.gwt.components.MessageBox;
import com.ihg.automation.selenium.gwt.components.MessageBoxType;
import com.ihg.automation.selenium.servicecenter.pages.LeftPanel;
import com.ihg.automation.selenium.servicecenter.pages.enroll.EnrollmentPage;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsGridRow;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsPage;
import com.ihg.automation.selenium.servicecenter.pages.event.order.OrderDetailsPopUp;
import com.ihg.automation.selenium.servicecenter.pages.event.order.OrderSystemDetailsGridRowContainer;
import com.ihg.automation.selenium.servicecenter.pages.event.voucher.VoucherDetailsPopUp;
import com.ihg.automation.selenium.servicecenter.pages.event.voucher.VoucherGridRowContainer;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.RewardClubPage;
import com.ihg.automation.selenium.servicecenter.pages.voucher.VoucherPopUp;

public class DepositVoucherTest extends VoucherOrderCommon
{
    private static final String VOUCHER_DEPOSIT = "VOUCHER DEPOSIT";
    private Member member = new Member();
    private String voucherNumber;

    private EarningEvent baseAwardEvent, baseUnitsEvent;
    private AllEventsPage allEventsPage = new AllEventsPage();

    private Order order = new Order();

    private static final String AVAILABLE_VOUCHER = "SELECT * FROM"//
            + " (SELECT TO_CHAR(BON_VOUCHER_ID,'FM000000000')||TO_CHAR(FACILITY_ID,'FM00000')||TO_CHAR(BON_VOUCHER_PIN,'FM0000') VOUCHER_NUMBER"//
            + " FROM FMDS.TMBNVCH"//
            + " WHERE VOUCHER_SENT_IND = 'Y' AND VOUCHER_STAT_CD != 'R'"//
            + " AND MEMBERSHIP_ID IS NULL"//
            + " AND PROMO_ID = '%s'"//
            + " ORDER BY LAST_UPDATE_TMSTMP"//
            + " ) WHERE ROWNUM < 2";

    @BeforeClass
    public void beforeClass()
    {
        member.setPersonalInfo(MemberPopulateHelper.getSimplePersonalInfo());
        member.addAddress(MemberPopulateHelper.getUnitedStatesAddress());
        member.addProgram(Program.RC);

        voucherNumber = getVoucherNumber(String.format(AVAILABLE_VOUCHER, "CTUCGE"));

        order.setShippingInfo(member);
        order.setTransactionType(ORDER_SYSTEM);
        OrderItem orderItem = new OrderItem("F0010", "ELITE UPGRADE CERTIFICATE KIT");
        orderItem.setDeliveryStatus("PROCESSING");
        order.getOrderItems().add(orderItem);

        baseAwardEvent = new EarningEvent(Constant.BASE_AWARDS, "1", EMPTY, EMPTY, "Created");
        baseAwardEvent.setAward(orderItem.getItemID());

        baseUnitsEvent = new EarningEvent(Constant.BASE_UNITS, "0", "0", Constant.RC_POINTS, EMPTY);

        login();
        new EnrollmentPage().enroll(member);
    }

    @Test
    public void depositVoucher()
    {
        new VoucherPopUp().getVoucherDepositTab().deposit(voucherNumber);
        verifyThat(new MessageBox(MessageBoxType.SUCCESS), hasText("Voucher has been successfully deposited."));
    }

    @Test(dependsOnMethods = { "depositVoucher" })
    public void verifyVoucherDepositEvent()
    {
        allEventsPage.goTo();

        AllEventsGridRow row = allEventsPage.getGrid().getRow(VOUCHER_DEPOSIT);
        verifyThat(row, displayed(true));

        VoucherGridRowContainer rowContainer = row.expand(VoucherGridRowContainer.class);

        rowContainer.getVoucherDetails().verify(voucherNumber, helper);

        rowContainer.clickDetails();
        VoucherDetailsPopUp voucherDetailsPopUp = new VoucherDetailsPopUp();

        // Voucher Details tab
        voucherDetailsPopUp.getVoucherDetailsTab().getVoucherDetails().verify(voucherNumber, helper);

        // Earning Details tab
        EventEarningDetailsTab earningDetailsTab = voucherDetailsPopUp.getEarningDetailsTab();
        earningDetailsTab.goTo();
        earningDetailsTab.verifyBasicDetailsNoPointsAndMiles();
        earningDetailsTab.getGrid().verifyRowsFoundByType(baseAwardEvent, baseUnitsEvent);

        // Billing Details tab
        EventBillingDetailsTab billingDetailsTab = voucherDetailsPopUp.getBillingDetailsTab();
        billingDetailsTab.goTo();
        billingDetailsTab.verifyTabIsEmpty("VOUCHER");

        voucherDetailsPopUp.clickClose();
    }

    @Test(dependsOnMethods = { "depositVoucher" })
    public void verifySystemOrderEvent()
    {
        allEventsPage.goTo();

        AllEventsGridRow row = allEventsPage.getGrid().getRow(ORDER_SYSTEM);
        verifyThat(row, displayed(true));

        OrderSystemDetailsGridRowContainer rowContainer = row.expand(OrderSystemDetailsGridRowContainer.class);

        rowContainer.verify(order);

        rowContainer.clickDetails();
        OrderDetailsPopUp orderDetailsPopUp = new OrderDetailsPopUp();

        // Voucher Details tab
        orderDetailsPopUp.getOrderDetailsTab().verify(order);

        // Earning Details tab
        EventEarningDetailsTab earningDetailsTab = orderDetailsPopUp.getEarningDetailsTab();
        earningDetailsTab.goTo();
        earningDetailsTab.verifyBasicDetailsNoPointsAndMiles();
        earningDetailsTab.getGrid().verify(baseAwardEvent);

        // Billing Details tab
        EventBillingDetailsTab billingDetailsTab = orderDetailsPopUp.getBillingDetailsTab();
        billingDetailsTab.goTo();
        billingDetailsTab.verifyTabIsEmpty(ORDER_SYSTEM);

        orderDetailsPopUp.clickClose();
    }

    @Test(dependsOnMethods = { "verifyVoucherDepositEvent", "verifySystemOrderEvent" }, alwaysRun = true)
    public void verifyResultingTierLevel()
    {
        final RewardClubLevel GOLD = RewardClubLevel.GOLD;

        RewardClubPage rewardClubPage = new RewardClubPage();
        rewardClubPage.goTo();
        rewardClubPage.getSummary().verify(ProgramStatus.OPEN, GOLD, 1);
        verifyThat(new LeftPanel().getProgramInfoPanel().getPrograms().getRewardClubProgram().getLevelCode(),
                hasText(GOLD.getValue()));
    }
}
