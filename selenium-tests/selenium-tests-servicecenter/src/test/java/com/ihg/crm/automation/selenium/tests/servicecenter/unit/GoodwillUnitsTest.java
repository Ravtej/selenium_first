package com.ihg.crm.automation.selenium.tests.servicecenter.unit;

import static com.ihg.automation.selenium.assertions.MessageBoxVerifierFactory.verifySuccess;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.assertNoErrors;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.common.Constant.RC_POINTS;
import static com.ihg.automation.selenium.common.event.EventTransactionType.Goodwill.GOODWILL;
import static com.ihg.automation.selenium.common.event.EventTransactionType.Goodwill.GOODWILL_CODE;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.isDisplayedWithWait;

import org.apache.commons.lang3.StringUtils;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ihg.automation.common.DateUtils;
import com.ihg.automation.selenium.common.billing.EventBillingDetailsTab;
import com.ihg.automation.selenium.common.event.AllEventsRow;
import com.ihg.automation.selenium.common.member.Member;
import com.ihg.automation.selenium.common.types.GoodwillReason;
import com.ihg.automation.selenium.common.types.program.ProgramStatus;
import com.ihg.automation.selenium.common.types.program.RewardClubLevel;
import com.ihg.automation.selenium.servicecenter.pages.annual.AnnualActivityPage;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsGridRow;
import com.ihg.automation.selenium.servicecenter.pages.event.all.AllEventsPage;
import com.ihg.automation.selenium.servicecenter.pages.event.goodwill.GoodwillDetailsPopUp;
import com.ihg.automation.selenium.servicecenter.pages.event.goodwill.GoodwillDetailsTab;
import com.ihg.automation.selenium.servicecenter.pages.event.goodwill.GoodwillGridRowContainer;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.RewardClubPage;
import com.ihg.automation.selenium.servicecenter.pages.unit.GoodwillPopUp;

public class GoodwillUnitsTest extends UnitsCommon
{
    private GoodwillGridRowContainer eventContainer;
    private GoodwillDetailsPopUp gdwillDetailsPopUp = new GoodwillDetailsPopUp();
    private Member recipient;

    private static final GoodwillReason GOODWILL_REASON = GoodwillReason.AGENT_ERROR;
    private static final int GOODWILL_POINTS = 21000;
    private static final String GOODWILL_POINTS_STRING = Integer.toString(GOODWILL_POINTS);

    private AllEventsRow goodWillEventRow = new AllEventsRow(DateUtils.getFormattedDate(), GOODWILL, RC_POINTS,
            GOODWILL_POINTS_STRING, StringUtils.EMPTY);

    @BeforeClass
    public void beforeClass()
    {
        login();
        recipient = new AnnualActivityPage().captureCounters(enrollMember(), jdbcTemplate);
    }

    @Test
    public void goodwillPoints()
    {
        GoodwillPopUp goodwillPopUp = new GoodwillPopUp();

        goodwillPopUp.goTo();
        assertNoErrors();

        goodwillPopUp.getAmount().type(GOODWILL_POINTS_STRING);
        goodwillPopUp.getUnitType().select(RC_POINTS);
        goodwillPopUp.getReason().selectByCode(GOODWILL_REASON);
        goodwillPopUp.clickSubmitNoError(verifySuccess(GoodwillPopUp.GOODWILL_SUCCESS));
    }

    @Test(dependsOnMethods = { "goodwillPoints" }, priority = 10)
    public void verifyActivityCounters()
    {
        recipient.getLifetimeActivity().setAdjustedUnits(GOODWILL_POINTS);
        recipient.getLifetimeActivity().setTotalUnits(GOODWILL_POINTS);
        recipient.getLifetimeActivity().setCurrentBalance(GOODWILL_POINTS);
        recipient.getAnnualActivity().setTotalUnits(GOODWILL_POINTS);

        new AnnualActivityPage().verifyCounters(recipient);
    }

    @Test(dependsOnMethods = { "goodwillPoints" }, priority = 10)
    public void verifyTierLevel()
    {
        RewardClubPage rewardClubPage = new RewardClubPage();
        rewardClubPage.goTo();
        rewardClubPage.getSummary().verify(ProgramStatus.OPEN, RewardClubLevel.CLUB);
    }

    @Test(dependsOnMethods = { "goodwillPoints" }, priority = 20)
    public void verifyGoodwillEventRow()
    {
        AllEventsPage allEventsPage = new AllEventsPage();
        allEventsPage.goTo();

        AllEventsGridRow row = allEventsPage.getGrid().getRow(1);
        row.verify(goodWillEventRow);

        eventContainer = row.expand(GoodwillGridRowContainer.class);
        eventContainer.getGoodwillDetails().verify(GOODWILL_REASON, helper);
    }

    @Test(dependsOnMethods = { "goodwillPoints" }, priority = 30)
    public void openGoodwillEventDetailsPopUp()
    {
        eventContainer.clickDetails();
        verifyThat(gdwillDetailsPopUp, isDisplayedWithWait());
    }

    @Test(dependsOnMethods = { "openGoodwillEventDetailsPopUp" })
    public void verifyEventGoodwillDetailsTab()
    {
        GoodwillDetailsTab tab = gdwillDetailsPopUp.getGoodwillDetailsTab();
        tab.goTo();
        tab.getGoodwillDetails().verify(GOODWILL_REASON, helper);
    }

    @Test(dependsOnMethods = { "openGoodwillEventDetailsPopUp" })
    public void verifyEventEarningDetailsTab()
    {
        gdwillDetailsPopUp.getEarningDetailsTab().verifyForBaseUnitsEvent(GOODWILL_POINTS, 0);
    }

    @Test(dependsOnMethods = { "openGoodwillEventDetailsPopUp" })
    public void verifyEventBillingDetailsTab()
    {
        EventBillingDetailsTab tab = gdwillDetailsPopUp.getBillingDetailsTab();
        tab.goTo();
        tab.verifyTabIsEmpty(GOODWILL_CODE);
    }

}
