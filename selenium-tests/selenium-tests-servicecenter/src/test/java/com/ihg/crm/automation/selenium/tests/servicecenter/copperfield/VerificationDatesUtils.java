package com.ihg.crm.automation.selenium.tests.servicecenter.copperfield;

import static com.ihg.automation.common.DateUtils.DATE_FORMAT;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasDefault;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasText;
import static com.ihg.automation.selenium.servicecenter.pages.programs.rc.RewardClubSummary.EXPIRATION_MONTH_SHIFT;

import org.joda.time.LocalDate;

import com.ihg.automation.selenium.servicecenter.pages.LeftPanel;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.RewardClubPage;
import com.ihg.automation.selenium.servicecenter.pages.programs.rc.RewardClubSummary;

public class VerificationDatesUtils
{
    public static void verifyDatesWithShiftedExpiration(LocalDate lastActivityDate)
    {
        RewardClubPage rewardClubPage = new RewardClubPage();
        rewardClubPage.goTo();
        RewardClubSummary summary = rewardClubPage.getSummary();

        String expirationDate = lastActivityDate.plusMonths(EXPIRATION_MONTH_SHIFT).toString(DATE_FORMAT);
        verifyThat(summary.getBalanceExpirationDate(), hasText(expirationDate));
        verifyThat(summary.getLastActivityDate(), hasText(lastActivityDate.toString(DATE_FORMAT)));
        verifyThat(summary.getLastAdjusted(), hasDefault());

        verifyThat(new LeftPanel().getProgramInfoPanel().getPrograms().getRewardClubProgram().getExpirationDate(),
                hasText(expirationDate));
    }

    public static void verifyDatesWithDefaultExpiration(LocalDate lastActivityDate)
    {
        RewardClubPage rewardClubPage = new RewardClubPage();
        rewardClubPage.goTo();
        RewardClubSummary summary = rewardClubPage.getSummary();

        verifyThat(summary.getBalanceExpirationDate(), hasDefault());
        verifyThat(summary.getLastActivityDate(), hasText(lastActivityDate.toString(DATE_FORMAT)));
        verifyThat(summary.getLastAdjusted(), hasDefault());

        verifyThat(new LeftPanel().getProgramInfoPanel().getPrograms().getRewardClubProgram().getExpirationDate(),
                displayed(false));
    }
}
