package com.ihg.automation.selenium.assertions;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

import org.testng.annotations.Test;

import com.ihg.automation.selenium.gwt.components.MessageBox;
import com.ihg.automation.selenium.gwt.components.MessageBoxType;
import com.ihg.automation.selenium.matchers.component.ComponentMatcher;

public class MessageBoxVerifierTest
{

    @Test
    public void is()
    {
        MessageBoxVerifier messageBoxVerifier = getMessageBoxVerifier();

        assertTrue(messageBoxVerifier.isVerify(true), "Verify with preconditions");
        assertTrue(messageBoxVerifier.isVerify(false), "Verify without preconditions");
    }

    @Test
    public void doNotVerifyIfAlreadyVerified()
    {
        MessageBoxVerifier messageBoxVerifier = getMessageBoxVerifier();
        messageBoxVerifier.setVerifiedAtLeastOnce(true);

        assertFalse(messageBoxVerifier.isVerify(true), "Do not verify with preconditions if it has already verified");
        assertFalse(messageBoxVerifier.isVerify(false),
                "Do not verify without preconditions if it has already verified");
    }

    @Test
    public void verifyIfAlreadyVerifiedButRepeated()
    {
        MessageBoxVerifier messageBoxVerifier = getMessageBoxVerifier();

        messageBoxVerifier.setRepeatVerification(true);
        messageBoxVerifier.setVerifiedAtLeastOnce(true);

        assertTrue(messageBoxVerifier.isVerify(true),
                "Verify with preconditions if it has already verified and repeat is true");
        assertTrue(messageBoxVerifier.isVerify(false),
                "Verify without preconditions if it has already verified and repeat is true");
    }

    @Test
    public void preconditionSetButNotExecuted()
    {
        MessageBoxVerifier messageBoxVerifier = new MessageBoxVerifier(MessageBoxType.WARNING,
                ComponentMatcher.displayed(true, false), ComponentMatcher.hasEmptyOrNullText());

        assertTrue(messageBoxVerifier.isVerify(false), "Verify without preconditions");
    }

    @Test
    public void preconditionIsTrue()
    {
        MessageBoxVerifier messageBoxVerifier = getMessageBoxVerifierWithPrecondition();

        MessageBox messageBox = mock(MessageBox.class);
        when(messageBox.isDisplayed(false)).thenReturn(true);
        messageBoxVerifier.setMessageBox(messageBox);

        assertTrue(messageBoxVerifier.isVerify(true), "Precondition (true)");
    }

    @Test
    public void preconditionIsFalse()
    {
        MessageBoxVerifier messageBoxVerifier = getMessageBoxVerifierWithPrecondition();

        MessageBox messageBox = mock(MessageBox.class);
        when(messageBox.isDisplayed(false)).thenReturn(false);
        messageBoxVerifier.setMessageBox(messageBox);

        assertFalse(messageBoxVerifier.isVerify(true), "Precondition (false)");
    }

    private MessageBoxVerifier getMessageBoxVerifier()
    {
        return new MessageBoxVerifier(MessageBoxType.WARNING, null, ComponentMatcher.hasEmptyOrNullText());
    }

    private MessageBoxVerifier getMessageBoxVerifierWithPrecondition()
    {
        return new MessageBoxVerifier(MessageBoxType.WARNING, ComponentMatcher.displayed(true, false),
                ComponentMatcher.hasEmptyOrNullText());
    }
}
