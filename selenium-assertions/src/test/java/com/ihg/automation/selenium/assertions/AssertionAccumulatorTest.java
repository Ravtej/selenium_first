package com.ihg.automation.selenium.assertions;

import static com.ihg.automation.selenium.matchers.HasSameItemsAsCollection.hasSameItemsAsCollection;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.enabled;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasDefault;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasDefaultOrText;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasEmptyText;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasTextAsDouble;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasTextAsInt;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasTextWithWait;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.isDisplayedAndEnabled;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.isHighlightedAsInvalid;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.isSelected;
import static com.ihg.automation.selenium.matchers.component.ViewableMatcher.hasText;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNot.not;
import static org.hamcrest.core.StringStartsWith.startsWith;
import static org.hamcrest.text.IsEmptyString.isEmptyOrNullString;

import java.util.Collection;

import org.hamcrest.Matcher;
import org.hamcrest.core.IsNull;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.ihg.automation.selenium.gwt.components.Mode;
import com.ihg.automation.selenium.matchers.component.HasText;

public class AssertionAccumulatorTest
{

    @DataProvider
    public static Object[][] skippedMatchers()
    {
        Object nullValue = null;
        Collection nullCollection = null;
        String nullString = null;
        Integer nullInteger = null;
        Double nullDouble = null;
        return new Object[][] { { is(nullValue) }, { not(is(nullValue)) }, { is(not(nullValue)) },
                { HasText.hasText(nullString) }, { not(HasText.hasText(nullString)) },
                { HasText.hasText(not(nullString)) }, { hasText(nullString, Mode.EDIT) },
                { hasText(nullString, Mode.VIEW) }, { hasTextAsInt(nullInteger) }, { hasTextAsDouble(nullDouble) },
                { hasTextWithWait(nullString) }, { hasSameItemsAsCollection(nullCollection) } };
    }

    @Test(dataProvider = "skippedMatchers")
    public void nullMatcher(Matcher<?> matcher)
    {
        Assert.assertTrue(SeleniumAssertionUtils.isNullExpectedValue(matcher));
    }

    @DataProvider
    public static Object[][] notSkippedMatchers()
    {
        String emptyString = "";
        String nullString = null;
        return new Object[][] { { is(emptyString) }, { not(is(emptyString)) }, { is(not(emptyString)) },
                { startsWith(emptyString) }, { startsWith(nullString) }, { HasText.hasText(emptyString) },
                { not(HasText.hasText(emptyString)) }, { isEmptyOrNullString() }, { IsNull.nullValue() }, //
                { hasEmptyText() }, { hasText(emptyString, Mode.EDIT) }, { hasText(emptyString, Mode.VIEW) },
                { hasTextAsInt(0) }, { hasTextAsDouble(0.0) }, { hasTextWithWait(emptyString) }, //
                { hasDefault() }, { hasDefaultOrText(emptyString) }, //
                { displayed(false) }, { displayed(true) }, //
                { enabled(false) }, { enabled(true) }, //
                { isDisplayedAndEnabled(false) }, { isDisplayedAndEnabled(true) }, //
                { isSelected(false) }, { isSelected(true) }, //
                { isHighlightedAsInvalid(false) }, { isHighlightedAsInvalid(true) } //
        };
    }

    @Test(dataProvider = "notSkippedMatchers")
    public void notNullMatcher(Matcher<?> matcher)
    {
        Assert.assertFalse(SeleniumAssertionUtils.isNullExpectedValue(matcher));
    }

}
