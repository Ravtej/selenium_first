package com.ihg.automation.selenium.assertions;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.OutputType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ihg.automation.selenium.runtime.ScreenException;

public class ScreenUtils
{
    private static final Logger logger = LoggerFactory.getLogger(ScreenUtils.class);

    private static void createDirectory(Path screenDir) throws IOException
    {
        if (!Files.exists(screenDir))
        {
            Files.createDirectories(screenDir);
        }
    }

    public static Path save(final String screenshotBase64, final Path screenDir)
    {
        if (StringUtils.isEmpty(screenshotBase64))
        {
            logger.warn("Impossible to save screenshot due to it is null");
            return null;
        }

        Path tempScreen = OutputType.FILE.convertFromBase64Png(screenshotBase64).toPath();

        Path screenshotPath = screenDir.resolve(tempScreen.getFileName());
        try
        {
            logger.debug("Save {} screenshot to {}", tempScreen.getFileName(), screenDir.toAbsolutePath());
            createDirectory(screenDir);
            Files.move(tempScreen, screenshotPath);
        }
        catch (IOException e)
        {
            logger.error("Save screenshot exception", e);
            return null;
        }

        return screenshotPath;
    }

    public static void save(ScreenException exception, final Path screenDir)
    {
        exception.setScreenshotPath(save(exception.getScreenshotBase64(), screenDir));
    }
}
