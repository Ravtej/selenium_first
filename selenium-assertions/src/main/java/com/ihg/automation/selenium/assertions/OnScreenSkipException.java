package com.ihg.automation.selenium.assertions;

import java.nio.file.Path;

import org.testng.SkipException;

import com.ihg.automation.selenium.runtime.ScreenException;

public class OnScreenSkipException extends SkipException implements ScreenException
{
    private String screenshotBase64;
    private Path screenshotPath;

    public OnScreenSkipException(String skipMessage, String screenshotBase64)
    {
        super(skipMessage);
        this.screenshotBase64 = screenshotBase64;
    }

    @Override
    public String getScreenshotBase64()
    {
        return screenshotBase64;
    }

    @Override
    public void setScreenshotPath(Path screenshotPath)
    {
        this.screenshotPath = screenshotPath;
    }

    @Override
    public Path getScreenshotPath()
    {
        return screenshotPath;
    }

}
