package com.ihg.automation.selenium.assertions;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.ihg.automation.selenium.runtime.SeleniumException;

public class OnScreenException extends RuntimeException
{
    private static final long serialVersionUID = -4482354092023222496L;

    private List<SeleniumException> exceptions;

    public OnScreenException(List<SeleniumException> bullets)
    {
        this.exceptions = bullets;
    }

    public OnScreenException()
    {
        this.exceptions = new ArrayList<SeleniumException>();
    }

    public List<SeleniumException> getExceptionList()
    {
        return exceptions;
    }

    @Override
    public String getMessage()
    {
        if (exceptions.isEmpty())
        {
            return null;
        }

        StringBuilder description = new StringBuilder();
        Iterator<SeleniumException> iterator = exceptions.iterator();
        while (iterator.hasNext())
        {
            description.append(iterator.next().getMessage());
            if (iterator.hasNext())
            {
                description.append("\n");
            }
        }

        return description.toString();
    }

}
