package com.ihg.automation.selenium.assertions;

import java.util.ArrayList;
import java.util.List;

import com.ihg.automation.selenium.runtime.SeleniumException;

public class AssertionAccumulator
{
    private static ThreadLocal<List<SeleniumException>> exceptions = new ThreadLocal<List<SeleniumException>>();

    public static List<SeleniumException> getExceptions()
    {
        if (null == exceptions.get())
        {
            exceptions.set(new ArrayList<SeleniumException>());
        }

        return exceptions.get();
    }

    /**
     * <b>Note:</b> Method clear stack of errors
     */
    public static List<SeleniumException> getExceptionsAndClear()
    {
        List<SeleniumException> toReturn = new ArrayList<SeleniumException>(getExceptions());
        getExceptions().clear();

        return toReturn;
    }

    public static void verify()
    {
        List<SeleniumException> exceptionList = getExceptionsAndClear();
        if (!exceptionList.isEmpty())
        {
            throw new OnScreenException(exceptionList);
        }
    }
}
