package com.ihg.automation.selenium.assertions;

import org.apache.commons.lang3.StringUtils;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.StringDescription;
import org.openqa.selenium.OutputType;

import com.ihg.automation.selenium.gwt.components.Componentable;
import com.ihg.automation.selenium.matchers.component.HighlightDifferent;
import com.ihg.automation.selenium.runtime.Cameron;

class AssertionUtils
{
    private AssertionUtils()
    {
    }

    static <T> String getDescription(T actual, Matcher<T> matcher, String reason)
    {
        Description description = new StringDescription();
        description.appendText(reason);
        description.appendText("\nExpected: ");
        matcher.describeTo(description);
        description.appendText("\n     got: ");
        matcher.describeMismatch(actual, description);

        return description.toString();
    }

    static <T extends Componentable> String getComponentSelfDescribedReason(T actual, Matcher<T> matcher, String reason)
    {
        Description description = new StringDescription();
        description.appendText(actual.getDescription()).appendText(" ").appendDescriptionOf(matcher);
        if (StringUtils.isNotEmpty(reason))
        {
            description.appendText(". ").appendText(reason);
        }
        return description.toString();
    }

    static <C extends Componentable, T> String getScreenshot(C component, T actual, Matcher<T> matcher)
    {
        Componentable highlightComponent = component;
        if (matcher instanceof HighlightDifferent)
        {
            highlightComponent = ((HighlightDifferent<?>) matcher).getComponent((Componentable) actual);
        }

        if (highlightComponent == null)
        {
            return new Cameron().getScreenshotAs(OutputType.BASE64);
        }

        return highlightComponent.getScreenshotAs(OutputType.BASE64);
    }

}
