package com.ihg.automation.selenium.assertions;

import org.hamcrest.Matcher;

import com.ihg.automation.selenium.gwt.components.Componentable;
import com.ihg.automation.selenium.gwt.components.MessageBoxType;
import com.ihg.automation.selenium.matchers.component.ComponentMatcher;

public class MessageBoxVerifierFactory
{
    public static MessageBoxVerifier verifyNoError()
    {
        MessageBoxVerifier errorVerifier = new MessageBoxVerifier(MessageBoxType.WARNING, null,
                ComponentMatcher.hasEmptyOrNullText());
        errorVerifier.setRepeatVerification(true);
        errorVerifier.setCheckErrorDetails(true);
        errorVerifier.setReason("No errors are expected");

        return errorVerifier;
    }

    public static MessageBoxVerifier assertNoError()
    {
        MessageBoxVerifier errorVerifier = verifyNoError();
        errorVerifier.setAssertNow(true);

        return errorVerifier;
    }

    public static MessageBoxVerifier verifySuccess(final String text)
    {
        return verifySuccess(ComponentMatcher.hasText(text));
    }

    public static MessageBoxVerifier verifySuccess(final Matcher<Componentable> matcher)
    {
        return verifyText(MessageBoxType.SUCCESS, matcher);
    }

    public static MessageBoxVerifier verifyError(final Matcher<Componentable> matcher)
    {
        return verifyText(MessageBoxType.WARNING, matcher);
    }

    public static MessageBoxVerifier verifyInfo(final Matcher<Componentable> matcher)
    {
        return verifyText(MessageBoxType.INFO, matcher);
    }

    public static MessageBoxVerifier verifyInfo(final String text)
    {
        return verifyInfo(ComponentMatcher.hasText(text));
    }

    public static MessageBoxVerifier verifyError(final String text)
    {
        return verifyError(ComponentMatcher.hasText(text));
    }

    public static MessageBoxVerifier verifyStaticError(final String text)
    {
        MessageBoxVerifier verifier = verifyText(MessageBoxType.INFO, ComponentMatcher.hasText(text));
        verifier.setClosable(false);
        return verifier;
    }

    private static MessageBoxVerifier verifyText(final MessageBoxType messageBox, final Matcher<Componentable> matcher)
    {
        return new MessageBoxVerifier(messageBox, ComponentMatcher.displayed(true, false), matcher);
    }
}
