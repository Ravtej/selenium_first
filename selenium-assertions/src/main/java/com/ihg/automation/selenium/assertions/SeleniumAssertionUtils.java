package com.ihg.automation.selenium.assertions;

import static com.ihg.automation.selenium.matchers.HasSameItemsAsCollection.hasSameItemsAsCollection;
import static com.ihg.automation.selenium.matchers.component.HasText.hasText;
import static java.text.MessageFormat.format;

import java.util.Collection;

import org.apache.commons.lang3.reflect.FieldUtils;
import org.hamcrest.Matcher;
import org.joda.time.LocalDate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ihg.automation.selenium.gwt.components.Componentable;
import com.ihg.automation.selenium.gwt.components.Mode;
import com.ihg.automation.selenium.gwt.components.input.DateInput;
import com.ihg.automation.selenium.matchers.component.ModeMatcher;
import com.ihg.automation.selenium.runtime.SeleniumException;

public class SeleniumAssertionUtils
{
    private static final Logger logger = LoggerFactory.getLogger(SeleniumAssertionUtils.class);

    public static <C extends Componentable, T> boolean assertThat(C component, T actual, Matcher<T> matcher,
            String reason, boolean isAssertNow)
    {
        boolean isSuccess = true;

        reason = (isAssertNow ? "Assert " : "Verify ") + reason;

        if ((matcher != null) && (!isNullExpectedValue(matcher)))
        {
            isSuccess = matcher.matches(actual);

            if (!isSuccess)
            {
                String description = AssertionUtils.getDescription(actual, matcher, reason);
                AssertionAccumulator.getExceptions().add(
                        new SeleniumException(description, AssertionUtils.getScreenshot(component, actual, matcher)));
                logger.error("***FAILED*** {}", description);
                isSuccess = false;

                if (isAssertNow)
                {
                    AssertionAccumulator.verify();
                }
            }
            else
            {
                logger.debug("***PASSED*** {}", reason);
            }
        }
        else
        {
            logger.debug("***SKIPPED*** {} {}", reason, "due to matcher/expected value is null");
        }
        return isSuccess;
    }

    public static <C extends Componentable, T> boolean verifyThat(C component, T actual, Matcher<T> matcher,
            String reason)
    {
        return assertThat(component, actual, matcher, reason, false);
    }

    public static <C extends Componentable, T> boolean assertThat(C component, T actual, Matcher<T> matcher,
            String reason)
    {
        return assertThat(component, actual, matcher, reason, true);
    }

    static <T extends Componentable> boolean assertThat(T actual, Matcher<T> matcher, String reason,
            boolean isAssertNow)
    {
        return assertThat(actual, actual, matcher,
                AssertionUtils.getComponentSelfDescribedReason(actual, matcher, reason), isAssertNow);
    }

    public static <T extends Componentable> boolean verifyThat(T actual, Matcher<T> matcher)
    {
        return assertThat(actual, matcher, null, false);
    }

    public static <T extends Componentable> boolean verifyThat(T actual, Matcher<T> matcher, String reason)
    {
        return assertThat(actual, matcher, reason, false);
    }

    public static <T extends Componentable> boolean assertThat(T actual, Matcher<T> matcher)
    {
        return assertThat(actual, matcher, null, true);
    }

    public static <T extends Componentable> boolean assertThat(T actual, Matcher<T> matcher, String reason)
    {
        return assertThat(actual, matcher, reason, true);
    }

    static <T> boolean isNullExpectedValue(Matcher<T> matcher)
    {
        try
        {
            Object expectedValue = FieldUtils.readField(matcher, "expectedValue", true);
            return (expectedValue == null);
        }
        catch (Exception e)
        {
        }

        try
        {
            Object expectedList = FieldUtils.readField(matcher, "expectedList", true);
            return (expectedList == null);
        }
        catch (Exception e)
        {
        }

        try
        {
            @SuppressWarnings("unchecked")
            Matcher<T> innerMatcher = (Matcher<T>) FieldUtils.readField(matcher, "matcher", true);
            return (innerMatcher != null) && isNullExpectedValue(innerMatcher);
        }
        catch (Exception e)
        {
        }

        return false;
    }

    public static void verifyDateInView(DateInput input, LocalDate localDate)
    {
        verifyDate(input, localDate, Mode.VIEW);
    }

    public static void verifyDate(DateInput component, LocalDate localDate, Mode mode)
    {
        Matcher<Componentable> matcher = hasText(localDate != null ? localDate.toString(component.getFormat()) : null);
        verifyThat(component, ModeMatcher.inMode(matcher, mode));
    }

    public static void assertNoErrors()
    {
        MessageBoxVerifierFactory.assertNoError().verify(false);
    }

    public static void verifyNoErrors()
    {
        MessageBoxVerifierFactory.verifyNoError().verify(false);
    }

    public static void compareLists(Componentable component, Collection<String> expectedList,
            Collection<String> actualList)
    {
        verifyThat(component, actualList, hasSameItemsAsCollection(expectedList),
                format("Verify that all expected items exist in {0}", component.getDescription()));
    }

}
