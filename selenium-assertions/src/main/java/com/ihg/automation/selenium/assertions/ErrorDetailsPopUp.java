package com.ihg.automation.selenium.assertions;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.displayed;

import org.openqa.selenium.Keys;
import org.openqa.selenium.interactions.Actions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ihg.automation.selenium.gwt.components.Button;
import com.ihg.automation.selenium.gwt.components.Component;
import com.ihg.automation.selenium.gwt.components.PopUpWindow;
import com.ihg.automation.selenium.gwt.components.utils.ByText;
import com.ihg.automation.selenium.gwt.components.utils.FindStepUtils;
import com.ihg.automation.selenium.runtime.Selenium;

public class ErrorDetailsPopUp extends PopUpWindow
{
    private Logger logger = LoggerFactory.getLogger(ErrorDetailsPopUp.class);

    public ErrorDetailsPopUp()
    {
        super(ByText.exactSelf("Error Details"));
    }

    @Override
    public String getText(boolean isWait)
    {
        Component textArea = new Component(this, "Text Area", getName(), FindStepUtils.byXpath(".//textarea"));
        return textArea.getAttribute("value");
    }

    public void open()
    {
        if (!isDisplayed())
        {
            Actions action = new Actions(Selenium.getInstance().getDriver());
            action.keyDown(Keys.CONTROL).keyDown(Keys.ALT).sendKeys("R").perform();

            Button errorButton = new Button(null, "Report Error");
            if (!isDisplayed() && errorButton.isDisplayed())
            {
                logger.debug(
                        "The error details were not opened by clicking the keys combinations. Try to open error details by clicking on 'Report Error' button.");
                errorButton.clickAndWait();
            }
            verifyThat(this, displayed(true));
        }
    }

    public void close()
    {
        if (isDisplayed())
        {
            getButton(Button.CLOSE).clickAndWait();
            verifyThat(this, displayed(false));
        }
    }
}
