package com.ihg.automation.selenium.assertions;

import org.hamcrest.Matcher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.SkipException;

import com.ihg.automation.selenium.gwt.components.Componentable;

public class Assume
{
    private static final Logger logger = LoggerFactory.getLogger(SeleniumAssertionUtils.class);

    public static <T> void assumeThat(T actual, Matcher<T> matcher, String reason)
    {
        if (!matcher.matches(actual))
        {
            String description = AssertionUtils.getDescription(actual, matcher, reason);
            logger.error("***FAILED*** {}", description);
            throw new SkipException(description);
        }
        else
        {
            logger.debug("***PASSED*** {}", reason);
        }
    }

    public static <C extends Componentable, T> void assumeThat(C component, T actual, Matcher<T> matcher, String reason)
    {
        if (!matcher.matches(actual))
        {
            String description = AssertionUtils.getDescription(actual, matcher, reason);
            logger.error("***FAILED*** {}", description);
            throw new OnScreenSkipException(description, AssertionUtils.getScreenshot(component, actual, matcher));
        }
        else
        {
            logger.debug("***PASSED*** {}", reason);
        }
    }

    public static <T extends Componentable> void assumeThat(T actual, Matcher<T> matcher, String reason)
    {
        assumeThat(actual, actual, matcher, AssertionUtils.getComponentSelfDescribedReason(actual, matcher, reason));
    }

}
