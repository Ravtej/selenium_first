package com.ihg.automation.selenium.assertions;

import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.assertThat;
import static com.ihg.automation.selenium.assertions.SeleniumAssertionUtils.verifyThat;
import static com.ihg.automation.selenium.matchers.component.ComponentMatcher.hasEmptyOrNullText;

import org.hamcrest.Matcher;

import com.ihg.automation.selenium.gwt.components.Componentable;
import com.ihg.automation.selenium.gwt.components.MessageBox;
import com.ihg.automation.selenium.gwt.components.MessageBoxType;
import com.ihg.automation.selenium.listener.Verifier;

public class MessageBoxVerifier extends Verifier
{
    private MessageBox messageBox;
    private Matcher<Componentable> precondition;
    private Matcher<Componentable> condition;

    private boolean repeatVerification = false;
    private boolean isVerifiedAtLeastOnce = false;

    private boolean assertNow = false;
    private boolean checkErrorDetails;
    private String reason;

    private boolean isClosable = true;

    MessageBoxVerifier(MessageBoxType messageBoxType, Matcher<Componentable> precondition,
            Matcher<Componentable> condition)
    {
        this.messageBox = new MessageBox(messageBoxType);
        this.precondition = precondition;
        this.condition = condition;
    }

    void setRepeatVerification(boolean repeatVerification)
    {
        this.repeatVerification = repeatVerification;
    }

    void setVerifiedAtLeastOnce(boolean isVerifiedAtLeastOnce)
    {
        this.isVerifiedAtLeastOnce = isVerifiedAtLeastOnce;
    }

    void setMessageBox(MessageBox messageBox)
    {
        this.messageBox = messageBox;
    }

    void setAssertNow(boolean assertNow)
    {
        this.assertNow = assertNow;
    }

    public void setCheckErrorDetails(boolean checkErrorDetails)
    {
        this.checkErrorDetails = checkErrorDetails;
    }

    void setReason(String reason)
    {
        this.reason = reason;
    }

    public void setClosable(boolean isClosable)
    {
        this.isClosable = isClosable;
    }

    @Override
    public void verify(boolean checkPrecondition)
    {
        if (isVerify(checkPrecondition))
        {
            isVerifiedAtLeastOnce = true;
            boolean result = false;
            try
            {
                result = assertThat(messageBox, condition, reason, assertNow);
            }
            finally
            {
                if (!result && checkErrorDetails & isClosable)
                {
                    verifyErrorDetails();
                }

                if (messageBox.isInitialized() & isClosable)
                {
                    messageBox.close(false);
                }
            }
        }
    }

    boolean isVerify(boolean checkPrecondition)
    {
        if ((isVerifiedAtLeastOnce) && (!repeatVerification))
        {
            return false;
        }

        return (precondition == null) || (!checkPrecondition) || (precondition.matches(messageBox));
    }

    private void verifyErrorDetails()
    {
        ErrorDetailsPopUp errorDetailsPopUp = new ErrorDetailsPopUp();
        errorDetailsPopUp.open();
        try
        {
            verifyThat(errorDetailsPopUp, hasEmptyOrNullText());
        }
        finally
        {
            errorDetailsPopUp.close();
        }
    }
}
